import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:ad1ms2_dev/shared/form_m_occupation_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class OccupationProfesionalWidget extends StatefulWidget {
  @override
  _OccupationProfesionalWidgetState createState() => _OccupationProfesionalWidgetState();
}

class _OccupationProfesionalWidgetState extends State<OccupationProfesionalWidget> {
  @override
  void initState() {
    super.initState();
    // if(Provider.of<FormMOccupationChangeNotif>(context,listen: false).listBusinessLocation.isEmpty)
    // Provider.of<FormMOccupationChangeNotif>(context,listen: false).getBusinessLocation();
  }
  @override
  Widget build(BuildContext context) {
    return Consumer<FormMOccupationChangeNotif>(
      builder: (context, formMOccupationChangeNotif, _) {
        return Column(
          children: [
           Visibility(
             visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isJenisProfesiProfesional(),
             child: TextFormField(
               autovalidate: formMOccupationChangeNotif.autoValidate,
               validator: (e) {
                 if (e.isEmpty && Provider.of<FormMOccupationChangeNotif>(context,listen: false).isSektorEkonomiProfesionalMandatory()) {
                   return "Tidak boleh kosong";
                 } else {
                   return null;
                 }
               },
               enabled: formMOccupationChangeNotif.isDisablePACIAAOSCONA ? false : true,
               readOnly: true,
               onTap: () {
                 FocusManager.instance.primaryFocus.unfocus();
                 formMOccupationChangeNotif.searchProfessionType(context);
               },
               controller: formMOccupationChangeNotif.controllerProfessionType,
               style: TextStyle(color: Colors.black),
               decoration: InputDecoration(
                   labelText: "Jenis Profesi",
                   filled: formMOccupationChangeNotif.isDisablePACIAAOSCONA,
                   fillColor: Colors.black12,
                   border: OutlineInputBorder(
                       borderRadius: BorderRadius.circular(8)),
                   enabledBorder: OutlineInputBorder(
                       borderSide: BorderSide(color: formMOccupationChangeNotif.editSektorEkonomiProfesional ? Colors.purple : Colors.grey)),
                   disabledBorder: OutlineInputBorder(
                       borderSide: BorderSide(color: formMOccupationChangeNotif.editSektorEkonomiProfesional ? Colors.purple : Colors.grey)),
                   contentPadding: EdgeInsets.symmetric(horizontal: 10)
               ),
             )
             // DropdownButtonFormField<ProfessionTypeModel>(
             //     autovalidate: formMOccupationChangeNotif.autoValidate,
             //     validator: (e) {
             //       if (e == null && Provider.of<FormMOccupationChangeNotif>(context,listen: false).isJenisProfesiProfesionalMandatory()) {
             //         return "Silahkan pilih pekerjaan";
             //       } else {
             //         return null;
             //       }
             //     },
             //     value: formMOccupationChangeNotif.professionTypeModelSelected,
             //     onChanged: (value) {
             //       formMOccupationChangeNotif.professionTypeModelSelected = value;
             //     },
             //     onTap: () {
             //       FocusManager.instance.primaryFocus.unfocus();
             //     },
             //     decoration: InputDecoration(
             //         labelText: "Jenis Profesi",
             //         border: OutlineInputBorder(
             //             borderRadius: BorderRadius.circular(8)),
             //         enabledBorder: OutlineInputBorder(
             //             borderSide: BorderSide(color: formMOccupationChangeNotif.editJenisProfesiProfesional ? Colors.purple : Colors.grey)),
             //         contentPadding: EdgeInsets.symmetric(horizontal: 10)
             //     ),
             //     // InputDecoration(
             //     //   labelText: "Jenis Profesi",
             //     //   border: OutlineInputBorder(),
             //     //   contentPadding: EdgeInsets.symmetric(horizontal: 10),
             //     // ),
             //     items: formMOccupationChangeNotif.listProfessionType.map((value) {
             //       return DropdownMenuItem<ProfessionTypeModel>(
             //         value: value,
             //         child: Text(
             //           value.desc,
             //           overflow: TextOverflow.ellipsis,
             //         ),
             //       );
             //     }).toList()),
           ),
           Visibility(
             visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isJenisProfesiProfesional(),
             child: SizedBox(height: MediaQuery.of(context).size.height / 47)
           ),
            Visibility(
              visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isSektorEkonomiProfesional(),
              child: TextFormField(
                autovalidate: formMOccupationChangeNotif.autoValidate,
                validator: (e) {
                  if (e.isEmpty && Provider.of<FormMOccupationChangeNotif>(context,listen: false).isSektorEkonomiProfesionalMandatory()) {
                    return "Tidak boleh kosong";
                  } else {
                    return null;
                  }
                },
                enabled: formMOccupationChangeNotif.isDisablePACIAAOSCONA ? false : true,
                readOnly: true,
                onTap: () {
                  FocusManager.instance.primaryFocus.unfocus();
                  formMOccupationChangeNotif.searchSectorEconomic(context);
                },
                controller: formMOccupationChangeNotif.controllerSectorEconomic,
                style: TextStyle(color: Colors.black),
                decoration: InputDecoration(
                    labelText: "Sektor Ekonomi",
                    filled: formMOccupationChangeNotif.isDisablePACIAAOSCONA,
                    fillColor: Colors.black12,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: formMOccupationChangeNotif.editSektorEkonomiProfesional ? Colors.purple : Colors.grey)),
                    disabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: formMOccupationChangeNotif.editSektorEkonomiProfesional ? Colors.purple : Colors.grey)),
                    contentPadding: EdgeInsets.symmetric(horizontal: 10)
                ),
                // InputDecoration(
                //     labelText: 'Sektor Ekonomi',
                //     labelStyle: TextStyle(color: Colors.black),
                //     border: OutlineInputBorder(
                //         borderRadius: BorderRadius.circular(8))),
              ),
            ),
           Visibility(
               visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isSektorEkonomiProfesional(),
               child: SizedBox(height: MediaQuery.of(context).size.height / 47)
           ),
           Visibility(
             visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isLapanganUsahaProfesional(),
             child: TextFormField(
               autovalidate: formMOccupationChangeNotif.autoValidate,
               validator: (e) {
                 if (e.isEmpty && Provider.of<FormMOccupationChangeNotif>(context,listen: false).isLapanganUsahaProfesionalMandatory()) {
                   return "Tidak boleh kosong";
                 } else {
                   return null;
                 }
               },
               readOnly: true,
               onTap: () {
                 FocusManager.instance.primaryFocus.unfocus();
                 formMOccupationChangeNotif.searchBusinesField(context);
               },
               controller: formMOccupationChangeNotif.controllerBusinessField,
               style: TextStyle(color: Colors.black),
               decoration: InputDecoration(
                   labelText: "Lapangan Usaha",
                   filled: formMOccupationChangeNotif.isDisablePACIAAOSCONA,
                   fillColor: Colors.black12,
                   border: OutlineInputBorder(
                       borderRadius: BorderRadius.circular(8)),
                   enabledBorder: OutlineInputBorder(
                       borderSide: BorderSide(color: formMOccupationChangeNotif.editLapanganUsahaProfesional ? Colors.purple : Colors.grey)),
                   disabledBorder: OutlineInputBorder(
                       borderSide: BorderSide(color: formMOccupationChangeNotif.editLapanganUsahaProfesional ? Colors.purple : Colors.grey)),
                   contentPadding: EdgeInsets.symmetric(horizontal: 10)
               ),
               // InputDecoration(
               //     labelText: 'Lapangan Usaha',
               //     labelStyle: TextStyle(color: Colors.black),
               //     border: OutlineInputBorder(
               //         borderRadius: BorderRadius.circular(8))),
               enabled: formMOccupationChangeNotif.isDisablePACIAAOSCONA ? false : formMOccupationChangeNotif.controllerSectorEconomic.text == "" ? false : true,
             ),
           ),
           Visibility(
               visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isLapanganUsahaProfesional(),
               child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
           Visibility(
             visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isStatusLokasiProfesional(),
             child: TextFormField(
                 readOnly: true,
                 autovalidate: formMOccupationChangeNotif.autoValidate,
                 validator: (e) {
                     if (e.isEmpty && formMOccupationChangeNotif.isStatusLokasiWiraswastaMandatory()) {
                         return "Tidak boleh kosong";
                     } else {
                         return null;
                     }
                 },
                 onTap: (){
                     FocusManager.instance.primaryFocus.unfocus();
                     formMOccupationChangeNotif.searchStatusLocation(context);
                 },
                 controller: formMOccupationChangeNotif.controllerStatusLocation,
                 style: TextStyle(color: Colors.black),
                 decoration: InputDecoration(
                     labelText: 'Status Lokasi',
                     labelStyle: TextStyle(color: Colors.black),
                     border: OutlineInputBorder(
                         borderRadius: BorderRadius.circular(8)
                     ),
                     enabledBorder: OutlineInputBorder(
                         borderSide: BorderSide(color: formMOccupationChangeNotif.editStatusLokasiProfesional ? Colors.purple : Colors.grey)),
                     disabledBorder: OutlineInputBorder(
                         borderSide: BorderSide(color: formMOccupationChangeNotif.editStatusLokasiProfesional ? Colors.purple : Colors.grey)),
                     contentPadding: EdgeInsets.symmetric(horizontal: 10)
                 ),
                 keyboardType: TextInputType.text,
                 textCapitalization: TextCapitalization.characters,
             )
           ),
            Visibility(
                visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isStatusLokasiProfesional(),
                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
            Visibility(
              visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isLokasiUsahaProfesional(),
              child: TextFormField(
                  readOnly: true,
                  autovalidate: formMOccupationChangeNotif.autoValidate,
                  validator: (e) {
                      if (e.isEmpty && formMOccupationChangeNotif.isLokasiUsahaWiraswastaMandatory()) {
                          return "Tidak boleh kosong";
                      } else {
                          return null;
                      }
                  },
                  onTap: (){
                      FocusManager.instance.primaryFocus.unfocus();
                      formMOccupationChangeNotif.searchBusinessLocation(context);
                  },
                  controller: formMOccupationChangeNotif.controllerBusinessLocation,
                  style: TextStyle(color: Colors.black),
                  decoration: InputDecoration(
                      labelText: 'Lokasi Usaha',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)
                      ),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMOccupationChangeNotif.editLokasiUsahaProfesional ? Colors.purple : Colors.grey)),
                      disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMOccupationChangeNotif.editLokasiUsahaProfesional ? Colors.purple : Colors.grey)),
                      contentPadding: EdgeInsets.symmetric(horizontal: 10)
                  ),
                  keyboardType: TextInputType.text,
                  textCapitalization: TextCapitalization.characters,
              )
            ),
            Visibility(
                visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isLokasiUsahaProfesional(),
                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
            Visibility(
              visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isTotalLamaBekerjaProfesional(),
              child: TextFormField(
                autovalidate: formMOccupationChangeNotif.autoValidate,
                validator: (e) {
                  if (e.isEmpty && Provider.of<FormMOccupationChangeNotif>(context,listen: false).isTotalLamaBekerjaProfesionalMandatory()) {
                    return "Tidak boleh kosong";
                  } else {
                    return null;
                  }
                },
                enabled: formMOccupationChangeNotif.isDisablePACIAAOSCONA ? false : true,
                controller: formMOccupationChangeNotif.controllerTotalEstablishedDate,
                style: TextStyle(color: Colors.black),
                decoration: InputDecoration(
                    labelText: "Total Lama Usaha/Kerja (bln)",
                    filled: formMOccupationChangeNotif.isDisablePACIAAOSCONA,
                    fillColor: Colors.black12,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: formMOccupationChangeNotif.editTotalLamaBekerjaProfesional ? Colors.purple : Colors.grey)),
                    disabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: formMOccupationChangeNotif.editTotalLamaBekerjaProfesional ? Colors.purple : Colors.grey)),
                    contentPadding: EdgeInsets.symmetric(horizontal: 10)
                ),
                // InputDecoration(
                //     labelText: 'Total Lama Usaha/Kerja (bln)',
                //     labelStyle: TextStyle(color: Colors.black),
                //     border: OutlineInputBorder(
                //         borderRadius: BorderRadius.circular(8))),
                keyboardType: TextInputType.number,
                inputFormatters: [
                  WhitelistingTextInputFormatter.digitsOnly,
                  // LengthLimitingTextInputFormatter(10),
                ],
              ),
            ),
          ],
        );
      },
    );
  }
}
