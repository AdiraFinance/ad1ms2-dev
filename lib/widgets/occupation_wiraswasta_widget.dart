import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:ad1ms2_dev/shared/form_m_occupation_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class OccupationWiraswastaWidget extends StatefulWidget {
  @override
  _OccupationWiraswastaWidgetState createState() => _OccupationWiraswastaWidgetState();
}

class _OccupationWiraswastaWidgetState extends State<OccupationWiraswastaWidget> {
  @override
  Widget build(BuildContext context) {
    return Consumer<FormMOccupationChangeNotif>(
      builder: (context, formMOccupationChangeNotif, _) {
        return Column(
          children: [
           Visibility(
             visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isNamaUsahaWiraswasta(),
             child: TextFormField(
               autovalidate: formMOccupationChangeNotif.autoValidate,
               validator: (e) {
                 if (e.isEmpty && Provider.of<FormMOccupationChangeNotif>(context,listen: false).isNamaUsahaWiraswastaMandatory()) {
                   return "Tidak boleh kosong";
                 } else {
                   return null;
                 }
               },
               controller: formMOccupationChangeNotif.controllerBusinessName,
               style: TextStyle(color: Colors.black),
               decoration: InputDecoration(
                   labelText: "Nama Usaha",
                   border: OutlineInputBorder(
                       borderRadius: BorderRadius.circular(8)),
                   enabledBorder: OutlineInputBorder(
                       borderSide: BorderSide(color: formMOccupationChangeNotif.editNamaUsahaWiraswasta ? Colors.purple : Colors.grey)),
                   disabledBorder: OutlineInputBorder(
                       borderSide: BorderSide(color: formMOccupationChangeNotif.editNamaUsahaWiraswasta ? Colors.purple : Colors.grey)),
                   contentPadding: EdgeInsets.symmetric(horizontal: 10)
               ),
               keyboardType: TextInputType.text,
               textCapitalization: TextCapitalization.characters,
             ),
           ),
           Visibility(
               child: SizedBox(height: MediaQuery.of(context).size.height / 47),
               visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isNamaUsahaWiraswasta(),
           ),
           Visibility(
             visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isJenisBadanUsahaWiraswasta(),
             child: DropdownButtonFormField<TypeOfBusinessModel>(
                 autovalidate: formMOccupationChangeNotif.autoValidate,
                 validator: (e) {
                   if (e == null && Provider.of<FormMOccupationChangeNotif>(context,listen: false).isJenisBadanUsahaWiraswastaMandatory()) {
                     return "Silahkan pilih jenis badan usaha";
                   } else {
                     return null;
                   }
                 },
                 value: formMOccupationChangeNotif.typeOfBusinessModelSelected,
                 onChanged: (value) {
                   formMOccupationChangeNotif.typeOfBusinessModelSelected =
                       value;
                 },
                 onTap: () {
                   FocusManager.instance.primaryFocus.unfocus();
                 },
                 decoration: InputDecoration(
                     labelText: "Jenis Badan Usaha",
                     border: OutlineInputBorder(
                         borderRadius: BorderRadius.circular(8)),
                     enabledBorder: OutlineInputBorder(
                         borderSide: BorderSide(color: formMOccupationChangeNotif.editJenisBadanUsahaWiraswasta ? Colors.purple : Colors.grey)),
                     disabledBorder: OutlineInputBorder(
                         borderSide: BorderSide(color: formMOccupationChangeNotif.editJenisBadanUsahaWiraswasta ? Colors.purple : Colors.grey)),
                     contentPadding: EdgeInsets.symmetric(horizontal: 10)
                 ),
                 items: formMOccupationChangeNotif.listTypeOfBusiness.map((value) {
                   return DropdownMenuItem<TypeOfBusinessModel>(
                     value: value,
                     child: Text(
                       value.text,
                       overflow: TextOverflow.ellipsis,
                     ),
                   );
                 }).toList()),
           ),
           Visibility(
               visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isJenisBadanUsahaWiraswasta(),
               child: SizedBox(height: MediaQuery.of(context).size.height / 47)
           ),
            Visibility(
              visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isSektorEkonomiWiraswasta(),
              child: TextFormField(
                autovalidate: formMOccupationChangeNotif.autoValidate,
                validator: (e) {
                  if (e.isEmpty && Provider.of<FormMOccupationChangeNotif>(context,listen: false).isSektorEkonomiWiraswastaMandatory()) {
                    return "Tidak boleh kosong";
                  } else {
                    return null;
                  }
                },
                enabled: formMOccupationChangeNotif.isDisablePACIAAOSCONA ? false : true,
                readOnly: true,
                onTap: () {
                  FocusManager.instance.primaryFocus.unfocus();
                  formMOccupationChangeNotif.searchSectorEconomic(context);
                },
                controller: formMOccupationChangeNotif.controllerSectorEconomic,
                style: TextStyle(color: Colors.black),
                decoration: InputDecoration(
                    labelText: "Sektor Ekonomi",
                    filled: formMOccupationChangeNotif.isDisablePACIAAOSCONA,
                    fillColor: Colors.black12,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: formMOccupationChangeNotif.editSektorEkonomiWiraswasta ? Colors.purple : Colors.grey)),
                    disabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: formMOccupationChangeNotif.editSektorEkonomiWiraswasta ? Colors.purple : Colors.grey)),
                    contentPadding: EdgeInsets.symmetric(horizontal: 10)
                ),
              ),
            ),
           Visibility(
               child: SizedBox(height: MediaQuery.of(context).size.height / 47),
               visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isSektorEkonomiWiraswasta(),
           ),
           Visibility(
             visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isLapanganUsahaWiraswasta(),
             child: TextFormField(
               autovalidate: formMOccupationChangeNotif.autoValidate,
               validator: (e) {
                 if (e.isEmpty && Provider.of<FormMOccupationChangeNotif>(context,listen: false).isLapanganUsahaWiraswastaMandatory()) {
                   return "Tidak boleh kosong";
                 } else {
                   return null;
                 }
               },
               readOnly: true,
               onTap: () {
                 FocusManager.instance.primaryFocus.unfocus();
                 formMOccupationChangeNotif.searchBusinesField(context);
               },
               controller: formMOccupationChangeNotif.controllerBusinessField,
               style: TextStyle(color: Colors.black),
               decoration: InputDecoration(
                   labelText: "Lapangan Usaha",
                   filled: formMOccupationChangeNotif.isDisablePACIAAOSCONA,
                   fillColor: Colors.black12,
                   border: OutlineInputBorder(
                       borderRadius: BorderRadius.circular(8)),
                   enabledBorder: OutlineInputBorder(
                       borderSide: BorderSide(color: formMOccupationChangeNotif.editLapanganUsahaWiraswasta ? Colors.purple : Colors.grey)),
                   disabledBorder: OutlineInputBorder(
                       borderSide: BorderSide(color: formMOccupationChangeNotif.editLapanganUsahaWiraswasta ? Colors.purple : Colors.grey)),
                   contentPadding: EdgeInsets.symmetric(horizontal: 10)
               ),
               enabled: formMOccupationChangeNotif.isDisablePACIAAOSCONA ? false : formMOccupationChangeNotif.controllerSectorEconomic.text == "" ? false : true,
             ),
           ),
           Visibility(
               child: SizedBox(height: MediaQuery.of(context).size.height / 47),
             visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isLapanganUsahaWiraswasta(),
           ),
           Visibility(
             visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isStatusLokasiWiraswasta(),
             child: TextFormField(
                 readOnly: true,
                 autovalidate: formMOccupationChangeNotif.autoValidate,
                 validator: (e) {
                     if (e.isEmpty && formMOccupationChangeNotif.isStatusLokasiWiraswastaMandatory()) {
                         return "Tidak boleh kosong";
                     } else {
                         return null;
                     }
                 },
                 onTap: (){
                     FocusManager.instance.primaryFocus.unfocus();
                     formMOccupationChangeNotif.searchStatusLocation(context);
                 },
                 controller: formMOccupationChangeNotif.controllerStatusLocation,
                 style: TextStyle(color: Colors.black),
                 decoration: InputDecoration(
                     labelText: 'Status Lokasi',
                     labelStyle: TextStyle(color: Colors.black),
                     border: OutlineInputBorder(
                         borderRadius: BorderRadius.circular(8)
                     ),
                     enabledBorder: OutlineInputBorder(
                         borderSide: BorderSide(color: formMOccupationChangeNotif.editStatusLokasiWiraswasta ? Colors.purple : Colors.grey)),
                     disabledBorder: OutlineInputBorder(
                         borderSide: BorderSide(color: formMOccupationChangeNotif.editStatusLokasiWiraswasta ? Colors.purple : Colors.grey)),
                     contentPadding: EdgeInsets.symmetric(horizontal: 10)
                 ),
                 keyboardType: TextInputType.text,
                 textCapitalization: TextCapitalization.characters,
             )
           ),
           Visibility(
               child: SizedBox(height: MediaQuery.of(context).size.height / 47),
             visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isStatusLokasiWiraswasta(),
           ),
           Visibility(
             visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isLokasiUsahaWiraswasta(),
             child: TextFormField(
                 readOnly: true,
                 autovalidate: formMOccupationChangeNotif.autoValidate,
                 validator: (e) {
                     if (e.isEmpty && formMOccupationChangeNotif.isLokasiUsahaWiraswastaMandatory()) {
                         return "Tidak boleh kosong";
                     } else {
                         return null;
                     }
                 },
                 onTap: (){
                     FocusManager.instance.primaryFocus.unfocus();
                     formMOccupationChangeNotif.searchBusinessLocation(context);
                 },
                 controller: formMOccupationChangeNotif.controllerBusinessLocation,
                 style: TextStyle(color: Colors.black),
                 decoration: InputDecoration(
                     labelText: 'Lokasi Usaha',
                     labelStyle: TextStyle(color: Colors.black),
                     border: OutlineInputBorder(
                         borderRadius: BorderRadius.circular(8)
                     ),
                     enabledBorder: OutlineInputBorder(
                         borderSide: BorderSide(color: formMOccupationChangeNotif.editLokasiUsahaWiraswasta ? Colors.purple : Colors.grey)),
                     disabledBorder: OutlineInputBorder(
                         borderSide: BorderSide(color: formMOccupationChangeNotif.editLokasiUsahaWiraswasta ? Colors.purple : Colors.grey)),
                     contentPadding: EdgeInsets.symmetric(horizontal: 10)
                 ),
                 keyboardType: TextInputType.text,
                 textCapitalization: TextCapitalization.characters,
             )
           ),
           Visibility(
             child: SizedBox(height: MediaQuery.of(context).size.height / 47),
             visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isLokasiUsahaWiraswasta(),
           ),
           Visibility(
             visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isTotalPegawaiWiraswasta(),
             child: TextFormField(
               autovalidate: formMOccupationChangeNotif.autoValidate,
               validator: (e) {
                 if (e.isEmpty && Provider.of<FormMOccupationChangeNotif>(context,listen: false).isTotalPegawaiWiraswastaMandatory()) {
                   return "Tidak boleh kosong";
                 } else {
                   return null;
                 }
               },
               controller: formMOccupationChangeNotif.controllerEmployeeTotal,
               style: TextStyle(color: Colors.black),
               decoration: InputDecoration(
                   labelText: "Total Pegawai",
                   border: OutlineInputBorder(
                       borderRadius: BorderRadius.circular(8)),
                   enabledBorder: OutlineInputBorder(
                       borderSide: BorderSide(color: formMOccupationChangeNotif.editTotalPegawaiWiraswasta ? Colors.purple : Colors.grey)),
                   disabledBorder: OutlineInputBorder(
                       borderSide: BorderSide(color: formMOccupationChangeNotif.editTotalPegawaiWiraswasta ? Colors.purple : Colors.grey)),
                   contentPadding: EdgeInsets.symmetric(horizontal: 10)
               ),
               keyboardType: TextInputType.number,
             ),
           ),
            Visibility(
              visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isTotalPegawaiWiraswasta(),
              child: SizedBox(height: MediaQuery.of(context).size.height / 47),
            ),
            Visibility(
              visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isTotalLamaBekerjaWiraswasta(),
              child: TextFormField(
                autovalidate: formMOccupationChangeNotif.autoValidate,
                validator: (e) {
                  if (e.isEmpty && Provider.of<FormMOccupationChangeNotif>(context,listen: false).isTotalLamaBekerjaWiraswastaMandatory()) {
                    return "Tidak boleh kosong";
                  } else {
                    return null;
                  }
                },
                enabled: formMOccupationChangeNotif.isDisablePACIAAOSCONA ? false : true,
                controller: formMOccupationChangeNotif.controllerTotalEstablishedDate,
                style: TextStyle(color: Colors.black),
                decoration: InputDecoration(
                    labelText: "Total Lama Usaha/Kerja (bln)",
                    filled: formMOccupationChangeNotif.isDisablePACIAAOSCONA,
                    fillColor: Colors.black12,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: formMOccupationChangeNotif.editTotalLamaBekerjaWiraswasta ? Colors.purple : Colors.grey)),
                    disabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: formMOccupationChangeNotif.editTotalLamaBekerjaWiraswasta ? Colors.purple : Colors.grey)),
                    contentPadding: EdgeInsets.symmetric(horizontal: 10)
                ),
                keyboardType: TextInputType.number,
              ),
            ),
          ],
        );
      },
    );
  }
}
