import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_type_installment_change_notifier.dart';
import 'package:ad1ms2_dev/shared/decimal_text_input_formatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class WidgetInfoCreditStructureBP extends StatefulWidget {
  @override
  _WidgetInfoCreditStructureBPState createState() => _WidgetInfoCreditStructureBPState();
}

class _WidgetInfoCreditStructureBPState extends State<WidgetInfoCreditStructureBP> {

  var _tenor;
  Future<void> _setPreference;

  @override
  void initState() {
    super.initState();
    _tenor = Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).periodOfTimeSelected;
    _setPreference = Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context, listen: false).setPreference(context);
    // Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false).getDataFromDashboard(context);
  }
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        primaryColor: Colors.black,
        accentColor: myPrimaryColor
      ),
      child: Scaffold(
        appBar: AppBar(
            title:
            Text("Struktur Kredit BP", style: TextStyle(color: Colors.black)),
            centerTitle: true,
            backgroundColor: myPrimaryColor,
            iconTheme: IconThemeData(color: Colors.black),
        ),
        body: FutureBuilder(
            future: _setPreference,
            builder: (context,snapshot){
              if(snapshot.connectionState == ConnectionState.waiting){
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
              return Consumer<InfoCreditStructureTypeInstallmentChangeNotifier>(
                builder: (context, value, child) {
                  return Container(
                    padding: EdgeInsets.symmetric(
                        vertical: MediaQuery.of(context).size.height / 47,
                        horizontal: MediaQuery.of(context).size.width / 27),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Visibility(
                          visible: value.isRadioValueBalloonPHOrBalloonInstallmentVisible(),
                          child: Text("Tipe Balloon",
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                              letterSpacing: 0.15,
                              color: value.isRadioValueBalloonPHOrBalloonInstallmentChanges ? Colors.purple : Colors.black,
                            ),
                          ),
                        ),
                        Visibility(
                          visible: value.isRadioValueBalloonPHOrBalloonInstallmentVisible(),
                          child: IgnorePointer(
                            ignoring: value.isDisablePACIAAOSCONA,
                            child: Row(
                              children: [
                                Row(
                                  children: [
                                    Radio(
                                        value: 0,
                                        activeColor: primaryOrange,
                                        groupValue: value.radioValueBalloonPHOrBalloonInstallment,
                                        onChanged: (data){
                                          value.radioValueBalloonPHOrBalloonInstallment = data;
                                        }
                                    ),
                                    SizedBox(width: 8),
                                    Text("Balloon PH")
                                  ],
                                ),
                                Row(
                                  children: [
                                    Radio(
                                        value: 1,
                                        activeColor: primaryOrange,
                                        groupValue: value.radioValueBalloonPHOrBalloonInstallment,
                                        onChanged: (data){
                                          value.radioValueBalloonPHOrBalloonInstallment = data;
                                        }
                                    ),
                                    SizedBox(width: 8),
                                    Text("Balloon Angsuran")
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        Visibility(
                            visible: value.isRadioValueBalloonPHOrBalloonInstallmentVisible(),
                            child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                        Visibility(
                          visible: value.isInstallmentPercentageVisible(),
                          child: Visibility(
                            visible: value.visibleTfPercentageInstallment,
                            child: IgnorePointer(
                              ignoring: value.isDisablePACIAAOSCONA,
                              child: TextFormField(
                                  validator: (e) {
                                    if (e.isEmpty && value.isInstallmentPercentageMandatory()) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  inputFormatters: [
                                    WhitelistingTextInputFormatter.digitsOnly
                                  ],
                                  onTap: () {
                                    value.calculatePresentaseOrNilai(context, 0);
                                  },
                                  onFieldSubmitted: (data){
                                    value.limitInput(data);
                                    value.calculatePresentaseOrNilai(context, 1);
                                  },
                                  keyboardType: TextInputType.number,
                                  controller: value.controllerInstallmentPercentage,
                                  autovalidate: value.autoValidateBP,
                                  decoration: new InputDecoration(
                                      labelText: 'Presentase Angsuran Ke - $_tenor',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: value.isDisablePACIAAOSCONA,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: value.isInstallmentPercentage ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: value.isInstallmentPercentage ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                  )
                              ),
                            ),
                          ),
                        ),
                        Visibility(
                            visible: value.isInstallmentPercentageVisible(),
                            child: Visibility(
                                visible: value.visibleTfPercentageInstallment,
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                            )
                        ),
                        Visibility(
                          visible: value.isInstallmentValueVisible(),
                          child: IgnorePointer(
                            ignoring: value.isDisablePACIAAOSCONA,
                            child: TextFormField(
                                validator: (e) {
                                  if (e.isEmpty && value.isInstallmentValueMandatory()) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                textAlign: TextAlign.end,
                                inputFormatters: [
                                  DecimalTextInputFormatter(decimalRange: 2),
                                  value.amountValidator
                                ],
                                onTap: () {
                                  value.calculatePresentaseOrNilai(context, 0);
                                },
                                onFieldSubmitted: (data){
                                  value.calculatePresentaseOrNilai(context, 2);
                                },
                                keyboardType: TextInputType.number,
                                controller: value.controllerInstallmentValue,
                                autovalidate: value.autoValidateBP,
                                decoration: new InputDecoration(
                                  labelText: 'Nilai Angsuran Ke - $_tenor',
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: value.isDisablePACIAAOSCONA,
                                  fillColor: Colors.black12,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: value.isInstallmentValue ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: value.isInstallmentValue ? Colors.purple : Colors.grey)),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                )
                            ),
                          ),
                        )
                      ],
                    ),
                  );
                },
              );
            }
        ),
        bottomNavigationBar: BottomAppBar(
              elevation: 0.0,
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Consumer<InfoCreditStructureTypeInstallmentChangeNotifier>(
                      builder: (context, infoCreditStructureTypeInstallmentChangeNotifier, _) {
                          return RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(8.0)),
                              color: myPrimaryColor,
                              onPressed: () {
                                  // Navigator.pop(context);
                                  infoCreditStructureTypeInstallmentChangeNotifier.checkBP(context);
                              },
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                      Text("DONE",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500,
                                              letterSpacing: 1.25))
                                  ],
                              ));
                      },
                  )),
          ),
      ),
    );
  }
}
