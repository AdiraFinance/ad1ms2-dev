import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/info_credit_structure_floating_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_type_installment_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class WidgetInfoCreditStructureFloating extends StatefulWidget {
  @override
  _WidgetInfoCreditStructureFloatingState createState() => _WidgetInfoCreditStructureFloatingState();
}

class _WidgetInfoCreditStructureFloatingState extends State<WidgetInfoCreditStructureFloating> {

  Future<void> _setPreference;

  @override
  void initState() {
    super.initState();
    // Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false).setListInstallment(context);
    _setPreference = Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context, listen: false).setPreference(context);
    // Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false).getDataFromDashboard(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:
        Text("Struktur Kredit Floating", style: TextStyle(color: Colors.black)),
        centerTitle: true,
        backgroundColor: myPrimaryColor,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: FutureBuilder(
        future: _setPreference,
        builder: (context,snapshot){
          if(snapshot.connectionState == ConnectionState.waiting){
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          return Consumer<InfoCreditStructureTypeInstallmentChangeNotifier>(
            builder: (context,infoCreditStructureFloatingChangeNotifier,_){
              return ListView.builder(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height / 47,
                      horizontal: MediaQuery.of(context).size.width / 27),
                  itemCount: infoCreditStructureFloatingChangeNotifier.listInfoCreditStructureFloatingModel.length,
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: (){
                        Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false).clearDialogFloating();
                        Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false)
                            .setValueForEditFloating(infoCreditStructureFloatingChangeNotifier.listInfoCreditStructureFloatingModel[index]);
                        _showDialogCreateAndUpdate(1,index);
                      },
                      child: Container(
                        child: Card(
                          elevation: 3.3,
                          child: Padding(
                              padding: const EdgeInsets.all(13.0),
                              child:Stack(
                                children: [
                                  Column(
                                    children: [
                                      Row(
                                        children: [
                                          Expanded(child: Text("Angsuran Ke"),flex: 5),
                                          Text(" : "),
                                          Expanded(
                                              flex: 5,
                                              child: Text(
                                                  "${infoCreditStructureFloatingChangeNotifier.listInfoCreditStructureFloatingModel[index].installment}"
                                              )
                                          ),
                                        ],
                                      ),
                                      SizedBox(height: MediaQuery.of(context).size.height / 57),
                                      Row(
                                        children: [
                                          Expanded(child: Text("Suku Bunga (Eff) Floating"),flex: 5),
                                          Text(" : "),
                                          Expanded(
                                              flex: 5,
                                              child: Text(
                                                  "${infoCreditStructureFloatingChangeNotifier.listInfoCreditStructureFloatingModel[index].interestRateEffFloat}"
                                              )
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  Align(
                                    alignment: Alignment.topRight,
                                    child: IconButton(
                                        icon: Icon(Icons.delete,color: Colors.red),
                                        onPressed: (){
                                          Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false)
                                              .deleteInfoCreditStructureFloating(index);
                                        }
                                    ),
                                  )
                                ],
                              )
                          ),
                        ),
                      ),
                    );
                  });
            },
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false).clearDialogFloating();
          _showDialogCreateAndUpdate(0,null);
        },
        backgroundColor: myPrimaryColor,
        child: Icon(Icons.add),
      ),
    );
  }

  _showDialogCreateAndUpdate(int flag,int index) async{
    var _provider = Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false);
    showDialog(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return Theme(
          data: ThemeData(
              fontFamily: "NunitoSans",
              primaryColor: Colors.black,
              accentColor: myPrimaryColor
          ),
          child: AlertDialog(
            title: Text('Add Floating'),
            content: SingleChildScrollView(
              child: Form(
                key: _provider.keyFloating,
                child: ListBody(
                  children: <Widget>[
                    Visibility(
                      visible: _provider.isInstallmentSelectedFloatingVisible(),
                      child: IgnorePointer(
                        ignoring: _provider.isDisablePACIAAOSCONA,
                        child: DropdownButtonFormField<String>(
                            autovalidate:
                            _provider.autoValidateDialogFloating,
                            validator: (e) {
                              if (e == null && _provider.isInstallmentSelectedFloatingMandatory()) {
                                return "Silahkan pilih jangka waktu";
                              } else {
                                return null;
                              }
                            },
                            value: _provider
                                .installmentSelected,
                            onChanged: (value) {
                              _provider
                                  .installmentSelected = value;
                            },
                            decoration: InputDecoration(
                              labelText: "Jangka Waktu",
                              border: OutlineInputBorder(),
                              contentPadding:
                              EdgeInsets.symmetric(horizontal: 10),
                            ),
                            items: _provider
                                .listInstallment
                                .map((value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(
                                  value,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              );
                            }).toList()
                        ),
                      ),
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    Visibility(
                      visible: _provider.isInterestRateVisible(),
                      child: IgnorePointer(
                        ignoring: _provider.isDisablePACIAAOSCONA,
                        child: TextFormField(
                            validator: (e) {
                              if (e.isEmpty && _provider.isInterestRateMandatory()) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            inputFormatters: [
                              WhitelistingTextInputFormatter.digitsOnly
                            ],
                            keyboardType: TextInputType.number,
                            controller: _provider.controllerInterestRate,
                            autovalidate: _provider.autoValidateDialogFloating,
                            decoration: new InputDecoration(
                                labelText: 'Suku Bunga Eff  (Floating)',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8)
                                )
                            )
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            actions: [
              FlatButton(
                child: Text(
                    flag == 0 ? "ADD" : "UPDATE",
                    style: TextStyle(
                        color: Colors.black
                    )
                ),
                onPressed: (){
                  if(flag==0){
                    _provider.checkFloating(
                        flag,
                        InfoCreditStructureFloatingModel(
                            _provider.installmentSelected,
                            _provider.controllerInterestRate.text
                        ),
                        null,context
                    );
                  }
                  else{
                    _provider.checkFloating(
                        flag,
                        InfoCreditStructureFloatingModel(
                            _provider.installmentSelected,
                            _provider.controllerInterestRate.text
                        ),
                        index,context
                    );
                  }
                },
              ),
              FlatButton(
                child: Text(
                    "CANCEL",
                    style: TextStyle(
                        color: Colors.black
                    )
                ),
                onPressed: (){
                  Navigator.pop(context);
                },
              ),
            ],
          ),
        );
      },
    );
  }
}
