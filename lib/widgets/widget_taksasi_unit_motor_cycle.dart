import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/taksasi_unit_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/taksasi_unit_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class WidgetTaksasiUnitMotorCycle extends StatefulWidget {
  final String kode;
  const WidgetTaksasiUnitMotorCycle({this.kode});
  @override
  _WidgetTaksasiUnitMotorCycleState createState() => _WidgetTaksasiUnitMotorCycleState();
}

class _WidgetTaksasiUnitMotorCycleState extends State<WidgetTaksasiUnitMotorCycle> {
  Future<void> _getData;
  @override
  void initState() {
    super.initState();
    // print("cek taksasi kode ${widget.kode != Provider.of<TaksasiUnitChangeNotifier>(context, listen: false).kode}");
    _getData = Provider.of<TaksasiUnitChangeNotifier>(context, listen: false).getListTaksasi(context,null);
  }

  @override
  Widget build(BuildContext context) {
    // print("cek taksasi ${widget.kode}");
    return FutureBuilder(
      future: _getData,
      builder: (context,snapshot){
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        return Consumer<TaksasiUnitChangeNotifier>(
          builder: (context, taksasiUnit, child) {
            return ListView.builder(
                padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height/77,
                  horizontal: MediaQuery.of(context).size.width/37,
                ),
                itemBuilder: (context, index) {
                  if(index == taksasiUnit.listTaksasi.length) {
                   return Column(
                     children: [
                       Row(
                         children: [
                           Expanded(
                             child: Visibility(
                               visible: taksasiUnit.listTaksasi.isNotEmpty,
                               child: Padding(
                                 padding: EdgeInsets.all(8.0),
                                 child: TextFormField(
                                   enabled: false,
                                   keyboardType: TextInputType.numberWithOptions(decimal: true),
                                   controller: taksasiUnit.controllerTotalNilaiBobot,
                                   decoration: InputDecoration(
                                       labelText: "Total Nilai Bobot",
                                       labelStyle: TextStyle(color: Colors.black),
                                       fillColor: Colors.black12,
                                       filled: true,
                                       border: OutlineInputBorder(
                                           borderRadius: BorderRadius.circular(8)
                                       ),
                                       suffixIconConstraints: BoxConstraints(
                                         minWidth: 20,
                                         minHeight: 20,
                                         maxHeight: 25,
                                         maxWidth: 30,),
                                       suffixIcon: GestureDetector(
                                         child: Padding(
                                           padding: EdgeInsets.only(right: 8.0),
                                           child: Container(
                                             child: Text("%"),
                                           ),
                                         ),
                                       ),
                                   ),
                                 ),
                               ),
                             ),
                           ),
                           SizedBox(width: 8.0),
                           Expanded(
                             child: Visibility(
                               visible: taksasiUnit.listTaksasi.isNotEmpty,
                               child: Padding(
                                 padding: EdgeInsets.all(8.0),
                                 child: RaisedButton(
                                     shape: RoundedRectangleBorder(
                                         borderRadius: new BorderRadius.circular(8.0)),
                                     color: myPrimaryColor,
                                     onPressed: () {
                                       taksasiUnit.calculatedMotorCycle(context);
                                     },
                                     child: Row(
                                       mainAxisAlignment: MainAxisAlignment.center,
                                       children: <Widget>[
                                         Text("CALCULATE",
                                             style: TextStyle(
                                                 color: Colors.black,
                                                 fontSize: 14,
                                                 fontWeight: FontWeight.w500,
                                                 letterSpacing: 1.25))
                                       ],
                                     )),
                               ),
                             ),
                           )
                         ],
                       )
                     ],
                   );
                  } else {
                    return Card(
                      elevation: 3.3,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: [
                            Row (
                              children: [
                                Expanded(
                                  // flex: 2,
                                  child: TextFormField(
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter.digitsOnly
                                    ],
                                    onChanged: (value){
                                      if(value.isNotEmpty){
                                        taksasiUnit.limitInput(index,value);
                                      }
                                    },
                                    onFieldSubmitted: (value) {
                                      taksasiUnit.setStatus();
                                    },
                                    onTap: () {
                                      taksasiUnit.setStatus();
                                    },
                                    keyboardType: TextInputType.numberWithOptions(decimal: true),
                                    controller: taksasiUnit.listTaksasi[index].CONTROLLER,
                                    decoration: InputDecoration(
                                        labelText: taksasiUnit.listTaksasi[index].TAKSASI_DESC,
                                        labelStyle: TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8)
                                        ),
                                        suffixIconConstraints: BoxConstraints(
                                          minWidth: 20,
                                          minHeight: 20,
                                          maxHeight: 25,
                                          maxWidth: 30,),
                                        suffixIcon: GestureDetector(
                                          child: Padding(
                                            padding: EdgeInsets.only(right: 8.0),
                                            child: Container(
                                              child: Text("%"),
                                            ),
                                          ),
                                        ),
                                    ),
                                    autovalidate: taksasiUnit.listTaksasi[index].AUTOVALIDATE,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                  ),
                                ),
                                // SizedBox(width: 8.0),
                                // Expanded(
                                //   flex: 2,
                                //   child: TextFormField(
                                //     // autovalidate: taksasiUnit.listTaksasi[index].AUTOVALIDATE,
                                //     // validator: (e) {
                                //     //   if (e.isEmpty) {
                                //     //     return "Tidak boleh kosong";
                                //     //   } else {
                                //     //     return null;
                                //     //   }
                                //     // },
                                //     inputFormatters: [
                                //       WhitelistingTextInputFormatter.digitsOnly
                                //     ],
                                //     // onChanged: (value){
                                //     //   if(value.isNotEmpty){
                                //     //     taksasiUnit.setValidate(index);
                                //     //   }
                                //     // },
                                //     enabled: false,
                                //     keyboardType: TextInputType.numberWithOptions(decimal: true),
                                //     controller: taksasiUnit.listTaksasi[index].CONTROLLER_NILAI_BOBOT,
                                //     decoration: InputDecoration(
                                //         labelText: "BOBOT ${taksasiUnit.listTaksasi[index].TAKSASI_DESC}",
                                //         labelStyle: TextStyle(color: Colors.black),
                                //         filled: true,
                                //         fillColor: Colors.black12,
                                //         border: OutlineInputBorder(
                                //             borderRadius: BorderRadius.circular(8)
                                //         )
                                //     ),
                                //   ),
                                // ),
                              ],
                            ),
                            SizedBox(height: MediaQuery.of(context).size.height / 57),
                            Container(
                              decoration: BoxDecoration(
                                  color: myPrimaryColor,
                                  borderRadius: BorderRadius.all(Radius.circular(8))
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Expanded(
                                            child: Center(child: Text("OK")),
                                            flex: 4
                                        ),
                                        SizedBox(
                                          height: MediaQuery.of(context).size.height/37,
                                          child: VerticalDivider(
                                            width: 2,
                                            color: Colors.black,
                                          ),
                                        ),
                                        Expanded(
                                          child: Center(child: Text("CUKUP")),
                                          flex: 4,
                                        ),
                                        SizedBox(
                                          height: MediaQuery.of(context).size.height/37,
                                          child: VerticalDivider(
                                            width: 2,
                                            color: Colors.black,
                                          ),
                                        ),
                                        Expanded(
                                          child:  Center(child: Text("TIDAK")),
                                          flex: 4,
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Expanded(
                                          flex: 4,
                                          child: Visibility(
                                              visible: taksasiUnit.listTaksasi[index].STATUS[0],
                                              child: Icon(Icons.check,color: Colors.green)
                                          ),
                                        ),
                                        Expanded(
                                          flex: 4,
                                          child: Visibility(
                                              visible: taksasiUnit.listTaksasi[index].STATUS[1],
                                              child: Icon(Icons.check,color: Colors.green)
                                          ),
                                        ),
                                        Expanded(
                                          flex: 4,
                                          child: Visibility(
                                              visible: taksasiUnit.listTaksasi[index].STATUS[2],
                                              child: Icon(Icons.check,color: Colors.green)
                                          ),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  }
                },
                itemCount: taksasiUnit.listTaksasi.length+1
            );
            //   SingleChildScrollView(
//           padding: EdgeInsets.symmetric(
//               vertical: MediaQuery.of(context).size.height / 47,
//               horizontal: MediaQuery.of(context).size.width / 27),
            //   child: Column(
            //     children: [
            //       Card(
            //         child: Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Column(
            //             children: [
            //               TextFormField(
            //                   inputFormatters: [
            //                     WhitelistingTextInputFormatter.digitsOnly
            //                   ],
            //                   // onChanged: (value){
            //                   //   taksasiUnit.calculateEngineSoundMotorCycle(value);
            //                   // },
            //                   onFieldSubmitted: (value) {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   onTap: () {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   keyboardType: TextInputType.numberWithOptions(decimal: true),
            //                   controller: taksasiUnit.controllerEngineSoundMotorCycle,
            //                   decoration: InputDecoration(
            //                       labelText: 'Suara Mesin',
            //                       labelStyle: TextStyle(color: Colors.black),
            //                       border: OutlineInputBorder(
            //                           borderRadius: BorderRadius.circular(8)
            //                       )
            //                   ),
            //                 autovalidate: taksasiUnit.autoValidateCar,
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //               ),
            //               SizedBox(height: MediaQuery.of(context).size.height / 57),
            //               Container(
            //                 decoration: BoxDecoration(
            //                     color: myPrimaryColor,
            //                     borderRadius: BorderRadius.all(Radius.circular(8))
            //                 ),
            //                 child: Padding(
            //                   padding: const EdgeInsets.all(8.0),
            //                   child:
            //                   Column(
            //                     children: [
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                               child: Center(child: Text("OK")),
            //                               flex: 4
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child: Center(child: Text("CUKUP")),
            //                             flex: 4,
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child:  Center(child: Text("TIDAK")),
            //                             flex: 4,
            //                           ),
            //                         ],
            //                       ),
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueEngineSoundMotorCycle.isNotEmpty
            //                                     ? taksasiUnit.listValueEngineSoundMotorCycle[0] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueEngineSoundMotorCycle.isNotEmpty
            //                                     ? taksasiUnit.listValueEngineSoundMotorCycle[1] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueEngineSoundMotorCycle.isNotEmpty
            //                                     ? taksasiUnit.listValueEngineSoundMotorCycle[2] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           )
            //                         ],
            //                       )
            //                     ],
            //                   ),
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       SizedBox(height: MediaQuery.of(context).size.height / 77),
            //       Card(
            //         child: Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Column(
            //             children: [
            //               TextFormField(
            //                   inputFormatters: [
            //                     WhitelistingTextInputFormatter.digitsOnly
            //                   ],
            //                   // onChanged: (value){
            //                   //   taksasiUnit.calculateEngineCleanMotorCycle(value);
            //                   // },
            //                   onFieldSubmitted: (value) {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   onTap: () {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   keyboardType: TextInputType.numberWithOptions(decimal: true),
            //                   controller: taksasiUnit.controllerMachineCleanMotorCycle,
            //                   decoration: InputDecoration(
            //                       labelText: 'Kebersihan Mesin',
            //                       labelStyle: TextStyle(color: Colors.black),
            //                       border: OutlineInputBorder(
            //                           borderRadius: BorderRadius.circular(8)
            //                       )
            //                   ),
            //                 autovalidate: taksasiUnit.autoValidateCar,
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //               ),
            //               SizedBox(height: MediaQuery.of(context).size.height / 57),
            //               Container(
            //                 decoration: BoxDecoration(
            //                     color: myPrimaryColor,
            //                     borderRadius: BorderRadius.all(Radius.circular(8))
            //                 ),
            //                 child: Padding(
            //                   padding: const EdgeInsets.all(8.0),
            //                   child:
            //                   Column(
            //                     children: [
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                               child: Center(child: Text("OK")),
            //                               flex: 4
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child: Center(child: Text("CUKUP")),
            //                             flex: 4,
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child:  Center(child: Text("TIDAK")),
            //                             flex: 4,
            //                           ),
            //                         ],
            //                       ),
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueEngineCleanMotorCycle.isNotEmpty
            //                                     ? taksasiUnit.listValueEngineCleanMotorCycle[0] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueEngineCleanMotorCycle.isNotEmpty
            //                                     ? taksasiUnit.listValueEngineCleanMotorCycle[1] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueEngineCleanMotorCycle.isNotEmpty
            //                                     ? taksasiUnit.listValueEngineCleanMotorCycle[2] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           )
            //                         ],
            //                       )
            //                     ],
            //                   ),
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       SizedBox(height: MediaQuery.of(context).size.height / 77),
            //       Card(
            //         child: Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Column(
            //             children: [
            //               TextFormField(
            //                   inputFormatters: [
            //                     WhitelistingTextInputFormatter.digitsOnly
            //                   ],
            //                   // onChanged: (value){
            //                   //   taksasiUnit.calculateTires(value);
            //                   // },
            //                   onFieldSubmitted: (value) {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   onTap: () {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   keyboardType: TextInputType.numberWithOptions(decimal: true),
            //                   controller: taksasiUnit.controllerTires,
            //                   decoration: InputDecoration(
            //                       labelText: 'Ban Depan  Belakang',
            //                       labelStyle: TextStyle(color: Colors.black),
            //                       border: OutlineInputBorder(
            //                           borderRadius: BorderRadius.circular(8)
            //                       )
            //                   ),
            //                 autovalidate: taksasiUnit.autoValidateCar,
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //               ),
            //               SizedBox(height: MediaQuery.of(context).size.height / 57),
            //               Container(
            //                 decoration: BoxDecoration(
            //                     color: myPrimaryColor,
            //                     borderRadius: BorderRadius.all(Radius.circular(8))
            //                 ),
            //                 child: Padding(
            //                   padding: const EdgeInsets.all(8.0),
            //                   child:
            //                   Column(
            //                     children: [
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                               child: Center(child: Text("OK")),
            //                               flex: 4
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child: Center(child: Text("CUKUP")),
            //                             flex: 4,
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child:  Center(child: Text("TIDAK")),
            //                             flex: 4,
            //                           ),
            //                         ],
            //                       ),
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueTires.isNotEmpty
            //                                     ? taksasiUnit.listValueTires[0] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueTires.isNotEmpty
            //                                     ? taksasiUnit.listValueTires[1] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueTires.isNotEmpty
            //                                     ? taksasiUnit.listValueTires[2] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           )
            //                         ],
            //                       )
            //                     ],
            //                   ),
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       SizedBox(height: MediaQuery.of(context).size.height / 77),
            //       Card(
            //         child: Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Column(
            //             children: [
            //               TextFormField(
            //                   inputFormatters: [
            //                     WhitelistingTextInputFormatter.digitsOnly
            //                   ],
            //                   // onChanged: (value){
            //                   //   taksasiUnit.calculateChassis(value);
            //                   // },
            //                   onFieldSubmitted: (value) {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   onTap: () {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   keyboardType: TextInputType.numberWithOptions(decimal: true),
            //                   controller: taksasiUnit.controllerChassis,
            //                   decoration: InputDecoration(
            //                       labelText: 'Casis/Rangka',
            //                       labelStyle: TextStyle(color: Colors.black),
            //                       border: OutlineInputBorder(
            //                           borderRadius: BorderRadius.circular(8)
            //                       )
            //                   ),autovalidate: taksasiUnit.autoValidateCar,
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //               ),
            //               SizedBox(height: MediaQuery.of(context).size.height / 57),
            //               Container(
            //                 decoration: BoxDecoration(
            //                     color: myPrimaryColor,
            //                     borderRadius: BorderRadius.all(Radius.circular(8))
            //                 ),
            //                 child: Padding(
            //                   padding: const EdgeInsets.all(8.0),
            //                   child:
            //                   Column(
            //                     children: [
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                               child: Center(child: Text("OK")),
            //                               flex: 4
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child: Center(child: Text("CUKUP")),
            //                             flex: 4,
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child:  Center(child: Text("TIDAK")),
            //                             flex: 4,
            //                           ),
            //                         ],
            //                       ),
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueChassis.isNotEmpty
            //                                     ? taksasiUnit.listValueChassis[0] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueChassis.isNotEmpty
            //                                     ? taksasiUnit.listValueChassis[1] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueChassis.isNotEmpty
            //                                     ? taksasiUnit.listValueChassis[2] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           )
            //                         ],
            //                       )
            //                     ],
            //                   ),
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       SizedBox(height: MediaQuery.of(context).size.height / 77),
            //       Card(
            //         child: Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Column(
            //             children: [
            //               TextFormField(
            //                   inputFormatters: [
            //                     WhitelistingTextInputFormatter.digitsOnly
            //                   ],
            //                   // onChanged: (value){
            //                   //   taksasiUnit.calculateCombinationMeter(value);
            //                   // },
            //                   onFieldSubmitted: (value) {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   onTap: () {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   keyboardType: TextInputType.numberWithOptions(decimal: true),
            //                   controller: taksasiUnit.controllerCombinationMeter,
            //                   decoration: InputDecoration(
            //                       labelText: 'Combination Meter',
            //                       labelStyle: TextStyle(color: Colors.black),
            //                       border: OutlineInputBorder(
            //                           borderRadius: BorderRadius.circular(8)
            //                       )
            //                   ),
            //                 autovalidate: taksasiUnit.autoValidateCar,
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //               ),
            //               SizedBox(height: MediaQuery.of(context).size.height / 57),
            //               Container(
            //                 decoration: BoxDecoration(
            //                     color: myPrimaryColor,
            //                     borderRadius: BorderRadius.all(Radius.circular(8))
            //                 ),
            //                 child: Padding(
            //                   padding: const EdgeInsets.all(8.0),
            //                   child:
            //                   Column(
            //                     children: [
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                               child: Center(child: Text("OK")),
            //                               flex: 4
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child: Center(child: Text("CUKUP")),
            //                             flex: 4,
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child:  Center(child: Text("TIDAK")),
            //                             flex: 4,
            //                           ),
            //                         ],
            //                       ),
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueCombinationMeter.isNotEmpty
            //                                     ? taksasiUnit.listValueCombinationMeter[0] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueCombinationMeter.isNotEmpty
            //                                     ? taksasiUnit.listValueCombinationMeter[1] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueCombinationMeter.isNotEmpty
            //                                     ? taksasiUnit.listValueCombinationMeter[2] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           )
            //                         ],
            //                       )
            //                     ],
            //                   ),
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       SizedBox(height: MediaQuery.of(context).size.height / 77),
            //       Card(
            //         child: Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Column(
            //             children: [
            //               TextFormField(
            //                   inputFormatters: [
            //                     WhitelistingTextInputFormatter.digitsOnly
            //                   ],
            //                   // onChanged: (value){
            //                   //   taksasiUnit.calculateCoverBodyPrimary(value);
            //                   // },
            //                   onFieldSubmitted: (value) {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   onTap: () {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   keyboardType: TextInputType.numberWithOptions(decimal: true),
            //                   controller: taksasiUnit.controllerCentralLock,
            //                   decoration: InputDecoration(
            //                       labelText: 'Cover Body Utama',
            //                       labelStyle: TextStyle(color: Colors.black),
            //                       border: OutlineInputBorder(
            //                           borderRadius: BorderRadius.circular(8)
            //                       )
            //                   ),
            //                 autovalidate: taksasiUnit.autoValidateCar,
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //               ),
            //               SizedBox(height: MediaQuery.of(context).size.height / 57),
            //               Container(
            //                 decoration: BoxDecoration(
            //                     color: myPrimaryColor,
            //                     borderRadius: BorderRadius.all(Radius.circular(8))
            //                 ),
            //                 child: Padding(
            //                   padding: const EdgeInsets.all(8.0),
            //                   child:
            //                   Column(
            //                     children: [
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                               child: Center(child: Text("OK")),
            //                               flex: 4
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child: Center(child: Text("CUKUP")),
            //                             flex: 4,
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child:  Center(child: Text("TIDAK")),
            //                             flex: 4,
            //                           ),
            //                         ],
            //                       ),
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueCoverBodyPrimary.isNotEmpty
            //                                     ? taksasiUnit.listValueCoverBodyPrimary[0] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueCoverBodyPrimary.isNotEmpty
            //                                     ? taksasiUnit.listValueCoverBodyPrimary[1] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueCoverBodyPrimary.isNotEmpty
            //                                     ? taksasiUnit.listValueCoverBodyPrimary[2] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           )
            //                         ],
            //                       )
            //                     ],
            //                   ),
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       SizedBox(height: MediaQuery.of(context).size.height / 77),
            //       Card(
            //         child: Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Column(
            //             children: [
            //               TextFormField(
            //                   inputFormatters: [
            //                     WhitelistingTextInputFormatter.digitsOnly
            //                   ],
            //                   // onChanged: (value){
            //                   //   taksasiUnit.calculateCoverStangKemudi(value);
            //                   // },
            //                   onFieldSubmitted: (value) {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   onTap: () {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   keyboardType: TextInputType.numberWithOptions(decimal: true),
            //                   controller: taksasiUnit.controllerCoverStangKemudi,
            //                   decoration: InputDecoration(
            //                       labelText: 'Cover Stang Kemudi',
            //                       labelStyle: TextStyle(color: Colors.black),
            //                       border: OutlineInputBorder(
            //                           borderRadius: BorderRadius.circular(8)
            //                       )
            //                   ),
            //                 autovalidate: taksasiUnit.autoValidateCar,
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //               ),
            //               SizedBox(height: MediaQuery.of(context).size.height / 57),
            //               Container(
            //                 decoration: BoxDecoration(
            //                     color: myPrimaryColor,
            //                     borderRadius: BorderRadius.all(Radius.circular(8))
            //                 ),
            //                 child: Padding(
            //                   padding: const EdgeInsets.all(8.0),
            //                   child:
            //                   Column(
            //                     children: [
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                               child: Center(child: Text("OK")),
            //                               flex: 4
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child: Center(child: Text("CUKUP")),
            //                             flex: 4,
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child:  Center(child: Text("TIDAK")),
            //                             flex: 4,
            //                           ),
            //                         ],
            //                       ),
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueCoverStangKemudi.isNotEmpty
            //                                     ? taksasiUnit.listValueCoverStangKemudi[0] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueCoverStangKemudi.isNotEmpty
            //                                     ? taksasiUnit.listValueCoverStangKemudi[1] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueCoverStangKemudi.isNotEmpty
            //                                     ? taksasiUnit.listValueCoverStangKemudi[2] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           )
            //                         ],
            //                       )
            //                     ],
            //                   ),
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       SizedBox(height: MediaQuery.of(context).size.height / 77),
            //       Card(
            //         child: Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Column(
            //             children: [
            //               TextFormField(
            //                   inputFormatters: [
            //                     WhitelistingTextInputFormatter.digitsOnly
            //                   ],
            //                   // onChanged: (value){
            //                   //   taksasiUnit.calculateEmblem(value);
            //                   // },
            //                   onFieldSubmitted: (value) {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   onTap: () {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   keyboardType: TextInputType.numberWithOptions(decimal: true),
            //                   controller: taksasiUnit.controllerEmblem,
            //                   decoration: InputDecoration(
            //                       labelText: 'Emblem/Logo Utama',
            //                       labelStyle: TextStyle(color: Colors.black),
            //                       border: OutlineInputBorder(
            //                           borderRadius: BorderRadius.circular(8)
            //                       )
            //                   ),
            //                 autovalidate: taksasiUnit.autoValidateCar,
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //               ),
            //               SizedBox(height: MediaQuery.of(context).size.height / 57),
            //               Container(
            //                 decoration: BoxDecoration(
            //                     color: myPrimaryColor,
            //                     borderRadius: BorderRadius.all(Radius.circular(8))
            //                 ),
            //                 child: Padding(
            //                   padding: const EdgeInsets.all(8.0),
            //                   child:
            //                   Column(
            //                     children: [
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                               child: Center(child: Text("OK")),
            //                               flex: 4
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child: Center(child: Text("CUKUP")),
            //                             flex: 4,
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child:  Center(child: Text("TIDAK")),
            //                             flex: 4,
            //                           ),
            //                         ],
            //                       ),
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueEmblem.isNotEmpty
            //                                     ? taksasiUnit.listValueEmblem[0] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueEmblem.isNotEmpty
            //                                     ? taksasiUnit.listValueEmblem[1] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueEmblem.isNotEmpty
            //                                     ? taksasiUnit.listValueEmblem[2] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           )
            //                         ],
            //                       )
            //                     ],
            //                   ),
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       SizedBox(height: MediaQuery.of(context).size.height / 77),
            //       Card(
            //         child: Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Column(
            //             children: [
            //               TextFormField(
            //                   inputFormatters: [
            //                     WhitelistingTextInputFormatter.digitsOnly
            //                   ],
            //                   // onChanged: (value){
            //                   //   taksasiUnit.calculateFootStepBracket(value);
            //                   // },
            //                   onFieldSubmitted: (value) {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   onTap: () {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   keyboardType: TextInputType.numberWithOptions(decimal: true),
            //                   controller: taksasiUnit.controllerFootStepBracket,
            //                   decoration: InputDecoration(
            //                       labelText: 'Footstep dan Bracket(Depan-Belakang)',
            //                       labelStyle: TextStyle(color: Colors.black),
            //                       border: OutlineInputBorder(
            //                           borderRadius: BorderRadius.circular(8)
            //                       )
            //                   ),
            //                 autovalidate: taksasiUnit.autoValidateCar,
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //               ),
            //               SizedBox(height: MediaQuery.of(context).size.height / 57),
            //               Container(
            //                 decoration: BoxDecoration(
            //                     color: myPrimaryColor,
            //                     borderRadius: BorderRadius.all(Radius.circular(8))
            //                 ),
            //                 child: Padding(
            //                   padding: const EdgeInsets.all(8.0),
            //                   child:
            //                   Column(
            //                     children: [
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                               child: Center(child: Text("OK")),
            //                               flex: 4
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child: Center(child: Text("CUKUP")),
            //                             flex: 4,
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child:  Center(child: Text("TIDAK")),
            //                             flex: 4,
            //                           ),
            //                         ],
            //                       ),
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueFootStepBracket.isNotEmpty
            //                                     ? taksasiUnit.listValueFootStepBracket[0] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueFootStepBracket.isNotEmpty
            //                                     ? taksasiUnit.listValueFootStepBracket[1] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueFootStepBracket.isNotEmpty
            //                                     ? taksasiUnit.listValueFootStepBracket[2] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           )
            //                         ],
            //                       )
            //                     ],
            //                   ),
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       SizedBox(height: MediaQuery.of(context).size.height / 77),
            //       Card(
            //         child: Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Column(
            //             children: [
            //               TextFormField(
            //                   inputFormatters: [
            //                     WhitelistingTextInputFormatter.digitsOnly
            //                   ],
            //                   // onChanged: (value){
            //                   //   taksasiUnit.calculateJokMotor(value);
            //                   // },
            //                   onFieldSubmitted: (value) {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   onTap: () {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   keyboardType: TextInputType.numberWithOptions(decimal: true),
            //                   controller: taksasiUnit.controllerJokMotor,
            //                   decoration: InputDecoration(
            //                       labelText: 'Jok ',
            //                       labelStyle: TextStyle(color: Colors.black),
            //                       border: OutlineInputBorder(
            //                           borderRadius: BorderRadius.circular(8)
            //                       )
            //                   ),
            //                 autovalidate: taksasiUnit.autoValidateCar,
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //               ),
            //               SizedBox(height: MediaQuery.of(context).size.height / 57),
            //               Container(
            //                 decoration: BoxDecoration(
            //                     color: myPrimaryColor,
            //                     borderRadius: BorderRadius.all(Radius.circular(8))
            //                 ),
            //                 child: Padding(
            //                   padding: const EdgeInsets.all(8.0),
            //                   child:
            //                   Column(
            //                     children: [
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                               child: Center(child: Text("OK")),
            //                               flex: 4
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child: Center(child: Text("CUKUP")),
            //                             flex: 4,
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child:  Center(child: Text("TIDAK")),
            //                             flex: 4,
            //                           ),
            //                         ],
            //                       ),
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueJokMotor.isNotEmpty
            //                                     ? taksasiUnit.listValueJokMotor[0] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueJokMotor.isNotEmpty
            //                                     ? taksasiUnit.listValueJokMotor[1] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueJokMotor.isNotEmpty
            //                                     ? taksasiUnit.listValueJokMotor[2] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           )
            //                         ],
            //                       )
            //                     ],
            //                   ),
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       SizedBox(height: MediaQuery.of(context).size.height / 77),
            //       Card(
            //         child: Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Column(
            //             children: [
            //               TextFormField(
            //                   inputFormatters: [
            //                     WhitelistingTextInputFormatter.digitsOnly
            //                   ],
            //                   // onChanged: (value){
            //                   //   taksasiUnit.calculateKnalpot(value);
            //                   // },
            //                   onFieldSubmitted: (value) {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   onTap: () {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   keyboardType: TextInputType.numberWithOptions(decimal: true),
            //                   controller: taksasiUnit.controllerKnalpot,
            //                   decoration: InputDecoration(
            //                       labelText: 'Knalpot',
            //                       labelStyle: TextStyle(color: Colors.black),
            //                       border: OutlineInputBorder(
            //                           borderRadius: BorderRadius.circular(8)
            //                       )
            //                   ),
            //                 autovalidate: taksasiUnit.autoValidateCar,
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //               ),
            //               SizedBox(height: MediaQuery.of(context).size.height / 57),
            //               Container(
            //                 decoration: BoxDecoration(
            //                     color: myPrimaryColor,
            //                     borderRadius: BorderRadius.all(Radius.circular(8))
            //                 ),
            //                 child: Padding(
            //                   padding: const EdgeInsets.all(8.0),
            //                   child:
            //                   Column(
            //                     children: [
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                               child: Center(child: Text("OK")),
            //                               flex: 4
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child: Center(child: Text("CUKUP")),
            //                             flex: 4,
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child:  Center(child: Text("TIDAK")),
            //                             flex: 4,
            //                           ),
            //                         ],
            //                       ),
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueKnalpot.isNotEmpty
            //                                     ? taksasiUnit.listValueKnalpot[0] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueKnalpot.isNotEmpty
            //                                     ? taksasiUnit.listValueKnalpot[1] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueKnalpot.isNotEmpty
            //                                     ? taksasiUnit.listValueKnalpot[2] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           )
            //                         ],
            //                       )
            //                     ],
            //                   ),
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       SizedBox(height: MediaQuery.of(context).size.height / 77),
            //       Card(
            //         child: Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Column(
            //             children: [
            //               TextFormField(
            //                   inputFormatters: [
            //                     WhitelistingTextInputFormatter.digitsOnly
            //                   ],
            //                   // onChanged: (value){
            //                   //   taksasiUnit.calculateLampuBelakang(value);
            //                   // },
            //                   onFieldSubmitted: (value) {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   onTap: () {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   keyboardType: TextInputType.numberWithOptions(decimal: true),
            //                   controller: taksasiUnit.controllerLampuBelakang,
            //                   decoration: InputDecoration(
            //                       labelText: 'Lampu Belakang',
            //                       labelStyle: TextStyle(color: Colors.black),
            //                       border: OutlineInputBorder(
            //                           borderRadius: BorderRadius.circular(8)
            //                       )
            //                   ),
            //                 autovalidate: taksasiUnit.autoValidateCar,
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //               ),
            //               SizedBox(height: MediaQuery.of(context).size.height / 57),
            //               Container(
            //                 decoration: BoxDecoration(
            //                     color: myPrimaryColor,
            //                     borderRadius: BorderRadius.all(Radius.circular(8))
            //                 ),
            //                 child: Padding(
            //                   padding: const EdgeInsets.all(8.0),
            //                   child:
            //                   Column(
            //                     children: [
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                               child: Center(child: Text("OK")),
            //                               flex: 4
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child: Center(child: Text("CUKUP")),
            //                             flex: 4,
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child:  Center(child: Text("TIDAK")),
            //                             flex: 4,
            //                           ),
            //                         ],
            //                       ),
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueLampuBelakang.isNotEmpty
            //                                     ? taksasiUnit.listValueLampuBelakang[0] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueLampuBelakang.isNotEmpty
            //                                     ? taksasiUnit.listValueLampuBelakang[1] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueLampuBelakang.isNotEmpty
            //                                     ? taksasiUnit.listValueLampuBelakang[2] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           )
            //                         ],
            //                       )
            //                     ],
            //                   ),
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       SizedBox(height: MediaQuery.of(context).size.height / 77),
            //       Card(
            //         child: Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Column(
            //             children: [
            //               TextFormField(
            //                   inputFormatters: [
            //                     WhitelistingTextInputFormatter.digitsOnly
            //                   ],
            //                   // onChanged: (value){
            //                   //   taksasiUnit.calculateLampuBesarDepan(value);
            //                   // },
            //                   onFieldSubmitted: (value) {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   onTap: () {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   keyboardType: TextInputType.numberWithOptions(decimal: true),
            //                   controller: taksasiUnit.controllerLampuBesarDepan,
            //                   decoration: InputDecoration(
            //                       labelText: 'Lampu Besar Depan',
            //                       labelStyle: TextStyle(color: Colors.black),
            //                       border: OutlineInputBorder(
            //                           borderRadius: BorderRadius.circular(8)
            //                       )
            //                   ),
            //                 autovalidate: taksasiUnit.autoValidateCar,
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //               ),
            //               SizedBox(height: MediaQuery.of(context).size.height / 57),
            //               Container(
            //                 decoration: BoxDecoration(
            //                     color: myPrimaryColor,
            //                     borderRadius: BorderRadius.all(Radius.circular(8))
            //                 ),
            //                 child: Padding(
            //                   padding: const EdgeInsets.all(8.0),
            //                   child:
            //                   Column(
            //                     children: [
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                               child: Center(child: Text("OK")),
            //                               flex: 4
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child: Center(child: Text("CUKUP")),
            //                             flex: 4,
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child:  Center(child: Text("TIDAK")),
            //                             flex: 4,
            //                           ),
            //                         ],
            //                       ),
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueLampuBesarDepan.isNotEmpty
            //                                     ? taksasiUnit.listValueLampuBesarDepan[0] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueLampuBesarDepan.isNotEmpty
            //                                     ? taksasiUnit.listValueLampuBesarDepan[1] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueLampuBesarDepan.isNotEmpty
            //                                     ? taksasiUnit.listValueLampuBesarDepan[2] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           )
            //                         ],
            //                       )
            //                     ],
            //                   ),
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       SizedBox(height: MediaQuery.of(context).size.height / 77),
            //       Card(
            //         child: Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Column(
            //             children: [
            //               TextFormField(
            //                   inputFormatters: [
            //                     WhitelistingTextInputFormatter.digitsOnly
            //                   ],
            //                   // onChanged: (value){
            //                   //   taksasiUnit.calculateLampuSein(value);
            //                   // },
            //                   onFieldSubmitted: (value) {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   onTap: () {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   keyboardType: TextInputType.numberWithOptions(decimal: true),
            //                   controller: taksasiUnit.controllerLampuSein,
            //                   decoration: InputDecoration(
            //                       labelText: 'Lampu Sein',
            //                       labelStyle: TextStyle(color: Colors.black),
            //                       border: OutlineInputBorder(
            //                           borderRadius: BorderRadius.circular(8)
            //                       )
            //                   ),
            //                 autovalidate: taksasiUnit.autoValidateCar,
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //               ),
            //               SizedBox(height: MediaQuery.of(context).size.height / 57),
            //               Container(
            //                 decoration: BoxDecoration(
            //                     color: myPrimaryColor,
            //                     borderRadius: BorderRadius.all(Radius.circular(8))
            //                 ),
            //                 child: Padding(
            //                   padding: const EdgeInsets.all(8.0),
            //                   child:
            //                   Column(
            //                     children: [
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                               child: Center(child: Text("OK")),
            //                               flex: 4
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child: Center(child: Text("CUKUP")),
            //                             flex: 4,
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child:  Center(child: Text("TIDAK")),
            //                             flex: 4,
            //                           ),
            //                         ],
            //                       ),
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueLampuSein.isNotEmpty
            //                                     ? taksasiUnit.listValueLampuSein[0] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueLampuSein.isNotEmpty
            //                                     ? taksasiUnit.listValueLampuSein[1] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueLampuSein.isNotEmpty
            //                                     ? taksasiUnit.listValueLampuSein[2] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           )
            //                         ],
            //                       )
            //                     ],
            //                   ),
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       SizedBox(height: MediaQuery.of(context).size.height / 77),
            //       Card(
            //         child: Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Column(
            //             children: [
            //               TextFormField(
            //                   inputFormatters: [
            //                     WhitelistingTextInputFormatter.digitsOnly
            //                   ],
            //                   // onChanged: (value){
            //                   //   taksasiUnit.calculateWings(value);
            //                   // },
            //                   onFieldSubmitted: (value) {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   onTap: () {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   keyboardType: TextInputType.numberWithOptions(decimal: true),
            //                   controller: taksasiUnit.controllerWings,
            //                   decoration: InputDecoration(
            //                       labelText: 'Sayap Body/Wings',
            //                       labelStyle: TextStyle(color: Colors.black),
            //                       border: OutlineInputBorder(
            //                           borderRadius: BorderRadius.circular(8)
            //                       )
            //                   ),
            //                 autovalidate: taksasiUnit.autoValidateCar,
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //               ),
            //               SizedBox(height: MediaQuery.of(context).size.height / 57),
            //               Container(
            //                 decoration: BoxDecoration(
            //                     color: myPrimaryColor,
            //                     borderRadius: BorderRadius.all(Radius.circular(8))
            //                 ),
            //                 child: Padding(
            //                   padding: const EdgeInsets.all(8.0),
            //                   child:
            //                   Column(
            //                     children: [
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                               child: Center(child: Text("OK")),
            //                               flex: 4
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child: Center(child: Text("CUKUP")),
            //                             flex: 4,
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child:  Center(child: Text("TIDAK")),
            //                             flex: 4,
            //                           ),
            //                         ],
            //                       ),
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueWings.isNotEmpty
            //                                     ? taksasiUnit.listValueWings[0] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueWings.isNotEmpty
            //                                     ? taksasiUnit.listValueWings[1] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueWings.isNotEmpty
            //                                     ? taksasiUnit.listValueWings[2] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           )
            //                         ],
            //                       )
            //                     ],
            //                   ),
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       SizedBox(height: MediaQuery.of(context).size.height / 77),
            //       Card(
            //         child: Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Column(
            //             children: [
            //               TextFormField(
            //                   inputFormatters: [
            //                     WhitelistingTextInputFormatter.digitsOnly
            //                   ],
            //                   // onChanged: (value){
            //                   //   taksasiUnit.calculateSpionMotorCycle(value);
            //                   // },
            //                   onFieldSubmitted: (value) {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   onTap: () {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   keyboardType: TextInputType.numberWithOptions(decimal: true),
            //                   controller: taksasiUnit.controllerSpionMotor,
            //                   decoration: InputDecoration(
            //                       labelText: 'Spion',
            //                       labelStyle: TextStyle(color: Colors.black),
            //                       border: OutlineInputBorder(
            //                           borderRadius: BorderRadius.circular(8)
            //                       )
            //                   ),
            //                 autovalidate: taksasiUnit.autoValidateCar,
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //               ),
            //               SizedBox(height: MediaQuery.of(context).size.height / 57),
            //               Container(
            //                 decoration: BoxDecoration(
            //                     color: myPrimaryColor,
            //                     borderRadius: BorderRadius.all(Radius.circular(8))
            //                 ),
            //                 child: Padding(
            //                   padding: const EdgeInsets.all(8.0),
            //                   child:
            //                   Column(
            //                     children: [
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                               child: Center(child: Text("OK")),
            //                               flex: 4
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child: Center(child: Text("CUKUP")),
            //                             flex: 4,
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child:  Center(child: Text("TIDAK")),
            //                             flex: 4,
            //                           ),
            //                         ],
            //                       ),
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueSpionMotorCycle.isNotEmpty
            //                                     ? taksasiUnit.listValueSpionMotorCycle[0] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueSpionMotorCycle.isNotEmpty
            //                                     ? taksasiUnit.listValueSpionMotorCycle[1] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueSpionMotorCycle.isNotEmpty
            //                                     ? taksasiUnit.listValueWindShield[2] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           )
            //                         ],
            //                       )
            //                     ],
            //                   ),
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       SizedBox(height: MediaQuery.of(context).size.height / 77),
            //       Card(
            //         child: Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Column(
            //             children: [
            //               TextFormField(
            //                   inputFormatters: [
            //                     WhitelistingTextInputFormatter.digitsOnly
            //                   ],
            //                   // onChanged: (value){
            //                   //   taksasiUnit.calculateStarter(value);
            //                   // },
            //                   onFieldSubmitted: (value) {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   onTap: () {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   keyboardType: TextInputType.numberWithOptions(decimal: true),
            //                   controller: taksasiUnit.controllerStarter,
            //                   decoration: InputDecoration(
            //                       labelText: 'Starter: Kunci Kontak & Kick Start',
            //                       labelStyle: TextStyle(color: Colors.black),
            //                       border: OutlineInputBorder(
            //                           borderRadius: BorderRadius.circular(8)
            //                       )
            //                   ),
            //                 autovalidate: taksasiUnit.autoValidateCar,
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //               ),
            //               SizedBox(height: MediaQuery.of(context).size.height / 57),
            //               Container(
            //                 decoration: BoxDecoration(
            //                     color: myPrimaryColor,
            //                     borderRadius: BorderRadius.all(Radius.circular(8))
            //                 ),
            //                 child: Padding(
            //                   padding: const EdgeInsets.all(8.0),
            //                   child:
            //                   Column(
            //                     children: [
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                               child: Center(child: Text("OK")),
            //                               flex: 4
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child: Center(child: Text("CUKUP")),
            //                             flex: 4,
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child:  Center(child: Text("TIDAK")),
            //                             flex: 4,
            //                           ),
            //                         ],
            //                       ),
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueStarter.isNotEmpty
            //                                     ? taksasiUnit.listValueStarter[0] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueStarter.isNotEmpty
            //                                     ? taksasiUnit.listValueStarter[1] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueStarter.isNotEmpty
            //                                     ? taksasiUnit.listValueStarter[2] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           )
            //                         ],
            //                       )
            //                     ],
            //                   ),
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       SizedBox(height: MediaQuery.of(context).size.height / 77),
            //       Card(
            //         child: Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Column(
            //             children: [
            //               TextFormField(
            //                   inputFormatters: [
            //                     WhitelistingTextInputFormatter.digitsOnly
            //                   ],
            //                   // onChanged: (value){
            //                   //   taksasiUnit.calculateIronTankCover(value);
            //                   // },
            //                   onFieldSubmitted: (value) {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   onTap: () {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   keyboardType: TextInputType.numberWithOptions(decimal: true),
            //                   controller: taksasiUnit.controllerIronTubAndCover,
            //                   decoration: InputDecoration(
            //                       labelText: 'Tangki mesin dan Tutup',
            //                       labelStyle: TextStyle(color: Colors.black),
            //                       border: OutlineInputBorder(
            //                           borderRadius: BorderRadius.circular(8)
            //                       )
            //                   ),
            //                 autovalidate: taksasiUnit.autoValidateCar,
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //               ),
            //               SizedBox(height: MediaQuery.of(context).size.height / 57),
            //               Container(
            //                 decoration: BoxDecoration(
            //                     color: myPrimaryColor,
            //                     borderRadius: BorderRadius.all(Radius.circular(8))
            //                 ),
            //                 child: Padding(
            //                   padding: const EdgeInsets.all(8.0),
            //                   child:
            //                   Column(
            //                     children: [
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                               child: Center(child: Text("OK")),
            //                               flex: 4
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child: Center(child: Text("CUKUP")),
            //                             flex: 4,
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child:  Center(child: Text("TIDAK")),
            //                             flex: 4,
            //                           ),
            //                         ],
            //                       ),
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueIronTankCover.isNotEmpty
            //                                     ? taksasiUnit.listValueIronTankCover[0] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueIronTankCover.isNotEmpty
            //                                     ? taksasiUnit.listValueIronTankCover[1] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueIronTankCover.isNotEmpty
            //                                     ? taksasiUnit.listValueIronTankCover[2] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           )
            //                         ],
            //                       )
            //                     ],
            //                   ),
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       SizedBox(height: MediaQuery.of(context).size.height / 77),
            //       Card(
            //         child: Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Column(
            //             children: [
            //               TextFormField(
            //                   inputFormatters: [
            //                     WhitelistingTextInputFormatter.digitsOnly
            //                   ],
            //                   // onChanged: (value){
            //                   //   taksasiUnit.calculateBakBelakang(value);
            //                   // },
            //                   onFieldSubmitted: (value) {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   onTap: () {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   keyboardType: TextInputType.numberWithOptions(decimal: true),
            //                   controller: taksasiUnit.controllerBakBelakang,
            //                   decoration: InputDecoration(
            //                       labelText: 'Bak Belakang',
            //                       labelStyle: TextStyle(color: Colors.black),
            //                       border: OutlineInputBorder(
            //                           borderRadius: BorderRadius.circular(8)
            //                       )
            //                   ),
            //                 autovalidate: taksasiUnit.autoValidateCar,
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //               ),
            //               SizedBox(height: MediaQuery.of(context).size.height / 57),
            //               Container(
            //                 decoration: BoxDecoration(
            //                     color: myPrimaryColor,
            //                     borderRadius: BorderRadius.all(Radius.circular(8))
            //                 ),
            //                 child: Padding(
            //                   padding: const EdgeInsets.all(8.0),
            //                   child:
            //                   Column(
            //                     children: [
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                               child: Center(child: Text("OK")),
            //                               flex: 4
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child: Center(child: Text("CUKUP")),
            //                             flex: 4,
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child:  Center(child: Text("TIDAK")),
            //                             flex: 4,
            //                           ),
            //                         ],
            //                       ),
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueBakBelakang.isNotEmpty
            //                                     ? taksasiUnit.listValueBakBelakang[0] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueBakBelakang.isNotEmpty
            //                                     ? taksasiUnit.listValueBakBelakang[1] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueBakBelakang.isNotEmpty
            //                                     ? taksasiUnit.listValueBakBelakang[2] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           )
            //                         ],
            //                       )
            //                     ],
            //                   ),
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       SizedBox(height: MediaQuery.of(context).size.height / 77),
            //       Card(
            //         child: Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Column(
            //             children: [
            //               TextFormField(
            //                   inputFormatters: [
            //                     WhitelistingTextInputFormatter.digitsOnly
            //                   ],
            //                   // onChanged: (value){
            //                   //   taksasiUnit.calculateMachineAndBlokMachine(value);
            //                   // },
            //                   onFieldSubmitted: (value) {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   onTap: () {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   keyboardType: TextInputType.numberWithOptions(decimal: true),
            //                   controller: taksasiUnit.controllerMachineAndBlokMachine,
            //                   decoration: InputDecoration(
            //                       labelText: 'Mesin & Blok Mesin',
            //                       labelStyle: TextStyle(color: Colors.black),
            //                       border: OutlineInputBorder(
            //                           borderRadius: BorderRadius.circular(8)
            //                       )
            //                   ),
            //                 autovalidate: taksasiUnit.autoValidateCar,
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //               ),
            //               SizedBox(height: MediaQuery.of(context).size.height / 57),
            //               Container(
            //                 decoration: BoxDecoration(
            //                     color: myPrimaryColor,
            //                     borderRadius: BorderRadius.all(Radius.circular(8))
            //                 ),
            //                 child: Padding(
            //                   padding: const EdgeInsets.all(8.0),
            //                   child:
            //                   Column(
            //                     children: [
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                               child: Center(child: Text("OK")),
            //                               flex: 4
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child: Center(child: Text("CUKUP")),
            //                             flex: 4,
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child:  Center(child: Text("TIDAK")),
            //                             flex: 4,
            //                           ),
            //                         ],
            //                       ),
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueMachineBlokMachine.isNotEmpty
            //                                     ? taksasiUnit.listValueMachineBlokMachine[0] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueMachineBlokMachine.isNotEmpty
            //                                     ? taksasiUnit.listValueMachineBlokMachine[1] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueMachineBlokMachine.isNotEmpty
            //                                     ? taksasiUnit.listValueMachineBlokMachine[2] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           )
            //                         ],
            //                       )
            //                     ],
            //                   ),
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       SizedBox(height: MediaQuery.of(context).size.height / 77),
            //       Card(
            //         child: Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Column(
            //             children: [
            //               TextFormField(
            //                   inputFormatters: [
            //                     WhitelistingTextInputFormatter.digitsOnly
            //                   ],
            //                   // onChanged: (value){
            //                   //   taksasiUnit.calculateRadiatorWaterPump(value);
            //                   // },
            //                   onFieldSubmitted: (value) {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   onTap: () {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   keyboardType: TextInputType.numberWithOptions(decimal: true),
            //                   controller: taksasiUnit.controllerRadiatorWaterPump,
            //                   decoration: InputDecoration(
            //                       labelText: 'Radiator & Waterpump',
            //                       labelStyle: TextStyle(color: Colors.black),
            //                       border: OutlineInputBorder(
            //                           borderRadius: BorderRadius.circular(8)
            //                       )
            //                   ),
            //                 autovalidate: taksasiUnit.autoValidateCar,
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //               ),
            //               SizedBox(height: MediaQuery.of(context).size.height / 57),
            //               Container(
            //                 decoration: BoxDecoration(
            //                     color: myPrimaryColor,
            //                     borderRadius: BorderRadius.all(Radius.circular(8))
            //                 ),
            //                 child: Padding(
            //                   padding: const EdgeInsets.all(8.0),
            //                   child:
            //                   Column(
            //                     children: [
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                               child: Center(child: Text("OK")),
            //                               flex: 4
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child: Center(child: Text("CUKUP")),
            //                             flex: 4,
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child:  Center(child: Text("TIDAK")),
            //                             flex: 4,
            //                           ),
            //                         ],
            //                       ),
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueRadiatorWaterPump.isNotEmpty
            //                                     ? taksasiUnit.listValueRadiatorWaterPump[0] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueRadiatorWaterPump.isNotEmpty
            //                                     ? taksasiUnit.listValueRadiatorWaterPump[1] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueRadiatorWaterPump.isNotEmpty
            //                                     ? taksasiUnit.listValueRadiatorWaterPump[2] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           )
            //                         ],
            //                       )
            //                     ],
            //                   ),
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       SizedBox(height: MediaQuery.of(context).size.height / 77),
            //       Card(
            //         child: Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Column(
            //             children: [
            //               TextFormField(
            //                   inputFormatters: [
            //                     WhitelistingTextInputFormatter.digitsOnly
            //                   ],
            //                   // onChanged: (value){
            //                   //   taksasiUnit.calculateKarburator(value);
            //                   // },
            //                   onFieldSubmitted: (value) {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   onTap: () {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   keyboardType: TextInputType.numberWithOptions(decimal: true),
            //                   controller: taksasiUnit.controllerKarburator,
            //                   decoration: InputDecoration(
            //                       labelText: 'Karburator',
            //                       labelStyle: TextStyle(color: Colors.black),
            //                       border: OutlineInputBorder(
            //                           borderRadius: BorderRadius.circular(8)
            //                       )
            //                   ),
            //                 autovalidate: taksasiUnit.autoValidateCar,
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //               ),
            //               SizedBox(height: MediaQuery.of(context).size.height / 57),
            //               Container(
            //                 decoration: BoxDecoration(
            //                     color: myPrimaryColor,
            //                     borderRadius: BorderRadius.all(Radius.circular(8))
            //                 ),
            //                 child: Padding(
            //                   padding: const EdgeInsets.all(8.0),
            //                   child:
            //                   Column(
            //                     children: [
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                               child: Center(child: Text("OK")),
            //                               flex: 4
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child: Center(child: Text("CUKUP")),
            //                             flex: 4,
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child:  Center(child: Text("TIDAK")),
            //                             flex: 4,
            //                           ),
            //                         ],
            //                       ),
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueKarburator.isNotEmpty
            //                                     ? taksasiUnit.listValueKarburator[0] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueKarburator.isNotEmpty
            //                                     ? taksasiUnit.listValueKarburator[1] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueKarburator.isNotEmpty
            //                                     ? taksasiUnit.listValueKarburator[2] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           )
            //                         ],
            //                       )
            //                     ],
            //                   ),
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       SizedBox(height: MediaQuery.of(context).size.height / 77),
            //       Card(
            //         child: Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Column(
            //             children: [
            //               TextFormField(
            //                   inputFormatters: [
            //                     WhitelistingTextInputFormatter.digitsOnly
            //                   ],
            //                   // onChanged: (value){
            //                   //   taksasiUnit.calculateGearSetCVT(value);
            //                   // },
            //                   onFieldSubmitted: (value) {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   onTap: () {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   keyboardType: TextInputType.numberWithOptions(decimal: true),
            //                   controller: taksasiUnit.controllerGearSetChainCVT,
            //                   decoration: InputDecoration(
            //                       labelText: 'Gear Set & Rantai/ CVT(Matic)',
            //                       labelStyle: TextStyle(color: Colors.black),
            //                       border: OutlineInputBorder(
            //                           borderRadius: BorderRadius.circular(8)
            //                       )
            //                   ),
            //                 autovalidate: taksasiUnit.autoValidateCar,
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //               ),
            //               SizedBox(height: MediaQuery.of(context).size.height / 57),
            //               Container(
            //                 decoration: BoxDecoration(
            //                     color: myPrimaryColor,
            //                     borderRadius: BorderRadius.all(Radius.circular(8))
            //                 ),
            //                 child: Padding(
            //                   padding: const EdgeInsets.all(8.0),
            //                   child:
            //                   Column(
            //                     children: [
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                               child: Center(child: Text("OK")),
            //                               flex: 4
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child: Center(child: Text("CUKUP")),
            //                             flex: 4,
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child:  Center(child: Text("TIDAK")),
            //                             flex: 4,
            //                           ),
            //                         ],
            //                       ),
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueGearSetCVT.isNotEmpty
            //                                     ? taksasiUnit.listValueGearSetCVT[0] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueGearSetCVT.isNotEmpty
            //                                     ? taksasiUnit.listValueGearSetCVT[1] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueGearSetCVT.isNotEmpty
            //                                     ? taksasiUnit.listValueGearSetCVT[2] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           )
            //                         ],
            //                       )
            //                     ],
            //                   ),
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       SizedBox(height: MediaQuery.of(context).size.height / 77),
            //       Card(
            //         child: Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Column(
            //             children: [
            //               TextFormField(
            //                   inputFormatters: [
            //                     WhitelistingTextInputFormatter.digitsOnly
            //                   ],
            //                   // onChanged: (value){
            //                   //   taksasiUnit.calculateMechanicBreak(value);
            //                   // },
            //                   onFieldSubmitted: (value) {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   onTap: () {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   keyboardType: TextInputType.numberWithOptions(decimal: true),
            //                   controller: taksasiUnit.controllerMechanicBreak,
            //                   decoration: InputDecoration(
            //                       labelText: 'Mekanik rem (Cakram/Tromol)',
            //                       labelStyle: TextStyle(color: Colors.black),
            //                       border: OutlineInputBorder(
            //                           borderRadius: BorderRadius.circular(8)
            //                       )
            //                   ),
            //                 autovalidate: taksasiUnit.autoValidateCar,
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //               ),
            //               SizedBox(height: MediaQuery.of(context).size.height / 57),
            //               Container(
            //                 decoration: BoxDecoration(
            //                     color: myPrimaryColor,
            //                     borderRadius: BorderRadius.all(Radius.circular(8))
            //                 ),
            //                 child: Padding(
            //                   padding: const EdgeInsets.all(8.0),
            //                   child:
            //                   Column(
            //                     children: [
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                               child: Center(child: Text("OK")),
            //                               flex: 4
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child: Center(child: Text("CUKUP")),
            //                             flex: 4,
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child:  Center(child: Text("TIDAK")),
            //                             flex: 4,
            //                           ),
            //                         ],
            //                       ),
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueMechanicBreak.isNotEmpty
            //                                     ? taksasiUnit.listValueMechanicBreak[0] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueMechanicBreak.isNotEmpty
            //                                     ? taksasiUnit.listValueMechanicBreak[1] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueMechanicBreak.isNotEmpty
            //                                     ? taksasiUnit.listValueMechanicBreak[2] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           )
            //                         ],
            //                       )
            //                     ],
            //                   ),
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       SizedBox(height: MediaQuery.of(context).size.height / 77),
            //       Card(
            //         child: Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Column(
            //             children: [
            //               TextFormField(
            //                   inputFormatters: [
            //                     WhitelistingTextInputFormatter.digitsOnly
            //                   ],
            //                   // onChanged: (value){
            //                   //   taksasiUnit.calculateSwingArmBosh(value);
            //                   // },
            //                   onFieldSubmitted: (value) {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   onTap: () {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   keyboardType: TextInputType.numberWithOptions(decimal: true),
            //                   controller: taksasiUnit.controllerSwingArmBosh,
            //                   decoration: InputDecoration(
            //                       labelText: 'Swing Arm dan Bosh',
            //                       labelStyle: TextStyle(color: Colors.black),
            //                       border: OutlineInputBorder(
            //                           borderRadius: BorderRadius.circular(8)
            //                       )
            //                   ),
            //                 autovalidate: taksasiUnit.autoValidateCar,
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //               ),
            //               SizedBox(height: MediaQuery.of(context).size.height / 57),
            //               Container(
            //                 decoration: BoxDecoration(
            //                     color: myPrimaryColor,
            //                     borderRadius: BorderRadius.all(Radius.circular(8))
            //                 ),
            //                 child: Padding(
            //                   padding: const EdgeInsets.all(8.0),
            //                   child:
            //                   Column(
            //                     children: [
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                               child: Center(child: Text("OK")),
            //                               flex: 4
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child: Center(child: Text("CUKUP")),
            //                             flex: 4,
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child:  Center(child: Text("TIDAK")),
            //                             flex: 4,
            //                           ),
            //                         ],
            //                       ),
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueSwingArmBosh.isNotEmpty
            //                                     ? taksasiUnit.listValueSwingArmBosh[0] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueSwingArmBosh.isNotEmpty
            //                                     ? taksasiUnit.listValueSwingArmBosh[1] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueSwingArmBosh.isNotEmpty
            //                                     ? taksasiUnit.listValueSwingArmBosh[2] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           )
            //                         ],
            //                       )
            //                     ],
            //                   ),
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       SizedBox(height: MediaQuery.of(context).size.height / 77),
            //       Card(
            //         child: Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Column(
            //             children: [
            //               TextFormField(
            //                   inputFormatters: [
            //                     WhitelistingTextInputFormatter.digitsOnly
            //                   ],
            //                   // onChanged: (value){
            //                   //   taksasiUnit.calculateShockAbsorber(value);
            //                   // },
            //                   onFieldSubmitted: (value) {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   onTap: () {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   keyboardType: TextInputType.numberWithOptions(decimal: true),
            //                   controller: taksasiUnit.controllerBackShockAbsorber,
            //                   decoration: InputDecoration(
            //                       labelText: 'Shock Absorber Belakang',
            //                       labelStyle: TextStyle(color: Colors.black),
            //                       border: OutlineInputBorder(
            //                           borderRadius: BorderRadius.circular(8)
            //                       )
            //                   ),
            //                 autovalidate: taksasiUnit.autoValidateCar,
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //               ),
            //               SizedBox(height: MediaQuery.of(context).size.height / 57),
            //               Container(
            //                 decoration: BoxDecoration(
            //                     color: myPrimaryColor,
            //                     borderRadius: BorderRadius.all(Radius.circular(8))
            //                 ),
            //                 child: Padding(
            //                   padding: const EdgeInsets.all(8.0),
            //                   child:
            //                   Column(
            //                     children: [
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                               child: Center(child: Text("OK")),
            //                               flex: 4
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child: Center(child: Text("CUKUP")),
            //                             flex: 4,
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child:  Center(child: Text("TIDAK")),
            //                             flex: 4,
            //                           ),
            //                         ],
            //                       ),
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueShockAbsorber.isNotEmpty
            //                                     ? taksasiUnit.listValueShockAbsorber[0] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueShockAbsorber.isNotEmpty
            //                                     ? taksasiUnit.listValueShockAbsorber[1] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueShockAbsorber.isNotEmpty
            //                                     ? taksasiUnit.listValueShockAbsorber[2] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           )
            //                         ],
            //                       )
            //                     ],
            //                   ),
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       SizedBox(height: MediaQuery.of(context).size.height / 77),
            //       Card(
            //         child: Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Column(
            //             children: [
            //               TextFormField(
            //                   inputFormatters: [
            //                     WhitelistingTextInputFormatter.digitsOnly
            //                   ],
            //                   // onChanged: (value){
            //                   //   taksasiUnit.calculateStangForkAbsorber(value);
            //                   // },
            //                   onFieldSubmitted: (value) {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   onTap: () {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   keyboardType: TextInputType.numberWithOptions(decimal: true),
            //                   controller: taksasiUnit.controllerStangForkShock,
            //                   decoration: InputDecoration(
            //                       labelText: 'Stang,Fork,Shock Depan',
            //                       labelStyle: TextStyle(color: Colors.black),
            //                       border: OutlineInputBorder(
            //                           borderRadius: BorderRadius.circular(8)
            //                       )
            //                   ),
            //                 autovalidate: taksasiUnit.autoValidateCar,
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //               ),
            //               SizedBox(height: MediaQuery.of(context).size.height / 57),
            //               Container(
            //                 decoration: BoxDecoration(
            //                     color: myPrimaryColor,
            //                     borderRadius: BorderRadius.all(Radius.circular(8))
            //                 ),
            //                 child: Padding(
            //                   padding: const EdgeInsets.all(8.0),
            //                   child:
            //                   Column(
            //                     children: [
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                               child: Center(child: Text("OK")),
            //                               flex: 4
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child: Center(child: Text("CUKUP")),
            //                             flex: 4,
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child:  Center(child: Text("TIDAK")),
            //                             flex: 4,
            //                           ),
            //                         ],
            //                       ),
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueStangForkShockDepan.isNotEmpty
            //                                     ? taksasiUnit.listValueStangForkShockDepan[0] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueStangForkShockDepan.isNotEmpty
            //                                     ? taksasiUnit.listValueStangForkShockDepan[1] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueStangForkShockDepan.isNotEmpty
            //                                     ? taksasiUnit.listValueStangForkShockDepan[2] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           )
            //                         ],
            //                       )
            //                     ],
            //                   ),
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       SizedBox(height: MediaQuery.of(context).size.height / 77),
            //       Card(
            //         child: Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Column(
            //             children: [
            //               TextFormField(
            //                   inputFormatters: [
            //                     WhitelistingTextInputFormatter.digitsOnly
            //                   ],
            //                   // onChanged: (value){
            //                   //   taksasiUnit.calculateVelg(value);
            //                   // },
            //                   onFieldSubmitted: (value) {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   onTap: () {
            //                     taksasiUnit.calculatedMotorCycle(context);
            //                   },
            //                   keyboardType: TextInputType.numberWithOptions(decimal: true),
            //                   controller: taksasiUnit.controllerVelg,
            //                   decoration: InputDecoration(
            //                       labelText: 'Velg Racing/Satndard Depan Belakang',
            //                       labelStyle: TextStyle(color: Colors.black),
            //                       border: OutlineInputBorder(
            //                           borderRadius: BorderRadius.circular(8)
            //                       )
            //                   ),
            //                 autovalidate: taksasiUnit.autoValidateCar,
            //                 validator: (e) {
            //                   if (e.isEmpty) {
            //                     return "Tidak boleh kosong";
            //                   } else {
            //                     return null;
            //                   }
            //                 },
            //               ),
            //               SizedBox(height: MediaQuery.of(context).size.height / 57),
            //               Container(
            //                 decoration: BoxDecoration(
            //                     color: myPrimaryColor,
            //                     borderRadius: BorderRadius.all(Radius.circular(8))
            //                 ),
            //                 child: Padding(
            //                   padding: const EdgeInsets.all(8.0),
            //                   child:
            //                   Column(
            //                     children: [
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                               child: Center(child: Text("OK")),
            //                               flex: 4
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child: Center(child: Text("CUKUP")),
            //                             flex: 4,
            //                           ),
            //                           SizedBox(
            //                             height: MediaQuery.of(context).size.height/37,
            //                             child: VerticalDivider(
            //                               width: 2,
            //                               color: Colors.black,
            //                             ),
            //                           ),
            //                           Expanded(
            //                             child:  Center(child: Text("TIDAK")),
            //                             flex: 4,
            //                           ),
            //                         ],
            //                       ),
            //                       Row(
            //                         children: [
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueVelg.isNotEmpty
            //                                     ? taksasiUnit.listValueVelg[0] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueVelg.isNotEmpty
            //                                     ? taksasiUnit.listValueVelg[1] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           ),
            //                           Expanded(
            //                             flex: 4,
            //                             child: Visibility(
            //                                 visible: taksasiUnit.listValueVelg.isNotEmpty
            //                                     ? taksasiUnit.listValueVelg[2] : false,
            //                                 child: Icon(Icons.check,color: Colors.green)
            //                             ),
            //                           )
            //                         ],
            //                       )
            //                     ],
            //                   ),
            //                 ),
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //     ],
            //   ),
            // );
          },
        );
      },
    );
  }
}
