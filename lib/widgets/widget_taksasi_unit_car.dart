import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/taksasi_unit_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class WidgetTaksasiUnitCar extends StatefulWidget {
  final String kode;
  const WidgetTaksasiUnitCar({this.kode});
  @override
  _WidgetTaksasiUnitCarState createState() => _WidgetTaksasiUnitCarState();
}

class _WidgetTaksasiUnitCarState extends State<WidgetTaksasiUnitCar> {

  Future<void> _getData;
  @override
  void initState() {
    super.initState();
    // if(widget.kode != Provider.of<TaksasiUnitChangeNotifier>(context, listen: false).kode){
    //   _getData = Provider.of<TaksasiUnitChangeNotifier>(context, listen: false).getListTaksasi(context,null,widget.kode, "");
    //   Provider.of<TaksasiUnitChangeNotifier>(context, listen: false).kode = widget.kode;
    // }

    _getData = Provider.of<TaksasiUnitChangeNotifier>(context, listen: false).getListTaksasi(context,null);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _getData,
        builder: (context,snapshot){
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          return Consumer<TaksasiUnitChangeNotifier>(
            builder: (context, taksasiUnit, child) {
              return ListView.builder(
                  padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height/77,
                    horizontal: MediaQuery.of(context).size.width/37,
                  ),
                  itemBuilder: (context, index) {
                    if(index == taksasiUnit.listTaksasi.length) {
                      return Column(
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  child: Visibility(
                                    visible: taksasiUnit.listTaksasi.isNotEmpty,
                                    child: Padding(
                                      padding: EdgeInsets.all(8.0),
                                      child: TextFormField(
                                        enabled: false,
                                        keyboardType: TextInputType.numberWithOptions(decimal: true),
                                        controller: taksasiUnit.controllerTotalNilaiBobot,
                                        decoration: InputDecoration(
                                            labelText: "Total Nilai Bobot",
                                            labelStyle: TextStyle(color: Colors.black),
                                            fillColor: Colors.black12,
                                            filled: true,
                                            border: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(8)
                                            ),
                                            suffixIconConstraints: BoxConstraints(
                                              minWidth: 20,
                                              minHeight: 20,
                                              maxHeight: 25,
                                              maxWidth: 30,),
                                            suffixIcon: GestureDetector(
                                              child: Padding(
                                                padding: EdgeInsets.only(right: 8.0),
                                                child: Container(
                                                  child: Text("%"),
                                                ),
                                              ),
                                            ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(width: 8.0),
                                Expanded(
                                  child: Visibility(
                                    visible: taksasiUnit.listTaksasi.isNotEmpty,
                                    child: Padding(
                                      padding: EdgeInsets.all(8.0),
                                      child: RaisedButton(
                                          shape: RoundedRectangleBorder(
                                              borderRadius: new BorderRadius.circular(8.0)),
                                          color: myPrimaryColor,
                                          onPressed: () {
                                            taksasiUnit.calculatedCar(context);
                                          },
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              Text("CALCULATE",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 14,
                                                      fontWeight: FontWeight.w500,
                                                      letterSpacing: 1.25))
                                            ],
                                          )
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            )
                          ],
                      );
                    } else {
                      return Card(
                        elevation: 3.3,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                    // flex: 2,
                                    child: TextFormField(
                                      inputFormatters: [
                                        WhitelistingTextInputFormatter.digitsOnly
                                      ],
                                      onChanged: (value){
                                        if(value.isNotEmpty){
                                          taksasiUnit.limitInput(index,value);
                                        }
                                      },
                                      onFieldSubmitted: (value) {
                                        taksasiUnit.setStatus();
                                      },
                                      onTap: () {
                                        taksasiUnit.setStatus();
                                      },
                                      keyboardType: TextInputType.numberWithOptions(decimal: true),
                                      controller: taksasiUnit.listTaksasi[index].CONTROLLER,
                                      decoration: InputDecoration(
                                          labelText: taksasiUnit.listTaksasi[index].TAKSASI_DESC,
                                          labelStyle: TextStyle(color: Colors.black),
                                          border: OutlineInputBorder(
                                              borderRadius: BorderRadius.circular(8)
                                          ),
                                          suffixIconConstraints: BoxConstraints(
                                            minWidth: 20,
                                            minHeight: 20,
                                            maxHeight: 25,
                                            maxWidth: 30,),
                                          suffixIcon: GestureDetector(
                                            child: Padding(
                                              padding: EdgeInsets.only(right: 8.0),
                                              child: Container(
                                                child: Text("%"),
                                              ),
                                            ),
                                          ),
                                      ),
                                      autovalidate: taksasiUnit.listTaksasi[index].AUTOVALIDATE,
                                      validator: (e) {
                                        if (e.isEmpty) {
                                          return "Tidak boleh kosong";
                                        } else {
                                          return null;
                                        }
                                      },
                                    ),
                                  ),
                                  // SizedBox(width: 8.0),
                                  // Expanded(
                                  //   flex: 2,
                                  //   child: TextFormField(
                                  //     // autovalidate: taksasiUnit.listTaksasi[index].AUTOVALIDATE,
                                  //     // validator: (e) {
                                  //     //   if (e.isEmpty) {
                                  //     //     return "Tidak boleh kosong";
                                  //     //   } else {
                                  //     //     return null;
                                  //     //   }
                                  //     // },
                                  //     inputFormatters: [
                                  //       WhitelistingTextInputFormatter.digitsOnly
                                  //     ],
                                  //     // onChanged: (value){
                                  //     //   if(value.isNotEmpty){
                                  //     //     taksasiUnit.setValidate(index);
                                  //     //   }
                                  //     // },
                                  //     enabled: false,
                                  //     keyboardType: TextInputType.numberWithOptions(decimal: true),
                                  //     controller: taksasiUnit.listTaksasi[index].CONTROLLER_NILAI_BOBOT,
                                  //     decoration: InputDecoration(
                                  //         labelText: "BOBOT ${taksasiUnit.listTaksasi[index].TAKSASI_DESC}",
                                  //         labelStyle: TextStyle(color: Colors.black),
                                  //         filled: true,
                                  //         fillColor: Colors.black12,
                                  //         border: OutlineInputBorder(
                                  //             borderRadius: BorderRadius.circular(8)
                                  //         )
                                  //     ),
                                  //   ),
                                  // )
                                ],
                              ),
                              SizedBox(height: MediaQuery.of(context).size.height / 57),
                              Container(
                                decoration: BoxDecoration(
                                    color: myPrimaryColor,
                                    borderRadius: BorderRadius.all(Radius.circular(8))
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    children: [
                                      Row(
                                        children: [
                                          Expanded(
                                              child: Center(child: Text("OK")),
                                              flex: 4
                                          ),
                                          SizedBox(
                                            height: MediaQuery.of(context).size.height/37,
                                            child: VerticalDivider(
                                              width: 2,
                                              color: Colors.black,
                                            ),
                                          ),
                                          Expanded(
                                            child: Center(child: Text("CUKUP")),
                                            flex: 4,
                                          ),
                                          SizedBox(
                                            height: MediaQuery.of(context).size.height/37,
                                            child: VerticalDivider(
                                              width: 2,
                                              color: Colors.black,
                                            ),
                                          ),
                                          Expanded(
                                            child:  Center(child: Text("TIDAK")),
                                            flex: 4,
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Expanded(
                                            flex: 4,
                                            child: Visibility(
                                                visible: taksasiUnit.listTaksasi[index].STATUS[0],
                                                child: Icon(Icons.check,color: Colors.green)
                                            ),
                                          ),
                                          Expanded(
                                            flex: 4,
                                            child: Visibility(
                                                visible: taksasiUnit.listTaksasi[index].STATUS[1],
                                                child: Icon(Icons.check,color: Colors.green)
                                            ),
                                          ),
                                          Expanded(
                                            flex: 4,
                                            child: Visibility(
                                                visible: taksasiUnit.listTaksasi[index].STATUS[2],
                                                child: Icon(Icons.check,color: Colors.green)
                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    }
                  },
                  itemCount: taksasiUnit.listTaksasi.length+1);
//          SingleChildScrollView(
//          padding: EdgeInsets.symmetric(
//              vertical: MediaQuery.of(context).size.height / 47,
//              horizontal: MediaQuery.of(context).size.width / 27),
//          child: Column(
//            children: [
//              Card(
//                child: Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: Column(
//                    children: [
//                      TextFormField(
//                          inputFormatters: [
//                            WhitelistingTextInputFormatter.digitsOnly
//                          ],
//                          // onChanged: (value){
//                          //   taksasiUnit.calculateEngineSound(value);
//                          // },
//                          onFieldSubmitted: (value) {
//                            taksasiUnit.calculatedCar(context);
//                          },
//                          onTap: () {
//                            taksasiUnit.calculatedCar(context);
//                          },
//                          keyboardType: TextInputType.numberWithOptions(decimal: true),
//                          controller: taksasiUnit.controllerEngineSound,
//                          decoration: InputDecoration(
//                              labelText: 'Suara Mesin',
//                              labelStyle: TextStyle(color: Colors.black),
//                              border: OutlineInputBorder(
//                                  borderRadius: BorderRadius.circular(8)
//                              )
//                          ),
//                        autovalidate: taksasiUnit.autoValidateCar,
//                        validator: (e) {
//                          if (e.isEmpty) {
//                            return "Tidak boleh kosong";
//                          } else {
//                            return null;
//                          }
//                        },
//                      ),
//                      SizedBox(height: MediaQuery.of(context).size.height / 57),
//                      Container(
//                        decoration: BoxDecoration(
//                          color: myPrimaryColor,
//                            borderRadius: BorderRadius.all(Radius.circular(8))
//                        ),
//                        child: Padding(
//                          padding: const EdgeInsets.all(8.0),
//                          child:
//                          Column(
//                            children: [
//                              Row(
//                                children: [
//                                  Expanded(
//                                      child: Center(child: Text("OK")),
//                                      flex: 4
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child: Center(child: Text("CUKUP")),
//                                    flex: 4,
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child:  Center(child: Text("TIDAK")),
//                                    flex: 4,
//                                  ),
//                                ],
//                              ),
//                              Row(
//                                children: [
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueEngineSound.isNotEmpty
//                                            ? taksasiUnit.listValueEngineSound[0] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueEngineSound.isNotEmpty
//                                            ? taksasiUnit.listValueEngineSound[1] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueEngineSound.isNotEmpty
//                                            ? taksasiUnit.listValueEngineSound[2] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  )
//                                ],
//                              )
//                            ],
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//              ),
//              SizedBox(height: MediaQuery.of(context).size.height / 77),
//              Card(
//                child: Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: Column(
//                    children: [
//                      TextFormField(
//                          inputFormatters: [
//                            WhitelistingTextInputFormatter.digitsOnly
//                          ],
//                          // onChanged: (value){
//                          //   taksasiUnit.calculateEngineClean(value);
//                          // },
//                          onFieldSubmitted: (value) {
//                            taksasiUnit.calculatedCar(context);
//                          },
//                          onTap: () {
//                            taksasiUnit.calculatedCar(context);
//                          },
//                          keyboardType: TextInputType.numberWithOptions(decimal: true),
//                          controller: taksasiUnit.controllerMachineClean,
//                          decoration: InputDecoration(
//                              labelText: 'Kebersihan Mesin',
//                              labelStyle: TextStyle(color: Colors.black),
//                              border: OutlineInputBorder(
//                                  borderRadius: BorderRadius.circular(8)
//                              )
//                          ),
//                        autovalidate: taksasiUnit.autoValidateCar,
//                        validator: (e) {
//                          if (e.isEmpty) {
//                            return "Tidak boleh kosong";
//                          } else {
//                            return null;
//                          }
//                        },
//                      ),
//                      SizedBox(height: MediaQuery.of(context).size.height / 57),
//                      Container(
//                        decoration: BoxDecoration(
//                            color: myPrimaryColor,
//                            borderRadius: BorderRadius.all(Radius.circular(8))
//                        ),
//                        child: Padding(
//                          padding: const EdgeInsets.all(8.0),
//                          child:
//                          Column(
//                            children: [
//                              Row(
//                                children: [
//                                  Expanded(
//                                      child: Center(child: Text("OK")),
//                                      flex: 4
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child: Center(child: Text("CUKUP")),
//                                    flex: 4,
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child:  Center(child: Text("TIDAK")),
//                                    flex: 4,
//                                  ),
//                                ],
//                              ),
//                              Row(
//                                children: [
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueEngineClean.isNotEmpty
//                                            ? taksasiUnit.listValueEngineClean[0] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueEngineClean.isNotEmpty
//                                            ? taksasiUnit.listValueEngineClean[1] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueEngineClean.isNotEmpty
//                                            ? taksasiUnit.listValueEngineClean[2] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  )
//                                ],
//                              )
//                            ],
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//              ),
//              SizedBox(height: MediaQuery.of(context).size.height / 77),
//              Card(
//                child: Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: Column(
//                    children: [
//                      TextFormField(
//                          inputFormatters: [
//                            WhitelistingTextInputFormatter.digitsOnly
//                          ],
//                          // onChanged: (value){
//                          //   taksasiUnit.calculateAc(value);
//                          // },
//                          onFieldSubmitted: (value) {
//                            taksasiUnit.calculatedCar(context);
//                          },
//                          onTap: () {
//                            taksasiUnit.calculatedCar(context);
//                          },
//                          keyboardType: TextInputType.numberWithOptions(decimal: true),
//                          controller: taksasiUnit.controllerAC,
//                          decoration: InputDecoration(
//                              labelText: 'Air Conditioner',
//                              labelStyle: TextStyle(color: Colors.black),
//                              border: OutlineInputBorder(
//                                  borderRadius: BorderRadius.circular(8)
//                              )
//                          ),
//                        autovalidate: taksasiUnit.autoValidateCar,
//                        validator: (e) {
//                          if (e.isEmpty) {
//                            return "Tidak boleh kosong";
//                          } else {
//                            return null;
//                          }
//                        },
//                      ),
//                      SizedBox(height: MediaQuery.of(context).size.height / 57),
//                      Container(
//                        decoration: BoxDecoration(
//                            color: myPrimaryColor,
//                            borderRadius: BorderRadius.all(Radius.circular(8))
//                        ),
//                        child: Padding(
//                          padding: const EdgeInsets.all(8.0),
//                          child:
//                          Column(
//                            children: [
//                              Row(
//                                children: [
//                                  Expanded(
//                                      child: Center(child: Text("OK")),
//                                      flex: 4
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child: Center(child: Text("CUKUP")),
//                                    flex: 4,
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child:  Center(child: Text("TIDAK")),
//                                    flex: 4,
//                                  ),
//                                ],
//                              ),
//                              Row(
//                                children: [
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueAC.isNotEmpty
//                                            ? taksasiUnit.listValueAC[0] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueAC.isNotEmpty
//                                            ? taksasiUnit.listValueAC[1] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueAC.isNotEmpty
//                                            ? taksasiUnit.listValueAC[2] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  )
//                                ],
//                              )
//                            ],
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//              ),
//              SizedBox(height: MediaQuery.of(context).size.height / 77),
//              Card(
//                child: Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: Column(
//                    children: [
//                      TextFormField(
//                          inputFormatters: [
//                            WhitelistingTextInputFormatter.digitsOnly
//                          ],
//                          // onChanged: (value){
//                          //   taksasiUnit.calculateRadioTapeCD(value);
//                          // },
//                          onFieldSubmitted: (value) {
//                            taksasiUnit.calculatedCar(context);
//                          },
//                          onTap: () {
//                            taksasiUnit.calculatedCar(context);
//                          },
//                          keyboardType: TextInputType.numberWithOptions(decimal: true),
//                          controller: taksasiUnit.controllerRadioTapeCD,
//                          decoration: InputDecoration(
//                              labelText: 'Radio/Tape/CD',
//                              labelStyle: TextStyle(color: Colors.black),
//                              border: OutlineInputBorder(
//                                  borderRadius: BorderRadius.circular(8)
//                              )
//                          ),
//                        autovalidate: taksasiUnit.autoValidateCar,
//                        validator: (e) {
//                          if (e.isEmpty) {
//                            return "Tidak boleh kosong";
//                          } else {
//                            return null;
//                          }
//                        },
//                      ),
//                      SizedBox(height: MediaQuery.of(context).size.height / 57),
//                      Container(
//                        decoration: BoxDecoration(
//                            color: myPrimaryColor,
//                            borderRadius: BorderRadius.all(Radius.circular(8))
//                        ),
//                        child: Padding(
//                          padding: const EdgeInsets.all(8.0),
//                          child:
//                          Column(
//                            children: [
//                              Row(
//                                children: [
//                                  Expanded(
//                                      child: Center(child: Text("OK")),
//                                      flex: 4
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child: Center(child: Text("CUKUP")),
//                                    flex: 4,
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child:  Center(child: Text("TIDAK")),
//                                    flex: 4,
//                                  ),
//                                ],
//                              ),
//                              Row(
//                                children: [
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueRadioTapeCD.isNotEmpty
//                                            ? taksasiUnit.listValueRadioTapeCD[0] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueRadioTapeCD.isNotEmpty
//                                            ? taksasiUnit.listValueRadioTapeCD[1] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueRadioTapeCD.isNotEmpty
//                                            ? taksasiUnit.listValueRadioTapeCD[2] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  )
//                                ],
//                              )
//                            ],
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//              ),
//              SizedBox(height: MediaQuery.of(context).size.height / 77),
//              Card(
//                child: Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: Column(
//                    children: [
//                      TextFormField(
//                          inputFormatters: [
//                            WhitelistingTextInputFormatter.digitsOnly
//                          ],
//                          // onChanged: (value){
//                          //   taksasiUnit.calculatePowerWindow(value);
//                          // },
//                          onFieldSubmitted: (value) {
//                            taksasiUnit.calculatedCar(context);
//                          },
//                          onTap: () {
//                            taksasiUnit.calculatedCar(context);
//                          },
//                          keyboardType: TextInputType.numberWithOptions(decimal: true),
//                          controller: taksasiUnit.controllerPowerWindow,
//                          decoration: InputDecoration(
//                              labelText: 'Power Window',
//                              labelStyle: TextStyle(color: Colors.black),
//                              border: OutlineInputBorder(
//                                  borderRadius: BorderRadius.circular(8)
//                              )
//                          ),
//                        autovalidate: taksasiUnit.autoValidateCar,
//                        validator: (e) {
//                          if (e.isEmpty) {
//                            return "Tidak boleh kosong";
//                          } else {
//                            return null;
//                          }
//                        },
//                      ),
//                      SizedBox(height: MediaQuery.of(context).size.height / 57),
//                      Container(
//                        decoration: BoxDecoration(
//                            color: myPrimaryColor,
//                            borderRadius: BorderRadius.all(Radius.circular(8))
//                        ),
//                        child: Padding(
//                          padding: const EdgeInsets.all(8.0),
//                          child:
//                          Column(
//                            children: [
//                              Row(
//                                children: [
//                                  Expanded(
//                                      child: Center(child: Text("OK")),
//                                      flex: 4
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child: Center(child: Text("CUKUP")),
//                                    flex: 4,
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child:  Center(child: Text("TIDAK")),
//                                    flex: 4,
//                                  ),
//                                ],
//                              ),
//                              Row(
//                                children: [
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValuePowerWindow.isNotEmpty
//                                            ? taksasiUnit.listValuePowerWindow[0] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValuePowerWindow.isNotEmpty
//                                            ? taksasiUnit.listValuePowerWindow[1] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValuePowerWindow.isNotEmpty
//                                            ? taksasiUnit.listValuePowerWindow[2] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  )
//                                ],
//                              )
//                            ],
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//              ),
//              SizedBox(height: MediaQuery.of(context).size.height / 77),
//              Card(
//                child: Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: Column(
//                    children: [
//                      TextFormField(
//                          inputFormatters: [
//                            WhitelistingTextInputFormatter.digitsOnly
//                          ],
//                          // onChanged: (value){
//                          //   taksasiUnit.calculateCentralLock(value);
//                          // },
//                          onFieldSubmitted: (value) {
//                            taksasiUnit.calculatedCar(context);
//                          },
//                          onTap: () {
//                            taksasiUnit.calculatedCar(context);
//                          },
//                          keyboardType: TextInputType.numberWithOptions(decimal: true),
//                          controller: taksasiUnit.controllerCentralLock,
//                          decoration: InputDecoration(
//                              labelText: 'Central Lock',
//                              labelStyle: TextStyle(color: Colors.black),
//                              border: OutlineInputBorder(
//                                  borderRadius: BorderRadius.circular(8)
//                              )
//                          ),
//                        autovalidate: taksasiUnit.autoValidateCar,
//                        validator: (e) {
//                          if (e.isEmpty) {
//                            return "Tidak boleh kosong";
//                          } else {
//                            return null;
//                          }
//                        },
//                      ),
//                      SizedBox(height: MediaQuery.of(context).size.height / 57),
//                      Container(
//                        decoration: BoxDecoration(
//                            color: myPrimaryColor,
//                            borderRadius: BorderRadius.all(Radius.circular(8))
//                        ),
//                        child: Padding(
//                          padding: const EdgeInsets.all(8.0),
//                          child:
//                          Column(
//                            children: [
//                              Row(
//                                children: [
//                                  Expanded(
//                                      child: Center(child: Text("OK")),
//                                      flex: 4
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child: Center(child: Text("CUKUP")),
//                                    flex: 4,
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child:  Center(child: Text("TIDAK")),
//                                    flex: 4,
//                                  ),
//                                ],
//                              ),
//                              Row(
//                                children: [
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueCentralLock.isNotEmpty
//                                            ? taksasiUnit.listValueCentralLock[0] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueCentralLock.isNotEmpty
//                                            ? taksasiUnit.listValueCentralLock[1] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueCentralLock.isNotEmpty
//                                            ? taksasiUnit.listValueCentralLock[2] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  )
//                                ],
//                              )
//                            ],
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//              ),
//              SizedBox(height: MediaQuery.of(context).size.height / 77),
//              Card(
//                child: Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: Column(
//                    children: [
//                      TextFormField(
//                        inputFormatters: [
//                          WhitelistingTextInputFormatter.digitsOnly
//                        ],
//                        // onChanged: (value){
//                        //   taksasiUnit.calculateDashboard(value);
//                        // },
//                        onFieldSubmitted: (value) {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        onTap: () {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        keyboardType: TextInputType.numberWithOptions(decimal: true),
//                        controller: taksasiUnit.controllerDashboard,
//                        decoration: InputDecoration(
//                            labelText: 'Dashboard',
//                            labelStyle: TextStyle(color: Colors.black),
//                            border: OutlineInputBorder(
//                                borderRadius: BorderRadius.circular(8)
//                            )
//                        ),
//                        autovalidate: taksasiUnit.autoValidateCar,
//                        validator: (e) {
//                          if (e.isEmpty) {
//                            return "Tidak boleh kosong";
//                          } else {
//                            return null;
//                          }
//                        },
//                      ),
//                      SizedBox(height: MediaQuery.of(context).size.height / 57),
//                      Container(
//                        decoration: BoxDecoration(
//                            color: myPrimaryColor,
//                            borderRadius: BorderRadius.all(Radius.circular(8))
//                        ),
//                        child: Padding(
//                          padding: const EdgeInsets.all(8.0),
//                          child:
//                          Column(
//                            children: [
//                              Row(
//                                children: [
//                                  Expanded(
//                                      child: Center(child: Text("OK")),
//                                      flex: 4
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child: Center(child: Text("CUKUP")),
//                                    flex: 4,
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child:  Center(child: Text("TIDAK")),
//                                    flex: 4,
//                                  ),
//                                ],
//                              ),
//                              Row(
//                                children: [
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueDashboard.isNotEmpty
//                                            ? taksasiUnit.listValueDashboard[0] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueDashboard.isNotEmpty
//                                            ? taksasiUnit.listValueDashboard[1] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueDashboard.isNotEmpty
//                                            ? taksasiUnit.listValueDashboard[2] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  )
//                                ],
//                              )
//                            ],
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//              ),
//              SizedBox(height: MediaQuery.of(context).size.height / 77),
//              Card(
//                child: Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: Column(
//                    children: [
//                      TextFormField(
//                        inputFormatters: [
//                          WhitelistingTextInputFormatter.digitsOnly
//                        ],
//                        // onChanged: (value){
//                        //   taksasiUnit.calculateJok(value);
//                        // },
//                        onFieldSubmitted: (value) {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        onTap: () {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        keyboardType: TextInputType.numberWithOptions(decimal: true),
//                        controller: taksasiUnit.controllerJok,
//                        decoration: InputDecoration(
//                            labelText: 'Jok',
//                            labelStyle: TextStyle(color: Colors.black),
//                            border: OutlineInputBorder(
//                                borderRadius: BorderRadius.circular(8)
//                            )
//                        ),
//                        autovalidate: taksasiUnit.autoValidateCar,
//                        validator: (e) {
//                          if (e.isEmpty) {
//                            return "Tidak boleh kosong";
//                          } else {
//                            return null;
//                          }
//                        },
//                      ),
//                      SizedBox(height: MediaQuery.of(context).size.height / 57),
//                      Container(
//                        decoration: BoxDecoration(
//                            color: myPrimaryColor,
//                            borderRadius: BorderRadius.all(Radius.circular(8))
//                        ),
//                        child: Padding(
//                          padding: const EdgeInsets.all(8.0),
//                          child:
//                          Column(
//                            children: [
//                              Row(
//                                children: [
//                                  Expanded(
//                                      child: Center(child: Text("OK")),
//                                      flex: 4
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child: Center(child: Text("CUKUP")),
//                                    flex: 4,
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child:  Center(child: Text("TIDAK")),
//                                    flex: 4,
//                                  ),
//                                ],
//                              ),
//                              Row(
//                                children: [
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueJok.isNotEmpty
//                                            ? taksasiUnit.listValueJok[0] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueJok.isNotEmpty
//                                            ? taksasiUnit.listValueJok[1] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueJok.isNotEmpty
//                                            ? taksasiUnit.listValueJok[2] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  )
//                                ],
//                              )
//                            ],
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//              ),
//              SizedBox(height: MediaQuery.of(context).size.height / 77),
//              Card(
//                child: Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: Column(
//                    children: [
//                      TextFormField(
//                        inputFormatters: [
//                          WhitelistingTextInputFormatter.digitsOnly
//                        ],
//                        // onChanged: (value){
//                        //   taksasiUnit.calculatePlafond(value);
//                        // },
//                        onFieldSubmitted: (value) {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        onTap: () {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        keyboardType: TextInputType.numberWithOptions(decimal: true),
//                        controller: taksasiUnit.controllerPlafond,
//                        decoration: InputDecoration(
//                            labelText: 'Plafond',
//                            labelStyle: TextStyle(color: Colors.black),
//                            border: OutlineInputBorder(
//                                borderRadius: BorderRadius.circular(8)
//                            )
//                        ),
//                        autovalidate: taksasiUnit.autoValidateCar,
//                        validator: (e) {
//                          if (e.isEmpty) {
//                            return "Tidak boleh kosong";
//                          } else {
//                            return null;
//                          }
//                        },
//                      ),
//                      SizedBox(height: MediaQuery.of(context).size.height / 57),
//                      Container(
//                        decoration: BoxDecoration(
//                            color: myPrimaryColor,
//                            borderRadius: BorderRadius.all(Radius.circular(8))
//                        ),
//                        child: Padding(
//                          padding: const EdgeInsets.all(8.0),
//                          child:
//                          Column(
//                            children: [
//                              Row(
//                                children: [
//                                  Expanded(
//                                      child: Center(child: Text("OK")),
//                                      flex: 4
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child: Center(child: Text("CUKUP")),
//                                    flex: 4,
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child:  Center(child: Text("TIDAK")),
//                                    flex: 4,
//                                  ),
//                                ],
//                              ),
//                              Row(
//                                children: [
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValuePlafond.isNotEmpty
//                                            ? taksasiUnit.listValuePlafond[0] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValuePlafond.isNotEmpty
//                                            ? taksasiUnit.listValuePlafond[1] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValuePlafond.isNotEmpty
//                                            ? taksasiUnit.listValuePlafond[2] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  )
//                                ],
//                              )
//                            ],
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//              ),
//              SizedBox(height: MediaQuery.of(context).size.height / 77),
//              Card(
//                child: Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: Column(
//                    children: [
//                      TextFormField(
//                        inputFormatters: [
//                          WhitelistingTextInputFormatter.digitsOnly
//                        ],
//                        // onChanged: (value){
//                        //   taksasiUnit.calculateAlarm(value);
//                        // },
//                        onFieldSubmitted: (value) {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        onTap: () {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        keyboardType: TextInputType.numberWithOptions(decimal: true),
//                        controller: taksasiUnit.controllerAlarm,
//                        decoration: InputDecoration(
//                            labelText: 'Alarm',
//                            labelStyle: TextStyle(color: Colors.black),
//                            border: OutlineInputBorder(
//                                borderRadius: BorderRadius.circular(8)
//                            )
//                        ),
//                        autovalidate: taksasiUnit.autoValidateCar,
//                        validator: (e) {
//                          if (e.isEmpty) {
//                            return "Tidak boleh kosong";
//                          } else {
//                            return null;
//                          }
//                        },
//                      ),
//                      SizedBox(height: MediaQuery.of(context).size.height / 57),
//                      Container(
//                        decoration: BoxDecoration(
//                            color: myPrimaryColor,
//                            borderRadius: BorderRadius.all(Radius.circular(8))
//                        ),
//                        child: Padding(
//                          padding: const EdgeInsets.all(8.0),
//                          child:
//                          Column(
//                            children: [
//                              Row(
//                                children: [
//                                  Expanded(
//                                      child: Center(child: Text("OK")),
//                                      flex: 4
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child: Center(child: Text("CUKUP")),
//                                    flex: 4,
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child:  Center(child: Text("TIDAK")),
//                                    flex: 4,
//                                  ),
//                                ],
//                              ),
//                              Row(
//                                children: [
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueAlarm.isNotEmpty
//                                            ? taksasiUnit.listValueAlarm[0] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueAlarm.isNotEmpty
//                                            ? taksasiUnit.listValueAlarm[1] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueAlarm.isNotEmpty
//                                            ? taksasiUnit.listValueAlarm[2] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  )
//                                ],
//                              )
//                            ],
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//              ),
//              SizedBox(height: MediaQuery.of(context).size.height / 77),
//              Card(
//                child: Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: Column(
//                    children: [
//                      TextFormField(
//                        inputFormatters: [
//                          WhitelistingTextInputFormatter.digitsOnly
//                        ],
//                        // onChanged: (value){
//                        //   taksasiUnit.calculateBumper(value);
//                        // },
//                        onFieldSubmitted: (value) {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        onTap: () {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        keyboardType: TextInputType.numberWithOptions(decimal: true),
//                        controller: taksasiUnit.controllerBumper,
//                        decoration: InputDecoration(
//                            labelText: 'Bumper',
//                            labelStyle: TextStyle(color: Colors.black),
//                            border: OutlineInputBorder(
//                                borderRadius: BorderRadius.circular(8)
//                            )
//                        ),
//                        autovalidate: taksasiUnit.autoValidateCar,
//                        validator: (e) {
//                          if (e.isEmpty) {
//                            return "Tidak boleh kosong";
//                          } else {
//                            return null;
//                          }
//                        },
//                      ),
//                      SizedBox(height: MediaQuery.of(context).size.height / 57),
//                      Container(
//                        decoration: BoxDecoration(
//                            color: myPrimaryColor,
//                            borderRadius: BorderRadius.all(Radius.circular(8))
//                        ),
//                        child: Padding(
//                          padding: const EdgeInsets.all(8.0),
//                          child:
//                          Column(
//                            children: [
//                              Row(
//                                children: [
//                                  Expanded(
//                                      child: Center(child: Text("OK")),
//                                      flex: 4
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child: Center(child: Text("CUKUP")),
//                                    flex: 4,
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child:  Center(child: Text("TIDAK")),
//                                    flex: 4,
//                                  ),
//                                ],
//                              ),
//                              Row(
//                                children: [
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueBumper.isNotEmpty
//                                            ? taksasiUnit.listValueBumper[0] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueBumper.isNotEmpty
//                                            ? taksasiUnit.listValueBumper[1] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueBumper.isNotEmpty
//                                            ? taksasiUnit.listValueBumper[2] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  )
//                                ],
//                              )
//                            ],
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//              ),
//              SizedBox(height: MediaQuery.of(context).size.height / 77),
//              Card(
//                child: Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: Column(
//                    children: [
//                      TextFormField(
//                        inputFormatters: [
//                          WhitelistingTextInputFormatter.digitsOnly
//                        ],
//                        // onChanged: (value){
//                        //   taksasiUnit.calculateLampu(value);
//                        // },
//                        onFieldSubmitted: (value) {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        onTap: () {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        keyboardType: TextInputType.numberWithOptions(decimal: true),
//                        controller: taksasiUnit.controllerLampu,
//                        decoration: InputDecoration(
//                            labelText: 'Lampu',
//                            labelStyle: TextStyle(color: Colors.black),
//                            border: OutlineInputBorder(
//                                borderRadius: BorderRadius.circular(8)
//                            )
//                        ),
//                        autovalidate: taksasiUnit.autoValidateCar,
//                        validator: (e) {
//                          if (e.isEmpty) {
//                            return "Tidak boleh kosong";
//                          } else {
//                            return null;
//                          }
//                        },
//                      ),
//                      SizedBox(height: MediaQuery.of(context).size.height / 57),
//                      Container(
//                        decoration: BoxDecoration(
//                            color: myPrimaryColor,
//                            borderRadius: BorderRadius.all(Radius.circular(8))
//                        ),
//                        child: Padding(
//                          padding: const EdgeInsets.all(8.0),
//                          child:
//                          Column(
//                            children: [
//                              Row(
//                                children: [
//                                  Expanded(
//                                      child: Center(child: Text("OK")),
//                                      flex: 4
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child: Center(child: Text("CUKUP")),
//                                    flex: 4,
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child:  Center(child: Text("TIDAK")),
//                                    flex: 4,
//                                  ),
//                                ],
//                              ),
//                              Row(
//                                children: [
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueLampu.isNotEmpty
//                                            ? taksasiUnit.listValueLampu[0] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueLampu.isNotEmpty
//                                            ? taksasiUnit.listValueLampu[1] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueLampu.isNotEmpty
//                                            ? taksasiUnit.listValueLampu[2] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  )
//                                ],
//                              )
//                            ],
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//              ),
//              SizedBox(height: MediaQuery.of(context).size.height / 77),
//              Card(
//                child: Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: Column(
//                    children: [
//                      TextFormField(
//                        inputFormatters: [
//                          WhitelistingTextInputFormatter.digitsOnly
//                        ],
//                        // onChanged: (value){
//                        //   taksasiUnit.calculateSpion(value);
//                        // },
//                        onFieldSubmitted: (value) {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        onTap: () {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        keyboardType: TextInputType.numberWithOptions(decimal: true),
//                        controller: taksasiUnit.controllerSpion,
//                        decoration: InputDecoration(
//                            labelText: 'Spion',
//                            labelStyle: TextStyle(color: Colors.black),
//                            border: OutlineInputBorder(
//                                borderRadius: BorderRadius.circular(8)
//                            )
//                        ),
//                        autovalidate: taksasiUnit.autoValidateCar,
//                        validator: (e) {
//                          if (e.isEmpty) {
//                            return "Tidak boleh kosong";
//                          } else {
//                            return null;
//                          }
//                        },
//                      ),
//                      SizedBox(height: MediaQuery.of(context).size.height / 57),
//                      Container(
//                        decoration: BoxDecoration(
//                            color: myPrimaryColor,
//                            borderRadius: BorderRadius.all(Radius.circular(8))
//                        ),
//                        child: Padding(
//                          padding: const EdgeInsets.all(8.0),
//                          child:
//                          Column(
//                            children: [
//                              Row(
//                                children: [
//                                  Expanded(
//                                      child: Center(child: Text("OK")),
//                                      flex: 4
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child: Center(child: Text("CUKUP")),
//                                    flex: 4,
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child:  Center(child: Text("TIDAK")),
//                                    flex: 4,
//                                  ),
//                                ],
//                              ),
//                              Row(
//                                children: [
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueSpion.isNotEmpty
//                                            ? taksasiUnit.listValueSpion[0] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueSpion.isNotEmpty
//                                            ? taksasiUnit.listValueSpion[1] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueSpion.isNotEmpty
//                                            ? taksasiUnit.listValueSpion[2] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  )
//                                ],
//                              )
//                            ],
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//              ),
//              SizedBox(height: MediaQuery.of(context).size.height / 77),
//              Card(
//                child: Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: Column(
//                    children: [
//                      TextFormField(
//                        inputFormatters: [
//                          WhitelistingTextInputFormatter.digitsOnly
//                        ],
//                        // onChanged: (value){
//                        //   taksasiUnit.calculateFender(value);
//                        // },
//                        onFieldSubmitted: (value) {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        onTap: () {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        keyboardType: TextInputType.numberWithOptions(decimal: true),
//                        controller: taksasiUnit.controllerFender,
//                        decoration: InputDecoration(
//                            labelText: 'Fender',
//                            labelStyle: TextStyle(color: Colors.black),
//                            border: OutlineInputBorder(
//                                borderRadius: BorderRadius.circular(8)
//                            )
//                        ),
//                        autovalidate: taksasiUnit.autoValidateCar,
//                        validator: (e) {
//                          if (e.isEmpty) {
//                            return "Tidak boleh kosong";
//                          } else {
//                            return null;
//                          }
//                        },
//                      ),
//                      SizedBox(height: MediaQuery.of(context).size.height / 57),
//                      Container(
//                        decoration: BoxDecoration(
//                            color: myPrimaryColor,
//                            borderRadius: BorderRadius.all(Radius.circular(8))
//                        ),
//                        child: Padding(
//                          padding: const EdgeInsets.all(8.0),
//                          child:
//                          Column(
//                            children: [
//                              Row(
//                                children: [
//                                  Expanded(
//                                      child: Center(child: Text("OK")),
//                                      flex: 4
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child: Center(child: Text("CUKUP")),
//                                    flex: 4,
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child:  Center(child: Text("TIDAK")),
//                                    flex: 4,
//                                  ),
//                                ],
//                              ),
//                              Row(
//                                children: [
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueFender.isNotEmpty
//                                            ? taksasiUnit.listValueFender[0] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueFender.isNotEmpty
//                                            ? taksasiUnit.listValueFender[1] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueFender.isNotEmpty
//                                            ? taksasiUnit.listValueFender[2] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  )
//                                ],
//                              )
//                            ],
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//              ),
//              SizedBox(height: MediaQuery.of(context).size.height / 77),
//              Card(
//                child: Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: Column(
//                    children: [
//                      TextFormField(
//                        inputFormatters: [
//                          WhitelistingTextInputFormatter.digitsOnly
//                        ],
//                        // onChanged: (value){
//                        //   taksasiUnit.calculateTireAndWheels(value);
//                        // },
//                        onFieldSubmitted: (value) {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        onTap: () {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        keyboardType: TextInputType.numberWithOptions(decimal: true),
//                        controller: taksasiUnit.controllerTiresAndWheels,
//                        decoration: InputDecoration(
//                            labelText: 'Ban dan Velg',
//                            labelStyle: TextStyle(color: Colors.black),
//                            border: OutlineInputBorder(
//                                borderRadius: BorderRadius.circular(8)
//                            )
//                        ),
//                        autovalidate: taksasiUnit.autoValidateCar,
//                        validator: (e) {
//                          if (e.isEmpty) {
//                            return "Tidak boleh kosong";
//                          } else {
//                            return null;
//                          }
//                        },
//                      ),
//                      SizedBox(height: MediaQuery.of(context).size.height / 57),
//                      Container(
//                        decoration: BoxDecoration(
//                            color: myPrimaryColor,
//                            borderRadius: BorderRadius.all(Radius.circular(8))
//                        ),
//                        child: Padding(
//                          padding: const EdgeInsets.all(8.0),
//                          child:
//                          Column(
//                            children: [
//                              Row(
//                                children: [
//                                  Expanded(
//                                      child: Center(child: Text("OK")),
//                                      flex: 4
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child: Center(child: Text("CUKUP")),
//                                    flex: 4,
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child:  Center(child: Text("TIDAK")),
//                                    flex: 4,
//                                  ),
//                                ],
//                              ),
//                              Row(
//                                children: [
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueTireAndWheels.isNotEmpty
//                                            ? taksasiUnit.listValueTireAndWheels[0] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueTireAndWheels.isNotEmpty
//                                            ? taksasiUnit.listValueTireAndWheels[1] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueTireAndWheels.isNotEmpty
//                                            ? taksasiUnit.listValueTireAndWheels[2] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  )
//                                ],
//                              )
//                            ],
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//              ),
//              SizedBox(height: MediaQuery.of(context).size.height / 77),
//              Card(
//                child: Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: Column(
//                    children: [
//                      TextFormField(
//                        inputFormatters: [
//                          WhitelistingTextInputFormatter.digitsOnly
//                        ],
//                        // onChanged: (value){
//                        //   taksasiUnit.calculateWindShield(value);
//                        // },
//                        onFieldSubmitted: (value) {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        onTap: () {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        keyboardType: TextInputType.numberWithOptions(decimal: true),
//                        controller: taksasiUnit.controllerWindshield,
//                        decoration: InputDecoration(
//                            labelText: 'Kaca',
//                            labelStyle: TextStyle(color: Colors.black),
//                            border: OutlineInputBorder(
//                                borderRadius: BorderRadius.circular(8)
//                            )
//                        ),
//                        autovalidate: taksasiUnit.autoValidateCar,
//                        validator: (e) {
//                          if (e.isEmpty) {
//                            return "Tidak boleh kosong";
//                          } else {
//                            return null;
//                          }
//                        },
//                      ),
//                      SizedBox(height: MediaQuery.of(context).size.height / 57),
//                      Container(
//                        decoration: BoxDecoration(
//                            color: myPrimaryColor,
//                            borderRadius: BorderRadius.all(Radius.circular(8))
//                        ),
//                        child: Padding(
//                          padding: const EdgeInsets.all(8.0),
//                          child:
//                          Column(
//                            children: [
//                              Row(
//                                children: [
//                                  Expanded(
//                                      child: Center(child: Text("OK")),
//                                      flex: 4
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child: Center(child: Text("CUKUP")),
//                                    flex: 4,
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child:  Center(child: Text("TIDAK")),
//                                    flex: 4,
//                                  ),
//                                ],
//                              ),
//                              Row(
//                                children: [
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueWindShield.isNotEmpty
//                                            ? taksasiUnit.listValueWindShield[0] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueWindShield.isNotEmpty
//                                            ? taksasiUnit.listValueWindShield[1] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueWindShield.isNotEmpty
//                                            ? taksasiUnit.listValueWindShield[2] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  )
//                                ],
//                              )
//                            ],
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//              ),
//              SizedBox(height: MediaQuery.of(context).size.height / 77),
//              Card(
//                child: Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: Column(
//                    children: [
//                      TextFormField(
//                        inputFormatters: [
//                          WhitelistingTextInputFormatter.digitsOnly
//                        ],
//                        // onChanged: (value){
//                        //   taksasiUnit.calculateFrameAndPole(value);
//                        // },
//                        onFieldSubmitted: (value) {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        onTap: () {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        keyboardType: TextInputType.numberWithOptions(decimal: true),
//                        controller: taksasiUnit.controllerFrameAndPole,
//                        decoration: InputDecoration(
//                            labelText: 'Rangka dan Tiang',
//                            labelStyle: TextStyle(color: Colors.black),
//                            border: OutlineInputBorder(
//                                borderRadius: BorderRadius.circular(8)
//                            )
//                        ),
//                        autovalidate: taksasiUnit.autoValidateCar,
//                        validator: (e) {
//                          if (e.isEmpty) {
//                            return "Tidak boleh kosong";
//                          } else {
//                            return null;
//                          }
//                        },
//                      ),
//                      SizedBox(height: MediaQuery.of(context).size.height / 57),
//                      Container(
//                        decoration: BoxDecoration(
//                            color: myPrimaryColor,
//                            borderRadius: BorderRadius.all(Radius.circular(8))
//                        ),
//                        child: Padding(
//                          padding: const EdgeInsets.all(8.0),
//                          child:
//                          Column(
//                            children: [
//                              Row(
//                                children: [
//                                  Expanded(
//                                      child: Center(child: Text("OK")),
//                                      flex: 4
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child: Center(child: Text("CUKUP")),
//                                    flex: 4,
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child:  Center(child: Text("TIDAK")),
//                                    flex: 4,
//                                  ),
//                                ],
//                              ),
//                              Row(
//                                children: [
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueFrameAndPole.isNotEmpty
//                                            ? taksasiUnit.listValueFrameAndPole[0] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueFrameAndPole.isNotEmpty
//                                            ? taksasiUnit.listValueFrameAndPole[1] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueFrameAndPole.isNotEmpty
//                                            ? taksasiUnit.listValueFrameAndPole[2] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  )
//                                ],
//                              )
//                            ],
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//              ),
//              SizedBox(height: MediaQuery.of(context).size.height / 77),
//              Card(
//                child: Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: Column(
//                    children: [
//                      TextFormField(
//                        inputFormatters: [
//                          WhitelistingTextInputFormatter.digitsOnly
//                        ],
//                        // onChanged: (value){
//                        //   taksasiUnit.calculateHood(value);
//                        // },
//                        onFieldSubmitted: (value) {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        onTap: () {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        keyboardType: TextInputType.numberWithOptions(decimal: true),
//                        controller: taksasiUnit.controllerHood,
//                        decoration: InputDecoration(
//                            labelText: 'Kap Mesin',
//                            labelStyle: TextStyle(color: Colors.black),
//                            border: OutlineInputBorder(
//                                borderRadius: BorderRadius.circular(8)
//                            )
//                        ),
//                        autovalidate: taksasiUnit.autoValidateCar,
//                        validator: (e) {
//                          if (e.isEmpty) {
//                            return "Tidak boleh kosong";
//                          } else {
//                            return null;
//                          }
//                        },
//                      ),
//                      SizedBox(height: MediaQuery.of(context).size.height / 57),
//                      Container(
//                        decoration: BoxDecoration(
//                            color: myPrimaryColor,
//                            borderRadius: BorderRadius.all(Radius.circular(8))
//                        ),
//                        child: Padding(
//                          padding: const EdgeInsets.all(8.0),
//                          child:
//                          Column(
//                            children: [
//                              Row(
//                                children: [
//                                  Expanded(
//                                      child: Center(child: Text("OK")),
//                                      flex: 4
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child: Center(child: Text("CUKUP")),
//                                    flex: 4,
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child:  Center(child: Text("TIDAK")),
//                                    flex: 4,
//                                  ),
//                                ],
//                              ),
//                              Row(
//                                children: [
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueHood.isNotEmpty
//                                            ? taksasiUnit.listValueHood[0] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueHood.isNotEmpty
//                                            ? taksasiUnit.listValueHood[1] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueHood.isNotEmpty
//                                            ? taksasiUnit.listValueHood[2] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  )
//                                ],
//                              )
//                            ],
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//              ),
//              SizedBox(height: MediaQuery.of(context).size.height / 77),
//              Card(
//                child: Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: Column(
//                    children: [
//                      TextFormField(
//                        inputFormatters: [
//                          WhitelistingTextInputFormatter.digitsOnly
//                        ],
//                        // onChanged: (value){
//                        //   taksasiUnit.calculateDoor(value);
//                        // },
//                        onFieldSubmitted: (value) {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        onTap: () {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        keyboardType: TextInputType.numberWithOptions(decimal: true),
//                        controller: taksasiUnit.controllerDoor,
//                        decoration: InputDecoration(
//                            labelText: 'Pintu',
//                            labelStyle: TextStyle(color: Colors.black),
//                            border: OutlineInputBorder(
//                                borderRadius: BorderRadius.circular(8)
//                            )
//                        ),
//                        autovalidate: taksasiUnit.autoValidateCar,
//                        validator: (e) {
//                          if (e.isEmpty) {
//                            return "Tidak boleh kosong";
//                          } else {
//                            return null;
//                          }
//                        },
//                      ),
//                      SizedBox(height: MediaQuery.of(context).size.height / 57),
//                      Container(
//                        decoration: BoxDecoration(
//                            color: myPrimaryColor,
//                            borderRadius: BorderRadius.all(Radius.circular(8))
//                        ),
//                        child: Padding(
//                          padding: const EdgeInsets.all(8.0),
//                          child:
//                          Column(
//                            children: [
//                              Row(
//                                children: [
//                                  Expanded(
//                                      child: Center(child: Text("OK")),
//                                      flex: 4
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child: Center(child: Text("CUKUP")),
//                                    flex: 4,
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child:  Center(child: Text("TIDAK")),
//                                    flex: 4,
//                                  ),
//                                ],
//                              ),
//                              Row(
//                                children: [
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueDoor.isNotEmpty
//                                            ? taksasiUnit.listValueDoor[0] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueDoor.isNotEmpty
//                                            ? taksasiUnit.listValueDoor[1] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueDoor.isNotEmpty
//                                            ? taksasiUnit.listValueDoor[2] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  )
//                                ],
//                              )
//                            ],
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//              ),
//              SizedBox(height: MediaQuery.of(context).size.height / 77),
//              Card(
//                child: Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: Column(
//                    children: [
//                      TextFormField(
//                        inputFormatters: [
//                          WhitelistingTextInputFormatter.digitsOnly
//                        ],
//                        // onChanged: (value){
//                        //   taksasiUnit.calculateCarTrunk(value);
//                        // },
//                        onFieldSubmitted: (value) {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        onTap: () {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        keyboardType: TextInputType.numberWithOptions(decimal: true),
//                        controller: taksasiUnit.controllerCarTrunk,
//                        decoration: InputDecoration(
//                            labelText: 'Bagasi',
//                            labelStyle: TextStyle(color: Colors.black),
//                            border: OutlineInputBorder(
//                                borderRadius: BorderRadius.circular(8)
//                            )
//                        ),
//                        autovalidate: taksasiUnit.autoValidateCar,
//                        validator: (e) {
//                          if (e.isEmpty) {
//                            return "Tidak boleh kosong";
//                          } else {
//                            return null;
//                          }
//                        },
//                      ),
//                      SizedBox(height: MediaQuery.of(context).size.height / 57),
//                      Container(
//                        decoration: BoxDecoration(
//                            color: myPrimaryColor,
//                            borderRadius: BorderRadius.all(Radius.circular(8))
//                        ),
//                        child: Padding(
//                          padding: const EdgeInsets.all(8.0),
//                          child:
//                          Column(
//                            children: [
//                              Row(
//                                children: [
//                                  Expanded(
//                                      child: Center(child: Text("OK")),
//                                      flex: 4
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child: Center(child: Text("CUKUP")),
//                                    flex: 4,
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child:  Center(child: Text("TIDAK")),
//                                    flex: 4,
//                                  ),
//                                ],
//                              ),
//                              Row(
//                                children: [
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueCarTrunk.isNotEmpty
//                                            ? taksasiUnit.listValueCarTrunk[0] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueCarTrunk.isNotEmpty
//                                            ? taksasiUnit.listValueCarTrunk[1] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueCarTrunk.isNotEmpty
//                                            ? taksasiUnit.listValueCarTrunk[2] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  )
//                                ],
//                              )
//                            ],
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//              ),
//              SizedBox(height: MediaQuery.of(context).size.height / 77),
//              Card(
//                child: Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: Column(
//                    children: [
//                      TextFormField(
//                        inputFormatters: [
//                          WhitelistingTextInputFormatter.digitsOnly
//                        ],
//                        // onChanged: (value){
//                        //   taksasiUnit.calculateBoxIronTubTank(value);
//                        // },
//                        onFieldSubmitted: (value) {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        onTap: () {
//                          taksasiUnit.calculatedCar(context);
//                        },
//                        keyboardType: TextInputType.numberWithOptions(decimal: true),
//                        controller: taksasiUnit.controllerBoxIronTubTank,
//                        decoration: InputDecoration(
//                            labelText: 'Box/Bak Kayu/Bak Besi/Tangki',
//                            labelStyle: TextStyle(color: Colors.black),
//                            border: OutlineInputBorder(
//                                borderRadius: BorderRadius.circular(8)
//                            )
//                        ),
//                        autovalidate: taksasiUnit.autoValidateCar,
//                        validator: (e) {
//                          if (e.isEmpty) {
//                            return "Tidak boleh kosong";
//                          } else {
//                            return null;
//                          }
//                        },
//                      ),
//                      SizedBox(height: MediaQuery.of(context).size.height / 57),
//                      Container(
//                        decoration: BoxDecoration(
//                            color: myPrimaryColor,
//                            borderRadius: BorderRadius.all(Radius.circular(8))
//                        ),
//                        child: Padding(
//                          padding: const EdgeInsets.all(8.0),
//                          child:
//                          Column(
//                            children: [
//                              Row(
//                                children: [
//                                  Expanded(
//                                      child: Center(child: Text("OK")),
//                                      flex: 4
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child: Center(child: Text("CUKUP")),
//                                    flex: 4,
//                                  ),
//                                  SizedBox(
//                                    height: MediaQuery.of(context).size.height/37,
//                                    child: VerticalDivider(
//                                      width: 2,
//                                      color: Colors.black,
//                                    ),
//                                  ),
//                                  Expanded(
//                                    child:  Center(child: Text("TIDAK")),
//                                    flex: 4,
//                                  ),
//                                ],
//                              ),
//                              Row(
//                                children: [
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueBoxIronTubTank.isNotEmpty
//                                            ? taksasiUnit.listValueBoxIronTubTank[0] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueBoxIronTubTank.isNotEmpty
//                                            ? taksasiUnit.listValueBoxIronTubTank[1] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  ),
//                                  Expanded(
//                                    flex: 4,
//                                    child: Visibility(
//                                        visible: taksasiUnit.listValueBoxIronTubTank.isNotEmpty
//                                            ? taksasiUnit.listValueBoxIronTubTank[2] : false,
//                                        child: Icon(Icons.check,color: Colors.green)
//                                    ),
//                                  )
//                                ],
//                              )
//                            ],
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//              ),
//            ],
//          ),
//        );
            } ,
          );
        }
    );
  }
}
