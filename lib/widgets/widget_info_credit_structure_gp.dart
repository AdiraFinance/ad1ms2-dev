import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/gp_type_model.dart';
import 'package:ad1ms2_dev/models/info_credit_structure_gp_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_type_installment_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class WidgetInfoCreditStructureGP extends StatefulWidget {
  @override
  _WidgetInfoCreditStructureGPState createState() => _WidgetInfoCreditStructureGPState();
}

class _WidgetInfoCreditStructureGPState extends State<WidgetInfoCreditStructureGP> {

  Future<void> _setPreference;

  @override
  void initState() {
    super.initState();
    // Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false).setListInstallment(context);
    _setPreference = Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context, listen: false).setPreference(context);
    // Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false).getDataFromDashboard(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:
        Text("Struktur Kredit GP", style: TextStyle(color: Colors.black)),
        centerTitle: true,
        backgroundColor: myPrimaryColor,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: FutureBuilder(
        future: _setPreference,
          builder: (context,snapshot){
            if(snapshot.connectionState == ConnectionState.waiting){
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            return Consumer<InfoCreditStructureTypeInstallmentChangeNotifier>(
              builder: (context,infoCreditStructureTypeInstallment,_){
                return ListView.builder(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height / 47,
                      horizontal: MediaQuery.of(context).size.width / 27),
                  itemCount: infoCreditStructureTypeInstallment.listInfoCreditStructureGPModel.length,
                  itemBuilder: (context, index) {
                    return Container(
                      child: InkWell(
                        onTap: (){
                          Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false).clearDialogGP();
                          Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false)
                              .setValueForEditGP(infoCreditStructureTypeInstallment.listInfoCreditStructureGPModel[index]);
                          _showDialogCreateAndUpdate(1,index);
                        },
                        child: Card(
                          elevation: 3.3,
                          child: Padding(
                            padding: EdgeInsets.all(13.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text("Angsuran Ke ${infoCreditStructureTypeInstallment.listInfoCreditStructureGPModel[index].installment}"),
                                IconButton(
                                    icon: Icon(Icons.delete,color: Colors.red),
                                    onPressed: (){
                                      Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false)
                                          .deleteInfoCreditStructureGPModel(context, index);
                                    }
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                );
              },
            );
          }
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: (){
            Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false).clearDialogGP();
            _showDialogCreateAndUpdate(0,null);
          },
        child: Icon(Icons.add, color: Colors.black),
        backgroundColor: myPrimaryColor,
      ),
    );
  }

  _showDialogCreateAndUpdate(int flag,int index) async{
    var _provider = Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false);
    showDialog(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return Theme(
          data: ThemeData(
              fontFamily: "NunitoSans",
              primaryColor: Colors.black,
              accentColor: myPrimaryColor
          ),
          child: AlertDialog(
            title: Text('Tambah Kredit GP'),
            content: SingleChildScrollView(
              child: Form(
                key: _provider.keyGP,
                child: ListBody(
                  children: <Widget>[
                    Visibility(
                      visible: _provider.isGpTypeSelectedVisible(),
                      child: IgnorePointer(
                        ignoring: _provider.isDisablePACIAAOSCONA,
                        child: DropdownButtonFormField<GPTypeModel>(
                            autovalidate:
                            _provider.autoValidateDialogFloating,
                            validator: (e) {
                              if (e == null && _provider.isGpTypeSelectedMandatory()) {
                                return "Silahkan pilih jenis GP";
                              } else {
                                return null;
                              }
                            },
                            value: _provider
                                .gpTypeSelected,
                            onChanged: (value) {
                              _provider
                                  .gpTypeSelected = value;
                              _provider.setValueNewTenor();
                            },
                            decoration: InputDecoration(
                              labelText: "Jenis GP",
                              border: OutlineInputBorder(),
                              contentPadding:
                              EdgeInsets.symmetric(horizontal: 10),
                            ),
                            items: _provider
                                .listGPType
                                .map((value) {
                              return DropdownMenuItem<GPTypeModel>(
                                value: value,
                                child: Text(
                                  value.name,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              );
                            }).toList()
                        ),
                      ),
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    Visibility(
                      visible: _provider.isInstallmentSelectedGPVisible(),
                      child: IgnorePointer(
                        ignoring: _provider.isDisablePACIAAOSCONA,
                        child: DropdownButtonFormField<String>(
                            autovalidate:
                            _provider.autoValidateDialogFloating,
                            validator: (e) {
                              if (e == null && _provider.isInstallmentSelectedGPMandatory()) {
                                return "Silahkan pilih angsuran ke";
                              } else {
                                return null;
                              }
                            },
                            value: _provider
                                .installmentSelected,
                            onChanged: (value) {
                              _provider
                                  .installmentSelected = value;
                            },
                            decoration: InputDecoration(
                              labelText: "Angsuran Ke",
                              border: OutlineInputBorder(),
                              contentPadding:
                              EdgeInsets.symmetric(horizontal: 10),
                            ),
                            items: _provider
                                .listInstallment
                                .map((value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(
                                  value,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              );
                            }).toList()
                        ),
                      ),
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    Visibility(
                      visible: _provider.isNewTenorVisible(),
                      child: IgnorePointer(
                        ignoring: _provider.isDisablePACIAAOSCONA,
                        child: TextFormField(
                            validator: (e) {
                              if (e.isEmpty && _provider.isNewTenorMandatory()) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            inputFormatters: [
                              WhitelistingTextInputFormatter.digitsOnly
                            ],
                            keyboardType: TextInputType.number,
                            controller: _provider.controllerNewTenor,
                            autovalidate: _provider.autoValidateDialogGP,
                            decoration: new InputDecoration(
                                labelText: 'Tenor Baru',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8)
                                )
                            )
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            actions: [
              FlatButton(
                child: Text(
                    flag == 0 ? "ADD" : "UPDATE",
                    style: TextStyle(
                        color: primaryOrange
                    )
                ),
                onPressed: (){
                  if(flag==0){
                    _provider.checkGP(
                        flag,
                        InfoCreditStructureGPModel(
                            _provider.gpTypeSelected,
                            _provider.installmentSelected,
                            _provider.controllerNewTenor.text
                        ),
                        null,context
                    );
                  }
                  else{
                    _provider.checkGP(
                        flag,
                        InfoCreditStructureGPModel(
                            _provider.gpTypeSelected,
                            _provider.installmentSelected,
                            _provider.controllerInterestRate.text
                        ),
                        index,context
                    );
                  }
                },
              ),
              FlatButton(
                child: Text(
                    "CANCEL",
                    style: TextStyle(
                        color: Colors.grey
                    )
                ),
                onPressed: (){
                  Navigator.pop(context);
                },
              ),
            ],
          ),
        );
      },
    );
  }
}
