import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_type_installment_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class WidgetInfoCreditStructureIrregular extends StatefulWidget {
  @override
  _WidgetInfoCreditStructureIrregularState createState() => _WidgetInfoCreditStructureIrregularState();
}

class _WidgetInfoCreditStructureIrregularState extends State<WidgetInfoCreditStructureIrregular> {

  Future<void> _setPreference;

  @override
  void initState() {
    super.initState();
    // Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false).setValue(context);
    _setPreference = Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context, listen: false).setPreference(context);
    // Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false).getDataFromDashboard(context);
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        primaryColor: Colors.black,
        accentColor: myPrimaryColor
      ),
      child: Scaffold(
        appBar: AppBar(
          title:
          Text("Struktur Kredit Irregular", style: TextStyle(color: Colors.black)),
          centerTitle: true,
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: FutureBuilder(
          future: _setPreference,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            return Consumer<InfoCreditStructureTypeInstallmentChangeNotifier>(
              builder: (context,infoCreditStructureInstallment,_){
                return ListView.builder(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height / 47,
                      horizontal: MediaQuery.of(context).size.width / 27),
                  itemBuilder: (context, index) {
                    return Card(
                      elevation: 3.3,
                      child: Padding(
                        padding: const EdgeInsets.all(13.0),
                        child: Column(
                          children: [
                            IgnorePointer(
                              ignoring: infoCreditStructureInstallment.isDisablePACIAAOSCONA,
                              child: Row(
                                children: [
                                  Expanded(
                                      flex: 4,
                                      child: Text("Angsuran Ke")
                                  ),
                                  Text(" : "),
                                  Expanded(
                                      flex: 7,
                                      child: Text("${index+1}")
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(height: MediaQuery.of(context).size.height / 47),
                            Form(
                              key: infoCreditStructureInstallment.listInfoCreditStructureIrregularModel[index].keyIrregular,
                              child: Visibility(
                                visible: infoCreditStructureInstallment.isControllerVisible(),
                                child: IgnorePointer(
                                  ignoring: infoCreditStructureInstallment.isDisablePACIAAOSCONA,
                                  child: TextFormField(
                                      validator: (e) {
                                        if (e.isEmpty && infoCreditStructureInstallment.isControllerMandatory()) {
                                          return "Tidak boleh kosong";
                                        } else {
                                          return null;
                                        }
                                      },
                                      inputFormatters: [
                                        WhitelistingTextInputFormatter.digitsOnly
                                      ],
                                      readOnly: infoCreditStructureInstallment.listInfoCreditStructureIrregularModel[index].isEnable,
                                      keyboardType: TextInputType.number,
                                      controller: infoCreditStructureInstallment.listInfoCreditStructureIrregularModel[index].controller,
                                      autovalidate: infoCreditStructureInstallment.listInfoCreditStructureIrregularModel[index].autoValidate,
                                      decoration: InputDecoration(
                                          labelText: 'Nilai Angsuran',
                                          labelStyle: TextStyle(color: Colors.black),
                                          border: OutlineInputBorder(
                                              borderRadius: BorderRadius.circular(8)
                                          )
                                      )
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: MediaQuery.of(context).size.height / 47),
                            Row(
                              children: [
                                Expanded(
                                  flex: 5,
                                  child: RaisedButton(
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      onPressed: infoCreditStructureInstallment.listInfoCreditStructureIrregularModel[index].isEnable ? (){
                                        infoCreditStructureInstallment.setEnableTf(index);
                                      } : null,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          Text("UPDATE")
                                        ],
                                      )
                                  ),
                                ),
                                SizedBox(width: MediaQuery.of(context).size.width / 37,),
                                Expanded(
                                  flex: 5,
                                  child: RaisedButton(
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      onPressed: !infoCreditStructureInstallment.listInfoCreditStructureIrregularModel[index].isEnable ? (){
                                        infoCreditStructureInstallment.checkIrregular(index);
                                      } : null,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          Text("SAVE")
                                        ],
                                      )
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    );
                  },
                  itemCount: infoCreditStructureInstallment.listInfoCreditStructureIrregularModel.length,
                );
              },
            );
          },
        ),
        bottomNavigationBar: BottomAppBar(
          elevation: 0.0,
          child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Consumer<InfoCreditStructureTypeInstallmentChangeNotifier>(
                builder: (context, infoCreditStructureTypeInstallmentChangeNotifier, _) {
                  return RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(8.0)),
                      color: myPrimaryColor,
                      onPressed: () {
                        Navigator.pop(context);
//                        infoCreditStructureTypeInstallmentChangeNotifier.check(context);
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text("DONE",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: 1.25))
                        ],
                      ));
                },
              )),
        ),
      ),
    );
  }
}
