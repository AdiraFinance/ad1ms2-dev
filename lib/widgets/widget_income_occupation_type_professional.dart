import 'package:ad1ms2_dev/shared/decimal_text_input_formatter.dart';
import 'package:ad1ms2_dev/shared/form_m_income_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class WidgetIncomeOccupationTypeProfessional extends StatefulWidget {
  final String maritalStatusKode;

  const WidgetIncomeOccupationTypeProfessional({Key key, this.maritalStatusKode}) : super(key: key);
  @override
  _WidgetIncomeOccupationTypeProfessionalState createState() =>
      _WidgetIncomeOccupationTypeProfessionalState();
}

class _WidgetIncomeOccupationTypeProfessionalState extends State<WidgetIncomeOccupationTypeProfessional> {
  @override
  void initState() {
    super.initState();
    Provider.of<FormMIncomeChangeNotifier>(context, listen: false).setIsMarried(context);
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<FormMIncomeChangeNotifier>(
      builder: (context, formMIncomeChangeNotif, _) {
        return SingleChildScrollView(
          padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width/37,
            vertical: MediaQuery.of(context).size.height/57,
          ),
          child: Column(
            children: [
              Visibility(
                visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isIncomeProfesionalShow(),
                child: TextFormField(
                  autovalidate: formMIncomeChangeNotif.autoValidate,
                  validator: (e) {
                    if (e.isEmpty && Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isIncomeProfesionalMandatory()) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  controller: formMIncomeChangeNotif.controllerMonthlyIncome,
                  style: new TextStyle(color: Colors.black),
                  decoration: new InputDecoration(
                      labelText: 'Pendapatan',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editIncomeProfesional ? Colors.purple : Colors.grey)),
                      disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editIncomeProfesional ? Colors.purple : Colors.grey)),
                      contentPadding: EdgeInsets.symmetric(horizontal: 10),),
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.end,
                  onFieldSubmitted: (value) {
                    formMIncomeChangeNotif.controllerMonthlyIncome.text = formMIncomeChangeNotif.formatCurrency.formatCurrency(value);
                    formMIncomeChangeNotif.calculateTotalIncomeNonEntrepreneur();
                    formMIncomeChangeNotif.calculateRestIncomeNonEntrepreneur();
                  },
                  onTap: () {
                    formMIncomeChangeNotif.formattingProfessionalOther();
                    formMIncomeChangeNotif.calculateTotalIncomeNonEntrepreneur();
                    formMIncomeChangeNotif.calculateRestIncomeNonEntrepreneur();
                  },
                  textInputAction: TextInputAction.done,
                  inputFormatters: [
                    DecimalTextInputFormatter(decimalRange: 2),
                    formMIncomeChangeNotif.amountValidator
                  ],
                ),
              ),
              formMIncomeChangeNotif.isMarried == true
              ? SizedBox(height: MediaQuery.of(context).size.height / 47)
              : SizedBox(),
              Visibility(
                visible: formMIncomeChangeNotif.isMarried,
                child: Visibility(
                  visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isIncomePasanganProfesionalShow(),
                  child: TextFormField(
                    autovalidate: formMIncomeChangeNotif.autoValidate,
                    validator: (e) {
                      if (e.isEmpty && Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isIncomePasanganProfesionalMandatory()) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    controller: formMIncomeChangeNotif.controllerSpouseIncome,
                    style: new TextStyle(color: Colors.black),
                    decoration: new InputDecoration(
                        labelText: 'Pendapatan Pasangan',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: formMIncomeChangeNotif.editIncomePasanganProfesional ? Colors.purple : Colors.grey)),
                        disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: formMIncomeChangeNotif.editIncomePasanganProfesional ? Colors.purple : Colors.grey)),
                        contentPadding: EdgeInsets.symmetric(horizontal: 10),),
                    keyboardType: TextInputType.number,
                    textAlign: TextAlign.end,
                    onFieldSubmitted: (value) {
                      formMIncomeChangeNotif.controllerSpouseIncome.text = formMIncomeChangeNotif.formatCurrency.formatCurrency(value);
                      formMIncomeChangeNotif.calculateTotalIncomeNonEntrepreneur();
                      formMIncomeChangeNotif.calculateRestIncomeNonEntrepreneur();
                    },
                    onTap: () {
                      formMIncomeChangeNotif.formattingProfessionalOther();
                      formMIncomeChangeNotif.calculateTotalIncomeNonEntrepreneur();
                      formMIncomeChangeNotif.calculateRestIncomeNonEntrepreneur();
                    },
                    textInputAction: TextInputAction.done,
                    inputFormatters: [
                      DecimalTextInputFormatter(decimalRange: 2),
                      formMIncomeChangeNotif.amountValidator
                    ],
                  ),
                ),
              ),
              Visibility(
                  visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isIncomePasanganProfesionalShow(),
                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
              Visibility(
                visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isIncomeLainnyaProfesionalShow(),
                child: TextFormField(
                  autovalidate: formMIncomeChangeNotif.autoValidate,
                  validator: (e) {
                    if (e.isEmpty && Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isIncomeLainnyaProfesionalMandatory()) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  controller: formMIncomeChangeNotif.controllerOtherIncome,
                  style: new TextStyle(color: Colors.black),
                  decoration: new InputDecoration(
                      labelText: 'Pendapatan Lainnya',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editIncomeLainnyaProfesional ? Colors.purple : Colors.grey)),
                      disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editIncomeLainnyaProfesional ? Colors.purple : Colors.grey)),
                      contentPadding: EdgeInsets.symmetric(horizontal: 10),),
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.end,
                  onFieldSubmitted: (value) {
                    formMIncomeChangeNotif.controllerOtherIncome.text = formMIncomeChangeNotif.formatCurrency.formatCurrency(value);
                    formMIncomeChangeNotif.calculateTotalIncomeNonEntrepreneur();
                    formMIncomeChangeNotif.calculateRestIncomeNonEntrepreneur();
                  },
                  onTap: () {
                    formMIncomeChangeNotif.formattingProfessionalOther();
                    formMIncomeChangeNotif.calculateTotalIncomeNonEntrepreneur();
                    formMIncomeChangeNotif.calculateRestIncomeNonEntrepreneur();
                  },
                  textInputAction: TextInputAction.done,
                  inputFormatters: [
                    DecimalTextInputFormatter(decimalRange: 2),
                    formMIncomeChangeNotif.amountValidator
                  ],
                ),
              ),
              Visibility(
                  visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isIncomeLainnyaProfesionalShow(),
                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
              Visibility(
                visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isTotalIncomeProfesionalShow(),
                child: TextFormField(
                  autovalidate: formMIncomeChangeNotif.autoValidate,
                  validator: (e) {
                    if (e.isEmpty && Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isTotalIncomeProfesionalMandatory()) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  controller: formMIncomeChangeNotif.controllerTotalIncome,
                  style: new TextStyle(color: Colors.black),
                  enabled: false,
                  decoration: new InputDecoration(
                      labelText: 'Total Pendapatan',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editTotalIncomeProfesional ? Colors.purple : Colors.grey)),
                      disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editTotalIncomeProfesional ? Colors.purple : Colors.grey)),
                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                      filled: true,
                      fillColor: Colors.black12,),
                  textAlign: TextAlign.end,
                  keyboardType: TextInputType.number,
                  textCapitalization: TextCapitalization.characters,
                ),
              ),
              Visibility(
                  visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isTotalIncomeProfesionalShow(),
                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
              Visibility(
                visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isBiayaHidupProfesionalShow(),
                child: TextFormField(
                  autovalidate: formMIncomeChangeNotif.autoValidate,
                  validator: (e) {
                    if (e.isEmpty && Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isBiayaHidupProfesionalMandatory()) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  controller: formMIncomeChangeNotif.controllerCostOfLiving,
                  style: new TextStyle(color: Colors.black),
                  decoration: new InputDecoration(
                      labelText: 'Biaya Hidup',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editBiayaHidupProfesional ? Colors.purple : Colors.grey)),
                      disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editBiayaHidupProfesional ? Colors.purple : Colors.grey)),
                      contentPadding: EdgeInsets.symmetric(horizontal: 10),),
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.end,
                  onFieldSubmitted: (value) {
                    formMIncomeChangeNotif.controllerCostOfLiving.text = formMIncomeChangeNotif.formatCurrency.formatCurrency(value);
                    formMIncomeChangeNotif.calculateTotalIncomeNonEntrepreneur();
                    formMIncomeChangeNotif.calculateRestIncomeNonEntrepreneur();
                  },
                  onTap: () {
                    formMIncomeChangeNotif.formattingProfessionalOther();
                    formMIncomeChangeNotif.calculateTotalIncomeNonEntrepreneur();
                    formMIncomeChangeNotif.calculateRestIncomeNonEntrepreneur();
                  },
                  textInputAction: TextInputAction.done,
                  inputFormatters: [
                    DecimalTextInputFormatter(decimalRange: 2),
                    formMIncomeChangeNotif.amountValidator
                  ],
                ),
              ),
              Visibility(
                  visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isBiayaHidupProfesionalShow(),
                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
              Visibility(
                visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isSisaIncomeProfesionalShow(),
                child: TextFormField(
                  enabled: false,
                  autovalidate: formMIncomeChangeNotif.autoValidate,
                  validator: (e) {
                    if (e.isEmpty  && Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isSisaIncomeProfesionalMandatory()) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  controller: formMIncomeChangeNotif.controllerRestIncome,
                  style: new TextStyle(color: Colors.black),
                  decoration: new InputDecoration(
                      labelText: 'Sisa Pendapatan',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editSisaIncomeProfesional ? Colors.purple : Colors.grey)),
                      disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editSisaIncomeProfesional ? Colors.purple : Colors.grey)),
                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                      filled: true,
                      fillColor: Colors.black12,),
                  textAlign: TextAlign.end,
                  keyboardType: TextInputType.number,
                  textCapitalization: TextCapitalization.characters,
                ),
              ),
              Visibility(
                  visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isSisaIncomeProfesionalShow(),
                  child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
              Visibility(
                visible: Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isAngsuranLainnyaProfesionalShow(),
                child: TextFormField(
                  autovalidate: formMIncomeChangeNotif.autoValidate,
                  validator: (e) {
                    if (e.isEmpty && Provider.of<FormMIncomeChangeNotifier>(context, listen: false).isAngsuranLainnyaProfesionalMandatory()) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  controller: formMIncomeChangeNotif.controllerOtherInstallments,
                  style: new TextStyle(color: Colors.black),
                  decoration: new InputDecoration(
                      labelText: 'Angsuran Lainnya',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editAngsuranLainnyaProfesional ? Colors.purple : Colors.grey)),
                      disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMIncomeChangeNotif.editAngsuranLainnyaProfesional ? Colors.purple : Colors.grey)),
                      contentPadding: EdgeInsets.symmetric(horizontal: 10),),
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.end,
                  onFieldSubmitted: (value) {
                    formMIncomeChangeNotif.controllerOtherInstallments.text = formMIncomeChangeNotif.formatCurrency.formatCurrency(value);
                    formMIncomeChangeNotif.calculateTotalIncomeNonEntrepreneur();
                    formMIncomeChangeNotif.calculateRestIncomeNonEntrepreneur();
                  },
                  onTap: () {
                    formMIncomeChangeNotif.formattingProfessionalOther();
                    formMIncomeChangeNotif.calculateTotalIncomeNonEntrepreneur();
                    formMIncomeChangeNotif.calculateRestIncomeNonEntrepreneur();
                  },
                  textInputAction: TextInputAction.done,
                  inputFormatters: [
                    DecimalTextInputFormatter(decimalRange: 2),
                    formMIncomeChangeNotif.amountValidator
                  ],
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 47),
            ],
          ),
        );
      },
    );
  }
}
