import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_collatelar_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/decimal_text_input_formatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class WidgetInfoCollateralAutomotive extends StatefulWidget {
  final String objectSelected;
  final String flag;

  const WidgetInfoCollateralAutomotive({this.objectSelected, this.flag});

  @override
  _WidgetInfoCollateralAutomotiveState createState() => _WidgetInfoCollateralAutomotiveState();
}

class _WidgetInfoCollateralAutomotiveState extends State<WidgetInfoCollateralAutomotive> {

  Future<void> _setPreference;

  @override
  void initState() {
    super.initState();
    // Provider.of<InformationCollateralChangeNotifier>(context,listen: false).setDataCollaOtoFromSQLite(context);
    _setPreference = Provider.of<InformationCollateralChangeNotifier>(context,listen: false).setPreference(context, widget.objectSelected);
    // Provider.of<InformationCollateralChangeNotifier>(context,listen: false).getDataFromDashboard(context);
  }
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _setPreference,
        builder: (context,snapshot){
          if(snapshot.connectionState == ConnectionState.waiting){
            return Center(
              child: CircularProgressIndicator()
            );
          }
          return Consumer<InformationCollateralChangeNotifier>(
            builder: (context,infoCollateralChangeNotifier,_){
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Visibility(
                    visible: infoCollateralChangeNotifier.isRadioValueIsCollaNameSameWithApplicantOtoVisible(),
                    child: Text(
                      "Nama Jaminan = Pemohon",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: infoCollateralChangeNotifier.isRadioValueIsCollaNameSameWithApplicantOtoChanges ? Colors.purple : Colors.black,
                      ),
                    ),
                  ),
                  Visibility(
                    visible: infoCollateralChangeNotifier.isRadioValueIsCollaNameSameWithApplicantOtoVisible(),
                    child: IgnorePointer(
                      ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                      child: Row(
                        children: [
                          Row(
                            children: [
                              Radio(
                                  activeColor: primaryOrange,
                                  value: 0,
                                  groupValue: infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto,
                                  onChanged: (value){
                                    setState(() {
                                      infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto = value;
                                    });
                                    infoCollateralChangeNotifier.sameWithApplicantAutomotive(context);
                                  }
                              ),
                              Text("Ya")
                            ],
                          ),
                          SizedBox(height: MediaQuery.of(context).size.width / 27),
                          Row(
                            children: [
                              Radio(
                                  activeColor: primaryOrange,
                                  value: 1,
                                  groupValue: infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto,
                                  onChanged: (value){
                                    setState(() {
                                      infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto = value;
                                    });
                                    infoCollateralChangeNotifier.sameWithApplicantAutomotive(context);
                                  }
                              ),
                              Text("Tidak")
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Visibility(
                      visible: infoCollateralChangeNotifier.isRadioValueIsCollaNameSameWithApplicantOtoVisible(),
                      child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                  // Visibility(
                  //   visible: infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto == 0,
                  //   child: DropdownButtonFormField<IdentityModel>(
                  //       value: infoCollateralChangeNotifier.identityTypeSelectedAuto,
                  //       // autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                  //       // validator: (e) {
                  //       //     if (e == null) {
                  //       //         return "Tidak boleh kosong";
                  //       //     } else {
                  //       //         return null;
                  //       //     }
                  //       // },
                  //       decoration: InputDecoration(
                  //           labelText: "Jenis Identitas",
                  //           border: OutlineInputBorder(),
                  //           contentPadding: EdgeInsets.symmetric(horizontal: 10),
                  //       ),
                  //       items: infoCollateralChangeNotifier.listIdentityType.map((data) {
                  //           return DropdownMenuItem<IdentityModel>(
                  //               value: data,
                  //               child: Text(
                  //                   data.name,
                  //                   overflow: TextOverflow.ellipsis,
                  //               ),
                  //           );
                  //       }).toList(),
                  //       onChanged: (newVal) {
                  //           infoCollateralChangeNotifier.identityTypeSelectedAuto = newVal;
                  //       },
                  //       onTap: () {
                  //           FocusManager.instance.primaryFocus.unfocus();
                  //       },
                  //   ),
                  // ),
                  infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto == 0
                      ?
                  SizedBox()
                      :
                  Column(
                    children: [
                      Visibility(
                        visible: infoCollateralChangeNotifier.isIdentityTypeSelectedAutoVisible(),
                        child: IgnorePointer(
                          ignoring: infoCollateralChangeNotifier.disableJenisPenawaran,
                          child: DropdownButtonFormField<IdentityModel>(
                              value: infoCollateralChangeNotifier.identityTypeSelectedAuto,
                              autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                              validator: (e) {
                                if (e == null && infoCollateralChangeNotifier.isIdentityTypeSelectedAutoMandatory()) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              decoration: InputDecoration(
                                  labelText: 'Jenis Identitas',
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: infoCollateralChangeNotifier.disableJenisPenawaran,
                                  fillColor: Colors.black12,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: infoCollateralChangeNotifier.isIdentityTypeSelectedAutoChanges ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: infoCollateralChangeNotifier.isIdentityTypeSelectedAutoChanges ? Colors.purple : Colors.grey)),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10)
                              ),
                              items: infoCollateralChangeNotifier.listIdentityType.map((data) {
                                return DropdownMenuItem<IdentityModel>(
                                  value: data,
                                  child: Text(
                                    data.name,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                );
                              }).toList(),
                              onChanged: (newVal) {
                                infoCollateralChangeNotifier.identityTypeSelectedAuto = newVal;
                              },
                              onTap: () {
                                FocusManager.instance.primaryFocus.unfocus();
                              }),
                        ),
                      ),
                      Visibility(
                          visible: infoCollateralChangeNotifier.isIdentityTypeSelectedAutoVisible(),
                          child:SizedBox(height: MediaQuery.of(context).size.height / 47)
                      ),
                      Visibility(
                        visible: infoCollateralChangeNotifier.isIdentityNumberAutoVisible(),
                        child: IgnorePointer(
                          ignoring: infoCollateralChangeNotifier.disableJenisPenawaran,
                          child: TextFormField(
                            autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                            validator: (e) {
                              if (e.isEmpty && infoCollateralChangeNotifier.isIdentityNumberAutoMandatory()) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            controller: infoCollateralChangeNotifier.controllerIdentityNumberAuto,
                            keyboardType: infoCollateralChangeNotifier.identityTypeSelectedAuto != null
                              ? infoCollateralChangeNotifier.identityTypeSelectedAuto.id != "03"
                              ? TextInputType.number
                                  : TextInputType.text
                                  : TextInputType.number,
                            textCapitalization: TextCapitalization.characters,
                            inputFormatters: infoCollateralChangeNotifier.identityTypeSelectedAuto != null
                              ? infoCollateralChangeNotifier.identityTypeSelectedAuto.id != "03"
                              ? infoCollateralChangeNotifier.identityTypeSelectedAuto.id == "01"
                              ? [WhitelistingTextInputFormatter.digitsOnly, LengthLimitingTextInputFormatter(16)]
                                  : [WhitelistingTextInputFormatter.digitsOnly]
                                  : null
                                  : [WhitelistingTextInputFormatter.digitsOnly],
                            decoration: InputDecoration(
                                labelText: 'No Identitas',
                                labelStyle: TextStyle(color: Colors.black),
                                filled: infoCollateralChangeNotifier.disableJenisPenawaran,
                                fillColor: Colors.black12,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: infoCollateralChangeNotifier.isIdentityNumberAutoChanges ? Colors.purple : Colors.grey)),
                                disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: infoCollateralChangeNotifier.isIdentityNumberAutoChanges ? Colors.purple : Colors.grey)),
                                contentPadding: EdgeInsets.symmetric(horizontal: 10)
                            ),
                          ),
                        ),
                      ),
                      Visibility(
                          visible: infoCollateralChangeNotifier.isIdentityNumberAutoVisible(),
                          child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                      ),
                      Visibility(
                          visible: infoCollateralChangeNotifier.isNameOnCollateralAutoVisible(),
                          child: IgnorePointer(
                            ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                            child: TextFormField(
                              validator: (e) {
                                if (e.isEmpty && infoCollateralChangeNotifier.isNameOnCollateralAutoMandatory()) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              keyboardType: TextInputType.text,
                              textCapitalization: TextCapitalization.characters,
                              controller: infoCollateralChangeNotifier.controllerNameOnCollateralAuto,
                              autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                              decoration: InputDecoration(
                                  labelText: 'Nama Pada Jaminan',
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                                  fillColor: Colors.black12,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: infoCollateralChangeNotifier.isNameOnCollateralAutoChanges ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: infoCollateralChangeNotifier.isNameOnCollateralAutoChanges ? Colors.purple : Colors.grey)),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10)
                              ),
                              inputFormatters: [
                                FilteringTextInputFormatter.allow(RegExp('[a-zA-Z  \`]')),
                              ],
                            ),
                          )
                      ),
                      Visibility(
                          visible: infoCollateralChangeNotifier.isNameOnCollateralAutoVisible(),
                          child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                      ),
                      Visibility(
                          visible: infoCollateralChangeNotifier.isBirthDateAutoVisible(),
                          child: IgnorePointer(
                            ignoring: infoCollateralChangeNotifier.disableJenisPenawaran,
                            child: TextFormField(
                              autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                              validator: (e) {
                                if (e.isEmpty && infoCollateralChangeNotifier.isBirthDateAutoMandatory()) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              onTap: () {
                                FocusManager.instance.primaryFocus.unfocus();
                                if(infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto == 1) infoCollateralChangeNotifier.selectBirthDate(context,0);
                              },
                              controller: infoCollateralChangeNotifier.controllerBirthDateAuto,
                              style: TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                  labelText: 'Tanggal Lahir',
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: infoCollateralChangeNotifier.disableJenisPenawaran,
                                  fillColor: Colors.black12,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: infoCollateralChangeNotifier.isBirthDateAutoChanges ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: infoCollateralChangeNotifier.isBirthDateAutoChanges ? Colors.purple : Colors.grey)),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10)
                              ),
                              readOnly: true,
                            ),
                          )
                      ),
                      Visibility(
                          visible: infoCollateralChangeNotifier.isBirthDateAutoVisible(),
                          child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                      ),
                      Visibility(
                          visible: infoCollateralChangeNotifier.isBirthPlaceValidWithIdentityAutoVisible(),
                          child: IgnorePointer(
                            ignoring: infoCollateralChangeNotifier.disableJenisPenawaran,
                            child: TextFormField(
                              autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                              validator: (e) {
                                if (e.isEmpty && infoCollateralChangeNotifier.isBirthPlaceValidWithIdentityAutoMandatory()) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              enabled: infoCollateralChangeNotifier.custType != "PER" ? false : true,
                              // readOnly: true,
                              textCapitalization: TextCapitalization.characters,
                              controller: infoCollateralChangeNotifier.controllerBirthPlaceValidWithIdentityAuto,
                              decoration: InputDecoration(
                                  labelText: 'Tempat Lahir Sesuai Identitas',
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: infoCollateralChangeNotifier.custType != "PER" ? true : false,
                                  fillColor: infoCollateralChangeNotifier.custType != "PER" ? Colors.black12 : null,
                                  errorStyle: TextStyle(
                                    color: Theme.of(context).errorColor, // or any other color
                                  ),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: infoCollateralChangeNotifier.isBirthPlaceValidWithIdentityAutoChanges ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: infoCollateralChangeNotifier.isBirthPlaceValidWithIdentityAutoChanges ? Colors.purple : Colors.grey)),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10)
                              ),
                              // decoration: new InputDecoration(
                              //     labelText: 'Tempat Lahir Sesuai Identitas',
                              //     labelStyle: TextStyle(color: Colors.black),
                              //     border: OutlineInputBorder(
                              //         borderRadius: BorderRadius.circular(8)))
                              inputFormatters: [
                                FilteringTextInputFormatter.allow(RegExp('[a-zA-Z]')),
                              ],
                            ),
                          )
                      ),
                      Visibility(
                          visible: infoCollateralChangeNotifier.isBirthPlaceValidWithIdentityAutoVisible(),
                          child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                      ),
                      Visibility(
                          visible: infoCollateralChangeNotifier.isBirthPlaceValidWithIdentityLOVAutoVisible(),
                          child: IgnorePointer(
                            ignoring: infoCollateralChangeNotifier.disableJenisPenawaran,
                            child: TextFormField(
                              validator: (e) {
                                if (e.isEmpty && infoCollateralChangeNotifier.isBirthPlaceValidWithIdentityLOVAutoMandatory()) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              readOnly: true,
                              enabled: infoCollateralChangeNotifier.custType != "PER" ? false : true,
                              onTap: (){
                                if(infoCollateralChangeNotifier.radioValueIsCollaNameSameWithApplicantOto == 1)
                                  infoCollateralChangeNotifier.searchBirthPlace(context,0);
                              },
                              controller: infoCollateralChangeNotifier.controllerBirthPlaceValidWithIdentityLOVAuto,
                              autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                              decoration: InputDecoration(
                                  labelText: 'Tempat Lahir Sesuai Identitas',
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: infoCollateralChangeNotifier.custType != "PER" ? true : false,
                                  fillColor: infoCollateralChangeNotifier.custType != "PER" ? Colors.black12 : null,
                                  errorStyle: TextStyle(
                                    color: Theme.of(context).errorColor, // or any other color
                                  ),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: infoCollateralChangeNotifier.isBirthPlaceValidWithIdentityLOVAutoChanges ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: infoCollateralChangeNotifier.isBirthPlaceValidWithIdentityLOVAutoChanges ? Colors.purple : Colors.grey)),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10)
                              ),
                            ),
                          )
                      ),
                      Visibility(
                          visible: infoCollateralChangeNotifier.isBirthPlaceValidWithIdentityLOVAutoVisible(),
                          child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                      ),
                    ],
                  ),
                  Visibility(
                    visible: infoCollateralChangeNotifier.isRadioValueIsCollateralSameWithUnitOtoVisible(),
                    child: Text("Collateral = Unit",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: infoCollateralChangeNotifier.isRadioValueIsCollateralSameWithUnitOtoChanges ? Colors.purple : Colors.black,
                      ),
                    ),
                  ),
                  Visibility(
                      visible: infoCollateralChangeNotifier.isRadioValueIsCollateralSameWithUnitOtoVisible(),
                      child: IgnorePointer(
                        ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                        child: Row(
                          children: [
                            Row(
                              children: [
                                Radio(
                                    activeColor: primaryOrange,
                                    value: 0,
                                    groupValue: infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto,
                                    onChanged: (value){
                                      Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).groupObjectSelected.KODE != "001"
                                          || Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).groupObjectSelected.KODE != "002"
                                          ? null
                                          : infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto = value;
                                      infoCollateralChangeNotifier.setSameWithApplicantAutomotive(context);
                                    }
                                ),
                                Text("Ya")
                              ],
                            ),
                            SizedBox(height: MediaQuery.of(context).size.width / 27),
                            Row(
                              children: [
                                Radio(
                                    activeColor: primaryOrange,
                                    value: 1,
                                    groupValue: infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto,
                                    onChanged: (value){
                                      Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).groupObjectSelected.KODE != "001"
                                          || Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).groupObjectSelected.KODE != "002"
                                          ? null
                                          : infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto = value;
                                      infoCollateralChangeNotifier.setSameWithApplicantAutomotive(context);
                                    }
                                ),
                                Text("Tidak")
                              ],
                            )
                          ],
                        ),
                      )
                  ),
                  Visibility(
                      visible: infoCollateralChangeNotifier.isRadioValueIsCollateralSameWithUnitOtoVisible(),
                      child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                  infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto == 0
                      ?
                  SizedBox()
                      :
                  Column(
                    children: [
                      Visibility(
                          visible: infoCollateralChangeNotifier.isGroupObjectVisible(),
                          child: IgnorePointer(
                            ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                            child: TextFormField(
                              validator: (e) {
                                if (e.isEmpty && infoCollateralChangeNotifier.isGroupObjectMandatory()) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              readOnly: true,
                              onTap: () {
                                infoCollateralChangeNotifier.searchGroupObject(context, widget.flag);
                              },
                              controller: infoCollateralChangeNotifier.controllerGroupObject,
                              autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                              decoration: InputDecoration(
                                  labelText: 'Grup Objek',
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                                  fillColor: Colors.black12,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: infoCollateralChangeNotifier.isGroupObjectChanges ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: infoCollateralChangeNotifier.isGroupObjectChanges ? Colors.purple : Colors.grey)),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10)
                              ),
                            ),
                          )
                      ),
                      Visibility(
                          visible: infoCollateralChangeNotifier.isGroupObjectVisible(),
                          child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                      ),
                      Visibility(
                          visible: infoCollateralChangeNotifier.isObjectVisible(),
                          child: IgnorePointer(
                            ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                            child: TextFormField(
                              validator: (e) {
                                if (e.isEmpty && infoCollateralChangeNotifier.isObjectMandatory()) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              readOnly: true,
                              onTap: () {
                                infoCollateralChangeNotifier.searchObject(context);
                              },
                              controller: infoCollateralChangeNotifier.controllerObject,
                              autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                              decoration: InputDecoration(
                                  labelText: 'Objek',
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                                  fillColor: Colors.black12,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: infoCollateralChangeNotifier.isObjectChanges ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: infoCollateralChangeNotifier.isObjectChanges ? Colors.purple : Colors.grey)),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10)
                              ),
                            ),
                          )
                      ),
                      Visibility(
                          visible: infoCollateralChangeNotifier.isObjectVisible(),
                          child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                      ),
                      Visibility(
                        visible: infoCollateralChangeNotifier.isTypeProductVisible(),
                        child: IgnorePointer(
                          ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                          child: TextFormField(
                            validator: (e) {
                              if (e.isEmpty && infoCollateralChangeNotifier.isTypeProductMandatory()) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            readOnly: true,
                            onTap: (){
                              infoCollateralChangeNotifier.searchProductType(context,widget.flag);
                            },
                            controller: infoCollateralChangeNotifier.controllerTypeProduct,
                            autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                            decoration: InputDecoration(
                                labelText: 'Jenis Produk',
                                labelStyle: TextStyle(color: Colors.black),
                                filled: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                                fillColor: Colors.black12,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: infoCollateralChangeNotifier.isTypeProductChanges ? Colors.purple : Colors.grey)),
                                disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: infoCollateralChangeNotifier.isTypeProductChanges ? Colors.purple : Colors.grey)),
                                contentPadding: EdgeInsets.symmetric(horizontal: 10)
                            ),
                          ),
                        ),
                      ),
                      Visibility(
                          visible: infoCollateralChangeNotifier.isTypeProductVisible(),
                          child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                      ),
                      Visibility(
                          visible: infoCollateralChangeNotifier.isBrandObjectVisible(),
                          child: IgnorePointer(
                            ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                            child: TextFormField(
                              validator: (e) {
                                if (e.isEmpty && infoCollateralChangeNotifier.isBrandObjectMandatory()) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              readOnly: true,
                              onTap: () {
                                // infoCollateralChangeNotifier.searchBrandObject(context,widget.flag,1);
                                infoCollateralChangeNotifier.searchMerkJenisModelObject(context, 1);
                              },
                              controller: infoCollateralChangeNotifier.controllerBrandObject,
                              autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                              decoration: InputDecoration(
                                  labelText: 'Merk Objek',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: infoCollateralChangeNotifier.isBrandObjectChanges ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: infoCollateralChangeNotifier.isBrandObjectChanges ? Colors.purple : Colors.grey)),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10)
                              ),
                            ),
                          )
                      ),
                      Visibility(
                          visible: infoCollateralChangeNotifier.isBrandObjectVisible(),
                          child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                      ),
                      Visibility(
                        visible: infoCollateralChangeNotifier.isObjectTypeVisible(),
                        child: IgnorePointer(
                          ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                          child: TextFormField(
                            validator: (e) {
                              if (e.isEmpty && infoCollateralChangeNotifier.isObjectTypeMandatory()) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            readOnly: true,
                            onTap: () {
                              // infoCollateralChangeNotifier.searchObjectType(context,widget.flag,2);
                              infoCollateralChangeNotifier.searchMerkJenisModelObject(context, 2);
                            },
                            controller: infoCollateralChangeNotifier.controllerObjectType,
                            autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                            decoration: InputDecoration(
                                labelText: 'Jenis Objek',
                                labelStyle: TextStyle(color: Colors.black),
                                filled: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                                fillColor: Colors.black12,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: infoCollateralChangeNotifier.isObjectTypeChanges ? Colors.purple : Colors.grey)),
                                disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: infoCollateralChangeNotifier.isObjectTypeChanges ? Colors.purple : Colors.grey)),
                                contentPadding: EdgeInsets.symmetric(horizontal: 10)
                            ),
                          ),
                        ),
                      ),
                      Visibility(
                          visible: infoCollateralChangeNotifier.isObjectTypeVisible(),
                          child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                      ),
                      Visibility(
                        visible: infoCollateralChangeNotifier.isModelObjectVisible(),
                        child: IgnorePointer(
                          ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                          child: TextFormField(
                            validator: (e) {
                              if (e.isEmpty && infoCollateralChangeNotifier.isModelObjectMandatory()) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            readOnly: true,
                            onTap: () {
                              // infoCollateralChangeNotifier.searchModelObject(context,widget.flag,3);
                              infoCollateralChangeNotifier.searchMerkJenisModelObject(context, 3);
                            },
                            controller: infoCollateralChangeNotifier.controllerModelObject,
                            autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                            decoration: InputDecoration(
                                labelText: 'Model Objek',
                                labelStyle: TextStyle(color: Colors.black),
                                filled: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                                fillColor: Colors.black12,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: infoCollateralChangeNotifier.isModelObjectChanges ? Colors.purple : Colors.grey)),
                                disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: infoCollateralChangeNotifier.isModelObjectChanges ? Colors.purple : Colors.grey)),
                                contentPadding: EdgeInsets.symmetric(horizontal: 10)
                            ),
                          ),
                        ),
                      ),
                      Visibility(
                          visible: infoCollateralChangeNotifier.isModelObjectVisible(),
                          child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                      ),
                      Visibility(
                          visible: infoCollateralChangeNotifier.isUsageObjectModelVisible(),
                          child: IgnorePointer(
                            ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                            child: TextFormField(
                              validator: (e) {
                                if (e.isEmpty && infoCollateralChangeNotifier.isUsageObjectModelMandatory()) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              readOnly: true,
                              onTap: () {
                                infoCollateralChangeNotifier.searchObjectUsage(context);
                              },
                              controller: infoCollateralChangeNotifier.controllerUsageObjectModel,
                              autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                              decoration: InputDecoration(
                                  labelText: 'Pemakaian Objek',
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                                  fillColor: Colors.black12,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: infoCollateralChangeNotifier.isUsageObjectModelChanges ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: infoCollateralChangeNotifier.isUsageObjectModelChanges ? Colors.purple : Colors.grey)),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10)
                              ),
                            ),
                          )
                      ),
                      Visibility(
                          visible: infoCollateralChangeNotifier.isUsageObjectModelVisible(),
                          child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                      ),
                    ],
                  ),
                  // DropdownButtonFormField<IdentityModel>(
                  //     autovalidate:
                  //     infoCollateralChangeNotifier.autoValidateAuto,
                  //     validator: (e) {
                  //         if (e == null) {
                  //             return "Silahkan pilih jenis identitas";
                  //         } else {
                  //             return null;
                  //         }
                  //     },
                  //     value: infoCollateralChangeNotifier
                  //         .identityTypeSelected,
                  //     onChanged: (value) {
                  //         infoCollateralChangeNotifier
                  //             .identityTypeSelected = value;
                  //     },
                  //     onTap: () {
                  //         FocusManager.instance.primaryFocus.unfocus();
                  //     },
                  //     decoration: InputDecoration(
                  //         labelText: "Jenis Identitas",
                  //         border: OutlineInputBorder(),
                  //         contentPadding:
                  //         EdgeInsets.symmetric(horizontal: 10),
                  //     ),
                  //     items: infoCollateralChangeNotifier
                  //         .listIdentityType
                  //         .map((value) {
                  //         return DropdownMenuItem<IdentityModel>(
                  //             value: value,
                  //             child: Text(
                  //                 value.name,
                  //                 overflow: TextOverflow.ellipsis,
                  //             ),
                  //         );
                  //     }).toList()),
                  // SizedBox(height: MediaQuery.of(context).size.height / 47),
                  Visibility(
                    visible: infoCollateralChangeNotifier.isYearProductionSelectedVisible(),
                    child: IgnorePointer(
                      ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                      child: DropdownButtonFormField<String>(
                          autovalidate:
                          infoCollateralChangeNotifier.autoValidateAuto,
                          validator: (e) {
                            if (e == null && infoCollateralChangeNotifier.isYearProductionSelectedMandatory()) {
                              return "Silahkan pilih tahun pembuatan";
                            } else {
                              return null;
                            }
                          },
                          value: infoCollateralChangeNotifier.yearProductionSelected,
                          onChanged: (value) {
                            infoCollateralChangeNotifier.yearProductionSelected = value;
                            infoCollateralChangeNotifier.getMPAdiraUpload(context);
                          },
                          onTap: () {
                            FocusManager.instance.primaryFocus.unfocus();
                          },
                          decoration: InputDecoration(
                              labelText: 'Tahun Pembuatan',
                              labelStyle: TextStyle(color: Colors.black),
                              filled: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                              fillColor: Colors.black12,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: infoCollateralChangeNotifier.isYearProductionSelectedChanges ? Colors.purple : Colors.grey)),
                              disabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: infoCollateralChangeNotifier.isYearProductionSelectedChanges ? Colors.purple : Colors.grey)),
                              contentPadding: EdgeInsets.symmetric(horizontal: 10)
                          ),
                          // decoration: InputDecoration(
                          //     labelText: "Tahun Pembuatan",
                          //     border: OutlineInputBorder(),
                          //     contentPadding:
                          //     EdgeInsets.symmetric(horizontal: 10),
                          // ),
                          items: infoCollateralChangeNotifier.listProductionYear.map((value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(
                                value,
                                overflow: TextOverflow.ellipsis,
                              ),
                            );
                          }).toList()),
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  Visibility(
                    visible: infoCollateralChangeNotifier.isYearRegistrationSelectedVisible(),
                    child: IgnorePointer(
                      ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                      child: DropdownButtonFormField<String>(
                          autovalidate:
                          infoCollateralChangeNotifier.autoValidateAuto,
                          validator: (e) {
                            if (e == null && infoCollateralChangeNotifier.isYearRegistrationSelectedMandatory()) {
                              return "Silahkan pilih tahun registrasi";
                            } else {
                              return null;
                            }
                          },
                          value: infoCollateralChangeNotifier.yearRegistrationSelected,
                          onChanged: (value) {
                            infoCollateralChangeNotifier.yearRegistrationSelected = value;
                          },
                          onTap: () {
                            FocusManager.instance.primaryFocus.unfocus();
                          },
                          decoration: InputDecoration(
                              labelText: 'Tahun Registrasi',
                              labelStyle: TextStyle(color: Colors.black),
                              filled: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                              fillColor: Colors.black12,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: infoCollateralChangeNotifier.isYearRegistrationSelectedChanges ? Colors.purple : Colors.grey)),
                              disabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: infoCollateralChangeNotifier.isYearRegistrationSelectedChanges ? Colors.purple : Colors.grey)),
                              contentPadding: EdgeInsets.symmetric(horizontal: 10)
                          ),
                          // decoration: InputDecoration(
                          //     labelText: "Tahun Registrasi",
                          //     border: OutlineInputBorder(),
                          //     contentPadding:
                          //     EdgeInsets.symmetric(horizontal: 10),
                          // ),
                          items: infoCollateralChangeNotifier.listRegistrationYear.map((value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(
                                value,
                                overflow: TextOverflow.ellipsis,
                              ),
                            );
                          }).toList()),
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  Visibility(
                    visible: infoCollateralChangeNotifier.isRadioValueYellowPlatVisible(),
                    child: Text("Plat Kuning",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: infoCollateralChangeNotifier.isRadioValueYellowPlatChanges ? Colors.purple : Colors.black,
                      ),
                    ),
                  ),
                  Visibility(
                    visible: infoCollateralChangeNotifier.isRadioValueYellowPlatVisible(),
                    child: IgnorePointer(
                      ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                      child: Row(
                        children: [
                          Row(
                            children: [
                              Radio(
                                  activeColor: primaryOrange,
                                  value: 0,
                                  groupValue: infoCollateralChangeNotifier.radioValueYellowPlat,
                                  onChanged: (value){
                                    setState(() {
                                      infoCollateralChangeNotifier.radioValueYellowPlat = value;
                                    });
                                  }
                              ),
                              Text("Ya")
                            ],
                          ),
                          SizedBox(height: MediaQuery.of(context).size.width / 27),
                          Row(
                            children: [
                              Radio(
                                  activeColor: primaryOrange,
                                  value: 1,
                                  groupValue: infoCollateralChangeNotifier.radioValueYellowPlat,
                                  onChanged: (value){
                                    setState(() {
                                      infoCollateralChangeNotifier.radioValueYellowPlat = value;
                                    });
                                  }
                              ),
                              Text("Tidak")
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Visibility(
                    visible: infoCollateralChangeNotifier.isRadioValueYellowPlatVisible(),
                    child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                  ),
                  Visibility(
                    visible: infoCollateralChangeNotifier.isRadioValueBuiltUpNonATPMVisible(),
                    child: Text("Built Up Non ATPM",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: infoCollateralChangeNotifier.isRadioValueBuiltUpNonATPMChanges ? Colors.purple : Colors.black,
                      ),
                    ),
                  ),
                  Visibility(
                    visible: infoCollateralChangeNotifier.isRadioValueBuiltUpNonATPMVisible(),
                    child: IgnorePointer(
                      ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                      child: Row(
                        children: [
                          Row(
                            children: [
                              Radio(
                                  activeColor: primaryOrange,
                                  value: 0,
                                  groupValue: infoCollateralChangeNotifier.radioValueBuiltUpNonATPM,
                                  onChanged: (value){
                                    infoCollateralChangeNotifier.radioValueBuiltUpNonATPM = value;
                                  }
                              ),
                              Text("Ya")
                            ],
                          ),
                          SizedBox(height: MediaQuery.of(context).size.width / 27),
                          Row(
                            children: [
                              Radio(
                                  activeColor: primaryOrange,
                                  value: 1,
                                  groupValue: infoCollateralChangeNotifier.radioValueBuiltUpNonATPM,
                                  onChanged: (value){
                                    infoCollateralChangeNotifier.radioValueBuiltUpNonATPM = value;
                                  }
                              ),
                              Text("Tidak")
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  widget.objectSelected == "001" || widget.objectSelected == "003"
                      ?
                  Visibility(
                    visible: infoCollateralChangeNotifier.isRadioValueBuiltUpNonATPMVisible(),
                    child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                  )
                      :
                  Column(
                    children: [
                      // SizedBox(height: MediaQuery.of(context).size.height / 47),
                      Visibility(
                        visible: infoCollateralChangeNotifier.isPoliceNumberVisible(),
                        child: IgnorePointer(
                          ignoring: infoCollateralChangeNotifier.disableJenisPenawaran,
                          child: TextFormField(
                            validator: (e) {
                              if (e.isEmpty && infoCollateralChangeNotifier.isPoliceNumberMandatory()) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                            controller: infoCollateralChangeNotifier.controllerPoliceNumber,
                            decoration: InputDecoration(
                                labelText: 'No Polisi',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: infoCollateralChangeNotifier.isPoliceNumberChanges ? Colors.purple : Colors.grey)),
                                disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: infoCollateralChangeNotifier.isPoliceNumberChanges ? Colors.purple : Colors.grey)),
                                contentPadding: EdgeInsets.symmetric(horizontal: 10)
                            ),
                            textCapitalization: TextCapitalization.characters,
                            inputFormatters: [
                              FilteringTextInputFormatter.allow(RegExp('[a-zA-Z0-9 ]')),
                            ],
                          ),
                        ),
                      ),
                      Visibility(
                          visible: infoCollateralChangeNotifier.isPoliceNumberVisible(),
                          child: SizedBox(height: MediaQuery.of(context).size.width / 27)
                      ),
                      Visibility(
                        visible: infoCollateralChangeNotifier.isFrameNumberVisible(),
                        child: IgnorePointer(
                          ignoring: infoCollateralChangeNotifier.disableJenisPenawaran,
                          child: TextFormField(
                            validator: (e) {
                              if (e.isEmpty && infoCollateralChangeNotifier.isFrameNumberMandatory()) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                            controller: infoCollateralChangeNotifier.controllerFrameNumber,
                            decoration: InputDecoration(
                                labelText: 'No Rangka',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: infoCollateralChangeNotifier.isFrameNumberChanges ? Colors.purple : Colors.grey)),
                                disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: infoCollateralChangeNotifier.isFrameNumberChanges ? Colors.purple : Colors.grey)),
                                contentPadding: EdgeInsets.symmetric(horizontal: 10)
                            ),
                            textCapitalization: TextCapitalization.characters,
                            inputFormatters: [
                              FilteringTextInputFormatter.allow(RegExp('[a-zA-Z0-9-]')),
                            ],
                          ),
                        ),
                      ),
                      Visibility(
                          visible: infoCollateralChangeNotifier.isFrameNumberVisible(),
                          child: SizedBox(height: MediaQuery.of(context).size.width / 27)
                      ),
                      Visibility(
                        visible: infoCollateralChangeNotifier.isMachineNumberVisible(),
                        child: IgnorePointer(
                          ignoring: infoCollateralChangeNotifier.disableJenisPenawaran,
                          child: TextFormField(
                            validator: (e) {
                              if (e.isEmpty && infoCollateralChangeNotifier.isMachineNumberMandatory()) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                            controller: infoCollateralChangeNotifier.controllerMachineNumber,
                            decoration: InputDecoration(
                                labelText: 'No Mesin',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: infoCollateralChangeNotifier.isMachineNumberChanges ? Colors.purple : Colors.grey)),
                                disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: infoCollateralChangeNotifier.isMachineNumberChanges ? Colors.purple : Colors.grey)),
                                contentPadding: EdgeInsets.symmetric(horizontal: 10)
                            ),
                            textCapitalization: TextCapitalization.characters,
                            inputFormatters: [
                              FilteringTextInputFormatter.allow(RegExp('[a-zA-Z0-9-]')),
                            ],
                          ),
                        ),
                      ),
                      Visibility(
                          visible: infoCollateralChangeNotifier.isMachineNumberVisible(),
                          child: SizedBox(height: MediaQuery.of(context).size.width / 27)
                      ),
                      Visibility(
                        visible: infoCollateralChangeNotifier.isBPKPNumberVisible(),
                        child: IgnorePointer(
                          ignoring: infoCollateralChangeNotifier.disableJenisPenawaran,
                          child: TextFormField(
                            validator: (e) {
                              if (e.isEmpty && infoCollateralChangeNotifier.isBPKPNumberMandatory()) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            controller: infoCollateralChangeNotifier.controllerBPKPNumber,
                            autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                            textCapitalization: TextCapitalization.characters,
                            decoration: InputDecoration(
                                labelText: 'No BPKB',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: infoCollateralChangeNotifier.isBPKPNumberChanges ? Colors.purple : Colors.grey)),
                                disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: infoCollateralChangeNotifier.isBPKPNumberChanges ? Colors.purple : Colors.grey)),
                                contentPadding: EdgeInsets.symmetric(horizontal: 10)
                            ),
                            inputFormatters: [
                              FilteringTextInputFormatter.allow(RegExp('[a-zA-Z0-9 ]')),
                            ],
                          ),
                        ),
                      ),
                      Visibility(
                          visible: infoCollateralChangeNotifier.isBPKPNumberVisible(),
                          child: SizedBox(height: MediaQuery.of(context).size.width / 27)
                      ),
                    ],
                  ),
                  Visibility(
                    visible: infoCollateralChangeNotifier.isGradeUnitVisible(),
                    child: IgnorePointer(
                      ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                      child: TextFormField(
                        autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                        validator: (e) {
                          if (e.isEmpty && infoCollateralChangeNotifier.isGradeUnitMandatory()) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        controller: infoCollateralChangeNotifier.controllerGradeUnit,
                        decoration: InputDecoration(
                            labelText: 'Grade Unit',
                            labelStyle: TextStyle(color: Colors.black),
                            filled: true,
                            fillColor: Colors.black12,
                            errorStyle: TextStyle(
                              color: Theme.of(context).errorColor, // or any other color
                            ),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isGradeUnitChanges ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isGradeUnitChanges ? Colors.purple : Colors.grey)),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10)
                        ),
                        enabled: false,
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                        inputFormatters: [
                          FilteringTextInputFormatter.allow(RegExp('[a-zA-Z0-9]')),
                        ],
                      ),
                    ),
                  ),
                  Visibility(
                      visible: infoCollateralChangeNotifier.isGradeUnitVisible(),
                      child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                  Visibility(
                    visible: infoCollateralChangeNotifier.isFasilitasUTJVisible(),
                    child: IgnorePointer(
                      ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                      child: TextFormField(
                        validator: (e) {
                          if (e.isEmpty && infoCollateralChangeNotifier.isFasilitasUTJMandatory()) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                        controller: infoCollateralChangeNotifier.controllerFasilitasUTJ,
                        decoration: InputDecoration(
                            labelText: 'Fasilitas UTJ',
                            labelStyle: TextStyle(color: Colors.black),
                            filled: true,
                            fillColor: Colors.black12,
                            errorStyle: TextStyle(
                              color: Theme.of(context).errorColor, // or any other color
                            ),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isFasilitasUTJChanges ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isFasilitasUTJChanges ? Colors.purple : Colors.grey)),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10)
                        ),
                        enabled: false,
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                        inputFormatters: [
                          FilteringTextInputFormatter.allow(RegExp('[a-zA-Z0-9]')),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  Visibility(
                    visible: infoCollateralChangeNotifier.isNamaBidderVisible(),
                    child: IgnorePointer(
                      ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                      child: TextFormField(
                        validator: (e) {
                          if (e.isEmpty && infoCollateralChangeNotifier.isNamaBidderMandatory()) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                        controller: infoCollateralChangeNotifier.controllerNamaBidder,
                        decoration: InputDecoration(
                            labelText: 'Nama Bidder/harga jual/harga pasar',
                            labelStyle: TextStyle(color: Colors.black),
                            filled: true,
                            fillColor: Colors.black12,
                            errorStyle: TextStyle(
                              color: Theme.of(context).errorColor, // or any other color
                            ),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isNamaBidderChanges ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isNamaBidderChanges ? Colors.purple : Colors.grey)),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 15)
                        ),
                        maxLines: 2,
                        enabled: false,
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                        inputFormatters: [
                          FilteringTextInputFormatter.allow(RegExp('[a-zA-Z0-9]')),
                        ],
                      ),
                    ),
                  ),
                  Visibility(
                      visible: infoCollateralChangeNotifier.isNamaBidderVisible(),
                      child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                  Visibility(
                    visible: infoCollateralChangeNotifier.isHargaJualShowroomVisible(),
                    child: IgnorePointer(
                      ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                      child: TextFormField(
                        validator: (e) {
                          if (e.isEmpty && infoCollateralChangeNotifier.isHargaJualShowroomMandatory()) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.end,
                        inputFormatters: [
                          DecimalTextInputFormatter(decimalRange: 2),
                          infoCollateralChangeNotifier.amountValidator
                        ],
                        onFieldSubmitted: (value){
                          infoCollateralChangeNotifier.controllerHargaJualShowroom.text = infoCollateralChangeNotifier.formatCurrency.formatCurrency(value);
                        },
                        onTap: () {
                          infoCollateralChangeNotifier.formatting();
                        },
                        controller: infoCollateralChangeNotifier.controllerHargaJualShowroom,
                        autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                        decoration: new InputDecoration(
                            labelText: 'Harga Jual Showroom/Penjual',
                            labelStyle: TextStyle(color: Colors.black),
                            filled: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran ? true : infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto == 0 ? true : false,
                            fillColor: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran ? Colors.black12 : infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto == 0 ? Colors.black12 : null,
                            errorStyle: TextStyle(
                              color: Theme.of(context).errorColor, // or any other color
                            ),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isHargaJualShowroomChanges ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isHargaJualShowroomChanges ? Colors.purple : Colors.grey)),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10)
                        ),
                        enabled: infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto == 0 ? false : true,
                      ),
                    ),
                  ),
                  Visibility(
                      visible: infoCollateralChangeNotifier.isHargaJualShowroomVisible(),
                      child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                  Visibility(
                    visible: infoCollateralChangeNotifier.isPerlengkapanTambahanVisible(),
                    child: IgnorePointer(
                      ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                      child: TextFormField(
                        validator: (e) {
                          if (e.isEmpty && infoCollateralChangeNotifier.isPerlengkapanTambahanMandatory()) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                        controller: infoCollateralChangeNotifier.controllerPerlengkapanTambahan,
                        decoration: new InputDecoration(
                            labelText: 'Perlengkapan Tambahan',
                            labelStyle: TextStyle(color: Colors.black),
                            filled: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran ? true : false,
                            fillColor: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran ? Colors.black12 : null,
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isPerlengkapanTambahanChanges ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isPerlengkapanTambahanChanges ? Colors.purple : Colors.grey)),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10)
                        ),
                        onTap: () {
                          infoCollateralChangeNotifier.formatting();
                        },
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                        inputFormatters: [
                          FilteringTextInputFormatter.allow(RegExp('[a-zA-Z0-9 ]')),
                        ],
                      ),
                    ),
                  ),
                  Visibility(
                      visible: infoCollateralChangeNotifier.isPerlengkapanTambahanVisible(),
                      child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                  Visibility(
                    visible: infoCollateralChangeNotifier.isMPAdiraVisible(),
                    child: IgnorePointer(
                      ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                      child: TextFormField(
                        validator: (e) {
                          if (e.isEmpty && infoCollateralChangeNotifier.isMPAdiraMandatory()) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        inputFormatters: [
                          WhitelistingTextInputFormatter.digitsOnly
                        ],
                        autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                        controller: infoCollateralChangeNotifier.controllerMPAdira,
                        decoration: new InputDecoration(
                            labelText: 'MP Adira',
                            labelStyle: TextStyle(color: Colors.black),
                            filled: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                            fillColor: Colors.black12,
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isMPAdiraChanges ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isMPAdiraChanges ? Colors.purple : Colors.grey)),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10)
                        ),
                        onTap: () {
                          infoCollateralChangeNotifier.formatting();
                        },
                        onFieldSubmitted: (val){
                          infoCollateralChangeNotifier.formatting();
                        },
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.end,
                      ),
                    ),
                  ),
                  Visibility(
                      visible: infoCollateralChangeNotifier.isMPAdiraVisible(),
                      child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                  Visibility(
                    visible: infoCollateralChangeNotifier.isMPAdiraUploadVisible(),
                    child: IgnorePointer(
                      ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                      child: TextFormField(
                        validator: (e) {
                          if (e.isEmpty && infoCollateralChangeNotifier.isMPAdiraUploadMandatory()) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        enabled: false,
                        autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                        controller: infoCollateralChangeNotifier.controllerMPAdiraUpload,
                        decoration: InputDecoration(
                            labelText: 'MP Adira Upload',
                            labelStyle: TextStyle(color: Colors.black),
                            filled: true,
                            fillColor: Colors.black12,
                            errorStyle: TextStyle(
                              color: Theme.of(context).errorColor, // or any other color
                            ),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isMPAdiraUploadChanges ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isMPAdiraUploadChanges ? Colors.purple : Colors.grey)),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10)
                        ),
                        textAlign: TextAlign.end,
                      ),
                    ),
                  ),
                  Visibility(
                      visible: infoCollateralChangeNotifier.isMPAdiraUploadVisible(),
                      child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                  // TextFormField(
                  //     autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                  //     validator: (e) {
                  //         if (e.isEmpty) {
                  //             return "Tidak boleh kosong";
                  //         } else {
                  //             return null;
                  //         }
                  //     },
                  //     keyboardType: TextInputType.number,
                  //     textAlign: TextAlign.end,
                  //     inputFormatters: [
                  //       DecimalTextInputFormatter(decimalRange: 2),
                  //       infoCollateralChangeNotifier.amountValidator
                  //     ],
                  //     onFieldSubmitted: (value){
                  //       infoCollateralChangeNotifier.controllerHargaPasar.text = infoCollateralChangeNotifier.formatCurrency.formatCurrency(value);
                  //     },
                  //     controller: infoCollateralChangeNotifier.controllerHargaPasar,
                  //     decoration: new InputDecoration(
                  //         labelText: 'Harga Pasar',
                  //         labelStyle: TextStyle(color: Colors.black),
                  //         border: OutlineInputBorder(
                  //             borderRadius: BorderRadius.circular(8))
                  //     ),
                  //     onTap: () {
                  //       infoCollateralChangeNotifier.formatting();
                  //     },
                  // ),
                  // SizedBox(height: MediaQuery.of(context).size.height / 47),
                  Visibility(
                    visible: infoCollateralChangeNotifier.isRekondisiFisikVisible(),
                    child: IgnorePointer(
                      ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                      child: TextFormField(
                        validator: (e) {
                          if (e.isEmpty && infoCollateralChangeNotifier.isRekondisiFisikMandatory()) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                        controller: infoCollateralChangeNotifier.controllerRekondisiFisik,
                        decoration: new InputDecoration(
                            labelText: 'Rekondisi Fisik',
                            labelStyle: TextStyle(color: Colors.black),
                            filled: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran ? true : false,
                            fillColor: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran ? Colors.black12 : null,
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isRekondisiFisikChanges ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isRekondisiFisikChanges ? Colors.purple : Colors.grey)),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10)
                        ),
                        onTap: () {
                          infoCollateralChangeNotifier.formatting();
                        },
                        onFieldSubmitted: (val){
                          infoCollateralChangeNotifier.formatting();
                        },
                        inputFormatters: [
                          WhitelistingTextInputFormatter.digitsOnly
                        ],
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.end,
                      ),
                    ),
                  ),
                  Visibility(
                      visible: infoCollateralChangeNotifier.isRekondisiFisikVisible(),
                      child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                  Visibility(
                    visible: infoCollateralChangeNotifier.isDpGuaranteeVisible(),
                    child: IgnorePointer(
                      ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                      child: TextFormField(
                        validator: (e) {
                          if (e.isEmpty && infoCollateralChangeNotifier.isDpGuaranteeMandatory()) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                        controller: infoCollateralChangeNotifier.controllerDPJaminan,
                        decoration: InputDecoration(
                            labelText: 'DP Jaminan',
                            labelStyle: TextStyle(color: Colors.black),
                            filled: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                            fillColor: Colors.black12,
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isDpGuaranteeChanges ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isDpGuaranteeChanges ? Colors.purple : Colors.grey)),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10)
                        ),
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.end,
                        inputFormatters: [
                          DecimalTextInputFormatter(decimalRange: 2),
                          infoCollateralChangeNotifier.amountValidator
                        ],
                        onFieldSubmitted: (value){
                          infoCollateralChangeNotifier.controllerDPJaminan.text = infoCollateralChangeNotifier.formatCurrency.formatCurrency(value);
                          infoCollateralChangeNotifier.formatting();
                        },
                        onTap: () {
                          infoCollateralChangeNotifier.formatting();
                        },
                      ),
                    ),
                  ),
                  Visibility(
                      visible: infoCollateralChangeNotifier.isDpGuaranteeVisible(),
                      child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                  Visibility(
                    visible: infoCollateralChangeNotifier.isPHMaxAutomotiveVisible(),
                    child: IgnorePointer(
                      ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                      child: TextFormField(
                        validator: (e) {
                          if (e.isEmpty && infoCollateralChangeNotifier.isPHMaxAutomotiveMandatory()) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                        controller: infoCollateralChangeNotifier.controllerPHMaxAutomotive,
                        decoration: InputDecoration(
                            labelText: 'PH Maksimal',
                            labelStyle: TextStyle(color: Colors.black),
                            filled: true,
                            fillColor: Colors.black12,
                            errorStyle: TextStyle(
                              color: Theme.of(context).errorColor, // or any other color
                            ),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isPHMaxAutomotiveChanges ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isPHMaxAutomotiveChanges ? Colors.purple : Colors.grey)),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10)
                        ),
                        enabled: false,
                        keyboardType: TextInputType.number,
                        // textCapitalization: TextCapitalization.characters,
                        onFieldSubmitted: (value){
                          infoCollateralChangeNotifier.controllerPHMaxAutomotive.text = infoCollateralChangeNotifier.formatCurrency.formatCurrency(value);
                        },
                        onTap: () {
                          infoCollateralChangeNotifier.formatting();
                        },
                        textAlign: TextAlign.end,
                        inputFormatters: [
                          DecimalTextInputFormatter(decimalRange: 2),
                          infoCollateralChangeNotifier.amountValidator
                        ],
                      ),
                    ),
                  ),
                  Visibility(
                      visible: infoCollateralChangeNotifier.isPHMaxAutomotiveVisible(),
                      child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                  Visibility(
                    visible: infoCollateralChangeNotifier.isTaksasiPriceAutomotiveVisible(),
                    child: IgnorePointer(
                      ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                      child: TextFormField(
                        autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                        validator: (e) {
                          if (e.isEmpty && infoCollateralChangeNotifier.isTaksasiPriceAutomotiveMandatory()) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.end,
                        inputFormatters: [
                          DecimalTextInputFormatter(decimalRange: 2),
                          infoCollateralChangeNotifier.amountValidator
                        ],
                        onFieldSubmitted: (value){
                          infoCollateralChangeNotifier.controllerTaksasiPriceAutomotive.text = infoCollateralChangeNotifier.formatCurrency.formatCurrency(value);
                        },
                        controller: infoCollateralChangeNotifier.controllerTaksasiPriceAutomotive,
                        decoration: InputDecoration(
                            labelText: 'Harga Taksasi / Appraisal',
                            labelStyle: TextStyle(color: Colors.black),
                            filled: true,
                            fillColor: Colors.black12,
                            errorStyle: TextStyle(
                              color: Theme.of(context).errorColor, // or any other color
                            ),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isTaksasiPriceAutomotiveChanges ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isTaksasiPriceAutomotiveChanges ? Colors.purple : Colors.grey)),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10)
                        ),
                        enabled: false,
                        onTap: () {
                          infoCollateralChangeNotifier.formatting();
                        },
                      ),
                    ),
                  ),
                  Visibility(
                      visible: infoCollateralChangeNotifier.isTaksasiPriceAutomotiveVisible(),
                      child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                  Visibility(
                    visible: infoCollateralChangeNotifier.isRadioValueWorthyOrUnworthyVisible(),
                    child: Text("Kesimpulan",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        letterSpacing: 0.15,
                        color: infoCollateralChangeNotifier.isRadioValueWorthyOrUnworthyChanges ? Colors.purple : Colors.black,
                      ),
                    ),
                  ),
                  Visibility(
                    visible: infoCollateralChangeNotifier.isRadioValueWorthyOrUnworthyVisible(),
                    child: IgnorePointer(
                      ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                      child: Row(
                        children: [
                          Row(
                            children: [
                              Radio(
                                  activeColor: primaryOrange,
                                  value: 0,
                                  groupValue: infoCollateralChangeNotifier.radioValueWorthyOrUnworthy,
                                  onChanged: (value){
                                    setState(() {
                                      infoCollateralChangeNotifier.radioValueWorthyOrUnworthy = value;
                                    });
                                  }
                              ),
                              Text("Layak")
                            ],
                          ),
                          SizedBox(height: MediaQuery.of(context).size.width / 27),
                          Row(
                            children: [
                              Radio(
                                  activeColor: primaryOrange,
                                  value: 1,
                                  groupValue: infoCollateralChangeNotifier.radioValueWorthyOrUnworthy,
                                  onChanged: (value){
                                      // widget.objectSelected == "001" || widget.objectSelected == "003" ? null :
                                    setState(() {
                                      infoCollateralChangeNotifier.radioValueWorthyOrUnworthy = value;
                                    });
                                  }
                              ),
                              Text("Tidak Layak")
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Visibility(
                      visible: infoCollateralChangeNotifier.isRadioValueWorthyOrUnworthyVisible(),
                      child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                  Visibility(
                    visible: infoCollateralChangeNotifier.isUsageCollateralOtoVisible(),
                    child: IgnorePointer(
                      ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                      child: TextFormField(
                          autovalidate: infoCollateralChangeNotifier.autoValidateAuto,
                          validator: (e) {
                            if (e.isEmpty && infoCollateralChangeNotifier.isUsageCollateralOtoMandatory()) {
                              return "Tidak boleh kosong";
                            } else {
                              return null;
                            }
                          },
                          enabled: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran ? false : infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto == 0 ? false : true,
                          readOnly: true,
                          // enabled: infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto == 0 ? false : true,
                          onTap: (){
                            infoCollateralChangeNotifier.formatting();
                            // infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto == 1 ?
                            infoCollateralChangeNotifier.searchUsageCollateral(context,1);
                            // : null;
                          },
                          controller: infoCollateralChangeNotifier.controllerUsageCollateralOto,
                          decoration: InputDecoration(
                              labelText: 'Tujuan Penggunaan Collateral',
                              labelStyle: TextStyle(color: Colors.black),
                              filled: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran ? true : infoCollateralChangeNotifier.radioValueIsCollateralSameWithUnitOto == 0 ? true : false,
                              fillColor: Colors.black12,
                              errorStyle: TextStyle(
                                color: Theme.of(context).errorColor, // or any other color
                              ),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: infoCollateralChangeNotifier.isUsageCollateralOtoChanges ? Colors.purple : Colors.grey)),
                              disabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: infoCollateralChangeNotifier.isUsageCollateralOtoChanges ? Colors.purple : Colors.grey)),
                              contentPadding: EdgeInsets.symmetric(horizontal: 10)
                          )
                      ),
                    ),
                  ),
                  Visibility(
                      visible: infoCollateralChangeNotifier.isUsageCollateralOtoVisible(),
                      child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                  Visibility(
                    visible: infoCollateralChangeNotifier.isRadioValueForAllUnitOtoVisible(),
                    child: Text("Untuk Semua Unit",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        letterSpacing: 0.15,
                        color: infoCollateralChangeNotifier.isRadioValueForAllUnitOtoChanges ? Colors.purple : Colors.black,
                      ),
                    ),
                  ),
                  Visibility(
                    visible: infoCollateralChangeNotifier.isRadioValueForAllUnitOtoVisible(),
                    child: IgnorePointer(
                      ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA || infoCollateralChangeNotifier.disableJenisPenawaran,
                      child: Row(
                        children: [
                          Row(
                            children: [
                              Radio(
                                  activeColor: primaryOrange,
                                  value: 0,
                                  groupValue: infoCollateralChangeNotifier.radioValueForAllUnitOto,
                                  onChanged: (value){
                                    infoCollateralChangeNotifier.radioValueForAllUnitOto = value;
                                  }
                              ),
                              Text("Ya")
                            ],
                          ),
                          SizedBox(height: MediaQuery.of(context).size.width / 27),
                          Row(
                            children: [
                              Radio(
                                  activeColor: primaryOrange,
                                  value: 1,
                                  groupValue: infoCollateralChangeNotifier.radioValueForAllUnitOto,
                                  onChanged: (value){
                                    infoCollateralChangeNotifier.radioValueForAllUnitOto = value;
                                  }
                              ),
                              Text("Tidak")
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              );
            },
          );
        }
    );
  }
}
