import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_type_installment_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class WidgetInfoCreditStructureStepping extends StatefulWidget {
  @override
  _WidgetInfoCreditStructureSteppingState createState() => _WidgetInfoCreditStructureSteppingState();
}

class _WidgetInfoCreditStructureSteppingState extends State<WidgetInfoCreditStructureStepping> {

  Future<void> _setPreference;

  @override
  void initState() {
    super.initState();
    // Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false).setValueTotalStepping(context);
    _setPreference = Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context, listen: false).setPreference(context);
    // Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false).getDataFromDashboard(context);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:
        Text("Struktur Kredit Stepping", style: TextStyle(color: Colors.black)),
        centerTitle: true,
        backgroundColor: myPrimaryColor,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: FutureBuilder(
        future: _setPreference,
          builder: (context,snapshot){
            return Consumer<InfoCreditStructureTypeInstallmentChangeNotifier>(
              builder: (context, value, child) {
                return Container(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height / 47,
                      horizontal: MediaQuery.of(context).size.width / 27),
                  child: Column(
                    children: [
                      Visibility(
                        visible: value.isTotalSteppingSelectedVisible(),
                        child: DropdownButtonFormField<String>(
                            autovalidate:
                            value.autoValidateDialogFloating,
                            validator: (e) {
                              if (e == null && value.isTotalSteppingSelectedMandatory()) {
                                return "Silahkan pilih angsuran ke";
                              } else {
                                return null;
                              }
                            },
                            value: value
                                .totalSteppingSelected,
                            onChanged: (data) {
                              value.totalSteppingSelected = data;
                              value.setListDataTotalStepping(data,context);
                            },
                            decoration: InputDecoration(
                              labelText: "Angsuran Ke",
                              border: OutlineInputBorder(),
                              contentPadding:
                              EdgeInsets.symmetric(horizontal: 10),
                            ),
                            items: value
                                .lisTotalStepping
                                .map((value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(
                                  value,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              );
                            }).toList()
                        ),
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 57),
                      Expanded(
                          child: ListView.builder(
                            itemBuilder: (context, index) {
                              return Card(
                                elevation: 3.3,
                                child: Container(
                                  padding: EdgeInsets.symmetric(
                                      vertical: MediaQuery.of(context).size.height / 47,
                                      horizontal: MediaQuery.of(context).size.width / 27),
                                  child: Column(
                                    children: [
                                      Row(
                                        children: [
                                          Expanded(child: Text("Agnsuran Ke"),flex: 5,),
                                          Text(" : "),
                                          Expanded(child: Text("${value.listInfoCreditStructureStepping[index].instalment}"),flex: 5)
                                        ],
                                      ),
                                      SizedBox(height: MediaQuery.of(context).size.height / 57),
                                      Visibility(
                                        visible: value.isPercentageVisible(),
                                        child: IgnorePointer(
                                          ignoring: value.isDisablePACIAAOSCONA,
                                          child: TextFormField(
                                              autovalidate: value.autoValidate,
                                              validator: (e) {
                                                if (e == null && value.isPercentageMandatory()) {
                                                  return "Silahkan pilih jenis angsuran";
                                                } else {
                                                  return null;
                                                }
                                              },
                                              inputFormatters: [
                                                WhitelistingTextInputFormatter.digitsOnly
                                              ],
                                              enabled: value.listInfoCreditStructureStepping[index].isEnable,
                                              keyboardType: TextInputType.number,
                                              controller: value.listInfoCreditStructureStepping[index].controllerPercentage,
                                              decoration: InputDecoration(
                                                  labelText: 'Presentase',
                                                  labelStyle: TextStyle(color: Colors.black),
                                                  border: OutlineInputBorder(
                                                      borderRadius: BorderRadius.circular(8)
                                                  )
                                              )
                                          ),
                                        ),
                                      ),
                                      SizedBox(height: MediaQuery.of(context).size.height / 57),
                                      Visibility(
                                        visible: value.isValueVisible(),
                                        child: IgnorePointer(
                                          ignoring: value.isDisablePACIAAOSCONA,
                                          child: TextFormField(
                                              autovalidate: value.autoValidate,
                                              validator: (e) {
                                                if (e == null && value.isValueMandatory()) {
                                                  return "Silahkan pilih jenis angsuran";
                                                } else {
                                                  return null;
                                                }
                                              },
                                              inputFormatters: [
                                                WhitelistingTextInputFormatter.digitsOnly
                                              ],
                                              enabled: value.listInfoCreditStructureStepping[index].isEnable,
                                              keyboardType: TextInputType.number,
                                              controller: value.listInfoCreditStructureStepping[index].controllerValue,
                                              decoration: InputDecoration(
                                                  labelText: 'Nilai',
                                                  labelStyle: TextStyle(color: Colors.black),
                                                  border: OutlineInputBorder(
                                                      borderRadius: BorderRadius.circular(8)
                                                  )
                                              )
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                            itemCount: value.listInfoCreditStructureStepping.length,
                          )
                      )
                    ],
                  ),
                );
              },
            );
          }
      ),
      bottomNavigationBar: BottomAppBar(
        elevation: 0.0,
        child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Consumer<InfoCreditStructureTypeInstallmentChangeNotifier>(
              builder: (context, infoCreditStructureTypeInstallmentChangeNotifier, _) {
                return RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(8.0)),
                    color: myPrimaryColor,
                    onPressed: () {
                      Navigator.pop(context);
//                        infoCreditStructureTypeInstallmentChangeNotifier.check(context);
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("DONE",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                                letterSpacing: 1.25))
                      ],
                    ));
              },
            )),
      ),
    );
  }
}
