import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/form_m_foto_model.dart';
import 'package:ad1ms2_dev/models/photo_type_model.dart';
import 'package:ad1ms2_dev/models/survey_photo_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/survey/result_survey_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class WidgetResultSurveyPhoto extends StatefulWidget {
  final int flag;
  final int index;
  final SurveyPhotoModel model;

  const WidgetResultSurveyPhoto({this.flag, this.index, this.model});

  @override
  _WidgetResultSurveyPhotoState createState() => _WidgetResultSurveyPhotoState();
}

class _WidgetResultSurveyPhotoState extends State<WidgetResultSurveyPhoto> {
  Future<void> _loadData;
  @override
  void initState() {
    super.initState();
    _loadData = Provider.of<ResultSurveyChangeNotifier>(context,listen: false).setValueEditSurveyPhoto(context, widget.model, widget.flag);
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          fontFamily: "NunitoSans",
          accentColor: myPrimaryColor,
          primaryColor: Colors.black,
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        appBar: AppBar(
          title: Text(
              "Tambah Survey Foto",
              style: TextStyle(color: Colors.black)
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(
              color: Colors.black
          ),
        ),
        body: FutureBuilder(
            future: _loadData,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
              return Consumer<ResultSurveyChangeNotifier>(
                builder: (context, value, child) {
                  return SingleChildScrollView(
                    padding: EdgeInsets.symmetric(
                        horizontal: MediaQuery.of(context).size.width / 27,
                        vertical: MediaQuery.of(context).size.height / 57),
                    child: Form(
                      key: value.keyFormSurveyPhoto,
                      child: Column(
                        children: [
                          Visibility(
                            // visible: value.isPhotoTypeVisible(),
                            child: AbsorbPointer(
                              absorbing: false,//  value.isPhotoTypeEnabled(),
                              child: DropdownButtonFormField<PhotoTypeModel>(
                                  autovalidate: value.autoValidateSurveyPhoto,
                                  validator: (e) {
                                    if (e == null ) {//&& value.isPhotoTypeMandatory()
                                      return "Silahkan pilih jenis foto";
                                    } else {
                                      return null;
                                    }
                                  },
                                  value: value.photoTypeSelected,
                                  onChanged: (data) {
                                    value.photoTypeSelected = data;
                                    value.clearListPhoto();
                                  },
                                  onTap: () {
                                    FocusManager.instance.primaryFocus.unfocus();
                                  },
                                  decoration: InputDecoration(
                                    labelText: "Jenis Foto",
                                    border: OutlineInputBorder(),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: value.isPhotoTypeChanges ? Colors.purple : Colors.black)
                                    ),
                                    disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: value.isPhotoTypeChanges ? Colors.purple : Colors.black)
                                    ),
                                    contentPadding:
                                    EdgeInsets.symmetric(horizontal: 10),
                                  ),
                                  items: value.listPhotoType.map((value) {
                                    return DropdownMenuItem<PhotoTypeModel>(
                                      value: value,
                                      child: Text(
                                        value.name,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    );
                                  }).toList()
                              ),
                            ),
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height / 47),
                          value.listPhoto.isEmpty
                              ?
                          RaisedButton(
                            onPressed: () {
                              value.addFile(context);
                            },
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                new BorderRadius.circular(8.0)),
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(Icons.camera_alt),
                                SizedBox(width:MediaQuery.of(context).size.width/47),
                                Text("ADD")
                              ],
                            ),
                            color: myPrimaryColor,
                          )
                              :
                          Row(
                            children: <Widget>[
                              Row(
                                children: _listWidgetPhoto(value.listPhoto),
                              ),
                              value.listPhoto.length < 4
                                  ? IconButton(
                                  onPressed: () {
                                    value.addFile(context);
                                  },
                                  icon: Icon(
                                    Icons.add_a_photo,
                                    color: myPrimaryColor,
                                    size: 33,
                                  ))
                                  : SizedBox(width: 0.0, height: 0.0)
                            ],
                          ),
                          value.validatePhoto && value.listPhoto.isEmpty ?
                          Padding(
                            padding: EdgeInsets.only(
                                left: MediaQuery.of(context).size.width / 37,
                                right: MediaQuery.of(context).size.width / 37,
                                bottom:
                                MediaQuery.of(context).size.height / 37),
                            child: Text(
                              "Tidak boleh kosong",
                              style:
                              TextStyle(fontSize: 12, color: Colors.red),
                            ),
                          )
                              :
                          SizedBox()
                        ],
                      ),
                    ),
                  );
                },
              );
            }
        ),
        bottomNavigationBar: BottomAppBar(
          elevation: 0.0,
          child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Consumer<ResultSurveyChangeNotifier>(
                builder: (context, _provider, _) {
                  return RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(8.0)),
                      color: myPrimaryColor,
                      onPressed: () {
                          _provider.checkSurveyPhoto(context, widget.flag, widget.index);
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(widget.flag == 0 ? "SAVE" : "UPDATE",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: 1.25))
                        ],
                      ));
                },
              )),
        ),
      ),
    );
  }

  List<Widget> _listWidgetPhoto(List<ImageFileModel> data) {
    List<Widget> _newListWidget = [];
    for (int i = 0; i < data.length; i++) {
      _newListWidget.add(Padding(
        padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width / 57,
            vertical: MediaQuery.of(context).size.height / 57),
        child: PopupMenuButton<String>(
            onSelected: (value) {
              Provider.of<ResultSurveyChangeNotifier>(context,listen: false).actionDetailPhoto(value, i, context);
            },
            child: ClipRRect(
              child: Image.file(data[i].imageFile,
                  width: 60,
                  height: 60,
                  fit: BoxFit.cover,
                  filterQuality: FilterQuality.medium),
              borderRadius: BorderRadius.circular(8.0),
            ),
            itemBuilder: (context) {
              return TitlePopUpMenuButton.choices.map((String choice) {
                return PopupMenuItem(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            }),
      ));
    }
    return _newListWidget;
  }
}
