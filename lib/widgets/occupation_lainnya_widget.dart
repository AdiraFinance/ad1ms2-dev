import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:ad1ms2_dev/shared/form_m_occupation_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class OccupationLainnyaWidget extends StatefulWidget {
  @override
  _OccupationLainnyaWidgetState createState() =>
      _OccupationLainnyaWidgetState();
}

class _OccupationLainnyaWidgetState extends State<OccupationLainnyaWidget> {
  @override
  Widget build(BuildContext context) {
    return Consumer<FormMOccupationChangeNotif>(
      builder: (context, formMChangeNotif, _) {
        return Column(
          children: [
           Visibility(
             visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isNamaPerusahaanLainnya(),
             child: TextFormField(
               autovalidate: formMChangeNotif.autoValidate,
               validator: (e) {
                 if (e.isEmpty && Provider.of<FormMOccupationChangeNotif>(context,listen: false).isNamaPerusahaanLainnyaMandatory()) {
                   return "Tidak boleh kosong";
                 } else {
                   return null;
                 }
               },
               controller: formMChangeNotif.controllerCompanyName,
               style: new TextStyle(color: Colors.black),
               decoration: InputDecoration(
                   labelText: "Nama Perusahaan",
                   border: OutlineInputBorder(
                       borderRadius: BorderRadius.circular(8)),
                   enabledBorder: OutlineInputBorder(
                       borderSide: BorderSide(color: formMChangeNotif.editJenisPerusahaanLainnya ? Colors.purple : Colors.grey)),
                   disabledBorder: OutlineInputBorder(
                       borderSide: BorderSide(color: formMChangeNotif.editJenisPerusahaanLainnya ? Colors.purple : Colors.grey)),
                   contentPadding: EdgeInsets.symmetric(horizontal: 10)
               ),
               // decoration: new InputDecoration(
               //     labelText: 'Nama Perusahaan',
               //     labelStyle: TextStyle(color: Colors.black),
               //     border: OutlineInputBorder(
               //         borderRadius: BorderRadius.circular(8))),
               keyboardType: TextInputType.text,
               textCapitalization: TextCapitalization.characters,
             ),
           ),
           Visibility(
               visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isNamaPerusahaanLainnya(),
               child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
           Visibility(
             visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isJenisPerusahaanLainnya(),
             child: DropdownButtonFormField<CompanyTypeModel>(
                 autovalidate: formMChangeNotif.autoValidate,
                 validator: (e) {
                   if (e == null && Provider.of<FormMOccupationChangeNotif>(context,listen: false).isJenisPerusahaanLainnyaMandatory()) {
                     return "Silahkan pilih jenis perusahaan";
                   } else {
                     return null;
                   }
                 },
                 value: formMChangeNotif.companyTypeModelSelected,
                 onChanged: (value) {
                   formMChangeNotif.companyTypeModelSelected = value;
                 },
                 onTap: () {
                   FocusManager.instance.primaryFocus.unfocus();
                 },
                 decoration: InputDecoration(
                     labelText: "Jenis Perusahaan",
                     border: OutlineInputBorder(
                         borderRadius: BorderRadius.circular(8)),
                     enabledBorder: OutlineInputBorder(
                         borderSide: BorderSide(color: formMChangeNotif.editJenisPerusahaanLainnya ? Colors.purple : Colors.grey)),
                     disabledBorder: OutlineInputBorder(
                         borderSide: BorderSide(color: formMChangeNotif.editJenisPerusahaanLainnya ? Colors.purple : Colors.grey)),
                     contentPadding: EdgeInsets.symmetric(horizontal: 10)
                 ),
                 // InputDecoration(
                 //   labelText: "Jenis Perusahaan",
                 //   border: OutlineInputBorder(),
                 //   contentPadding: EdgeInsets.symmetric(horizontal: 10),
                 // ),
                 items: formMChangeNotif.listCompanyType.map((value) {
                   return DropdownMenuItem<CompanyTypeModel>(
                     value: value,
                     child: Text(
                       value.desc,
                       overflow: TextOverflow.ellipsis,
                     ),
                   );
                 }).toList()),
           ),
           Visibility(
               visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isJenisPerusahaanLainnya(),
               child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
           Visibility(
             visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isSektorEkonomiLainnya(),
             child: TextFormField(
               autovalidate: formMChangeNotif.autoValidate,
               validator: (e) {
                 if (e.isEmpty && Provider.of<FormMOccupationChangeNotif>(context,listen: false).isSektorEkonomiLainnyaMandatory()) {
                   return "Tidak boleh kosong";
                 } else {
                   return null;
                 }
               },
               onTap: () {
                 FocusManager.instance.primaryFocus.unfocus();
                 formMChangeNotif.searchSectorEconomic(context);
               },
               enabled: formMChangeNotif.isDisablePACIAAOSCONA ? false : true,
               readOnly: true,
               controller: formMChangeNotif.controllerSectorEconomic,
               style: new TextStyle(color: Colors.black),
               decoration: InputDecoration(
                   labelText: "Sektor Ekonomi",
                   filled: formMChangeNotif.isDisablePACIAAOSCONA,
                   fillColor: Colors.black12,
                   border: OutlineInputBorder(
                       borderRadius: BorderRadius.circular(8)),
                   enabledBorder: OutlineInputBorder(
                       borderSide: BorderSide(color: formMChangeNotif.editSektorEkonomiLainnya ? Colors.purple : Colors.grey)),
                   disabledBorder: OutlineInputBorder(
                       borderSide: BorderSide(color: formMChangeNotif.editSektorEkonomiLainnya ? Colors.purple : Colors.grey)),
                   contentPadding: EdgeInsets.symmetric(horizontal: 10)
               ),
               // InputDecoration(
               //     labelText: 'Sektor Ekonomi',
               //     labelStyle: TextStyle(color: Colors.black),
               //     border: OutlineInputBorder(
               //         borderRadius: BorderRadius.circular(8))),
             ),
           ),
           Visibility(
               visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isSektorEkonomiLainnya(),
               child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
           Visibility(
             visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isLapanganUsahaLainnya(),
             child: TextFormField(
               autovalidate: formMChangeNotif.autoValidate,
               validator: (e) {
                 if (e.isEmpty && Provider.of<FormMOccupationChangeNotif>(context,listen: false).isLapanganUsahaLainnyaMandatory()) {
                   return "Tidak boleh kosong";
                 } else {
                   return null;
                 }
               },
               readOnly: true,
               onTap: () {
                 FocusManager.instance.primaryFocus.unfocus();
                 formMChangeNotif.searchBusinesField(context);
               },
               controller: formMChangeNotif.controllerBusinessField,
               style: new TextStyle(color: Colors.black),
               decoration: InputDecoration(
                   labelText: "Lapangan Usaha",
                   filled: formMChangeNotif.isDisablePACIAAOSCONA,
                   fillColor: Colors.black12,
                   border: OutlineInputBorder(
                       borderRadius: BorderRadius.circular(8)),
                   enabledBorder: OutlineInputBorder(
                       borderSide: BorderSide(color: formMChangeNotif.editLapanganUsahaLainnya ? Colors.purple : Colors.grey)),
                   disabledBorder: OutlineInputBorder(
                       borderSide: BorderSide(color: formMChangeNotif.editLapanganUsahaLainnya ? Colors.purple : Colors.grey)),
                   contentPadding: EdgeInsets.symmetric(horizontal: 10)
               ),
               // InputDecoration(
               //     labelText: 'Lapangan Usaha',
               //     labelStyle: TextStyle(color: Colors.black),
               //     border: OutlineInputBorder(
               //         borderRadius: BorderRadius.circular(8))),
               enabled: formMChangeNotif.isDisablePACIAAOSCONA ? false : formMChangeNotif.controllerSectorEconomic.text == "" ? false : true,
             ),
           ),
           Visibility(
               visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isLapanganUsahaLainnya(),
               child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
           Visibility(
             visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isTotalPegawaiLainnya(),
             child: TextFormField(
               autovalidate: formMChangeNotif.autoValidate,
               validator: (e) {
                 if (e.isEmpty && Provider.of<FormMOccupationChangeNotif>(context,listen: false).isTotalPegawaiLainnyaMandatory()) {
                   return "Tidak boleh kosong";
                 } else {
                   return null;
                 }
               },
               inputFormatters: [
                 WhitelistingTextInputFormatter.digitsOnly,
                 // LengthLimitingTextInputFormatter(10),
               ],
               controller: formMChangeNotif.controllerEmployeeTotal,
               style: new TextStyle(color: Colors.black),
               decoration: InputDecoration(
                   labelText: "Total Pegawai",
                   border: OutlineInputBorder(
                       borderRadius: BorderRadius.circular(8)),
                   enabledBorder: OutlineInputBorder(
                       borderSide: BorderSide(color: formMChangeNotif.editTotalPegawaiLainnya ? Colors.purple : Colors.grey)),
                   disabledBorder: OutlineInputBorder(
                       borderSide: BorderSide(color: formMChangeNotif.editTotalPegawaiLainnya ? Colors.purple : Colors.grey)),
                   contentPadding: EdgeInsets.symmetric(horizontal: 10)
               ),
               // InputDecoration(
               //     labelText: 'Total Pegawai',
               //     labelStyle: TextStyle(color: Colors.black),
               //     border: OutlineInputBorder(
               //         borderRadius: BorderRadius.circular(8))),
               keyboardType: TextInputType.number,
             ),
           ),
           Visibility(
               visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isTotalPegawaiLainnya(),
               child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
            Visibility(
              visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isTotalLamaBekerjaLainnya(),
              child: TextFormField(
                autovalidate: formMChangeNotif.autoValidate,
                validator: (e) {
                  if (e.isEmpty && Provider.of<FormMOccupationChangeNotif>(context,listen: false).isTotalLamaBekerjaLainnyaMandatory()) {
                    return "Tidak boleh kosong";
                  } else {
                    return null;
                  }
                },
                inputFormatters: [
                  WhitelistingTextInputFormatter.digitsOnly,
                  // LengthLimitingTextInputFormatter(10),
                ],
                enabled: formMChangeNotif.isDisablePACIAAOSCONA ? false : true,
                controller: formMChangeNotif.controllerTotalEstablishedDate,
                style: new TextStyle(color: Colors.black),
                decoration: InputDecoration(
                    labelText: "Total Lama Usaha/Kerja (bln)",
                    filled: formMChangeNotif.isDisablePACIAAOSCONA,
                    fillColor: Colors.black12,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: formMChangeNotif.editTotalLamaBekerjaLainnya ? Colors.purple : Colors.grey)),
                    disabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: formMChangeNotif.editTotalLamaBekerjaLainnya ? Colors.purple : Colors.grey)),
                    contentPadding: EdgeInsets.symmetric(horizontal: 10)
                ),
                // InputDecoration(
                //     labelText: 'Total Lama Usaha/Kerja (bln)',
                //     labelStyle: TextStyle(color: Colors.black),
                //     border: OutlineInputBorder(
                //         borderRadius: BorderRadius.circular(8))),
                keyboardType: TextInputType.number,
              ),
            ),
            Visibility(
                visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isTotalLamaBekerjaLainnya(),
                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
            Visibility(
              visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isJenisPepHighRiskLainnya(),
              child: TextFormField(
                autovalidate: formMChangeNotif.autoValidate,
                validator: (e) {
                  if (e.isEmpty && Provider.of<FormMOccupationChangeNotif>(context,listen: false).isJenisPepHighRiskLainnyaMandatory()) {
                    return "Tidak boleh kosong";
                  } else {
                    return null;
                  }
                },
                readOnly: true,
                onTap: () {
                  FocusManager.instance.primaryFocus.unfocus();
                  formMChangeNotif.searchPEP(context);
                },
                controller: formMChangeNotif.controllerJenisPEP,
                style: new TextStyle(color: Colors.black),
                decoration: InputDecoration(
                    labelText: "Jenis PEP & High Risk Cust",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: formMChangeNotif.editJenisPepHighRiskLainnya ? Colors.purple : Colors.grey)),
                    disabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: formMChangeNotif.editJenisPepHighRiskLainnya ? Colors.purple : Colors.grey)),
                    contentPadding: EdgeInsets.symmetric(horizontal: 10)
                ),
                // InputDecoration(
                //     labelText: 'Jenis PEP & High Risk Cust',
                //     labelStyle: TextStyle(color: Colors.black),
                //     border: OutlineInputBorder(
                //         borderRadius: BorderRadius.circular(8))),
              ),
            ),
            Visibility(
                visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isJenisPepHighRiskLainnya(),
                child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
            Visibility(
              visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isStatusPegawaiLainnya(),
              child: IgnorePointer(
                ignoring: formMChangeNotif.isDisablePACIAAOSCONA,
                child: DropdownButtonFormField<EmployeeStatusModel>(
                  autovalidate: formMChangeNotif.autoValidate,
                  validator: (e) {
                    if (e == null && Provider.of<FormMOccupationChangeNotif>(context,listen: false).isStatusPegawaiLainnyaMandatory()) {
                      return "Silahkan pilih status pegawai";
                    } else {
                      return null;
                    }
                  },
                  value: formMChangeNotif.employeeStatusModelSelected,
                  onChanged: (value) {
                    formMChangeNotif.employeeStatusModelSelected = value;
                  },
                  onTap: () {
                    FocusManager.instance.primaryFocus.unfocus();
                  },
                  decoration: InputDecoration(
                      labelText: "Status Pegawai",
                      filled: formMChangeNotif.isDisablePACIAAOSCONA,
                      fillColor: Colors.black12,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMChangeNotif.editStatusPegawaiLainnya ? Colors.purple : Colors.grey)),
                      disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: formMChangeNotif.editStatusPegawaiLainnya ? Colors.purple : Colors.grey)),
                      contentPadding: EdgeInsets.symmetric(horizontal: 10)
                  ),
                  // InputDecoration(
                  //   labelText: "Status Pegawai",
                  //   border: OutlineInputBorder(),
                  //   contentPadding: EdgeInsets.symmetric(horizontal: 10),
                  // ),
                  items: formMChangeNotif.listEmployeeStatus.map((value) {
                    return DropdownMenuItem<EmployeeStatusModel>(
                      value: value,
                      child: Text(
                        value.desc,
                        overflow: TextOverflow.ellipsis,
                      ),
                    );
                  }).toList()
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}
