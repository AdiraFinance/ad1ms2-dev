import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/screens/map_page.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_individu_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../main.dart';

class WidgetAddressIndividu extends StatefulWidget {
    final int flag;
    final int index;
    final AddressModel addressModel;
    final editVal;
    final int typeAddress;

    const WidgetAddressIndividu({Key key, this.flag, this.index, this.addressModel, this.editVal, this.typeAddress});

    @override
    _WidgetAddressIndividuState createState() => _WidgetAddressIndividuState();
}

class _WidgetAddressIndividuState extends State<WidgetAddressIndividu> {
    @override
    void initState() {
      super.initState();
      Provider.of<FormMAddAddressIndividuChangeNotifier>(context, listen: false).setPreference();
    }

    @override
    Widget build(BuildContext context) {
    return Theme(
        data: ThemeData(
            primaryColor: Colors.black,
            accentColor: myPrimaryColor,
            fontFamily: "NunitoSans",
            primarySwatch: primaryOrange
        ),
      child: Scaffold(
          appBar: AppBar(
              title: Text(
                  widget.flag == 0
                      ? "Tambah Alamat"
                      : "Edit Alamat",
                  style: TextStyle(color: Colors.black)),
              centerTitle: true,
              backgroundColor: myPrimaryColor,
              iconTheme: IconThemeData(color: Colors.black),
          ),
          body:
//          widget.flag == 0
//              ? Consumer<FormMAddAddressIndividuChangeNotifier>(
//              builder: (context, formMAddAlamatKorespon, _) {
//                  return SingleChildScrollView(
//                      padding: EdgeInsets.symmetric(
//                          horizontal: MediaQuery.of(context).size.width / 27,
//                          vertical: MediaQuery.of(context).size.height / 57),
//                      child: Form(
//                          key: formMAddAlamatKorespon.key,
//                          onWillPop: _onWillPop,
//                          child: Column(
//                              crossAxisAlignment: CrossAxisAlignment.start,
//                              children: [
//                                  DropdownButtonFormField<JenisAlamatModel>(
//                                      autovalidate: formMAddAlamatKorespon.autoValidate,
//                                      validator: (e) {
//                                          if (e == null) {
//                                              return "Silahkan pilih jenis alamat";
//                                          } else {
//                                              return null;
//                                          }
//                                      },
//                                      value: formMAddAlamatKorespon.jenisAlamatSelected,
//                                      onChanged: (value) {
//                                          formMAddAlamatKorespon.jenisAlamatSelected = value;
//                                      },
//                                      decoration: InputDecoration(
//                                          labelText: "Jenis Alamat",
//                                          border: OutlineInputBorder(),
//                                          contentPadding:
//                                          EdgeInsets.symmetric(horizontal: 10),
//                                      ),
//                                      items: formMAddAlamatKorespon
//                                          .listJenisAlamat
//                                          .map((value) {
//                                          return DropdownMenuItem<JenisAlamatModel>(
//                                              value: value,
//                                              child: Text(
//                                                  value.DESKRIPSI,
//                                                  overflow: TextOverflow.ellipsis,
//                                              ),
//                                          );
//                                      }).toList()),
////                                formMAddAlamatKorespon.jenisAlamatSelected != null
////                                    ? formMAddAlamatKorespon.jenisAlamatSelected.KODE == "01"
////                                    ? Row(
////                                    children: [
////                                        Checkbox(
////                                            value: formMAddAlamatKorespon.isSameWithIdentity,
////                                            onChanged: (value) {
////                                                formMAddAlamatKorespon.isSameWithIdentity = value;
////                                                formMAddAlamatKorespon.setValueIsSameWithIdentity(value, context, null);
////                                            },
////                                            activeColor: myPrimaryColor),
////                                        SizedBox(width: 8),
////                                        Text("Sama dengan identitas")
////                                    ],
////                                )
////                                    : SizedBox(height: MediaQuery.of(context).size.height / 47)
////                                    : SizedBox(height: MediaQuery.of(context).size.height / 47),
//                                  SizedBox(height: MediaQuery.of(context).size.height / 47),
//                                  TextFormField(
//                                      autovalidate:
//                                      formMAddAlamatKorespon.autoValidate,
//                                      validator: (e) {
//                                          if (e.isEmpty) {
//                                              return "Tidak boleh kosong";
//                                          } else {
//                                              return null;
//                                          }
//                                      },
//                                      controller:
//                                      formMAddAlamatKorespon.controllerAlamat,
//                                      style: TextStyle(color: Colors.black),
//                                      decoration: InputDecoration(
//                                          labelText: 'Alamat',
//                                          labelStyle:
//                                          TextStyle(color: Colors.black),
//                                          border: OutlineInputBorder(
//                                              borderRadius:
//                                              BorderRadius.circular(8))),
//                                      maxLines: 3,
//                                      keyboardType: TextInputType.text,
//                                      textCapitalization: TextCapitalization.characters,
//                                  ),
//                                  SizedBox(
//                                      height: MediaQuery.of(context).size.height /
//                                          47),
//                                  Row(
//                                      children: [
//                                          Expanded(
//                                              flex: 5,
//                                              child: TextFormField(
//                                                  autovalidate:
//                                                  formMAddAlamatKorespon.autoValidate,
//                                                  validator: (e) {
//                                                      if (e.isEmpty) {
//                                                          return "Tidak boleh kosong";
//                                                      } else {
//                                                          return null;
//                                                      }
//                                                  },
//                                                  inputFormatters: [
//                                                      WhitelistingTextInputFormatter.digitsOnly,
//                                                      // LengthLimitingTextInputFormatter(10),
//                                                  ],
//                                                  controller:
//                                                  formMAddAlamatKorespon.controllerRT,
//                                                  style: TextStyle(color: Colors.black),
//                                                  decoration: InputDecoration(
//                                                      labelText: 'RT',
//                                                      labelStyle:
//                                                      TextStyle(color: Colors.black),
//                                                      border: OutlineInputBorder(
//                                                          borderRadius:
//                                                          BorderRadius.circular(8))),
//                                                  keyboardType: TextInputType.number,
//                                              ),
//                                          ),
//                                          SizedBox(width: 8),
//                                          Expanded(
//                                              flex: 5,
//                                              child: TextFormField(
//                                                  autovalidate:
//                                                  formMAddAlamatKorespon.autoValidate,
//                                                  validator: (e) {
//                                                      if (e.isEmpty) {
//                                                          return "Tidak boleh kosong";
//                                                      } else {
//                                                          return null;
//                                                      }
//                                                  },
//                                                  inputFormatters: [
//                                                      WhitelistingTextInputFormatter.digitsOnly,
//                                                      // LengthLimitingTextInputFormatter(10),
//                                                  ],
//                                                  controller:
//                                                  formMAddAlamatKorespon.controllerRW,
//                                                  style: TextStyle(color: Colors.black),
//                                                  decoration: InputDecoration(
//                                                      labelText: 'RW',
//                                                      labelStyle:
//                                                      TextStyle(color: Colors.black),
//                                                      border: OutlineInputBorder(
//                                                          borderRadius:
//                                                          BorderRadius.circular(8))),
//                                                  keyboardType: TextInputType.number,
//                                              ),
//                                          )
//                                      ],
//                                  ),
//                                  SizedBox(
//                                      height: MediaQuery.of(context).size.height /
//                                          47),
//                                  FocusScope(
//                                      node: FocusScopeNode(),
//                                      child: TextFormField(
//                                          autovalidate:
//                                          formMAddAlamatKorespon.autoValidate,
//                                          validator: (e) {
//                                              if (e.isEmpty) {
//                                                  return "Tidak boleh kosong";
//                                              } else {
//                                                  return null;
//                                              }
//                                          },
//                                          onTap: () {
//                                              formMAddAlamatKorespon
//                                                  .searchKelurahan(context);
//                                          },
//                                          controller: formMAddAlamatKorespon
//                                              .controllerKelurahan,
//                                          readOnly: true,
//                                          style: new TextStyle(color: Colors.black),
//                                          decoration: new InputDecoration(
//                                              labelText: 'Kelurahan',
//                                              labelStyle:
//                                              TextStyle(color: Colors.black),
//                                              border: OutlineInputBorder(
//                                                  borderRadius:
//                                                  BorderRadius.circular(8))),
//                                          keyboardType: TextInputType.text,
//                                          textCapitalization: TextCapitalization.characters,
//                                      ),
//                                  ),
//                                  SizedBox(
//                                      height: MediaQuery.of(context).size.height /
//                                          47),
//                                  TextFormField(
//                                      autovalidate:
//                                      formMAddAlamatKorespon.autoValidate,
//                                      validator: (e) {
//                                          if (e.isEmpty) {
//                                              return "Tidak boleh kosong";
//                                          } else {
//                                              return null;
//                                          }
//                                      },
//                                      enabled: false,
//                                      controller: formMAddAlamatKorespon
//                                          .controllerKecamatan,
//                                      style: new TextStyle(color: Colors.black),
//                                      decoration: new InputDecoration(
//                                          labelText: 'Kecamatan',
//                                          labelStyle:
//                                          TextStyle(color: Colors.black),
//                                          border: OutlineInputBorder(
//                                              borderRadius:
//                                              BorderRadius.circular(8))),
//                                      keyboardType: TextInputType.text,
//                                      textCapitalization: TextCapitalization.characters,
//                                  ),
//                                  SizedBox(
//                                      height: MediaQuery.of(context).size.height /
//                                          47),
//                                  TextFormField(
//                                      autovalidate:
//                                      formMAddAlamatKorespon.autoValidate,
//                                      validator: (e) {
//                                          if (e.isEmpty) {
//                                              return "Tidak boleh kosong";
//                                          } else {
//                                              return null;
//                                          }
//                                      },
//                                      enabled: false,
//                                      controller:
//                                      formMAddAlamatKorespon.controllerKota,
//                                      style: new TextStyle(color: Colors.black),
//                                      decoration: new InputDecoration(
//                                          labelText: 'Kabupaten/Kota',
//                                          labelStyle:
//                                          TextStyle(color: Colors.black),
//                                          border: OutlineInputBorder(
//                                              borderRadius:
//                                              BorderRadius.circular(8))),
//                                      keyboardType: TextInputType.text,
//                                      textCapitalization: TextCapitalization.characters,
//                                  ),
//                                  SizedBox(
//                                      height: MediaQuery.of(context).size.height /
//                                          47),
//                                  Row(
//                                      children: [
//                                          Expanded(
//                                              flex: 7,
//                                              child: TextFormField(
//                                                  autovalidate:
//                                                  formMAddAlamatKorespon.autoValidate,
//                                                  validator: (e) {
//                                                      if (e.isEmpty) {
//                                                          return "Tidak boleh kosong";
//                                                      } else {
//                                                          return null;
//                                                      }
//                                                  },
//                                                  enabled: false,
//                                                  controller: formMAddAlamatKorespon
//                                                      .controllerProvinsi,
//                                                  style:
//                                                  new TextStyle(color: Colors.black),
//                                                  decoration: new InputDecoration(
//                                                      labelText: 'Provinsi',
//                                                      labelStyle:
//                                                      TextStyle(color: Colors.black),
//                                                      border: OutlineInputBorder(
//                                                          borderRadius:
//                                                          BorderRadius.circular(8))),
//                                                  keyboardType: TextInputType.text,
//                                                  textCapitalization: TextCapitalization.characters,
//                                              ),
//                                          ),
//                                          SizedBox(width: 8),
//                                          Expanded(
//                                              flex: 3,
//                                              child: TextFormField(
//                                                  autovalidate:
//                                                  formMAddAlamatKorespon.autoValidate,
//                                                  validator: (e) {
//                                                      if (e.isEmpty) {
//                                                          return "Tidak boleh kosong";
//                                                      } else {
//                                                          return null;
//                                                      }
//                                                  },
//                                                  enabled: false,
//                                                  controller: formMAddAlamatKorespon
//                                                      .controllerPostalCode,
//                                                  style:
//                                                  new TextStyle(color: Colors.black),
//                                                  decoration: new InputDecoration(
//                                                      labelText: 'Kode Pos',
//                                                      labelStyle:
//                                                      TextStyle(color: Colors.black),
//                                                      border: OutlineInputBorder(
//                                                          borderRadius:
//                                                          BorderRadius.circular(8))),
//                                                  keyboardType: TextInputType.number,
//                                              ),
//                                          ),
//                                      ],
//                                  ),
//                                  SizedBox(
//                                      height: MediaQuery.of(context).size.height /
//                                          47),
//                                  TextFormField(
//                                      // autovalidate:
//                                      // formMAddAlamatKorespon.autoValidate,
//                                      // validator: (e) {
//                                      //     if (e.isEmpty) {
//                                      //         return "Tidak boleh kosong";
//                                      //     } else {
//                                      //         return null;
//                                      //     }
//                                      // },
//                                      readOnly: true,
//                                      onTap: (){
//                                          formMAddAlamatKorespon.setLocationAddressByMap(context);
//                                      },
//                                      maxLines: 3,
//                                      controller: formMAddAlamatKorespon
//                                          .controllerAddressFromMap,
//                                      style: TextStyle(color: Colors.black),
//                                      decoration: new InputDecoration(
//                                          labelText: 'Alamat Lokasi',
//                                          labelStyle:
//                                          TextStyle(color: Colors.black),
//                                          border: OutlineInputBorder(
//                                              borderRadius:
//                                              BorderRadius.circular(8))),
//                                      keyboardType: TextInputType.number,
//                                  ),
//                                  SizedBox(
//                                      height: MediaQuery.of(context).size.height /
//                                          47),
//                                  Row(
//                                      children: [
//                                          Expanded(
//                                              flex: 4,
//                                              child: TextFormField(
//                                                  autovalidate:
//                                                  formMAddAlamatKorespon.autoValidate,
//                                                  validator: (e) {
//                                                      if (e.isEmpty) {
//                                                          return "Tidak boleh kosong";
//                                                      } else {
//                                                          return null;
//                                                      }
//                                                  },
//                                                  onChanged: (e) {
//                                                      formMAddAlamatKorespon
//                                                          .checkValidCOdeArea(e);
//                                                  },
//                                                  inputFormatters: [
//                                                      WhitelistingTextInputFormatter
//                                                          .digitsOnly,
//                                                      LengthLimitingTextInputFormatter(10),
//                                                  ],
//                                                  controller: formMAddAlamatKorespon
//                                                      .controllerKodeArea,
//                                                  style:
//                                                  new TextStyle(color: Colors.black),
//                                                  decoration: new InputDecoration(
//                                                      labelText: 'Kode Area',
//                                                      labelStyle:
//                                                      TextStyle(color: Colors.black),
//                                                      border: OutlineInputBorder(
//                                                          borderRadius:
//                                                          BorderRadius.circular(8))),
//                                                  keyboardType: TextInputType.number,
//                                              ),
//                                          ),
//                                          SizedBox(
//                                              width:
//                                              MediaQuery.of(context).size.width /
//                                                  37),
//                                          Expanded(
//                                              flex: 6,
//                                              child: TextFormField(
//                                                  autovalidate:
//                                                  formMAddAlamatKorespon.autoValidate,
//                                                  validator: (e) {
//                                                      if (e.isEmpty) {
//                                                          return "Tidak boleh kosong";
//                                                      } else {
//                                                          return null;
//                                                      }
//                                                  },
//                                                  inputFormatters: [
//                                                      WhitelistingTextInputFormatter
//                                                          .digitsOnly,
//                                                      LengthLimitingTextInputFormatter(10),
//                                                  ],
//                                                  controller: formMAddAlamatKorespon
//                                                      .controllerTlpn,
//                                                  style:
//                                                  new TextStyle(color: Colors.black),
//                                                  decoration: new InputDecoration(
//                                                      labelText: 'Telepon',
//                                                      labelStyle:
//                                                      TextStyle(color: Colors.black),
//                                                      border: OutlineInputBorder(
//                                                          borderRadius:
//                                                          BorderRadius.circular(8))),
//                                                  keyboardType: TextInputType.number,
//                                              ),
//                                          ),
//                                      ],
//                                  ),
//                              ],
//                          ),
//                      ),
//                  );
//              },
//          )
//              :
          FutureBuilder(
              future: widget.editVal,
              builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                      return Center(
                          child: CircularProgressIndicator(),
                      );
                  }
                  return Consumer<FormMAddAddressIndividuChangeNotifier>(
                      builder: (context, formMAddAlamatKorespon, _) {
                          return SingleChildScrollView(
                              padding: EdgeInsets.symmetric(
                                  horizontal: MediaQuery.of(context).size.width / 27,
                                  vertical: MediaQuery.of(context).size.height / 57),
                              child: Form(
                                  key: formMAddAlamatKorespon.key,
                                  onWillPop: _onWillPop,
                                  child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                          Visibility(
                                            visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressTypeShow(widget.typeAddress),
                                              child: IgnorePointer(
                                                ignoring: (formMAddAlamatKorespon.isDisablePACIAAOSCONA || formMAddAlamatKorespon.disableJenisPenawaran) && widget.flag == 1 && widget.typeAddress == 1 ? true : false,
                                                child: DropdownButtonFormField<JenisAlamatModel>(
                                                  autovalidate: formMAddAlamatKorespon.autoValidate,
                                                  validator: (e) {
                                                      if (e == null && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressTypeMan(widget.typeAddress)) {
                                                          return "Silahkan pilih jenis alamat";
                                                      } else {
                                                          return null;
                                                      }
                                                  },
                                                  value: formMAddAlamatKorespon.jenisAlamatSelected,
                                                  onChanged: (value) {
                                                      formMAddAlamatKorespon.jenisAlamatSelected = value;
                                                  },
                                                  decoration: InputDecoration(
                                                      labelText: "Jenis Alamat",
                                                      filled: (formMAddAlamatKorespon.isDisablePACIAAOSCONA || formMAddAlamatKorespon.disableJenisPenawaran) && widget.flag == 1 && widget.typeAddress == 1 ? true : false,
                                                      fillColor: Colors.black12,
                                                      border: OutlineInputBorder(),
                                                      enabledBorder: OutlineInputBorder(
                                                          borderSide: BorderSide(color: formMAddAlamatKorespon.isAddressTypeChanges ? Colors.purple : Colors.grey)),
                                                      disabledBorder: OutlineInputBorder(
                                                          borderSide: BorderSide(color: formMAddAlamatKorespon.isAddressTypeChanges ? Colors.purple : Colors.grey)),
                                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                  ),
                                                  items: formMAddAlamatKorespon.listJenisAlamat.map((value) {
                                                      return DropdownMenuItem<JenisAlamatModel>(
                                                          value: value,
                                                          child: Text(
                                                              value.DESKRIPSI,
                                                              overflow: TextOverflow.ellipsis,
                                                          ),
                                                      );
                                                  }).toList()),
                                              ),
                                          ),
//                                        formMAddAlamatKorespon.jenisAlamatSelected != null
//                                            ? formMAddAlamatKorespon.jenisAlamatSelected.KODE == "01"
//                                            ? Row(
//                                            children: [
//                                                Checkbox(
//                                                    value: formMAddAlamatKorespon.isSameWithIdentity,
//                                                    onChanged: (value) {
//                                                        formMAddAlamatKorespon.isSameWithIdentity = value;
//                                                        formMAddAlamatKorespon.setValueIsSameWithIdentity(value, context, null);
//                                                    },
//                                                    activeColor: myPrimaryColor),
//                                                SizedBox(width: 8),
//                                                Text("Sama dengan identitas")
//                                            ],
//                                        )
//                                            : SizedBox(height: MediaQuery.of(context).size.height / 47)
//                                            : SizedBox(height: MediaQuery.of(context).size.height / 47),
                                          Visibility(
                                              child: SizedBox(height: MediaQuery.of(context).size.height / 47),
                                              visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressTypeShow(widget.typeAddress),
                                          ),
                                          Visibility(
                                            visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressShow(widget.typeAddress),
                                              child: TextFormField(
                                                autovalidate: formMAddAlamatKorespon.autoValidate,
                                                validator: (e) {
                                                    if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressMan(widget.typeAddress)) {
                                                        return "Tidak boleh kosong";
                                                    } else {
                                                        return null;
                                                    }
                                                },
                                                enabled: (formMAddAlamatKorespon.isDisablePACIAAOSCONA || formMAddAlamatKorespon.disableJenisPenawaran) && widget.flag == 1 && widget.typeAddress == 1 ? false : true,
                                                controller: formMAddAlamatKorespon.controllerAlamat,
                                                style: TextStyle(color: Colors.black),
                                                decoration: InputDecoration(
                                                    labelText: 'Alamat',
                                                    labelStyle: TextStyle(color: Colors.black),
                                                    filled: (formMAddAlamatKorespon.isDisablePACIAAOSCONA || formMAddAlamatKorespon.disableJenisPenawaran) && widget.flag == 1 && widget.typeAddress == 1 ? true : false,
                                                    fillColor: Colors.black12,
                                                    border: OutlineInputBorder(
                                                        borderRadius: BorderRadius.circular(8)),
                                                    enabledBorder: OutlineInputBorder(
                                                        borderSide: BorderSide(color: formMAddAlamatKorespon.isAlamatChanges ? Colors.purple : Colors.grey)),
                                                    disabledBorder: OutlineInputBorder(
                                                        borderSide: BorderSide(color: formMAddAlamatKorespon.isAlamatChanges ? Colors.purple : Colors.grey)),
                                                ),
                                                maxLines: 3,
                                                keyboardType: TextInputType.text,
                                                textCapitalization: TextCapitalization.characters,
                                                inputFormatters: [
                                                  FilteringTextInputFormatter.allow(RegExp('[a-zA-Z  \`0-9./]')),
                                                ],
                                            ),
                                          ),
                                          Visibility(
                                              child: SizedBox(height: MediaQuery.of(context).size.height / 47),
                                              visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressShow(widget.typeAddress)
                                          ),
                                          Row(
                                              children: [
                                                  Visibility(
                                                      visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRTShow(widget.typeAddress),
                                                      child: Expanded(
                                                        flex: 5,
                                                        child: IgnorePointer(
                                                          ignoring: formMAddAlamatKorespon.disableJenisPenawaran && widget.flag == 1 && widget.typeAddress == 1 ? true : false,
                                                          child: TextFormField(
                                                              autovalidate: formMAddAlamatKorespon.autoValidate,
                                                              validator: (e) {
                                                                  if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRTMan(widget.typeAddress)) {
                                                                      return "Tidak boleh kosong";
                                                                  } else {
                                                                      return null;
                                                                  }
                                                              },
                                                              inputFormatters: [
                                                                  WhitelistingTextInputFormatter.digitsOnly,
                                                                  LengthLimitingTextInputFormatter(3),
                                                              ],
                                                              enabled: (formMAddAlamatKorespon.isDisablePACIAAOSCONA || formMAddAlamatKorespon.disableJenisPenawaran) && widget.flag == 1 && widget.typeAddress == 1 ? false : true,
                                                              controller: formMAddAlamatKorespon.controllerRT,
                                                              style: TextStyle(color: Colors.black),
                                                              decoration: InputDecoration(
                                                                  labelText: 'RT',
                                                                  labelStyle: TextStyle(color: Colors.black),
                                                                  filled: (formMAddAlamatKorespon.isDisablePACIAAOSCONA || formMAddAlamatKorespon.disableJenisPenawaran) && widget.flag == 1 && widget.typeAddress == 1 ? true : false,
                                                                  fillColor: Colors.black12,
                                                                  border: OutlineInputBorder(
                                                                      borderRadius: BorderRadius.circular(8)),
                                                                  enabledBorder: OutlineInputBorder(
                                                                      borderSide: BorderSide(color: formMAddAlamatKorespon.isRTChanges ? Colors.purple : Colors.grey)),
                                                                  disabledBorder: OutlineInputBorder(
                                                                      borderSide: BorderSide(color: formMAddAlamatKorespon.isRTChanges ? Colors.purple : Colors.grey)),
                                                              ),
                                                              keyboardType: TextInputType.number,
                                                          ),
                                                        ),
                                                    ),
                                                  ),
                                                  SizedBox(width: 8),
                                                  Expanded(
                                                      flex: 5,
                                                      child: IgnorePointer(
                                                        ignoring: formMAddAlamatKorespon.disableJenisPenawaran && widget.flag == 1 && widget.typeAddress == 1 ? true : false,
                                                        child: Visibility(
                                                            visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRWShow(widget.typeAddress),
                                                            child: TextFormField(
                                                              autovalidate: formMAddAlamatKorespon.autoValidate,
                                                              validator: (e) {
                                                                  if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRWMan(widget.typeAddress)) {
                                                                      return "Tidak boleh kosong";
                                                                  } else {
                                                                      return null;
                                                                  }
                                                              },
                                                              inputFormatters: [
                                                                  WhitelistingTextInputFormatter.digitsOnly,
                                                                  LengthLimitingTextInputFormatter(3),
                                                              ],
                                                              enabled: (formMAddAlamatKorespon.isDisablePACIAAOSCONA || formMAddAlamatKorespon.disableJenisPenawaran) && widget.flag == 1 && widget.typeAddress == 1 ? false : true,
                                                              controller: formMAddAlamatKorespon.controllerRW,
                                                              style: TextStyle(color: Colors.black),
                                                              decoration: InputDecoration(
                                                                  labelText: 'RW',
                                                                  labelStyle: TextStyle(color: Colors.black),
                                                                  filled: (formMAddAlamatKorespon.isDisablePACIAAOSCONA || formMAddAlamatKorespon.disableJenisPenawaran) && widget.flag == 1 && widget.typeAddress == 1 ? true : false,
                                                                  fillColor: Colors.black12,
                                                                  border: OutlineInputBorder(
                                                                      borderRadius:
                                                                      BorderRadius.circular(8)),
                                                                  enabledBorder: OutlineInputBorder(
                                                                      borderSide: BorderSide(color: formMAddAlamatKorespon.isRWChanges ? Colors.purple : Colors.grey)),
                                                                  disabledBorder: OutlineInputBorder(
                                                                      borderSide: BorderSide(color: formMAddAlamatKorespon.isRWChanges ? Colors.purple : Colors.grey)),
                                                              ),
                                                              keyboardType: TextInputType.number,
                                                          ),
                                                        ),
                                                      ),
                                                  )
                                              ],
                                          ),
                                          Visibility(
                                              child: SizedBox(height: MediaQuery.of(context).size.height / 47),
                                              visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRWShow(widget.typeAddress)
                                                  && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRTShow(widget.typeAddress),
                                          ),
                                          Visibility(
                                              visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKelurahanShow(widget.typeAddress),
                                              child: IgnorePointer(
                                                ignoring: formMAddAlamatKorespon.disableJenisPenawaran && widget.flag == 1 && widget.typeAddress == 1 ? true : false,
                                                child: TextFormField(
                                                  autovalidate: formMAddAlamatKorespon.autoValidate,
                                                  validator: (e) {
                                                      if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKelurahanMan(widget.typeAddress)) {
                                                          return "Tidak boleh kosong";
                                                      } else {
                                                          return null;
                                                      }
                                                  },
                                                  onTap: () {
                                                      formMAddAlamatKorespon.searchKelurahan(context);
                                                  },
                                                  controller: formMAddAlamatKorespon.controllerKelurahan,
                                                  enabled: (formMAddAlamatKorespon.isDisablePACIAAOSCONA) && widget.flag == 1 && widget.typeAddress == 1 ? false : (formMAddAlamatKorespon.isDisablePACIAAOSCONA) && widget.flag == 1 && widget.typeAddress == 2 ? false : true,
                                                  readOnly: true,
                                                  style: TextStyle(color: Colors.black),
                                                  decoration: InputDecoration(
                                                      labelText: 'Kelurahan',
                                                      labelStyle: TextStyle(color: Colors.black),
                                                      filled: (formMAddAlamatKorespon.isDisablePACIAAOSCONA || formMAddAlamatKorespon.disableJenisPenawaran) && widget.flag == 1 && widget.typeAddress == 1
                                                          ? formMAddAlamatKorespon.jenisAlamatSelected != null
                                                            ? formMAddAlamatKorespon.jenisAlamatSelected.KODE == "03" || formMAddAlamatKorespon.jenisAlamatSelected.KODE == "01"
                                                              ? true : false
                                                              : false
                                                          : (formMAddAlamatKorespon.isDisablePACIAAOSCONA) && widget.flag == 1 && widget.typeAddress == 2
                                                            ? true : false,
                                                      fillColor: Colors.black12,
                                                      border: OutlineInputBorder(
                                                          borderRadius: BorderRadius.circular(8)),
                                                      enabledBorder: OutlineInputBorder(
                                                          borderSide: BorderSide(color: formMAddAlamatKorespon.isKelurahanChanges ? Colors.purple : Colors.grey)),
                                                      disabledBorder: OutlineInputBorder(
                                                          borderSide: BorderSide(color: formMAddAlamatKorespon.isKelurahanChanges ? Colors.purple : Colors.grey)),
                                                  ),
                                                  keyboardType: TextInputType.text,
                                                  textCapitalization: TextCapitalization.characters,
                                            ),
                                              ),
                                          ),
                                          Visibility(
                                              child: SizedBox(height: MediaQuery.of(context).size.height / 47),
                                              visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKelurahanShow(widget.typeAddress),
                                          ),
                                          Visibility(
                                              visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKecamatanShow(widget.typeAddress),
                                              child: TextFormField(
                                                autovalidate: formMAddAlamatKorespon.autoValidate,
                                                validator: (e) {
                                                    if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKecamatanMan(widget.typeAddress)) {
                                                        return "Tidak boleh kosong";
                                                    } else {
                                                        return null;
                                                    }
                                                },
                                                enabled: false,
                                                controller: formMAddAlamatKorespon.controllerKecamatan,
                                                style: TextStyle(color: Colors.black),
                                                decoration: InputDecoration(
                                                    labelText: 'Kecamatan',
                                                    labelStyle: TextStyle(color: Colors.black),
                                                    filled: true,
                                                    fillColor: Colors.black12,
                                                    border: OutlineInputBorder(
                                                        borderRadius: BorderRadius.circular(8)),
                                                    enabledBorder: OutlineInputBorder(
                                                        borderSide: BorderSide(color: formMAddAlamatKorespon.isKecamatanChanges ? Colors.purple : Colors.grey)),
                                                    disabledBorder: OutlineInputBorder(
                                                        borderSide: BorderSide(color: formMAddAlamatKorespon.isKecamatanChanges ? Colors.purple : Colors.grey)),
                                                ),
                                                keyboardType: TextInputType.text,
                                                textCapitalization: TextCapitalization.characters,
                                            ),
                                          ),
                                          Visibility(
                                              child: SizedBox(height: MediaQuery.of(context).size.height / 47),
                                              visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKecamatanShow(widget.typeAddress),
                                          ),
                                          Visibility(
                                              visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKabKotShow(widget.typeAddress),
                                              child: TextFormField(
                                                autovalidate: formMAddAlamatKorespon.autoValidate,
                                                validator: (e) {
                                                    if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKabKotMan(widget.typeAddress)) {
                                                        return "Tidak boleh kosong";
                                                    } else {
                                                        return null;
                                                    }
                                                },
                                                enabled: false,
                                                controller: formMAddAlamatKorespon.controllerKota,
                                                style: TextStyle(color: Colors.black),
                                                decoration: InputDecoration(
                                                    labelText: 'Kabupaten/Kota',
                                                    labelStyle: TextStyle(color: Colors.black),
                                                    filled: true,
                                                    fillColor: Colors.black12,
                                                    border: OutlineInputBorder(
                                                        borderRadius:BorderRadius.circular(8)),
                                                    enabledBorder: OutlineInputBorder(
                                                        borderSide: BorderSide(color: formMAddAlamatKorespon.isKotaChanges ? Colors.purple : Colors.grey)),
                                                    disabledBorder: OutlineInputBorder(
                                                        borderSide: BorderSide(color: formMAddAlamatKorespon.isKotaChanges ? Colors.purple : Colors.grey)),
                                                ),
                                                keyboardType: TextInputType.text,
                                                textCapitalization: TextCapitalization.characters,
                                            ),
                                          ),
                                          Visibility(
                                              child: SizedBox(height: MediaQuery.of(context).size.height / 47),
                                              visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKabKotShow(widget.typeAddress),
                                          ),
                                          Row(
                                              children: [
                                                  Expanded(
                                                      flex: 7,
                                                      child: Visibility(
                                                        visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setProvinsiShow(widget.typeAddress),
                                                          child: TextFormField(
                                                            autovalidate: formMAddAlamatKorespon.autoValidate,
                                                            validator: (e) {
                                                                if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setProvinsiMan(widget.typeAddress)) {
                                                                    return "Tidak boleh kosong";
                                                                } else {
                                                                    return null;
                                                                }
                                                            },
                                                            enabled: false,
                                                            controller: formMAddAlamatKorespon.controllerProvinsi,
                                                            style: TextStyle(color: Colors.black),
                                                            decoration: InputDecoration(
                                                                labelText: 'Provinsi',
                                                                labelStyle: TextStyle(color: Colors.black),
                                                                filled: true,
                                                                fillColor: Colors.black12,
                                                                border: OutlineInputBorder(
                                                                    borderRadius: BorderRadius.circular(8)),
                                                                enabledBorder: OutlineInputBorder(
                                                                    borderSide: BorderSide(color: formMAddAlamatKorespon.isProvinsiChanges ? Colors.purple : Colors.grey)),
                                                                disabledBorder: OutlineInputBorder(
                                                                    borderSide: BorderSide(color: formMAddAlamatKorespon.isProvinsiChanges ? Colors.purple : Colors.grey)),
                                                            ),
                                                            keyboardType: TextInputType.text,
                                                            textCapitalization: TextCapitalization.characters,
                                                        ),
                                                      ),
                                                  ),
                                                  SizedBox(width: 8),
                                                  Expanded(
                                                      flex: 3,
                                                      child: Visibility(
                                                          visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKodePosShow(widget.typeAddress),
                                                        child: TextFormField(
                                                            autovalidate: formMAddAlamatKorespon.autoValidate,
                                                            validator: (e) {
                                                                if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKodePosMan(widget.typeAddress)) {
                                                                    return "Tidak boleh kosong";
                                                                } else {
                                                                    return null;
                                                                }
                                                            },
                                                            enabled: false,
                                                            controller: formMAddAlamatKorespon.controllerPostalCode,
                                                            style: TextStyle(color: Colors.black),
                                                            decoration: InputDecoration(
                                                                labelText: 'Kode Pos',
                                                                labelStyle: TextStyle(color: Colors.black),
                                                                filled: true,
                                                                fillColor: Colors.black12,
                                                                border: OutlineInputBorder(
                                                                    borderRadius:
                                                                    BorderRadius.circular(8)),
                                                                enabledBorder: OutlineInputBorder(
                                                                    borderSide: BorderSide(color: formMAddAlamatKorespon.isPostalCodeChanges ? Colors.purple : Colors.grey)),
                                                                disabledBorder: OutlineInputBorder(
                                                                    borderSide: BorderSide(color: formMAddAlamatKorespon.isPostalCodeChanges ? Colors.purple : Colors.grey)),
                                                            ),
                                                            keyboardType: TextInputType.number,
                                                        ),
                                                      ),
                                                  ),
                                              ],
                                          ),
                                          SizedBox(height: MediaQuery.of(context).size.height / 47),
                                          TextFormField(
                                             // autovalidate: formMAddAlamatKorespon.autoValidate,
                                             // validator: (e) {
                                             //     if (e.isEmpty) {
                                             //         return "Tidak boleh kosong";
                                             //     } else {
                                             //         return null;
                                             //     }
                                             // },
                                             readOnly: true,
                                             onTap: (){
                                                 formMAddAlamatKorespon.setLocationAddressByMap(context);
                                             },
                                             maxLines: 3,
                                             controller: formMAddAlamatKorespon.controllerAddressFromMap,
                                             style: TextStyle(color: Colors.black),
                                             decoration: InputDecoration(
                                                 labelText: 'Geolocation Alamat',
                                                 labelStyle: TextStyle(color: Colors.black),
                                                 border: OutlineInputBorder(
                                                     borderRadius: BorderRadius.circular(8)),
                                                 enabledBorder: OutlineInputBorder(
                                                     borderSide: BorderSide(color: formMAddAlamatKorespon.isAddressFromMapChanges ? Colors.purple : Colors.grey)),
                                                 disabledBorder: OutlineInputBorder(
                                                     borderSide: BorderSide(color: formMAddAlamatKorespon.isAddressFromMapChanges ? Colors.purple : Colors.grey)),
                                             ),
                                             keyboardType: TextInputType.number,
                                          ),
                                          SizedBox(height: MediaQuery.of(context).size.height / 47),
                                          Row(
                                              children: [
                                                  Expanded(
                                                      flex: 4,
                                                      child: Visibility(
                                                        visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setTelp1AreaShow(widget.typeAddress),
                                                          child: TextFormField(
                                                         autovalidate: formMAddAlamatKorespon.autoValidate,
                                                         validator: (e) {
                                                             if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setTelp1AreaMan(widget.typeAddress)) {
                                                                 return "Tidak boleh kosong";
                                                             } else {
                                                                 return null;
                                                             }
                                                         },
                                                            onChanged: (e) {
                                                                formMAddAlamatKorespon.checkValidCOdeArea(e);
                                                            },
                                                            inputFormatters: [
                                                                WhitelistingTextInputFormatter.digitsOnly,
                                                                LengthLimitingTextInputFormatter(4),
                                                            ],
                                                            controller: formMAddAlamatKorespon.controllerKodeArea,
                                                            style: TextStyle(color: Colors.black),
                                                            decoration: InputDecoration(
                                                                labelText: 'Kode Area',
                                                                labelStyle: TextStyle(color: Colors.black),
                                                                border: OutlineInputBorder(
                                                                    borderRadius: BorderRadius.circular(8)),
                                                                enabledBorder: OutlineInputBorder(
                                                                    borderSide: BorderSide(color: formMAddAlamatKorespon.isTeleponAreaChanges ? Colors.purple : Colors.grey)),
                                                                disabledBorder: OutlineInputBorder(
                                                                    borderSide: BorderSide(color: formMAddAlamatKorespon.isTeleponAreaChanges ? Colors.purple : Colors.grey)),
                                                            ),
                                                            keyboardType: TextInputType.number,
                                                        ),
                                                      ),
                                                  ),
                                                  SizedBox(width: MediaQuery.of(context).size.width / 37),
                                                  Expanded(
                                                      flex: 6,
                                                      child: Visibility(
                                                        visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setTelp1Show(widget.typeAddress),
                                                          child: TextFormField(
                                                           autovalidate: formMAddAlamatKorespon.autoValidate,
                                                           validator: (e) {
                                                               if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setTelp1Man(widget.typeAddress)) {
                                                                   return "Tidak boleh kosong";
                                                               } else {
                                                                   return null;
                                                               }
                                                           },
                                                            inputFormatters: [
                                                                WhitelistingTextInputFormatter.digitsOnly,
                                                                LengthLimitingTextInputFormatter(10),
                                                            ],
                                                            controller: formMAddAlamatKorespon.controllerTlpn,
                                                            style: TextStyle(color: Colors.black),
                                                            decoration: InputDecoration(
                                                                labelText: 'Telepon',
                                                                labelStyle: TextStyle(color: Colors.black),
                                                                border: OutlineInputBorder(
                                                                    borderRadius: BorderRadius.circular(8)),
                                                                enabledBorder: OutlineInputBorder(
                                                                    borderSide: BorderSide(color: formMAddAlamatKorespon.isTeleponChanges ? Colors.purple : Colors.grey)),
                                                                disabledBorder: OutlineInputBorder(
                                                                    borderSide: BorderSide(color: formMAddAlamatKorespon.isTeleponChanges ? Colors.purple : Colors.grey)),
                                                            ),
                                                            keyboardType: TextInputType.number,
                                                        ),
                                                      ),
                                                  ),
                                              ],
                                          ),
                                      ],
                                  ),
                              ),
                          );
                      },
                  );
              }),
          bottomNavigationBar: BottomAppBar(
              elevation: 0.0,
              child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(8.0)),
                      color: myPrimaryColor,
                      onPressed: () {
                          if (widget.flag == 0) {
                              // add
                              Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false)
                                  .check(context, widget.flag, null, widget.typeAddress);
                          } else {
                              // edit
                              Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false)
                                  .check(context, widget.flag, widget.index, widget.typeAddress);
                          }
                      },
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                              Text(widget.flag == 0 ? "SAVE" : "UPDATE",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                      letterSpacing: 1.25))
                          ],
                      ))),
          ),
      ),
    );
  }

    Future<bool> _onWillPop() async {
        var _provider = Provider.of<FormMAddAddressIndividuChangeNotifier>(context, listen: false);
        if (widget.flag == 0) {
            if (_provider.jenisAlamatSelected != null ||
                _provider.controllerAlamat.text != "" ||
                _provider.controllerRT.text != "" ||
                _provider.controllerRW.text != "" ||
                _provider.controllerKelurahan.text != "" ||
                _provider.controllerKecamatan.text != "" ||
                _provider.controllerKota.text != "" ||
                _provider.controllerProvinsi.text != "" ||
                _provider.controllerPostalCode.text != "" ||
                _provider.controllerKodeArea.text != "" ||
                _provider.controllerTlpn.text != "") {
                return (await showDialog(
                    context: context,
                    builder: (myContext) => AlertDialog(
                        title: new Text('Warning'),
                        content: new Text('Keluar dengan simpan perubahan?'),
                        actions: <Widget>[
                            new FlatButton(
                                onPressed: () {
                                    _provider.check(context, widget.flag, null, widget.typeAddress);
                                    Navigator.pop(context);
                                },
                                child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                            ),
                            new FlatButton(
                                onPressed: () => Navigator.of(context).pop(true),
                                child: new Text('Tidak', style: TextStyle(color: primaryOrange),),
                            ),
                        ],
                    ),
                )) ?? false;
            } else {
                return true;
            }
        } else {
            if (_provider.jenisAlamatSelectedTemp.KODE != _provider.jenisAlamatSelected.KODE ||
                _provider.alamatTemp != _provider.controllerAlamat.text ||
                _provider.rtTemp != _provider.controllerRT.text ||
                _provider.rwTemp != _provider.controllerRW.text ||
                _provider.kelurahanTemp != _provider.controllerKelurahan.text ||
                _provider.kecamatanTemp != _provider.controllerKecamatan.text ||
                _provider.kotaTemp != _provider.controllerKota.text ||
                _provider.provinsiTemp != _provider.controllerProvinsi.text ||
                _provider.postalCodeTemp != _provider.controllerPostalCode.text ||
                _provider.areaCodeTemp != _provider.controllerKodeArea.text ||
                _provider.phoneTemp != _provider.controllerTlpn.text) {
                return (await showDialog(
                    context: context,
                    builder: (myContext) => AlertDialog(
                        title: new Text('Warning'),
                        content: new Text('Keluar dengan simpan perubahan?'),
                        actions: <Widget>[
                            new FlatButton(
                                onPressed: () {
                                    _provider.check(context, widget.flag, widget.index, widget.typeAddress);
                                    Navigator.pop(context);
                                },
                                child: new Text('Ya', style: TextStyle(color: Colors.grey)),
                            ),
                            new FlatButton(
                                onPressed: () => Navigator.of(context).pop(true),
                                child: new Text('Tidak', style: TextStyle(color: primaryOrange),),
                            ),
                        ],
                    ),
                )) ?? false;
            } else {
                return true;
            }
        }
    }
}
