import 'package:ad1ms2_dev/models/certificate_type_model.dart';
import 'package:ad1ms2_dev/models/floor_type_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/models/foundation_type_model.dart';
import 'package:ad1ms2_dev/models/property_type_model.dart';
import 'package:ad1ms2_dev/models/street_type_model.dart';
import 'package:ad1ms2_dev/models/type_of_roof_model.dart';
import 'package:ad1ms2_dev/models/wall_type_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_collatelar_change_notifier.dart';
import 'package:ad1ms2_dev/shared/decimal_text_input_formatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../main.dart';

class WidgetInfoCollateralProperty extends StatefulWidget {

  @override
  _WidgetInfoCollateralPropertyState createState() => _WidgetInfoCollateralPropertyState();
}

class _WidgetInfoCollateralPropertyState extends State<WidgetInfoCollateralProperty> {
  Future<void> _setPreference;

  @override
  void initState() {
    super.initState();
    Provider.of<InformationCollateralChangeNotifier>(context,listen: false).addListAddressType();
    // Provider.of<InformationCollateralChangeNotifier>(context, listen: false).setDataCollaOtoFromSQLite(context);
    _setPreference = Provider.of<InformationCollateralChangeNotifier>(context,listen: false).setPreference(context, '');
    // Provider.of<InformationCollateralChangeNotifier>(context,listen: false).getDataFromDashboard(context);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _setPreference,
      builder: (context,snapshot){
        if(snapshot.connectionState == ConnectionState.waiting){
          return Center(
            child: CircularProgressIndicator()
          );
        }
        return Consumer<InformationCollateralChangeNotifier>(
          builder: (context,infoCollateralChangeNotifier,_){
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Visibility(
                  visible: infoCollateralChangeNotifier.isRadioValueIsCollateralSameWithApplicantPropertyVisible(),
                  child: Text(
                    "Nama Jaminan = Pemohon",
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      color: infoCollateralChangeNotifier.isRadioValueIsCollateralSameWithApplicantPropertyChanges ? Colors.purple : Colors.black,
                    ),
                  ),
                ),
                // SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isRadioValueIsCollateralSameWithApplicantPropertyVisible(),
                  child: IgnorePointer(
                    ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA,
                    child: Row(
                      children: [
                        Row(
                          children: [
                            Radio(
                                activeColor: primaryOrange,
                                value: 0,
                                groupValue: infoCollateralChangeNotifier.radioValueIsCollateralSameWithApplicantProperty,
                                onChanged: (value){
                                  setState(() {
                                    infoCollateralChangeNotifier.radioValueIsCollateralSameWithApplicantProperty = value;
                                  });
                                  infoCollateralChangeNotifier.sameWithApplicantProperty(context);
                                }
                            ),
                            Text("Ya")
                          ],
                        ),
                        SizedBox(height: MediaQuery.of(context).size.width / 27),
                        Row(
                          children: [
                            Radio(
                                activeColor: primaryOrange,
                                value: 1,
                                groupValue: infoCollateralChangeNotifier.radioValueIsCollateralSameWithApplicantProperty,
                                onChanged: (value){
                                  setState(() {
                                    infoCollateralChangeNotifier.radioValueIsCollateralSameWithApplicantProperty = value;
                                  });
                                  infoCollateralChangeNotifier.sameWithApplicantProperty(context);
                                }
                            ),
                            Text("Tidak")
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                infoCollateralChangeNotifier.radioValueIsCollateralSameWithApplicantProperty == 0
                    ?
                SizedBox()
                // TextFormField(
                //   // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                //   // validator: (e) {
                //   //   if (e.isEmpty) {
                //   //     return "Tidak boleh kosong";
                //   //   } else {
                //   //     return null;
                //   //   }
                //   // },
                //     enabled: false,
                //     controller: infoCollateralChangeNotifier.controllerIdentityType,
                //     decoration: new InputDecoration(
                //         labelText: 'Jenis Identitas',
                //         labelStyle: TextStyle(color: Colors.black),
                //         border: OutlineInputBorder(
                //             borderRadius: BorderRadius.circular(8))))
                    :
                Column(
                  children: <Widget>[
                    Visibility(
                      visible: infoCollateralChangeNotifier.isIdentityModelVisible(),
                      child: DropdownButtonFormField<IdentityModel>(
                          autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                          validator: (e) {
                            if (e == null && infoCollateralChangeNotifier.isIdentityModelMandatory()) {
                              return "Silahkan pilih jenis sertifikat";
                            } else {
                              return null;
                            }
                          },
                          value: infoCollateralChangeNotifier.identityModel,
                          onChanged: (value) {
                            infoCollateralChangeNotifier.identityModel = value;
                          },
                          decoration: InputDecoration(
                              labelText: "Jenis Identitas",
                              border: OutlineInputBorder(),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: infoCollateralChangeNotifier.isIdentityModelChanges ? Colors.purple : Colors.grey)),
                              disabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: infoCollateralChangeNotifier.isIdentityModelChanges ? Colors.purple : Colors.grey)),
                              contentPadding: EdgeInsets.symmetric(horizontal: 10)
                          ),
                          items: infoCollateralChangeNotifier.listIdentityType.map((value) {
                            return DropdownMenuItem<IdentityModel>(
                              value: value,
                              child: Text(
                                value.name,
                                overflow: TextOverflow.ellipsis,
                              ),
                            );
                          }).toList()),
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    Visibility(
                      visible: infoCollateralChangeNotifier.isIdentityNumberVisible(),
                      child: TextFormField(
                          autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                          validator: (e) {
                            if (e.isEmpty && infoCollateralChangeNotifier.isIdentityNumberMandatory()) {
                              return "Tidak boleh kosong";
                            } else {
                              return null;
                            }
                          },
                          readOnly: true,
                          controller: infoCollateralChangeNotifier.controllerIdentityNumber,
                          decoration: InputDecoration(
                              labelText: 'No Identitas',
                              labelStyle: TextStyle(color: Colors.black),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: infoCollateralChangeNotifier.isIdentityNumberPropChanges ? Colors.purple : Colors.grey)),
                              disabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: infoCollateralChangeNotifier.isIdentityNumberPropChanges ? Colors.purple : Colors.grey)),
                              contentPadding: EdgeInsets.symmetric(horizontal: 10)
                          )
                      ),
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    Visibility(
                      visible: infoCollateralChangeNotifier.isNameOnCollateralVisible(),
                      child: IgnorePointer(
                        ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA,
                        child: TextFormField(
                          validator: (e) {
                            if (e.isEmpty && infoCollateralChangeNotifier.isNameOnCollateralMandatory()) {
                              return "Tidak boleh kosong";
                            } else {
                              return null;
                            }
                          },
                          controller: infoCollateralChangeNotifier.controllerNameOnCollateral,
                          autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                          decoration: InputDecoration(
                              labelText: 'Nama Pada Jaminan',
                              labelStyle: TextStyle(color: Colors.black),
                              filled: infoCollateralChangeNotifier.isDisablePACIAAOSCONA,
                              fillColor: Colors.black12,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: infoCollateralChangeNotifier.isNameOnCollateralChanges ? Colors.purple : Colors.grey)),
                              disabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: infoCollateralChangeNotifier.isNameOnCollateralChanges ? Colors.purple : Colors.grey)),
                              contentPadding: EdgeInsets.symmetric(horizontal: 10)
                          ),
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(RegExp('[a-zA-Z  \`]')),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    Visibility(
                      visible: infoCollateralChangeNotifier.isBirthDatePropVisible(),
                      child: TextFormField(
                        onTap: () {
                          FocusManager.instance.primaryFocus.unfocus();
                          infoCollateralChangeNotifier.selectBirthDate(context,1);
                        },
                        controller: infoCollateralChangeNotifier.controllerBirthDateProp,
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            labelText: 'Tanggal Lahir',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isBirthDatePropChanges ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isBirthDatePropChanges ? Colors.purple : Colors.grey)),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10)
                        ),
                        autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                        validator: (e) {
                          if (e.isEmpty && infoCollateralChangeNotifier.isBirthDatePropMandatory()) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        readOnly: true,
                      ),
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    Visibility(
                      visible: infoCollateralChangeNotifier.isBirthPlaceValidWithIdentity1Visible(),
                      child: TextFormField(
                        autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                        validator: (e) {
                          if (e.isEmpty && infoCollateralChangeNotifier.isBirthPlaceValidWithIdentity1Mandatory()) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        readOnly: true,
                        controller: infoCollateralChangeNotifier.controllerBirthPlaceValidWithIdentity1,
                        decoration: InputDecoration(
                            labelText: 'Tempat Lahir Sesuai Identitas',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isBirthPlaceValidWithIdentity1Changes ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isBirthPlaceValidWithIdentity1Changes ? Colors.purple : Colors.grey)),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10)
                        ),
                        inputFormatters: [
                          FilteringTextInputFormatter.allow(RegExp('[a-zA-Z]')),
                        ],
                      ),
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 47),
                    Visibility(
                      visible: infoCollateralChangeNotifier.isBirthPlaceValidWithIdentityLOVVisible(),
                      child: TextFormField(
                        autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                        validator: (e) {
                          if (e.isEmpty && infoCollateralChangeNotifier.isBirthPlaceValidWithIdentityLOVMandatory()) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        readOnly: true,
                        onTap: (){
                          infoCollateralChangeNotifier.searchBirthPlace(context,1);
                        },
                        controller: infoCollateralChangeNotifier.controllerBirthPlaceValidWithIdentityLOV,
                        decoration: InputDecoration(
                            labelText: 'Tempat Lahir Sesuai Identitas',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isBirthPlaceValidWithIdentityLOVChanges ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isBirthPlaceValidWithIdentityLOVChanges ? Colors.purple : Colors.grey)),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10)
                        ),
                        inputFormatters: [
                          FilteringTextInputFormatter.allow(RegExp('[a-zA-Z]')),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isCertificateNumberVisible(),
                  child: TextFormField(
                    autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                    validator: (e) {
                      if (e.isEmpty && infoCollateralChangeNotifier.isCertificateNumberMandatory()) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    controller: infoCollateralChangeNotifier.controllerCertificateNumber,
                    decoration: InputDecoration(
                        labelText: 'No Sertifikat',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: infoCollateralChangeNotifier.isCertificateNumberChanges ? Colors.purple : Colors.grey)),
                        disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: infoCollateralChangeNotifier.isCertificateNumberChanges ? Colors.purple : Colors.grey)),
                        contentPadding: EdgeInsets.symmetric(horizontal: 10)
                    ),
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(RegExp('[a-zA-Z 0-9]')),
                    ],
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isCertificateTypeSelectedVisible(),
                  child: DropdownButtonFormField<CertificateTypeModel>(
                      autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                      validator: (e) {
                        if (e == null && infoCollateralChangeNotifier.isCertificateTypeSelectedMandatory()) {
                          return "Silahkan pilih jenis sertifikat";
                        } else {
                          return null;
                        }
                      },
                      onTap: () {
                        FocusManager.instance.primaryFocus.unfocus();
                      },
                      value: infoCollateralChangeNotifier.certificateTypeSelected,
                      onChanged: (value) {
                        infoCollateralChangeNotifier.certificateTypeSelected = value;
                      },
                      decoration: InputDecoration(
                          labelText: "Jenis Sertifikat",
                          border: OutlineInputBorder(),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isCertificateTypeSelectedChanges ? Colors.purple : Colors.grey)),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isCertificateTypeSelectedChanges ? Colors.purple : Colors.grey)),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10)
                      ),
                      items: infoCollateralChangeNotifier.listCertificateType.map((value) {
                        return DropdownMenuItem<CertificateTypeModel>(
                          value: value,
                          child: Text(
                            value.name,
                            overflow: TextOverflow.ellipsis,
                          ),
                        );
                      }).toList()),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isPropertyTypeSelectedVisible(),
                  child: IgnorePointer(
                    ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA,
                    child: DropdownButtonFormField<PropertyTypeModel>(
                        autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                        validator: (e) {
                          if (e == null && infoCollateralChangeNotifier.isPropertyTypeSelectedMandatory()) {
                            return "Silahkan pilih jenis properti";
                          } else {
                            return null;
                          }
                        },
                        value: infoCollateralChangeNotifier.propertyTypeSelected,
                        onChanged: (value) {
                          infoCollateralChangeNotifier.propertyTypeSelected = value;
                        },
                        decoration: InputDecoration(
                            labelText: "Jenis Properti",
                            filled: infoCollateralChangeNotifier.isDisablePACIAAOSCONA,
                            fillColor: Colors.black12,
                            border: OutlineInputBorder(),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isPropertyTypeSelectedChanges ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isPropertyTypeSelectedChanges ? Colors.purple : Colors.grey)),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10)
                        ),
                        items: infoCollateralChangeNotifier.listPropertyType.map((value) {
                          return DropdownMenuItem<PropertyTypeModel>(
                            value: value,
                            child: Text(
                              value.name,
                              overflow: TextOverflow.ellipsis,
                            ),
                          );
                        }).toList()),
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isBuildingAreaVisible(),
                  child: TextFormField(
                      autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                      validator: (e) {
                        if (e.isEmpty && infoCollateralChangeNotifier.isBuildingAreaMandatory()) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: infoCollateralChangeNotifier.controllerBuildingArea,
                      keyboardType: TextInputType.number,
                      // textAlign: TextAlign.end,
                      onFieldSubmitted: (value) {
                        infoCollateralChangeNotifier.controllerBuildingArea.text = infoCollateralChangeNotifier.formatCurrency.formatCurrency(value);
                      },
                      textInputAction: TextInputAction.done,
                      inputFormatters: [
                        DecimalTextInputFormatter(decimalRange: 2),
                        infoCollateralChangeNotifier.amountValidator
                      ],
                      decoration: InputDecoration(
                          labelText: 'Luas Bangunan',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isBuildingAreaChanges ? Colors.purple : Colors.grey)),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isBuildingAreaChanges ? Colors.purple : Colors.grey)),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10)
                      )
                  ),

                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isSurfaceAreaVisible(),
                  child: TextFormField(
                      autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                      validator: (e) {
                        if (e.isEmpty && infoCollateralChangeNotifier.isSurfaceAreaMandatory()) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: infoCollateralChangeNotifier.controllerSurfaceArea,
                      keyboardType: TextInputType.number,
                      // textAlign: TextAlign.end,
                      // onFieldSubmitted: (value) {
                      //   infoCollateralChangeNotifier.controllerSurfaceArea.text = infoCollateralChangeNotifier.formatCurrency.formatCurrency(value);
                      // },
                      textInputAction: TextInputAction.done,
                      inputFormatters: [
                        DecimalTextInputFormatter(decimalRange: 2),
                        infoCollateralChangeNotifier.amountValidator
                      ],
                      decoration: InputDecoration(
                          labelText: 'Luas Tanah',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isSurfaceAreaChanges ? Colors.purple : Colors.grey)),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isSurfaceAreaChanges ? Colors.purple : Colors.grey)),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10)
                      )
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isTaksasiPriceVisible(),
                  child: IgnorePointer(
                    ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA,
                    child: TextFormField(
                      autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                      validator: (e) {
                        if (e.isEmpty && infoCollateralChangeNotifier.isTaksasiPriceMandatory()) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: infoCollateralChangeNotifier.controllerTaksasiPrice,
                      decoration: InputDecoration(
                          labelText: 'Harga Taksasi/Appraisal',
                          labelStyle: TextStyle(color: Colors.black),
                          filled: infoCollateralChangeNotifier.isDisablePACIAAOSCONA,
                          fillColor: Colors.black12,
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isTaksasiPriceChanges ? Colors.purple : Colors.grey)),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isTaksasiPriceChanges ? Colors.purple : Colors.grey)),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10)
                      ),
                      keyboardType: TextInputType.number,
                      textAlign: TextAlign.end,
                      onFieldSubmitted: (value) {
                        infoCollateralChangeNotifier.controllerTaksasiPrice.text = infoCollateralChangeNotifier.formatCurrency.formatCurrency(value);
                      },
                      onTap: () {
                        infoCollateralChangeNotifier.formatting();
                      },
                      textInputAction: TextInputAction.done,
                      inputFormatters: [
                        DecimalTextInputFormatter(decimalRange: 2),
                        infoCollateralChangeNotifier.amountValidator
                      ],
                    ),
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isSifatJaminanVisible(),
                  child: TextFormField(
                    autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                    validator: (e) {
                      if (e.isEmpty && infoCollateralChangeNotifier.isSifatJaminanMandatory()) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    controller: infoCollateralChangeNotifier.controllerSifatJaminan,
                    decoration: InputDecoration(
                        labelText: 'Sifat Jaminan',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: infoCollateralChangeNotifier.isSifatJaminanChanges ? Colors.purple : Colors.grey)),
                        disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: infoCollateralChangeNotifier.isSifatJaminanChanges ? Colors.purple : Colors.grey)),
                        contentPadding: EdgeInsets.symmetric(horizontal: 10)
                    ),
                    textCapitalization: TextCapitalization.characters,
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(RegExp('[a-zA-Z 0-9]')),
                    ],
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isBuktiKepemilikanVisible(),
                  child: TextFormField(
                    autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                    validator: (e) {
                      if (e.isEmpty && infoCollateralChangeNotifier.isBuktiKepemilikanMandatory()) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    controller: infoCollateralChangeNotifier.controllerBuktiKepemilikan,
                    textCapitalization: TextCapitalization.characters,
                    decoration: InputDecoration(
                        labelText: 'Bukti Kepemilikan Jaminan',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: infoCollateralChangeNotifier.isBuktiKepemilikanChanges ? Colors.purple : Colors.grey)),
                        disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: infoCollateralChangeNotifier.isBuktiKepemilikanChanges ? Colors.purple : Colors.grey)),
                        contentPadding: EdgeInsets.symmetric(horizontal: 10)
                    ),
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(RegExp('[a-zA-Z 0-9]')),
                    ],
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isCertificateReleaseDateVisible(),
                  child: TextFormField(
                      autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                      validator: (e) {
                        if (e.isEmpty && infoCollateralChangeNotifier.isCertificateReleaseDateMandatory()) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      readOnly: true,
                      controller: infoCollateralChangeNotifier.controllerCertificateReleaseDate,
                      onTap: (){
                        infoCollateralChangeNotifier.selectCertificateReleaseDate(context);
                      },
                      decoration: InputDecoration(
                          labelText: 'Tanggal Penerbitan Sertifikat',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isCertificateReleaseDateChanges ? Colors.purple : Colors.grey)),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isCertificateReleaseDateChanges ? Colors.purple : Colors.grey)),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10)
                      )
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isCertificateReleaseYearVisible(),
                  child: TextFormField(
                      autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                      validator: (e) {
                        if (e.isEmpty && infoCollateralChangeNotifier.isCertificateReleaseYearMandatory()) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: infoCollateralChangeNotifier.controllerCertificateReleaseYear,
                      keyboardType: TextInputType.number,
                      inputFormatters: [
                        FilteringTextInputFormatter.allow(RegExp('[ 0-9]')),
                      ],
                      decoration: InputDecoration(
                          labelText: 'Tahun Penerbitan Sertifikat',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isCertificateReleaseYearChanges ? Colors.purple : Colors.grey)),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isCertificateReleaseYearChanges ? Colors.purple : Colors.grey)),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10)
                      )
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isNamaPemegangHakVisible(),
                  child: TextFormField(
                    autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                    validator: (e) {
                      if (e.isEmpty && infoCollateralChangeNotifier.isNamaPemegangHakMandatory()) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    textCapitalization: TextCapitalization.characters,
                    controller: infoCollateralChangeNotifier.controllerNamaPemegangHak,
                    decoration: InputDecoration(
                        labelText: 'Nama Pemegang Hak',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: infoCollateralChangeNotifier.isNamaPemegangHakChanges ? Colors.purple : Colors.grey)),
                        disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: infoCollateralChangeNotifier.isNamaPemegangHakChanges ? Colors.purple : Colors.grey)),
                        contentPadding: EdgeInsets.symmetric(horizontal: 10)
                    ),
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(RegExp('[a-zA-Z  \`]')),
                    ],
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isNoSuratUkurVisible(),
                  child: TextFormField(
                    autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                    validator: (e) {
                      if (e.isEmpty && infoCollateralChangeNotifier.isNoSuratUkurMandatory()) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    controller: infoCollateralChangeNotifier.controllerNoSuratUkur,
                    textCapitalization: TextCapitalization.characters,
                    decoration: InputDecoration(
                        labelText: 'Nomor Surat Ukur/Gambar Situasi',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: infoCollateralChangeNotifier.isNoSuratUkurChanges ? Colors.purple : Colors.grey)),
                        disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: infoCollateralChangeNotifier.isNoSuratUkurChanges ? Colors.purple : Colors.grey)),
                        contentPadding: EdgeInsets.symmetric(horizontal: 10)
                    ),
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(RegExp('[a-zA-Z 0-9]')),
                    ],
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isDateOfMeasuringLetterVisible(),
                  child: TextFormField(
                      autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                      validator: (e) {
                        if (e.isEmpty && infoCollateralChangeNotifier.isDateOfMeasuringLetterMandatory()) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      readOnly: true,
                      controller: infoCollateralChangeNotifier.controllerDateOfMeasuringLetter,
                      onTap: (){
                        infoCollateralChangeNotifier.selectDateOfMeasuringLetter(context);
                      },
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          labelText: 'Tanggal Surat Ukur/Gambar Situasi',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isDateOfMeasuringLetterChanges ? Colors.purple : Colors.grey)),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isDateOfMeasuringLetterChanges ? Colors.purple : Colors.grey)),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10)
                      )
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isCertificateOfMeasuringLetterVisible(),
                  child: TextFormField(
                    autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                    validator: (e) {
                      if (e.isEmpty && infoCollateralChangeNotifier.isCertificateOfMeasuringLetterMandatory()) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    controller: infoCollateralChangeNotifier.controllerCertificateOfMeasuringLetter,
                    textCapitalization: TextCapitalization.characters,
                    decoration: InputDecoration(
                        labelText: 'Sertifikat dan Surat Ukur/Gambar Situasi Dikeluarkan Oleh',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: infoCollateralChangeNotifier.isCertificateOfMeasuringLetterChanges ? Colors.purple : Colors.grey)),
                        disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: infoCollateralChangeNotifier.isCertificateOfMeasuringLetterChanges ? Colors.purple : Colors.grey)),
                        contentPadding: EdgeInsets.symmetric(horizontal: 10)
                    ),
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(RegExp('[a-zA-Z 0-9]')),
                      LengthLimitingTextInputFormatter(10)
                    ],
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isDPJaminanVisible(),
                  child: IgnorePointer(
                    ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA,
                    child: TextFormField(
                        autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                        validator: (e) {
                          if (e.isEmpty && infoCollateralChangeNotifier.isDPJaminanMandatory()) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.end,
                        inputFormatters: [
                          DecimalTextInputFormatter(decimalRange: 2),
                          infoCollateralChangeNotifier.amountValidator
                        ],
                        onFieldSubmitted: (value){
                          infoCollateralChangeNotifier.controllerDPJaminan.text = infoCollateralChangeNotifier.formatCurrency.formatCurrency(value);
                        },
                        controller: infoCollateralChangeNotifier.controllerDPJaminan,
                        decoration: InputDecoration(
                            labelText: 'DP Jaminan',
                            labelStyle: TextStyle(color: Colors.black),
                            filled: infoCollateralChangeNotifier.isDisablePACIAAOSCONA,
                            fillColor: Colors.black12,
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isDPJaminanChanges ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isDPJaminanChanges ? Colors.purple : Colors.grey)),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10)
                        )
                    ),
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isPHMaxVisible(),
                  child: IgnorePointer(
                    ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA,
                    child: TextFormField(
                        autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                        validator: (e) {
                          if (e.isEmpty && infoCollateralChangeNotifier.isPHMaxMandatory()) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.end,
                        inputFormatters: [
                          DecimalTextInputFormatter(decimalRange: 2),
                          infoCollateralChangeNotifier.amountValidator
                        ],
                        onFieldSubmitted: (value){
                          infoCollateralChangeNotifier.controllerPHMax.text = infoCollateralChangeNotifier.formatCurrency.formatCurrency(value);
                        },
                        onTap: () {
                          infoCollateralChangeNotifier.formatting();
                        },
                        controller: infoCollateralChangeNotifier.controllerPHMax,
                        decoration: InputDecoration(
                            labelText: 'PH Maksimal',
                            labelStyle: TextStyle(color: Colors.black),
                            filled: infoCollateralChangeNotifier.isDisablePACIAAOSCONA,
                            fillColor: Colors.black12,
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isPHMaxChanges ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isPHMaxChanges ? Colors.purple : Colors.grey)),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10)
                        )
                    ),
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isJarakFasumPositifVisible(),
                  child: TextFormField(
                      autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                      validator: (e) {
                        if (e.isEmpty && infoCollateralChangeNotifier.isJarakFasumPositifMandatory()) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: infoCollateralChangeNotifier.controllerJarakFasumPositif,
                      keyboardType: TextInputType.number,
                      // textAlign: TextAlign.end,
                      // onFieldSubmitted: (value) {
                      //   infoCollateralChangeNotifier.controllerSurfaceArea.text = infoCollateralChangeNotifier.formatCurrency.formatCurrency(value);
                      // },
                      textInputAction: TextInputAction.done,
                      inputFormatters: [
                        DecimalTextInputFormatter(decimalRange: 2),
                        infoCollateralChangeNotifier.amountValidator
                      ],
                      decoration: InputDecoration(
                          labelText: 'Jarak Fasum Positif',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isJarakFasumPositifChanges ? Colors.purple : Colors.grey)),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isJarakFasumPositifChanges ? Colors.purple : Colors.grey)),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10)
                      )
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isJarakFasumNegatifVisible(),
                  child: TextFormField(
                      autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                      validator: (e) {
                        if (e.isEmpty && infoCollateralChangeNotifier.isJarakFasumNegatifMandatory()) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: infoCollateralChangeNotifier.controllerJarakFasumNegatif,
                      keyboardType: TextInputType.number,
                      // textAlign: TextAlign.end,
                      // onFieldSubmitted: (value) {
                      //   infoCollateralChangeNotifier.controllerSurfaceArea.text = infoCollateralChangeNotifier.formatCurrency.formatCurrency(value);
                      // },
                      textInputAction: TextInputAction.done,
                      inputFormatters: [
                        DecimalTextInputFormatter(decimalRange: 2),
                        infoCollateralChangeNotifier.amountValidator
                      ],
                      decoration: InputDecoration(
                          labelText: 'Jarak Fasum Negatif',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isJarakFasumNegatifChanges ? Colors.purple : Colors.grey)),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isJarakFasumNegatifChanges ? Colors.purple : Colors.grey)),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10)
                      )
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isHargaTanahVisible(),
                  child: TextFormField(
                    autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                    validator: (e) {
                      if (e.isEmpty && infoCollateralChangeNotifier.isHargaTanahMandatory()) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    controller: infoCollateralChangeNotifier.controllerHargaTanah,
                    decoration: InputDecoration(
                        labelText: 'Harga Tanah',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: infoCollateralChangeNotifier.isHargaTanahChanges ? Colors.purple : Colors.grey)),
                        disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: infoCollateralChangeNotifier.isHargaTanahChanges ? Colors.purple : Colors.grey)),
                        contentPadding: EdgeInsets.symmetric(horizontal: 10)
                    ),
                    keyboardType: TextInputType.number,
                    textAlign: TextAlign.end,
                    onFieldSubmitted: (value) {
                      infoCollateralChangeNotifier.controllerHargaTanah.text = infoCollateralChangeNotifier.formatCurrency.formatCurrency(value);
                    },
                    onTap: () {
                      infoCollateralChangeNotifier.formatting();
                    },
                    textInputAction: TextInputAction.done,
                    inputFormatters: [
                      DecimalTextInputFormatter(decimalRange: 2),
                      infoCollateralChangeNotifier.amountValidator
                    ],
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isHargaNJOPVisible(),
                  child: TextFormField(
                      autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                      validator: (e) {
                        if (e.isEmpty && infoCollateralChangeNotifier.isHargaNJOPMandatory()) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: infoCollateralChangeNotifier.controllerHargaNJOP,
                      keyboardType: TextInputType.number,
                      textAlign: TextAlign.end,
                      onFieldSubmitted: (value) {
                        infoCollateralChangeNotifier.controllerHargaNJOP.text = infoCollateralChangeNotifier.formatCurrency.formatCurrency(value);
                      },
                      onTap: () {
                        infoCollateralChangeNotifier.formatting();
                      },
                      textInputAction: TextInputAction.done,
                      inputFormatters: [
                        DecimalTextInputFormatter(decimalRange: 2),
                        infoCollateralChangeNotifier.amountValidator
                      ],
                      decoration: InputDecoration(
                          labelText: 'Harga NJOP',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isHargaNJOPChanges ? Colors.purple : Colors.grey)),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isHargaNJOPChanges ? Colors.purple : Colors.grey)),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10)
                      )
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isHargaBangunanVisible(),
                  child: IgnorePointer(
                    ignoring: infoCollateralChangeNotifier.isDisablePACIAAOSCONA,
                    child: TextFormField(
                        autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                        validator: (e) {
                          if (e.isEmpty && infoCollateralChangeNotifier.isHargaBangunanMandatory()) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.end,
                        inputFormatters: [
                          DecimalTextInputFormatter(decimalRange: 2),
                          infoCollateralChangeNotifier.amountValidator
                        ],
                        onFieldSubmitted: (value){
                          infoCollateralChangeNotifier.controllerHargaBangunan.text = infoCollateralChangeNotifier.formatCurrency.formatCurrency(value);
                        },
                        onTap: () {
                          infoCollateralChangeNotifier.formatting();
                        },
                        controller: infoCollateralChangeNotifier.controllerHargaBangunan,
                        decoration: InputDecoration(
                            labelText: 'Harga Bangunan',
                            labelStyle: TextStyle(color: Colors.black),
                            filled: infoCollateralChangeNotifier.isDisablePACIAAOSCONA,
                            fillColor: Colors.black12,
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isHargaBangunanChanges ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: infoCollateralChangeNotifier.isHargaBangunanChanges ? Colors.purple : Colors.grey)),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10)
                        )
                    ),
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isTypeOfRoofSelectedVisible(),
                  child: DropdownButtonFormField<TypeOfRoofModel>(
                      autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                      validator: (e) {
                        if (e == null && infoCollateralChangeNotifier.isTypeOfRoofSelectedMandatory()) {
                          return "Silahkan pilih jenis atap";
                        } else {
                          return null;
                        }
                      },
                      value: infoCollateralChangeNotifier.typeOfRoofSelected,
                      onChanged: (value) {
                        infoCollateralChangeNotifier.typeOfRoofSelected = value;
                      },
                      decoration: InputDecoration(
                          labelText: "Jenis Atap",
                          border: OutlineInputBorder(),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isTypeOfRoofSelectedChanges ? Colors.purple : Colors.grey)),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isTypeOfRoofSelectedChanges ? Colors.purple : Colors.grey)),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10)
                      ),
                      items: infoCollateralChangeNotifier.listTypeOfRoof.map((value) {
                        return DropdownMenuItem<TypeOfRoofModel>(
                          value: value,
                          child: Text(
                            value.name,
                            overflow: TextOverflow.ellipsis,
                          ),
                        );
                      }).toList()),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isWallTypeSelectedVisible(),
                  child: DropdownButtonFormField<WallTypeModel>(
                      autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                      validator: (e) {
                        if (e == null && infoCollateralChangeNotifier.isWallTypeSelectedMandatory()) {
                          return "Silahkan pilih jenis dinding";
                        } else {
                          return null;
                        }
                      },
                      value: infoCollateralChangeNotifier.wallTypeSelected,
                      onChanged: (value) {
                        infoCollateralChangeNotifier.wallTypeSelected = value;
                      },
                      decoration: InputDecoration(
                          labelText: "Jenis Dinding",
                          border: OutlineInputBorder(),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isWallTypeSelectedChanges ? Colors.purple : Colors.grey)),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isWallTypeSelectedChanges ? Colors.purple : Colors.grey)),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10)
                      ),
                      items: infoCollateralChangeNotifier.listWallType.map((value) {
                        return DropdownMenuItem<WallTypeModel>(
                          value: value,
                          child: Text(
                            value.name,
                            overflow: TextOverflow.ellipsis,
                          ),
                        );
                      }).toList()),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isFloorTypeSelectedVisible(),
                  child: DropdownButtonFormField<FloorTypeModel>(
                      autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                      validator: (e) {
                        if (e == null && infoCollateralChangeNotifier.isFloorTypeSelectedMandatory()) {
                          return "Silahkan pilih jenis lantai";
                        } else {
                          return null;
                        }
                      },
                      value: infoCollateralChangeNotifier.floorTypeSelected,
                      onChanged: (value) {
                        infoCollateralChangeNotifier.floorTypeSelected = value;
                      },
                      decoration: InputDecoration(
                          labelText: "Jenis Lantai",
                          border: OutlineInputBorder(),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isFloorTypeSelectedChanges ? Colors.purple : Colors.grey)),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isFloorTypeSelectedChanges ? Colors.purple : Colors.grey)),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10)
                      ),
                      items: infoCollateralChangeNotifier.listFloorType.map((value) {
                        return DropdownMenuItem<FloorTypeModel>(
                          value: value,
                          child: Text(
                            value.name,
                            overflow: TextOverflow.ellipsis,
                          ),
                        );
                      }).toList()),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isFoundationTypeSelectedVisible(),
                  child: DropdownButtonFormField<FoundationTypeModel>(
                      autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                      validator: (e) {
                        if (e == null && infoCollateralChangeNotifier.isFoundationTypeSelectedMandatory()) {
                          return "Silahkan pilih jenis fondasi";
                        } else {
                          return null;
                        }
                      },
                      value: infoCollateralChangeNotifier.foundationTypeSelected,
                      onChanged: (value) {
                        infoCollateralChangeNotifier.foundationTypeSelected = value;
                      },
                      decoration: InputDecoration(
                          labelText: "Jenis Fondasi",
                          border: OutlineInputBorder(),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isFoundationTypeSelectedChanges ? Colors.purple : Colors.grey)),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isFoundationTypeSelectedChanges ? Colors.purple : Colors.grey)),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10)
                      ),
                      items: infoCollateralChangeNotifier.listFoundationType.map((value) {
                        return DropdownMenuItem<FoundationTypeModel>(
                          value: value,
                          child: Text(
                            value.name,
                            overflow: TextOverflow.ellipsis,
                          ),
                        );
                      }).toList()),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isStreetTypeSelectedVisible(),
                  child: DropdownButtonFormField<StreetTypeModel>(
                      autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                      validator: (e) {
                        if (e == null && infoCollateralChangeNotifier.isStreetTypeSelectedMandatory()) {
                          return "Silahkan pilih tipe jalan";
                        } else {
                          return null;
                        }
                      },
                      value: infoCollateralChangeNotifier.streetTypeSelected,
                      onChanged: (value) {
                        infoCollateralChangeNotifier.streetTypeSelected = value;
                      },
                      decoration: InputDecoration(
                          labelText: "Tipe Jalan",
                          border: OutlineInputBorder(),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isStreetTypeSelectedChanges ? Colors.purple : Colors.grey)),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isStreetTypeSelectedChanges ? Colors.purple : Colors.grey)),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10)
                      ),
                      items: infoCollateralChangeNotifier.listStreetType.map((value) {
                        return DropdownMenuItem<StreetTypeModel>(
                          value: value,
                          child: Text(
                            value.name,
                            overflow: TextOverflow.ellipsis,
                          ),
                        );
                      }).toList()),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isRadioValueAccessCarVisible(),
                  child: Text(
                    "Dilewati Mobil",
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 0.15,
                        color: infoCollateralChangeNotifier.isRadioValueAccessCarChanges ? Colors.purple : Colors.black,
                    ),
                  ),
                ),
                // SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isRadioValueAccessCarVisible(),
                  child: Row(
                    children: [
                      Row(
                        children: [
                          Radio(
                              activeColor: primaryOrange,
                              value: 0,
                              groupValue: infoCollateralChangeNotifier.radioValueAccessCar,
                              onChanged: (value){
                                setState(() {
                                  infoCollateralChangeNotifier.radioValueAccessCar = value;
                                });
                              }
                          ),
                          Text("Ya")
                        ],
                      ),
                      SizedBox(height: MediaQuery.of(context).size.width / 27),
                      Row(
                        children: [
                          Radio(
                              activeColor: primaryOrange,
                              value: 1,
                              groupValue: infoCollateralChangeNotifier.radioValueAccessCar,
                              onChanged: (value){
                                setState(() {
                                  infoCollateralChangeNotifier.radioValueAccessCar = value;
                                });
                              }
                          ),
                          Text("Tidak")
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isJumlahRumahDalamRadiusVisible(),
                  child: TextFormField(
                      autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                      validator: (e) {
                        if (e.isEmpty && infoCollateralChangeNotifier.isJumlahRumahDalamRadiusMandatory()) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: infoCollateralChangeNotifier.controllerJumlahRumahDalamRadius,
                      keyboardType: TextInputType.number,
                      // textAlign: TextAlign.end,
                      // onFieldSubmitted: (value) {
                      //   infoCollateralChangeNotifier.controllerSurfaceArea.text = infoCollateralChangeNotifier.formatCurrency.formatCurrency(value);
                      // },
                      textInputAction: TextInputAction.done,
                      inputFormatters: [
                        DecimalTextInputFormatter(decimalRange: 2),
                        infoCollateralChangeNotifier.amountValidator
                      ],
                      decoration: InputDecoration(
                          labelText: 'Jumlah Rumah Dalam Radius',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isJumlahRumahDalamRadiusChanges ? Colors.purple : Colors.grey)),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isJumlahRumahDalamRadiusChanges ? Colors.purple : Colors.grey)),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10)
                      )
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isMasaHakBerlakuVisible(),
                  child: TextFormField(
                    validator: (e) {
                      if (e.isEmpty && infoCollateralChangeNotifier.isMasaHakBerlakuMandatory()) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                    controller: infoCollateralChangeNotifier.controllerMasaHakBerlaku,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        labelText: 'Masa Berlaku Hak',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: infoCollateralChangeNotifier.isMasaHakBerlakuChanges ? Colors.purple : Colors.grey)),
                        disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: infoCollateralChangeNotifier.isMasaHakBerlakuChanges ? Colors.purple : Colors.grey)),
                        contentPadding: EdgeInsets.symmetric(horizontal: 10)
                    ),
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(RegExp('[ 0-9]')),
                    ],
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isNoIMBVisible(),
                  child: TextFormField(
                    autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                    validator: (e) {
                      if (e.isEmpty && infoCollateralChangeNotifier.isNoIMBMandatory()) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    controller: infoCollateralChangeNotifier.controllerNoIMB,
                    textCapitalization: TextCapitalization.characters,
                    decoration: InputDecoration(
                        labelText: 'No IMB',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: infoCollateralChangeNotifier.isNoIMBChanges ? Colors.purple : Colors.grey)),
                        disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: infoCollateralChangeNotifier.isNoIMBChanges ? Colors.purple : Colors.grey)),
                        contentPadding: EdgeInsets.symmetric(horizontal: 10)
                    ),
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(RegExp('[a-zA-Z 0-9]')),
                    ],
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isDateOfIMBVisible(),
                  child: TextFormField(
                      autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                      validator: (e) {
                        if (e.isEmpty && infoCollateralChangeNotifier.isDateOfIMBMandatory()) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      readOnly: true,
                      controller: infoCollateralChangeNotifier.controllerDateOfIMB,
                      onTap: (){
                        infoCollateralChangeNotifier.selectDateOfIMB(context);
                      },
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          labelText: 'Tanggal IMB',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isDateOfIMBChanges ? Colors.purple : Colors.grey)),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isDateOfIMBChanges ? Colors.purple : Colors.grey)),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10)
                      )
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isLuasBangunanIMBVisible(),
                  child: TextFormField(
                      autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                      validator: (e) {
                        if (e.isEmpty && infoCollateralChangeNotifier.isLuasBangunanIMBMandatory()) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: infoCollateralChangeNotifier.controllerLuasBangunanIMB,
                      keyboardType: TextInputType.number,
                      // textAlign: TextAlign.end,
                      // onFieldSubmitted: (value) {
                      //   infoCollateralChangeNotifier.controllerSurfaceArea.text = infoCollateralChangeNotifier.formatCurrency.formatCurrency(value);
                      // },
                      textInputAction: TextInputAction.done,
                      inputFormatters: [
                        DecimalTextInputFormatter(decimalRange: 2),
                        infoCollateralChangeNotifier.amountValidator
                      ],
                      decoration: InputDecoration(
                          labelText: 'Luas Bangunan Berdasarkan IMB',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isLuasBangunanIMBChanges ? Colors.purple : Colors.grey)),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isLuasBangunanIMBChanges ? Colors.purple : Colors.grey)),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10)
                      )
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isUsageCollateralPropertyVisible(),
                  child: TextFormField(
                      autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                      validator: (e) {
                        if (e.isEmpty && infoCollateralChangeNotifier.isUsageCollateralPropertyMandatory()) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      readOnly: true,
                      onTap: (){
                        infoCollateralChangeNotifier.searchUsageCollateral(context,2);
                      },
                      controller: infoCollateralChangeNotifier.controllerUsageCollateralProperty,
                      decoration: InputDecoration(
                          labelText: 'Tujuan Penggunaan Jaminan/Collateral',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isUsageCollateralPropertyChanges ? Colors.purple : Colors.grey)),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isUsageCollateralPropertyChanges ? Colors.purple : Colors.grey)),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10)
                      )
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isLTVVisible(),
                  child: TextFormField(
                      autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                      validator: (e) {
                        if (e.isEmpty && infoCollateralChangeNotifier.isLTVMandatory()) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: infoCollateralChangeNotifier.controllerLTV,
                      keyboardType: TextInputType.number,
                      inputFormatters: [
                        DecimalTextInputFormatter(decimalRange: 2),
                        infoCollateralChangeNotifier.amountValidator
                      ],
                      decoration: InputDecoration(
                          labelText: 'LTV',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isLTVChanges ? Colors.purple : Colors.grey)),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: infoCollateralChangeNotifier.isLTVChanges ? Colors.purple : Colors.grey)),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10)
                      )
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isRadioValueForAllUnitPropertyVisible(),
                  child: Text("Untuk Semua Unit",
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      color: infoCollateralChangeNotifier.isRadioValueForAllUnitPropertyChanges ? Colors.purple : Colors.black,
                    ),
                  ),
                ),
                // SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  visible: infoCollateralChangeNotifier.isRadioValueForAllUnitPropertyVisible(),
                  child: Row(
                    children: [
                      Row(
                        children: [
                          Radio(
                              activeColor: primaryOrange,
                              value: 0,
                              groupValue: infoCollateralChangeNotifier.radioValueForAllUnitProperty,
                              onChanged: (value){
                                setState(() {
                                  infoCollateralChangeNotifier.radioValueForAllUnitProperty = value;
                                });
                              }
                          ),
                          Text("Ya")
                        ],
                      ),
                      SizedBox(height: MediaQuery.of(context).size.width / 27),
                      Row(
                        children: [
                          Radio(
                              activeColor: primaryOrange,
                              value: 1,
                              groupValue: infoCollateralChangeNotifier.radioValueForAllUnitProperty,
                              onChanged: (value){
                                setState(() {
                                  infoCollateralChangeNotifier.radioValueForAllUnitProperty = value;
                                });
                              }
                          ),
                          Text("Tidak")
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  // visible: infoCollateralChangeNotifier.isAddressTypeSelectedVisible(),
                  child: DropdownButtonFormField<JenisAlamatModel>(
                      autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                      validator: (e) {
                        if (e == null && infoCollateralChangeNotifier.isAddressTypeSelectedMandatory()) {
                          return "Silahkan pilih jenis alamat";
                        } else {
                          return null;
                        }
                      },
                      value: infoCollateralChangeNotifier.addressTypeSelected,
                      onChanged: (value) {
                        infoCollateralChangeNotifier.addressTypeSelected = value;
                        if(infoCollateralChangeNotifier.radioValueIsCollateralSameWithApplicantProperty == 0){
                          infoCollateralChangeNotifier.setValueAddress(1);
                        }
                      },
                      decoration: InputDecoration(
                        labelText: "Jenis Alamat",
                        border: OutlineInputBorder(),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: infoCollateralChangeNotifier.isAddressTypeDakor ? Colors.purple : Colors.grey)),
                        disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: infoCollateralChangeNotifier.isAddressTypeDakor ? Colors.purple : Colors.grey)),
                        contentPadding: EdgeInsets.symmetric(horizontal: 10),
                      ),
                      items: infoCollateralChangeNotifier.listAddressType.map((value) {
                        return DropdownMenuItem<JenisAlamatModel>(
                          value: value,
                          child: Text(
                            value.DESKRIPSI,
                            overflow: TextOverflow.ellipsis,
                          ),
                        );
                      }).toList()),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  // visible: infoCollateralChangeNotifier.isAddressVisible(),
                  child: TextFormField(
                    autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                    validator: (e) {
                      if (e.isEmpty && infoCollateralChangeNotifier.isAddressMandatory()) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    controller: infoCollateralChangeNotifier.controllerAddress,
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                        labelText: 'Alamat',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: infoCollateralChangeNotifier.isAddressDakor ? Colors.purple : Colors.grey)),
                        disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: infoCollateralChangeNotifier.isAddressDakor ? Colors.purple : Colors.grey)),
                        filled: !infoCollateralChangeNotifier.enableTf,
                        fillColor: !infoCollateralChangeNotifier.enableTf
                            ? Colors.black12
                            : Colors.white
                    ),
                    maxLines: 3,
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(RegExp('[a-zA-Z  \`0-9]')),
                    ],
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Row(
                  children: [
                    Expanded(
                      flex: 5,
                      child: Visibility(
                        // visible: infoCollateralChangeNotifier.isRTVisible(),
                        child: TextFormField(
                          autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                          validator: (e) {
                            if (e.isEmpty && infoCollateralChangeNotifier.isRTMandatory()) {
                              return "Tidak boleh kosong";
                            } else {
                              return null;
                            }
                          },
                          inputFormatters: [
                            WhitelistingTextInputFormatter.digitsOnly,
                            // LengthLimitingTextInputFormatter(10),
                          ],
                          controller: infoCollateralChangeNotifier.controllerRT,
                          style: TextStyle(color: Colors.black),
                          decoration: InputDecoration(
                              labelText: 'RT',
                              labelStyle: TextStyle(color: Colors.black),
                              border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: infoCollateralChangeNotifier.isRTDakor ? Colors.purple : Colors.grey)),
                              disabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: infoCollateralChangeNotifier.isRTDakor ? Colors.purple : Colors.grey)),
                              filled: !infoCollateralChangeNotifier.enableTf,
                              fillColor: !infoCollateralChangeNotifier.enableTf
                                  ? Colors.black12
                                  : Colors.white
                          ),
                          keyboardType: TextInputType.number,
                        ),
                      ),
                    ),
                    SizedBox(width: 8),
                    Expanded(
                      flex: 5,
                      child: Visibility(
                        // visible: infoCollateralChangeNotifier.isRWVisible(),
                        child: TextFormField(
                          autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                          validator: (e) {
                            if (e.isEmpty && infoCollateralChangeNotifier.isRWMandatory()) {
                              return "Tidak boleh kosong";
                            } else {
                              return null;
                            }
                          },
                          inputFormatters: [
                            WhitelistingTextInputFormatter.digitsOnly,
                            // LengthLimitingTextInputFormatter(10),
                          ],
                          controller: infoCollateralChangeNotifier.controllerRW,
                          style: TextStyle(color: Colors.black),
                          decoration: InputDecoration(
                              labelText: 'RW',
                              labelStyle: TextStyle(color: Colors.black),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: infoCollateralChangeNotifier.isRWDakor ? Colors.purple : Colors.grey)),
                              disabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: infoCollateralChangeNotifier.isRWDakor ? Colors.purple : Colors.grey)),
                              filled: !infoCollateralChangeNotifier.enableTf,
                              fillColor: !infoCollateralChangeNotifier.enableTf
                                  ? Colors.black12
                                  : Colors.white
                          ),
                          keyboardType: TextInputType.number,
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  // visible: infoCollateralChangeNotifier.isKelurahanVisible(),
                  child: TextFormField(
                    autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                    validator: (e) {
                      if (e.isEmpty && infoCollateralChangeNotifier.isKelurahanMandatory()) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    onTap: () {
                      infoCollateralChangeNotifier.searchKelurahan(context);
                    },
                    controller: infoCollateralChangeNotifier.controllerKelurahan,
                    readOnly: true,
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                        labelText: 'Kelurahan',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: infoCollateralChangeNotifier.isKelurahanDakor ? Colors.purple : Colors.grey)),
                        disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: infoCollateralChangeNotifier.isKelurahanDakor ? Colors.purple : Colors.grey))
                    ),
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  // visible: infoCollateralChangeNotifier.isKecamatanVisible(),
                  child: TextFormField(
                    // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                    // validator: (e) {
                    //   if (e.isEmpty) {
                    //     return "Tidak boleh kosong";
                    //   } else {
                    //     return null;
                    //   }
                    // },
                    controller: infoCollateralChangeNotifier.controllerKecamatan,
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                        labelText: 'Kecamatan',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: infoCollateralChangeNotifier.isKecamatanDakor ? Colors.purple : Colors.grey)),
                        disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: infoCollateralChangeNotifier.isKecamatanDakor ? Colors.purple : Colors.grey)),
                        filled: !infoCollateralChangeNotifier.enableTf,
                        fillColor: !infoCollateralChangeNotifier.enableTf
                            ? Colors.black12
                            : Colors.white
                    ),
                    enabled: false,
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  // visible: infoCollateralChangeNotifier.isKotaVisible(),
                  child: TextFormField(
                    // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                    // validator: (e) {
                    //   if (e.isEmpty) {
                    //     return "Tidak boleh kosong";
                    //   } else {
                    //     return null;
                    //   }
                    // },
                    controller: infoCollateralChangeNotifier.controllerKota,
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                        labelText: 'Kabupaten/Kota',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: infoCollateralChangeNotifier.isKabKotDakor ? Colors.purple : Colors.grey)),
                        disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: infoCollateralChangeNotifier.isKabKotDakor ? Colors.purple : Colors.grey)),
                        filled: !infoCollateralChangeNotifier.enableTf,
                        fillColor: !infoCollateralChangeNotifier.enableTf
                            ? Colors.black12
                            : Colors.white
                    ),
                    enabled: false,
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Row(
                  children: [
                    Expanded(
                      flex: 7,
                      child: Visibility(
                        // visible: infoCollateralChangeNotifier.isProvVisible(),
                        child: TextFormField(
                          // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                          // validator: (e) {
                          //   if (e.isEmpty) {
                          //     return "Tidak boleh kosong";
                          //   } else {
                          //     return null;
                          //   }
                          // },
                          controller: infoCollateralChangeNotifier.controllerProv,
                          style: TextStyle(color: Colors.black),
                          decoration: InputDecoration(
                              labelText: 'Provinsi',
                              labelStyle: TextStyle(color: Colors.black),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: infoCollateralChangeNotifier.isProvinsiDakor ? Colors.purple : Colors.grey)),
                              disabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: infoCollateralChangeNotifier.isProvinsiDakor ? Colors.purple : Colors.grey)),
                              filled: !infoCollateralChangeNotifier.enableTf,
                              fillColor: !infoCollateralChangeNotifier.enableTf
                                  ? Colors.black12
                                  : Colors.white
                          ),
                          enabled: false,
                        ),
                      ),
                    ),
                    SizedBox(width: 8),
                    Expanded(
                      flex: 3,
                      child: Visibility(
                        // visible: infoCollateralChangeNotifier.isPostalCodeVisible(),
                        child: TextFormField(
                          // autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                          // validator: (e) {
                          //   if (e.isEmpty) {
                          //     return "Tidak boleh kosong";
                          //   } else {
                          //     return null;
                          //   }
                          // },
                          controller: infoCollateralChangeNotifier.controllerPostalCode,
                          style: TextStyle(color: Colors.black),
                          decoration: InputDecoration(
                              labelText: 'Kode Pos',
                              labelStyle: TextStyle(color: Colors.black),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: infoCollateralChangeNotifier.isKodePosDakor ? Colors.purple : Colors.grey)),
                              disabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: infoCollateralChangeNotifier.isKodePosDakor ? Colors.purple : Colors.grey)),
                              filled: !infoCollateralChangeNotifier.enableTf,
                              fillColor: !infoCollateralChangeNotifier.enableTf
                                  ? Colors.black12
                                  : Colors.white
                          ),
                          enabled: false,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                Visibility(
                  // visible: infoCollateralChangeNotifier.isAddressFromMapVisible(),
                  child: TextFormField(
                    autovalidate: infoCollateralChangeNotifier.autoValidateProp,
                    validator: (e) {
                      if (e.isEmpty && infoCollateralChangeNotifier.isAddressFromMapMandatory()) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    readOnly: true,
                    onTap: (){
                      infoCollateralChangeNotifier.setLocationAddressByMap(context);
                    },
                    maxLines: 3,
                    controller: infoCollateralChangeNotifier.controllerAddressFromMap,
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                      labelText: 'Geolocation Alamat',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: infoCollateralChangeNotifier.isDataLatLongDakor ? Colors.purple : Colors.grey)),
                      disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: infoCollateralChangeNotifier.isDataLatLongDakor ? Colors.purple : Colors.grey)),
                    ),
                    keyboardType: TextInputType.number,
                  ),
                ),
              ],
            );
          },
        );
      },
    );
  }
}
