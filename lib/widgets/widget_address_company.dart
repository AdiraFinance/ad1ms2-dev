import 'package:ad1ms2_dev/models/address_guarantor_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_company_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../main.dart';

class WidgetAddressCompany extends StatefulWidget {
    final int flag;
    final int index;
    final AddressModelCompany addressModel;
    final editVal;
    final int typeAddress;

    const WidgetAddressCompany({Key key, this.flag, this.index, this.addressModel, this.editVal, this.typeAddress});

    @override
    _WidgetAddressCompanyState createState() => _WidgetAddressCompanyState();
}

class _WidgetAddressCompanyState extends State<WidgetAddressCompany> {
  @override
  void initState() {
    super.initState();
    Provider.of<FormMAddAddressCompanyChangeNotifier>(context, listen: false).getDataFromDashboard(context);
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
        data: ThemeData(
            primaryColor: Colors.black,
            accentColor: myPrimaryColor,
            fontFamily: "NunitoSans",
            primarySwatch: primaryOrange
        ),
      child: Scaffold(
          appBar: AppBar(
              title: Text(
                  widget.flag == 0
                      ? "Tambah Alamat"
                      : "Edit Alamat",
                  style: TextStyle(color: Colors.black)),
              centerTitle: true,
              backgroundColor: myPrimaryColor,
              iconTheme: IconThemeData(color: Colors.black),
          ),
          body: SingleChildScrollView(
              padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width / 27,
                  vertical: MediaQuery.of(context).size.height / 57),
              child: FutureBuilder(
                  future: widget.editVal,
                  builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                          return Center(
                              child: CircularProgressIndicator(),
                          );
                      }
                      return Consumer<FormMAddAddressCompanyChangeNotifier>(
                          builder: (context, formMAddGuarantorAddress, _) {
                              return Form(
                                  key: formMAddGuarantorAddress.key,
                                  onWillPop: _onWillPop,
                                  child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                          Visibility(
                                            visible: formMAddGuarantorAddress.setAddressTypeShow(widget.typeAddress),
                                            child: DropdownButtonFormField<JenisAlamatModel>(
                                                autovalidate: formMAddGuarantorAddress.autoValidate,
                                                validator: (e) {
                                                    if (e == null && formMAddGuarantorAddress.setAddressTypeMandatory(widget.typeAddress)) {
                                                        return "Silahkan pilih jenis alamat";
                                                    } else {
                                                        return null;
                                                    }
                                                },
                                                value: formMAddGuarantorAddress.jenisAlamatSelected,
                                                onChanged: (value) {
                                                    formMAddGuarantorAddress.jenisAlamatSelected =value;
                                                },
                                                onTap: () {
                                                    FocusManager.instance.primaryFocus.unfocus();
                                                },
                                                decoration: InputDecoration(
                                                    labelText: "Jenis Alamat",
                                                    border: OutlineInputBorder(),
                                                    enabledBorder: OutlineInputBorder(
                                                        borderSide: BorderSide(color: formMAddGuarantorAddress.isAddressTypeDakor ? Colors.purple : Colors.grey)),
                                                    disabledBorder: OutlineInputBorder(
                                                        borderSide: BorderSide(color: formMAddGuarantorAddress.isAddressTypeDakor ? Colors.purple : Colors.grey)),
                                                    contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                ),
                                                items: formMAddGuarantorAddress.listJenisAlamat.map((value) {
                                                    return DropdownMenuItem<JenisAlamatModel>(
                                                        value: value,
                                                        child: Text(
                                                            value.DESKRIPSI,
                                                            overflow: TextOverflow.ellipsis,
                                                        ),
                                                    );
                                                }).toList()
                                            ),
                                          ),
                                          Visibility(visible: formMAddGuarantorAddress.setAddressTypeShow(widget.typeAddress),child: SizedBox(height: MediaQuery.of(context).size.height/47)),
                                          Visibility(
                                            visible: formMAddGuarantorAddress.setAddressShow(widget.typeAddress),
                                            child: TextFormField(
                                                autovalidate: formMAddGuarantorAddress.autoValidate,
                                                validator: (e) {
                                                    if (e.isEmpty && formMAddGuarantorAddress.setAddressMandatory(widget.typeAddress)) {
                                                        return "Tidak boleh kosong";
                                                    } else {
                                                        return null;
                                                    }
                                                },
                                                controller: formMAddGuarantorAddress.controllerAlamat,
                                                enabled: formMAddGuarantorAddress.enableTfAddress,
                                                style: TextStyle(color: Colors.black),
                                                decoration: InputDecoration(
                                                    filled: !formMAddGuarantorAddress.enableTfAddress,
                                                    fillColor: !formMAddGuarantorAddress.enableTfAddress
                                                        ? Colors.black12
                                                        : Colors.white,
                                                    labelText: 'Alamat',
                                                    labelStyle: TextStyle(color: Colors.black),
                                                    border: OutlineInputBorder(
                                                        borderRadius: BorderRadius.circular(8)),
                                                    enabledBorder: OutlineInputBorder(
                                                        borderSide: BorderSide(color: formMAddGuarantorAddress.isAddressDakor ? Colors.purple : Colors.grey)),
                                                    disabledBorder: OutlineInputBorder(
                                                        borderSide: BorderSide(color: formMAddGuarantorAddress.isAddressDakor ? Colors.purple : Colors.grey)),
                                                ),
                                                maxLines: 3,
                                                keyboardType: TextInputType.text,
                                                textCapitalization: TextCapitalization.characters,
                                                inputFormatters: [
                                                    FilteringTextInputFormatter.allow(RegExp('[a-zA-Z  \`0-9./]')),
                                                ],
                                            ),
                                          ),
                                          Visibility(visible: formMAddGuarantorAddress.setAddressShow(widget.typeAddress),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                                          Visibility(
                                            visible: formMAddGuarantorAddress.setRTShow(widget.typeAddress) || formMAddGuarantorAddress.setRWShow(widget.typeAddress),
                                            child: Row(
                                                children: [
                                                    Visibility(
                                                      visible: formMAddGuarantorAddress.setRTShow(widget.typeAddress),
                                                      child: Expanded(
                                                          flex: 5,
                                                          child: TextFormField(
                                                              autovalidate: formMAddGuarantorAddress.autoValidate,
                                                              validator: (e) {
                                                                  if (e.isEmpty && formMAddGuarantorAddress.setRTMandatory(widget.typeAddress)) {
                                                                      return "Tidak boleh kosong";
                                                                  } else {
                                                                      return null;
                                                                  }
                                                              },
                                                              enabled: formMAddGuarantorAddress.enableTfRT,
                                                              controller: formMAddGuarantorAddress.controllerRT,
                                                              style: TextStyle(color: Colors.black),
                                                              decoration: InputDecoration(
                                                                  filled: !formMAddGuarantorAddress.enableTfRT,
                                                                  fillColor: !formMAddGuarantorAddress.enableTfRT
                                                                      ? Colors.black12
                                                                      : Colors.white,
                                                                  labelText: 'RT',
                                                                  labelStyle: TextStyle(color: Colors.black),
                                                                  border: OutlineInputBorder(
                                                                      borderRadius: BorderRadius.circular(8)),
                                                                  enabledBorder: OutlineInputBorder(
                                                                      borderSide: BorderSide(color: formMAddGuarantorAddress.isRTDakor ? Colors.purple : Colors.grey)),
                                                                  disabledBorder: OutlineInputBorder(
                                                                      borderSide: BorderSide(color: formMAddGuarantorAddress.isRTDakor ? Colors.purple : Colors.grey)),
                                                              ),
                                                              inputFormatters: [
                                                                  WhitelistingTextInputFormatter.digitsOnly,
                                                                  LengthLimitingTextInputFormatter(3),
                                                              ],
                                                              keyboardType: TextInputType.number,
                                                          ),
                                                      ),
                                                    ),
                                                    Visibility(visible: formMAddGuarantorAddress.setRTShow(widget.typeAddress),child: SizedBox(width: 8)),
                                                    Visibility(
                                                      visible: formMAddGuarantorAddress.setRWShow(widget.typeAddress),
                                                      child: Expanded(
                                                          flex: 5,
                                                          child: TextFormField(
                                                              autovalidate: formMAddGuarantorAddress.autoValidate,
                                                              validator: (e) {
                                                                  if (e.isEmpty && formMAddGuarantorAddress.setRWMandatory(widget.typeAddress)) {
                                                                      return "Tidak boleh kosong";
                                                                  } else {
                                                                      return null;
                                                                  }
                                                              },
                                                              enabled: formMAddGuarantorAddress.enableTfRW,
                                                              controller: formMAddGuarantorAddress.controllerRW,
                                                              style: TextStyle(color: Colors.black),
                                                              decoration: InputDecoration(
                                                                  filled: !formMAddGuarantorAddress.enableTfRW,
                                                                  fillColor: !formMAddGuarantorAddress.enableTfRW
                                                                      ? Colors.black12
                                                                      : Colors.white,
                                                                  labelText: 'RW',
                                                                  labelStyle: TextStyle(color: Colors.black),
                                                                  border: OutlineInputBorder(
                                                                      borderRadius: BorderRadius.circular(8)),
                                                                  enabledBorder: OutlineInputBorder(
                                                                      borderSide: BorderSide(color: formMAddGuarantorAddress.isRWDakor ? Colors.purple : Colors.grey)),
                                                                  disabledBorder: OutlineInputBorder(
                                                                      borderSide: BorderSide(color: formMAddGuarantorAddress.isRWDakor ? Colors.purple : Colors.grey)),
                                                              ),
                                                              inputFormatters: [
                                                                  WhitelistingTextInputFormatter.digitsOnly,
                                                                  LengthLimitingTextInputFormatter(3),
                                                              ],
                                                              keyboardType: TextInputType.number,
                                                          ),
                                                      ),
                                                    )
                                                ],
                                            ),
                                          ),
                                          Visibility(visible: formMAddGuarantorAddress.setRTShow(widget.typeAddress) || formMAddGuarantorAddress.setRWShow(widget.typeAddress),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                                          Visibility(
                                            visible: formMAddGuarantorAddress.setKelurahanShow(widget.typeAddress),
                                            child: FocusScope(
                                                node: FocusScopeNode(),
                                                child: TextFormField(
                                                    autovalidate: formMAddGuarantorAddress.autoValidate,
                                                    validator: (e) {
                                                        if (e.isEmpty && formMAddGuarantorAddress.setKelurahanMandatory(widget.typeAddress)) {
                                                            return "Tidak boleh kosong";
                                                        } else {
                                                            return null;
                                                        }
                                                    },
                                                    onTap: () {
                                                        FocusManager.instance.primaryFocus.unfocus();
                                                        formMAddGuarantorAddress.searchKelurahan(context);
                                                    },
                                                    enabled: formMAddGuarantorAddress.enableTfKelurahan,
                                                    controller: formMAddGuarantorAddress.controllerKelurahan,
                                                    style: TextStyle(color: Colors.black),
                                                    decoration: InputDecoration(
                                                        filled: !formMAddGuarantorAddress.enableTfKelurahan,
                                                        fillColor: !formMAddGuarantorAddress.enableTfKelurahan
                                                            ? Colors.black12
                                                            : Colors.white,
                                                        labelText: 'Kelurahan',
                                                        labelStyle: TextStyle(color: Colors.black),
                                                        border: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(8)),
                                                        enabledBorder: OutlineInputBorder(
                                                            borderSide: BorderSide(color: formMAddGuarantorAddress.isKelurahanDakor ? Colors.purple : Colors.grey)),
                                                        disabledBorder: OutlineInputBorder(
                                                            borderSide: BorderSide(color: formMAddGuarantorAddress.isKelurahanDakor ? Colors.purple : Colors.grey)),
                                                    ),
                                                ),
                                            ),
                                          ),
                                          Visibility(visible: formMAddGuarantorAddress.setKelurahanShow(widget.typeAddress),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                                          Visibility(
                                            visible: formMAddGuarantorAddress.setKecamatanShow(widget.typeAddress),
                                            child: TextFormField(
                                                autovalidate: formMAddGuarantorAddress.autoValidate,
                                                validator: (e) {
                                                    if (e.isEmpty && formMAddGuarantorAddress.setKecamatanMandatory(widget.typeAddress)) {
                                                        return "Tidak boleh kosong";
                                                    } else {
                                                        return null;
                                                    }
                                                },
                                                // enabled: formMAddGuarantorAddress.enableTfKecamatan,
                                                controller: formMAddGuarantorAddress.controllerKecamatan,
                                                style: TextStyle(color: Colors.black),
                                                decoration: InputDecoration(
                                                    filled: !formMAddGuarantorAddress.enableTfKecamatan,
                                                    fillColor: !formMAddGuarantorAddress.enableTfKecamatan
                                                        ? Colors.black12
                                                        : Colors.white,
                                                    labelText: 'Kecamatan',
                                                    labelStyle: TextStyle(color: Colors.black),
                                                    border: OutlineInputBorder(
                                                        borderRadius: BorderRadius.circular(8)),
                                                    disabledBorder: OutlineInputBorder(
                                                        borderSide: BorderSide(color: formMAddGuarantorAddress.isKecamatanDakor ? Colors.purple : Colors.grey)),
                                                    enabledBorder: OutlineInputBorder(
                                                        borderSide: BorderSide(color: formMAddGuarantorAddress.isKecamatanDakor ? Colors.purple : Colors.grey)),
                                                ),
                                                enabled: false,
                                            ),
                                          ),
                                          Visibility(visible: formMAddGuarantorAddress.setKecamatanShow(widget.typeAddress),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                                          Visibility(
                                            visible: formMAddGuarantorAddress.setKabKotShow(widget.typeAddress),
                                            child: TextFormField(
                                                autovalidate: formMAddGuarantorAddress.autoValidate,
                                                validator: (e) {
                                                    if (e.isEmpty && formMAddGuarantorAddress.setKabKotMandatory(widget.typeAddress)) {
                                                        return "Tidak boleh kosong";
                                                    } else {
                                                        return null;
                                                    }
                                                },
                                                controller: formMAddGuarantorAddress.controllerKota,
                                                style: TextStyle(color: Colors.black),
                                                decoration: InputDecoration(
                                                    filled: !formMAddGuarantorAddress.enableTfKota,
                                                    fillColor: !formMAddGuarantorAddress.enableTfKota
                                                        ? Colors.black12
                                                        : Colors.white,
                                                    labelText: 'Kabupaten/Kota',
                                                    labelStyle: TextStyle(color: Colors.black),
                                                    border: OutlineInputBorder(
                                                        borderRadius: BorderRadius.circular(8)),
                                                    disabledBorder: OutlineInputBorder(
                                                        borderSide: BorderSide(color: formMAddGuarantorAddress.isKabKotDakor ? Colors.purple : Colors.grey)),
                                                    enabledBorder: OutlineInputBorder(
                                                        borderSide: BorderSide(color: formMAddGuarantorAddress.isKabKotDakor ? Colors.purple : Colors.grey)),
                                                ),
                                                enabled: false,
                                            ),
                                          ),
                                          Visibility(visible: formMAddGuarantorAddress.setKabKotShow(widget.typeAddress),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                                          Visibility(
                                            visible: formMAddGuarantorAddress.setProvinsiShow(widget.typeAddress) || formMAddGuarantorAddress.setKodePosShow(widget.typeAddress),
                                            child: Row(
                                                children: [
                                                    Visibility(
                                                      visible: formMAddGuarantorAddress.setProvinsiShow(widget.typeAddress),
                                                      child: Expanded(
                                                          flex: 7,
                                                          child: TextFormField(
                                                              autovalidate: formMAddGuarantorAddress.autoValidate,
                                                              validator: (e) {
                                                                  if (e.isEmpty && formMAddGuarantorAddress.setProvinsiMandatory(widget.typeAddress)) {
                                                                      return "Tidak boleh kosong";
                                                                  } else {
                                                                      return null;
                                                                  }
                                                              },
                                                              controller: formMAddGuarantorAddress.controllerProvinsi,
                                                              style: TextStyle(color: Colors.black),
                                                              decoration: InputDecoration(
                                                                  filled: !formMAddGuarantorAddress.enableTfProv,
                                                                  fillColor: !formMAddGuarantorAddress.enableTfProv
                                                                      ? Colors.black12
                                                                      : Colors.white,
                                                                  labelText: 'Provinsi',
                                                                  labelStyle: TextStyle(color: Colors.black),
                                                                  border: OutlineInputBorder(
                                                                      borderRadius: BorderRadius.circular(8)),
                                                                  disabledBorder: OutlineInputBorder(
                                                                      borderSide: BorderSide(color: formMAddGuarantorAddress.isProvinsiDakor ? Colors.purple : Colors.grey)),
                                                                  enabledBorder: OutlineInputBorder(
                                                                      borderSide: BorderSide(color: formMAddGuarantorAddress.isProvinsiDakor ? Colors.purple : Colors.grey)),
                                                              ),
                                                              enabled: false,
                                                          ),
                                                      ),
                                                    ),
                                                    Visibility(visible: formMAddGuarantorAddress.setProvinsiShow(widget.typeAddress),child: SizedBox(width: 8)),
                                                    Visibility(
                                                      visible: formMAddGuarantorAddress.setKodePosShow(widget.typeAddress),
                                                      child: Expanded(
                                                          flex: 3,
                                                          child: TextFormField(
                                                              autovalidate: formMAddGuarantorAddress.autoValidate,
                                                              validator: (e) {
                                                                  if (e.isEmpty && formMAddGuarantorAddress.setKodePosMandatory(widget.typeAddress)) {
                                                                      return "Tidak boleh kosong";
                                                                  } else {
                                                                      return null;
                                                                  }
                                                              },
                                                              controller: formMAddGuarantorAddress.controllerPostalCode,
                                                              style: TextStyle(color: Colors.black),
                                                              decoration: InputDecoration(
                                                                  filled: !formMAddGuarantorAddress.enableTfPostalCode,
                                                                  fillColor: !formMAddGuarantorAddress.enableTfPostalCode
                                                                      ? Colors.black12
                                                                      : Colors.white,
                                                                  labelText: 'Kode Pos',
                                                                  labelStyle: TextStyle(color: Colors.black),
                                                                  border: OutlineInputBorder(
                                                                      borderRadius: BorderRadius.circular(8)),
                                                                  disabledBorder: OutlineInputBorder(
                                                                      borderSide: BorderSide(color: formMAddGuarantorAddress.isKodePosDakor ? Colors.purple : Colors.grey)),
                                                                  enabledBorder: OutlineInputBorder(
                                                                      borderSide: BorderSide(color: formMAddGuarantorAddress.isKodePosDakor ? Colors.purple : Colors.grey)),
                                                              ),
                                                              enabled: false,
                                                          ),
                                                      ),
                                                    ),
                                                ],
                                            ),
                                          ),
                                          Visibility(visible: formMAddGuarantorAddress.setProvinsiShow(widget.typeAddress) || formMAddGuarantorAddress.setKodePosShow(widget.typeAddress),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                                          TextFormField(
                                              // autovalidate: formMAddAlamatKorespon.autoValidate,
                                              // validator: (e) {
                                              //     if (e.isEmpty) {
                                              //         return "Tidak boleh kosong";
                                              //     } else {
                                              //         return null;
                                              //     }
                                              // },
                                              readOnly: true,
                                              onTap: (){
                                                  formMAddGuarantorAddress.setLocationAddressByMap(context);
                                              },
                                              maxLines: 3,
                                              controller: formMAddGuarantorAddress.controllerAddressFromMap,
                                              style: TextStyle(color: Colors.black),
                                              decoration: InputDecoration(
                                                  labelText: 'Geolocation Alamat',
                                                  labelStyle: TextStyle(color: Colors.black),
                                                  border: OutlineInputBorder(
                                                      borderRadius: BorderRadius.circular(8)),
                                                  enabledBorder: OutlineInputBorder(
                                                      borderSide: BorderSide(color: formMAddGuarantorAddress.isDataLatLongDakor ? Colors.purple : Colors.grey)),
                                                  disabledBorder: OutlineInputBorder(
                                                      borderSide: BorderSide(color: formMAddGuarantorAddress.isDataLatLongDakor ? Colors.purple : Colors.grey)),
                                              ),
                                              keyboardType: TextInputType.number,
                                          ),
                                          SizedBox(height: MediaQuery.of(context).size.height / 47),
                                          Visibility(
                                            visible: formMAddGuarantorAddress.setTelp1AreaShow(widget.typeAddress) || formMAddGuarantorAddress.setTelp1Show(widget.typeAddress),
                                            child: Row(
                                                children: [
                                                    Visibility(
                                                      visible: formMAddGuarantorAddress.setTelp1AreaShow(widget.typeAddress),
                                                      child: Expanded(
                                                          flex: 4,
                                                          child: TextFormField(
                                                              autovalidate: formMAddGuarantorAddress.autoValidate,
                                                              validator: (e) {
                                                                  if (e.isEmpty && formMAddGuarantorAddress.setTelp1AreaMandatory(widget.typeAddress)) {
                                                                      return "Tidak boleh kosong";
                                                                  } else {
                                                                      return null;
                                                                  }
                                                              },
                                                              onChanged: (e) {
                                                                  formMAddGuarantorAddress.checkValidCodeArea1(e);
                                                              },
                                                              keyboardType: TextInputType.number,
                                                              inputFormatters: [
                                                                  WhitelistingTextInputFormatter.digitsOnly,
                                                                  LengthLimitingTextInputFormatter(4),
                                                              ],
                                                              enabled: formMAddGuarantorAddress.enableTfTelephone1Area,
                                                              controller: formMAddGuarantorAddress.controllerTelephone1Area,
                                                              style: TextStyle(color: Colors.black),
                                                              decoration: InputDecoration(
                                                                  filled: !formMAddGuarantorAddress.enableTfTelephone1Area,
                                                                  fillColor: !formMAddGuarantorAddress.enableTfTelephone1Area
                                                                      ? Colors.black12
                                                                      : Colors.white,
                                                                  labelText: 'Telepon 1 (Area)',
                                                                  labelStyle: TextStyle(color: Colors.black),
                                                                  border: OutlineInputBorder(
                                                                      borderRadius: BorderRadius.circular(8)),
                                                                  enabledBorder: OutlineInputBorder(
                                                                      borderSide: BorderSide(color: formMAddGuarantorAddress.isTelp1AreaDakor ? Colors.purple : Colors.grey)),
                                                                  disabledBorder: OutlineInputBorder(
                                                                      borderSide: BorderSide(color: formMAddGuarantorAddress.isTelp1AreaDakor ? Colors.purple : Colors.grey)),
                                                              ),
                                                          ),
                                                      ),
                                                    ),
                                                    Visibility(visible: formMAddGuarantorAddress.setTelp1AreaShow(widget.typeAddress),child: SizedBox(width: MediaQuery.of(context).size.width / 37)),
                                                    Visibility(
                                                      visible: formMAddGuarantorAddress.setTelp1Show(widget.typeAddress),
                                                      child: Expanded(
                                                          flex: 6,
                                                          child: TextFormField(
                                                              autovalidate: formMAddGuarantorAddress.autoValidate,
                                                              validator: (e) {
                                                                  if (e.isEmpty && formMAddGuarantorAddress.setTelp1Mandatory(widget.typeAddress)) {
                                                                      return "Tidak boleh kosong";
                                                                  } else {
                                                                      return null;
                                                                  }
                                                              },
                                                              enabled: formMAddGuarantorAddress.enableTfPhone1,
                                                              keyboardType: TextInputType.number,
                                                              inputFormatters: [
                                                                  WhitelistingTextInputFormatter.digitsOnly,
                                                                  // LengthLimitingTextInputFormatter(10),
                                                              ],
                                                              controller: formMAddGuarantorAddress.controllerTelephone1,
                                                              style: TextStyle(color: Colors.black),
                                                              decoration: InputDecoration(
                                                                  filled: !formMAddGuarantorAddress.enableTfPhone1,
                                                                  fillColor: !formMAddGuarantorAddress.enableTfPhone1
                                                                      ? Colors.black12
                                                                      : Colors.white,
                                                                  labelText: 'Telepon 1',
                                                                  labelStyle: TextStyle(color: Colors.black),
                                                                  border: OutlineInputBorder(
                                                                      borderRadius: BorderRadius.circular(8)),
                                                                  enabledBorder: OutlineInputBorder(
                                                                      borderSide: BorderSide(color: formMAddGuarantorAddress.isTelp1Dakor ? Colors.purple : Colors.grey)),
                                                                  disabledBorder: OutlineInputBorder(
                                                                      borderSide: BorderSide(color: formMAddGuarantorAddress.isTelp1Dakor ? Colors.purple : Colors.grey)),
                                                              ),
                                                          ),
                                                      ),
                                                    ),
                                                ],
                                            ),
                                          ),
                                          Visibility(visible: formMAddGuarantorAddress.setTelp1AreaShow(widget.typeAddress) || formMAddGuarantorAddress.setTelp1Show(widget.typeAddress),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                                          Visibility(
                                            visible: formMAddGuarantorAddress.setTelp2AreaShow(widget.typeAddress) || formMAddGuarantorAddress.setTelp2Show(widget.typeAddress),
                                            child: Row(
                                                children: [
                                                    Visibility(
                                                      visible: formMAddGuarantorAddress.setTelp2AreaShow(widget.typeAddress),
                                                      child: Expanded(
                                                          flex: 4,
                                                          child: TextFormField(
                                                              autovalidate: formMAddGuarantorAddress.autoValidate,
                                                              validator: (e) {
                                                                  if (e.isEmpty && formMAddGuarantorAddress.setTelp2AreaMandatory(widget.typeAddress)) {
                                                                      return "Tidak boleh kosong";
                                                                  } else {
                                                                      return null;
                                                                  }
                                                              },
                                                              onChanged: (e) {
                                                                  formMAddGuarantorAddress.checkValidCodeArea2(e);
                                                              },
                                                              keyboardType: TextInputType.number,
                                                              inputFormatters: [
                                                                  WhitelistingTextInputFormatter.digitsOnly,
                                                                  LengthLimitingTextInputFormatter(4),
                                                              ],
                                                              enabled: formMAddGuarantorAddress.enableTfTelephone2Area,
                                                              controller: formMAddGuarantorAddress.controllerTelephone2Area,
                                                              style: TextStyle(color: Colors.black),
                                                              decoration: InputDecoration(
                                                                  filled: !formMAddGuarantorAddress.enableTfTelephone2Area,
                                                                  fillColor: !formMAddGuarantorAddress.enableTfTelephone2Area
                                                                      ? Colors.black12
                                                                      : Colors.white,
                                                                  labelText: 'Telepon 2 (Area)',
                                                                  labelStyle: TextStyle(color: Colors.black),
                                                                  border: OutlineInputBorder(
                                                                      borderRadius: BorderRadius.circular(8)),
                                                                  enabledBorder: OutlineInputBorder(
                                                                      borderSide: BorderSide(color: formMAddGuarantorAddress.isTelp2AreaDakor ? Colors.purple : Colors.grey)),
                                                                  disabledBorder: OutlineInputBorder(
                                                                      borderSide: BorderSide(color: formMAddGuarantorAddress.isTelp2AreaDakor ? Colors.purple : Colors.grey)),
                                                              ),
                                                          ),
                                                      ),
                                                    ),
                                                    Visibility(visible: formMAddGuarantorAddress.setTelp2AreaShow(widget.typeAddress),child: SizedBox(width: MediaQuery.of(context).size.width / 37)),
                                                    Visibility(
                                                      visible: formMAddGuarantorAddress.setTelp2Show(widget.typeAddress),
                                                      child: Expanded(
                                                          flex: 6,
                                                          child: TextFormField(
                                                              autovalidate: formMAddGuarantorAddress.autoValidate,
                                                              validator: (e) {
                                                                  if (e.isEmpty && formMAddGuarantorAddress.setTelp2Mandatory(widget.typeAddress)) {
                                                                      return "Tidak boleh kosong";
                                                                  } else {
                                                                      return null;
                                                                  }
                                                              },
                                                              keyboardType: TextInputType.number,
                                                              inputFormatters: [
                                                                  WhitelistingTextInputFormatter.digitsOnly,
                                                                  // LengthLimitingTextInputFormatter(10),
                                                              ],
                                                              enabled: formMAddGuarantorAddress.enableTfPhone2,
                                                              controller: formMAddGuarantorAddress.controllerTelephone2,
                                                              style: TextStyle(color: Colors.black),
                                                              decoration: InputDecoration(
                                                                  filled: !formMAddGuarantorAddress.enableTfPhone2,
                                                                  fillColor: !formMAddGuarantorAddress.enableTfPhone2
                                                                      ? Colors.black12
                                                                      : Colors.white,
                                                                  labelText: 'Telepon 2',
                                                                  labelStyle: TextStyle(color: Colors.black),
                                                                  border: OutlineInputBorder(
                                                                      borderRadius: BorderRadius.circular(8)),
                                                                  enabledBorder: OutlineInputBorder(
                                                                      borderSide: BorderSide(color: formMAddGuarantorAddress.isTelp2Dakor ? Colors.purple : Colors.grey)),
                                                                  disabledBorder: OutlineInputBorder(
                                                                      borderSide: BorderSide(color: formMAddGuarantorAddress.isTelp2Dakor ? Colors.purple : Colors.grey)),
                                                              ),
                                                          ),
                                                      ),
                                                    ),
                                                ],
                                            ),
                                          ),
                                          Visibility(visible: formMAddGuarantorAddress.setTelp2AreaShow(widget.typeAddress) || formMAddGuarantorAddress.setTelp2Show(widget.typeAddress),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                                          Visibility(
                                            visible: formMAddGuarantorAddress.setFaxAreaShow(widget.typeAddress) || formMAddGuarantorAddress.setFaxShow(widget.typeAddress),
                                            child: Row(
                                                children: [
                                                    Visibility(
                                                      visible: formMAddGuarantorAddress.setFaxAreaShow(widget.typeAddress),
                                                      child: Expanded(
                                                          flex: 4,
                                                          child: TextFormField(
                                                              autovalidate:formMAddGuarantorAddress.autoValidate,
                                                              validator: (e) {
                                                                  if (e.isEmpty && formMAddGuarantorAddress.setFaxAreaMandatory(widget.typeAddress)) {
                                                                      return "Tidak boleh kosong";
                                                                  } else {
                                                                      return null;
                                                                  }
                                                              },
                                                              onChanged: (e) {
                                                                  formMAddGuarantorAddress.checkValidCodeAreaFax(e);
                                                              },
                                                              keyboardType: TextInputType.number,
                                                              inputFormatters: [
                                                                  WhitelistingTextInputFormatter.digitsOnly,
                                                                  LengthLimitingTextInputFormatter(4),
                                                              ],
                                                              enabled: formMAddGuarantorAddress.enableTfFaxArea,
                                                              controller: formMAddGuarantorAddress.controllerFaxArea,
                                                              style: TextStyle(color: Colors.black),
                                                              decoration: InputDecoration(
                                                                  filled: !formMAddGuarantorAddress.enableTfFaxArea,
                                                                  fillColor: !formMAddGuarantorAddress.enableTfFaxArea
                                                                      ? Colors.black12
                                                                      : Colors.white,
                                                                  labelText: 'Fax (Area)',
                                                                  labelStyle: TextStyle(color: Colors.black),
                                                                  border: OutlineInputBorder(
                                                                      borderRadius: BorderRadius.circular(8)),
                                                                  enabledBorder: OutlineInputBorder(
                                                                      borderSide: BorderSide(color: formMAddGuarantorAddress.isFaxAreaDakor ? Colors.purple : Colors.grey)),
                                                                  disabledBorder: OutlineInputBorder(
                                                                      borderSide: BorderSide(color: formMAddGuarantorAddress.isFaxAreaDakor ? Colors.purple : Colors.grey)),
                                                              ),
                                                          ),
                                                      ),
                                                    ),
                                                    Visibility(visible: formMAddGuarantorAddress.setFaxAreaShow(widget.typeAddress),child: SizedBox(width: MediaQuery.of(context).size.width / 37)),
                                                    Visibility(
                                                      visible: formMAddGuarantorAddress.setFaxShow(widget.typeAddress),
                                                      child: Expanded(
                                                          flex: 6,
                                                          child: TextFormField(
                                                              autovalidate: formMAddGuarantorAddress.autoValidate,
                                                              validator: (e) {
                                                                  if (e.isEmpty && formMAddGuarantorAddress.setFaxMandatory(widget.typeAddress)) {
                                                                      return "Tidak boleh kosong";
                                                                  } else {
                                                                      return null;
                                                                  }
                                                              },
                                                              keyboardType: TextInputType.number,
                                                              inputFormatters: [
                                                                  WhitelistingTextInputFormatter.digitsOnly,
                                                                  // LengthLimitingTextInputFormatter(10),
                                                              ],
                                                              enabled: formMAddGuarantorAddress.enableTfFax,
                                                              controller: formMAddGuarantorAddress.controllerFax,
                                                              style: TextStyle(color: Colors.black),
                                                              decoration: InputDecoration(
                                                                  filled: !formMAddGuarantorAddress .enableTfFax,
                                                                  fillColor: !formMAddGuarantorAddress.enableTfFax
                                                                      ? Colors.black12
                                                                      : Colors.white,
                                                                  labelText: 'Fax',
                                                                  labelStyle: TextStyle(color: Colors.black),
                                                                  border: OutlineInputBorder(
                                                                      borderRadius: BorderRadius.circular(8)),
                                                                  enabledBorder: OutlineInputBorder(
                                                                      borderSide: BorderSide(color: formMAddGuarantorAddress.isFaxDakor ? Colors.purple : Colors.grey)),
                                                                  disabledBorder: OutlineInputBorder(
                                                                      borderSide: BorderSide(color: formMAddGuarantorAddress.isFaxDakor ? Colors.purple : Colors.grey)),
                                                              ),
                                                          ),
                                                      ),
                                                    ),
                                                ],
                                            ),
                                          ),
                                      ],
                                  ),
                              );
                          },
                      );
                  })
          ),
          bottomNavigationBar: BottomAppBar(
              elevation: 0.0,
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(8.0)),
                      color: myPrimaryColor,
                      onPressed: () {
                          Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: true)
                              .checkDataDakor(widget.addressModel, context, widget.index, widget.typeAddress);
                          if (widget.flag == 0) {
                              Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: true)
                                  .check(context, widget.flag, null, widget.typeAddress);
                          } else {
                              Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: true)
                                  .check(context, widget.flag, widget.index, widget.typeAddress);
                          }
                      },
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                              Text(widget.flag == 0 ? "SAVE" : "UPDATE",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                      letterSpacing: 1.25))
                          ],
                      ))),
          ),
      ),
    );
  }

  Future<bool> _onWillPop() async {
      var _provider = Provider.of<FormMAddAddressCompanyChangeNotifier>(
          context,
          listen: false);
      if (widget.flag == 0) {
          if (_provider.jenisAlamatSelected != null ||
              _provider.controllerAlamat.text != "" ||
              _provider.controllerRT.text != "" ||
              _provider.controllerRW.text != "" ||
              _provider.controllerKelurahan.text != "" ||
              _provider.controllerKecamatan.text != "" ||
              _provider.controllerKota.text != "" ||
              _provider.controllerProvinsi.text != "" ||
              _provider.controllerPostalCode.text != "" ||
              _provider.controllerTelephone1Area.text != "" ||
              _provider.controllerTelephone1.text != "" ||
              _provider.controllerTelephone2Area.text != "" ||
              _provider.controllerTelephone2.text != "" ||
              _provider.controllerFaxArea.text != "" ||
              _provider.controllerFax.text != "") {
              return (await showDialog(
                  context: context,
                  builder: (myContext) => AlertDialog(
                      title: new Text('Warning'),
                      content: new Text('Keluar dengan simpan perubahan?'),
                      actions: <Widget>[
                          new FlatButton(
                              onPressed: () {
                                  _provider.check(context, widget.flag, null, widget.typeAddress);
                                  Navigator.pop(context);
                              },
                              child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                          ),
                          new FlatButton(
                              onPressed: () => Navigator.of(context).pop(true),
                              child: new Text('Tidak', style: TextStyle(color: primaryOrange),),
                          ),
                      ],
                  ),
              )) ??
                  false;
          } else {
              return true;
          }
      } else {
          if (_provider.jenisAlamatSelectedTemp.KODE !=
              _provider.jenisAlamatSelected.KODE ||
              _provider.alamatTemp != _provider.controllerAlamat.text ||
              _provider.rtTemp != _provider.controllerRT.text ||
              _provider.rwTemp != _provider.controllerRW.text ||
              _provider.kelurahanTemp != _provider.controllerKelurahan.text ||
              _provider.kecamatanTemp != _provider.controllerKecamatan.text ||
              _provider.kotaTemp != _provider.controllerKota.text ||
              _provider.provinsiTemp != _provider.controllerProvinsi.text ||
              _provider.postalCodeTemp != _provider.controllerPostalCode.text ||
              _provider.phone1AreaTemp != _provider.controllerTelephone1Area.text ||
              _provider.phone1Temp != _provider.controllerTelephone1.text ||
              _provider.phone2AreaTemp != _provider.controllerTelephone2Area.text ||
              _provider.phone2Temp != _provider.controllerTelephone2.text ||
              _provider.faxAreaTemp != _provider.controllerFaxArea.text ||
              _provider.faxTemp != _provider.controllerFax.text) {
              return (await showDialog(
                  context: context,
                  builder: (myContext) => AlertDialog(
                      title: new Text('Warning'),
                      content: new Text('Keluar dengan simpan perubahan?'),
                      actions: <Widget>[
                          new FlatButton(
                              onPressed: () {
                                  _provider.check(context, widget.flag, widget.index, widget.typeAddress);
                                  Navigator.pop(context);
                              },
                              child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                          ),
                          new FlatButton(
                              onPressed: () => Navigator.of(context).pop(true),
                              child: new Text('Tidak', style: TextStyle(color: primaryOrange),),
                          ),
                      ],
                  ),
              )) ??
                  false;
          } else {
              return true;
          }
      }
  }
}
