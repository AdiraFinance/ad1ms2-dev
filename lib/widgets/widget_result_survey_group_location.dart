import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/survey_type_model.dart';
import 'package:ad1ms2_dev/shared/decimal_text_input_formatter.dart';
import 'package:ad1ms2_dev/shared/survey/result_survey_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class WidgetResultSurveyGroupLocation extends StatefulWidget {
  @override
  _WidgetResultSurveyGroupLocationState createState() => _WidgetResultSurveyGroupLocationState();
}

class _WidgetResultSurveyGroupLocationState extends State<WidgetResultSurveyGroupLocation> {

  @override
  void initState() {
    super.initState();
    Provider.of<ResultSurveyChangeNotifier>(context,listen: false).setDefaultTypeSurvey();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          fontFamily: "NunitoSans",
          accentColor: myPrimaryColor,
          primaryColor: Colors.black,
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
          appBar: AppBar(
            title: Text(
                "Lokasi",
                style: TextStyle(color: Colors.black)
            ),
            backgroundColor: myPrimaryColor,
            iconTheme: IconThemeData(
                color: Colors.black
            ),
          ),
          body: SingleChildScrollView(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width / 27,
                vertical: MediaQuery.of(context).size.height / 57),
            child: Consumer<ResultSurveyChangeNotifier>(
              builder: (context, value, _) {
                return Form(
                  onWillPop: value.onBackPressResultSurveyGroupLocation,
                  key: value.keyResultSurveyGroupLocation,
                  child: Column(
                    children: [
                      // DropdownButtonFormField<SurveyTypeModel>(
                      //     autovalidate: value.autoValidateResultSurveyGroupLocation,
                      //     validator: (e) {
                      //       if (e == null) {
                      //         return "Silahkan pilih jenis identitas";
                      //       } else {
                      //         return null;
                      //       }
                      //     },
                      //     value: value.surveyTypeSelected,
                      //     onChanged: (data) {
                      //       value.surveyTypeSelected = data;
                      //     },
                      //     onTap: () {
                      //       FocusManager.instance.primaryFocus.unfocus();
                      //     },
                      //     decoration: InputDecoration(
                      //       labelText: "Jenis Survey",
                      //       border: OutlineInputBorder(),
                      //       contentPadding:
                      //       EdgeInsets.symmetric(horizontal: 10),
                      //     ),
                      //     items: value
                      //         .listSurveyType
                      //         .map((value) {
                      //       return DropdownMenuItem<SurveyTypeModel>(
                      //         value: value,
                      //         child: Text(
                      //           value.name,
                      //           overflow: TextOverflow.ellipsis,
                      //         ),
                      //       );
                      //     }).toList()
                      // ),
                      Visibility(
                        visible: value.isSurveyTypeVisible(),
                        child: IgnorePointer(
                          ignoring: value.isSurveyTypeEnabled(),
                          child: TextFormField(
                            validator: (e) {
                              if (e.isEmpty && value.mandatoryFieldResultSurveyBerhasil) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            autovalidate: value.autoValidateResultSurveyGroupLocation,
                            controller: value.controllerSurveyType,
                            readOnly: true,
                            decoration: new InputDecoration(
                              labelText: 'Jenis Survey',
                              labelStyle: TextStyle(color: Colors.black),
                              filled: true, //value.isSurveyTypeEnabled() ? true : false
                              fillColor: Colors.black12,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8)
                              ),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: value.isSurveyTypeChanges ? Colors.purple : Colors.black)
                              ),
                              disabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: value.isSurveyTypeChanges ? Colors.purple : Colors.black)
                              ),
                            ),
                            inputFormatters: [
                              FilteringTextInputFormatter.allow(RegExp('[a-zA-Z 0-9]')),
                            ],
                          ),
                        ),
                      ),
                      Visibility(
                          visible: value.isSurveyTypeVisible(),
                          child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                      Visibility(
                        visible: value.isDistanceLocationWithSentraUnitVisible(),
                        child: IgnorePointer(
                          ignoring: value.isDistanceLocationWithSentraUnitEnabled(),
                          child: TextFormField(
                            validator: (e) {
                              if (e.isEmpty && value.mandatoryFieldResultSurveyBerhasil) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            autovalidate: value.autoValidateResultSurveyGroupLocation,
                            controller: value.controllerDistanceLocationWithSentraUnit,
                            decoration: new InputDecoration(
                              labelText: 'Jarak Lokasi Dengan Sentra/Unit (Km)',
                              labelStyle: TextStyle(color: Colors.black),
                              filled: value.isDistanceLocationWithSentraUnitEnabled() ? true : false,
                              fillColor: Colors.black12,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8)
                              ),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: value.isDistanceLocationWithSentraUnitChanges ? Colors.purple : Colors.black)
                              ),
                              disabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: value.isDistanceLocationWithSentraUnitChanges ? Colors.purple : Colors.black)
                              ),
                            ),
                            keyboardType: TextInputType.number,
                            // textAlign: TextAlign.end,
                            // onFieldSubmitted: (data) {
                            //   value.controllerDistanceLocationWithSentraUnit.text = value.formatCurrency.formatCurrency(data);
                            // },
                            textInputAction: TextInputAction.done,
                            inputFormatters: [
                              DecimalTextInputFormatter(decimalRange: 2),
                              value.amountValidator
                            ],
                          ),
                        ),
                      ),
                      Visibility(
                          visible: value.isDistanceLocationWithSentraUnitVisible(),
                          child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                      Visibility(
                        visible: value.isDistanceLocationWithDealerMerchantAXIVisible(),
                        child: IgnorePointer(
                          ignoring: value.isDistanceLocationWithDealerMerchantAXIEnabled(),
                          child: TextFormField(
                            validator: (e) {
                              if (e.isEmpty && value.mandatoryFieldResultSurveyBerhasil) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            autovalidate: value.autoValidateResultSurveyGroupLocation,
                            controller: value.controllerDistanceLocationWithDealerMerchantAXI,
                            decoration: new InputDecoration(
                              labelText: 'Jarak Lokasi Dengan Dealer/Merchant/AXI (Km)',
                              labelStyle: TextStyle(color: Colors.black),
                              filled: value.isDistanceLocationWithDealerMerchantAXIEnabled() ? true : false,
                              fillColor: Colors.black12,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8)
                              ),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: value.isDistanceLocationWithDealerMerchantAXIChanges ? Colors.purple : Colors.black)
                              ),
                              disabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: value.isDistanceLocationWithDealerMerchantAXIChanges ? Colors.purple : Colors.black)
                              ),
                            ),
                            keyboardType: TextInputType.number,
                            // textAlign: TextAlign.end,
                            // onFieldSubmitted: (data) {
                            //   value.controllerDistanceLocationWithDealerMerchantAXI.text = value.formatCurrency.formatCurrency(data);
                            // },
                            textInputAction: TextInputAction.done,
                            inputFormatters: [
                              DecimalTextInputFormatter(decimalRange: 2),
                              value.amountValidator
                            ],
                          ),
                        ),
                      ),
                      Visibility(
                          visible: value.isDistanceLocationWithDealerMerchantAXIVisible(),
                          child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                      Visibility(
                        visible: value.isDistanceLocationWithUsageObjectWithSentraUnitVisible(),
                        child: IgnorePointer(
                          ignoring: value.isDistanceLocationWithUsageObjectWithSentraUnitEnabled(),
                          child: TextFormField(
                            validator: (e) {
                              if (e.isEmpty && value.mandatoryFieldResultSurveyBerhasil) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            autovalidate: value.autoValidateResultSurveyGroupLocation,
                            controller: value.controllerDistanceLocationWithUsageObjectWithSentraUnit,
                            decoration: new InputDecoration(
                              labelText: 'Jarak Lokasi Penggunaan Objek dengan Sentra/Unit (Km)',
                              labelStyle: TextStyle(color: Colors.black),
                              filled: value.isDistanceLocationWithUsageObjectWithSentraUnitEnabled() ? true : false,
                              fillColor: Colors.black12,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8)
                              ),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: value.isDistanceLocationWithUsageObjectWithSentraUnitChanges ? Colors.purple : Colors.black)
                              ),
                              disabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: value.isDistanceLocationWithUsageObjectWithSentraUnitChanges ? Colors.purple : Colors.black)
                              ),
                            ),
                            keyboardType: TextInputType.number,
                            // textAlign: TextAlign.end,
                            // onFieldSubmitted: (data) {
                            //   value.controllerDistanceLocationWithUsageObjectWithSentraUnit.text = value.formatCurrency.formatCurrency(data);
                            // },
                            textInputAction: TextInputAction.done,
                            inputFormatters: [
                              DecimalTextInputFormatter(decimalRange: 2),
                              value.amountValidator
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
        bottomNavigationBar: BottomAppBar(
          elevation: 0.0,
          child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Consumer<ResultSurveyChangeNotifier>(
                builder: (context, resultSurveyChangeNotifier, _) {
                  return RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(8.0)),
                      color: myPrimaryColor,
                      onPressed: () {
                        resultSurveyChangeNotifier.checkResultSurveyGroupLocation(context);
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text("DONE",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: 1.25))
                        ],
                      ));
                },
              )),
        ),
      ),
    );
  }
}
