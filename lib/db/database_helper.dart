import 'dart:io';
import 'package:ad1ms2_dev/models/data_dedup_model.dart';
import 'package:ad1ms2_dev/models/manajemen_pic_sqlite_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_fee_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_insurance_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_loan_detail.dart';
import 'package:ad1ms2_dev/models/ms2_appl_object_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_objt_coll_oto_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_objt_coll_prop_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_objt_wmp_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_photo.dart';
import 'package:ad1ms2_dev/models/ms2_appl_refund_detail_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_refund_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_taksasi_model.dart';
import 'package:ad1ms2_dev/models/ms2_application_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_addr_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_family_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_income_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_ind_occupation_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_photo_detail_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_photo_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_shrhldr_com_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_shrhldr_ind_model.dart';
import 'package:ad1ms2_dev/models/ms2_customer_company_model.dart';
import 'package:ad1ms2_dev/models/ms2_customer_individu_model.dart';
import 'package:ad1ms2_dev/models/ms2_customer_model.dart';
import 'package:ad1ms2_dev/models/ms2_document_model.dart';
import 'package:ad1ms2_dev/models/ms2_grntr_comp_model.dart';
import 'package:ad1ms2_dev/models/ms2_grntr_ind_model.dart';
import 'package:ad1ms2_dev/models/ms2_lme_detail_model.dart';
import 'package:ad1ms2_dev/models/ms2_lme_model.dart';
import 'package:ad1ms2_dev/models/ms2_objek_sales_model.dart';
import 'package:ad1ms2_dev/models/ms2_objt_karoseri_model.dart';
import 'package:ad1ms2_dev/models/ms2_sby_assgnmt_model.dart';
import 'package:ad1ms2_dev/models/ms2_svy_rslt_asst_model.dart';
import 'package:ad1ms2_dev/models/ms2_svy_rslt_dtl_model.dart';
import 'package:ad1ms2_dev/models/survey_photo_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';

class DbHelper {
  Database db;
  static const NEW_DB_VERSION = 2;
  DateTime dateTime = DateTime.now();
  String createdBy;
  String orderNo;

  initDatabase() async {
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");

    SharedPreferences _preferences = await SharedPreferences.getInstance();
    int _firstInstall = _preferences.getInt("firstInstall");
    if (_firstInstall == null) {
      await deleteDatabase(path);
      _preferences.setInt("firstInstall", 1);
    }
// Check if the database exists
    var exists = await databaseExists(path);

    if (!exists) {
      // Should happen only the first time you launch your application
      print("Creating new copy from asset");

      // Make sure the parent directory exists
      try {
        await Directory(dirname(path)).create(recursive: true);
      }
      catch (e) {
        print("error di db helper ${e.toString()}");
      }

      // Copy from asset
      ByteData data = await rootBundle.load("assets/Ad1MS2.db");
      List<int> bytes =
          data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);

      // Write and flush the bytes written
      await File(path).writeAsBytes(bytes, flush: true);
      //open the newly created db
      db = await openDatabase(path, version: NEW_DB_VERSION);
    }
    else {
      db = await openDatabase(path);
      int _dbVersion = await db.getVersion();
      print("check db version $_dbVersion");
      db.close();
      if(_dbVersion < NEW_DB_VERSION){
        db = await openDatabase(path,version: NEW_DB_VERSION);
        await db.execute('CREATE TABLE Test (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, path_dir TEXT)');
        db.close();
      }
      else{
//        checkDb();
      }
    }
  }

  insertFlagStepper() async{
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    int id  = await db.rawInsert('INSERT INTO para_stepper(step, flag, form_type) VALUES(1,1,"FORM M")');
    print(id);
    db.close();
  }

  insertDedupData(Map data) async{
    print(data);
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    int id1 = await db.rawInsert(
        'INSERT INTO para_dedup_individu(nomor_identitas, nama_lengkap, tanggal_lahir,tmpt_lahir_sesuai_identitas,nama_gadis_ibu_kandung,alamat_identitas) '
            'VALUES("${data['nomor_identitas']}", "${data['nama_lengkap']}", "${data['tanggal_lahir']}","${data['tmpt_lahir_sesuai_identitas']}",'
            '"${data['nama_gadis_ibu_kandung']}","${data['alamat_identitas']}")'
    );
    print(id1);
    db.close();
  }

  Future<List> getDedupIndividuData() async{
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    List _dataDedup = await db.rawQuery("select * from para_dedup_individu");
    return _dataDedup;
  }

  insertPathDir(String pathDir) async{
    print(pathDir);
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    int _pathDir = await db.rawInsert("INSERT INTO Test(name,path_dir) VALUES('testing',$pathDir)");
    print(_pathDir);
    db.close();
  }


  // set variable orderNo
  Future<bool> setOrderNumber() async{
    bool _isSuccessSetOrderNo = false;
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    createdBy = _preferences.getString("username");
    orderNo = _preferences.getString("order_no");
    if(orderNo != null){
      _isSuccessSetOrderNo = true;
    }
    return _isSuccessSetOrderNo;
  }

  //insert form m foto header
  Future<void> insertMS2PhotoHeader(MS2ApplPhotoModel model) async{
    print("insert foto jalan");
    if(await setOrderNumber()){
      String _occupationKode = model.occupation != null ? model.occupation.KODE : "";
      String _occupationDesd = model.occupation != null ? model.occupation.DESKRIPSI : "";
      String _finTypeId = model.financial != null ? model.financial.financingTypeId : "";
      String _finTypeDesc = model.financial != null ? model.financial.financingTypeName : "";
      int _kegiatanUsaha = model.kegiatanUsaha != null ? model.kegiatanUsaha.id : null;
      String _kegiatanUsahaDesc = model.kegiatanUsaha != null ? model.kegiatanUsaha.text : "";
      int _jenisKegiatanUsaha = model.jenisKegiatanUsaha != null ? model.jenisKegiatanUsaha.id : null;
      String _jenisKegiatanUsahaDesc = model.jenisKegiatanUsaha != null ? model.jenisKegiatanUsaha.text : "";
      String _jenisKonsep = model.jenisKonsep != null ? model.jenisKonsep.id : "";
      String _jenisKonsepDesc = model.jenisKonsep != null ? model.jenisKonsep.text : "";
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawInsert("INSERT INTO MS2_CUST_PHOTO(order_no, work, work_desc, financing_type, financing_type_desc, "
          "business_activities, business_activities_desc, business_activities_type, business_activities_type_desc, concept_type, "
          "concept_type_desc, created_date, created_by, active)"
          "VALUES("
          "'$orderNo',"
          "'$_occupationKode',"
          "'$_occupationDesd',"
          "'$_finTypeId',"
          "'$_finTypeDesc',"
          "'$_kegiatanUsaha',"
          "'$_kegiatanUsahaDesc',"
          "'$_jenisKegiatanUsaha',"
          "'$_jenisKegiatanUsahaDesc',"
          "'$_jenisKonsep',"
          "'$_jenisKonsepDesc',"
          "'${DateTime.now()}',"
          "'admin',"
          "1)"
      );
      var _lastInsert = await db.rawQuery('SELECT last_insert_rowid()');
      print("order no $orderNo");
      print(await db.rawQuery("SELECT * from MS2_CUST_PHOTO where order_no = '$orderNo'"));
      print("cek _lastInsert $_lastInsert");
      var _lastID = _lastInsert[0]['last_insert_rowid()'];

      //insert foto tempat tinggal
      for(int i=0;i<model.listFotoTempatTinggal.length;i++){
        await db.rawQuery("INSERT INTO MS2_CUST_PHOTO_DETAIL_2(photo_id, order_no, flag, path_photo, latitude, longitude, file_header_id, created_date, created_by, active) "
            "VALUES("
            "'$_lastID',"
            "'$orderNo',"
            "'1',"
            "'${model.listFotoTempatTinggal[i].path}',"
            "'${model.listFotoTempatTinggal[i].latitude}',"
            "'${model.listFotoTempatTinggal[i].longitude}',"
            "'${model.listFotoTempatTinggal[i].fileHeaderID}',"
            "'$dateTime',"
            "'$createdBy',"
            "1)");
      }

      //insert foto tempat usaha
      for(int i=0;i<model.listFotoTempatUsaha.length;i++){
        await db.rawQuery("INSERT INTO MS2_CUST_PHOTO_DETAIL_2(photo_id, order_no, flag, path_photo, latitude, longitude, file_header_id, created_date, created_by, active) "
            "VALUES("
            "'$_lastID',"
            "'$orderNo',"
            "'2',"
            "'${model.listFotoTempatUsaha[i].path}',"
            "'${model.listFotoTempatUsaha[i].latitude}',"
            "'${model.listFotoTempatUsaha[i].longitude}',"
            "'${model.listFotoTempatUsaha[i].fileHeaderID}',"
            "'$dateTime',"
            "'$createdBy',"
            "1)");
      }
      insertMS2PhotoDetail(model, _lastID);
    }
  }

  Future<bool> deleteMS2PhotoHeader(bool isFromDB) async{
    print("delete foto jalan");
    var _successDelete = false;
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      int _lastID;


      // perlu di adjust lagi karena ECM belum bisa di get atau submit
      if(isFromDB){
        await db.rawQuery("delete from MS2_CUST_PHOTO where order_no = '$orderNo'");
        var _checkHeader = await db.rawQuery("SELECT * from MS2_CUST_PHOTO where order_no = '$orderNo'");
        if(_checkHeader.isEmpty){
          _successDelete = true;
        }
      }
      else{
        var _lastInsert = await db.rawQuery("SELECT photo_id from MS2_CUST_PHOTO where order_no = '$orderNo'");
        if(_lastInsert.isNotEmpty){
          _lastID = _lastInsert[0]['photo_id'];
          deleteMS2PhotoDetail2(_lastID);
          // deleteMS2PhotoDetail(_lastID);
          var _check = await db.rawQuery("SELECT photo_id from MS2_CUST_PHOTO_DETAIL where photo_id = '$orderNo'");
          if(_check.isEmpty){
            await db.rawQuery("delete from MS2_CUST_PHOTO where order_no = '$orderNo'");
          }
          var _checkDetail = await db.rawQuery("SELECT * from MS2_CUST_PHOTO_DETAIL where photo_id = $_lastID");
          var _checkDetail2 = await db.rawQuery("SELECT * from MS2_CUST_PHOTO_DETAIL_2 where photo_id = $_lastID");
          if(_checkDetail.isEmpty && _checkDetail2.isEmpty){
            _successDelete = true;
          }
        }
        else{
          _successDelete = true;
        }
      }
    }
    return _successDelete;
    // db.close();
  }

  Future<List> selectMS2PhotoHeader() async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * from MS2_CUST_PHOTO where order_no = '$orderNo'");
    }
    return _data;
  }

  // Future<bool> deleteMS2PhotoHeader() async{
  //   SharedPreferences _preferences = await SharedPreferences.getInstance();
  //   createdBy = _preferences.getString("username");
  //   orderNo = _preferences.getString("order_no");
  //   print(orderNo);
  //   var databasesPath = await getDatabasesPath();
  //   var path = join(databasesPath, "Ad1MS2.db");
  //   db = await openDatabase(path);
  //   var _successDelete = false;
  //
  //   var _lastInsert = await db.rawQuery("SELECT photo_id from MS2_CUST_PHOTO where order_no = '$orderNo'");
  //   if(_lastInsert.isNotEmpty){
  //     var _lastID = _lastInsert[0]['photo_id'];
  //     if(await deleteMS2PhotoDetail2(_lastID)){
  //       await db.rawQuery("delete from MS2_CUST_PHOTO where order_no = '$orderNo'");
  //     }
  //   }
  //   // db.close();
  // }

  // void selectPhoto() async{
  //   var _data1 = await db.rawQuery("SELECT photo_id from MS2_CUST_PHOTO where order_no = '$orderNo'");
  //   var _data2 = await db.rawQuery("SELECT photo_id from MS2_CUST_PHOTO_DETAIL where photo_id = '$orderNo'");
  //   var _data3 = await db.rawQuery("SELECT photo_id from MS2_CUST_PHOTO_DETAIL_2 where photo_id = '$orderNo'");
  //   if(_data1.isNotEmpty && _data2.isNotEmpty && _data3.isNotEmpty){
  //     deleteMS2PhotoHeader();
  //   } else {
  //
  //   }
  //   // insert
  // }

  //insert form m foto list detail

  void insertMS2PhotoDetail(MS2ApplPhotoModel model, lastID) async{
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);

    //insert list grup unit
    for(int i=0;i<model.listGroupUnitObject.length;i++){
      String _groupObjectUnitId = model.listGroupUnitObject[i].groupObjectUnit == null ? "" : model.listGroupUnitObject[i].groupObjectUnit.id;
      String _groupObjectUnitGroupObject = model.listGroupUnitObject[i].groupObjectUnit == null ? "" : model.listGroupUnitObject[i].groupObjectUnit.groupObject;
      String _objectUnitId = model.listGroupUnitObject[i].objectUnit == null ? "" : model.listGroupUnitObject[i].objectUnit.id;
      String _objectUnitObjectUnit = model.listGroupUnitObject[i].objectUnit == null ? "" : model.listGroupUnitObject[i].objectUnit.objectUnit;
      await db.rawQuery("INSERT INTO MS2_CUST_PHOTO_DETAIL(photo_id, order_no, flag, group_object, group_object_desc, unit_object, unit_object_desc,"
          "created_date, created_by, active)"
          "VALUES("
          "'$lastID',"
          "'$orderNo',"
          "'1',"
          "'$_groupObjectUnitId',"
          "'$_groupObjectUnitGroupObject',"
          "'$_objectUnitId',"
          "'$_objectUnitObjectUnit',"
          "'$dateTime',"
          "'$createdBy',"
          "1)"
      );
      // var _lastInsert = await db.rawQuery('SELECT last_insert_rowid()');
      // var _detailID = _lastInsert[0]['last_insert_rowid()'];
      var _lastInsert = await db.rawQuery('SELECT * FROM MS2_CUST_PHOTO_DETAIL ORDER BY detail_id DESC');
      var _detailID = _lastInsert[0]['detail_id'];
      //insert list grup unit foto
      if(model.listGroupUnitObject[i].listImageGroupObjectUnit != null){
        for(int j=0;j<model.listGroupUnitObject[i].listImageGroupObjectUnit.length;j++){
          await db.rawQuery("INSERT INTO MS2_CUST_PHOTO_DETAIL_2(detail_id, photo_id, order_no, flag, path_photo, latitude, longitude, file_header_id,"
              "created_date, created_by, active) "
              "VALUES("
              "'$_detailID',"
              "'$lastID',"
              "'$orderNo',"
              "'3',"
              "'${model.listGroupUnitObject[i].listImageGroupObjectUnit[j].path}',"
              "'${model.listGroupUnitObject[i].listImageGroupObjectUnit[j].latitude}',"
              "'${model.listGroupUnitObject[i].listImageGroupObjectUnit[j].longitude}',"
              "'${model.listGroupUnitObject[i].listImageGroupObjectUnit[j].fileHeaderID}',"
              "'$dateTime',"
              "'$createdBy',"
              "1)");
        }
      }
    }

    //insert dokumen
    for(int i=0;i<model.listDocument.length;i++){
      print("cek foto db helper ${model.listDocument[i].path}");
      String _docTypeId = model.listDocument[i].jenisDocument == null ? "" : model.listDocument[i].jenisDocument.docTypeId;
      String _docTypeName = model.listDocument[i].jenisDocument == null ? "" : model.listDocument[i].jenisDocument.docTypeName;
      DateTime _dateTime = model.listDocument[i].dateTime == null ? "" : model.listDocument[i].dateTime;
      await db.rawQuery("INSERT INTO MS2_CUST_PHOTO_DETAIL(photo_id, order_no, flag, document_type, document_type_desc, "
          "is_mandatory, is_display, is_unit, date_receive, created_date, created_by, active)"
          "VALUES("
          "'$lastID',"
          "'$orderNo',"
          "'2',"
          "'$_docTypeId',"
          "'$_docTypeName',"
          "'${model.listDocument[i].jenisDocument.mandatory}',"
          "'${model.listDocument[i].jenisDocument.display}',"
          "'${model.listDocument[i].jenisDocument.flag_unit}',"
          "'$_dateTime',"
          "'$dateTime',"
          "'$createdBy',"
          "1)");
      var _lastInsert = await db.rawQuery('SELECT *  FROM MS2_CUST_PHOTO_DETAIL ORDER BY detail_id DESC');
      var _detailID = _lastInsert[0]['detail_id'];

      //insert dokumen foto
      // for(int i=0;i<model.listDocument.length;i++){
        await db.rawQuery("INSERT INTO MS2_CUST_PHOTO_DETAIL_2(detail_id, photo_id, order_no, flag, path_photo, latitude, longitude, orderSupportingDocumentID, file_header_id,"
            " created_date, created_by, active) "
            "VALUES("
            "'$_detailID',"
            "'$lastID',"
            "'$orderNo',"
            "'4',"
            "'${model.listDocument[i].path}',"
            "'${model.listDocument[i].latitude}',"
            "'${model.listDocument[i].longitude}',"
            "'${model.listDocument[i].orderSupportingDocumentID}',"
            "'${model.listDocument[i].fileHeaderID}',"
            "'$dateTime',"
            "'$createdBy',"
            "1)");
      // }
    }
    // db.close();
  }

  Future<List> selectMS2PhotoDetail() async{
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    createdBy = _preferences.getString("username");
    orderNo = _preferences.getString("order_no");
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    return await db.rawQuery("SELECT * from MS2_CUST_PHOTO_DETAIL where order_no = '$orderNo'");
  }

  Future<List> selectMS2PhotoDetail2() async{
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    createdBy = _preferences.getString("username");
    orderNo = _preferences.getString("order_no");
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    return await db.rawQuery("SELECT * from MS2_CUST_PHOTO_DETAIL_2 where order_no = '$orderNo'");
  }

  void deleteMS2PhotoDetail(int photoID) async{
    if (await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      // await db.rawQuery("delete from MS2_CUST_PHOTO_DETAIL where photo_id = $photoID");
      await db.rawQuery("delete from MS2_CUST_PHOTO_DETAIL where order_no = '$orderNo'");
    }
  }

  void deleteMS2PhotoDetail2(int photoID) async{
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      // await db.rawQuery("delete from MS2_CUST_PHOTO_DETAIL_2 where photo_id = $photoID");
      await db.rawQuery("delete from MS2_CUST_PHOTO_DETAIL_2 where order_no = '$orderNo'");
      deleteMS2PhotoDetail(photoID);
    }
  }

  // Future<bool> deleteMS2PhotoDetail(int photoID) async{
  //   var databasesPath = await getDatabasesPath();
  //   var path = join(databasesPath, "Ad1MS2.db");
  //   db = await openDatabase(path);
  //   await db.rawQuery("delete from MS2_CUST_PHOTO_DETAIL where photo_id = $photoID");
  //   return _checkDataPhoto(photoID);
  // }
  //
  // Future<bool> deleteMS2PhotoDetail2(int photoID) async{
  //   var databasesPath = await getDatabasesPath();
  //   var path = join(databasesPath, "Ad1MS2.db");
  //   db = await openDatabase(path);
  //   await db.rawQuery("delete from MS2_CUST_PHOTO_DETAIL_2 where photo_id = $photoID");
  //   return deleteMS2PhotoDetail(photoID);
  // }
  //
  // Future<bool> _checkDataPhoto(int photoID) async{
  //   var _successDelete = false;
  //   var _checkDetail = await db.rawQuery("SELECT * from MS2_CUST_PHOTO_DETAIL where photo_id = $photoID");
  //   var _checkDetail2 = await db.rawQuery("SELECT * from MS2_CUST_PHOTO_DETAIL_2 where photo_id = $photoID");
  //   if(_checkDetail.isEmpty && _checkDetail2.isEmpty){
  //     _successDelete = true;
  //   }
  //   return _successDelete;
  // }

  //insert data info nasabah

  Future<void> insertMS2CustomerPersonal(MS2CustomerIndividuModel model) async{
      String _gcModelId = model.gcModel != null ? model.gcModel.id : "";
      String _gcModelName = model.gcModel != null ? model.gcModel.name : "";
      String _educationId = model.educationModel != null ? model.educationModel.id : "";
      String _educationName = model.educationModel != null ? model.educationModel.text : "";
      String _identityModelID = model.identityModel != null ? model.identityModel.id : "";
      String _identityModelName = model.identityModel != null ? model.identityModel.name : "";
      String _birthPlaceKotId = model.birthPlaceModel != null ? model.birthPlaceModel.KABKOT_ID : "";
      String _birthPlaceKotName = model.birthPlaceModel != null ? model.birthPlaceModel.KABKOT_NAME : "";
      String _maritalStatusId = model.maritalStatusModel != null ? model.maritalStatusModel.id : "";
      String _maritalStatusName = model.maritalStatusModel != null ? model.maritalStatusModel.text : "";
      String _religion = model.religionModel != null ? model.religionModel.id : "";
      String _religionDesc = model.religionModel != null ? model.religionModel.text : "";

      String _isGCChanges = model.isGCChanges ? '1' : '0';
      String _isIdentitasModelChanges = model.isIdentitasModelChanges ? '1' : '0';
      String _isNoIdentitasChanges = model.isNoIdentitasChanges ? '1' : '0';
      String _isTglIdentitasChanges = model.isTglIdentitasChanges ? '1' : '0';
      String _isKTPBerlakuSeumurHidupChanges = model.isKTPBerlakuSeumurHidupChanges ? '1' : '0';
      String _isIdentitasBerlakuSampaiChanges = model.isIdentitasBerlakuSampaiChanges ? '1' : '0';
      String _isNamaLengkapSesuaiIdentitasChanges = model.isNamaLengkapSesuaiIdentitasChanges ? '1' : '0';
      String _isNamaLengkapChanges = model.isNamaLengkapChanges ? '1' : '0';
      String _isTglLahirChanges = model.isTglLahirChanges ? '1' : '0';
      String _isTempatLahirSesuaiIdentitasChanges = model.isTempatLahirSesuaiIdentitasChanges ? '1' : '0';
      String _isTempatLahirSesuaiIdentitasLOVChanges = model.isTempatLahirSesuaiIdentitasLOVChanges ? '1' : '0';
      String _isGenderChanges = model.isGenderChanges ? '1' : '0';
      String _isReligionChanges = model.isReligionChanges ? '1' : '0';
      String _isEducationSelectedChanges = model.isEducationSelectedChanges ? '1' : '0';
      String _isJumlahTanggunganChanges = model.isJumlahTanggunganChanges ? '1' : '0';
      String _isMaritalStatusSelectedChanges = model.isMaritalStatusSelectedChanges ? '1' : '0';
      String _isEmailChanges = model.isEmailChanges ? '1' : '0';
      String _isNoHpChanges = model.isNoHpChanges ? '1' : '0';
      String _isNoHp1WAChanges = model.isNoHp1WAChanges ? '1' : '0';
      // String _isNoHp2Changes = model.isNoHp2Changes ? '1' : '0';
      String _isNoHp2WAChanges = model.isNoHp2WAChanges ? '1' : '0';
      // String _isNoHp3Changes = model.isNoHp3Changes ? '1' : '0';
      String _isNoHp3WAChanges = model.isNoHp3WAChanges ? '1' : '0';

      if(await setOrderNumber()){
        var databasesPath = await getDatabasesPath();
        var path = join(databasesPath, "Ad1MS2.db");
        db = await openDatabase(path);
        await db.rawInsert("INSERT INTO MS2_CUSTOMER_PERSONAL (order_no, customerIndividualID, no_of_liability, cust_group, cust_group_desc, education, education_desc, "
            "id_type, id_desc, id_no, full_name_id, full_name, date_of_birth, place_of_birth, place_of_birth_kabkota, "
            "place_of_birth_kabkota_desc, gender, gender_desc, id_date, id_expired_date, religion, religion_desc, "
            "marital_status, marital_status_desc, handphone_no, no_wa, no_wa_2, no_wa_3, email, created_date, "
            "created_by, active, flag_id_lifetime,"
            "edit_no_of_liability,edit_cust_group, edit_education, edit_id_type, edit_id_no, edit_full_name_id, edit_full_name, edit_date_of_birth, "
            "edit_place_of_birth, edit_place_of_birth_kabkota, edit_gender, edit_id_date, edit_id_expired_date, "
            "edit_religion, edit_marital_status, "
            "edit_handphone_no, edit_no_wa, edit_no_wa_2, edit_no_wa_3, edit_email, edit_flag_id_lifetime)"
            "VALUES("
            "'$orderNo','${model.customerIndividualID}','${model.no_of_liability}','$_gcModelId','$_gcModelName','$_educationId','$_educationName',"
            "'$_identityModelID','$_identityModelName','${model.id_no}','${model.full_name_id}','${model.full_name}',"
            "'${model.date_of_birth}',"
            "'${model.place_of_birth}',"
            "'$_birthPlaceKotId',"
            "'$_birthPlaceKotName',"
            "'${model.gender}',"
            "'${model.gender_desc}',"
            "'${model.id_date}',"
            "'${model.id_expired_date}',"
            "'$_religion',"
            "'$_religionDesc',"
            "'$_maritalStatusId',"
            "'$_maritalStatusName',"
            "'${model.handphone_no}',"
            "'${model.no_wa}',"
            "'${model.no_wa_2}',"
            "'${model.no_wa_3}',"
            "'${model.email}',"
            "'$dateTime',"
            "'$createdBy',"
            "'${model.active}',"
            "'${model.flag_id_lifetime}',"
            "'$_isJumlahTanggunganChanges',"
            "'$_isGCChanges',"
            "'$_isEducationSelectedChanges',"
            "'$_isIdentitasModelChanges',"
            "'$_isNoIdentitasChanges',"
            "'$_isNamaLengkapSesuaiIdentitasChanges',"
            "'$_isNamaLengkapChanges',"
            "'$_isTglLahirChanges',"
            "'$_isTempatLahirSesuaiIdentitasChanges',"
            "'$_isTempatLahirSesuaiIdentitasLOVChanges',"
            "'$_isGenderChanges',"
            "'$_isTglIdentitasChanges',"
            "'$_isIdentitasBerlakuSampaiChanges',"
            "'$_isReligionChanges',"
            "'$_isMaritalStatusSelectedChanges',"
            "'$_isNoHp1WAChanges',"
            "'$_isNoHp2WAChanges',"
            "'$_isNoHp3WAChanges',"
            "'$_isNoHpChanges',"
            "'$_isEmailChanges',"
            "'$_isKTPBerlakuSeumurHidupChanges')"
        );
      }
  }

  Future<bool> deleteMS2CustomerPersonal() async{
    bool _isDelete = false;
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      var _check = await db.rawQuery("select * from MS2_CUSTOMER_PERSONAL where order_no = '$orderNo'");

      if(_check.isNotEmpty){
        print("masuk if");
        var databasesPath = await getDatabasesPath();
        var path = join(databasesPath, "Ad1MS2.db");
        db = await openDatabase(path);
        await db.rawQuery("delete from MS2_CUSTOMER_PERSONAL where order_no = '$orderNo'");
        _isDelete = true;
      }
      else {
        print("masuk else");
        _isDelete = true;
      }
      await deleteMS2Customer();
      print("hasil delete");
      print(_check);
    }
    return _isDelete;
    // db.close();
  }

  Future<void> insertMS2Customer(MS2CustomerModel model) async{
    if(await setOrderNumber()){
      var npwpID = model.npwpModel != null ? "${model.npwpModel.id}" : "";
      var npwpName = model.npwpModel != null ? model.npwpModel.text : "";
      print("cek brada $npwpName ${model.flag_npwp} ${model.npwp_no} ${model.npwp_address}");
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      // int _result = await db.rawInsert(
      //     "INSERT INTO MS2_CUSTOMER VALUES("
      //         "${model.order_no},"
      //         "${model.id_obligor},"
      //         "${model.cust_type},"
      //         "${model.npwp_address},"
      //         "${model.npwp_name},"
      //         "${model.npwp_no},"
      //         "${model.npwpModel.id},"
      //         "${model.npwpModel.text},"
      //         "${model.flag_npwp},"
      //         "${model.pkp_flag},"
      //         "${model.is_guarantor},"
      //         "${model.corespondence},"
      //         "${model.debitur_code_12},"
      //         "${model.debitur_code_13},"
      //         "${model.last_known_cust_state},"
      //         "${model.created_date},"
      //         "${model.created_by},"
      //         "${model.modified_date},"
      //         "${model.modified_by},"
      //         "${model.active})"
      // );
      // print(_result);
      await db.rawInsert("INSERT INTO MS2_CUSTOMER(order_no, customerID, id_obligor, cust_type,flag_npwp, pkp_flag, npwp_address,npwp_name,npwp_no, npwp_type, npwp_type_desc,"
          "created_date, created_by, active, edit_flag_npwp, edit_npwp_no, edit_npwp_name, edit_npwp_type, edit_pkp_flag, edit_npwp_address)"
          " VALUES("
          "'$orderNo',"
          "'${model.customerID}',"
          "'${model.id_obligor}',"
          "'${model.cust_type}',"
          "${model.flag_npwp},"
          "'${model.pkp_flag}',"
          "'${model.npwp_address}',"
          "'${model.npwp_name}',"
          "'${model.npwp_no}',"
          "'$npwpID',"
          "'$npwpName',"
          "'${model.created_date}',"
          "'$createdBy',"
          "'${model.active}',"
          "'${model.isHaveNPWPDakor}',"
          "'${model.isNoNPWPDakor}',"
          "'${model.isNameNPWPDakor}',"
          "'${model.isTypeNPWPDakor}',"
          "'${model.isPKPSignDakor}',"
          "'${model.isAddressNPWPDakor}')"
      );
    }
    // db.close();
  }

  Future<bool> deleteMS2Customer() async{
    bool _isDelete = false;
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      var _check = await db.rawQuery("SELECT * from MS2_CUSTOMER where order_no = '$orderNo'");
      if(_check.isNotEmpty){
        await db.rawQuery("delete from MS2_CUSTOMER where order_no = '$orderNo'");
        _isDelete = true;
      } else {
        _isDelete = true;
      }
    }
    return _isDelete;
  }

  Future<List> selectMS2Customer() async{
    debugPrint("start");
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * from MS2_CUSTOMER where order_no = '$orderNo'");
    }
    return _data;
  }


  Future<void> insertMS2CustomerCompany (MS2CustomerCompanyModel model) async {
    if(await setOrderNumber()){
      var sectorEconomicID = model.sectorEconomicModel != null ? "${model.sectorEconomicModel.KODE}" : "";
      debugPrint("CHECK_SECTOR_ECO_DB $sectorEconomicID");
      var sectorEconomicName = model.sectorEconomicModel != null ? "${model.sectorEconomicModel.DESKRIPSI}" : "";
      var locationStatusID = model.locationStatusModel != null ? "${model.locationStatusModel.id}" : "";
      var locationStatusName = model.locationStatusModel != null ? "${model.locationStatusModel.desc}" : "";
      var bussLocationID = model.bussLocationModel != null ? "${model.bussLocationModel.id}" : "";
      var bussLocationName = model.bussLocationModel != null ? "${model.bussLocationModel.desc}" : "";
      var businessFieldID = model.businessFieldModel != null ? "${model.businessFieldModel.KODE}" : "";
      var businessFieldName = model.businessFieldModel != null ? "${model.businessFieldModel.DESKRIPSI}" : "";
      var _typeInstitutionID = model.typeInstitutionModel != null ? "${model.typeInstitutionModel.PARA_ID}" : "";
      var _typeInstitutionName = model.typeInstitutionModel != null ? "${model.typeInstitutionModel.PARA_NAME}" : "";
      var _profileID = model.profilModel != null ? "${model.profilModel.id}" : "";
      var _profileName = model.profilModel != null ? "${model.profilModel.text}" : "";

      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawInsert(
          "INSERT INTO MS2_CUSTOMER_COMPANY(order_no, customerCorporateID,comp_type,comp_desc,profil,profil_desc,"
              "comp_name,establish_date,sector_economic,sector_economic_desc,"
              "nature_of_buss,nature_of_buss_desc, location_status, location_status_desc, buss_location, buss_location_desc,"
              "total_emp, no_of_year_work,no_of_year_buss, flag_account, account_no_form, created_date,created_by,active,"
              "edit_comp_type, edit_profil, edit_comp_name, edit_establish_date, edit_sector_economic, edit_nature_of_buss, "
              "edit_location_status, edit_buss_location, edit_total_emp, edit_no_of_year_work, edit_no_of_year_buss) "
              "VALUES("
              "'$orderNo',"
              "'${model.customerCorporateID}',"
              "'$_typeInstitutionID',"
              "'$_typeInstitutionName',"
              "'$_profileID',"
              "'$_profileName',"
              "'${model.comp_name}',"
              "'${model.establish_date}',"
              "'$sectorEconomicID',"
              "'$sectorEconomicName',"
              "'$businessFieldID',"
              "'$businessFieldName',"
              "'$locationStatusID',"
              "'$locationStatusName',"
              "'$bussLocationID',"
              "'$bussLocationName',"
              "'${model.total_emp}',"
              "'${model.no_of_year_work}',"
              "'${model.no_of_year_buss}',"
              "'${model.flag_account}',"
              "'${model.account_no_form}',"
              "'${model.created_date}',"
              "'${model.created_by}',"
              "'${model.active}',"
              "'${model.isCompanyTypeDakor}',"
              "'${model.isProfileDakor}',"
              "'${model.isCompanyNameDakor}',"
              "'${model.isEstablishDateDakor}',"
              "'${model.isSectorEconomicDakor}',"
              "'${model.isBusinessFieldDakor}',"
              "'${model.isLocationStatusDakor}',"
              "'${model.isBusinessLocationDakor}',"
              "'${model.isTotalEmpDakor}',"
              "'${model.isLamaUsahaDakor}',"
              "'${model.isTotalLamaUsahaDakor}')"
      );
    }
    // db.close();
  }

  Future<bool> deleteMS2CustomerCompany() async{
    bool _isDelete = false;
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      var _check = await db.rawQuery("SELECT * from MS2_CUSTOMER_COMPANY where order_no = '$orderNo'");
      if(_check.isNotEmpty){
        await db.rawQuery("delete from MS2_CUSTOMER_COMPANY where order_no = '$orderNo'");
        _isDelete = true;
      } else {
        _isDelete = true;
      }
      await deleteMS2Customer();
    }
    return _isDelete;
  }

  Future<List> selectMS2CustomerCompany() async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * from MS2_CUSTOMER_COMPANY where order_no = '$orderNo'");
    }
    return _data;
  }

  Future<void> insertMS2CustomerCompanyForPIC(ManagementPICSQLiteModel model) async{
   if(await  setOrderNumber()){
     var databasesPath = await getDatabasesPath();
     var path = join(databasesPath, "Ad1MS2.db");
     db = await openDatabase(path);
     String _identityID = model.identityModel != null ? model.identityModel.id : "";
     String _identityText = model.identityModel != null ? model.identityModel.text : "";
     String _birthPlaceKabKotID = model.birthPlaceModel != null ? model.birthPlaceModel.KABKOT_ID : "";
     String _birthPlaceModelKabKotName = model.birthPlaceModel != null ? model.birthPlaceModel.KABKOT_NAME : "";
     await db.rawUpdate("UPDATE MS2_CUSTOMER_COMPANY SET "
         "id_type = '$_identityID',"
         "id_desc = '$_identityText',"
         "id_no = '${model.id_no}',"
         "full_name_id = '${model.full_name_id}',"
         "full_name = '${model.full_name}',"
         "alias_name = '${model.alias_name}',"
         "degree = '${model.degree}',"
         "date_of_birth = '${model.date_of_birth}',"
         "place_of_birth = '${model.place_of_birth}',"
         "place_of_birth_kabkota = '$_birthPlaceKabKotID',"
         "place_of_birth_kabkota_desc = '$_birthPlaceModelKabKotName',"
         "id_date = '${model.id_date}',"
         "id_expire_date = '${model.id_expire_date}',"
         "ac_level = '${model.ac_level}',"
         "position = '${model.position}',"
         "handphone_no = '${model.handphone_no}',"
         "email = '${model.email}',"
         "shrldng_percent = '${model.shrldng_percent}',"
         "dedup_score = '${model.dedup_score}',"
         "created_date = '${model.created_date}',"
         "created_by = '${model.created_by}',"
         "modified_date = '${model.modified_date}',"
         "modified_by = '${model.modified_by}',"
         "active = '${model.active}',"
         "edit_id_type = " '${model.identityTypeDakor},'
         "edit_id_no = " '${model.identityNoDakor},'
         "edit_full_name_id = " '${model.fullnameIdDakor},'
         "edit_full_name = " '${model.fullnameDakor},'
         "edit_date_of_birth = " '${model.dateOfBirthDakor},'
         "edit_place_of_birth = " '${model.placeOfBirthDakor},'
         "edit_place_of_birth_kabkota = " '${model.placeOfBirthLOVDakor},'
         "edit_ac_level = " '${model.positionDakor},'
         "edit_email = " '${model.emailDakor},'
         "edit_handphone_no = " '${model.phoneDakor} '
         "WHERE order_no = '$orderNo'");
     // db.close();
   }
  }

  Future<bool> deleteMS2CustomerCompanyForPIC() async{
    bool _isDelete = false;
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      var _check = await db.rawQuery("SELECT * from MS2_CUSTOMER_COMPANY where order_no = '$orderNo'");
      if(_check.isNotEmpty){
        await db.rawQuery("delete from MS2_CUSTOMER_COMPANY where order_no = '$orderNo'");
        _isDelete = true;
      } else {
        _isDelete = true;
      }
    }
    return _isDelete;
    // db.close();
  }

  Future<List> selectMS2CustomerCompanyForPIC() async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * from MS2_CUSTOMER_COMPANY where order_no = '$orderNo'");
    }
    return _data;
  }

  Future<void> insertMS2CustAddr (List<MS2CustAddrModel> model) async {
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      // int _result = await db.rawInsert(
      //     "INSERT INTO MS2_CUST_ADDR VALUES("
      //         "${model.order_no},"
      //         "${model.koresponden},"
      //         "${model.matrix_addr},"
      //         "${model.address},"
      //         "${model.rt},"
      //         "${model.rt_desc},"
      //         "${model.rw},"
      //         "${model.rw_desc},"
      //         "${model.provinsi},"
      //         "${model.provinsi_desc},"
      //         "${model.kabkot},"
      //         "${model.kabkot_desc},"
      //         "${model.kecamatan},"
      //         "${model.kecamatan_desc},"
      //         "${model.kelurahan},"
      //         "${model.kelurahan_desc},"
      //         "${model.zip_code},"
      //         "${model.handphone_no},"
      //         "${model.phone1},"
      //         "${model.phone1_area},"
      //         "${model.phone_2},"
      //         "${model.phone_2_area},"
      //         "${model.fax},"
      //         "${model.fax_area},"
      //         "${model.addr_type},"
      //         "${model.addr_desc},"
      //         "${model.created_date},"
      //         "${model.created_by},"
      //         "${model.modified_date},"
      //         "${model.modified_by},"
      //         "${model.active},"
      // );
      // print(_result);
      // edit_addr_type, edit_address, edit_rt, edit_rw, edit_kelurahan, edit_kecamatan,
      // edit_kabkot, edit_provinsi, edit_zip_code, edit_phone1_area, edit_phone1,
      // edit_phone_2_area, edit_phone_2, edit_fax_area, edit_fax, edit_address_from_map, 
      for(int i=0; i<model.length; i++){
        debugPrint("CEK_MATRIX_ADDRESS ${model[i].matrix_addr}");
        await db.rawInsert("INSERT INTO MS2_CUST_ADDR(order_no, addressID, foreignBusinessID, id_no, koresponden, matrix_addr, address, rt, rw, provinsi, provinsi_desc,"
            "kabkot, kabkot_desc, kecamatan, kecamatan_desc, kelurahan, kelurahan_desc, zip_code, phone1, phone1_area, "
            "phone_2, phone_2_area, fax, fax_area, addr_type, addr_desc, latitude, longitude, "
            "address_from_map, created_date, created_by, active, edit_addr_type, edit_address, "
            "edit_rt, edit_rw, edit_kelurahan, edit_kecamatan, edit_kabkot, edit_provinsi, "
            "edit_zip_code, edit_phone1_area, edit_phone1, edit_phone2_area, edit_phone2, "
            "edit_fax_area, edit_fax, edit_address_from_map) "
            "VALUES("
            "'$orderNo',"
            "'${model[i].addressID}',"
            "'${model[i].foreignBusinessID}',"
            "'${model[i].id_no}',"
            "'${model[i].koresponden}',"
            "'${model[i].matrix_addr}',"
            "'${model[i].address}',"
            "'${model[i].rt}',"
            "'${model[i].rw}',"
            "'${model[i].provinsi}',"
            "'${model[i].provinsi_desc}',"
            "'${model[i].kabkot}',"
            "'${model[i].kabkot_desc}',"
            "'${model[i].kecamatan}',"
            "'${model[i].kecamatan_desc}',"
            "'${model[i].kelurahan}',"
            "'${model[i].kelurahan_desc}',"
            "'${model[i].zip_code}',"
            "'${model[i].phone1}',"
            "'${model[i].phone1_area}',"
            "'${model[i].phone_2}',"
            "'${model[i].phone_2_area}',"
            "'${model[i].fax}',"
            "'${model[i].fax_area}',"
            "'${model[i].addr_type}',"
            "'${model[i].addr_desc}',"
            "'${model[i].latitude}',"
            "'${model[i].longitude}',"
            "'${model[i].addr_from_map}',"
            "'$dateTime',"
            "'$createdBy',"
            "'${model[i].active}',"
            "'${model[i].edit_addr_type}',"
            "'${model[i].edit_address}',"
            "'${model[i].edit_rt}',"
            "'${model[i].edit_rw}',"
            "'${model[i].edit_kelurahan}',"
            "'${model[i].edit_kecamatan}',"
            "'${model[i].edit_kabkot}',"
            "'${model[i].edit_provinsi}',"
            "'${model[i].edit_zip_code}',"
            "'${model[i].edit_phone1_area}',"
            "'${model[i].edit_phone1}',"
            "'${model[i].edit_phone_2_area}',"
            "'${model[i].edit_phone_2}',"
            "'${model[i].edit_fax_area}',"
            "'${model[i].edit_fax}',"
            "'${model[i].edit_address_from_map}')"
        );
      }
    }
//    db.close();
  }

  Future<List> selectMS2CustAddr(String type) async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * from MS2_CUST_ADDR where order_no = '$orderNo' AND matrix_addr = '$type'");
    }
    return _data;
  }

  Future<bool> deleteMS2CustAddr(String type) async{
    bool _isDelete = false;
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      var _check = await db.rawQuery("SELECT * from MS2_CUST_ADDR where order_no = '$orderNo' AND matrix_addr = '$type'");
      if(_check.isNotEmpty){
        print("di if");
        await db.rawQuery("DELETE FROM MS2_CUST_ADDR WHERE order_no = '$orderNo' AND matrix_addr = '$type'");
        _isDelete = true;
      } else {
        print("di else");
        _isDelete = true;
      }
    }
    return _isDelete;
    // db.close();
  }

  Future<void> insertMS2CustFamily (MS2CustFamilyModel model) async {
    print("RELATION_STATUS_ID db helper ${model.relation_status}");
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      // int _result = await db.rawInsert(
      //     "INSERT INTO MS2_CUST_FAMILY VALUES("
      //         "${model.order_no},"
      //         "${model.relation_status},"
      //         "${model.relation_status_desc},"
      //         "${model.full_name_id},"
      //         "${model.full_name},"
      //         "${model.id_no},"
      //         "${model.date_of_birth},"
      //         "${model.place_of_birth},"
      //         "${model.place_of_birth_kabkota},"
      //         "${model.place_of_birth_kabkota_desc},"
      //         "${model.gender},"
      //         "${model.gender_desc},"
      //         "${model.degree},"
      //         "${model.id_type},"
      //         "${model.id_desc},"
      //         "${model.created_date},"
      //         "${model.created_by},"
      //         "${model.modified_date},"
      //         "${model.modified_by},"
      //         "${model.active},"
      //         "${model.dedup_score},"
      //         "${model.phone1},"
      //         "${model.phone1_area},"
      //         "${model.handphone_no},"
      // );
      // print(_result);

      await db.rawInsert("INSERT INTO MS2_CUST_FAMILY(order_no, familyInfoID, relation_status, relation_status_desc, full_name_id, full_name, "
          "id_no, date_of_birth, place_of_birth, place_of_birth_kabkota, place_of_birth_kabkota_desc, gender, gender_desc, id_type,"
          "id_desc, phone1, phone1_area, handphone_no, created_date, created_by, active,"
          "edit_id_type, edit_relation_status, edit_id_no, edit_full_name_id, edit_full_name, edit_date_of_birth, edit_gender, edit_phone1_area,"
          "edit_phone1, edit_place_of_birth_kabkota, edit_handphone_no, edit_place_of_birth)"
          "VALUES("
          "'$orderNo',"
          "'${model.familyInfoID}',"
          "'${model.relation_status}',"
          "'${model.relation_status_desc}',"
          "'${model.full_name_id}',"
          "'${model.full_name}',"
          "'${model.id_no}',"
          "'${model.date_of_birth}',"
          "'${model.place_of_birth}',"
          "'${model.place_of_birth_kabkota}',"
          "'${model.place_of_birth_kabkota_desc}',"
          "'${model.gender}',"
          "'${model.gender_desc}',"
          "'${model.id_type}',"
          "'${model.id_desc}',"
          "'${model.phone1}',"
          "'${model.phone1_area}',"
          "'${model.handphone_no}',"
          "'$dateTime',"
          "'$createdBy',"
          "'${model.active}',"
          "'${model.edit_id_type}',"
          "'${model.edit_relation_status}',"
          "'${model.edit_id_no}',"
          "'${model.edit_full_name_id}',"
          "'${model.edit_full_name}',"
          "'${model.edit_date_of_birth}',"
          "'${model.edit_gender}',"
          "'${model.edit_phone1_area}',"
          "'${model.edit_phone1}',"
          "'${model.edit_place_of_birth_kabkota}',"
          "'${model.edit_handphone_no}',"
          "'${model.edit_place_of_birth}')"
      );
    }
    // db.close();
  }

  Future<bool> deleteMS2CustFamily() async{
    //disini
    bool _isDelete = false;
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      var _check = await db.rawQuery("SELECT * from MS2_CUST_FAMILY where order_no = '$orderNo'");
      print("cek data keluarga $_check");
      print(orderNo);
      if(_check.isNotEmpty){
        var databasesPath = await getDatabasesPath();
        var path = join(databasesPath, "Ad1MS2.db");
        db = await openDatabase(path);
        await db.rawQuery("delete from MS2_CUST_FAMILY where order_no = '$orderNo'");
        _isDelete = true;
      } else {
        _isDelete = true;
      }
    }
    return _isDelete;
    // db.close();
  }

  Future<void> insertMS2CustIncome(List<MS2CustIncomeModel> model) async {
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      for(int i=0; i<model.length; i++){
        print("cek income ${model[i].income_value}");
        await db.rawInsert("INSERT INTO MS2_CUST_INCOME(cust_income_id, customerIncomeID, type_income_frml, income_type, income_desc,"
            "income_value, idx, edit_income_value, created_date, crated_by, active) "
            "VALUES("
            "'$orderNo',"
            "'${model[i].customerIncomeID}',"
            "'${model[i].type_income_frml}',"
            "'${model[i].income_type}',"
            "'${model[i].income_desc}',"
            "'${model[i].income_value}',"
            "'${model[i].idx}',"
            "'${model[i].edit_income_value}',"
            "'$dateTime',"
            "'$createdBy',"
            "'${model[i].active}')"
        );
      }
    }
    // db.close();
  }

  Future<bool> deleteMS2CustIncome() async{
    bool _isDelete = false;
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      var _check = await db.rawQuery("SELECT * from MS2_CUST_INCOME where cust_income_id = '$orderNo'");
      if(_check.isNotEmpty){
        await db.rawQuery("delete from MS2_CUST_INCOME where cust_income_id = '$orderNo'");
        _isDelete = true;
      } else {
        _isDelete = true;
      }
    }
    return _isDelete;
    // db.close();
  }

  void selectIncome() async{
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    print(await db.rawQuery("SELECT * FROM MS2_CUST_INCOME"));
    // db.close();
  }

  Future<void> insertMS2CustIndOccupation (MS2CustIndOccupationModel model) async {
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      int _result = await db.rawInsert("INSERT INTO MS2_CUST_IND_OCCUPATION(order_no, customerOccupationID, occupation, occupation_desc, buss_name, "
          "buss_type, pep_type, pep_desc, emp_status, emp_status_desc, profession_type, profession_desc, sector_economic,"
          "sector_economic_desc, nature_of_buss, nature_of_buss_desc, location_status, location_status_desc, buss_location, buss_location_desc, total_emp, "
          "no_of_year_work, no_of_year_buss, created_date, created_by, active) "
          "VALUES("
          "'$orderNo',"
          "'${model.customerOccupationID}',"
          "'${model.occupation}',"
          "'${model.occupation_desc}',"
          "'${model.buss_name}',"
          "'${model.buss_type}',"
          "'${model.pep_type}',"
          "'${model.pep_desc}',"
          "'${model.emp_status}',"
          "'${model.emp_status_desc}',"
          "'${model.profession_type}',"
          "'${model.profession_desc}',"
          "'${model.sector_economic}',"
          "'${model.sector_economic_desc}',"
          "'${model.nature_of_buss}',"
          "'${model.nature_of_buss_desc}',"
          "'${model.location_status}',"
          "'${model.location_status_desc}',"
          "'${model.buss_location}',"
          "'${model.buss_location_desc}',"
          "'${model.total_emp}',"
          "'${model.no_of_year_work}',"
          "'${model.no_of_year_buss}',"
          "'$dateTime',"
          "'$createdBy',"
          "'${model.active}')"
      );
      print(_result);
    }
    // db.close();
  }

  Future<List> selectMS2CustIndOccupation() async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * from MS2_CUST_IND_OCCUPATION where order_no = '$orderNo'");
    }
    return _data;
  }

  Future<bool> deleteMS2CustIndOccupation() async{
    bool _isDelete = false;
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      var _check = await db.rawQuery("SELECT * from MS2_CUST_IND_OCCUPATION where order_no = '$orderNo'");
      print(_check);
      print(orderNo);
      if(_check.isNotEmpty){
        var databasesPath = await getDatabasesPath();
        var path = join(databasesPath, "Ad1MS2.db");
        db = await openDatabase(path);
        await db.rawQuery("delete from MS2_CUST_IND_OCCUPATION where order_no = '$orderNo'");
        _isDelete = true;
      } else {
        _isDelete = true;
      }
    }
    return _isDelete;
    // db.close();
  }

  void insertMS2CustPhoto (MS2CustPhotoModel model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    int _result = await db.rawInsert(
        "INSERT INTO MS2_CUST_PHOTO VALUES("
            "${model.photo_id},"
            "${model.order_no},"
            "${model.work},"
            "${model.work_desc},"
            "${model.financing_type},"
            "${model.financing_type_desc},"
            "${model.business_activities},"
            "${model.business_activities_desc},"
            "${model.business_activities_type},"
            "${model.business_activities_type_desc},"
            "${model.concept_type},"
            "${model.concept_type_desc},"
            "${model.created_date},"
            "${model.created_by},"
            "${model.modified_date},"
            "${model.modified_by},"
            "${model.active},"
    );
    print(_result);
    // db.close();
  }

  void insertMS2CustPhotoDetail (MS2CustPhotoDetailModel model) async {
    setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    int _result = await db.rawInsert(
        "INSERT INTO MS2_CUST_PHOTO_DETAIL VALUES("
            "${model.photo_id},"
            "${model.order_no},"
            "${model.index},"
            "${model.residence_photo},"
            "${model.business_photo},"
            "${model.group_unit_photo},"
            "${model.document_unit_photo},"
            "${model.created_date},"
            "${model.created_by},"
            "${model.modified_date},"
            "${model.modified_by},"
            "${model.active},"
    );
    print(_result);
    // db.close();
  }

  Future<void> insertMS2CustShrhldrCom (List<MS2CustShrhldrComModel> model) async {
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      for(int i=0; i<model.length; i++){
        var institutionID = model[i].typeInstitutionModel != null ? "${model[i].typeInstitutionModel.PARA_ID}" : "";
        var institutionName = model[i].typeInstitutionModel != null ? "${model[i].typeInstitutionModel.PARA_NAME}" : "";
        await db.rawInsert(
            "INSERT INTO MS2_CUST_SHRHLDR_COM(shrhldr_comp_id,shareholdersCorporateID,comp_type,comp_type_desc,profil,profil_desc,"
                "comp_name,akta_no,akta_expire_date,siup_no,siup_start_date,tdp_no,tdp_start_date,establish_date,"
                "flag_comp,basic_capital,paid_capital,npwp_address,npwp_name,npwp_no,npwp_type,	npwp_type_desc,flag_npwp,"
                "sector_economic,sector_economic_desc,nature_of_buss,nature_of_buss_desc,location_status,location_status_desc,"
                "buss_location,buss_location_desc,total_emp,	no_of_year_work,no_of_year_buss,ac_level,shrldng_percent,dedup_score,created_date,"
                "created_by,modified_date,modified_by,active, edit_comp_type, edit_comp_name, edit_establish_date, edit_npwp_no, edit_shrldng_percent) "
                "VALUES("
            // "'${model.shrhldr_comp_id}',"
                "'$orderNo',"
                "'${model[i].shareholdersCorporateID}',"
                "'$institutionID',"
                "'$institutionName',"
                "'${model[i].profil}',"
                "'${model[i].profil_desc}',"
                "'${model[i].comp_name}',"
                "'${model[i].akta_no}',"
                "'${model[i].akta_expire_date}',"
                "'${model[i].siup_no}',"
                "'${model[i].siup_start_date}',"
                "'${model[i].tdp_no}',"
                "'${model[i].tdp_start_date}',"
                "'${model[i].establish_date}',"
                "'${model[i].flag_comp}',"
                "'${model[i].basic_capital}',"
                "'${model[i].paid_capital}',"
                "'${model[i].npwp_address}',"
                "'${model[i].npwp_name}',"
                "'${model[i].npwp_no}',"
                "'${model[i].npwp_type}',"
                "'${model[i].npwp_type_desc}',"
                "'${model[i].flag_npwp}',"
            // "$'{model.pkp_flag.id}',"
                "'${model[i].sector_economic}',"
                "'${model[i].sector_economic_desc}',"
                "'${model[i].nature_of_buss}',"
                "'${model[i].nature_of_buss_desc}',"
                "'${model[i].location_status}',"
                "'${model[i].location_status_desc}',"
                "'${model[i].buss_location}',"
                "'${model[i].buss_location_desc}',"
                "'${model[i].total_emp}',"
                "'${model[i].no_of_year_work}',"
                "'${model[i].no_of_year_buss}',"
                "'${model[i].ac_level}',"
                "'${model[i].shrldng_percent}',"
                "'${model[i].dedup_score}',"
                "'${model[i].created_date}',"
                "'${model[i].created_by}',"
                "'${model[i].modified_date}',"
                "'${model[i].modified_by}',"
                "'${model[i].active}',"
                "'${model[i].edit_comp_type}',"
                "'${model[i].edit_comp_name}',"
                "'${model[i].edit_establish_date}',"
                "'${model[i].edit_npwp_no}',"
                "'${model[i].edit_shrldng_percent}')"
        );
      }
    }
    // db.close();
  }

  Future<bool> deleteMS2CustShrhldrCom() async{
      bool _isDelete = false;
      if(await setOrderNumber()){
        var databasesPath = await getDatabasesPath();
        var path = join(databasesPath, "Ad1MS2.db");
        db = await openDatabase(path);
        var _check = await db.rawQuery("SELECT * from MS2_CUST_SHRHLDR_COM where shrhldr_comp_id = '$orderNo'");
         if(_check.isNotEmpty){
           await db.rawQuery("delete from MS2_CUST_SHRHLDR_COM where shrhldr_comp_id = '$orderNo'");
           _isDelete = true;
         } else {
           _isDelete = true;
         }
      }
       return _isDelete;
  }

  Future<List> selectMS2CustShrhldrCom() async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * from MS2_CUST_SHRHLDR_COM where shrhldr_comp_id = '$orderNo'");
    }
    return _data;
  }

  Future<void> insertMS2CustShrhldrInd (List<MS2CustShrhldrIndModel> model) async {
    print("list pribadi = ${model.length}");
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      for(int i=0; i<model.length; i++){
        await db.rawInsert("INSERT INTO MS2_CUST_SHRHLDR_IND(shrhldr_ind_id, shareholdersIndividualID,dedup_score, relation_status, relation_status_desc, "
            "id_type, id_type_desc, id_no, full_name_id, full_name, date_of_birth, place_of_birth, "
            "place_of_birth_kabkota, place_of_birth_kabkota_desc, gender, shrhldng_percent, created_date, created_by, active, "
            "edit_relation_status, edit_id_type, edit_id_no, edit_full_name_id, edit_full_name, "
            "edit_date_of_birth, edit_place_of_birth, edit_place_of_birth_kabkota, edit_gender, edit_shrhldng_percent)  "
            "VALUES("
            "'$orderNo',"
            "'${model[i].shareholdersIndividualID}',"
            "'${model[i].dedup_score}',"
            "'${model[i].relation_status}',"
            "'${model[i].relation_status_desc}',"
            "'${model[i].id_type}',"
            "'${model[i].id_type_desc}',"
            "'${model[i].id_no}',"
            "'${model[i].full_name_id}',"
            "'${model[i].full_name}',"
            "'${model[i].date_of_birth}',"
            "'${model[i].place_of_birth}',"
            "'${model[i].place_of_birth_kabkota}',"
            "'${model[i].place_of_birth_kabkota_desc}',"
            "'${model[i].gender}',"
            "'${model[i].shrhldng_percent}',"
            "'$dateTime',"
            "'$createdBy',"
            "1,"
            "'${model[i].edit_relation_status}',"
            "'${model[i].edit_id_type}',"
            "'${model[i].edit_id_no}',"
            "'${model[i].edit_full_name_id}',"
            "'${model[i].edit_full_name}',"
            "'${model[i].edit_date_of_birth}',"
            "'${model[i].edit_place_of_birth}',"
            "'${model[i].edit_place_of_birth_kabkota}',"
            "'${model[i].edit_gender}',"
            "'${model[i].edit_shrhldng_percent}')"
        );
      }
      print("CEK_DATA_HOLDER_IND ${await db.rawQuery("SELECT * from MS2_CUST_SHRHLDR_IND where shrhldr_ind_id = '$orderNo'")}");
    }
  }

  Future<bool> deleteMS2CustShrhldrInd() async{
    bool _isDelete = false;
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      var _check = await db.rawQuery("SELECT * from MS2_CUST_SHRHLDR_IND where shrhldr_ind_id = '$orderNo'");
      if(_check.isNotEmpty){
        await db.rawQuery("delete from MS2_CUST_SHRHLDR_IND where shrhldr_ind_id = '$orderNo'");
        _isDelete = true;
      } else {
        _isDelete = true;
      }
         }
    return _isDelete;
    // db.close();
  }

  Future<List> selectMS2CustShrhldrInd() async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * from MS2_CUST_SHRHLDR_IND where shrhldr_ind_id = '$orderNo'");
    }
    return _data;
  }


  Future<void> insertMS2Document (List<MS2DocumentModel> model) async {
    print("insert dkumen");
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      for(int i=0; i<model.length; i++){
        await db.rawInsert("INSERT INTO MS2_DOCUMENT(order_no, orderSupportingDocumentID, ac_appl_objt_id, ac_appl_no, file_header_id, file_name, "
            "document_type_id, document_type_desc, mandatory, display, flag_unit, upload_date, latitude, longitude, "
            "created_date, created_by) "
            "VALUES("
            "'$orderNo',"
            "'${model[i].orderSupportingDocumentID}',"
            "'${model[i].ac_appl_objt_id}',"
            "'${model[i].ac_appl_no}',"
            "'${model[i].file_header_id}',"
            "'${model[i].file_name}',"
            "'${model[i].document_type_id}',"
            "'${model[i].document_type_desc}',"
            "'${model[i].mandatory}',"
            "'${model[i].display}',"
            "'${model[i].flag_unit}',"
            "'${model[i].upload_date}',"
            "'${model[i].latitude}',"
            "'${model[i].longitude}',"
            "'$dateTime',"
            "'$createdBy')"
        );
      }
      debugPrint("CEK DATA ${await selectMS2Document()}");
    }
    // db.close();
  }

  Future<bool> deleteMS2Document() async{
    bool _isDelete = false;
    debugPrint("JALAN DELETE DOCUMENT");
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      var _check = await db.rawQuery("SELECT * from MS2_DOCUMENT where order_no = '$orderNo'");
      if(_check.isNotEmpty){
        await db.rawQuery("delete from MS2_DOCUMENT where order_no = '$orderNo'");
        _isDelete = true;
      }
      else {
        _isDelete = true;
      }
    }
    return _isDelete;
    // db.close();
  }

  Future<List> selectMS2Document() async{
      List _data = [];
      if(await setOrderNumber()){
          var databasesPath = await getDatabasesPath();
          var path = join(databasesPath, "Ad1MS2.db");
          db = await openDatabase(path);
          _data = await db.rawQuery("SELECT * from MS2_DOCUMENT where order_no = '$orderNo'");
      }
      return _data;
  }

  Future<void> insertMS2GrntrComp (List<MS2GrntrCompModel> model) async {
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      for(int i=0; i<model.length;i++){
        await db.rawInsert("INSERT INTO MS2_GRNTR_COMP(grntr_comp_id, guarantorCorporateID, dedup_score, comp_type, comp_desc, comp_name, establish_date, "
            "npwp_no, flag_comp, created_date, created_by, active, edit_comp_type, edit_comp_name, edit_establish_date, edit_npwp_no) "
            "VALUES("
            "'$orderNo',"
            "'${model[i].guarantorCorporateID}',"
            "'${model[i].dedup_score}',"
            "'${model[i].comp_type}',"
            "'${model[i].comp_desc}',"
            "'${model[i].comp_name}',"
            "'${model[i].establish_date}',"
            "'${model[i].npwp_no}',"
            "1,"
            "'$dateTime',"
            "'$createdBy',"
            "1,"
            "'${model[i].edit_comp_type}',"
            "'${model[i].edit_comp_name}',"
            "'${model[i].edit_establish_date}',"
            "'${model[i].edit_npwp_no}')"
        );
      }
    }
    // db.close();
  }

  Future<bool> deleteMS2GrntrComp() async{
    bool _isDelete = false;
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      List _check = await db.rawQuery("SELECT * from MS2_GRNTR_COMP where grntr_comp_id = '$orderNo'");
      if(_check.isNotEmpty){
        await db.rawQuery("delete from MS2_GRNTR_COMP where grntr_comp_id = '$orderNo'");
        _isDelete = true;
      }
      else {
        _isDelete = true;
      }
    }
    return _isDelete;
    // db.close();
  }

  Future<void> insertMS2GrntrInd (List<MS2GrntrIndModel> model) async {
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      for(int i=0; i<model.length;i++){
      await db.rawInsert("INSERT INTO MS2_GRNTR_IND(grntr_ind_id, guarantorIndividualID, relation_status, relation_status_desc, dedup_score, id_type, "
            "id_desc, id_no, full_name_id, full_name, date_of_birth, place_of_birth, place_of_birth_kabkota, "
            "place_of_birth_kabkota_desc, gender, handphone_no, created_date, created_by, active, "
            "edit_relation_status, edit_id_type, edit_id_no, edit_full_name_id, edit_full_name, "
            "edit_date_of_birth, edit_place_of_birth, edit_place_of_birth_kabkota, edit_gender, edit_handphone_no) "
            "VALUES("
            "'$orderNo',"
            "'${model[i].guarantorIndividualID}',"
            "'${model[i].relation_status}',"
            "'${model[i].relation_status_desc}',"
            "${model[i].dedup_score},"
            "'${model[i].id_type}',"
            "'${model[i].id_desc}',"
            "'${model[i].id_no}',"
            "'${model[i].full_name_id}',"
            "'${model[i].full_name}',"
            "'${model[i].date_of_birth}',"
            "'${model[i].place_of_birth}',"
            "'${model[i].place_of_birth_kabkota}',"
            "'${model[i].place_of_birth_kabkota_desc}',"
            "'${model[i].gender}',"
            "'${model[i].handphone_no}',"
            "'$dateTime',"
            "'$createdBy',"
            "1,"
            "'${model[i].edit_relation_status}',"
            "'${model[i].edit_id_type}',"
            "'${model[i].edit_id_no}',"
            "'${model[i].edit_full_name_id}',"
            "'${model[i].edit_full_name}',"
            "'${model[i].edit_date_of_birth}',"
            "'${model[i].edit_place_of_birth}',"
            "'${model[i].edit_place_of_birth_kabkota}',"
            "'${model[i].edit_gender}',"
            "'${model[i].edit_handphone_no}')"
        );
        print("CEK GNTR IND ${"INSERT INTO MS2_GRNTR_IND(grntr_ind_id, relation_status, relation_status_desc, dedup_score, id_type, "
            "id_desc, id_no, full_name_id, full_name, created_date, created_by, active) "
            "VALUES("
            "'$orderNo',"
            "'${model[i].guarantorIndividualID}',"
            "'${model[i].relation_status}',"
            "'${model[i].relation_status_desc}',"
            "'${model[i].dedup_score}',"
            "'${model[i].id_type}',"
            "'${model[i].id_desc}',"
            "'${model[i].id_no}',"
            "'${model[i].full_name_id}',"
            "'${model[i].full_name}',"
            "'$dateTime',"
            "'$createdBy',"
            "1)"}");
      }
    }
    // db.close();
  }

  Future<bool> deleteMS2GrntrInd() async{
    bool _isDelete = false;
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      List _check = await db.rawQuery("SELECT * from MS2_GRNTR_IND where grntr_ind_id = '$orderNo'");
      if(_check.isNotEmpty){
        await db.rawQuery("delete from MS2_GRNTR_IND where grntr_ind_id = '$orderNo'");
        _isDelete = true;
      }
      else {
        _isDelete = true;
      }
    }

    return _isDelete;
    // db.close();
  }

  Future<List> selectGuarantorInd() async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * from MS2_GRNTR_IND where grntr_ind_id = '$orderNo'");
    }
    return _data;
  }

  Future<List> selectGuarantorCom() async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * from MS2_GRNTR_COMP where grntr_comp_id = '$orderNo'");
    }
    return _data;
  }

  // Inf Salesman
  Future<void> insertMS2ObjekSales (List<MS2ObjekSalesModel> model) async {
    if(await setOrderNumber()){
        var databasesPath = await getDatabasesPath();
        var path = join(databasesPath, "Ad1MS2.db");
        db = await openDatabase(path);
        for(int i = 0; i < model.length; i++) {
            await db.rawInsert(
                "INSERT INTO MS2_OBJEK_SALES (appl_sales_id, orderProductSalesID, empl_id, sales_type, sales_type_desc, empl_head_id, "
                    "empl_job, empl_job_desc, ref_contract_no, active, created_date, created_by, modified_date, modified_by, "
                    "empl_head_job, empl_head_job_desc, empl_head_name, empl_name, empl_jabatan, empl_jabatan_alias, "
                    "edit_sales_type, edit_empl_job, edit_empl_name, edit_empl_head_id)"
                    "VALUES("
                    "'$orderNo',"
                    "'${model[i].orderProductSalesID}',"
                    "'${model[i].empl_id}',"
                    "'${model[i].sales_type}',"
                    "'${model[i].sales_type_desc}',"
                    "'${model[i].empl_head_id}',"
                    "'${model[i].empl_job}',"
                    "'${model[i].empl_job_desc}',"
                    "'${model[i].ref_contract_no}',"
                    "'${model[i].active}',"
                    "'${model[i].created_date}',"
                    "'${model[i].created_by}',"
                    "'${model[i].modified_date}',"
                    "'${model[i].modified_by}',"
                    "'${model[i].empl_head_job}',"
                    "'${model[i].empl_head_job_desc}',"
                    "'${model[i].empl_head_name}',"
                    "'${model[i].empl_name}',"
                    "'${model[i].empl_jabatan}',"
                    "'${model[i].empl_jabatan_alias}',"
                    "'${model[i].edit_sales_type}',"
                    "'${model[i].edit_empl_job}',"
                    "'${model[i].edit_empl_name}',"
                    "'${model[i].edit_empl_head_id}')"
            );
        }
    }
//    db.close();
  }

  Future<bool> deleteMS2ObjekSales() async{
    bool _isDelete = false;
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      var _check = await db.rawQuery("SELECT * from MS2_OBJEK_SALES where appl_sales_id = '$orderNo'");
      print(_check);
      print(orderNo);
      if(_check.isNotEmpty){

        await db.rawQuery("delete from MS2_OBJEK_SALES where appl_sales_id = '$orderNo'");
        _isDelete = true;
      } else {
        _isDelete = true;
      }
    }
    // db.close();
    return _isDelete;
  }

  Future<List> selectMS2ObjekSales() async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * from MS2_OBJEK_SALES where appl_sales_id = '$orderNo'");
    }
    return _data;
  }

  // Inf Objek Karoseri
  Future<void> insertMS2ObjtKaroseri (List<MS2ObjtKaroseriModel> model) async {
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      for(int i = 0; i < model.length; i++) {
        await db.rawInsert(
            "INSERT INTO MS2_OBJT_KAROSERI (appl_karoseri_id, orderKaroseriID, pks_karoseri, comp_karoseri, comp_karoseri_desc, karoseri, "
                "karoseri_desc, price, karoseri_qty, total_karoseri, created_date, "
                "created_by, modified_date, modified_by, active, dp_karoseri, flag_karoseri, "
                "edit_pks_karoseri, edit_comp_karoseri, edit_karoseri, edit_karoseri_qty, edit_price, edit_total_karoseri) "
                "VALUES("
                "'$orderNo',"
                "'${model[i].orderKaroseriID}',"
                "'${model[i].pks_karoseri}',"
                "'${model[i].comp_karoseri}',"
                "'${model[i].comp_karoseri_desc}',"
                "'${model[i].karoseri}',"
                "'${model[i].karoseri_desc}',"
                "'${model[i].price}',"
                "'${model[i].karoseri_qty}',"
                "'${model[i].total_karoseri}',"
                "'${model[i].created_date}',"
                "'${model[i].created_by}',"
                "'${model[i].modified_date}',"
                "'${model[i].modified_by}',"
                "'${model[i].active}',"
                "'${model[i].dp_karoseri}',"
                "'${model[i].flag_karoseri}',"
                "'${model[i].edit_pks_karoseri}',"
                "'${model[i].edit_comp_karoseri}',"
                "'${model[i].edit_karoseri}',"
                "'${model[i].edit_karoseri_qty}',"
                "'${model[i].edit_price}',"
                "'${model[i].edit_total_karoseri}')"
        );
      }
    }
    // db.close();
  }

  Future<bool> deleteMS2ObjtKaroseri() async{
    bool _isDelete = false;
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      var _check = await db.rawQuery("SELECT * from MS2_OBJT_KAROSERI where appl_karoseri_id = '$orderNo'");
      if(_check.isNotEmpty){
        await db.rawQuery("delete from MS2_OBJT_KAROSERI where appl_karoseri_id = '$orderNo'");
        _isDelete = true;
      } else {
        _isDelete = true;
      }
    }
    // db.close();
    return _isDelete;
  }

  Future<List> selectMS2ObjtKaroseri() async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * from MS2_OBJT_KAROSERI where appl_karoseri_id = '$orderNo'");
    }
    return _data;
  }

  // Inf Aplikasi
  Future<void> insertMS2Application (MS2ApplicationModel model) async {
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawInsert(
        // appl_no == order_no
        // applNo == idUnique
          "INSERT INTO MS2_APPLICATION (appl_no, applNo, flag_mayor, flag_fiducia, source_application, order_date, "
              "application_date, svy_appointment_date, order_type, flag_account, account_no_form, "
              "sentra_c, sentra_c_desc, unit_c, unit_c_desc, initial_recomendation, "
              "final_recomendation, brms_scoring, persetujuan_di_tempat, sign_pk, max_level_approval, "
              "flag_receive_information, konsep_type, konsep_type_desc, objt_qty, prop_insr_type, "
              "prop_insr_type_desc, unit, cola_type, cola_type_desc, appl_contract_no, "
              "calculated_at, last_known_appl_state, last_known_appl_position, last_known_appl_status, last_known_handled_by, "
              "created_date, created_by, modified_date, modified_by, active, "
              "doc_ver_status, cust_status, appr_aos_status, flag_das_pps, flag_upload, "
              "flag_ia, flag_dakor_ca, flag_bypasscapo, flag_dedup_ca, flag_disable_dedup_ca, "
              "order_ad1gate, user_ad1gate, dealer_note, flag_source_ms2, marketing_notes, edit_order_date, edit_svy_appointment_date, "
              "edit_sign_pk, edit_konsep_type, edit_objt_qty, edit_prop_insr_type, edit_unit )"
              "VALUES("
              "'$orderNo',"
              "'${model.applNo}',"
              "'${model.flag_mayor}',"
              "'${model.flag_fiducia}',"
              "'${model.source_application}',"
              "'${model.order_date}',"
              "'${model.application_date}',"
              "'${model.svy_appointment_date}',"
              "'${model.order_type}',"
              "'${model.flag_account}',"
              "'${model.account_no_form}',"
              "'${model.sentra_c}',"
              "'${model.sentra_c_desc}',"
              "'${model.unit_c}',"
              "'${model.unit_c_desc}',"
              "'${model.initial_recomendation}',"
              "'${model.final_recomendation}',"
              "'${model.brms_scoring}',"
              "'${model.persetujuan_di_tempat}',"
              "'${model.sign_pk}',"
              "'${model.max_level_approval}',"
              "'${model.flag_receive_information}',"
              "'${model.konsep_type}',"
              "'${model.konsep_type_desc}',"
              "'${model.objt_qty}',"
              "'${model.prop_insr_type}',"
              "'${model.prop_insr_type_desc}',"
              "'${model.unit}',"
              "'${model.cola_type}',"
              "'${model.cola_type_desc}',"
              "'${model.appl_contract_no}',"
              "'${model.calculated_at}',"
              "'${model.last_known_appl_state}',"
              "'${model.last_known_appl_position}',"
              "'${model.last_known_appl_status}',"
              "'${model.last_known_handled_by}',"
              "'$dateTime',"
              "'${model.created_by}',"
              "'${model.modified_date}',"
              "'${model.modified_by}',"
              "1,"
              "'${model.doc_ver_status}',"
              "'${model.cust_status}',"
              "'${model.appr_aos_status}',"
              "'${model.flag_das_pps}',"
              "'${model.flag_upload}',"
              "'${model.flag_ia}',"
              "'${model.flag_dakor_ca}',"
              "'${model.flag_bypasscapo}',"
              "'${model.flag_dedup_ca}',"
              "'${model.flag_disable_dedup_ca}',"
              "'${model.order_ad1gate}',"
              "'${model.user_ad1gate}',"
              "'${model.dealer_note}',"
              "'${model.flag_source_ms2}',"
              "'${model.marketing_notes}',"
              "'${model.edit_order_date}',"
              "'${model.edit_svy_appointment_date}',"
              "'${model.edit_sign_pk}',"
              "'${model.edit_konsep_type}',"
              "'${model.edit_objt_qty}',"
              "'${model.edit_prop_insr_type}',"
              "'${model.edit_unit}')"
      );
    }
    // db.close();
  }

  //marketing notes
  void updateMS2Application (MS2ApplicationModel model) async {
    print("updatemarketing notes = ${model.marketing_notes}");
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawQuery("UPDATE MS2_APPLICATION "
          "SET marketing_notes = '${model.marketing_notes}',"
          "edit_marketing_notes = '${model.edit_marketing_notes}',"
          "modified_date = '${model.modified_date}',"
          "modified_by = '${model.modified_by}' "
          "WHERE appl_no = '$orderNo'"
      );
    }
  }

  Future<bool> deleteMS2Application() async{
    bool _isDelete = false;
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      var _check = await db.rawQuery("SELECT * from MS2_APPLICATION where appl_no = '$orderNo'");
      if(_check.isNotEmpty){
        await db.rawQuery("delete from MS2_APPLICATION where appl_no = '$orderNo'");
        _isDelete = true;
      } else {
        _isDelete = true;
      }
    }
    return _isDelete;
    // db.close();
  }

  Future<List> selectMS2Application() async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * from MS2_APPLICATION where appl_no = '$orderNo'");
    }
    return _data;
  }

  Future<List> selectMS2ApplLoanDetail() async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * from MS2_APPL_LOAN_DETAIL where ac_installment_id = '$orderNo'");
    }
    return _data;
  }

  // Inf Struktur Kredit Biaya
  Future<void> insertMS2ApplFee (List<MS2ApplFeeModel> model) async {
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      print("model length db helper ${model.length}");
      for(int i = 0; i < model.length; i++) {
        print("model insert ke $i");
        await db.rawInsert(
            "INSERT INTO MS2_APPL_FEE (fee_id,orderFeeID,fee_type, fee_type_desc, fee_cash, fee_credit, total_fee, "
                "active, created_date, created_by,edit_fee_type,edit_fee_cash,edit_fee_credit,edit_total_fee)"
                "VALUES("
                "'$orderNo',"
                "'${model[i].orderFeeID}',"
                "'${model[i].fee_type}',"
                "'${model[i].fee_type_desc}',"
                "'${model[i].fee_cash}',"
                "'${model[i].fee_credit}',"
                "'${model[i].total_fee}',"
                "'${model[i].active}',"
                "'${model[i].created_date}',"
                "'${model[i].created_by}',"
                "'${model[i].editFeeType}',"
                "'${model[i].editFeeCash}',"
                "'${model[i].editFeeCredit}',"
                "'${model[i].editTotalFee}')"
        );
      }
    }
//    db.close();
  }

  Future<bool> deleteMS2ApplFee() async{
    bool _isDelete = false;
    if (await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawQuery("delete from MS2_APPL_FEE where fee_id = '$orderNo'");
      _isDelete = true;
    } else {
      _isDelete = true;
    }
    return _isDelete;
      // db.close();
  }

  Future<List> selectMS2ApplFee() async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * from MS2_APPL_FEE where fee_id = '$orderNo'");
    }
    return _data;
  }

  // Inf Struktur Kredit Stepping
  void insertMS2ApplLoanDetailStepping (List<MS2ApplLoanDetailModel> model) async {
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      for(int i = 0; i < model.length; i++) {
        await db.rawInsert(
            "INSERT INTO MS2_APPL_LOAN_DETAIL VALUES("
                "'$orderNo',"
                "'${model[i].ac_installment_no}',"
                "'${model[i].ac_percentage}',"
                "'${model[i].ac_amount}',"
                "'${model[i].active}',"
                "'${model[i].created_date}',"
                "'${model[i].created_by}',"
                "'${model[i].modified_date}',"
                "'${model[i].modified_by}',"
        );
      }
    }
    // db.close();
  }

  // Inf Struktur Kredit Irreguler
  void insertMS2ApplLoanDetailIrreguler (List<MS2ApplLoanDetailModel> model) async {
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      for(int i = 0; i < model.length; i++) {
        await db.rawInsert(
            "INSERT INTO MS2_APPL_LOAN_DETAIL VALUES("
                "'$orderNo',"
                "'${model[i].ac_installment_no}',"
                "'${model[i].ac_percentage}',"
                "'${model[i].ac_amount}',"
                "'${model[i].active}',"
                "'${model[i].created_date}',"
                "'${model[i].created_by}',"
                "'${model[i].modified_date}',"
                "'${model[i].modified_by}',"
        );
      }
    }
    // db.close();
  }

  // Inf Unit Object
  Future<void> insertMS2ApplObject (MS2ApplObjectModel model) async {
    debugPrint("CEK_MODEL_DETAIL ${model.model_detail}");
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawInsert(
          "INSERT INTO MS2_APPL_OBJECT ("
              "appl_objt_id, "
              "applObjtID, "
              "financing_type, "
              "buss_activities, "
              "buss_activities_desc, "
              "buss_activities_type, "
              "buss_activities_type_desc, "
              "group_object, "
              "group_object_desc, "
              "object, "
              "object_desc, "
              "product_type, "
              "product_type_desc, "
              "brand_object, "
              "brand_object_desc, "
              "object_type, "
              "object_type_desc, "
              "object_model, "
              "object_model_desc, "
              "model_detail, "
              "object_used, "
              "object_used_desc, "
              "object_purpose, "
              "object_purpose_desc, "
              "group_sales, "
              "group_sales_desc, "
              "grup_id, "
              "grup_id_desc, "
              "order_source, "
              "order_source_name, "
              "flag_third_party, "
              "third_party_type, "
              "third_party_type_desc, "
              "third_party_id, "
              "work, "
              "work_desc, "
              "sentra_d, "
              "sentra_d_desc, "
              "unit_d, "
              "unit_d_desc, "
              "program, "
              "program_desc, "
              "rehab_type, "
              "rehab_type_desc, "
              "reference_no, "
              "reference_no_desc, "
              "appl_contrac_no, "
              "installment_type, "
              "installment_type_desc, "
              "appl_top, "
              "payment_method, "
              "payment_method_desc, "
              "eff_rate, "
              "flat_rate, "
              "object_price, "
              "karoseri_price, "
              "total_amt, "
              "dp_net, "
              "installment_decline_n, "
              "payment_per_year, "
              "dp_branch, "
              "dp_gross, "
              "principal_amt, "
              "installment_paid, "
              "interest_amt, "
              "ltv, "
              "disbursement, "
              "dsr, "
              "dir, "
              "dsc, "
              "irr, "
              "gp_type, "
              "new_tenor, "
              "total_stepping, "
              "balloon_type, "
              "last_percen_installment, "
              "last_installment_amt, "
              "is_auto_debit,"
              "bank_id, "
              "account_no, "
              "account_behalf, "
              "purpose_type, "
              "dp_source, "
              "va_no, "
              "va_amt, "
              "disburse_purpose, "
              "disburse_type, "
              "appl_objt_last_state, "
              "appl_objt_next_state, "
              "created_date, "
              "created_by, "
              "modified_date, "
              "modified_by, "
              "active, "
              "tact, "
              "tmax, "
              "appl_send_flag, "
              "is_without_colla,"
              "first_installment, "
              "flag_save_karoseri, "
              "pelunasan_amt, "
              "appl_withdrawal_flag, "
              "appl_model_dtl, "
              "insentif_sales, "
              "insentif_payment, "
              "nik, "
              "nama, "
              "jabatan, "
              "outlet_id, "
              "group_id, "
              "dealer_matrix_id, "
              "dealer_matrix, "
              "ppd_date, "
              "ppd_cancel_date, "
              "dp_chasis_net, "
              "dp_chasis_gross, "
              "ujrah_price, "
              "total_dp_krs,"
              "name_order_source, "
              "name_order_source_desc,"
              "third_party_desc,"
              "edit_financing_type, edit_buss_activities, edit_buss_activities_type, edit_group_object, edit_object, edit_product_type, edit_brand_object, "
              "edit_object_type, edit_object_model, edit_model_detail, edit_object_used, edit_object_purpose, edit_group_sales, edit_grup_id, edit_order_source, edit_name_order_source, "
              "edit_third_party_type, edit_third_party_id, edit_dealer_matrix, edit_work, edit_sentra_d, edit_unit_d, edit_program, edit_rehab_type, edit_reference_no)"
              "VALUES("
              "'$orderNo',"
              "'${model.applObjtID}',"
              "'${model.financing_type}',"
              "'${model.buss_activities}',"
              "'${model.buss_activities_desc}',"
              "'${model.buss_activities_type}',"
              "'${model.buss_activities_type_desc}',"
              "'${model.group_object}',"
              "'${model.group_object_desc}',"
              "'${model.object}',"
              "'${model.object_desc}',"
              "'${model.product_type}',"
              "'${model.product_type_desc}',"
              "'${model.brand_object}',"
              "'${model.brand_object_desc}',"
              "'${model.object_type}',"
              "'${model.object_type_desc}',"
              "'${model.object_model}',"
              "'${model.object_model_desc}',"
              "'${model.model_detail}',"
              "'${model.object_used}',"
              "'${model.object_used_desc}',"
              "'${model.object_purpose}',"
              "'${model.object_purpose_desc}',"
              "'${model.group_sales}',"
              "'${model.group_sales_desc}',"
              "'${model.grup_id}',"
              "'${model.grup_id_desc}',"
              "'${model.order_source}',"
              "'${model.order_source_name}',"
              "'${model.flag_third_party}',"
              "'${model.third_party_type}',"
              "'${model.third_party_type_desc}',"
              "'${model.third_party_id}',"
              "'${model.work}',"
              "'${model.work_desc}',"
              "'${model.sentra_d}',"
              "'${model.sentra_d_desc}',"
              "'${model.unit_d}',"
              "'${model.unit_d_desc}',"
              "'${model.program}',"
              "'${model.program_desc}',"
              "'${model.rehab_type}',"
              "'${model.rehab_type_desc}',"
              "'${model.reference_no}',"
              "'${model.reference_no_desc}',"
              "'${model.appl_contrac_no}',"
              "'${model.installment_type}',"
              "'${model.installment_type_desc}',"
              "'${model.appl_top}',"
              "'${model.payment_method}',"
              "'${model.payment_method_desc}',"
              "'${model.eff_rate}',"
              "'${model.flat_rate}',"
              "'${model.object_price}',"
              "'${model.karoseri_price}',"
              "'${model.total_amt}',"
              "'${model.dp_net}',"
              "'${model.installment_decline_n}',"
              "'${model.payment_per_year}',"
              "'${model.dp_branch}',"
              "'${model.dp_gross}',"
              "'${model.principal_amt}',"
              "'${model.installment_paid}',"
              "'${model.interest_amt}',"
              "'${model.ltv}',"
              "'${model.disbursement}',"
              "'${model.dsr}',"
              "'${model.dir}',"
              "'${model.dsc}',"
              "'${model.irr}',"
              "'${model.gp_type}',"
              "'${model.new_tenor}',"
              "'${model.total_stepping}',"
              "'${model.balloon_type}',"
              "'${model.last_percen_installment}',"
              "'${model.last_installment_amt}',"
              "'${model.is_auto_debit}',"
              "'${model.bank_id}',"
              "'${model.account_no}',"
              "'${model.account_behalf}',"
              "'${model.purpose_type}',"
              "'${model.dp_source}',"
              "'${model.va_no}',"
              "'${model.va_amt}',"
              "'${model.disburse_purpose}',"
              "'${model.disburse_type}',"
              "'${model.appl_objt_last_state}',"
              "'${model.appl_objt_next_state}',"
              "'${model.created_date}',"
              "'${model.created_by}',"
              "'${model.modified_date}',"
              "'${model.modified_by}',"
              "'${model.active}',"
              "'${model.tact}',"
              "'${model.tmax}',"
              "'${model.appl_send_flag}',"
              "'${model.is_without_colla}',"
              "'${model.first_installment}',"
              "'${model.flag_save_karoseri}',"
              "'${model.pelunasan_amt}',"
              "'${model.appl_withdrawal_flag}',"
              "'${model.appl_model_dtl}',"
              "'${model.insentif_sales}',"
              "'${model.insentif_payment}',"
              "'${model.nik}',"
              "'${model.nama}',"
              "'${model.jabatan}',"
              "'${model.outlet_id}',"
              "'${model.group_id}',"
              "'${model.dealer_matrix_id}',"
              "'${model.dealer_matrix}',"
              "'${model.ppd_date}',"
              "'${model.ppd_cancel_date}',"
              "'${model.dp_chasis_net}',"
              "'${model.dp_chasis_gross}',"
              "'${model.ujrah_price}',"
              "'${model.total_dp_krs}',"
              "'${model.name_order_source}',"
              "'${model.name_order_source_desc}',"
              "'${model.third_party_desc}',"
              "'${model.edit_financing_type}',"
              "'${model.edit_buss_activities}',"
              "'${model.edit_buss_activities_type}',"
              "'${model.edit_group_object}',"
              "'${model.edit_object}',"
              "'${model.edit_product_type}',"
              "'${model.edit_brand_object}',"
              "'${model.edit_object_type}',"
              "'${model.edit_object_model}',"
              "'${model.edit_model_detail}',"
              "'${model.edit_object_used}',"
              "'${model.edit_object_purpose}',"
              "'${model.edit_group_sales}',"
              "'${model.edit_grup_id}',"
              "'${model.edit_order_source}',"
              "'${model.edit_name_order_source}',"
              "'${model.edit_third_party_type}',"
              "'${model.edit_third_party_id}',"
              "'${model.edit_dealer_matrix}',"
              "'${model.edit_work}',"
              "'${model.edit_sentra_d}',"
              "'${model.edit_unit_d}',"
              "'${model.edit_program}',"
              "'${model.edit_rehab_type}',"
              "'${model.edit_reference_no}')"
      );
    }
//    db.close();
  }

  Future<bool> deleteMS2ApplObject() async{
    bool _isDelete = false;
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      var _check = await db.rawQuery("SELECT * from MS2_APPL_OBJECT where appl_objt_id = '$orderNo'");
      print(_check);
      print(orderNo);
      if(_check.isNotEmpty){
        await db.rawQuery("delete from MS2_APPL_OBJECT where appl_objt_id = '$orderNo'");
        _isDelete = true;
      } else {
        _isDelete = true;
      }
    }
    // db.close();
    return _isDelete;
  }

  Future<List> selectMS2ApplObject() async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * FROM MS2_APPL_OBJECT WHERE appl_objt_id = '$orderNo'");
    }
    return _data;
  }

  Future<void> updateMS2ApplObjectInfStrukturKredit (MS2ApplObjectModel model) async {
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawQuery("UPDATE MS2_APPL_OBJECT "
          "SET installment_type = '${model.installment_type}',"
          "installment_type_desc = '${model.installment_type_desc}',"
          "appl_top = '${model.appl_top}',"
          "payment_method = '${model.payment_method}',"
          "payment_method_desc = '${model.payment_method_desc}',"
          "eff_rate = '${model.eff_rate}',"
          "flat_rate = '${model.flat_rate}',"
          "object_price = '${model.object_price}',"
          "karoseri_price = '${model.karoseri_price}',"
          "total_amt = '${model.total_amt}',"
          "dp_net = '${model.dp_net}',"
          "payment_per_year = '${model.payment_per_year}',"
          "dp_branch = '${model.dp_branch}',"
          "dp_gross = '${model.dp_gross}',"
          "principal_amt = '${model.principal_amt}',"
          "installment_paid = '${model.installment_paid}',"
          "interest_amt = '${model.interest_amt}',"
          "ltv = '${model.ltv}',"
          "balloon_type = '${model.balloon_type}',"
          "last_percen_installment = '${model.last_percen_installment}',"
          "last_installment_amt = '${model.last_installment_amt}',"
          "modified_date = '${model.modified_date}',"
          "modified_by = '${model.modified_by}' "
          "WHERE appl_objt_id = '$orderNo'"
      );
    }
   // db.close();
  }

  // Inf Struktur Kredit BP
  void updateMS2ApplObjectInfStrukturKreditGP (MS2ApplObjectModel model) async {
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawUpdate("UPDATE MS2_APPL_OBJECT "
          "SET gp_type = '${model.gp_type}',"
          "new_tenor = '${model.new_tenor}' "
          "WHERE appl_objt_id = '$orderNo'"
      );
    }
    // db.close();
  }

  // Inf Struktur Kredit Stepping
  void updateMS2ApplObjectInfStrukturStepping (MS2ApplObjectModel model) async {
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawUpdate("UPDATE MS2_APPL_OBJECT "
          "SET total_stepping = '${model.total_stepping}' "
          "WHERE appl_objt_id = '$orderNo'"
      );
    }
    // db.close();
  }

  // Inf Struktur Kredit BP
  void updateMS2ApplObjectInfStrukturKreditBP (MS2ApplObjectModel model) async {
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawUpdate("UPDATE MS2_APPL_OBJECT "
          "SET balloon_type = '${model.balloon_type}',"
          "last_percen_installment = '${model.last_percen_installment}',"
          "last_installment_amt = '${model.last_installment_amt}',"
          "modified_date = '${model.modified_date}',"
          "modified_by = '${model.modified_by}' "
          "WHERE appl_objt_id = '$orderNo'"
      );
    }
    // db.close();
  }

  // Inf Struktur Kredit Income
  void updateMS2ApplObjectInfStrukturKreditIncome (MS2ApplObjectModel model) async {
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawUpdate("UPDATE MS2_APPL_OBJECT "
          "SET dsr = '${model.dsr}',"
          "dir = '${model.dir}',"
          "dsc = '${model.dsc}',"
          "irr = '${model.irr}' "
          "WHERE appl_objt_id = '$orderNo'"
      );
    }
    // db.close();
  }

  Future<List> selectMS2ApplObjectInfStrukturKreditIncome() async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * FROM MS2_APPL_OBJECT WHERE appl_objt_id = '$orderNo'");
    }
    return _data;
  }

  // Inf Colla Oto
  Future<void> insertMS2ApplObjtCollOto (MS2ApplObjtCollOtoModel model) async {
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawInsert(
          "INSERT INTO MS2_APPL_OBJT_COLL_OTO (colla_oto_id, collateralID,flag_multi_coll, flag_coll_is_unit, flag_coll_name_is_appl, id_type, "
              "id_type_desc, id_no, colla_name, date_of_birth, place_of_birth, "
              "place_of_birth_kabkota, place_of_birth_kabkota_desc, group_object, group_object_desc, object, "
              "object_desc, product_type, product_type_desc, brand_object, brand_object_desc, object_type, object_type_desc, "
              "object_model, object_model_desc, object_purpose, object_purpose_desc, mfg_year, "
              "registration_year, yellow_plate, built_up, bpkp_no, frame_no, "
              "engine_no, police_no, grade_unit, utj_facilities, bidder_name, "
              "showroom_price, add_equip, mp_adira, rekondisi_fisik, result, "
              "ltv, dp_jaminan, max_ph, taksasi_price, cola_purpose, "
              "cola_purpose_desc, capacity, color, made_in, made_in_desc, "
              "serial_no, no_invoice, stnk_valid, bpkp_address, active, "
              "created_date, created_by, modified_date, modified_by, bpkb_id_type, bpkb_id_type_desc, mp_adira_upld, "
              "edit_flag_multi_coll, edit_flag_coll_is_unit, edit_flag_coll_name_is_appl, edit_id_type, edit_id_no, "
              "edit_colla_name, edit_date_of_birth, edit_place_of_birth, edit_place_of_birth_kabkota, edit_group_object, "
              "edit_object, edit_product_type, edit_brand_object, edit_object_type, edit_object_model, edit_object_purpose, "
              "edit_mfg_year, edit_registration_year, edit_yellow_plate, edit_built_up, edit_bpkp_no, edit_frame_no, "
              "edit_engine_no, edit_police_no, edit_grade_unit, edit_utj_facilities, edit_bidder_name, edit_showroom_price, "
              "edit_add_equip, edit_mp_adira, edit_mp_adira_upld, edit_rekondisi_fisik, edit_result, edit_ltv, edit_dp_jaminan, "
              "edit_max_ph, edit_taksasi_price, edit_cola_purpose, edit_capacity, edit_color, edit_made_in, edit_serial_no, "
              "edit_no_invoice, edit_stnk_valid, edit_bpkp_address, edit_bpkb_id_type)"
              "VALUES("
              "'$orderNo',"
              "'${model.collateralID}',"
              "'${model.flag_multi_coll}',"
              "'${model.flag_coll_is_unit}',"
              "'${model.flag_coll_name_is_appl}',"
              "'${model.id_type}',"
              "'${model.id_type_desc}',"
              "'${model.id_no}',"
              "'${model.colla_name}',"
              "'${model.date_of_birth}',"
              "'${model.place_of_birth}',"
              "'${model.place_of_birth_kabkota}',"
              "'${model.place_of_birth_kabkota_desc}',"
              "'${model.group_object}',"
              "'${model.group_object_desc}',"
              "'${model.object}',"
              "'${model.object_desc}',"
              "'${model.product_type}',"
              "'${model.product_type_desc}',"
              "'${model.brand_object}',"
              "'${model.brand_object_desc}',"
              "'${model.object_type}',"
              "'${model.object_type_desc}',"
              "'${model.object_model}',"
              "'${model.object_model_desc}',"
              "'${model.object_purpose}',"
              "'${model.object_purpose_desc}',"
              "'${model.mfg_year}',"
              "'${model.registration_year}',"
              "'${model.yellow_plate}',"
              "'${model.built_up}',"
              "'${model.bpkp_no}',"
              "'${model.frame_no}',"
              "'${model.engine_no}',"
              "'${model.police_no}',"
              "'${model.grade_unit}',"
              "'${model.utj_facilities}',"
              "'${model.bidder_name}',"
              "'${model.showroom_price}',"
              "'${model.add_equip}',"
              "'${model.mp_adira}',"
              "'${model.rekondisi_fisik}',"
              "'${model.result}',"
              "'${model.ltv}',"
              "'${model.dp_jaminan}',"
              "'${model.max_ph}',"
              "'${model.taksasi_price}',"
              "'${model.cola_purpose}',"
              "'${model.cola_purpose_desc}',"
              "'${model.capacity}',"
              "'${model.color}',"
              "'${model.made_in}',"
              "'${model.made_in_desc}',"
              "'${model.serial_no}',"
              "'${model.no_invoice}',"
              "'${model.stnk_valid}',"
              "'${model.bpkp_address}',"
              "'${model.active}',"
              "'${model.created_date}',"
              "'${model.created_by}',"
              "'${model.modified_date}',"
              "'${model.modified_by}',"
              "'${model.bpkb_id_type}',"
              "'${model.bpkb_id_type_desc}',"
              "'${model.mp_adira_upld}',"
              "'${model.edit_flag_multi_coll}',"
              "'${model.edit_flag_coll_is_unit}',"
              "'${model.edit_flag_coll_name_is_appl}',"
              "'${model.edit_id_type}',"
              "'${model.edit_id_no}',"
              "'${model.edit_colla_name}',"
              "'${model.edit_date_of_birth}',"
              "'${model.edit_place_of_birth}',"
              "'${model.edit_place_of_birth_kabkota}',"
              "'${model.edit_group_object}',"
              "'${model.edit_object}',"
              "'${model.edit_product_type}',"
              "'${model.edit_brand_object}',"
              "'${model.edit_object_type}',"
              "'${model.edit_object_model}',"
              "'${model.edit_object_purpose}',"
              "'${model.edit_mfg_year}',"
              "'${model.edit_registration_year}',"
              "'${model.edit_yellow_plate}',"
              "'${model.edit_built_up}',"
              "'${model.edit_bpkp_no}',"
              "'${model.edit_frame_no}',"
              "'${model.edit_engine_no}',"
              "'${model.edit_police_no}',"
              "'${model.edit_grade_unit}',"
              "'${model.edit_utj_facilities}',"
              "'${model.edit_bidder_name}',"
              "'${model.edit_showroom_price}',"
              "'${model.edit_add_equip}',"
              "'${model.edit_mp_adira}',"
              "'${model.edit_mp_adira_upld}',"
              "'${model.edit_rekondisi_fisik}',"
              "'${model.edit_result}',"
              "'${model.edit_ltv}',"
              "'${model.edit_dp_jaminan}',"
              "'${model.edit_max_ph}',"
              "'${model.edit_taksasi_price}',"
              "'${model.edit_cola_purpose}',"
              "'${model.edit_capacity}',"
              "'${model.edit_color}',"
              "'${model.edit_made_in}',"
              "'${model.edit_serial_no}',"
              "'${model.edit_no_invoice}',"
              "'${model.edit_stnk_valid}',"
              "'${model.edit_bpkp_address}',"
              "'${model.edit_bpkb_id_type}')"
      );
      debugPrint("colla oto ${await db.rawQuery("SELECT * FROM MS2_APPL_OBJT_COLL_OTO WHERE colla_oto_id = '$orderNo'")}");
    }
    // db.close();
  }

  //delete colla oto
  Future<bool> deleteApplCollaOto() async{
    bool _isDelete = false;
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      var _check = await db.rawQuery("SELECT * from MS2_APPL_OBJT_COLL_OTO where colla_oto_id = '$orderNo'");
      print(_check);
      print(orderNo);
      if(_check.isNotEmpty){

        await db.rawQuery("delete from MS2_APPL_OBJT_COLL_OTO where colla_oto_id = '$orderNo'");
        _isDelete = true;
      } else {
        _isDelete = true;
      }
    }
    return _isDelete;
  }

  // Inf Colla Properti
  Future<void> insertMS2ApplObjtCollProp (MS2ApplObjtCollPropModel model) async {
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawInsert(
          "INSERT INTO MS2_APPL_OBJT_COLL_PROP (colla_prop_id, collateralID, flag_multi_coll, flag_coll_name_is_appl, id_type, id_type_desc, "
              "id_no, cola_name, date_of_birth, place_of_birth, place_of_birth_kabkota, "
              "place_of_birth_kabkota_desc, sertifikat_no, seritifikat_type, seritifikat_type_desc, property_type, "
              "property_type_desc, building_area, land_area, dp_guarantee, max_ph, "
              "taksasi_price, cola_purpose, cola_purpose_desc, jarak_fasum_positif, jarak_fasum_negatif, "
              "land_price, njop_price, building_price, roof_type, roof_type_desc, "
              "wall_type, wall_type_desc, floor_type, floor_type_desc, foundation_type, "
              "foundation_type_desc, road_type, road_type_desc, flag_car_pas, no_of_house, "
              "guarantee_character, grntr_evdnce_ownr, pblsh_sertifikat_date, pblsh_sertifikat_year, license_name, "
              "no_srt_ukur, tgl_srt_ukur, srtfkt_pblsh_by, expire_right, imb_no, "
              "imb_date, size_of_imb, ltv, land_location, active, "
              "created_date, created_by, modified_date, modified_by, "
              "edit_flag_multi_coll, edit_flag_coll_name_is_appl, edit_id_type, edit_id_no, edit_cola_name,"
              " edit_date_of_birth, edit_place_of_birth, edit_place_of_birth_kabkota, edit_sertifikat_no, "
              "edit_seritifikat_type, edit_property_type, edit_building_area, edit_land_area, edit_dp_guarantee,"
              " edit_max_ph, edit_taksasi_price, edit_cola_purpose, edit_jarak_fasum_positif, edit_jarak_fasum_negatif,"
              " edit_land_price, edit_njop_price, edit_building_price, edit_roof_type, edit_wall_type, edit_floor_type, "
              "edit_foundation_type, edit_road_type, edit_flag_car_pas, edit_no_of_house, edit_guarantee_character, "
              "edit_grntr_evdnce_ownr, edit_pblsh_sertifikat_date, edit_pblsh_sertifikat_year, edit_license_name, "
              "edit_no_srt_ukur, edit_tgl_srt_ukur, edit_srtfkt_pblsh_by, edit_expire_right, edit_imb_no, edit_imb_date, edit_size_of_imb, edit_ltv, edit_land_location, )"
              "VALUES("
              "'$orderNo',"
              "'${model.collateralID}',"
              "'${model.flag_multi_coll}',"
              "'${model.flag_coll_name_is_appl}',"
              "'${model.id_type}',"
              "'${model.id_type_desc}',"
              "'${model.id_no}',"
              "'${model.cola_name}',"
              "'${model.date_of_birth}',"
              "'${model.place_of_birth}',"
              "'${model.place_of_birth_kabkota}',"
              "'${model.place_of_birth_kabkota_desc}',"
              "'${model.sertifikat_no}',"
              "'${model.seritifikat_type}',"
              "'${model.seritifikat_type_desc}',"
              "'${model.property_type}',"
              "'${model.property_type_desc}',"
              "'${model.building_area}',"
              "'${model.land_area}',"
              "'${model.dp_guarantee}',"
              "'${model.max_ph}',"
              "'${model.taksasi_price}',"
              "'${model.cola_purpose}',"
              "'${model.cola_purpose_desc}',"
              "'${model.jarak_fasum_positif}',"
              "'${model.jarak_fasum_negatif}',"
              "'${model.land_price}',"
              "'${model.njop_price}',"
              "'${model.building_price}',"
              "'${model.roof_type}',"
              "'${model.roof_type_desc}',"
              "'${model.wall_type}',"
              "'${model.wall_type_desc}',"
              "'${model.floor_type}',"
              "'${model.floor_type_desc}',"
              "'${model.foundation_type}',"
              "'${model.foundation_type_desc}',"
              "'${model.road_type}',"
              "'${model.road_type_desc}',"
              "'${model.flag_car_pas}',"
              "'${model.no_of_house}',"
              "'${model.guarantee_character}',"
              "'${model.grntr_evdnce_ownr}',"
              "'${model.pblsh_sertifikat_date}',"
              "'${model.pblsh_sertifikat_year}',"
              "'${model.license_name}',"
              "'${model.no_srt_ukur}',"
              "'${model.tgl_srt_ukur}',"
              "'${model.srtfkt_pblsh_by}',"
              "'${model.expire_right}',"
              "'${model.imb_no}',"
              "'${model.imb_date}',"
              "'${model.size_of_imb}',"
              "'${model.ltv}',"
              "'${model.land_location}',"
              "'${model.active}',"
              "'${model.created_date}',"
              "'${model.created_by}',"
              "'${model.modified_date}',"
              "'${model.modified_by}',"
              "'${model.edit_flag_multi_coll}',"
              "'${model.edit_flag_coll_name_is_appl}',"
              "'${model.edit_id_type}',"
              "'${model.edit_id_no}',"
              "'${model.edit_cola_name}',"
              "'${model.edit_date_of_birth}',"
              "'${model.edit_place_of_birth}',"
              "'${model.edit_place_of_birth_kabkota}',"
              "'${model.edit_sertifikat_no}',"
              "'${model.edit_seritifikat_type}',"
              "'${model.edit_property_type}',"
              "'${model.edit_building_area}',"
              "'${model.edit_land_area}',"
              "'${model.edit_dp_guarantee}',"
              "'${model.edit_max_ph}',"
              "'${model.edit_taksasi_price}',"
              "'${model.edit_cola_purpose}',"
              "'${model.edit_jarak_fasum_positif}',"
              "'${model.edit_jarak_fasum_negatif}',"
              "'${model.edit_land_price}',"
              "'${model.edit_njop_price}',"
              "'${model.edit_building_price}',"
              "'${model.edit_roof_type}',"
              "'${model.edit_wall_type}',"
              "'${model.edit_floor_type}',"
              "'${model.edit_foundation_type}',"
              "'${model.edit_road_type}',"
              "'${model.edit_flag_car_pas}',"
              "'${model.edit_no_of_house}',"
              "'${model.edit_guarantee_character}',"
              "'${model.edit_grntr_evdnce_ownr}',"
              "'${model.edit_pblsh_sertifikat_date}',"
              "'${model.edit_pblsh_sertifikat_year}',"
              "'${model.edit_license_name}',"
              "'${model.edit_no_srt_ukur}',"
              "'${model.edit_tgl_srt_ukur}',"
              "'${model.edit_srtfkt_pblsh_by}',"
              "'${model.edit_expire_right}',"
              "'${model.edit_imb_no}',"
              "'${model.edit_imb_date}',"
              "'${model.edit_size_of_imb}',"
              "'${model.edit_ltv}',"
              "'${model.edit_land_location}')"
      );
    }
    // db.close();
  }

  //delete colla property
  Future<bool> deleteApplCollaProp() async{
    bool _isDelete = false;
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      var _check = await db.rawQuery("SELECT * from MS2_APPL_OBJT_COLL_PROP where colla_prop_id = '$orderNo'");
      print(_check);
      print(orderNo);
      if(_check.isNotEmpty){
        await db.rawQuery("delete from MS2_APPL_OBJT_COLL_PROP where colla_prop_id = '$orderNo'");
        _isDelete = true;
      } else {
        _isDelete = true;
      }
    }
    return _isDelete;
  }

  // Inf Asuransi Utama & Perluasan (2 & 3)
  Future<void> insertMS2ApplInsuranceMain (List<MS2ApplInsuranceModel> model) async {
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      for(int i = 0; i < model.length; i++) {
        await db.rawInsert(
            "INSERT INTO MS2_APPL_INSURANCE (insurance_id, orderProductInsuranceID, cola_type, type_of_insurance, insuran_index, insuran_type_parent, "
                "insuran_type, insuran_type_desc, company_id, company_desc, company_parent, product, product_desc, period, type, type_1, type_1_desc"
                ", type_1_parent, type_2, type_2_desc, type_2_parent, sub_type, sub_type_coverage, coverage_type, coverage_type_desc, coverage_amt, "
                "upper_limit_pct, upper_limit_amt, lower_limit_pct, lower_limit_amt, cash_amt, credit_amt, total_split, "
                "total_pct, total_amt, created_date, created_by, modified_date, modified_by, active, idx, "
                "edit_insuran_type_parent, edit_insuran_index, edit_company_id, edit_product, edit_period, edit_type, "
                "edit_type_1, edit_type_2, edit_coverage_type, edit_coverage_amt, edit_upper_limit_pct, edit_upper_limit_amt, "
                "edit_lower_limit_pct, edit_lower_limit_amt, edit_cash_amt, edit_credit_amt, edit_total_split, edit_total_pct, edit_total_amt)"
                "VALUES("
                "'$orderNo',"
                "'${model[i].orderProductInsuranceID}',"
                "'${model[i].cola_type}',"
                "'1',"
                "'${model[i].insuran_index}',"
                "'${model[i].insurance_type_parent}',"
                "'${model[i].insurance_type}',"
                "'${model[i].insurance_type_desc}',"
                "'${model[i].company_id}',"
                "'${model[i].company_desc}',"
                "'${model[i].company_parent}',"
                "'${model[i].product}',"
                "'${model[i].product_desc}',"
                "'${model[i].period}',"
                "'${model[i].type}',"
                "'${model[i].type_1}',"
                "'${model[i].type_1_desc}',"
                "'${model[i].type_1_parent}',"
                "'${model[i].type_2}',"
                "'${model[i].type_2_desc}',"
                "'${model[i].type_2_parent}',"
                "'${model[i].sub_type}',"
                "'${model[i].sub_type_coverage}',"
                "'${model[i].coverage_type}',"
                "'${model[i].coverage_type_desc}',"
                "'${model[i].coverage_amt}',"
                "'${model[i].upper_limit_pct}',"
                "'${model[i].upper_limit_amt}',"
                "'${model[i].lower_limit_pct}',"
                "'${model[i].lower_limit_amt}',"
                "'${model[i].cash_amt}',"
                "'${model[i].credit_amt}',"
                "'${model[i].total_split}',"
                "'${model[i].total_pct}',"
                "'${model[i].total_amt}',"
                "'${model[i].created_date}',"
                "'${model[i].created_by}',"
                "'${model[i].modified_date}',"
                "'${model[i].modified_by}',"
                "'${model[i].active}',"
                "'${model[i].idx}',"
                "'${model[i].edit_insuran_type_parent}',"
                "'${model[i].edit_insuran_index}',"
                "'${model[i].edit_company_id}',"
                "'${model[i].edit_product}',"
                "'${model[i].edit_period}',"
                "'${model[i].edit_type}',"
                "'${model[i].edit_type_1}',"
                "'${model[i].edit_type_2}',"
                "'${model[i].edit_coverage_type}',"
                "'${model[i].edit_coverage_amt}',"
                "'${model[i].edit_upper_limit_pct}',"
                "'${model[i].edit_upper_limit_amt}',"
                "'${model[i].edit_lower_limit_pct}',"
                "'${model[i].edit_lower_limit_amt}',"
                "'${model[i].edit_cash_amt}',"
                "'${model[i].edit_credit_amt}',"
                "'${model[i].edit_total_split}',"
                "'${model[i].edit_total_pct}',"
                "'${model[i].edit_total_amt}')"
        );
      }
    }
//    db.close();
  }

  Future<bool> deleteMS2ApplInsuranceMain() async{
    bool _isDelete = false;
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      var _check = await db.rawQuery("SELECT * from MS2_APPL_INSURANCE where insurance_id = '$orderNo' AND insuran_type = 2");
      if(_check.isNotEmpty){
        await db.rawQuery("delete from MS2_APPL_INSURANCE where insurance_id = '$orderNo' AND insuran_type = 2");
        _isDelete = true;
      } else {
        _isDelete = true;
      }
    }
    return _isDelete;
  }

  Future<bool> deleteMS2ApplInsurancePeruluasan() async{
    bool _isDelete = false;
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      var _check = await db.rawQuery("SELECT * from MS2_APPL_INSURANCE where insurance_id = '$orderNo' AND insuran_type = 3");
      if(_check.isNotEmpty){
        await db.rawQuery("delete from MS2_APPL_INSURANCE where insurance_id = '$orderNo' AND insuran_type = 3");
        _isDelete = true;
      } else {
        _isDelete = true;
      }
    }
    return _isDelete;
  }

  Future<List> selectMS2ApplInsuranceMain() async{
    List _data = [];
    if(await setOrderNumber()){
      List _insurance1 = [];
      List _insurance2 = [];
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _insurance1 = await db.rawQuery("SELECT * FROM MS2_APPL_INSURANCE WHERE insurance_id = '$orderNo' AND insuran_type = 2");
      for(int i=0; i<_insurance1.length; i++){
        _data.add(_insurance1[i]);
      }
      _insurance2 = await db.rawQuery("SELECT * FROM MS2_APPL_INSURANCE WHERE insurance_id = '$orderNo' AND insuran_type = 3");
      for(int i=0; i<_insurance2.length; i++){
        _data.add(_insurance2[i]);
      }
    }
    return _data;
  }

  // Inf Asuransi Tambahan (1)
  void insertMS2ApplInsuranceAdditional (List<MS2ApplInsuranceModel> model) async {
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      for(int i = 0; i < model.length; i++) {
        await db.rawInsert(
            "INSERT INTO MS2_APPL_INSURANCE (insurance_id,orderProductInsuranceID,cola_type, type_of_insurance, insuran_type_parent, insuran_type, "
                "insuran_type_desc, company_id, company_desc, product, product_desc, period, type, type_1, type_2, sub_type, "
                "sub_type_coverage, coverage_type, coverage_type_desc, coverage_amt, upper_limit_pct, upper_limit_amt, "
                "lower_limit_pct, lower_limit_amt, cash_amt, credit_amt, total_split, "
                "total_pct, total_amt, created_date, created_by, modified_date, "
                "modified_by, active, idx, edit_insuran_type_parent, edit_company_id, edit_product, edit_period, "
                "edit_coverage_type, edit_coverage_amt, edit_upper_limit_pct, edit_upper_limit_amt, edit_lower_limit_pct, "
                "edit_lower_limit_amt, edit_cash_amt, edit_credit_amt, edit_total_pct, edit_total_amt)"
                "VALUES("
                "'$orderNo',"
                "'${model[i].orderProductInsuranceID}',"
                "'${model[i].cola_type}',"
                "'2',"
                "'${model[i].insurance_type_parent}',"
                "'${model[i].insurance_type}',"
                "'${model[i].insurance_type_desc}',"
                "'${model[i].company_id}',"
                "'${model[i].company_desc}',"
                "'${model[i].product}',"
                "'${model[i].product_desc}',"
                "'${model[i].period}',"
                "'${model[i].type}',"
                "'${model[i].type_1}',"
                "'${model[i].type_2}',"
                "'${model[i].sub_type}',"
                "'${model[i].sub_type_coverage}',"
                "'${model[i].coverage_type}',"
                "'${model[i].coverage_type_desc}',"
                "'${model[i].coverage_amt}',"
                "'${model[i].upper_limit_pct}',"
                "'${model[i].upper_limit_amt}',"
                "'${model[i].lower_limit_pct}',"
                "'${model[i].lower_limit_amt}',"
                "'${model[i].cash_amt}',"
                "'${model[i].credit_amt}',"
                "'${model[i].total_split}',"
                "'${model[i].total_pct}',"
                "'${model[i].total_amt}',"
                "'${model[i].created_date}',"
                "'${model[i].created_by}',"
                "'${model[i].modified_date}',"
                "'${model[i].modified_by}',"
                "'${model[i].active}',"
                "'${model[i].idx}',"
                "'${model[i].edit_insuran_type_parent}',"
                "'${model[i].edit_company_id}',"
                "'${model[i].edit_product}',"
                "'${model[i].edit_period}',"
                "'${model[i].edit_coverage_type}',"
                "'${model[i].edit_coverage_amt}',"
                "'${model[i].edit_upper_limit_pct}',"
                "'${model[i].edit_upper_limit_amt}',"
                "'${model[i].edit_lower_limit_pct}',"
                "'${model[i].edit_lower_limit_amt}',"
                "'${model[i].edit_cash_amt}',"
                "'${model[i].edit_credit_amt}',"
                "'${model[i].edit_total_pct}',"
                "'${model[i].edit_total_amt}')"
        );
      }
    }
    // db.close();
  }

  Future<bool> deleteMS2ApplInsuranceAdditional() async{
    bool _isDelete = false;
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      var _check = await db.rawQuery("SELECT * from MS2_APPL_INSURANCE where insurance_id = '$orderNo' AND insuran_type = 1");
      print(_check);
      print(orderNo);
      if(_check.isNotEmpty){
        await db.rawQuery("delete from MS2_APPL_INSURANCE where insurance_id = '$orderNo' AND insuran_type = 1");
        _isDelete = true;
      } else {
        _isDelete = true;
      }
    }
    return _isDelete;
  }

  Future<List> selectMS2ApplInsuranceAdditional() async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * FROM MS2_APPL_INSURANCE WHERE insurance_id = '$orderNo' AND insuran_type = 1");
    }
    return _data;
  }

  // Inf WMP
  Future<void> insertMS2ApplObjtWmp (List<MS2ApplObjtWmpModel> model) async {
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      for(int i=0; i<model.length; i++){
        await db.rawInsert(
            "INSERT INTO MS2_APPL_OBJT_WMP(wmp_id, orderWmpID, wmp_no, wmp_type, wmp_type_desc, wmp_subsidy_type_id, wmp_subsidy_type_id_desc, "
                "max_refund_amt, active, created_date, created_by, edit_wmp_no, edit_wmp_type, edit_wmp_subsidy_type_id, edit_max_refund_amt) "
                "VALUES("
                "'$orderNo',"
                "'${model[i].orderWmpID}',"
                "'${model[i].wmp_no}',"
                "'${model[i].wmp_type}',"
                "'${model[i].wmp_type_desc}',"
                "'${model[i].wmp_subsidy_type_id}',"
                "'${model[i].wmp_subsidy_type_id_desc}',"
                "'${model[i].max_refund_amt}',"
                "1,"
                "'$dateTime',"
                "'$createdBy',"
                "'${model[i].edit_wmp_no}',"
                "'${model[i].edit_wmp_type}',"
                "'${model[i].edit_wmp_subsidy_type_id}',"
                "'${model[i].edit_max_refund_amt}')"
        );
      }
    }
    // db.close();
  }

  Future<bool> deleteMS2ApplOBJWmp() async{
    bool _isDelete = false;
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      var _check = await db.rawQuery("SELECT * from MS2_APPL_OBJT_WMP where wmp_id = '$orderNo'");
      print(_check);
      print(orderNo);
      if(_check.isNotEmpty){
        await db.rawQuery("delete from MS2_APPL_OBJT_WMP where wmp_id = '$orderNo'");
        _isDelete = true;
      } else {
        _isDelete = true;
      }
    }
    return _isDelete;
  }

  Future<List> selectMS2ApplObjtWmp() async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * FROM MS2_APPL_OBJT_WMP WHERE wmp_id = '$orderNo' ORDER BY wmp_subsidy_type_id ASC");
    }
    return _data;
  }

  // Inf Kredit Subsidi Detail
  Future<void> insertMS2ApplRefund (List<MS2ApplRefundModel> model) async {
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      for(int i = 0; i < model.length; i++) {
        await db.rawInsert(
            "INSERT INTO MS2_APPL_REFUND (order_no, orderSubsidyID, giver_refund, type_refund, type_refund_desc, deduction_method, "
                "deduction_method_desc, refund_amt, eff_rate_bef, flat_rate_bef, dp_real, "
                "installment_amt, active, created_date, created_by, interest_rate, refund_amt_klaim, "
                "edit_giver_refund, edit_type_refund, edit_deduction_method, edit_refund_amt)"
                "VALUES("
                "'$orderNo',"
                "'${model[i].orderSubsidyID}',"
                "'${model[i].giver_refund}',"
                "'${model[i].type_refund}',"
                "'${model[i].type_refund_desc}',"
                "'${model[i].deduction_method}',"
                "'${model[i].deduction_method_desc}',"
                "'${model[i].refund_amt}',"
                "'${model[i].eff_rate_bef}',"
                "'${model[i].flat_rate_bef}',"
                "'${model[i].dp_real}',"
                "'${model[i].installment_amt}',"
                "1,"
                "'$dateTime',"
                "'$createdBy',"
                "'${model[i].interest_rate}',"
                "'${model[i].refund_amt_klaim}',"
                "'${model[i].edit_giver_refund}',"
                "'${model[i].edit_type_refund}',"
                "'${model[i].edit_deduction_method}',"
                "'${model[i].edit_refund_amt}')"
        );
        if(model[i].installmentDetail != null){
          debugPrint("INSERT_DETAIL_SUBSIDY ${await selectMS2ApplRefund()}");
          var _lastInsert = await selectMS2ApplRefund();
          var _lastID = _lastInsert[0]['subsidy_id'];
          for(int j=0; j<model[i].installmentDetail.length; j++){
            insertMS2ApplRefundDetail(MS2ApplRefundDetailModel(
                "$orderNo",
                model[i].installmentDetail[j].orderSubsidyDetailID,
                int.parse(model[i].installmentDetail[j].installmentIndex),
                double.parse(model[i].installmentDetail[j].installmentSubsidy.replaceAll(",", "")),
                null,
                null,
                null,
                null,
                null
            ), _lastID.toString());
          }
        }
      }
    }
    // db.close();
  }

  void insertMS2ApplRefundDetail (MS2ApplRefundDetailModel model, String lastID) async {
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      debugPrint('subsidy_id $lastID');
      await db.rawInsert("INSERT INTO MS2_APPL_REFUND_DETAIL (subsidy_id, orderSubsidyDetailID,ac_installment_no, ac_installment_amt, "
          "created_date, created_by, active)"
          "VALUES("
          "'$lastID',"
          "'${model.orderSubsidyDetailID}',"
          "${model.ac_installment_no},"
          "${model.ac_installment_amt.toInt()},"
          "'$dateTime',"
          "'$createdBy',"
          "'1')"
      );
    }
  }

  // delete header credit subsidy
  Future<bool> deleteMS2ApplRefundHeader() async{
    bool _isDelete = false;
    List _lastInsert = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _lastInsert = await db.rawQuery("SELECT * FROM MS2_APPL_REFUND WHERE order_no = '$orderNo'");
      if(_lastInsert.isNotEmpty){
        for(int i=0; i<_lastInsert.length; i++){
          var _lastID = _lastInsert[i]['subsidy_id'];
          await deleteMS2ApplRefundDetail(_lastID.toString());
        }
        await db.rawQuery("delete from MS2_APPL_REFUND where order_no = '$orderNo'");
        _isDelete = true;
      } else {
        _isDelete = true;
      }
    }
    return _isDelete;
    // db.close();
  }

  Future<void> deleteMS2ApplRefundDetail(String id) async{
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawQuery("delete from MS2_APPL_REFUND_DETAIL where subsidy_id = '$id'");
  }

  Future<List> selectMS2ApplRefund() async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * FROM MS2_APPL_REFUND WHERE order_no = '$orderNo'");
    }
    return _data;
  }

  Future<List> selectMS2ApplRefundDetail(String id) async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * FROM MS2_APPL_REFUND_DETAIL WHERE subsidy_id = '$id'");
    }
    return _data;
  }

  // Taksasi Unit
  Future<void> insertMS2ApplTaksasi (List<MS2ApplTaksasiModel> model) async {
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      for(int i=0; i < model.length; i++){
        await db.rawInsert(
            "INSERT INTO MS2_APPL_TAKSASI (order_no, unit_type, unit_type_desc, taksasi_code, taksasi_desc, "
                "nilai_input_rekondisi, created_date, created_by, modified_date, modified_by, "
                "active)"
                "VALUES("
                "'$orderNo',"
                "'${model[i].unit_type}',"
                "'${model[i].unit_type_desc}',"
                "'${model[i].taksasi_code}',"
                "'${model[i].taksasi_desc}',"
                "'${model[i].nilai_input_rekondisi}',"
                "'${model[i].created_date}',"
                "'${model[i].created_by}',"
                "'${model[i].modified_date}',"
                "'${model[i].modified_by}',"
                "'${model[i].active}')"
        );
      }
    }
    // db.close();
  }

  Future<bool> deleteMS2ApplTaksasi() async{
    bool _isDelete = false;
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      var _check = await db.rawQuery("SELECT * from MS2_APPL_TAKSASI where order_no = '$orderNo'");
      if(_check.isNotEmpty){
        await db.rawQuery("delete from MS2_APPL_TAKSASI where order_no = '$orderNo'");
        _isDelete = true;
      } else {
        _isDelete = true;
      }
    }
    return _isDelete;
  }

  // select taksasi
  Future<List> selectTaksasi() async{
    List _checkData = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _checkData = await db.rawQuery("SELECT * FROM MS2_APPL_TAKSASI WHERE order_no = '$orderNo'");
    }
    return _checkData;
  }

  void insertMS2SvyAssgnmt(MS2SvyAssgnmtModel model) async {
    if (await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawInsert(
          "INSERT INTO MS2_SVY_ASSGNMT (order_no, surveyID,survey_type_id, empl_nik, handphone_no, janji_survey_date, "
              "survey_reason, process_survey, flag_brms, status_survey, active, "
              "created_date, created_by, modified_date, modified_by, survey_result,survey_result_desc, "
              "survey_recomendation,survey_recomendation_desc,notes, survey_result_date, distance_sentra, distance_deal, "
              "distance_objt_purp_sentra, status_eligible_cfo, job_surveyor)"
              "VALUES("
              "'$orderNo',"
              "'${model.surveyID}',"
              "'${model.survey_type_id}',"
              "'${model.empl_nik}',"
              "'${model.handphone_no}',"
              "'${model.janji_survey_date}',"
              "'${model.survey_reason}',"
              "'${model.process_survey}',"
              "'${model.flag_brms}',"
              "'${model.status_survey}',"
              "'${model.active}',"
              "'${model.created_date}',"
              "'${model.created_by}',"
              "'${model.modified_date}',"
              "'${model.modified_by}',"
              "'${model.survey_result}',"
              "'${model.survey_result_desc}',"
              "'${model.survey_recomendation}',"
              "'${model.survey_recomendation_desc}',"
              "'${model.notes}',"
              "'${model.survey_result_date}',"
              "'${model.distance_sentra}',"
              "'${model.distance_deal}',"
              "'${model.distance_objt_purp_sentra}',"
              "'${model.status_eligible_cfo}',"
              "'${model.job_surveyor}')"
      );
    }
    await selectMS2SvyAssgnmt();
    // db.close();
  }

  // delete result survey ms2Assignment
  Future<bool> deleteMS2SvyAssgnmt() async{
    List _checkData = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawDelete("delete from MS2_SVY_ASSGNMT where order_no = '$orderNo'");
      _checkData = await db.rawQuery("SELECT * FROM MS2_SVY_ASSGNMT WHERE order_no = '$orderNo'");
    }
    // List _checkData = await db.rawQuery("SELECT * FROM MS2_TASK_LIST WHERE order_no = '$orderNo'");
    return _checkData.isEmpty;
  }

  //select result survey ms2Assignment
  Future<List> selectMS2SvyAssgnmt() async{
    List _checkData = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _checkData = await db.rawQuery("SELECT * FROM MS2_SVY_ASSGNMT WHERE order_no = '$orderNo'");
      print("ini asignment survey $_checkData");
    }
    return _checkData;
  }

  //select result survey ms2Assignment
  Future<List> selectResultSurveyMS2Assignment() async{
    List _checkData = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _checkData = await db.rawQuery("SELECT * FROM MS2_SVY_ASSGNMT WHERE order_no = '$orderNo'");
    }
    return _checkData;
  }

  // Hasil Survey - Survey Detail
  void insertMS2SvyRsltDtl (List<MS2SvyRsltDtlModel> model) async {
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      for(int i = 0; i < model.length; i++) {
        await db.rawInsert(
            "INSERT INTO MS2_SVY_RSLT_DTL (order_no,surveyResultDetailID,info_envirnmt,info_environt_desc,info_source,info_source_desc,info_source_name, active, "
                "created_date, created_by, modified_date, modified_by)"
                "VALUES("
                "'$orderNo',"
                "'${model[i].surveyResultDetailID}',"
                "'${model[i].info_envirnmt}',"
                "'${model[i].info_environt_desc}',"
                "'${model[i].info_source}',"
                "'${model[i].info_source_desc}',"
                "'${model[i].info_source_name}',"
                "'${model[i].active}',"
                "'${model[i].created_date}',"
                "'${model[i].created_by}',"
                "'${model[i].modified_date}',"
                "'${model[i].modified_by}')"
        );
      }
    }
    print("cek masuk ga ni ${await selectResultSurveyDetail()}");
//    db.close();
  }

  // delete hasil survey detail
  Future<bool> deleteResultSurveyDetail() async{
    List _checkData = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawDelete("delete from MS2_SVY_RSLT_DTL where order_no = '$orderNo'");
      _checkData = await db.rawQuery("SELECT * FROM MS2_SVY_RSLT_DTL WHERE order_no = '$orderNo'");
    }
    // List _checkData = await db.rawQuery("SELECT * FROM MS2_TASK_LIST WHERE order_no = '$orderNo'");
    return _checkData.isEmpty;
  }

  //select result survey detail
  Future<List> selectResultSurveyDetail() async{
    List _checkData = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _checkData = await db.rawQuery("SELECT * FROM MS2_SVY_RSLT_DTL WHERE order_no = '$orderNo'");
    }
    return _checkData;
  }

  // Hasil Survey - Aset
  void insertMS2SvyRsltAsst(List<MS2SvyRsltAsstModel> model) async {
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      for(int i=0; i <model.length; i++){
        String _streetCode = model[i].street_type != null ? model[i].street_type.kode : "";
        String _streetDesc = model[i].street_type != null ? model[i].street_type.deskripsi : "";
        String _elcCode = model[i].electricity != null ? model[i].electricity.PARA_ELECTRICITY_ID : "";
        String _elcDesc = model[i].electricity != null ? model[i].electricity.PARA_ELECTRICITY_NAME : "";
        await db.rawInsert(
            "INSERT INTO MS2_SVY_RSLT_ASST (ORDER_NO, surveyResultAssetID, asset_type, asset_type_desc,asset_amt, asset_own, asset_own_desc,size_of_land, "
                "stree_type, street_type_desc,electricity, electricity_desc,electricity_bill, expired_date_contract, no_of_stay, "
                "active, created_date, created_by, modified_date, modified_by,edit_asset_type,edit_asset_amt,edit_asset_own,edit_size_of_land,edit_stree_type,"
                "edit_electricity,edit_electricity_bill,edit_expired_date_contract,edit_no_of_stay)"
                "VALUES("
                "'$orderNo',"
                "'${model[i].surveyResultAssetID}',"
                "'${model[i].asset_type.id}',"
                "'${model[i].asset_type.name}',"
                "'${model[i].asset_amt}',"
                "'${model[i].asset_own.id}',"
                "'${model[i].asset_own.name}',"
                "'${model[i].size_of_land}',"
                "'$_streetCode',"
                "'$_streetDesc',"
                "'$_elcCode',"
                "'$_elcDesc',"
                "'${model[i].electricity_bill}',"
                "'${model[i].expired_date_contract}',"
                "'${model[i].no_of_stay}',"
                "'${model[i].active}',"
                "'${model[i].created_date}',"
                "'${model[i].created_by}',"
                "'${model[i].modified_date}',"
                "'${model[i].modified_by}',"
                "'${model[i].isAssetTypeChanges}',"
                "'${model[i].isValueAssetChanges}',"
                "'${model[i].isOwneshipInfoChanges}',"
                "'${model[i].isSurfaceBuildingAreaInfoChanges}',"
                "'${model[i].isRoadTypeInfoChanges}',"
                "'${model[i].isElectricityTypeInfoChanges}',"
                "'${model[i].isElectricityBillInfoChanges}',"
                "'${model[i].isEndDateInfoChanges}',"
                "'${model[i].isLengthStayInfoChanges}')"
        );
      }
    }
    await selectResultSurveyAsset();
    // db.close();
  }

  // delete result survey asset
  Future<bool> deleteResultSurveyAsset() async{
    List _checkData = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawDelete("delete from MS2_SVY_RSLT_ASST where order_no = '$orderNo'");
      _checkData = await db.rawQuery("SELECT * FROM MS2_SVY_RSLT_ASST WHERE order_no = '$orderNo'");
    }
    return _checkData.isEmpty;
  }

  //select result survey asset
  Future<List> selectResultSurveyAsset() async{
    List _checkData = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _checkData = await db.rawQuery("SELECT * FROM MS2_SVY_RSLT_ASST WHERE order_no = '$orderNo'");
    }
    return _checkData;
  }

  Future<void> insertMS2PhotoSurvey(List<SurveyPhotoModel> model) async{
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);

    if(await setOrderNumber()){
      for(int i=0;i<model.length;i++){
        String _docTypeId = model[i].photoTypeModel == null ? "" : model[i].photoTypeModel.id;
        String _docTypeName = model[i].photoTypeModel == null ? "" : model[i].photoTypeModel.name;
        await db.rawQuery("INSERT INTO MS2_CUST_PHOTO_DETAIL(order_no, photo_id, flag, document_type, document_type_desc, "
            "is_mandatory, is_display, is_unit, created_date, created_by, active)"
            "VALUES("
            "'$orderNo',"
            "'',"
            "'3',"
            "'$_docTypeId',"
            "'$_docTypeName',"
            "'1',"
            "'1',"
            "'1',"
            "'$dateTime',"
            "'$createdBy',"
            "1)");
        var _lastInsert = await db.rawQuery('SELECT * FROM MS2_CUST_PHOTO_DETAIL ORDER BY detail_id DESC');
        var _detailID = _lastInsert[0]['detail_id'];

        //insert detail foto survey
        for(int j=0;j<model[i].listImageModel.length;j++){
          await db.rawQuery("INSERT INTO MS2_CUST_PHOTO_DETAIL_2(detail_id, order_no, flag, path_photo, latitude, longitude, file_header_id,"
              " created_date, created_by, active) "
              "VALUES("
              "'$_detailID',"
              "'$orderNo',"
              "'5',"
              "'${model[i].listImageModel[j].path}',"
              "'${model[i].listImageModel[j].latitude}',"
              "'${model[i].listImageModel[j].longitude}',"
              "'${model[i].listImageModel[j].fileHeaderID}',"
              "'$dateTime',"
              "'$createdBy',"
              "1)");
        }
      }
    }
  }

  Future<bool> deletePhotoSurvey() async{
    List _checkData = [];
    List _checkDataDetail = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawQuery("delete from MS2_CUST_PHOTO_DETAIL where order_no = '$orderNo' AND flag = 3");
      _checkData = await db.rawQuery("SELECT * FROM MS2_CUST_PHOTO_DETAIL WHERE order_no = '$orderNo' AND flag = 3");

      await db.rawQuery("delete from MS2_CUST_PHOTO_DETAIL_2 where order_no = '$orderNo' AND flag = 5");
      _checkDataDetail = await db.rawQuery("SELECT * FROM MS2_CUST_PHOTO_DETAIL_2 WHERE order_no = '$orderNo' AND flag = 5");
    }
    return _checkData.isEmpty && _checkDataDetail.isEmpty;
  }

  Future<List> selectPhotoSurvey() async{
    List _checkData = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _checkData = await db.rawQuery("SELECT * FROM MS2_CUST_PHOTO_DETAIL WHERE order_no = '$orderNo' AND flag = 3");
    }
    return _checkData;
  }

  Future<List> selectPhotoSurveyDetail() async{
    List _checkData = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _checkData = await db.rawQuery("SELECT * FROM MS2_CUST_PHOTO_DETAIL_2 WHERE order_no = '$orderNo' AND flag = 5");
    }
    return _checkData;
  }

  // save last step
  Future<void> saveLastStep(int index) async{
    print("cek index db helper last step $index");
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    String _orderDate = _preferences.getString("order_date");
    String _custName = _preferences.getString("cust_name");
    String _lastKnownState = _preferences.getString("last_known_state");
    String _user = _preferences.getString("username");
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawInsert("INSERT INTO MS2_TASK_LIST "
        "(order_no,order_date,cust_name,last_known_state,last_known_handled_by,idx,source_application)"
        " VALUES('$orderNo','$_orderDate','$_custName','$_lastKnownState','$_user','$index','AD1MS2')");
  }


  //delete save last step
  Future<bool> deleteSaveLastStep() async{
    List _checkData = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawDelete("delete from MS2_TASK_LIST where order_no = '$orderNo'");
      _checkData = await db.rawQuery("SELECT * FROM MS2_TASK_LIST WHERE order_no = '$orderNo'");
    }
    // List _checkData = await db.rawQuery("SELECT * FROM MS2_TASK_LIST WHERE order_no = '$orderNo'");
    return _checkData.isEmpty;
  }

  //select last step
  Future<List> selectLastStep() async{
    List _checkData = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _checkData = await db.rawQuery("SELECT * FROM MS2_TASK_LIST WHERE order_no = '$orderNo'");
      print(_checkData);
    }
    return _checkData;
  }

  // insert dedup data
  Future<void> insertDataDedup(DataDedupModel model,String orderNo) async{
    print(orderNo);
    if(await setOrderNumber()){
      var _motherName = model.nama_gadis_ibu_kandung != null ? "${model.nama_gadis_ibu_kandung}" : "";
      var _birthPlace = model.tempat_lahir_sesuai_identitas != null ? "${model.tempat_lahir_sesuai_identitas}" : "";
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawInsert("INSERT INTO MS2_DEDUP "
          "(order_no,flag,no_identitas,nama,tanggal_lahir,tempat_lahir_sesuai_identitas,nama_gadis_ibu_kandung,alamat,created_date,created_by,updated_date,updated_by)"
          " VALUES("
          "'$orderNo',"
          "'${model.flag}',"
          "'${model.no_identitas}',"
          "'${model.nama}',"
          "'${model.tanggal_lahir}',"
          "'$_birthPlace',"
          "'$_motherName',"
          "'${model.alamat}',"
          "'${model.created_date}',"
          "'${model.created_by}',"
          "'${model.updated_date}',"
          "'${model.updated_by}')");
    }
  }

  //delete MS2_DEDUP
  Future<bool> deleteMS2Dedup() async{
    List _checkData = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawDelete("delete from MS2_DEDUP where order_no = '$orderNo'");
      _checkData = await db.rawQuery("SELECT * FROM MS2_DEDUP WHERE order_no = '$orderNo'");
    }
    return _checkData.isEmpty;
  }


  //select data dedup
  Future<List> selectDataInfoNasabahFromDataDedup() async{
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
    }
    return await db.rawQuery("SELECT * FROM MS2_DEDUP WHERE order_no = '$orderNo'");
  }

  //select data info nasabah
  Future<List> selectDataInfoNasabah() async{
    print('selectDataInfoNasabah');
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * FROM MS2_CUSTOMER_PERSONAL WHERE order_no = '$orderNo'");
      // db.close();
    }
    return _data;
  }

  //select data info keluarga
  Future<List> selectDataInfoKeluarga(String relationStatusId) async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      if(relationStatusId == "05"){
        _data = await db.rawQuery("SELECT * FROM MS2_CUST_FAMILY WHERE order_no = '$orderNo' AND relation_status = '$relationStatusId'");
      }
      else{
        _data = await db.rawQuery("SELECT * FROM MS2_CUST_FAMILY WHERE order_no = '$orderNo' AND relation_status != '05'");
      }
    }
    return _data;
  }

  //select data info nasabah NPWP
  Future<List> selectDataInfoNasabahNPWP() async{
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
    }
    return await db.rawQuery("SELECT * FROM MS2_CUSTOMER WHERE order_no = '$orderNo'");
  }

  //select data pendapatan
  Future<List> selectDataIncome() async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * FROM MS2_CUST_INCOME WHERE cust_income_id = '$orderNo'");
    }
    return _data;
  }

  Future<List> selectDataCollaOto() async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * FROM MS2_APPL_OBJT_COLL_OTO WHERE colla_oto_id = '$orderNo'");
    }
    return  _data;
  }

  Future<List> selectDataCollaProp() async{
    List _data = [];
    if(await setOrderNumber()){
      print(orderNo);
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * FROM MS2_APPL_OBJT_COLL_PROP WHERE colla_prop_id = '$orderNo'");
    }
    return  _data;
  }

  Future<List> selectDataNoPayung() async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT no_aplikasi_payung FROM MS2_NOMER WHERE order_nomer = '$orderNo'");
    }
    return _data;
  }

  Future<List> selectDataNoUnit() async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT no_unit FROM MS2_NOMER WHERE order_nomer = '$orderNo'");
    }
    return _data;
  }

  Future<void> insertDataNoPayung(String noPayung) async{
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      // SharedPreferences _preferences = await SharedPreferences.getInstance();
      // String orderNO = _preferences.getString("order_no");
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawInsert("INSERT INTO MS2_NOMER(order_nomer,no_aplikasi_payung) VALUES('$orderNo','$noPayung')");
    }
  }

  Future<void> updateDataNoUnit(String noUnit) async{
    print("cek data no unit dbHelper $noUnit");
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      // SharedPreferences _preferences = await SharedPreferences.getInstance();
      // String orderNO = _preferences.getString("order_no");
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawInsert("UPDATE MS2_NOMER SET no_unit = '$noUnit' WHERE order_nomer = '$orderNo'");
    }
  }

  Future<void> insertDataNoPayungNoUnit(String noPayung,String noUnit) async{
    print("cek data no unit dbHelper $noUnit");
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      // SharedPreferences _preferences = await SharedPreferences.getInstance();
      // String orderNO = _preferences.getString("order_no");
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawInsert("INSERT INTO MS2_NOMER(order_nomer,no_aplikasi_payung,no_unit) VALUES('$orderNo','$noPayung','$noUnit')");
    }
  }

  // Credit Limit
  void insertMS2LME(MS2LMEModel model) async {
    await setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawInsert(
      "INSERT INTO MS2_LME (order_no, oid, lme_id, appl_no, flag_eligible, "
        "disburse_type, product_id, product_id_desc, installment_amount, ph_amount,"
        " voucher_code, tenor, created_date, created_by, modified_date, modified_by, active,"
        " grading, jenis_penawaran, jenis_penawaran_desc, pencairan_ke, opsi_multidisburse,"
        " max_ph, max_tenor, no_referensi, max_installment, portfolio,"
        " plafond_mrp_risk, remaining_tenor, activation_date, cust_name, cust_address, cust_type, notes) "
          "VALUES("
          "'$orderNo',"
          "'${model.oid}',"
          "'${model.lme_id}',"
          "'${model.appl_no}',"
          "'${model.flag_eligible}',"
          "'${model.disburse_type}',"
          "'${model.product_id}',"
          "'${model.product_id_desc}',"
          "'${model.installment_amount}',"
          "'${model.ph_amount}',"
          "'${model.voucher_code}',"
          "'${model.tenor}',"
          "'${model.created_date}',"
          "'${model.created_by}',"
          "'${model.modified_date}',"
          "'${model.modified_by}',"
          "'1',"
          "'${model.grading}',"
          "'${model.jenis_penawaran}',"
          "'${model.jenis_penawaran_desc}',"
          "'${model.pencairan_ke}',"
          "'${model.opsi_multidisburse}',"
          "'${model.max_ph}',"
          "'${model.max_tenor}',"
          "'${model.no_referensi}',"
          "'${model.max_installment}',"
          "'${model.portfolio}',"
          "'${model.plafond_mrp_risk}',"
          "'${model.remaining_tenor}',"
          "'${model.activation_date}',"
          "'${model.cust_name}',"
          "'${model.cust_address}',"
          "'${model.cust_type}',"
          "'${model.notes}')"
    );
    // db.close();
  }

  // Credit Limit
  void insertMS2LMEDetail(MS2LMEDetailModel model) async {
    await setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawInsert(
        "INSERT INTO MS2_LME_DETAIL (order_no, ac_source_reff_id, ac_appl_objt_id, ac_flag_lme, created_date, created_by, "
            "modified_date, modified_by, active) "
            "VALUES("
            "'$orderNo',"
            "'${model.ac_source_reff_id}',"
            "'${model.ac_appl_objt_id}',"
            "'${model.ac_flag_lme}',"
            "'${model.created_date}',"
            "'${model.created_by}',"
            "'${model.modified_date}',"
            "'${model.modified_by}',"
            "'${model.active}')"
    );
    // db.close();
  }

  Future<List> selectMS2LME() async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * FROM MS2_LME WHERE order_no = '$orderNo'");
    }
    return _data;
  }


  Future<bool> deleteMS2LME() async{
    List _checkData = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawDelete("delete from MS2_LME where order_no = '$orderNo'");
      // await db.rawDelete("delete from MS2_LME_DETAIL where order_no = '$orderNo'");
      _checkData = await db.rawQuery("SELECT * FROM MS2_LME WHERE order_no = '$orderNo'");
    }
    return _checkData.isEmpty;
  }

  // MS2 ECM
  Future<void> insertMS2ECM(String fileHeaderID) async {
    await setOrderNumber();
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "Ad1MS2.db");
    db = await openDatabase(path);
    await db.rawInsert(
        "INSERT INTO MS2_ECM (order_no, fileHeaderID, created_date, created_by, active) "
            "VALUES("
            "'$orderNo',"
            "'$fileHeaderID',"
            "'$dateTime',"
            "'$createdBy',"
            "'1')"
    );
    // db.close();
  }

  Future<List> selectMS2ECM() async{
    List _data = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      _data = await db.rawQuery("SELECT * FROM MS2_ECM WHERE order_no = '$orderNo'");
    }
    return _data;
  }

  Future<bool> deleteMS2ECM() async{
    List _checkData = [];
    if(await setOrderNumber()){
      var databasesPath = await getDatabasesPath();
      var path = join(databasesPath, "Ad1MS2.db");
      db = await openDatabase(path);
      await db.rawDelete("delete from MS2_ECM where order_no = '$orderNo'");
      _checkData = await db.rawQuery("SELECT * FROM MS2_ECM WHERE order_no = '$orderNo'");
    }
    return _checkData.isEmpty;
  }

  // void insertMS2CreditLimit(MS2CreditLimitModel model) async{
  //   var databasesPath = await getDatabasesPath();
  //   var path = join(databasesPath, "Ad1MS2.db");
  //   db = await openDatabase(path);
  //   await db.rawInsert("INSERT INTO MS2_CREDIT_LIMIT(order_no, nama_konsumen, nomer_telepon, alamat_konsumen, jenis_nasabah, "
  //       "jenis_nasabah_desc, jenis_penawaran, jenis_penawaran_desc, no_referensi, grading, grading_desc, proses, notes, max_ph, "
  //       "max_installment, portofolio, tenor, opsi_multidisburse, pencarian_ke) "
  //       "VALUES("
  //       "'${model.order_no}',"
  //       "'${model.consumer_name}',"
  //       "'${model.phone_no}',"
  //       "'${model.consumer_address}',"
  //       "'${model.consumer_type}',"
  //       "'${model.consumer_type_desc}',"
  //       "'${model.offer_type}',"
  //       "'${model.offer_type_desc}',"
  //       "'${model.no_reference}',"
  //       "'${model.grading}',"
  //       "'${model.grading_desc}',"
  //       "'${model.process}',"
  //       "'${model.notes}',"
  //       "'${model.max_ph}',"
  //       "'${model.max_installment}',"
  //       "'${model.portofolio}',"
  //       "'${model.tenor}',"
  //       "'${model.opsi_multidisburse}',"
  //       "'${model.search_no}')"
  //   );
  // }

}
