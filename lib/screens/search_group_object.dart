import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/search_group_object_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchGroupObject extends StatefulWidget {
  final String flag;
  final String unitColla;

  const SearchGroupObject({this.flag, this.unitColla});
  @override
  _SearchGroupObjectState createState() => _SearchGroupObjectState();
}

class _SearchGroupObjectState extends State<SearchGroupObject> {

  @override
  void initState() {
    super.initState();
    Provider.of<SearchGroupObjectChangeNotifier>(context,listen: false).getGroupObject(context, widget.flag, widget.unitColla);
  }
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        primaryColor: Colors.black,
        accentColor: myPrimaryColor,
        fontFamily: "NunitoSans",
        primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<SearchGroupObjectChangeNotifier>(context,listen: false).scaffoldKey,
        appBar: AppBar(
          title: Consumer<SearchGroupObjectChangeNotifier>(
            builder: (context, searchModelObjectChangeNotifier, _) {
              return TextFormField(
                controller: searchModelObjectChangeNotifier.controllerSearch,
                style: TextStyle(color: Colors.black),
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (query) {
                  searchModelObjectChangeNotifier.searchGroupObject(query);
                },
                onChanged: (e) {
                  searchModelObjectChangeNotifier.changeAction(e);
                },
                textCapitalization: TextCapitalization.characters,
                cursorColor: Colors.black,
                decoration: new InputDecoration(
                  hintText: "Cari Grup Objek (minimal 3 karakter)",
                  hintStyle: TextStyle(color: Colors.black),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                ),
                autofocus: true,
              );
            },
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            Provider.of<SearchGroupObjectChangeNotifier>(context, listen: true)
                    .showClear
                ? IconButton(
                    icon: Icon(Icons.clear),
                    onPressed: () {
                      Provider.of<SearchGroupObjectChangeNotifier>(context,
                          listen: false).clearSearchTemp();
                      Provider.of<SearchGroupObjectChangeNotifier>(context,
                              listen: false)
                          .controllerSearch
                          .clear();
                      Provider.of<SearchGroupObjectChangeNotifier>(context,
                              listen: false)
                          .changeAction(
                              Provider.of<SearchGroupObjectChangeNotifier>(
                                      context,
                                      listen: false)
                                  .controllerSearch
                                  .text);
                    })
                : SizedBox(
                    width: 0.0,
                    height: 0.0,
                  )
          ],
        ),
        body: Consumer<SearchGroupObjectChangeNotifier>(
          builder: (context, searchGroupObject, _) {
            return searchGroupObject.loadData
                ?
            Center(child: CircularProgressIndicator())
                :
            searchGroupObject.listGroupObjectTemp.isNotEmpty
                ?
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount: searchGroupObject.listGroupObjectTemp.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(
                        context, searchGroupObject.listGroupObjectTemp[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchGroupObject.listGroupObjectTemp[index].KODE} - "
                              "${searchGroupObject.listGroupObjectTemp[index].DESKRIPSI} ",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            )
                :
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount: searchGroupObject.listGroupObject.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(
                        context, searchGroupObject.listGroupObject[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchGroupObject.listGroupObject[index].KODE} - "
                          "${searchGroupObject.listGroupObject[index].DESKRIPSI} ",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          },
        ),
      ),
    );
  }
}
