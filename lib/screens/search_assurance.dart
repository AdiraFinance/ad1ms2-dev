import 'package:ad1ms2_dev/shared/search_assurance_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../main.dart';

class SearchAssurance extends StatefulWidget {
    @override
    _SearchAssuranceState createState() => _SearchAssuranceState();
}

class _SearchAssuranceState extends State<SearchAssurance> {
    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                title: Consumer<SearchAssuranceChangeNotifier>(
                    builder: (context, searchAssuranceChangeNotifier, _) {
                        return TextFormField(
                            controller: searchAssuranceChangeNotifier.controllerSearch,
                            style: TextStyle(color: Colors.black),
                            textInputAction: TextInputAction.search,
                            onFieldSubmitted: (e) {
//            _getCustomer(e);
                            },
                            onChanged: (e) {
                                searchAssuranceChangeNotifier.changeAction(e);
                            },
                            cursorColor: Colors.black,
                            decoration: new InputDecoration(
                                hintText: "Cari Jaminan",
                                hintStyle: TextStyle(color: Colors.black),
                                enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(color: myPrimaryColor),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(color: myPrimaryColor),
                                ),
                            ),
                            autofocus: true,
                        );
                    },
                ),
                backgroundColor: myPrimaryColor,
                iconTheme: IconThemeData(color: Colors.black),
                actions: <Widget>[
                    Provider.of<SearchAssuranceChangeNotifier>(context, listen: true)
                        .showClear
                        ? IconButton(
                        icon: Icon(Icons.clear),
                        onPressed: () {
                            Provider.of<SearchAssuranceChangeNotifier>(context,
                                listen: false)
                                .controllerSearch
                                .clear();
                            Provider.of<SearchAssuranceChangeNotifier>(context,
                                listen: false)
                                .changeAction(Provider.of<SearchAssuranceChangeNotifier>(
                                context,
                                listen: false)
                                .controllerSearch
                                .text);
                        })
                        : SizedBox(
                        width: 0.0,
                        height: 0.0,
                    )
                ],
            ),
            body: Consumer<SearchAssuranceChangeNotifier>(
                builder: (context, searchAssuranceChangeNotifier, _) {
                    return ListView.separated(
                        padding: EdgeInsets.symmetric(
                            vertical: MediaQuery.of(context).size.height / 57,
                            horizontal: MediaQuery.of(context).size.width / 27),
                        itemCount: searchAssuranceChangeNotifier.listAssuranceModel.length,
                        itemBuilder: (listContext, index) {
                            return InkWell(
                                onTap: () {
                                    Navigator.pop(context,
                                        searchAssuranceChangeNotifier.listAssuranceModel[index]);
                                },
                                child: Container(
                                    child: Row(
                                        mainAxisSize: MainAxisSize.max,
                                        children: [
                                            Text(
                                                "${searchAssuranceChangeNotifier.listAssuranceModel[index].id} - "
                                                    "${searchAssuranceChangeNotifier.listAssuranceModel[index].text} ",
                                                style: TextStyle(fontSize: 16),
                                            )
                                        ],
                                    ),
                                ),
                            );
                        },
                        separatorBuilder: (context, index) {
                            return Divider();
                        },
                    );
                },
            ),
        );
    }
}
