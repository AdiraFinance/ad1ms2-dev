import 'package:ad1ms2_dev/shared/search_object_type_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../main.dart';

class SearchObjectType extends StatefulWidget {
  final int flagByBrandModelType;
  final String flag;
  final String kodeGroupObject;
  final String kodeObject;
  final String prodMatrix;

  const SearchObjectType({this.flagByBrandModelType, this.flag, this.kodeGroupObject, this.kodeObject, this.prodMatrix});
  @override
  _SearchObjectTypeState createState() => _SearchObjectTypeState();
}

class _SearchObjectTypeState extends State<SearchObjectType> {
  @override
  void initState() {
    super.initState();
    // Provider.of<SearchObjectTypeChangeNotifier>(context, listen: false)
    //     .setDataObjectType(widget.id);
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<SearchObjectTypeChangeNotifier>(context, listen: false).scaffoldKey,
        appBar: AppBar(
          title: Consumer<SearchObjectTypeChangeNotifier>(
            builder: (context, searchObjectTypeChangeNotifier, _) {
              return TextFormField(
                controller: searchObjectTypeChangeNotifier.controllerSearch,
                style: TextStyle(color: Colors.black),
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (query) {
                  searchObjectTypeChangeNotifier.searchObjectType(context, widget.flagByBrandModelType, widget.flag, query, widget.kodeGroupObject, widget.kodeObject, widget.prodMatrix);
                },
                onChanged: (e) {
                  searchObjectTypeChangeNotifier.changeAction(e);
                },
                textCapitalization: TextCapitalization.characters,
                cursorColor: Colors.black,
                decoration: new InputDecoration(
                  hintText: "Cari Jenis Objek (minimal 3 karakter)",
                  hintStyle: TextStyle(color: Colors.black),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                ),
                autofocus: true,
              );
            },
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            Provider.of<SearchObjectTypeChangeNotifier>(context, listen: true)
                    .showClear
                ? IconButton(
                    icon: Icon(Icons.clear),
                    onPressed: () {
                      Provider.of<SearchObjectTypeChangeNotifier>(context,
                              listen: false)
                          .controllerSearch
                          .clear();
                      Provider.of<SearchObjectTypeChangeNotifier>(context,
                              listen: false)
                          .changeAction(
                              Provider.of<SearchObjectTypeChangeNotifier>(context,
                                      listen: false)
                                  .controllerSearch
                                  .text);
                    })
                : SizedBox(
                    width: 0.0,
                    height: 0.0,
                  )
          ],
        ),
        body: Consumer<SearchObjectTypeChangeNotifier>(
          builder: (context, searchObjectTypeChangeNotifier, _) {
            return ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount: searchObjectTypeChangeNotifier.listBrandTypeModelGenreModel.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(context,
                        searchObjectTypeChangeNotifier.listBrandTypeModelGenreModel[index]);
                  },
                  child: Container(
                    child:  Column(
                      children: <Widget>[
                        SizedBox(
                          height: MediaQuery.of(context).size.height / 57,
                        ),
                        Row(
                          children: [
                            Expanded(
                                child: Text(
                                  "${searchObjectTypeChangeNotifier.listBrandTypeModelGenreModel[index].brandObjectModel.id} - "
                                      "${searchObjectTypeChangeNotifier.listBrandTypeModelGenreModel[index].brandObjectModel.name}",
                                  style: TextStyle(fontSize: 16),
                                ))
                          ],
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height / 57,
                        ),
                        Row(
                          children: [
                            Expanded(
                                child: Text(
                                  "${searchObjectTypeChangeNotifier.listBrandTypeModelGenreModel[index].objectTypeModel.id} - "
                                      "${searchObjectTypeChangeNotifier.listBrandTypeModelGenreModel[index].objectTypeModel.name}",
                                  style: TextStyle(fontSize: 16),
                                ))
                          ],
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height / 57,
                        ),
                        Row(
                          children: [
                            Expanded(
                                child: Text(
                                  "${searchObjectTypeChangeNotifier.listBrandTypeModelGenreModel[index].modelObjectModel.id} - "
                                      "${searchObjectTypeChangeNotifier.listBrandTypeModelGenreModel[index].modelObjectModel.name}",
                                  style: TextStyle(fontSize: 16),
                                ))
                          ],
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height / 57,
                        ),
                        // Row(
                        //   children: [
                        //     Expanded(
                        //         child: Text(
                        //           "${searchObjectTypeChangeNotifier.listBrandTypeModelGenreModel[index].objectUsageModel.id} - "
                        //               "${searchObjectTypeChangeNotifier.listBrandTypeModelGenreModel[index].objectUsageModel.name}",
                        //           style: TextStyle(fontSize: 16),
                        //         ))
                        //   ],
                        // ),
                        // SizedBox(
                        //   height: MediaQuery.of(context).size.height / 57,
                        // ),
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          },
        ),
      ),
    );
  }
}
