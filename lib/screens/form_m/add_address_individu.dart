import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_individu_change_notifier.dart';
import 'package:ad1ms2_dev/widgets/widget_address_individu.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class AddAddressIndividu extends StatefulWidget {
  final int flag;
  final int index;
  final AddressModel addressModel;
  final int typeAddress;

  const AddAddressIndividu({this.flag, this.index, this.addressModel, this.typeAddress});
  @override
  _AddAddressIndividuState createState() => _AddAddressIndividuState();
}

class _AddAddressIndividuState extends State<AddAddressIndividu> {
  Future<void> _setValueForEdit;

  @override
  void initState() {
    super.initState();
    if (widget.flag != 0) {
      _setValueForEdit = Provider.of<FormMAddAddressIndividuChangeNotifier>(
          context,
          listen: false)
          .addDataListJenisAlamat(widget.addressModel, context, widget.flag,
          widget.addressModel.isSameWithIdentity,widget.index, widget.typeAddress);
    } else {
      _setValueForEdit =  Provider.of<FormMAddAddressIndividuChangeNotifier>(context,
              listen: false)
          .addDataListJenisAlamat(null,context,widget.flag,null,null,widget.typeAddress);
    }
    Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setDataAddressIndividu(context);
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange),
      child:
      WidgetAddressIndividu(flag: widget.flag, addressModel: widget.addressModel,
        index: widget.index, editVal: _setValueForEdit, typeAddress: widget.typeAddress)
//      Scaffold(
//        appBar: AppBar(
//          title: Text(
//              widget.flag == 0
//                  ? "Add Alamat Korespondensi"
//                  : "Edit Alamat Korespondensi",
//              style: TextStyle(color: Colors.black)),
//          centerTitle: true,
//          backgroundColor: myPrimaryColor,
//          iconTheme: IconThemeData(color: Colors.black),
//        ),
//        body: widget.flag == 0
//                ? Consumer<FormMAddAlamatKorespondensiChangeNotif>(
//                    builder: (context, formMAddAlamatKorespon, _) {
//                      return SingleChildScrollView(
//                        padding: EdgeInsets.symmetric(
//                            horizontal: MediaQuery.of(context).size.width / 27,
//                            vertical: MediaQuery.of(context).size.height / 57),
//                        child: Form(
//                          key: formMAddAlamatKorespon.key,
//                          onWillPop: _onWillPop,
//                          child: Column(
//                            crossAxisAlignment: CrossAxisAlignment.start,
//                            children: [
//                              DropdownButtonFormField<JenisAlamatModel>(
//                                  autovalidate: formMAddAlamatKorespon.autoValidate,
//                                  validator: (e) {
//                                    if (e == null) {
//                                      return "Silahkan pilih jenis alamat";
//                                    } else {
//                                      return null;
//                                    }
//                                  },
//                                  value: formMAddAlamatKorespon.jenisAlamatSelected,
//                                  onChanged: (value) {
//                                    formMAddAlamatKorespon.jenisAlamatSelected = value;
//                                  },
//                                  decoration: InputDecoration(
//                                    labelText: "Jenis Alamat",
//                                    border: OutlineInputBorder(),
//                                    contentPadding:
//                                    EdgeInsets.symmetric(horizontal: 10),
//                                  ),
//                                  items: formMAddAlamatKorespon
//                                      .listJenisAlamat
//                                      .map((value) {
//                                    return DropdownMenuItem<JenisAlamatModel>(
//                                      value: value,
//                                      child: Text(
//                                        value.DESKRIPSI,
//                                        overflow: TextOverflow.ellipsis,
//                                      ),
//                                    );
//                                  }).toList()),
//                              formMAddAlamatKorespon.jenisAlamatSelected != null
//                                  ? formMAddAlamatKorespon.jenisAlamatSelected.KODE == "01"
//                                  ? Row(
//                                children: [
//                                  Checkbox(
//                                      value: formMAddAlamatKorespon.isSameWithIdentity,
//                                      onChanged: (value) {
//                                        formMAddAlamatKorespon.isSameWithIdentity = value;
//                                        formMAddAlamatKorespon.setValueIsSameWithIdentity(value, context, null);
//                                      },
//                                      activeColor: myPrimaryColor),
//                                  SizedBox(width: 8),
//                                  Text("Sama dengan identitas")
//                                ],
//                              )
//                                  : SizedBox(height: MediaQuery.of(context).size.height / 47)
//                                  : SizedBox(height: MediaQuery.of(context).size.height / 47),
//                              TextFormField(
//                                autovalidate:
//                                formMAddAlamatKorespon.autoValidate,
//                                validator: (e) {
//                                  if (e.isEmpty) {
//                                    return "Tidak boleh kosong";
//                                  } else {
//                                    return null;
//                                  }
//                                },
//                                controller:
//                                formMAddAlamatKorespon.controllerAlamat,
//                                style: TextStyle(color: Colors.black),
//                                decoration: InputDecoration(
//                                    labelText: 'Alamat',
//                                    labelStyle:
//                                    TextStyle(color: Colors.black),
//                                    border: OutlineInputBorder(
//                                        borderRadius:
//                                        BorderRadius.circular(8))),
//                                maxLines: 3,
//                                keyboardType: TextInputType.text,
//                                textCapitalization: TextCapitalization.characters,
//                              ),
//                              SizedBox(
//                                  height: MediaQuery.of(context).size.height /
//                                      47),
//                              Row(
//                                children: [
//                                  Expanded(
//                                    flex: 5,
//                                    child: TextFormField(
//                                      autovalidate:
//                                      formMAddAlamatKorespon.autoValidate,
//                                      validator: (e) {
//                                        if (e.isEmpty) {
//                                          return "Tidak boleh kosong";
//                                        } else {
//                                          return null;
//                                        }
//                                      },
//                                      inputFormatters: [
//                                        WhitelistingTextInputFormatter.digitsOnly,
//                                        // LengthLimitingTextInputFormatter(10),
//                                      ],
//                                      controller:
//                                      formMAddAlamatKorespon.controllerRT,
//                                      style: TextStyle(color: Colors.black),
//                                      decoration: InputDecoration(
//                                          labelText: 'RT',
//                                          labelStyle:
//                                          TextStyle(color: Colors.black),
//                                          border: OutlineInputBorder(
//                                              borderRadius:
//                                              BorderRadius.circular(8))),
//                                      keyboardType: TextInputType.number,
//                                    ),
//                                  ),
//                                  SizedBox(width: 8),
//                                  Expanded(
//                                    flex: 5,
//                                    child: TextFormField(
//                                      autovalidate:
//                                      formMAddAlamatKorespon.autoValidate,
//                                      validator: (e) {
//                                        if (e.isEmpty) {
//                                          return "Tidak boleh kosong";
//                                        } else {
//                                          return null;
//                                        }
//                                      },
//                                      inputFormatters: [
//                                        WhitelistingTextInputFormatter.digitsOnly,
//                                        // LengthLimitingTextInputFormatter(10),
//                                      ],
//                                      controller:
//                                      formMAddAlamatKorespon.controllerRW,
//                                      style: TextStyle(color: Colors.black),
//                                      decoration: InputDecoration(
//                                          labelText: 'RW',
//                                          labelStyle:
//                                          TextStyle(color: Colors.black),
//                                          border: OutlineInputBorder(
//                                              borderRadius:
//                                              BorderRadius.circular(8))),
//                                      keyboardType: TextInputType.number,
//                                    ),
//                                  )
//                                ],
//                              ),
//                              SizedBox(
//                                  height: MediaQuery.of(context).size.height /
//                                      47),
//                              FocusScope(
//                                node: FocusScopeNode(),
//                                child: TextFormField(
//                                  autovalidate:
//                                  formMAddAlamatKorespon.autoValidate,
//                                  validator: (e) {
//                                    if (e.isEmpty) {
//                                      return "Tidak boleh kosong";
//                                    } else {
//                                      return null;
//                                    }
//                                  },
//                                  onTap: () {
//                                    formMAddAlamatKorespon
//                                        .searchKelurahan(context);
//                                  },
//                                  controller: formMAddAlamatKorespon
//                                      .controllerKelurahan,
//                                  readOnly: true,
//                                  style: new TextStyle(color: Colors.black),
//                                  decoration: new InputDecoration(
//                                      labelText: 'Kelurahan',
//                                      labelStyle:
//                                      TextStyle(color: Colors.black),
//                                      border: OutlineInputBorder(
//                                          borderRadius:
//                                          BorderRadius.circular(8))),
//                                  keyboardType: TextInputType.text,
//                                  textCapitalization: TextCapitalization.characters,
//                                ),
//                              ),
//                              SizedBox(
//                                  height: MediaQuery.of(context).size.height /
//                                      47),
//                              TextFormField(
//                                autovalidate:
//                                formMAddAlamatKorespon.autoValidate,
//                                validator: (e) {
//                                  if (e.isEmpty) {
//                                    return "Tidak boleh kosong";
//                                  } else {
//                                    return null;
//                                  }
//                                },
//                                enabled: false,
//                                controller: formMAddAlamatKorespon
//                                    .controllerKecamatan,
//                                style: new TextStyle(color: Colors.black),
//                                decoration: new InputDecoration(
//                                    labelText: 'Kecamatan',
//                                    labelStyle:
//                                    TextStyle(color: Colors.black),
//                                    border: OutlineInputBorder(
//                                        borderRadius:
//                                        BorderRadius.circular(8))),
//                                keyboardType: TextInputType.text,
//                                textCapitalization: TextCapitalization.characters,
//                              ),
//                              SizedBox(
//                                  height: MediaQuery.of(context).size.height /
//                                      47),
//                              TextFormField(
//                                autovalidate:
//                                formMAddAlamatKorespon.autoValidate,
//                                validator: (e) {
//                                  if (e.isEmpty) {
//                                    return "Tidak boleh kosong";
//                                  } else {
//                                    return null;
//                                  }
//                                },
//                                enabled: false,
//                                controller:
//                                formMAddAlamatKorespon.controllerKota,
//                                style: new TextStyle(color: Colors.black),
//                                decoration: new InputDecoration(
//                                    labelText: 'Kabupaten/Kota',
//                                    labelStyle:
//                                    TextStyle(color: Colors.black),
//                                    border: OutlineInputBorder(
//                                        borderRadius:
//                                        BorderRadius.circular(8))),
//                                keyboardType: TextInputType.text,
//                                textCapitalization: TextCapitalization.characters,
//                              ),
//                              SizedBox(
//                                  height: MediaQuery.of(context).size.height /
//                                      47),
//                              Row(
//                                children: [
//                                  Expanded(
//                                    flex: 7,
//                                    child: TextFormField(
//                                      autovalidate:
//                                      formMAddAlamatKorespon.autoValidate,
//                                      validator: (e) {
//                                        if (e.isEmpty) {
//                                          return "Tidak boleh kosong";
//                                        } else {
//                                          return null;
//                                        }
//                                      },
//                                      enabled: false,
//                                      controller: formMAddAlamatKorespon
//                                          .controllerProvinsi,
//                                      style:
//                                      new TextStyle(color: Colors.black),
//                                      decoration: new InputDecoration(
//                                          labelText: 'Provinsi',
//                                          labelStyle:
//                                          TextStyle(color: Colors.black),
//                                          border: OutlineInputBorder(
//                                              borderRadius:
//                                              BorderRadius.circular(8))),
//                                      keyboardType: TextInputType.text,
//                                      textCapitalization: TextCapitalization.characters,
//                                    ),
//                                  ),
//                                  SizedBox(width: 8),
//                                  Expanded(
//                                    flex: 3,
//                                    child: TextFormField(
//                                      autovalidate:
//                                      formMAddAlamatKorespon.autoValidate,
//                                      validator: (e) {
//                                        if (e.isEmpty) {
//                                          return "Tidak boleh kosong";
//                                        } else {
//                                          return null;
//                                        }
//                                      },
//                                      enabled: false,
//                                      controller: formMAddAlamatKorespon
//                                          .controllerPostalCode,
//                                      style:
//                                      new TextStyle(color: Colors.black),
//                                      decoration: new InputDecoration(
//                                          labelText: 'Kode Pos',
//                                          labelStyle:
//                                          TextStyle(color: Colors.black),
//                                          border: OutlineInputBorder(
//                                              borderRadius:
//                                              BorderRadius.circular(8))),
//                                      keyboardType: TextInputType.number,
//                                    ),
//                                  ),
//                                ],
//                              ),
//                              SizedBox(
//                                  height: MediaQuery.of(context).size.height /
//                                      47),
//                              Row(
//                                children: [
//                                  Expanded(
//                                    flex: 4,
//                                    child: TextFormField(
//                                      autovalidate:
//                                      formMAddAlamatKorespon.autoValidate,
//                                      validator: (e) {
//                                        if (e.isEmpty) {
//                                          return "Tidak boleh kosong";
//                                        } else {
//                                          return null;
//                                        }
//                                      },
//                                      onChanged: (e) {
//                                        formMAddAlamatKorespon
//                                            .checkValidCOdeArea(e);
//                                      },
//                                      inputFormatters: [
//                                        WhitelistingTextInputFormatter
//                                            .digitsOnly,
//                                        LengthLimitingTextInputFormatter(10),
//                                      ],
//                                      controller: formMAddAlamatKorespon
//                                          .controllerKodeArea,
//                                      style:
//                                      new TextStyle(color: Colors.black),
//                                      decoration: new InputDecoration(
//                                          labelText: 'Kode Area',
//                                          labelStyle:
//                                          TextStyle(color: Colors.black),
//                                          border: OutlineInputBorder(
//                                              borderRadius:
//                                              BorderRadius.circular(8))),
//                                      keyboardType: TextInputType.number,
//                                    ),
//                                  ),
//                                  SizedBox(
//                                      width:
//                                      MediaQuery.of(context).size.width /
//                                          37),
//                                  Expanded(
//                                    flex: 6,
//                                    child: TextFormField(
//                                      autovalidate:
//                                      formMAddAlamatKorespon.autoValidate,
//                                      validator: (e) {
//                                        if (e.isEmpty) {
//                                          return "Tidak boleh kosong";
//                                        } else {
//                                          return null;
//                                        }
//                                      },
//                                      inputFormatters: [
//                                        WhitelistingTextInputFormatter
//                                            .digitsOnly,
//                                        LengthLimitingTextInputFormatter(10),
//                                      ],
//                                      controller: formMAddAlamatKorespon
//                                          .controllerTlpn,
//                                      style:
//                                      new TextStyle(color: Colors.black),
//                                      decoration: new InputDecoration(
//                                          labelText: 'Telepon',
//                                          labelStyle:
//                                          TextStyle(color: Colors.black),
//                                          border: OutlineInputBorder(
//                                              borderRadius:
//                                              BorderRadius.circular(8))),
//                                      keyboardType: TextInputType.number,
//                                    ),
//                                  ),
//                                ],
//                              ),
//                            ],
//                          ),
//                        ),
//                      );
//                    },
//                  )
//                : FutureBuilder(
//                    future: _setValueForEdit,
//                    builder: (context, snapshot) {
//                      if (snapshot.connectionState == ConnectionState.waiting) {
//                        return Center(
//                          child: CircularProgressIndicator(),
//                        );
//                      }
//                      return Consumer<FormMAddAlamatKorespondensiChangeNotif>(
//                        builder: (context, formMAddAlamatKorespon, _) {
//                          return SingleChildScrollView(
//                            padding: EdgeInsets.symmetric(
//                                horizontal: MediaQuery.of(context).size.width / 27,
//                                vertical: MediaQuery.of(context).size.height / 57),
//                            child: Form(
//                              key: formMAddAlamatKorespon.key,
//                              onWillPop: _onWillPop,
//                              child: Column(
//                                crossAxisAlignment: CrossAxisAlignment.start,
//                                children: [
//                                  DropdownButtonFormField<JenisAlamatModel>(
//                                      autovalidate: formMAddAlamatKorespon.autoValidate,
//                                      validator: (e) {
//                                        if (e == null) {
//                                          return "Silahkan pilih jenis alamat";
//                                        } else {
//                                          return null;
//                                        }
//                                      },
//                                      value: formMAddAlamatKorespon.jenisAlamatSelected,
//                                      onChanged: (value) {
//                                        formMAddAlamatKorespon.jenisAlamatSelected = value;
//                                      },
//                                      decoration: InputDecoration(
//                                        labelText: "Jenis Alamat",
//                                        border: OutlineInputBorder(),
//                                        contentPadding:
//                                        EdgeInsets.symmetric(horizontal: 10),
//                                      ),
//                                      items: formMAddAlamatKorespon
//                                          .listJenisAlamat
//                                          .map((value) {
//                                        return DropdownMenuItem<JenisAlamatModel>(
//                                          value: value,
//                                          child: Text(
//                                            value.DESKRIPSI,
//                                            overflow: TextOverflow.ellipsis,
//                                          ),
//                                        );
//                                      }).toList()),
//                                  formMAddAlamatKorespon.jenisAlamatSelected != null
//                                      ? formMAddAlamatKorespon.jenisAlamatSelected.KODE == "01"
//                                      ? Row(
//                                    children: [
//                                      Checkbox(
//                                          value: formMAddAlamatKorespon.isSameWithIdentity,
//                                          onChanged: (value) {
//                                            formMAddAlamatKorespon.isSameWithIdentity = value;
//                                            formMAddAlamatKorespon.setValueIsSameWithIdentity(value, context, null);
//                                          },
//                                          activeColor: myPrimaryColor),
//                                      SizedBox(width: 8),
//                                      Text("Sama dengan identitas")
//                                    ],
//                                  )
//                                      : SizedBox(height: MediaQuery.of(context).size.height / 47)
//                                      : SizedBox(height: MediaQuery.of(context).size.height / 47),
//                                  TextFormField(
//                                    autovalidate:
//                                    formMAddAlamatKorespon.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    controller:
//                                    formMAddAlamatKorespon.controllerAlamat,
//                                    style: TextStyle(color: Colors.black),
//                                    decoration: InputDecoration(
//                                        labelText: 'Alamat',
//                                        labelStyle:
//                                        TextStyle(color: Colors.black),
//                                        border: OutlineInputBorder(
//                                            borderRadius:
//                                            BorderRadius.circular(8))),
//                                    maxLines: 3,
//                                    keyboardType: TextInputType.text,
//                                    textCapitalization: TextCapitalization.characters,
//                                  ),
//                                  SizedBox(
//                                      height: MediaQuery.of(context).size.height /
//                                          47),
//                                  Row(
//                                    children: [
//                                      Expanded(
//                                        flex: 5,
//                                        child: TextFormField(
//                                          autovalidate:
//                                          formMAddAlamatKorespon.autoValidate,
//                                          validator: (e) {
//                                            if (e.isEmpty) {
//                                              return "Tidak boleh kosong";
//                                            } else {
//                                              return null;
//                                            }
//                                          },
//                                          inputFormatters: [
//                                            WhitelistingTextInputFormatter.digitsOnly,
//                                            // LengthLimitingTextInputFormatter(10),
//                                          ],
//                                          controller:
//                                          formMAddAlamatKorespon.controllerRT,
//                                          style: TextStyle(color: Colors.black),
//                                          decoration: InputDecoration(
//                                              labelText: 'RT',
//                                              labelStyle:
//                                              TextStyle(color: Colors.black),
//                                              border: OutlineInputBorder(
//                                                  borderRadius:
//                                                  BorderRadius.circular(8))),
//                                          keyboardType: TextInputType.number,
//                                        ),
//                                      ),
//                                      SizedBox(width: 8),
//                                      Expanded(
//                                        flex: 5,
//                                        child: TextFormField(
//                                          autovalidate:
//                                          formMAddAlamatKorespon.autoValidate,
//                                          validator: (e) {
//                                            if (e.isEmpty) {
//                                              return "Tidak boleh kosong";
//                                            } else {
//                                              return null;
//                                            }
//                                          },
//                                          inputFormatters: [
//                                            WhitelistingTextInputFormatter.digitsOnly,
//                                            // LengthLimitingTextInputFormatter(10),
//                                          ],
//                                          controller:
//                                          formMAddAlamatKorespon.controllerRW,
//                                          style: TextStyle(color: Colors.black),
//                                          decoration: InputDecoration(
//                                              labelText: 'RW',
//                                              labelStyle:
//                                              TextStyle(color: Colors.black),
//                                              border: OutlineInputBorder(
//                                                  borderRadius:
//                                                  BorderRadius.circular(8))),
//                                          keyboardType: TextInputType.number,
//                                        ),
//                                      )
//                                    ],
//                                  ),
//                                  SizedBox(
//                                      height: MediaQuery.of(context).size.height /
//                                          47),
//                                  FocusScope(
//                                    node: FocusScopeNode(),
//                                    child: TextFormField(
//                                      autovalidate:
//                                      formMAddAlamatKorespon.autoValidate,
//                                      validator: (e) {
//                                        if (e.isEmpty) {
//                                          return "Tidak boleh kosong";
//                                        } else {
//                                          return null;
//                                        }
//                                      },
//                                      onTap: () {
//                                        formMAddAlamatKorespon
//                                            .searchKelurahan(context);
//                                      },
//                                      controller: formMAddAlamatKorespon
//                                          .controllerKelurahan,
//                                      readOnly: true,
//                                      style: new TextStyle(color: Colors.black),
//                                      decoration: new InputDecoration(
//                                          labelText: 'Kelurahan',
//                                          labelStyle:
//                                          TextStyle(color: Colors.black),
//                                          border: OutlineInputBorder(
//                                              borderRadius:
//                                              BorderRadius.circular(8))),
//                                      keyboardType: TextInputType.text,
//                                      textCapitalization: TextCapitalization.characters,
//                                    ),
//                                  ),
//                                  SizedBox(
//                                      height: MediaQuery.of(context).size.height /
//                                          47),
//                                  TextFormField(
//                                    autovalidate:
//                                    formMAddAlamatKorespon.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    enabled: false,
//                                    controller: formMAddAlamatKorespon
//                                        .controllerKecamatan,
//                                    style: new TextStyle(color: Colors.black),
//                                    decoration: new InputDecoration(
//                                        labelText: 'Kecamatan',
//                                        labelStyle:
//                                        TextStyle(color: Colors.black),
//                                        border: OutlineInputBorder(
//                                            borderRadius:
//                                            BorderRadius.circular(8))),
//                                    keyboardType: TextInputType.text,
//                                    textCapitalization: TextCapitalization.characters,
//                                  ),
//                                  SizedBox(
//                                      height: MediaQuery.of(context).size.height /
//                                          47),
//                                  TextFormField(
//                                    autovalidate:
//                                    formMAddAlamatKorespon.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    enabled: false,
//                                    controller:
//                                    formMAddAlamatKorespon.controllerKota,
//                                    style: new TextStyle(color: Colors.black),
//                                    decoration: new InputDecoration(
//                                        labelText: 'Kabupaten/Kota',
//                                        labelStyle:
//                                        TextStyle(color: Colors.black),
//                                        border: OutlineInputBorder(
//                                            borderRadius:
//                                            BorderRadius.circular(8))),
//                                    keyboardType: TextInputType.text,
//                                    textCapitalization: TextCapitalization.characters,
//                                  ),
//                                  SizedBox(
//                                      height: MediaQuery.of(context).size.height /
//                                          47),
//                                  Row(
//                                    children: [
//                                      Expanded(
//                                        flex: 7,
//                                        child: TextFormField(
//                                          autovalidate:
//                                          formMAddAlamatKorespon.autoValidate,
//                                          validator: (e) {
//                                            if (e.isEmpty) {
//                                              return "Tidak boleh kosong";
//                                            } else {
//                                              return null;
//                                            }
//                                          },
//                                          enabled: false,
//                                          controller: formMAddAlamatKorespon
//                                              .controllerProvinsi,
//                                          style:
//                                          new TextStyle(color: Colors.black),
//                                          decoration: new InputDecoration(
//                                              labelText: 'Provinsi',
//                                              labelStyle:
//                                              TextStyle(color: Colors.black),
//                                              border: OutlineInputBorder(
//                                                  borderRadius:
//                                                  BorderRadius.circular(8))),
//                                          keyboardType: TextInputType.text,
//                                          textCapitalization: TextCapitalization.characters,
//                                        ),
//                                      ),
//                                      SizedBox(width: 8),
//                                      Expanded(
//                                        flex: 3,
//                                        child: TextFormField(
//                                          autovalidate:
//                                          formMAddAlamatKorespon.autoValidate,
//                                          validator: (e) {
//                                            if (e.isEmpty) {
//                                              return "Tidak boleh kosong";
//                                            } else {
//                                              return null;
//                                            }
//                                          },
//                                          enabled: false,
//                                          controller: formMAddAlamatKorespon
//                                              .controllerPostalCode,
//                                          style:
//                                          new TextStyle(color: Colors.black),
//                                          decoration: new InputDecoration(
//                                              labelText: 'Kode Pos',
//                                              labelStyle:
//                                              TextStyle(color: Colors.black),
//                                              border: OutlineInputBorder(
//                                                  borderRadius:
//                                                  BorderRadius.circular(8))),
//                                          keyboardType: TextInputType.number,
//                                        ),
//                                      ),
//                                    ],
//                                  ),
//                                  SizedBox(
//                                      height: MediaQuery.of(context).size.height /
//                                          47),
//                                  Row(
//                                    children: [
//                                      Expanded(
//                                        flex: 4,
//                                        child: TextFormField(
//                                          autovalidate:
//                                          formMAddAlamatKorespon.autoValidate,
//                                          validator: (e) {
//                                            if (e.isEmpty) {
//                                              return "Tidak boleh kosong";
//                                            } else {
//                                              return null;
//                                            }
//                                          },
//                                          onChanged: (e) {
//                                            formMAddAlamatKorespon
//                                                .checkValidCOdeArea(e);
//                                          },
//                                          inputFormatters: [
//                                            WhitelistingTextInputFormatter
//                                                .digitsOnly,
//                                            LengthLimitingTextInputFormatter(10),
//                                          ],
//                                          controller: formMAddAlamatKorespon
//                                              .controllerKodeArea,
//                                          style:
//                                          new TextStyle(color: Colors.black),
//                                          decoration: new InputDecoration(
//                                              labelText: 'Kode Area',
//                                              labelStyle:
//                                              TextStyle(color: Colors.black),
//                                              border: OutlineInputBorder(
//                                                  borderRadius:
//                                                  BorderRadius.circular(8))),
//                                          keyboardType: TextInputType.number,
//                                        ),
//                                      ),
//                                      SizedBox(
//                                          width:
//                                          MediaQuery.of(context).size.width /
//                                              37),
//                                      Expanded(
//                                        flex: 6,
//                                        child: TextFormField(
//                                          autovalidate:
//                                          formMAddAlamatKorespon.autoValidate,
//                                          validator: (e) {
//                                            if (e.isEmpty) {
//                                              return "Tidak boleh kosong";
//                                            } else {
//                                              return null;
//                                            }
//                                          },
//                                          inputFormatters: [
//                                            WhitelistingTextInputFormatter
//                                                .digitsOnly,
//                                            LengthLimitingTextInputFormatter(10),
//                                          ],
//                                          controller: formMAddAlamatKorespon
//                                              .controllerTlpn,
//                                          style:
//                                          new TextStyle(color: Colors.black),
//                                          decoration: new InputDecoration(
//                                              labelText: 'Telepon',
//                                              labelStyle:
//                                              TextStyle(color: Colors.black),
//                                              border: OutlineInputBorder(
//                                                  borderRadius:
//                                                  BorderRadius.circular(8))),
//                                          keyboardType: TextInputType.number,
//                                        ),
//                                      ),
//                                    ],
//                                  ),
//                                ],
//                              ),
//                            ),
//                          );
//                        },
//                      );
//                    }),
//        bottomNavigationBar: BottomAppBar(
//          elevation: 0.0,
//          child: Padding(
//              padding: const EdgeInsets.all(8.0),
//              child: RaisedButton(
//                  shape: RoundedRectangleBorder(
//                      borderRadius: new BorderRadius.circular(8.0)),
//                  color: myPrimaryColor,
//                  onPressed: () {
//                    if (widget.flag == 0) {
//                      Provider.of<FormMAddAlamatKorespondensiChangeNotif>(
//                              context,
//                              listen: false)
//                          .check(context, widget.flag, null);
//                    } else {
//                      Provider.of<FormMAddAlamatKorespondensiChangeNotif>(
//                              context,
//                              listen: false)
//                          .check(context, widget.flag, widget.index);
//                    }
//                  },
//                  child: Row(
//                    mainAxisAlignment: MainAxisAlignment.center,
//                    children: <Widget>[
//                      Text(widget.flag == 0 ? "SAVE" : "UPDATE",
//                          style: TextStyle(
//                              color: Colors.black,
//                              fontSize: 14,
//                              fontWeight: FontWeight.w500,
//                              letterSpacing: 1.25))
//                    ],
//                  ))),
//        ),
//      ),
    );
  }

//  Future<bool> _onWillPop() async {/*
//    var _provider = Provider.of<FormMAddAlamatKorespondensiChangeNotif>(context, listen: false);
//    if (widget.flag == 0) {
//      if (_provider.jenisAlamatSelected != null ||
//          _provider.controllerAlamat.text != "" ||
//          _provider.controllerRT.text != "" ||
//          _provider.controllerRW.text != "" ||
//          _provider.controllerKelurahan.text != "" ||
//          _provider.controllerKecamatan.text != "" ||
//          _provider.controllerKota.text != "" ||
//          _provider.controllerProvinsi.text != "" ||
//          _provider.controllerPostalCode.text != "" ||
//          _provider.controllerKodeArea.text != "" ||
//          _provider.controllerTlpn.text != "") {
//          return (await showDialog(
//            context: context,
//            builder: (myContext) => AlertDialog(
//              title: new Text('Warning'),
//              content: new Text('Keluar dengan simpan perubahan?'),
//              actions: <Widget>[
//                new FlatButton(
//                  onPressed: () {
//                    _provider.check(context, widget.flag, null);
//                    Navigator.pop(context);
//                  },
//                  child: new Text('Ya'),
//                ),
//                new FlatButton(
//                  onPressed: () => Navigator.of(context).pop(true),
//                  child: new Text('Tidak'),
//                ),
//              ],
//            ),
//          )) ?? false;
//      } else {
//        return true;
//      }
//    } else {
//      if (_provider.jenisAlamatSelectedTemp.KODE != _provider.jenisAlamatSelected.KODE ||
//          _provider.alamatTemp != _provider.controllerAlamat.text ||
//          _provider.rtTemp != _provider.controllerRT.text ||
//          _provider.rwTemp != _provider.controllerRW.text ||
//          _provider.kelurahanTemp != _provider.controllerKelurahan.text ||
//          _provider.kecamatanTemp != _provider.controllerKecamatan.text ||
//          _provider.kotaTemp != _provider.controllerKota.text ||
//          _provider.provinsiTemp != _provider.controllerProvinsi.text ||
//          _provider.postalCodeTemp != _provider.controllerPostalCode.text ||
//          _provider.areaCodeTemp != _provider.controllerKodeArea.text ||
//          _provider.phoneTemp != _provider.controllerTlpn.text) {
//          return (await showDialog(
//            context: context,
//            builder: (myContext) => AlertDialog(
//              title: new Text('Warning'),
//              content: new Text('Simpan perubahan?'),
//              actions: <Widget>[
//                new FlatButton(
//                  onPressed: () {
//                    _provider.check(context, widget.flag, widget.index);
//                    Navigator.pop(context);
//                  },
//                  child: new Text('Ya'),
//                ),
//                new FlatButton(
//                  onPressed: () => Navigator.of(context).pop(true),
//                  child: new Text('Tidak'),
//                ),
//              ],
//            ),
//          )) ?? false;
//      } else {
//        return true;
//      }
//    }
//  }
}
