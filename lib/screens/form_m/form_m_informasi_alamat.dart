import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/form_m/list_alamat_korespondensi.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_alamat_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class FormMInformasiAlamat extends StatefulWidget {
  @override
  _FormMInformasiAlamatState createState() => _FormMInformasiAlamatState();
}

class _FormMInformasiAlamatState extends State<FormMInformasiAlamat> {

  @override
  void initState() {
    super.initState();
    Provider.of<FormMInfoAlamatChangeNotif>(context,listen: false).setPreference();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<FormMInfoAlamatChangeNotif>(
      builder: (context, formMAlamatChangeNotif, _) {
        return Theme(
          data: ThemeData(
              primaryColor: Colors.black,
              accentColor: myPrimaryColor,
              fontFamily: "NunitoSans",
              primarySwatch: Colors.yellow),
          child: Scaffold(
            appBar: AppBar(
              title:
              Text("Informasi Alamat", style: TextStyle(color: Colors.black)),
              centerTitle: true,
              backgroundColor: myPrimaryColor,
              iconTheme: IconThemeData(color: Colors.black),
            ),
            body: Form(
              onWillPop: formMAlamatChangeNotif.onBackPress,
              key: formMAlamatChangeNotif.keyForm,
              child: ListView(
                padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height/57,
                    horizontal: MediaQuery.of(context).size.width/37
                ),
                children: [
                  Visibility(
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressTypeShow(1),
                    child: TextFormField(
                      autovalidate: formMAlamatChangeNotif.autoValidate,
                      validator: (e) {
                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressTypeMan(1)) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: formMAlamatChangeNotif.controllerAddressType,
                      style: new TextStyle(color: Colors.black),
                      decoration: new InputDecoration(
                        labelText: 'Jenis Alamat',
                        labelStyle: TextStyle(color: Colors.black),
                        filled: true,
                        fillColor: Colors.black12,
                        // errorStyle: TextStyle(
                        //   color: Theme.of(context).errorColor, // or any other color
                        // ),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: formMAlamatChangeNotif.isAddressTypeChanges ? Colors.purple : Colors.grey)),
                        disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: formMAlamatChangeNotif.isAddressTypeChanges ? Colors.purple : Colors.grey)),
                      ),
                      enabled: false,
                    ),
                  ),
                  Visibility(
                      visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressTypeShow(1),
                      child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                  ),
                  Visibility(
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressShow(1),
                    child: TextFormField(
                      autovalidate: formMAlamatChangeNotif.autoValidate,
                      validator: (e) {
                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressMan(1)) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      // onTap: () {
                      //   Navigator.push(
                      //       context,
                      //       MaterialPageRoute(
                      //           builder: (context) => ListAlamatKorespondensi()));
                      // },
                      controller: formMAlamatChangeNotif.controllerInfoAlamat,
                      style: new TextStyle(color: Colors.black),
                      decoration: new InputDecoration(
                          labelText: 'Alamat',
                          labelStyle: TextStyle(color: Colors.black),
                          filled: true,
                          fillColor: Colors.black12,
                          // errorStyle: TextStyle(
                          //   color: Theme.of(context).errorColor, // or any other color
                          // ),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: formMAlamatChangeNotif.isInfoAlamatChanges ? Colors.purple : Colors.grey)),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: formMAlamatChangeNotif.isInfoAlamatChanges ? Colors.purple : Colors.grey)),
                      ),
                      enabled: false,
                      // readOnly: true,
                    ),
                  ),
                  Visibility(
                      child: SizedBox(height: MediaQuery.of(context).size.height / 47),
                      visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressShow(1)
                  ),
                  Row(
                    children: [
                      Expanded(
                        flex: 5,
                        child: Visibility(
                          visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRTShow(1),
                          child: TextFormField(
                            autovalidate: formMAlamatChangeNotif.autoValidate,
                            validator: (e) {
                              if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRTMan(1)) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            controller: formMAlamatChangeNotif.controllerRT,
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                                filled: true,
                                fillColor: Colors.black12,
                                // errorStyle: TextStyle(
                                //   color: Theme.of(context).errorColor, // or any other color
                                // ),
                                labelText: 'RT',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: formMAlamatChangeNotif.isRTChanges ? Colors.purple : Colors.grey)),
                                disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: formMAlamatChangeNotif.isRTChanges ? Colors.purple : Colors.grey)),
                            ),
                            enabled: false,
                          ),
                        ),
                      ),
                      SizedBox(width: 8),
                      Expanded(
                          flex: 5,
                          child: Visibility(
                            visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRWShow(1),
                            child: TextFormField(
                              autovalidate: formMAlamatChangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRWMan(1)) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              controller: formMAlamatChangeNotif.controllerRW,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  filled: true,
                                  fillColor: Colors.black12,
                                  // errorStyle: TextStyle(
                                  //   color: Theme.of(context).errorColor, // or any other color
                                  // ),
                                  labelText: 'RW',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: formMAlamatChangeNotif.isRWChanges ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: formMAlamatChangeNotif.isRWChanges ? Colors.purple : Colors.grey)),
                              ),
                              enabled: false,
                            ),
                          ))
                    ],
                  ),
                  Visibility(
                    child: SizedBox(height: MediaQuery.of(context).size.height / 47),
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRWShow(1)
                        && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRTShow(1),
                  ),
                  Visibility(
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKelurahanShow(1),
                    child: TextFormField(
                      autovalidate: formMAlamatChangeNotif.autoValidate,
                      validator: (e) {
                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKelurahanMan(1)) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: formMAlamatChangeNotif.controllerKelurahan,
                      style: new TextStyle(color: Colors.black),
                      decoration: new InputDecoration(
                          filled: true,
                          fillColor: Colors.black12,
                          // errorStyle: TextStyle(
                          //   color: Theme.of(context).errorColor, // or any other color
                          // ),
                          labelText: 'Kelurahan',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: formMAlamatChangeNotif.isKelurahanChanges ? Colors.purple : Colors.grey)),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: formMAlamatChangeNotif.isKelurahanChanges ? Colors.purple : Colors.grey)),
                      ),
                      enabled: false,
                    ),
                  ),
                  Visibility(child: SizedBox(height: MediaQuery.of(context).size.height / 47),visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKelurahanShow(1),),
                  Visibility(
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKecamatanShow(1),
                    child: TextFormField(
                      autovalidate: formMAlamatChangeNotif.autoValidate,
                      validator: (e) {
                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKecamatanMan(1)) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: formMAlamatChangeNotif.controllerKecamatan,
                      style: new TextStyle(color: Colors.black),
                      decoration: new InputDecoration(
                          filled: true,
                          fillColor: Colors.black12,
                          // errorStyle: TextStyle(
                          //   color: Theme.of(context).errorColor, // or any other color
                          // ),
                          labelText: 'Kecamatan',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: formMAlamatChangeNotif.isKecamatanChanges ? Colors.purple : Colors.grey)),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: formMAlamatChangeNotif.isKecamatanChanges ? Colors.purple : Colors.grey)),
                      ),
                      enabled: false,
                    ),
                  ),
                  Visibility(
                      child: SizedBox(height: MediaQuery.of(context).size.height / 47),
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKecamatanShow(1),
                  ),
                  Visibility(
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKabKotShow(1),
                    child: TextFormField(
                      autovalidate: formMAlamatChangeNotif.autoValidate,
                      validator: (e) {
                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKabKotMan(1)) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: formMAlamatChangeNotif.controllerKota,
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.black12,
                          // errorStyle: TextStyle(
                          //   color: Theme.of(context).errorColor, // or any other color
                          // ),
                          labelText: 'Kabupaten/Kota',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: formMAlamatChangeNotif.isKotaChanges ? Colors.purple : Colors.grey)),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: formMAlamatChangeNotif.isKotaChanges ? Colors.purple : Colors.grey)),
                      ),
                      enabled: false,
                    ),
                  ),
                  Visibility(child: SizedBox(height: MediaQuery.of(context).size.height / 47),visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKabKotShow(1)),
                  Row(
                    children: [
                      Expanded(
                        flex: 7,
                        child:Visibility(
                            visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setProvinsiShow(1),
                            child:TextFormField(
                              autovalidate: formMAlamatChangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setProvinsiMan(1)) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              controller: formMAlamatChangeNotif.controllerProvinsi,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  filled: true,
                                  fillColor: Colors.black12,
                                  // errorStyle: TextStyle(
                                  //   color: Theme.of(context).errorColor, // or any other color
                                  // ),
                                  labelText: 'Provinsi',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: formMAlamatChangeNotif.isProvinsiChanges ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: formMAlamatChangeNotif.isProvinsiChanges ? Colors.purple : Colors.grey)),
                              ),
                              enabled: false,
                            ),
                        ),
                      ),
                      SizedBox(width: 8),
                      Expanded(
                        flex: 3,
                        child: Visibility(
                          visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKodePosShow(1),
                          child: TextFormField(
                            autovalidate: formMAlamatChangeNotif.autoValidate,
                            validator: (e) {
                              if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKodePosMan(1)) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            controller:
                            formMAlamatChangeNotif.controllerPostalCode,
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                                filled: true,
                                fillColor: Colors.black12,
                                // errorStyle: TextStyle(
                                //   color: Theme.of(context).errorColor, // or any other color
                                // ),
                                labelText: 'Kode Pos',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: formMAlamatChangeNotif.isPostalCodeChanges ? Colors.purple : Colors.grey)),
                                disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: formMAlamatChangeNotif.isPostalCodeChanges ? Colors.purple : Colors.grey)),
                            ),
                            enabled: false,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Visibility(
                    child: SizedBox(height: MediaQuery.of(context).size.height / 47),
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setProvinsiShow(1)
                        && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKodePosShow(1),
                  ),
                  Row(
                    children: [
                      Expanded(
                      flex: 4,
                        child: Visibility(
                          visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setTelp1AreaShow(1),
                          child: TextFormField(
                           autovalidate: formMAlamatChangeNotif.autoValidate,
                           validator: (e) {
                             if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setTelp1AreaMan(1)) {
                               return "Tidak boleh kosong";
                             } else {
                               return null;
                             }
                           },
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              WhitelistingTextInputFormatter.digitsOnly,
                              LengthLimitingTextInputFormatter(10),
                            ],
                            controller: formMAlamatChangeNotif.controllerTeleponArea,
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                                filled: true,
                                fillColor: Colors.black12,
                                // errorStyle: TextStyle(
                                //   color: Theme.of(context).errorColor, // or any other color
                                // ),
                                labelText: 'Telepon (Area)',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: formMAlamatChangeNotif.isTeleponAreaChanges ? Colors.purple : Colors.grey)),
                                disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: formMAlamatChangeNotif.isTeleponAreaChanges ? Colors.purple : Colors.grey)),
                            ),
                            enabled: false,
                          ),
                        ),
                      ),
                      SizedBox(width: MediaQuery.of(context).size.width / 37),
                      Expanded(
                        flex: 6,
                        child: Visibility(
                          visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setTelp1Show(1),
                          child: TextFormField(
                           autovalidate: formMAlamatChangeNotif.autoValidate,
                           validator: (e) {
                             if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setTelp1Man(1)) {
                               return "Tidak boleh kosong";
                             } else {
                               return null;
                             }
                           },
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              WhitelistingTextInputFormatter.digitsOnly,
                              LengthLimitingTextInputFormatter(10),
                            ],
                            controller: formMAlamatChangeNotif.controllerTelepon,
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                                filled: true,
                                fillColor: Colors.black12,
                                // errorStyle: TextStyle(
                                //   color: Theme.of(context).errorColor, // or any other color
                                // ),
                                labelText: 'Telepon',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: formMAlamatChangeNotif.isTeleponChanges ? Colors.purple : Colors.grey)),
                                disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: formMAlamatChangeNotif.isTeleponChanges ? Colors.purple : Colors.grey)),
                            ),
                            enabled: false,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            bottomNavigationBar: BottomAppBar(
              elevation: 0.0,
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Consumer<FormMInfoAlamatChangeNotif>(
                    builder: (context, formMAlamatChangeNotif, _) {
                      return Row(
                        children: [
                          Expanded(
                            child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: new BorderRadius.circular(8.0)),
                                color: myPrimaryColor,
                                onPressed: () {
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => ListAlamatKorespondensi()));
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text("EDIT ALAMAT",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500,
                                            letterSpacing: 1.25))
                                  ],
                                )
                            ),
                          ),
                          SizedBox(width: MediaQuery.of(context).size.width / 37),
                          Expanded(
                            child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: new BorderRadius.circular(8.0)),
                                color: myPrimaryColor,
                                onPressed: () {
                                  formMAlamatChangeNotif.check(context);
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text("DONE",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500,
                                            letterSpacing: 1.25))
                                  ],
                                )
                            ),
                          )
                        ],
                      );
                    },
                  )),
            ),
          ),
        );
      },
    );
  }
}
