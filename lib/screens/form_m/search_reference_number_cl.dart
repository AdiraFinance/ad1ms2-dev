import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/form_m_credit_limit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_reference_number_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_reference_number_cl_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchReferenceNumberCL extends StatefulWidget {
  final String jenisPenawaran;
  const SearchReferenceNumberCL({this.jenisPenawaran});

  @override
  _SearchReferenceNumberCLState createState() => _SearchReferenceNumberCLState();
}

class _SearchReferenceNumberCLState extends State<SearchReferenceNumberCL> {
  @override
  void initState() {
    super.initState();
    Provider.of<SearchReferenceNumberCLChangeNotifier>(context,listen: false).getReferenceNumberCL(context, widget.jenisPenawaran, "");
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: Provider.of<SearchReferenceNumberCLChangeNotifier>(context,listen: false).scaffoldKey,
      appBar: AppBar(
        title: Consumer<SearchReferenceNumberCLChangeNotifier>(
          builder: (context, searchReferenceNumberCLChangeNotifier, _) {
            return TextFormField(
              controller: searchReferenceNumberCLChangeNotifier.controllerSearch,
              style: TextStyle(color: Colors.black),
              textInputAction: TextInputAction.search,
              onFieldSubmitted: (e) {
                // searchReferenceNumberCLChangeNotifier.getToken(context, e);
                if(widget.jenisPenawaran == "001") {
                  searchReferenceNumberCLChangeNotifier.getReferenceNumberCL(context, widget.jenisPenawaran, e);
                } else if(widget.jenisPenawaran == "002") {
                  searchReferenceNumberCLChangeNotifier.getReferenceNumberCL(context, widget.jenisPenawaran, e);
                }
              },
              onChanged: (e) {
                searchReferenceNumberCLChangeNotifier.changeAction(e);
              },
              cursorColor: Colors.black,
              decoration: new InputDecoration(
                hintText: "Cari Nomor Referensi (minimal 3 karakter)",
                hintStyle: TextStyle(color: Colors.black),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: myPrimaryColor),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: myPrimaryColor),
                ),
              ),
              textCapitalization: TextCapitalization.characters,
              autofocus: true,
            );
          },
        ),
        backgroundColor: myPrimaryColor,
        iconTheme: IconThemeData(color: Colors.black),
        actions: <Widget>[
          Provider.of<SearchReferenceNumberCLChangeNotifier>(context, listen: true).showClear
              ? IconButton(
              icon: Icon(Icons.clear),
              onPressed: () {
                Provider.of<SearchReferenceNumberCLChangeNotifier>(context, listen: false).controllerSearch.clear();
                Provider.of<SearchReferenceNumberCLChangeNotifier>(context, listen: false)
                    .changeAction(Provider.of<SearchReferenceNumberCLChangeNotifier>(context, listen: false)
                    .controllerSearch.text);
              })
              : SizedBox(
            width: 0.0,
            height: 0.0,
          )
        ],
      ),
      body: Consumer<SearchReferenceNumberCLChangeNotifier>(
        builder: (context, searchReferenceNumberCLChangeNotifier, _) {
          if (searchReferenceNumberCLChangeNotifier.loadData == true) {
            return Center(
              child: CircularProgressIndicator(
                valueColor: new AlwaysStoppedAnimation<Color>(primaryOrange),
              ),
            );
          }
          else{
            return Provider.of<FormMCreditLimitChangeNotifier>(context, listen: false).typeOfferSelected.KODE == "001"
            ? ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount: searchReferenceNumberCLChangeNotifier.listVoucherCL.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(context,
                        searchReferenceNumberCLChangeNotifier.listVoucherCL[index]);
                  },
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: [
                            Expanded(
                                child: Text(
                                  "Voucher Code : ${searchReferenceNumberCLChangeNotifier.listVoucherCL[index].voucher_code}",
                                  style: TextStyle(fontSize: 16),
                                ))
                          ],
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height / 57,
                        ),
                        Row(
                          children: [
                            Expanded(
                                child: Text(
                                  "Contract No : ${searchReferenceNumberCLChangeNotifier.listVoucherCL[index].contract_no}",
                                  style: TextStyle(fontSize: 16),
                                ))
                          ],
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height / 57,
                        ),
                        Row(
                          children: [
                            Expanded(
                                child: Text(
                                  "Product : ${searchReferenceNumberCLChangeNotifier.listVoucherCL[index].product_type} - ${searchReferenceNumberCLChangeNotifier.listVoucherCL[index].product_type_desc}",
                                  style: TextStyle(fontSize: 16),
                                ))
                          ],
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height / 57,
                        ),
                        Row(
                          children: [
                            Expanded(
                                child: Text(
                                  "Plafond Installment : ${searchReferenceNumberCLChangeNotifier.formatCurrency.formatCurrency2(searchReferenceNumberCLChangeNotifier.listVoucherCL[index].plafond_installment)}",
                                  style: TextStyle(fontSize: 16),
                                ))
                          ],
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height / 57,
                        ),
                        Row(
                          children: [
                            Expanded(
                                child: Text(
                                  "Tenor : ${searchReferenceNumberCLChangeNotifier.listVoucherCL[index].tenor}",
                                  style: TextStyle(fontSize: 16),
                                ))
                          ],
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height / 57,
                        ),
                        // Text(
                        //   "Voucher Code: ${searchReferenceNumberCLChangeNotifier.listVoucherCL[index].voucher_code}",
                        //   style: TextStyle(fontSize: 16),
                        // ),
                        // Text(
                        //   "Contract No: ${searchReferenceNumberCLChangeNotifier.listVoucherCL[index].contract_no}",
                        //   style: TextStyle(fontSize: 16),
                        // ),
                        // Text(
                        //   "Plafond Installment: ${searchReferenceNumberCLChangeNotifier.listVoucherCL[index].plafond_installment}",
                        //   style: TextStyle(fontSize: 16),
                        // ),
                        // Text(
                        //   "Tenor: ${searchReferenceNumberCLChangeNotifier.listVoucherCL[index].tenor}",
                        //   style: TextStyle(fontSize: 16),
                        // )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            )
            : ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount: searchReferenceNumberCLChangeNotifier.listDisbursementCL.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(context,
                        searchReferenceNumberCLChangeNotifier.listDisbursementCL[index]);
                  },
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: [
                            Expanded(
                                child: Text(
                                  "Tenor : ${searchReferenceNumberCLChangeNotifier.listDisbursementCL[index].disbursement_no}",
                                  style: TextStyle(fontSize: 16),
                                ))
                          ],
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height / 57,
                        ),
                        Row(
                          children: [
                            Expanded(
                                child: Text(
                                  "Tenor : ${searchReferenceNumberCLChangeNotifier.listDisbursementCL[index].application_no}",
                                  style: TextStyle(fontSize: 16),
                                ))
                          ],
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height / 57,
                        ),
                        // Text(
                        //   "${searchReferenceNumberCLChangeNotifier.listDisbursementCL[index].voucher_code}",
                        //   style: TextStyle(fontSize: 16),
                        // ),
                        // Text(
                        //   "${searchReferenceNumberCLChangeNotifier.listDisbursementCL[index].contract_no}",
                        //   style: TextStyle(fontSize: 16),
                        // )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          }
        },
      ),
    );
  }
}
