import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/list_oid_model.dart';
import 'package:ad1ms2_dev/models/type_offer_model.dart';
import 'package:ad1ms2_dev/screens/home.dart';
import 'package:ad1ms2_dev/shared/form_m_credit_limit_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class FormMCreditLimit extends StatefulWidget {
  final String flag;
  final ListOidModel model;
  final String identityNumber;
  final String fullName;
  final DateTime initialDateBirthDate;
  final String birthPlace;
  final String motherName;
  final String identityAddress;
  const FormMCreditLimit({this.flag, this.model, this.identityNumber, this.fullName, this.initialDateBirthDate, this.birthPlace, this.motherName, this.identityAddress});

  @override
  _FormMCreditLimitState createState() => _FormMCreditLimitState();
}

class _FormMCreditLimitState extends State<FormMCreditLimit> {

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      Provider.of<FormMCreditLimitChangeNotifier>(context,listen: false).dialogStatusAORO(context);
    });
    Provider.of<FormMCreditLimitChangeNotifier>(context,listen: false).setValue(context, widget.model);
    Provider.of<FormMCreditLimitChangeNotifier>(context,listen: false).getCheckLimit(context);
    // Provider.of<FormMCreditLimitChangeNotifier>(context,listen: false).getBlacklistCustomer(context, widget.identityNumber, widget.fullName, widget.initialDateBirthDate, widget.birthPlace, widget.motherName, widget.identityAddress, widget.flag);
    // Provider.of<FormMCreditLimitChangeNotifier>(context,listen: false).getSaveDedupBuruk(context);
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        primaryColor: Colors.black,
        accentColor: myPrimaryColor,
      ),
      child: Consumer<FormMCreditLimitChangeNotifier>(
        builder: (context, formMCreditLimitChangeNotif, _) {
          return Scaffold(
            key: formMCreditLimitChangeNotif.scaffoldKey,
            appBar: AppBar(
              backgroundColor: myPrimaryColor,
              centerTitle: true,
              title: Text("Credit Limit", style: TextStyle(color: Colors.black)),
              iconTheme: IconThemeData(color: Colors.black),
            ),
            body: Form(
              onWillPop: formMCreditLimitChangeNotif.onBackPress,
              key: formMCreditLimitChangeNotif.keyForm,
              child: ListView(
                padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height/57,
                    horizontal: MediaQuery.of(context).size.width/37
                ),
                children: [
                  TextFormField(
                    // autovalidate: formMCreditLimitChangeNotif.autoValidate,
                    // validator: (e) {
                    //   if (e.isEmpty) {
                    //     return "Tidak boleh kosong";
                    //   } else {
                    //     return null;
                    //   }
                    // },
                    enabled: false,
                    controller: formMCreditLimitChangeNotif.controllerConsumerName,
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                        labelText: 'Nama Konsumen',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)),
                        filled: true,
                        fillColor: Colors.black12,),
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  TextFormField(
                    // autovalidate: formMCreditLimitChangeNotif.autoValidate,
                    // validator: (e) {
                    //   if (e.isEmpty) {
                    //     return "Tidak boleh kosong";
                    //   } else {
                    //     return null;
                    //   }
                    // },
                    enabled: false,
                    controller: formMCreditLimitChangeNotif.controllerTelephoneNumber,
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                        labelText: 'Nomer Telepon',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)),
                        filled: true,
                        fillColor: Colors.black12,),
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly,
                      // LengthLimitingTextInputFormatter(10),
                    ],
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  TextFormField(
                    // autovalidate: formMCreditLimitChangeNotif.autoValidate,
                    // validator: (e) {
                    //   if (e.isEmpty) {
                    //     return "Tidak boleh kosong";
                    //   } else {
                    //     return null;
                    //   }
                    // },
                    enabled: false,
                    controller: formMCreditLimitChangeNotif.controllerConsumerAddress,
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                        labelText: 'Alamat Konsumen',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)),
                        filled: true,
                        fillColor: Colors.black12,),
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  TextFormField(
                    // autovalidate: formMCreditLimitChangeNotif.autoValidate,
                    // validator: (e) {
                    //   if (e.isEmpty) {
                    //     return "Tidak boleh kosong";
                    //   } else {
                    //     return null;
                    //   }
                    // },
                    enabled: false,
                    controller: formMCreditLimitChangeNotif.controllerCustomerType,
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                        labelText: 'Jenis Nasabah',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)),
                        filled: true,
                        fillColor: Colors.black12,),
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  // TextFormField(
                  //   enabled: false,
                  //   controller: formMCreditLimitChangeNotif.controllerNotesAboutConsumer,
                  //   autovalidate: formMCreditLimitChangeNotif.autoValidate,
                  //   validator: (e) {
                  //     if (e.isEmpty) {
                  //       return "Tidak boleh kosong";
                  //     } else {
                  //       return null;
                  //     }
                  //   },
                  //   style: TextStyle(color: Colors.black),
                  //   decoration: InputDecoration(
                  //       labelText: 'Catatan Tentang Konsumen',
                  //       labelStyle: TextStyle(color: Colors.black),
                  //       border: OutlineInputBorder(
                  //           borderRadius: BorderRadius.circular(8))),
                  //   keyboardType: TextInputType.text,
                  //   textCapitalization: TextCapitalization.characters,
                  // ),
                  // SizedBox(height: MediaQuery.of(context).size.height / 47),
                  DropdownButtonFormField<TypeOfferModel>(
                      autovalidate: formMCreditLimitChangeNotif.autoValidate,
                      validator: (e) {
                        if (e == null) {
                          return "Silahkan pilih jenis penawaran";
                        } else {
                          return null;
                        }
                      },
                      value: formMCreditLimitChangeNotif.typeOfferSelected,
                      onChanged: (value) {
                        formMCreditLimitChangeNotif.typeOfferSelected = value;
                        // if(value.KODE == "001"){
                        //   // VOUCHER
                        //   // API inqVoucher
                        //   formMCreditLimitChangeNotif.getInqVoucher(context);
                        // }
                        // else
                          if(value.KODE == "002"){
                            // CREDIT LIMIT
                            // API checkMPLActivation
                            formMCreditLimitChangeNotif.getMplActivation(context);
                        }
                        else if(value.KODE == "003"){
                          // NON VOUCHER
                          // API checklimit
                          formMCreditLimitChangeNotif.getCheckLimit(context);
                        }
                      },
                      onTap: () {
                        FocusManager.instance.primaryFocus.unfocus();
                      },
                      decoration: InputDecoration(
                        labelText: "Jenis Penawaran",
                        border: OutlineInputBorder(),
                        contentPadding:
                        EdgeInsets.symmetric(horizontal: 10),
                      ),
                      items: formMCreditLimitChangeNotif.listTypeOffer.map((value) {
                        return DropdownMenuItem<TypeOfferModel>(
                          value: value,
                          child: Text(
                            value.DESCRIPTION,
                            overflow: TextOverflow.ellipsis,
                          ),
                        );
                      }).toList()
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  formMCreditLimitChangeNotif.typeOfferSelected != null
                    ? formMCreditLimitChangeNotif.typeOfferSelected.KODE != "003"
                      ? Container(
                          margin: EdgeInsets.only(
                            bottom: MediaQuery.of(context).size.height / 47
                          ),
                          child: TextFormField(
                              autovalidate: formMCreditLimitChangeNotif.autoValidate,
                              validator: (e) {
                                if (e == null || e == "") {
                                  return "Nomor Referensi tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              enabled: formMCreditLimitChangeNotif.typeOfferSelected != null ? formMCreditLimitChangeNotif.typeOfferSelected.KODE == "002" ? false : true : true,
                              controller: formMCreditLimitChangeNotif.controllerReferenceNumberCL,
                              onTap: () {
                                FocusManager.instance.primaryFocus.unfocus();
                                formMCreditLimitChangeNotif.searchReferenceNumberCreditLimit(context);
                              },
                              decoration: InputDecoration(
                                labelText: "Nomor Referensi",
                                filled: formMCreditLimitChangeNotif.typeOfferSelected != null ? formMCreditLimitChangeNotif.typeOfferSelected.KODE == "002" ? true : false : false,
                                fillColor: Colors.black12,
                                errorStyle: TextStyle(
                                  color: Theme.of(context).errorColor,
                                ),
                                border: OutlineInputBorder(),
                                contentPadding: EdgeInsets.symmetric(horizontal: 10),
                              ),
                              readOnly: true,
                          ),
                        )
                      : SizedBox(height: 0.0,width: 0.0)
                    : SizedBox(height: 0.0,width: 0.0),
                  TextFormField(
                    // autovalidate: formMCreditLimitChangeNotif.autoValidate,
                    // validator: (e) {
                    //   if (e.isEmpty) {
                    //     return "Tidak boleh kosong";
                    //   } else {
                    //     return null;
                    //   }
                    // },
                    enabled: false,
                    controller: formMCreditLimitChangeNotif.controllerGrading,
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                        labelText: 'Grading',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)),
                        filled: true,
                        fillColor: Colors.black12,),
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  Text("Apakah akan melanjutkan proses penginputan atau tidak?",
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        letterSpacing: 0.15),
                  ),
                  // SizedBox(height: _size.height / 47),
                  Row(
                    children: [
                      Row(
                        children: [
                          Radio(
                              activeColor: primaryOrange,
                              value: 1,
                              groupValue: formMCreditLimitChangeNotif.radioProceedProcess,
                              onChanged: (value) {
                                // formMCreditLimitChangeNotif.getInformation(context);
                                formMCreditLimitChangeNotif.radioProceedProcess = value;
                              }),
                          Text("Ya")
                        ],
                      ),
                      Row(
                        children: [
                          Radio(
                              activeColor: primaryOrange,
                              value: 0,
                              groupValue: formMCreditLimitChangeNotif.radioProceedProcess,
                              onChanged: (value) {
                                formMCreditLimitChangeNotif.radioProceedProcess = value;
                              }),
                          Text("Tidak")
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  formMCreditLimitChangeNotif.radioProceedProcess == 0
                  ? TextFormField(
                    // autovalidate: formMCreditLimitChangeNotif.autoValidate,
                    // validator: (e) {
                    //   if (e.isEmpty) {
                    //     return "Tidak boleh kosong";
                    //   } else {
                    //     return null;
                    //   }
                    // },
                    // enabled: false,
                    controller: formMCreditLimitChangeNotif.controllerNotes,
                    style: TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                        labelText: 'Notes',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))),
                    maxLines: 3,
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                  )
                  : Column(
                    children: [
                      TextFormField(
                        // autovalidate: formMCreditLimitChangeNotif.autoValidate,
                        // validator: (e) {
                        //   if (e.isEmpty) {
                        //     return "Tidak boleh kosong";
                        //   } else {
                        //     return null;
                        //   }
                        // },
                        controller: formMCreditLimitChangeNotif.controllerMaxPH,
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            labelText: 'Max PH',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            filled: true,
                            fillColor: Colors.black12,),
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                        enabled: false,
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        // autovalidate: formMCreditLimitChangeNotif.autoValidate,
                        // validator: (e) {
                        //   if (e.isEmpty) {
                        //     return "Tidak boleh kosong";
                        //   } else {
                        //     return null;
                        //   }
                        // },
                        controller: formMCreditLimitChangeNotif.controllerMaxInstallment,
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            labelText: 'Max Installment',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            filled: true,
                            fillColor: Colors.black12,),
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                        enabled: false,
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      // TextFormField(
                      //   controller: formMCreditLimitChangeNotif.controllerBiddingType,
                      //   autovalidate: formMCreditLimitChangeNotif.autoValidate,
                      //   validator: (e) {
                      //     if (e.isEmpty) {
                      //       return "Tidak boleh kosong";
                      //     } else {
                      //       return null;
                      //     }
                      //   },
                      //   style: TextStyle(color: Colors.black),
                      //   decoration: InputDecoration(
                      //       labelText: 'Jenis Penawaran',
                      //       labelStyle: TextStyle(color: Colors.black),
                      //       border: OutlineInputBorder(
                      //           borderRadius: BorderRadius.circular(8))),
                      //   keyboardType: TextInputType.text,
                      //   textCapitalization: TextCapitalization.characters,
                      //   enabled: false,
                      // ),
                      // SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        // autovalidate: formMCreditLimitChangeNotif.autoValidate,
                        // validator: (e) {
                        //   if (e.isEmpty) {
                        //     return "Tidak boleh kosong";
                        //   } else {
                        //     return null;
                        //   }
                        // },
                        controller: formMCreditLimitChangeNotif.controllerPortofolio,
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            labelText: 'Portofolio',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            filled: true,
                            fillColor: Colors.black12,),
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                        enabled: false,
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        // autovalidate: formMCreditLimitChangeNotif.autoValidate,
                        // validator: (e) {
                        //   if (e.isEmpty) {
                        //     return "Tidak boleh kosong";
                        //   } else {
                        //     return null;
                        //   }
                        // },
                        controller: formMCreditLimitChangeNotif.controllerTenor,
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            labelText: 'Tenor',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            filled: true,
                            fillColor: Colors.black12,),
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                        enabled: false,
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      // TextFormField(
                      //   controller: formMCreditLimitChangeNotif.controllerReference,
                      //   autovalidate: formMCreditLimitChangeNotif.autoValidate,
                      //   validator: (e) {
                      //     if (e.isEmpty) {
                      //       return "Tidak boleh kosong";
                      //     } else {
                      //       return null;
                      //     }
                      //   },
                      //   style: TextStyle(color: Colors.black),
                      //   decoration: InputDecoration(
                      //       labelText: 'No Reference',
                      //       labelStyle: TextStyle(color: Colors.black),
                      //       border: OutlineInputBorder(
                      //           borderRadius: BorderRadius.circular(8))),
                      //   keyboardType: TextInputType.text,
                      //   textCapitalization: TextCapitalization.characters,
                      //   enabled: false,
                      // ),
                      // SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        // autovalidate: formMCreditLimitChangeNotif.autoValidate,
                        // validator: (e) {
                        //   if (e.isEmpty) {
                        //     return "Tidak boleh kosong";
                        //   } else {
                        //     return null;
                        //   }
                        // },
                        controller: formMCreditLimitChangeNotif.controllerMultidisburseOption,
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            labelText: 'Opsi Multidisburse',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            filled: true,
                            fillColor: Colors.black12,),
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                        enabled: false,
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        // autovalidate: formMCreditLimitChangeNotif.autoValidate,
                        // validator: (e) {
                        //   if (e.isEmpty) {
                        //     return "Tidak boleh kosong";
                        //   } else {
                        //     return null;
                        //   }
                        // },
                        controller: formMCreditLimitChangeNotif.controllerDisbursementNo,
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            labelText: 'Pencairan ke',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            filled: true,
                            fillColor: Colors.black12,),
                        keyboardType: TextInputType.text,
                        textCapitalization: TextCapitalization.characters,
                        enabled: false,
                      )
                    ],
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                ],
              ),
            ),
            bottomNavigationBar: BottomAppBar(
              child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Row(
                    children: [
//                       Expanded(
//                         flex: 5,
//                         child: RaisedButton(
//                             shape: RoundedRectangleBorder(
//                                 borderRadius: BorderRadius.circular(8.0)),
//                             color: myPrimaryColor,
//                             onPressed: () {
//                               Navigator.push(
//                                   context,
//                                   MaterialPageRoute(builder: (context) => FormMParent(flag: widget.flag)));
// //                          if (_selectedIndex != -1) {
// //                            if (widget.flag != 0) {
// //                              Navigator.push(
// //                                  context,
// //                                  MaterialPageRoute(
// //                                      builder: (context) => FormMParent()));
// //                            } else {
// //                              Navigator.push(
// //                                  context,
// //                                  MaterialPageRoute(
// //                                      builder: (context) => FormMCompanyParent()));
// //                            }
// //                          }
//                         },
//                         child: Row(
//                           mainAxisAlignment: MainAxisAlignment.center,
//                           children: <Widget>[
//                             Text("OID BARU",
//                                 style: TextStyle(
//                                     color: Colors.black,
//                                     fontSize: 14,
//                                     fontWeight: FontWeight.w500,
//                                     letterSpacing: 1.25)
//                             )
//                           ],
//                         )),
//                       ),
//                       SizedBox(width: MediaQuery.of(context).size.width / 37),
                      Expanded(
                        child: formMCreditLimitChangeNotif.processSubmit
                            ?
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CircularProgressIndicator(),
                          ],
                        )
                            :
                        RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.0)),
                            color: myPrimaryColor,
                            onPressed: () {
                              formMCreditLimitChangeNotif.check(
                                context,
                                widget.identityNumber,
                                widget.fullName,
                                widget.initialDateBirthDate,
                                widget.birthPlace,
                                widget.motherName,
                                widget.identityAddress,
                                widget.flag,
                              );
                              // Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage()));
                              // Navigator.push(context, MaterialPageRoute(builder: (context) => FormMParent(flag: widget.flag)));
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text("SUBMIT",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        letterSpacing: 1.25)
                                )
                              ],
                            )
                        ),
                      ),
                    ],
                  )
              ),
            ),
          );
        },
      ),
    );
  }
}
