import 'package:ad1ms2_dev/models/form_m_foto_model.dart';
import 'package:ad1ms2_dev/screens/form_m/list_occupation_address.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_occupation_change_notif.dart';
import 'package:ad1ms2_dev/widgets/occupation_lainnya_widget.dart';
import 'package:ad1ms2_dev/widgets/occupation_wiraswasta_widget.dart';
import 'package:ad1ms2_dev/widgets/ocupation_profesional_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class FormMPekerjaan extends StatefulWidget {
  @override
  _FormMPekerjaanState createState() => _FormMPekerjaanState();
}

class _FormMPekerjaanState extends State<FormMPekerjaan> {
  Future<void> _getDataDropdown;

  @override
  void initState() {
    super.initState();
    Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setDataAddressIndividu(context);
    _getDataDropdown = Provider.of<FormMOccupationChangeNotif>(context, listen: false).setPreference(context);
    Provider.of<FormMOccupationChangeNotif>(context, listen: false).setDefaultLocation(context);
    if(Provider.of<FormMFotoChangeNotifier>(context, listen: false).occupationSelected.KODE != "05"
        || Provider.of<FormMFotoChangeNotifier>(context, listen: false).occupationSelected.KODE != "07"
        || Provider.of<FormMFotoChangeNotifier>(context, listen: false).occupationSelected.KODE!= "08"){
      Provider.of<FormMOccupationChangeNotif>(context, listen: false).getDefaultPEP(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    var _provider = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
    return FutureBuilder(
      future: _getDataDropdown,
      builder: (context, snapshot) {
        if(snapshot.connectionState == ConnectionState.waiting){
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        return Consumer<FormMOccupationChangeNotif>(
          builder: (context, formMPekerjaanChangeNotif, _) {
            return Theme(
              data: ThemeData(
                  primaryColor: Colors.black,
                  accentColor: myPrimaryColor,
                  fontFamily: "NunitoSans",
                  primarySwatch: Colors.yellow),
              child: ListView(
                padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width/37,
                  vertical: MediaQuery.of(context).size.height/57,
                ),
                children: [
                  // DropdownButtonFormField<OccupationModel>(
                  //   autovalidate: formMPekerjaanChangeNotif.autoValidate,
                  //   validator: (e) {
                  //     if (e == null) {
                  //       return "Tidak boleh kosong";
                  //     } else {
                  //       return null;
                  //     }
                  //   },
                  //   value: formMPekerjaanChangeNotif.occupationSelected,
                  //   onChanged: (value) {
                  //     formMPekerjaanChangeNotif.occupationSelected = value;
                  //     formMPekerjaanChangeNotif.clearOccupationSelected();
                  //     if(value.KODE != "05" || value.KODE != "07"){
                  //       formMPekerjaanChangeNotif.getDefaultPEP(context);
                  //     }
                  //   },
                  //   onTap: () {
                  //     FocusManager.instance.primaryFocus.unfocus();
                  //   },
                  //   decoration: InputDecoration(
                  //     labelText: "Pekerjaan",
                  //     border: OutlineInputBorder(),
                  //     contentPadding: EdgeInsets.symmetric(horizontal: 10),
                  //   ),
                  //   items: formMPekerjaanChangeNotif.listOccupation.map((value) {
                  //     return DropdownMenuItem<OccupationModel>(
                  //       value: value,
                  //       child: Text(
                  //         value.DESKRIPSI,
                  //         overflow: TextOverflow.ellipsis,
                  //       ),
                  //     );
                  //   }).toList(),
                  // ),
                  // SizedBox(height: MediaQuery.of(context).size.height / 47),
                  Visibility(
                    visible: Provider.of<FormMFotoChangeNotifier>(context,listen: false).isOccupationVisible(),
                    child: TextFormField(
                      enabled: false,
                      controller: formMPekerjaanChangeNotif.controllerOccupation,
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                          labelText: 'Pekerjaan',
                          labelStyle: TextStyle(color: Colors.black),
                          filled: true,
                          fillColor: Colors.black12,
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: formMPekerjaanChangeNotif.editOccupation ? Colors.purple : Colors.grey)),
                        disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: formMPekerjaanChangeNotif.editOccupation ? Colors.purple : Colors.grey)),
                      ),
                    ),
                  ),
                  Visibility(
                      visible: Provider.of<FormMFotoChangeNotifier>(context,listen: false).isOccupationVisible(),
                      child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                  ),
                  Visibility(
                    visible: Provider.of<FormMOccupationChangeNotif>(context,listen: false).isLamaBekerjaBerjalan(),
                    child: TextFormField(
                      autovalidate: formMPekerjaanChangeNotif.autoValidate,
                      validator: (e) {
                        if (e.isEmpty && Provider.of<FormMOccupationChangeNotif>(context,listen: false).isLamaBekerjaBerjalanMandatory()) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      inputFormatters: [
                        WhitelistingTextInputFormatter.digitsOnly,
                        // LengthLimitingTextInputFormatter(10),
                      ],
                      controller: formMPekerjaanChangeNotif.controllerEstablishedDate,
                      style: new TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                          labelText: "Lama Usaha(bln)",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: formMPekerjaanChangeNotif.editLamaBekerjaBerjalan ? Colors.purple : Colors.grey)),
                          disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: formMPekerjaanChangeNotif.editLamaBekerjaBerjalan ? Colors.purple : Colors.grey)),
                          contentPadding: EdgeInsets.symmetric(horizontal: 10)
                      ),
                      // InputDecoration(
                      //     labelText: 'Lama Usaha(bln)',
                      //     labelStyle: TextStyle(color: Colors.black),
                      //     border: OutlineInputBorder(
                      //         borderRadius: BorderRadius.circular(8))),
                      keyboardType: TextInputType.number,
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  Container(
                    margin: EdgeInsets.only(bottom: MediaQuery.of(context).size.height / 47),
                    child: _provider.occupationSelected.KODE == "05" || _provider.occupationSelected.KODE == "07"
                        ? OccupationWiraswastaWidget()
                        : _provider.occupationSelected.KODE == "08"
                        ? OccupationProfesionalWidget()
                        : OccupationLainnyaWidget(),
                  ),
                  Visibility(
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressShow(2),
                    child: TextFormField(
                      autovalidate: formMPekerjaanChangeNotif.autoValidate,
                      validator: (e) {
                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressMan(2)) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(
                                builder: (context) => ListOccupationAddress()));
                      },
                      controller: formMPekerjaanChangeNotif.controllerAddress,
                      style: new TextStyle(color: Colors.black),
                      decoration: new InputDecoration(
                          labelText: 'Alamat',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8))),
                      readOnly: true,
                    ),
                  ),
                  Visibility(
                      visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressShow(2),
                      child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                  Visibility(
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressTypeShow(2),
                    child: TextFormField(
                      autovalidate: formMPekerjaanChangeNotif.autoValidate,
                      validator: (e) {
                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressTypeMan(2)) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: formMPekerjaanChangeNotif.controllerAddressType,
                      style: new TextStyle(color: Colors.black),
                      decoration: new InputDecoration(
                          filled: true,
                          fillColor: Colors.black12,
                          labelText: 'Jenis Alamat',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8))),
                      enabled: false,
                    ),
                  ),
                  Visibility(
                      visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressTypeShow(2),
                      child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                  // Visibility(
                  //     visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressTypeShow(1),
                  //     child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                  // ),
                  Row(
                    children: [
                      Expanded(
                        flex: 5,
                        child: Visibility(
                          visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRTShow(1),
                          child: TextFormField(
                            autovalidate: formMPekerjaanChangeNotif.autoValidate,
                            validator: (e) {
                              if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRTMan(1)) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            controller: formMPekerjaanChangeNotif.controllerRT,
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                                filled: true,
                                fillColor: Colors.black12,
                                labelText: 'RT',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8))),
                            enabled: false,
                          ),
                        ),
                      ),
                      SizedBox(width: 8),
                      Expanded(
                          flex: 5,
                          child: Visibility(
                            visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRWShow(1),
                            child: TextFormField(
                              autovalidate: formMPekerjaanChangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRWMan(1)) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              controller: formMPekerjaanChangeNotif.controllerRW,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  filled: true,
                                  fillColor: Colors.black12,
                                  labelText: 'RW',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              enabled: false,
                            ),
                          ))
                    ],
                  ),
                  Visibility(
                    child: SizedBox(height: MediaQuery.of(context).size.height / 47),
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRWShow(1)
                        && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRTShow(1),
                  ),
                  Visibility(
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKelurahanShow(1),
                    child: TextFormField(
                      autovalidate: formMPekerjaanChangeNotif.autoValidate,
                      validator: (e) {
                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKelurahanMan(1)) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: formMPekerjaanChangeNotif.controllerKelurahan,
                      style: new TextStyle(color: Colors.black),
                      decoration: new InputDecoration(
                          filled: true,
                          fillColor: Colors.black12,
                          labelText: 'Kelurahan',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8))),
                      enabled: false,
                    ),
                  ),
                  Visibility(child: SizedBox(height: MediaQuery.of(context).size.height / 47),visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKelurahanShow(1),),
                  Visibility(
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKecamatanShow(1),
                    child: TextFormField(
                      autovalidate: formMPekerjaanChangeNotif.autoValidate,
                      validator: (e) {
                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKecamatanMan(1)) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: formMPekerjaanChangeNotif.controllerKecamatan,
                      style: new TextStyle(color: Colors.black),
                      decoration: new InputDecoration(
                          filled: true,
                          fillColor: Colors.black12,
                          labelText: 'Kecamatan',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8))),
                      enabled: false,
                    ),
                  ),
                  Visibility(
                    child: SizedBox(height: MediaQuery.of(context).size.height / 47),
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKecamatanShow(1),
                  ),
                  Visibility(
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKabKotShow(1),
                    child: TextFormField(
                      autovalidate: formMPekerjaanChangeNotif.autoValidate,
                      validator: (e) {
                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKabKotMan(1)) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: formMPekerjaanChangeNotif.controllerKota,
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.black12,
                          labelText: 'Kabupaten/Kota',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8))),
                      enabled: false,
                    ),
                  ),
                  Visibility(child: SizedBox(height: MediaQuery.of(context).size.height / 47),visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKabKotShow(1)),
                  Row(
                    children: [
                      Expanded(
                        flex: 7,
                        child:Visibility(
                          visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setProvinsiShow(1),
                          child:TextFormField(
                            autovalidate: formMPekerjaanChangeNotif.autoValidate,
                            validator: (e) {
                              if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setProvinsiMan(1)) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            controller: formMPekerjaanChangeNotif.controllerProvinsi,
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                                filled: true,
                                fillColor: Colors.black12,
                                labelText: 'Provinsi',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8))),
                            enabled: false,
                          ),
                        ),
                      ),
                      SizedBox(width: 8),
                      Expanded(
                        flex: 3,
                        child: Visibility(
                          visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKodePosShow(1),
                          child: TextFormField(
                            autovalidate: formMPekerjaanChangeNotif.autoValidate,
                            validator: (e) {
                              if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKodePosMan(1)) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            controller: formMPekerjaanChangeNotif.controllerPostalCode,
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                                filled: true,
                                fillColor: Colors.black12,
                                labelText: 'Kode Pos',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8))),
                            enabled: false,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Visibility(
                    child: SizedBox(height: MediaQuery.of(context).size.height / 47),
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setProvinsiShow(1)
                        && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKodePosShow(1),
                  ),
                  Row(
                    children: [
                      Expanded(
                        flex: 4,
                        child: Visibility(
                          visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setTelp1AreaShow(1),
                          child: TextFormField(
                            autovalidate: formMPekerjaanChangeNotif.autoValidate,
                            validator: (e) {
                              if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setTelp1AreaMan(1)) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              WhitelistingTextInputFormatter.digitsOnly,
                              LengthLimitingTextInputFormatter(10),
                            ],
                            controller: formMPekerjaanChangeNotif.controllerTelepon1Area,
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                                filled: true,
                                fillColor: Colors.black12,
                                labelText: 'Telepon (Area)',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8))),
                            enabled: false,
                          ),
                        ),
                      ),
                      SizedBox(width: MediaQuery.of(context).size.width / 37),
                      Expanded(
                        flex: 6,
                        child: Visibility(
                          visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setTelp1Show(1),
                          child: TextFormField(
                            autovalidate: formMPekerjaanChangeNotif.autoValidate,
                            validator: (e) {
                              if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setTelp1Man(1)) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              WhitelistingTextInputFormatter.digitsOnly,
                              LengthLimitingTextInputFormatter(10),
                            ],
                            controller: formMPekerjaanChangeNotif.controllerTelepon1,
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                                filled: true,
                                fillColor: Colors.black12,
                                labelText: 'Telepon',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8))),
                            enabled: false,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }
}
