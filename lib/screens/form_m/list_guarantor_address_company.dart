import 'package:ad1ms2_dev/screens/form_m/add_guarantor_address_company.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_company_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_add_guarantor_address_company_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_add_guarantor_company_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class ListGuarantorAddressCompany extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
        data: ThemeData(
            primaryColor: Colors.black,
            accentColor: myPrimaryColor,
            fontFamily: "NunitoSans"),
        child: Scaffold(
          appBar: AppBar(
            title: Text("List Alamat Penjamin Kelembagaan",
                style: TextStyle(color: Colors.black)),
            centerTitle: true,
            backgroundColor: myPrimaryColor,
            iconTheme: IconThemeData(color: Colors.black),
            actions: [
              // IconButton(
              //     icon: Icon(Icons.info_outline),
              //     onPressed: (){
              //       Provider.of<FormMAddGuarantorCompanyChangeNotifier>(context, listen: true).iconShowDialog(context);
              //     })
            ],
          ),
          bottomSheet: Container(
              padding: EdgeInsets.all(8),
              height: 100,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                      "∙ Pilih salah satu alamat untuk mengubah detail alamat",
                      style: TextStyle(fontWeight: FontWeight.w400)
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                            text: "∙ Klik tanda",
                            style: TextStyle(fontWeight: FontWeight.w400)
                        ),
                        WidgetSpan(
                          child: Icon(Icons.more_vert, color: Colors.grey),
                        ),
                        TextSpan(
                            text: "pada salah satu alamat untuk memilih sebagai Alamat Korespondensi atau menghapus alamat",
                            style: TextStyle(fontWeight: FontWeight.w400)
                        )
                      ],
                    ),
                  ),
                ],
              )
          ),
          body: Consumer<FormMAddGuarantorCompanyChangeNotifier>(
            builder: (context, formMAddGuarantorChangeNotifier, _) {
              return formMAddGuarantorChangeNotifier.listGuarantorAddress.isEmpty
              ? Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset("img/alamat_kosong.png", height: MediaQuery.of(context).size.height / 9,),
                    SizedBox(height: MediaQuery.of(context).size.height / 47,),
                    Text("Tambah Alamat", style: TextStyle(color: Colors.grey, fontSize: 16),)
                  ],
                ),
              )
              : ListView.builder(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height / 77,
                      horizontal: MediaQuery.of(context).size.width / 47),
                  itemBuilder: (context, index) {
                    return InkWell(
                      onLongPress: () {
                        formMAddGuarantorChangeNotifier.selectedIndex = index;
                        formMAddGuarantorChangeNotifier.setCorrespondenceAddress(formMAddGuarantorChangeNotifier.listGuarantorAddress[index], index);
                        Navigator.pop(context);
                      },
                      onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => AddGuarantorAddressCompany(
                                          flag: 1,
                                          index: index,
                                          addressModel: formMAddGuarantorChangeNotifier.listGuarantorAddress[index],
                                          typeAddress: 1),
                                  )
                          ).then((value) => formMAddGuarantorChangeNotifier.listGuarantorAddress[index].isCorrespondence ? formMAddGuarantorChangeNotifier.setCorrespondenceAddress(formMAddGuarantorChangeNotifier.listGuarantorAddress[index], index) : null );
                        // Navigator.push(
                        //     context,
                        //     MaterialPageRoute(
                        //         builder: (context) => ChangeNotifierProvider(
                        //             create: (context) =>
                        //                 FormMAddAddressCompanyChangeNotifier(),
                        //             child: AddGuarantorAddressCompany(
                        //                 flag: 1,
                        //                 index: index,
                        //                 addressModel: formMAddGuarantorChangeNotifier.listGuarantorAddress[index],
                        //                 typeAddress: 1),
                        //         )
                        //     )
                        // ).then((value) => formMAddGuarantorChangeNotifier.listGuarantorAddress[index].isCorrespondence ? formMAddGuarantorChangeNotifier.setCorrespondenceAddress(formMAddGuarantorChangeNotifier.listGuarantorAddress[index]) : null );
                      },
                      child: formMAddGuarantorChangeNotifier.listGuarantorAddress[index].active == 0 ?
                      Card(
                        shape: formMAddGuarantorChangeNotifier.selectedIndex ==
                                index
                            ? RoundedRectangleBorder(
                                side:
                                    BorderSide(color: primaryOrange, width: 2),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4)))
                            : null,
                        elevation: 3.3,
                        child: Padding(
                          padding: const EdgeInsets.all(13.0),
                            child: Stack(
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text("${formMAddGuarantorChangeNotifier.listGuarantorAddress[index].jenisAlamatModel.KODE} - ${formMAddGuarantorChangeNotifier.listGuarantorAddress[index].jenisAlamatModel.DESKRIPSI}",
                                        style: TextStyle(fontWeight: FontWeight.bold)
                                    ),
                                    SizedBox(
                                      height:
                                      MediaQuery.of(context).size.height /
                                          97,
                                    ),
                                    Text("${formMAddGuarantorChangeNotifier.listGuarantorAddress[index].address}",
                                      style: TextStyle(color: Colors.grey, fontSize: 13),
                                    ),
                                    Text("${formMAddGuarantorChangeNotifier.listGuarantorAddress[index].kelurahanModel.KEC_ID} - ${formMAddGuarantorChangeNotifier.listGuarantorAddress[index].kelurahanModel.KEC_NAME},",
                                      style: TextStyle(color: Colors.grey, fontSize: 13),
                                    ),
                                    Text("${formMAddGuarantorChangeNotifier.listGuarantorAddress[index].kelurahanModel.KABKOT_ID} - ${formMAddGuarantorChangeNotifier.listGuarantorAddress[index].kelurahanModel.KABKOT_NAME},",
                                      style: TextStyle(color: Colors.grey, fontSize: 13),
                                    ),
                                    Text("${formMAddGuarantorChangeNotifier.listGuarantorAddress[index].kelurahanModel.PROV_ID} - ${formMAddGuarantorChangeNotifier.listGuarantorAddress[index].kelurahanModel.PROV_NAME}",
                                      style: TextStyle(color: Colors.grey, fontSize: 13),
                                    ),
                                    Text("${formMAddGuarantorChangeNotifier.listGuarantorAddress[index].kelurahanModel.ZIPCODE}",
                                      style: TextStyle(color: Colors.grey, fontSize: 13),),
                                    SizedBox(
                                      height:
                                      MediaQuery.of(context).size.height /
                                          97,
                                    ),
                                    Text("${formMAddGuarantorChangeNotifier.listGuarantorAddress[index].phoneArea1} ${formMAddGuarantorChangeNotifier.listGuarantorAddress[index].phone1}",
                                      style: TextStyle(color: Colors.grey, fontSize: 13),)
                                  ],
                                ),
                                Align(
                                    alignment: Alignment.topRight,
                                    child: Column(
                                      children: [
                                        GestureDetector(
                                          // onTap: () {formMinfoAlamat.deleteAlamatKorespondensi(context, index);},
                                            onTap: () {formMAddGuarantorChangeNotifier.moreDialog(context, index);},
                                            child: Icon(Icons.more_vert, color: Colors.grey)
                                        ),
                                        SizedBox(height: 3),
                                        formMAddGuarantorChangeNotifier.listGuarantorAddress[index].isEditAddress ? Icon(Icons.edit, color: Colors.purple) : Text(""),
                                      ],
                                    )
                                  // formMAddGuarantorChangeNotifier.listGuarantorAddress[index].jenisAlamatModel.KODE != "03"
                                  //     ? formMAddGuarantorChangeNotifier.listGuarantorAddress[index].isSameWithIdentity
                                  //     ? SizedBox()
                                  //     : GestureDetector(
                                  //     onTap: () {formMAddGuarantorChangeNotifier.deleteListOccupationAddress(context, index);},
                                  //     child: Icon(Icons.delete, color: Colors.red)
                                )
                                // IconButton(icon: Icon(Icons.delete, color: Colors.red),
                                //     onPressed: () {
                                //       // formMinfoAlamat.deleteAlamatKorespondensi(index);
                                //     })
                                //     : SizedBox(),
                              ],
                            )
                          // child: Stack(
                          //   children: [
                          //     Column(
                          //       children: [
                          //         Row(
                          //           children: [
                          //             Expanded(
                          //                 flex: 4, child: Text("Jenis alamat")),
                          //             Text(" : "),
                          //             Expanded(
                          //                 flex: 6,
                          //                 child: Text(
                          //                     formMAddGuarantorChangeNotifier
                          //                         .listGuarantorAddress[index]
                          //                         .jenisAlamatModel
                          //                         .DESKRIPSI))
                          //           ],
                          //         ),
                          //         Row(
                          //           children: [
                          //             Expanded(flex: 4, child: Text("Alamat")),
                          //             Text(" : "),
                          //             Expanded(
                          //                 flex: 6,
                          //                 child: Text(
                          //                     "${formMAddGuarantorChangeNotifier.listGuarantorAddress[index].address}"))
                          //           ],
                          //         ),
                          //       ],
                          //     ),
                          //     Align(
                          //       alignment: Alignment.topRight,
                          //       child: formMAddGuarantorChangeNotifier.listGuarantorAddress[index].jenisAlamatModel.KODE == "01" || formMAddGuarantorChangeNotifier.listGuarantorAddress[index].jenisAlamatModel.KODE == "03"
                          //           ? SizedBox()
                          //           : IconButton(
                          //           icon: Icon(Icons.delete,
                          //               color: Colors.red),
                          //           onPressed: () {
                          //               formMAddGuarantorChangeNotifier
                          //                   .deleteListGuarantorAddress(
                          //                   index);
                          //           }),
                          //     )
                          //   ],
                          // ),
                        ),
                      )
                          :
                      SizedBox()
                    );
                  },
                  itemCount: formMAddGuarantorChangeNotifier.listGuarantorAddress.length);
            },
          ),
          floatingActionButton: FloatingActionButton(
              onPressed: () {
                  Provider.of<FormMAddAddressCompanyChangeNotifier>(context, listen: false).clearData();
                  Navigator.push(context,
                      MaterialPageRoute(
                          builder: (context) => AddGuarantorAddressCompany(
                                  flag: 0,
                                  index: null,
                                  addressModel: null,
                                  typeAddress: 1,
                              )
                          )
                  );
                // Navigator.push(
                //     context,
                //     MaterialPageRoute(
                //         builder: (context) => ChangeNotifierProvider(
                //             create: (context) =>
                //                 FormMAddAddressCompanyChangeNotifier(),
                //             child: AddGuarantorAddressCompany(
                //               flag: 0,
                //               index: null,
                //               addressModel: null,
                //               typeAddress: 1,
                //             )
                //         )
                //     )
                // );
                // .then((value) => Provider.of<FormMAddGuarantorCompanyChangeNotifier>(context, listen: false).isShowDialog(context));
              },
              child: Icon(Icons.add),
              backgroundColor: myPrimaryColor),
        ));
  }
}
