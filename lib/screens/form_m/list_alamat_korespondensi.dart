import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/form_m/add_address_individu.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_alamat_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ListAlamatKorespondensi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(fontFamily: "NunitoSans", accentColor: myPrimaryColor),
      child: Scaffold(
        appBar: AppBar(
          title: Text("List Alamat Korespondensi",
              style: TextStyle(color: Colors.black)),
          centerTitle: true,
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: [
            // IconButton(
            //     icon: Icon(Icons.info_outline),
            //     onPressed: (){
            //       Provider.of<FormMInfoAlamatChangeNotif>(context, listen: true).iconShowDialog(context);
            //     })
          ],
        ),
        bottomSheet: Container(
          padding: EdgeInsets.all(16),
          height: 100,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                  "∙ Pilih salah satu alamat untuk mengubah detail alamat",
                  style: TextStyle(fontWeight: FontWeight.w400)
              ),
              Text.rich(
                TextSpan(
                  children: [
                    TextSpan(
                        text: "∙ Klik tanda",
                        style: TextStyle(fontWeight: FontWeight.w400)
                    ),
                    WidgetSpan(
                      child: Icon(Icons.more_vert, color: Colors.grey),
                    ),
                    TextSpan(
                        text: "pada salah satu alamat untuk memilih sebagai Alamat Korespondensi atau menghapus alamat",
                        style: TextStyle(fontWeight: FontWeight.w400)
                    )
                  ],
                ),
              ),
            ],
          )
        ),
        body: Consumer<FormMInfoAlamatChangeNotif>(
          builder: (context, formMinfoAlamat, _) {
            return formMinfoAlamat.listAlamatKorespondensi.isEmpty
            ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset("img/alamat_kosong.png", height: MediaQuery.of(context).size.height / 9,),
                  SizedBox(height: MediaQuery.of(context).size.height / 47,),
                  Text("Tambah Alamat", style: TextStyle(color: Colors.grey, fontSize: 16),)
                ],
              ),
            )
            : ListView.builder(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 77,
                  horizontal: MediaQuery.of(context).size.width / 47),
              itemBuilder: (context, index) {
                return InkWell(
                  // onLongPress: () {
                  //   formMinfoAlamat.selectedIndex = index;
                  //   formMinfoAlamat.controllerInfoAlamat.clear();
                  //   formMinfoAlamat.setAddress(formMinfoAlamat.listAlamatKorespondensi[index]);
                  //   formMinfoAlamat.setCorrespondence(index);
                  //   Navigator.pop(context);
                  // },
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => AddAddressIndividu(
                                  flag: 1,
                                  index: index,
                                  addressModel: formMinfoAlamat.listAlamatKorespondensi[index],
                                  typeAddress: 1
                              ),
                        )
                    ).then((value) => formMinfoAlamat.listAlamatKorespondensi[index].isCorrespondence ? formMinfoAlamat.setAddress(formMinfoAlamat.listAlamatKorespondensi[index]) : null );
                    /*Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ChangeNotifierProvider(
                          create: (context) => FormMAddAddressIndividuChangeNotifier(),
                          child: AddAddressIndividu(
                              flag: 1,
                              index: index,
                              addressModel: formMinfoAlamat.listAlamatKorespondensi[index],
                              typeAddress: 1
                          ),
                        )
                      )
                    ).then((value) => formMinfoAlamat.listAlamatKorespondensi[index].isCorrespondence ? formMinfoAlamat.setAddress(formMinfoAlamat.listAlamatKorespondensi[index]) : null );*/
                  },
                  child: formMinfoAlamat.listAlamatKorespondensi[index].active == 0 ?
                  Card(
                    margin: EdgeInsets.fromLTRB(4, 4, 4, 12),
                    shape: formMinfoAlamat.selectedIndex == index
                        ? RoundedRectangleBorder(
                            side: BorderSide(color: primaryOrange, width: 2),
                            borderRadius: BorderRadius.all(Radius.circular(4)))
                        : null,
                    elevation: formMinfoAlamat.isAlamatCardChanges ? 5.0 : 3.3,
                    child: Padding(
                      padding: const EdgeInsets.all(13.0),
                        child: Stack(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("${formMinfoAlamat.listAlamatKorespondensi[index].jenisAlamatModel.KODE} - ${formMinfoAlamat.listAlamatKorespondensi[index].jenisAlamatModel.DESKRIPSI}",
                                  style: TextStyle(fontWeight: FontWeight.bold)
                                ),
                                SizedBox(
                                  height:
                                  MediaQuery.of(context).size.height /
                                      97,
                                ),
                                Text("${formMinfoAlamat.listAlamatKorespondensi[index].address}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),
                                ),
                                Text("${formMinfoAlamat.listAlamatKorespondensi[index].kelurahanModel.KEC_ID} - ${formMinfoAlamat.listAlamatKorespondensi[index].kelurahanModel.KEC_NAME},",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),
                                ),
                                Text("${formMinfoAlamat.listAlamatKorespondensi[index].kelurahanModel.KABKOT_ID} - ${formMinfoAlamat.listAlamatKorespondensi[index].kelurahanModel.KABKOT_NAME},",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),
                                ),
                                Text("${formMinfoAlamat.listAlamatKorespondensi[index].kelurahanModel.PROV_ID} - ${formMinfoAlamat.listAlamatKorespondensi[index].kelurahanModel.PROV_NAME}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),
                                ),
                                Text("${formMinfoAlamat.listAlamatKorespondensi[index].kelurahanModel.ZIPCODE}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),),
                                SizedBox(height: MediaQuery.of(context).size.height / 97,),
                                Text("${formMinfoAlamat.listAlamatKorespondensi[index].areaCode} ${formMinfoAlamat.listAlamatKorespondensi[index].phone}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),),
                              ],
                            ),
                            Align(
                              alignment: Alignment.topRight,
                              child:
                              // formMinfoAlamat.listAlamatKorespondensi[index].jenisAlamatModel.KODE != "03"
                              //     ? formMinfoAlamat.listAlamatKorespondensi[index].isSameWithIdentity
                              //     ? SizedBox()
                              //     :
                              Column(
                                children: [
                                  GestureDetector(
                                      // onTap: () {formMinfoAlamat.deleteAlamatKorespondensi(context, index);},
                                      onTap: () {formMinfoAlamat.moreDialog(context, index);},
                                      child: Icon(Icons.more_vert, color: Colors.grey)
                                  ),
                                  SizedBox(height: 3),
                                  formMinfoAlamat.listAlamatKorespondensi[index].isEditAddress ? Icon(Icons.edit, color: Colors.purple) : Text(""),
                                ],
                              )
                              // IconButton(icon: Icon(Icons.delete, color: Colors.red),
                              //     onPressed: () {
                              //       // formMinfoAlamat.deleteAlamatKorespondensi(index);
                              //     })
                              //     : SizedBox(),
                            )
                          ],
                        )
                    ),
                  )
                      :
                  SizedBox()
                );
              },
              itemCount: formMinfoAlamat.listAlamatKorespondensi.length,
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).clearData();
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => AddAddressIndividu(
                          flag: 0,
                          index: null,
                          addressModel: null,
                          typeAddress: 1,
                        )
                    )
            );
            /*Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ChangeNotifierProvider(
                  create: (context) => FormMAddAddressIndividuChangeNotifier(),
                  child: AddAddressIndividu(
                    flag: 0,
                    index: null,
                    addressModel: null,
                    typeAddress: 1,
                  )
                )
              )
            );*/
            // .then((value) => Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false).isShowDialog(context))
          },
          backgroundColor: myPrimaryColor,
          child: Icon(Icons.add, color: Colors.black),
        ),
      ),
    );
  }
}
