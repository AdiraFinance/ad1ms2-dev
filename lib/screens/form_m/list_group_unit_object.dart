import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/form_m/add_group_unit_object.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_add_group_unit_object_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_individu_change_notifier/form_m_parent_individu_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ListGroupUnitObjectProvider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          fontFamily: "NunitoSans",
          accentColor: myPrimaryColor,
      ),
      child: Scaffold(
        key: Provider.of<FormMFotoChangeNotifier>(context,listen: false).scaffoldKeyListGroupObjecUnit,
        appBar: AppBar(
          title: Text("List Group Unit Object",
              style: TextStyle(color: Colors.black)),
          centerTitle: true,
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          // percobaan insert ECM
          // actions: [
          //   IconButton(
          //       icon: Icon(Icons.save),
          //       onPressed: (){
          //         Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).submitFotoToECM(context);
          //       }
          //   )
          // ],
        ),
        body: Consumer<FormMFotoChangeNotifier>(
          builder: (context, formMFotoChangeNotif, _) {
            return formMFotoChangeNotif.listGroupUnitObject.isEmpty
            ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // Image.asset("img/alamat_kosong.png", height: MediaQuery.of(context).size.height / 9,),
                  // SizedBox(height: MediaQuery.of(context).size.height / 47,),
                  Text("Tambah Unit", style: TextStyle(color: Colors.grey, fontSize: 16),)
                ],
              ),
            )
            : ListView.builder(
                padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height / 47,
                    horizontal: MediaQuery.of(context).size.width / 27),
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ChangeNotifierProvider(
                                  create: (context) =>
                                      FormMFotoAddGroupUnitObject(),
                                  child: AddGroUnitObjectProvider(
                                      1,
                                      formMFotoChangeNotif.listGroupUnitObject[index],
                                      index))));
                    },
                    child: Container(
                      child: Card(
                        elevation: 3.3,
                        child: Padding(
                          padding: const EdgeInsets.all(13.0),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                flex: 20,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: Text("Group Object"),
                                          flex: 4,
                                        ),
                                        Expanded(child: Text("  :  "), flex: 0),
                                        Expanded(
                                          child: Text(
                                              "${formMFotoChangeNotif.listGroupUnitObject[index].groupObjectUnit.groupObject}"),
                                          flex: 6,
                                        )
                                      ],
                                    ),
                                    SizedBox(
                                        height:
                                            MediaQuery.of(context).size.height /
                                                97),
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                            child: Text("Unit Object"),
                                            flex: 4),
                                        Expanded(child: Text("  :  "), flex: 0),
                                        Expanded(
                                            child: Text(
                                                "${formMFotoChangeNotif.listGroupUnitObject[index].objectUnit.objectUnit}"),
                                            flex: 6),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                  flex: 3,
                                  child: formMFotoChangeNotif.listGroupUnitObject[index].isDeleted ?
                                  SizedBox()
                                      :
                                  IconButton(
                                      icon: Icon(Icons.delete, color: Colors.red),
                                      onPressed: () {
                                        formMFotoChangeNotif.removeListGroupUnitObject(context, index);
                                      }
                                  )
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                },
                itemCount: formMFotoChangeNotif.listGroupUnitObject.length);
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            if(Provider.of<FormMFotoChangeNotifier>(context,listen: false).isLimitAddGroupUnitObject()){
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ChangeNotifierProvider(
                          create: (context) => FormMFotoAddGroupUnitObject(),
                          child: AddGroUnitObjectProvider(0, null, null))));
            }
            else{
              Provider.of<FormMFotoChangeNotifier>(context,listen: false).showSnackBarListGroupUnitObject("Tidak dapat menambah group unit object lagi");
            }
          },
          child: Icon(Icons.add, color: Colors.black),
          backgroundColor: myPrimaryColor,
        ),
      ),
    );
  }
}
