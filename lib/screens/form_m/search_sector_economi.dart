import 'package:ad1ms2_dev/shared/search_sector_economic_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class SearchSectorEconomic extends StatefulWidget {
  @override
  _SearchSectorEconomicState createState() => _SearchSectorEconomicState();
}

class _SearchSectorEconomicState extends State<SearchSectorEconomic> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Provider.of<SearchSectorEconomicChangeNotif>(context, listen: false).getSectorEconomic();
  }
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          fontFamily: "NunitoSans",
          accentColor: myPrimaryColor,
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<SearchSectorEconomicChangeNotif>(context, listen: false).scaffoldKey,
          appBar: AppBar(
            title: Consumer<SearchSectorEconomicChangeNotif>(
              builder: (context, searchSectorEconomic, _) {
                return TextFormField(
                  controller: searchSectorEconomic.controllerSearch,
                  style: TextStyle(color: Colors.black),
                  textInputAction: TextInputAction.search,
                  onFieldSubmitted: (e) {
                    searchSectorEconomic.searchSectorEconomic(e);
                  },
                  onChanged: (e) {
                    searchSectorEconomic.changeAction(e);
                  },
                  cursorColor: Colors.black,
                  textCapitalization: TextCapitalization.characters,
                  decoration: new InputDecoration(
                    hintText: "Cari Sektor Ekonomi (minimal 3 karakter)",
                    hintStyle: TextStyle(color: Colors.black),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: myPrimaryColor),
                      //  when the TextFormField in unfocused
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: myPrimaryColor),
                      //  when the TextFormField in focused
                    ),
                  ),
                  autofocus: true,
                );
              },
            ),
            backgroundColor: myPrimaryColor,
            iconTheme: IconThemeData(color: Colors.black),
            actions: <Widget>[
              Provider.of<SearchSectorEconomicChangeNotif>(context,
                          listen: true)
                      .showClear
                  ? IconButton(
                      icon: Icon(Icons.clear),
                      onPressed: () {
                        Provider.of<SearchSectorEconomicChangeNotif>(context,
                            listen: false).clearSearchTemp();
                        Provider.of<SearchSectorEconomicChangeNotif>(context,
                                listen: false)
                            .controllerSearch
                            .clear();
                        Provider.of<SearchSectorEconomicChangeNotif>(context,
                                listen: false)
                            .changeAction(
                                Provider.of<SearchSectorEconomicChangeNotif>(
                                        context,
                                        listen: false)
                                    .controllerSearch
                                    .text);
                      })
                  : SizedBox(
                      width: 0.0,
                      height: 0.0,
                    )
            ],
          ),
          body: Consumer<SearchSectorEconomicChangeNotif>(
            builder: (context, searchSectorEconomic, _) {
              return searchSectorEconomic.listSectorEconomicTemp.isNotEmpty
                ? ListView.separated(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height / 57,
                      horizontal: MediaQuery.of(context).size.width / 27),
                  itemCount: searchSectorEconomic.listSectorEconomicTemp.length,
                  itemBuilder: (listContext, index) {
                    return InkWell(
                      onTap: () {
                        Navigator.pop(context,
                            searchSectorEconomic.listSectorEconomicTemp[index]);
                      },
                      child: Container(
                        child: Row(
                          children: [
                            Expanded(
                                child: Text(
                                  "${searchSectorEconomic.listSectorEconomicTemp[index].KODE} - "
                                      "${searchSectorEconomic.listSectorEconomicTemp[index].DESKRIPSI}",
                                  style: TextStyle(fontSize: 16),
                                ))
                          ],
                        ),
                      ),
                    );
                  },
                  separatorBuilder: (context, index) {
                    return Divider();
                  },
                )
                : ListView.separated(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height / 57,
                      horizontal: MediaQuery.of(context).size.width / 27),
                  itemCount: searchSectorEconomic.listSectorEconomic.length,
                  itemBuilder: (listContext, index) {
                    return InkWell(
                      onTap: () {
                        Navigator.pop(context,
                            searchSectorEconomic.listSectorEconomic[index]);
                      },
                      child: Container(
                        child: Row(
                          children: [
                            Expanded(
                                child: Text(
                              "${searchSectorEconomic.listSectorEconomic[index].KODE} - "
                              "${searchSectorEconomic.listSectorEconomic[index].DESKRIPSI}",
                              style: TextStyle(fontSize: 16),
                            ))
                          ],
                        ),
                      ),
                    );
                  },
                  separatorBuilder: (context, index) {
                    return Divider();
                  },
                );
            },
          )),
    );
  }
}
