import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_keluarga(ibu)_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class FormMInformasiKeluargaIBU extends StatefulWidget {

  @override
  _FormMInformasiKeluargaIBUState createState() => _FormMInformasiKeluargaIBUState();
}

class _FormMInformasiKeluargaIBUState extends State<FormMInformasiKeluargaIBU> {
  Future<void> _setPreference;

  @override
  void initState() {
    super.initState();
    _setPreference = Provider.of<FormMInformasiKeluargaIBUChangeNotif>(context, listen: false).setPreference();
    // Provider.of<FormMInformasiKeluargaIBUChangeNotif>(context,listen: false).setDataSQLite();
  }
  @override
  Widget build(BuildContext context) {
    var _size = MediaQuery.of(context).size;
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange),
      child: FutureBuilder(
        future: _setPreference,
        builder: (context, snapshot) {
          return Consumer<FormMInformasiKeluargaIBUChangeNotif>(
            builder: (context, formMInfoKelIBUchangeNotif, _) {
              formMInfoKelIBUchangeNotif.setDefaultValue();
              return Scaffold(
                appBar: AppBar(
                  title:
                  Text("Informasi Keluarga (Ibu)", style: TextStyle(color: Colors.black)),
                  centerTitle: true,
                  backgroundColor: myPrimaryColor,
                  iconTheme: IconThemeData(color: Colors.black),
                ),
                body: Form(
                  onWillPop: formMInfoKelIBUchangeNotif.onBackPress,
                  key: formMInfoKelIBUchangeNotif.keyForm,
                  child: ListView(
                    padding: EdgeInsets.symmetric(
                        vertical: MediaQuery.of(context).size.height/57,
                        horizontal: MediaQuery.of(context).size.width/37
                    ),
                    children: [
                      Visibility(visible: formMInfoKelIBUchangeNotif.isRelationshipStatusVisible,
                        child: DropdownButtonFormField<RelationshipStatusModel>(
                            autovalidate: formMInfoKelIBUchangeNotif.autoValidate,
                            validator: (e) {
                              if (e == null && formMInfoKelIBUchangeNotif.isRelationshipStatusMandatory) {
                                return "Silahkan pilih status hubungan";
                              } else {
                                return null;
                              }
                            },
                            disabledHint: Text(formMInfoKelIBUchangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_NAME),
                            value: formMInfoKelIBUchangeNotif.relationshipStatusSelected,
                            onChanged: null,
                            //     (value) {
                            //   // formMInfoKelIBUchangeNotif.relationshipStatusSelected = value;
                            // },
                            onTap: () {
                              FocusManager.instance.primaryFocus.unfocus();
                            },
                            decoration: InputDecoration(
                              labelText: "Status hubungan",
                              filled: true,
                              fillColor: Colors.black12,
                              border: OutlineInputBorder(),
                              enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: formMInfoKelIBUchangeNotif.isRelationshipStatusChanges
                                  ? Colors.purple : Colors.grey)),
                              disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: formMInfoKelIBUchangeNotif.isRelationshipStatusChanges
                                  ? Colors.purple : Colors.grey)),
                              contentPadding: EdgeInsets.symmetric(horizontal: 10),
                            ),
                            items: formMInfoKelIBUchangeNotif.listRelationShipStatus
                                .map((value) {
                              return DropdownMenuItem<RelationshipStatusModel>(
                                value: value,
                                child: Text(
                                  value.PARA_FAMILY_TYPE_NAME,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              );
                            }).toList()),
                      ),
                      Visibility(visible: formMInfoKelIBUchangeNotif.isRelationshipStatusVisible,
                        child: SizedBox(height: _size.height / 47),),
                      Visibility(visible: formMInfoKelIBUchangeNotif.isIdentityVisible,
                        child: DropdownButtonFormField<IdentityModel>(
                          value: formMInfoKelIBUchangeNotif.identitySelected,
                          autovalidate: formMInfoKelIBUchangeNotif.autoValidate,
                          validator: (e) {
                            if (e == null && formMInfoKelIBUchangeNotif.isIdentityMandatory) {
                              return "Tidak boleh kosong";
                            } else {
                              return null;
                            }
                          },
                          decoration: InputDecoration(
                            labelText: "Jenis Identitas",
                            border: OutlineInputBorder(),
                            enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: formMInfoKelIBUchangeNotif.isIdentityChanges
                                ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: formMInfoKelIBUchangeNotif.isIdentityChanges
                                ? Colors.purple : Colors.grey)),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10),
                          ),
                          items: formMInfoKelIBUchangeNotif.listIdentityType.map((data) {
                            return DropdownMenuItem<IdentityModel>(
                              value: data,
                              child: Text(
                                data.name,
                                overflow: TextOverflow.ellipsis,
                              ),
                            );
                          }).toList(),
                          onChanged: (newVal) {
                            formMInfoKelIBUchangeNotif.identitySelected = newVal;
                          },
                          onTap: () {
                            FocusManager.instance.primaryFocus.unfocus();
                          },
                        ),
                      ),
                      Visibility(visible: formMInfoKelIBUchangeNotif.isIdentityVisible,
                          child: SizedBox(height: _size.height / 47)),
                      Visibility(visible: formMInfoKelIBUchangeNotif.isNoIdentitasVisible,
                        child: TextFormField(
                            controller: formMInfoKelIBUchangeNotif.controllerNoIdentitas,
                            autovalidate: formMInfoKelIBUchangeNotif.autoValidate,
                            validator: (e) {
                              if (e.isEmpty && formMInfoKelIBUchangeNotif.isNoIdentitasMandatory) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                              labelText: 'Nomor Identitas',
                              labelStyle: TextStyle(color: Colors.black),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: formMInfoKelIBUchangeNotif.isNoIdentitasChanges
                                  ? Colors.purple : Colors.grey)),
                              disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: formMInfoKelIBUchangeNotif.isNoIdentitasChanges
                                  ? Colors.purple : Colors.grey)),
                              contentPadding: EdgeInsets.symmetric(horizontal: 10),
                            ),
                            keyboardType: formMInfoKelIBUchangeNotif.identitySelected != null
                                ? formMInfoKelIBUchangeNotif.identitySelected.id != "03"
                                ? TextInputType.number
                                : TextInputType.text
                                : TextInputType.number,
                            textCapitalization: TextCapitalization.characters,
                            inputFormatters: formMInfoKelIBUchangeNotif.identitySelected != null
                                ? formMInfoKelIBUchangeNotif.identitySelected.id != "03"
                                ? [WhitelistingTextInputFormatter.digitsOnly]
                                : null
                                : [WhitelistingTextInputFormatter.digitsOnly]
                        ),
                      ),
                      Visibility(visible: formMInfoKelIBUchangeNotif.isNoIdentitasVisible,
                          child: SizedBox(height: _size.height / 47)),
                      Visibility(visible: formMInfoKelIBUchangeNotif.isNamaLengkapdentitasVisible,
                        child: TextFormField(
                          controller: formMInfoKelIBUchangeNotif.controllerNamaLengkapdentitas,
                          enabled: formMInfoKelIBUchangeNotif.isDisablePACIAAOSCONA ? false : true,
                          style: new TextStyle(color: Colors.black),
                          decoration: new InputDecoration(
                            labelText: 'Nama Lengkap Sesuai Identitas',
                            labelStyle: TextStyle(color: Colors.black),
                            filled: formMInfoKelIBUchangeNotif.isDisablePACIAAOSCONA,
                            fillColor: Colors.black12,
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: formMInfoKelIBUchangeNotif.isNamaLengkapdentitasChanges
                                ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: formMInfoKelIBUchangeNotif.isNamaLengkapdentitasChanges
                                ? Colors.purple : Colors.grey)),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10),
                          ),
                          keyboardType: TextInputType.text,
                          textCapitalization: TextCapitalization.characters,
                          autovalidate: formMInfoKelIBUchangeNotif.autoValidate,
                          validator: (e) {
                            if (e.isEmpty && formMInfoKelIBUchangeNotif.isNamaLengkapdentitasMandatory) {
                              return "Tidak boleh kosong";
                            } else {
                              return null;
                            }
                          },
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(RegExp('[a-zA-Z  \`]')),
                          ],
                        ),
                      ),
                      Visibility(visible: formMInfoKelIBUchangeNotif.isNamaLengkapdentitasVisible,
                          child: SizedBox(height: _size.height / 47)),
                      Visibility(visible: formMInfoKelIBUchangeNotif.isNamaIdentitasVisible,
                        child: TextFormField(
                          enabled: formMInfoKelIBUchangeNotif.enableDataDedup,// false,
                          controller: formMInfoKelIBUchangeNotif.controllerNamaIdentitas,
                          style: new TextStyle(color: Colors.black),
                          decoration: new InputDecoration(
                            labelText: 'Nama Lengkap',
                            labelStyle: TextStyle(color: Colors.black),
                            filled: formMInfoKelIBUchangeNotif.isDisablePACIAAOSCONA,
                            fillColor: Colors.black12,
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: formMInfoKelIBUchangeNotif.isNamaIdentitasChanges
                                ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: formMInfoKelIBUchangeNotif.isNamaIdentitasChanges
                                ? Colors.purple : Colors.grey)),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10),
                          ),
                          keyboardType: TextInputType.text,
                          textCapitalization: TextCapitalization.characters,
                          autovalidate: formMInfoKelIBUchangeNotif.autoValidate,
                          validator: (e) {
                            if (e.isEmpty && formMInfoKelIBUchangeNotif.isNamaIdentitasMandatory) {
                              return "Tidak boleh kosong";
                            } else {
                              return null;
                            }
                          },
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(RegExp('[a-zA-Z  \`]')),
                          ],
                        ),
                      ),
                      Visibility(visible: formMInfoKelIBUchangeNotif.isNamaIdentitasVisible,
                          child: SizedBox(height: _size.height / 47)),
                      Visibility(visible: formMInfoKelIBUchangeNotif.isTglLahirVisible,
                        child: FocusScope(
                            node: FocusScopeNode(),
                            child: TextFormField(
                              onTap: () {
                                FocusManager.instance.primaryFocus.unfocus();
                                formMInfoKelIBUchangeNotif.selectBirthDate(context);
                              },
                              controller: formMInfoKelIBUchangeNotif.controllerTglLahir,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                labelText: 'Tanggal Lahir',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: formMInfoKelIBUchangeNotif.isTglLahirChanges
                                    ? Colors.purple : Colors.grey)),
                                disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: formMInfoKelIBUchangeNotif.isTglLahirChanges
                                    ? Colors.purple : Colors.grey)),
                                contentPadding: EdgeInsets.symmetric(horizontal: 10),
                              ),
                              autovalidate: formMInfoKelIBUchangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty && formMInfoKelIBUchangeNotif.isTglLahirMandatory) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              inputFormatters: [
                                FilteringTextInputFormatter.allow(RegExp('[a-zA-Z]')),
                              ],
                            )),
                      ),
                      Visibility(visible: formMInfoKelIBUchangeNotif.isTglLahirVisible,
                          child: SizedBox(height: _size.height / 47)),
                      Visibility(visible: formMInfoKelIBUchangeNotif.isTempatLahirSesuaiIdentitasVisible,
                        child: TextFormField(
                          controller: formMInfoKelIBUchangeNotif
                              .controllerTempatLahirSesuaiIdentitas,
                          style: new TextStyle(color: Colors.black),
                          decoration: new InputDecoration(
                            labelText: 'Tempat Lahir Sesuai Identitas',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: formMInfoKelIBUchangeNotif.isTempatLahirSesuaiIdentitasChanges
                                ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: formMInfoKelIBUchangeNotif.isTempatLahirSesuaiIdentitasChanges
                                ? Colors.purple : Colors.grey)),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10),
                          ),
                          keyboardType: TextInputType.text,
                          textCapitalization: TextCapitalization.characters,
                          autovalidate: formMInfoKelIBUchangeNotif.autoValidate,
                          validator: (e) {
                            if (e.isEmpty && formMInfoKelIBUchangeNotif.isTempatLahirSesuaiIdentitasMandatory) {
                              return "Tidak boleh kosong";
                            } else {
                              return null;
                            }
                          },
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(RegExp('[a-zA-Z]')),
                          ],
                        ),
                      ),
                      Visibility(visible: formMInfoKelIBUchangeNotif.isTempatLahirSesuaiIdentitasVisible,
                          child: SizedBox(height: _size.height / 47)),
                      Visibility(visible: formMInfoKelIBUchangeNotif.isTempatLahirSesuaiIdentitasLOVVisible,
                        child: TextFormField(
                          controller: formMInfoKelIBUchangeNotif
                              .controllerTempatLahirSesuaiIdentitasLOV,
                          style: new TextStyle(color: Colors.black),
                          decoration: new InputDecoration(
                            labelText: 'Tempat Lahir Sesuai Identitas',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: formMInfoKelIBUchangeNotif.isTempatLahirSesuaiIdentitasLOVChanges
                                ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: formMInfoKelIBUchangeNotif.isTempatLahirSesuaiIdentitasLOVChanges
                                ? Colors.purple : Colors.grey)),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10),
                          ),
                          keyboardType: TextInputType.text,
                          textCapitalization: TextCapitalization.characters,
                          autovalidate: formMInfoKelIBUchangeNotif.autoValidate,
                          validator: (e) {
                            if (e.isEmpty && formMInfoKelIBUchangeNotif.isTempatLahirSesuaiIdentitasLOVMandatory) {
                              return "Tidak boleh kosong";
                            } else {
                              return null;
                            }
                          },
                          readOnly: true,
                          onTap: (){
                            formMInfoKelIBUchangeNotif.searchBirthPlace(context);
                          },
                        ),
                      ),
                      Visibility(visible: formMInfoKelIBUchangeNotif.isTempatLahirSesuaiIdentitasLOVVisible,
                          child: SizedBox(height: _size.height / 47)),
                      Visibility(visible: formMInfoKelIBUchangeNotif.isRadioValueGenderVisible,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Jenis Kelamin",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w700,
                                  letterSpacing: 0.15,
                                  color: formMInfoKelIBUchangeNotif.isRadioValueGenderChanges ? Colors.purple : Colors.black),
                            ),
                            Row(
                              children: [
                                Row(
                                  children: [
                                    Radio(
                                        activeColor: primaryOrange,
                                        value: "01",
                                        groupValue: formMInfoKelIBUchangeNotif.radioValueGender,
                                        onChanged: (value) {
                                          // formMInfoKelIBUchangeNotif.radioValueGender = value;
                                        }),
                                    Text("Laki Laki")
                                  ],
                                ),
                                Row(
                                  children: [
                                    Radio(
                                        activeColor: primaryOrange,
                                        value: "02",
                                        groupValue: formMInfoKelIBUchangeNotif.radioValueGender,
                                        onChanged: (value) {
                                          formMInfoKelIBUchangeNotif.radioValueGender = value;
                                        }),
                                    Text("Perempuan")
                                  ],
                                ),
                              ],
                            ),
                            SizedBox(height: _size.height / 47),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 4,
                            child: Visibility(visible: formMInfoKelIBUchangeNotif.isKodeAreaVisible,
                              child: TextFormField(
                                autovalidate: formMInfoKelIBUchangeNotif.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty && formMInfoKelIBUchangeNotif.isKodeAreaMandatory) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                onChanged: (e) {
                                  formMInfoKelIBUchangeNotif.checkValidCodeArea(e);
                                },
                                keyboardType: TextInputType.number,
                                inputFormatters: [
                                  WhitelistingTextInputFormatter.digitsOnly,
                                  LengthLimitingTextInputFormatter(10),
                                ],
                                controller: formMInfoKelIBUchangeNotif.controllerKodeArea,
                                style: new TextStyle(color: Colors.black),
                                decoration: new InputDecoration(
                                  labelText: 'Kode Area',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: formMInfoKelIBUchangeNotif.isKodeAreaChanges
                                      ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: formMInfoKelIBUchangeNotif.isKodeAreaChanges
                                      ? Colors.purple : Colors.grey)),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                ),
                              ),
                            ),
                          ),
                          Visibility(visible: formMInfoKelIBUchangeNotif.isKodeAreaVisible,
                              child: SizedBox(width: MediaQuery.of(context).size.width / 37)),
                          Expanded(
                            flex: 6,
                            child: Visibility(visible: formMInfoKelIBUchangeNotif.isTlpnVisible,
                              child: TextFormField(
                                autovalidate: formMInfoKelIBUchangeNotif.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty && formMInfoKelIBUchangeNotif.isTlpnMandatory) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                keyboardType: TextInputType.number,
                                inputFormatters: [
                                  WhitelistingTextInputFormatter.digitsOnly,
                                  LengthLimitingTextInputFormatter(10),
                                ],
                                controller: formMInfoKelIBUchangeNotif.controllerTlpn,
                                style: new TextStyle(color: Colors.black),
                                decoration: new InputDecoration(
                                  labelText: 'Telepon',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: formMInfoKelIBUchangeNotif.isTlpnChanges
                                      ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: formMInfoKelIBUchangeNotif.isTlpnChanges
                                      ? Colors.purple : Colors.grey)),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Visibility(
                          visible: formMInfoKelIBUchangeNotif.isKodeAreaVisible && formMInfoKelIBUchangeNotif.isTlpnVisible == true ? true : false,
                          child: SizedBox(height: _size.height / 47)),
                      Visibility(visible: formMInfoKelIBUchangeNotif.isNoHpVisible,
                        child: TextFormField(
                          controller: formMInfoKelIBUchangeNotif.controllerNoHp,
                          style: new TextStyle(color: Colors.black),
                          decoration: new InputDecoration(
                            labelText: 'No Handphone',
                            labelStyle: TextStyle(color: Colors.black),
                            prefixText: "08",
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: formMInfoKelIBUchangeNotif.isNoHpChanges
                                ? Colors.purple : Colors.grey)),
                            disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: formMInfoKelIBUchangeNotif.isNoHpChanges
                                ? Colors.purple : Colors.grey)),
                            contentPadding: EdgeInsets.symmetric(horizontal: 10),
                          ),
                          keyboardType: TextInputType.number,
                          autovalidate: formMInfoKelIBUchangeNotif.autoValidate,
                          onChanged: (e) {
                            formMInfoKelIBUchangeNotif.checkValidNotZero(e);
                          },
                          validator: (e) {
                            if (e.isEmpty && formMInfoKelIBUchangeNotif.isNoHpMandatory) {
                              return "Tidak boleh kosong";
                            } else {
                              return null;
                            }
                          },
                          inputFormatters: [
                            WhitelistingTextInputFormatter.digitsOnly,
                            LengthLimitingTextInputFormatter(12)
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                bottomNavigationBar: BottomAppBar(
                  elevation: 0.0,
                  child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Consumer<FormMInformasiKeluargaIBUChangeNotif>(
                        builder: (context, formMInfoKelIBUchangeNotif, _) {
                          return RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(8.0)),
                              color: myPrimaryColor,
                              onPressed: () {
                                formMInfoKelIBUchangeNotif.check(context);
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text("DONE",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500,
                                          letterSpacing: 1.25))
                                ],
                              ));
                        },
                      )),
                ),
              );
            },
          );
        }
      )
    );
  }
}
