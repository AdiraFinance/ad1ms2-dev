import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/app/info_application.dart';
import 'package:ad1ms2_dev/screens/app/list_app_credit_subsidy.dart';
import 'package:ad1ms2_dev/screens/app/list_app_info_document.dart';
import 'package:ad1ms2_dev/screens/app/list_object_karoseri.dart';
import 'package:ad1ms2_dev/screens/app/marketing_notes.dart';
import 'package:ad1ms2_dev/screens/app/menu_customer_detail.dart';
import 'package:ad1ms2_dev/screens/app/taksasi_unit.dart';
import 'package:ad1ms2_dev/screens/form_m/form_m_foto.dart';
import 'package:ad1ms2_dev/screens/form_m/form_m_guarantor.dart';
import 'package:ad1ms2_dev/screens/form_m/form_m_income.dart';
import 'package:ad1ms2_dev/screens/form_m/form_m_pekerjaan.dart';
import 'package:ad1ms2_dev/screens/survey/result_survey.dart';
import 'package:ad1ms2_dev/services/connectivity_service.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_object_karoseri_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/taksasi_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_individu_change_notifier/form_m_parent_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';
import 'package:ad1ms2_dev/widgets/widget_title_appbar_form_m_parent.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'menu_detail_loan.dart';
import 'menu_object_information.dart';

class FormMParent extends StatefulWidget {
  final String typeFormId;
  final String flag;

  const FormMParent({this.typeFormId, this.flag});
  @override
  _FormMParentState createState() => _FormMParentState();
}

class _FormMParentState extends State<FormMParent> {
  int clear = 1;
  // DbHelper _dbHelper = DbHelper();
  Map _source = {ConnectivityResult.none: false};
  MyConnectivity _connectivity = MyConnectivity();
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print(widget.flag);
    if(clear == 1){
      Provider.of<FormMParentIndividualChangeNotifier>(context, listen: true).clearData(context);
      clear = 2;
    }
  }

  @override
  void initState() {
    super.initState();
    _connectivity.initialise();
    _connectivity.myStream.listen((source) {
      setState(() => _source = source);
    });
  }

  @override
  Widget build(BuildContext context) {

    String _indicatorMessage;
    switch (_source.keys.toList()[0]) {
      case ConnectivityResult.none:
        _indicatorMessage = "Offline";
        break;
      case ConnectivityResult.mobile:
        _indicatorMessage = "Mobile: Online";
        break;
      case ConnectivityResult.wifi:
        _indicatorMessage = "WiFi: Online";
    }

    List<Widget> _list = [
      FormMFoto(),//0
      MenuCustomerDetail(),//1
      FormMPekerjaan(),//2
      FormMIncome(),//3
      Visibility(
        child: FormMGuarantor(),
        visible: Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false).isGuarantorVisible,
      ),//4
      InfoApplication(),//5
      //cek lagi ke atas untuk regex
      MenuObjectInformation(flag: widget.flag),//6
//      InformationObjectUnit(),//9
//      InformationSalesman(),//10
//      InformationCollateral(),//11
      Visibility(
          visible: Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: true).isVisible,
          child: ListObjectKaroseri()
      ),//7
      MenuDetailLoan(),//8
//      InfoCreditStructure(),//13
//      Visibility(
//          visible: Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).isVisible,
//          child:InfoCreditStructureTypeInstallment()
//      ),//14
//      ListMajorInsurance(),//15
//      ListAdditionalInsurance(),//16
//      InfoWMP(),//17
//      InfoCreditIncome(),//18
      ListAppCreditSubsidy(),//9
      ListAppInfoDocument(),//10
      Visibility(
          visible: Provider.of<TaksasiUnitChangeNotifier>(context,listen: false).isVisible,
          child: TaksasiUnit()
      ),//11
      MarketingNotes(),//12
      Visibility(
        child: ResultSurvey(),
        visible: widget.typeFormId != "IDE",
      ),//13
    ];

    var _provider = Provider.of<FormMParentIndividualChangeNotifier>(context, listen: true);

    return Theme(
      data: ThemeData(
          fontFamily: "NunitoSans",
          primaryColor: primaryOrange,
          accentColor: myPrimaryColor
      ),
      child: Scaffold(
        key: _provider.scaffoldKey,
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: myPrimaryColor,
          title: TitleAppBarFormM(
            selectedIndex: _provider.selectedIndex,
          ),
          centerTitle: true,
          iconTheme: IconThemeData(color: Colors.black),
            actions: [
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    padding: EdgeInsets.all(2.1),
                    decoration: new BoxDecoration(
                      color: _indicatorMessage == "Offline" ? Colors.red : Colors.green,
                      borderRadius: BorderRadius.circular(20),
                    ),
                    constraints: BoxConstraints(
                      minWidth: 20,
                      minHeight: 20,
                    ),
                    child: SizedBox(),
                  ),
                ),
              ),
              PopupMenuButton(
                  onSelected: (selected){
                    // Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).loadData = false;
                    Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).isShowDialog(context, selected);
                  },
                  child: Icon(Icons.more_vert),
                  itemBuilder: (context) {
                    return LabelPopUpMenuButtonSaveToDraftBackToHome.choices.map((String choice) {
                      return PopupMenuItem(
                        value: choice,
                        child: Text(choice),
                      );
                    }).toList();
                  }
              )
            ],
        ),
        body: Consumer<FormMParentIndividualChangeNotifier>(
            builder: (context, parent, _) {
              return Form(
                  child: _list.elementAt(parent.selectedIndex),
                  key: parent.formKeys[parent.selectedIndex]
              );
            },
        ),
        drawer: Drawer(
            child: ListView(
              children: [
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "Nasabah",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                ),
                // ListTile(
                //   onTap: () {
                //     _provider.checkTabDrawer(context, 0);
                //     Navigator.pop(context);
                //   },
                //   title: Padding(
                //     padding: EdgeInsets.all(16.0),
                //     child: Text("Credit Limit"),
                //   ),
                //   trailing: _provider.isCreditLimitDone
                //       ? Icon(Icons.check, color: Colors.green)
                //       : null,
                //   selected: _provider.selectedIndex == 0,
                // ),
                ListTile(
                  onTap: () {
                    _provider.checkTabDrawer(context, 0);
                    Navigator.pop(context);
                  },
                  title: Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Text("Rincian Foto"),
                  ),
                  trailing: _provider.isPhotoDone
                      ? Icon(Icons.check, color: Colors.green)
                      : null,
                  selected: _provider.selectedIndex == 0,
                ),
                ListTile(
                  onTap: _provider.isPhotoDone
                      ?
                      ()
                  {
                    _provider.checkTabDrawer(context, 1);
                    Navigator.pop(context);
                  }
                      : null,
                  title: Padding(
                    padding: EdgeInsets.all(16.0),
                    child: _provider.isMenuCustomerDetailDone
                    ? Text("Rincian Nasabah")
                    : _provider.selectedIndex == 1
                      ? Text("Rincian Nasabah")
                      : _provider.isPhotoDone
                        ? Text("Rincian Nasabah")
                        : Text("Rincian Nasabah", style: TextStyle(color: Colors.grey),)
                  ),
                  trailing: _provider.isMenuCustomerDetailDone
                      ? Icon(Icons.check, color: Colors.green)
                      : null,
                  selected: _provider.selectedIndex == 1,
                ),
//                  ListTile(
//                    onTap: _provider.isCustomerInfoDone
//                        ?
//                        (){_provider.checkTabDrawer(context, 2);Navigator.pop(context);}
//                        : null,
//                    title: Container(
//                        margin: EdgeInsets.only(left: 16),
//                        child: Text("Informasi Alamat")),
//                    trailing: _provider.isAddressInfoDone
//                        ? Icon(Icons.check, color: Colors.green)
//                        : null,
//                    selected: _provider.selectedIndex == 2,
//                  ),
//                  ListTile(
//                    onTap: _provider.isAddressInfoDone
//                        ?
//                        (){_provider.checkTabDrawer(context, 3);Navigator.pop(context);}
//                        : null,
//                    title: Container(
//                        margin: EdgeInsets.only(left: 16),
//                        child: Text("Informasi Keluarga(Ibu)")),
//                    trailing: _provider.isFamilyInformationMotherDone
//                        ? Icon(Icons.check, color: Colors.green)
//                        : null,
//                    selected: _provider.selectedIndex == 3,
//                  ),
//                  ListTile(
//                    onTap: _provider.isMenuCustomerDetailDone
//                        ?
//                        (){_provider.checkTabDrawer(context, 2);Navigator.pop(context);}
//                        : null,
//                    title: Container(
//                        margin: EdgeInsets.only(left: 16),
//                        child: Text("Informasi Keluarga")),
//                    trailing: _provider.isFamilyInformationDone
//                        ? Icon(Icons.check, color: Colors.green)
//                        : null,
//                    selected: _provider.selectedIndex == 2,
//                  ),
                ListTile(
                  onTap: _provider.isMenuCustomerDetailDone
                      ?
                      (){_provider.checkTabDrawer(context, 2);Navigator.pop(context);}
                      : null,
                  title: Container(
                      margin: EdgeInsets.only(left: 16),
                      child: _provider.isOccupationDone
                      ? Text("Pekerjaan")
                      : _provider.selectedIndex == 2
                        ? Text("Pekerjaan")
                        : _provider.isMenuCustomerDetailDone
                          ? Text("Pekerjaan")
                          : Text("Pekerjaan", style: TextStyle(color: Colors.grey),)
                  ),
                  trailing: _provider.isOccupationDone
                      ? Icon(Icons.check, color: Colors.green)
                      : null,
                  selected: _provider.selectedIndex == 2,
                ),
                ListTile(
                  onTap: _provider.isOccupationDone
                      ?
                      (){_provider.checkTabDrawer(context, 3);Navigator.pop(context);}
                      : null,
                  title: Container(
                      margin: EdgeInsets.only(left: 16),
                      child: _provider.isIncomeDone
                      ? Text("Pendapatan")
                      : _provider.selectedIndex == 3
                        ? Text("Pendapatan")
                        : _provider.isOccupationDone
                          ? Text("Pendapatan")
                          : Text("Pendapatan", style: TextStyle(color: Colors.grey),)
                  ),
                  trailing: _provider.isIncomeDone
                      ? Icon(Icons.check, color: Colors.green)
                      : null,
                  selected: _provider.selectedIndex == 3,
                ),
                Visibility(
                  visible: Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false).isGuarantorVisible,
                  child: ListTile(
                    onTap: _provider.isIncomeDone
                        ?
                        (){_provider.checkTabDrawer(context, 4);Navigator.pop(context);}
                        : null,
                    title: Container(
                        margin: EdgeInsets.only(left: 16),
                        child: _provider.isGuarantorDone
                        ? Text("Penjamin")
                        : _provider.selectedIndex == 4
                          ? Text("Penjamin")
                          : _provider.isIncomeDone
                            ? Text("Penjamin")
                            : Text("Penjamin", style: TextStyle(color: Colors.grey),)
                    ),
                    trailing: _provider.isGuarantorDone
                        ? Icon(Icons.check, color: Colors.green)
                        : null,
                    selected: _provider.selectedIndex == 4,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "Aplikasi",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                ),
                ListTile(
                  onTap: _provider.isGuarantorDone || _provider.isIncomeDone
                      ?
                      (){_provider.checkTabDrawer(context, 5);Navigator.pop(context);}
                      : null,
                  title: Container(
                      margin: EdgeInsets.only(left: 16),
                      child: _provider.isInfoAppDone
                      ? Text("Aplikasi")
                      : _provider.selectedIndex == 5
                        ? Text("Aplikasi")
                        : _provider.isGuarantorDone
                          ? Text("Aplikasi")
                          : Text("Aplikasi", style: TextStyle(color: Colors.grey),)
                  ),
                  trailing: _provider.isInfoAppDone
                      ? Icon(Icons.check, color: Colors.green)
                      : null,
                  selected: _provider.selectedIndex == 5,
                ),
                ListTile(
                  onTap: _provider.isInfoAppDone
                      ?
                      (){_provider.checkTabDrawer(context, 6);Navigator.pop(context);}
                      : null,
                  title: Container(
                      margin: EdgeInsets.only(left: 16),
                      child:  _provider.isMenuObjectInformationDone
                      ? Text("Informasi Objek")
                      : _provider.selectedIndex == 6
                        ? Text("Informasi Objek")
                        : _provider.isInfoAppDone
                          ? Text("Informasi Objek")
                          : Text("Informasi Objek", style: TextStyle(color: Colors.grey),)
                  ),
                  trailing: _provider.isMenuObjectInformationDone
                      ? Icon(Icons.check, color: Colors.green)
                      : null,
                  selected: _provider.selectedIndex == 6,
                ),
                Visibility(
                  visible: Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: true).isVisible,
                  child: ListTile(
                    onTap: _provider.isMenuObjectInformationDone
                        ?
                        (){_provider.checkTabDrawer(context, 7);Navigator.pop(context);}
                        : null,
                    title: Container(
                        margin: EdgeInsets.only(left: 16),
                        child: _provider.isInfoObjKaroseriDone
                        ? Text("Karoseri")
                        : _provider.selectedIndex == 7
                          ? Text("Karoseri")
                          : _provider.isMenuObjectInformationDone
                            ? Text("Karoseri")
                            : Text("Karoseri", style: TextStyle(color: Colors.grey),)
                    ),
                    trailing: _provider.isInfoObjKaroseriDone
                        ? Icon(Icons.check, color: Colors.green)
                        : null,
                    selected: _provider.selectedIndex == 7,
                  ),
                ),
                ListTile(
                  onTap: _provider.isInfoObjKaroseriDone
                      ?
                      (){_provider.checkTabDrawer(context, 8);Navigator.pop(context);}
                      : null,
                  title: Container(
                      margin: EdgeInsets.only(left: 16),
                      child: _provider.isMenuDetailLoanDone
                      ? Text("Rincian Pinjaman")
                      : _provider.selectedIndex == 8
                        ? Text("Rincian Pinjaman")
                        : _provider.isInfoObjKaroseriDone
                          ? Text("Rincian Pinjaman")
                          : Text("Rincian Pinjaman", style: TextStyle(color: Colors.grey),)
                  ),
                  trailing: _provider.isMenuDetailLoanDone
                      ? Icon(Icons.check, color: Colors.green)
                      : null,
                  selected: _provider.selectedIndex == 8,
                ),
//                  ListTile(
//                    onTap: _provider.isInfoAppDone
//                        ?
//                        (){_provider.checkTabDrawer(context, 7);Navigator.pop(context);}
//                        : null,
//                    title: Container(
//                        margin: EdgeInsets.only(left: 16),
//                        child: Text("Info Unit Objek")),
//                    trailing: _provider.isUnitObjectInfoDone
//                        ? Icon(Icons.check, color: Colors.green)
//                        : null,
//                    selected: _provider.selectedIndex == 7,
//                  ),
//                  ListTile(
//                    onTap: _provider.isUnitObjectInfoDone
//                        ?
//                        (){_provider.checkTabDrawer(context, 8);Navigator.pop(context);}
//                        : null,
//                    title: Container(
//                        margin: EdgeInsets.only(left: 16),
//                        child: Text("Info Salesman")),
//                    trailing: _provider.isSalesmanInfoDone
//                        ? Icon(Icons.check, color: Colors.green)
//                        : null,
//                    selected: _provider.selectedIndex == 8,
//                  ),
//                  ListTile(
//                    onTap: _provider.isSalesmanInfoDone
//                        ?
//                        (){_provider.checkTabDrawer(context, 9);Navigator.pop(context);}
//                        : null,
//                    title: Container(
//                        margin: EdgeInsets.only(left: 16),
//                        child: Text("Info Jaminan")),
//                    trailing: _provider.isCollateralInfoDone
//                        ? Icon(Icons.check, color: Colors.green)
//                        : null,
//                    selected: _provider.selectedIndex == 9,
//                  ),
//                  ListTile(
//                    onTap: _provider.isMenuObjectInformationDone
//                        ?
//                        (){_provider.checkTabDrawer(context, 8);Navigator.pop(context);}
//                        : null,
//                    title: Container(
//                        margin: EdgeInsets.only(left: 16),
//                        child: Text("Info Struktur Kredit")),
//                    trailing: _provider.isCreditStructureDone
//                        ? Icon(Icons.check, color: Colors.green)
//                        : null,
//                    selected: _provider.selectedIndex == 8,
//                  ),
//                  Visibility(
//                    visible: Provider.of<InfoCreditStructureChangeNotifier>(context,listen: true).isVisible,
//                    child: ListTile(
//                      onTap: _provider.isCreditStructureDone
//                          ?
//                          (){_provider.checkTabDrawer(context, 9);Navigator.pop(context);}
//                          : null,
//                      title: Container(
//                          margin: EdgeInsets.only(left: 16),
//                          child: Text("Info Struktur Kredit Jenis Angsuran")),
//                      trailing: _provider.isCreditStructureTypeInstallmentDone
//                          ? Icon(Icons.check, color: Colors.green)
//                          : null,
//                      selected: _provider.selectedIndex == 9,
//                    ),
//                  ),
//                  ListTile(
//                    onTap:
//                    Provider.of<InfoCreditStructureChangeNotifier>(context,listen: true).isVisible
//                        ? _provider.isCreditStructureTypeInstallmentDone
//                        ?  (){_provider.checkTabDrawer(context, 10);Navigator.pop(context);}
//                        : null
//                        : _provider.isCreditStructureDone
//                        ?(){_provider.checkTabDrawer(context, 13);Navigator.pop(context);}
//                        :null,
//                    title: Container(
//                        margin: EdgeInsets.only(left: 16),
//                        child: Text("Info Asuransi Utama")),
//                    trailing: _provider.isMajorInsuranceDone
//                        ? Icon(Icons.check, color: Colors.green)
//                        : null,
//                    selected: _provider.selectedIndex == 10,
//                  ),
//                  ListTile(
//                    onTap: _provider.isMajorInsuranceDone
//                        ?
//                        (){_provider.checkTabDrawer(context, 11);Navigator.pop(context);}
//                        : null,
//                    title: Container(
//                        margin: EdgeInsets.only(left: 16),
//                        child: Text("Info Asuransi Tambahan")),
//                    trailing: _provider.isAdditionalInsuranceDone
//                        ? Icon(Icons.check, color: Colors.green)
//                        : null,
//                    selected: _provider.selectedIndex == 11,
//                  ),
//                  ListTile(
//                    onTap: _provider.isAdditionalInsuranceDone
//                        ?
//                        (){_provider.checkTabDrawer(context, 12);Navigator.pop(context);}
//                        : null,
//                    title: Container(
//                        margin: EdgeInsets.only(left: 16),
//                        child: Text("Info WMP")),
//                    trailing: _provider.isInfoWMPDone
//                        ? Icon(Icons.check, color: Colors.green)
//                        : null,
//                    selected: _provider.selectedIndex == 12,
//                  ),
//                  ListTile(
//                    onTap: _provider.isMenuDetailLoanDone
//                        ?
//                        (){_provider.checkTabDrawer(context, 10);Navigator.pop(context);}
//                        : null,
//                    title: Container(
//                        margin: EdgeInsets.only(left: 16),
//                        child: Text("Info Kredit Income")),
//                    trailing: _provider.isCreditIncomeDone
//                        ? Icon(Icons.check, color: Colors.green)
//                        : null,
//                    selected: _provider.selectedIndex == 10,
//                  ),
                ListTile(
                  onTap: _provider.isMenuDetailLoanDone
                      ?
                      (){_provider.checkTabDrawer(context, 9);Navigator.pop(context);}
                      : null,
                  title: Container(
                      margin: EdgeInsets.only(left: 16),
                      child: _provider.isCreditSubsidyDone
                      ? Text("Subsidi")
                      : _provider.selectedIndex == 9
                        ? Text("Subsidi")
                        : _provider.isMenuDetailLoanDone
                          ? Text("Subsidi")
                          : Text("Subsidi", style: TextStyle(color: Colors.grey),)
                  ),
                  trailing: _provider.isCreditSubsidyDone
                      ? Icon(Icons.check, color: Colors.green)
                      : null,
                  selected: _provider.selectedIndex == 9
                ),
                ListTile(
                  onTap: _provider.isCreditSubsidyDone
                  ? (){_provider.checkTabDrawer(context, 10);Navigator.pop(context);}
                  : null,
                  title: Container(
                      margin: EdgeInsets.only(left: 16),
                      child: _provider.isDocumentInfoDone
                      ? Text("Informasi Dokumen")
                      : _provider.selectedIndex == 10
                        ? Text("Informasi Dokumen")
                        : _provider.isCreditSubsidyDone
                          ? Text("Informasi Dokumen")
                          : Text("Informasi Dokumen", style: TextStyle(color: Colors.grey),)
                  ),
                  trailing: _provider.isDocumentInfoDone
                      ? Icon(Icons.check, color: Colors.green)
                      : null,
                  selected: _provider.selectedIndex == 10,
                ),
                Visibility(
                  visible: Provider.of<TaksasiUnitChangeNotifier>(context,listen: false).isVisible,
                  child: ListTile(
                    onTap: _provider.isDocumentInfoDone
                    ? (){_provider.checkTabDrawer(context, 11);Navigator.pop(context);}
                    : null,
                    title: Container(
                        margin: EdgeInsets.only(left: 16),
                        child: _provider.isTaksasiUnitDone
                        ? Text("Taksasi Unit")
                        : _provider.selectedIndex == 11
                          ? Text("Taksasi Unit")
                          : _provider.isDocumentInfoDone
                            ? Text("Taksasi Unit")
                            : Text("Taksasi Unit", style: TextStyle(color: Colors.grey),)
                    ),
                    trailing: _provider.isTaksasiUnitDone
                        ? Icon(Icons.check, color: Colors.green)
                        : null,
                    selected: _provider.selectedIndex == 11,
                  ),
                ),
                ListTile(
                  onTap: Provider.of<TaksasiUnitChangeNotifier>(context,listen: true).isVisible
                  ? _provider.isTaksasiUnitDone
                    ? (){_provider.checkTabDrawer(context, 12);Navigator.pop(context);}
                    : null
                  : _provider.isDocumentInfoDone
                    ? (){_provider.checkTabDrawer(context, 12);Navigator.pop(context);}
                    : null,
                  title: Container(
                      margin: EdgeInsets.only(left: 16),
                      child: _provider.isMarketingNotesDone
                      ? Text("Marketing Notes")
                      : _provider.selectedIndex == 12
                        ? Text("Marketing Notes")
                        : Provider.of<TaksasiUnitChangeNotifier>(context,listen: true).isVisible
                          ? _provider.isTaksasiUnitDone
                            ? Text("Marketing Notes")
                            : Text("Marketing Notes", style: TextStyle(color: Colors.grey),)
                          : _provider.isDocumentInfoDone
                            ? Text("Marketing Notes")
                            : Text("Marketing Notes", style: TextStyle(color: Colors.grey),)
                ),
                  trailing: _provider.isMarketingNotesDone
                      ? Icon(Icons.check, color: Colors.green)
                      : null,
                  selected: _provider.selectedIndex == 12,
                ),
                Visibility(
                  visible: widget.typeFormId != "IDE",
                  child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      "SURVEY",
                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                Visibility(
                  visible: widget.typeFormId != "IDE",
                  child: ListTile(
                    onTap: _provider.isDocumentInfoDone
                    ? (){_provider.checkTabDrawer(context, 13);Navigator.pop(context);}
                    : null,
                    title: Container(
                        margin: EdgeInsets.only(left: 16),
                        child: _provider.isTaksasiUnitDone
                        ? Text("Hasil Survey")
                        : _provider.selectedIndex == 13
                          ? Text("Hasil Survey")
                          : _provider.isDocumentInfoDone
                            ? Text("Hasil Survey")
                            : Text("Hasil Survey", style: TextStyle(color: Colors.grey),)
                    ),
                    trailing: _provider.isTaksasiUnitDone
                        ? Icon(Icons.check, color: Colors.green)
                        : null,
                    selected: _provider.selectedIndex == 13,
                  ),
                ),
              ],
            )
        ),
        bottomNavigationBar:
        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).loadData
            ?
        Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                CircularProgressIndicator(),
                SizedBox(height: 13),
                Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).messageProcess != null
                    ?
                Text(
                    "${Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).messageProcess}",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w500
                  ),
                )
                    :
                SizedBox()
              ],
            )
        )
            :
        _provider.selectedIndex == 0
            ?
        BottomAppBar(
          child: Container(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width / 27,
                vertical: MediaQuery.of(context).size.height / 57),
            child: RaisedButton(
                color: myPrimaryColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)
                ),
                onPressed: () {
                  Provider.of<FormMParentIndividualChangeNotifier>(context, listen: false)
                      .checkBtnNext(context);
                },
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [Text("NEXT", style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, letterSpacing: 1.25),)]
                )
            ),
          ),
        )
            :
        BottomAppBar(
          elevation: 0.0,
          child: Container(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width / 27,
                vertical: MediaQuery.of(context).size.height / 57),
            child: Row(
              children: [
                Expanded(
                  flex: 5,
                  child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)),
                      onPressed: () {
                        Provider.of<FormMParentIndividualChangeNotifier>(context,
                            listen: false)
                            .backBtn(context);
                      },
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [Text("BACK")])),
                ),
                SizedBox(width: MediaQuery.of(context).size.width / 37),
                Expanded(
                  flex: 5,
                  child: RaisedButton(
                      color: myPrimaryColor,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)),
                      onPressed: () {
                        print(Provider.of<FormMParentIndividualChangeNotifier>(context, listen: false).selectedIndex);
                        // Provider.of<FormMParentIndividualChangeNotifier>(context, listen: false).generateAplikasiPayung(context);
                        if(widget.typeFormId != "IDE"){
                          if(Provider.of<FormMParentIndividualChangeNotifier>(context, listen: false).selectedIndex == 13) {
                            print("sre if");
                            Provider.of<FormMParentIndividualChangeNotifier>(context, listen: false).generateAplikasiPayung(context);
                            // SURVEY INDIVIDU
//                            Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).checkBtnNext(context);
//                            Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => Dashboard()), (Route <dynamic> route) => false);
                          }
                          else {
                            print("sre else");
                            Provider.of<FormMParentIndividualChangeNotifier>(context, listen: false).checkBtnNext(context);
                          }
                        }
                        else{
                          if(Provider.of<FormMParentIndividualChangeNotifier>(context, listen: false).selectedIndex == 12) {
                            print("mlaku");
                            // IDE INDIVIDU
                            Provider.of<FormMParentIndividualChangeNotifier>(context, listen: false).generateAplikasiPayung(context);
//                            Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).checkBtnNext(context);
//                            Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => Dashboard()), (Route <dynamic> route) => false);
                          }
                          else {
                            print("mlaku next");
                            Provider.of<FormMParentIndividualChangeNotifier>(context, listen: false).checkBtnNext(context);
                          }
                        }
                      },
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            widget.typeFormId != "IDE"
                                ?
                            Provider.of<FormMParentIndividualChangeNotifier>(context, listen: false).selectedIndex != 13
                                ?
                            Text("NEXT")
                                :
                            Text("SUBMIT")
                                :
                            Provider.of<FormMParentIndividualChangeNotifier>(context, listen: false).selectedIndex != 12
                                ?
                            Text("NEXT")
                                :
                            Text("SUBMIT")
                          ]
                      )
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

//                      children: [
//                Padding(
//                  padding: EdgeInsets.all(8.0),
//                  child: Text(
//                    "Foto",
//                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
//                  ),
//                ),
//                ListTile(
//                  onTap: () {
////                        _provider.checkTabDrawer(context, 0);
////                        Navigator.pop(context);
//                  },
//                  title: Text("Foto"),
//                  trailing: _provider.isPhotoDone
//                      ? Icon(Icons.check, color: Colors.green)
//                      : null,
//                  selected: _provider.selectedIndex == 0,
//                ),
//                ListTile(
//                  onTap: _provider.isPhotoDone
//                      ? () {
////                          _provider.checkTabDrawer(context, 1);
////                          Navigator.pop(context);
//                        }
//                      : null,
//                  title: Text("Informasi Nasabah"),
//                  trailing: _provider.isCustomerInfoDone
//                      ? Icon(Icons.check, color: Colors.green)
//                      : null,
//                  selected: _provider.selectedIndex == 1,
//                ),
//                ListTile(
//                  onTap: _provider.isCustomerInfoDone
//                      ? () {
////                          _provider.checkTabDrawer(context, 2);
////                          Navigator.pop(context);
//                        }
//                      : null,
//                  title: Container(
//                      margin: EdgeInsets.only(left: 16),
//                      child: Text("Informasi Alamat")),
//                  trailing: _provider.isAddressInfoDone
//                      ? Icon(Icons.check, color: Colors.green)
//                      : null,
//                  selected: _provider.selectedIndex == 2,
//                ),
//                ListTile(
//                  onTap: _provider.isInfoSalesDone
//                      ? () {
//                          _provider.checkTabDrawer(context, 3);
//                          Navigator.pop(context);
//                        }
//                      : null,
//                  title: Container(
//                      margin: EdgeInsets.only(left: 16),
//                      child: Text("Info Collateral")),
//                  trailing: _provider.isInfoCollateralDone
//                      ? Icon(Icons.check, color: Colors.green)
//                      : null,
//                  selected: _provider.selectedIndex == 3,
//                ),
//
////                ListTile(
////                  onTap: () {},
////                  title: Container(
////                      margin: EdgeInsets.only(left: 16),
////                      child: Text("Info Nasabah")),
////                  selected: _provider.selectedIndex == 1,
////                ),
////                ListTile(
////                  title: Container(
////                    child: Text('Info Alamat'),
////                    margin: EdgeInsets.only(left: 16),
////                  ),
////                  onTap: () {},
////                ),
////                ListTile(
////                  title: Container(
////                    child: Text('Info Keluarga(Ibu)'),
////                    margin: EdgeInsets.only(left: 16),
////                  ),
////                  onTap: () {},
////                ),
////                ListTile(
////                  title: Container(
////                    child: Text('Info Keluarga(Ibu)'),
////                    margin: EdgeInsets.only(left: 16),
////                  ),
////                  onTap: () {},
////                ),
////                ListTile(
////                  title: Container(
////                    child: Text('Info Keluarga'),
////                    margin: EdgeInsets.only(left: 16),
////                  ),
////                  onTap: () {},
////                ),
////                Padding(
////                  padding: const EdgeInsets.all(8.0),
////                  child: Text(
////                    "Pekerjaan",
////                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
////                  ),
////                ),
////                ListTile(
////                  title: Container(
////                    child: Text('Wiraswasta'),
////                    margin: EdgeInsets.only(left: 16),
////                  ),
////                  onTap: () {},
////                ),
////                ListTile(
////                  title: Container(
////                    child: Text('Profesional'),
////                    margin: EdgeInsets.only(left: 16),
////                  ),
////                  onTap: () {},
////                ),
////                ListTile(
////                  title: Container(
////                    child: Text('Lainnya'),
////                    margin: EdgeInsets.only(left: 16),
////                  ),
////                  onTap: () {},
////                ),
////                Padding(
////                  padding: const EdgeInsets.all(8.0),
////                  child: Text(
////                    "Pendapatan",
////                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
////                  ),
////                ),
////                ListTile(
////                  title: Container(
////                    child: Text('Wiraswasta'),
////                    margin: EdgeInsets.only(left: 16),
////                  ),
////                  onTap: () {},
////                ),
////                ListTile(
////                  title: Container(
////                    child: Text('Profesional'),
////                    margin: EdgeInsets.only(left: 16),
////                  ),
////                  onTap: () {},
////                ),
////                ListTile(
////                  title: Container(
////                    child: Text('Lainnya'),
////                    margin: EdgeInsets.only(left: 16),
////                  ),
////                  onTap: () {},
////                ),
////                Padding(
////                  padding: const EdgeInsets.all(8.0),
////                  child: Text(
////                    "Penjamin",
////                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
////                  ),
////                ),
////                ListTile(
////                  title: Container(
////                    child: Text('Individu'),
////                    margin: EdgeInsets.only(left: 16),
////                  ),
////                  onTap: () {},
////                ),
////                ListTile(
////                  title: Container(
////                    child: Text('Kelembagaan'),
////                    margin: EdgeInsets.only(left: 16),
////                  ),
////                  onTap: () {},
////                ),
//              ]
