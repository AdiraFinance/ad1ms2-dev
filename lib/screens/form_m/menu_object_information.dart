import 'package:ad1ms2_dev/screens/app/information_collateral.dart';
import 'package:ad1ms2_dev/screens/app/information_object_unit.dart';
import 'package:ad1ms2_dev/screens/app/information_salesman.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_object_karoseri_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_collatelar_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_salesman_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_parent_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_individu_change_notifier/form_m_parent_individu_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';

class MenuObjectInformation extends StatefulWidget {
    final String flag;
    const MenuObjectInformation({this.flag});

    Future<void> setNextState(BuildContext context,int index) async{
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        var _providerUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
        var _providerInfoSales = Provider.of<InformationSalesmanChangeNotifier>(context, listen: false);
        var _providerKaroseri = Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false);
        var _providerColla = Provider.of<InformationCollateralChangeNotifier>(context, listen: false);

        await _providerUnitObject.setDataFromSQLite(context);
        await _providerInfoSales.setDataFromSQLite(context);
        await _providerColla.setDataCollaOtoFromSQLite(context);

        if(_preferences.getString("last_known_state") == "IDE" && index != null){
            if(index != 6){
                _providerUnitObject.flag = true;
                _providerInfoSales.isCheckData = true;
                _providerColla.flag = true;
                if(_preferences.getString("cust_type") == "PER"){
                    Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).isMenuObjectInformationDone = true;
                    Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).selectedIndex += 1;
                } else {
                    Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).isMenuObjectInformationDone = true;
                    Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).selectedIndex += 1;
                }
                await _providerKaroseri.setDataFromSQLite(context, index);
            }
            else{
                if(_preferences.getString("cust_type") == "PER"){
                    Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).selectedIndex = 6;
                } else {
                    Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).selectedIndex = 5;
                }
            }
        }
    }

  @override
  _MenuObjectInformationState createState() => _MenuObjectInformationState();
}

class _MenuObjectInformationState extends State<MenuObjectInformation> {
    @override
    Widget build(BuildContext context) {
        var _providerObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: true);
        var _providerSalesman = Provider.of<InformationSalesmanChangeNotifier>(context, listen: true);
        var _providerCollateral = Provider.of<InformationCollateralChangeNotifier>(context, listen: true);

        return Padding(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width/37,
                vertical: MediaQuery.of(context).size.height/57,
            ),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    InkWell(
                        onTap: () {
                            Navigator.push(context, MaterialPageRoute(
                                    builder: (context) => InformationObjectUnit(flag: widget.flag)
                                )
                            );
                        },
                        child: Card(
                            elevation: 3.3,
                            child: Padding(
                                padding: EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text("Informasi Unit Detail/Objek"),
                                        _providerObjectUnit.flag
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
                    _providerObjectUnit.autoValidate
                        ?
                    Container(
                        margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                        child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
                    )
                        :
                    SizedBox(height: MediaQuery.of(context).size.height / 87),
                    SizedBox(height: MediaQuery.of(context).size.height / 87),
                    InkWell(
                        onTap: () {
                            // _providerObjectUnit.flag ?
                            Navigator.push(context, MaterialPageRoute(
                                    builder: (context) => InformationSalesman())
                            );
                            // : null;
                        },
                        child: Card(
                            // color: _providerObjectUnit.flag ? Colors.white : Colors.black12,
                            elevation: 3.3,
                            child: Padding(
                                padding: EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text("Informasi Sales"),
                                        _providerSalesman.listInfoSales.length != 0 && _providerSalesman.isCheckData
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
                    _providerSalesman.autoValidate
                        ?
                    Container(
                        margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                        child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
                    )
                        :
                    SizedBox(height: MediaQuery.of(context).size.height / 87),
                    SizedBox(height: MediaQuery.of(context).size.height / 87),
                    InkWell(
                        onTap: () {
                            _providerObjectUnit.objectSelected != null ?
                            Navigator.push(context, MaterialPageRoute(
                                    builder: (context) => InformationCollateral(flag: widget.flag))
                            ).then((value) {
                              setState(() {});
                            })
                            : null;
                        },
                        child: Card(
                            color: _providerObjectUnit.objectSelected != null ? Colors.white : Colors.black12,
                            elevation: 3.3,
                            child: Padding(
                                padding: EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text("Informasi Jaminan/Colla"),
                                        _providerCollateral.flag
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
                    _providerCollateral.autoValidateAuto || _providerCollateral.autoValidateProp
                        ?
                    Container(
                        margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                        child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
                    )
                        :
                    SizedBox(height: MediaQuery.of(context).size.height / 87),
                ],
            ),
        );
    }
}
