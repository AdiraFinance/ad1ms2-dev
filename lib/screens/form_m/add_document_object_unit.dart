import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/document_unit_model.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_add_document_object_unit_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../detail_image.dart';

class AddDocumentObjectUnitProvider extends StatefulWidget {
  final int flag;
  final int index;
  final DocumentUnitModel documentUnitModel;

  const AddDocumentObjectUnitProvider(this.flag, this.index, this.documentUnitModel);

  @override
  _AddDocumentObjectUnitProviderState createState() => _AddDocumentObjectUnitProviderState();
}

class _AddDocumentObjectUnitProviderState extends State<AddDocumentObjectUnitProvider> {
  Future<void> _getData;

  @override
  void initState() {
    super.initState();
    _getData = Provider.of<FormMFotoAddDocumentObjectUnit>(context,listen: false).setValueForEditDocument(widget.documentUnitModel,widget.flag, widget.index);
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
        data: ThemeData(
            primaryColor: Colors.black,
            accentColor: myPrimaryColor,
            fontFamily: "NunitoSans",
            primarySwatch: primaryOrange),
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            backgroundColor: myPrimaryColor,
            title: Text(
                widget.flag == 0
                    ? "Tambah Dokumen Unit Object"
                    : "Edit Dokumen Unit Object",
                style: TextStyle(color: Colors.black)),
            centerTitle: true,
            iconTheme: IconThemeData(
              color: Colors.black,
            ),
          ),
          body:
         widget.flag == 0 ?
          FutureBuilder(
                  future: _getData,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                    return Consumer<FormMFotoAddDocumentObjectUnit>(
                      builder: (context, formMFotoAddDocument, _) {
                        return Form(
                          key: formMFotoAddDocument.key,
                          onWillPop: _onWillPop,
                          child: SingleChildScrollView(
                            padding: EdgeInsets.symmetric(
                                horizontal: MediaQuery.of(context).size.width / 27,
                                vertical: MediaQuery.of(context).size.height / 57),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                // DropdownButtonFormField<JenisDocument>(
                                //     autovalidate:
                                //         formMFotoAddDocument.autoValidate,
                                //     validator: (e) {
                                //       if (e == null) {
                                //         return "Silahkan pilih jenis dokumen";
                                //       } else {
                                //         return null;
                                //       }
                                //     },
                                //     value: formMFotoAddDocument
                                //         .jenisDocumentSelected,
                                //     onChanged: (value) {
                                //       formMFotoAddDocument
                                //           .jenisDocumentSelected = value;
                                //     },
                                //     decoration: InputDecoration(
                                //       labelText: "Jenis Dokumen",
                                //       border: OutlineInputBorder(),
                                //       contentPadding:
                                //           EdgeInsets.symmetric(horizontal: 10),
                                //     ),
                                //     items: formMFotoAddDocument
                                //         .listJenisDocument
                                //         .map((value) {
                                //       return DropdownMenuItem<JenisDocument>(
                                //         value: value,
                                //         child: Text(
                                //           value.name,
                                //           overflow: TextOverflow.ellipsis,
                                //         ),
                                //       );
                                //     }).toList()),
                               TextFormField(
                                 controller: formMFotoAddDocument.controllerDocumentType,
                                 style: TextStyle(color: Colors.black),
                                 decoration: InputDecoration(
                                     labelText: 'Jenis Dokumen',
                                     labelStyle: TextStyle(color: Colors.black),
                                     border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
//                                  validator: (e) {
//                                    if (e.isEmpty) {
//                                      return "Tidak boleh kosong";
//                                    } else {
//                                      return null;
//                                    }
//                                  },
//                                  autovalidate:
//                                  formMFotoAddDocument.autoValidate,
                                 onTap: () {
                                   formMFotoAddDocument.searchDocumentType(context,1);
                                 },
                                 readOnly: true,
                               ),
                                SizedBox(height: MediaQuery.of(context).size.height/47),
                                TextFormField(
                                  controller: formMFotoAddDocument.controllerReceiveDocumentDate,
                                  style: TextStyle(color: Colors.black),
                                  decoration: InputDecoration(
                                      labelText: 'Tanggal terima dokumen',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                                  keyboardType: TextInputType.emailAddress,
                                  validator: (e) {
                                    if (e.isEmpty) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  autovalidate: formMFotoAddDocument.autoValidate,
                                  onTap: () {
                                    formMFotoAddDocument.showDatePicker(context);
                                  },
                                  readOnly: true,
                                ),
                                SizedBox(height: MediaQuery.of(context).size.height/47),
                                formMFotoAddDocument.fileDocumentUnitObject == null
                                    ? RaisedButton(
                                        onPressed: () {
                                          formMFotoAddDocument.showBottomSheetChooseFile(context);
                                        },
                                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
                                        child: Row(
                                          mainAxisSize: MainAxisSize.max,
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            Icon(Icons.camera_alt),
                                            SizedBox(width: MediaQuery.of(context).size.width/47),
                                            Text("ADD")
                                          ],
                                        ),
                                        color: myPrimaryColor,
                                      )
                                    : Card(
                                        elevation: 3.3,
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Padding(
                                                padding: EdgeInsets.all(8.0),
                                                child: Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Text("${formMFotoAddDocument.fileDocumentUnitObject['file_name']}"),
                                                    Text("${formMFotoAddDocument.fileDocumentUnitObject['file'].lengthSync() / 1000} kB")
                                                  ],
                                                ),
                                              ),
                                              flex: 8,
                                            ),
                                            Expanded(
                                              flex: 2,
                                              child: IconButton(
                                                  icon: Icon(Icons.delete, color: Colors.red),
                                                  onPressed: () {
                                                    formMFotoAddDocument.fileDocumentUnitObject = null;
                                                    formMFotoAddDocument.deleteFile();
                                                  }),
                                            )
                                          ],
                                        ),
                                      ),
                                formMFotoAddDocument.autoValidateFile
                                    ? Padding(
                                        padding: EdgeInsets.only(left: 12),
                                        child: Text("Tidak boleh kosong",
                                            style: TextStyle(
                                                color: Colors.red,
                                                fontSize: 12)),
                                      )
                                    : SizedBox()
                              ],
                            ),
                          ),
                        );
                      },
                    );
                  })
         :
          FutureBuilder(
             future: _getData,
             builder: (context, snapshot) {
               if (snapshot.connectionState == ConnectionState.waiting) {
                 return Center(
                   child: CircularProgressIndicator(),
                 );
               }
               return Consumer<FormMFotoAddDocumentObjectUnit>(
                 builder:(consumerContext, addDocumentChangeNotifier, _) {
                   return Form(
                     key: addDocumentChangeNotifier.key,
                     child: Column(
                       crossAxisAlignment: CrossAxisAlignment.start,
                       children: [
                         Padding(
                           padding: const EdgeInsets.all(12.0),
                           child: Column(
                             children: [
                               Row(
                                 children: [
                                   Expanded(
                                       child: Text("Tipe Dokumen"),
                                       flex: 5),
                                   Text(" : "),
                                   Expanded(
                                       child: Text("${addDocumentChangeNotifier.documentTypeSelected.docTypeId} - ${addDocumentChangeNotifier.documentTypeSelected.docTypeName}"),
                                       flex: 5)
                                 ],
                               ),
                               SizedBox(
                                   height:
                                   MediaQuery.of(context).size.height /
                                       77),
                               Row(
                                 children: [
                                   Expanded(
                                       child: Text("Tanggal Terima"),
                                       flex: 5),
                                   Text(" : "),
                                   Expanded(
                                       child: Text(addDocumentChangeNotifier.controllerReceiveDocumentDate.text),
                                       flex: 5)
                                 ],
                               ),
                             ],
                           ),
                         ),
                         SizedBox(height: 8),
                         Center(
                           child: Card(
                             elevation: 3.3,
                             child: Padding(
                                 padding: const EdgeInsets.all(8.0),
                                 child: InkWell(
                                   onTap: (){
                                     Navigator.push(context, MaterialPageRoute(builder: (context) => DetailImage(imageFile: addDocumentChangeNotifier.fileDocumentUnitObject['file'])));
                                   },
                                   child: ClipRRect(
                                     child: Image.file(addDocumentChangeNotifier.fileDocumentUnitObject['file'],
                                         width: 250,
                                         height: 250,
                                         fit: BoxFit.cover,
                                         filterQuality: FilterQuality.medium),
                                     borderRadius: BorderRadius.circular(8.0),
                                   ),
                                 )
                             ),
                           ),
                         )
                       ],
                     ),
                   );
                 },
               );
             }),
//              : Consumer<FormMFotoAddDocumentObjectUnit>(
//                  builder: (context, formMFotoAddDocument, _) {
//                    return Form(
//                      key: formMFotoAddDocument.key,
//                      onWillPop: _onWillPop,
//                      child: SingleChildScrollView(
//                        padding: EdgeInsets.symmetric(
//                            horizontal: MediaQuery.of(context).size.width / 27,
//                            vertical: MediaQuery.of(context).size.height / 57),
//                        child: Column(
//                          crossAxisAlignment: CrossAxisAlignment.start,
//                          children: <Widget>[
//                            DropdownButtonFormField<JenisDocument>(
//                                autovalidate: formMFotoAddDocument.autoValidate,
//                                validator: (e) {
//                                  if (e == null) {
//                                    return "Silahkan pilih jenis dokumen";
//                                  } else {
//                                    return null;
//                                  }
//                                },
//                                value:
//                                    formMFotoAddDocument.jenisDocumentSelected,
//                                onChanged: (value) {
//                                  formMFotoAddDocument.jenisDocumentSelected =
//                                      value;
//                                },
//                                decoration: InputDecoration(
//                                  labelText: "Jenis Dokumen",
//                                  border: OutlineInputBorder(),
//                                  contentPadding:
//                                      EdgeInsets.symmetric(horizontal: 10),
//                                ),
//                                items: formMFotoAddDocument.listJenisDocument
//                                    .map((value) {
//                                  return DropdownMenuItem<JenisDocument>(
//                                    value: value,
//                                    child: Text(
//                                      value.name,
//                                      overflow: TextOverflow.ellipsis,
//                                    ),
//                                  );
//                                }).toList()),
//                            SizedBox(
//                                height:
//                                    MediaQuery.of(context).size.height / 47),
//                            FocusScope(
//                              node: FocusScopeNode(),
//                              child: TextFormField(
//                                controller: formMFotoAddDocument
//                                    .controllerReceiveDocumentDate,
//                                style: TextStyle(color: Colors.black),
//                                decoration: InputDecoration(
//                                    labelText: 'Tanggal terima dokumen',
//                                    labelStyle: TextStyle(color: Colors.black),
//                                    border: OutlineInputBorder(
//                                        borderRadius:
//                                            BorderRadius.circular(8))),
//                                keyboardType: TextInputType.emailAddress,
//                                validator: (e) {
//                                  if (e.isEmpty) {
//                                    return "Tidak boleh kosong";
//                                  } else {
//                                    return null;
//                                  }
//                                },
//                                autovalidate: formMFotoAddDocument.autoValidate,
//                                onTap: () {
//                                  formMFotoAddDocument.showDatePicker(context);
//                                },
//                                readOnly: true,
//                              ),
//                            ),
//                            SizedBox(
//                                height:
//                                    MediaQuery.of(context).size.height / 47),
//                            formMFotoAddDocument.fileDocumentUnitObject == null
//                                ? RaisedButton(
//                                    onPressed: () {
//                                      formMFotoAddDocument
//                                          .showBottomSheetChooseFile(context);
//                                    },
//                                    shape: RoundedRectangleBorder(
//                                        borderRadius:
//                                            new BorderRadius.circular(8.0)),
//                                    child: Row(
//                                      mainAxisSize: MainAxisSize.max,
//                                      mainAxisAlignment:
//                                          MainAxisAlignment.center,
//                                      children: <Widget>[
//                                        Icon(Icons.camera_alt),
//                                        SizedBox(
//                                            width: MediaQuery.of(context)
//                                                    .size
//                                                    .width /
//                                                47),
//                                        Text("ADD")
//                                      ],
//                                    ),
//                                    color: myPrimaryColor,
//                                  )
//                                : Card(
//                                    elevation: 3.3,
//                                    child: Row(
//                                      children: <Widget>[
//                                        Expanded(
//                                          child: Padding(
//                                            padding: const EdgeInsets.all(8.0),
//                                            child: Column(
//                                              crossAxisAlignment:
//                                                  CrossAxisAlignment.start,
//                                              children: <Widget>[
//                                                Text(
//                                                    "${formMFotoAddDocument.fileDocumentUnitObject['file_name']}"),
//                                                Text(
//                                                    "${formMFotoAddDocument.fileDocumentUnitObject['file'].lengthSync() / 1000} kB")
//                                              ],
//                                            ),
//                                          ),
//                                          flex: 8,
//                                        ),
//                                        Expanded(
//                                          flex: 2,
//                                          child: IconButton(
//                                              icon: Icon(Icons.delete,
//                                                  color: Colors.red),
//                                              onPressed: () {
//                                                formMFotoAddDocument
//                                                        .fileDocumentUnitObject =
//                                                    null;
//                                              }),
//                                        )
//                                      ],
//                                    ),
//                                  ),
//                            formMFotoAddDocument.autoValidateFile
//                                ? Padding(
//                                    padding: EdgeInsets.only(left: 12),
//                                    child: Text("Tidak boleh kosong",
//                                        style: TextStyle(
//                                            color: Colors.red, fontSize: 12)),
//                                  )
//                                : SizedBox()
//                          ],
//                        ),
//                      ),
//                    );
//                  },
//                ),
          bottomNavigationBar: widget.flag == 0 ? BottomAppBar(
            elevation: 0.0,
            child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Consumer<FormMFotoAddDocumentObjectUnit>(
                  builder: (context, formMAddDocumentObjectUnit, _) {
                    return RaisedButton(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
                        color: myPrimaryColor,
                        onPressed: () {
                          if (widget.flag == 0) {
                            formMAddDocumentObjectUnit.check(context, widget.flag, null);
                          } else {
                            formMAddDocumentObjectUnit.check(context, widget.flag, widget.index);
                          }
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("SAVE",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500,
                                    letterSpacing: 1.25))
                          ],
                        ));
                  },
                )),
          ) : null,
        ));
  }

  Future<bool> _onWillPop() async {
    var _provider = Provider.of<FormMFotoAddDocumentObjectUnit>(context, listen: false);
    if (widget.flag == 0) {
      if (_provider.documentTypeSelected != null ||
          _provider.controllerReceiveDocumentDate.text != "" ||
          _provider.fileDocumentUnitObject != null) {
        return (await showDialog(
              context: context,
              builder: (myContext) => AlertDialog(
                title: new Text('Warning'),
                content: new Text('Keluar dengan simpan perubahan?'),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      _provider.check(context, widget.flag, null);
                      Navigator.pop(context);
                    },
                    child: new Text('Ya', style: TextStyle(color: Colors.grey)),
                  ),
                  new FlatButton(
                    onPressed: () => Navigator.of(context).pop(true),
                    child: new Text('Tidak'),
                  ),
                ],
              ),
            )) ??
            false;
      } else {
        return true;
      }
    } else {
      if (_provider.controllerReceiveDocumentDate.text != _provider.tglTerimaDocumentTemp ||
          _provider.fileDocumentUnitObjectTemp != _provider.fileDocumentUnitObject ||
          _provider.fileDocumentUnitObject['file_name'] != _provider.fileName) {
        return (await showDialog(
              context: context,
              builder: (myContext) => AlertDialog(
                title: new Text('Warning'),
                content: new Text('Keluar dengan simpan perubahan?'),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      _provider.check(context, widget.flag, widget.index);
                      Navigator.pop(context);
                    },
                    child: new Text('Ya', style: TextStyle(color: Colors.grey)),
                  ),
                  new FlatButton(
                    onPressed: () => Navigator.of(context).pop(true),
                    child: new Text('Tidak'),
                  ),
                ],
              ),
            )) ??
            false;
      } else {
        return true;
      }
    }
  }
}
