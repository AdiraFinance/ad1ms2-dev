import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_income_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_occupation_change_notif.dart';
import 'package:ad1ms2_dev/widgets/widget_income_occupation_type_entrepreneur.dart';
import 'package:ad1ms2_dev/widgets/widget_income_occupation_type_professional.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FormMIncome extends StatelessWidget {
  final String flag;

  const FormMIncome({this.flag});
  @override
  Widget build(BuildContext context) {
    String _provider;
    // Provider.of<FormMIncomeChangeNotifier>(context, listen: false).deleteSQLite();
    if(flag == "COM"){
      _provider =
          Provider.of<FormMOccupationChangeNotif>(context, listen: false)
              .occupationSelected
              .KODE;
    }else{
      _provider =
          Provider.of<FormMFotoChangeNotifier>(context, listen: false)
              .occupationSelected
              .KODE;
    }

    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange),
      child: _provider == "05" || _provider == "07"
          ? WidgetIncomeOccupationTypeEntrepreneur()
          : WidgetIncomeOccupationTypeProfessional()
          // : _provider == "08"
          //     ? WidgetIncomeOccupationTypeProfessional()
          //     : WidgetIncomeOccupationTypeOther(),
    );
  }
}
