import 'package:ad1ms2_dev/models/form_m_company_rincian_model.dart';
import 'package:ad1ms2_dev/models/guarantor_idividual_model.dart';
import 'package:ad1ms2_dev/screens/form_m/list_guarantor_address_company.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_company_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_add_guarantor_company_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class AddGuarantorCompany extends StatefulWidget {
  final int flag;
  final int index;
  final GuarantorCompanyModel model;
  const AddGuarantorCompany({this.flag, this.index, this.model});
  @override
  _AddGuarantorCompanyState createState() => _AddGuarantorCompanyState();
}

class _AddGuarantorCompanyState extends State<AddGuarantorCompany> {
  Future<void> _setValueForEdit;

  @override
  void initState() {
    super.initState();
    _setValueForEdit = Provider.of<FormMAddGuarantorCompanyChangeNotifier>(context,listen: false).setValueForEdit(context, widget.model, widget.flag, widget.index);
    Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).getDataFromDashboard(context);
  }

  @override
  Widget build(BuildContext context) {
    var _dataGuarantor = Provider.of<FormMAddGuarantorCompanyChangeNotifier>(context,listen: false);
    return Theme(
      data: ThemeData(
        primaryColor: Colors.black,
        fontFamily: "NunitoSans",
        primarySwatch: primaryOrange,
        accentColor: myPrimaryColor,),
      child: Scaffold(
        key: Provider.of<FormMAddGuarantorCompanyChangeNotifier>(context, listen: false).scaffoldKey,
        appBar: AppBar(
          title: Text(
              widget.flag == 0
                  ? "Tambah Penjamin Kelembagaan"
                  : "Edit Penjamin Kelembagaan",
              style: TextStyle(color: Colors.black)),
          centerTitle: true,
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width / 27,
              vertical: MediaQuery.of(context).size.height / 57),
          child:
          // widget.flag == 0
          //     ? Consumer<FormMAddGuarantorCompanyChangeNotifier>(
          //         builder: (context, formMAdGuarantorCompanyChangeNotif, _) {
          //           return Form(
          //             key: formMAdGuarantorCompanyChangeNotif.key,
          //             onWillPop: _onWillPop,
          //             child: Column(
          //               crossAxisAlignment: CrossAxisAlignment.start,
          //               children: [
          //                 DropdownButtonFormField<TypeInstitutionModel>(
          //                   autovalidate:
          //                       formMAdGuarantorCompanyChangeNotif.autoValidate,
          //                   validator: (e) {
          //                     if (e == null) {
          //                       return "Tidak boleh kosong";
          //                     } else {
          //                       return null;
          //                     }
          //                   },
          //                   value: formMAdGuarantorCompanyChangeNotif
          //                       .typeInstitutionSelected,
          //                   onChanged: (value) {
          //                     formMAdGuarantorCompanyChangeNotif
          //                         .typeInstitutionSelected = value;
          //                   },
          //                   onTap: () {
          //                     FocusManager.instance.primaryFocus.unfocus();
          //                   },
          //                   decoration: InputDecoration(
          //                     labelText: "Jenis Lembaga",
          //                     border: OutlineInputBorder(),
          //                     contentPadding:
          //                         EdgeInsets.symmetric(horizontal: 10),
          //                   ),
          //                   items: formMAdGuarantorCompanyChangeNotif
          //                       .listTypeInstitution
          //                       .map((value) {
          //                     return DropdownMenuItem<TypeInstitutionModel>(
          //                       value: value,
          //                       child: Text(
          //                         value.PARA_NAME,
          //                         overflow: TextOverflow.ellipsis,
          //                       ),
          //                     );
          //                   }).toList(),
          //                 ),
          //                 SizedBox(
          //                     height: MediaQuery.of(context).size.height / 47),
          //                 DropdownButtonFormField<ProfilModel>(
          //                   autovalidate:
          //                       formMAdGuarantorCompanyChangeNotif.autoValidate,
          //                   validator: (e) {
          //                     if (e == null) {
          //                       return "Tidak boleh kosong";
          //                     } else {
          //                       return null;
          //                     }
          //                   },
          //                   value: formMAdGuarantorCompanyChangeNotif
          //                       .profilSelected,
          //                   onChanged: (value) {
          //                     formMAdGuarantorCompanyChangeNotif
          //                         .profilSelected = value;
          //                   },
          //                   onTap: () {
          //                     FocusManager.instance.primaryFocus.unfocus();
          //                   },
          //                   decoration: InputDecoration(
          //                     labelText: "Profil",
          //                     border: OutlineInputBorder(),
          //                     contentPadding:
          //                         EdgeInsets.symmetric(horizontal: 10),
          //                   ),
          //                   items: formMAdGuarantorCompanyChangeNotif.listProfil
          //                       .map((value) {
          //                     return DropdownMenuItem<ProfilModel>(
          //                       value: value,
          //                       child: Text(
          //                         value.text,
          //                         overflow: TextOverflow.ellipsis,
          //                       ),
          //                     );
          //                   }).toList(),
          //                 ),
          //                 SizedBox(
          //                     height: MediaQuery.of(context).size.height / 47),
          //                 TextFormField(
          //                   autovalidate:
          //                       formMAdGuarantorCompanyChangeNotif.autoValidate,
          //                   validator: (e) {
          //                     if (e.isEmpty) {
          //                       return "Tidak boleh kosong";
          //                     } else {
          //                       return null;
          //                     }
          //                   },
          //                   controller: formMAdGuarantorCompanyChangeNotif
          //                       .controllerInstitutionName,
          //                   style: new TextStyle(color: Colors.black),
          //                   decoration: new InputDecoration(
          //                       labelText: 'Nama Lembaga',
          //                       labelStyle: TextStyle(color: Colors.black),
          //                       border: OutlineInputBorder(
          //                           borderRadius: BorderRadius.circular(8))),
          //                   keyboardType: TextInputType.text,
          //                   textCapitalization: TextCapitalization.characters,
          //                 ),
          //                 SizedBox(
          //                     height: MediaQuery.of(context).size.height / 47),
          //                 TextFormField(
          //                   autovalidate:
          //                       formMAdGuarantorCompanyChangeNotif.autoValidate,
          //                   validator: (e) {
          //                     if (e.isEmpty) {
          //                       return "Tidak boleh kosong";
          //                     } else {
          //                       return null;
          //                     }
          //                   },
          //                   inputFormatters: [
          //                     WhitelistingTextInputFormatter.digitsOnly,
          //                     // LengthLimitingTextInputFormatter(10),
          //                   ],
          //                   controller: formMAdGuarantorCompanyChangeNotif
          //                       .controllerNPWP,
          //                   style: new TextStyle(color: Colors.black),
          //                   decoration: new InputDecoration(
          //                       labelText: 'NPWP',
          //                       labelStyle: TextStyle(color: Colors.black),
          //                       border: OutlineInputBorder(
          //                           borderRadius: BorderRadius.circular(8))),
          //                   keyboardType: TextInputType.number,
          //                 ),
          //                 SizedBox(
          //                     height: MediaQuery.of(context).size.height / 47),
          //                 // TextFormField(
          //                 //   autovalidate:
          //                 //       formMAdGuarantorCompanyChangeNotif.autoValidate,
          //                 //   validator: (e) {
          //                 //     if (e.isEmpty) {
          //                 //       return "Tidak boleh kosong";
          //                 //     } else {
          //                 //       return null;
          //                 //     }
          //                 //   },
          //                 //   onTap: () {
          //                 //     Navigator.push(
          //                 //         context,
          //                 //         MaterialPageRoute(
          //                 //             builder: (context) =>
          //                 //                 ListGuarantorAddressCompany()));
          //                 //   },
          //                 //   controller: formMAdGuarantorCompanyChangeNotif
          //                 //       .controllerAddress,
          //                 //   style: TextStyle(color: Colors.black),
          //                 //   decoration: new InputDecoration(
          //                 //       labelText: 'Alamat',
          //                 //       labelStyle: TextStyle(color: Colors.black),
          //                 //       border: OutlineInputBorder(
          //                 //           borderRadius: BorderRadius.circular(8))),
          //                 //   readOnly: true,
          //                 // ),
          //                 // SizedBox(
          //                 //     height: MediaQuery.of(context).size.height / 47),
          //                 // TextFormField(
          //                 //   readOnly: true,
          //                 //   autovalidate:
          //                 //       formMAdGuarantorCompanyChangeNotif.autoValidate,
          //                 //   validator: (e) {
          //                 //     if (e.isEmpty) {
          //                 //       return "Tidak boleh kosong";
          //                 //     } else {
          //                 //       return null;
          //                 //     }
          //                 //   },
          //                 //   controller: formMAdGuarantorCompanyChangeNotif
          //                 //       .controllerAddressType,
          //                 //   style: new TextStyle(color: Colors.black),
          //                 //   decoration: new InputDecoration(
          //                 //       filled: true,
          //                 //       fillColor: Colors.black12,
          //                 //       labelText: 'Jenis Alamat',
          //                 //       labelStyle: TextStyle(color: Colors.black),
          //                 //       border: OutlineInputBorder(
          //                 //           borderRadius: BorderRadius.circular(8))),
          //                 //   enabled: false,
          //                 // ),
          //                 // SizedBox(
          //                 //     height: MediaQuery.of(context).size.height / 47),
          //                 // Row(
          //                 //   children: [
          //                 //     Expanded(
          //                 //       flex: 5,
          //                 //       child: TextFormField(
          //                 //         readOnly: true,
          //                 //         autovalidate:
          //                 //             formMAdGuarantorCompanyChangeNotif
          //                 //                 .autoValidate,
          //                 //         validator: (e) {
          //                 //           if (e.isEmpty) {
          //                 //             return "Tidak boleh kosong";
          //                 //           } else {
          //                 //             return null;
          //                 //           }
          //                 //         },
          //                 //         controller: formMAdGuarantorCompanyChangeNotif
          //                 //             .controllerRT,
          //                 //         style: new TextStyle(color: Colors.black),
          //                 //         decoration: new InputDecoration(
          //                 //             filled: true,
          //                 //             fillColor: Colors.black12,
          //                 //             labelText: 'RT',
          //                 //             labelStyle:
          //                 //                 TextStyle(color: Colors.black),
          //                 //             border: OutlineInputBorder(
          //                 //                 borderRadius:
          //                 //                     BorderRadius.circular(8))),
          //                 //         enabled: false,
          //                 //       ),
          //                 //     ),
          //                 //     SizedBox(width: 8),
          //                 //     Expanded(
          //                 //         flex: 5,
          //                 //         child: TextFormField(
          //                 //           readOnly: true,
          //                 //           autovalidate:
          //                 //               formMAdGuarantorCompanyChangeNotif
          //                 //                   .autoValidate,
          //                 //           validator: (e) {
          //                 //             if (e.isEmpty) {
          //                 //               return "Tidak boleh kosong";
          //                 //             } else {
          //                 //               return null;
          //                 //             }
          //                 //           },
          //                 //           controller:
          //                 //               formMAdGuarantorCompanyChangeNotif
          //                 //                   .controllerRW,
          //                 //           style: new TextStyle(color: Colors.black),
          //                 //           decoration: new InputDecoration(
          //                 //               filled: true,
          //                 //               fillColor: Colors.black12,
          //                 //               labelText: 'RW',
          //                 //               labelStyle:
          //                 //                   TextStyle(color: Colors.black),
          //                 //               border: OutlineInputBorder(
          //                 //                   borderRadius:
          //                 //                       BorderRadius.circular(8))),
          //                 //           enabled: false,
          //                 //         ))
          //                 //   ],
          //                 // ),
          //                 // SizedBox(
          //                 //     height: MediaQuery.of(context).size.height / 47),
          //                 // TextFormField(
          //                 //   readOnly: true,
          //                 //   autovalidate:
          //                 //       formMAdGuarantorCompanyChangeNotif.autoValidate,
          //                 //   validator: (e) {
          //                 //     if (e.isEmpty) {
          //                 //       return "Tidak boleh kosong";
          //                 //     } else {
          //                 //       return null;
          //                 //     }
          //                 //   },
          //                 //   controller: formMAdGuarantorCompanyChangeNotif
          //                 //       .controllerKelurahan,
          //                 //   style: new TextStyle(color: Colors.black),
          //                 //   decoration: new InputDecoration(
          //                 //       filled: true,
          //                 //       fillColor: Colors.black12,
          //                 //       labelText: 'Kelurahan',
          //                 //       labelStyle: TextStyle(color: Colors.black),
          //                 //       border: OutlineInputBorder(
          //                 //           borderRadius: BorderRadius.circular(8))),
          //                 //   enabled: false,
          //                 // ),
          //                 // SizedBox(
          //                 //     height: MediaQuery.of(context).size.height / 47),
          //                 // TextFormField(
          //                 //   readOnly: true,
          //                 //   autovalidate:
          //                 //       formMAdGuarantorCompanyChangeNotif.autoValidate,
          //                 //   validator: (e) {
          //                 //     if (e.isEmpty) {
          //                 //       return "Tidak boleh kosong";
          //                 //     } else {
          //                 //       return null;
          //                 //     }
          //                 //   },
          //                 //   controller: formMAdGuarantorCompanyChangeNotif
          //                 //       .controllerKecamatan,
          //                 //   style: new TextStyle(color: Colors.black),
          //                 //   decoration: new InputDecoration(
          //                 //       filled: true,
          //                 //       fillColor: Colors.black12,
          //                 //       labelText: 'Kecamatan',
          //                 //       labelStyle: TextStyle(color: Colors.black),
          //                 //       border: OutlineInputBorder(
          //                 //           borderRadius: BorderRadius.circular(8))),
          //                 //   enabled: false,
          //                 // ),
          //                 // SizedBox(
          //                 //     height: MediaQuery.of(context).size.height / 47),
          //                 // TextFormField(
          //                 //   readOnly: true,
          //                 //   autovalidate:
          //                 //       formMAdGuarantorCompanyChangeNotif.autoValidate,
          //                 //   validator: (e) {
          //                 //     if (e.isEmpty) {
          //                 //       return "Tidak boleh kosong";
          //                 //     } else {
          //                 //       return null;
          //                 //     }
          //                 //   },
          //                 //   controller: formMAdGuarantorCompanyChangeNotif
          //                 //       .controllerKota,
          //                 //   style: TextStyle(color: Colors.black),
          //                 //   decoration: InputDecoration(
          //                 //       filled: true,
          //                 //       fillColor: Colors.black12,
          //                 //       labelText: 'Kabupaten/Kota',
          //                 //       labelStyle: TextStyle(color: Colors.black),
          //                 //       border: OutlineInputBorder(
          //                 //           borderRadius: BorderRadius.circular(8))),
          //                 //   enabled: false,
          //                 // ),
          //                 // SizedBox(
          //                 //     height: MediaQuery.of(context).size.height / 47),
          //                 // Row(
          //                 //   children: [
          //                 //     Expanded(
          //                 //       flex: 7,
          //                 //       child: TextFormField(
          //                 //         readOnly: true,
          //                 //         autovalidate:
          //                 //             formMAdGuarantorCompanyChangeNotif
          //                 //                 .autoValidate,
          //                 //         validator: (e) {
          //                 //           if (e.isEmpty) {
          //                 //             return "Tidak boleh kosong";
          //                 //           } else {
          //                 //             return null;
          //                 //           }
          //                 //         },
          //                 //         controller: formMAdGuarantorCompanyChangeNotif
          //                 //             .controllerProvinsi,
          //                 //         style: new TextStyle(color: Colors.black),
          //                 //         decoration: new InputDecoration(
          //                 //             filled: true,
          //                 //             fillColor: Colors.black12,
          //                 //             labelText: 'Provinsi',
          //                 //             labelStyle:
          //                 //                 TextStyle(color: Colors.black),
          //                 //             border: OutlineInputBorder(
          //                 //                 borderRadius:
          //                 //                     BorderRadius.circular(8))),
          //                 //         enabled: false,
          //                 //       ),
          //                 //     ),
          //                 //     SizedBox(width: 8),
          //                 //     Expanded(
          //                 //       flex: 3,
          //                 //       child: TextFormField(
          //                 //         readOnly: true,
          //                 //         autovalidate:
          //                 //             formMAdGuarantorCompanyChangeNotif
          //                 //                 .autoValidate,
          //                 //         validator: (e) {
          //                 //           if (e.isEmpty) {
          //                 //             return "Tidak boleh kosong";
          //                 //           } else {
          //                 //             return null;
          //                 //           }
          //                 //         },
          //                 //         controller: formMAdGuarantorCompanyChangeNotif
          //                 //             .controllerPostalCode,
          //                 //         style: new TextStyle(color: Colors.black),
          //                 //         decoration: new InputDecoration(
          //                 //             filled: true,
          //                 //             fillColor: Colors.black12,
          //                 //             labelText: 'Kode Pos',
          //                 //             labelStyle:
          //                 //                 TextStyle(color: Colors.black),
          //                 //             border: OutlineInputBorder(
          //                 //                 borderRadius:
          //                 //                     BorderRadius.circular(8))),
          //                 //         enabled: false,
          //                 //       ),
          //                 //     ),
          //                 //   ],
          //                 // ),
          //                 // SizedBox(
          //                 //     height: MediaQuery.of(context).size.height / 47),
          //                 // Row(
          //                 //   children: [
          //                 //     Expanded(
          //                 //       flex: 4,
          //                 //       child: TextFormField(
          //                 //         readOnly: true,
          //                 //         autovalidate:
          //                 //             formMAdGuarantorCompanyChangeNotif
          //                 //                 .autoValidate,
          //                 //         validator: (e) {
          //                 //           if (e.isEmpty) {
          //                 //             return "Tidak boleh kosong";
          //                 //           } else {
          //                 //             return null;
          //                 //           }
          //                 //         },
          //                 //         keyboardType: TextInputType.number,
          //                 //         controller: formMAdGuarantorCompanyChangeNotif
          //                 //             .controllerTelephoneArea1,
          //                 //         style: new TextStyle(color: Colors.black),
          //                 //         decoration: new InputDecoration(
          //                 //             filled: true,
          //                 //             fillColor: Colors.black12,
          //                 //             labelText: 'Telepon 1 (Area)',
          //                 //             labelStyle:
          //                 //                 TextStyle(color: Colors.black),
          //                 //             border: OutlineInputBorder(
          //                 //                 borderRadius:
          //                 //                     BorderRadius.circular(8))),
          //                 //         enabled: false,
          //                 //       ),
          //                 //     ),
          //                 //     SizedBox(
          //                 //         width:
          //                 //             MediaQuery.of(context).size.width / 37),
          //                 //     Expanded(
          //                 //       flex: 6,
          //                 //       child: TextFormField(
          //                 //         readOnly: true,
          //                 //         autovalidate:
          //                 //             formMAdGuarantorCompanyChangeNotif
          //                 //                 .autoValidate,
          //                 //         validator: (e) {
          //                 //           if (e.isEmpty) {
          //                 //             return "Tidak boleh kosong";
          //                 //           } else {
          //                 //             return null;
          //                 //           }
          //                 //         },
          //                 //         keyboardType: TextInputType.number,
          //                 //         inputFormatters: [
          //                 //           WhitelistingTextInputFormatter.digitsOnly,
          //                 //           LengthLimitingTextInputFormatter(10),
          //                 //         ],
          //                 //         controller: formMAdGuarantorCompanyChangeNotif
          //                 //             .controllerTelephone1,
          //                 //         style: new TextStyle(color: Colors.black),
          //                 //         decoration: new InputDecoration(
          //                 //             filled: true,
          //                 //             fillColor: Colors.black12,
          //                 //             labelText: 'Telepon 1',
          //                 //             labelStyle:
          //                 //                 TextStyle(color: Colors.black),
          //                 //             border: OutlineInputBorder(
          //                 //                 borderRadius:
          //                 //                     BorderRadius.circular(8))),
          //                 //         enabled: false,
          //                 //       ),
          //                 //     ),
          //                 //   ],
          //                 // ),
          //                 // SizedBox(
          //                 //     height: MediaQuery.of(context).size.height / 47),
          //                 // Row(
          //                 //   children: [
          //                 //     Expanded(
          //                 //       flex: 4,
          //                 //       child: TextFormField(
          //                 //         readOnly: true,
          //                 //         autovalidate:
          //                 //             formMAdGuarantorCompanyChangeNotif
          //                 //                 .autoValidate,
          //                 //         validator: (e) {
          //                 //           if (e.isEmpty) {
          //                 //             return "Tidak boleh kosong";
          //                 //           } else {
          //                 //             return null;
          //                 //           }
          //                 //         },
          //                 //         keyboardType: TextInputType.number,
          //                 //         controller: formMAdGuarantorCompanyChangeNotif
          //                 //             .controllerTelephoneArea2,
          //                 //         style: new TextStyle(color: Colors.black),
          //                 //         decoration: new InputDecoration(
          //                 //             filled: true,
          //                 //             fillColor: Colors.black12,
          //                 //             labelText: 'Telepon 2 (Area)',
          //                 //             labelStyle:
          //                 //                 TextStyle(color: Colors.black),
          //                 //             border: OutlineInputBorder(
          //                 //                 borderRadius:
          //                 //                     BorderRadius.circular(8))),
          //                 //         enabled: false,
          //                 //       ),
          //                 //     ),
          //                 //     SizedBox(
          //                 //         width:
          //                 //             MediaQuery.of(context).size.width / 37),
          //                 //     Expanded(
          //                 //       flex: 6,
          //                 //       child: TextFormField(
          //                 //         readOnly: true,
          //                 //         autovalidate:
          //                 //             formMAdGuarantorCompanyChangeNotif
          //                 //                 .autoValidate,
          //                 //         validator: (e) {
          //                 //           if (e.isEmpty) {
          //                 //             return "Tidak boleh kosong";
          //                 //           } else {
          //                 //             return null;
          //                 //           }
          //                 //         },
          //                 //         keyboardType: TextInputType.number,
          //                 //         inputFormatters: [
          //                 //           WhitelistingTextInputFormatter.digitsOnly,
          //                 //           LengthLimitingTextInputFormatter(10),
          //                 //         ],
          //                 //         controller: formMAdGuarantorCompanyChangeNotif
          //                 //             .controllerTelephone2,
          //                 //         style: new TextStyle(color: Colors.black),
          //                 //         decoration: new InputDecoration(
          //                 //             filled: true,
          //                 //             fillColor: Colors.black12,
          //                 //             labelText: 'Telepon 2',
          //                 //             labelStyle:
          //                 //                 TextStyle(color: Colors.black),
          //                 //             border: OutlineInputBorder(
          //                 //                 borderRadius:
          //                 //                     BorderRadius.circular(8))),
          //                 //         enabled: false,
          //                 //       ),
          //                 //     ),
          //                 //   ],
          //                 // ),
          //                 // SizedBox(
          //                 //     height: MediaQuery.of(context).size.height / 47),
          //                 // Row(
          //                 //   children: [
          //                 //     Expanded(
          //                 //       flex: 4,
          //                 //       child: TextFormField(
          //                 //         readOnly: true,
          //                 //         autovalidate:
          //                 //             formMAdGuarantorCompanyChangeNotif
          //                 //                 .autoValidate,
          //                 //         validator: (e) {
          //                 //           if (e.isEmpty) {
          //                 //             return "Tidak boleh kosong";
          //                 //           } else {
          //                 //             return null;
          //                 //           }
          //                 //         },
          //                 //         keyboardType: TextInputType.number,
          //                 //         controller: formMAdGuarantorCompanyChangeNotif
          //                 //             .controllerFaxArea,
          //                 //         style: new TextStyle(color: Colors.black),
          //                 //         decoration: new InputDecoration(
          //                 //             filled: true,
          //                 //             fillColor: Colors.black12,
          //                 //             labelText: 'Fax (Area)',
          //                 //             labelStyle:
          //                 //                 TextStyle(color: Colors.black),
          //                 //             border: OutlineInputBorder(
          //                 //                 borderRadius:
          //                 //                     BorderRadius.circular(8))),
          //                 //         enabled: false,
          //                 //       ),
          //                 //     ),
          //                 //     SizedBox(
          //                 //         width:
          //                 //             MediaQuery.of(context).size.width / 37),
          //                 //     Expanded(
          //                 //       flex: 6,
          //                 //       child: TextFormField(
          //                 //         readOnly: true,
          //                 //         autovalidate:
          //                 //             formMAdGuarantorCompanyChangeNotif
          //                 //                 .autoValidate,
          //                 //         validator: (e) {
          //                 //           if (e.isEmpty) {
          //                 //             return "Tidak boleh kosong";
          //                 //           } else {
          //                 //             return null;
          //                 //           }
          //                 //         },
          //                 //         keyboardType: TextInputType.number,
          //                 //         inputFormatters: [
          //                 //           WhitelistingTextInputFormatter.digitsOnly,
          //                 //           LengthLimitingTextInputFormatter(10),
          //                 //         ],
          //                 //         controller: formMAdGuarantorCompanyChangeNotif
          //                 //             .controllerFax,
          //                 //         style: new TextStyle(color: Colors.black),
          //                 //         decoration: new InputDecoration(
          //                 //             filled: true,
          //                 //             fillColor: Colors.black12,
          //                 //             labelText: 'Fax',
          //                 //             labelStyle:
          //                 //                 TextStyle(color: Colors.black),
          //                 //             border: OutlineInputBorder(
          //                 //                 borderRadius:
          //                 //                     BorderRadius.circular(8))),
          //                 //         enabled: false,
          //                 //       ),
          //                 //     ),
          //                 //   ],
          //                 // ),
          //               ],
          //             ),
          //           );
          //         },
          //       )
          //     :
          FutureBuilder(
                  future: _setValueForEdit,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                    return Consumer<FormMAddGuarantorCompanyChangeNotifier>(
                      builder: (context, formMAdGuarantorCompanyChangeNotif, _) {
                        return Form(
                          key: formMAdGuarantorCompanyChangeNotif.key,
                          onWillPop: _onWillPop,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Visibility(
                                visible: _dataGuarantor.isCompanyTypeVisible(),
                                child: DropdownButtonFormField<TypeInstitutionModel>(
                                  autovalidate: formMAdGuarantorCompanyChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e == null && _dataGuarantor.isCompanyTypeMandatory()) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  value: formMAdGuarantorCompanyChangeNotif.typeInstitutionSelected,
                                  onChanged: (value) {
                                    formMAdGuarantorCompanyChangeNotif.typeInstitutionSelected = value;
                                  },
                                  onTap: () {
                                    FocusManager.instance.primaryFocus.unfocus();
                                  },
                                  decoration: InputDecoration(
                                    labelText: "Jenis Lembaga",
                                    border: OutlineInputBorder(),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: formMAdGuarantorCompanyChangeNotif.companyTypeDakor ? Colors.purple : Colors.grey)),
                                    disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: formMAdGuarantorCompanyChangeNotif.companyTypeDakor ? Colors.purple : Colors.grey)),
                                    contentPadding:
                                        EdgeInsets.symmetric(horizontal: 10),
                                  ),
                                  items: formMAdGuarantorCompanyChangeNotif.listTypeInstitution.map((value) {
                                    return DropdownMenuItem<TypeInstitutionModel>(
                                      value: value,
                                      child: Text(
                                        value.PARA_NAME,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    );
                                  }).toList(),
                                ),
                              ),
                              Visibility(visible: _dataGuarantor.isCompanyTypeVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              // DropdownButtonFormField<ProfilModel>(
                              //   autovalidate: formMAdGuarantorCompanyChangeNotif
                              //       .autoValidate,
                              //   validator: (e) {
                              //     if (e == null) {
                              //       return "Tidak boleh kosong";
                              //     } else {
                              //       return null;
                              //     }
                              //   },
                              //   value: formMAdGuarantorCompanyChangeNotif
                              //       .profilSelected,
                              //   onChanged: (value) {
                              //     formMAdGuarantorCompanyChangeNotif
                              //         .profilSelected = value;
                              //   },
                              //   onTap: () {
                              //     FocusManager.instance.primaryFocus.unfocus();
                              //   },
                              //   decoration: InputDecoration(
                              //     labelText: "Profil",
                              //     border: OutlineInputBorder(),
                              //     contentPadding:
                              //         EdgeInsets.symmetric(horizontal: 10),
                              //   ),
                              //   items: formMAdGuarantorCompanyChangeNotif
                              //       .listProfil
                              //       .map((value) {
                              //     return DropdownMenuItem<ProfilModel>(
                              //       value: value,
                              //       child: Text(
                              //         value.text,
                              //         overflow: TextOverflow.ellipsis,
                              //       ),
                              //     );
                              //   }).toList(),
                              // ),
                              // SizedBox(
                              //     height:
                              //         MediaQuery.of(context).size.height / 47),
                              Visibility(
                                visible: _dataGuarantor.isCompanyNameVisible(),
                                child: TextFormField(
                                  autovalidate: formMAdGuarantorCompanyChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && _dataGuarantor.isCompanyNameMandatory()) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  controller: formMAdGuarantorCompanyChangeNotif.controllerInstitutionName,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      labelText: 'Nama Lembaga',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius:BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMAdGuarantorCompanyChangeNotif.companyNameDakor ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMAdGuarantorCompanyChangeNotif.companyNameDakor ? Colors.purple : Colors.grey)),
                                  ),
                                  keyboardType: TextInputType.text,
                                  textCapitalization: TextCapitalization.characters,
                                  inputFormatters: [
                                    FilteringTextInputFormatter.allow(RegExp('[a-zA-Z  \`0-9]')),
                                  ],
                                ),
                              ),
                              Visibility(visible: _dataGuarantor.isCompanyNameVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              Visibility(
                                visible: _dataGuarantor.isEstablishedDateVisible(),
                                child: TextFormField(
                                    validator: (e) {
                                      if (e.isEmpty && _dataGuarantor.isEstablishedDateMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    style: new TextStyle(color: Colors.black),
                                    controller: formMAdGuarantorCompanyChangeNotif.controllerEstablishDate,
                                    autovalidate: formMAdGuarantorCompanyChangeNotif.autoValidate,
                                    readOnly: true,
                                    onTap: () {
                                      FocusManager.instance.primaryFocus.unfocus();
                                      formMAdGuarantorCompanyChangeNotif.selectEstablishDate(context);
                                    },
                                    decoration: new InputDecoration(
                                        labelText: 'Tanggal Pendirian',
                                        labelStyle: TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8)),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: formMAdGuarantorCompanyChangeNotif.establishedDateDakor ? Colors.purple : Colors.grey)),
                                        disabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: formMAdGuarantorCompanyChangeNotif.establishedDateDakor ? Colors.purple : Colors.grey)),
                                    )
                                ),
                              ),
                              Visibility(visible: _dataGuarantor.isEstablishedDateVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              Visibility(
                                visible: _dataGuarantor.isNpwpVisible(),
                                child: TextFormField(
                                  autovalidate: formMAdGuarantorCompanyChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && _dataGuarantor.isNpwpMandatory()) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  onTap: (){
                                    _dataGuarantor.validateNameOrNPWP(context, "NPWP");
                                  },
                                  onFieldSubmitted: (val){
                                    _dataGuarantor.validateNameOrNPWP(context, "NPWP");
                                  },
                                  controller: formMAdGuarantorCompanyChangeNotif.controllerNPWP,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      labelText: 'NPWP',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius:BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMAdGuarantorCompanyChangeNotif.npwpDakor ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMAdGuarantorCompanyChangeNotif.npwpDakor ? Colors.purple : Colors.grey)),
                                  ),
                                  inputFormatters: [
                                    WhitelistingTextInputFormatter.digitsOnly,
                                    FilteringTextInputFormatter.allow(RegExp('[0-9]')),
                                    LengthLimitingTextInputFormatter(15)
                                  ],
                                  keyboardType: TextInputType.number,
                                ),
                              ),
                              Visibility(visible: _dataGuarantor.isNpwpVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              Visibility(
                                visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setAddressShow(1),
                                child: TextFormField(
                                  autovalidate: formMAdGuarantorCompanyChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setAddressMandatory(1)) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => ListGuarantorAddressCompany()));
                                  },
                                  controller: formMAdGuarantorCompanyChangeNotif.controllerAddress,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      labelText: 'Alamat',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8))),
                                  readOnly: true,
                                ),
                              ),
                              Visibility(visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setAddressShow(1),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              Visibility(
                                visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setAddressShow(1),
                                child: TextFormField(
                                  autovalidate: formMAdGuarantorCompanyChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setAddressTypeMandatory(1)) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  controller: formMAdGuarantorCompanyChangeNotif.controllerAddressType,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      filled: true,
                                      fillColor: Colors.black12,
                                      labelText: 'Jenis Alamat',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8))),
                                  enabled: false,
                                ),
                              ),
                              Visibility(visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setAddressTypeShow(1),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              Visibility(
                                visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setRTShow(1) || Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setRWShow(1),
                                child: Row(
                                  children: [
                                    Visibility(
                                      visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setRTShow(1),
                                      child: Expanded(
                                        flex: 5,
                                        child: TextFormField(
                                          autovalidate: formMAdGuarantorCompanyChangeNotif.autoValidate,
                                          validator: (e) {
                                            if (e.isEmpty && Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setRTMandatory(1)) {
                                              return "Tidak boleh kosong";
                                            } else {
                                              return null;
                                            }
                                          },
                                          controller: formMAdGuarantorCompanyChangeNotif.controllerRT,
                                          style: new TextStyle(color: Colors.black),
                                          decoration: new InputDecoration(
                                              filled: true,
                                              fillColor: Colors.black12,
                                              labelText: 'RT',
                                              labelStyle: TextStyle(color: Colors.black),
                                              border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(8))),
                                          enabled: false,
                                        ),
                                      ),
                                    ),
                                    Visibility(visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setRTShow(1),child: SizedBox(width: 8)),
                                    Visibility(
                                      visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setRWShow(1),
                                      child: Expanded(
                                          flex: 5,
                                          child: TextFormField(
                                            autovalidate: formMAdGuarantorCompanyChangeNotif.autoValidate,
                                            validator: (e) {
                                              if (e.isEmpty && Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setRWMandatory(1)) {
                                                return "Tidak boleh kosong";
                                              } else {
                                                return null;
                                              }
                                            },
                                            controller: formMAdGuarantorCompanyChangeNotif.controllerRW,
                                            style: new TextStyle(color: Colors.black),
                                            decoration: new InputDecoration(
                                                filled: true,
                                                fillColor: Colors.black12,
                                                labelText: 'RW',
                                                labelStyle: TextStyle(color: Colors.black),
                                                border: OutlineInputBorder(
                                                    borderRadius: BorderRadius.circular(8))),
                                            enabled: false,
                                          )),
                                    )
                                  ],
                                ),
                              ),
                              Visibility(visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setRTShow(1) || Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setRWShow(1), child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              Visibility(
                                visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setKelurahanShow(1),
                                child: FocusScope(
                                  node: FocusScopeNode(),
                                  child: TextFormField(
                                    autovalidate: formMAdGuarantorCompanyChangeNotif.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty && Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setKelurahanMandatory(1)) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    controller: formMAdGuarantorCompanyChangeNotif.controllerKelurahan,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        filled: true,
                                        fillColor: Colors.black12,
                                        labelText: 'Kelurahan',
                                        labelStyle: TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8))),
                                    enabled: false,
                                  ),
                                ),
                              ),
                              Visibility(visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setKelurahanShow(1), child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              Visibility(
                                visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setKecamatanShow(1),
                                child: TextFormField(
                                  autovalidate: formMAdGuarantorCompanyChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setKecamatanMandatory(1)) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  controller: formMAdGuarantorCompanyChangeNotif.controllerKecamatan,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      filled: true,
                                      fillColor: Colors.black12,
                                      labelText: 'Kecamatan',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8))),
                                  enabled: false,
                                ),
                              ),
                              Visibility(visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setKecamatanShow(1),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              Visibility(
                                visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setKabKotShow(1),
                                child: TextFormField(
                                  autovalidate: formMAdGuarantorCompanyChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setKabKotMandatory(1)) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  controller: formMAdGuarantorCompanyChangeNotif.controllerKota,
                                  style: TextStyle(color: Colors.black),
                                  decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.black12,
                                      labelText: 'Kabupaten/Kota',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8))),
                                  enabled: false,
                                ),
                              ),
                              Visibility(visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setKabKotShow(1),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              Visibility(
                                visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setProvinsiShow(1) || Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setKodePosShow(1),
                                child: Row(
                                  children: [
                                    Visibility(
                                      visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setProvinsiShow(1),
                                      child: Expanded(
                                        flex: 7,
                                        child: TextFormField(
                                          autovalidate: formMAdGuarantorCompanyChangeNotif.autoValidate,
                                          validator: (e) {
                                            if (e.isEmpty && Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setProvinsiMandatory(1)) {
                                              return "Tidak boleh kosong";
                                            } else {
                                              return null;
                                            }
                                          },
                                          controller: formMAdGuarantorCompanyChangeNotif.controllerProvinsi,
                                          style: new TextStyle(color: Colors.black),
                                          decoration: new InputDecoration(
                                              filled: true,
                                              fillColor: Colors.black12,
                                              labelText: 'Provinsi',
                                              labelStyle: TextStyle(color: Colors.black),
                                              border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(8))),
                                          enabled: false,
                                        ),
                                      ),
                                    ),
                                    Visibility(visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setProvinsiShow(1),child: SizedBox(width: 8)),
                                    Visibility(
                                      visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setKodePosShow(1),
                                      child: Expanded(
                                        flex: 3,
                                        child: TextFormField(
                                          autovalidate: formMAdGuarantorCompanyChangeNotif.autoValidate,
                                          validator: (e) {
                                            if (e.isEmpty && Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setKodePosMandatory(1)) {
                                              return "Tidak boleh kosong";
                                            } else {
                                              return null;
                                            }
                                          },
                                          controller:
                                          formMAdGuarantorCompanyChangeNotif.controllerPostalCode,
                                          style: new TextStyle(color: Colors.black),
                                          decoration: new InputDecoration(
                                              filled: true,
                                              fillColor: Colors.black12,
                                              labelText: 'Kode Pos',
                                              labelStyle: TextStyle(color: Colors.black),
                                              border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(8))),
                                          enabled: false,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Visibility(visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setProvinsiShow(1) || Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setKodePosShow(1), child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              Visibility(
                                visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setTelp1AreaShow(1) || Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setTelp1Show(1),
                                child: Row(
                                  children: [
                                    Visibility(
                                      visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setTelp1AreaShow(1),
                                      child: Expanded(
                                        flex: 4,
                                        child: TextFormField(
                                          autovalidate: formMAdGuarantorCompanyChangeNotif.autoValidate,
                                          validator: (e) {
                                            if (e.isEmpty && Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setTelp1AreaMandatory(1)) {
                                              return "Tidak boleh kosong";
                                            } else {
                                              return null;
                                            }
                                          },
                                          keyboardType: TextInputType.number,
                                          inputFormatters: [
                                            WhitelistingTextInputFormatter.digitsOnly,
                                            LengthLimitingTextInputFormatter(10),
                                          ],
                                          controller: formMAdGuarantorCompanyChangeNotif.controllerTelephoneArea1,
                                          style: new TextStyle(color: Colors.black),
                                          decoration: new InputDecoration(
                                              filled: true,
                                              fillColor: Colors.black12,
                                              labelText: 'Telepon 1 (Area)',
                                              labelStyle: TextStyle(color: Colors.black),
                                              border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(8))),
                                          enabled: false,
                                        ),
                                      ),
                                    ),
                                    Visibility(visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setTelp1AreaShow(1),child: SizedBox(width: MediaQuery.of(context).size.width / 37)),
                                    Visibility(
                                      visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setTelp1Show(1),
                                      child: Expanded(
                                        flex: 6,
                                        child: TextFormField(
                                          autovalidate: formMAdGuarantorCompanyChangeNotif.autoValidate,
                                          validator: (e) {
                                            if (e.isEmpty && Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setTelp1Mandatory(1)) {
                                              return "Tidak boleh kosong";
                                            } else {
                                              return null;
                                            }
                                          },
                                          keyboardType: TextInputType.number,
                                          inputFormatters: [
                                            WhitelistingTextInputFormatter.digitsOnly,
                                            LengthLimitingTextInputFormatter(10),
                                          ],
                                          controller: formMAdGuarantorCompanyChangeNotif.controllerTelephone1,
                                          style: new TextStyle(color: Colors.black),
                                          decoration: new InputDecoration(
                                              filled: true,
                                              fillColor: Colors.black12,
                                              labelText: 'Telepon 1',
                                              labelStyle: TextStyle(color: Colors.black),
                                              border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(8))),
                                          enabled: false,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Visibility(visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setTelp1AreaShow(1) || Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setTelp1Show(1),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              Visibility(
                                visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setTelp2AreaShow(1) || Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setTelp2Show(1),
                                child: Row(
                                  children: [
                                    Visibility(
                                      visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setTelp2AreaShow(1),
                                      child: Expanded(
                                        flex: 4,
                                        child: TextFormField(
                                          autovalidate: formMAdGuarantorCompanyChangeNotif.autoValidate,
                                          validator: (e) {
                                            if (e.isEmpty && Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setTelp2AreaMandatory(1)) {
                                              return "Tidak boleh kosong";
                                            } else {
                                              return null;
                                            }
                                          },
                                          keyboardType: TextInputType.number,
                                          inputFormatters: [
                                            WhitelistingTextInputFormatter.digitsOnly,
                                            LengthLimitingTextInputFormatter(10),
                                          ],
                                          controller: formMAdGuarantorCompanyChangeNotif.controllerTelephoneArea2,
                                          style: new TextStyle(color: Colors.black),
                                          decoration: new InputDecoration(
                                              filled: true,
                                              fillColor: Colors.black12,
                                              labelText: 'Telepon 2 (Area)',
                                              labelStyle: TextStyle(color: Colors.black),
                                              border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(8))),
                                          enabled: false,
                                        ),
                                      ),
                                    ),
                                    Visibility(visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setTelp2AreaShow(1),child: SizedBox(width: MediaQuery.of(context).size.width / 37)),
                                    Visibility(
                                      visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setTelp2Show(1),
                                      child: Expanded(
                                        flex: 6,
                                        child: TextFormField(
                                          autovalidate: formMAdGuarantorCompanyChangeNotif.autoValidate,
                                          validator: (e) {
                                            if (e.isEmpty && Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setTelp2Mandatory(1)) {
                                              return "Tidak boleh kosong";
                                            } else {
                                              return null;
                                            }
                                          },
                                          keyboardType: TextInputType.number,
                                          inputFormatters: [
                                            WhitelistingTextInputFormatter.digitsOnly,
                                            LengthLimitingTextInputFormatter(10),
                                          ],
                                          controller: formMAdGuarantorCompanyChangeNotif.controllerTelephone2,
                                          style: new TextStyle(color: Colors.black),
                                          decoration: new InputDecoration(
                                              filled: true,
                                              fillColor: Colors.black12,
                                              labelText: 'Telepon 2',
                                              labelStyle: TextStyle(color: Colors.black),
                                              border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(8))),
                                          enabled: false,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Visibility(visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setTelp2AreaShow(1) || Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setTelp2Show(1), child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              Visibility(
                                visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setFaxAreaShow(1) || Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setFaxShow(1),
                                child: Row(
                                  children: [
                                    Visibility(
                                      visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setFaxAreaShow(1),
                                      child: Expanded(
                                        flex: 4,
                                        child: TextFormField(
                                          autovalidate: formMAdGuarantorCompanyChangeNotif.autoValidate,
                                          validator: (e) {
                                            if (e.isEmpty && Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setFaxAreaMandatory(1)) {
                                              return "Tidak boleh kosong";
                                            } else {
                                              return null;
                                            }
                                          },
                                          keyboardType: TextInputType.number,
                                          inputFormatters: [
                                            WhitelistingTextInputFormatter.digitsOnly,
                                            LengthLimitingTextInputFormatter(10),
                                          ],
                                          controller: formMAdGuarantorCompanyChangeNotif.controllerFaxArea,
                                          style: new TextStyle(color: Colors.black),
                                          decoration: new InputDecoration(
                                              filled: true,
                                              fillColor: Colors.black12,
                                              labelText: 'Fax (Area)',
                                              labelStyle: TextStyle(color: Colors.black),
                                              border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(8))),
                                          enabled: false,
                                        ),
                                      ),
                                    ),
                                    Visibility(visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setFaxAreaShow(1), child: SizedBox(width: MediaQuery.of(context).size.width / 37)),
                                    Visibility(
                                      visible: Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setFaxShow(1),
                                      child: Expanded(
                                        flex: 6,
                                        child: TextFormField(
                                          autovalidate: formMAdGuarantorCompanyChangeNotif.autoValidate,
                                          validator: (e) {
                                            if (e.isEmpty && Provider.of<FormMAddAddressCompanyChangeNotifier>(context,listen: false).setFaxMandatory(1)) {
                                              return "Tidak boleh kosong";
                                            } else {
                                              return null;
                                            }
                                          },
                                          keyboardType: TextInputType.number,
                                          inputFormatters: [
                                            WhitelistingTextInputFormatter.digitsOnly,
                                            LengthLimitingTextInputFormatter(10),
                                          ],
                                          controller: formMAdGuarantorCompanyChangeNotif.controllerFax,
                                          style: new TextStyle(color: Colors.black),
                                          decoration: new InputDecoration(
                                              filled: true,
                                              fillColor: Colors.black12,
                                              labelText: 'Fax',
                                              labelStyle: TextStyle(color: Colors.black),
                                              border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(8))),
                                          enabled: false,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    );
                  }),
        ),
        bottomNavigationBar: BottomAppBar(
          elevation: 0.0,
          child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(8.0)),
                  color: myPrimaryColor,
                  onPressed: () {
                     Provider.of<FormMAddGuarantorCompanyChangeNotifier>(context,listen: false).check(context, widget.flag, widget.index);
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(widget.flag == 0 ? "SAVE" : "UPDATE",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 1.25))
                    ],
                  ))),
        ),
      ),
    );
  }

  Future<bool> _onWillPop() async {
    var _provider = Provider.of<FormMAddGuarantorCompanyChangeNotifier>(context, listen: false);
    if (widget.flag == 0) {
      if (_provider.typeInstitutionSelected != null ||
          _provider.listGuarantorAddress.isNotEmpty ||
          _provider.controllerEstablishDate.text != "" ||
          // _provider.profilSelected != null ||
          _provider.controllerInstitutionName.text != "" ||
          _provider.controllerNPWP.text != "") {
        return (await showDialog(
              context: context,
              builder: (myContext) => AlertDialog(
                title: new Text('Warning'),
                content: new Text('Simpan perubahan?'),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      _provider.check(context, widget.flag, null);
                      // Navigator.pop(context, true);
                    },
                    child: new Text('Ya', style: TextStyle(fontSize: 12, color: Colors.grey),),
                  ),
                  new FlatButton(
                    onPressed: () {
                      _provider.autoValidate = false;
                      Navigator.of(context).pop(true);
                    },
                    child: new Text('Tidak'),
                  ),
                ],
              ),
            )) ??
            false;
      } else {
        return true;
      }
    }
    else {
      if (_provider.typeInstitutionSelected.PARA_ID != _provider.typeInstitutionTemp.PARA_ID ||
          // _provider.profilSelected.id != _provider.profilTemp.id ||
          _provider.controllerInstitutionName.text != _provider.institutionNameTemp ||
          _provider.npwpTemp != _provider.controllerNPWP.text ||
          _provider.controllerEstablishDate.text != widget.model.establishDate
          // _provider.addressTemp != _provider.controllerAddress.text ||
          // _provider.addressTypeTemp != _provider.controllerAddressType.text ||
          // _provider.rtTemp != _provider.controllerRT.text ||
          // _provider.rwTemp != _provider.controllerRW.text ||
          // _provider.kelurahanTemp != _provider.controllerKelurahan.text ||
          // _provider.kecamatanTemp != _provider.controllerKecamatan.text ||
          // _provider.kotaTemp != _provider.controllerKota.text ||
          // _provider.provTemp != _provider.controllerProvinsi.text ||
          // _provider.postalCodeTemp != _provider.controllerPostalCode.text ||
          // _provider.phoneArea1Temp != _provider.controllerTelephoneArea1.text ||
          // _provider.phone1Temp != _provider.controllerTelephone1.text ||
          // _provider.phoneArea2Temp != _provider.controllerTelephoneArea2.text ||
          // _provider.phone2Temp != _provider.controllerTelephone2.text ||
          // _provider.faxAreaTemp != _provider.controllerFaxArea.text ||
          // _provider.faxTemp != _provider.controllerFax.text
      ) {
          return (await showDialog(
            context: context,
            builder: (myContext) => AlertDialog(
              title: new Text('Warning'),
              content: new Text('Simpan perubahan?'),
              actions: <Widget>[
                FlatButton(
                  onPressed: () {
                    _provider.check(context, widget.flag, widget.index);
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(fontSize: 12, color: Colors.grey),),
                ),
                FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop(true);
                  },
                  child: new Text('Tidak'),
                ),
              ],
            ),
          )) ?? false;
      } else {
        return (await showDialog(
          context: context,
          builder: (myContext) => AlertDialog(
            title: new Text('Warning'),
            content: new Text('Keluar dari edit data?'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () {
                  Navigator.pop(context, true);
                },
                child: new Text('Ya', style: TextStyle(fontSize: 12, color: Colors.grey),),
              ),
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('Tidak'),
              ),
            ],
          ),
        )) ?? false;
      }
    }
  }
}
