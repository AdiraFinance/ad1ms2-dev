import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/shared/form_m_add_occupation_address_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import '../../main.dart';

//tidak terpakai
class AddOccupationAddress extends StatefulWidget {
  final int flag;
  final int index;
  final AddressModel addressModel;

  const AddOccupationAddress({this.flag, this.index, this.addressModel});
  @override
  _AddOccupationAddressState createState() => _AddOccupationAddressState();
}

class _AddOccupationAddressState extends State<AddOccupationAddress> {
  Future<void> _setValueForEdit;

  @override
  void initState() {
    super.initState();
    if (widget.flag != 0) {
      _setValueForEdit = Provider.of<FormMAddOccupationAddressChangeNotifier>(
          context,
          listen: false)
          .addDataListAddressType(widget.addressModel, context, widget.flag,
          widget.addressModel.isSameWithIdentity,widget.index);
    } else {
      Provider.of<FormMAddOccupationAddressChangeNotifier>(context,
          listen: false)
          .addDataListAddressType(null,context,widget.flag,null,null);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange),
      child: Scaffold(
        appBar: AppBar(
          title: Text(
              widget.flag == 0
                  ? "Tambah Alamat Korespondensi"
                  : "Edit Alamat Korespondensi",
              style: TextStyle(color: Colors.black)),
          centerTitle: true,
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: SingleChildScrollView(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width / 27,
                vertical: MediaQuery.of(context).size.height / 57),
            child: widget.flag == 0
                ? Consumer<FormMAddOccupationAddressChangeNotifier>(
                    builder: (context, formMAddOccupationAddress, _) {
                      return Form(
                        key: formMAddOccupationAddress.key,
                        onWillPop: _onWillPop,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            DropdownButtonFormField<JenisAlamatModel>(
                                autovalidate:
                                    formMAddOccupationAddress.autoValidate,
                                validator: (e) {
                                  if (e == null) {
                                    return "Silahkan pilih pekerjaan";
                                  } else {
                                    return null;
                                  }
                                },
                                value: formMAddOccupationAddress
                                    .jenisAlamatSelected,
                                onChanged: (value) {
                                  formMAddOccupationAddress
                                      .jenisAlamatSelected = value;
                                },
                                decoration: InputDecoration(
                                  labelText: "Jenis Identitas",
                                  border: OutlineInputBorder(),
                                  contentPadding:
                                      EdgeInsets.symmetric(horizontal: 10),
                                ),
                                items: formMAddOccupationAddress.listJenisAlamat
                                    .map((value) {
                                  return DropdownMenuItem<JenisAlamatModel>(
                                    value: value,
                                    child: Text(
                                      value.DESKRIPSI,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  );
                                }).toList()),
                            formMAddOccupationAddress.jenisAlamatSelected != null
                            ? formMAddOccupationAddress.jenisAlamatSelected.KODE == "02"
                                ? Row(
                                  children: [
                                    Checkbox(
                                        value: formMAddOccupationAddress
                                            .isSameWithIdentity,
                                        onChanged: (value) {
                                          formMAddOccupationAddress
                                              .isSameWithIdentity = value;
                                          formMAddOccupationAddress
                                              .setValueIsSameWithIdentity(
                                                  value, context, null);
                                        },
                                        activeColor: myPrimaryColor),
                                    SizedBox(width: 8),
                                    Text("Sama dengan identitas")
                                  ],
                                )
                              : SizedBox(height: MediaQuery.of(context).size.height / 47)
                            : SizedBox(height: MediaQuery.of(context).size.height / 47),
                            TextFormField(
                              autovalidate:
                                  formMAddOccupationAddress.autoValidate,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              controller:
                                  formMAddOccupationAddress.controllerAlamat,
                              enabled:
                                  formMAddOccupationAddress.enableTfAddress,
                              style: TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                  filled: !formMAddOccupationAddress
                                      .enableTfAddress,
                                  fillColor:
                                      !formMAddOccupationAddress.enableTfAddress
                                          ? Colors.black12
                                          : Colors.white,
                                  labelText: 'Alamat',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              maxLines: 3,
                              keyboardType: TextInputType.text,
                              textCapitalization: TextCapitalization.characters,
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            Row(
                              children: [
                                Expanded(
                                  flex: 5,
                                  child: TextFormField(
                                    autovalidate:
                                        formMAddOccupationAddress.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter.digitsOnly,
                                      // LengthLimitingTextInputFormatter(10),
                                    ],
                                    enabled:
                                        formMAddOccupationAddress.enableTfRT,
                                    controller:
                                        formMAddOccupationAddress.controllerRT,
                                    style: TextStyle(color: Colors.black),
                                    decoration: InputDecoration(
                                        filled: !formMAddOccupationAddress
                                            .enableTfRT,
                                        fillColor: !formMAddOccupationAddress
                                                .enableTfRT
                                            ? Colors.black12
                                            : Colors.white,
                                        labelText: 'RT',
                                        labelStyle:
                                            TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8))),
                                    keyboardType: TextInputType.number,
                                  ),
                                ),
                                SizedBox(width: 8),
                                Expanded(
                                  flex: 5,
                                  child: TextFormField(
                                    autovalidate:
                                        formMAddOccupationAddress.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter.digitsOnly,
                                      // LengthLimitingTextInputFormatter(10),
                                    ],
                                    enabled: formMAddOccupationAddress.enableTfRW,
                                    controller:
                                        formMAddOccupationAddress.controllerRW,
                                    style: TextStyle(color: Colors.black),
                                    decoration: InputDecoration(
                                        filled: !formMAddOccupationAddress
                                            .enableTfRW,
                                        fillColor: !formMAddOccupationAddress
                                                .enableTfRW
                                            ? Colors.black12
                                            : Colors.white,
                                        labelText: 'RW',
                                        labelStyle:
                                            TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8))),
                                    keyboardType: TextInputType.number,
                                  ),
                                )
                              ],
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            FocusScope(
                              node: FocusScopeNode(),
                              child: TextFormField(
                                autovalidate:
                                    formMAddOccupationAddress.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                onTap: () {
                                  formMAddOccupationAddress
                                      .searchKelurahan(context);
                                },
                                enabled: formMAddOccupationAddress.enableTfKelurahan,
                                controller: formMAddOccupationAddress
                                    .controllerKelurahan,
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    filled: !formMAddOccupationAddress
                                        .enableTfKelurahan,
                                    fillColor: !formMAddOccupationAddress
                                            .enableTfKelurahan
                                        ? Colors.black12
                                        : Colors.white,
                                    labelText: 'Kelurahan',
                                    labelStyle: TextStyle(color: Colors.black),
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(8))),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            TextFormField(
                              autovalidate:
                                  formMAddOccupationAddress.autoValidate,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              // enabled: formMAddOccupationAddress.enableTfKecamatan,
                              controller:
                                  formMAddOccupationAddress.controllerKecamatan,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  filled: !formMAddOccupationAddress
                                      .enableTfKecamatan,
                                  fillColor: !formMAddOccupationAddress
                                          .enableTfKecamatan
                                      ? Colors.black12
                                      : Colors.white,
                                  labelText: 'Kecamatan',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              enabled: false,
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            TextFormField(
                              autovalidate:
                                  formMAddOccupationAddress.autoValidate,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              // enabled: formMAddOccupationAddress.enableTfKota,
                              controller:
                                  formMAddOccupationAddress.controllerKota,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  filled:
                                      !formMAddOccupationAddress.enableTfKota,
                                  fillColor:
                                      !formMAddOccupationAddress.enableTfKota
                                          ? Colors.black12
                                          : Colors.white,
                                  labelText: 'Kabupaten/Kota',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              enabled: false,
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            Row(
                              children: [
                                Expanded(
                                  flex: 7,
                                  child: TextFormField(
                                    autovalidate:
                                        formMAddOccupationAddress.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    // enabled: formMAddOccupationAddress.enableTfProv,
                                    controller: formMAddOccupationAddress
                                        .controllerProvinsi,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        filled: !formMAddOccupationAddress
                                            .enableTfProv,
                                        fillColor: !formMAddOccupationAddress
                                                .enableTfProv
                                            ? Colors.black12
                                            : Colors.white,
                                        labelText: 'Provinsi',
                                        labelStyle:
                                            TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8))),
                                    enabled: false,
                                  ),
                                ),
                                SizedBox(width: 8),
                                Expanded(
                                  flex: 3,
                                  child: TextFormField(
                                    autovalidate:
                                        formMAddOccupationAddress.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter.digitsOnly,
                                      // LengthLimitingTextInputFormatter(10),
                                    ],
                                    // enabled: formMAddOccupationAddress.enableTfPostalCode,
                                    controller: formMAddOccupationAddress
                                        .controllerPostalCode,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        filled: !formMAddOccupationAddress
                                            .enableTfPostalCode,
                                        fillColor: !formMAddOccupationAddress
                                                .enableTfPostalCode
                                            ? Colors.black12
                                            : Colors.white,
                                        labelText: 'Kode Pos',
                                        labelStyle:
                                            TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8))),
                                    enabled: false,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            Row(
                              children: [
                                Expanded(
                                  flex: 4,
                                  child: TextFormField(
                                    autovalidate:
                                        formMAddOccupationAddress.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    onChanged: (e) {
                                      formMAddOccupationAddress
                                          .checkValidCodeArea(e);
                                    },
                                    keyboardType: TextInputType.number,
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter.digitsOnly,
                                      LengthLimitingTextInputFormatter(10),
                                    ],
                                    enabled: formMAddOccupationAddress
                                        .enableTfAreaCode,
                                    controller: formMAddOccupationAddress
                                        .controllerKodeArea,
                                    style: TextStyle(color: Colors.black),
                                    decoration: InputDecoration(
                                        filled: !formMAddOccupationAddress
                                            .enableTfAreaCode,
                                        fillColor: !formMAddOccupationAddress
                                                .enableTfAreaCode
                                            ? Colors.black12
                                            : Colors.white,
                                        labelText: 'Kode Area',
                                        labelStyle:
                                            TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8))),
                                  ),
                                ),
                                SizedBox(
                                    width:
                                        MediaQuery.of(context).size.width / 37),
                                Expanded(
                                  flex: 6,
                                  child: TextFormField(
                                    autovalidate:
                                        formMAddOccupationAddress.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    enabled:
                                        formMAddOccupationAddress.enableTfPhone,
                                    keyboardType: TextInputType.number,
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter.digitsOnly,
                                      LengthLimitingTextInputFormatter(10),
                                    ],
                                    controller: formMAddOccupationAddress
                                        .controllerTlpn,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        filled: !formMAddOccupationAddress
                                            .enableTfPhone,
                                        fillColor: !formMAddOccupationAddress
                                                .enableTfPhone
                                            ? Colors.black12
                                            : Colors.white,
                                        labelText: 'Telepon',
                                        labelStyle:
                                            TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8))),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      );
                    },
                  )
                : FutureBuilder(
                    future: _setValueForEdit,
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                      return Consumer<FormMAddOccupationAddressChangeNotifier>(
                        builder: (context, formMAddOccupationAddress, _) {
                          return Form(
                            key: formMAddOccupationAddress.key,
                            onWillPop: _onWillPop,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                DropdownButtonFormField<JenisAlamatModel>(
                                    autovalidate:
                                        formMAddOccupationAddress.autoValidate,
                                    validator: (e) {
                                      if (e == null) {
                                        return "Silahkan pilih jenis alamat";
                                      } else {
                                        return null;
                                      }
                                    },
                                    value: formMAddOccupationAddress
                                        .jenisAlamatSelected,
                                    onChanged: (value) {
                                      formMAddOccupationAddress
                                          .jenisAlamatSelected = value;
                                    },
                                    decoration: InputDecoration(
                                      labelText: "Jenis Alamat",
                                      border: OutlineInputBorder(),
                                      contentPadding:
                                          EdgeInsets.symmetric(horizontal: 10),
                                    ),
                                    items: formMAddOccupationAddress
                                        .listJenisAlamat
                                        .map((value) {
                                      return DropdownMenuItem<JenisAlamatModel>(
                                        value: value,
                                        child: Text(
                                          value.DESKRIPSI,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      );
                                    }).toList()),
                                formMAddOccupationAddress.jenisAlamatSelected != null
                                    ? formMAddOccupationAddress.jenisAlamatSelected.KODE == "02"
                                        ? Row(
                                            children: [
                                              Checkbox(
                                                  value:
                                                      formMAddOccupationAddress
                                                          .isSameWithIdentity,
                                                  onChanged: (value) {
                                                    formMAddOccupationAddress
                                                            .isSameWithIdentity =
                                                        value;
                                                    formMAddOccupationAddress
                                                        .setValueIsSameWithIdentity(
                                                            value,
                                                            context,
                                                            null);
                                                  },
                                                  activeColor: myPrimaryColor),
                                              SizedBox(width: 8),
                                              Text("Sama dengan identitas")
                                            ],
                                          )
                                        : SizedBox(
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height /
                                                47)
                                    : SizedBox(
                                        height:
                                            MediaQuery.of(context).size.height /
                                                47),
                                TextFormField(
                                  autovalidate:
                                      formMAddOccupationAddress.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  controller: formMAddOccupationAddress
                                      .controllerAlamat,
                                  enabled:
                                      formMAddOccupationAddress.enableTfAddress,
                                  style: TextStyle(color: Colors.black),
                                  decoration: InputDecoration(
                                      filled: !formMAddOccupationAddress
                                          .enableTfAddress,
                                      fillColor: !formMAddOccupationAddress
                                              .enableTfAddress
                                          ? Colors.black12
                                          : Colors.white,
                                      labelText: 'Alamat',
                                      labelStyle:
                                          TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(8))),
                                  maxLines: 3,
                                  keyboardType: TextInputType.text,
                                  textCapitalization: TextCapitalization.characters,
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                Row(
                                  children: [
                                    Expanded(
                                      flex: 5,
                                      child: TextFormField(
                                        autovalidate: formMAddOccupationAddress
                                            .autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        inputFormatters: [
                                          WhitelistingTextInputFormatter.digitsOnly,
                                          // LengthLimitingTextInputFormatter(10),
                                        ],
                                        enabled: formMAddOccupationAddress
                                            .enableTfRT,
                                        controller: formMAddOccupationAddress
                                            .controllerRT,
                                        style: TextStyle(color: Colors.black),
                                        decoration: InputDecoration(
                                            filled: !formMAddOccupationAddress
                                                .enableTfRT,
                                            fillColor:
                                                !formMAddOccupationAddress
                                                        .enableTfRT
                                                    ? Colors.black12
                                                    : Colors.white,
                                            labelText: 'RT',
                                            labelStyle:
                                                TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                        keyboardType: TextInputType.number,
                                      ),
                                    ),
                                    SizedBox(width: 8),
                                    Expanded(
                                      flex: 5,
                                      child: TextFormField(
                                        autovalidate: formMAddOccupationAddress
                                            .autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        inputFormatters: [
                                          WhitelistingTextInputFormatter.digitsOnly,
                                          // LengthLimitingTextInputFormatter(10),
                                        ],
                                        enabled: formMAddOccupationAddress
                                            .enableTfRW,
                                        controller: formMAddOccupationAddress
                                            .controllerRW,
                                        style: TextStyle(color: Colors.black),
                                        decoration: InputDecoration(
                                            filled: !formMAddOccupationAddress
                                                .enableTfRW,
                                            fillColor:
                                                !formMAddOccupationAddress
                                                        .enableTfRW
                                                    ? Colors.black12
                                                    : Colors.white,
                                            labelText: 'RW',
                                            labelStyle:
                                                TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                        keyboardType: TextInputType.number,
                                      ),
                                    )
                                  ],
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                FocusScope(
                                  node: FocusScopeNode(),
                                  child: TextFormField(
                                    autovalidate:
                                        formMAddOccupationAddress.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    onTap: () {
                                      formMAddOccupationAddress
                                          .searchKelurahan(context);
                                    },
                                    enabled: formMAddOccupationAddress
                                        .enableTfKelurahan,
                                    controller: formMAddOccupationAddress
                                        .controllerKelurahan,
                                    style: TextStyle(color: Colors.black),
                                    decoration: InputDecoration(
                                        filled: !formMAddOccupationAddress
                                            .enableTfKelurahan,
                                        fillColor: !formMAddOccupationAddress
                                                .enableTfKelurahan
                                            ? Colors.black12
                                            : Colors.white,
                                        labelText: 'Kelurahan',
                                        labelStyle:
                                            TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8))),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                TextFormField(
                                  autovalidate:
                                      formMAddOccupationAddress.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  // enabled: formMAddOccupationAddress.enableTfKecamatan,
                                  controller: formMAddOccupationAddress
                                      .controllerKecamatan,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      filled: !formMAddOccupationAddress
                                          .enableTfKecamatan,
                                      fillColor: !formMAddOccupationAddress
                                              .enableTfKecamatan
                                          ? Colors.black12
                                          : Colors.white,
                                      labelText: 'Kecamatan',
                                      labelStyle:
                                          TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(8))),
                                  enabled: false,
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                TextFormField(
                                  autovalidate:
                                      formMAddOccupationAddress.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  // enabled: formMAddOccupationAddress.enableTfKota,
                                  controller:
                                      formMAddOccupationAddress.controllerKota,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      filled: !formMAddOccupationAddress
                                          .enableTfKota,
                                      fillColor: !formMAddOccupationAddress
                                              .enableTfKota
                                          ? Colors.black12
                                          : Colors.white,
                                      labelText: 'Kabupaten/Kota',
                                      labelStyle:
                                          TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(8))),
                                  enabled: false,
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                Row(
                                  children: [
                                    Expanded(
                                      flex: 7,
                                      child: TextFormField(
                                        autovalidate: formMAddOccupationAddress
                                            .autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        // enabled: formMAddOccupationAddress.enableTfProv,
                                        controller: formMAddOccupationAddress
                                            .controllerProvinsi,
                                        style:
                                            new TextStyle(color: Colors.black),
                                        decoration: new InputDecoration(
                                            filled: !formMAddOccupationAddress
                                                .enableTfProv,
                                            fillColor:
                                                !formMAddOccupationAddress
                                                        .enableTfProv
                                                    ? Colors.black12
                                                    : Colors.white,
                                            labelText: 'Provinsi',
                                            labelStyle:
                                                TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                        enabled: false,
                                      ),
                                    ),
                                    SizedBox(width: 8),
                                    Expanded(
                                      flex: 3,
                                      child: TextFormField(
                                        autovalidate: formMAddOccupationAddress
                                            .autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        // enabled: formMAddOccupationAddress.enableTfPostalCode,
                                        controller: formMAddOccupationAddress
                                            .controllerPostalCode,
                                        style:
                                            new TextStyle(color: Colors.black),
                                        decoration: new InputDecoration(
                                            filled: !formMAddOccupationAddress
                                                .enableTfPostalCode,
                                            fillColor:
                                                !formMAddOccupationAddress
                                                        .enableTfPostalCode
                                                    ? Colors.black12
                                                    : Colors.white,
                                            labelText: 'Kode Pos',
                                            labelStyle:
                                                TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                        enabled: false,
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                Row(
                                  children: [
                                    Expanded(
                                      flex: 4,
                                      child: TextFormField(
                                        autovalidate: formMAddOccupationAddress
                                            .autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        onChanged: (e) {
                                          formMAddOccupationAddress
                                              .checkValidCodeArea(e);
                                        },
                                        keyboardType: TextInputType.number,
                                        inputFormatters: [
                                          WhitelistingTextInputFormatter
                                              .digitsOnly,
                                          LengthLimitingTextInputFormatter(10),
                                        ],
                                        enabled: formMAddOccupationAddress
                                            .enableTfAreaCode,
                                        controller: formMAddOccupationAddress
                                            .controllerKodeArea,
                                        style: TextStyle(color: Colors.black),
                                        decoration: InputDecoration(
                                            filled: !formMAddOccupationAddress
                                                .enableTfAreaCode,
                                            fillColor:
                                                !formMAddOccupationAddress
                                                        .enableTfAreaCode
                                                    ? Colors.black12
                                                    : Colors.white,
                                            labelText: 'Kode Area',
                                            labelStyle:
                                                TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                      ),
                                    ),
                                    SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width /
                                                37),
                                    Expanded(
                                      flex: 6,
                                      child: TextFormField(
                                        autovalidate: formMAddOccupationAddress
                                            .autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        enabled: formMAddOccupationAddress
                                            .enableTfPhone,
                                        keyboardType: TextInputType.number,
                                        inputFormatters: [
                                          WhitelistingTextInputFormatter
                                              .digitsOnly,
                                          LengthLimitingTextInputFormatter(10),
                                        ],
                                        controller: formMAddOccupationAddress
                                            .controllerTlpn,
                                        style:
                                            new TextStyle(color: Colors.black),
                                        decoration: new InputDecoration(
                                            filled: !formMAddOccupationAddress
                                                .enableTfPhone,
                                            fillColor:
                                                !formMAddOccupationAddress
                                                        .enableTfPhone
                                                    ? Colors.black12
                                                    : Colors.white,
                                            labelText: 'Telepon',
                                            labelStyle:
                                                TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          );
                        },
                      );
                    })),
        bottomNavigationBar: BottomAppBar(
          elevation: 0.0,
          child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(8.0)),
                  color: myPrimaryColor,
                  onPressed: () {
                    if (widget.flag == 0) {
                      Provider.of<FormMAddOccupationAddressChangeNotifier>(
                              context,
                              listen: false)
                          .check(context, widget.flag, null);
                    } else {
                      Provider.of<FormMAddOccupationAddressChangeNotifier>(
                              context,
                              listen: false)
                          .check(context, widget.flag, widget.index);
                    }
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(widget.flag == 0 ? "SAVE" : "UPDATE",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 1.25))
                    ],
                  ))),
        ),
      ),
    );
  }

  Future<bool> _onWillPop() async {
    var _provider = Provider.of<FormMAddOccupationAddressChangeNotifier>(
        context,
        listen: false);
    if (widget.flag == 0) {
      if (_provider.jenisAlamatSelected != null ||
          _provider.controllerAlamat.text != "" ||
          _provider.controllerRT.text != "" ||
          _provider.controllerRW.text != "" ||
          _provider.controllerKelurahan.text != "" ||
          _provider.controllerKecamatan.text != "" ||
          _provider.controllerKota.text != "" ||
          _provider.controllerProvinsi.text != "" ||
          _provider.controllerPostalCode.text != "" ||
          _provider.controllerKodeArea.text != "" ||
          _provider.controllerTlpn.text != "") {
        return (await showDialog(
              context: context,
              builder: (myContext) => AlertDialog(
                title: new Text('Warning'),
                content: new Text('Simpan perubahan?'),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      _provider.check(context, widget.flag, null);
                      Navigator.pop(context);
                    },
                    child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                  ),
                  new FlatButton(
                    onPressed: () => Navigator.of(context).pop(true),
                    child: new Text('Tidak'),
                  ),
                ],
              ),
            )) ??
            false;
      } else {
        return true;
      }
    } else {
      if (_provider.jenisAlamatSelectedTemp.KODE != _provider.jenisAlamatSelected.KODE ||
          _provider.alamatTemp != _provider.controllerAlamat.text ||
          _provider.rtTemp != _provider.controllerRT.text ||
          _provider.rwTemp != _provider.controllerRW.text ||
          _provider.kelurahanTemp != _provider.controllerKelurahan.text ||
          _provider.kecamatanTemp != _provider.controllerKecamatan.text ||
          _provider.kotaTemp != _provider.controllerKota.text ||
          _provider.provinsiTemp != _provider.controllerProvinsi.text ||
          _provider.postalCodeTemp != _provider.controllerPostalCode.text ||
          _provider.areaCOdeTemp != _provider.controllerKodeArea.text ||
          _provider.phoneTemp != _provider.controllerTlpn.text) {
          return (await showDialog(
              context: context,
              builder: (myContext) => AlertDialog(
                title: new Text('Warning'),
                content: new Text('Simpan perubahan?'),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      _provider.check(context, widget.flag, widget.index);
                      Navigator.pop(context);
                    },
                    child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                  ),
                  new FlatButton(
                    onPressed: () => Navigator.of(context).pop(true),
                    child: new Text('Tidak'),
                  ),
                ],
              ),
            )) ??
            false;
      } else {
        return true;
      }
    }
  }
}
