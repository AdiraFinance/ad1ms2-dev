import 'package:ad1ms2_dev/models/address_guarantor_model.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_company_change_notifier.dart';
import 'package:ad1ms2_dev/widgets/widget_address_company.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class AddGuarantorAddressCompany extends StatefulWidget {
  final int flag;
  final int index;
  final AddressModelCompany addressModel;
  final int typeAddress;

  const AddGuarantorAddressCompany({this.flag, this.index, this.addressModel, this.typeAddress});
  @override
  _AddGuarantorAddressCompanyState createState() =>
      _AddGuarantorAddressCompanyState();
}

class _AddGuarantorAddressCompanyState
    extends State<AddGuarantorAddressCompany> {
  Future<void> _setValueForEdit;

  @override
  void initState() {
    super.initState();
    if (widget.flag != 0) {
      _setValueForEdit = Provider.of<FormMAddAddressCompanyChangeNotifier>(
          context,
          listen: false)
          .addDataListJenisAlamat(widget.addressModel, context, widget.flag,
          widget.addressModel.isSameWithIdentity,widget.index, widget.typeAddress);
    } else {
      Provider.of<FormMAddAddressCompanyChangeNotifier>(context,
          listen: false)
          .addDataListJenisAlamat(null,context,widget.flag,null,null, widget.typeAddress);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans"),
      child:
      WidgetAddressCompany(flag: widget.flag, index: widget.index, editVal: _setValueForEdit,
        addressModel: widget.addressModel, typeAddress: widget.typeAddress,)

//      Scaffold(
//        appBar: AppBar(
//          title: Text(
//              widget.flag == 0
//                  ? "Add Alamat Penjamin Kelembagaan"
//                  : "Edit Alamat Penjamin Kelembagaan",
//              style: TextStyle(color: Colors.black)),
//          centerTitle: true,
//          backgroundColor: myPrimaryColor,
//          iconTheme: IconThemeData(color: Colors.black),
//        ),
//        body: SingleChildScrollView(
//            padding: EdgeInsets.symmetric(
//                horizontal: MediaQuery.of(context).size.width / 27,
//                vertical: MediaQuery.of(context).size.height / 57),
//            child: widget.flag == 0
//                ? Consumer<FormMAddGuarantorAddressCompanyChangeNotifier>(
//                    builder: (context, formMAddGuarantorAddress, _) {
//                      return Form(
//                        key: formMAddGuarantorAddress.key,
//                        onWillPop: _onWillPop,
//                        child: Column(
//                          crossAxisAlignment: CrossAxisAlignment.start,
//                          children: [
//                            DropdownButtonFormField<JenisAlamatModel>(
//                                autovalidate:
//                                    formMAddGuarantorAddress.autoValidate,
//                                validator: (e) {
//                                  if (e == null) {
//                                    return "Silahkan pilih jenis alamat";
//                                  } else {
//                                    return null;
//                                  }
//                                },
//                                value: formMAddGuarantorAddress
//                                    .jenisAlamatSelected,
//                                onChanged: (value) {
//                                  formMAddGuarantorAddress.jenisAlamatSelected =
//                                      value;
//                                },
//                                onTap: () {
//                                  FocusManager.instance.primaryFocus.unfocus();
//                                },
//                                decoration: InputDecoration(
//                                  labelText: "Jenis Alamat",
//                                  border: OutlineInputBorder(),
//                                  contentPadding:
//                                      EdgeInsets.symmetric(horizontal: 10),
//                                ),
//                                items: formMAddGuarantorAddress.listJenisAlamat
//                                    .map((value) {
//                                  return DropdownMenuItem<JenisAlamatModel>(
//                                    value: value,
//                                    child: Text(
//                                      value.DESKRIPSI,
//                                      overflow: TextOverflow.ellipsis,
//                                    ),
//                                  );
//                                }).toList()),
//                            formMAddGuarantorAddress.jenisAlamatSelected != null
//                                ? formMAddGuarantorAddress
//                                            .jenisAlamatSelected.KODE ==
//                                        "02"
//                                    ? Row(
//                                        children: [
//                                          Checkbox(
//                                              value: formMAddGuarantorAddress
//                                                  .isSameWithIdentity,
//                                              onChanged: (value) {
//                                                formMAddGuarantorAddress
//                                                    .isSameWithIdentity = value;
//                                                formMAddGuarantorAddress
//                                                    .setValueIsSameWithIdentity(
//                                                        value, context, null);
//                                              },
//                                              activeColor: myPrimaryColor),
//                                          SizedBox(width: 8),
//                                          Text("Sama dengan identitas")
//                                        ],
//                                      )
//                                    : SizedBox(
//                                        height:
//                                            MediaQuery.of(context).size.height /
//                                                47)
//                                : SizedBox(
//                                    height: MediaQuery.of(context).size.height /
//                                        47),
//                            TextFormField(
//                              autovalidate:
//                                  formMAddGuarantorAddress.autoValidate,
//                              validator: (e) {
//                                if (e.isEmpty) {
//                                  return "Tidak boleh kosong";
//                                } else {
//                                  return null;
//                                }
//                              },
//                              controller:
//                                  formMAddGuarantorAddress.controllerAlamat,
//                              enabled: formMAddGuarantorAddress.enableTfAddress,
//                              style: TextStyle(color: Colors.black),
//                              decoration: InputDecoration(
//                                  filled:
//                                      !formMAddGuarantorAddress.enableTfAddress,
//                                  fillColor:
//                                      !formMAddGuarantorAddress.enableTfAddress
//                                          ? Colors.black12
//                                          : Colors.white,
//                                  labelText: 'Alamat',
//                                  labelStyle: TextStyle(color: Colors.black),
//                                  border: OutlineInputBorder(
//                                      borderRadius: BorderRadius.circular(8))),
//                              maxLines: 3,
//                              keyboardType: TextInputType.text,
//                              textCapitalization: TextCapitalization.characters,
//                            ),
//                            SizedBox(
//                                height:
//                                    MediaQuery.of(context).size.height / 47),
//                            Row(
//                              children: [
//                                Expanded(
//                                  flex: 5,
//                                  child: TextFormField(
//                                    autovalidate:
//                                        formMAddGuarantorAddress.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    enabled:
//                                        formMAddGuarantorAddress.enableTfRT,
//                                    controller:
//                                        formMAddGuarantorAddress.controllerRT,
//                                    style: TextStyle(color: Colors.black),
//                                    decoration: InputDecoration(
//                                        filled: !formMAddGuarantorAddress
//                                            .enableTfRT,
//                                        fillColor:
//                                            !formMAddGuarantorAddress.enableTfRT
//                                                ? Colors.black12
//                                                : Colors.white,
//                                        labelText: 'RT',
//                                        labelStyle:
//                                            TextStyle(color: Colors.black),
//                                        border: OutlineInputBorder(
//                                            borderRadius:
//                                                BorderRadius.circular(8))),
//                                    inputFormatters: [
//                                      WhitelistingTextInputFormatter.digitsOnly,
//                                      // LengthLimitingTextInputFormatter(10),
//                                    ],
//                                    keyboardType: TextInputType.number,
//                                  ),
//                                ),
//                                SizedBox(width: 8),
//                                Expanded(
//                                  flex: 5,
//                                  child: TextFormField(
//                                    autovalidate:
//                                        formMAddGuarantorAddress.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    enabled:
//                                        formMAddGuarantorAddress.enableTfRW,
//                                    controller:
//                                        formMAddGuarantorAddress.controllerRW,
//                                    style: TextStyle(color: Colors.black),
//                                    decoration: InputDecoration(
//                                        filled: !formMAddGuarantorAddress
//                                            .enableTfRW,
//                                        fillColor:
//                                            !formMAddGuarantorAddress.enableTfRW
//                                                ? Colors.black12
//                                                : Colors.white,
//                                        labelText: 'RW',
//                                        labelStyle:
//                                            TextStyle(color: Colors.black),
//                                        border: OutlineInputBorder(
//                                            borderRadius:
//                                                BorderRadius.circular(8))),
//                                    inputFormatters: [
//                                      WhitelistingTextInputFormatter.digitsOnly,
//                                      // LengthLimitingTextInputFormatter(10),
//                                    ],
//                                    keyboardType: TextInputType.number,
//                                  ),
//                                )
//                              ],
//                            ),
//                            SizedBox(
//                                height:
//                                    MediaQuery.of(context).size.height / 47),
//                            FocusScope(
//                              node: FocusScopeNode(),
//                              child: TextFormField(
//                                autovalidate:
//                                    formMAddGuarantorAddress.autoValidate,
//                                validator: (e) {
//                                  if (e.isEmpty) {
//                                    return "Tidak boleh kosong";
//                                  } else {
//                                    return null;
//                                  }
//                                },
//                                onTap: () {
//                                  FocusManager.instance.primaryFocus.unfocus();
//                                  formMAddGuarantorAddress
//                                      .searchKelurahan(context);
//                                },
//                                enabled:
//                                    formMAddGuarantorAddress.enableTfKelurahan,
//                                controller: formMAddGuarantorAddress
//                                    .controllerKelurahan,
//                                style: TextStyle(color: Colors.black),
//                                decoration: InputDecoration(
//                                    filled: !formMAddGuarantorAddress
//                                        .enableTfKelurahan,
//                                    fillColor: !formMAddGuarantorAddress
//                                            .enableTfKelurahan
//                                        ? Colors.black12
//                                        : Colors.white,
//                                    labelText: 'Kelurahan',
//                                    labelStyle: TextStyle(color: Colors.black),
//                                    border: OutlineInputBorder(
//                                        borderRadius:
//                                            BorderRadius.circular(8))),
//                              ),
//                            ),
//                            SizedBox(
//                                height:
//                                    MediaQuery.of(context).size.height / 47),
//                            TextFormField(
//                              autovalidate:
//                                  formMAddGuarantorAddress.autoValidate,
//                              validator: (e) {
//                                if (e.isEmpty) {
//                                  return "Tidak boleh kosong";
//                                } else {
//                                  return null;
//                                }
//                              },
//                              // enabled: formMAddGuarantorAddress.enableTfKecamatan,
//                              controller:
//                                  formMAddGuarantorAddress.controllerKecamatan,
//                              style: new TextStyle(color: Colors.black),
//                              decoration: new InputDecoration(
//                                  filled: !formMAddGuarantorAddress
//                                      .enableTfKecamatan,
//                                  fillColor: !formMAddGuarantorAddress
//                                          .enableTfKecamatan
//                                      ? Colors.black12
//                                      : Colors.white,
//                                  labelText: 'Kecamatan',
//                                  labelStyle: TextStyle(color: Colors.black),
//                                  border: OutlineInputBorder(
//                                      borderRadius: BorderRadius.circular(8))),
//                              enabled: false,
//                            ),
//                            SizedBox(
//                                height:
//                                    MediaQuery.of(context).size.height / 47),
//                            TextFormField(
//                              autovalidate:
//                                  formMAddGuarantorAddress.autoValidate,
//                              validator: (e) {
//                                if (e.isEmpty) {
//                                  return "Tidak boleh kosong";
//                                } else {
//                                  return null;
//                                }
//                              },
//                              // enabled: formMAddGuarantorAddress.enableTfKota,
//                              controller:
//                                  formMAddGuarantorAddress.controllerKota,
//                              style: new TextStyle(color: Colors.black),
//                              decoration: new InputDecoration(
//                                  filled:
//                                      !formMAddGuarantorAddress.enableTfKota,
//                                  fillColor:
//                                      !formMAddGuarantorAddress.enableTfKota
//                                          ? Colors.black12
//                                          : Colors.white,
//                                  labelText: 'Kabupaten/Kota',
//                                  labelStyle: TextStyle(color: Colors.black),
//                                  border: OutlineInputBorder(
//                                      borderRadius: BorderRadius.circular(8))),
//                              enabled: false,
//                            ),
//                            SizedBox(
//                                height:
//                                    MediaQuery.of(context).size.height / 47),
//                            Row(
//                              children: [
//                                Expanded(
//                                  flex: 7,
//                                  child: TextFormField(
//                                    autovalidate:
//                                        formMAddGuarantorAddress.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    // enabled: formMAddGuarantorAddress.enableTfProv,
//                                    controller: formMAddGuarantorAddress
//                                        .controllerProvinsi,
//                                    style: new TextStyle(color: Colors.black),
//                                    decoration: new InputDecoration(
//                                        filled: !formMAddGuarantorAddress
//                                            .enableTfProv,
//                                        fillColor: !formMAddGuarantorAddress
//                                                .enableTfProv
//                                            ? Colors.black12
//                                            : Colors.white,
//                                        labelText: 'Provinsi',
//                                        labelStyle:
//                                            TextStyle(color: Colors.black),
//                                        border: OutlineInputBorder(
//                                            borderRadius:
//                                                BorderRadius.circular(8))),
//                                    enabled: false,
//                                  ),
//                                ),
//                                SizedBox(width: 8),
//                                Expanded(
//                                  flex: 3,
//                                  child: TextFormField(
//                                    autovalidate:
//                                        formMAddGuarantorAddress.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    // enabled: formMAddGuarantorAddress.enableTfPostalCode,
//                                    controller: formMAddGuarantorAddress
//                                        .controllerPostalCode,
//                                    style: new TextStyle(color: Colors.black),
//                                    decoration: new InputDecoration(
//                                        filled: !formMAddGuarantorAddress
//                                            .enableTfPostalCode,
//                                        fillColor: !formMAddGuarantorAddress
//                                                .enableTfPostalCode
//                                            ? Colors.black12
//                                            : Colors.white,
//                                        labelText: 'Kode Pos',
//                                        labelStyle:
//                                            TextStyle(color: Colors.black),
//                                        border: OutlineInputBorder(
//                                            borderRadius:
//                                                BorderRadius.circular(8))),
//                                    enabled: false,
//                                  ),
//                                ),
//                              ],
//                            ),
//                            SizedBox(
//                                height:
//                                    MediaQuery.of(context).size.height / 47),
//                            Row(
//                              children: [
//                                Expanded(
//                                  flex: 4,
//                                  child: TextFormField(
//                                    autovalidate:
//                                        formMAddGuarantorAddress.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    onChanged: (e) {
//                                      formMAddGuarantorAddress
//                                          .checkValidCodeArea1(e);
//                                    },
//                                    keyboardType: TextInputType.number,
//                                    inputFormatters: [
//                                      WhitelistingTextInputFormatter.digitsOnly,
//                                      // LengthLimitingTextInputFormatter(10),
//                                    ],
//                                    enabled: formMAddGuarantorAddress
//                                        .enableTfTelephone1Area,
//                                    controller: formMAddGuarantorAddress
//                                        .controllerTelephone1Area,
//                                    style: TextStyle(color: Colors.black),
//                                    decoration: InputDecoration(
//                                        filled: !formMAddGuarantorAddress
//                                            .enableTfTelephone1Area,
//                                        fillColor: !formMAddGuarantorAddress
//                                                .enableTfTelephone1Area
//                                            ? Colors.black12
//                                            : Colors.white,
//                                        labelText: 'Telepon 1 (Area)',
//                                        labelStyle:
//                                            TextStyle(color: Colors.black),
//                                        border: OutlineInputBorder(
//                                            borderRadius:
//                                                BorderRadius.circular(8))),
//                                  ),
//                                ),
//                                SizedBox(
//                                    width:
//                                        MediaQuery.of(context).size.width / 37),
//                                Expanded(
//                                  flex: 6,
//                                  child: TextFormField(
//                                    autovalidate:
//                                        formMAddGuarantorAddress.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    enabled:
//                                        formMAddGuarantorAddress.enableTfPhone1,
//                                    keyboardType: TextInputType.number,
//                                    inputFormatters: [
//                                      WhitelistingTextInputFormatter.digitsOnly,
//                                      // LengthLimitingTextInputFormatter(10),
//                                    ],
//                                    controller: formMAddGuarantorAddress
//                                        .controllerTelephone1,
//                                    style: new TextStyle(color: Colors.black),
//                                    decoration: new InputDecoration(
//                                        filled: !formMAddGuarantorAddress
//                                            .enableTfPhone1,
//                                        fillColor: !formMAddGuarantorAddress
//                                                .enableTfPhone1
//                                            ? Colors.black12
//                                            : Colors.white,
//                                        labelText: 'Telepon 1',
//                                        labelStyle:
//                                            TextStyle(color: Colors.black),
//                                        border: OutlineInputBorder(
//                                            borderRadius:
//                                                BorderRadius.circular(8))),
//                                  ),
//                                ),
//                              ],
//                            ),
//                            SizedBox(
//                                height:
//                                    MediaQuery.of(context).size.height / 47),
//                            Row(
//                              children: [
//                                Expanded(
//                                  flex: 4,
//                                  child: TextFormField(
//                                    autovalidate:
//                                        formMAddGuarantorAddress.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    onChanged: (e) {
//                                      formMAddGuarantorAddress
//                                          .checkValidCodeArea2(e);
//                                    },
//                                    keyboardType: TextInputType.number,
//                                    inputFormatters: [
//                                      WhitelistingTextInputFormatter.digitsOnly,
//                                      // LengthLimitingTextInputFormatter(10),
//                                    ],
//                                    enabled: formMAddGuarantorAddress
//                                        .enableTfTelephone2Area,
//                                    controller: formMAddGuarantorAddress
//                                        .controllerTelephone2Area,
//                                    style: new TextStyle(color: Colors.black),
//                                    decoration: InputDecoration(
//                                        filled: !formMAddGuarantorAddress
//                                            .enableTfTelephone2Area,
//                                        fillColor: !formMAddGuarantorAddress
//                                                .enableTfTelephone2Area
//                                            ? Colors.black12
//                                            : Colors.white,
//                                        labelText: 'Telepon 2 (Area)',
//                                        labelStyle:
//                                            TextStyle(color: Colors.black),
//                                        border: OutlineInputBorder(
//                                            borderRadius:
//                                                BorderRadius.circular(8))),
//                                  ),
//                                ),
//                                SizedBox(
//                                    width:
//                                        MediaQuery.of(context).size.width / 37),
//                                Expanded(
//                                  flex: 6,
//                                  child: TextFormField(
//                                    autovalidate:
//                                        formMAddGuarantorAddress.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    keyboardType: TextInputType.number,
//                                    inputFormatters: [
//                                      WhitelistingTextInputFormatter.digitsOnly,
//                                      // LengthLimitingTextInputFormatter(10),
//                                    ],
//                                    enabled:
//                                        formMAddGuarantorAddress.enableTfPhone2,
//                                    controller: formMAddGuarantorAddress
//                                        .controllerTelephone2,
//                                    style: new TextStyle(color: Colors.black),
//                                    decoration: new InputDecoration(
//                                        filled: !formMAddGuarantorAddress
//                                            .enableTfPhone2,
//                                        fillColor: !formMAddGuarantorAddress
//                                                .enableTfPhone2
//                                            ? Colors.black12
//                                            : Colors.white,
//                                        labelText: 'Telepon 2',
//                                        labelStyle:
//                                            TextStyle(color: Colors.black),
//                                        border: OutlineInputBorder(
//                                            borderRadius:
//                                                BorderRadius.circular(8))),
//                                  ),
//                                ),
//                              ],
//                            ),
//                            SizedBox(
//                                height:
//                                    MediaQuery.of(context).size.height / 47),
//                            Row(
//                              children: [
//                                Expanded(
//                                  flex: 4,
//                                  child: TextFormField(
//                                    autovalidate:
//                                        formMAddGuarantorAddress.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    onChanged: (e) {
//                                      formMAddGuarantorAddress
//                                          .checkValidCodeAreaFax(e);
//                                    },
//                                    keyboardType: TextInputType.number,
//                                    inputFormatters: [
//                                      WhitelistingTextInputFormatter.digitsOnly,
//                                      // LengthLimitingTextInputFormatter(10),
//                                    ],
//                                    enabled: formMAddGuarantorAddress
//                                        .enableTfFaxArea,
//                                    controller: formMAddGuarantorAddress
//                                        .controllerFaxArea,
//                                    style: new TextStyle(color: Colors.black),
//                                    decoration: InputDecoration(
//                                        filled: !formMAddGuarantorAddress
//                                            .enableTfFaxArea,
//                                        fillColor: !formMAddGuarantorAddress
//                                                .enableTfFaxArea
//                                            ? Colors.black12
//                                            : Colors.white,
//                                        labelText: 'Fax (Area)',
//                                        labelStyle:
//                                            TextStyle(color: Colors.black),
//                                        border: OutlineInputBorder(
//                                            borderRadius:
//                                                BorderRadius.circular(8))),
//                                  ),
//                                ),
//                                SizedBox(
//                                    width:
//                                        MediaQuery.of(context).size.width / 37),
//                                Expanded(
//                                  flex: 6,
//                                  child: TextFormField(
//                                    autovalidate:
//                                        formMAddGuarantorAddress.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    keyboardType: TextInputType.number,
//                                    inputFormatters: [
//                                      WhitelistingTextInputFormatter.digitsOnly,
//                                      // LengthLimitingTextInputFormatter(10),
//                                    ],
//                                    enabled:
//                                        formMAddGuarantorAddress.enableTfFax,
//                                    controller:
//                                        formMAddGuarantorAddress.controllerFax,
//                                    style: new TextStyle(color: Colors.black),
//                                    decoration: new InputDecoration(
//                                        filled: !formMAddGuarantorAddress
//                                            .enableTfFax,
//                                        fillColor: !formMAddGuarantorAddress
//                                                .enableTfFax
//                                            ? Colors.black12
//                                            : Colors.white,
//                                        labelText: 'Fax',
//                                        labelStyle:
//                                            TextStyle(color: Colors.black),
//                                        border: OutlineInputBorder(
//                                            borderRadius:
//                                                BorderRadius.circular(8))),
//                                  ),
//                                ),
//                              ],
//                            ),
//                          ],
//                        ),
//                      );
//                    },
//                  )
//                : FutureBuilder(
//                    future: _setValueForEdit,
//                    builder: (context, snapshot) {
//                      if (snapshot.connectionState == ConnectionState.waiting) {
//                        return Center(
//                          child: CircularProgressIndicator(),
//                        );
//                      }
//                      return Consumer<
//                          FormMAddGuarantorAddressCompanyChangeNotifier>(
//                        builder: (context, formMAddGuarantorAddress, _) {
//                          return Form(
//                            key: formMAddGuarantorAddress.key,
//                            onWillPop: _onWillPop,
//                            child: Column(
//                              crossAxisAlignment: CrossAxisAlignment.start,
//                              children: [
//                                DropdownButtonFormField<JenisAlamatModel>(
//                                    autovalidate:
//                                        formMAddGuarantorAddress.autoValidate,
//                                    validator: (e) {
//                                      if (e == null) {
//                                        return "Silahkan pilih jenis alamat";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    value: formMAddGuarantorAddress
//                                        .jenisAlamatSelected,
//                                    onChanged: (value) {
//                                      formMAddGuarantorAddress
//                                          .jenisAlamatSelected = value;
//                                    },
//                                    onTap: () {
//                                      FocusManager.instance.primaryFocus.unfocus();
//                                    },
//                                    decoration: InputDecoration(
//                                      labelText: "Jenis Alamat",
//                                      border: OutlineInputBorder(),
//                                      contentPadding:
//                                          EdgeInsets.symmetric(horizontal: 10),
//                                    ),
//                                    items: formMAddGuarantorAddress
//                                        .listJenisAlamat
//                                        .map((value) {
//                                      return DropdownMenuItem<JenisAlamatModel>(
//                                        value: value,
//                                        child: Text(
//                                          value.DESKRIPSI,
//                                          overflow: TextOverflow.ellipsis,
//                                        ),
//                                      );
//                                    }).toList()),
//                                formMAddGuarantorAddress.jenisAlamatSelected !=
//                                        null
//                                    ? formMAddGuarantorAddress
//                                                .jenisAlamatSelected.KODE ==
//                                            "02"
//                                        ? Row(
//                                            children: [
//                                              Checkbox(
//                                                  value:
//                                                      formMAddGuarantorAddress
//                                                          .isSameWithIdentity,
//                                                  onChanged: (value) {
//                                                    formMAddGuarantorAddress
//                                                            .isSameWithIdentity =
//                                                        value;
//                                                    formMAddGuarantorAddress
//                                                        .setValueIsSameWithIdentity(
//                                                            value,
//                                                            context,
//                                                            null);
//                                                  },
//                                                  activeColor: myPrimaryColor),
//                                              SizedBox(width: 8),
//                                              Text("Sama dengan identitas")
//                                            ],
//                                          )
//                                        : SizedBox(
//                                            height: MediaQuery.of(context)
//                                                    .size
//                                                    .height /
//                                                47)
//                                    : SizedBox(
//                                        height:
//                                            MediaQuery.of(context).size.height /
//                                                47),
//                                TextFormField(
//                                  autovalidate:
//                                      formMAddGuarantorAddress.autoValidate,
//                                  validator: (e) {
//                                    if (e.isEmpty) {
//                                      return "Tidak boleh kosong";
//                                    } else {
//                                      return null;
//                                    }
//                                  },
//                                  controller:
//                                      formMAddGuarantorAddress.controllerAlamat,
//                                  enabled:
//                                      formMAddGuarantorAddress.enableTfAddress,
//                                  style: TextStyle(color: Colors.black),
//                                  decoration: InputDecoration(
//                                      filled: !formMAddGuarantorAddress
//                                          .enableTfAddress,
//                                      fillColor: !formMAddGuarantorAddress
//                                              .enableTfAddress
//                                          ? Colors.black12
//                                          : Colors.white,
//                                      labelText: 'Alamat',
//                                      labelStyle:
//                                          TextStyle(color: Colors.black),
//                                      border: OutlineInputBorder(
//                                          borderRadius:
//                                              BorderRadius.circular(8))),
//                                  keyboardType: TextInputType.text,
//                                  textCapitalization: TextCapitalization.characters,
//                                  maxLines: 3,
//                                ),
//                                SizedBox(
//                                    height: MediaQuery.of(context).size.height /
//                                        47),
//                                Row(
//                                  children: [
//                                    Expanded(
//                                      flex: 5,
//                                      child: TextFormField(
//                                        autovalidate: formMAddGuarantorAddress
//                                            .autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        enabled:
//                                            formMAddGuarantorAddress.enableTfRT,
//                                        controller: formMAddGuarantorAddress
//                                            .controllerRT,
//                                        style: TextStyle(color: Colors.black),
//                                        decoration: InputDecoration(
//                                            filled: !formMAddGuarantorAddress
//                                                .enableTfRT,
//                                            fillColor: !formMAddGuarantorAddress
//                                                    .enableTfRT
//                                                ? Colors.black12
//                                                : Colors.white,
//                                            labelText: 'RT',
//                                            labelStyle:
//                                                TextStyle(color: Colors.black),
//                                            border: OutlineInputBorder(
//                                                borderRadius:
//                                                    BorderRadius.circular(8))),
//                                        inputFormatters: [
//                                          WhitelistingTextInputFormatter.digitsOnly,
//                                          // LengthLimitingTextInputFormatter(10),
//                                        ],
//                                        keyboardType: TextInputType.number,
//                                      ),
//                                    ),
//                                    SizedBox(width: 8),
//                                    Expanded(
//                                      flex: 5,
//                                      child: TextFormField(
//                                        autovalidate: formMAddGuarantorAddress
//                                            .autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        enabled:
//                                            formMAddGuarantorAddress.enableTfRW,
//                                        controller: formMAddGuarantorAddress
//                                            .controllerRW,
//                                        style: TextStyle(color: Colors.black),
//                                        decoration: InputDecoration(
//                                            filled: !formMAddGuarantorAddress
//                                                .enableTfRW,
//                                            fillColor: !formMAddGuarantorAddress
//                                                    .enableTfRW
//                                                ? Colors.black12
//                                                : Colors.white,
//                                            labelText: 'RW',
//                                            labelStyle:
//                                                TextStyle(color: Colors.black),
//                                            border: OutlineInputBorder(
//                                                borderRadius:
//                                                    BorderRadius.circular(8))),
//                                        inputFormatters: [
//                                          WhitelistingTextInputFormatter.digitsOnly,
//                                          // LengthLimitingTextInputFormatter(10),
//                                        ],
//                                        keyboardType: TextInputType.number,
//                                      ),
//                                    )
//                                  ],
//                                ),
//                                SizedBox(
//                                    height: MediaQuery.of(context).size.height /
//                                        47),
//                                FocusScope(
//                                  node: FocusScopeNode(),
//                                  child: TextFormField(
//                                    autovalidate:
//                                        formMAddGuarantorAddress.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    onTap: () {
//                                      FocusManager.instance.primaryFocus.unfocus();
//                                      formMAddGuarantorAddress
//                                          .searchKelurahan(context);
//                                    },
//                                    enabled: formMAddGuarantorAddress
//                                        .enableTfKelurahan,
//                                    controller: formMAddGuarantorAddress
//                                        .controllerKelurahan,
//                                    style: TextStyle(color: Colors.black),
//                                    decoration: InputDecoration(
//                                        filled: !formMAddGuarantorAddress
//                                            .enableTfKelurahan,
//                                        fillColor: !formMAddGuarantorAddress
//                                                .enableTfKelurahan
//                                            ? Colors.black12
//                                            : Colors.white,
//                                        labelText: 'Kelurahan',
//                                        labelStyle:
//                                            TextStyle(color: Colors.black),
//                                        border: OutlineInputBorder(
//                                            borderRadius:
//                                                BorderRadius.circular(8))),
//                                  ),
//                                ),
//                                SizedBox(
//                                    height: MediaQuery.of(context).size.height /
//                                        47),
//                                TextFormField(
//                                  autovalidate:
//                                      formMAddGuarantorAddress.autoValidate,
//                                  validator: (e) {
//                                    if (e.isEmpty) {
//                                      return "Tidak boleh kosong";
//                                    } else {
//                                      return null;
//                                    }
//                                  },
//                                  // enabled: formMAddGuarantorAddress.enableTfKecamatan,
//                                  controller: formMAddGuarantorAddress
//                                      .controllerKecamatan,
//                                  style: new TextStyle(color: Colors.black),
//                                  decoration: new InputDecoration(
//                                      filled: !formMAddGuarantorAddress
//                                          .enableTfKecamatan,
//                                      fillColor: !formMAddGuarantorAddress
//                                              .enableTfKecamatan
//                                          ? Colors.black12
//                                          : Colors.white,
//                                      labelText: 'Kecamatan',
//                                      labelStyle:
//                                          TextStyle(color: Colors.black),
//                                      border: OutlineInputBorder(
//                                          borderRadius:
//                                              BorderRadius.circular(8))),
//                                  enabled: false,
//                                ),
//                                SizedBox(
//                                    height: MediaQuery.of(context).size.height /
//                                        47),
//                                TextFormField(
//                                  autovalidate:
//                                      formMAddGuarantorAddress.autoValidate,
//                                  validator: (e) {
//                                    if (e.isEmpty) {
//                                      return "Tidak boleh kosong";
//                                    } else {
//                                      return null;
//                                    }
//                                  },
//                                  // enabled: formMAddGuarantorAddress.enableTfKota,
//                                  controller:
//                                      formMAddGuarantorAddress.controllerKota,
//                                  style: new TextStyle(color: Colors.black),
//                                  decoration: new InputDecoration(
//                                      filled: !formMAddGuarantorAddress
//                                          .enableTfKota,
//                                      fillColor:
//                                          !formMAddGuarantorAddress.enableTfKota
//                                              ? Colors.black12
//                                              : Colors.white,
//                                      labelText: 'Kabupaten/Kota',
//                                      labelStyle:
//                                          TextStyle(color: Colors.black),
//                                      border: OutlineInputBorder(
//                                          borderRadius:
//                                              BorderRadius.circular(8))),
//                                  enabled: false,
//                                ),
//                                SizedBox(
//                                    height: MediaQuery.of(context).size.height /
//                                        47),
//                                Row(
//                                  children: [
//                                    Expanded(
//                                      flex: 7,
//                                      child: TextFormField(
//                                        autovalidate: formMAddGuarantorAddress
//                                            .autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        // enabled: formMAddGuarantorAddress.enableTfProv,
//                                        controller: formMAddGuarantorAddress
//                                            .controllerProvinsi,
//                                        style:
//                                            new TextStyle(color: Colors.black),
//                                        decoration: new InputDecoration(
//                                            filled: !formMAddGuarantorAddress
//                                                .enableTfProv,
//                                            fillColor: !formMAddGuarantorAddress
//                                                    .enableTfProv
//                                                ? Colors.black12
//                                                : Colors.white,
//                                            labelText: 'Provinsi',
//                                            labelStyle:
//                                                TextStyle(color: Colors.black),
//                                            border: OutlineInputBorder(
//                                                borderRadius:
//                                                    BorderRadius.circular(8))),
//                                        enabled: false,
//                                      ),
//                                    ),
//                                    SizedBox(width: 8),
//                                    Expanded(
//                                      flex: 3,
//                                      child: TextFormField(
//                                        autovalidate: formMAddGuarantorAddress
//                                            .autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        // enabled: formMAddGuarantorAddress.enableTfPostalCode,
//                                        controller: formMAddGuarantorAddress
//                                            .controllerPostalCode,
//                                        style:
//                                            new TextStyle(color: Colors.black),
//                                        decoration: new InputDecoration(
//                                            filled: !formMAddGuarantorAddress
//                                                .enableTfPostalCode,
//                                            fillColor: !formMAddGuarantorAddress
//                                                    .enableTfPostalCode
//                                                ? Colors.black12
//                                                : Colors.white,
//                                            labelText: 'Kode Pos',
//                                            labelStyle:
//                                                TextStyle(color: Colors.black),
//                                            border: OutlineInputBorder(
//                                                borderRadius:
//                                                    BorderRadius.circular(8))),
//                                        enabled: false,
//                                      ),
//                                    ),
//                                  ],
//                                ),
//                                SizedBox(
//                                    height: MediaQuery.of(context).size.height /
//                                        47),
//                                Row(
//                                  children: [
//                                    Expanded(
//                                      flex: 4,
//                                      child: TextFormField(
//                                        autovalidate: formMAddGuarantorAddress
//                                            .autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        onChanged: (e) {
//                                          formMAddGuarantorAddress
//                                              .checkValidCodeArea1(e);
//                                        },
//                                        keyboardType: TextInputType.number,
//                                        inputFormatters: [
//                                          WhitelistingTextInputFormatter
//                                              .digitsOnly,
//                                          LengthLimitingTextInputFormatter(10),
//                                        ],
//                                        enabled: formMAddGuarantorAddress
//                                            .enableTfTelephone1Area,
//                                        controller: formMAddGuarantorAddress
//                                            .controllerTelephone1Area,
//                                        style: TextStyle(color: Colors.black),
//                                        decoration: InputDecoration(
//                                            filled: !formMAddGuarantorAddress
//                                                .enableTfTelephone1Area,
//                                            fillColor: !formMAddGuarantorAddress
//                                                    .enableTfTelephone1Area
//                                                ? Colors.black12
//                                                : Colors.white,
//                                            labelText: 'Telepon 1 (Area)',
//                                            labelStyle:
//                                                TextStyle(color: Colors.black),
//                                            border: OutlineInputBorder(
//                                                borderRadius:
//                                                    BorderRadius.circular(8))),
//                                      ),
//                                    ),
//                                    SizedBox(
//                                        width:
//                                            MediaQuery.of(context).size.width /
//                                                37),
//                                    Expanded(
//                                      flex: 6,
//                                      child: TextFormField(
//                                        autovalidate: formMAddGuarantorAddress
//                                            .autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        enabled: formMAddGuarantorAddress
//                                            .enableTfPhone1,
//                                        keyboardType: TextInputType.number,
//                                        inputFormatters: [
//                                          WhitelistingTextInputFormatter
//                                              .digitsOnly,
//                                          // LengthLimitingTextInputFormatter(10),
//                                        ],
//                                        controller: formMAddGuarantorAddress
//                                            .controllerTelephone1,
//                                        style:
//                                            new TextStyle(color: Colors.black),
//                                        decoration: new InputDecoration(
//                                            filled: !formMAddGuarantorAddress
//                                                .enableTfPhone1,
//                                            fillColor: !formMAddGuarantorAddress
//                                                    .enableTfPhone1
//                                                ? Colors.black12
//                                                : Colors.white,
//                                            labelText: 'Telepon 1',
//                                            labelStyle:
//                                                TextStyle(color: Colors.black),
//                                            border: OutlineInputBorder(
//                                                borderRadius:
//                                                    BorderRadius.circular(8))),
//                                      ),
//                                    ),
//                                  ],
//                                ),
//                                SizedBox(
//                                    height: MediaQuery.of(context).size.height /
//                                        47),
//                                Row(
//                                  children: [
//                                    Expanded(
//                                      flex: 4,
//                                      child: TextFormField(
//                                        autovalidate: formMAddGuarantorAddress
//                                            .autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        onChanged: (e) {
//                                          formMAddGuarantorAddress
//                                              .checkValidCodeArea2(e);
//                                        },
//                                        keyboardType: TextInputType.number,
//                                        inputFormatters: [
//                                          WhitelistingTextInputFormatter
//                                              .digitsOnly,
//                                          // LengthLimitingTextInputFormatter(10),
//                                        ],
//                                        enabled: formMAddGuarantorAddress
//                                            .enableTfTelephone2Area,
//                                        controller: formMAddGuarantorAddress
//                                            .controllerTelephone2Area,
//                                        style:
//                                            new TextStyle(color: Colors.black),
//                                        decoration: InputDecoration(
//                                            filled: !formMAddGuarantorAddress
//                                                .enableTfTelephone2Area,
//                                            fillColor: !formMAddGuarantorAddress
//                                                    .enableTfTelephone2Area
//                                                ? Colors.black12
//                                                : Colors.white,
//                                            labelText: 'Telepon 2 (Area)',
//                                            labelStyle:
//                                                TextStyle(color: Colors.black),
//                                            border: OutlineInputBorder(
//                                                borderRadius:
//                                                    BorderRadius.circular(8))),
//                                      ),
//                                    ),
//                                    SizedBox(
//                                        width:
//                                            MediaQuery.of(context).size.width /
//                                                37),
//                                    Expanded(
//                                      flex: 6,
//                                      child: TextFormField(
//                                        autovalidate: formMAddGuarantorAddress
//                                            .autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        keyboardType: TextInputType.number,
//                                        inputFormatters: [
//                                          WhitelistingTextInputFormatter
//                                              .digitsOnly,
//                                          // LengthLimitingTextInputFormatter(10),
//                                        ],
//                                        enabled: formMAddGuarantorAddress
//                                            .enableTfPhone2,
//                                        controller: formMAddGuarantorAddress
//                                            .controllerTelephone2,
//                                        style:
//                                            new TextStyle(color: Colors.black),
//                                        decoration: new InputDecoration(
//                                            filled: !formMAddGuarantorAddress
//                                                .enableTfPhone2,
//                                            fillColor: !formMAddGuarantorAddress
//                                                    .enableTfPhone2
//                                                ? Colors.black12
//                                                : Colors.white,
//                                            labelText: 'Telepon 2',
//                                            labelStyle:
//                                                TextStyle(color: Colors.black),
//                                            border: OutlineInputBorder(
//                                                borderRadius:
//                                                    BorderRadius.circular(8))),
//                                      ),
//                                    ),
//                                  ],
//                                ),
//                                SizedBox(
//                                    height: MediaQuery.of(context).size.height /
//                                        47),
//                                Row(
//                                  children: [
//                                    Expanded(
//                                      flex: 4,
//                                      child: TextFormField(
//                                        autovalidate: formMAddGuarantorAddress
//                                            .autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        onChanged: (e) {
//                                          formMAddGuarantorAddress
//                                              .checkValidCodeAreaFax(e);
//                                        },
//                                        keyboardType: TextInputType.number,
//                                        inputFormatters: [
//                                          WhitelistingTextInputFormatter
//                                              .digitsOnly,
//                                          LengthLimitingTextInputFormatter(10),
//                                        ],
//                                        enabled: formMAddGuarantorAddress
//                                            .enableTfFaxArea,
//                                        controller: formMAddGuarantorAddress
//                                            .controllerFaxArea,
//                                        style:
//                                            new TextStyle(color: Colors.black),
//                                        decoration: InputDecoration(
//                                            filled: !formMAddGuarantorAddress
//                                                .enableTfFaxArea,
//                                            fillColor: !formMAddGuarantorAddress
//                                                    .enableTfFaxArea
//                                                ? Colors.black12
//                                                : Colors.white,
//                                            labelText: 'Fax (Area)',
//                                            labelStyle:
//                                                TextStyle(color: Colors.black),
//                                            border: OutlineInputBorder(
//                                                borderRadius:
//                                                    BorderRadius.circular(8))),
//                                      ),
//                                    ),
//                                    SizedBox(
//                                        width:
//                                            MediaQuery.of(context).size.width /
//                                                37),
//                                    Expanded(
//                                      flex: 6,
//                                      child: TextFormField(
//                                        autovalidate: formMAddGuarantorAddress
//                                            .autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        keyboardType: TextInputType.number,
//                                        inputFormatters: [
//                                          WhitelistingTextInputFormatter
//                                              .digitsOnly,
//                                          LengthLimitingTextInputFormatter(10),
//                                        ],
//                                        enabled: formMAddGuarantorAddress
//                                            .enableTfFax,
//                                        controller: formMAddGuarantorAddress
//                                            .controllerFax,
//                                        style:
//                                            new TextStyle(color: Colors.black),
//                                        decoration: new InputDecoration(
//                                            filled: !formMAddGuarantorAddress
//                                                .enableTfFax,
//                                            fillColor: !formMAddGuarantorAddress
//                                                    .enableTfFax
//                                                ? Colors.black12
//                                                : Colors.white,
//                                            labelText: 'Fax',
//                                            labelStyle:
//                                                TextStyle(color: Colors.black),
//                                            border: OutlineInputBorder(
//                                                borderRadius:
//                                                    BorderRadius.circular(8))),
//                                      ),
//                                    ),
//                                  ],
//                                ),
//                              ],
//                            ),
//                          );
//                        },
//                      );
//                    })),
//        bottomNavigationBar: BottomAppBar(
//          elevation: 0.0,
//          child: Padding(
//              padding: const EdgeInsets.all(8.0),
//              child: RaisedButton(
//                  shape: RoundedRectangleBorder(
//                      borderRadius: new BorderRadius.circular(8.0)),
//                  color: myPrimaryColor,
//                  onPressed: () {
//                    if (widget.flag == 0) {
//                      Provider.of<FormMAddGuarantorAddressCompanyChangeNotifier>(
//                              context,
//                              listen: true)
//                          .check(context, widget.flag, null);
//                    } else {
//                      Provider.of<FormMAddGuarantorAddressCompanyChangeNotifier>(
//                              context,
//                              listen: true)
//                          .check(context, widget.flag, widget.index);
//                    }
//                  },
//                  child: Row(
//                    mainAxisAlignment: MainAxisAlignment.center,
//                    children: <Widget>[
//                      Text(widget.flag == 0 ? "SAVE" : "UPDATE",
//                          style: TextStyle(
//                              color: Colors.black,
//                              fontSize: 14,
//                              fontWeight: FontWeight.w500,
//                              letterSpacing: 1.25))
//                    ],
//                  ))),
//        ),
//      ),
    );
  }

//  Future<bool> _onWillPop() async {
//    var _provider = Provider.of<FormMAddGuarantorAddressCompanyChangeNotifier>(
//        context,
//        listen: false);
//    if (widget.flag == 0) {
//      if (_provider.jenisAlamatSelected != null ||
//          _provider.controllerAlamat.text != "" ||
//          _provider.controllerRT.text != "" ||
//          _provider.controllerRW.text != "" ||
//          _provider.controllerKelurahan.text != "" ||
//          _provider.controllerKecamatan.text != "" ||
//          _provider.controllerKota.text != "" ||
//          _provider.controllerProvinsi.text != "" ||
//          _provider.controllerPostalCode.text != "" ||
//          _provider.controllerTelephone1Area.text != "" ||
//          _provider.controllerTelephone1.text != "" ||
//          _provider.controllerTelephone2Area.text != "" ||
//          _provider.controllerTelephone2.text != "" ||
//          _provider.controllerFaxArea.text != "" ||
//          _provider.controllerFax.text != "") {
//        return (await showDialog(
//              context: context,
//              builder: (myContext) => AlertDialog(
//                title: new Text('Warning'),
//                content: new Text('Simpan perubahan?'),
//                actions: <Widget>[
//                  new FlatButton(
//                    onPressed: () {
//                      _provider.check(context, widget.flag, null);
//                      Navigator.pop(context);
//                    },
//                    child: new Text('Ya', style: TextStyle(color: Colors.grey),),
//                  ),
//                  new FlatButton(
//                    onPressed: () => Navigator.of(context).pop(true),
//                    child: new Text('Tidak'),
//                  ),
//                ],
//              ),
//            )) ??
//            false;
//      } else {
//        return true;
//      }
//    } else {
//      if (_provider.jenisAlamatSelectedTemp.KODE !=
//              _provider.jenisAlamatSelected.KODE ||
//          _provider.alamatTemp != _provider.controllerAlamat.text ||
//          _provider.rtTemp != _provider.controllerRT.text ||
//          _provider.rwTemp != _provider.controllerRW.text ||
//          _provider.kelurahanTemp != _provider.controllerKelurahan.text ||
//          _provider.kecamatanTemp != _provider.controllerKecamatan.text ||
//          _provider.kotaTemp != _provider.controllerKota.text ||
//          _provider.provinsiTemp != _provider.controllerProvinsi.text ||
//          _provider.postalCodeTemp != _provider.controllerPostalCode.text ||
//          _provider.phone1AreaTemp != _provider.controllerTelephone1Area.text ||
//          _provider.phone1Temp != _provider.controllerTelephone1.text ||
//          _provider.phone2AreaTemp != _provider.controllerTelephone2Area.text ||
//          _provider.phone2Temp != _provider.controllerTelephone2.text ||
//          _provider.faxAreaTemp != _provider.controllerFaxArea.text ||
//          _provider.faxTemp != _provider.controllerFax.text) {
//        return (await showDialog(
//              context: context,
//              builder: (myContext) => AlertDialog(
//                title: new Text('Warning'),
//                content: new Text('Simpan perubahan?'),
//                actions: <Widget>[
//                  new FlatButton(
//                    onPressed: () {
//                      _provider.check(context, widget.flag, widget.index);
//                      Navigator.pop(context);
//                    },
//                    child: new Text('Ya', style: TextStyle(color: Colors.grey),),
//                  ),
//                  new FlatButton(
//                    onPressed: () => Navigator.of(context).pop(true),
//                    child: new Text('Tidak'),
//                  ),
//                ],
//              ),
//            )) ??
//            false;
//      } else {
//        return true;
//      }
//    }
//  }
}
