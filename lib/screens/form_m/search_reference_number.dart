import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/search_birth_place_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_reference_number_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchReferenceNumber extends StatefulWidget { //ga kepake
  @override
  _SearchReferenceNumberState createState() => _SearchReferenceNumberState();
}

class _SearchReferenceNumberState extends State<SearchReferenceNumber> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: Provider.of<SearchReferenceNumberChangeNotifier>(context,listen: false).scaffoldKey,
      appBar: AppBar(
        title: Consumer<SearchReferenceNumberChangeNotifier>(
          builder: (context, searchReferenceNumberChangeNotifier, _) {
            return TextFormField(
              controller: searchReferenceNumberChangeNotifier.controllerSearch,
              style: TextStyle(color: Colors.black),
              textInputAction: TextInputAction.search,
              onFieldSubmitted: (e) {
                searchReferenceNumberChangeNotifier.getReferenceNumber(e,context);
              },
              onChanged: (e) {
                searchReferenceNumberChangeNotifier.changeAction(e);
              },
              cursorColor: Colors.black,
              decoration: new InputDecoration(
                hintText: "Cari Reference Number (minimal 3 karakter)",
                hintStyle: TextStyle(color: Colors.black),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: myPrimaryColor),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: myPrimaryColor),
                ),
              ),
              textCapitalization: TextCapitalization.characters,
              autofocus: true,
            );
          },
        ),
        backgroundColor: myPrimaryColor,
        iconTheme: IconThemeData(color: Colors.black),
        actions: <Widget>[
          Provider.of<SearchReferenceNumberChangeNotifier>(context, listen: true)
              .showClear
              ? IconButton(
              icon: Icon(Icons.clear),
              onPressed: () {
                Provider.of<SearchReferenceNumberChangeNotifier>(context, listen: false).controllerSearch.clear();
                Provider.of<SearchReferenceNumberChangeNotifier>(context, listen: false)
                    .changeAction(Provider.of<SearchReferenceNumberChangeNotifier>(context, listen: false)
                    .controllerSearch.text);
              })
              : SizedBox(
            width: 0.0,
            height: 0.0,
          )
        ],
      ),
      body: Consumer<SearchReferenceNumberChangeNotifier>(
        builder: (context, searchReferenceNumberChangeNotifier, _) {
          if (searchReferenceNumberChangeNotifier.loadData == true) {
            return Center(
              child: CircularProgressIndicator(
                valueColor: new AlwaysStoppedAnimation<Color>(primaryOrange),
              ),
            );
          }
          else{
            return ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount: searchReferenceNumberChangeNotifier.listReferenceNumber.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(context,
                        searchReferenceNumberChangeNotifier.listReferenceNumber[index]);
                  },
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "${searchReferenceNumberChangeNotifier.listReferenceNumber[index].noPK}",
                          style: TextStyle(fontSize: 16),
                        ),
                        Text(
                            "${searchReferenceNumberChangeNotifier.listReferenceNumber[index].custName}",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          }
        },
      ),
    );
  }
}
