import 'package:ad1ms2_dev/models/address_guarantor_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/screens/form_m/add_guarantor_individual.dart';
import 'package:ad1ms2_dev/shared/form_m_add_guarantor_individual_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_guarantor_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class ListGuarantorIndividual extends StatefulWidget {
  @override
  _ListGuarantorIndividualState createState() => _ListGuarantorIndividualState();
}

class _ListGuarantorIndividualState extends State<ListGuarantorIndividual> {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        accentColor: myPrimaryColor
      ),
      child: Scaffold(
        appBar: AppBar(
          title: Text("List Penjamin Pribadi",
              style: TextStyle(color: Colors.black)),
          centerTitle: true,
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: Consumer<FormMGuarantorChangeNotifier>(
          builder: (context, formMGuarantorChangeNotifier, _) {
            return formMGuarantorChangeNotifier.listGuarantorIndividual.isEmpty
                ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // Image.asset("img/alamat_kosong.png", height: MediaQuery.of(context).size.height / 9,),
                  // SizedBox(height: MediaQuery.of(context).size.height / 47,),
                  Text("Tambah Penjamin", style: TextStyle(color: Colors.grey, fontSize: 16),)
                ],
              ),
            )
            : ListView.builder(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 47,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemBuilder: (context, index) {
                return InkWell(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(
                            builder: (context) => AddGuarantorIndividual(
                                flag: 1,
                                index: index,
                                model: formMGuarantorChangeNotifier.listGuarantorIndividual[index])
//                              ChangeNotifierProvider(
//                              create: (context) =>
//                                  FormMAddAddressGuarantorIndividualChangeNotifier(),
//                              child: AddGuarantorAddressIndividual(
//                                  flag: 1,
//                                  index: index,
//                                  addressModel:
//                                  formMAddGuarantorChangeNotifier
//                                      .listGuarantorAddress[
//                                  index]))
                            ));
                  },
                  child: Container(
                    child: Card(
                      shape: formMGuarantorChangeNotifier.listGuarantorIndividual[index].isEditList
                          ? RoundedRectangleBorder(
                          side: BorderSide(color: Colors.purple, width: 2),
                          borderRadius: BorderRadius.all(Radius.circular(4)))
                          : null,
//                  shape: formMAddGuarantorChangeNotifier.selectedIndex ==
//                      index
//                      ? RoundedRectangleBorder(
//                      side:
//                      BorderSide(color: myPrimaryColor, width: 2),
//                      borderRadius:
//                      BorderRadius.all(Radius.circular(4)))
//                      : null,
                      elevation: 3.3,
                      child: Padding(
                        padding: const EdgeInsets.all(13.0),
                        child: Stack(
                          children: [
                            Column(
                              children: [
                                Row(
                                  children: [
                                    Expanded(flex: 4, child: Text("Status Hubungan")),
                                    Text(" : "),
                                    Expanded(
                                        flex: 6,
                                        child: Text(formMGuarantorChangeNotifier.listGuarantorIndividual[index].relationshipStatusModel.PARA_FAMILY_TYPE_NAME))
                                  ],
                                ),
                                Row(
                                  children: [
                                    Expanded(flex: 4, child: Text("Nama Penjamin")),
                                    Text(" : "),
                                    Expanded(
                                        flex: 6,
                                        child: Text(formMGuarantorChangeNotifier.listGuarantorIndividual[index].fullNameIdentity))
                                  ],
                                ),
                                // Row(
                                //   children: [
                                //     Expanded(flex: 4, child: Text("Alamat")),
                                //     Text(" : "),
                                //     Expanded(
                                //         flex: 6,
                                //         child: _widget(formMGuarantorChangeNotifier.listGuarantorIndividual[index].listAddressGuarantorModel))
                                //   ],
                                // ),
                              ],
                            ),
                            Align(
                                alignment: Alignment.topRight,
                                child: IconButton(
                                    icon: Icon(Icons.delete, color: Colors.red),
                                    onPressed: () {
                                      formMGuarantorChangeNotifier.deleteListGuarantorIndividual(context, index);
                                    }
                                )
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
              itemCount: formMGuarantorChangeNotifier.listGuarantorIndividual.length,
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(
                      builder: (context) => AddGuarantorIndividual(
                          flag: 0, index: null, model: null)
                      ));
            },
            child: Icon(Icons.add, color: Colors.black),
            backgroundColor: myPrimaryColor),
      ),
    );
  }

  Widget _widget(List<AddressModel> model) {
    String _data = "";
    for (int i = 0; i < model.length; i++) {
      if (model[i].isCorrespondence) {
        _data = model[i].address;
      }
    }
    return Text(_data);
  }
}
