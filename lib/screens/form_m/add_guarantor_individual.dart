import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/models/guarantor_idividual_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_add_guarantor_individual_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';
import 'list_guarantor_address_individual.dart';

class AddGuarantorIndividual extends StatefulWidget {
  final int flag;
  final int index;
  final GuarantorIndividualModel model;
  const AddGuarantorIndividual({this.flag, this.index, this.model});
  @override
  _AddGuarantorIndividualState createState() => _AddGuarantorIndividualState();
}

class _AddGuarantorIndividualState extends State<AddGuarantorIndividual> {
  Future<void> _setValueForEdit;

  @override
  void initState() {
    super.initState();
    _setValueForEdit = Provider.of<FormMAddGuarantorIndividualChangeNotifier>(context,listen: false).setValueForEdit(context, widget.model, widget.flag, widget.index);
    Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setDataAddressIndividu(context);
  }

  @override
  Widget build(BuildContext context) {
    var _data = Provider.of<FormMAddGuarantorIndividualChangeNotifier>(context,listen: false);
    return Theme(
      data: ThemeData(
        primaryColor: Colors.black,
        fontFamily: "NunitoSans",
        primarySwatch: primaryOrange,
        accentColor: myPrimaryColor,
      ),
      child: Scaffold(
        key: Provider.of<FormMAddGuarantorIndividualChangeNotifier>(context, listen: false).scaffoldKey,
        appBar: AppBar(
          title: Text(
              widget.flag == 0
                  ? "Tambah Penjamin Pribadi"
                  : "Edit Penjamin Pribadi",
              style: TextStyle(color: Colors.black)),
          centerTitle: true,
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: SingleChildScrollView(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width / 27,
                vertical: MediaQuery.of(context).size.height / 57),
            child:
//             widget.flag == 0
//                 ? Consumer<FormMAddGuarantorIndividualChangeNotifier>(
//                     builder: (context,
//                         _formMAddGuarantorIndividualChangeNotifier, _) {
//                       return Form(
//                         key: _formMAddGuarantorIndividualChangeNotifier.key,
//                         onWillPop: _onWillPop,
//                         child: Column(
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           children: [
//                             DropdownButtonFormField<RelationshipStatusModel>(
//                                 autovalidate:
//                                     _formMAddGuarantorIndividualChangeNotifier
//                                         .autoValidate,
//                                 validator: (e) {
//                                   if (e == null) {
//                                     return "Silahkan pilih status hubungan";
//                                   } else {
//                                     return null;
//                                   }
//                                 },
//                                 value:
//                                     _formMAddGuarantorIndividualChangeNotifier
//                                         .relationshipStatusModelSelected,
//                                 onChanged: (value) {
//                                   _formMAddGuarantorIndividualChangeNotifier
//                                       .relationshipStatusModelSelected = value;
//                                 },
//                                 onTap: () {
//                                   FocusManager.instance.primaryFocus.unfocus();
//                                 },
//                                 decoration: InputDecoration(
//                                   labelText: "Status Hubungan ",
//                                   border: OutlineInputBorder(),
//                                   contentPadding:
//                                       EdgeInsets.symmetric(horizontal: 10),
//                                 ),
//                                 items:
//                                     _formMAddGuarantorIndividualChangeNotifier
//                                         .listRelationShipStatus
//                                         .map((value) {
//                                   return DropdownMenuItem<
//                                       RelationshipStatusModel>(
//                                     value: value,
//                                     child: Text(
//                                       value.PARA_FAMILY_TYPE_NAME,
//                                       overflow: TextOverflow.ellipsis,
//                                     ),
//                                   );
//                                 }).toList()),
//                             SizedBox(
//                                 height:
//                                     MediaQuery.of(context).size.height / 47),
//                             DropdownButtonFormField<IdentityModel>(
//                                 autovalidate:
//                                     _formMAddGuarantorIndividualChangeNotifier
//                                         .autoValidate,
//                                 validator: (e) {
//                                   if (e == null) {
//                                     return "Silahkan pilih jenis identitas";
//                                   } else {
//                                     return null;
//                                   }
//                                 },
//                                 value:
//                                     _formMAddGuarantorIndividualChangeNotifier
//                                         .identityModelSelected,
//                                 onChanged: (value) {
//                                   _formMAddGuarantorIndividualChangeNotifier
//                                       .identityModelSelected = value;
//                                 },
//                                 onTap: () {
//                                   FocusManager.instance.primaryFocus.unfocus();
//                                 },
//                                 decoration: InputDecoration(
//                                   labelText: "Jenis Identitas",
//                                   border: OutlineInputBorder(),
//                                   contentPadding:
//                                       EdgeInsets.symmetric(horizontal: 10),
//                                 ),
//                                 items:
//                                     _formMAddGuarantorIndividualChangeNotifier
//                                         .lisIdentityModel
//                                         .map((value) {
//                                   return DropdownMenuItem<IdentityModel>(
//                                     value: value,
//                                     child: Text(
//                                       value.name,
//                                       overflow: TextOverflow.ellipsis,
//                                     ),
//                                   );
//                                 }).toList()),
//                             SizedBox(
//                                 height:
//                                     MediaQuery.of(context).size.height / 47),
//                             TextFormField(
//                               controller:
//                                   _formMAddGuarantorIndividualChangeNotifier
//                                       .controllerIdentityNumber,
//                               autovalidate:
//                                   _formMAddGuarantorIndividualChangeNotifier
//                                       .autoValidate,
//                               validator: (e) {
//                                 if (e.isEmpty) {
//                                   return "Tidak boleh kosong";
//                                 } else {
//                                   return null;
//                                 }
//                               },
//                               style: new TextStyle(color: Colors.black),
//                               decoration: new InputDecoration(
//                                   labelText: 'Nomor Identitas',
//                                   labelStyle: TextStyle(color: Colors.black),
//                                   border: OutlineInputBorder(
//                                       borderRadius: BorderRadius.circular(8))),
//                                 keyboardType: _formMAddGuarantorIndividualChangeNotifier.identityModelSelected != null
//                                     ? _formMAddGuarantorIndividualChangeNotifier.identityModelSelected.id != "03"
//                                       ? TextInputType.number
//                                       : TextInputType.text
//                                     : TextInputType.number,
//                                 textCapitalization: TextCapitalization.characters,
//                                 inputFormatters: _formMAddGuarantorIndividualChangeNotifier.identityModelSelected != null
//                                     ? _formMAddGuarantorIndividualChangeNotifier.identityModelSelected.id != "03"
//                                       ? [WhitelistingTextInputFormatter.digitsOnly]
//                                       : null
//                                     : [WhitelistingTextInputFormatter.digitsOnly]
//                             ),
//                             SizedBox(
//                                 height:
//                                     MediaQuery.of(context).size.height / 47),
//                             TextFormField(
//                               controller:
//                                   _formMAddGuarantorIndividualChangeNotifier
//                                       .controllerFullNameIdentity,
//                               style: new TextStyle(color: Colors.black),
//                               decoration: new InputDecoration(
//                                   labelText: 'Nama Lengkap Sesuai Identitas',
//                                   labelStyle: TextStyle(color: Colors.black),
//                                   border: OutlineInputBorder(
//                                       borderRadius: BorderRadius.circular(8))),
//                               keyboardType: TextInputType.text,
//                               textCapitalization: TextCapitalization.characters,
//                               autovalidate:
//                                   _formMAddGuarantorIndividualChangeNotifier
//                                       .autoValidate,
//                               validator: (e) {
//                                 if (e.isEmpty) {
//                                   return "Tidak boleh kosong";
//                                 } else {
//                                   return null;
//                                 }
//                               },
//                             ),
//                             SizedBox(
//                                 height:
//                                     MediaQuery.of(context).size.height / 47),
//                             TextFormField(
//                               controller:
//                                   _formMAddGuarantorIndividualChangeNotifier
//                                       .controllerFullName,
//                               style: new TextStyle(color: Colors.black),
//                               decoration: new InputDecoration(
//                                   labelText: 'Nama Lengkap',
//                                   labelStyle: TextStyle(color: Colors.black),
//                                   border: OutlineInputBorder(
//                                       borderRadius: BorderRadius.circular(8))),
//                               keyboardType: TextInputType.text,
//                               textCapitalization: TextCapitalization.characters,
//                               autovalidate:
//                                   _formMAddGuarantorIndividualChangeNotifier
//                                       .autoValidate,
//                               validator: (e) {
//                                 if (e.isEmpty) {
//                                   return "Tidak boleh kosong";
//                                 } else {
//                                   return null;
//                                 }
//                               },
//                             ),
//                             // SizedBox(
//                             //     height:
//                             //         MediaQuery.of(context).size.height / 47),
//                             // TextFormField(
//                             //   onTap: () {
//                             //     FocusManager.instance.primaryFocus.unfocus();
//                             //     _formMAddGuarantorIndividualChangeNotifier
//                             //         .selectBirthDate(context);
//                             //   },
//                             //   controller:
//                             //       _formMAddGuarantorIndividualChangeNotifier
//                             //           .controllerBirthDate,
//                             //   style: new TextStyle(color: Colors.black),
//                             //   decoration: new InputDecoration(
//                             //       labelText: 'Tanggal Lahir',
//                             //       labelStyle: TextStyle(color: Colors.black),
//                             //       border: OutlineInputBorder(
//                             //           borderRadius: BorderRadius.circular(8))),
//                             //   autovalidate:
//                             //       _formMAddGuarantorIndividualChangeNotifier
//                             //           .autoValidate,
//                             //   validator: (e) {
//                             //     if (e.isEmpty) {
//                             //       return "Tidak boleh kosong";
//                             //     } else {
//                             //       return null;
//                             //     }
//                             //   },
//                             //   readOnly: true,
//                             // ),
//                             // SizedBox(
//                             //     height:
//                             //         MediaQuery.of(context).size.height / 47),
//                             // TextFormField(
//                             //   controller:
//                             //       _formMAddGuarantorIndividualChangeNotifier
//                             //           .controllerBirthPlaceIdentity,
//                             //   style: new TextStyle(color: Colors.black),
//                             //   decoration: new InputDecoration(
//                             //       labelText: 'Tempat Lahir Sesuai Identitas',
//                             //       labelStyle: TextStyle(color: Colors.black),
//                             //       border: OutlineInputBorder(
//                             //           borderRadius: BorderRadius.circular(8))),
//                             //   keyboardType: TextInputType.text,
//                             //   textCapitalization: TextCapitalization.characters,
//                             //   autovalidate:
//                             //       _formMAddGuarantorIndividualChangeNotifier
//                             //           .autoValidate,
//                             //   validator: (e) {
//                             //     if (e.isEmpty) {
//                             //       return "Tidak boleh kosong";
//                             //     } else {
//                             //       return null;
//                             //     }
//                             //   },
//                             // ),
//                             // SizedBox(
//                             //     height:
//                             //         MediaQuery.of(context).size.height / 47),
//                             // TextFormField(
//                             //   controller:
//                             //       _formMAddGuarantorIndividualChangeNotifier
//                             //           .controllerBirthPlaceIdentityLOV,
//                             //   style: new TextStyle(color: Colors.black),
//                             //   decoration: new InputDecoration(
//                             //       labelText: 'Tempat Lahir Sesuai Identitas',
//                             //       labelStyle: TextStyle(color: Colors.black),
//                             //       border: OutlineInputBorder(
//                             //           borderRadius: BorderRadius.circular(8))),
//                             //   keyboardType: TextInputType.text,
//                             //   textCapitalization: TextCapitalization.characters,
//                             //   autovalidate:
//                             //       _formMAddGuarantorIndividualChangeNotifier
//                             //           .autoValidate,
//                             //   validator: (e) {
//                             //     if (e.isEmpty) {
//                             //       return "Tidak boleh kosong";
//                             //     } else {
//                             //       return null;
//                             //     }
//                             //   },
//                             //   readOnly: true,
//                             //   onTap: (){
//                             //     _formMAddGuarantorIndividualChangeNotifier.searchBirthPlace(context);
//                             //   },
//                             // ),
//                             // SizedBox(
//                             //     height:
//                             //         MediaQuery.of(context).size.height / 47),
//                             // Text(
//                             //   "Jenis Kelamin",
//                             //   style: TextStyle(
//                             //       fontSize: 16,
//                             //       fontWeight: FontWeight.w700,
//                             //       letterSpacing: 0.15),
//                             // ),
//                             // Row(
//                             //   children: [
//                             //     Row(
//                             //       children: [
//                             //         Radio(
//                             //             activeColor: primaryOrange,
//                             //             value: "01",
//                             //             groupValue:
//                             //             _formMAddGuarantorIndividualChangeNotifier
//                             //                 .radioValueGender,
//                             //             onChanged: _formMAddGuarantorIndividualChangeNotifier.relationshipStatusModelSelected != null
//                             //                 ? _formMAddGuarantorIndividualChangeNotifier.relationshipStatusModelSelected.PARA_FAMILY_TYPE_ID == "01" || _formMAddGuarantorIndividualChangeNotifier.relationshipStatusModelSelected.PARA_FAMILY_TYPE_ID == "02" || _formMAddGuarantorIndividualChangeNotifier.relationshipStatusModelSelected.PARA_FAMILY_TYPE_ID == "04"
//                             //                 ? (value) {}
//                             //                 : (value) {_formMAddGuarantorIndividualChangeNotifier.radioValueGender = value;}
//                             //                 : (value) {}
//                             //         ),
//                             //         Text("Laki Laki")
//                             //       ],
//                             //     ),
//                             //     Row(
//                             //       children: [
//                             //         Radio(
//                             //             activeColor: primaryOrange,
//                             //             value: "02",
//                             //             groupValue:
//                             //             _formMAddGuarantorIndividualChangeNotifier
//                             //                 .radioValueGender,
//                             //             onChanged: _formMAddGuarantorIndividualChangeNotifier.relationshipStatusModelSelected != null
//                             //                 ? _formMAddGuarantorIndividualChangeNotifier.relationshipStatusModelSelected.PARA_FAMILY_TYPE_ID == "01" || _formMAddGuarantorIndividualChangeNotifier.relationshipStatusModelSelected.PARA_FAMILY_TYPE_ID == "02" || _formMAddGuarantorIndividualChangeNotifier.relationshipStatusModelSelected.PARA_FAMILY_TYPE_ID == "04"
//                             //                 ? (value) {}
//                             //                 : (value) {_formMAddGuarantorIndividualChangeNotifier.radioValueGender = value;}
//                             //                 : (value) {}
//                             //         ),
//                             //         Text("Perempuan")
//                             //       ],
//                             //     ),
//                             //   ],
//                             // ),
//                             // SizedBox(
//                             //     height:
//                             //         MediaQuery.of(context).size.height / 47),
//                             // TextFormField(
//                             //   controller:
//                             //       _formMAddGuarantorIndividualChangeNotifier
//                             //           .controllerCellPhoneNumber,
//                             //   style: new TextStyle(color: Colors.black),
//                             //   decoration: new InputDecoration(
//                             //       labelText: 'Nomor Handphone',
//                             //       labelStyle: TextStyle(color: Colors.black),
//                             //       prefixText: "08",
//                             //       border: OutlineInputBorder(
//                             //           borderRadius: BorderRadius.circular(8))),
//                             //   inputFormatters: [
//                             //     WhitelistingTextInputFormatter.digitsOnly,
//                             //     // LengthLimitingTextInputFormatter(10),
//                             //   ],
//                             //   keyboardType: TextInputType.number,
//                             //   textCapitalization: TextCapitalization.characters,
//                             //   autovalidate:
//                             //       _formMAddGuarantorIndividualChangeNotifier
//                             //           .autoValidate,
//                             //   validator: (e) {
//                             //     if (e.isEmpty) {
//                             //       return "Tidak boleh kosong";
//                             //     } else {
//                             //       return null;
//                             //     }
//                             //   },
//                             // ),
// //                             SizedBox(
// //                                 height:
// //                                     MediaQuery.of(context).size.height / 47),
// //                             TextFormField(
// //                               autovalidate:
// //                                   _formMAddGuarantorIndividualChangeNotifier
// //                                       .autoValidate,
// //                               validator: (e) {
// //                                 if (e.isEmpty) {
// //                                   return "Tidak boleh kosong";
// //                                 } else {
// //                                   return null;
// //                                 }
// //                               },
// //                               onTap: () {
// //                                 Navigator.push(
// //                                     context,
// //                                     MaterialPageRoute(
// //                                         builder: (context) =>
// //                                             ListGuarantorAddressIndividual()
// // //                                  ChangeNotifierProvider(
// // //                                      create: (context) =>
// // //                                          FormMAddGuarantorIndividualChangeNotifier(),
// // //                                      child:
// // //                                          ListGuarantorAddressIndividual())
// //                                         ));
// //                               },
// //                               controller:
// //                                   _formMAddGuarantorIndividualChangeNotifier
// //                                       .controllerAddress,
// //                               style: TextStyle(color: Colors.black),
// //                               decoration: new InputDecoration(
// //                                   labelText: 'Alamat',
// //                                   labelStyle: TextStyle(color: Colors.black),
// //                                   border: OutlineInputBorder(
// //                                       borderRadius: BorderRadius.circular(8))),
// //                               readOnly: true,
// //                             ),
// //                             SizedBox(
// //                                 height:
// //                                     MediaQuery.of(context).size.height / 47),
// //                             TextFormField(
// //                               readOnly: true,
// //                               autovalidate:
// //                                   _formMAddGuarantorIndividualChangeNotifier
// //                                       .autoValidate,
// //                               validator: (e) {
// //                                 if (e.isEmpty) {
// //                                   return "Tidak boleh kosong";
// //                                 } else {
// //                                   return null;
// //                                 }
// //                               },
// //                               controller:
// //                                   _formMAddGuarantorIndividualChangeNotifier
// //                                       .controllerAddressType,
// //                               style: new TextStyle(color: Colors.black),
// //                               decoration: new InputDecoration(
// //                                   filled: true,
// //                                   fillColor: Colors.black12,
// //                                   labelText: 'Jenis Alamat',
// //                                   labelStyle: TextStyle(color: Colors.black),
// //                                   border: OutlineInputBorder(
// //                                       borderRadius: BorderRadius.circular(8))),
// //                               enabled: false,
// //                             ),
// //                             SizedBox(
// //                                 height:
// //                                     MediaQuery.of(context).size.height / 47),
// //                             Row(
// //                               children: [
// //                                 Expanded(
// //                                   flex: 5,
// //                                   child: TextFormField(
// //                                     readOnly: true,
// //                                     autovalidate:
// //                                         _formMAddGuarantorIndividualChangeNotifier
// //                                             .autoValidate,
// //                                     validator: (e) {
// //                                       if (e.isEmpty) {
// //                                         return "Tidak boleh kosong";
// //                                       } else {
// //                                         return null;
// //                                       }
// //                                     },
// //                                     controller:
// //                                         _formMAddGuarantorIndividualChangeNotifier
// //                                             .controllerRT,
// //                                     style: new TextStyle(color: Colors.black),
// //                                     decoration: new InputDecoration(
// //                                         filled: true,
// //                                         fillColor: Colors.black12,
// //                                         labelText: 'RT',
// //                                         labelStyle:
// //                                             TextStyle(color: Colors.black),
// //                                         border: OutlineInputBorder(
// //                                             borderRadius:
// //                                                 BorderRadius.circular(8))),
// //                                     enabled: false,
// //                                   ),
// //                                 ),
// //                                 SizedBox(width: 8),
// //                                 Expanded(
// //                                     flex: 5,
// //                                     child: TextFormField(
// //                                       readOnly: true,
// //                                       autovalidate:
// //                                           _formMAddGuarantorIndividualChangeNotifier
// //                                               .autoValidate,
// //                                       validator: (e) {
// //                                         if (e.isEmpty) {
// //                                           return "Tidak boleh kosong";
// //                                         } else {
// //                                           return null;
// //                                         }
// //                                       },
// //                                       controller:
// //                                           _formMAddGuarantorIndividualChangeNotifier
// //                                               .controllerRW,
// //                                       style: new TextStyle(color: Colors.black),
// //                                       decoration: new InputDecoration(
// //                                           filled: true,
// //                                           fillColor: Colors.black12,
// //                                           labelText: 'RW',
// //                                           labelStyle:
// //                                               TextStyle(color: Colors.black),
// //                                           border: OutlineInputBorder(
// //                                               borderRadius:
// //                                                   BorderRadius.circular(8))),
// //                                       enabled: false,
// //                                     ))
// //                               ],
// //                             ),
// //                             SizedBox(
// //                                 height:
// //                                     MediaQuery.of(context).size.height / 47),
// //                             TextFormField(
// //                               readOnly: true,
// //                               autovalidate:
// //                                   _formMAddGuarantorIndividualChangeNotifier
// //                                       .autoValidate,
// //                               validator: (e) {
// //                                 if (e.isEmpty) {
// //                                   return "Tidak boleh kosong";
// //                                 } else {
// //                                   return null;
// //                                 }
// //                               },
// //                               controller:
// //                                   _formMAddGuarantorIndividualChangeNotifier
// //                                       .controllerKelurahan,
// //                               style: new TextStyle(color: Colors.black),
// //                               decoration: new InputDecoration(
// //                                   filled: true,
// //                                   fillColor: Colors.black12,
// //                                   labelText: 'Kelurahan',
// //                                   labelStyle: TextStyle(color: Colors.black),
// //                                   border: OutlineInputBorder(
// //                                       borderRadius: BorderRadius.circular(8))),
// //                               enabled: false,
// //                             ),
// //                             SizedBox(
// //                                 height:
// //                                     MediaQuery.of(context).size.height / 47),
// //                             TextFormField(
// //                               readOnly: true,
// //                               autovalidate:
// //                                   _formMAddGuarantorIndividualChangeNotifier
// //                                       .autoValidate,
// //                               validator: (e) {
// //                                 if (e.isEmpty) {
// //                                   return "Tidak boleh kosong";
// //                                 } else {
// //                                   return null;
// //                                 }
// //                               },
// //                               controller:
// //                                   _formMAddGuarantorIndividualChangeNotifier
// //                                       .controllerKecamatan,
// //                               style: new TextStyle(color: Colors.black),
// //                               decoration: new InputDecoration(
// //                                   filled: true,
// //                                   fillColor: Colors.black12,
// //                                   labelText: 'Kecamatan',
// //                                   labelStyle: TextStyle(color: Colors.black),
// //                                   border: OutlineInputBorder(
// //                                       borderRadius: BorderRadius.circular(8))),
// //                               enabled: false,
// //                             ),
// //                             SizedBox(
// //                                 height:
// //                                     MediaQuery.of(context).size.height / 47),
// //                             TextFormField(
// //                               readOnly: true,
// //                               autovalidate:
// //                                   _formMAddGuarantorIndividualChangeNotifier
// //                                       .autoValidate,
// //                               validator: (e) {
// //                                 if (e.isEmpty) {
// //                                   return "Tidak boleh kosong";
// //                                 } else {
// //                                   return null;
// //                                 }
// //                               },
// //                               controller:
// //                                   _formMAddGuarantorIndividualChangeNotifier
// //                                       .controllerKota,
// //                               style: TextStyle(color: Colors.black),
// //                               decoration: InputDecoration(
// //                                   filled: true,
// //                                   fillColor: Colors.black12,
// //                                   labelText: 'Kabupaten/Kota',
// //                                   labelStyle: TextStyle(color: Colors.black),
// //                                   border: OutlineInputBorder(
// //                                       borderRadius: BorderRadius.circular(8))),
// //                               enabled: false,
// //                             ),
// //                             SizedBox(
// //                                 height:
// //                                     MediaQuery.of(context).size.height / 47),
// //                             Row(
// //                               children: [
// //                                 Expanded(
// //                                   flex: 7,
// //                                   child: TextFormField(
// //                                     readOnly: true,
// //                                     autovalidate:
// //                                         _formMAddGuarantorIndividualChangeNotifier
// //                                             .autoValidate,
// //                                     validator: (e) {
// //                                       if (e.isEmpty) {
// //                                         return "Tidak boleh kosong";
// //                                       } else {
// //                                         return null;
// //                                       }
// //                                     },
// //                                     controller:
// //                                         _formMAddGuarantorIndividualChangeNotifier
// //                                             .controllerProvinsi,
// //                                     style: new TextStyle(color: Colors.black),
// //                                     decoration: new InputDecoration(
// //                                         filled: true,
// //                                         fillColor: Colors.black12,
// //                                         labelText: 'Provinsi',
// //                                         labelStyle:
// //                                             TextStyle(color: Colors.black),
// //                                         border: OutlineInputBorder(
// //                                             borderRadius:
// //                                                 BorderRadius.circular(8))),
// //                                     enabled: false,
// //                                   ),
// //                                 ),
// //                                 SizedBox(width: 8),
// //                                 Expanded(
// //                                   flex: 3,
// //                                   child: TextFormField(
// //                                     readOnly: true,
// //                                     autovalidate:
// //                                         _formMAddGuarantorIndividualChangeNotifier
// //                                             .autoValidate,
// //                                     validator: (e) {
// //                                       if (e.isEmpty) {
// //                                         return "Tidak boleh kosong";
// //                                       } else {
// //                                         return null;
// //                                       }
// //                                     },
// //                                     controller:
// //                                         _formMAddGuarantorIndividualChangeNotifier
// //                                             .controllerPostalCode,
// //                                     style: new TextStyle(color: Colors.black),
// //                                     decoration: new InputDecoration(
// //                                         filled: true,
// //                                         fillColor: Colors.black12,
// //                                         labelText: 'Kode Pos',
// //                                         labelStyle:
// //                                             TextStyle(color: Colors.black),
// //                                         border: OutlineInputBorder(
// //                                             borderRadius:
// //                                                 BorderRadius.circular(8))),
// //                                     enabled: false,
// //                                   ),
// //                                 ),
// //                               ],
// //                             ),
// //                             SizedBox(
// //                                 height:
// //                                     MediaQuery.of(context).size.height / 47),
// //                             Row(
// //                               children: [
// //                                 Expanded(
// //                                   flex: 4,
// //                                   child: TextFormField(
// //                                     readOnly: true,
// //                                     autovalidate:
// //                                         _formMAddGuarantorIndividualChangeNotifier
// //                                             .autoValidate,
// //                                     validator: (e) {
// //                                       if (e.isEmpty) {
// //                                         return "Tidak boleh kosong";
// //                                       } else {
// //                                         return null;
// //                                       }
// //                                     },
// //                                     keyboardType: TextInputType.number,
// //                                     controller:
// //                                         _formMAddGuarantorIndividualChangeNotifier
// //                                             .controllerKodeArea,
// //                                     style: new TextStyle(color: Colors.black),
// //                                     decoration: new InputDecoration(
// //                                         filled: true,
// //                                         fillColor: Colors.black12,
// //                                         labelText: 'Kode Area',
// //                                         labelStyle:
// //                                             TextStyle(color: Colors.black),
// //                                         border: OutlineInputBorder(
// //                                             borderRadius:
// //                                                 BorderRadius.circular(8))),
// //                                     enabled: false,
// //                                   ),
// //                                 ),
// //                                 SizedBox(
// //                                     width:
// //                                         MediaQuery.of(context).size.width / 37),
// //                                 Expanded(
// //                                   flex: 6,
// //                                   child: TextFormField(
// //                                     readOnly: true,
// //                                     autovalidate:
// //                                         _formMAddGuarantorIndividualChangeNotifier
// //                                             .autoValidate,
// //                                     validator: (e) {
// //                                       if (e.isEmpty) {
// //                                         return "Tidak boleh kosong";
// //                                       } else {
// //                                         return null;
// //                                       }
// //                                     },
// //                                     keyboardType: TextInputType.number,
// //                                     inputFormatters: [
// //                                       WhitelistingTextInputFormatter.digitsOnly,
// //                                       LengthLimitingTextInputFormatter(10),
// //                                     ],
// //                                     controller:
// //                                         _formMAddGuarantorIndividualChangeNotifier
// //                                             .controllerTlpn,
// //                                     style: new TextStyle(color: Colors.black),
// //                                     decoration: new InputDecoration(
// //                                         filled: true,
// //                                         fillColor: Colors.black12,
// //                                         labelText: 'Telepon',
// //                                         labelStyle:
// //                                             TextStyle(color: Colors.black),
// //                                         border: OutlineInputBorder(
// //                                             borderRadius:
// //                                                 BorderRadius.circular(8))),
// //                                     enabled: false,
// //                                   ),
// //                                 ),
// //                               ],
// //                             ),
//                           ],
//                         ),
//                       );
//                     },
//                   )
//                 :
            FutureBuilder(
                    future: _setValueForEdit,
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                      return Consumer<FormMAddGuarantorIndividualChangeNotifier>(builder: (context, _formMAddGuarantorIndividualChangeNotifier, _) {
                          return Form(
                            key: _formMAddGuarantorIndividualChangeNotifier.key,
                            onWillPop: _onWillPop,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Visibility(
                                  visible: _data.isRelationshipStatusVisible(),
                                  child: DropdownButtonFormField<RelationshipStatusModel>(
                                      autovalidate: _formMAddGuarantorIndividualChangeNotifier.autoValidate,
                                      validator: (e) {
                                        if (e == null && _data.isRelationshipStatusMandatory()) {
                                          return "Silahkan pilih hubungan";
                                        } else {
                                          return null;
                                        }
                                      },
                                      value: _formMAddGuarantorIndividualChangeNotifier.relationshipStatusModelSelected,
                                      onChanged: (value) {
                                        _formMAddGuarantorIndividualChangeNotifier.relationshipStatusModelSelected = value;
                                      },
                                      onTap: () {
                                        FocusManager.instance.primaryFocus.unfocus();
                                      },
                                      decoration: InputDecoration(
                                        labelText: "Status Hubungan",
                                        border: OutlineInputBorder(),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: _formMAddGuarantorIndividualChangeNotifier.relationshipStatusDakor ? Colors.purple : Colors.grey)),
                                        disabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: _formMAddGuarantorIndividualChangeNotifier.relationshipStatusDakor ? Colors.purple : Colors.grey)),
                                        contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                      ),
                                      items: _formMAddGuarantorIndividualChangeNotifier.listRelationShipStatus.map((value) {
                                        return DropdownMenuItem<RelationshipStatusModel>(
                                          value: value,
                                          child: Text(value.PARA_FAMILY_TYPE_NAME, overflow: TextOverflow.ellipsis),
                                        );
                                      }).toList()),
                                ),
                                Visibility(visible: _data.isRelationshipStatusVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                                Visibility(
                                  visible: _data.isIdentityTypeVisible(),
                                  child: DropdownButtonFormField<IdentityModel>(
                                      autovalidate: _formMAddGuarantorIndividualChangeNotifier.autoValidate,
                                      validator: (e) {
                                        if (e == null && _data.isIdentityTypeMandatory()) {
                                          return "Silahkan pilih identitas";
                                        } else {
                                          return null;
                                        }
                                      },
                                      value: _formMAddGuarantorIndividualChangeNotifier.identityModelSelected,
                                      onChanged: (value) {
                                        _formMAddGuarantorIndividualChangeNotifier.identityModelSelected = value;
                                      },
                                      onTap: () {
                                        FocusManager.instance.primaryFocus.unfocus();
                                      },
                                      decoration: InputDecoration(
                                        labelText: "Jenis Identitas",
                                        border: OutlineInputBorder(),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: _formMAddGuarantorIndividualChangeNotifier.identityTypeDakor ? Colors.purple : Colors.grey)),
                                        disabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: _formMAddGuarantorIndividualChangeNotifier.identityTypeDakor ? Colors.purple : Colors.grey)),
                                        contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                      ),
                                      items: _formMAddGuarantorIndividualChangeNotifier.lisIdentityModel.map((value) {
                                        return DropdownMenuItem<IdentityModel>(
                                          value: value,
                                          child: Text(
                                            value.name,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        );
                                      }).toList()),
                                ),
                                Visibility(visible: _data.isIdentityTypeVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                                Visibility(
                                    visible: _data.isIdentityNoVisible(),
                                    child: TextFormField(
                                        controller: _formMAddGuarantorIndividualChangeNotifier.controllerIdentityNumber,
                                        autovalidate: _formMAddGuarantorIndividualChangeNotifier.autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty && _data.isIdentityNoMandatory()) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        style: new TextStyle(color: Colors.black),
                                        decoration: new InputDecoration(
                                            labelText: 'Nomor Identitas',
                                            labelStyle:TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius:BorderRadius.circular(8)
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                                borderSide: BorderSide(color: _formMAddGuarantorIndividualChangeNotifier.identityNoDakor ? Colors.purple : Colors.grey)),
                                            disabledBorder: OutlineInputBorder(
                                                borderSide: BorderSide(color: _formMAddGuarantorIndividualChangeNotifier.identityNoDakor ? Colors.purple : Colors.grey)),
                                        ),
                                        inputFormatters: [
                                          WhitelistingTextInputFormatter.digitsOnly,
                                          LengthLimitingTextInputFormatter(16)
                                        ],
                                        keyboardType: TextInputType.number,
                                  ),
                                ),
                                Visibility(visible: _data.isIdentityNoVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                                Visibility(
                                  visible: _data.isFullnameIDVisible(),
                                  child: IgnorePointer(
                                    ignoring: _data.disableJenisPenawaran ? true : false,
                                    child: TextFormField(
                                      controller: _formMAddGuarantorIndividualChangeNotifier.controllerFullNameIdentity,
                                      style: new TextStyle(color: Colors.black),
                                      decoration: new InputDecoration(
                                          labelText: 'Nama Lengkap Sesuai Identitas',
                                          labelStyle: TextStyle(color: Colors.black),
                                          border: OutlineInputBorder(
                                              borderRadius:BorderRadius.circular(8)),
                                          enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(color: _formMAddGuarantorIndividualChangeNotifier.fullnameIDDakor ? Colors.purple : Colors.grey)),
                                          disabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(color: _formMAddGuarantorIndividualChangeNotifier.fullnameIDDakor ? Colors.purple : Colors.grey)),
                                      ),
                                      keyboardType: TextInputType.text,
                                      textCapitalization:TextCapitalization.characters,
                                      autovalidate: _formMAddGuarantorIndividualChangeNotifier.autoValidate,
                                      validator: (e) {
                                        if (e.isEmpty && _data.isFullnameIDMandatory()) {
                                          return "Tidak boleh kosong";
                                        } else {
                                          return null;
                                        }
                                      },
                                      inputFormatters: [
                                        FilteringTextInputFormatter.allow(RegExp('[a-zA-Z  \`]')),
                                      ],
                                    ),
                                  ),
                                ),
                                Visibility(visible: _data.isFullnameIDVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                                Visibility(
                                  visible: _data.isFullNameVisible(),
                                  child: TextFormField(
                                    controller: _formMAddGuarantorIndividualChangeNotifier.controllerFullName,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        labelText: 'Nama Lengkap',
                                        labelStyle:
                                            TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8)),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: _formMAddGuarantorIndividualChangeNotifier.fullNameDakor ? Colors.purple : Colors.grey)),
                                        disabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: _formMAddGuarantorIndividualChangeNotifier.fullNameDakor ? Colors.purple : Colors.grey)),
                                    ),
                                    keyboardType: TextInputType.text,
                                    textCapitalization: TextCapitalization.characters,
                                    autovalidate: _formMAddGuarantorIndividualChangeNotifier.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty && _data.isFullNameMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    inputFormatters: [
                                      FilteringTextInputFormatter.allow(RegExp('[a-zA-Z  \`]')),
                                    ],
                                  ),
                                ),
                                Visibility(visible: _data.isFullNameVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                                Visibility(
                                  visible: _data.isDateOfBirthVisible(),
                                  child: TextFormField(
                                    onTap: () {
                                      FocusManager.instance.primaryFocus.unfocus();
                                      _formMAddGuarantorIndividualChangeNotifier.selectBirthDate(context);
                                    },
                                    controller: _formMAddGuarantorIndividualChangeNotifier.controllerBirthDate,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        labelText: 'Tanggal Lahir',
                                        labelStyle:TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius:BorderRadius.circular(8)),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: _formMAddGuarantorIndividualChangeNotifier.dateOfBirthDakor ? Colors.purple : Colors.grey)),
                                        disabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: _formMAddGuarantorIndividualChangeNotifier.dateOfBirthDakor ? Colors.purple : Colors.grey)),
                                    ),
                                    autovalidate: _formMAddGuarantorIndividualChangeNotifier.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty && _data.isDateOfBirthMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    readOnly: true,
                                  ),
                                ),
                                Visibility(visible: _data.isDateOfBirthVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                                Visibility(
                                  visible: _data.isPlaceOfBirthVisible(),
                                  child: TextFormField(
                                    controller: _formMAddGuarantorIndividualChangeNotifier.controllerBirthPlaceIdentity,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        labelText: 'Tempat Lahir Sesuai Identitas',
                                        labelStyle: TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8)),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: _formMAddGuarantorIndividualChangeNotifier.placeOfBirthDakor ? Colors.purple : Colors.grey)),
                                        disabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: _formMAddGuarantorIndividualChangeNotifier.placeOfBirthDakor ? Colors.purple : Colors.grey)),
                                    ),
                                    keyboardType: TextInputType.text,
                                    textCapitalization: TextCapitalization.characters,
                                    autovalidate: _formMAddGuarantorIndividualChangeNotifier.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty && _data.isPlaceOfBirthMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    inputFormatters: [
                                      FilteringTextInputFormatter.allow(RegExp('[a-zA-Z]')),
                                    ],
                                  ),
                                ),
                                Visibility(visible: _data.isPlaceOfBirthVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                                Visibility(
                                  visible: _data.isPlaceOfBirthLOVVisible(),
                                  child: TextFormField(
                                    controller: _formMAddGuarantorIndividualChangeNotifier.controllerBirthPlaceIdentityLOV,
                                    onTap: (){
                                        FocusManager.instance.primaryFocus.unfocus();
                                        _formMAddGuarantorIndividualChangeNotifier.searchBirthPlace(context);
                                    },
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        labelText: 'Tempat Lahir Sesuai Identitas',
                                        labelStyle: TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8)),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: _formMAddGuarantorIndividualChangeNotifier.placeOfBirthLOVDakor ? Colors.purple : Colors.grey)),
                                        disabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: _formMAddGuarantorIndividualChangeNotifier.placeOfBirthLOVDakor ? Colors.purple : Colors.grey)),
                                    ),
                                    keyboardType: TextInputType.text,
                                    textCapitalization: TextCapitalization.characters,
                                    autovalidate: _formMAddGuarantorIndividualChangeNotifier.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty && _data.isPlaceOfBirthLOVMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                  ),
                                ),
                                Visibility(visible: _data.isPlaceOfBirthLOVVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                                Visibility(
                                  visible: _data.isGenderVisible(),
                                  child: Text(
                                    "Jenis Kelamin",
                                    style: TextStyle(
                                        color: _formMAddGuarantorIndividualChangeNotifier.genderDakor ? Colors.purple : Colors.black,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w700,
                                        letterSpacing: 0.15),
                                  ),
                                ),
                                Visibility(
                                  visible: _data.isGenderVisible(),
                                  child: Row(
                                    children: [
                                      Row(
                                        children: [
                                          Radio(
                                              activeColor: primaryOrange,
                                              value: "01",
                                              groupValue:
                                              _formMAddGuarantorIndividualChangeNotifier
                                                  .radioValueGender,
                                              onChanged: _formMAddGuarantorIndividualChangeNotifier.relationshipStatusModelSelected != null
                                                  ? (_formMAddGuarantorIndividualChangeNotifier.relationshipStatusModelSelected.PARA_FAMILY_TYPE_ID == "01" || _formMAddGuarantorIndividualChangeNotifier.relationshipStatusModelSelected.PARA_FAMILY_TYPE_ID == "02" || _formMAddGuarantorIndividualChangeNotifier.relationshipStatusModelSelected.PARA_FAMILY_TYPE_ID == "04") && _formMAddGuarantorIndividualChangeNotifier.custType == "PER"
                                                  ? (value) {}
                                                  : (value) {_formMAddGuarantorIndividualChangeNotifier.radioValueGender = value;}
                                                  : (value) {}
                                          ),
                                          Text("Laki Laki")
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Radio(
                                              activeColor: primaryOrange,
                                              value: "02",
                                              groupValue:
                                              _formMAddGuarantorIndividualChangeNotifier
                                                  .radioValueGender,
                                              onChanged: _formMAddGuarantorIndividualChangeNotifier.relationshipStatusModelSelected != null
                                                  ? (_formMAddGuarantorIndividualChangeNotifier.relationshipStatusModelSelected.PARA_FAMILY_TYPE_ID == "01" || _formMAddGuarantorIndividualChangeNotifier.relationshipStatusModelSelected.PARA_FAMILY_TYPE_ID == "02" || _formMAddGuarantorIndividualChangeNotifier.relationshipStatusModelSelected.PARA_FAMILY_TYPE_ID == "04") && _formMAddGuarantorIndividualChangeNotifier.custType == "PER"
                                                  ? (value) {}
                                                  : (value) {_formMAddGuarantorIndividualChangeNotifier.radioValueGender = value;}
                                                  : (value) {}
                                          ),
                                          Text("Perempuan")
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Visibility(visible: _data.isGenderVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                                Visibility(
                                  visible: _data.isPhoneVisible(),
                                  child: TextFormField(
                                    controller: _formMAddGuarantorIndividualChangeNotifier.controllerCellPhoneNumber,
                                    style: new TextStyle(color: Colors.black),
                                    onChanged: (value){
                                      _formMAddGuarantorIndividualChangeNotifier.checkValidNoHP(value);
                                    },
                                    decoration: new InputDecoration(
                                        labelText: 'Nomor Handphone',
                                        prefixText: '08',
                                        labelStyle: TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8)),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: _formMAddGuarantorIndividualChangeNotifier.phoneDakor ? Colors.purple : Colors.grey)),
                                        disabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: _formMAddGuarantorIndividualChangeNotifier.phoneDakor ? Colors.purple : Colors.grey)),
                                    ),
                                    keyboardType: TextInputType.number,
                                    inputFormatters: [
                                      LengthLimitingTextInputFormatter(14),
                                      FilteringTextInputFormatter.allow(RegExp('[0-9]')),
                                      // WhitelistingTextInputFormatter(RegExp(r"^[1-9][0-9]*$"))
                                    ],
                                    textCapitalization: TextCapitalization.characters,
                                    autovalidate: _formMAddGuarantorIndividualChangeNotifier.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty && _data.isPhoneMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                  ),
                                ),
                                Visibility(visible: _data.isPhoneVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                                Visibility(
                                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressShow(3),
                                    child: TextFormField(
                                        autovalidate: _formMAddGuarantorIndividualChangeNotifier.autoValidate,
                                        validator: (e) {
                                            if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressMan(3)) {
                                                return "Tidak boleh kosong";
                                            } else {
                                                return null;
                                            }
                                        },
                                        onTap: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) => ListGuarantorAddressIndividual()));
                                        },
                                        controller: _formMAddGuarantorIndividualChangeNotifier.controllerAddress,
                                        style: new TextStyle(color: Colors.black),
                                        decoration: new InputDecoration(
                                            labelText: 'Alamat',
                                            labelStyle: TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(8))),
                                        readOnly: true,
                                    ),
                                 ),
                                Visibility(
                                    child: SizedBox(height: MediaQuery.of(context).size.height / 47),
                                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressShow(3)
                                 ),
                                Visibility(
                                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressTypeShow(3),
                                    child: TextFormField(
                                        autovalidate: _formMAddGuarantorIndividualChangeNotifier.autoValidate,
                                        validator: (e) {
                                            if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressTypeMan(3)) {
                                                return "Tidak boleh kosong";
                                            } else {
                                                return null;
                                            }
                                        },
                                        controller: _formMAddGuarantorIndividualChangeNotifier.controllerAddressType,
                                        style: new TextStyle(color: Colors.black),
                                        decoration: new InputDecoration(
                                            filled: true,
                                            fillColor: Colors.black12,
                                            labelText: 'Jenis Alamat',
                                            labelStyle: TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(8))),
                                        enabled: false,
                                    ),
                                 ),
                                Visibility(
                                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressTypeShow(3),
                                    child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                                 ),
                                Row(
                                    children: [
                                        Expanded(
                                            flex: 5,
                                            child: Visibility(
                                                visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRTShow(3),
                                                child: TextFormField(
                                                    autovalidate: _formMAddGuarantorIndividualChangeNotifier.autoValidate,
                                                    validator: (e) {
                                                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRTMan(3)) {
                                                            return "Tidak boleh kosong";
                                                        } else {
                                                            return null;
                                                        }
                                                    },
                                                    controller: _formMAddGuarantorIndividualChangeNotifier.controllerRT,
                                                    style: new TextStyle(color: Colors.black),
                                                    decoration: new InputDecoration(
                                                        filled: true,
                                                        fillColor: Colors.black12,
                                                        labelText: 'RT',
                                                        labelStyle: TextStyle(color: Colors.black),
                                                        border: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(8))),
                                                    enabled: false,
                                                ),
                                            ),
                                        ),
                                        SizedBox(width: 8),
                                        Expanded(
                                            flex: 5,
                                            child: Visibility(
                                                visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRWShow(3),
                                                child: TextFormField(
                                                    autovalidate: _formMAddGuarantorIndividualChangeNotifier.autoValidate,
                                                    validator: (e) {
                                                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRWMan(3)) {
                                                            return "Tidak boleh kosong";
                                                        } else {
                                                            return null;
                                                        }
                                                    },
                                                    controller: _formMAddGuarantorIndividualChangeNotifier.controllerRW,
                                                    style: new TextStyle(color: Colors.black),
                                                    decoration: new InputDecoration(
                                                        filled: true,
                                                        fillColor: Colors.black12,
                                                        labelText: 'RW',
                                                        labelStyle: TextStyle(color: Colors.black),
                                                        border: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(8))),
                                                    enabled: false,
                                                ),
                                            ))
                                    ],
                                 ),
                                Visibility(
                                    child: SizedBox(height: MediaQuery.of(context).size.height / 47),
                                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRWShow(3)
                                        && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRTShow(3),
                                 ),
                                Visibility(
                                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKelurahanShow(3),
                                    child: TextFormField(
                                        autovalidate: _formMAddGuarantorIndividualChangeNotifier.autoValidate,
                                        validator: (e) {
                                            if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKelurahanMan(3)) {
                                                return "Tidak boleh kosong";
                                            } else {
                                                return null;
                                            }
                                        },
                                        controller: _formMAddGuarantorIndividualChangeNotifier.controllerKelurahan,
                                        style: new TextStyle(color: Colors.black),
                                        decoration: new InputDecoration(
                                            filled: true,
                                            fillColor: Colors.black12,
                                            labelText: 'Kelurahan',
                                            labelStyle: TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(8))),
                                        enabled: false,
                                    ),
                                 ),
                                Visibility(child: SizedBox(height: MediaQuery.of(context).size.height / 47),visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKelurahanShow(3),),
                                Visibility(
                                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKecamatanShow(3),
                                    child: TextFormField(
                                        autovalidate: _formMAddGuarantorIndividualChangeNotifier.autoValidate,
                                        validator: (e) {
                                            if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKecamatanMan(3)) {
                                                return "Tidak boleh kosong";
                                            } else {
                                                return null;
                                            }
                                        },
                                        controller: _formMAddGuarantorIndividualChangeNotifier.controllerKecamatan,
                                        style: new TextStyle(color: Colors.black),
                                        decoration: new InputDecoration(
                                            filled: true,
                                            fillColor: Colors.black12,
                                            labelText: 'Kecamatan',
                                            labelStyle: TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(8))),
                                        enabled: false,
                                    ),
                                 ),
                                Visibility(
                                    child: SizedBox(height: MediaQuery.of(context).size.height / 47),
                                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKecamatanShow(3),
                                 ),
                                Visibility(
                                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKabKotShow(3),
                                    child: TextFormField(
                                        autovalidate: _formMAddGuarantorIndividualChangeNotifier.autoValidate,
                                        validator: (e) {
                                            if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKabKotMan(3)) {
                                                return "Tidak boleh kosong";
                                            } else {
                                                return null;
                                            }
                                        },
                                        controller: _formMAddGuarantorIndividualChangeNotifier.controllerKota,
                                        style: TextStyle(color: Colors.black),
                                        decoration: InputDecoration(
                                            filled: true,
                                            fillColor: Colors.black12,
                                            labelText: 'Kabupaten/Kota',
                                            labelStyle: TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(8))),
                                        enabled: false,
                                    ),
                                 ),
                                Visibility(child: SizedBox(height: MediaQuery.of(context).size.height / 47),visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKabKotShow(3)),
                                Row(
                                    children: [
                                        Expanded(
                                            flex: 7,
                                            child:Visibility(
                                                visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setProvinsiShow(3),
                                                child:TextFormField(
                                                    autovalidate: _formMAddGuarantorIndividualChangeNotifier.autoValidate,
                                                    validator: (e) {
                                                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setProvinsiMan(3)) {
                                                            return "Tidak boleh kosong";
                                                        } else {
                                                            return null;
                                                        }
                                                    },
                                                    controller: _formMAddGuarantorIndividualChangeNotifier.controllerProvinsi,
                                                    style: new TextStyle(color: Colors.black),
                                                    decoration: new InputDecoration(
                                                        filled: true,
                                                        fillColor: Colors.black12,
                                                        labelText: 'Provinsi',
                                                        labelStyle: TextStyle(color: Colors.black),
                                                        border: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(8))),
                                                    enabled: false,
                                                ),
                                            ),
                                        ),
                                        SizedBox(width: 8),
                                        Expanded(
                                            flex: 3,
                                            child: Visibility(
                                                visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKodePosShow(3),
                                                child: TextFormField(
                                                    autovalidate: _formMAddGuarantorIndividualChangeNotifier.autoValidate,
                                                    validator: (e) {
                                                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKodePosMan(3)) {
                                                            return "Tidak boleh kosong";
                                                        } else {
                                                            return null;
                                                        }
                                                    },
                                                    controller:
                                                    _formMAddGuarantorIndividualChangeNotifier.controllerPostalCode,
                                                    style: new TextStyle(color: Colors.black),
                                                    decoration: new InputDecoration(
                                                        filled: true,
                                                        fillColor: Colors.black12,
                                                        labelText: 'Kode Pos',
                                                        labelStyle: TextStyle(color: Colors.black),
                                                        border: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(8))),
                                                    enabled: false,
                                                ),
                                            ),
                                        ),
                                    ],
                                 ),
                                Visibility(
                                    child: SizedBox(height: MediaQuery.of(context).size.height / 47),
                                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setProvinsiShow(3)
                                        && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKodePosShow(3),
                                 ),
                                Row(
                                    children: [
                                        Expanded(
                                            flex: 4,
                                            child: Visibility(
                                                visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setTelp1AreaShow(3),
                                                child: TextFormField(
                                                    autovalidate: _formMAddGuarantorIndividualChangeNotifier.autoValidate,
                                                    validator: (e) {
                                                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setTelp1AreaMan(3)) {
                                                            return "Tidak boleh kosong";
                                                        } else {
                                                            return null;
                                                        }
                                                    },
                                                    keyboardType: TextInputType.number,
                                                    inputFormatters: [
                                                        WhitelistingTextInputFormatter.digitsOnly,
                                                        LengthLimitingTextInputFormatter(10),
                                                    ],
                                                    controller: _formMAddGuarantorIndividualChangeNotifier.controllerKodeArea,
                                                    style: new TextStyle(color: Colors.black),
                                                    decoration: new InputDecoration(
                                                        filled: true,
                                                        fillColor: Colors.black12,
                                                        labelText: 'Telepon (Area)',
                                                        labelStyle: TextStyle(color: Colors.black),
                                                        border: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(8))),
                                                    enabled: false,
                                                ),
                                            ),
                                        ),
                                        SizedBox(width: MediaQuery.of(context).size.width / 37),
                                        Expanded(
                                            flex: 6,
                                            child: Visibility(
                                                visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setTelp1Show(3),
                                                child: TextFormField(
                                                    autovalidate: _formMAddGuarantorIndividualChangeNotifier.autoValidate,
                                                    validator: (e) {
                                                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setTelp1Man(3)) {
                                                            return "Tidak boleh kosong";
                                                        } else {
                                                            return null;
                                                        }
                                                    },
                                                    keyboardType: TextInputType.number,
                                                    inputFormatters: [
                                                        WhitelistingTextInputFormatter.digitsOnly,
                                                        LengthLimitingTextInputFormatter(10),
                                                    ],
                                                    controller: _formMAddGuarantorIndividualChangeNotifier.controllerTlpn,
                                                    style: new TextStyle(color: Colors.black),
                                                    decoration: new InputDecoration(
                                                        filled: true,
                                                        fillColor: Colors.black12,
                                                        labelText: 'Telepon',
                                                        labelStyle: TextStyle(color: Colors.black),
                                                        border: OutlineInputBorder(
                                                            borderRadius: BorderRadius.circular(8))),
                                                    enabled: false,
                                                ),
                                            ),
                                        ),
                                    ],
                                  ),
                              ],
                            ),
                          );
                        },
                      );
                    })),
        bottomNavigationBar: BottomAppBar(
          elevation: 0.0,
          child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(8.0)),
                  color: myPrimaryColor,
                  onPressed: () {
                    Provider.of<FormMAddGuarantorIndividualChangeNotifier>(context,listen: false).check(context, widget.flag, widget.index);
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(widget.flag == 0 ? "SAVE" : "UPDATE",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 1.25))
                    ],
                  ))),
        ),
      ),
    );
  }

  Future<bool> _onWillPop() async {
    var _provider = Provider.of<FormMAddGuarantorIndividualChangeNotifier>(context, listen: false);
    if (widget.flag == 0) {
      if (_provider.relationshipStatusModelSelected != null ||
          // _provider.listGuarantorAddress.isNotEmpty ||
          _provider.identityModelSelected != null ||
          _provider.controllerIdentityNumber.text != "" ||
          _provider.controllerFullNameIdentity.text != "" ||
          _provider.controllerFullName.text != ""
          // _provider.controllerBirthDate.text != "" ||
          // _provider.controllerBirthPlaceIdentity.text != ""
        ) {
        return (await showDialog(
              context: context,
              builder: (myContext) => AlertDialog(
                title: Text('Warning'),
                content: Text('Simpan perubahan?'),
                actions: <Widget>[
                  FlatButton(
                    onPressed: () {
                      _provider.check(context, widget.flag, null);
                      Navigator.pop(context);
                    },
                    child: Text('Ya'),
                  ),
                  FlatButton(
                    onPressed: () {
                      _provider.autoValidate = false;
                      Navigator.of(context).pop(true);
                    },
                    child: Text('Tidak', style: TextStyle(fontSize: 12, color: Colors.grey)),
                  ),
                ],
              ),
            )) ??
            false;
      } else {
        return true;
      }
    } else {
      if (_provider.relationshipStatusModelTemp.PARA_FAMILY_TYPE_ID != _provider.relationshipStatusModelSelected.PARA_FAMILY_TYPE_ID ||
          _provider.identityModelTemp.id != _provider.identityModelSelected.id ||
          _provider.identiyNumberTemp != _provider.controllerIdentityNumber.text ||
          _provider.fullNameIdentityTemp != _provider.controllerFullNameIdentity.text ||
          _provider.fullNameTemp != _provider.controllerFullName.text
          // _provider.birthDateTemp != _provider.controllerBirthDate.text ||
          // _provider.birthPlaceTemp !=
          //     _provider.controllerBirthPlaceIdentity.text ||
          // _provider.addressTemp != _provider.controllerAddress.text ||
          // _provider.addressTypeTemp != _provider.controllerAddressType.text ||
          // _provider.rtTemp != _provider.controllerRT.text ||
          // _provider.rwTemp != _provider.controllerRW.text ||
          // _provider.kelurahanTemp != _provider.controllerKelurahan.text ||
          // _provider.kecamatanTemp != _provider.controllerKecamatan.text ||
          // _provider.kotaTemp != _provider.controllerKota.text ||
          // _provider.provTemp != _provider.controllerProvinsi.text ||
          // _provider.postalCodeTemp != _provider.controllerPostalCode.text ||
          // _provider.areaCodeTemp != _provider.controllerKodeArea.text ||
          // _provider.phoneTemp != _provider.controllerTlpn.text ||
          // _provider.sizeList != _provider.listGuarantorAddress.length
      ) {
        return (await showDialog(
              context: context,
              builder: (myContext) => AlertDialog(
                title: Text('Warning'),
                content: Text('Simpan perubahan?'),
                actions: <Widget>[
                  FlatButton(
                    onPressed: () {
                      _provider.check(context, widget.flag, widget.index);
                      Navigator.pop(context);
                    },
                    child: Text('Ya'),
                  ),
                  FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop(true);
                    },
                    child: Text('Tidak', style: TextStyle(fontSize: 12, color: Colors.grey)),
                  ),
                ],
              ),
            )) ??
            false;
      } else {
        return (await showDialog(
              context: context,
              builder: (myContext) => AlertDialog(
                title: Text('Warning'),
                content: Text('Keluar dari edit data?'),
                actions: <Widget>[
                  FlatButton(
                    onPressed: () {
                      Navigator.pop(context, true);
                    },
                    child: Text('Ya', style: TextStyle(color: Colors.grey)),
                  ),
                  FlatButton(
                    onPressed: () => Navigator.of(context).pop(false),
                    child: Text('Tidak'),
                  ),
                ],
              ),
            )) ??
            false;
      }
    }
  }
}
