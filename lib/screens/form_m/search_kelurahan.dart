import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_kelurahan_change_notif.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchKelurahan extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          fontFamily: "NunitoSans",
          accentColor: myPrimaryColor,
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<SearchKelurahanChangeNotif>(context,listen: false).scaffoldKey,
          appBar: AppBar(
            title: Consumer<SearchKelurahanChangeNotif>(
              builder: (context, searchKelurahanChangeNotif, _) {
                return TextFormField(
                  controller: searchKelurahanChangeNotif.controllerSearch,
                  style: TextStyle(color: Colors.black),
                  textInputAction: TextInputAction.search,
                  onFieldSubmitted: (e) {
                    if(e.length < 3){
                      searchKelurahanChangeNotif.showSnackBar("Input minimal 3 karakter");
                    }
                    else{
                      searchKelurahanChangeNotif.getAddressFull(e);
                    }
                  },
                  onChanged: (e) {
                    searchKelurahanChangeNotif.changeAction(e);
                  },
                  cursorColor: Colors.black,
                  decoration: new InputDecoration(
                    hintText: "Cari Kelurahan (minimal 3 karakter)",
                    hintStyle: TextStyle(color: Colors.black),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: myPrimaryColor),
                      //  when the TextFormField in unfocused
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: myPrimaryColor),
                      //  when the TextFormField in focused
                    ),
                  ),
                  textCapitalization: TextCapitalization.characters,
                  autofocus: true,
                );
              },
            ),
            backgroundColor: myPrimaryColor,
            iconTheme: IconThemeData(color: Colors.black),
            actions: <Widget>[
              Provider.of<SearchKelurahanChangeNotif>(context, listen: true).showClear
                  ?
              IconButton(
                      icon: Icon(Icons.clear),
                      onPressed: () {
                        Provider.of<SearchKelurahanChangeNotif>(context,listen: false).controllerSearch.clear();
                        Provider.of<SearchKelurahanChangeNotif>(context,listen: false).changeAction(Provider.of<SearchKelurahanChangeNotif>(context,listen: false).controllerSearch.text);
                      })
                  :
              SizedBox(width: 0.0, height: 0.0)
            ],
          ),
          body: Consumer<SearchKelurahanChangeNotif>(
            builder: (context, searchKelurahanChangeNotif, _) {
              if (searchKelurahanChangeNotif.loadData == true) {
                return Center(
                  child: CircularProgressIndicator(
                    valueColor: new AlwaysStoppedAnimation<Color>(primaryOrange),
                  ),
                );
              }
              else{
                return
                  ListView.separated(
                    padding: EdgeInsets.symmetric(
                        vertical: MediaQuery.of(context).size.height / 57,
                        horizontal: MediaQuery.of(context).size.width / 27),
                    itemCount: searchKelurahanChangeNotif.listKelurahan.length,
                    itemBuilder: (listContext, index) {
                      return InkWell(
                          onTap: () {
                            Navigator.pop(context,
                                searchKelurahanChangeNotif.listKelurahan[index]);
                          },
                          child: Container(
                              child: Row(
                                children: [
                                  Expanded(
                                      child: Text(
                                        "${searchKelurahanChangeNotif.listKelurahan[index].KEL_NAME} - "
                                            "${searchKelurahanChangeNotif.listKelurahan[index].KEC_NAME} - "
                                            "${searchKelurahanChangeNotif.listKelurahan[index].KABKOT_NAME} - "
                                            "${searchKelurahanChangeNotif.listKelurahan[index].PROV_NAME} - "
                                            "${searchKelurahanChangeNotif.listKelurahan[index].ZIPCODE}",
                                        style: TextStyle(fontSize: 16),
                                      )
                                  )
                                ],
                              )
                          )
                      );},
                    separatorBuilder: (context, index) {
                      return Divider();
                    },
                  );
              }
            },
          )
      ),
    );
  }
}
