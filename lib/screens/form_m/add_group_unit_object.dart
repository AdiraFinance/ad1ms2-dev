import 'package:ad1ms2_dev/models/form_m_foto_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_add_group_unit_object_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/group_unit_object_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class AddGroUnitObjectProvider extends StatefulWidget {
  final int flag;
  final int index;
  final GroupUnitObjectModel groupUnitObjectModel;

  const AddGroUnitObjectProvider(this.flag, this.groupUnitObjectModel, this.index);

  @override
  _AddGroUnitObjectProviderState createState() => _AddGroUnitObjectProviderState();
}

class _AddGroUnitObjectProviderState extends State<AddGroUnitObjectProvider> {
  Future<void> _loadData;
  @override
  void initState() {
    super.initState();
    _loadData = Provider.of<FormMFotoAddGroupUnitObject>(context,listen: false).setValueForEdit(context, widget.groupUnitObjectModel, widget.flag);
    Provider.of<FormMFotoAddGroupUnitObject>(context,listen: false).setShowMandatoryFotoModel(context);
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        accentColor: myPrimaryColor,
      ),
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(
              widget.flag == 0
                  ? "Tambah Group Unit Object"
                  : "Edit Group Unit Object",
              style: TextStyle(color: Colors.black)),
          centerTitle: true,
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body:
        // widget.flag != 0
        //     ?
        FutureBuilder(
                future: _loadData,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  return Consumer<FormMFotoAddGroupUnitObject>(
                    builder: (context, data, _) {
                      return SingleChildScrollView(
                        padding: EdgeInsets.symmetric(
                            vertical: MediaQuery.of(context).size.height / 37,
                            horizontal: MediaQuery.of(context).size.width / 27),
                        child: Form(
                          key: data.key,
                          onWillPop: _onWillPop,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              DropdownButtonFormField<GroupObjectUnit>(
                                  autovalidate: data.autoValidateAddGroupUnitObject,
                                  validator: (e) {
                                    if (e == null) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  value: data.groupObjectUnitSelected,
                                  onChanged: (value) {
                                    data.objectUnitSelected = null;
                                    data.listFotoGroupObjectUnit.clear();
                                    data.groupObjectUnitSelected = value;
                                    data.setDataListObjectUnit(context, true);
                                  },
                                  decoration: InputDecoration(
                                    labelText: "Group Object",
                                    border: OutlineInputBorder(),
                                    contentPadding:
                                        EdgeInsets.symmetric(horizontal: 10),
                                  ),
                                  items: data.listGroupObjectUnit.map((value) {
                                    return DropdownMenuItem<GroupObjectUnit>(
                                      value: value,
                                      child: Text(
                                        value.groupObject,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    );
                                  }).toList()),
                              SizedBox(height:MediaQuery.of(context).size.height / 47),
                              DropdownButtonFormField<ObjectUnit>(
                                  autovalidate: data.autoValidateAddGroupUnitObject,
                                  validator: (e) {
                                    if (e == null) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  value: data.objectUnitSelected,
                                  onChanged: (value) {
                                    data.objectUnitSelected = value;
                                  },
                                  decoration: InputDecoration(
                                    labelText: "Unit Object",
                                    border: OutlineInputBorder(),
                                    contentPadding:
                                        EdgeInsets.symmetric(horizontal: 10),
                                  ),
                                  items: data.listObjectUnit.map((value) {
                                    return DropdownMenuItem<ObjectUnit>(
                                      value: value,
                                      child: Text(
                                        value.objectUnit,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    );
                                  }).toList()),
                              SizedBox(height: MediaQuery.of(context).size.height / 47),
                              data.objectUnitSelected != null
                                  // ? data.objectUnitSelected.id != "011"
                                      ? data.listFotoGroupObjectUnit.isEmpty
                                          ? RaisedButton(
                                              onPressed: () {
                                                // data.addFile();
                                                data.showBottomSheetChooseFile(context, widget.index);
                                              },
                                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
                                              child: Row(
                                                mainAxisSize: MainAxisSize.max,
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Icon(Icons.camera_alt),
                                                  SizedBox(width:MediaQuery.of(context).size.width/47),
                                                  Text("ADD")
                                                ],
                                              ),
                                              color: myPrimaryColor,
                                            )
                                          : Row(
                                              children: <Widget>[
                                                Row(
                                                  children: _listWidgetImageGroupObjectUnit(data.listFotoGroupObjectUnit,context),
                                                ),
                                                data.listFotoGroupObjectUnit.length<4
                                                    ? IconButton(
                                                        onPressed: () {
                                                          // data.addFile();
                                                          data.showBottomSheetChooseFile(context, widget.index);
                                                        },
                                                        icon: Icon(
                                                          Icons.add_a_photo,
                                                          color: myPrimaryColor,
                                                          size: 33,
                                                        ))
                                                    : SizedBox(width: 0.0, height: 0.0)
                                              ],
                                            )
                                      // : SizedBox()
                                  : SizedBox(),
                              // data.objectUnitSelected != null
                              // ?
                              data.autoValidateAddGroupUnitObject
                                  // && data.isFotoGroupUnitObjectMandatory()
                                  // && data.objectUnitSelected.id == "012"
                                  ? Text("Tidak boleh kosong",
                                    style: TextStyle(
                                      color: Colors.red,
                                      fontSize: 12))
                                  : SizedBox()
                              // : SizedBox()
                            ],
                          ),
                        ),
                      );
                    },
                  );
                },
              ),
            // : FutureBuilder(
            //   future: _loadData,
            //   builder: (context, snapshot){
            //     if(snapshot.connectionState == ConnectionState.waiting){
            //       return Center(child: CircularProgressIndicator());
            //     }
            //     return Consumer<FormMFotoAddGroupUnitObject>(
            //       builder: (context, formMFotoAddGroupUnitObjectChangeNotif, _) {
            //         return SingleChildScrollView(
            //           padding: EdgeInsets.symmetric(
            //               vertical: MediaQuery.of(context).size.height / 37,
            //               horizontal: MediaQuery.of(context).size.width / 27),
            //           child: Form(
            //             key: formMFotoAddGroupUnitObjectChangeNotif.key,
            //             onWillPop: _onWillPop,
            //             child: Column(
            //               children: [
            //                 DropdownButtonFormField<GroupObjectUnit>(
            //                     autovalidate: formMFotoAddGroupUnitObjectChangeNotif.autoValidateAddGroupUnitObject,
            //                     validator: (e) {
            //                       if (e == null) {
            //                         return "Tidak boleh kosong";
            //                       } else {
            //                         return null;
            //                       }
            //                     },
            //                     value: formMFotoAddGroupUnitObjectChangeNotif.groupObjectUnitSelected,
            //                     onChanged: (value) {
            //                       formMFotoAddGroupUnitObjectChangeNotif.objectUnitSelected = null;
            //                       formMFotoAddGroupUnitObjectChangeNotif.listFotoGroupObjectUnit.clear();
            //                       formMFotoAddGroupUnitObjectChangeNotif.groupObjectUnitSelected = value;
            //                       formMFotoAddGroupUnitObjectChangeNotif.setDataListObjectUnit();
            //                     },
            //                     decoration: InputDecoration(
            //                       labelText: "Group Object",
            //                       border: OutlineInputBorder(),
            //                       contentPadding: EdgeInsets.symmetric(horizontal: 10),
            //                     ),
            //                     items: formMFotoAddGroupUnitObjectChangeNotif.listGroupObjectUnit.map((value) {
            //                       return DropdownMenuItem<GroupObjectUnit>(
            //                         value: value,
            //                         child: Text(
            //                           value.groupObject,
            //                           overflow: TextOverflow.ellipsis,
            //                         ),
            //                       );
            //                     }).toList()),
            //                 SizedBox(height: MediaQuery.of(context).size.height / 47),
            //                 DropdownButtonFormField<ObjectUnit>(
            //                     autovalidate: formMFotoAddGroupUnitObjectChangeNotif.autoValidateAddGroupUnitObject,
            //                     validator: (e) {
            //                       if (e == null) {
            //                         return "Tidak boleh kosong";
            //                       } else {
            //                         return null;
            //                       }
            //                     },
            //                     value: formMFotoAddGroupUnitObjectChangeNotif.objectUnitSelected,
            //                     onChanged: (value) {
            //                       formMFotoAddGroupUnitObjectChangeNotif.objectUnitSelected = value;
            //                     },
            //                     decoration: InputDecoration(
            //                       labelText: "Object Unit",
            //                       border: OutlineInputBorder(),
            //                       contentPadding:
            //                       EdgeInsets.symmetric(horizontal: 10),
            //                     ),
            //                     items: formMFotoAddGroupUnitObjectChangeNotif.listObjectUnit.map((value) {
            //                       return DropdownMenuItem<ObjectUnit>(
            //                         value: value,
            //                         child: Text(
            //                           value.objectUnit,
            //                           overflow: TextOverflow.ellipsis,
            //                         ),
            //                       );
            //                     }).toList()),
            //                 SizedBox(height: MediaQuery.of(context).size.height / 47),
            //                 formMFotoAddGroupUnitObjectChangeNotif.objectUnitSelected !=null
            //                 // ? formMFotoAddGroupUnitObjectChangeNotif
            //                 //             .objectUnitSelected.id !=
            //                 //         "011"
            //                     ? formMFotoAddGroupUnitObjectChangeNotif.listFotoGroupObjectUnit.isEmpty
            //                     ? RaisedButton(
            //                   onPressed: () {
            //                     // formMFotoAddGroupUnitObjectChangeNotif.addFile();
            //                     formMFotoAddGroupUnitObjectChangeNotif.showBottomSheetChooseFile(context);
            //                   },
            //                   shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
            //                   child: Row(
            //                     mainAxisSize: MainAxisSize.max,
            //                     mainAxisAlignment: MainAxisAlignment.center,
            //                     children: <Widget>[
            //                       Icon(Icons.camera_alt),
            //                       SizedBox(width: MediaQuery.of(context).size.width /47),
            //                       Text("ADD")
            //                     ],
            //                   ),
            //                   color: myPrimaryColor,
            //                 )
            //                     : Row(
            //                   children: <Widget>[
            //                     Row(
            //                       children: _listWidgetImageGroupObjectUnit(formMFotoAddGroupUnitObjectChangeNotif.listFotoGroupObjectUnit,context),
            //                     ),
            //                     formMFotoAddGroupUnitObjectChangeNotif.listFotoGroupObjectUnit.length < 4
            //                         ? IconButton(
            //                         onPressed: () {
            //                           // formMFotoAddGroupUnitObjectChangeNotif.addFile();
            //                           formMFotoAddGroupUnitObjectChangeNotif.showBottomSheetChooseFile(context);
            //                         },
            //                         icon: Icon(
            //                           Icons.add_a_photo,
            //                           color: myPrimaryColor,
            //                           size: 33,
            //                         ))
            //                         : SizedBox(width: 0.0, height: 0.0)
            //                   ],
            //                 )
            //                 // : SizedBox()
            //                     : SizedBox(),
            //                 formMFotoAddGroupUnitObjectChangeNotif.objectUnitSelected != null
            //                 // ? formMFotoAddGroupUnitObjectChangeNotif
            //                 //             .objectUnitSelected.id !=
            //                 //         "011"
            //                     ? formMFotoAddGroupUnitObjectChangeNotif.autoValidateAddGroupUnitObject
            //                     ? Text("Tidak boleh kosong",
            //                     style: TextStyle(color: Colors.red, fontSize: 12))
            //                     : SizedBox()
            //                 // : SizedBox()
            //                     : SizedBox()
            //               ],
            //             ),
            //           ),
            //         );
            //       },
            //     );
            //   }
            // ),
        bottomNavigationBar: BottomAppBar(
          elevation: 0.0,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Consumer<FormMFotoAddGroupUnitObject>(
              builder: (context, formMFotoAddGroupUnitObjectChangeNotif, _) {
                return RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(8.0)),
                    color: myPrimaryColor,
                    onPressed: () {
                      if (widget.flag != 0) {
                        if (Provider.of<FormMFotoAddGroupUnitObject>(context, listen: false).check()) {
                          Provider.of<FormMFotoAddGroupUnitObject>(context,listen: false).autoValidateAddGroupUnitObject = true;
                        }
                        else {
                          bool _isDeleted = false;
                          for(int i=0; i<Provider.of<FormMFotoAddGroupUnitObject>(context,listen: false).listFotoGroupObjectUnit.length; i++){
                            if(Provider.of<FormMFotoAddGroupUnitObject>(context,listen: false).listFotoGroupObjectUnit[i].fileHeaderID != ""){
                                _isDeleted = true;
                            }
                          }
                          Provider.of<FormMFotoChangeNotifier>(context,listen: false).updateToListGroupUnitObject(
                                  Provider.of<FormMFotoAddGroupUnitObject>(context,listen: false).groupObjectUnitSelected,
                                  Provider.of<FormMFotoAddGroupUnitObject>(context,listen: false).objectUnitSelected,
                                  Provider.of<FormMFotoAddGroupUnitObject>(context,listen: false).listFotoGroupObjectUnit,
                                  _isDeleted, context, widget.index);
                        }
                      } else {
                        if (formMFotoAddGroupUnitObjectChangeNotif.check()) {
                          formMFotoAddGroupUnitObjectChangeNotif.autoValidateAddGroupUnitObject = formMFotoAddGroupUnitObjectChangeNotif.check();
                        }
                        else {
                          bool _isDeleted = false;
                          for(int i=0; i<Provider.of<FormMFotoAddGroupUnitObject>(context,listen: false).listFotoGroupObjectUnit.length; i++){
                            if(Provider.of<FormMFotoAddGroupUnitObject>(context,listen: false).listFotoGroupObjectUnit[i].fileHeaderID != ""){
                              _isDeleted = true;
                            }
                          }
                          Provider.of<FormMFotoChangeNotifier>(context,listen: false).addToListGroupUnitObject(
                              formMFotoAddGroupUnitObjectChangeNotif.groupObjectUnitSelected,
                                  formMFotoAddGroupUnitObjectChangeNotif.objectUnitSelected,
                                  formMFotoAddGroupUnitObjectChangeNotif.listFotoGroupObjectUnit,
                                  _isDeleted, context);
                        }
                      }
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("SAVE",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                                letterSpacing: 1.25))
                      ],
                    ));
              },
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _listWidgetImageGroupObjectUnit(List<ImageFileModel> data, BuildContext context) {
    List<Widget> _newListWidget = [];
    for (int i = 0; i < data.length; i++) {
      _newListWidget.add(Container(
        child: PopupMenuButton<String>(
            onSelected: (value) {
              Provider.of<FormMFotoAddGroupUnitObject>(context, listen: false).actionImageSelected(value, i,context);
            },
            child: Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: Image.file(data[i].imageFile,
                        width: 60,
                        height: 60,
                        fit: BoxFit.cover,
                        filterQuality: FilterQuality.medium),
                  ),
                )
              ],
            ),
            itemBuilder: (context) {
              return data[i].fileHeaderID != "" && Provider.of<FormMFotoChangeNotifier>(context, listen: false).lastKnownState != "IDE" ?
              TitlePopUpMenuButtonSRE.choices.map((String choice) {
                return PopupMenuItem(
                  value: choice,
                  child: Text(choice),
                );
              }).toList()
                  :
              TitlePopUpMenuButton.choices.map((String choice) {
                return PopupMenuItem(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            }),
      ));
    }
    return _newListWidget;
  }

  Future<bool> _onWillPop() async {
    if (widget.flag == 0) {
      if (Provider.of<FormMFotoAddGroupUnitObject>(context, listen: false).groupObjectUnitSelected != null &&
              Provider.of<FormMFotoAddGroupUnitObject>(context, listen: false).objectUnitSelected != null ||
          Provider.of<FormMFotoAddGroupUnitObject>(context, listen: false).listFotoGroupObjectUnit.isNotEmpty) {
        return (await showDialog(
              context: context,
              builder: (myContext) => AlertDialog(
                title: new Text('Warning'),
                content: new Text('Keluar dengan simpan perubahan?'),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      if (Provider.of<FormMFotoAddGroupUnitObject>(context,listen: false).check()) {
                        Provider.of<FormMFotoAddGroupUnitObject>(context,listen: false).autoValidateAddGroupUnitObject = true;
                        Navigator.of(myContext).pop(false);
                      } else {
                        bool _isDeleted = false;
                        for(int i=0; i<Provider.of<FormMFotoAddGroupUnitObject>(context,listen: false).listFotoGroupObjectUnit.length; i++){
                          if(Provider.of<FormMFotoAddGroupUnitObject>(context,listen: false).listFotoGroupObjectUnit[i].fileHeaderID != ""){
                            _isDeleted = true;
                          }
                        }
                        Provider.of<FormMFotoChangeNotifier>(context,listen: false).
                        addToListGroupUnitObject(Provider.of<FormMFotoAddGroupUnitObject>(context,listen: false).groupObjectUnitSelected,
                        Provider.of<FormMFotoAddGroupUnitObject>(context,listen: false).objectUnitSelected,
                        Provider.of<FormMFotoAddGroupUnitObject>(context,listen: false).listFotoGroupObjectUnit, _isDeleted, context);
                      }
                    },
                    child: new Text('Ya', style: TextStyle(color: Colors.grey)),
                  ),
                  new FlatButton(
                    onPressed: () => Navigator.of(context).pop(true),
                    child: new Text('Tidak'),
                  ),
                ],
              ),
            )) ??
            false;
      } else {
        return true;
      }
    } else {
      if (Provider.of<FormMFotoAddGroupUnitObject>(context, listen: false).groupObjectUnitTemp.id != Provider.of<FormMFotoAddGroupUnitObject>(context, listen: false).groupObjectUnitSelected.id ||
          Provider.of<FormMFotoAddGroupUnitObject>(context, listen: false).objectUnitTemp.id != Provider.of<FormMFotoAddGroupUnitObject>(context, listen: false).objectUnitSelected.id ||
          Provider.of<FormMFotoAddGroupUnitObject>(context, listen: false).lengthListFileImage != Provider.of<FormMFotoAddGroupUnitObject>(context, listen: false).listFotoGroupObjectUnit.length) {
        return (await showDialog(
              context: context,
              builder: (myContext) => AlertDialog(
                title: new Text('Warning'),
                content: new Text('Keluar dengan simpan perubahan?'),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      if (Provider.of<FormMFotoAddGroupUnitObject>(context,listen: false).check()) {
                        Provider.of<FormMFotoAddGroupUnitObject>(context,listen: false).autoValidateAddGroupUnitObject = true;
                        Navigator.of(myContext).pop(false);
                      } else {
                        bool _isDeleted = false;
                        for(int i=0; i<Provider.of<FormMFotoAddGroupUnitObject>(context,listen: false).listFotoGroupObjectUnit.length; i++){
                          if(Provider.of<FormMFotoAddGroupUnitObject>(context,listen: false).listFotoGroupObjectUnit[i].fileHeaderID != ""){
                            _isDeleted = true;
                          }
                        }
                        Provider.of<FormMFotoChangeNotifier>(context,listen: false).updateToListGroupUnitObject(
                                Provider.of<FormMFotoAddGroupUnitObject>(context,listen: false).groupObjectUnitSelected,
                                Provider.of<FormMFotoAddGroupUnitObject>(context,listen: false).objectUnitSelected,
                                Provider.of<FormMFotoAddGroupUnitObject>(context,listen: false).listFotoGroupObjectUnit,
                                _isDeleted, context, widget.index);
                      }
                    },
                    child: new Text('Ya', style: TextStyle(color: Colors.grey)),
                  ),
                  new FlatButton(
                    onPressed: () => Navigator.of(context).pop(true),
                    child: new Text('Tidak'),
                  ),
                ],
              ),
            )) ??
            false;
      } else {
        return true;
      }
    }
  }
}
