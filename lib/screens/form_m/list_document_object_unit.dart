import 'package:ad1ms2_dev/screens/form_m/add_document_object_unit.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_add_document_object_unit_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class ListDocumentObjectUnitProvider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
        data: ThemeData(
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
        ),
        child: Scaffold(
          appBar: AppBar(
            title: Text("List Dokumen Unit Object",
                style: TextStyle(color: Colors.black)),
            centerTitle: true,
            backgroundColor: myPrimaryColor,
            iconTheme: IconThemeData(color: Colors.black),
          ),
          body: Consumer<FormMFotoChangeNotifier>(
            builder: (context, formMFotoChangeNotif, _) {
              return formMFotoChangeNotif.listDocument.isEmpty
              ? Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // Image.asset("img/alamat_kosong.png", height: MediaQuery.of(context).size.height / 9,),
                    // SizedBox(height: MediaQuery.of(context).size.height / 47,),
                    Text("Tambah Dokumen Unit", style: TextStyle(color: Colors.grey, fontSize: 16),)
                  ],
                ),
              )
              : ListView.builder(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.height / 77,
                      horizontal: MediaQuery.of(context).size.width / 47),
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () {
                        // print(formMFotoChangeNotif
                        //     .listDocument[index].fileDocumentUnitObject);
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ChangeNotifierProvider(
                                      create: (context) => FormMFotoAddDocumentObjectUnit(),
                                      child: AddDocumentObjectUnitProvider(
                                          1,
                                          index,
                                          formMFotoChangeNotif
                                              .listDocument[index]),
                                    )));
                      },
                      child: Card(
                        elevation: 3.3,
                       shape: formMFotoChangeNotif.listDocument[index].isEdit
                      ?
                      RoundedRectangleBorder(
                      side: BorderSide(color: Colors.purple, width: 2),
                        borderRadius: BorderRadius.all(Radius.circular(4))
                      ) : null,
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: MediaQuery.of(context).size.height / 77,
                              horizontal:
                                  MediaQuery.of(context).size.width / 47),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                flex: 20,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: Text("Jenis Dokumen"),
                                          flex: 4,
                                        ),
                                        Expanded(child: Text("  :  "), flex: 0),
                                        Expanded(
                                          child: Text("${formMFotoChangeNotif.listDocument[index].jenisDocument.docTypeId} - ${formMFotoChangeNotif.listDocument[index].jenisDocument.docTypeName}"),
                                          flex: 6,
                                        )
                                      ],
                                    ),
                                    SizedBox(
                                      height:
                                          MediaQuery.of(context).size.height /
                                              97,
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                            child: Text("Tanggal terima"),
                                            flex: 4),
                                        Expanded(child: Text("  :  "), flex: 0),
                                        Expanded(
                                            child: Text(
                                                "${dateFormat.format(formMFotoChangeNotif.listDocument[index].dateTime)}"),
                                            flex: 6),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                  flex: 3,
                                  child:
                                  // formMFotoChangeNotif.listDocument[index].orderSupportingDocumentID != "NEW" && formMFotoChangeNotif.lastKnownState != "IDE" ?
                                  // SizedBox()
                                  // :
                                  IconButton(
                                    icon: Icon(
                                      Icons.delete,
                                      color: Colors.red,
                                    ),
                                    onPressed: () {
                                      formMFotoChangeNotif.deleteListDocumentObjectUnit(context,index);
                                    }
                                  )
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                  itemCount: formMFotoChangeNotif.listDocument.length);
            },
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ChangeNotifierProvider(
                            create: (context) =>
                                FormMFotoAddDocumentObjectUnit(),
                            child: AddDocumentObjectUnitProvider(0, null, null),
                          )));
            },
            child: Icon(Icons.add, color: Colors.black),
            backgroundColor: myPrimaryColor,
          ),
        ));
  }
}
