import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_collatelar_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/search_collateral_type_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchCollateralType extends StatefulWidget {
  @override
  _SearchCollateralTypeState createState() => _SearchCollateralTypeState();
}

class _SearchCollateralTypeState extends State<SearchCollateralType> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Consumer<SearchCollateralTypeChangeNotifier>(
          builder: (context, searchCollateralTypeChangeNotifier, _) {
            return TextFormField(
              controller: searchCollateralTypeChangeNotifier.controllerSearch,
              style: TextStyle(color: Colors.black),
              textInputAction: TextInputAction.search,
              onFieldSubmitted: (e) {
//            _getCustomer(e);
              },
              onChanged: (e) {
                searchCollateralTypeChangeNotifier.changeAction(e);
              },
              cursorColor: Colors.black,
              decoration: new InputDecoration(
                hintText: "Cari Jenis Jaminan (minimal 3 karakter)",
                hintStyle: TextStyle(color: Colors.black),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: myPrimaryColor),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: myPrimaryColor),
                ),
              ),
              autofocus: true,
            );
          },
        ),
        backgroundColor: myPrimaryColor,
        iconTheme: IconThemeData(color: Colors.black),
        actions: <Widget>[
          Provider.of<SearchCollateralTypeChangeNotifier>(context, listen: true)
              .showClear
              ? IconButton(
              icon: Icon(Icons.clear),
              onPressed: () {
                Provider.of<SearchCollateralTypeChangeNotifier>(context,
                    listen: false)
                    .controllerSearch
                    .clear();
                Provider.of<SearchCollateralTypeChangeNotifier>(context,
                    listen: false)
                    .changeAction(
                    Provider.of<SearchCollateralTypeChangeNotifier>(context,
                        listen: false)
                        .controllerSearch
                        .text);
              })
              : SizedBox(
            width: 0.0,
            height: 0.0,
          )
        ],
      ),
      body: Consumer<SearchCollateralTypeChangeNotifier>(
        builder: (context, searchCollateralTypeChangeNotifier, _) {
          return ListView.separated(
            padding: EdgeInsets.symmetric(
                vertical: MediaQuery.of(context).size.height / 57,
                horizontal: MediaQuery.of(context).size.width / 27),
            itemCount: Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).groupObjectSelected.KODE != "003"
                ? Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).groupObjectSelected.KODE == "001" || Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).groupObjectSelected.KODE == "002" || Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).groupObjectSelected.KODE == "007"
                  ? searchCollateralTypeChangeNotifier.listCollateralType.length
                  : searchCollateralTypeChangeNotifier.listCollateralTypePaket.length
                : searchCollateralTypeChangeNotifier.listCollateralTypeDurable.length,
            itemBuilder: (listContext, index) {
              return InkWell(
                onTap: () {
                  Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).groupObjectSelected.KODE != "003"
                      ?
                  Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).groupObjectSelected.KODE == "001" || Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).groupObjectSelected.KODE == "002" || Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).groupObjectSelected.KODE == "007"
                      ?
                  Navigator.pop(context, searchCollateralTypeChangeNotifier.listCollateralType[index])
                      :
                  Navigator.pop(context, searchCollateralTypeChangeNotifier.listCollateralTypePaket[index])
                      :
                  Navigator.pop(context, searchCollateralTypeChangeNotifier.listCollateralTypeDurable[index]);
                },
                child: Container(
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).groupObjectSelected.KODE != "003"
                          ?
                      Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).groupObjectSelected.KODE == "001" || Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).groupObjectSelected.KODE == "002" || Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).groupObjectSelected.KODE == "007"
                          ?
                      Text(
                        "${searchCollateralTypeChangeNotifier.listCollateralType[index].id} - "
                            "${searchCollateralTypeChangeNotifier.listCollateralType[index].name} ",
                        style: TextStyle(fontSize: 16),
                      )
                          :
                      Text(
                        "${searchCollateralTypeChangeNotifier.listCollateralTypePaket[index].id} - "
                            "${searchCollateralTypeChangeNotifier.listCollateralTypePaket[index].name} ",
                        style: TextStyle(fontSize: 16),
                      )
                          :
                      Text(
                        "${searchCollateralTypeChangeNotifier.listCollateralTypeDurable[index].id} - "
                            "${searchCollateralTypeChangeNotifier.listCollateralTypeDurable[index].name} ",
                        style: TextStyle(fontSize: 16),
                      )
                    ],
                  ),
                ),
              );
            },
            separatorBuilder: (context, index) {
              return Divider();
            },
          );
        },
      ),
    );
  }
}