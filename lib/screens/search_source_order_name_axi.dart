import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/search_source_order_name_axi_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchSourceOrderNameAXI extends StatefulWidget {
  @override
  _SearchSourceOrderNameAXIState createState() => _SearchSourceOrderNameAXIState();
}

class _SearchSourceOrderNameAXIState extends State<SearchSourceOrderNameAXI> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<SearchSourceOrderNameAXIChangeNotifier>(context,listen: false).scaffoldKey,
        appBar: AppBar(
          title: Consumer<SearchSourceOrderNameAXIChangeNotifier>(
            builder: (context, searchSourceOrderNameAXIChangeNotifier, _) {
              return TextFormField(
                controller: searchSourceOrderNameAXIChangeNotifier.controllerSearch,
                style: TextStyle(color: Colors.black),
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (query) {
                  if(query.length >= 3) {
                    searchSourceOrderNameAXIChangeNotifier.getSourceOrderNameAXI(query);
                  } else {
                    searchSourceOrderNameAXIChangeNotifier.showSnackBar("Input minimal 3 karakter");
                  }
                },
                onChanged: (e) {
                  searchSourceOrderNameAXIChangeNotifier.changeAction(e);
                },
                textCapitalization: TextCapitalization.characters,
                cursorColor: Colors.black,
                decoration: new InputDecoration(
                  hintText: "Cari Nama Sumber Order (minimal 3 karakter)",
                  hintStyle: TextStyle(color: Colors.black),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                ),
                autofocus: true,
              );
            },
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            Provider.of<SearchSourceOrderNameAXIChangeNotifier>(context,
                        listen: true)
                    .showClear
                ? IconButton(
                    icon: Icon(Icons.clear),
                    onPressed: () {
                      Provider.of<SearchSourceOrderNameAXIChangeNotifier>(context,
                          listen: false).clearSearchTemp();
                      Provider.of<SearchSourceOrderNameAXIChangeNotifier>(context,
                              listen: false)
                          .controllerSearch
                          .clear();
                      Provider.of<SearchSourceOrderNameAXIChangeNotifier>(context,
                              listen: false)
                          .changeAction(
                              Provider.of<SearchSourceOrderNameAXIChangeNotifier>(
                                      context,
                                      listen: false)
                                  .controllerSearch
                                  .text);
                    })
                : SizedBox(
                    width: 0.0,
                    height: 0.0,
                  )
          ],
        ),
        body: Consumer<SearchSourceOrderNameAXIChangeNotifier>(
          builder: (context, searchSourceOrderNameAXIChangeNotifier, _) {
            return searchSourceOrderNameAXIChangeNotifier.loadData
                ?
            Center(
              child: CircularProgressIndicator(),
            )
                :
            searchSourceOrderNameAXIChangeNotifier.listSourceOrderNameAXITemp.isNotEmpty
                ?
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount:
              searchSourceOrderNameAXIChangeNotifier.listSourceOrderNameAXITemp.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(
                        context,
                        searchSourceOrderNameAXIChangeNotifier
                            .listSourceOrderNameAXITemp[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchSourceOrderNameAXIChangeNotifier.listSourceOrderNameAXITemp[index].kode} - "
                              "${searchSourceOrderNameAXIChangeNotifier.listSourceOrderNameAXITemp[index].deskripsi}",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            )
                :
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount:
                  searchSourceOrderNameAXIChangeNotifier.listSourceOrderNameAXI.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(
                        context,
                        searchSourceOrderNameAXIChangeNotifier
                            .listSourceOrderNameAXI[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchSourceOrderNameAXIChangeNotifier
                              .listSourceOrderNameAXI[index].kode} - "
                              "${searchSourceOrderNameAXIChangeNotifier
                              .listSourceOrderNameAXI[index].deskripsi}",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          },
        ),
      ),
    );
  }
}
