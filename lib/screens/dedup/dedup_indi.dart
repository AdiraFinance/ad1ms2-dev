// import 'package:ad1ms2_dev/db/database_helper.dart';
// import 'package:ad1ms2_dev/main.dart';
// import 'package:ad1ms2_dev/screens/list_oid.dart';
// import 'package:ad1ms2_dev/shared/constants.dart';
// import 'package:ad1ms2_dev/shared/date_picker.dart';
// import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';
// import 'package:ad1ms2_dev/shared/responsive_screen.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:intl/intl.dart';
// import 'package:provider/provider.dart';
//
// class FormDedupIndividu extends StatefulWidget {
//   @override
//   _FormDedupIndividuState createState() => _FormDedupIndividuState();
// }
//
// class _FormDedupIndividuState extends State<FormDedupIndividu> {
//   Screen _size;
//   final _controllerNomorIdentitas = TextEditingController();
//   final _controllerNamaLengkap = TextEditingController();
//   final _controllerTempatLahir = TextEditingController();
//   final _controllerNamaGadisIbuKandung = TextEditingController();
//   final _controllerAlamatIdentitas = TextEditingController();
//   final _controllerBirthDate = TextEditingController();
//   DateTime _initialDate;
//   bool _processSaveDedupData = false;
//   final _key = new GlobalKey<FormState>();
//   bool _autoValidate = false;
//   bool _loadData = false;
//
//   @override
//   void initState() {
//     super.initState();
//     _initialDate = DateTime(dateNow.year, dateNow.month, dateNow.day);
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     _size = Screen(MediaQuery.of(context).size);
//     return Theme(
//       data: ThemeData(
//           fontFamily: "NunitoSans",
//           primaryColor: Colors.black,
//           accentColor: myPrimaryColor,
//           primarySwatch: primaryOrange
//       ),
//       child: Scaffold(
//         appBar: AppBar(
//           backgroundColor: myPrimaryColor,
//           centerTitle: true,
//           title: Text("Dedup Individu", style: TextStyle(color: Colors.black)),
//           iconTheme: IconThemeData(color: Colors.black),
//         ),
//         body: _loadData
//             ? Center(child: CircularProgressIndicator())
//             : Form(
//           key: _key,
//           child: SingleChildScrollView(
//             padding: EdgeInsets.symmetric(
//                 vertical: _size.hp(2.5), horizontal: _size.wp(3)),
//             child: Column(
//               children: <Widget>[
//                 TextFormField(
//                   autovalidate: _autoValidate,
//                   validator: (e) {
//                     if (e.isEmpty) {
//                       return "Tidak boleh kosong";
//                     } else {
//                       return null;
//                     }
//                   },
//                   controller: _controllerNomorIdentitas,
//                   style: new TextStyle(color: Colors.black),
//                   decoration: new InputDecoration(
//                       labelText: 'Nomor Identitas',
//                       labelStyle: TextStyle(color: Colors.black),
//                       border: OutlineInputBorder(
//                           borderRadius: BorderRadius.circular(8))),
//                   keyboardType: TextInputType.number,
//                   textCapitalization: TextCapitalization.characters,
//                   inputFormatters: [
//                     WhitelistingTextInputFormatter.digitsOnly,
//                     LengthLimitingTextInputFormatter(16)
//                   ],
//                 ),
//                 SizedBox(height: MediaQuery.of(context).size.height / 47),
//                 TextFormField(
//                   controller: _controllerNamaLengkap,
//                   style: new TextStyle(color: Colors.black),
//                   decoration: new InputDecoration(
//                       labelText: 'Nama Lengkap',
//                       labelStyle: TextStyle(color: Colors.black),
//                       border: OutlineInputBorder(
//                           borderRadius: BorderRadius.circular(8))),
//                   validator: (e) {
//                     if (e.isEmpty) {
//                       return "Tidak boleh kosong";
//                     } else {
//                       return null;
//                     }
//                   },
//                   autovalidate: _autoValidate,
//                   keyboardType: TextInputType.text,
//                   textCapitalization: TextCapitalization.characters,
//                 ),
//                 SizedBox(height: MediaQuery.of(context).size.height / 47),
//                 TextFormField(
//                   autovalidate: _autoValidate,
//                   validator: (e) {
//                     if (e.isEmpty) {
//                       return "Tidak boleh kosong";
//                     } else {
//                       return null;
//                     }
//                   },
//                   controller: _controllerBirthDate,
//                   style: TextStyle(color: Colors.black),
//                   decoration: InputDecoration(
//                       labelText: 'Tanggal Lahir',
//                       labelStyle: TextStyle(color: Colors.black),
//                       border: OutlineInputBorder(
//                           borderRadius: BorderRadius.circular(8))),
//                   onTap: () {
//                     _showDatePicker();
//                   },
//                   readOnly: true,
//                 ),
//                 SizedBox(height: MediaQuery.of(context).size.height / 47),
//                 TextFormField(
//                   controller: _controllerTempatLahir,
//                   style: new TextStyle(color: Colors.black),
//                   decoration: new InputDecoration(
//                       labelText: 'Tempat Lahir Sesuai Identitas',
//                       labelStyle: TextStyle(color: Colors.black),
//                       border: OutlineInputBorder(
//                           borderRadius: BorderRadius.circular(8))),
//                   validator: (e) {
//                     if (e.isEmpty) {
//                       return "Tidak boleh kosong";
//                     } else {
//                       return null;
//                     }
//                   },
//                   autovalidate: _autoValidate,
//                   keyboardType: TextInputType.text,
//                   textCapitalization: TextCapitalization.characters,
//                 ),
//                 SizedBox(height: MediaQuery.of(context).size.height / 47),
//                 TextFormField(
//                   controller: _controllerNamaGadisIbuKandung,
//                   style: new TextStyle(color: Colors.black),
//                   decoration: new InputDecoration(
//                       labelText: 'Nama Gadis Ibu kandung',
//                       labelStyle: TextStyle(color: Colors.black),
//                       border: OutlineInputBorder(
//                           borderRadius: BorderRadius.circular(8))),
//                   validator: (e) {
//                     if (e.isEmpty) {
//                       return "Tidak boleh kosong";
//                     } else {
//                       return null;
//                     }
//                   },
//                   autovalidate: _autoValidate,
//                   keyboardType: TextInputType.text,
//                   textCapitalization: TextCapitalization.characters,
//                 ),
//                 SizedBox(height: MediaQuery.of(context).size.height / 47),
//                 TextFormField(
//                   // autovalidate: _autoValidate,
//                   // validator: (e) {
//                   //   if (e.isEmpty) {
//                   //     return "Tidak boleh kosong";
//                   //   } else {
//                   //     return null;
//                   //   }
//                   // },
//                   controller: _controllerAlamatIdentitas,
//                   style: new TextStyle(color: Colors.black),
//                   decoration: new InputDecoration(
//                       labelText: 'Alamat Identitas',
//                       labelStyle: TextStyle(color: Colors.black),
//                       border: OutlineInputBorder(
//                           borderRadius: BorderRadius.circular(8))),
//                   maxLines: 3,
//                   keyboardType: TextInputType.text,
//                   textCapitalization: TextCapitalization.characters,
//                 ),
//                 SizedBox(height: MediaQuery.of(context).size.height / 47),
//                 RaisedButton(
//                     onPressed: () {
//                       // _insertDataDedup();
//                       _check();
//                     },
//                     padding:
//                     EdgeInsets.symmetric(vertical: _size.hp(1.5)),
//                     color: myPrimaryColor,
//                     shape: RoundedRectangleBorder(
//                         borderRadius: BorderRadius.circular(8)),
//                     child: Row(
//                         mainAxisAlignment: MainAxisAlignment.center,
//                         children: <Widget>[
//                           Text(
//                             "SUBMIT",
//                             style: TextStyle(
//                                 fontSize: 14,
//                                 fontWeight: FontWeight.w500,
//                                 letterSpacing: 1.25),
//                           )
//                         ]))
//               ],
//             ),
//           ),
//         ),
//       ),
//     );
//   }
//
//   _showDatePicker() async {
//     // DatePickerShared _datePicker = DatePickerShared();
//     // var _dateSelected = await _datePicker.selectStartDate(context, _initialDate,
//     //     canAccessNextDay: false);
//     // if (_dateSelected != null) {
//     //   setState(() {
//     //     _initialDate = _dateSelected;
//     //     _controllerBirthDate.text = dateFormat.format(_dateSelected);
//     //   });
//     // } else {
//     //   return;
//     // }
//     var _dateSelected = await selectDate(context, _initialDate);
//     if (_dateSelected != null) {
//       setState(() {
//         _initialDate = _dateSelected;
//         _controllerBirthDate.text = dateFormat.format(_dateSelected);
//       });
//     } else {
//       return;
//     }
//   }
//
//   _check() {
//     final _form = _key.currentState;
//     if (_form.validate()) {
//       Navigator.push(
//           context,
//           MaterialPageRoute(
//               builder: (context) => ListOid(
//                 flag: "PER",
//                 identityNumber: _controllerNomorIdentitas.text,
//                 fullname: _controllerNamaLengkap.text,
//                 birthDate: _controllerBirthDate.text,
//                 birthPlace: _controllerTempatLahir.text,
//                 motherName: _controllerNamaGadisIbuKandung.text,
//                 identityAddress: _controllerAlamatIdentitas.text,
//                 initialDateBirthDate: _initialDate,
//               )
//           )
//       );
//     } else {
//       setState(() {
//         _autoValidate = true;
//       });
//     }
//   }
// }

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/camera_screen.dart';
import 'package:ad1ms2_dev/screens/list_oid.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/date_picker.dart';
import 'package:ad1ms2_dev/shared/dedup/dedup_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';
import 'package:ad1ms2_dev/shared/resource/validate_data.dart';
import 'package:ad1ms2_dev/shared/responsive_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class FormDedupIndividu extends StatefulWidget {
  @override
  _FormDedupIndividuState createState() => _FormDedupIndividuState();
}

class _FormDedupIndividuState extends State<FormDedupIndividu> {
  Screen _size;
  Future<void> _setGetData;

  @override
  void initState() {
    super.initState();
    _setGetData = Provider.of<DedupIndiChangeNotifier>(context,listen: false).setValueMandatoryVisible();
  }

  @override
  Widget build(BuildContext context) {
    _size = Screen(MediaQuery.of(context).size);
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        primaryColor: Colors.black,
        accentColor: myPrimaryColor,
        primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<DedupIndiChangeNotifier>(context,listen: false).scaffoldKey,
        appBar: AppBar(
          backgroundColor: myPrimaryColor,
          centerTitle: true,
          title: Text("Dedup Individu", style: TextStyle(color: Colors.black)),
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: FutureBuilder(
          future: _setGetData,
            builder: (context,snapshot){
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
              return Consumer<DedupIndiChangeNotifier>(
                builder: (context,dedupIndi,_){
                  return Form(
                    key: dedupIndi.key,
                    child: SingleChildScrollView(
                      padding: EdgeInsets.symmetric(
                          vertical: _size.hp(2.5), horizontal: _size.wp(3)),
                      child: Column(
                        children: <Widget>[
                          Visibility(
                            visible: dedupIndi.isIdentityNoVisible,
                            child: Row(
                              children: [
                                Expanded(
                                  flex: 6,
                                  child: TextFormField(
                                    autovalidate: dedupIndi.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty && dedupIndi.isIdentityNoMandatory) {
                                        return "Tidak boleh kosong";
                                      } else if(e.length != 16){
                                        return "KTP wajib 16 digit";
                                      }
                                      else {
                                        return null;
                                      }
                                    },
                                    controller: dedupIndi.controllerNomorIdentitas,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        labelText: 'Nomor Identitas',
                                        labelStyle: TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8))),
                                    keyboardType: TextInputType.number,
                                    textCapitalization: TextCapitalization.characters,
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter.digitsOnly,
                                      LengthLimitingTextInputFormatter(16)
                                    ],
                                    onTap: (){
                                      dedupIndi.validateAgeOrName(context, "NAME");
                                      dedupIndi.validateAgeOrName(context, "MOTHER_NAME");
                                    },
                                  ),
                                ),
                                Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: RaisedButton(
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(8)),
                                          onPressed: (){
                                            Navigator.push(context, MaterialPageRoute(builder: (context) => CameraScreen()))
                                                .then((value) => dedupIndi.controllerNomorIdentitas.text = value);
                                            },
                                          child: Text("OCR")
                                      ),
                                ), flex: 4)
                              ],
                            ),
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height / 47),
                          Visibility(
                            visible: dedupIndi.isFullNameVisible,
                            child: TextFormField(
                              controller: dedupIndi.controllerNamaLengkap,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  labelText: 'Nama Lengkap',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)
                                  )
                              ),
                              validator: (e) {
                                if (e.isEmpty && dedupIndi.isFullNameMandatory) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              autovalidate: dedupIndi.autoValidate,
                              keyboardType: TextInputType.text,
                              textCapitalization: TextCapitalization.characters,
                              inputFormatters: [
                                FilteringTextInputFormatter.allow(RegExp('[A-Z \'`]')),
                              ],
                              onFieldSubmitted: (value){
                                dedupIndi.validateAgeOrName(context, "NAME");
                              },
                              onTap: (){
                                dedupIndi.validateAgeOrName(context, "NAME");
                                dedupIndi.validateAgeOrName(context, "MOTHER_NAME");
                              },
                            ),
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height / 47),
                          Visibility(
                            visible: dedupIndi.isBirthDateVisible,
                            child: TextFormField(
                              autovalidate: dedupIndi.autoValidate,
                              validator: (e) {
                                if (e.isEmpty && dedupIndi.isBirthDateMandatory) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              controller: dedupIndi.controllerBirthDate,
                              style: TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                  labelText: 'Tanggal Lahir',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              onTap: () {
                                dedupIndi.showDatePicker(context);
                                dedupIndi.validateAgeOrName(context, "NAME");
                                dedupIndi.validateAgeOrName(context, "MOTHER_NAME");
                              },
                              readOnly: true,
                            ),
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height / 47),
                          Visibility(
                            visible: dedupIndi.isBirthPlaceVisible,
                            child: TextFormField(
                              controller: dedupIndi.controllerTempatLahir,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  labelText: 'Tempat Lahir Sesuai Identitas',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              validator: (e) {
                                if (e.isEmpty  && dedupIndi.isBirthPlaceMandatory) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              autovalidate: dedupIndi.autoValidate,
                              keyboardType: TextInputType.text,
                              textCapitalization: TextCapitalization.characters,
                              inputFormatters: [
                                FilteringTextInputFormatter.allow(RegExp('[A-Z ]')),
                              ],
                              onTap: (){
                                dedupIndi.validateAgeOrName(context, "NAME");
                                dedupIndi.validateAgeOrName(context, "MOTHER_NAME");
                              },
                            ),
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height / 47),
                          Visibility(
                            visible: dedupIndi.isMotherNameVisible,
                            child: TextFormField(
                              controller: dedupIndi.controllerNamaGadisIbuKandung,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  labelText: 'Nama Gadis Ibu kandung',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              validator: (e) {
                                if (e.isEmpty && dedupIndi.isMotherNameMandatory) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              autovalidate: dedupIndi.autoValidate,
                              keyboardType: TextInputType.text,
                              textCapitalization: TextCapitalization.characters,
                              inputFormatters: [
                                FilteringTextInputFormatter.allow(RegExp('[A-Z \`]')),
                              ],
                              onFieldSubmitted: (value){
                                dedupIndi.validateAgeOrName(context, "MOTHER_NAME");
                              },
                              onTap: (){
                                dedupIndi.validateAgeOrName(context, "NAME");
                                dedupIndi.validateAgeOrName(context, "MOTHER_NAME");
                              },
                            ),
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height / 47),
                          Visibility(
                            visible: dedupIndi.isAddressIdentityVisible,
                            child: TextFormField(
                              autovalidate: dedupIndi.autoValidate,
                              validator: (e) {
                                if (e.isEmpty && dedupIndi.isAddressIdentityMandatory) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              controller: dedupIndi.controllerAlamatIdentitas,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  labelText: 'Alamat Identitas',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              maxLines: 3,
                              keyboardType: TextInputType.text,
                              textCapitalization: TextCapitalization.characters,
                              inputFormatters: [
                                FilteringTextInputFormatter.allow(RegExp('[A-Z0-9 /.\`-]')),
                              ],
                              onTap: (){
                                dedupIndi.validateAgeOrName(context, "NAME");
                                dedupIndi.validateAgeOrName(context, "MOTHER_NAME");
                              },
                            ),
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height / 47),
                          dedupIndi.loadData
                              ?
                          Center(
                            child: CircularProgressIndicator(),
                          )
                              :
                          RaisedButton(
                              onPressed: () {
                                // _insertDataDedup();
                                dedupIndi.check(context);
                              },
                              padding:
                              EdgeInsets.symmetric(vertical: _size.hp(1.5)),
                              color: myPrimaryColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      "SUBMIT",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500,
                                          letterSpacing: 1.25),
                                    )
                                  ]))
                        ],
                      ),
                    ),
                  );
                }
              );
            }
        ),
      ),
    );
  }
}
//
// //import 'package:ad1ms2_dev/screens/ide/ide_dokumen.dart';
// //import 'package:ad1ms2_dev/shared/constants.dart';
// //import 'package:flutter/material.dart';
// // import 'package:AD1MS2/screens/ide/ide_main.dart';
//
// //class DedupIndi extends StatefulWidget {
// //  @override
// //  _DedupIndiState createState() => _DedupIndiState();
// //}
// //
// //class _DedupIndiState extends State<DedupIndi> {
// //  @override
// //  Widget build(BuildContext context) {
// //    return Scaffold(
// //      appBar: AppBar(
// //        centerTitle: true,
// //        title: Text('Dedup Individu'),
// //      ),
// //      body: Container(
// //        padding: EdgeInsets.zero,
// //        child: ListView(
// //          children: <Widget>[
// //            SizedBox(
// //              height: 5.0,
// //            ),
// //            TextFormField(
// //              decoration: textInputDecoration.copyWith(
// //                  hintText: 'Input No KTP', labelText: 'No Identitas'),
// //              validator: (val) => val.isEmpty ? 'Enter an Email!' : null,
// //              onChanged: (val) {
// ////                setState(() => email = val);
// //              },
// //            ),
// //            SizedBox(
// //              height: 5.0,
// //            ),
// //            TextFormField(
// //              decoration: textInputDecoration.copyWith(
// //                  hintText: 'Input Nama Lengkap', labelText: 'Nama Lengkap'),
// //              validator: (val) => val.isEmpty ? 'Enter an Email!' : null,
// //              onChanged: (val) {
// ////                setState(() => email = val);
// //              },
// //            ),
// //            SizedBox(
// //              height: 5.0,
// //            ),
// //            TextFormField(
// //              decoration: textInputDecoration.copyWith(
// //                  hintText: 'DD/MMM/YYYY', labelText: 'Tanggal Lahir'),
// //              validator: (val) => val.isEmpty ? 'Enter an Email!' : null,
// //              onChanged: (val) {
// ////                setState(() => email = val);
// //              },
// //            ),
// //            SizedBox(
// //              height: 5.0,
// //            ),
// //            TextFormField(
// //              decoration: textInputDecoration.copyWith(
// //                  hintText: 'Input Tempat Lahir', labelText: 'Tempat Lahir'),
// //              validator: (val) => val.isEmpty ? 'Enter an Email!' : null,
// //              onChanged: (val) {
// ////                setState(() => email = val);
// //              },
// //            ),
// //            SizedBox(
// //              height: 5.0,
// //            ),
// //            TextFormField(
// //              decoration: textInputDecoration.copyWith(
// //                  hintText: 'Input Nama Gadis Ibu Kandung',
// //                  labelText: 'Nama Ibu'),
// //              validator: (val) => val.isEmpty ? 'Enter an Email!' : null,
// //              onChanged: (val) {
// ////                setState(() => email = val);
// //              },
// //            ),
// //            SizedBox(
// //              height: 5.0,
// //            ),
// //            TextFormField(
// //              decoration: textInputDecoration.copyWith(
// //                  hintText: 'Input Alamat', labelText: 'Alamat'),
// //              validator: (val) => val.isEmpty ? 'Enter an Email!' : null,
// //              onChanged: (val) {
// ////                setState(() => email = val);
// //              },
// //            ),
// //            SizedBox(
// //              height: 20.0,
// //            ),
// //            RaisedButton(
// //              color: Colors.yellow[800],
// //              child: Text(
// //                'Dedup',
// //                style: TextStyle(
// //                  color: Colors.white,
// //                ),
// //              ),
// //              onPressed: () async {
// //                Navigator.push(
// //                  context,
// //                  MaterialPageRoute(builder: (context) => IdeDokumen()),
// //                );
// //              },
// //            ),
// //          ],
// //        ),
// //      ),
// //    );
// //  }
// //}
