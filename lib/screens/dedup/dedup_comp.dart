import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/form_m_company/form_m_company_parent_old.dart';
import 'package:ad1ms2_dev/screens/ide/ide_dokumen.dart';
import 'package:ad1ms2_dev/screens/list_oid.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/date_picker.dart';
import 'package:ad1ms2_dev/shared/dedup/dedup_company_change_notifier.dart';
import 'package:ad1ms2_dev/shared/responsive_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class FormDedupComp extends StatefulWidget {
  @override
  _FormDedupCompState createState() => _FormDedupCompState();
}

class _FormDedupCompState extends State<FormDedupComp> {

  Screen _size;
  Future<void> _setGetData;

  @override
  void initState() {
    super.initState();
    _setGetData = Provider.of<DedupCompanyChangeNotifier>(context,listen: false).setValueMandatoryVisible();
  }

  @override
  Widget build(BuildContext context) {
    _size = Screen(MediaQuery.of(context).size);
    return Theme(
      data: ThemeData(
          fontFamily: "NunitoSans",
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<DedupCompanyChangeNotifier>(context,listen: false).scaffoldKey,
          appBar: AppBar(
            backgroundColor: myPrimaryColor,
            centerTitle: true,
            title: Text('Dedup Kelembagaan',
                style: TextStyle(color: Colors.black)),
            iconTheme: IconThemeData(color: Colors.black),
          ),
          body: FutureBuilder(
            future: _setGetData,
            builder: (context,snapshot){
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
              return Consumer<DedupCompanyChangeNotifier>(
                builder: (context,dedupCom,_){
                  return SingleChildScrollView(
                    padding: EdgeInsets.symmetric(
                        vertical: _size.hp(2.5), horizontal: _size.wp(3)),
                    child: Form(
                      key: dedupCom.key,
                      child: Column(
                        children: <Widget>[
                          Visibility(
                            visible: dedupCom.isNameVisible,
                            child: TextFormField(
                              controller: dedupCom.controllerName,
                              autovalidate: dedupCom.autoValidate,
                              validator: (e) {
                                if (e.isEmpty && dedupCom.isNameVisible) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  labelText: 'Nama Lembaga',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              keyboardType: TextInputType.text,
                              textCapitalization: TextCapitalization.characters,
                              inputFormatters: [
                                FilteringTextInputFormatter.allow(RegExp('[A-Z  \'`0-9]')),
                              ],
                              onFieldSubmitted: (value){
                                dedupCom.validateNameOrNPWP(context, "NAME");
                              },
                              onTap: (){
                                dedupCom.validateNameOrNPWP(context, "NAME");
                                dedupCom.validateNameOrNPWP(context, "NPWP");
                              },
                            ),
                          ),
                          SizedBox(height: _size.hp(2.5)),
                          Visibility(
                            visible: dedupCom.isNoNPWPVisible,
                            child: TextFormField(
                              controller: dedupCom.controllerNPWPNumber,
                              autovalidate: dedupCom.autoValidate,
                              validator: (e) {
                                if (e.isEmpty && dedupCom.isNoNPWPMandatory) {
                                  return "Tidak boleh kosong";
                                }
                                else {
                                  return null;
                                }
                              },
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  labelText: 'NPWP',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              keyboardType: TextInputType.number,
                              inputFormatters: [
                                FilteringTextInputFormatter.allow(RegExp('[0-9]')),
                                LengthLimitingTextInputFormatter(15)
                              ],
                              onFieldSubmitted: (value){
                                dedupCom.validateNameOrNPWP(context, "NPWP");
                              },
                              onTap: (){
                                dedupCom.validateNameOrNPWP(context, "NAME");
                                dedupCom.validateNameOrNPWP(context, "NPWP");
                              },
                            ),
                          ),
                          SizedBox(height: _size.hp(2.5)),
                          Visibility(
                            visible: dedupCom.isAddressVisible,
                            child: TextFormField(
                              controller: dedupCom.controllerAddress,
                              autovalidate: dedupCom.autoValidate,
                              validator: (e) {
                                if (e.isEmpty && dedupCom.isAddressMandatory) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  labelText: 'Alamat NPWP',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              keyboardType: TextInputType.text,
                              textCapitalization: TextCapitalization.characters,
                              inputFormatters: [
                                FilteringTextInputFormatter.allow(RegExp('[A-Z0-9 /.\`]')),
                              ],
                              onTap: (){
                                dedupCom.validateNameOrNPWP(context, "NAME");
                                dedupCom.validateNameOrNPWP(context, "NPWP");
                              },
                            ),
                          ),
                          SizedBox(height: _size.hp(2.5)),
                          Visibility(
                            visible: dedupCom.isAddressVisible,
                            child: TextFormField(
                              autovalidate: dedupCom.autoValidate,
                              validator: (e) {
                                if (e.isEmpty && dedupCom.isAddressMandatory) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              controller: dedupCom.controllerTglPendirian,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  labelText: 'Tanggal Pendirian',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              onTap: () {
                                dedupCom.showDatePicker(context);
                                dedupCom.validateNameOrNPWP(context, "NAME");
                                dedupCom.validateNameOrNPWP(context, "NPWP");
                              },
                              readOnly: true,
                              keyboardType: TextInputType.text,
                              textCapitalization: TextCapitalization.characters,
                            ),
                          ),
                          SizedBox(height: _size.hp(3.5)),
                          dedupCom.loadData
                              ?
                          Center(child: CircularProgressIndicator())
                              :
                          RaisedButton(
                              onPressed: () {
                                dedupCom.check(context);
                              },
                              padding: EdgeInsets.symmetric(vertical: _size.hp(1.5)),
                              color: myPrimaryColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      "SUBMIT",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500,
                                          letterSpacing: 1.25),
                                    )
                                  ]))
                        ],
                      ),
                    ),
                  );
                },
              );
            },
          )
//        Container(
//          padding: EdgeInsets.zero,
//          child: ListView(
//            children: <Widget>[
//              SizedBox(
//                height: 5.0,
//              ),
//              TextFormField(
//                decoration: textInputDecoration.copyWith(
//                    hintText: 'Input No NPWP', labelText: 'NPWP'),
//                validator: (val) => val.isEmpty ? 'Enter an Email!' : null,
//                onChanged: (val) {
////                setState(() => email = val);
//                },
//              ),
//              SizedBox(
//                height: 5.0,
//              ),
//              TextFormField(
//                decoration: textInputDecoration.copyWith(
//                    hintText: 'Input Nama Lembaga', labelText: 'Nama Lembaga'),
//                validator: (val) => val.isEmpty ? 'Enter an Email!' : null,
//                onChanged: (val) {
////                setState(() => email = val);
//                },
//              ),
//              SizedBox(
//                height: 5.0,
//              ),
//              TextFormField(
//                decoration: textInputDecoration.copyWith(
//                    hintText: 'DD/MMM/YYYY', labelText: 'Tanggal Pendirian'),
//                validator: (val) => val.isEmpty ? 'Enter an Email!' : null,
//                onChanged: (val) {
////                setState(() => email = val);
//                },
//              ),
//              TextFormField(
//                decoration: textInputDecoration.copyWith(
//                    hintText: 'Input Alamat', labelText: 'Alamat'),
//                validator: (val) => val.isEmpty ? 'Enter an Email!' : null,
//                onChanged: (val) {
////                setState(() => email = val);
//                },
//              ),
//              SizedBox(
//                height: 20.0,
//              ),
//              RaisedButton(
//                color: Colors.yellow[800],
//                child: Text(
//                  'Dedup',
//                  style: TextStyle(
//                    color: Colors.white,
//                  ),
//                ),
//                onPressed: () async {
//                  Navigator.push(
//                    context,
//                    MaterialPageRoute(builder: (context) => IdeDokumen()),
//                  );
//                },
//              ),
//            ],
//          ),
//        ),
          ),
    );
  }

}
