import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/search_colla_merk_jenis_model_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchCollaMerkJenisModel extends StatefulWidget {
  final int type;
  final String groupObject;
  const SearchCollaMerkJenisModel({this.type, this.groupObject});

  @override
  _SearchCollaMerkJenisModelState createState() => _SearchCollaMerkJenisModelState();
}

class _SearchCollaMerkJenisModelState extends State<SearchCollaMerkJenisModel> {
  @override
  void initState() {
    super.initState();
    print("cek data = ${widget.type}");
    if(widget.type == 3){
      Provider.of<SearchCollaMerkJenisModelChangeNotifier>(context, listen: false).getData(context, widget.type, null, null);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<SearchCollaMerkJenisModelChangeNotifier>(context, listen: false).scaffoldKey,
        appBar: AppBar(
          title: Consumer<SearchCollaMerkJenisModelChangeNotifier>(
            builder: (context, _provider, _) {
              return TextFormField(
                controller: _provider.controllerSearch,
                style: TextStyle(color: Colors.black),
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (query) {
                  _provider.searchData(context, widget.type, query, widget.groupObject);
                },
                onChanged: (e) {
                  _provider.changeAction(e);
                },
                textCapitalization: TextCapitalization.characters,
                cursorColor: Colors.black,
                decoration: new InputDecoration(
                  hintText: "Cari ${widget.type == 1 ? "Merk" : widget.type == 2 ? "Jenis" : "Model"} Objek (minimal 3 karakter)",
                  hintStyle: TextStyle(color: Colors.black),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                ),
                autofocus: true,
              );
            },
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            Provider.of<SearchCollaMerkJenisModelChangeNotifier>(context, listen: true).showClear
                ?
            IconButton(
                icon: Icon(Icons.clear),
                onPressed: () {
                  Provider.of<SearchCollaMerkJenisModelChangeNotifier>(context, listen: false).controllerSearch.clear();
                  Provider.of<SearchCollaMerkJenisModelChangeNotifier>(context,listen: false)
                      .changeAction(Provider.of<SearchCollaMerkJenisModelChangeNotifier>(context,listen: false).controllerSearch.text);
                }
            )
                :
            SizedBox(width: 0.0, height: 0.0)
          ],
        ),
        body: Consumer<SearchCollaMerkJenisModelChangeNotifier>(
          builder: (context, _provider, _) {
            return _provider.loadData
                ?
            Center(child: CircularProgressIndicator())
                :
            _provider.listDataSearch.isEmpty
                ?
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27
              ),
              itemCount: _provider.listData.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(context, _provider.listData[index]);
                  },
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: [
                            Expanded(child: widget.type != 3
                                ?
                            Text(widget.type == 1
                                ?
                            "${_provider.listData[index].brandObjectModel.id} - ${_provider.listData[index].brandObjectModel.name}"
                                :
                            "${_provider.listData[index].objectTypeModel.id} - ${_provider.listData[index].objectTypeModel.name}",
                              style: TextStyle(fontSize: 16),
                            )
                                :
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("${_provider.listData[index].modelObjectModel.id} - ${_provider.listData[index].modelObjectModel.name}",
                                  style: TextStyle(fontSize: 16),
                                ),
                                SizedBox(height: MediaQuery.of(context).size.height / 57),
                                Text("${_provider.listData[index].objectUsageModel.id} - ${_provider.listData[index].objectUsageModel.name}",
                                  style: TextStyle(fontSize: 16),
                                ),
                              ],
                            )
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            )
                :
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27
              ),
              itemCount: _provider.listDataSearch.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(context, _provider.listDataSearch[index]);
                  },
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: [
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("${_provider.listDataSearch[index].modelObjectModel.id} - ${_provider.listDataSearch[index].modelObjectModel.name}",
                                    style: TextStyle(fontSize: 16),
                                  ),
                                  SizedBox(height: MediaQuery.of(context).size.height / 57),
                                  Text("${_provider.listDataSearch[index].objectUsageModel.id} - ${_provider.listDataSearch[index].objectUsageModel.name}",
                                    style: TextStyle(fontSize: 16),
                                  ),
                                ],
                              )
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          },
        )
      ),
    );
  }
}
