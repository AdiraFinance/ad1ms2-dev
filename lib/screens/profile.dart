import 'package:ad1ms2_dev/screens/app/info_credit_income.dart';
import 'package:ad1ms2_dev/screens/app/info_credit_structure.dart';
import 'package:ad1ms2_dev/screens/app/info_credit_structure_type_installment.dart';
import 'package:ad1ms2_dev/screens/app/info_wmp.dart';
import 'package:ad1ms2_dev/screens/app/list_additional_insurance.dart';
import 'package:ad1ms2_dev/screens/app/list_major_insurance.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_additional_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_major_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_password_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../main.dart';
import 'change_password.dart';

class Profile extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        var _providerCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: true);
        var _providerMajorInsurance = Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: true);
        var _providerAdditionalInsurance = Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: true);

        return Padding(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width/37,
                vertical: MediaQuery.of(context).size.height/57,
            ),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    InkWell(
                        onTap: () {
                            // Navigator.push(
                            //     context,
                            //     MaterialPageRoute(
                            //         builder: (context) => InfoCreditStructure()
                            //     )
                            // );
                        },
                        child: Card(
                            elevation: 3.3,
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text(
                                            "Edit Profile"),
                                        _providerCreditStructure.flag
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height / 87),
                    InkWell(
                        onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ChangeNotifierProvider(
                                        create: (context) => ChangePasswordChangeNotifier(),
                                        child: ChangePassword(),
                                    )
                                )
                            );
                        },
                        child: Card(
                            elevation: 3.3,
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text("Change Password"),
                                    ],
                                ),
                            ),
                        ),
                    ),
                ],
            ),
        );
    }
}
