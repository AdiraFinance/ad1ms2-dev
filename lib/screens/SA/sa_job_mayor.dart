import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/branch_marketing_model.dart';
import 'package:ad1ms2_dev/screens/task_list/task_list.dart';
import 'package:ad1ms2_dev/shared/SA_change_notifier/sa_job_mayor_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class SAJobMayor extends StatefulWidget {
  @override
  _SAJobMayorState createState() => _SAJobMayorState();
}

class _SAJobMayorState extends State<SAJobMayor> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "SA For Job Mayor",
          style: TextStyle(
            color: Colors.black
          ),
        ),
        backgroundColor: myPrimaryColor,
        iconTheme: IconThemeData(
          color: Colors.black
        ),
      ),
      body: Consumer<SAJobMayorChangeNotifier>(
        builder: (context, value, child) {
          return SingleChildScrollView(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width / 27,
                vertical: MediaQuery.of(context).size.height / 57),
            child: Column(
              children: [
                TextFormField(
                  autovalidate: value.autoValidate,
                  validator: (e) {
                    if (e.isEmpty) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    WhitelistingTextInputFormatter.digitsOnly,
                  ],
                  controller: value.controllerAppNo,
                  style: new TextStyle(color: Colors.black),
                  decoration: new InputDecoration(
                      labelText: 'No Aplikasi',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8))),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                TextFormField(
                  autovalidate: value.autoValidate,
                  validator: (e) {
                    if (e.isEmpty) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    WhitelistingTextInputFormatter.digitsOnly,
                  ],
                  controller: value.controllerNumberOrder,
                  style: new TextStyle(color: Colors.black),
                  decoration: new InputDecoration(
                      labelText: 'Nomer Order',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8))),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                TextFormField(
                  autovalidate: value.autoValidate,
                  validator: (e) {
                    if (e.isEmpty) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    WhitelistingTextInputFormatter.digitsOnly,
                  ],
                  controller: value.controllerCustomerName,
                  style: new TextStyle(color: Colors.black),
                  decoration: new InputDecoration(
                      labelText: 'Nama Nasabah',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8))),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                TextFormField(
                  autovalidate: value.autoValidate,
                  validator: (e) {
                    if (e.isEmpty) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    WhitelistingTextInputFormatter.digitsOnly,
                  ],
                  controller: value.controllerCellPhoneNumber,
                  style: new TextStyle(color: Colors.black),
                  decoration: new InputDecoration(
                      labelText: 'Nomer Handphone',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8))),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                TextFormField(
                  autovalidate: value.autoValidate,
                  validator: (e) {
                    if (e.isEmpty) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    WhitelistingTextInputFormatter.digitsOnly,
                  ],
                  controller: value.controllerPhoneNumber,
                  style: new TextStyle(color: Colors.black),
                  decoration: new InputDecoration(
                      labelText: 'Nomer Telepon',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8))),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                DropdownButtonFormField<BranchMarketingModel>(
                    autovalidate: value.autoValidate,
                    validator: (e) {
                      if (e == null) {
                        return "Silahkan pilih jenis identitas";
                      } else {
                        return null;
                      }
                    },
                    value: value.branchMarketingSelected,
                    onChanged: (data) {
                      value.branchMarketingSelected = data;
                    },
                    decoration: InputDecoration(
                      labelText: "Jenis Sertifikat",
                      border: OutlineInputBorder(),
                      contentPadding:
                      EdgeInsets.symmetric(horizontal: 10),
                    ),
                    items: value
                        .listBranchMarketing
                        .map((value) {
                      return DropdownMenuItem<BranchMarketingModel>(
                        value: value,
                        child: Text(
                          value.name,
                          overflow: TextOverflow.ellipsis,
                        ),
                      );
                    }).toList()
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                TextFormField(
                  autovalidate: value.autoValidate,
                  validator: (e) {
                    if (e.isEmpty) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  readOnly: true,
                  onTap: (){
                    value.selectAppointmentSurveyDate(context);
                  },
                  controller: value.controllerAppointmentSurveyDate,
                  style: new TextStyle(color: Colors.black),
                  decoration: new InputDecoration(
                      labelText: 'Tanggal Janji Survey',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8))
                  ),
                ),
              ],
            ),
          );
        },
      ),
      bottomNavigationBar: BottomAppBar(
        elevation: 0.0,
        child: Container(
          padding: const EdgeInsets.all(8.0),
          child: RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(8.0)),
              color: myPrimaryColor,
              onPressed: () {
                Provider.of<SAJobMayorChangeNotifier>(context, listen: false).generateAplikasiPayung(context);
                // Navigator.push(context, MaterialPageRoute(builder: (context) => TaskList()));
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("SUBMIT",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          letterSpacing: 1.25))
                ],
              )),
        ),
      ),
    );
  }
}
