import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/search_program_change_notfier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchProgram extends StatefulWidget {
  @override
  _SearchProgramState createState() => _SearchProgramState();
}

class _SearchProgramState extends State<SearchProgram> {

  @override
  void initState() {
    super.initState();
    Provider.of<SearchProgramChangeNotifier>(context,listen: false).getProgram(context);
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<SearchProgramChangeNotifier>(context,listen: false).scaffoldKey,
        appBar: AppBar(
          title: Consumer<SearchProgramChangeNotifier>(
            builder: (context, searchProgramChangeNotifier, _) {
              return TextFormField(
                controller: searchProgramChangeNotifier.controllerSearch,
                style: TextStyle(color: Colors.black),
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (query) {
                  searchProgramChangeNotifier.searchProgram(query);
                },
                onChanged: (e) {
                  searchProgramChangeNotifier.changeAction(e);
                },
                cursorColor: Colors.black,
                decoration: new InputDecoration(
                  hintText: "Cari Program (minimal 3 karakter)",
                  hintStyle: TextStyle(color: Colors.black),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                ),
                autofocus: true,
                textCapitalization: TextCapitalization.characters,
              );
            },
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            Provider.of<SearchProgramChangeNotifier>(context, listen: true)
                    .showClear
                ? IconButton(
                    icon: Icon(Icons.clear),
                    onPressed: () {
                      Provider.of<SearchProgramChangeNotifier>(context,
                          listen: false).clearSearchTemp();
                      Provider.of<SearchProgramChangeNotifier>(context,
                              listen: false)
                          .controllerSearch
                          .clear();
                      Provider.of<SearchProgramChangeNotifier>(context,
                              listen: false)
                          .changeAction(Provider.of<SearchProgramChangeNotifier>(
                                  context,
                                  listen: false)
                              .controllerSearch
                              .text);
                    })
                : SizedBox(
                    width: 0.0,
                    height: 0.0,
                  )
          ],
        ),
        body: Consumer<SearchProgramChangeNotifier>(
          builder: (context, searchProgramChangeNotifier, _) {
            return searchProgramChangeNotifier.loadData
                ?
            Center(child: CircularProgressIndicator())
                :
            searchProgramChangeNotifier.listProgramTemp.isNotEmpty
                ?
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount: searchProgramChangeNotifier.listProgramTemp.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(context,
                        searchProgramChangeNotifier.listProgramTemp[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchProgramChangeNotifier.listProgramTemp[index].kode} - "
                              "${searchProgramChangeNotifier.listProgramTemp[index].deskripsi} ",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            )
                :
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount: searchProgramChangeNotifier.listProgramModel.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(context,
                        searchProgramChangeNotifier.listProgramModel[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchProgramChangeNotifier.listProgramModel[index].kode} - "
                          "${searchProgramChangeNotifier.listProgramModel[index].deskripsi} ",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          },
        ),
      ),
    );
  }
}
