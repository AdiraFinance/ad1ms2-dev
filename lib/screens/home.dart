import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/task_list_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/home/home_change_notifier.dart';
import 'package:ad1ms2_dev/shared/responsive_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  final BuildContext context;
  const HomePage({this.context});
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Screen _size;
  // DbHelper _dbHelper = DbHelper();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
   _setDataProfile();
  }

  _setDataProfile() async{
    // print("dijalankan");
    try{
      if(await Provider.of<HomeChangeNotifier>(context, listen: false).getTaskList() == "isEmpty"){
        _showSnackBar("Task list tidak ditemukan");
      }
    }
    catch(e){
      _showSnackBar(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    _size = Screen(MediaQuery.of(context).size);
    return Theme(
      data: ThemeData(
          // fontFamily: "NunitoSans",
          accentColor: myPrimaryColor
      ),
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: myPrimaryColor,
        body: Stack(
          children: <Widget>[
            Container(
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height / 47
                ),
                child: Column(
                  // crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Center(
                      child: Container(
                        margin: EdgeInsets.only(left: _size.wp(5)),
                        child: Row(
                          children: <Widget>[
                            Image.asset(
                              "img/user.webp",
                              width: _size.wp(15),
                              height: _size.wp(15),
                            ),
                            SizedBox(
                              width: _size.wp(5),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("${Provider.of<HomeChangeNotifier>(context, listen: true).username}"),
                                SizedBox(height: MediaQuery.of(context).size.height / 177),
                                Text("${Provider.of<HomeChangeNotifier>(context, listen: true).fullName}"),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding:  EdgeInsets.symmetric(
                        vertical: MediaQuery.of(context).size.height/37,
                        horizontal: MediaQuery.of(context).size.width/17
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text("${versionApp(1)}")
                        ],
                      ),
                    )
                  ],
                )
            ),
            DraggableScrollableSheet(
              initialChildSize: 0.8,
              maxChildSize: 1,
              minChildSize: 0.8,
              builder: (context, scrollController) {
                return Material(
                  borderRadius: const BorderRadius.only(
                      topLeft: const Radius.circular(25.0),
                      topRight: const Radius.circular(25.0)),
                  child: Consumer<HomeChangeNotifier>(
                      builder: (context, value, child) {
                        return
                          value.loadData
                              ?
                          Center(child: CircularProgressIndicator())
                              :
                          value.taskList.isNotEmpty
                            ?
                        GridView.builder(
                          controller: scrollController,
                          itemCount: value.taskList.length,
                          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            crossAxisSpacing: 6,
                            mainAxisSpacing: 6,
                            childAspectRatio: 0.975
                          ),
                          padding: EdgeInsets.symmetric(
                              vertical: MediaQuery.of(context).size.height / 47,
                              horizontal: MediaQuery.of(context).size.width / 37),
                          itemBuilder: (BuildContext context, int index) {
                            return InkWell(
                              onTap: (){
                                _chooseForm(widget.context,value.taskList[index]);
                                // value.navigateForm(
                                //     value.taskList[index].LAST_KNOWN_STATE,
                                //     context,
                                //     value.taskList[index].ORDER_NO,
                                //     value.taskList[index].ORDER_DATE.toString(),
                                //     value.taskList[index].CUST_NAME,
                                // );
                              },
                              child: Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8.0),
                                ),
                                color: "${value.taskList[index].LAST_KNOWN_STATE}" == "IDE"
                                    ? primaryColorIDE
                                    : "${value.taskList[index].LAST_KNOWN_STATE}" == "SRE"
                                    ? primaryColorRegulerSurvey
                                    : "${value.taskList[index].LAST_KNOWN_STATE}" == "AOS"
                                    ? primaryColorAOS
                                    : "${value.taskList[index].LAST_KNOWN_STATE}" == "CONA"
                                    ? primaryColorCONA
                                    : "${value.taskList[index].LAST_KNOWN_STATE}" == "SA" || "${value.taskList[index].LAST_KNOWN_STATE}" == "IA" || "${value.taskList[index].LAST_KNOWN_STATE}" == "SARS"
                                    ? primaryColorMiniForm
                                    : "${value.taskList[index].LAST_KNOWN_STATE}" == "PAC"
                                    ? primaryColorPAC
                                    : "${value.taskList[index].LAST_KNOWN_STATE}" == "RSVY"
                                    ? primaryColorResurvey
                                    : "${value.taskList[index].LAST_KNOWN_STATE}" == "DKR"
                                    ? primaryColorDataKoreksi
                                    : primaryColorIDE,
                                child: Container(
                                  padding: EdgeInsets.all(0.0),
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(0.0),
                                        child: Align(
                                            alignment: Alignment.topRight,
                                            child: Text(
                                              "${value.taskList[index].LAST_KNOWN_STATE}" != "CONA" ? "${value.taskList[index].LAST_KNOWN_STATE}" : "CONAPP",
                                              style: TextStyle(
                                                  color: Colors.white70,
                                                  letterSpacing: 0.000001,
                                                  fontSize: 36,
                                                  fontWeight: FontWeight.w900,
                                                  fontFamily: "couture-bld"
                                              ),
                                            )
                                        ),
                                      ),
                                      SizedBox(height: MediaQuery.of(context).size.height / 177),
                                      Container(
                                        margin: EdgeInsets.fromLTRB(13, 8, 13, 0),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              value.taskList[index].ORDER_NO,
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                letterSpacing: 0.2,
                                                fontSize: 14,
                                                fontFamily: "NunitoSans",
                                              ),
                                            ),
                                            SizedBox(height: MediaQuery.of(context).size.height / 177),
                                            Text(
                                              "${value.taskList[index].CUST_NAME}",
                                              style: TextStyle(
                                                fontSize: 13,
                                                letterSpacing: 0.2,
                                                color: Colors.white,
                                                fontFamily: "NunitoSans",
                                              ),
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                            SizedBox(height: MediaQuery.of(context).size.height / 177),
                                            Text(
                                              "${dateFormat2.format(value.taskList[index].MS2PROCESS_DATE)}",
                                              style: TextStyle(
                                                color: Colors.white,
                                                letterSpacing: 0.2,
                                                fontSize: 12,
                                              ),
                                            ),
                                            SizedBox(height: MediaQuery.of(context).size.height / 47),
                                            Container(
                                              decoration: BoxDecoration(
                                                color: value.taskList[index].PRIORITY == "3"
                                                    ?
                                                Color(0xff1B5E20)
                                                    :
                                                value.taskList[index].PRIORITY == "2"
                                                    ?
                                                Color(0xffE65100)
                                                // Color(0xff46be8a)
                                                // Color(0xffec7f00)
                                                // Color(0xffdc3030)
                                                    :
                                                Color(0xffB71C1C),
                                                borderRadius: new BorderRadius.circular(25),
                                                // border: Border.all(color: Colors.white)
                                              ),
                                              child: Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: MediaQuery.of(context).size.width / 37,
                                                    vertical: MediaQuery.of(context).size.height / 177
                                                ),
                                                child: Text(value.taskList[index].PRIORITY == "3" ? "Low Priority" : value.taskList[index].PRIORITY == "2" ? "Medium Priority" : "High Priority",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    letterSpacing: 0.2,
                                                    fontFamily: "NunitoSans",
                                                  ),
                                                ),
                                                // child: Text("Low Priority"
                                                //     "${value.taskList[index].PRIORITY}",
                                                //   style: TextStyle(
                                                //       color: Colors.white
                                                //   ),
                                                // ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              ),
                            );
                          },
                        )
                            :
                        Center(child: Text("Tidak Ada Task List", style: TextStyle(color: Colors.grey, fontSize: 16),));
                      }
                  )
                );
              },
            )
          ],
        ),
      ),
    );
  }

  void _showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  void _chooseForm(BuildContext context,TaskListModel model) async{
    try{
      await checkGetCustType(
          model.LAST_KNOWN_STATE,
          context,
          model.ORDER_NO,
          model.ORDER_DATE.toString(),
          model.CUST_NAME,
          model.CUST_TYPE,
          model.IDX,
          model.OID,
          model.STATUS_AORO,
          model.JENIS_PENAWARAN,
          model.NO_REFERENSI,
          model.OPSI_MULTIDISBURSE,
          true
      );
    }
    catch(e){
      _showSnackBar(e.toString());
    }
  }
}

//import 'package:flutter/material.dart';
//
//import 'dedup/dedup_comp.dart';
//import 'dedup/dedup_indi.dart';
//
//class Home extends StatefulWidget {
//  @override
//  _HomeState createState() => _HomeState();
//}
//
//class _HomeState extends State<Home> {
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      drawer: Drawer(
//        child: ListView(
//          padding: EdgeInsets.zero,
//          children: <Widget>[
//            DrawerHeader(
//              child: Text('Drawer Header'),
//              decoration: BoxDecoration(
//                color: Colors.blue,
//              ),
//            ),
//            ListTile(
//              title: Text('Initial Data Entry Personal'),
//              onTap: () {
////                Navigator.push(
////                  context,
////                  MaterialPageRoute(builder: (context) => DedupIndi()),
////                );
//              },
//            ),
//            ListTile(
//              title: Text('Initial Data Entry Company'),
//              onTap: () {
//                Navigator.push(
//                  context,
//                  MaterialPageRoute(builder: (context) => FormDedupComp()),
//                );
//              },
//            ),
//          ],
//        ),
//      ),
//      appBar: AppBar(
//        centerTitle: true,
//        title: Text('Home'),
//        elevation: 0.0,
//        actions: <Widget>[
//          FlatButton.icon(
//            icon: Icon(Icons.person),
//            label: Text('Logout'),
//            onPressed: () async {
////              await _auth.signOut();
//            },
//          )
//        ],
//      ),
//      body: Container(
//        child: Wrap(
//          children: <Widget>[
//            Card(),
//          ],
//        ),
//      ),
//    );
//  }
//}
