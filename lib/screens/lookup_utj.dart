import 'package:ad1ms2_dev/shared/change_notifier_app/information_collatelar_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_lookup_utj_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../main.dart';

class LookUpUTJ extends StatefulWidget {
  @override
  _LookUpUTJState createState() => _LookUpUTJState();
}

class _LookUpUTJState extends State<LookUpUTJ> {
  @override
  Widget build(BuildContext context) {
    return Theme(
        data: ThemeData(
            primaryColor: Colors.black,
            accentColor: myPrimaryColor,
            fontFamily: "NunitoSans",
            primarySwatch: primaryOrange
        ),
        child: Consumer<FormMLookupUTJChangeNotif>(
            builder: (context,_provider,_){
                return Scaffold(
                    key: _provider.scaffoldKey,
                    appBar: AppBar(
                        title: Text("Lookup UTJ", style: TextStyle(color: Colors.black)),
                        centerTitle: true,
                        backgroundColor: myPrimaryColor,
                        iconTheme: IconThemeData(color: Colors.black),
                    ),
                    body: SingleChildScrollView(
                        padding: EdgeInsets.symmetric(
                            horizontal: MediaQuery.of(context).size.width / 27,
                            vertical: MediaQuery.of(context).size.height / 57),
                        child: Form(
                            onWillPop: _provider.onBackPress,
                            key: _provider.keyForm,
                            child: Column(
                                children: [
                                    Visibility(
                                      visible: Provider.of<InformationCollateralChangeNotifier>(context,listen: false).isPoliceNumberVisible(),
                                      child: IgnorePointer(
                                        ignoring: Provider.of<InformationCollateralChangeNotifier>(context,listen: false).disableJenisPenawaran,
                                        child: TextFormField(
                                            autovalidate: _provider.autoValidate,
                                            validator: (e) {
                                                if (e.isEmpty && Provider.of<InformationCollateralChangeNotifier>(context,listen: false).isPoliceNumberMandatory()) {
                                                    return "Tidak boleh kosong";
                                                } else {
                                                    return null;
                                                }
                                            },
                                            keyboardType: TextInputType.text,
                                            textCapitalization: TextCapitalization.characters,
                                            controller: _provider.controllerPoliceNumber,
                                            decoration: InputDecoration(
                                                labelText: 'No Polisi',
                                                labelStyle: TextStyle(color: Colors.black),
                                                border: OutlineInputBorder(
                                                    borderRadius: BorderRadius.circular(8)),
                                                contentPadding: EdgeInsets.symmetric(horizontal: 10)
                                            ),
                                            inputFormatters: [
                                              FilteringTextInputFormatter.allow(RegExp('[a-zA-Z0-9 ]')),
                                            ],
                                        ),
                                      ),
                                    ),
                                    Visibility(
                                        visible: Provider.of<InformationCollateralChangeNotifier>(context,listen: false).isPoliceNumberVisible(),
                                        child: SizedBox(height: MediaQuery.of(context).size.width / 27)
                                    ),
                                    Visibility(
                                      visible: Provider.of<InformationCollateralChangeNotifier>(context,listen: false).isFrameNumberVisible(),
                                      child: IgnorePointer(
                                        ignoring: Provider.of<InformationCollateralChangeNotifier>(context,listen: false).disableJenisPenawaran,
                                        child: TextFormField(
                                            autovalidate: _provider.autoValidate,
                                            validator: (e) {
                                                if (e.isEmpty && Provider.of<InformationCollateralChangeNotifier>(context,listen: false).isFrameNumberMandatory()) {
                                                    return "Tidak boleh kosong";
                                                } else {
                                                    return null;
                                                }
                                            },
                                            keyboardType: TextInputType.text,
                                            textCapitalization: TextCapitalization.characters,
                                            controller: _provider.controllerFrameNumber,
                                            decoration: InputDecoration(
                                                labelText: 'No Rangka',
                                                labelStyle: TextStyle(color: Colors.black),
                                                border: OutlineInputBorder(
                                                    borderRadius: BorderRadius.circular(8)),
                                                contentPadding: EdgeInsets.symmetric(horizontal: 10)
                                            ),
                                            inputFormatters: [
                                              FilteringTextInputFormatter.allow(RegExp('[a-zA-Z0-9-]')),
                                            ],
                                        ),
                                      ),
                                    ),
                                    Visibility(
                                        visible: Provider.of<InformationCollateralChangeNotifier>(context,listen: false).isFrameNumberVisible(),
                                        child: SizedBox(height: MediaQuery.of(context).size.width / 27)
                                    ),
                                    Visibility(
                                      visible: Provider.of<InformationCollateralChangeNotifier>(context,listen: false).isMachineNumberVisible(),
                                      child: IgnorePointer(
                                        ignoring: Provider.of<InformationCollateralChangeNotifier>(context,listen: false).disableJenisPenawaran,
                                        child: TextFormField(
                                            autovalidate: _provider.autoValidate,
                                            validator: (e) {
                                                if (e.isEmpty && Provider.of<InformationCollateralChangeNotifier>(context,listen: false).isMachineNumberMandatory()) {
                                                    return "Tidak boleh kosong";
                                                } else {
                                                    return null;
                                                }
                                            },
                                            keyboardType: TextInputType.text,
                                            textCapitalization: TextCapitalization.characters,
                                            controller: _provider.controllerMachineNumber,
                                            decoration: InputDecoration(
                                                labelText: 'No Mesin',
                                                labelStyle: TextStyle(color: Colors.black),
                                                border: OutlineInputBorder(
                                                    borderRadius: BorderRadius.circular(8)),
                                                contentPadding: EdgeInsets.symmetric(horizontal: 10)
                                            ),
                                            inputFormatters: [
                                              FilteringTextInputFormatter.allow(RegExp('[a-zA-Z0-9-]')),
                                            ],
                                        ),
                                      ),
                                    ),
                                    Visibility(
                                        visible: Provider.of<InformationCollateralChangeNotifier>(context,listen: false).isMachineNumberVisible(),
                                        child: SizedBox(height: MediaQuery.of(context).size.width / 27)
                                    ),
                                    Visibility(
                                      visible: Provider.of<InformationCollateralChangeNotifier>(context,listen: false).isBPKPNumberVisible(),
                                      child: IgnorePointer(
                                        ignoring: Provider.of<InformationCollateralChangeNotifier>(context,listen: false).disableJenisPenawaran,
                                        child: TextFormField(
                                            autovalidate: _provider.autoValidate,
                                            validator: (e) {
                                                if (e.isEmpty && Provider.of<InformationCollateralChangeNotifier>(context,listen: false).isBPKPNumberMandatory()) {
                                                    return "Tidak boleh kosong";
                                                } else {
                                                    return null;
                                                }
                                            },
                                            keyboardType: TextInputType.text,
                                            textCapitalization: TextCapitalization.characters,
                                            controller: _provider.controllerBPKPNumber,
                                            decoration: InputDecoration(
                                                labelText: 'No BPKB',
                                                labelStyle: TextStyle(color: Colors.black),
                                                border: OutlineInputBorder(
                                                    borderRadius: BorderRadius.circular(8)),
                                                contentPadding: EdgeInsets.symmetric(horizontal: 10)
                                            ),
                                            inputFormatters: [
                                              FilteringTextInputFormatter.allow(RegExp('[a-zA-Z0-9 ]')),
                                            ],
                                        ),
                                      ),
                                    ),
                                ],
                            )
                        ),
                    ),
                    bottomNavigationBar: BottomAppBar(
                        elevation: 0.0,
                        child: Padding(
                            padding: EdgeInsets.all(8.0),
                            child:  Provider.of<FormMLookupUTJChangeNotif>(context, listen: false).loadData
                                ?
                            Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                    CircularProgressIndicator(),
                                ],
                            )
                                :
                            RaisedButton(
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
                                color: myPrimaryColor,
                                onPressed: () {
                                    _provider.check(context);
                                },
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                        Text("CHECK",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500,
                                                letterSpacing: 1.25))
                                    ],
                                )
                            )
                        ),
                    ),
                );
            }
        )
    );
  }
}
