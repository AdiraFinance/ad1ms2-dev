import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/additional_insurance_model.dart';
import 'package:ad1ms2_dev/models/insurance_type_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/form_m_add_additional_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/decimal_text_input_formatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';


class AddAdditionalInsurance extends StatefulWidget {
    final int flag;
    final AdditionalInsuranceModel formMAdditionalInsurance;
    final int index;

    const AddAdditionalInsurance({Key key, this.flag, this.formMAdditionalInsurance, this.index}) : super(key: key);
    @override
    _AddAdditionalInsuranceState createState() => _AddAdditionalInsuranceState();
}

class _AddAdditionalInsuranceState extends State<AddAdditionalInsurance> {
    Future<void> _loadData;
    @override
    void initState() {
        super.initState();
        // if (widget.flag != 0) {
        //     _loadData =
        //         Provider.of<FormMAddAdditionalInsuranceChangeNotifier>(context, listen: false)
        //             .setValueForEditAdditionalInsurance(widget.formMAdditionalInsurance);
        // }
        _loadData = Provider.of<FormMAddAdditionalInsuranceChangeNotifier>(context, listen: false).setPreference(context,widget.flag, widget.formMAdditionalInsurance, widget.index);
        // Provider.of<FormMAddAdditionalInsuranceChangeNotifier>(context,listen: false).getDataFromDashboard(context);
        // Provider.of<FormMAddAdditionalInsuranceChangeNotifier>(context, listen: false).getListInsuranceType(context, widget.flag, widget.formMAdditionalInsurance, widget.index);
    }
    @override
    Widget build(BuildContext context) {
        return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                primaryColor: Colors.black,
                accentColor: myPrimaryColor,
            ),
            child: Consumer<FormMAddAdditionalInsuranceChangeNotifier>(
                builder: (context, formMAddAdditionalInsuranceChangeNotifier, _) {
                    return Scaffold(
                        key: formMAddAdditionalInsuranceChangeNotifier.scaffoldKey,
                        appBar: AppBar(
                            backgroundColor: myPrimaryColor,
                            title: Text(
                                widget.flag == 0 ? "Tambah Asuransi Tambahan" : formMAddAdditionalInsuranceChangeNotifier.isDisablePACIAAOSCONA ? "Detail Asuransi Tambahan" : "Edit Asuransi Tambahan",
                                style: TextStyle(color: Colors.black)),
                            centerTitle: true,
                            iconTheme: IconThemeData(
                                color: Colors.black,
                            ),
                        ),
                        body: FutureBuilder(
                            future: _loadData,
                            builder: (context, snapshot) {
                                if (snapshot.connectionState == ConnectionState.waiting) {
                                    return Center(
                                        child: CircularProgressIndicator(),
                                    );
                                }
                                return Consumer<FormMAddAdditionalInsuranceChangeNotifier>(
                                    builder:
                                        (consumerContext, formMaddAdditionalInsuranceChangeNotifier, _) {
                                        return Form(
                                            onWillPop: _onWillPop,
                                            key: formMaddAdditionalInsuranceChangeNotifier.key,
                                            child: SingleChildScrollView(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: MediaQuery.of(context).size.width / 27,
                                                    vertical: MediaQuery.of(context).size.height / 57),
                                                child: Column(
                                                    children: [
                                                        Visibility(
                                                            visible: formMaddAdditionalInsuranceChangeNotifier.isInsuranceTypeSelectedVisible(),
                                                            child: IgnorePointer(
                                                              ignoring: formMAddAdditionalInsuranceChangeNotifier.isDisablePACIAAOSCONA || formMaddAdditionalInsuranceChangeNotifier.disableJenisPenawaran ? true : false,
                                                              child: DropdownButtonFormField<InsuranceTypeModel>(
                                                                  autovalidate: formMaddAdditionalInsuranceChangeNotifier.autoValidate,
                                                                  validator: (e) {
                                                                      if (e == null && formMaddAdditionalInsuranceChangeNotifier.isInsuranceTypeSelectedMandatory()) {
                                                                          return "Tidak boleh kosong";
                                                                      } else {
                                                                          return null;
                                                                      }
                                                                  },
                                                                  value: formMaddAdditionalInsuranceChangeNotifier.insuranceTypeSelected,
                                                                  onChanged: (value) {
                                                                      formMaddAdditionalInsuranceChangeNotifier.insuranceTypeSelected = value;
                                                                  },
                                                                  decoration: InputDecoration(
                                                                      labelText: "Tipe Asuransi",
                                                                      border: OutlineInputBorder(),
                                                                      filled: formMAddAdditionalInsuranceChangeNotifier.isDisablePACIAAOSCONA || formMaddAdditionalInsuranceChangeNotifier.disableJenisPenawaran ? true : false,
                                                                      fillColor: Colors.black12,
                                                                      enabledBorder: OutlineInputBorder(
                                                                          borderSide: BorderSide(color: formMaddAdditionalInsuranceChangeNotifier.insuranceTypeDakor ? Colors.purple : Colors.grey)),
                                                                      disabledBorder: OutlineInputBorder(
                                                                          borderSide: BorderSide(color: formMaddAdditionalInsuranceChangeNotifier.insuranceTypeDakor ? Colors.purple : Colors.grey)),
                                                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                                  ),
                                                                  items: formMaddAdditionalInsuranceChangeNotifier.listInsurance.map((value) {
                                                                      return DropdownMenuItem<InsuranceTypeModel>(
                                                                          value: value,
                                                                          child: Text(
                                                                              "${value.KODE} - ${value.DESKRIPSI}",
                                                                              overflow: TextOverflow.ellipsis,
                                                                          ),
                                                                      );
                                                                  }).toList(),
                                                              ),
                                                            ),
                                                        ),
                                                        SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                        Visibility(
                                                            visible: formMaddAdditionalInsuranceChangeNotifier.isCompanyVisible(),
                                                            child: IgnorePointer(
                                                                ignoring: formMAddAdditionalInsuranceChangeNotifier.isDisablePACIAAOSCONA || formMaddAdditionalInsuranceChangeNotifier.disableJenisPenawaran ? true : false,
                                                              child: TextFormField(
                                                                  autovalidate: formMaddAdditionalInsuranceChangeNotifier.autoValidate,
                                                                  validator: (e) {
                                                                      if (e.isEmpty && formMaddAdditionalInsuranceChangeNotifier.isCompanyMandatory()) {
                                                                          return "Tidak boleh kosong";
                                                                      } else {
                                                                          return null;
                                                                      }
                                                                  },
                                                                  readOnly: true,
                                                                  onTap:() {
                                                                      formMaddAdditionalInsuranceChangeNotifier.searchCompanyInsurance(context);
                                                                  },
                                                                  controller: formMaddAdditionalInsuranceChangeNotifier.controllerCompany,
                                                                  style: new TextStyle(color: Colors.black),
                                                                  decoration: new InputDecoration(
                                                                      labelText: 'Perusahaan',
                                                                      labelStyle: TextStyle(color: Colors.black),
                                                                      filled: formMAddAdditionalInsuranceChangeNotifier.isDisablePACIAAOSCONA || formMaddAdditionalInsuranceChangeNotifier.disableJenisPenawaran ? true : false,
                                                                      fillColor: Colors.black12,
                                                                      border: OutlineInputBorder(
                                                                          borderRadius: BorderRadius.circular(8)),
                                                                      enabledBorder: OutlineInputBorder(
                                                                          borderSide: BorderSide(color: formMaddAdditionalInsuranceChangeNotifier.companyDakor ? Colors.purple : Colors.grey)),
                                                                      disabledBorder: OutlineInputBorder(
                                                                          borderSide: BorderSide(color: formMaddAdditionalInsuranceChangeNotifier.companyDakor ? Colors.purple : Colors.grey)),
                                                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                                  ),
                                                                  keyboardType: TextInputType.text,
                                                                  textCapitalization: TextCapitalization.characters,
                                                              ),
                                                            ),
                                                        ),
                                                        SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                        Visibility(
                                                            visible: formMaddAdditionalInsuranceChangeNotifier.isProductVisible(),
                                                            child: IgnorePointer(
                                                                ignoring: formMAddAdditionalInsuranceChangeNotifier.isDisablePACIAAOSCONA || formMaddAdditionalInsuranceChangeNotifier.disableJenisPenawaran ? true : false,
                                                              child: TextFormField(
                                                                  readOnly: true,
                                                                  onTap:() {
                                                                      formMaddAdditionalInsuranceChangeNotifier.searchProduct(context);
                                                                  },
                                                                  controller: formMaddAdditionalInsuranceChangeNotifier.controllerProduct,
                                                                  style: new TextStyle(color: Colors.black),
                                                                  decoration: new InputDecoration(
                                                                      labelText: 'Produk',
                                                                      labelStyle: TextStyle(color: Colors.black),
                                                                      filled: formMAddAdditionalInsuranceChangeNotifier.isDisablePACIAAOSCONA || formMaddAdditionalInsuranceChangeNotifier.disableJenisPenawaran ? true : false,
                                                                      fillColor: Colors.black12,
                                                                      border: OutlineInputBorder(
                                                                          borderRadius: BorderRadius.circular(8)),
                                                                      enabledBorder: OutlineInputBorder(
                                                                          borderSide: BorderSide(color: formMaddAdditionalInsuranceChangeNotifier.productDakor ? Colors.purple : Colors.grey)),
                                                                      disabledBorder: OutlineInputBorder(
                                                                          borderSide: BorderSide(color: formMaddAdditionalInsuranceChangeNotifier.productDakor ? Colors.purple : Colors.grey)),
                                                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                                  ),
                                                                  keyboardType: TextInputType.text,
                                                                  textCapitalization: TextCapitalization.characters,
                                                                  autovalidate: formMaddAdditionalInsuranceChangeNotifier.autoValidate,
                                                                  validator: (e) {
                                                                      if (e.isEmpty && formMaddAdditionalInsuranceChangeNotifier.isProductMandatory()) {
                                                                          return "Tidak boleh kosong";
                                                                      } else {
                                                                          return null;
                                                                      }
                                                                  },
                                                              ),
                                                            ),
                                                        ),
                                                        SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                        Visibility(
                                                            visible: formMaddAdditionalInsuranceChangeNotifier.isPeriodTypeVisible(),
                                                            child: IgnorePointer(
                                                              ignoring: formMaddAdditionalInsuranceChangeNotifier.disableJenisPenawaran,
                                                              child: TextFormField(
                                                                  enabled: false,
                                                                  controller: formMaddAdditionalInsuranceChangeNotifier.controllerPeriodType,
                                                                  style: new TextStyle(color: Colors.black),
                                                                  decoration: new InputDecoration(
                                                                      filled: true,
                                                                      fillColor: Colors.black12,
                                                                      labelText: 'Periode',
                                                                      labelStyle: TextStyle(color: Colors.black),
                                                                      border: OutlineInputBorder(
                                                                          borderRadius: BorderRadius.circular(8)),
                                                                      enabledBorder: OutlineInputBorder(
                                                                          borderSide: BorderSide(color: formMaddAdditionalInsuranceChangeNotifier.periodDakor ? Colors.purple : Colors.grey)),
                                                                      disabledBorder: OutlineInputBorder(
                                                                          borderSide: BorderSide(color: formMaddAdditionalInsuranceChangeNotifier.periodDakor ? Colors.purple : Colors.grey)),
                                                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                                  ),
                                                                  keyboardType: TextInputType.number,
                                                                  textCapitalization: TextCapitalization.characters,
                                                                  inputFormatters: [
                                                                      WhitelistingTextInputFormatter.digitsOnly
                                                                  ],
                                                                  autovalidate: formMaddAdditionalInsuranceChangeNotifier.autoValidate,
                                                                  validator: (e) {
                                                                      if (e.isEmpty && formMaddAdditionalInsuranceChangeNotifier.isPeriodTypeMandatory()) {
                                                                          return "Tidak boleh kosong";
                                                                      } else {
                                                                          return null;
                                                                      }
                                                                  },
                                                              ),
                                                            ),
                                                        ),
                                                        SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                        Visibility(
                                                            visible: formMaddAdditionalInsuranceChangeNotifier.isCoverageTypeVisible(),
                                                            child: IgnorePointer(
                                                              ignoring: formMaddAdditionalInsuranceChangeNotifier.disableJenisPenawaran,
                                                              child: TextFormField(
                                                                  enabled: false,
                                                                  controller: formMaddAdditionalInsuranceChangeNotifier.controllerCoverageType,
                                                                  style: new TextStyle(color: Colors.black),
                                                                  decoration: new InputDecoration(
                                                                      filled: true,
                                                                      fillColor: Colors.black12,
                                                                      labelText: 'Jenis Pertanggungan',
                                                                      labelStyle: TextStyle(color: Colors.black),
                                                                      border: OutlineInputBorder(
                                                                          borderRadius: BorderRadius.circular(8)),
                                                                      enabledBorder: OutlineInputBorder(
                                                                          borderSide: BorderSide(color: formMaddAdditionalInsuranceChangeNotifier.coverageTypeDakor ? Colors.purple : Colors.grey)),
                                                                      disabledBorder: OutlineInputBorder(
                                                                          borderSide: BorderSide(color: formMaddAdditionalInsuranceChangeNotifier.coverageTypeDakor ? Colors.purple : Colors.grey)),
                                                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                                  ),
                                                                  keyboardType: TextInputType.text,
                                                                  textCapitalization: TextCapitalization.characters,
                                                                  autovalidate: formMaddAdditionalInsuranceChangeNotifier.autoValidate,
                                                                  validator: (e) {
                                                                      if (e.isEmpty && formMaddAdditionalInsuranceChangeNotifier.isCoverageTypeMandatory()) {
                                                                          return "Tidak boleh kosong";
                                                                      } else {
                                                                          return null;
                                                                      }
                                                                  },
                                                                  inputFormatters: [
                                                                      FilteringTextInputFormatter.allow(RegExp('[a-zA-Z 0-9]')),
                                                                  ],
                                                              ),
                                                            ),
                                                        ),
                                                        SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                        Visibility(
                                                            visible: formMaddAdditionalInsuranceChangeNotifier.isCoverageValueVisible(),
                                                            child: IgnorePointer(
                                                              ignoring: formMaddAdditionalInsuranceChangeNotifier.disableJenisPenawaran,
                                                              child: TextFormField(
                                                                  enabled: formMaddAdditionalInsuranceChangeNotifier.flagDisableNilaiPertanggungan ? false : true,
                                                                  controller: formMaddAdditionalInsuranceChangeNotifier.controllerCoverageValue,
                                                                  style: new TextStyle(color: Colors.black),
                                                                  decoration: new InputDecoration(
                                                                      labelText: 'Nilai Pertanggungan',
                                                                      labelStyle: TextStyle(color: Colors.black),
                                                                      border: OutlineInputBorder(
                                                                          borderRadius: BorderRadius.circular(8)),
                                                                      filled: formMaddAdditionalInsuranceChangeNotifier.flagDisableNilaiPertanggungan ? true : false,
                                                                      fillColor: formMaddAdditionalInsuranceChangeNotifier.flagDisableNilaiPertanggungan ? Colors.black12 : null,
                                                                      enabledBorder: OutlineInputBorder(
                                                                          borderSide: BorderSide(color: formMaddAdditionalInsuranceChangeNotifier.coverageValueDakor ? Colors.purple : Colors.grey)),
                                                                      disabledBorder: OutlineInputBorder(
                                                                          borderSide: BorderSide(color: formMaddAdditionalInsuranceChangeNotifier.coverageValueDakor ? Colors.purple : Colors.grey)),
                                                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                                  ),
                                                                  keyboardType: TextInputType.number,
                                                                  textCapitalization: TextCapitalization.characters,
                                                                  inputFormatters: [
                                                                      WhitelistingTextInputFormatter.digitsOnly
                                                                  ],
                                                                  textAlign: TextAlign.end,
                                                                  autovalidate: formMaddAdditionalInsuranceChangeNotifier.autoValidate,
                                                                  validator: (e) {
                                                                      if (e.isEmpty && formMaddAdditionalInsuranceChangeNotifier.isCoverageValueMandatory()) {
                                                                          return "Tidak boleh kosong";
                                                                      } else {
                                                                          return null;
                                                                      }
                                                                  },
                                                              ),
                                                            ),
                                                        ),
                                                        SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                        Row(
                                                            children: [
                                                                Expanded(
                                                                    flex: 2,
                                                                    child: Visibility(
                                                                        visible: formMaddAdditionalInsuranceChangeNotifier.isUpperLimitRateVisible(),
                                                                        child: IgnorePointer(
                                                                          ignoring: formMaddAdditionalInsuranceChangeNotifier.disableJenisPenawaran,
                                                                          child: TextFormField(
                                                                              enabled: false,
                                                                              controller: formMaddAdditionalInsuranceChangeNotifier.controllerUpperLimitRate,
                                                                              style: new TextStyle(color: Colors.black),
                                                                              decoration: new InputDecoration(
                                                                                  labelText: 'Batas Atas %',
                                                                                  labelStyle: TextStyle(color: Colors.black),
                                                                                  border: OutlineInputBorder(
                                                                                      borderRadius: BorderRadius.circular(8)),
                                                                                  filled: true,
                                                                                  fillColor: Colors.black12,
                                                                                  enabledBorder: OutlineInputBorder(
                                                                                      borderSide: BorderSide(color: formMaddAdditionalInsuranceChangeNotifier.upperLimitRateDakor ? Colors.purple : Colors.grey)),
                                                                                  disabledBorder: OutlineInputBorder(
                                                                                      borderSide: BorderSide(color: formMaddAdditionalInsuranceChangeNotifier.upperLimitRateDakor ? Colors.purple : Colors.grey)),
                                                                                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                                              ),
                                                                              keyboardType: TextInputType.number,
                                                                              textCapitalization: TextCapitalization.characters,
                                                                              inputFormatters: [
                                                                                  WhitelistingTextInputFormatter.digitsOnly
                                                                              ],
                                                                              autovalidate: formMaddAdditionalInsuranceChangeNotifier.autoValidate,
                                                                              validator: (e) {
                                                                                  if (e.isEmpty && formMaddAdditionalInsuranceChangeNotifier.isUpperLimitRateMandatory()) {
                                                                                      return "Tidak boleh kosong";
                                                                                  } else {
                                                                                      return null;
                                                                                  }
                                                                              },
                                                                          ),
                                                                        ),
                                                                    ),
                                                                ),
                                                                SizedBox(width: MediaQuery.of(context).size.width / 47),
                                                                Expanded(
                                                                    flex: 3,
                                                                    child: Visibility(
                                                                        visible: formMaddAdditionalInsuranceChangeNotifier.isUpperLimitVisible(),
                                                                        child: IgnorePointer(
                                                                          ignoring: formMaddAdditionalInsuranceChangeNotifier.disableJenisPenawaran,
                                                                          child: TextFormField(
                                                                              enabled: false,
                                                                              controller: formMaddAdditionalInsuranceChangeNotifier.controllerUpperLimit,
                                                                              style: new TextStyle(color: Colors.black),
                                                                              decoration: new InputDecoration(
                                                                                  labelText: 'Batas Atas',
                                                                                  labelStyle: TextStyle(color: Colors.black),
                                                                                  border: OutlineInputBorder(
                                                                                      borderRadius: BorderRadius.circular(8)),
                                                                                  filled: true,
                                                                                  fillColor: Colors.black12,
                                                                                  enabledBorder: OutlineInputBorder(
                                                                                      borderSide: BorderSide(color: formMaddAdditionalInsuranceChangeNotifier.upperLimitValueDakor ? Colors.purple : Colors.grey)),
                                                                                  disabledBorder: OutlineInputBorder(
                                                                                      borderSide: BorderSide(color: formMaddAdditionalInsuranceChangeNotifier.upperLimitValueDakor ? Colors.purple : Colors.grey)),
                                                                                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                                              ),
                                                                              textAlign: TextAlign.end,
                                                                              keyboardType: TextInputType.number,
                                                                              textCapitalization: TextCapitalization.characters,
                                                                              inputFormatters: [
                                                                                  WhitelistingTextInputFormatter.digitsOnly
                                                                              ],
                                                                              autovalidate: formMaddAdditionalInsuranceChangeNotifier.autoValidate,
                                                                              validator: (e) {
                                                                                  if (e.isEmpty && formMaddAdditionalInsuranceChangeNotifier.isUpperLimitMandatory()) {
                                                                                      return "Tidak boleh kosong";
                                                                                  } else {
                                                                                      return null;
                                                                                  }
                                                                              },
                                                                          ),
                                                                        ),
                                                                    ),
                                                                ),
                                                            ],
                                                        ),
                                                        SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                        Row(
                                                            children: [
                                                                Expanded(
                                                                    flex: 2,
                                                                    child: Visibility(
                                                                        visible: formMaddAdditionalInsuranceChangeNotifier.isLowerLimitRateVisible(),
                                                                        child: IgnorePointer(
                                                                          ignoring: formMaddAdditionalInsuranceChangeNotifier.disableJenisPenawaran,
                                                                          child: TextFormField(
                                                                              enabled: false,
                                                                              controller: formMaddAdditionalInsuranceChangeNotifier.controllerLowerLimitRate,
                                                                              style: new TextStyle(color: Colors.black),
                                                                              decoration: new InputDecoration(
                                                                                  labelText: 'Batas Bawah %',
                                                                                  labelStyle: TextStyle(color: Colors.black),
                                                                                  border: OutlineInputBorder(
                                                                                      borderRadius: BorderRadius.circular(8)),
                                                                                  filled: true,
                                                                                  fillColor: Colors.black12,
                                                                                  enabledBorder: OutlineInputBorder(
                                                                                      borderSide: BorderSide(color: formMaddAdditionalInsuranceChangeNotifier.lowerLimitRateDakor ? Colors.purple : Colors.grey)),
                                                                                  disabledBorder: OutlineInputBorder(
                                                                                      borderSide: BorderSide(color: formMaddAdditionalInsuranceChangeNotifier.lowerLimitRateDakor ? Colors.purple : Colors.grey)),
                                                                                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                                              ),
                                                                              keyboardType: TextInputType.number,
                                                                              textCapitalization: TextCapitalization.characters,
                                                                              inputFormatters: [
                                                                                  WhitelistingTextInputFormatter.digitsOnly
                                                                              ],
                                                                              autovalidate: formMaddAdditionalInsuranceChangeNotifier.autoValidate,
                                                                              validator: (e) {
                                                                                  if (e.isEmpty && formMaddAdditionalInsuranceChangeNotifier.isLowerLimitRateMandatory()) {
                                                                                      return "Tidak boleh kosong";
                                                                                  } else {
                                                                                      return null;
                                                                                  }
                                                                              },
                                                                          ),
                                                                        ),
                                                                    ),
                                                                ),
                                                                SizedBox(width: MediaQuery.of(context).size.width / 47),
                                                                Expanded(
                                                                    flex: 3,
                                                                    child: Visibility(
                                                                        visible: formMaddAdditionalInsuranceChangeNotifier.isLowerLimitVisible(),
                                                                        child: IgnorePointer(
                                                                          ignoring: formMaddAdditionalInsuranceChangeNotifier.disableJenisPenawaran,
                                                                          child: TextFormField(
                                                                              enabled: false,
                                                                              controller: formMaddAdditionalInsuranceChangeNotifier.controllerLowerLimit,
                                                                              style: new TextStyle(color: Colors.black),
                                                                              decoration: new InputDecoration(
                                                                                  labelText: 'Batas Bawah',
                                                                                  labelStyle: TextStyle(color: Colors.black),
                                                                                  border: OutlineInputBorder(
                                                                                      borderRadius: BorderRadius.circular(8)),
                                                                                  filled: true,
                                                                                  fillColor: Colors.black12,
                                                                                  enabledBorder: OutlineInputBorder(
                                                                                      borderSide: BorderSide(color: formMaddAdditionalInsuranceChangeNotifier.lowerLimitValueDakor ? Colors.purple : Colors.grey)),
                                                                                  disabledBorder: OutlineInputBorder(
                                                                                      borderSide: BorderSide(color: formMaddAdditionalInsuranceChangeNotifier.lowerLimitValueDakor ? Colors.purple : Colors.grey)),
                                                                                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                                              ),
                                                                              textAlign: TextAlign.end,
                                                                              keyboardType: TextInputType.number,
                                                                              textCapitalization: TextCapitalization.characters,
                                                                              inputFormatters: [
                                                                                  WhitelistingTextInputFormatter.digitsOnly
                                                                              ],
                                                                              autovalidate: formMaddAdditionalInsuranceChangeNotifier.autoValidate,
                                                                              validator: (e) {
                                                                                  if (e.isEmpty && formMaddAdditionalInsuranceChangeNotifier.isLowerLimitMandatory()) {
                                                                                      return "Tidak boleh kosong";
                                                                                  } else {
                                                                                      return null;
                                                                                  }
                                                                              },
                                                                          ),
                                                                        ),
                                                                    ),
                                                                ),
                                                            ],
                                                        ),
                                                        SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                        Visibility(
                                                            visible: formMaddAdditionalInsuranceChangeNotifier.isPriceCashVisible(),
                                                            child: IgnorePointer(
                                                                ignoring: formMAddAdditionalInsuranceChangeNotifier.isDisablePACIAAOSCONA || formMaddAdditionalInsuranceChangeNotifier.disableJenisPenawaran ? true : false,
                                                              child: TextFormField(
                                                                  onFieldSubmitted: (value){
                                                                      formMaddAdditionalInsuranceChangeNotifier.formatting();
                                                                      formMaddAdditionalInsuranceChangeNotifier.controllerPriceCash.text = formMaddAdditionalInsuranceChangeNotifier.formatCurrency.formatCurrency(value);
                                                                      formMaddAdditionalInsuranceChangeNotifier.calculateTotalCost();
                                                                  },
                                                                  onTap: (){
                                                                      formMaddAdditionalInsuranceChangeNotifier.calculateTotalCost();
                                                                  },
                                                                  textAlign: TextAlign.end,
                                                                  controller: formMaddAdditionalInsuranceChangeNotifier.controllerPriceCash,
                                                                  style: new TextStyle(color: Colors.black),
                                                                  decoration: new InputDecoration(
                                                                      labelText: 'Biaya Tunai',
                                                                      labelStyle: TextStyle(color: Colors.black),
                                                                      filled: formMAddAdditionalInsuranceChangeNotifier.isDisablePACIAAOSCONA || formMaddAdditionalInsuranceChangeNotifier.disableJenisPenawaran ? true : false,
                                                                      fillColor: Colors.black12,
                                                                      border: OutlineInputBorder(
                                                                          borderRadius: BorderRadius.circular(8)),
                                                                      enabledBorder: OutlineInputBorder(
                                                                          borderSide: BorderSide(color: formMaddAdditionalInsuranceChangeNotifier.cashDakor ? Colors.purple : Colors.grey)),
                                                                      disabledBorder: OutlineInputBorder(
                                                                          borderSide: BorderSide(color: formMaddAdditionalInsuranceChangeNotifier.cashDakor ? Colors.purple : Colors.grey)),
                                                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                                  ),
                                                                  keyboardType: TextInputType.number,
                                                                  textCapitalization: TextCapitalization.characters,
                                                                  inputFormatters: [
                                                                      DecimalTextInputFormatter(decimalRange: 2),
                                                                      formMaddAdditionalInsuranceChangeNotifier.amountValidator
                                                                  ],
                                                                  autovalidate: formMAddAdditionalInsuranceChangeNotifier.autoValidate,
                                                                  validator: (e) {
                                                                      if (e.isEmpty && formMaddAdditionalInsuranceChangeNotifier.isPriceCashMandatory()) {
                                                                          return "Tidak boleh kosong";
                                                                      } else {
                                                                          return null;
                                                                      }
                                                                  },
                                                              ),
                                                            ),
                                                        ),
                                                        SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                        Visibility(
                                                            visible: formMaddAdditionalInsuranceChangeNotifier.isPriceCreditVisible(),
                                                            child: IgnorePointer(
                                                                ignoring: formMAddAdditionalInsuranceChangeNotifier.isDisablePACIAAOSCONA || formMaddAdditionalInsuranceChangeNotifier.disableJenisPenawaran ? true : false,
                                                              child: TextFormField(
                                                                  onFieldSubmitted: (value){
                                                                      formMaddAdditionalInsuranceChangeNotifier.formatting();
                                                                      formMaddAdditionalInsuranceChangeNotifier.controllerPriceCredit.text = formMaddAdditionalInsuranceChangeNotifier.formatCurrency.formatCurrency(value);
                                                                      formMaddAdditionalInsuranceChangeNotifier.calculateTotalCost();
                                                                  },
                                                                  onTap: (){
                                                                      formMaddAdditionalInsuranceChangeNotifier.formatting();
                                                                      formMaddAdditionalInsuranceChangeNotifier.calculateTotalCost();
                                                                  },
                                                                  textAlign: TextAlign.end,
                                                                  controller: formMaddAdditionalInsuranceChangeNotifier.controllerPriceCredit,
                                                                  style: new TextStyle(color: Colors.black),
                                                                  decoration: new InputDecoration(
                                                                      labelText: 'Biaya Kredit',
                                                                      labelStyle: TextStyle(color: Colors.black),
                                                                      filled: formMAddAdditionalInsuranceChangeNotifier.isDisablePACIAAOSCONA || formMaddAdditionalInsuranceChangeNotifier.disableJenisPenawaran ? true : false,
                                                                      fillColor: Colors.black12,
                                                                      border: OutlineInputBorder(
                                                                          borderRadius: BorderRadius.circular(8)),
                                                                      enabledBorder: OutlineInputBorder(
                                                                          borderSide: BorderSide(color: formMaddAdditionalInsuranceChangeNotifier.creditDakor ? Colors.purple : Colors.grey)),
                                                                      disabledBorder: OutlineInputBorder(
                                                                          borderSide: BorderSide(color: formMaddAdditionalInsuranceChangeNotifier.creditDakor ? Colors.purple : Colors.grey)),
                                                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                                  ),
                                                                  keyboardType: TextInputType.number,
                                                                  textCapitalization: TextCapitalization.characters,
                                                                  inputFormatters: [
                                                                      DecimalTextInputFormatter(decimalRange: 2),
                                                                      formMaddAdditionalInsuranceChangeNotifier.amountValidator
                                                                  ],
                                                                  autovalidate: formMAddAdditionalInsuranceChangeNotifier.autoValidate,
                                                                  validator: (e) {
                                                                      if (e.isEmpty && formMaddAdditionalInsuranceChangeNotifier.isPriceCreditMandatory()) {
                                                                          return "Tidak boleh kosong";
                                                                      } else {
                                                                          return null;
                                                                      }
                                                                  },
                                                              ),
                                                            ),
                                                        ),
                                                        SizedBox(height: MediaQuery.of(context).size.height / 47),
                                                        Row(
                                                            children: [
                                                                Expanded(
                                                                    flex: 2,
                                                                    child: Visibility(
                                                                        visible: formMaddAdditionalInsuranceChangeNotifier.isTotalPriceRateVisible(),
                                                                        child: IgnorePointer(
                                                                          ignoring: formMaddAdditionalInsuranceChangeNotifier.disableJenisPenawaran,
                                                                          child: TextFormField(
                                                                              enabled: false,
                                                                              controller: formMaddAdditionalInsuranceChangeNotifier.controllerTotalPriceRate,
                                                                              style: new TextStyle(color: Colors.black),
                                                                              decoration: new InputDecoration(
                                                                                  errorStyle: TextStyle(
                                                                                      color: Theme.of(context).errorColor,
                                                                                  ),
                                                                                  labelText: 'Total Biaya %',
                                                                                  labelStyle: TextStyle(color: Colors.black),
                                                                                  border: OutlineInputBorder(
                                                                                      borderRadius: BorderRadius.circular(8)),
                                                                                  filled: true,
                                                                                  fillColor: Colors.black12,
                                                                                  enabledBorder: OutlineInputBorder(
                                                                                      borderSide: BorderSide(color: formMaddAdditionalInsuranceChangeNotifier.totalPriceRateDakor ? Colors.purple : Colors.grey)),
                                                                                  disabledBorder: OutlineInputBorder(
                                                                                      borderSide: BorderSide(color: formMaddAdditionalInsuranceChangeNotifier.totalPriceRateDakor ? Colors.purple : Colors.grey)),
                                                                                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                                              ),
                                                                              keyboardType: TextInputType.number,
                                                                              textCapitalization: TextCapitalization.characters,
                                                                              inputFormatters: [
                                                                                  WhitelistingTextInputFormatter.digitsOnly
                                                                              ],
                                                                              autovalidate: formMaddAdditionalInsuranceChangeNotifier.autoValidate,
                                                                              validator: (e) {
                                                                                  if (e.isEmpty && formMaddAdditionalInsuranceChangeNotifier.isTotalPriceRateMandatory()) {
                                                                                      return "Tidak boleh kosong";
                                                                                  } else {
                                                                                      return null;
                                                                                  }
                                                                              },
                                                                          ),
                                                                        ),
                                                                    ),
                                                                ),
                                                                SizedBox(width: MediaQuery.of(context).size.width / 47),
                                                                Expanded(
                                                                    flex: 3,
                                                                    child: Visibility(
                                                                        visible: formMaddAdditionalInsuranceChangeNotifier.isTotalPriceVisible(),
                                                                        child: IgnorePointer(
                                                                          ignoring: formMaddAdditionalInsuranceChangeNotifier.disableJenisPenawaran,
                                                                          child: TextFormField(
                                                                              enabled: false,
                                                                              controller: formMaddAdditionalInsuranceChangeNotifier.controllerTotalPrice,
                                                                              style: new TextStyle(color: Colors.black),
                                                                              decoration: new InputDecoration(
                                                                                  errorStyle: TextStyle(
                                                                                      color: Theme.of(context).errorColor,
                                                                                  ),
                                                                                  labelText: 'Total Biaya',
                                                                                  labelStyle: TextStyle(color: Colors.black),
                                                                                  border: OutlineInputBorder(
                                                                                      borderRadius: BorderRadius.circular(8)),
                                                                                  filled: true,
                                                                                  fillColor: Colors.black12,
                                                                                  enabledBorder: OutlineInputBorder(
                                                                                      borderSide: BorderSide(color: formMaddAdditionalInsuranceChangeNotifier.totalPriceDakor ? Colors.purple : Colors.grey)),
                                                                                  disabledBorder: OutlineInputBorder(
                                                                                      borderSide: BorderSide(color: formMaddAdditionalInsuranceChangeNotifier.totalPriceDakor ? Colors.purple : Colors.grey)),
                                                                                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                                              ),
                                                                              textAlign: TextAlign.end,
                                                                              keyboardType: TextInputType.number,
                                                                              textCapitalization: TextCapitalization.characters,
                                                                              inputFormatters: [
                                                                                  WhitelistingTextInputFormatter.digitsOnly
                                                                              ],
                                                                              autovalidate: formMaddAdditionalInsuranceChangeNotifier.autoValidate,
                                                                              validator: (e) {
                                                                                  if (e.isEmpty && formMaddAdditionalInsuranceChangeNotifier.isTotalPriceMandatory()) {
                                                                                      return "Tidak boleh kosong";
                                                                                  } else {
                                                                                      return null;
                                                                                  }
                                                                              },
                                                                          ),
                                                                        ),
                                                                    ),
                                                                ),
                                                            ],
                                                        ),
                                                    ],
                                                )
                                            ),
                                        );
                                    },
                                );
                            }),
                        bottomNavigationBar: BottomAppBar(
                            elevation: 0.0,
                            child: formMAddAdditionalInsuranceChangeNotifier.isDisablePACIAAOSCONA
                            ? SizedBox()
                            : Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Consumer<FormMAddAdditionalInsuranceChangeNotifier>(
                                    builder: (context, formMAddAdditionalInsuranceChangeNotifier, _) {
                                        return RaisedButton(
                                            shape: RoundedRectangleBorder(
                                                borderRadius: new BorderRadius.circular(8.0)),
                                            color: myPrimaryColor,
                                            onPressed: () {
                                                if (widget.flag == 0) {
                                                    formMAddAdditionalInsuranceChangeNotifier.check(context, widget.flag, null);
                                                } else {
                                                    formMAddAdditionalInsuranceChangeNotifier.check(context, widget.flag, widget.index);
                                                }
                                            },
                                            child: Row(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: <Widget>[
                                                    Text(widget.flag == 0 ? "SAVE" : "UPDATE",
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontSize: 14,
                                                            fontWeight: FontWeight.w500,
                                                            letterSpacing: 1.25))
                                                ],
                                            ));
                                    },
                                )),
                        ),
                    );
                },
            )
        );

    }

    Future<bool> _onWillPop() async {
      var _provider = Provider.of<FormMAddAdditionalInsuranceChangeNotifier>(context, listen: false);
      if (widget.flag == 0) {
        if (_provider.insuranceTypeSelected != null ||
            _provider.controllerCompany.text != "" ||
            _provider.controllerProduct.text != "" ||
            _provider.controllerPriceCredit.text != "" ||
            _provider.controllerPriceCash.text != "") {
            return (await showDialog(
                context: context,
                builder: (myContext) => AlertDialog(
                    title: new Text('Warning'),
                    content: new Text('Keluar dengan simpan perubahan?'),
                    actions: <Widget>[
                        new FlatButton(
                            onPressed: () {
                                widget.flag == 0
                                    ?
                                _provider.check(context, widget.flag, null)
                                    :
                                _provider.check(context, widget.flag, widget.index);
                                Navigator.pop(context);
                            },
                            child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                        ),
                        new FlatButton(
                            onPressed: () => Navigator.of(context).pop(true),
                            child: new Text('Tidak'),
                        ),
                    ],
                ),
            )) ??
                false;
        } else {
            return true;
        }
      } else {
          if (widget.formMAdditionalInsurance.insuranceType.KODE != _provider.insuranceTypeSelected.KODE ||
              widget.formMAdditionalInsurance.company.KODE != _provider.companySelected.KODE ||
              widget.formMAdditionalInsurance.product.KODE != _provider.productSelected.DESKRIPSI ||
              widget.formMAdditionalInsurance.priceCredit != _provider.controllerPriceCredit.text ||
              widget.formMAdditionalInsurance.priceCash != _provider.controllerPriceCash.text) {
              return (await showDialog(
                  context: context,
                  builder: (myContext) => AlertDialog(
                      title: new Text('Warning'),
                      content: new Text('Keluar dengan simpan perubahan?'),
                      actions: <Widget>[
                          Provider.of<FormMAddAdditionalInsuranceChangeNotifier>(context, listen: false).lastKnownState == "PAC" || Provider.of<FormMAddAdditionalInsuranceChangeNotifier>(context, listen: false).lastKnownState == "IA"
                          ? null
                          : new FlatButton(
                              onPressed: () {
                                  _provider.check(context, widget.flag, widget.index);
                                  Navigator.pop(context);
                              },
                              child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                          ),
                          new FlatButton(
                              onPressed: () => Navigator.of(context).pop(true),
                              child: new Text('Tidak'),
                          ),
                      ],
                  ),
              )) ??
                  false;
          } else {
              return true;
          }
      }
    }
}
