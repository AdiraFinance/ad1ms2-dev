import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_collatelar_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/widgets/widget_taksasi_unit_car.dart';
import 'package:ad1ms2_dev/widgets/widget_taksasi_unit_motor_cycle.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TaksasiUnit extends StatefulWidget {
  @override
  _TaksasiUnitState createState() => _TaksasiUnitState();
}

class _TaksasiUnitState extends State<TaksasiUnit> {

  @override
  Widget build(BuildContext context) {
    var _provider = Provider.of<InformationCollateralChangeNotifier>(context,listen: false);
    print("object: ${_provider.objectSelected.id}");
    return Theme(
      data: ThemeData(
        primaryColor: Colors.black,
        fontFamily: "NunitoSans",
        accentColor: myPrimaryColor
      ),
      child:  _provider.groupObjectSelected.KODE == "002"
          ?
      _provider.objectSelected.id != "003" ? WidgetTaksasiUnitCar(kode: _provider.groupObjectSelected.KODE,) : SizedBox()
          :
      _provider.objectSelected.id != "001" ? WidgetTaksasiUnitMotorCycle(kode: _provider.groupObjectSelected.KODE,) : SizedBox(),
    );
  }
}
