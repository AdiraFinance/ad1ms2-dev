import 'package:ad1ms2_dev/models/info_document_model.dart';
import 'package:ad1ms2_dev/models/info_document_type_model.dart';
import 'package:ad1ms2_dev/screens/detail_image.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/add_info_document_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class AddInfoDocument extends StatefulWidget {
    final int flag;
    final InfoDocumentModel formMInfoDocument;
    final int index;
    const AddInfoDocument({this.flag, this.formMInfoDocument, this.index});

    @override
    _AddInfoDocumentState createState() => _AddInfoDocumentState();
}

class _AddInfoDocumentState extends State<AddInfoDocument> {
    Future<void> _loadData;
    @override
    void initState() {
        _loadData = Provider.of<AddInfoDocumentChangeNotifier>(context, listen: false).setValueForEditDocument(context, widget.formMInfoDocument, widget.flag);
        super.initState();
    }
    @override
    Widget build(BuildContext context) {
        var _size = MediaQuery.of(context).size;
        return Theme(
            data: ThemeData(
                primaryColor: Colors.black,
                accentColor: myPrimaryColor,
                fontFamily: "NunitoSans",
                primarySwatch: primaryOrange),
            child: Scaffold(
                appBar: AppBar(
                    backgroundColor: myPrimaryColor,
                    title: Text(
                        widget.flag == 0 ? "Tambah Dokumen" : "Detail Dokumen",
                        style: TextStyle(color: Colors.black)),
                    centerTitle: true,
                    iconTheme: IconThemeData(
                        color: Colors.black,
                    ),
                ),
                body: SingleChildScrollView(
                    padding: EdgeInsets.symmetric(
                        horizontal: MediaQuery.of(context).size.width / 27,
                        vertical: MediaQuery.of(context).size.height / 57),
                    child: widget.flag == 0
                        ? FutureBuilder(
                            future: _loadData,
                            builder: (context, snapshot){
                                if(snapshot.connectionState == ConnectionState.waiting){
                                    return Center(child: CircularProgressIndicator());
                                }
                                return Consumer<AddInfoDocumentChangeNotifier>(
                                builder: (consumerContext, addDocumentChangeNotifier, _) {
                                    return Form(
                                        onWillPop: _onWillPop,
                                        key: addDocumentChangeNotifier.key,
                                        child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                                Visibility(
                                                    visible: addDocumentChangeNotifier.isDocumentTypeVisible(),
                                                    child: TextFormField(
                                                        autovalidate: addDocumentChangeNotifier.autoValidate,
                                                        validator: (e) {
                                                            if (e.isEmpty && addDocumentChangeNotifier.isDocumentTypeMandatory()) {
                                                                return "Tidak boleh kosong";
                                                            } else {
                                                                return null;
                                                            }
                                                        },
                                                        controller: addDocumentChangeNotifier.controllerDocumentType,
                                                        onTap: () {
                                                            FocusManager.instance.primaryFocus.unfocus();
                                                            addDocumentChangeNotifier.searchDocumentType(context,0);
                                                        },
                                                        decoration: InputDecoration(
                                                            labelText: "Tipe Dokumen",
                                                            border: OutlineInputBorder(),
                                                            contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                        ),
                                                    ),
                                                ),
                                                // DropdownButtonFormField<InfoDocumentTypeModel>(
                                                //     autovalidate: addDocumentChangeNotifier.autoValidate,
                                                //     validator: (e) {
                                                //         if (e == null) {
                                                //             return "Tidak boleh kosong";
                                                //         } else {
                                                //             return null;
                                                //         }
                                                //     },
                                                //     value: addDocumentChangeNotifier.documentTypeSelected,
                                                //     onChanged: (value) {
                                                //         addDocumentChangeNotifier.documentTypeSelected = value;
                                                //     },
                                                //     decoration: InputDecoration(
                                                //         labelText: "Jenis",
                                                //         border: OutlineInputBorder(),
                                                //         contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                                //     ),
                                                //     items: addDocumentChangeNotifier.listDocumentType.map((value) {
                                                //         return DropdownMenuItem<InfoDocumentTypeModel>(
                                                //             value: value,
                                                //             child: Text(
                                                //                 value.text,
                                                //                 overflow: TextOverflow.ellipsis,
                                                //             ),
                                                //         );
                                                //     }).toList(),
                                                // ),
                                                SizedBox(height: _size.height / 47),
                                                Visibility(
                                                    visible: addDocumentChangeNotifier.isDateDocumentVisible(),
                                                    child: TextFormField(
                                                        onTap: () {
                                                            addDocumentChangeNotifier.selectDocumentDate(context);
                                                        },
                                                        controller: addDocumentChangeNotifier.controllerDateDocument,
                                                        style: new TextStyle(color: Colors.black),
                                                        decoration: new InputDecoration(
                                                            labelText: 'Tanggal Terima',
                                                            labelStyle: TextStyle(color: Colors.black),
                                                            border: OutlineInputBorder(
                                                                borderRadius: BorderRadius.circular(8))),
                                                        autovalidate: addDocumentChangeNotifier.autoValidate,
                                                        validator: (e) {
                                                            if (e.isEmpty && addDocumentChangeNotifier.isDateDocumentMandatory()) {
                                                                return "Tidak boleh kosong";
                                                            } else {
                                                                return null;
                                                            }
                                                        },
                                                        readOnly: true,
                                                    ),
                                                ),
                                                SizedBox(height: _size.height / 47),
                                                addDocumentChangeNotifier.fileDocumentUnitObject == null
                                                    ?
                                                Visibility(
                                                    visible: addDocumentChangeNotifier.isBrowseDocumentVisible(),
                                                    child: RaisedButton(
                                                        onPressed: () {
                                                            addDocumentChangeNotifier.showBottomSheetChooseFile(context);
                                                        },
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius: new BorderRadius.circular(8.0)
                                                        ),
                                                        child: Row(
                                                            mainAxisSize: MainAxisSize.max,
                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                            children: <Widget>[
                                                                Icon(Icons.camera_alt),
                                                                SizedBox(width: _size.width/47),
                                                                Text("ADD")
                                                            ],
                                                        ),
                                                        color: myPrimaryColor,
                                                    ),
                                                )
                                                    : Card(
                                                    elevation: 3.3,
                                                    child: Row(
                                                        children: <Widget>[
                                                            Expanded(
                                                                child: Padding(
                                                                    padding:
                                                                    const EdgeInsets.all(8.0),
                                                                    child: Column(
                                                                        crossAxisAlignment:
                                                                        CrossAxisAlignment.start,
                                                                        children: <Widget>[
                                                                            Text(
                                                                                "${addDocumentChangeNotifier.fileDocumentUnitObject['file_name']}"),
                                                                            Text(
                                                                                "${addDocumentChangeNotifier.fileDocumentUnitObject['file'].lengthSync() / 1000} kB")
                                                                        ],
                                                                    ),
                                                                ),
                                                                flex: 8,
                                                            ),
                                                            Expanded(
                                                                flex: 2,
                                                                child: IconButton(
                                                                    icon: Icon(Icons.delete,
                                                                        color: Colors.red),
                                                                    onPressed: () {
                                                                        addDocumentChangeNotifier
                                                                            .fileDocumentUnitObject =
                                                                        null;
                                                                    }),
                                                            )
                                                        ],
                                                    ),
                                                ),
                                                addDocumentChangeNotifier.autoValidateFile
                                                    ? Padding(
                                                    padding: EdgeInsets.only(left: 12),
                                                    child: Text("Tidak boleh kosong",
                                                        style: TextStyle(
                                                            color: Colors.red,
                                                            fontSize: 12)),
                                                )
                                                    : SizedBox()
                                            ],
                                        ),
                                    );
                                },
                            );
                        },
                        )
                        : FutureBuilder(
                        future: _loadData,
                        builder: (context, snapshot) {
                            if (snapshot.connectionState == ConnectionState.waiting) {
                                return Center(
                                    child: CircularProgressIndicator(),
                                );
                            }
                            return Consumer<AddInfoDocumentChangeNotifier>(
                                builder:
                                    (consumerContext, addDocumentChangeNotifier, _) {
                                        return Form(
                                            key: addDocumentChangeNotifier.key,
                                            child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                    Padding(
                                                        padding: const EdgeInsets.all(12.0),
                                                        child: Column(
                                                            children: [
                                                                Row(
                                                                    children: [
                                                                        Expanded(
                                                                            child: Text(
                                                                                "Tipe Dokumen"),
                                                                            flex: 5),
                                                                        Text(" : "),
                                                                        Expanded(
                                                                            child: Text("${addDocumentChangeNotifier.documentTypeSelected.docTypeId} - ${addDocumentChangeNotifier.documentTypeSelected.docTypeName}"),
                                                                            flex: 5)
                                                                    ],
                                                                ),
                                                                SizedBox(
                                                                    height:
                                                                    MediaQuery.of(context).size.height /
                                                                        77),
                                                                Row(
                                                                    children: [
                                                                        Expanded(
                                                                            child: Text("Tanggal Terima"),
                                                                            flex: 5),
                                                                        Text(" : "),
                                                                        Expanded(
                                                                            child: Text(dateFormat.format(DateTime.parse(addDocumentChangeNotifier.controllerDateDocument.text))),
                                                                            flex: 5)
                                                                    ],
                                                                ),
                                                            ],
                                                        ),
                                                    ),
                                                    SizedBox(height: _size.height / 47),
                                                    Center(
                                                        child: Card(
                                                            elevation: 3.3,
                                                            child: Padding(
                                                                padding: const EdgeInsets.all(8.0),
                                                                child:
                                                                    addDocumentChangeNotifier.splitTypeFile(addDocumentChangeNotifier.documentDetailSelected.file.toString()) == "pdf" ||
                                                                    addDocumentChangeNotifier.splitTypeFile(addDocumentChangeNotifier.documentDetailSelected.file.toString()) == "docx" ||
                                                                    addDocumentChangeNotifier.splitTypeFile(addDocumentChangeNotifier.documentDetailSelected.file.toString()) == "xlsx"
                                                                ? Column(
                                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                                    children: <Widget>[
                                                                        Text("${addDocumentChangeNotifier.documentDetailSelected.fileName}"),
                                                                        Text("${addDocumentChangeNotifier.documentDetailSelected.file.lengthSync() / 1000} kB")
                                                                    ],
                                                                 )
                                                                : InkWell(
                                                                        onTap: (){
                                                                            Navigator.push(context, MaterialPageRoute(builder: (context) => DetailImage(imageFile: addDocumentChangeNotifier.documentDetailSelected.file)));
                                                                        },
                                                                  child: ClipRRect(
                                                                      child: Image.file(addDocumentChangeNotifier.documentDetailSelected.file,
                                                                          width: 250,
                                                                          height: 250,
                                                                          fit: BoxFit.cover,
                                                                          filterQuality: FilterQuality.medium),
                                                                          borderRadius: BorderRadius.circular(8.0),
                                                                   ),
                                                                )
                                                            ),
                                                        ),
                                                    )
                                                ],
                                            ),
                                        );
                                },
                            );
                        }),
                ),
                bottomNavigationBar: widget.flag == 0 ?  BottomAppBar(
                    elevation: 0.0,
                    child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Consumer<AddInfoDocumentChangeNotifier>(
                            builder: (context, addDocumentChangeNotifier, _) {
                                return RaisedButton(
                                    shape: RoundedRectangleBorder(
                                        borderRadius: new BorderRadius.circular(8.0)),
                                    color: myPrimaryColor,
                                    onPressed: () {
                                        if (widget.flag == 0) {
                                            addDocumentChangeNotifier.check(
                                                context, widget.flag, null);
                                        } else {
                                            addDocumentChangeNotifier.check(
                                                context, widget.flag, widget.index);
                                        }
                                    },
                                    child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                            Text("SAVE",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 14,
                                                    fontWeight: FontWeight.w500,
                                                    letterSpacing: 1.25))
                                        ],
                                    ));
                            },
                        )),
                ) : SizedBox()
            ),
        );
    }

    Future<bool> _onWillPop() async {
        var _provider = Provider.of<AddInfoDocumentChangeNotifier>(context, listen: false);
        if (widget.flag == 0) {
            if (_provider.documentTypeSelected != null ||
                _provider.controllerDateDocument.text != "") {
                return (await showDialog(
                    context: context,
                    builder: (myContext) => AlertDialog(
                        title: new Text('Warning'),
                        content: new Text('Keluar dengan simpan perubahan?'),
                        actions: <Widget>[
                            new FlatButton(
                                onPressed: () {
                                    widget.flag == 0
                                        ?
                                    _provider.check(context, widget.flag, null)
                                        :
                                    _provider.check(context, widget.flag, widget.index);
                                    Navigator.pop(context);
                                },
                                child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                            ),
                            new FlatButton(
                                onPressed: () => Navigator.of(context).pop(true),
                                child: new Text('Tidak'),
                            ),
                        ],
                    ),
                )) ??
                    false;
            } else {
                return true;
            }
        } else {
//            if (widget.formMInfoCreditSubsidyModel.giver != _provider.radioValueGiver ||
//                widget.formMInfoCreditSubsidyModel.type.id != _provider.typeSelected.id ||
//                widget.formMInfoCreditSubsidyModel.cuttingMethod.id != _provider.cuttingMethodSelected.id ||
//                widget.formMInfoCreditSubsidyModel.value != _provider.controllerValue.text) {
//                return (await showDialog(
//                    context: context,
//                    builder: (myContext) => AlertDialog(
//                        title: new Text('Warning'),
//                        content: new Text('Simpan perubahan?'),
//                        actions: <Widget>[
//                            new FlatButton(
//                                onPressed: () {
//                                    _provider.check(context, widget.flag, widget.index);
//                                    Navigator.pop(context);
//                                },
//                                child: new Text('Ya'),
//                            ),
//                            new FlatButton(
//                                onPressed: () => Navigator.of(context).pop(true),
//                                child: new Text('Tidak'),
//                            ),
//                        ],
//                    ),
//                )) ??
//                    false;
//            } else {
//                return true;
//            }
        }
    }


//    List<Widget> _listWidgetImageTempatTinggal(List<ImageFileModel> data, BuildContext context) {
//        List<Widget> _newListWidget = [];
//            _newListWidget.add(Padding(
//                padding: EdgeInsets.symmetric(
//                    horizontal: MediaQuery.of(context).size.width / 57,
//                    vertical: MediaQuery.of(context).size.height / 57),
//                child: PopupMenuButton<String>(
//                    onSelected: (value) {},
//                    child: ClipRRect(
//                        child: Image.file(data[i].imageFile,
//                            width: 150,
//                            height: 150,
//                            fit: BoxFit.cover,
//                            filterQuality: FilterQuality.medium),
//                        borderRadius: BorderRadius.circular(8.0),
//                    ),
//                    itemBuilder: (context) {
//                        return TitlePopUpMenuButton.choices.map((String choice) {
//                            return PopupMenuItem(
//                                value: choice,
//                                child: Text(choice),
//                            );
//                        }).toList();
//                    }),
//            ));
//        return _newListWidget;
//    }
}
