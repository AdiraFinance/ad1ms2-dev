import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/installment_type_model.dart';
import 'package:ad1ms2_dev/models/payment_method_model.dart';
import 'package:ad1ms2_dev/models/period_of_time_model.dart';
import 'package:ad1ms2_dev/screens/app/add_info_fee_credit_structure.dart';
import 'package:ad1ms2_dev/screens/app/list_info_fee_credit_structure.dart';
import 'package:ad1ms2_dev/screens/form_m/form_m_foto.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_type_installment_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_collatelar_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/decimal_text_input_formatter.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class InfoCreditStructure extends StatefulWidget {
  @override
  _InfoCreditStructureState createState() => _InfoCreditStructureState();
}

class _InfoCreditStructureState extends State<InfoCreditStructure> {

  Future<void> _setDataFromSQLite;

  @override
  void initState() {
    super.initState();
    // if(Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).listPeriodOfTime.isEmpty){
    _setDataFromSQLite = Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).setCustType(context);
    // }
    // Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).getDPFlag(context);
    // Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).getDataFromDashboard(context);
    // Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).installmentTypeSyariah(context);
  }
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        accentColor: myPrimaryColor,
        primaryColor: Colors.black,
        fontFamily: "NunitoSans"
      ),
      child: Scaffold(
        key: Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).scaffoldKey,
        appBar: AppBar(
            title:
            Text("Informasi Struktur Kredit", style: TextStyle(color: Colors.black)),
            centerTitle: true,
            backgroundColor: myPrimaryColor,
            iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            FlatButton(
                onPressed: (){
                  Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).calculateCredit(context);
                  // Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).conditionGetMinDP(context);
                },
                child: Text("CALC")
            )
          ],
        ),
        body:
        // Provider.of<InfoCreditStructureChangeNotifier>(context,listen: true).loadData
        //     ?
        // Padding(
        //   padding: const EdgeInsets.all(8.0),
        //   child: Center(child: CircularProgressIndicator()),
        // )
        //     :
        FutureBuilder(
            future: _setDataFromSQLite,
            builder: (context,snapshot){
              if(snapshot.connectionState == ConnectionState.waiting){
                return Center(child: CircularProgressIndicator());
              }
              return SingleChildScrollView(
                padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width/37,
                  vertical: MediaQuery.of(context).size.height/57,
                ),
                child: Consumer<InfoCreditStructureChangeNotifier>(
                  builder: (context,infoCreditStructureChangeNotifier,_){
                    return Form(
                        key: infoCreditStructureChangeNotifier.keyForm,
                        onWillPop: infoCreditStructureChangeNotifier.onBackPress,
                        child: Column(
                        children: [
                          Visibility(
                            visible: infoCreditStructureChangeNotifier.isInstallmentTypeSelectedVisible(),
                            child: IgnorePointer(
                              ignoring: infoCreditStructureChangeNotifier.isDisablePACIAAOSCONA,
                              child: DropdownButtonFormField<InstallmentTypeModel>(
                                  autovalidate: infoCreditStructureChangeNotifier.autoValidate,
                                  validator: (e) {
                                    if (e == null && infoCreditStructureChangeNotifier.isInstallmentTypeSelectedMandatory()) {
                                      return "Silahkan pilih jenis angsuran";
                                    } else {
                                      return null;
                                    }
                                  },
                                  value: infoCreditStructureChangeNotifier.installmentTypeSelected,
                                  onChanged: (value) {
                                    infoCreditStructureChangeNotifier.installmentTypeSelected = value;
                                    Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false).clearData();
                                    Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).setIsVisible(value.id);
                                    infoCreditStructureChangeNotifier.hideShowEffRate(context);
                                    infoCreditStructureChangeNotifier.clearInfoCreditStructureWithoutInstallmentType();
                                  },
                                  decoration: InputDecoration(
                                    labelText: "Jenis Angsuran",
                                    filled: infoCreditStructureChangeNotifier.isDisablePACIAAOSCONA,
                                    fillColor: Colors.black12,
                                    border: OutlineInputBorder(),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: infoCreditStructureChangeNotifier.installmentTypeDakor ? Colors.purple : Colors.grey)),
                                    disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: infoCreditStructureChangeNotifier.installmentTypeDakor ? Colors.purple : Colors.grey)),
                                    contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                  ),
                                  items: infoCreditStructureChangeNotifier.listInstallmentType.map((value) {
                                    return DropdownMenuItem<InstallmentTypeModel>(
                                      value: value,
                                      child: Text(
                                        value.name,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    );
                                  }).toList()),
                            ),
                          ),
                          Visibility(
                              visible: infoCreditStructureChangeNotifier.isInstallmentTypeSelectedVisible(),
                              child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                          ),
                          Visibility(
                            visible: infoCreditStructureChangeNotifier.isPeriodOfTimeSelectedVisible(),
                            child: IgnorePointer(
                              ignoring: infoCreditStructureChangeNotifier.isDisablePACIAAOSCONA,
                              child: DropdownButtonFormField<String>(
                                  autovalidate: infoCreditStructureChangeNotifier.autoValidate,
                                  validator: (e) {
                                    if (e == null && infoCreditStructureChangeNotifier.isPeriodOfTimeSelectedMandatory()) {
                                      return "Silahkan pilih jangka waktu";
                                    } else {
                                      return null;
                                    }
                                  },
                                  value: infoCreditStructureChangeNotifier.periodOfTimeSelected,
                                  onChanged: (value) {
                                    infoCreditStructureChangeNotifier.periodOfTimeSelected = value;
                                    Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false).totalSteppingSelected = null;
                                    Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false).listInfoCreditStructureStepping.clear();
                                  },
                                  decoration: InputDecoration(
                                    labelText: "Jangka Waktu",
                                    filled: infoCreditStructureChangeNotifier.isDisablePACIAAOSCONA,
                                    fillColor: Colors.black12,
                                    border: OutlineInputBorder(),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: infoCreditStructureChangeNotifier.periodTimeDakor ? Colors.purple : Colors.grey)),
                                    disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: infoCreditStructureChangeNotifier.periodTimeDakor ? Colors.purple : Colors.grey)),
                                    contentPadding:
                                    EdgeInsets.symmetric(horizontal: 10),
                                  ),
                                  items: infoCreditStructureChangeNotifier.listPeriodOfTime.map((value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(
                                        value,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    );
                                  }).toList()),
                            ),
                          ),
                          Visibility(
                              visible: infoCreditStructureChangeNotifier.isPeriodOfTimeSelectedVisible(),
                              child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                          ),
                          Visibility(
                            visible: infoCreditStructureChangeNotifier.isPaymentPerYearVisible() && infoCreditStructureChangeNotifier.hideShowPaymentPerYear(),
                            child: IgnorePointer(
                              ignoring: infoCreditStructureChangeNotifier.isDisablePACIAAOSCONA,
                              child: TextFormField(
                                validator: (e) {
                                  if (e.isEmpty && infoCreditStructureChangeNotifier.isPaymentPerYearMandatory()) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                keyboardType: TextInputType.number,
                                onFieldSubmitted: (value){
                                  infoCreditStructureChangeNotifier.checkPaymentValid(value);
                                },
                                controller: infoCreditStructureChangeNotifier.controllerPaymentPerYear,
                                autovalidate: infoCreditStructureChangeNotifier.autoValidate,
                                decoration: new InputDecoration(
                                  labelText: 'Pembayaran Per Tahun',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: infoCreditStructureChangeNotifier.paymentPerYearDakor ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: infoCreditStructureChangeNotifier.paymentPerYearDakor ? Colors.purple : Colors.grey)),
                                  contentPadding:
                                  EdgeInsets.symmetric(horizontal: 10),
                                ),
                                inputFormatters: [
                                  FilteringTextInputFormatter.allow(RegExp('[0-9]')),
                                ],
                              ),
                            ),
                          ),
                          Visibility(
                              visible: infoCreditStructureChangeNotifier.isPaymentPerYearVisible() && infoCreditStructureChangeNotifier.hideShowPaymentPerYear(),
                              child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                          ),
                          Visibility(
                            visible: infoCreditStructureChangeNotifier.isPaymentMethodSelectedVisible(),
                            child: IgnorePointer(
                              ignoring: infoCreditStructureChangeNotifier.isDisablePACIAAOSCONA,
                              child: DropdownButtonFormField<PaymentMethodModel>(
                                  autovalidate: infoCreditStructureChangeNotifier.autoValidate,
                                  validator: (e) {
                                    if (e == null && infoCreditStructureChangeNotifier.isPaymentMethodSelectedMandatory()) {
                                      return "Silahkan pilih metode pembayaran";
                                    } else {
                                      return null;
                                    }
                                  },
                                  value: infoCreditStructureChangeNotifier.paymentMethodSelected,
                                  onTap: () {
                                    infoCreditStructureChangeNotifier.checkPaymentValid(infoCreditStructureChangeNotifier.controllerPaymentPerYear.text);
                                  },
                                  onChanged: (value) {
                                    infoCreditStructureChangeNotifier.paymentMethodSelected = value;
                                  },
                                  decoration: InputDecoration(
                                    labelText: "Metode Pembayaran",
                                    filled: infoCreditStructureChangeNotifier.isDisablePACIAAOSCONA,
                                    fillColor: Colors.black12,
                                    border: OutlineInputBorder(),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: infoCreditStructureChangeNotifier.paymentMethodDakor ? Colors.purple : Colors.grey)),
                                    disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: infoCreditStructureChangeNotifier.paymentMethodDakor ? Colors.purple : Colors.grey)),
                                    contentPadding:
                                    EdgeInsets.symmetric(horizontal: 10),
                                  ),
                                  items: infoCreditStructureChangeNotifier.listPaymentMethod.map((value) {
                                    return DropdownMenuItem<PaymentMethodModel>(
                                      value: value,
                                      child: Text(
                                        value.name,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    );
                                  }).toList()),
                            ),
                          ),
                          Visibility(
                              visible: infoCreditStructureChangeNotifier.isPaymentMethodSelectedVisible(),
                              child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                          ),
                          Visibility(
                            visible: infoCreditStructureChangeNotifier.isInterestRateEffectiveVisible() && infoCreditStructureChangeNotifier.hideShowEffRate(context),
                            child: IgnorePointer(
                              ignoring: infoCreditStructureChangeNotifier.isDisablePACIAAOSCONA || infoCreditStructureChangeNotifier.isDisableEffRate,
                              child: TextFormField(
                                  validator: (e) {
                                    if (e.isEmpty && infoCreditStructureChangeNotifier.isInterestRateEffectiveMandatory()) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  inputFormatters: [
                                    DecimalTextInputFormatter(decimalRange: 2),
                                  ],
                                  keyboardType: TextInputType.number,
                                  // readOnly: true,
                                  onTap: () {
                                    // infoCreditStructureChangeNotifier.searchGroupObject(context);
                                    // infoCreditStructureChangeNotifier.calculateCredit(context, flag);
                                    infoCreditStructureChangeNotifier.checkPaymentValid(infoCreditStructureChangeNotifier.controllerPaymentPerYear.text);
                                  },
                                  onFieldSubmitted: (value){
                                    infoCreditStructureChangeNotifier.controllerInterestRateEffective.text = infoCreditStructureChangeNotifier.formatCurrency.formatCurrencyPoint5(value);
                                    // infoCreditStructureChangeNotifier.calculateCredit(context, "EFF_RATE");
                                    // infoCreditStructureChangeNotifier.getMinDP(context, "EFF_RATE");
                                  },
                                  controller: infoCreditStructureChangeNotifier.controllerInterestRateEffective,
                                  autovalidate: infoCreditStructureChangeNotifier.autoValidate,
                                  decoration: new InputDecoration(
                                    labelText: 'Suku Bunga/Tahun (Eff)',
                                    labelStyle: TextStyle(color: Colors.black),
                                    filled: infoCreditStructureChangeNotifier.isDisablePACIAAOSCONA || infoCreditStructureChangeNotifier.isDisableEffRate,
                                    fillColor: Colors.black12,
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: infoCreditStructureChangeNotifier.interestRateEffDakor ? Colors.purple : Colors.grey)),
                                    disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: infoCreditStructureChangeNotifier.interestRateEffDakor ? Colors.purple : Colors.grey)),
                                  ),
                                  // enabled: infoCreditStructureChangeNotifier.enableInterestRateEffective(context)
                                // infoCreditStructureChangeNotifier.custType != "COM"
                                //     ?
                                // Provider.of<FormMFotoChangeNotifier>(context,listen: false).typeOfFinancingModelSelected.financingTypeId == "1"
                                //     ?
                                // infoCreditStructureChangeNotifier.installmentTypeSelected.id == "04" ? false : true
                                //     :
                                // false
                                //     :
                                // Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).typeOfFinancingModelSelected.financingTypeId == "1"
                                //     ?
                                // infoCreditStructureChangeNotifier.installmentTypeSelected.id == "04"
                                //     ?
                                // false
                                //     :
                                // true
                                //   :
                                // false
                                // infoCreditStructureChangeNotifier.installmentTypeSelected.id == "04" ? false : true
                              ),
                            ),
                          ),
                          Visibility(
                              visible: infoCreditStructureChangeNotifier.isInterestRateEffectiveVisible() && infoCreditStructureChangeNotifier.hideShowEffRate(context),
                              child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                          ),
                          // infoCreditStructureChangeNotifier.installmentTypeSelected != null
                          //     ?
                          // infoCreditStructureChangeNotifier.installmentTypeSelected.id != "09"
                          //     ?
                          Visibility(
                            visible: infoCreditStructureChangeNotifier.isInterestRateFlatVisible(),
                            child: IgnorePointer(
                              ignoring: infoCreditStructureChangeNotifier.isDisablePACIAAOSCONA,
                              child: TextFormField(
                                  validator: (e) {
                                    if (e.isEmpty && infoCreditStructureChangeNotifier.isInterestRateFlatMandatory()) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  inputFormatters: [
                                    DecimalTextInputFormatter(decimalRange: 2),
                                  ],
                                  keyboardType: TextInputType.number,
                                  // readOnly: true,
                                  onTap: () {
                                    // infoCreditStructureChangeNotifier.searchGroupObject(context);
                                    // infoCreditStructureChangeNotifier.calculateCredit(context, flag);
                                    infoCreditStructureChangeNotifier.checkPaymentValid(infoCreditStructureChangeNotifier.controllerPaymentPerYear.text);
                                  },
                                  onFieldSubmitted: (value){
                                    infoCreditStructureChangeNotifier.controllerInterestRateFlat.text = infoCreditStructureChangeNotifier.formatCurrency.formatCurrencyPoint5(value);
                                    // infoCreditStructureChangeNotifier.calculateCredit(context, "FLAT_RATE");
                                    // infoCreditStructureChangeNotifier.getMinDP(context, "FLAT_RATE");
                                  },
                                  controller: infoCreditStructureChangeNotifier.controllerInterestRateFlat,
                                  autovalidate: infoCreditStructureChangeNotifier.autoValidate,
                                  decoration: new InputDecoration(
                                    // labelText: true
                                      labelText: infoCreditStructureChangeNotifier.custType != "COM"
                                          ? Provider.of<FormMFotoChangeNotifier>(context, listen: false).typeOfFinancingModelSelected.financingTypeId == "1"
                                          ? infoCreditStructureChangeNotifier.installmentTypeSelected == null ? 'Suku Bunga/Tahun (Flat)' : infoCreditStructureChangeNotifier.installmentTypeSelected.id == "09"
                                          ? 'Ujrah'
                                          : 'Suku Bunga/Tahun (Flat)'
                                          : infoCreditStructureChangeNotifier.installmentTypeSelected == null ? 'Margin' : infoCreditStructureChangeNotifier.installmentTypeSelected.id == "09"
                                          ? 'Ujrah'
                                          : 'Margin'
                                          : Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).typeOfFinancingModelSelected.financingTypeId == "1"
                                          ? infoCreditStructureChangeNotifier.installmentTypeSelected == null ? 'Suku Bunga/Tahun (Flat)' : infoCreditStructureChangeNotifier.installmentTypeSelected.id == "09"
                                          ? 'Ujrah'
                                          : 'Suku Bunga/Tahun (Flat)'
                                          : infoCreditStructureChangeNotifier.installmentTypeSelected == null ? 'Margin' : infoCreditStructureChangeNotifier.installmentTypeSelected.id == "09"
                                          ? 'Ujrah'
                                          : 'Margin',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: infoCreditStructureChangeNotifier.isDisablePACIAAOSCONA,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoCreditStructureChangeNotifier.interestRateFlatDakor ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: infoCreditStructureChangeNotifier.interestRateFlatDakor ? Colors.purple : Colors.grey)),
                                  )
                              ),
                            ),
                          ),
                          //     :
                          // SizedBox()
                          //     :
                          // SizedBox(),
                          Visibility(
                              visible: infoCreditStructureChangeNotifier.isInterestRateFlatVisible(),
                              child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                          ),
                          Visibility(
                            visible: infoCreditStructureChangeNotifier.isObjectPriceVisible(),
                            child: IgnorePointer(
                              ignoring: infoCreditStructureChangeNotifier.isDisablePACIAAOSCONA,
                              child: TextFormField(
                                  validator: (e) {
                                    if (e.isEmpty && infoCreditStructureChangeNotifier.isObjectPriceMandatory()) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  onChanged: (value){
                                    infoCreditStructureChangeNotifier.countPHMaxColla(context); //lempar harga objek ke showroom jaminan
                                  },
                                  controller: infoCreditStructureChangeNotifier.controllerObjectPrice,
                                  autovalidate: infoCreditStructureChangeNotifier.autoValidate,
                                  keyboardType: TextInputType.number,
                                  textAlign: TextAlign.end,
                                  inputFormatters: [
                                    DecimalTextInputFormatter(decimalRange: 2),
                                    infoCreditStructureChangeNotifier.amountValidator
                                  ],
                                  onFieldSubmitted: (value){
                                    infoCreditStructureChangeNotifier.controllerObjectPrice.text = infoCreditStructureChangeNotifier.formatCurrency.formatCurrency(value);
                                    infoCreditStructureChangeNotifier.countPHMaxColla(context); //lempar harga objek ke showroom jaminan
                                    // infoCreditStructureChangeNotifier.getMinDP(context, "");
                                  },
                                  decoration: new InputDecoration(
                                    labelText: 'Harga Objek',
                                    labelStyle: TextStyle(color: Colors.black),
                                    filled: infoCreditStructureChangeNotifier.isDisablePACIAAOSCONA,
                                    fillColor: Colors.black12,
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: infoCreditStructureChangeNotifier.objectPriceDakor ? Colors.purple : Colors.grey)),
                                    disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: infoCreditStructureChangeNotifier.objectPriceDakor ? Colors.purple : Colors.grey)),
                                  ),
                                  onTap: () {
                                    infoCreditStructureChangeNotifier.formatting();
                                    // infoCreditStructureChangeNotifier.getMinDP(context, "");
                                    infoCreditStructureChangeNotifier.checkPaymentValid(infoCreditStructureChangeNotifier.controllerPaymentPerYear.text);
                                  }
                              ),
                            ),
                          ),
                          Visibility(
                              visible: infoCreditStructureChangeNotifier.isObjectPriceVisible(),
                              child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          Visibility(
                            visible: infoCreditStructureChangeNotifier.isKaroseriTotalPriceVisible(), //true,
                            child: IgnorePointer(
                              ignoring: infoCreditStructureChangeNotifier.isDisablePACIAAOSCONA,
                              child: TextFormField(
                                  textAlign: TextAlign.end,
                                  enabled: false,
                                  controller: infoCreditStructureChangeNotifier.controllerKaroseriTotalPrice,
                                  decoration: new InputDecoration(
                                    labelText: 'Total Harga Karoseri',
                                    labelStyle: TextStyle(color: Colors.black),
                                    filled: true,
                                    fillColor: Colors.black12,
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: infoCreditStructureChangeNotifier.totalPriceKaroseriDakor ? Colors.purple : Colors.grey)),
                                    disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: infoCreditStructureChangeNotifier.totalPriceKaroseriDakor ? Colors.purple : Colors.grey)),
                                  )
                              ),
                            ),
                          ),
                          Visibility(
                              visible: infoCreditStructureChangeNotifier.isKaroseriTotalPriceVisible(),
                              child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          Visibility(
                            visible: infoCreditStructureChangeNotifier.isTotalPriceVisible(),
                            child: IgnorePointer(
                              ignoring: infoCreditStructureChangeNotifier.isDisablePACIAAOSCONA,
                              child: TextFormField(
                                  textAlign: TextAlign.end,
                                  enabled: false,
                                  controller: infoCreditStructureChangeNotifier.controllerTotalPrice,
                                  decoration: new InputDecoration(
                                    labelText: 'Total Harga',
                                    labelStyle: TextStyle(color: Colors.black),
                                    filled: true,
                                    fillColor: Colors.black12,
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: infoCreditStructureChangeNotifier.totalPriceDakor ? Colors.purple : Colors.grey)),
                                    disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: infoCreditStructureChangeNotifier.totalPriceDakor ? Colors.purple : Colors.grey)),
                                  )
                              ),
                            ),
                          ),
                          Visibility(
                              visible: infoCreditStructureChangeNotifier.isTotalPriceVisible(),
                              child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          Visibility(
                            visible: infoCreditStructureChangeNotifier.isNetDPVisible(),
                            child: IgnorePointer(
                              ignoring: infoCreditStructureChangeNotifier.isDisablePACIAAOSCONA,
                              child: TextFormField(
                                controller: infoCreditStructureChangeNotifier.controllerNetDP,
                                keyboardType: TextInputType.number,
                                textAlign: TextAlign.end,
                                inputFormatters: [
                                  DecimalTextInputFormatter(decimalRange: 2),
                                  infoCreditStructureChangeNotifier.amountValidator
                                ],
                                onTap: () {
                                  infoCreditStructureChangeNotifier.formatting();
                                  // infoCreditStructureChangeNotifier.getMinDP(context, "");
                                  infoCreditStructureChangeNotifier.checkPaymentValid(infoCreditStructureChangeNotifier.controllerPaymentPerYear.text);
                                },
                                onFieldSubmitted: (value){
                                  infoCreditStructureChangeNotifier.controllerNetDP.text = infoCreditStructureChangeNotifier.formatCurrency.formatCurrency(value);
                                  // infoCreditStructureChangeNotifier.getMinDP(context, "");
                                },
                                decoration: new InputDecoration(
                                  labelText: 'Uang Muka (Net)',
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: infoCreditStructureChangeNotifier.dpFlag == "0" ? true : false,
                                  fillColor: Colors.black12,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: infoCreditStructureChangeNotifier.netDPDakor ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: infoCreditStructureChangeNotifier.netDPDakor ? Colors.purple : Colors.grey)),
                                ),
                                enabled: infoCreditStructureChangeNotifier.dpFlag == "0" ? false : true,
                              ),
                            ),
                          ),
                          Visibility(
                              visible: infoCreditStructureChangeNotifier.isNetDPVisible(),
                              child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          Visibility(
                            visible: infoCreditStructureChangeNotifier.isBranchDPVisible(),
                            child: IgnorePointer(
                              ignoring: infoCreditStructureChangeNotifier.isDisablePACIAAOSCONA,
                              child: TextFormField(
                                autovalidate: infoCreditStructureChangeNotifier.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty ) {//&& infoCreditStructureChangeNotifier.isBranchDPMandatory()
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                controller: infoCreditStructureChangeNotifier.controllerBranchDP,
                                keyboardType: TextInputType.number,
                                textAlign: TextAlign.end,
                                inputFormatters: [
                                  DecimalTextInputFormatter(decimalRange: 2),
                                  infoCreditStructureChangeNotifier.amountValidator
                                ],
                                onTap: () {
                                  infoCreditStructureChangeNotifier.formatting();
                                  infoCreditStructureChangeNotifier.checkPaymentValid(infoCreditStructureChangeNotifier.controllerPaymentPerYear.text);
                                },
                                onFieldSubmitted: (value){
                                  infoCreditStructureChangeNotifier.controllerBranchDP.text = infoCreditStructureChangeNotifier.formatCurrency.formatCurrency(value);
                                },
                                decoration: new InputDecoration(
                                  labelText: 'Uang Muka (Cabang)',
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: infoCreditStructureChangeNotifier.isDisablePACIAAOSCONA,
                                  fillColor: Colors.black12,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: infoCreditStructureChangeNotifier.branchDPDakor ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: infoCreditStructureChangeNotifier.branchDPDakor ? Colors.purple : Colors.grey)),
                                ),
                              ),
                            ),
                          ),
                          Visibility(
                              visible: infoCreditStructureChangeNotifier.isBranchDPVisible(),
                              child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          Visibility(
                            visible: infoCreditStructureChangeNotifier.isGrossDPVisible(),
                            child: IgnorePointer(
                              ignoring: infoCreditStructureChangeNotifier.isDisablePACIAAOSCONA,
                              child: TextFormField(
                                validator: (e) {
                                  if (e.isEmpty && infoCreditStructureChangeNotifier.isGrossDPMandatory()) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                keyboardType: TextInputType.number,
                                textAlign: TextAlign.end,
                                inputFormatters: [
                                  DecimalTextInputFormatter(decimalRange: 2),
                                  infoCreditStructureChangeNotifier.amountValidator
                                ],
                                onTap: () {
                                  infoCreditStructureChangeNotifier.formatting();
                                  // infoCreditStructureChangeNotifier.getMinDP(context, "");
                                  infoCreditStructureChangeNotifier.checkPaymentValid(infoCreditStructureChangeNotifier.controllerPaymentPerYear.text);
                                },
                                onFieldSubmitted: (value){
                                  infoCreditStructureChangeNotifier.controllerGrossDP.text = infoCreditStructureChangeNotifier.formatCurrency.formatCurrency(value);
                                  // infoCreditStructureChangeNotifier.getMinDP(context, "");

                                },
                                controller: infoCreditStructureChangeNotifier.controllerGrossDP,
                                autovalidate: infoCreditStructureChangeNotifier.autoValidate,
                                decoration: new InputDecoration(
                                  labelText: 'Uang Muka (Gross)',
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: infoCreditStructureChangeNotifier.isDisablePACIAAOSCONA ? true : infoCreditStructureChangeNotifier.dpFlag == "1" ? true : false,
                                  fillColor: Colors.black12,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: infoCreditStructureChangeNotifier.grossDPDakor ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: infoCreditStructureChangeNotifier.grossDPDakor ? Colors.purple : Colors.grey)),
                                ),
                                enabled: infoCreditStructureChangeNotifier.dpFlag == "1" ? false : true,
                              ),
                            ),
                          ),
                          Visibility(
                              visible: infoCreditStructureChangeNotifier.isGrossDPVisible(),
                              child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          Visibility(
                            visible: infoCreditStructureChangeNotifier.isTotalLoanVisible(),
                            child: IgnorePointer(
                              ignoring: infoCreditStructureChangeNotifier.isDisablePACIAAOSCONA,
                              child: TextFormField(
                                  textAlign: TextAlign.end,
                                  enabled: false,
                                  controller: infoCreditStructureChangeNotifier.controllerTotalLoan,
                                  decoration: new InputDecoration(
                                    labelText: 'Jumlah Pinjaman',
                                    labelStyle: TextStyle(color: Colors.black),
                                    filled: true,
                                    fillColor: Colors.black12,
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: infoCreditStructureChangeNotifier.totalLoanDakor ? Colors.purple : Colors.grey)),
                                    disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: infoCreditStructureChangeNotifier.totalLoanDakor ? Colors.purple : Colors.grey)),
                                  )
                              ),
                            ),
                          ),
                          Visibility(
                              visible: infoCreditStructureChangeNotifier.isTotalLoanVisible(),
                              child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          Visibility(
                            visible: infoCreditStructureChangeNotifier.isInstallmentVisible(),
                            child: IgnorePointer(
                              ignoring: infoCreditStructureChangeNotifier.isDisablePACIAAOSCONA,
                              child: TextFormField(
                                  textAlign: TextAlign.end,
                                  enabled: false,
                                  controller: infoCreditStructureChangeNotifier.controllerInstallment,
                                  decoration: new InputDecoration(
                                    labelText: 'Angsuran',
                                    labelStyle: TextStyle(color: Colors.black),
                                    filled: true,
                                    fillColor: Colors.black12,
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: infoCreditStructureChangeNotifier.installmentDakor ? Colors.purple : Colors.grey)),
                                    disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: infoCreditStructureChangeNotifier.installmentDakor ? Colors.purple : Colors.grey)),
                                  )
                              ),
                            ),
                          ),
                          Visibility(
                              visible: infoCreditStructureChangeNotifier.isInstallmentVisible(),
                              child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          Visibility(
                            visible: infoCreditStructureChangeNotifier.isLTVVisible(),
                            child: IgnorePointer(
                              ignoring: infoCreditStructureChangeNotifier.isDisablePACIAAOSCONA,
                              child: TextFormField(
                                  textAlign: TextAlign.end,
                                  enabled: false,
                                  controller: infoCreditStructureChangeNotifier.controllerLTV,
                                  decoration: new InputDecoration(
                                    labelText: 'LTV (%)',
                                    labelStyle: TextStyle(color: Colors.black),
                                    filled: true,
                                    fillColor: Colors.black12,
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: infoCreditStructureChangeNotifier.ltvDakor ? Colors.purple : Colors.grey)),
                                    disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: infoCreditStructureChangeNotifier.ltvDakor ? Colors.purple : Colors.grey)),
                                  )
                              ),
                            ),
                          ),
                          Visibility(
                              visible: infoCreditStructureChangeNotifier.isLTVVisible(),
                              child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          InkWell(
                            onTap: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context) => ListInfoFeeCreditStructure()));
                            },
                            child: Card(
                              elevation: 3.3,
                              child: Padding(
                                padding: EdgeInsets.all(13.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text("Jumlah Info Struktur Kredit Biaya : ${infoCreditStructureChangeNotifier.listInfoFeeCreditStructure.length}"),
                                    Icon(Icons.add_circle_outline, color: primaryOrange,)
                                  ],
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height / 47),
                          infoCreditStructureChangeNotifier.autoValidateInfoFeeCreditStructure
                              ?
                          Container(
                            margin: EdgeInsets.only(
                                left: MediaQuery.of(context).size.width / 37),
                            child: Text("Tidak boleh kosong",
                                style: TextStyle(color: Colors.red, fontSize: 12)),
                          )
                              :
                          SizedBox(),
                        ],
                      ),
                    );
                  },
                ),
              );
            },
          ),
        bottomNavigationBar: BottomAppBar(
              elevation: 0.0,
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Consumer<InfoCreditStructureChangeNotifier>(
                      builder: (context, infoCreditStructureChangeNotifier, _) {
                          return RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(8.0)),
                              color: myPrimaryColor,
                              onPressed: () {
                                  infoCreditStructureChangeNotifier.check(context);
                              },
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                      Text("DONE",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500,
                                              letterSpacing: 1.25))
                                  ],
                              ));
                      },
                  )),
          ),
      ),
    );
  }
}
