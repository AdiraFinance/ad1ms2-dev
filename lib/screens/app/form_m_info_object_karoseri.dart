
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/app/list_object_karoseri.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_object_karoseri_change_notifier.dart';

class FormMInfoObjectKaroseri extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<InfoObjectKaroseriChangeNotifier>(
        builder: (context, formMInfoObjKaroseriChangeNotif, _) {
            return Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: MediaQuery.of(context).size.width/37,
                    vertical: MediaQuery.of(context).size.height/57,
                ),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                        InkWell(
                            onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ListObjectKaroseri()));
                            },
                            child: Card(
                                elevation: 3.3,
                                child: Padding(
                                    padding: const EdgeInsets.all(16.0),
                                    child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                            Text(
                                                "Jumlah Informasi Object Karoseri : ${formMInfoObjKaroseriChangeNotif.listFormKaroseriObject.length}"),
                                            Icon(Icons.add_circle_outline, color: primaryOrange,)
                                        ],
                                    ),
                                ),
                            ),
                        ),
                        formMInfoObjKaroseriChangeNotif.autoValidate
                            ? Container(
                                margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                                child: Text("Tidak boleh kosong",style: TextStyle(color: Colors.red, fontSize: 12)),
                            )
                            : SizedBox(height: MediaQuery.of(context).size.height / 87),
                    ],
                ),
            );
        },
    );
  }
}
