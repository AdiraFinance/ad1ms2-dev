import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/app/add_sales.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/add_salesman_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_salesman_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_individu_change_notifier/form_m_parent_individu_change_notifier.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class InformationSalesman extends StatefulWidget {
  @override
  _InformationSalesmanState createState() => _InformationSalesmanState();
}

class _InformationSalesmanState extends State<InformationSalesman> {
  @override
  void initState() {
    super.initState();
    Provider.of<InformationSalesmanChangeNotifier>(context, listen: false).setPreference();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        primaryColor: Colors.black,
        primarySwatch: primaryOrange,
        accentColor: myPrimaryColor,
      ),
      child: Scaffold(
        appBar: AppBar(
          title:
          Text("List Informasi Salesman", style: TextStyle(color: Colors.black)),
          centerTitle: true,
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        body: Consumer<InformationSalesmanChangeNotifier>(
          builder: (context, infoSalesmanChangeNotifier, _) {
            return Form(
              onWillPop: infoSalesmanChangeNotifier.onBackPress,
              child: infoSalesmanChangeNotifier.listInfoSales.isEmpty
                  ? Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // Image.asset("img/alamat_kosong.png", height: MediaQuery.of(context).size.height / 9,),
                    // SizedBox(height: MediaQuery.of(context).size.height / 47,),
                    Text("Tambah Salesman", style: TextStyle(color: Colors.grey, fontSize: 16),)
                  ],
                ),
              )
              : ListView.builder(
                padding: EdgeInsets.symmetric(
                    horizontal: MediaQuery.of(context).size.width / 27,
                    vertical: MediaQuery.of(context).size.height / 47),
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ChangeNotifierProvider(
                              create: (context) => AddSalesmanChangeNotifier(),
                              child: AddSales(
                                flag: 1,
                                model: infoSalesmanChangeNotifier.listInfoSales[index],
                                index: index,
                              )
                            ),
                          )
                      );
                    },
                    child: Card(
                      shape: infoSalesmanChangeNotifier.listInfoSales[index].isEdit
                          ?
                      RoundedRectangleBorder(
                          side: BorderSide(color: Colors.purple, width: 2),
                          borderRadius: BorderRadius.all(Radius.circular(4))
                      )
                          :
                      null,
                      elevation: 3.3,
                      child: Padding(
                        padding: const EdgeInsets.all(13.0),
                        child: Stack(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("${infoSalesmanChangeNotifier.listInfoSales[index].salesmanTypeModel.id} - ${infoSalesmanChangeNotifier.listInfoSales[index].salesmanTypeModel.salesmanType}"),
                                SizedBox(height: MediaQuery.of(context).size.height / 57),
                                Text("${infoSalesmanChangeNotifier.listInfoSales[index].positionModel.id} - ${infoSalesmanChangeNotifier.listInfoSales[index].positionModel.positionName}"),
                                SizedBox(height: MediaQuery.of(context).size.height / 57),
                                Text(infoSalesmanChangeNotifier.listInfoSales[index].employeeModel != null ? "${infoSalesmanChangeNotifier.listInfoSales[index].employeeModel.NIK} - ${infoSalesmanChangeNotifier.listInfoSales[index].employeeModel.Nama}" : ""),
                                SizedBox(height: MediaQuery.of(context).size.height / 57),
                                Text(infoSalesmanChangeNotifier.listInfoSales[index].employeeHeadModel != null ? "${infoSalesmanChangeNotifier.listInfoSales[index].employeeHeadModel.NIK} - ${infoSalesmanChangeNotifier.listInfoSales[index].employeeHeadModel.Nama}" : ""),
                              ],
                            ),
                            Align(
                              alignment: Alignment.topRight,
                              child: Provider.of<InformationSalesmanChangeNotifier>(context, listen: false).isDisablePACIAAOSCONA
                              ? SizedBox()
                              : IconButton(
                                  icon: Icon(
                                    Icons.delete,
                                    color: Colors.red,
                                  ),
                                  onPressed: () {
                                    Provider.of<InformationSalesmanChangeNotifier>(
                                            context,
                                            listen: false)
                                        .deleteListInfoSales(context, index);
                                  }),
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                },
                itemCount: infoSalesmanChangeNotifier.listInfoSales.length,
              ),
            );
          },
        ),
        floatingActionButton:
        Provider.of<InformationSalesmanChangeNotifier>(context, listen: false).isDisablePACIAAOSCONA
          ?
        null
          :
        FloatingActionButton(
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ChangeNotifierProvider(
                    create: (context) => AddSalesmanChangeNotifier(),
                    child: AddSales(
                      flag: 0,
                      index: null,
                      model: null,
                    ),
                  ),
                ));
          },
          child: Icon(
            Icons.add,
          ),
          backgroundColor: myPrimaryColor,
        ),
      ),
    );
  }
}
