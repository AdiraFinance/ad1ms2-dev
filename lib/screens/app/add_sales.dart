import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/employee_head_model.dart';
import 'package:ad1ms2_dev/models/infromation_sales_model.dart';
import 'package:ad1ms2_dev/models/position_model.dart';
import 'package:ad1ms2_dev/models/salesman_type_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/add_salesman_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddSales extends StatefulWidget {
  final int flag;
  final int index;
  final InformationSalesModel model;
  const AddSales({this.flag, this.model, this.index});
  @override
  _AddSalesState createState() => _AddSalesState();
}

class _AddSalesState extends State<AddSales> {
  Future<void> _setValueForEdit;
  @override
  void initState() {
    super.initState();
    _setValueForEdit = Provider.of<AddSalesmanChangeNotifier>(context, listen: false).setValue(widget.flag, widget.index, widget.model, context);
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          fontFamily: "NunitoSans",
          primaryColor: Colors.black,
          primarySwatch: primaryOrange,
          accentColor: myPrimaryColor),
      child: Scaffold(
        key: Provider.of<AddSalesmanChangeNotifier>(context, listen: false).scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(
            widget.flag == 0 ? "Tambah Salesman" : Provider.of<AddSalesmanChangeNotifier>(context, listen: false).isDisablePACIAAOSCONA ? "Detail Salesman" : "Edit Salesman",
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          centerTitle: true,
        ),
        body: FutureBuilder(
            future: _setValueForEdit,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
              return Consumer<AddSalesmanChangeNotifier>(
                builder: (context, addSalesmanChangeNotifier, _) {
                  return SingleChildScrollView(
                    padding: EdgeInsets.symmetric(horizontal: 13, vertical: 18),
                    child: Form(
                      onWillPop: _onWillPop,
                      key: addSalesmanChangeNotifier.key,
                      child: Column(
                        children: [
                          Visibility(
                            visible: addSalesmanChangeNotifier.isSalesmanTypeModelVisible(),
                            child: IgnorePointer(
                              ignoring: addSalesmanChangeNotifier.isDisablePACIAAOSCONA,
                              child: DropdownButtonFormField<SalesmanTypeModel>(
                                isExpanded: true,
                                autovalidate: addSalesmanChangeNotifier.autoValidate,
                                validator: (e) {
                                  if (e == null && addSalesmanChangeNotifier.isSalesmanTypeModelMandatory()) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                value: addSalesmanChangeNotifier.salesmanTypeModelSelected,
                                onChanged: (value) {
                                  addSalesmanChangeNotifier.salesmanTypeModelSelected = value;
                                  addSalesmanChangeNotifier.getSalesPosition(context);
                                },
                                onTap: () {
                                  FocusManager.instance.primaryFocus.unfocus();
                                },
                                decoration: InputDecoration(
                                  labelText: "Jenis Sales",
                                  filled: addSalesmanChangeNotifier.isDisablePACIAAOSCONA,
                                  fillColor: Colors.black12,
                                  border: OutlineInputBorder(),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: addSalesmanChangeNotifier.isSalesmanTypeModelChanges ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: addSalesmanChangeNotifier.isSalesmanTypeModelChanges ? Colors.purple : Colors.grey)),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                ),
                                items: addSalesmanChangeNotifier.listSalesmanType.map((value) {
                                  return DropdownMenuItem<SalesmanTypeModel>(
                                    value: value,
                                    child: Text(
                                      value.salesmanType,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                          ),
                          SizedBox(
                              height:
                              MediaQuery.of(context).size.height / 47),
                          Visibility(
                            visible: addSalesmanChangeNotifier.isPositionModelVisible(),
                            child: IgnorePointer(
                              ignoring: addSalesmanChangeNotifier.isDisablePACIAAOSCONA,
                              child: DropdownButtonFormField<PositionModel>(
                                isExpanded: true,
                                autovalidate: addSalesmanChangeNotifier.autoValidate,
                                validator: (e) {
                                  if (e == null && addSalesmanChangeNotifier.isPositionModelMandatory()) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                value: addSalesmanChangeNotifier.positionModelSelected,
                                onChanged: (value) {
                                  addSalesmanChangeNotifier.positionModelSelected = value;
                                },
                                onTap: () {
                                  FocusManager.instance.primaryFocus.unfocus();
                                },
                                decoration: InputDecoration(
                                  labelText: "Jabatan",
                                  filled: addSalesmanChangeNotifier.isDisablePACIAAOSCONA,
                                  fillColor: Colors.black12,
                                  border: OutlineInputBorder(),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: addSalesmanChangeNotifier.isPositionModelChanges ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: addSalesmanChangeNotifier.isPositionModelChanges ? Colors.purple : Colors.grey)),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                ),
                                items: addSalesmanChangeNotifier.listPosition
                                    .map((value) {
                                  return DropdownMenuItem<PositionModel>(
                                    value: value,
                                    child: Text(
                                      value.positionName,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                          ),
                          SizedBox(
                              height:
                              MediaQuery.of(context).size.height / 47),
                          Visibility(
                            visible: addSalesmanChangeNotifier.isEmployeeVisible(),
                            child: IgnorePointer(
                              ignoring: addSalesmanChangeNotifier.isDisablePACIAAOSCONA,
                              child: TextFormField(
                                  autovalidate: addSalesmanChangeNotifier.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && addSalesmanChangeNotifier.isEmployeeMandatory()) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  readOnly: true,
                                  onTap: () {
                                    addSalesmanChangeNotifier.searchEmployee(context);
                                  },
                                  // onChanged: (value) {
                                  //   addSalesmanChangeNotifier.checkStopSelling(context, value);
                                  // },
                                  controller: addSalesmanChangeNotifier.controllerEmployee,
                                  decoration: new InputDecoration(
                                    labelText: 'Pegawai',
                                    filled: addSalesmanChangeNotifier.isDisablePACIAAOSCONA,
                                    fillColor: Colors.black12,
                                    labelStyle: TextStyle(color: Colors.black),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: addSalesmanChangeNotifier.isEmployeeChanges ? Colors.purple : Colors.grey)),
                                    disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: addSalesmanChangeNotifier.isEmployeeChanges ? Colors.purple : Colors.grey)),
                                    contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                  )
                              ),
                            ),
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height / 47),
                          Visibility(
                            visible: addSalesmanChangeNotifier.isEmployeeHeadModelVisible(),
                            child: IgnorePointer(
                              ignoring: addSalesmanChangeNotifier.isDisablePACIAAOSCONA,
                              child: DropdownButtonFormField<EmployeeHeadModel>(
                                isExpanded: true,
                                autovalidate: addSalesmanChangeNotifier.autoValidate,
                                validator: (e) {
                                  if (e == null && addSalesmanChangeNotifier.isEmployeeHeadModelMandatory() && addSalesmanChangeNotifier.salesmanTypeModelSelected.id == "001") {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                value: addSalesmanChangeNotifier.employeeHeadModelSelected,
                                onChanged: (value) {
                                  addSalesmanChangeNotifier.employeeHeadModelSelected = value;
                                },
                                onTap: () {
                                  FocusManager.instance.primaryFocus.unfocus();
                                },
                                decoration: InputDecoration(
                                  labelText: "Atasan",
                                  filled: addSalesmanChangeNotifier.isDisablePACIAAOSCONA,
                                  fillColor: Colors.black12,
                                  border: OutlineInputBorder(),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: addSalesmanChangeNotifier.isEmployeeHeadModelChanges ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: addSalesmanChangeNotifier.isEmployeeHeadModelChanges ? Colors.purple : Colors.grey)),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                ),
                                items: addSalesmanChangeNotifier.listEmployeeHead.map((value) {
                                  return DropdownMenuItem<EmployeeHeadModel>(
                                    value: value,
                                    child: Text(
                                      "${value.NIK} - ${value.Nama}",
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              );
            }),
        bottomNavigationBar: BottomAppBar(
          elevation: 0.0,
          child: Container(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width / 27,
                vertical: MediaQuery.of(context).size.height / 57),
            child: Provider.of<AddSalesmanChangeNotifier>(context, listen: false).lastKnownState == "PAC" || Provider.of<AddSalesmanChangeNotifier>(context, listen: false).lastKnownState == "IA" || Provider.of<AddSalesmanChangeNotifier>(context, listen: false).lastKnownState == "AOS" || Provider.of<AddSalesmanChangeNotifier>(context, listen: false).lastKnownState == "CONA"
                ? SizedBox()
                : RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)),
                color: myPrimaryColor,
                onPressed: () {
                  if (widget.flag != 0) {
                    Provider.of<AddSalesmanChangeNotifier>(context,listen: false).check(context, widget.flag, widget.index);
                  } else {
                    Provider.of<AddSalesmanChangeNotifier>(context,listen: false).check(context, widget.flag, null);
                  }
                },
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: [Text(widget.flag == 0 ? "SAVE" : "UPDATE")])),
          ),
        ),
      ),
    );
  }

  Future<bool> _onWillPop() async {
    var _provider = Provider.of<AddSalesmanChangeNotifier>(context, listen: false);
    if (widget.flag == 0) {
      if (_provider.salesmanTypeModelSelected != null ||
          _provider.positionModelSelected != null ||
          _provider.controllerEmployee.text != "" ||
          _provider.employeeHeadModelSelected != null) {
        return (await showDialog(
              context: context,
              builder: (myContext) => AlertDialog(
                title: new Text('Warning'),
                content: new Text('Keluar dengan simpan perubahan?'),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      _provider.check(context, widget.flag, null);
                      Navigator.pop(context);
                    },
                    child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                  ),
                  new FlatButton(
                    onPressed: () => Navigator.of(context).pop(true),
                    child: new Text('Tidak'),
                  ),
                ],
              ),
            )) ??
            false;
      } else {
        return true;
      }
    } else {
      if (_provider.salesmanTypeModelSelected.id != _provider.salesmanTypeModelTemp.id ||
          _provider.employeeTemp != _provider.controllerEmployee.text ||
          _provider.positionModelSelected.id != _provider.positionModeltemp.id){
          // _provider.employeeHeadModelSelected.NIK != _provider.employeeHeadModelTemp.NIK) {
        return (await showDialog(
              context: context,
              builder: (myContext) => AlertDialog(
                title: new Text('Warning'),
                content: new Text('Keluar dengan simpan perubahan?'),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      print(widget.index);
                      _provider.check(context, widget.flag, widget.index);
                      Navigator.pop(context);
                    },
                    child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                  ),
                  new FlatButton(
                    onPressed: () => Navigator.of(context).pop(true),
                    child: new Text('Tidak'),
                  ),
                ],
              ),
            )) ??
            false;
      } else {
        return true;
      }
    }
  }
}
