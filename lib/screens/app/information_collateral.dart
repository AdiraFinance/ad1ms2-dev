import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_collatelar_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_lookup_utj_change_notif.dart';
import 'package:ad1ms2_dev/widgets/widget_info_collateral_automotive.dart';
import 'package:ad1ms2_dev/widgets/widget_info_collateral_property.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../lookup_utj.dart';

class InformationCollateral extends StatefulWidget {
  final String flag;
  const InformationCollateral({this.flag});

  @override
  _InformationCollateralState createState() => _InformationCollateralState();
}

class _InformationCollateralState extends State<InformationCollateral> {
  String _objectSelected;

  @override
  void initState() {
    super.initState();
    _objectSelected = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).objectSelected.id;
    // Provider.of<InformationCollateralChangeNotifier>(context,listen: false).addDataListJenisAlamat(context);
  }

  @override
  Widget build(BuildContext context) {
    var _provider = Provider.of<InformationCollateralChangeNotifier>(context,listen: false);
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: _provider.scaffoldKey,
        appBar: AppBar(
          title: Text("Informasi Jaminan/Colla", style: TextStyle(color: Colors.black)),
            centerTitle: true,
            backgroundColor: myPrimaryColor,
            iconTheme: IconThemeData(color: Colors.black),
          actions: [
            Visibility(
              visible: _provider.collateralTypeModel != null
                  ? _provider.collateralTypeModel.id == "001" && (_objectSelected == "001" || _objectSelected == "003")
                    ? false : _provider.collateralTypeModel.id == "002" || _provider.collateralTypeModel.id == "003" ? false : true
                  : false,
              child: FlatButton(
                  onPressed: (){
                    _provider.searchLookupUTJ(context);
                  },
                  child: Text(
                    "Lookup UTJ",
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        letterSpacing: 1.25
                    ),
                  )
              ),
            )
          ],
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width / 27,
              vertical: MediaQuery.of(context).size.height / 57),
          child: Form(
            onWillPop: _provider.collateralTypeModel != null
                ?
            _provider.collateralTypeModel.id == "001"
                ?
            _provider.onBackPress
                :
            _provider.onBackPress
                :
            _provider.onBackPress,
            key: _provider.collateralTypeModel != null
                ?
            _provider.collateralTypeModel.id == "001"
                ?
            _provider.keyFormAuto
                :
            _provider.keyFormProp
                :
            null,
            child: Column(
              children: [
                IgnorePointer(
                  ignoring: _provider.isDisablePACIAAOSCONA || _provider.disableJenisPenawaran,
                  child: TextFormField(
                      readOnly: true,
                      onTap: () {
                        _provider.searchCollateralType(context);
                      },
                      controller: _provider.controllerCollateralType,
                      decoration: InputDecoration(
                          labelText: 'Jenis Jaminan',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))
                      )
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height / 47),
                _provider.collateralTypeModel != null
                    ?
                _provider.collateralTypeModel.id == "001"
                    ?
                WidgetInfoCollateralAutomotive(objectSelected: _objectSelected, flag: widget.flag)
                    :
                _provider.collateralTypeModel.id != "003"
                    ?
                WidgetInfoCollateralProperty()
                    :
                SizedBox()
                    :
                SizedBox(),
              ],
            ),
          ),
        ),
        bottomNavigationBar: BottomAppBar(
          elevation: 0.0,
          child: Padding(
              padding: EdgeInsets.all(8.0),
              child: RaisedButton(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
                  color: myPrimaryColor,
                  onPressed: () {_provider.collateralTypeModel != null
                      ?
                  _provider.collateralTypeModel.id == "001"
                      ?
                  _provider.check(context, 1)
                      :
                  _provider.collateralTypeModel.id == "002"
                      ?
                  _provider.check(context, 2)
                      :
                  _provider.check(context, 3)
                      :
                  null;
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("DONE",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 1.25))
                    ],
                  )
              )
          ),
        ),
      ),
    );
  }
}
