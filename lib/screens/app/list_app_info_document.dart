import 'package:ad1ms2_dev/screens/app/add_info_document.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/add_info_document_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_app_document_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class ListAppInfoDocument extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                accentColor: myPrimaryColor
            ),
            child: Scaffold(
                body: Consumer<InfoDocumentChangeNotifier>(
                    builder: (context, infoDocumentChangeNotifier, _) {
                        return infoDocumentChangeNotifier.listInfoDocument.isEmpty
                            ? Center(
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                    // Image.asset("img/alamat_kosong.png", height: MediaQuery.of(context).size.height / 9,),
                                    // SizedBox(height: MediaQuery.of(context).size.height / 47,),
                                    Text("Tambah Dokumen", style: TextStyle(color: Colors.grey, fontSize: 16),)
                                ],
                            ),
                        )
                            :
                        ListView.builder(
                            padding: EdgeInsets.symmetric(
                                vertical: MediaQuery.of(context).size.height / 47,
                                horizontal: MediaQuery.of(context).size.width / 37),
                            itemBuilder: (context, index) {
                                return InkWell(
                                    onTap: () {
                                        Navigator.push(
                                            context, MaterialPageRoute(
                                            builder: (context) => ChangeNotifierProvider(
                                            create: (context) => AddInfoDocumentChangeNotifier(),
                                                child: AddInfoDocument(
                                                flag: 1,
                                                formMInfoDocument: infoDocumentChangeNotifier.listInfoDocument[index],
                                                index: index
                                             ))));
                                    },
                                    child: Card(
                                        elevation: 3.3,
                                        child: Stack(
                                            children: [
                                                Padding(
                                                    padding: EdgeInsets.all(12.0),
                                                    child: Column(
                                                        children: [
                                                            Row(
                                                                children: [
                                                                    Expanded(
                                                                        child: Text("Dokumen"),
                                                                        flex: 5),
                                                                    Text(" : "),
                                                                    Expanded(
                                                                        child: Text("${infoDocumentChangeNotifier.listInfoDocument[index].documentType.docTypeId} - ${infoDocumentChangeNotifier.listInfoDocument[index].documentType.docTypeName}"),
                                                                        flex: 5)
                                                                ],
                                                            ),
                                                            // SizedBox(
                                                            //     height:
                                                            //     MediaQuery.of(context).size.height /
                                                            //         77),
                                                            // Row(
                                                            //     children: [
                                                            //         Expanded(
                                                            //             child: Text("Nama Berkas"), flex: 5),
                                                            //         Text(" : "),
                                                            //         Expanded(
                                                            //             child: Text(
                                                            //                 infoDocumentChangeNotifier.listInfoDocument[index].documentDetail.fileName),
                                                            //             flex: 5)
                                                            //     ],
                                                            // ),
                                                            SizedBox(height: MediaQuery.of(context).size.height/77),
                                                            Row(
                                                                children: [
                                                                    Expanded(
                                                                        child: Text("Tanggal Unggah"),
                                                                        flex: 5),
                                                                    Text(" : "),
                                                                    Expanded(
                                                                        child: Text(dateFormat.format(DateTime.parse(infoDocumentChangeNotifier.listInfoDocument[index].date))),
                                                                        flex: 5)
                                                                ],
                                                            ),
                                                        ],
                                                    ),
                                                ),
                                                Align(
                                                    alignment: Alignment.topRight,
                                                    child:
                                                    // infoDocumentChangeNotifier.listInfoDocument[index].orderSupportingDocumentID != "NEW" && infoDocumentChangeNotifier.lastKnownState != "IDE" ?
                                                    // SizedBox()
                                                    // :
                                                    IconButton(
                                                        icon: Icon(Icons.delete, color: Colors.red),
                                                        onPressed: () {
                                                            infoDocumentChangeNotifier.deleteListInfoDocument(index);
                                                        }
                                                    ),
                                                ),
                                            ],
                                        )),
                                );
                            },
                            itemCount: infoDocumentChangeNotifier.listInfoDocument.length);
                    },
                ),
                floatingActionButton: FloatingActionButton(
                    onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(
                                builder: (context) => ChangeNotifierProvider(
                                    create: (context) => AddInfoDocumentChangeNotifier(),
                                    child: AddInfoDocument(
                                        flag: 0,
                                        formMInfoDocument: null,
                                        index: null,
                                    ))));
                    },
                    child: Icon(Icons.add, color: Colors.black),
                    backgroundColor: myPrimaryColor,
                ),
            ),
        );
    }
}

