import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/fee_type_model.dart';
import 'package:ad1ms2_dev/models/info_fee_credit_structure_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/add_info_fee_credit_stucture_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class AddInfoFeeCreditStructure extends StatefulWidget {
  final int flag;
  final int index;
  final InfoFeeCreditStructureModel model;
  const AddInfoFeeCreditStructure({this.flag, this.index, this.model});
  @override
  _AddInfoFeeCreditStructureState createState() => _AddInfoFeeCreditStructureState();
}

class _AddInfoFeeCreditStructureState extends State<AddInfoFeeCreditStructure> {

  Future<void> _setValueForEdit;
  @override
  void initState() {
    super.initState();
    // if (widget.flag != 0) {
      _setValueForEdit = Provider.of<AddInfoFeeCreditStructureChangeNotifier>(context, listen: false).setValue(context, widget.model, widget.index, widget.flag);
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        primaryColor: Colors.black,
        accentColor: myPrimaryColor
      ),
      child: Scaffold(
        key: Provider.of<AddInfoFeeCreditStructureChangeNotifier>(context, listen: false).scaffoldKey,
        appBar: AppBar(
          title:
          Text(
              widget.flag == 0
                  ? "Tambah Info Struktur Kredit Biaya"
                  : "Edit Info Struktur Kredit Biaya",
              style: TextStyle(color: Colors.black)
          ),
          centerTitle: true,
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.symmetric(
              vertical: MediaQuery.of(context).size.height/57,
              horizontal: MediaQuery.of(context).size.width/37
          ),
          child:
          // widget.flag == 0
          //     ?
          // Consumer<AddInfoFeeCreditStructureChangeNotifier>(
          //   builder: (context, addInfoCreditStructureChangeNotifier, child){
          //     return Form(
          //       onWillPop: _onWillPop,
          //       key: addInfoCreditStructureChangeNotifier.key,
          //       child: Column(
          //         children: [
          //           DropdownButtonFormField<FeeTypeModel>(
          //               autovalidate:
          //               addInfoCreditStructureChangeNotifier.autoValidate,
          //               validator: (e) {
          //                 if (e == null) {
          //                   return "Silahkan pilih jenis biaya";
          //                 } else {
          //                   return null;
          //                 }
          //               },
          //               value: addInfoCreditStructureChangeNotifier
          //                   .feeTypeSelected,
          //               onChanged: (value) {
          //                 addInfoCreditStructureChangeNotifier
          //                     .feeTypeSelected = value;
          //               },
          //               decoration: InputDecoration(
          //                 labelText: "Jenis Biaya",
          //                 border: OutlineInputBorder(),
          //                 contentPadding:
          //                 EdgeInsets.symmetric(horizontal: 10),
          //               ),
          //               items: addInfoCreditStructureChangeNotifier
          //                   .listFeeType
          //                   .map((value) {
          //                 return DropdownMenuItem<FeeTypeModel>(
          //                   value: value,
          //                   child: Text(
          //                     value.name,
          //                     overflow: TextOverflow.ellipsis,
          //                   ),
          //                 );
          //               }).toList()),
          //           SizedBox(height: MediaQuery.of(context).size.height / 47),
          //           TextFormField(
          //               autovalidate: addInfoCreditStructureChangeNotifier.autoValidate,
          //               validator: (e) {
          //                 if (e.isEmpty) {
          //                   return "Tidak boleh kosong";
          //                 } else {
          //                   return null;
          //                 }
          //               },
          //               inputFormatters: [
          //                 WhitelistingTextInputFormatter.digitsOnly
          //               ],
          //               keyboardType: TextInputType.number,
          //               controller: addInfoCreditStructureChangeNotifier.controllerCashCost,
          //               decoration: new InputDecoration(
          //                   labelText: 'Biaya Tunai',
          //                   labelStyle: TextStyle(color: Colors.black),
          //                   border: OutlineInputBorder(
          //                       borderRadius: BorderRadius.circular(8))
          //               ),
          //               textAlign: TextAlign.end,
          //               onFieldSubmitted: (value) {
          //                 addInfoCreditStructureChangeNotifier.controllerCashCost.text = addInfoCreditStructureChangeNotifier.formatCurrency.formatCurrency(value);
          //                 addInfoCreditStructureChangeNotifier.calculateTotalCost();
          //               },
          //               onTap: () {
          //                 addInfoCreditStructureChangeNotifier.formatting();
          //                 addInfoCreditStructureChangeNotifier.calculateTotalCost();
          //               }
          //           ),
          //           SizedBox(height: MediaQuery.of(context).size.height / 47),
          //           TextFormField(
          //               autovalidate: addInfoCreditStructureChangeNotifier.autoValidate,
          //               validator: (e) {
          //                 if (e.isEmpty) {
          //                   return "Tidak boleh kosong";
          //                 } else {
          //                   return null;
          //                 }
          //               },
          //               keyboardType: TextInputType.number,
          //               controller: addInfoCreditStructureChangeNotifier.controllerCreditCost,
          //               decoration: new InputDecoration(
          //                   labelText: 'Biaya Kredit',
          //                   labelStyle: TextStyle(color: Colors.black),
          //                   border: OutlineInputBorder(
          //                       borderRadius: BorderRadius.circular(8))
          //               ),
          //               textAlign: TextAlign.end,
          //               onFieldSubmitted: (value) {
          //                 addInfoCreditStructureChangeNotifier.controllerCreditCost.text = addInfoCreditStructureChangeNotifier.formatCurrency.formatCurrency(value);
          //                 addInfoCreditStructureChangeNotifier.calculateTotalCost();
          //               },
          //               onTap: () {
          //                 addInfoCreditStructureChangeNotifier.formatting();
          //                 addInfoCreditStructureChangeNotifier.calculateTotalCost();
          //               }
          //           ),
          //           SizedBox(height: MediaQuery.of(context).size.height / 47),
          //           TextFormField(
          //               enabled: false,
          //               controller: addInfoCreditStructureChangeNotifier.controllerTotalCost,
          //               autovalidate: addInfoCreditStructureChangeNotifier.autoValidate,
          //               decoration: new InputDecoration(
          //                   labelText: 'Total Biaya',
          //                   labelStyle: TextStyle(color: Colors.black),
          //                   border: OutlineInputBorder(
          //                       borderRadius: BorderRadius.circular(8))
          //               ),
          //               textAlign: TextAlign.end,
          //               // onFieldSubmitted: (value) {
          //               //   addInfoCreditStructureChangeNotifier.controllerTotalCost.text = addInfoCreditStructureChangeNotifier.formatCurrency.formatCurrency(value);
          //               // },
          //           ),
          //         ],
          //       ),
          //     );
          //   } ,
          // )
          //     :
          FutureBuilder(
              future: _setValueForEdit,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Center(
                      child: CircularProgressIndicator(),
                    );
                }
                return Consumer<AddInfoFeeCreditStructureChangeNotifier>(
                  builder: (context, addInfoCreditStructureChangeNotifier, child){
                    return Form(
                      key: addInfoCreditStructureChangeNotifier.key,
                      onWillPop: _onWillPop,
                      child: Column(
                        children: [
                          Visibility(
                            visible: addInfoCreditStructureChangeNotifier.isFeeTypeSelectedVisible(),
                            child: IgnorePointer(
                              ignoring: addInfoCreditStructureChangeNotifier.isDisablePACIA,
                              child: DropdownButtonFormField<FeeTypeModel>(
                                  autovalidate: addInfoCreditStructureChangeNotifier.autoValidate,
                                  validator: (e) {
                                    if (e == null && addInfoCreditStructureChangeNotifier.isFeeTypeSelectedMandatory()) {
                                      return "Silahkan pilih jenis biaya";
                                    } else {
                                      return null;
                                    }
                                  },
                                  value: addInfoCreditStructureChangeNotifier.feeTypeSelected,
                                  onChanged: (value) {
                                    addInfoCreditStructureChangeNotifier.feeTypeSelected = value;
                                    if(value.id == "01") {
                                      if(addInfoCreditStructureChangeNotifier.jenisPenawaran == "002") {
                                        addInfoCreditStructureChangeNotifier.getDataCL(context);
                                      } else {
                                        addInfoCreditStructureChangeNotifier.getBiayaAdmin(context);
                                      }
                                    }
                                  },
                                  decoration: InputDecoration(
                                    labelText: "Jenis Biaya",
                                    border: OutlineInputBorder(),
                                    filled: addInfoCreditStructureChangeNotifier.isDisablePACIA,
                                    fillColor: Colors.black12,
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: addInfoCreditStructureChangeNotifier.costTypeDakor ? Colors.purple : Colors.grey)),
                                    disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: addInfoCreditStructureChangeNotifier.costTypeDakor ? Colors.purple : Colors.grey)),
                                    contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                  ),
                                  items: addInfoCreditStructureChangeNotifier.listFeeType.map((value) {
                                    return DropdownMenuItem<FeeTypeModel>(
                                      value: value,
                                      child: Text(
                                        value.name,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    );
                                  }).toList()),
                            ),
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height / 47),
                          Visibility(
                            visible: addInfoCreditStructureChangeNotifier.isCashCostVisible(),
                            child: IgnorePointer(
                              ignoring: addInfoCreditStructureChangeNotifier.isDisablePACIA,
                              child: TextFormField(
                                autovalidate: addInfoCreditStructureChangeNotifier.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty && addInfoCreditStructureChangeNotifier.isCashCostMandatory()) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                inputFormatters: [
                                  WhitelistingTextInputFormatter.digitsOnly
                                ],
                                keyboardType: TextInputType.number,
                                controller: addInfoCreditStructureChangeNotifier.controllerCashCost,
                                decoration: new InputDecoration(
                                    labelText: 'Biaya Tunai',
                                    labelStyle: TextStyle(color: Colors.black),
                                    filled: addInfoCreditStructureChangeNotifier.isDisablePACIA,
                                    fillColor: Colors.black12,
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: addInfoCreditStructureChangeNotifier.cashDakor ? Colors.purple : Colors.grey)),
                                    disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: addInfoCreditStructureChangeNotifier.cashDakor ? Colors.purple : Colors.grey)),
                                ),
                                textAlign: TextAlign.end,
                                onTap: () {
                                  // addInfoCreditStructureChangeNotifier.checkValidateAdminFee(addInfoCreditStructureChangeNotifier.controllerCashCost.text);
                                  addInfoCreditStructureChangeNotifier.formatting();
                                  addInfoCreditStructureChangeNotifier.calculateTotalCost();
                                },
                                onFieldSubmitted: (value){
                                  addInfoCreditStructureChangeNotifier.controllerCashCost.text = addInfoCreditStructureChangeNotifier.formatCurrency.formatCurrency(value);
                                  // addInfoCreditStructureChangeNotifier.checkValidateAdminFee(value);
                                  addInfoCreditStructureChangeNotifier.calculateTotalCost();
                                },
                              ),
                            ),
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height / 47),
                          Visibility(
                            visible: addInfoCreditStructureChangeNotifier.isCreditCostVisible(),
                            child: IgnorePointer(
                              ignoring: addInfoCreditStructureChangeNotifier.isDisablePACIA,
                              child: TextFormField(
                                  autovalidate: addInfoCreditStructureChangeNotifier.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && addInfoCreditStructureChangeNotifier.isCreditCostMandatory()) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  keyboardType: TextInputType.number,
                                  controller: addInfoCreditStructureChangeNotifier.controllerCreditCost,
                                  decoration: new InputDecoration(
                                      labelText: 'Biaya Kredit',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: addInfoCreditStructureChangeNotifier.isDisablePACIA,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: addInfoCreditStructureChangeNotifier.creditDakor ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: addInfoCreditStructureChangeNotifier.creditDakor ? Colors.purple : Colors.grey)),
                                  ),
                                  textAlign: TextAlign.end,
                                  onTap: () {
                                    // addInfoCreditStructureChangeNotifier.checkValidateAdminFee(addInfoCreditStructureChangeNotifier.controllerCashCost.text);
                                    addInfoCreditStructureChangeNotifier.formatting();
                                    addInfoCreditStructureChangeNotifier.calculateTotalCost();
                                  },
                                  onFieldSubmitted: (value){
                                    addInfoCreditStructureChangeNotifier.controllerCreditCost.text = addInfoCreditStructureChangeNotifier.formatCurrency.formatCurrency(value);
                                    addInfoCreditStructureChangeNotifier.calculateTotalCost();
                                  },
                              ),
                            ),
                          ),
                          SizedBox(height: MediaQuery.of(context).size.height / 47),
                          Visibility(
                            visible: addInfoCreditStructureChangeNotifier.isTotalCostVisible(),
                            child: TextFormField(
                              autovalidate: addInfoCreditStructureChangeNotifier.autoValidate,
                              validator: (e) {
                                if (e.isEmpty && addInfoCreditStructureChangeNotifier.isTotalCostMandatory()) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              enabled: false,
                              controller: addInfoCreditStructureChangeNotifier.controllerTotalCost,
                              decoration: new InputDecoration(
                                  labelText: 'Total Biaya',
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: true,
                                  fillColor: Colors.black12,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: addInfoCreditStructureChangeNotifier.totalPriceDakor ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: addInfoCreditStructureChangeNotifier.totalPriceDakor ? Colors.purple : Colors.grey)),
                              ),
                              textAlign: TextAlign.end,
                              // onFieldSubmitted: (value) {
                              //   addInfoCreditStructureChangeNotifier.controllerTotalCost.text = addInfoCreditStructureChangeNotifier.formatCurrency.formatCurrency(value);
                              // },
                            ),
                          ),
                        ],
                      ),
                    );
                  } ,
                );
              },
          ),
        ),
        bottomNavigationBar: BottomAppBar(
          child: Container(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width / 27,
                vertical: MediaQuery.of(context).size.height / 57),
            child: RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)),
                color: myPrimaryColor,
                onPressed: () {
                  if (widget.flag != 0) {
                    Provider.of<AddInfoFeeCreditStructureChangeNotifier>(context,listen: false).check(context, widget.flag, widget.index);
                  } else {
                    Provider.of<AddInfoFeeCreditStructureChangeNotifier>(context,listen: false).check(context, widget.flag, null);
                  }
                },
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: [Text(widget.flag == 0 ? "SAVE" : "UPDATE")]
                )
            ),
          ),
        ),
      ),
    );
  }

  Future<bool> _onWillPop() async {
    var _provider =
    Provider.of<AddInfoFeeCreditStructureChangeNotifier>(context, listen: false);
    if (widget.flag == 0) {
      if (_provider.feeTypeSelected != null ||
          _provider.controllerTotalCost.text != "" ||
          _provider.controllerCreditCost.text != "" ||
          _provider.controllerCashCost.text != "") {
        return (await showDialog(
          context: context,
          builder: (myContext) => AlertDialog(
            title: new Text('Warning'),
            content: new Text('Simpan perubahan?'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () {
                  _provider.check(context, widget.flag, null);
                  Navigator.pop(context);
                },
                child: new Text('Ya', style: TextStyle(color: Colors.grey),),
              ),
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: new Text('Tidak'),
              ),
            ],
          ),
        )) ??
            false;
      } else {
        return true;
      }
    } else {
      print("fee type selected: ${_provider.feeTypeSelected.id}");
      print("fee type temp: ${_provider.feeTypeTemp.id}");
      if (_provider.feeTypeSelected.id !=
          _provider.feeTypeTemp.id ||
          _provider.cashCostTemp != _provider.controllerCashCost.text ||
          _provider.creditCostTemp !=
              _provider.controllerCreditCost.text ||
          _provider.totalCostTemp !=
              _provider.controllerTotalCost.text) {
        return (await showDialog(
          context: context,
          builder: (myContext) => AlertDialog(
            title: new Text('Warning'),
            content: new Text('Simpan perubahan?'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () {
                  _provider.check(context, widget.flag, widget.index);
                  Navigator.pop(context);
                },
                child: new Text('Ya', style: TextStyle(color: Colors.grey),),
              ),
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: new Text('Tidak'),
              ),
            ],
          ),
        )) ??
            false;
      } else {
        return true;
      }
    }
  }
}
