import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/info_wmp_change_notifier.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class InfoWMP extends StatefulWidget {
  @override
  _InfoWMPState createState() => _InfoWMPState();
}

class _InfoWMPState extends State<InfoWMP> {

  Future<void> _setPreference;
  @override
  void initState() {
    super.initState();
    _setPreference = Provider.of<InfoWMPChangeNotifier>(context, listen: false).setPreference(context);
    // Provider.of<InfoWMPChangeNotifier>(context,listen: false).getDataFromDashboard(context);
  }
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans"
      ),
      child: FutureBuilder(
        future: _setPreference,
          builder: (context,snapshot){
            if(snapshot.connectionState == ConnectionState.waiting){
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            return Consumer<InfoWMPChangeNotifier>(
              builder: (context, infoWMPChangeNotif, _) {
                return Scaffold(
                  key: infoWMPChangeNotif.scaffoldKey,
                  appBar: AppBar(
                    title: Text("Informasi WMP", style: TextStyle(color: Colors.black)),
                    centerTitle: true,
                    backgroundColor: myPrimaryColor,
                    iconTheme: IconThemeData(color: Colors.black),
                    actions: [
                      Visibility(
                        visible: infoWMPChangeNotif.isGetWMPVisible(),
                        child: FlatButton(
                            onPressed: (){
                              infoWMPChangeNotif.isDisablePACIAAOSCONA ? null : infoWMPChangeNotif.getWMP(context);
                            },
                            child: Text(
                              "GET WMP",
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: 1.25
                              ),
                            )
                        ),
                      )
                    ],
                  ),
                  body: SingleChildScrollView(
                    padding: EdgeInsets.symmetric(
                        vertical: MediaQuery.of(context).size.height / 47,
                        horizontal: MediaQuery.of(context).size.width / 27),
                    child: Column(
                      children: [
                        Card(
                          elevation: 3.3,
                          child: Column(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  color: myPrimaryColor,
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    children: [
                                      Expanded(child: Center(child: Text("No WMP")),flex: 5),
                                      SizedBox(
                                        height: MediaQuery.of(context).size.height/37,
                                        child: VerticalDivider(
                                          width: 2,
                                          color: Colors.black,
                                        ),
                                      ),
                                      Expanded(child: Center(child: Text("Jenis WMP")),flex: 5,)
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(height: MediaQuery.of(context).size.height / 57),
                              Provider.of<InfoWMPChangeNotifier>(context, listen: false).listWMP.isNotEmpty
                                  ?
                              Column(
                                children: _listHeader()
                              )
                                  :
                              Text(
                                "Tidak Ada Data",
                                style: TextStyle(
                                    color: Colors.black12,
                                    fontSize: 20,
                                    fontWeight: FontWeight.w400
                                ),
                              ),
                              SizedBox(height: MediaQuery.of(context).size.height / 57),
                            ],
                          ),
                        ),
                        SizedBox(height: MediaQuery.of(context).size.height / 57),
                        Card(
                          elevation: 3.3,
                          child: Column(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  color: myPrimaryColor,
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    children: [
                                      Expanded(child: Center(child: Text("Jenis Subsidi")),flex: 5),
                                      SizedBox(
                                        height: MediaQuery.of(context).size.height/37,
                                        child: VerticalDivider(
                                          width: 2,
                                          color: Colors.black,
                                        ),
                                      ),
                                      Expanded(child: Center(child: Text("Total Subsidi")),flex: 5,)
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(height: MediaQuery.of(context).size.height / 57),
                              Provider.of<InfoWMPChangeNotifier>(context, listen: false).listWMP.isNotEmpty
                                  ?
                              Column(
                                  children:  _listDetail()
                              )
                                  :
                              Text(
                                "Tidak Ada Data",
                                style: TextStyle(
                                    color: Colors.black12,
                                    fontSize: 20,
                                    fontWeight: FontWeight.w400
                                ),
                              ),
                              SizedBox(height: MediaQuery.of(context).size.height / 57),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  bottomNavigationBar: BottomAppBar(
                    elevation: 0.0,
                    child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(8.0)),
                            color: myPrimaryColor,
                            onPressed: () {
                              Provider.of<InfoWMPChangeNotifier>(context, listen: false).check(context);
                              Navigator.pop(context);
//                        infoCreditStructureTypeInstallcheckmentChangeNotifier.check(context);
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text("DONE",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        letterSpacing: 1.25))
                              ],
                            ))),
                  ),
                );
              },
            );
          }
      ),
    );
  }

  List<Widget> _listHeader(){
    var _provider = Provider.of<InfoWMPChangeNotifier>(context, listen: false);
    List _listHeader = [];
    var _uniqueNoProposal =  _provider.listWMP.map((e) => e.noProposal.trim()).toSet().toList();
    var _uniqueDeskripsiProposal =  _provider.listWMP.map((e) => e.deskripsiProposal.trim()).toSet().toList();
    for(int i=0; i < _uniqueNoProposal.length; i++){
      _listHeader.add(
          {
            "noProposal": _uniqueNoProposal[i],
            "deskripsiProposal":_uniqueDeskripsiProposal[i]
          }
      );
    }
    List<Widget> _temp = [];
    for(int i=0; i < _listHeader.length; i++){
      _temp.add(
          Row(
            children: [
              Expanded(
                  child: Center(
                      child: Text("${_listHeader[i]['noProposal']}",
                          style: TextStyle(
                          color: _provider.listWMP[i].isEditNoProposal ? Colors.purple : Colors.black
                      ))
                  ),
                  flex: 5
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height/37,
                child: VerticalDivider(
                  width: 2,
                  color: Colors.black,
                ),
              ),
              Expanded(child: Center(child: Text("${_listHeader[i]['deskripsiProposal']}")),flex: 5,)
            ],
          )
      );
    }
    return _temp;
  }

  List<Widget> _listDetail(){
    var _provider = Provider.of<InfoWMPChangeNotifier>(context, listen: false);
    List<Widget> _temp = [];
    for(int i=0; i <  _provider.listWMP.length; i++){
      _temp.add(
          Row(
            children: [
              Expanded(child: Center(child: Text("${_provider.listWMP[i].typeSubsidi}-${_provider.listWMP[i].deskripsi}", style: TextStyle(
                  color: _provider.listWMP[i].isEditTypeSubsidi ? Colors.purple : Colors.black),)),flex: 5),
              SizedBox(
                height: MediaQuery.of(context).size.height/37,
                child: VerticalDivider(
                  width: 2,
                  color: Colors.black,
                ),
              ),
              Expanded(child: Center(child: Text("${_provider.listWMP[i].amount}", style: TextStyle(
                  color: _provider.listWMP[i].isEditAmount ? Colors.purple : Colors.black),)),flex: 5,)
            ],
          )
      );
    }
    return _temp;
  }
}
