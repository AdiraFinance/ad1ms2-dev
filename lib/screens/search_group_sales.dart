import 'package:ad1ms2_dev/shared/search_group_sales_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../main.dart';

class SearchGroupSales extends StatefulWidget {
  @override
  _SearchGroupSalesState createState() => _SearchGroupSalesState();
}

class _SearchGroupSalesState extends State<SearchGroupSales> {

  @override
  void initState() {
    super.initState();
    Provider.of<SearchGroupSalesChangeNotifier>(context,listen: false).getGroupSales(context);
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<SearchGroupSalesChangeNotifier>(context,listen: false).scaffoldKey,
        appBar: AppBar(
          title: Consumer<SearchGroupSalesChangeNotifier>(
            builder: (context, searchGroupSalesChangeNotifier, _) {
              return TextFormField(
                controller: searchGroupSalesChangeNotifier.controllerSearch,
                style: TextStyle(color: Colors.black),
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (e) {
                  searchGroupSalesChangeNotifier.searchGrupSales(e);
                },
                onChanged: (e) {
                  searchGroupSalesChangeNotifier.changeAction(e);
                },
                cursorColor: Colors.black,
                decoration: new InputDecoration(
                  hintText: "Cari Grup Sales (minimal 3 karakter)",
                  hintStyle: TextStyle(color: Colors.black),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                ),
                autofocus: true,
                textCapitalization: TextCapitalization.characters,
              );
            },
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            Provider.of<SearchGroupSalesChangeNotifier>(context, listen: true)
                    .showClear
                ? IconButton(
                    icon: Icon(Icons.clear),
                    onPressed: () {
                      Provider.of<SearchGroupSalesChangeNotifier>(context,
                          listen: false).clearSearchTemp();
                      Provider.of<SearchGroupSalesChangeNotifier>(context,
                              listen: false)
                          .controllerSearch
                          .clear();
                      Provider.of<SearchGroupSalesChangeNotifier>(context,
                              listen: false)
                          .changeAction(
                              Provider.of<SearchGroupSalesChangeNotifier>(context,
                                      listen: false)
                                  .controllerSearch
                                  .text);
                    })
                : SizedBox(
                    width: 0.0,
                    height: 0.0,
                  )
          ],
        ),
        body: Consumer<SearchGroupSalesChangeNotifier>(
          builder: (context, searchGroupSales, _) {
            return searchGroupSales.loadData
                ?
            Center(child: CircularProgressIndicator())
                :
            searchGroupSales.listGroupSalesTemp.isNotEmpty
                ?
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount: searchGroupSales.listGroupSalesTemp.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(context, searchGroupSales.listGroupSalesTemp[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchGroupSales.listGroupSalesTemp[index].kode} - "
                              "${searchGroupSales.listGroupSalesTemp[index].deskripsi} ",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            )
                :
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount: searchGroupSales.listGroupSales.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(context, searchGroupSales.listGroupSales[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchGroupSales.listGroupSales[index].kode} - "
                          "${searchGroupSales.listGroupSales[index].deskripsi} ",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          },
        ),
      ),
    );
  }
}
