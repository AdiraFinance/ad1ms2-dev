import 'package:ad1ms2_dev/shared/search_company_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_coverage1_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../main.dart';

class SearchCoverage1 extends StatefulWidget {
    final int flag;
    final String company;
    final String product;
    final String coverage1;
    final String period;
    final String type;
    const SearchCoverage1({this.flag, this.company, this.product, this.coverage1, this.period, this.type});

    @override
    _SearchCoverage1State createState() => _SearchCoverage1State();
}

class _SearchCoverage1State extends State<SearchCoverage1> {

    @override
    void initState() {
    super.initState();
    widget.flag == 1 ?
    Provider.of<SearchCoverage1ChangeNotifier>(context,listen: false).getCoverage(context, widget.company, widget.product)
        :
    Provider.of<SearchCoverage1ChangeNotifier>(context,listen: false).getCoverage2(context, widget.company, widget.product, widget.coverage1, widget.period, widget.type);
  }
    @override
    Widget build(BuildContext context) {
        return Scaffold(
            key: Provider.of<SearchCoverage1ChangeNotifier>(context,listen: false).scaffoldKey,
            appBar: AppBar(
                title: Consumer<SearchCoverage1ChangeNotifier>(
                    builder: (context, searchCoverage1ChangeNotifier, _) {
                        return TextFormField(
                            controller: searchCoverage1ChangeNotifier.controllerSearch,
                            style: TextStyle(color: Colors.black),
                            textInputAction: TextInputAction.search,
                            onFieldSubmitted: (e) {
                                searchCoverage1ChangeNotifier.searchCoverage(e);
                            },
                            onChanged: (e) {
                                searchCoverage1ChangeNotifier.changeAction(e);
                            },
                            cursorColor: Colors.black,
                            decoration: new InputDecoration(
                                hintText: "Cari Coverage ${widget.flag}",
                                hintStyle: TextStyle(color: Colors.black),
                                enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(color: myPrimaryColor),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(color: myPrimaryColor),
                                ),
                            ),
                            autofocus: true,
                        );
                    },
                ),
                backgroundColor: myPrimaryColor,
                iconTheme: IconThemeData(color: Colors.black),
                actions: <Widget>[
                    Provider.of<SearchCoverage1ChangeNotifier>(context, listen: true)
                        .showClear
                        ? IconButton(
                        icon: Icon(Icons.clear),
                        onPressed: () {
                            Provider.of<SearchCoverage1ChangeNotifier>(context,
                                listen: false)
                                .controllerSearch
                                .clear();
                            Provider.of<SearchCoverage1ChangeNotifier>(context,
                                listen: false)
                                .changeAction(Provider.of<SearchCoverage1ChangeNotifier>(
                                context,
                                listen: false)
                                .controllerSearch
                                .text);
                        })
                        : SizedBox(
                        width: 0.0,
                        height: 0.0,
                    )
                ],
            ),
            body: Consumer<SearchCoverage1ChangeNotifier>(
                builder: (context, searchCoverage1ChangeNotifier, _) {
                    return ListView.separated(
                        padding: EdgeInsets.symmetric(
                            vertical: MediaQuery.of(context).size.height / 57,
                            horizontal: MediaQuery.of(context).size.width / 27),
                        itemCount: searchCoverage1ChangeNotifier.listCoverage1Model.length,
                        itemBuilder: (listContext, index) {
                            return InkWell(
                                onTap: () {
                                    Navigator.pop(context,
                                        searchCoverage1ChangeNotifier.listCoverage1Model[index]);
                                },
                                child: Container(
                                    child: Row(
                                        mainAxisSize: MainAxisSize.max,
                                        children: [
                                            Text(
                                                "${searchCoverage1ChangeNotifier.listCoverage1Model[index].KODE} - "
                                                    "${searchCoverage1ChangeNotifier.listCoverage1Model[index].DESKRIPSI} ",
                                                style: TextStyle(fontSize: 16),
                                            )
                                        ],
                                    ),
                                ),
                            );
                        },
                        separatorBuilder: (context, index) {
                            return Divider();
                        },
                    );
                },
            ),
        );
    }
}
