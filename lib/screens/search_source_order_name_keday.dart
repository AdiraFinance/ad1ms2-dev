import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/search_source_order_name_keday_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchSourceOrderNameKeday extends StatefulWidget {
  @override
  _SearchSourceOrderNameKedayState createState() => _SearchSourceOrderNameKedayState();
}

class _SearchSourceOrderNameKedayState extends State<SearchSourceOrderNameKeday> {
  @override
  void initState() {
    super.initState();
    Provider.of<SearchSourceOrderNameKedayChangeNotifier>(context, listen: false).getSourceOrderNameKeday();
  }
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<SearchSourceOrderNameKedayChangeNotifier>(context,listen: false).scaffoldKey,
        appBar: AppBar(
          title: Consumer<SearchSourceOrderNameKedayChangeNotifier>(
            builder: (context, searchSourceOrderNameKedayChangeNotifier, _) {
              return TextFormField(
                controller: searchSourceOrderNameKedayChangeNotifier.controllerSearch,
                style: TextStyle(color: Colors.black),
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (query) {
                  searchSourceOrderNameKedayChangeNotifier.searchSourceOrderNameKeday(query);
                },
                onChanged: (e) {
                  searchSourceOrderNameKedayChangeNotifier.changeAction(e);
                },
                textCapitalization: TextCapitalization.characters,
                cursorColor: Colors.black,
                decoration: new InputDecoration(
                  hintText: "Cari Nama Sumber Order (minimal 3 karakter)",
                  hintStyle: TextStyle(color: Colors.black),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                ),
                autofocus: true,
              );
            },
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            Provider.of<SearchSourceOrderNameKedayChangeNotifier>(context,
                        listen: true)
                    .showClear
                ? IconButton(
                    icon: Icon(Icons.clear),
                    onPressed: () {
                      Provider.of<SearchSourceOrderNameKedayChangeNotifier>(context,
                          listen: false).clearSearchTemp();
                      Provider.of<SearchSourceOrderNameKedayChangeNotifier>(context,
                              listen: false)
                          .controllerSearch
                          .clear();
                      Provider.of<SearchSourceOrderNameKedayChangeNotifier>(context,
                              listen: false)
                          .changeAction(
                              Provider.of<SearchSourceOrderNameKedayChangeNotifier>(
                                      context,
                                      listen: false)
                                  .controllerSearch
                                  .text);
                    })
                : SizedBox(
                    width: 0.0,
                    height: 0.0,
                  )
          ],
        ),
        body: Consumer<SearchSourceOrderNameKedayChangeNotifier>(
          builder: (context, searchSourceOrderNameKedayChangeNotifier, _) {
            return searchSourceOrderNameKedayChangeNotifier.loadData
                ?
            Center(
              child: CircularProgressIndicator(),
            )
                :
            searchSourceOrderNameKedayChangeNotifier.listSourceOrderNameKedayTemp.isNotEmpty
                ?
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount:
              searchSourceOrderNameKedayChangeNotifier.listSourceOrderNameKedayTemp.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(
                        context,
                        searchSourceOrderNameKedayChangeNotifier
                            .listSourceOrderNameKedayTemp[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchSourceOrderNameKedayChangeNotifier.listSourceOrderNameKedayTemp[index].kode} - "
                              "${searchSourceOrderNameKedayChangeNotifier.listSourceOrderNameKedayTemp[index].deskripsi}",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            )
                :
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount:
                  searchSourceOrderNameKedayChangeNotifier.listSourceOrderNameKeday.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(
                        context,
                        searchSourceOrderNameKedayChangeNotifier
                            .listSourceOrderNameKeday[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchSourceOrderNameKedayChangeNotifier
                              .listSourceOrderNameKeday[index].kode} - "
                              "${searchSourceOrderNameKedayChangeNotifier
                              .listSourceOrderNameKeday[index].deskripsi}",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          },
        ),
      ),
    );
  }
}
