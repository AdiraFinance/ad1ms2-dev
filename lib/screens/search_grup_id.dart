import 'package:ad1ms2_dev/shared/search_grup_id_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../main.dart';

class SearchGrupId extends StatefulWidget {
  @override
  _SearchGrupIdState createState() => _SearchGrupIdState();
}

class _SearchGrupIdState extends State<SearchGrupId> {

  @override
  void initState() {
    super.initState();
    Provider.of<SearchGrupIdChangeNotifier>(context,listen: false).getGrupId();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<SearchGrupIdChangeNotifier>(context,listen: false).scaffoldKey,
        appBar: AppBar(
          title: Consumer<SearchGrupIdChangeNotifier>(
            builder: (context, searchGrupIdChangeNotifier, _) {
              return TextFormField(
                controller: searchGrupIdChangeNotifier.controllerSearch,
                style: TextStyle(color: Colors.black),
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (e) {
                  searchGrupIdChangeNotifier.searchGrupId(e);
                },
                onChanged: (e) {
                  searchGrupIdChangeNotifier.changeAction(e);
                },
                cursorColor: Colors.black,
                decoration: new InputDecoration(
                  hintText: "Cari Grup Id (minimal 3 karakter)",
                  hintStyle: TextStyle(color: Colors.black),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                ),
                autofocus: true,
                textCapitalization: TextCapitalization.characters,
              );
            },
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            Provider.of<SearchGrupIdChangeNotifier>(context, listen: true).showClear
                ? IconButton(
                    icon: Icon(Icons.clear),
                    onPressed: () {
                      Provider.of<SearchGrupIdChangeNotifier>(context,
                          listen: false).clearSearchTemp();
                      Provider.of<SearchGrupIdChangeNotifier>(context,
                              listen: false)
                          .controllerSearch
                          .clear();
                      Provider.of<SearchGrupIdChangeNotifier>(context,
                              listen: false)
                          .changeAction(
                              Provider.of<SearchGrupIdChangeNotifier>(context,
                                      listen: false)
                                  .controllerSearch
                                  .text);
                    })
                : SizedBox(
                    width: 0.0,
                    height: 0.0,
                  )
          ],
        ),
        body: Consumer<SearchGrupIdChangeNotifier>(
          builder: (context, searchGrupId, _) {
            return searchGrupId.loadData
                ?
            Center(child: CircularProgressIndicator())
                :
            searchGrupId.listGrupIdTemp.isNotEmpty
                ?
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount: searchGrupId.listGrupIdTemp.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(context, searchGrupId.listGrupIdTemp[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchGrupId.listGrupIdTemp[index].kode} - "
                              "${searchGrupId.listGrupIdTemp[index].deskripsi} ",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            )
                :
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount: searchGrupId.listGrupId.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(context, searchGrupId.listGrupId[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchGrupId.listGrupId[index].kode} - "
                          "${searchGrupId.listGrupId[index].deskripsi} ",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          },
        ),
      ),
    );
  }
}
