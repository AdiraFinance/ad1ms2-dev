import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/task_list_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/task_list_change_notifier/task_list_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class TaskList extends StatefulWidget {
  final BuildContext context;
  const TaskList({this.context});
  @override
  _TaskListState createState() => _TaskListState();
}

class _TaskListState extends State<TaskList> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  void initState() {
    super.initState();
    _getDataTaskList();
  }

  _getDataTaskList() async{
    try{
      if(await Provider.of<TaskListChangeNotifier>(context, listen: false).getTaskList() == "isEmpty"){
        _showSnackBar("Task list tidak ditemukan");
      }
    }
    catch(e){
      _showSnackBar(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<TaskListChangeNotifier>(
      builder: (context, value, child) {
        return Theme(
          data: ThemeData(
            primaryColor: Colors.black,
            accentColor: myPrimaryColor,
            // fontFamily: "NunitoSans",
          ),
          child: Scaffold(
            key: _scaffoldKey,
            body: Column(
              children: [
                Container(
                  padding: EdgeInsets.only(
                    right: MediaQuery.of(context).size.width / 37,
                    left: MediaQuery.of(context).size.width / 37,
                    top:  MediaQuery.of(context).size.height / 47,
                    bottom:  MediaQuery.of(context).size.height / 47
                  ),
                  child: value.showTextFieldSearch
                  ? TextFormField(
                      onFieldSubmitted: (query){
                        value.searchTaskList(query);
                        // if(value.searchTaskList(query) != "") _showSnackBar(value.searchTaskList(query)) ;
                      },
                      keyboardType: TextInputType.text,
                      textCapitalization: TextCapitalization.characters,
                      controller: value.controllerSearch,
                      // textInputAction: TextInputAction.search,
                      style: new TextStyle(color: Colors.black),
                      decoration: new InputDecoration(
                          prefixIcon: Icon(Icons.search, color: Colors.black,),
                          suffixIconConstraints: BoxConstraints(
                            minWidth: 20,
                            minHeight: 20,
                            maxHeight: 25,
                            maxWidth: 30,),
                          suffixIcon: GestureDetector(
                            onTap: () {
                              value.clearSearchTemp();
                            },
                            child: Padding(
                              padding: EdgeInsets.only(right: 8.0),
                              child: Container(
                                child: Icon(Icons.close, color: Colors.grey,),
                              ),
                            ),
                          ),
                          labelText: 'Search task list',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)
                          )
                      ),
                    )
                  : TextFormField(
                    onFieldSubmitted: (query){
                      value.searchTaskList(query);
                      // if(value.searchTaskList(query) != "") _showSnackBar(value.searchTaskList(query)) ;
                    },
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                    controller: value.controllerSearch,
                    // textInputAction: TextInputAction.search,
                    style: new TextStyle(color: Colors.black),
                    decoration: new InputDecoration(
                        prefixIcon: Icon(Icons.search, color: Colors.black,),
                        labelText: 'Search task list',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)
                        )
                    ),
                  ),
                ),
                Expanded(
                  child:
                      value.loadData
                          ?
                      Center(
                        child: CircularProgressIndicator(),
                      )
                          :
                      value.taskList.isEmpty
                          ?
                      Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            // Image.asset("img/alamat_kosong.png", height: MediaQuery.of(context).size.height / 9,),
                            // SizedBox(height: MediaQuery.of(context).size.height / 47,),
                            Text("Tidak Ada Task List", style: TextStyle(color: Colors.grey, fontSize: 16),)
                          ],
                        ),
                      )
                          :
                      value.taskListTemp.isNotEmpty
                          ?
                      GridView.builder(
                        itemCount: value.taskListTemp.length,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            crossAxisSpacing: 6,
                            mainAxisSpacing: 6,
                            childAspectRatio: 0.975
                        ),
                        padding: EdgeInsets.symmetric(
                          // vertical: MediaQuery.of(context).size.height / 87,
                            horizontal: MediaQuery.of(context).size.width / 37),
                        itemBuilder: (BuildContext context, int index) {
                          return InkWell(
                            onTap: (){
                              _chooseForm(widget.context,value.taskListTemp[index]);
                                                       // value.navigateForm(value.taskList[index].LAST_KNOWN_STATE, context);
                              // checkGetCustType(value.taskListTemp[index].LAST_KNOWN_STATE,
                              //     widget.context,
                              //     value.taskListTemp[index].ORDER_NO,
                              //     value.taskListTemp[index].ORDER_DATE.toString(),
                              //     value.taskListTemp[index].CUST_NAME,
                              //     value.taskListTemp[index].CUST_TYPE
                              // );
                              // value.clearSearch();
                            },
                            child: Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8.0),
                                ),
                                color: "${value.taskListTemp[index].LAST_KNOWN_STATE}" == "IDE"
                                    ? primaryColorIDE
                                    : "${value.taskListTemp[index].LAST_KNOWN_STATE}" == "SRE"
                                    ? primaryColorRegulerSurvey
                                    : "${value.taskListTemp[index].LAST_KNOWN_STATE}" == "AOS"
                                    ? primaryColorAOS
                                    : "${value.taskListTemp[index].LAST_KNOWN_STATE}" == "CONA"
                                    ? primaryColorCONA
                                    : "${value.taskListTemp[index].LAST_KNOWN_STATE}" == "SA" || "${value.taskListTemp[index].LAST_KNOWN_STATE}" == "IA" || "${value.taskListTemp[index].LAST_KNOWN_STATE}" == "SARS"
                                    ? primaryColorMiniForm
                                    : "${value.taskListTemp[index].LAST_KNOWN_STATE}" == "PAC"
                                    ? primaryColorPAC
                                    : "${value.taskListTemp[index].LAST_KNOWN_STATE}" == "RSVY"
                                    ? primaryColorResurvey
                                    : "${value.taskListTemp[index].LAST_KNOWN_STATE}" == "DKR"
                                    ? primaryColorDataKoreksi
                                    : primaryColorIDE,
                                child: Container(
                                  padding: EdgeInsets.all(0.0),
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(0.0),
                                        child: Align(
                                            alignment: Alignment.topRight,
                                            child: Text(
                                              "${value.taskListTemp[index].LAST_KNOWN_STATE}" != "CONA" ? "${value.taskListTemp[index].LAST_KNOWN_STATE}" : "CONAPP",
                                              style: TextStyle(
                                                  color: Colors.white70,
                                                  letterSpacing: 0.000001,
                                                  fontSize: 36,
                                                  fontWeight: FontWeight.w900,
                                                  fontFamily: "couture-bld"
                                              ),
                                            )
                                        ),
                                      ),
                                      SizedBox(height: MediaQuery.of(context).size.height / 177),
                                      Container(
                                        margin: EdgeInsets.fromLTRB(13, 8, 13, 0),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              value.taskListTemp[index].ORDER_NO,
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                letterSpacing: 0.2,
                                                fontSize: 14,
                                                fontFamily: "NunitoSans",
                                              ),
                                            ),
                                            SizedBox(height: MediaQuery.of(context).size.height / 177),
                                            Text(
                                              "${value.taskListTemp[index].CUST_NAME}",
                                              style: TextStyle(
                                                fontSize: 13,
                                                letterSpacing: 0.2,
                                                color: Colors.white,
                                                fontFamily: "NunitoSans",
                                              ),
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                            SizedBox(height: MediaQuery.of(context).size.height / 177),
                                            Text(
                                              "${dateFormat2.format(value.taskListTemp[index].MS2PROCESS_DATE)}",
                                              style: TextStyle(
                                                color: Colors.white,
                                                letterSpacing: 0.2,
                                                fontSize: 12,
                                              ),
                                            ),
                                            SizedBox(height: MediaQuery.of(context).size.height / 47),
                                            Container(
                                              decoration: BoxDecoration(
                                                color: value.taskListTemp[index].PRIORITY == "3"
                                                    ?
                                                Color(0xff1B5E20)
                                                    :
                                                value.taskListTemp[index].PRIORITY == "2"
                                                    ?
                                                Color(0xffE65100)
                                                // Color(0xff46be8a)
                                                // Color(0xffec7f00)
                                                // Color(0xffdc3030)
                                                    :
                                                Color(0xffB71C1C),
                                                borderRadius: new BorderRadius.circular(25),
                                                // border: Border.all(color: Colors.white)
                                              ),
                                              child: Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: MediaQuery.of(context).size.width / 37,
                                                    vertical: MediaQuery.of(context).size.height / 177
                                                ),
                                                child: Text(value.taskListTemp[index].PRIORITY == "3" ? "Low Priority" : value.taskListTemp[index].PRIORITY == "2" ? "Medium Priority" : "High Priority",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    letterSpacing: 0.2,
                                                    fontFamily: "NunitoSans",
                                                  ),
                                                ),
                                                // child: Text("Low Priority"
                                                //     "${value.taskList[index].PRIORITY}",
                                                //   style: TextStyle(
                                                //       color: Colors.white
                                                //   ),
                                                // ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                )
                            ),
                          );
                        },
                      )
                          :
                      GridView.builder(
                        itemCount: value.taskList.length,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            crossAxisSpacing: 6,
                            mainAxisSpacing: 6,
                            childAspectRatio: 0.975
                        ),
                        padding: EdgeInsets.symmetric(
                          // vertical: MediaQuery.of(context).size.height / 87,
                            horizontal: MediaQuery.of(context).size.width / 37),
                        itemBuilder: (BuildContext context, int index) {
                          return InkWell(
                            onTap: (){
                              _chooseForm(widget.context,value.taskList[index]);
                              // value.navigateForm(value.taskList[index].LAST_KNOWN_STATE, context);

                              // mark as edit
                              // checkGetCustType(value.taskList[index].LAST_KNOWN_STATE,
                              //     widget.context,
                              //     value.taskList[index].ORDER_NO,
                              //     value.taskList[index].ORDER_DATE.toString(),
                              //     value.taskList[index].CUST_NAME,
                              //     value.taskList[index].CUST_TYPE
                              // );
                              // value.clearSearch();
                            },
                            child: Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8.0),
                                ),
                                color: "${value.taskList[index].LAST_KNOWN_STATE}" == "IDE"
                                    ? primaryColorIDE
                                    : "${value.taskList[index].LAST_KNOWN_STATE}" == "SRE"
                                    ? primaryColorRegulerSurvey
                                    : "${value.taskList[index].LAST_KNOWN_STATE}" == "AOS"
                                    ? primaryColorAOS
                                    : "${value.taskList[index].LAST_KNOWN_STATE}" == "CONA"
                                    ? primaryColorCONA
                                    : "${value.taskList[index].LAST_KNOWN_STATE}" == "SA" || "${value.taskList[index].LAST_KNOWN_STATE}" == "IA" || "${value.taskList[index].LAST_KNOWN_STATE}" == "SARS"
                                    ? primaryColorMiniForm
                                    : "${value.taskList[index].LAST_KNOWN_STATE}" == "PAC"
                                    ? primaryColorPAC
                                    : "${value.taskList[index].LAST_KNOWN_STATE}" == "RSVY"
                                    ? primaryColorResurvey
                                    : "${value.taskList[index].LAST_KNOWN_STATE}" == "DKR"
                                    ? primaryColorDataKoreksi
                                    : primaryColorIDE,
                                child: Container(
                                  padding: EdgeInsets.all(0.0),
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(0.0),
                                        child: Align(
                                            alignment: Alignment.topRight,
                                            child: Text(
                                              "${value.taskList[index].LAST_KNOWN_STATE}" != "CONA" ? "${value.taskList[index].LAST_KNOWN_STATE}" : "CONAPP",
                                              style: TextStyle(
                                                  color: Colors.white70,
                                                  letterSpacing: 0.000001,
                                                  fontSize: 36,
                                                  fontWeight: FontWeight.w900,
                                                  fontFamily: "couture-bld"
                                              ),
                                            )
                                        ),
                                      ),
                                      SizedBox(height: MediaQuery.of(context).size.height / 177),
                                      Container(
                                        margin: EdgeInsets.fromLTRB(13, 8, 13, 0),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              value.taskList[index].ORDER_NO,
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                letterSpacing: 0.2,
                                                fontSize: 14,
                                                fontFamily: "NunitoSans",
                                              ),
                                            ),
                                            SizedBox(height: MediaQuery.of(context).size.height / 177),
                                            Text(
                                              "${value.taskList[index].CUST_NAME}",
                                              style: TextStyle(
                                                fontSize: 13,
                                                letterSpacing: 0.2,
                                                color: Colors.white,
                                                fontFamily: "NunitoSans",
                                              ),
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                            SizedBox(height: MediaQuery.of(context).size.height / 177),
                                            Text(
                                              "${dateFormat2.format(value.taskList[index].ORDER_DATE)}",
                                              style: TextStyle(
                                                color: Colors.white,
                                                letterSpacing: 0.2,
                                                fontSize: 12,
                                              ),
                                            ),
                                            SizedBox(height: MediaQuery.of(context).size.height / 47),
                                            Container(
                                              decoration: BoxDecoration(
                                                color: value.taskList[index].PRIORITY == "3"
                                                    ?
                                                Color(0xff1B5E20)
                                                    :
                                                value.taskList[index].PRIORITY == "2"
                                                    ?
                                                Color(0xffE65100)
                                                // Color(0xff46be8a)
                                                // Color(0xffec7f00)
                                                // Color(0xffdc3030)
                                                    :
                                                Color(0xffB71C1C),
                                                borderRadius: new BorderRadius.circular(25),
                                                // border: Border.all(color: Colors.white)
                                              ),
                                              child: Padding(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: MediaQuery.of(context).size.width / 37,
                                                    vertical: MediaQuery.of(context).size.height / 177
                                                ),
                                                child: Text(value.taskList[index].PRIORITY == "3" ? "Low Priority" : value.taskList[index].PRIORITY == "2" ? "Medium Priority" : "High Priority",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    letterSpacing: 0.2,
                                                    fontFamily: "NunitoSans",
                                                  ),
                                                ),
                                                // child: Text("Low Priority"
                                                //     "${value.taskList[index].PRIORITY}",
                                                //   style: TextStyle(
                                                //       color: Colors.white
                                                //   ),
                                                // ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                )
                            ),
                          );
                        },
                      )
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void _showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  void _chooseForm(BuildContext context,TaskListModel model) async{
    try{
      await checkGetCustType(
          model.LAST_KNOWN_STATE,
          context,
          model.ORDER_NO,
          model.ORDER_DATE.toString(),
          model.CUST_NAME,
          model.CUST_TYPE,
          model.IDX,
          model.OID,
          model.STATUS_AORO,
          model.JENIS_PENAWARAN,
          model.NO_REFERENSI,
          model.OPSI_MULTIDISBURSE,false
      );
    }
    catch(e){
      _showSnackBar(e.toString());
    }
  }
}
