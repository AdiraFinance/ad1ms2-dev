import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/search_source_order_name_spg_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchSourceOrderNameSPG extends StatefulWidget {
  @override
  _SearchSourceOrderNameSPGState createState() => _SearchSourceOrderNameSPGState();
}

class _SearchSourceOrderNameSPGState extends State<SearchSourceOrderNameSPG> {
  @override
  void initState() {
    super.initState();
    Provider.of<SearchSourceOrderNameSPGChangeNotifier>(context, listen: false).getSourceOrderNameSPG();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<SearchSourceOrderNameSPGChangeNotifier>(context,listen: false).scaffoldKey,
        appBar: AppBar(
          title: Consumer<SearchSourceOrderNameSPGChangeNotifier>(
            builder: (context, searchSourceOrderNameSPGChangeNotifier, _) {
              return TextFormField(
                controller: searchSourceOrderNameSPGChangeNotifier.controllerSearch,
                style: TextStyle(color: Colors.black),
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (query) {
                  // if(query.length >= 3) {
                    searchSourceOrderNameSPGChangeNotifier.searchSourceOrderNameSPG(query);
                  // } else {
                  //   searchSourceOrderNameSPGChangeNotifier.showSnackBar("Input minimal 3 karakter");
                  // }
                },
                onChanged: (e) {
                  searchSourceOrderNameSPGChangeNotifier.changeAction(e);
                },
                textCapitalization: TextCapitalization.characters,
                cursorColor: Colors.black,
                decoration: new InputDecoration(
                  hintText: "Cari Nama Sumber Order (minimal 3 karakter)",
                  hintStyle: TextStyle(color: Colors.black),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                ),
                autofocus: true,
              );
            },
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            Provider.of<SearchSourceOrderNameSPGChangeNotifier>(context,
                        listen: true)
                    .showClear
                ? IconButton(
                    icon: Icon(Icons.clear),
                    onPressed: () {
                      Provider.of<SearchSourceOrderNameSPGChangeNotifier>(context, listen: false).clearSearchTemp();
                      Provider.of<SearchSourceOrderNameSPGChangeNotifier>(context, listen: false).controllerSearch.clear();
                      Provider.of<SearchSourceOrderNameSPGChangeNotifier>(context, listen: false).changeAction(Provider.of<SearchSourceOrderNameSPGChangeNotifier>(context, listen: false).controllerSearch.text);
                    })
                : SizedBox(
                    width: 0.0,
                    height: 0.0,
                  )
          ],
        ),
        body: Consumer<SearchSourceOrderNameSPGChangeNotifier>(
          builder: (context, searchSourceOrderNameSPGChangeNotifier, _) {
            return searchSourceOrderNameSPGChangeNotifier.loadData
                ?
            Center(
              child: CircularProgressIndicator(),
            )
                :
            searchSourceOrderNameSPGChangeNotifier.listSourceOrderNameSPGTemp.isNotEmpty
                ?
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount:
              searchSourceOrderNameSPGChangeNotifier.listSourceOrderNameSPGTemp.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(
                        context,
                        searchSourceOrderNameSPGChangeNotifier
                            .listSourceOrderNameSPGTemp[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchSourceOrderNameSPGChangeNotifier.listSourceOrderNameSPGTemp[index].kode} - "
                              "${searchSourceOrderNameSPGChangeNotifier.listSourceOrderNameSPGTemp[index].deskripsi}",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            )
                :
            searchSourceOrderNameSPGChangeNotifier.listSourceOrderNameSPG.isNotEmpty
                ?
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount:
                  searchSourceOrderNameSPGChangeNotifier.listSourceOrderNameSPG.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(
                        context,
                        searchSourceOrderNameSPGChangeNotifier
                            .listSourceOrderNameSPG[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchSourceOrderNameSPGChangeNotifier
                              .listSourceOrderNameSPG[index].kode} - "
                              "${searchSourceOrderNameSPGChangeNotifier
                              .listSourceOrderNameSPG[index].deskripsi}",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            )
                :
            Center(
              child: Text("Data tidak ditemukan"),
            );
          },
        ),
      ),
    );
  }
}
