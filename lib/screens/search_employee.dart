import 'package:ad1ms2_dev/shared/search_employee_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../main.dart';

class SearchEmployee extends StatefulWidget {
  final String salesType;
  final String positionId;
  final String position;
  const SearchEmployee({this.salesType, this.positionId, this.position});

  @override
  _SearchEmployeeState createState() => _SearchEmployeeState();
}

class _SearchEmployeeState extends State<SearchEmployee> {
  @override
  void initState() {
    super.initState();
    Provider.of<SearchEmployeeChangeNotifier>(context, listen: false).getEmployee(context, widget.salesType, widget.positionId, widget.position);
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          fontFamily: "NunitoSans",
          primaryColor: Colors.black,
          primarySwatch: primaryOrange,
          accentColor: myPrimaryColor
      ),
      child: Scaffold(
        key: Provider.of<SearchEmployeeChangeNotifier>(context, listen: false).scaffoldKey,
        appBar: AppBar(
          title: Consumer<SearchEmployeeChangeNotifier>(
            builder: (context, searchEmployeeChangeNotifier, _) {
              return TextFormField(
                controller: searchEmployeeChangeNotifier.controllerSearch,
                style: TextStyle(color: Colors.black),
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (e) {
                  searchEmployeeChangeNotifier.searchEmployee(e);
                },
                onChanged: (e) {
                  searchEmployeeChangeNotifier.changeAction(e);
                },
                cursorColor: Colors.black,
                textCapitalization: TextCapitalization.sentences,
                decoration: new InputDecoration(
                  hintText: "Cari Pegawai (minimal 3 karakter)",
                  hintStyle: TextStyle(color: Colors.black),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                ),
                autofocus: true,
              );
            },
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            Provider.of<SearchEmployeeChangeNotifier>(context, listen: true)
                    .showClear
                ? IconButton(
                    icon: Icon(Icons.clear),
                    onPressed: () {
                      Provider.of<SearchEmployeeChangeNotifier>(context, listen: false).clearSearchTemp();
                      Provider.of<SearchEmployeeChangeNotifier>(context, listen: false).controllerSearch.clear();
                      Provider.of<SearchEmployeeChangeNotifier>(context, listen: false).changeAction(
                          Provider.of<SearchEmployeeChangeNotifier>(context, listen: false).controllerSearch.text
                      );
                    })
                : SizedBox(
                    width: 0.0,
                    height: 0.0,
                  )
          ],
        ),
        body: Consumer<SearchEmployeeChangeNotifier>(
          builder: (context, searchEmployeeChangeNotifier, _) {
            return searchEmployeeChangeNotifier.loadData
                ?
            Center(
              child: CircularProgressIndicator(),
            )
                :
            searchEmployeeChangeNotifier.listEmployeeModelTemp.isNotEmpty
                ?
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount: searchEmployeeChangeNotifier.listEmployeeModelTemp.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(context,
                        searchEmployeeChangeNotifier.listEmployeeModelTemp[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchEmployeeChangeNotifier.listEmployeeModelTemp[index].NIK} - "
                          "${searchEmployeeChangeNotifier.listEmployeeModelTemp[index].Nama} ",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            )
                :
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount: searchEmployeeChangeNotifier.listEmployeeModel.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(context,
                        searchEmployeeChangeNotifier.listEmployeeModel[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchEmployeeChangeNotifier.listEmployeeModel[index].NIK} - "
                              "${searchEmployeeChangeNotifier.listEmployeeModel[index].Nama} ",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          },
        ),
      ),
    );
  }
}
