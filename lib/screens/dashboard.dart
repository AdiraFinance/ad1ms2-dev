import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/home.dart';
import 'package:ad1ms2_dev/screens/login.dart';
import 'package:ad1ms2_dev/screens/profile.dart';
import 'package:ad1ms2_dev/screens/survey/result_survey.dart';
import 'package:ad1ms2_dev/screens/task_list/task_list.dart';
import 'package:ad1ms2_dev/services/connectivity_service.dart';
import 'package:ad1ms2_dev/shared/dashboard/dashboard_change_notif.dart';
import 'package:ad1ms2_dev/shared/login_change_notifier.dart';
import 'package:ad1ms2_dev/screens/order_tracking/order_tracking.dart';
import 'package:ad1ms2_dev/shared/login_change_notifier.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'dedup/dedup_menu.dart';

import 'login.dart';

class Dashboard extends StatefulWidget {
  final int flag;
  const Dashboard({this.flag});

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  int _selectedIndex;
  DbHelper _dbHelper = DbHelper();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  Future<void> _getData;
  MyConnectivity _connectivity = MyConnectivity();

  @override
  void initState() {
    super.initState();
    setState(() {
      _selectedIndex = 0;
    });
    _getToken();
    if(widget.flag == null) {
      _getData = Provider.of<DashboardChangeNotif>(context, listen: false).getData(context);
    }
    if(widget.flag != null){
      setState(() {
        _selectedIndex = widget.flag;
      });
    }
  }

  @override
  void dispose() {
    debugPrint("DISPOSE JALAN");
    _connectivity.disposeStream();
    super.dispose();
  }

  _getToken() async{
    SharedPreferences _preference = await SharedPreferences.getInstance();
    token = _preference.getString("token");
  }

  _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> _listWidget = [
      HomePage(context: context),
      DedupMenu(),
      TaskList(context: context),
      Text("Inbox"),
      OrderTracking(),
      Text("Attendance"),
      Profile()
    ];
    return Theme(
      data: ThemeData(primaryColor: myPrimaryColor, fontFamily: "NunitoSans", accentColor: myPrimaryColor, primarySwatch: primaryOrange),
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: myPrimaryColor,
          title: TitleAppBarDashboard(selectedIndex: _selectedIndex),
          centerTitle: true,
          actions: [
            _selectedIndex == 2
            ? IconButton(icon: Icon(Icons.filter_list), onPressed: (){
              // _moreOption(context);
            })
            : SizedBox(width: 0.0,height: 0.0,)
          ],
        ),
        body: FutureBuilder(
          future: _getData,
          builder: (context,snapshot){
            if(snapshot.connectionState == ConnectionState.waiting){
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    CircularProgressIndicator(),
                    SizedBox(height: 12),
                    Text("Loading get parameterize ...")
                  ],
                )
              );
            }
            return Container(
              child: _listWidget.elementAt(_selectedIndex),
            );
          },
        ),
        drawer: Drawer(
          child: Container(
            color: Colors.white,
            child: Column(
              children: <Widget>[
                DrawerHeader(
                  padding: EdgeInsets.only(top: 0, bottom: 0),
                  margin: EdgeInsets.only(top: 0, bottom: 0),
                  child: Image.asset(
                    "img/logo_adira.png",
                    scale: 1.17,
                  ),
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                ),
                Flexible(
                    child: ListWidgetDrawerMenu(
                        onItemTap: _onItemTapped,
                        selectedIndex: _selectedIndex)),
                ListTile(
                  leading: Icon(Icons.input, color: Colors.red),
                  title: Text('Logout', style: TextStyle(color: Colors.red)),
                  onTap: () {
                    _logout();
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating,
        backgroundColor: snackbarColor, duration: Duration(seconds: 2))
    );
  }

  // _setFilter(int indexSelectedItem, int indexVoid){
  //   String _sortById = '';
  //   String _sortByDate = '';
  //   String _sortByName = '';
  //   String _sortByPrice = '';
  //
  //   if(indexSelectedItem == 0){
  //     _sortById = 'true';
  //   }
  //   else if(indexSelectedItem == 1){
  //     _sortById = 'false';
  //   }
  //   else if(indexSelectedItem == 2){
  //     _sortByDate = 'true';
  //   }
  //   else if(indexSelectedItem == 3){
  //     _sortByDate = 'false';
  //   }
  //   else if(indexSelectedItem == 4){
  //     _sortByName = 'true';
  //   }
  //   else if(indexSelectedItem == 5){
  //     _sortByName = 'false';
  //   }
  //   else if(indexSelectedItem == 6){
  //     _sortByPrice = 'true';
  //   }
  //   else if(indexSelectedItem == 7){
  //     _sortByPrice = 'false';
  //   }
  //   if(indexVoid == -1){
  //     indexVoid = 0;
  //   }
  //
  //   var _dataFilter = {
  //     "sortId":_sortById,
  //     "sortDate":_sortByDate,
  //     "sortName":_sortByName,
  //     "sortPrice":_sortByPrice,
  //     "void": indexVoid,
  //     "filterVal" : indexSelectedItem,
  //   };
  //
  //   if(indexSelectedItem < 0){
  //      Navigator.pop(context);
  //   } else{
  //     widget.dataFilter(_dataFilter);
  //     Navigator.pop(context);
  //   }
  // }

  _logout() async{
    SharedPreferences _preference = await SharedPreferences.getInstance();
    await _preference.clear();
    var _providerLogin = Provider.of<LoginChangeNotifier>(context, listen:false);
    _providerLogin.controllerPassword.clear();
    _providerLogin.valueRandomText.clear();
    _providerLogin.autoValidate = false;
    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => LoginPage()), (Route <dynamic> route) => false);
  }

}

class TitleAppBarDashboard extends StatelessWidget {
  final int selectedIndex;
  const TitleAppBarDashboard({this.selectedIndex});

  @override
  Widget build(BuildContext context) {
    if (selectedIndex == 0) {
      return Image.asset(
        "img/logo_adira.png",
        width: 70,
        height: 35,
      );
    } else if (selectedIndex == 1) {
      return Text("Dedup");
    } else if (selectedIndex == 2) {
      return Text("Task List");
    } else if (selectedIndex == 3) {
      return Text("Inbox");
    } else if (selectedIndex == 4) {
      return Text("Order Tracking");
    } else if (selectedIndex == 5) {
      return Text("Cuti & Lembur");
    } else {
      return Text("Profile");
    }
  }
}

class ListWidgetDrawerMenu extends StatelessWidget {
  final ValueChanged<int> onItemTap;
  final int selectedIndex;
  const ListWidgetDrawerMenu({this.onItemTap, this.selectedIndex});
  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.zero,
      children: <Widget>[
        ListTile(
          leading: Icon(Icons.home),
          title: Text('Home'),
          onTap: () {
            onItemTap(0);
          },
          selected: selectedIndex == 0,
        ),
        ListTile(
          leading: Icon(Icons.assignment),
          title: Text('Dedup'),
          onTap: () {
            onItemTap(1);
          },
          selected: selectedIndex == 1,
        ),
        ListTile(
          leading: Icon(Icons.list),
          title: Text('Task List'),
          onTap: () {
            onItemTap(2);
          },
          selected: selectedIndex == 2,
        ),
        ListTile(
          leading: Icon(Icons.mail),
          title: Text('Inbox'),
          onTap: () {
            onItemTap(3);
          },
          selected: selectedIndex == 3,
        ),
        ListTile(
          leading: Icon(Icons.explore),
          title: Text('Order Tracking'),
          onTap: () {
            onItemTap(4);
          },
          selected: selectedIndex == 4,
        ),
        ListTile(
          leading: Icon(Icons.notification_important),
          title: Text('Attendance'),
          onTap: () {
            onItemTap(5);
          },
          selected: selectedIndex == 5,
        ),
        ListTile(
          leading: Icon(Icons.account_circle),
          title: Text('Profile'),
          onTap: () {
            onItemTap(6);
          },
          selected: selectedIndex == 6,
        ),
      ],
    );
  }
}
