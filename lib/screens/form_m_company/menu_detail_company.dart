import 'package:ad1ms2_dev/shared/form_m_add_address_company_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_parent_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_pendapatan_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_rincian_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';
import 'form_m_alamat.dart';
import 'form_m_rincian.dart';

class MenuDetailCompany extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        var _providerDetailCompany = Provider.of<FormMCompanyRincianChangeNotifier>(context, listen: true);
        var _providerAddressCompany = Provider.of<FormMCompanyAlamatChangeNotifier>(context, listen: true);

        return Padding(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width/37,
                vertical: MediaQuery.of(context).size.height/57,
            ),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    InkWell(
                        onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FormMCompanyRincianProvider()
                                )
                            );
                        },
                        child: Card(
                            elevation: 3.3,
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text(
                                            "Rincian Lembaga"),
                                        _providerDetailCompany.flag
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
                    _providerDetailCompany.autoValidate
                        ? Container(
                        margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                        child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
                    )
                        : SizedBox(height: MediaQuery.of(context).size.height / 87),

                    SizedBox(height: MediaQuery.of(context).size.height / 87),

                    InkWell(
                        onTap: () {
                            Provider.of<FormMAddAddressCompanyChangeNotifier>(context, listen: false).getDataFromDashboard(context);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FormMCompanyAlamat()));
                        },
                        child: Card(
                            elevation: 3.3,
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text(
                                            "Alamat Lembaga"),
                                        _providerAddressCompany.flag
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
                    _providerAddressCompany.autoValidate
                        ? Container(
                        margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                        child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
                    )
                        : SizedBox(height: MediaQuery.of(context).size.height / 87),
                ],
            ),
        );
    }

    Future<void> setNextState(BuildContext context,int index) async{
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        var _providerDetailCompany = Provider.of<FormMCompanyRincianChangeNotifier>(context, listen: true);
        var _providerAddressCompany = Provider.of<FormMCompanyAlamatChangeNotifier>(context, listen: true);
        var _providerCompanyPendapatan = Provider.of<FormMCompanyPendapatanChangeNotifier>(context, listen: true);
        debugPrint("SET_INFO_CUST_COM");
        _providerDetailCompany.setDataSQLite(context);
        _providerAddressCompany.setDataSQLite(context);

        if(_preferences.getString("last_known_state") == "IDE" && index != null){
            if(index != 0){
                Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).isMenuCompany = true;
                Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).selectedIndex += 1;
                await _providerCompanyPendapatan.setDataFromSQLite(context, index);
                _providerAddressCompany.flag = true;
                _providerDetailCompany.flag = true;
            }
            else{
                Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).selectedIndex = 0;
            }
        }
    }
}
