import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/add_pemegang_saham_pribadi_change_notif.dart';
import 'package:ad1ms2_dev/shared/pemegang_saham_pribadi_change_notifier.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'add_pemegang_saham_pribadi.dart';

class ListPemegangSahamPribadi extends StatefulWidget {
  @override
  _ListPemegangSahamPribadiState createState() => _ListPemegangSahamPribadiState();
}

class _ListPemegangSahamPribadiState extends State<ListPemegangSahamPribadi> {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        primaryColor: Colors.black,
        primarySwatch: primaryOrange,
        accentColor: myPrimaryColor,
      ),
      child: Scaffold(
        appBar: AppBar(
          title:
          Text("List Pemegang Saham Pribadi", style: TextStyle(color: Colors.black)),
          centerTitle: true,
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        body: Consumer<PemegangSahamPribadiChangeNotifier>(
          builder: (context, _provider, _) {
            return Form(
              onWillPop: _provider.onBackPress,
              child: _provider.listPemegangSahamPribadi.isEmpty
                  ?
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Tambah Pemegang Saham Pribadi", style: TextStyle(color: Colors.grey, fontSize: 16),)
                  ],
                ),
              )
                  :
              ListView.builder(
                padding: EdgeInsets.symmetric(
                    horizontal: MediaQuery.of(context).size.width / 27,
                    vertical: MediaQuery.of(context).size.height / 47),
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      Provider.of<AddPemegangSahamPribadiChangeNotifier>(context, listen: false).clearDataPemegangSahamPribadi();
                      Navigator.push(context, MaterialPageRoute(
                            builder: (context) => AddPemegangSahamPribadi(
                              flag: 1,
                              model: _provider.listPemegangSahamPribadi[index],
                              index: index,
                            )
                          )
                      );
                    },
                    child: Card(
                      shape: _provider.listPemegangSahamPribadi[index].isEdit
                          ?
                      RoundedRectangleBorder(
                          side: BorderSide(color: Colors.purple, width: 2),
                          borderRadius: BorderRadius.all(Radius.circular(4))
                      )
                          :
                      null,
                      elevation: 3.3,
                      child: Padding(
                        padding: const EdgeInsets.all(13.0),
                        child: Stack(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Expanded(
                                        child: Text("Status Hubungan"),
                                        flex: 5
                                    ),
                                    Text(" : "),
                                    Expanded(
                                        child: Text("${_provider.listPemegangSahamPribadi[index].relationshipStatusSelected.PARA_FAMILY_TYPE_NAME}"),
                                        flex: 5
                                    )
                                  ],
                                ),
                                SizedBox(height: MediaQuery.of(context).size.height / 57),
                                Row(
                                  children: [
                                    Expanded(
                                        child: Text("Tipe Identitas"),
                                        flex: 5
                                    ),
                                    Text(" : "),
                                    Expanded(
                                        child: Text("${_provider.listPemegangSahamPribadi[index].typeIdentitySelected.name}"),
                                        flex: 5
                                    )
                                  ],
                                ),
                                SizedBox(height: MediaQuery.of(context).size.height / 57),
                                Row(
                                  children: [
                                    Expanded(
                                        child: Text("No. Identitas"),
                                        flex: 5
                                    ),
                                    Text(" : "),
                                    Expanded(
                                        child: Text("${_provider.listPemegangSahamPribadi[index].identityNumber}"),
                                        flex: 5
                                    )
                                  ],
                                ),
                                SizedBox(height: MediaQuery.of(context).size.height / 57),
                                Row(
                                  children: [
                                    Expanded(
                                        child: Text("Nama Lengkap Identitas"),
                                        flex: 5
                                    ),
                                    Text(" : "),
                                    Expanded(
                                        child: Text("${_provider.listPemegangSahamPribadi[index].fullNameIdentity}"),
                                        flex: 5
                                    )
                                  ],
                                ),
                                SizedBox(height: MediaQuery.of(context).size.height / 57),
                              ],
                            ),
                            Align(
                              alignment: Alignment.topRight,
                              child: IconButton(
                                  icon: Icon(
                                    Icons.delete,
                                    color: Colors.red,
                                  ),
                                  onPressed: () {
                                    _provider.deleteListPemegangSahamPribadi(context, index);
                                  }),
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                },
                itemCount: _provider.listPemegangSahamPribadi.length,
              ),
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Provider.of<AddPemegangSahamPribadiChangeNotifier>(context, listen: false).clearDataPemegangSahamPribadi();
            Navigator.push(context, MaterialPageRoute(
                  builder: (context) => AddPemegangSahamPribadi(
                    flag: 0,
                    index: null,
                    model: null,
                  ),
                ));
          },
          child: Icon(
            Icons.add,
          ),
          backgroundColor: myPrimaryColor,
        ),
      ),
    );
  }
}
