import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/pemegang_saham_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/add_pemegang_saham_lembaga_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_company_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import 'list_pemegang_saham_kelembagaan_alamat.dart';

class AddPemegangSahamLembaga extends StatefulWidget {
  final int flag;
  final int index;
  final PemegangSahamLembagaModel model;

  AddPemegangSahamLembaga({this.flag, this.index, this.model});

  @override
  _AddPemegangSahamLembagaState createState() => _AddPemegangSahamLembagaState();
}

class _AddPemegangSahamLembagaState extends State<AddPemegangSahamLembaga> {
  Future<void> _setValueForEdit;
  @override
  void initState() {
    super.initState();
    _setValueForEdit = Provider.of<AddPemegangSahamLembagaChangeNotif>(context, listen: false).setValue(widget.flag, widget.index, widget.model, context);
  }

  @override
  Widget build(BuildContext context) {
    var _data = Provider.of<FormMAddAddressCompanyChangeNotifier>(context, listen: false);
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        primaryColor: Colors.black,
        primarySwatch: primaryOrange,
      ),
      child: Consumer<AddPemegangSahamLembagaChangeNotif>(
        builder: (context, formMCompanyPemegangSahamKelembagaanChangeNotif, _) {
          return Scaffold(
            key: formMCompanyPemegangSahamKelembagaanChangeNotif.scaffoldKey,
            appBar: AppBar(
              title: Text(widget.flag == 0 ? "Tambah Pemegang Saham Lembaga" : "Edit Pemegang Saham Lembaga", style: TextStyle(color: Colors.black)),
              centerTitle: true,
              backgroundColor: myPrimaryColor,
              iconTheme: IconThemeData(color: Colors.black),
            ),
            body: FutureBuilder(
                future: _setValueForEdit,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  return Form(
                    key: formMCompanyPemegangSahamKelembagaanChangeNotif.keyForm,
                    onWillPop: _onWillPop,
                    child: ListView(
                      padding: EdgeInsets.symmetric(
                          vertical: MediaQuery.of(context).size.height/57,
                          horizontal: MediaQuery.of(context).size.width/37
                      ),
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Visibility(
                              visible: formMCompanyPemegangSahamKelembagaanChangeNotif.isCompanyTypeVisible(),
                              child: DropdownButtonFormField<TypeInstitutionModel>(
                                autovalidate: formMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate,
                                validator: (e) {
                                  if (e == null && formMCompanyPemegangSahamKelembagaanChangeNotif.isCompanyTypeMandatory()) {
                                    return "Silahkan pilih jenis lembaga";
                                  } else {
                                    return null;
                                  }
                                },
                                value: formMCompanyPemegangSahamKelembagaanChangeNotif.typeInstitutionSelected,
                                onChanged: (value) {
                                  formMCompanyPemegangSahamKelembagaanChangeNotif.typeInstitutionSelected = value;
                                },
                                onTap: () {
                                  FocusManager.instance.primaryFocus.unfocus();
                                },
                                decoration: InputDecoration(
                                  labelText: "Jenis Lembaga",
                                  border: OutlineInputBorder(),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyPemegangSahamKelembagaanChangeNotif.companyTypeDakor ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyPemegangSahamKelembagaanChangeNotif.companyTypeDakor ? Colors.purple : Colors.grey)),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                ),
                                items: formMCompanyPemegangSahamKelembagaanChangeNotif.listTypeInstitution.map((value) {
                                  return DropdownMenuItem<TypeInstitutionModel>(
                                    value: value,
                                    child: Text(
                                      value.PARA_NAME,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                            Visibility(visible: formMCompanyPemegangSahamKelembagaanChangeNotif.isCompanyTypeVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: formMCompanyPemegangSahamKelembagaanChangeNotif.isCompanyNameVisible(),
                              child: TextFormField(
                                autovalidate: formMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty && formMCompanyPemegangSahamKelembagaanChangeNotif.isCompanyNameMandatory()) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                controller: formMCompanyPemegangSahamKelembagaanChangeNotif.controllerInstitutionName,
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                  labelText: 'Nama Lembaga',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyPemegangSahamKelembagaanChangeNotif.companyNameDakor ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyPemegangSahamKelembagaanChangeNotif.companyNameDakor ? Colors.purple : Colors.grey)),
                                ),
                                keyboardType: TextInputType.text,
                                textCapitalization: TextCapitalization.characters,
                              ),
                            ),
                            Visibility(visible: formMCompanyPemegangSahamKelembagaanChangeNotif.isCompanyNameVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: formMCompanyPemegangSahamKelembagaanChangeNotif.isEstablishDateVisible(),
                              child: FocusScope(
                                  node: FocusScopeNode(),
                                  child: TextFormField(
                                    autovalidate: formMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty && formMCompanyPemegangSahamKelembagaanChangeNotif.isEstablishDateMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    controller: formMCompanyPemegangSahamKelembagaanChangeNotif.controllerDateOfEstablishment,
                                    style: TextStyle(color: Colors.black),
                                    decoration: InputDecoration(
                                      labelText: 'Tanggal Pendirian',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMCompanyPemegangSahamKelembagaanChangeNotif.establishDateDakor ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMCompanyPemegangSahamKelembagaanChangeNotif.establishDateDakor ? Colors.purple : Colors.grey)),
                                    ),
                                    onTap: () {
                                      FocusManager.instance.primaryFocus.unfocus();
                                      formMCompanyPemegangSahamKelembagaanChangeNotif.selectDateOfEstablishment(context);
                                    },
                                  )
                              ),
                            ),
                            Visibility(visible: formMCompanyPemegangSahamKelembagaanChangeNotif.isEstablishDateVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: formMCompanyPemegangSahamKelembagaanChangeNotif.isNpwpVisible(),
                              child: TextFormField(
                                autovalidate: formMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty && formMCompanyPemegangSahamKelembagaanChangeNotif.isNpwpMandatory()) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                onFieldSubmitted: (val){
                                  formMCompanyPemegangSahamKelembagaanChangeNotif.validateNameOrNPWP(context, "NPWP");
                                },
                                onTap: (){
                                  formMCompanyPemegangSahamKelembagaanChangeNotif.validateNameOrNPWP(context, "NPWP");
                                },
                                inputFormatters: [
                                  WhitelistingTextInputFormatter.digitsOnly,
                                  FilteringTextInputFormatter.allow(RegExp('[0-9]')),
                                  LengthLimitingTextInputFormatter(15)
                                ],
                                controller: formMCompanyPemegangSahamKelembagaanChangeNotif.controllerNPWP,
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                  labelText: 'NPWP',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyPemegangSahamKelembagaanChangeNotif.npwpDakor ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyPemegangSahamKelembagaanChangeNotif.npwpDakor ? Colors.purple : Colors.grey)),
                                ),
                                keyboardType: TextInputType.number,
                              ),
                            ),
                            Visibility(visible: formMCompanyPemegangSahamKelembagaanChangeNotif.isNpwpVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: formMCompanyPemegangSahamKelembagaanChangeNotif.isShareVisible(),
                              child: TextFormField(
                                autovalidate: formMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty && formMCompanyPemegangSahamKelembagaanChangeNotif.isShareMandatory()) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                inputFormatters: [
                                  WhitelistingTextInputFormatter.digitsOnly,
                                  // LengthLimitingTextInputFormatter(10),
                                ],
                                onChanged: (value){
                                  if(value.isNotEmpty){
                                    formMCompanyPemegangSahamKelembagaanChangeNotif.limitInput(value);
                                  }
                                },
                                onFieldSubmitted: (value) {
                                  formMCompanyPemegangSahamKelembagaanChangeNotif.limitInput(value);
                                },
                                onTap: (){
                                  formMCompanyPemegangSahamKelembagaanChangeNotif.validateNameOrNPWP(context, "NPWP");
                                },
                                controller: formMCompanyPemegangSahamKelembagaanChangeNotif.controllerPercentShare,
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                  labelText: '% Share',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyPemegangSahamKelembagaanChangeNotif.shareDakor ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyPemegangSahamKelembagaanChangeNotif.shareDakor ? Colors.purple : Colors.grey)),
                                ),
                                keyboardType: TextInputType.number,
                              ),
                            ),
                            Visibility(visible: formMCompanyPemegangSahamKelembagaanChangeNotif.isShareVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: _data.setAddressTypeShow(3),
                              child: TextFormField(
                                autovalidate: formMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty && _data.setAddressTypeMandatory(3)) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                controller: formMCompanyPemegangSahamKelembagaanChangeNotif.controllerAddressType,
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    labelText: 'Jenis Alamat',
                                    labelStyle: TextStyle(color: Colors.black),
                                    filled: true,
                                    fillColor: Colors.black12,
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8))),
                                enabled: false,
                              ),
                            ),
                            Visibility(visible: _data.setAddressTypeShow(3),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: _data.setAddressShow(3),
                              child: TextFormField(
                                autovalidate: formMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty && _data.setAddressMandatory(3)) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                // onTap: () {
                                //   Navigator.push(
                                //       context,
                                //       MaterialPageRoute(
                                //           builder: (context) => ListPemegangSahamKelembagaanAddress()));
                                // },
                                controller: formMCompanyPemegangSahamKelembagaanChangeNotif.controllerAddress,
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    labelText: 'Alamat',
                                    labelStyle: TextStyle(color: Colors.black),
                                    filled: true,
                                    fillColor: Colors.black12,
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8))),
                                enabled: false,
                                // readOnly: true,
                              ),
                            ),
                            Visibility(visible: _data.setAddressShow(3),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: _data.setRTShow(3) || _data.setRWShow(3),
                              child: Row(
                                children: [
                                  Visibility(
                                    visible: _data.setRTShow(3),
                                    child: Expanded(
                                      flex: 5,
                                      child: TextFormField(
                                        autovalidate: formMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty && _data.setRTMandatory(3)) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        controller: formMCompanyPemegangSahamKelembagaanChangeNotif.controllerRT,
                                        style: TextStyle(color: Colors.black),
                                        decoration: InputDecoration(
                                            filled: true,
                                            fillColor: Colors.black12,
                                            labelText: 'RT',
                                            labelStyle: TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(8))),
                                        enabled: false,
                                      ),
                                    ),
                                  ),
                                  Visibility(visible: _data.setRTShow(3),child: SizedBox(width: 8)),
                                  Visibility(
                                    visible: _data.setRWShow(3),
                                    child: Expanded(
                                        flex: 5,
                                        child: TextFormField(
                                          autovalidate: formMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate,
                                          validator: (e) {
                                            if (e.isEmpty && _data.setRWMandatory(3)) {
                                              return "Tidak boleh kosong";
                                            } else {
                                              return null;
                                            }
                                          },
                                          controller: formMCompanyPemegangSahamKelembagaanChangeNotif.controllerRW,
                                          style: TextStyle(color: Colors.black),
                                          decoration: InputDecoration(
                                              filled: true,
                                              fillColor: Colors.black12,
                                              labelText: 'RW',
                                              labelStyle: TextStyle(color: Colors.black),
                                              border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(8))),
                                          enabled: false,
                                        )),
                                  )
                                ],
                              ),
                            ),
                            Visibility(visible: _data.setRTShow(3) || _data.setRWShow(3), child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: _data.setKelurahanShow(3),
                              child: FocusScope(
                                node: FocusScopeNode(),
                                child: TextFormField(
                                  autovalidate: formMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && _data.setKelurahanMandatory(3)) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  controller: formMCompanyPemegangSahamKelembagaanChangeNotif.controllerKelurahan,
                                  style: TextStyle(color: Colors.black),
                                  decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.black12,
                                      labelText: 'Kelurahan',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8))),
                                  enabled: false,
                                ),
                              ),
                            ),
                            Visibility(visible: _data.setKelurahanShow(3), child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: _data.setKecamatanShow(3),
                              child: TextFormField(
                                autovalidate: formMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty && _data.setKecamatanMandatory(3)) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                controller: formMCompanyPemegangSahamKelembagaanChangeNotif.controllerKecamatan,
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    filled: true,
                                    fillColor: Colors.black12,
                                    labelText: 'Kecamatan',
                                    labelStyle: TextStyle(color: Colors.black),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8))),
                                enabled: false,
                              ),
                            ),
                            Visibility(visible: _data.setKecamatanShow(3),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: _data.setKabKotShow(3),
                              child: TextFormField(
                                autovalidate: formMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty && _data.setKabKotMandatory(3)) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                controller: formMCompanyPemegangSahamKelembagaanChangeNotif.controllerKota,
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    filled: true,
                                    fillColor: Colors.black12,
                                    labelText: 'Kabupaten/Kota',
                                    labelStyle: TextStyle(color: Colors.black),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8))),
                                enabled: false,
                              ),
                            ),
                            Visibility(visible: _data.setKabKotShow(3),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: _data.setProvinsiShow(3) || _data.setKodePosShow(3),
                              child: Row(
                                children: [
                                  Visibility(
                                    visible: _data.setProvinsiShow(3),
                                    child: Expanded(
                                      flex: 7,
                                      child: TextFormField(
                                        autovalidate: formMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty && _data.setProvinsiMandatory(3)) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        controller: formMCompanyPemegangSahamKelembagaanChangeNotif.controllerProvinsi,
                                        style: TextStyle(color: Colors.black),
                                        decoration: InputDecoration(
                                            filled: true,
                                            fillColor: Colors.black12,
                                            labelText: 'Provinsi',
                                            labelStyle: TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(8))),
                                        enabled: false,
                                      ),
                                    ),
                                  ),
                                  Visibility(visible: _data.setProvinsiShow(3),child: SizedBox(width: 8)),
                                  Visibility(
                                    visible: _data.setKodePosShow(3),
                                    child: Expanded(
                                      flex: 3,
                                      child: TextFormField(
                                        autovalidate: formMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty && _data.setKodePosMandatory(3)) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        controller:
                                        formMCompanyPemegangSahamKelembagaanChangeNotif.controllerPostalCode,
                                        style: TextStyle(color: Colors.black),
                                        decoration: InputDecoration(
                                            filled: true,
                                            fillColor: Colors.black12,
                                            labelText: 'Kode Pos',
                                            labelStyle: TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(8))),
                                        enabled: false,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Visibility(visible: _data.setProvinsiShow(3) || _data.setKodePosShow(3), child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: _data.setTelp1AreaShow(3) || _data.setTelp1Show(3),
                              child: Row(
                                children: [
                                  Visibility(
                                    visible: _data.setTelp1AreaShow(3),
                                    child: Expanded(
                                      flex: 4,
                                      child: TextFormField(
                                        autovalidate: formMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty && _data.setTelp1AreaMandatory(3)) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        keyboardType: TextInputType.number,
                                        inputFormatters: [
                                          WhitelistingTextInputFormatter.digitsOnly,
                                          LengthLimitingTextInputFormatter(10),
                                        ],
                                        controller: formMCompanyPemegangSahamKelembagaanChangeNotif.controllerTelepon1Area,
                                        style: TextStyle(color: Colors.black),
                                        decoration: InputDecoration(
                                            filled: true,
                                            fillColor: Colors.black12,
                                            labelText: 'Telepon 1 (Area)',
                                            labelStyle: TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(8))),
                                        enabled: false,
                                      ),
                                    ),
                                  ),
                                  Visibility(visible: _data.setTelp1AreaShow(3),child: SizedBox(width: MediaQuery.of(context).size.width / 37)),
                                  Visibility(
                                    visible: _data.setTelp1Show(3),
                                    child: Expanded(
                                      flex: 6,
                                      child: TextFormField(
                                        autovalidate: formMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty && _data.setTelp1Mandatory(3)) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        keyboardType: TextInputType.number,
                                        inputFormatters: [
                                          WhitelistingTextInputFormatter.digitsOnly,
                                          LengthLimitingTextInputFormatter(10),
                                        ],
                                        controller: formMCompanyPemegangSahamKelembagaanChangeNotif.controllerTelepon1,
                                        style: TextStyle(color: Colors.black),
                                        decoration: InputDecoration(
                                            filled: true,
                                            fillColor: Colors.black12,
                                            labelText: 'Telepon 1',
                                            labelStyle: TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(8))),
                                        enabled: false,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Visibility(visible: _data.setTelp1AreaShow(3) || _data.setTelp1Show(3),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: _data.setTelp2AreaShow(3) || _data.setTelp2Show(3),
                              child: Row(
                                children: [
                                  Visibility(
                                    visible: _data.setTelp2AreaShow(3),
                                    child: Expanded(
                                      flex: 4,
                                      child: TextFormField(
                                        autovalidate: formMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty && _data.setTelp2AreaMandatory(3)) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        keyboardType: TextInputType.number,
                                        inputFormatters: [
                                          WhitelistingTextInputFormatter.digitsOnly,
                                          LengthLimitingTextInputFormatter(10),
                                        ],
                                        controller: formMCompanyPemegangSahamKelembagaanChangeNotif.controllerTelepon2Area,
                                        style: TextStyle(color: Colors.black),
                                        decoration: InputDecoration(
                                            filled: true,
                                            fillColor: Colors.black12,
                                            labelText: 'Telepon 2 (Area)',
                                            labelStyle: TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(8))),
                                        enabled: false,
                                      ),
                                    ),
                                  ),
                                  Visibility(visible: _data.setTelp2AreaShow(3),child: SizedBox(width: MediaQuery.of(context).size.width / 37)),
                                  Visibility(
                                    visible: _data.setTelp2Show(3),
                                    child: Expanded(
                                      flex: 6,
                                      child: TextFormField(
                                        autovalidate: formMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty && _data.setTelp2Mandatory(3)) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        keyboardType: TextInputType.number,
                                        inputFormatters: [
                                          WhitelistingTextInputFormatter.digitsOnly,
                                          LengthLimitingTextInputFormatter(10),
                                        ],
                                        controller: formMCompanyPemegangSahamKelembagaanChangeNotif.controllerTelepon2,
                                        style: TextStyle(color: Colors.black),
                                        decoration: InputDecoration(
                                            filled: true,
                                            fillColor: Colors.black12,
                                            labelText: 'Telepon 2',
                                            labelStyle: TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(8))),
                                        enabled: false,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Visibility(visible: _data.setTelp2AreaShow(3) || _data.setTelp2Show(3), child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: _data.setFaxAreaShow(3) || _data.setFaxShow(3),
                              child: Row(
                                children: [
                                  Visibility(
                                    visible: _data.setFaxAreaShow(3),
                                    child: Expanded(
                                      flex: 4,
                                      child: TextFormField(
                                        autovalidate: formMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty && _data.setFaxAreaMandatory(3)) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        keyboardType: TextInputType.number,
                                        inputFormatters: [
                                          WhitelistingTextInputFormatter.digitsOnly,
                                          LengthLimitingTextInputFormatter(10),
                                        ],
                                        controller: formMCompanyPemegangSahamKelembagaanChangeNotif.controllerFaxArea,
                                        style: TextStyle(color: Colors.black),
                                        decoration: InputDecoration(
                                            filled: true,
                                            fillColor: Colors.black12,
                                            labelText: 'Fax (Area)',
                                            labelStyle: TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(8))),
                                        enabled: false,
                                      ),
                                    ),
                                  ),
                                  Visibility(visible: _data.setFaxAreaShow(3), child: SizedBox(width: MediaQuery.of(context).size.width / 37)),
                                  Visibility(
                                    visible: _data.setFaxShow(3),
                                    child: Expanded(
                                      flex: 6,
                                      child: TextFormField(
                                        autovalidate: formMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty && _data.setFaxMandatory(3)) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        keyboardType: TextInputType.number,
                                        inputFormatters: [
                                          WhitelistingTextInputFormatter.digitsOnly,
                                          LengthLimitingTextInputFormatter(10),
                                        ],
                                        controller: formMCompanyPemegangSahamKelembagaanChangeNotif.controllerFax,
                                        style: TextStyle(color: Colors.black),
                                        decoration: InputDecoration(
                                            filled: true,
                                            fillColor: Colors.black12,
                                            labelText: 'Fax',
                                            labelStyle: TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(8))),
                                        enabled: false,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  );
                }),
            bottomNavigationBar: BottomAppBar(
              elevation: 0.0,
              child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Consumer<AddPemegangSahamLembagaChangeNotif>(
                    builder: (context, formMCompanyPemegangSahamKelembagaanChangeNotif, _) {
                      return Row(
                        children: [
                          Expanded(
                            child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: new BorderRadius.circular(8.0)),
                                color: myPrimaryColor,
                                onPressed: () {
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => ListPemegangSahamKelembagaanAddress()));
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text("EDIT ALAMAT",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500,
                                            letterSpacing: 1.25))
                                  ],
                                )
                            ),
                          ),
                          SizedBox(width: MediaQuery.of(context).size.width / 37),
                          Expanded(
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8.0)),
                              color: myPrimaryColor,
                              onPressed: () {
                                formMCompanyPemegangSahamKelembagaanChangeNotif.check(context, widget.flag, widget.index);
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(widget.flag == 0 ? "SAVE" : "UPDATE",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500,
                                          letterSpacing: 1.25))
                                ],
                              )))
                        ],
                      );
                    },
                  )),
            ),
          );
        },
      )
    );
  }

  Future<bool> _onWillPop() async {
    var _provider = Provider.of<AddPemegangSahamLembagaChangeNotif>(context, listen: false);
    if (widget.flag == 0){
      if (_provider.typeInstitutionSelected != null || _provider.controllerInstitutionName.text != null ||
          _provider.controllerNPWP.text.isNotEmpty || _provider.controllerDateOfEstablishment.text.isNotEmpty ||
          _provider.controllerPercentShare.text.isNotEmpty) {
        return (await showDialog(context: context,
          builder: (myContext) => AlertDialog(
            title: Text('Warning'),
            content: Text('Keluar dengan simpan perubahan?'),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  _provider.check(context, widget.flag, widget.index);
                  Navigator.pop(context);
                },
                child: Text('Ya', style: TextStyle(color: Colors.grey)),
              ),
              FlatButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: Text('Tidak'),
              ),
            ],
          ),
        )) ??
            false;
      } else {
        return true;
      }
    }
    else {
      if (widget.model.typeInstitutionModel.PARA_ID != _provider.typeInstitutionSelected.PARA_ID ||
          widget.model.institutionName != _provider.controllerInstitutionName.text ||
          widget.model.initialDateForDateOfEstablishment != _provider.controllerDateOfEstablishment.text ||
          widget.model.npwp != _provider.controllerNPWP.text ||
          widget.model.percentShare != _provider.controllerPercentShare.text){
        return (await showDialog(
          context: context,
          builder: (myContext) => AlertDialog(
            title: Text('Warning'),
            content: Text('Keluar dengan simpan perubahan?'),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  _provider.check(context, widget.flag, widget.index);
                  Navigator.pop(context);
                },
                child: Text('Ya', style: TextStyle(color: Colors.grey)),
              ),
              FlatButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: Text('Tidak'),
              ),
            ],
          ),
        )) ??
            false;
      } else {
        return true;
      }
    }
  }

}
