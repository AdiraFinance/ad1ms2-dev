import 'dart:io';

import 'package:ad1ms2_dev/models/form_m_company_manajemen_pic_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_company_manajemen_pic_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class FormMCompanyManajemenPICProvider extends StatefulWidget {
  @override
  _FormMCompanyManajemenPICProviderState createState() => _FormMCompanyManajemenPICProviderState();
}

class _FormMCompanyManajemenPICProviderState extends State<FormMCompanyManajemenPICProvider> {
  Future<void> _setPreference;

  @override
  void initState() {
    super.initState();
    _setPreference = Provider.of<FormMCompanyManajemenPICChangeNotifier>(context, listen: false).setPreference(context);
  }

  @override
  Widget build(BuildContext context) {
    var _data = Provider.of<FormMCompanyManajemenPICChangeNotifier>(context, listen: false);
    _data.getDataFromDashboard(context);
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        primaryColor: Colors.black,
        accentColor: myPrimaryColor,
        primarySwatch: primaryOrange,
      ),
      child: FutureBuilder(
        future: _setPreference,
        builder: (context, snapshot) {
          return Consumer<FormMCompanyManajemenPICChangeNotifier>(
            builder: (context, formMCompanyManajemenPICChangeNotif, _) {
              return Scaffold(
                appBar: AppBar(
                  title:
                  Text("Informasi Manajemen PIC", style: TextStyle(color: Colors.black)),
                  centerTitle: true,
                  backgroundColor: myPrimaryColor,
                  iconTheme: IconThemeData(color: Colors.black),
                ),
                body: Form(
                  key: formMCompanyManajemenPICChangeNotif.keyForm,
                  onWillPop: formMCompanyManajemenPICChangeNotif.onBackPress,
                  child: ListView(
                    padding: EdgeInsets.symmetric(
                        vertical: MediaQuery.of(context).size.height/57,
                        horizontal: MediaQuery.of(context).size.width/37
                    ),
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Visibility(
                            visible: _data.isIdentityTypeVisible(),
                            child: DropdownButtonFormField<TypeIdentityModel>(
                              autovalidate: formMCompanyManajemenPICChangeNotif.autoValidate,
                              validator: (e) {
                                if (e == null && _data.isIdentityTypeMandatory()) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              value: formMCompanyManajemenPICChangeNotif.typeIdentitySelected,
                              onChanged: (value) {
                                formMCompanyManajemenPICChangeNotif.typeIdentitySelected = value;
                              },
                              onTap: () {
                                FocusManager.instance.primaryFocus.unfocus();
                              },
                              decoration: InputDecoration(
                                labelText: "Jenis Identitas",
                                border: OutlineInputBorder(),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: formMCompanyManajemenPICChangeNotif.identityTypeDakor ? Colors.purple : Colors.grey)),
                                disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: formMCompanyManajemenPICChangeNotif.identityTypeDakor ? Colors.purple : Colors.grey)),
                                contentPadding: EdgeInsets.symmetric(horizontal: 10),
                              ),
                              items: formMCompanyManajemenPICChangeNotif.listTypeIdentity.map((value) {
                                return DropdownMenuItem<TypeIdentityModel>(
                                  value: value,
                                  child: Text(
                                    value.text,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                );
                              }).toList(),
                            ),
                          ),
                          Visibility(visible: _data.isIdentityTypeVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          Visibility(
                            visible: _data.isIdentityNoVisible(),
                            child: TextFormField(
                              autovalidate: formMCompanyManajemenPICChangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty && _data.isIdentityNoMandatory()) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              enabled: (formMCompanyManajemenPICChangeNotif.lastKnownState == "PAC" || formMCompanyManajemenPICChangeNotif.lastKnownState == "IA") ? false : true,
                              inputFormatters: formMCompanyManajemenPICChangeNotif.typeIdentitySelected != null
                                  ? formMCompanyManajemenPICChangeNotif.typeIdentitySelected.id != "03"
                                  ? [WhitelistingTextInputFormatter.digitsOnly, LengthLimitingTextInputFormatter(16),]
                                  : null
                                  : [WhitelistingTextInputFormatter.digitsOnly],
                              controller: formMCompanyManajemenPICChangeNotif.controllerIdentityNumber,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                labelText: 'No Identitas',
                                labelStyle: TextStyle(color: Colors.black),
                                filled: (formMCompanyManajemenPICChangeNotif.lastKnownState == "PAC" || formMCompanyManajemenPICChangeNotif.lastKnownState == "IA") ? true : false,
                                fillColor: Colors.black12,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8)
                                ),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: formMCompanyManajemenPICChangeNotif.identityNoDakor ? Colors.purple : Colors.grey)),
                                disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: formMCompanyManajemenPICChangeNotif.identityNoDakor ? Colors.purple : Colors.grey)),
                                contentPadding: EdgeInsets.symmetric(horizontal: 10),
                              ),
                              keyboardType: formMCompanyManajemenPICChangeNotif.typeIdentitySelected != null
                                  ? formMCompanyManajemenPICChangeNotif.typeIdentitySelected.id != "03"
                                  ? TextInputType.number
                                  : TextInputType.text
                                  : TextInputType.number,
                              textCapitalization: TextCapitalization.characters,
                            ),
                          ),
                          Visibility(visible: _data.isIdentityNoVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          Visibility(
                            visible: _data.isFullnameIdVisible(),
                            child: TextFormField(
                              autovalidate: formMCompanyManajemenPICChangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty && _data.isFullnameIdMandatory()) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              enabled: (formMCompanyManajemenPICChangeNotif.lastKnownState == "PAC" || formMCompanyManajemenPICChangeNotif.lastKnownState == "IA") ? false : true,
                              controller: formMCompanyManajemenPICChangeNotif.controllerFullNameIdentity,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                labelText: 'Nama Lengkap Sesuai Identitas',
                                labelStyle: TextStyle(color: Colors.black),
                                filled: (formMCompanyManajemenPICChangeNotif.lastKnownState == "PAC" || formMCompanyManajemenPICChangeNotif.lastKnownState == "IA") ? true : false,
                                fillColor: Colors.black12,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8)
                                ),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: formMCompanyManajemenPICChangeNotif.fullnameIdDakor ? Colors.purple : Colors.grey)),
                                disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: formMCompanyManajemenPICChangeNotif.fullnameIdDakor ? Colors.purple : Colors.grey)),
                              ),
                              keyboardType: TextInputType.text,
                              textCapitalization: TextCapitalization.characters,
                              inputFormatters: [
                                FilteringTextInputFormatter.allow(RegExp('[a-zA-Z  \`]')),
                              ],
                            ),
                          ),
                          Visibility(visible: _data.isFullnameIdVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          Visibility(
                            visible: _data.isFullnameVisible(),
                            child: TextFormField(
                              autovalidate: formMCompanyManajemenPICChangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty && _data.isFullnameMandatory()) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              enabled: (formMCompanyManajemenPICChangeNotif.lastKnownState == "PAC" || formMCompanyManajemenPICChangeNotif.lastKnownState == "IA") ? false : true,
                              controller: formMCompanyManajemenPICChangeNotif.controllerFullName,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                labelText: 'Nama Lengkap',
                                labelStyle: TextStyle(color: Colors.black),
                                filled: (formMCompanyManajemenPICChangeNotif.lastKnownState == "PAC" || formMCompanyManajemenPICChangeNotif.lastKnownState == "IA") ? true : false,
                                fillColor: Colors.black12,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8)
                                ),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: formMCompanyManajemenPICChangeNotif.fullnameDakor ? Colors.purple : Colors.grey)),
                                disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: formMCompanyManajemenPICChangeNotif.fullnameDakor ? Colors.purple : Colors.grey)),
                              ),
                              keyboardType: TextInputType.text,
                              textCapitalization: TextCapitalization.characters,
                              inputFormatters: [
                                FilteringTextInputFormatter.allow(RegExp('[a-zA-Z  \`]')),
                              ],
                            ),
                          ),
                          Visibility(visible: _data.isFullnameVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          // TextFormField(
                          //   autovalidate: formMCompanyManajemenPICChangeNotif.autoValidate,
                          //   validator: (e) {
                          //     if (e.isEmpty) {
                          //       return "Tidak boleh kosong";
                          //     } else {
                          //       return null;
                          //     }
                          //   },
                          //   controller: formMCompanyManajemenPICChangeNotif.controllerAlias,
                          //   style: new TextStyle(color: Colors.black),
                          //   decoration: new InputDecoration(
                          //       labelText: 'Alias',
                          //       labelStyle: TextStyle(color: Colors.black),
                          //       border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                          //   keyboardType: TextInputType.text,
                          //   textCapitalization: TextCapitalization.characters,
                          // ),
                          // SizedBox(height: MediaQuery.of(context).size.height / 47),
                          // TextFormField(
                          //   autovalidate: formMCompanyManajemenPICChangeNotif.autoValidate,
                          //   validator: (e) {
                          //     if (e.isEmpty) {
                          //       return "Tidak boleh kosong";
                          //     } else {
                          //       return null;
                          //     }
                          //   },
                          //   controller: formMCompanyManajemenPICChangeNotif.controllerDegree,
                          //   style: new TextStyle(color: Colors.black),
                          //   decoration: new InputDecoration(
                          //       labelText: 'Gelar',
                          //       labelStyle: TextStyle(color: Colors.black),
                          //       border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                          //   keyboardType: TextInputType.text,
                          //   textCapitalization: TextCapitalization.characters,
                          // ),
                          // SizedBox(height: MediaQuery.of(context).size.height / 47),
                          Visibility(
                            visible: _data.isDateOfBirthVisible(),
                            child: FocusScope(
                                node: FocusScopeNode(),
                                child: TextFormField(
                                  autovalidate: formMCompanyManajemenPICChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && _data.isDateOfBirthMandatory()) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  controller: formMCompanyManajemenPICChangeNotif.controllerBirthOfDate,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                    labelText: 'Tanggal Lahir',
                                    labelStyle: TextStyle(color: Colors.black),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8)
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: formMCompanyManajemenPICChangeNotif.dateOfBirthDakor ? Colors.purple : Colors.grey)),
                                    disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: formMCompanyManajemenPICChangeNotif.dateOfBirthDakor ? Colors.purple : Colors.grey)),
                                  ),
                                  onTap: () {
                                    FocusManager.instance.primaryFocus.unfocus();
                                    formMCompanyManajemenPICChangeNotif.selectBirthDate(context);
                                  },
                                  readOnly: true,
                                )
                            ),
                          ),
                          Visibility(visible: _data.isDateOfBirthVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          Visibility(
                            visible: _data.isPlaceOfBirthVisible(),
                            child: TextFormField(
                              autovalidate: formMCompanyManajemenPICChangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty && _data.isPlaceOfBirthMandatory()) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              controller: formMCompanyManajemenPICChangeNotif.controllerPlaceOfBirthIdentity,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                labelText: 'Tempat Lahir Sesuai Identitas',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8)
                                ),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: formMCompanyManajemenPICChangeNotif.placeOfBirthDakor ? Colors.purple : Colors.grey)),
                                disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: formMCompanyManajemenPICChangeNotif.placeOfBirthDakor ? Colors.purple : Colors.grey)),
                              ),
                              keyboardType: TextInputType.text,
                              textCapitalization: TextCapitalization.characters,
                            ),
                          ),
                          Visibility(visible: _data.isPlaceOfBirthVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          Visibility(
                            visible: _data.isPlaceOfBirthLOVVisible(),
                            child: TextFormField(
                              autovalidate: formMCompanyManajemenPICChangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty && _data.isPlaceOfBirthMandatory()) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              onTap: (){
                                FocusManager.instance.primaryFocus.unfocus();
                                formMCompanyManajemenPICChangeNotif.searchBirthPlace(context);
                              },
                              controller: formMCompanyManajemenPICChangeNotif.controllerBirthPlaceIdentityLOV,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                labelText: 'Tempat Lahir Sesuai Identitas',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8)
                                ),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: formMCompanyManajemenPICChangeNotif.placeOfBirthLOVDakor ? Colors.purple : Colors.grey)),
                                disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: formMCompanyManajemenPICChangeNotif.placeOfBirthLOVDakor ? Colors.purple : Colors.grey)),
                              ),
                              keyboardType: TextInputType.text,
                              textCapitalization: TextCapitalization.characters,
                            ),
                          ),
                          Visibility(visible: _data.isPlaceOfBirthLOVVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          Visibility(
                            visible: _data.isPositionVisible(),
                            child: DropdownButtonFormField<RelationshipStatusModel>(
                              autovalidate: formMCompanyManajemenPICChangeNotif.autoValidate,
                              validator: (e) {
                                if (e == null && formMCompanyManajemenPICChangeNotif.isPositionMandatory()) {
                                  return "Silahkan pilih jabatan";
                                } else {
                                  return null;
                                }
                              },
                              value: formMCompanyManajemenPICChangeNotif.positionSelected,
                              onChanged: (value) {
                                formMCompanyManajemenPICChangeNotif.positionSelected = value;
                              },
                              onTap: () {
                                FocusManager.instance.primaryFocus.unfocus();
                              },
                              decoration: InputDecoration(
                                labelText: "Jabatan",
                                border: OutlineInputBorder(),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: formMCompanyManajemenPICChangeNotif.positionDakor ? Colors.purple : Colors.grey)),
                                disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: formMCompanyManajemenPICChangeNotif.positionDakor ? Colors.purple : Colors.grey)),
                                contentPadding: EdgeInsets.symmetric(horizontal: 10),
                              ),
                              items: formMCompanyManajemenPICChangeNotif.listPosition.map((value) {
                                return DropdownMenuItem<RelationshipStatusModel>(
                                  value: value,
                                  child: Text(
                                    value.PARA_FAMILY_TYPE_NAME,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                );
                              }).toList(),
                            ),
                            // TextFormField(
                            //   autovalidate: formMCompanyManajemenPICChangeNotif.autoValidate,
                            //   validator: (e) {
                            //     if (e.isEmpty && _data.isPositionMandatory()) {
                            //       return "Tidak boleh kosong";
                            //     } else {
                            //       return null;
                            //     }
                            //   },
                            //   controller: formMCompanyManajemenPICChangeNotif.controllerPosition,
                            //   style: new TextStyle(color: Colors.black),
                            //   decoration: new InputDecoration(
                            //     labelText: 'Jabatan',
                            //     labelStyle: TextStyle(color: Colors.black),
                            //     border: OutlineInputBorder(
                            //         borderRadius: BorderRadius.circular(8)
                            //     ),
                            //     enabledBorder: OutlineInputBorder(
                            //         borderSide: BorderSide(color: formMCompanyManajemenPICChangeNotif.positionDakor ? Colors.purple : Colors.grey)),
                            //     disabledBorder: OutlineInputBorder(
                            //         borderSide: BorderSide(color: formMCompanyManajemenPICChangeNotif.positionDakor ? Colors.purple : Colors.grey)),
                            //   ),
                            //   keyboardType: TextInputType.text,
                            //   textCapitalization: TextCapitalization.characters,
                            // ),
                          ),
                          Visibility(visible: _data.isPositionVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          Visibility(
                            visible: _data.isEmailVisible(),
                            child: TextFormField(
                              autovalidate: formMCompanyManajemenPICChangeNotif.autoValidate,
                              validator: formMCompanyManajemenPICChangeNotif.validateEmail,
                              controller: formMCompanyManajemenPICChangeNotif.controllerEmail,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                labelText: 'Email',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8)
                                ),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: formMCompanyManajemenPICChangeNotif.emailDakor ? Colors.purple : Colors.grey)),
                                disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: formMCompanyManajemenPICChangeNotif.emailDakor ? Colors.purple : Colors.grey)),
                              ),
                              keyboardType: TextInputType.text,
                              textCapitalization: TextCapitalization.characters,
                            ),
                          ),
                          Visibility(visible: _data.isEmailVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          Visibility(
                            visible: _data.isPhoneVisible(),
                            child: TextFormField(
                              autovalidate: formMCompanyManajemenPICChangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty && _data.isPhoneMandatory()) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(14),
                                FilteringTextInputFormatter.allow(RegExp('[0-9]')),
                                // WhitelistingTextInputFormatter(RegExp(r"^[1-9][0-9]*$"))
                              ],
                              controller: formMCompanyManajemenPICChangeNotif.controllerHandphoneNumber,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                labelText: 'No Handphone',
                                labelStyle: TextStyle(color: Colors.black),
                                prefixText: '08',
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8)
                                ),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: formMCompanyManajemenPICChangeNotif.phoneDakor ? Colors.purple : Colors.grey)),
                                disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: formMCompanyManajemenPICChangeNotif.phoneDakor ? Colors.purple : Colors.grey)),
                              ),
                              keyboardType: TextInputType.number,
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                bottomNavigationBar: BottomAppBar(
                  elevation: 0.0,
                  child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Consumer<FormMCompanyManajemenPICChangeNotifier>(
                        builder: (context, formMCompanyManajemenPICChangeNotif, _) {
                          return RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(8.0)),
                              color: myPrimaryColor,
                              onPressed: () {
                                formMCompanyManajemenPICChangeNotif.check(context);
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text("DONE",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500,
                                          letterSpacing: 1.25))
                                ],
                              ));
                        },
                      )),
                ),
              );
            },
          );
        }
      )
    );
  }
}
