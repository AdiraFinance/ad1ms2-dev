import 'package:ad1ms2_dev/models/address_guarantor_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/shared/form_m_company_guarantor_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../main.dart';
import 'company_add_guarantor_individual.dart';

class ListCompanyGuarantorIndividual extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans"),
      child: Scaffold(
        appBar: AppBar(
          title: Text("List Company Penjamin Pribadi",
              style: TextStyle(color: Colors.black)),
          centerTitle: true,
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: Consumer<FormMCompanyGuarantorChangeNotifier>(
          builder: (context, formMGuarantorChangeNotifier, _) {
            return ListView.builder(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 77,
                  horizontal: MediaQuery.of(context).size.width / 47),
              itemBuilder: (context, index) {
                return InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => CompanyAddGuarantorIndividual(
                          flag: 1,
                          index: index,
                          model: formMGuarantorChangeNotifier.listGuarantorIndividual[index])
                      )
                    );
                  },
                  child: Card(
                    elevation: 3.3,
                    child: Padding(
                        padding: const EdgeInsets.all(16),
                        child: Stack(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  children: [
                                    Text(formMGuarantorChangeNotifier.listGuarantorIndividual[index].fullNameIdentity,
                                      style: TextStyle(fontWeight: FontWeight.bold),),
                                    Text(" (${formMGuarantorChangeNotifier.listGuarantorIndividual[index].relationshipStatusModel.PARA_FAMILY_TYPE_NAME})",
                                      style: TextStyle(color: Colors.grey, fontSize: 13),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height:
                                  MediaQuery.of(context).size.height /
                                      97,
                                ),
                                Row(
                                  children: [
                                    Text("${formMGuarantorChangeNotifier.listGuarantorIndividual[index].birthPlaceIdentity1}",
                                      style: TextStyle(color: Colors.grey, fontSize: 13),
                                    ),
                                    Text(", ${formMGuarantorChangeNotifier.listGuarantorIndividual[index].birthDate}",
                                      style: TextStyle(color: Colors.grey, fontSize: 13),
                                    ),
                                  ],
                                ),
                                Text("${formMGuarantorChangeNotifier.listGuarantorIndividual[index].gender}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),
                                ),
                                Text("08${formMGuarantorChangeNotifier.listGuarantorIndividual[index].cellPhoneNumber}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),
                                ),
                              ],
                            ),
                            Align(
                              alignment: Alignment.topRight,
                              child: GestureDetector(
                                  onTap: () {formMGuarantorChangeNotifier.deleteListGuarantorIndividual(context, index);},
                                  child: Icon(Icons.delete, color: Colors.red)
                              )
                              // IconButton(icon: Icon(Icons.delete, color: Colors.red),
                              //     onPressed: () {
                              //       // formMinfoAlamat.deleteAlamatKorespondensi(index);
                              //     })
                              //     : SizedBox(),
                            )
                          ],
                        )
                    ),
                  ),
                );
              },
              itemCount:
                  formMGuarantorChangeNotifier.listGuarantorIndividual.length,
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CompanyAddGuarantorIndividual(
                        model: null,
                        index: null,
                        flag: 0,
                      )
                  )
              );
            },
            child: Icon(Icons.add),
            backgroundColor: myPrimaryColor),
      ),
    );
  }

  Widget _widget(List<AddressModel> model) {
    String _data = "";
    for (int i = 0; i < model.length; i++) {
      if (model[i].isCorrespondence) {
        _data = model[i].address;
      }
    }
    return Text(_data);
  }
}
