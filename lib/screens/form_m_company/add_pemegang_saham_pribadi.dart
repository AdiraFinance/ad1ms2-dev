import 'dart:io';

import 'package:ad1ms2_dev/models/form_m_company_pemegang_saham_pribadi_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/models/pemegang_saham_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/add_pemegang_saham_pribadi_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_individu_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../../main.dart';
import 'list_pemegang_saham_pribadi_alamat.dart';

class AddPemegangSahamPribadi extends StatefulWidget {
  final int flag;
  final int index;
  final PemegangSahamPribadiModel model;

  const AddPemegangSahamPribadi({this.flag, this.index, this.model});
  @override
  _AddPemegangSahamPribadiState createState() => _AddPemegangSahamPribadiState();
}

class _AddPemegangSahamPribadiState extends State<AddPemegangSahamPribadi> {
  Future<void> _setValueForEdit;
  @override
  void initState() {
    super.initState();
    _setValueForEdit = Provider.of<AddPemegangSahamPribadiChangeNotifier>(context, listen: false).setValue(widget.flag, widget.index, widget.model, context);
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        primaryColor: Colors.black,
        accentColor: myPrimaryColor,
        primarySwatch: primaryOrange,
      ),
      child: Consumer<AddPemegangSahamPribadiChangeNotifier>(
        builder: (context, formMCompanyPemegangSahamPribadiChangeNotif, _) {
          return Scaffold(
            key: formMCompanyPemegangSahamPribadiChangeNotif.scaffoldKey,
            appBar: AppBar(
              title: Text(widget.flag == 0 ? "Tambah Pemegang Saham Pribadi" : "Edit Pemegang Saham Pribadi", style: TextStyle(color: Colors.black)),
              centerTitle: true,
              backgroundColor: myPrimaryColor,
              iconTheme: IconThemeData(color: Colors.black),
            ),
            body: FutureBuilder(
                future: _setValueForEdit,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  return Form(
                    key: formMCompanyPemegangSahamPribadiChangeNotif.keyForm,
                    onWillPop: _onWillPop,
                    child: ListView(
                      padding: EdgeInsets.symmetric(
                          vertical: MediaQuery.of(context).size.height/57,
                          horizontal: MediaQuery.of(context).size.width/37
                      ),
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Visibility(
                              visible: formMCompanyPemegangSahamPribadiChangeNotif.isRelationshipStatusVisible(),
                              child: DropdownButtonFormField<RelationshipStatusModel>(
                                autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                                validator: (e) {
                                  if (e == null && formMCompanyPemegangSahamPribadiChangeNotif.isRelationshipStatusMandatory()) {
                                    return "Silahkan pilih status hubungan";
                                  } else {
                                    return null;
                                  }
                                },
                                value: formMCompanyPemegangSahamPribadiChangeNotif.relationshipStatusSelected,
                                onChanged: (value) {
                                  formMCompanyPemegangSahamPribadiChangeNotif.relationshipStatusSelected = value;
                                },
                                onTap: () {
                                  FocusManager.instance.primaryFocus.unfocus();
                                },
                                decoration: InputDecoration(
                                  labelText: "Status Hubungan",
                                  border: OutlineInputBorder(),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyPemegangSahamPribadiChangeNotif.relationshipStatusDakor ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyPemegangSahamPribadiChangeNotif.relationshipStatusDakor ? Colors.purple : Colors.grey)),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                ),
                                items: formMCompanyPemegangSahamPribadiChangeNotif.listRelationshipStatus.map((value) {
                                  return DropdownMenuItem<RelationshipStatusModel>(
                                    value: value,
                                    child: Text(
                                      value.PARA_FAMILY_TYPE_NAME,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                            Visibility(visible: formMCompanyPemegangSahamPribadiChangeNotif.isRelationshipStatusVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: formMCompanyPemegangSahamPribadiChangeNotif.isIdentityTypeVisible(),
                              child: DropdownButtonFormField<IdentityModel>(
                                autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                                validator: (e) {
                                  if (e == null && formMCompanyPemegangSahamPribadiChangeNotif.isIdentityTypeMandatory()) {
                                    return "Silahkan pilih jenis identitas";
                                  } else {
                                    return null;
                                  }
                                },
                                value: formMCompanyPemegangSahamPribadiChangeNotif.typeIdentitySelected,
                                onChanged: (value) {
                                  formMCompanyPemegangSahamPribadiChangeNotif.typeIdentitySelected = value;
                                },
                                onTap: () {
                                  FocusManager.instance.primaryFocus.unfocus();
                                },
                                decoration: InputDecoration(
                                  labelText: "Jenis Identitas",
                                  border: OutlineInputBorder(),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyPemegangSahamPribadiChangeNotif.identityTypeDakor ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyPemegangSahamPribadiChangeNotif.identityTypeDakor ? Colors.purple : Colors.grey)),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                ),
                                items: formMCompanyPemegangSahamPribadiChangeNotif.listTypeIdentity.map((value) {
                                  return DropdownMenuItem<IdentityModel>(
                                    value: value,
                                    child: Text(
                                      value.name,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                            Visibility(visible: formMCompanyPemegangSahamPribadiChangeNotif.isIdentityTypeVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: formMCompanyPemegangSahamPribadiChangeNotif.isIdentityNoVisible(),
                              child: TextFormField(
                                  autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && formMCompanyPemegangSahamPribadiChangeNotif.isIdentityNoMandatory()) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  inputFormatters: formMCompanyPemegangSahamPribadiChangeNotif.typeIdentitySelected != null
                                      ? formMCompanyPemegangSahamPribadiChangeNotif.typeIdentitySelected.id != "03"
                                      ? [WhitelistingTextInputFormatter.digitsOnly,LengthLimitingTextInputFormatter(16)]
                                      : null
                                      : [WhitelistingTextInputFormatter.digitsOnly],
                                  controller: formMCompanyPemegangSahamPribadiChangeNotif.controllerIdentityNumber,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                    labelText: 'No Identitas',
                                    labelStyle: TextStyle(color: Colors.black),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8)
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: formMCompanyPemegangSahamPribadiChangeNotif.identityNoDakor ? Colors.purple : Colors.grey)),
                                    disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: formMCompanyPemegangSahamPribadiChangeNotif.identityNoDakor ? Colors.purple : Colors.grey)),
                                  ),
                                  keyboardType: formMCompanyPemegangSahamPribadiChangeNotif.typeIdentitySelected != null
                                      ? formMCompanyPemegangSahamPribadiChangeNotif.typeIdentitySelected.id != "03"
                                      ? TextInputType.number
                                      : TextInputType.text
                                      : TextInputType.number
                              ),
                            ),
                            Visibility(visible: formMCompanyPemegangSahamPribadiChangeNotif.isIdentityNoVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: formMCompanyPemegangSahamPribadiChangeNotif.isFullnameIDVisible(),
                              child: TextFormField(
                                autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty && formMCompanyPemegangSahamPribadiChangeNotif.isFullnameMandatory()) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                controller: formMCompanyPemegangSahamPribadiChangeNotif.controllerFullNameIdentity,
                                style: new TextStyle(color: Colors.black),
                                decoration: new InputDecoration(
                                  labelText: 'Nama Lengkap Sesuai Identitas',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyPemegangSahamPribadiChangeNotif.fullnameIDDakor ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyPemegangSahamPribadiChangeNotif.fullnameIDDakor ? Colors.purple : Colors.grey)),
                                ),
                                keyboardType: TextInputType.text,
                                textCapitalization: TextCapitalization.characters,
                                inputFormatters: [
                                  FilteringTextInputFormatter.allow(RegExp('[a-zA-Z  \`]')),
                                ],
                              ),
                            ),
                            Visibility(visible: formMCompanyPemegangSahamPribadiChangeNotif.isFullnameIDVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: formMCompanyPemegangSahamPribadiChangeNotif.isFullnameVisible(),
                              child: TextFormField(
                                autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty && formMCompanyPemegangSahamPribadiChangeNotif.isFullnameMandatory()) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                controller: formMCompanyPemegangSahamPribadiChangeNotif.controllerFullName,
                                style: new TextStyle(color: Colors.black),
                                decoration: new InputDecoration(
                                  labelText: 'Nama Lengkap',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyPemegangSahamPribadiChangeNotif.fullnameDakor ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyPemegangSahamPribadiChangeNotif.fullnameDakor ? Colors.purple : Colors.grey)),
                                ),
                                keyboardType: TextInputType.text,
                                textCapitalization: TextCapitalization.characters,
                                inputFormatters: [
                                  FilteringTextInputFormatter.allow(RegExp('[a-zA-Z  \`]')),
                                ],
                              ),
                            ),
                            Visibility(visible: formMCompanyPemegangSahamPribadiChangeNotif.isFullnameVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            // TextFormField(
                            //   autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                            //   validator: (e) {
                            //     if (e.isEmpty) {
                            //       return "Tidak boleh kosong";
                            //     } else {
                            //       return null;
                            //     }
                            //   },
                            //   controller: formMCompanyPemegangSahamPribadiChangeNotif.controllerAlias,
                            //   style: new TextStyle(color: Colors.black),
                            //   decoration: new InputDecoration(
                            //       labelText: 'Alias',
                            //       labelStyle: TextStyle(color: Colors.black),
                            //       border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                            //   keyboardType: TextInputType.text,
                            //   textCapitalization: TextCapitalization.characters,
                            // ),
                            // SizedBox(height: MediaQuery.of(context).size.height / 47),
                            // TextFormField(
                            //   autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                            //   validator: (e) {
                            //     if (e.isEmpty) {
                            //       return "Tidak boleh kosong";
                            //     } else {
                            //       return null;
                            //     }
                            //   },
                            //   controller: formMCompanyPemegangSahamPribadiChangeNotif.controllerDegree,
                            //   style: new TextStyle(color: Colors.black),
                            //   decoration: new InputDecoration(
                            //       labelText: 'Gelar',
                            //       labelStyle: TextStyle(color: Colors.black),
                            //       border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                            //   keyboardType: TextInputType.text,
                            //   textCapitalization: TextCapitalization.characters,
                            // ),
                            // SizedBox(height: MediaQuery.of(context).size.height / 47),
                            Visibility(
                              visible: formMCompanyPemegangSahamPribadiChangeNotif.isDateOfBirthVisible(),
                              child: FocusScope(
                                  node: FocusScopeNode(),
                                  child: TextFormField(
                                    autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty && formMCompanyPemegangSahamPribadiChangeNotif.isDateOfBirthMandatory()) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    controller: formMCompanyPemegangSahamPribadiChangeNotif.controllerBirthOfDate,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                      labelText: 'Tanggal Lahir',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMCompanyPemegangSahamPribadiChangeNotif.dateOfBirthDakor ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMCompanyPemegangSahamPribadiChangeNotif.dateOfBirthDakor ? Colors.purple : Colors.grey)),
                                    ),
                                    onTap: () {
                                      FocusManager.instance.primaryFocus.unfocus();
                                      formMCompanyPemegangSahamPribadiChangeNotif.selectBirthDate(context);
                                    },
                                    readOnly: true,
                                  )
                              ),
                            ),
                            Visibility(visible: formMCompanyPemegangSahamPribadiChangeNotif.isDateOfBirthVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: formMCompanyPemegangSahamPribadiChangeNotif.isPlaceOfBirthVisible(),
                              child: TextFormField(
                                autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty && formMCompanyPemegangSahamPribadiChangeNotif.isPlaceOfBirthMandatory()) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                controller: formMCompanyPemegangSahamPribadiChangeNotif.controllerPlaceOfBirthIdentity,
                                style: new TextStyle(color: Colors.black),
                                decoration: new InputDecoration(
                                  labelText: 'Tempat Lahir Sesuai Identitas',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyPemegangSahamPribadiChangeNotif.placeOfBirthDakor ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyPemegangSahamPribadiChangeNotif.placeOfBirthDakor ? Colors.purple : Colors.grey)),
                                ),
                                keyboardType: TextInputType.text,
                                textCapitalization: TextCapitalization.characters,
                              ),
                            ),
                            Visibility(visible: formMCompanyPemegangSahamPribadiChangeNotif.isPlaceOfBirthVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: formMCompanyPemegangSahamPribadiChangeNotif.isPlaceOfBirthLOVVisible(),
                              child: TextFormField(
                                autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty && formMCompanyPemegangSahamPribadiChangeNotif.isPlaceOfBirthLOVMandatory()) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                onTap: (){
                                  formMCompanyPemegangSahamPribadiChangeNotif.searchBirthPlace(context);
                                },
                                controller: formMCompanyPemegangSahamPribadiChangeNotif.controllerBirthPlaceIdentityLOV,
                                style: new TextStyle(color: Colors.black),
                                decoration: new InputDecoration(
                                  labelText: 'Tempat Lahir Sesuai Identitas',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyPemegangSahamPribadiChangeNotif.placeOfBirthLOVDakor ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyPemegangSahamPribadiChangeNotif.placeOfBirthLOVDakor ? Colors.purple : Colors.grey)),
                                ),
                                keyboardType: TextInputType.text,
                                textCapitalization: TextCapitalization.characters,
                                readOnly: true,
                              ),
                            ),
                            Visibility(visible: formMCompanyPemegangSahamPribadiChangeNotif.isPlaceOfBirthLOVVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: formMCompanyPemegangSahamPribadiChangeNotif.isGenderVisible(),
                              child: Text(
                                "Jenis Kelamin",
                                style: TextStyle(
                                    color: formMCompanyPemegangSahamPribadiChangeNotif.genderDakor ? Colors.purple : Colors.black,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                    letterSpacing: 0.15),
                              ),
                            ),
                            Visibility(
                              visible: formMCompanyPemegangSahamPribadiChangeNotif.isGenderVisible(),
                              child: Row(
                                children: [
                                  Row(
                                    children: [
                                      Radio(
                                          value: "01",
                                          activeColor: primaryOrange,
                                          groupValue: formMCompanyPemegangSahamPribadiChangeNotif.radioValueGender,
                                          onChanged: (value) {formMCompanyPemegangSahamPribadiChangeNotif.radioValueGender = value;}
                                          // formMCompanyPemegangSahamPribadiChangeNotif.relationshipStatusSelected != null
                                          //     ? formMCompanyPemegangSahamPribadiChangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "01" ||
                                          //     formMCompanyPemegangSahamPribadiChangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "02" ||
                                          //     formMCompanyPemegangSahamPribadiChangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "04" ||
                                          //     formMCompanyPemegangSahamPribadiChangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "05"
                                          //     ? (value) {;}
                                          //     : (value) {formMCompanyPemegangSahamPribadiChangeNotif.radioValueGender = value;}
                                          //     : (value) {;}
                                      ),
                                      Text("Laki Laki")
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Radio(
                                          value: "02",
                                          activeColor: primaryOrange,
                                          groupValue: formMCompanyPemegangSahamPribadiChangeNotif.radioValueGender,
                                          onChanged: (value) {formMCompanyPemegangSahamPribadiChangeNotif.radioValueGender = value;}
                                          // formMCompanyPemegangSahamPribadiChangeNotif.relationshipStatusSelected != null
                                          //     ? formMCompanyPemegangSahamPribadiChangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "01" ||
                                          //     formMCompanyPemegangSahamPribadiChangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "02" ||
                                          //     formMCompanyPemegangSahamPribadiChangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "04"||
                                          //     formMCompanyPemegangSahamPribadiChangeNotif.relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "05"
                                          //     ? (value) {;}
                                          //     : (value) {formMCompanyPemegangSahamPribadiChangeNotif.radioValueGender = value;}
                                          //     : (value) {;}
                                      ),
                                      Text("Perempuan")
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Visibility(visible: formMCompanyPemegangSahamPribadiChangeNotif.isGenderVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: formMCompanyPemegangSahamPribadiChangeNotif.isShareVisible(),
                              child: TextFormField(
                                autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty && formMCompanyPemegangSahamPribadiChangeNotif.isShareMandatory()) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                inputFormatters: [
                                  WhitelistingTextInputFormatter.digitsOnly,
                                  // LengthLimitingTextInputFormatter(10),
                                ],
                                onChanged: (value){
                                  if(value.isNotEmpty){
                                    formMCompanyPemegangSahamPribadiChangeNotif.limitInput(value);
                                  }
                                },
                                onFieldSubmitted: (value) {
                                  formMCompanyPemegangSahamPribadiChangeNotif.limitInput(value);
                                },
                                controller: formMCompanyPemegangSahamPribadiChangeNotif.controllerPercentShare,
                                style: new TextStyle(color: Colors.black),
                                decoration: new InputDecoration(
                                  labelText: '% Share',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyPemegangSahamPribadiChangeNotif.shareDakor ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyPemegangSahamPribadiChangeNotif.shareDakor ? Colors.purple : Colors.grey)),
                                ),
                                keyboardType: TextInputType.number,
                              ),
                            ),
                            Visibility(visible: formMCompanyPemegangSahamPribadiChangeNotif.isShareVisible(),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            Visibility(
                              visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressTypeShow(5),
                              child: TextFormField(
                                autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressTypeMan(5)) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                controller: formMCompanyPemegangSahamPribadiChangeNotif.controllerAddressType,
                                style: new TextStyle(color: Colors.black),
                                decoration: new InputDecoration(
                                    labelText: 'Jenis Alamat',
                                    labelStyle: TextStyle(color: Colors.black),
                                    filled: true,
                                    fillColor: Colors.black12,
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8))),
                                enabled: false,
                              ),
                            ),
                            Visibility(
                                visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressTypeShow(5),
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                            ),
                            Visibility(
                              visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressShow(5),
                              child: TextFormField(
                                autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressMan(5)) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                // onTap: () {
                                //   Navigator.push(
                                //       context,
                                //       MaterialPageRoute(
                                //           builder: (context) => ListPemegangSahamPribadiAlamat()));
                                // },
                                controller: formMCompanyPemegangSahamPribadiChangeNotif.controllerAddress,
                                style: new TextStyle(color: Colors.black),
                                decoration: new InputDecoration(
                                    labelText: 'Alamat',
                                    labelStyle: TextStyle(color: Colors.black),
                                    filled: true,
                                    fillColor: Colors.black12,
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8))),
                                enabled: false,
                                // readOnly: true,
                              ),
                            ),
                            Visibility(
                                child: SizedBox(height: MediaQuery.of(context).size.height / 47),
                                visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressShow(5)
                            ),
                            Row(
                              children: [
                                Expanded(
                                  flex: 5,
                                  child: Visibility(
                                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRTShow(5),
                                    child: TextFormField(
                                      autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                                      validator: (e) {
                                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRTMan(5)) {
                                          return "Tidak boleh kosong";
                                        } else {
                                          return null;
                                        }
                                      },
                                      controller: formMCompanyPemegangSahamPribadiChangeNotif.controllerRT,
                                      style: new TextStyle(color: Colors.black),
                                      decoration: new InputDecoration(
                                          filled: true,
                                          fillColor: Colors.black12,
                                          labelText: 'RT',
                                          labelStyle: TextStyle(color: Colors.black),
                                          border: OutlineInputBorder(
                                              borderRadius: BorderRadius.circular(8))),
                                      enabled: false,
                                    ),
                                  ),
                                ),
                                SizedBox(width: 8),
                                Expanded(
                                    flex: 5,
                                    child: Visibility(
                                      visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRWShow(5),
                                      child: TextFormField(
                                        autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRWMan(5)) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        controller: formMCompanyPemegangSahamPribadiChangeNotif.controllerRW,
                                        style: new TextStyle(color: Colors.black),
                                        decoration: new InputDecoration(
                                            filled: true,
                                            fillColor: Colors.black12,
                                            labelText: 'RW',
                                            labelStyle: TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(8))),
                                        enabled: false,
                                      ),
                                    ))
                              ],
                            ),
                            Visibility(
                              child: SizedBox(height: MediaQuery.of(context).size.height / 47),
                              visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRWShow(5)
                                  && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRTShow(5),
                            ),
                            Visibility(
                              visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKelurahanShow(5),
                              child: TextFormField(
                                autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKelurahanMan(5)) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                controller: formMCompanyPemegangSahamPribadiChangeNotif.controllerKelurahan,
                                style: new TextStyle(color: Colors.black),
                                decoration: new InputDecoration(
                                    filled: true,
                                    fillColor: Colors.black12,
                                    labelText: 'Kelurahan',
                                    labelStyle: TextStyle(color: Colors.black),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8))),
                                enabled: false,
                              ),
                            ),
                            Visibility(child: SizedBox(height: MediaQuery.of(context).size.height / 47),visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKelurahanShow(5),),
                            Visibility(
                              visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKecamatanShow(5),
                              child: TextFormField(
                                autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKecamatanMan(5)) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                controller: formMCompanyPemegangSahamPribadiChangeNotif.controllerKecamatan,
                                style: new TextStyle(color: Colors.black),
                                decoration: new InputDecoration(
                                    filled: true,
                                    fillColor: Colors.black12,
                                    labelText: 'Kecamatan',
                                    labelStyle: TextStyle(color: Colors.black),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8))),
                                enabled: false,
                              ),
                            ),
                            Visibility(
                              child: SizedBox(height: MediaQuery.of(context).size.height / 47),
                              visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKecamatanShow(5),
                            ),
                            Visibility(
                              visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKabKotShow(5),
                              child: TextFormField(
                                autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKabKotMan(5)) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                controller: formMCompanyPemegangSahamPribadiChangeNotif.controllerKota,
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    filled: true,
                                    fillColor: Colors.black12,
                                    labelText: 'Kabupaten/Kota',
                                    labelStyle: TextStyle(color: Colors.black),
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8))),
                                enabled: false,
                              ),
                            ),
                            Visibility(child: SizedBox(height: MediaQuery.of(context).size.height / 47),visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKabKotShow(5)),
                            Row(
                              children: [
                                Expanded(
                                  flex: 7,
                                  child:Visibility(
                                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setProvinsiShow(5),
                                    child:TextFormField(
                                      autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                                      validator: (e) {
                                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setProvinsiMan(5)) {
                                          return "Tidak boleh kosong";
                                        } else {
                                          return null;
                                        }
                                      },
                                      controller: formMCompanyPemegangSahamPribadiChangeNotif.controllerProvinsi,
                                      style: new TextStyle(color: Colors.black),
                                      decoration: new InputDecoration(
                                          filled: true,
                                          fillColor: Colors.black12,
                                          labelText: 'Provinsi',
                                          labelStyle: TextStyle(color: Colors.black),
                                          border: OutlineInputBorder(
                                              borderRadius: BorderRadius.circular(8))),
                                      enabled: false,
                                    ),
                                  ),
                                ),
                                SizedBox(width: 8),
                                Expanded(
                                  flex: 3,
                                  child: Visibility(
                                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKodePosShow(5),
                                    child: TextFormField(
                                      autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                                      validator: (e) {
                                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKodePosMan(5)) {
                                          return "Tidak boleh kosong";
                                        } else {
                                          return null;
                                        }
                                      },
                                      controller:
                                      formMCompanyPemegangSahamPribadiChangeNotif.controllerPostalCode,
                                      style: new TextStyle(color: Colors.black),
                                      decoration: new InputDecoration(
                                          filled: true,
                                          fillColor: Colors.black12,
                                          labelText: 'Kode Pos',
                                          labelStyle: TextStyle(color: Colors.black),
                                          border: OutlineInputBorder(
                                              borderRadius: BorderRadius.circular(8))),
                                      enabled: false,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Visibility(
                              child: SizedBox(height: MediaQuery.of(context).size.height / 47),
                              visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setProvinsiShow(5)
                                  && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKodePosShow(5),
                            ),
                            Row(
                              children: [
                                Expanded(
                                  flex: 4,
                                  child: Visibility(
                                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setTelp1AreaShow(5),
                                    child: TextFormField(
                                      autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                                      validator: (e) {
                                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setTelp1AreaMan(5)) {
                                          return "Tidak boleh kosong";
                                        } else {
                                          return null;
                                        }
                                      },
                                      keyboardType: TextInputType.number,
                                      inputFormatters: [
                                        WhitelistingTextInputFormatter.digitsOnly,
                                        LengthLimitingTextInputFormatter(10),
                                      ],
                                      controller: formMCompanyPemegangSahamPribadiChangeNotif.controllerTeleponArea,
                                      style: new TextStyle(color: Colors.black),
                                      decoration: new InputDecoration(
                                          filled: true,
                                          fillColor: Colors.black12,
                                          labelText: 'Telepon (Area)',
                                          labelStyle: TextStyle(color: Colors.black),
                                          border: OutlineInputBorder(
                                              borderRadius: BorderRadius.circular(8))),
                                      enabled: false,
                                    ),
                                  ),
                                ),
                                SizedBox(width: MediaQuery.of(context).size.width / 37),
                                Expanded(
                                  flex: 6,
                                  child: Visibility(
                                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setTelp1Show(5),
                                    child: TextFormField(
                                      autovalidate: formMCompanyPemegangSahamPribadiChangeNotif.autoValidate,
                                      validator: (e) {
                                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setTelp1Man(5)) {
                                          return "Tidak boleh kosong";
                                        } else {
                                          return null;
                                        }
                                      },
                                      keyboardType: TextInputType.number,
                                      inputFormatters: [
                                        WhitelistingTextInputFormatter.digitsOnly,
                                        LengthLimitingTextInputFormatter(10),
                                      ],
                                      controller: formMCompanyPemegangSahamPribadiChangeNotif.controllerTelepon,
                                      style: new TextStyle(color: Colors.black),
                                      decoration: new InputDecoration(
                                          filled: true,
                                          fillColor: Colors.black12,
                                          labelText: 'Telepon',
                                          labelStyle: TextStyle(color: Colors.black),
                                          border: OutlineInputBorder(
                                              borderRadius: BorderRadius.circular(8))),
                                      enabled: false,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        )
                      ],
                    ),
                  );
                }),
            bottomNavigationBar: BottomAppBar(
              elevation: 0.0,
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Consumer<AddPemegangSahamPribadiChangeNotifier>(
                    builder: (context, formMCompanyPemegangSahamPribadiChangeNotif, _) {
                      return Row(
                        children: [
                          Expanded(
                            child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: new BorderRadius.circular(8.0)),
                                color: myPrimaryColor,
                                onPressed: () {
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => ListPemegangSahamPribadiAlamat()));
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text("EDIT ALAMAT",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500,
                                            letterSpacing: 1.25))
                                  ],
                                )
                            ),
                          ),
                          SizedBox(width: MediaQuery.of(context).size.width / 37),
                          Expanded(
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(8.0)),
                              color: myPrimaryColor,
                              onPressed: () {
                                formMCompanyPemegangSahamPribadiChangeNotif.check(context, widget.flag, widget.index);
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(widget.flag == 0 ? "SAVE" : "UPDATE",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500,
                                          letterSpacing: 1.25))
                                ],
                              )))
                        ],
                      );
                    },
                  )),
            ),
          );
        },
      )
    );
  }

  Future<bool> _onWillPop() async {
    var _provider = Provider.of<AddPemegangSahamPribadiChangeNotifier>(context, listen: false);
    if (widget.flag == 0){
      if (_provider.relationshipStatusSelected != null || _provider.typeIdentitySelected != null ||
          _provider.controllerIdentityNumber.text.isNotEmpty || _provider.controllerFullNameIdentity.text.isNotEmpty ||
          _provider.controllerFullName.text.isNotEmpty || _provider.controllerBirthOfDate.text.isNotEmpty ||
          _provider.controllerPlaceOfBirthIdentity.text.isNotEmpty || _provider.controllerBirthPlaceIdentityLOV.text.isNotEmpty ||
          _provider.controllerPercentShare.text.isNotEmpty) {
        return (await showDialog(context: context,
          builder: (myContext) => AlertDialog(
            title: new Text('Warning'),
            content: new Text('Keluar dengan simpan perubahan?'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () {
                  _provider.check(context, widget.flag, widget.index);
                  Navigator.pop(context);
                },
                child: new Text('Ya', style: TextStyle(color: Colors.grey),),
              ),
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: new Text('Tidak'),
              ),
            ],
          ),
        )) ??
            false;
      } else {
        return true;
      }
    }
    else {
      if (widget.model.relationshipStatusSelected.PARA_FAMILY_TYPE_ID != _provider.relationshipStatusSelected.PARA_FAMILY_TYPE_ID ||
          widget.model.typeIdentitySelected.id != _provider.typeIdentitySelected.id ||
          widget.model.identityNumber != _provider.controllerIdentityNumber.text ||
          widget.model.fullNameIdentity != _provider.controllerFullNameIdentity.text ||
          widget.model.fullName != _provider.controllerFullName.text ||
          widget.model.birthOfDate != _provider.controllerBirthOfDate.text ||
          widget.model.placeOfBirthIdentity != _provider.controllerPlaceOfBirthIdentity.text ||
          widget.model.birthPlaceIdentityLOV.KABKOT_NAME != _provider.controllerBirthPlaceIdentityLOV.text ||
          widget.model.percentShare != _provider.controllerPercentShare.text){
        return (await showDialog(
          context: context,
          builder: (myContext) => AlertDialog(
            title: new Text('Warning'),
            content: new Text('Keluar dengan simpan perubahan?'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () {
                  _provider.check(context, widget.flag, widget.index);
                  Navigator.pop(context);
                },
                child: new Text('Ya', style: TextStyle(color: Colors.grey),),
              ),
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: new Text('Tidak'),
              ),
            ],
          ),
        )) ??
            false;
      } else {
        return true;
      }
    }
  }

}

