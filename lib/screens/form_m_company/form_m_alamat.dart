import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/form_m_company/list_alamat_korespondensi.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_company_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_alamat_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class FormMCompanyAlamat extends StatefulWidget {
  @override
  _FormMCompanyAlamatState createState() => _FormMCompanyAlamatState();
}

class _FormMCompanyAlamatState extends State<FormMCompanyAlamat> {

  @override
  void initState() {
    super.initState();
    Provider.of<FormMCompanyAlamatChangeNotifier>(context,listen: false).setPreference();
  }

  @override
  Widget build(BuildContext context) {
    var _data = Provider.of<FormMAddAddressCompanyChangeNotifier>(context, listen: false);
    return Consumer<FormMCompanyAlamatChangeNotifier>(
      builder: (context, formMCompanyAlamatChangeNotif, _) {
        return Theme(
          data: ThemeData(
              primaryColor: Colors.black,
              accentColor: myPrimaryColor,
              fontFamily: "NunitoSans",
              primarySwatch: Colors.yellow),
          child: Scaffold(
            appBar: AppBar(
              title:
              Text("Alamat Lembaga", style: TextStyle(color: Colors.black)),
              centerTitle: true,
              backgroundColor: myPrimaryColor,
              iconTheme: IconThemeData(color: Colors.black),
            ),
            body: Form(
              key: formMCompanyAlamatChangeNotif.keyForm,
              onWillPop: formMCompanyAlamatChangeNotif.onBackPress,
              child: ListView(
                padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height/57,
                    horizontal: MediaQuery.of(context).size.width/37
                ),
                children: [
                  Column(
                    children: [
                      Visibility(
                        visible: _data.setAddressTypeShow(2),
                        child: TextFormField(
                          autovalidate: formMCompanyAlamatChangeNotif.autoValidate,
                          validator: (e) {
                            if (e.isEmpty && _data.setAddressTypeMandatory(2)) {
                              return "Tidak boleh kosong";
                            } else {
                              return null;
                            }
                          },
                          controller: formMCompanyAlamatChangeNotif.controllerAddressType,
                          style: new TextStyle(color: Colors.black),
                          decoration: new InputDecoration(
                              labelText: 'Jenis Alamat',
                              labelStyle: TextStyle(color: Colors.black),
                              filled: true,
                              fillColor: Colors.black12,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8))),
                          enabled: false,
                        ),
                      ),
                      Visibility(visible: _data.setAddressTypeShow(2),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                      Visibility(
                        visible: _data.setAddressShow(2),
                        child: TextFormField(
                          autovalidate: formMCompanyAlamatChangeNotif.autoValidate,
                          validator: (e) {
                            if (e.isEmpty && _data.setAddressMandatory(2)) {
                              return "Tidak boleh kosong";
                            } else {
                              return null;
                            }
                          },
                          // onTap: () {
                          //   Navigator.push(
                          //       context,
                          //       MaterialPageRoute(
                          //           builder: (context) => ListAddress()));
                          // },
                          controller: formMCompanyAlamatChangeNotif.controllerAddress,
                          style: new TextStyle(color: Colors.black),
                          decoration: new InputDecoration(
                              labelText: 'Alamat',
                              labelStyle: TextStyle(color: Colors.black),
                              filled: true,
                              fillColor: Colors.black12,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8))),
                          enabled: false,
                          // readOnly: true,
                        ),
                      ),
                      Visibility(visible: _data.setAddressShow(2),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                      Visibility(
                        visible: _data.setRTShow(2) || _data.setRWShow(2),
                        child: Row(
                          children: [
                            Visibility(
                              visible: _data.setRTShow(2),
                              child: Expanded(
                                flex: 5,
                                child: TextFormField(
                                  autovalidate: formMCompanyAlamatChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && _data.setRTMandatory(2)) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  controller: formMCompanyAlamatChangeNotif.controllerRT,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      filled: true,
                                      fillColor: Colors.black12,
                                      labelText: 'RT',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8))),
                                  enabled: false,
                                ),
                              ),
                            ),
                            Visibility(visible: _data.setRTShow(2),child: SizedBox(width: 8)),
                            Visibility(
                              visible: _data.setRWShow(2),
                              child: Expanded(
                                  flex: 5,
                                  child: TextFormField(
                                    autovalidate: formMCompanyAlamatChangeNotif.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty && _data.setRWMandatory(2)) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    controller: formMCompanyAlamatChangeNotif.controllerRW,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        filled: true,
                                        fillColor: Colors.black12,
                                        labelText: 'RW',
                                        labelStyle: TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8))),
                                    enabled: false,
                                  )),
                            )
                          ],
                        ),
                      ),
                      Visibility(visible: _data.setRTShow(2) || _data.setRWShow(2), child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                      Visibility(
                        visible: _data.setKelurahanShow(2),
                        child: FocusScope(
                          node: FocusScopeNode(),
                          child: TextFormField(
                            autovalidate: formMCompanyAlamatChangeNotif.autoValidate,
                            validator: (e) {
                              if (e.isEmpty && _data.setKelurahanMandatory(2)) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            controller: formMCompanyAlamatChangeNotif.controllerKelurahan,
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                                filled: true,
                                fillColor: Colors.black12,
                                labelText: 'Kelurahan',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8))),
                            enabled: false,
                          ),
                        ),
                      ),
                      Visibility(visible: _data.setKelurahanShow(2), child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                      Visibility(
                        visible: _data.setKecamatanShow(2),
                        child: TextFormField(
                          autovalidate: formMCompanyAlamatChangeNotif.autoValidate,
                          validator: (e) {
                            if (e.isEmpty && _data.setKecamatanMandatory(2)) {
                              return "Tidak boleh kosong";
                            } else {
                              return null;
                            }
                          },
                          controller: formMCompanyAlamatChangeNotif.controllerKecamatan,
                          style: new TextStyle(color: Colors.black),
                          decoration: new InputDecoration(
                              filled: true,
                              fillColor: Colors.black12,
                              labelText: 'Kecamatan',
                              labelStyle: TextStyle(color: Colors.black),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8))),
                          enabled: false,
                        ),
                      ),
                      Visibility(visible: _data.setKecamatanShow(2),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                      Visibility(
                        visible: _data.setKabKotShow(2),
                        child: TextFormField(
                          autovalidate: formMCompanyAlamatChangeNotif.autoValidate,
                          validator: (e) {
                            if (e.isEmpty && _data.setKabKotMandatory(2)) {
                              return "Tidak boleh kosong";
                            } else {
                              return null;
                            }
                          },
                          controller: formMCompanyAlamatChangeNotif.controllerKota,
                          style: TextStyle(color: Colors.black),
                          decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.black12,
                              labelText: 'Kabupaten/Kota',
                              labelStyle: TextStyle(color: Colors.black),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8))),
                          enabled: false,
                        ),
                      ),
                      Visibility(visible: _data.setKabKotShow(2),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                      Visibility(
                        visible: _data.setProvinsiShow(2) || _data.setKodePosShow(2),
                        child: Row(
                          children: [
                            Visibility(
                              visible: _data.setProvinsiShow(2),
                              child: Expanded(
                                flex: 7,
                                child: TextFormField(
                                  autovalidate: formMCompanyAlamatChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && _data.setProvinsiMandatory(2)) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  controller: formMCompanyAlamatChangeNotif.controllerProvinsi,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      filled: true,
                                      fillColor: Colors.black12,
                                      labelText: 'Provinsi',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8))),
                                  enabled: false,
                                ),
                              ),
                            ),
                            Visibility(visible: _data.setProvinsiShow(2),child: SizedBox(width: 8)),
                            Visibility(
                              visible: _data.setKodePosShow(2),
                              child: Expanded(
                                flex: 3,
                                child: TextFormField(
                                  autovalidate: formMCompanyAlamatChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && _data.setKodePosMandatory(2)) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  controller:
                                  formMCompanyAlamatChangeNotif.controllerPostalCode,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      filled: true,
                                      fillColor: Colors.black12,
                                      labelText: 'Kode Pos',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8))),
                                  enabled: false,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Visibility(visible: _data.setProvinsiShow(2) || _data.setKodePosShow(2), child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                      Visibility(
                        visible: _data.setTelp1AreaShow(2) || _data.setTelp1Show(2),
                        child: Row(
                          children: [
                            Visibility(
                              visible: _data.setTelp1AreaShow(2),
                              child: Expanded(
                                flex: 4,
                                child: TextFormField(
                                  autovalidate: formMCompanyAlamatChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && _data.setTelp1AreaMandatory(2)) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  keyboardType: TextInputType.number,
                                  inputFormatters: [
                                    WhitelistingTextInputFormatter.digitsOnly,
                                    LengthLimitingTextInputFormatter(10),
                                  ],
                                  controller: formMCompanyAlamatChangeNotif.controllerTelepon1Area,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      filled: true,
                                      fillColor: Colors.black12,
                                      labelText: 'Telepon 1 (Area)',
                                      labelStyle: TextStyle(color: Colors.black),
                                      errorStyle: TextStyle(
                                        color: Theme.of(context).errorColor, // or any other color
                                      ),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8))),
                                  enabled: false,
                                ),
                              ),
                            ),
                            Visibility(visible: _data.setTelp1AreaShow(2),child: SizedBox(width: MediaQuery.of(context).size.width / 37)),
                            Visibility(
                              visible: _data.setTelp1Show(2),
                              child: Expanded(
                                flex: 6,
                                child: TextFormField(
                                  autovalidate: formMCompanyAlamatChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && _data.setTelp1Mandatory(2)) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  keyboardType: TextInputType.number,
                                  inputFormatters: [
                                    WhitelistingTextInputFormatter.digitsOnly,
                                    LengthLimitingTextInputFormatter(10),
                                  ],
                                  controller: formMCompanyAlamatChangeNotif.controllerTelepon1,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      filled: true,
                                      fillColor: Colors.black12,
                                      labelText: 'Telepon 1',
                                      labelStyle: TextStyle(color: Colors.black),
                                      errorStyle: TextStyle(
                                        color: Theme.of(context).errorColor, // or any other color
                                      ),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8))),
                                  enabled: false,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Visibility(visible: _data.setTelp1AreaShow(2) || _data.setTelp1Show(2),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                      Visibility(
                        visible: _data.setTelp2AreaShow(2) || _data.setTelp2Show(2),
                        child: Row(
                          children: [
                            Visibility(
                              visible: _data.setTelp2AreaShow(2),
                              child: Expanded(
                                flex: 4,
                                child: TextFormField(
                                  autovalidate: formMCompanyAlamatChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && _data.setTelp2AreaMandatory(2)) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  keyboardType: TextInputType.number,
                                  inputFormatters: [
                                    WhitelistingTextInputFormatter.digitsOnly,
                                    LengthLimitingTextInputFormatter(10),
                                  ],
                                  controller: formMCompanyAlamatChangeNotif.controllerTelepon2Area,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      filled: true,
                                      fillColor: Colors.black12,
                                      labelText: 'Telepon 2 (Area)',
                                      labelStyle: TextStyle(color: Colors.black),
                                      errorStyle: TextStyle(
                                        color: Theme.of(context).errorColor, // or any other color
                                      ),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8))),
                                  enabled: false,
                                ),
                              ),
                            ),
                            Visibility(visible: _data.setTelp2AreaShow(2),child: SizedBox(width: MediaQuery.of(context).size.width / 37)),
                            Visibility(
                              visible: _data.setTelp2Show(2),
                              child: Expanded(
                                flex: 6,
                                child: TextFormField(
                                  autovalidate: formMCompanyAlamatChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && _data.setTelp2Mandatory(2)) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  keyboardType: TextInputType.number,
                                  inputFormatters: [
                                    WhitelistingTextInputFormatter.digitsOnly,
                                    LengthLimitingTextInputFormatter(10),
                                  ],
                                  controller: formMCompanyAlamatChangeNotif.controllerTelepon2,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      filled: true,
                                      fillColor: Colors.black12,
                                      labelText: 'Telepon 2',
                                      labelStyle: TextStyle(color: Colors.black),
                                      errorStyle: TextStyle(
                                        color: Theme.of(context).errorColor, // or any other color
                                      ),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8))),
                                  enabled: false,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Visibility(visible: _data.setTelp2AreaShow(2) || _data.setTelp2Show(2), child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                      Visibility(
                        visible: _data.setFaxAreaShow(2) || _data.setFaxShow(2),
                        child: Row(
                          children: [
                            Visibility(
                              visible: _data.setFaxAreaShow(2),
                              child: Expanded(
                                flex: 4,
                                child: TextFormField(
                                  autovalidate: formMCompanyAlamatChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && _data.setFaxAreaMandatory(2)) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  keyboardType: TextInputType.number,
                                  inputFormatters: [
                                    WhitelistingTextInputFormatter.digitsOnly,
                                    LengthLimitingTextInputFormatter(10),
                                  ],
                                  controller: formMCompanyAlamatChangeNotif.controllerFaxArea,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      filled: true,
                                      fillColor: Colors.black12,
                                      labelText: 'Fax (Area)',
                                      labelStyle: TextStyle(color: Colors.black),
                                      errorStyle: TextStyle(
                                        color: Theme.of(context).errorColor, // or any other color
                                      ),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8))),
                                  enabled: false,
                                ),
                              ),
                            ),
                            Visibility(visible: _data.setFaxAreaShow(2), child: SizedBox(width: MediaQuery.of(context).size.width / 37)),
                            Visibility(
                              visible: _data.setFaxShow(2),
                              child: Expanded(
                                flex: 6,
                                child: TextFormField(
                                  autovalidate: formMCompanyAlamatChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && _data.setFaxMandatory(2)) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  keyboardType: TextInputType.number,
                                  inputFormatters: [
                                    WhitelistingTextInputFormatter.digitsOnly,
                                    LengthLimitingTextInputFormatter(10),
                                  ],
                                  controller: formMCompanyAlamatChangeNotif.controllerFax,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      filled: true,
                                      fillColor: Colors.black12,
                                      labelText: 'Fax',
                                      labelStyle: TextStyle(color: Colors.black),
                                      errorStyle: TextStyle(
                                        color: Theme.of(context).errorColor, // or any other color
                                      ),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8))),
                                  enabled: false,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            bottomNavigationBar: BottomAppBar(
              elevation: 0.0,
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Consumer<FormMCompanyAlamatChangeNotifier>(
                    builder: (context, formMCompanyAlamatChangeNotif, _) {
                      return Row(
                        children: [
                          Expanded(
                            child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: new BorderRadius.circular(8.0)),
                                color: myPrimaryColor,
                                onPressed: () {
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => ListAddress()));
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text("EDIT ALAMAT",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500,
                                            letterSpacing: 1.25))
                                  ],
                                )
                            ),
                          ),
                          SizedBox(width: MediaQuery.of(context).size.width / 37),
                          Expanded(
                            child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: new BorderRadius.circular(8.0)),
                                color: myPrimaryColor,
                                onPressed: () {
                                  formMCompanyAlamatChangeNotif.check(context);
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text("DONE",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500,
                                            letterSpacing: 1.25))
                                  ],
                                )
                            ),
                          )
                        ],
                      );
                    },
                  )),
            ),
          ),
        );
      },
    );
  }
}
