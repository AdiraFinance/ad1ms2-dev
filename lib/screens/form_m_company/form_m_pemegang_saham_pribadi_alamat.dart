import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/form_m_company/list_pemegang_saham_pribadi_alamat.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_pemegang_saham_pribadi_alamat_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

//ga kepake
class FormMCompanyPemegangSahamPribadiAlamatProvider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<FormMCompanyPemegangSahamPribadiAlamatChangeNotifier>(
      builder: (context, formMCompanyPemegangSahamPribadiAlamatChangeNotif, _) {
        return Theme(
          data: ThemeData(
              primaryColor: Colors.black,
              accentColor: myPrimaryColor,
              fontFamily: "NunitoSans",
              primarySwatch: Colors.yellow),
          child: Scaffold(
            appBar: AppBar(
              title:
              Text("Pemegang Saham Pribadi Alamat", style: TextStyle(color: Colors.black)),
              centerTitle: true,
              backgroundColor: myPrimaryColor,
              iconTheme: IconThemeData(color: Colors.black),
            ),
            body: Form(
              key: formMCompanyPemegangSahamPribadiAlamatChangeNotif.keyForm,
              onWillPop: formMCompanyPemegangSahamPribadiAlamatChangeNotif.onBackPress,
              child: ListView(
                padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height/57,
                    horizontal: MediaQuery.of(context).size.width/37
                ),
                children: [
                  Visibility(
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressShow(5),
                    child: TextFormField(
                      autovalidate: formMCompanyPemegangSahamPribadiAlamatChangeNotif.autoValidate,
                      validator: (e) {
                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressMan(5)) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ListPemegangSahamPribadiAlamat()));
                      },
                      controller: formMCompanyPemegangSahamPribadiAlamatChangeNotif.controllerAddress,
                      style: new TextStyle(color: Colors.black),
                      decoration: new InputDecoration(
                          labelText: 'Alamat',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8))),
                      readOnly: true,
                    ),
                  ),
                  Visibility(
                      child: SizedBox(height: MediaQuery.of(context).size.height / 47),
                      visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressShow(5)
                  ),
                  Visibility(
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressTypeShow(5),
                    child: TextFormField(
                      autovalidate: formMCompanyPemegangSahamPribadiAlamatChangeNotif.autoValidate,
                      validator: (e) {
                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressTypeMan(5)) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: formMCompanyPemegangSahamPribadiAlamatChangeNotif.controllerAddressType,
                      style: new TextStyle(color: Colors.black),
                      decoration: new InputDecoration(
                          filled: true,
                          fillColor: Colors.black12,
                          labelText: 'Jenis Alamat',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8))),
                      enabled: false,
                    ),
                  ),
                  Visibility(
                      visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressTypeShow(5),
                      child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                  ),
                  Row(
                    children: [
                      Expanded(
                        flex: 5,
                        child: Visibility(
                          visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRTShow(5),
                          child: TextFormField(
                            autovalidate: formMCompanyPemegangSahamPribadiAlamatChangeNotif.autoValidate,
                            validator: (e) {
                              if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRTMan(5)) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            controller: formMCompanyPemegangSahamPribadiAlamatChangeNotif.controllerRT,
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                                filled: true,
                                fillColor: Colors.black12,
                                labelText: 'RT',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8))),
                            enabled: false,
                          ),
                        ),
                      ),
                      SizedBox(width: 8),
                      Expanded(
                          flex: 5,
                          child: Visibility(
                            visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRWShow(5),
                            child: TextFormField(
                              autovalidate: formMCompanyPemegangSahamPribadiAlamatChangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRWMan(5)) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              controller: formMCompanyPemegangSahamPribadiAlamatChangeNotif.controllerRW,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  filled: true,
                                  fillColor: Colors.black12,
                                  labelText: 'RW',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              enabled: false,
                            ),
                          ))
                    ],
                  ),
                  Visibility(
                    child: SizedBox(height: MediaQuery.of(context).size.height / 47),
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRWShow(5)
                        && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRTShow(5),
                  ),
                  Visibility(
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKelurahanShow(5),
                    child: TextFormField(
                      autovalidate: formMCompanyPemegangSahamPribadiAlamatChangeNotif.autoValidate,
                      validator: (e) {
                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKelurahanMan(5)) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: formMCompanyPemegangSahamPribadiAlamatChangeNotif.controllerKelurahan,
                      style: new TextStyle(color: Colors.black),
                      decoration: new InputDecoration(
                          filled: true,
                          fillColor: Colors.black12,
                          labelText: 'Kelurahan',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8))),
                      enabled: false,
                    ),
                  ),
                  Visibility(child: SizedBox(height: MediaQuery.of(context).size.height / 47),visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKelurahanShow(5),),
                  Visibility(
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKecamatanShow(5),
                    child: TextFormField(
                      autovalidate: formMCompanyPemegangSahamPribadiAlamatChangeNotif.autoValidate,
                      validator: (e) {
                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKecamatanMan(5)) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: formMCompanyPemegangSahamPribadiAlamatChangeNotif.controllerKecamatan,
                      style: new TextStyle(color: Colors.black),
                      decoration: new InputDecoration(
                          filled: true,
                          fillColor: Colors.black12,
                          labelText: 'Kecamatan',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8))),
                      enabled: false,
                    ),
                  ),
                  Visibility(
                    child: SizedBox(height: MediaQuery.of(context).size.height / 47),
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKecamatanShow(5),
                  ),
                  Visibility(
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKabKotShow(5),
                    child: TextFormField(
                      autovalidate: formMCompanyPemegangSahamPribadiAlamatChangeNotif.autoValidate,
                      validator: (e) {
                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKabKotMan(5)) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: formMCompanyPemegangSahamPribadiAlamatChangeNotif.controllerKota,
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.black12,
                          labelText: 'Kabupaten/Kota',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8))),
                      enabled: false,
                    ),
                  ),
                  Visibility(child: SizedBox(height: MediaQuery.of(context).size.height / 47),visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKabKotShow(5)),
                  Row(
                    children: [
                      Expanded(
                        flex: 7,
                        child:Visibility(
                          visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setProvinsiShow(5),
                          child:TextFormField(
                            autovalidate: formMCompanyPemegangSahamPribadiAlamatChangeNotif.autoValidate,
                            validator: (e) {
                              if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setProvinsiMan(5)) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            controller: formMCompanyPemegangSahamPribadiAlamatChangeNotif.controllerProvinsi,
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                                filled: true,
                                fillColor: Colors.black12,
                                labelText: 'Provinsi',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8))),
                            enabled: false,
                          ),
                        ),
                      ),
                      SizedBox(width: 8),
                      Expanded(
                        flex: 3,
                        child: Visibility(
                          visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKodePosShow(5),
                          child: TextFormField(
                            autovalidate: formMCompanyPemegangSahamPribadiAlamatChangeNotif.autoValidate,
                            validator: (e) {
                              if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKodePosMan(5)) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            controller:
                            formMCompanyPemegangSahamPribadiAlamatChangeNotif.controllerPostalCode,
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                                filled: true,
                                fillColor: Colors.black12,
                                labelText: 'Kode Pos',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8))),
                            enabled: false,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Visibility(
                    child: SizedBox(height: MediaQuery.of(context).size.height / 47),
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setProvinsiShow(5)
                        && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKodePosShow(5),
                  ),
                  Row(
                    children: [
                      Expanded(
                        flex: 4,
                        child: Visibility(
                          visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setTelp1AreaShow(5),
                          child: TextFormField(
                            autovalidate: formMCompanyPemegangSahamPribadiAlamatChangeNotif.autoValidate,
                            validator: (e) {
                              if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setTelp1AreaMan(5)) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              WhitelistingTextInputFormatter.digitsOnly,
                              LengthLimitingTextInputFormatter(10),
                            ],
                            controller: formMCompanyPemegangSahamPribadiAlamatChangeNotif.controllerTeleponArea,
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                                filled: true,
                                fillColor: Colors.black12,
                                labelText: 'Telepon (Area)',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8))),
                            enabled: false,
                          ),
                        ),
                      ),
                      SizedBox(width: MediaQuery.of(context).size.width / 37),
                      Expanded(
                        flex: 6,
                        child: Visibility(
                          visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setTelp1Show(5),
                          child: TextFormField(
                            autovalidate: formMCompanyPemegangSahamPribadiAlamatChangeNotif.autoValidate,
                            validator: (e) {
                              if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setTelp1Man(5)) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              WhitelistingTextInputFormatter.digitsOnly,
                              LengthLimitingTextInputFormatter(10),
                            ],
                            controller: formMCompanyPemegangSahamPribadiAlamatChangeNotif.controllerTelepon,
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                                filled: true,
                                fillColor: Colors.black12,
                                labelText: 'Telepon',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8))),
                            enabled: false,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            bottomNavigationBar: BottomAppBar(
              elevation: 0.0,
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Consumer<FormMCompanyPemegangSahamPribadiAlamatChangeNotifier>(
                    builder: (context, formMCompanyPemegangSahamPribadiAlamatChangeNotif, _) {
                      return RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(8.0)),
                          color: myPrimaryColor,
                          onPressed: () {
                            formMCompanyPemegangSahamPribadiAlamatChangeNotif.check(context);
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("DONE",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                      letterSpacing: 1.25))
                            ],
                          ));
                    },
                  )),
            ),
          ),
        );
      },
    );
  }
}
