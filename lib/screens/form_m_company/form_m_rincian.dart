import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/form_m_company_rincian_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_company_rincian_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class FormMCompanyRincianProvider extends StatefulWidget {
  @override
  _FormMCompanyRincianProviderState createState() => _FormMCompanyRincianProviderState();
}

class _FormMCompanyRincianProviderState extends State<FormMCompanyRincianProvider> {
  Future<void> _setPreference;

  @override
  void initState() {
    super.initState();
    _setPreference = Provider.of<FormMCompanyRincianChangeNotifier>(context, listen: false).setPreference(context);
  }
  @override
  Widget build(BuildContext context) {
    // Provider.of<FormMCompanyRincianChangeNotifier>(context, listen: false).setDataSQLite(context);
    Provider.of<FormMCompanyRincianChangeNotifier>(context, listen: false).checkDataDakor();
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        primaryColor: Colors.black,
        accentColor: myPrimaryColor,
        primarySwatch: primaryOrange,
      ),
      child: FutureBuilder(
        future: _setPreference,
        builder: (context, snapshot) {
          return Consumer<FormMCompanyRincianChangeNotifier>(
            builder: (context, formMCompanyRincianChangeNotif, _) {
              return Scaffold(
                key: formMCompanyRincianChangeNotif.scaffoldKey,
                appBar: AppBar(
                  title:
                  Text("Rincian Lembaga", style: TextStyle(color: Colors.black)),
                  centerTitle: true,
                  backgroundColor: myPrimaryColor,
                  iconTheme: IconThemeData(color: Colors.black),
                ),
                body: formMCompanyRincianChangeNotif.loadDataDedup
                    ?
                Center(child: CircularProgressIndicator())
                    :
                Form(
                  key: formMCompanyRincianChangeNotif.keyForm,
                  onWillPop: formMCompanyRincianChangeNotif.onBackPress,
                  child: ListView(
                    padding: EdgeInsets.symmetric(
                        vertical: MediaQuery.of(context).size.height/57,
                        horizontal: MediaQuery.of(context).size.width/37
                    ),
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Visibility(
                            visible: formMCompanyRincianChangeNotif.isCompanyTypeShow,
                            child: DropdownButtonFormField<TypeInstitutionModel>(
                              autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                              validator: (e) {
                                if (e == null && formMCompanyRincianChangeNotif.isCompanyTypeMandatory) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              value: formMCompanyRincianChangeNotif.typeInstitutionSelected,
                              onChanged: (value) {
                                formMCompanyRincianChangeNotif.typeInstitutionSelected = value;
                              },
                              onTap: () {
                                FocusManager.instance.primaryFocus.unfocus();
                              },
                              decoration: InputDecoration(
                                labelText: "Jenis Lembaga",
                                border: OutlineInputBorder(),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isCompanyTypeDakor ? Colors.purple : Colors.grey)),
                                disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isCompanyTypeDakor ? Colors.purple : Colors.grey)),
                                contentPadding: EdgeInsets.symmetric(horizontal: 10),
                              ),
                              items: formMCompanyRincianChangeNotif.listTypeInstitution.map((value) {
                                return DropdownMenuItem<TypeInstitutionModel>(
                                  value: value,
                                  child: Text(
                                    value.PARA_NAME,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                );
                              }).toList(),
                            ),
                          ),
                          Visibility(visible: formMCompanyRincianChangeNotif.isCompanyTypeShow, child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          Visibility(
                            visible: formMCompanyRincianChangeNotif.isProfileShow,
                            child: DropdownButtonFormField<ProfilModel>(
                              autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                              validator: (e) {
                                if (e == null && formMCompanyRincianChangeNotif.isProfileMandatory) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              value: formMCompanyRincianChangeNotif.profilSelected,
                              onChanged: (value) {
                                formMCompanyRincianChangeNotif.profilSelected = value;
                              },
                              onTap: () {
                                FocusManager.instance.primaryFocus.unfocus();
                              },
                              decoration: InputDecoration(
                                labelText: "Profil",
                                border: OutlineInputBorder(),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isProfileDakor ? Colors.purple : Colors.grey)),
                                disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isProfileDakor ? Colors.purple : Colors.grey)),
                                contentPadding: EdgeInsets.symmetric(horizontal: 10),
                              ),
                              items: formMCompanyRincianChangeNotif.listProfil.map((value) {
                                return DropdownMenuItem<ProfilModel>(
                                  value: value,
                                  child: Text(
                                    value.text,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                );
                              }).toList(),
                            ),
                          ),
                          Visibility(visible: formMCompanyRincianChangeNotif.isProfileShow,child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          Visibility(
                            visible: formMCompanyRincianChangeNotif.isCompanyNameShow,
                            child: TextFormField(
                              autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty && formMCompanyRincianChangeNotif.isCompanyNameMandatory) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              controller: formMCompanyRincianChangeNotif.controllerInstitutionName,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                labelText: 'Nama Lembaga',
                                filled: formMCompanyRincianChangeNotif.enableDataDedup ? false : true,
                                fillColor: Colors.black12,
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isCompanyNameDakor ? Colors.purple : Colors.grey)),
                                disabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isCompanyNameDakor ? Colors.purple : Colors.grey)),
                                contentPadding: EdgeInsets.symmetric(horizontal: 10),
                              ),
                              keyboardType: TextInputType.text,
                              textCapitalization: TextCapitalization.characters,
                              enabled: formMCompanyRincianChangeNotif.enableDataDedup,
                              inputFormatters: [
                                FilteringTextInputFormatter.allow(RegExp('[a-zA-Z  \`0-9]')),
                              ],
                            ),
                          ),
                          Visibility(visible: formMCompanyRincianChangeNotif.isCompanyNameShow,child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          Visibility(
                            visible: formMCompanyRincianChangeNotif.isHaveNPWPShow,
                            child: Text(
                              "Punya NPWP ?",
                              style: TextStyle(
                                  color: formMCompanyRincianChangeNotif.isHaveNPWPDakor ? Colors.purple : Colors.black,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w700,
                                  letterSpacing: 0.15),
                            ),
                          ),
                          // Visibility(visible: formMCompanyRincianChangeNotif.isHaveNPWPShow,child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          Visibility(
                            visible: formMCompanyRincianChangeNotif.isHaveNPWPShow,
                            child: Row(
                              children: [
                                Row(
                                  children: [
                                    Radio(
                                        activeColor: primaryOrange,
                                        value: 1,
                                        groupValue:
                                        formMCompanyRincianChangeNotif.radioValueIsHaveNPWP,
                                        onChanged: (value) {
                                          formMCompanyRincianChangeNotif.radioValueIsHaveNPWP =
                                              value;
                                        }),
                                    Text("Ya")
                                  ],
                                ),
                                Row(
                                  children: [
                                    Radio(
                                        activeColor: primaryOrange,
                                        value: 0,
                                        groupValue:
                                        formMCompanyRincianChangeNotif.radioValueIsHaveNPWP,
                                        onChanged: (value) {
                                          // formMCompanyRincianChangeNotif.radioValueIsHaveNPWP = value;
                                        }),
                                    Text("Tidak")
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Visibility(visible: formMCompanyRincianChangeNotif.isHaveNPWPShow,child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          formMCompanyRincianChangeNotif.radioValueIsHaveNPWP == 0
                              ? SizedBox(height: 0.0)
                              : Column(
                            children: [
                              Visibility(
                                visible: formMCompanyRincianChangeNotif.isNoNPWPShow,
                                child: TextFormField(
                                  autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && formMCompanyRincianChangeNotif.isNoNPWPMandatory) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  inputFormatters: [
                                    WhitelistingTextInputFormatter.digitsOnly,
                                    FilteringTextInputFormatter.allow(RegExp('[0-9]')),
                                    LengthLimitingTextInputFormatter(15)
                                  ],
                                  controller: formMCompanyRincianChangeNotif.controllerNPWP,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      labelText: 'NPWP',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: formMCompanyRincianChangeNotif.enableDataDedup ? false : true,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isNoNPWPDakor ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isNoNPWPDakor ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                  ),
                                  keyboardType: TextInputType.number,
                                  enabled: formMCompanyRincianChangeNotif.enableDataDedup,
                                  // enabled: false,
                                ),
                              ),
                              Visibility(visible: formMCompanyRincianChangeNotif.isNoNPWPShow,child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              Visibility(
                                visible: formMCompanyRincianChangeNotif.isNameNPWPShow,
                                child: TextFormField(
                                  autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && formMCompanyRincianChangeNotif.isNameNPWPMandatory) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  enabled: (formMCompanyRincianChangeNotif.lastKnownState == "PAC" || formMCompanyRincianChangeNotif.lastKnownState == "IA") ? false : true,
                                  controller: formMCompanyRincianChangeNotif.controllerFullNameNPWP,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      labelText: 'Nama Lengkap Sesuai NPWP',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: (formMCompanyRincianChangeNotif.lastKnownState == "PAC" || formMCompanyRincianChangeNotif.lastKnownState == "IA") ? true : false,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isNameNPWPDakor ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isNameNPWPDakor ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                  ),
                                  keyboardType: TextInputType.text,
                                  textCapitalization: TextCapitalization.characters,
                                  inputFormatters: [
                                    FilteringTextInputFormatter.allow(RegExp('[a-zA-Z  \`0-9]')),
                                  ],
                                ),
                              ),
                              Visibility(visible: formMCompanyRincianChangeNotif.isNameNPWPShow,child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              Visibility(
                                visible: formMCompanyRincianChangeNotif.isTypeNPWPShow,
                                child: DropdownButtonFormField<JenisNPWPModel>(
                                  autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e == null && formMCompanyRincianChangeNotif.isTypeNPWPMandatory) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  value: formMCompanyRincianChangeNotif.typeNPWPSelected,
                                  decoration: InputDecoration(
                                    labelText: "Jenis NPWP",
                                    border: OutlineInputBorder(),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isTypeNPWPDakor ? Colors.purple : Colors.grey)),
                                    disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isTypeNPWPDakor ? Colors.purple : Colors.grey)),
                                    contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                  ),
                                  items: formMCompanyRincianChangeNotif.listTypeNPWP.map((data) {
                                    return DropdownMenuItem<JenisNPWPModel>(
                                      value: data,
                                      child: Text(
                                        data.text,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    );
                                  }).toList(),
                                  onChanged: (newVal) {
                                    formMCompanyRincianChangeNotif.typeNPWPSelected = newVal;
                                  },
                                  onTap: () {
                                    FocusManager.instance.primaryFocus.unfocus();
                                  },
                                ),
                              ),
                              Visibility(visible: formMCompanyRincianChangeNotif.isTypeNPWPShow,child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              Visibility(
                                visible: formMCompanyRincianChangeNotif.isPKPSignShow,
                                child: DropdownButtonFormField<SignPKPModel>(
                                  autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e == null && formMCompanyRincianChangeNotif.isPKPSignMandatory) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  value: formMCompanyRincianChangeNotif.signPKPSelected,
                                  decoration: InputDecoration(
                                    labelText: "Tanda PKP",
                                    border: OutlineInputBorder(),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isPKPSignDakor ? Colors.purple : Colors.grey)),
                                    disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isPKPSignDakor ? Colors.purple : Colors.grey)),
                                    contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                  ),
                                  items: formMCompanyRincianChangeNotif.listSignPKP.map((data) {
                                    return DropdownMenuItem<SignPKPModel>(
                                      value: data,
                                      child: Text(
                                        data.text,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    );
                                  }).toList(),
                                  onChanged: (newVal) {
                                    formMCompanyRincianChangeNotif.signPKPSelected = newVal;
                                  },
                                  onTap: () {
                                    FocusManager.instance.primaryFocus.unfocus();
                                  },
                                ),
                              ),
                              Visibility(visible: formMCompanyRincianChangeNotif.isPKPSignShow,child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                              Visibility(
                                visible: formMCompanyRincianChangeNotif.isAddressNPWPShow,
                                child: TextFormField(
                                  autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && formMCompanyRincianChangeNotif.isAddressNPWPMandatory) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  controller: formMCompanyRincianChangeNotif.controllerNPWPAddress,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                    labelText: 'Alamat NPWP',
                                    labelStyle: TextStyle(color: Colors.black),
                                    filled: formMCompanyRincianChangeNotif.enableDataDedup ? false : true,
                                    fillColor: Colors.black12,
                                    border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isAddressNPWPDakor ? Colors.purple : Colors.grey)),
                                    disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isAddressNPWPDakor ? Colors.purple : Colors.grey)),
                                    contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                  ),
                                  keyboardType: TextInputType.text,
                                  enabled: formMCompanyRincianChangeNotif.enableDataDedup,
                                  textCapitalization: TextCapitalization.characters,
                                ),
                              ),
                              Visibility(visible: formMCompanyRincianChangeNotif.isAddressNPWPShow,child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                            ],
                          ),
                          // SizedBox(height: MediaQuery.of(context).size.height / 47),
                          Visibility(
                            visible: formMCompanyRincianChangeNotif.isEstablishDateShow,
                            child: TextFormField(
                              autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty && formMCompanyRincianChangeNotif.isEstablishDateMandatory) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              onTap: () {
                                FocusManager.instance.primaryFocus.unfocus();
                                formMCompanyRincianChangeNotif.selectDateEstablishment(context);
                              },
                              controller: formMCompanyRincianChangeNotif.controllerDateEstablishment,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  labelText: 'Tanggal Pendirian',
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: formMCompanyRincianChangeNotif.enableDataDedup ? false : true,
                                  fillColor: Colors.black12,
                                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isEstablishDateDakor ? Colors.purple : Colors.grey))
                              ),
                              // readOnly: true,
                              enabled: formMCompanyRincianChangeNotif.enableDataDedup,
                            ),
                          ),
                          Visibility(visible: formMCompanyRincianChangeNotif.isEstablishDateShow,child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          Visibility(
                            visible: formMCompanyRincianChangeNotif.isSectorEconomicShow,
                            child: FocusScope(
                                node: FocusScopeNode(),
                                child: TextFormField(
                                  autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && formMCompanyRincianChangeNotif.isSectorEconomicMandatory) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  onTap: () {
                                    FocusManager.instance.primaryFocus.unfocus();
                                    formMCompanyRincianChangeNotif.searchSectorEconomic(context);
                                  },
                                  enabled: (formMCompanyRincianChangeNotif.lastKnownState == "PAC" || formMCompanyRincianChangeNotif.lastKnownState == "IA") ? false : true,
                                  controller:
                                  formMCompanyRincianChangeNotif.controllerSectorEconomic,
                                  style: TextStyle(color: Colors.black),
                                  decoration: InputDecoration(
                                      labelText: 'Sektor Ekonomi',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: (formMCompanyRincianChangeNotif.lastKnownState == "PAC" || formMCompanyRincianChangeNotif.lastKnownState == "IA") ? true : false,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isSectorEconomicDakor ? Colors.purple : Colors.grey))
                                  ),
                                )),
                          ),
                          Visibility(visible: formMCompanyRincianChangeNotif.isSectorEconomicShow,child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          Visibility(
                            visible: formMCompanyRincianChangeNotif.isBusinessFieldShow,
                            child: FocusScope(
                                node: FocusScopeNode(),
                                child: TextFormField(
                                  autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && formMCompanyRincianChangeNotif.isBusinessFieldMandatory) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  onTap: () {
                                    FocusManager.instance.primaryFocus.unfocus();
                                    formMCompanyRincianChangeNotif.searchBusinesField(context);
                                  },
                                  controller:
                                  formMCompanyRincianChangeNotif.controllerBusinessField,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      labelText: 'Lapangan Usaha',
                                      labelStyle: TextStyle(color: Colors.black),
                                      filled: (formMCompanyRincianChangeNotif.lastKnownState == "PAC" || formMCompanyRincianChangeNotif.lastKnownState == "IA") ? true : false,
                                      fillColor: Colors.black12,
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isBusinessFieldDakor ? Colors.purple : Colors.grey)),
                                      disabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isBusinessFieldDakor ? Colors.purple : Colors.grey)),
                                      contentPadding: EdgeInsets.symmetric(horizontal: 10)
                                  ),
                                  enabled: (formMCompanyRincianChangeNotif.lastKnownState == "PAC" || formMCompanyRincianChangeNotif.lastKnownState == "IA") ? false : formMCompanyRincianChangeNotif.controllerSectorEconomic.text == "" ? false : true,
                                )),
                          ),
                          Visibility(visible: formMCompanyRincianChangeNotif.isBusinessFieldShow,child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          Visibility(
                              visible: formMCompanyRincianChangeNotif.isLocationStatusShow,
                              child: TextFormField(
                                readOnly: true,
                                autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty && formMCompanyRincianChangeNotif.isLocationStatusMandatory) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                onTap: (){
                                  FocusManager.instance.primaryFocus.unfocus();
                                  formMCompanyRincianChangeNotif.searchStatusLocation(context);
                                },
                                controller: formMCompanyRincianChangeNotif.controllerStatusLocation,
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                  labelText: 'Status Lokasi',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isLocationStatusDakor ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isLocationStatusDakor ? Colors.purple : Colors.grey)),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10)
                                ),
                                keyboardType: TextInputType.text,
                                textCapitalization: TextCapitalization.characters,
                              )
                          ),
                          Visibility(visible: formMCompanyRincianChangeNotif.isLocationStatusShow,child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          Visibility(
                              visible: formMCompanyRincianChangeNotif.isBusinessLocationShow,
                              child: TextFormField(
                                readOnly: true,
                                autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                                validator: (e) {
                                  if (e.isEmpty && formMCompanyRincianChangeNotif.isBusinessLocationMandatory) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                onTap: (){
                                  FocusManager.instance.primaryFocus.unfocus();
                                  formMCompanyRincianChangeNotif.searchBusinessLocation(context);
                                },
                                controller: formMCompanyRincianChangeNotif.controllerBusinessLocation,
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                  labelText: 'Lokasi Usaha',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isBusinessLocationDakor ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isBusinessLocationDakor ? Colors.purple : Colors.grey)),
                                    contentPadding: EdgeInsets.symmetric(horizontal: 10)
                                ),
                                keyboardType: TextInputType.text,
                                textCapitalization: TextCapitalization.characters,
                              )
                          ),
                          Visibility(visible: formMCompanyRincianChangeNotif.isBusinessLocationShow,child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          Visibility(
                            visible: formMCompanyRincianChangeNotif.isTotalEmpShow,
                            child: TextFormField(
                              autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty && formMCompanyRincianChangeNotif.isTotalEmpMandatory) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              inputFormatters: [
                                WhitelistingTextInputFormatter.digitsOnly,
                                // LengthLimitingTextInputFormatter(10),
                              ],
                              controller: formMCompanyRincianChangeNotif.controllerTotalEmployees,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  labelText: 'Total Pegawai',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isTotalEmpDakor ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isTotalEmpDakor ? Colors.purple : Colors.grey)),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10)
                              ),
                              keyboardType: TextInputType.number,
                            ),
                          ),
                          Visibility(visible: formMCompanyRincianChangeNotif.isTotalEmpShow,child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          Visibility(
                            visible: formMCompanyRincianChangeNotif.isLamaUsahaShow,
                            child: TextFormField(
                              autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty && formMCompanyRincianChangeNotif.isLamaUsahaMandatory) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              inputFormatters: [
                                WhitelistingTextInputFormatter.digitsOnly,
                                // LengthLimitingTextInputFormatter(10),
                              ],
                              controller: formMCompanyRincianChangeNotif.controllerLamaUsahaBerjalan,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  labelText: 'Lama Usaha/Kerja Berjalan (bln)',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isLamaUsahaDakor ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isLamaUsahaDakor ? Colors.purple : Colors.grey)),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10)
                              ),
                              keyboardType: TextInputType.number,
                            ),
                          ),
                          Visibility(visible: formMCompanyRincianChangeNotif.isLamaUsahaShow,child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          Visibility(
                            visible: formMCompanyRincianChangeNotif.isTotalLamaUsahaShow,
                            child: TextFormField(
                              autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty && formMCompanyRincianChangeNotif.isTotalLamaUsahaMandatory) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              inputFormatters: [
                                WhitelistingTextInputFormatter.digitsOnly,
                                // LengthLimitingTextInputFormatter(10),
                              ],
                              enabled: (formMCompanyRincianChangeNotif.lastKnownState == "PAC" || formMCompanyRincianChangeNotif.lastKnownState == "IA") ? false : true,
                              controller: formMCompanyRincianChangeNotif.controllerTotalLamaUsahaBerjalan,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  labelText: 'Total Lama Usaha/Kerja Berjalan (bln)',
                                  filled: formMCompanyRincianChangeNotif.lastKnownState == "PAC" || formMCompanyRincianChangeNotif.lastKnownState == "IA" ? true : false,
                                  fillColor: Colors.black12,
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isTotalLamaUsahaDakor ? Colors.purple : Colors.grey)),
                                  disabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: formMCompanyRincianChangeNotif.isTotalLamaUsahaDakor ? Colors.purple : Colors.grey)),
                                  contentPadding: EdgeInsets.symmetric(horizontal: 10)
                              ),
                              keyboardType: TextInputType.number,
                            ),
                          ),
                          Visibility(visible: formMCompanyRincianChangeNotif.isTotalLamaUsahaShow,child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                          // Text(
                          //   "Buka Rekening ?",
                          //   style: TextStyle(
                          //       fontSize: 16,
                          //       fontWeight: FontWeight.w700,
                          //       letterSpacing: 0.15),
                          // ),
                          // SizedBox(height: MediaQuery.of(context).size.height / 47),
                          // Row(
                          //   children: [
                          //     Row(
                          //       children: [
                          //         Radio(
                          //             activeColor: primaryOrange,
                          //             value: 0,
                          //             groupValue:
                          //             formMCompanyRincianChangeNotif.radioValueOpenAccount,
                          //             onChanged: (value) {
                          //               formMCompanyRincianChangeNotif.radioValueOpenAccount = value;
                          //             }),
                          //         Text("Ya")
                          //       ],
                          //     ),
                          //     Row(
                          //       children: [
                          //         Radio(
                          //             activeColor: primaryOrange,
                          //             value: 1,
                          //             groupValue:
                          //             formMCompanyRincianChangeNotif.radioValueOpenAccount,
                          //             onChanged: (value) {
                          //               formMCompanyRincianChangeNotif.radioValueOpenAccount = value;
                          //             }),
                          //         Text("Tidak")
                          //       ],
                          //     ),
                          //   ],
                          // ),
                          // SizedBox(height: MediaQuery.of(context).size.height / 47),
                          // formMCompanyRincianChangeNotif.radioValueOpenAccount == 1
                          //     ? SizedBox(height: 0.0)
                          //     :
                          // TextFormField(
                          //   autovalidate: formMCompanyRincianChangeNotif.autoValidate,
                          //   validator: (e) {
                          //     if (e.isEmpty) {
                          //       return "Tidak boleh kosong";
                          //     } else {
                          //       return null;
                          //     }
                          //   },
                          //   inputFormatters: [
                          //     WhitelistingTextInputFormatter.digitsOnly,
                          //     // LengthLimitingTextInputFormatter(10),
                          //   ],
                          //   controller: formMCompanyRincianChangeNotif.controllerFormNumber,
                          //   style: new TextStyle(color: Colors.black),
                          //   decoration: new InputDecoration(
                          //       labelText: 'Nomor Form',
                          //       labelStyle: TextStyle(color: Colors.black),
                          //       border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))),
                          //   keyboardType: TextInputType.number,
                          // ),
                        ],
                      )
                    ],
                  ),
                ),
                bottomNavigationBar: BottomAppBar(
                  elevation: 0.0,
                  child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Consumer<FormMCompanyRincianChangeNotifier>(
                        builder: (context, formMCompanyRincianChangeNotif, _) {
                          return RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(8.0)),
                              color: myPrimaryColor,
                              onPressed: () {
                                formMCompanyRincianChangeNotif.check(context);
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text("DONE",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500,
                                          letterSpacing: 1.25))
                                ],
                              ));
                        },
                      )),
                ),
              );
            },
          );
        }
      )
    );
  }
}
