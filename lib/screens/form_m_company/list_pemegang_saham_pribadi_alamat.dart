import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/form_m_company/add_pemegang_saham_pribadi_alamat.dart';
import 'package:ad1ms2_dev/shared/add_pemegang_saham_pribadi_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_add_pemegang_saham_pribadi_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_pemegang_saham_pribadi_alamat_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ListPemegangSahamPribadiAlamat extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(fontFamily: "NunitoSans"),
      child: Scaffold(
        appBar: AppBar(
          title: Text("List Alamat Korespondensi",
            style: TextStyle(color: Colors.black)),
          centerTitle: true,
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: [
          //   IconButton(
          //       icon: Icon(Icons.info_outline),
          //       onPressed: (){
          //         Provider.of<AddPemegangSahamPribadiChangeNotifier>(context, listen: true).iconShowDialog(context);
          //       })
          ],
        ),
        bottomSheet: Container(
            padding: EdgeInsets.all(8),
            height: 100,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                    "∙ Pilih salah satu alamat untuk mengubah detail alamat",
                    style: TextStyle(fontWeight: FontWeight.w400)
                ),
                Text.rich(
                  TextSpan(
                    children: [
                      TextSpan(
                          text: "∙ Klik tanda",
                          style: TextStyle(fontWeight: FontWeight.w400)
                      ),
                      WidgetSpan(
                        child: Icon(Icons.more_vert, color: Colors.grey),
                      ),
                      TextSpan(
                          text: "pada salah satu alamat untuk memilih sebagai Alamat Korespondensi atau menghapus alamat",
                          style: TextStyle(fontWeight: FontWeight.w400)
                      )
                    ],
                  ),
                ),
              ],
            )
        ),
        body: Consumer<AddPemegangSahamPribadiChangeNotifier>(
          builder: (context, addPemegangSahamPribadiChangeNotifier, _) {
            return addPemegangSahamPribadiChangeNotifier.listPemegangSahamPribadiAddress.isEmpty
                ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset("img/alamat_kosong.png", height: MediaQuery.of(context).size.height / 9,),
                  SizedBox(height: MediaQuery.of(context).size.height / 47,),
                  Text("Tambah Alamat", style: TextStyle(color: Colors.grey, fontSize: 16),)
                ],
              ),
            )
                :
            ListView.builder(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 77,
                  horizontal: MediaQuery.of(context).size.width / 47),
              itemBuilder: (context, index) {
                return InkWell(
                  // onLongPress: () {
                  //   addPemegangSahamPribadiChangeNotifier.selectedIndex = index;
                  //   addPemegangSahamPribadiChangeNotifier.controllerAddress.clear();
                  //   addPemegangSahamPribadiChangeNotifier.setAddress(addPemegangSahamPribadiChangeNotifier.listPemegangSahamPribadiAddress[index]);
                  //   Navigator.pop(context);
                  // },
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => CompanyAddPemegangSahamPribadiAlamat(
                                  flag: 1,
                                  index: index,
                                  addressModel: addPemegangSahamPribadiChangeNotifier.listPemegangSahamPribadiAddress[index],
                                  typeAddress: 5,
                                )
                        )
                    );
                    // Navigator.push(
                    //   context,
                    //   MaterialPageRoute(
                    //     builder: (context) => ChangeNotifierProvider(
                    //       create: (context) => FormMAddAddressIndividuChangeNotifier(),
                    //       child: CompanyAddPemegangSahamPribadiAlamat(
                    //           flag: 1,
                    //           index: index,
                    //           addressModel: addPemegangSahamPribadiChangeNotifier.listPemegangSahamPribadiAddress[index],
                    //           typeAddress: 5,
                    //       )
                    //     )
                    //   )
                    // );
                  },
                  child: addPemegangSahamPribadiChangeNotifier.listPemegangSahamPribadiAddress[index].active == 0 ?
                  Card(
                    shape: addPemegangSahamPribadiChangeNotifier.selectedIndex == index
                        ? RoundedRectangleBorder(
                          side: BorderSide(color: primaryOrange, width: 2),
                          borderRadius: BorderRadius.all(Radius.circular(4)))
                        : null,
                    elevation: 3.3,
                    child: Padding(
                      padding: const EdgeInsets.all(16),
                        child: Stack(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("${addPemegangSahamPribadiChangeNotifier.listPemegangSahamPribadiAddress[index].jenisAlamatModel.KODE} - ${addPemegangSahamPribadiChangeNotifier.listPemegangSahamPribadiAddress[index].jenisAlamatModel.DESKRIPSI}",
                                    style: TextStyle(fontWeight: FontWeight.bold)
                                ),
                                SizedBox(
                                  height:
                                  MediaQuery.of(context).size.height /
                                      97,
                                ),
                                Text("${addPemegangSahamPribadiChangeNotifier.listPemegangSahamPribadiAddress[index].address}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),
                                ),
                                Text("${addPemegangSahamPribadiChangeNotifier.listPemegangSahamPribadiAddress[index].kelurahanModel.KEC_ID} - ${addPemegangSahamPribadiChangeNotifier.listPemegangSahamPribadiAddress[index].kelurahanModel.KEC_NAME},",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),
                                ),
                                Text("${addPemegangSahamPribadiChangeNotifier.listPemegangSahamPribadiAddress[index].kelurahanModel.KABKOT_ID} - ${addPemegangSahamPribadiChangeNotifier.listPemegangSahamPribadiAddress[index].kelurahanModel.KABKOT_NAME},",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),
                                ),
                                Text("${addPemegangSahamPribadiChangeNotifier.listPemegangSahamPribadiAddress[index].kelurahanModel.PROV_ID} - ${addPemegangSahamPribadiChangeNotifier.listPemegangSahamPribadiAddress[index].kelurahanModel.PROV_NAME}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),
                                ),
                                Text("${addPemegangSahamPribadiChangeNotifier.listPemegangSahamPribadiAddress[index].kelurahanModel.ZIPCODE}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),),
                                SizedBox(
                                  height:
                                  MediaQuery.of(context).size.height /
                                      97,
                                ),
                                Text("${addPemegangSahamPribadiChangeNotifier.listPemegangSahamPribadiAddress[index].areaCode} ${addPemegangSahamPribadiChangeNotifier.listPemegangSahamPribadiAddress[index].phone}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),)
                              ],
                            ),
                            Align(
                                alignment: Alignment.topRight,
                                child:
                                // formMinfoAlamat.listAlamatKorespondensi[index].jenisAlamatModel.KODE != "03"
                                //     ? formMinfoAlamat.listAlamatKorespondensi[index].isSameWithIdentity
                                //     ? SizedBox()
                                //     :
                                GestureDetector(
                                  // onTap: () {formMinfoAlamat.deleteAlamatKorespondensi(context, index);},
                                    onTap: () {addPemegangSahamPribadiChangeNotifier.moreDialog(context, index);},
                                    child: Icon(Icons.more_vert, color: Colors.grey)
                                )
                              // IconButton(icon: Icon(Icons.delete, color: Colors.red),
                              //     onPressed: () {
                              //       // formMinfoAlamat.deleteAlamatKorespondensi(index);
                              //     })
                              //     : SizedBox(),
                            )
//                            Align(
//                              alignment: Alignment.topRight,
//                              child: addPemegangSahamPribadiChangeNotifier.listPemegangSahamPribadiAddress[index].jenisAlamatModel.KODE == "03" || addPemegangSahamPribadiChangeNotifier.listPemegangSahamPribadiAddress[index].jenisAlamatModel.KODE == "01"
//                                  ? SizedBox()
//                                  : GestureDetector(
//                                      onTap: () {addPemegangSahamPribadiChangeNotifier.deletePemegangSahamPribadiAddress(context, index);},
//                                      child: Icon(Icons.delete, color: Colors.red)
//                                  )
//                            )
                          ],
                      ),
                    ),
                  )
                      :
                  SizedBox()
                );
              },
              itemCount: addPemegangSahamPribadiChangeNotifier.listPemegangSahamPribadiAddress.length,
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).clearData();
            Navigator.push(context,
                MaterialPageRoute(
                    builder: (context) => CompanyAddPemegangSahamPribadiAlamat(
                          flag: 0,
                          index: null,
                          addressModel: null,
                          typeAddress: 5,
                        )
                )
            );
                // .then((value) => Provider.of<AddPemegangSahamPribadiChangeNotifier>(context, listen: false).isShowDialog(context));
            // Navigator.push(
            //   context,
            //   MaterialPageRoute(
            //       builder: (context) => ChangeNotifierProvider(
            //           create: (context) => FormMAddAddressIndividuChangeNotifier(),
            //           child: CompanyAddPemegangSahamPribadiAlamat(
            //             flag: 0,
            //             index: null,
            //             addressModel: null,
            //             typeAddress: 5,
            //           )
            //       )
            //   )
            // ).then((value) => Provider.of<AddPemegangSahamPribadiChangeNotifier>(context, listen: false).isShowDialog(context));
          },
          backgroundColor: myPrimaryColor,
          child: Icon(Icons.add, color: Colors.black),
        ),
      ),
    );
  }
}
