import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/form_m_company/add_pemegang_saham_lembaga.dart';
import 'package:ad1ms2_dev/shared/add_pemegang_saham_lembaga_change_notif.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/pemegang_saham_lembaga_change_notif.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ListPemegangSahamLembaga extends StatefulWidget {
  @override
  _ListPemegangSahamLembagaState createState() => _ListPemegangSahamLembagaState();
}

class _ListPemegangSahamLembagaState extends State<ListPemegangSahamLembaga> {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        primaryColor: Colors.black,
        primarySwatch: primaryOrange,
        accentColor: myPrimaryColor,
      ),
      child: Scaffold(
        appBar: AppBar(
          title:
          Text("List Pemegang Saham Lembaga", style: TextStyle(color: Colors.black)),
          centerTitle: true,
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        body: Consumer<PemegangSahamLembagaChangeNotifier>(
          builder: (context, _provider, _) {
            return Form(
              onWillPop: _provider.onBackPress,
              child: _provider.listPemegangSahamLembaga.isEmpty
                  ?
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Tambah Pemegang Saham Lembaga", style: TextStyle(color: Colors.grey, fontSize: 16),)
                  ],
                ),
              )
                  :
              ListView.builder(
                padding: EdgeInsets.symmetric(
                    horizontal: MediaQuery.of(context).size.width / 27,
                    vertical: MediaQuery.of(context).size.height / 47),
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      Provider.of<AddPemegangSahamLembagaChangeNotif>(context, listen: false).clearDataPemegangSahamKelembagaan();
                      Navigator.push(context, MaterialPageRoute(
                            builder: (context) => AddPemegangSahamLembaga(
                              flag: 1,
                              model: _provider.listPemegangSahamLembaga[index],
                              index: index,
                            )
                          )
                      );
                    },
                    child: Card(
                      shape: _provider.listPemegangSahamLembaga[index].isEdit
                          ?
                      RoundedRectangleBorder(
                          side: BorderSide(color: Colors.purple, width: 2),
                          borderRadius: BorderRadius.all(Radius.circular(4))
                      )
                          :
                      null,
                      elevation: 3.3,
                      child: Padding(
                        padding: const EdgeInsets.all(13.0),
                        child: Stack(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Expanded(
                                        child: Text("Tipe Institusi"),
                                        flex: 5
                                    ),
                                    Text(" : "),
                                    Expanded(
                                        child: Text(_provider.listPemegangSahamLembaga[index].typeInstitutionModel != null
                                            ?
                                        "${_provider.listPemegangSahamLembaga[index].typeInstitutionModel.PARA_NAME}" : ""),
                                        flex: 5
                                    )
                                  ],
                                ),
                                SizedBox(height: MediaQuery.of(context).size.height / 57),
                                Row(
                                  children: [
                                    Expanded(
                                        child: Text("Nama Institusi"),
                                        flex: 5
                                    ),
                                    Text(" : "),
                                    Expanded(
                                        child: Text("${_provider.listPemegangSahamLembaga[index].institutionName}"),
                                        flex: 5
                                    )
                                  ],
                                ),
                                SizedBox(height: MediaQuery.of(context).size.height / 57),
                                Row(
                                  children: [
                                    Expanded(
                                        child: Text("Tanggal Pendirian"),
                                        flex: 5
                                    ),
                                    Text(" : "),
                                    Expanded(
                                        child: Text("${dateFormat.format(DateTime.parse(_provider.listPemegangSahamLembaga[index].initialDateForDateOfEstablishment))}"),
                                        flex: 5
                                    )
                                  ],
                                ),
                                SizedBox(height: MediaQuery.of(context).size.height / 57),
                                Row(
                                  children: [
                                    Expanded(
                                        child: Text("NPWP"),
                                        flex: 5
                                    ),
                                    Text(" : "),
                                    Expanded(
                                        child: Text("${_provider.listPemegangSahamLembaga[index].npwp}"),
                                        flex: 5
                                    )
                                  ],
                                ),
                                SizedBox(height: MediaQuery.of(context).size.height / 57),
                              ],
                            ),
                            Align(
                              alignment: Alignment.topRight,
                              child: IconButton(
                                  icon: Icon(
                                    Icons.delete,
                                    color: Colors.red,
                                  ),
                                  onPressed: () {
                                    _provider.deleteListPemegangSahamLembaga(context, index);
                                  }),
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                },
                itemCount: _provider.listPemegangSahamLembaga.length,
              ),
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Provider.of<AddPemegangSahamLembagaChangeNotif>(context, listen: false).clearDataPemegangSahamKelembagaan();
            Navigator.push(context, MaterialPageRoute(
                  builder: (context) => AddPemegangSahamLembaga(
                    flag: 0,
                    index: null,
                    model: null,
                  ),
                ));
          },
          child: Icon(
            Icons.add,
          ),
          backgroundColor: myPrimaryColor,
        ),
      ),
    );
  }
}