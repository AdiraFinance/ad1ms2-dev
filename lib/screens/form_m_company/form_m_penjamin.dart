import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/form_m_company_penjamin_model.dart';
import 'package:ad1ms2_dev/screens/form_m_company/list_company_guarantor_company.dart';
import 'package:ad1ms2_dev/screens/form_m_company/list_company_guarantor_individu.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_company_guarantor_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_penjamin_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

//ga kepake
class FormMCompanyPenjaminProvider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
        data: ThemeData(
          fontFamily: "NunitoSans",
          primaryColor: Colors.black,
          primarySwatch: primaryOrange,
        ),
        child: Consumer<FormMCompanyGuarantorChangeNotifier>(
          builder: (context, formMCompanyGuarantorChangeNotifier, _) {
            return Scaffold(
              body: ListView(
                padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height/57,
                    horizontal: MediaQuery.of(context).size.width/37
                ),
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Ada Penjamin ?",
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                            letterSpacing: 0.15),
                      ),
                      // SizedBox(height: MediaQuery.of(context).size.height / 47),
                      Row(
                        children: [
                          Row(
                            children: [
                              Radio(
                                  value: 1,
                                  groupValue: formMCompanyGuarantorChangeNotifier
                                      .radioValueIsWithGuarantor,
                                  onChanged: (value) {
                                    formMCompanyGuarantorChangeNotifier
                                        .radioValueIsWithGuarantor = value;
                                  }),
                              Text("Ya")
                            ],
                          ),
                          Row(
                            children: [
                              Radio(
                                  value: 0,
                                  groupValue: formMCompanyGuarantorChangeNotifier
                                      .radioValueIsWithGuarantor,
                                  onChanged: (value) {
                                    formMCompanyGuarantorChangeNotifier
                                        .radioValueIsWithGuarantor = value;
                                  }),
                              Text("Tidak")
                            ],
                          ),
                        ],
                      ),
                      formMCompanyGuarantorChangeNotifier.radioValueIsWithGuarantor ==
                          0
                          ? SizedBox(height: 0.0)
                          : Column(
                        children: [
                          InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          ListCompanyGuarantorIndividual()));
                            },
                            child: Card(
                              elevation: 3.3,
                              child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                        "Jumlah penjamin (pribadi) :  ${formMCompanyGuarantorChangeNotifier.listGuarantorIndividual.length}"),
                                    IconButton(
                                        icon: Icon(Icons.add_circle_outline,
                                            color: primaryOrange),
                                        onPressed: () {}),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                              height: MediaQuery.of(context).size.height / 47),
                          InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          ListCompanyGuarantorCompany()));
                            },
                            child: Card(
                              elevation: 3.3,
                              child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                        "Jumlah penjamin (kelembagaan) :  ${formMCompanyGuarantorChangeNotifier.listGuarantorCompany.length}"),
                                    IconButton(
                                        icon: Icon(Icons.add_circle_outline,
                                            color: primaryOrange),
                                        onPressed: () {})
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  )
                ],
              ),
            );
          },
        ));
  }
}
