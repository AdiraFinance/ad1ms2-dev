import 'package:ad1ms2_dev/shared/form_m_add_address_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_manajemen_pic_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_manajemen_pic_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_parent_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';
import 'form_m_manajemen_pic.dart';
import 'form_m_manajemen_pic_alamat.dart';
import 'menu_pemegang_saham.dart';

class MenuManagementPIC extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        var _providerDetailPIC = Provider.of<FormMCompanyManajemenPICChangeNotifier>(context, listen: true);
        var _providerAddressPIC = Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: true);

        return Padding(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width/37,
                vertical: MediaQuery.of(context).size.height/57,
            ),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    InkWell(
                        onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FormMCompanyManajemenPICProvider()
                                )
                            );
                        },
                        child: Card(
                            elevation: 3.3,
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text(
                                            "Informasi Manajemen PIC"),
                                        _providerDetailPIC.flag
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
                    _providerDetailPIC.autoValidate
                        ? Container(
                        margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                        child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
                    )
                        : SizedBox(height: MediaQuery.of(context).size.height / 87),
                    SizedBox(height: MediaQuery.of(context).size.height / 87),
                    InkWell(
                        onTap: () {
                            Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setDataAddressIndividu(context);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FormMCompanyManajemenPICAlamatProvider()));
                        },
                        child: Card(
                            elevation: 3.3,
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                        Text(
                                            "Alamat Manajemen PIC"),
                                        _providerAddressPIC.flag
                                            ? Icon(Icons.check, color: primaryOrange)
                                            : SizedBox()
                                    ],
                                ),
                            ),
                        ),
                    ),
                    _providerAddressPIC.autoValidate
                        ? Container(
                        margin: EdgeInsets.only(left: MediaQuery.of(context).size.width / 37),
                        child: Text("Data belum lengkap",style: TextStyle(color: Colors.red, fontSize: 12)),
                    )
                        : SizedBox(height: MediaQuery.of(context).size.height / 87),
                ],
            ),
        );
    }

    Future<void> setNextState(BuildContext context,int index) async{
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        var _providerDetailPIC = Provider.of<FormMCompanyManajemenPICChangeNotifier>(context, listen: true);
        var _providerAddressPIC = Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: true);
        MenuPemegangSaham _menuSaham = MenuPemegangSaham();

        _providerDetailPIC.setDataSQLite();
        _providerAddressPIC.setDataSQLite();

        if(_preferences.getString("last_known_state") == "IDE" && index != null){
            if(index != 3){
                _providerDetailPIC.flag = true;
                _providerAddressPIC.flag = true;
                Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).isMenuManagementPIC = true;
                Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).selectedIndex += 1;
                await _menuSaham.setNextState(context, index);
            }
            else{
                Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).selectedIndex = 3;
            }
        }
    }
}
