import 'package:ad1ms2_dev/screens/app/form_m_info_object_karoseri.dart';
import 'package:ad1ms2_dev/screens/app/info_application.dart';
import 'package:ad1ms2_dev/screens/app/info_credit_income.dart';
import 'package:ad1ms2_dev/screens/app/info_credit_structure.dart';
import 'package:ad1ms2_dev/screens/app/info_credit_structure_type_installment.dart';
import 'package:ad1ms2_dev/screens/app/info_wmp.dart';
import 'package:ad1ms2_dev/screens/app/information_collateral.dart';
import 'package:ad1ms2_dev/screens/app/information_object_unit.dart';
import 'package:ad1ms2_dev/screens/app/information_salesman.dart';
import 'package:ad1ms2_dev/screens/app/list_additional_insurance.dart';
import 'package:ad1ms2_dev/screens/app/list_app_credit_subsidy.dart';
import 'package:ad1ms2_dev/screens/app/list_app_info_document.dart';
import 'package:ad1ms2_dev/screens/app/list_major_insurance.dart';
import 'package:ad1ms2_dev/screens/app/list_object_karoseri.dart';
import 'package:ad1ms2_dev/screens/app/marketing_notes.dart';
import 'package:ad1ms2_dev/screens/app/taksasi_unit.dart';
import 'package:ad1ms2_dev/screens/form_m/form_m_guarantor.dart';
import 'package:ad1ms2_dev/screens/form_m/form_m_parent.dart';
import 'package:ad1ms2_dev/screens/form_m/menu_detail_loan.dart';
import 'package:ad1ms2_dev/screens/form_m/menu_object_information.dart';
import 'package:ad1ms2_dev/screens/survey/result_survey.dart';
import 'package:ad1ms2_dev/services/connectivity_service.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_object_karoseri_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/taksasi_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_company_parent_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_individu_change_notifier/form_m_parent_individu_change_notifier.dart';
import 'package:ad1ms2_dev/widgets/widget_title_appbar_from_m_company.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../main.dart';
import 'form_m_pendapatan.dart';
import 'menu_detail_company.dart';
import 'menu_management_pic.dart';
import 'menu_pemegang_saham.dart';

class FormMCompanyParent extends StatefulWidget {
  final String typeFormId;
  final String flag;

  const FormMCompanyParent({this.typeFormId, this.flag});
  @override
  _FormMCompanyParentState createState() => _FormMCompanyParentState();
}

class _FormMCompanyParentState extends State<FormMCompanyParent> {
  Map _source = {ConnectivityResult.none: false};
  MyConnectivity _connectivity = MyConnectivity();

  int flag = 1;
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    debugPrint("cek typeForm ${widget.typeFormId}");
    debugPrint("cek flag ${widget.flag}");
    if(flag == 1){
      // Provider.of<FormMParentIndividualChangeNotifier>(context, listen: true).clearData(context);
      Provider.of<FormMParentCompanyChangeNotifier>(context, listen: true).clearData(context);
      flag = 2;
    }
  }

  @override
  void initState() {
    super.initState();
    _connectivity.initialise();
    _connectivity.myStream.listen((source) {
      setState(() => _source = source);
    });
  }

  // @override
  // void dispose() {
  //   _connectivity.disposeStream();
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    String _indicatorMessage;
    switch (_source.keys.toList()[0]) {
      case ConnectivityResult.none:
        _indicatorMessage = "Offline";
        break;
      case ConnectivityResult.mobile:
        _indicatorMessage = "Mobile: Online";
        break;
      case ConnectivityResult.wifi:
        _indicatorMessage = "WiFi: Online";
    }

    List<Widget> _list = [
      MenuDetailCompany(),//0
//      FormMCompanyRincianProvider(),//0
//      FormMCompanyAlamat(),
      FormMCompanyPendapatanProvider(),//1
//      FormMCompanyPenjaminProvider(),
      FormMGuarantor(),//2
      MenuManagementPIC(),//3
//      FormMCompanyManajemenPICProvider(),
      MenuPemegangSaham(),//4
//      FormMCompanyPemegangSahamPribadiProvider(),
//      FormMCompanyPemegangSahamPribadiAlamatProvider(),
//      FormMCompanyPemegangSahamKelembagaanProvider(),
//      FormMCompanyPemegangSahamKelembagaanAlamatProvider(),
//      InfoApplication(),//10
//      InformationObjectUnit(),//11
//      InformationSalesman(),//12
//      InformationCollateral(),//13
//      FormMInfoObjectKaroseri(),//14
//      InfoCreditStructure(),//15
//      Visibility(
//          visible: Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).isVisible,
//          child:InfoCreditStructureTypeInstallment()
//      ),//16
//      ListMajorInsurance(),//17
//      ListAdditionalInsurance(),//18
//      InfoWMP(),//19
//      InfoCreditIncome(),//20
//      ListAppCreditSubsidy(),//21
//      ListAppInfoDocument(),//22
//      Visibility(
//          visible: Provider.of<TaksasiUnitChangeNotifier>(context,listen: false).isVisible,
//          child: TaksasiUnit()
//      ),//23
//      MarketingNotes()//24
      InfoApplication(),//5
      MenuObjectInformation(flag: widget.flag),//6
      Visibility(
          visible: Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: true).isVisible,
          child: ListObjectKaroseri()
      ),//7
      MenuDetailLoan(),//8
      ListAppCreditSubsidy(),//9
      ListAppInfoDocument(),//10
      Visibility(
          visible: Provider.of<TaksasiUnitChangeNotifier>(context,listen: false).isVisible,
          child: TaksasiUnit()
      ),//11
      MarketingNotes(),//12
      Visibility(
        child: ResultSurvey(),//13
        visible: widget.typeFormId != "IDE",
      )//13
    ];

    var _provider = Provider.of<FormMParentCompanyChangeNotifier>(context, listen: true);

    return Theme(
      data: ThemeData(
          fontFamily: "NunitoSans",
          primaryColor: primaryOrange,
          accentColor: myPrimaryColor
      ),
      child: Scaffold(
        key: _provider.scaffoldKey,
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: myPrimaryColor,
          title: TitleAppBarFormMCompany(
            selectedIndex: _provider.selectedIndex,
          ),
          centerTitle: true,
          iconTheme: IconThemeData(color: Colors.black),
         actions: [
           Center(
             child: Padding(
               padding: const EdgeInsets.all(8.0),
               child: Container(
                 padding: EdgeInsets.all(2.1),
                 decoration: new BoxDecoration(
                   color: _indicatorMessage == "Offline" ? Colors.red : Colors.green,
                   borderRadius: BorderRadius.circular(20),
                 ),
                 constraints: BoxConstraints(
                   minWidth: 20,
                   minHeight: 20,
                 ),
                 child: SizedBox(),
               ),
             ),
           ),
           PopupMenuButton(
               onSelected: (selected){
                 // Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).loadData = false;
                 Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).isShowDialog(context, selected);
               },
               child: Icon(Icons.more_vert),
               itemBuilder: (context) {
                 return LabelPopUpMenuButtonSaveToDraftBackToHome.choices.map((String choice) {
                   return PopupMenuItem(
                     value: choice,
                     child: Text(choice),
                   );
                 }).toList();
               }
           )
         ],
        ),
        body: Consumer<FormMParentCompanyChangeNotifier>(
          builder: (context, parent, _) {
            // return parent.loadData
            //     ?
            // Center(child: CircularProgressIndicator())
            //     :
            return Form(
                  child: _list.elementAt(parent.selectedIndex),
                  key: parent.formKeys[parent.selectedIndex]);
          },
        ),
        drawer: Drawer(
            child: ListView(
              children: [
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "Nasabah",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                ),
                ListTile(
                  onTap: () {
                    _provider.checkTabDrawer(context, 0);
                    Navigator.pop(context);
                  },
                  title: Container(
                      margin: EdgeInsets.only(left: 16),
                      child: Text("Rincian Lembaga")),
                  trailing: _provider.isMenuCompany
                      ? Icon(Icons.check, color: Colors.green)
                      : null,
                  selected: _provider.selectedIndex == 0,
                ),
                ListTile(
                  onTap: _provider.isMenuCompany
                      ?
                      (){_provider.checkTabDrawer(context, 1);Navigator.pop(context);}
                      : null,
                  title: Container(
                      margin: EdgeInsets.only(left: 16),
                      child: _provider.isPendapatan
                      ? Text("Pendapatan")
                      : _provider.selectedIndex == 1
                        ? Text("Pendapatan")
                        : _provider.isMenuCompany
                          ? Text("Pendapatan")
                          : Text("Pendapatan", style: TextStyle(color: Colors.grey),)
                  ),
                  trailing: _provider.isPendapatan
                      ? Icon(Icons.check, color: Colors.green)
                      : null,
                  selected: _provider.selectedIndex == 1,
                ),
                ListTile(
                  onTap: _provider.isPendapatan
                      ?
                      (){_provider.checkTabDrawer(context, 2);Navigator.pop(context);}
                      : null,
                  title: Container(
                      margin: EdgeInsets.only(left: 16),
                      child: _provider.isPenjamin
                      ? Text("Penjamin")
                      : _provider.selectedIndex == 2
                        ? Text("Penjamin")
                        : _provider.isPendapatan
                          ? Text("Penjamin")
                          : Text("Penjamin", style: TextStyle(color: Colors.grey),)
                  ),
                  trailing: _provider.isPenjamin
                      ? Icon(Icons.check, color: Colors.green)
                      : null,
                  selected: _provider.selectedIndex == 2,
                ),
                ListTile(
                  onTap: _provider.isPenjamin
                      ?
                      (){_provider.checkTabDrawer(context, 3);Navigator.pop(context);}
                      : null,
                  title: Container(
                      margin: EdgeInsets.only(left: 16),
                      child: _provider.isMenuManagementPIC
                      ? Text("Manajemen PIC")
                      : _provider.selectedIndex == 3
                        ? Text("Manajemen PIC")
                        : _provider.isPenjamin
                          ? Text("Manajemen PIC")
                          : Text("Manajemen PIC", style: TextStyle(color: Colors.grey),)
                  ),
                  trailing: _provider.isMenuManagementPIC
                      ? Icon(Icons.check, color: Colors.green)
                      : null,
                  selected: _provider.selectedIndex == 3,
                ),
                 ListTile(
                   onTap: _provider.isMenuManagementPIC
                       ?
                       (){_provider.checkTabDrawer(context, 4);Navigator.pop(context);}
                       : null,
                   title: Container(
                       margin: EdgeInsets.only(left: 16),
                       child: _provider.isMenuPemegangSaham
                       ? Text("Pemegang Saham")
                       : _provider.selectedIndex == 4
                         ? Text("Pemegang Saham")
                         : _provider.isMenuManagementPIC
                           ? Text("Pemegang Saham")
                           : Text("Pemegang Saham", style: TextStyle(color: Colors.grey),)
                   ),
                   trailing: _provider.isMenuPemegangSaham
                       ? Icon(Icons.check, color: Colors.green)
                       : null,
                   selected: _provider.selectedIndex == 4,
                 ),
//                ListTile(
//                  onTap: _provider.isPenjamin
//                      ?
//                      (){_provider.checkTabDrawer(context, 4);Navigator.pop(context);}
//                      : null,
//                  title: Container(
//                      margin: EdgeInsets.only(left: 16),
//                      child: Text("Manajemen PIC")),
//                  trailing: _provider.isManajemenPIC
//                      ? Icon(Icons.check, color: Colors.green)
//                      : null,
//                  selected: _provider.selectedIndex == 4,
//                ),
//                ListTile(
//                  onTap: _provider.isManajemenPIC
//                      ?
//                      (){_provider.checkTabDrawer(context, 5);Navigator.pop(context);}
//                      : null,
//                  title: Container(
//                      margin: EdgeInsets.only(left: 16),
//                      child: Text("Manajemen PIC - Alamat")),
//                  trailing: _provider.isManajemenPICAlamat
//                      ? Icon(Icons.check, color: Colors.green)
//                      : null,
//                  selected: _provider.selectedIndex == 5,
//                ),
//                ListTile(
//                  onTap: _provider.isPemegangSahamPribadi
//                      ?
//                      (){_provider.checkTabDrawer(context, 7);Navigator.pop(context);}
//                      : null,
//                  title: Container(
//                      margin: EdgeInsets.only(left: 16),
//                      child: Text("Pemegang Saham Pribadi - Alamat")),
//                  trailing: _provider.isPemegangSahamPribadiAlamat
//                      ? Icon(Icons.check, color: Colors.green)
//                      : null,
//                  selected: _provider.selectedIndex == 7,
//                ),
//                ListTile(
//                  onTap: _provider.isPemegangSahamPribadiAlamat
//                      ?
//                      (){_provider.checkTabDrawer(context, 8);Navigator.pop(context);}
//                      : null,
//                  title: Container(
//                      margin: EdgeInsets.only(left: 16),
//                      child: Text("Pemegang Saham Kelembagaan")),
//                  trailing: _provider.isPemegangSahamKelembagaan
//                      ? Icon(Icons.check, color: Colors.green)
//                      : null,
//                  selected: _provider.selectedIndex == 8,
//                ),
//                ListTile(
//                  onTap: _provider.isPemegangSahamKelembagaan
//                      ?
//                      (){_provider.checkTabDrawer(context, 9);Navigator.pop(context);}
//                      : null,
//                  title: Container(
//                      margin: EdgeInsets.only(left: 16),
//                      child: Text("Pemegang Saham Kelembagaan - Alamat")),
//                  trailing: _provider.isPemegangSahamKelembagaanAlamat
//                      ? Icon(Icons.check, color: Colors.green)
//                      : null,
//                  selected: _provider.selectedIndex == 9,
//                ),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "Aplikasi",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                ),
                ListTile(
                  onTap: _provider.isMenuManagementPIC
                      ?
                      (){_provider.checkTabDrawer(context, 5);Navigator.pop(context);}
                      : null,
                  title: Container(
                      margin: EdgeInsets.only(left: 16),
                      child: _provider.isInfoAppDone
                      ? Text("Aplikasi")
                      : _provider.selectedIndex == 5
                        ? Text("Aplikasi")
                        : _provider.isMenuManagementPIC
                          ? Text("Aplikasi")
                          : Text("Aplikasi", style: TextStyle(color: Colors.grey),)
                  ),
                  trailing: _provider.isInfoAppDone
                      ? Icon(Icons.check, color: Colors.green)
                      : null,
                  selected: _provider.selectedIndex == 5,
                ),
                ListTile(
                  onTap: _provider.isInfoAppDone
                      ?
                      (){_provider.checkTabDrawer(context, 6);Navigator.pop(context);}
                      : null,
                  title: Container(
                      margin: EdgeInsets.only(left: 16),
                      child: _provider.isMenuObjectInformationDone
                      ? Text("Informasi Objek")
                      : _provider.selectedIndex == 6
                        ? Text("Informasi Objek")
                        : _provider.isInfoAppDone
                          ? Text("Informasi Objek")
                          : Text("Informasi Objek", style: TextStyle(color: Colors.grey),)
                  ),
                  trailing: _provider.isMenuObjectInformationDone
                      ? Icon(Icons.check, color: Colors.green)
                      : null,
                  selected: _provider.selectedIndex == 6,
                ),
                Visibility(
                  visible: Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: true).isVisible,
                  child: ListTile(
                    onTap: _provider.isMenuObjectInformationDone
                        ?
                        (){_provider.checkTabDrawer(context, 7);Navigator.pop(context);}
                        : null,
                    title: Container(
                        margin: EdgeInsets.only(left: 16),
                        child: _provider.isInfoObjKaroseriDone
                        ? Text("Karoseri")
                        : _provider.selectedIndex == 7
                          ? Text("Karoseri")
                          : _provider.isMenuObjectInformationDone
                            ? Text("Karoseri")
                            : Text("Karoseri", style: TextStyle(color: Colors.grey),)
                    ),
                    trailing: _provider.isInfoObjKaroseriDone
                        ? Icon(Icons.check, color: Colors.green)
                        : null,
                    selected: _provider.selectedIndex == 7,
                  ),
                ),
                ListTile(
                  onTap: _provider.isInfoObjKaroseriDone
                      ?
                      (){_provider.checkTabDrawer(context, 8);Navigator.pop(context);}
                      : null,
                  title: Container(
                      margin: EdgeInsets.only(left: 16),
                      child: _provider.isMenuDetailLoanDone
                      ? Text("Rincian Pinjaman")
                      : _provider.selectedIndex == 8
                        ? Text("Rincian Pinjaman")
                        : _provider.isInfoObjKaroseriDone
                          ? Text("Rincian Pinjaman")
                          : Text("Rincian Pinjaman", style: TextStyle(color: Colors.grey),)
                  ),
                  trailing: _provider.isMenuDetailLoanDone
                      ? Icon(Icons.check, color: Colors.green)
                      : null,
                  selected: _provider.selectedIndex == 8,
                ),
                ListTile(
                  onTap: _provider.isMenuDetailLoanDone
                      ?
                      (){_provider.checkTabDrawer(context, 9);Navigator.pop(context);}
                      : null,
                  title: Container(
                      margin: EdgeInsets.only(left: 16),
                      child: _provider.isCreditSubsidyDone
                      ? Text("Subsidi")
                      : _provider.selectedIndex == 9
                        ? Text("Subsidi")
                        : _provider.isMenuDetailLoanDone
                          ? Text("Subsidi")
                          : Text("Subsidi", style: TextStyle(color: Colors.grey),)
                  ),
                  trailing: _provider.isCreditSubsidyDone
                      ? Icon(Icons.check, color: Colors.green)
                      : null,
                  selected: _provider.selectedIndex == 9,
                ),
                ListTile(
                  onTap: _provider.isCreditSubsidyDone
                      ?
                      (){_provider.checkTabDrawer(context, 10);Navigator.pop(context);}
                      : null,
                  title: Container(
                      margin: EdgeInsets.only(left: 16),
                      child: _provider.isDocumentInfoDone
                      ? Text("Informasi Dokumen")
                      : _provider.selectedIndex == 10
                        ? Text("Informasi Dokumen")
                        : _provider.isCreditSubsidyDone
                          ? Text("Informasi Dokumen")
                          : Text("Informasi Dokumen", style: TextStyle(color: Colors.grey),)
                  ),
                  trailing: _provider.isDocumentInfoDone
                      ? Icon(Icons.check, color: Colors.green)
                      : null,
                  selected: _provider.selectedIndex == 10,
                ),
                Visibility(
                  visible: Provider.of<TaksasiUnitChangeNotifier>(context,listen: false).isVisible,
                  child: ListTile(
                    onTap: _provider.isDocumentInfoDone
                        ?
                        (){_provider.checkTabDrawer(context, 11);Navigator.pop(context);}
                        : null,
                    title: Container(
                        margin: EdgeInsets.only(left: 16),
                        child: _provider.isTaksasiUnitDone
                        ? Text("Taksasi Unit")
                        : _provider.selectedIndex == 11
                          ? Text("Taksasi Unit")
                          : _provider.isDocumentInfoDone
                            ? Text("Taksasi Unit")
                            : Text("Informasi Dokumen", style: TextStyle(color: Colors.grey),)
                    ),
                    trailing: _provider.isTaksasiUnitDone
                        ? Icon(Icons.check, color: Colors.green)
                        : null,
                    selected: _provider.selectedIndex == 11,
                  ),
                ),
                ListTile(
                  onTap:
                  Provider.of<TaksasiUnitChangeNotifier>(context,listen: true).isVisible
                    ? _provider.isTaksasiUnitDone
                      ? (){_provider.checkTabDrawer(context, 12);Navigator.pop(context);}
                      : null
                    : _provider.isDocumentInfoDone
                      ? (){_provider.checkTabDrawer(context, 12);Navigator.pop(context);}
                      : null,
                  title: Container(
                      margin: EdgeInsets.only(left: 16),
                      child: _provider.isMarketingNotesDone
                      ? Text("Marketing Notes")
                      : _provider.selectedIndex == 12
                        ? Text("Marketing Notes")
                        : Provider.of<TaksasiUnitChangeNotifier>(context,listen: true).isVisible
                          ? _provider.isTaksasiUnitDone
                            ? Text("Marketing Notes")
                            : Text("Marketing Notes", style: TextStyle(color: Colors.grey),)
                          : _provider.isDocumentInfoDone
                            ? Text("Marketing Notes")
                            : Text("Marketing Notes", style: TextStyle(color: Colors.grey),)
                  ),
                  trailing: _provider.isMarketingNotesDone
                      ? Icon(Icons.check, color: Colors.green)
                      : null,
                  selected: _provider.selectedIndex == 12,
                ),
                Visibility(
                  visible: widget.typeFormId != "IDE",
                  child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      "SURVEY",
                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                Visibility(
                  visible: widget.typeFormId != "IDE",
                  child: ListTile(
                    onTap: _provider.isDocumentInfoDone
                        ? (){_provider.checkTabDrawer(context, 13);Navigator.pop(context);}
                        : null,
                    title: Container(
                        margin: EdgeInsets.only(left: 16),
                        child: _provider.isTaksasiUnitDone
                            ? Text("Hasil Survey")
                            : _provider.selectedIndex == 13
                            ? Text("Hasil Survey")
                            : _provider.isDocumentInfoDone
                            ? Text("Hasil Survey")
                            : Text("Hasil Survey", style: TextStyle(color: Colors.grey),)
                    ),
                    trailing: _provider.isTaksasiUnitDone
                        ? Icon(Icons.check, color: Colors.green)
                        : null,
                    selected: _provider.selectedIndex == 13,
                  ),
                ),
              ],
            )
        ),
        bottomNavigationBar:
        Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).loadData
            ?
        Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                CircularProgressIndicator(),
                SizedBox(height: 13),
                Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).messageProcess != null
                    ?
                Text(
                  "${Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).messageProcess}",
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w500
                  ),
                )
                    :
                SizedBox()
              ],
            )
        )
            :
        _provider.selectedIndex == 0
            ?
        BottomAppBar(
          child: Container(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width / 27,
                vertical: MediaQuery.of(context).size.height / 57),
            child: RaisedButton(
                color: myPrimaryColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)
                ),
                onPressed: () {
                  Provider.of<FormMParentCompanyChangeNotifier>(context, listen: false).checkBtnNext(context);
                },
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [Text("NEXT")]
                )
            ),
          ),
        )
            :
        BottomAppBar(
          elevation: 0.0,
          child: Container(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width / 27,
                vertical: MediaQuery.of(context).size.height / 57),
            child: Row(
              children: [
                Expanded(
                  flex: 5,
                  child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)),
                      onPressed: () {
                        Provider.of<FormMParentCompanyChangeNotifier>(context, listen: false).backBtn(context);
                      },
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [Text("BACK")])),
                ),
                SizedBox(width: MediaQuery.of(context).size.width / 37),
                Expanded(
                  flex: 5,
                  child: RaisedButton(
                      color: myPrimaryColor,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)),
                      onPressed: () {
                        print("cek selectedIndex ${Provider.of<FormMParentCompanyChangeNotifier>(context, listen: false).selectedIndex}");
                        if(widget.typeFormId != "IDE"){
                          if(Provider.of<FormMParentCompanyChangeNotifier>(context, listen: false).selectedIndex == 13) {
                            Provider.of<FormMParentCompanyChangeNotifier>(context, listen: false).generateAplikasiPayung(context);
                            // SURVEY COMPANY
                            // Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).checkBtnNext(context);
                            // Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => Dashboard()), (Route <dynamic> route) => false);
                          }
                          else {
                            Provider.of<FormMParentCompanyChangeNotifier>(context, listen: false).checkBtnNext(context);
                          }
                        }
                        else{
                          if(Provider.of<FormMParentCompanyChangeNotifier>(context, listen: false).selectedIndex == 12) {
                            // IDE COMPANY
                            Provider.of<FormMParentCompanyChangeNotifier>(context, listen: false).generateAplikasiPayung(context);
                            // Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).checkBtnNext(context);
                            // Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => Dashboard()), (Route <dynamic> route) => false);
                          }
                          else {
                            Provider.of<FormMParentCompanyChangeNotifier>(context, listen: false).checkBtnNext(context);
                          }
                        }
                      },
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            widget.typeFormId != "IDE"
                                ?
                            Provider.of<FormMParentCompanyChangeNotifier>(context, listen: false).selectedIndex != 13
                                ?
                            Text("NEXT")
                                :
                            Text("SUBMIT")
                                :
                            Provider.of<FormMParentCompanyChangeNotifier>(context, listen: false).selectedIndex != 12
                                ?
                            Text("NEXT")
                                :
                            Text("SUBMIT")
                          ]
                      )
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
