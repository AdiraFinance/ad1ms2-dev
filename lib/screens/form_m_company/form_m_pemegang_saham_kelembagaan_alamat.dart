import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/form_m_company/list_pemegang_saham_kelembagaan_alamat.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_company_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_pemegang_saham_kelembagaan_alamat_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

//ga kepake
class FormMCompanyPemegangSahamKelembagaanAlamatProvider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var _data = Provider.of<FormMAddAddressCompanyChangeNotifier>(context, listen: false);
    return Consumer<FormMCompanyPemegangSahamKelembagaanAlamatChangeNotifier>(
      builder: (context, formMCompanyPemegangSahamKelembagaanAlamatChangeNotif, _) {
        return Theme(
          data: ThemeData(
              primaryColor: Colors.black,
              accentColor: myPrimaryColor,
              fontFamily: "NunitoSans",
              primarySwatch: Colors.yellow),
          child: Scaffold(
            appBar: AppBar(
              title:
              Text("Pemegang Saham Kelembagaan Alamat", style: TextStyle(color: Colors.black)),
              centerTitle: true,
              backgroundColor: myPrimaryColor,
              iconTheme: IconThemeData(color: Colors.black),
            ),
            body: Form(
              key: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.keyForm,
              onWillPop: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.onBackPress,
              child: ListView(
                padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height/57,
                    horizontal: MediaQuery.of(context).size.width/37
                ),
                children: [
                  Column(
                    children: [
                      Visibility(
                        visible: _data.setAddressShow(3),
                        child: TextFormField(
                          autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                          validator: (e) {
                            if (e.isEmpty && _data.setAddressMandatory(3)) {
                              return "Tidak boleh kosong";
                            } else {
                              return null;
                            }
                          },
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ListPemegangSahamKelembagaanAddress()));
                          },
                          controller: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerAddress,
                          style: new TextStyle(color: Colors.black),
                          decoration: new InputDecoration(
                              labelText: 'Alamat',
                              labelStyle: TextStyle(color: Colors.black),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8))),
                          readOnly: true,
                        ),
                      ),
                      Visibility(visible: _data.setAddressShow(3),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                      Visibility(
                        visible: _data.setAddressTypeShow(3),
                        child: TextFormField(
                          autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                          validator: (e) {
                            if (e.isEmpty && _data.setAddressTypeMandatory(3)) {
                              return "Tidak boleh kosong";
                            } else {
                              return null;
                            }
                          },
                          controller: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerAddressType,
                          style: new TextStyle(color: Colors.black),
                          decoration: new InputDecoration(
                              filled: true,
                              fillColor: Colors.black12,
                              labelText: 'Jenis Alamat',
                              labelStyle: TextStyle(color: Colors.black),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8))),
                          enabled: false,
                        ),
                      ),
                      Visibility(visible: _data.setAddressTypeShow(3),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                      Visibility(
                        visible: _data.setRTShow(3) || _data.setRWShow(3),
                        child: Row(
                          children: [
                            Visibility(
                              visible: _data.setRTShow(3),
                              child: Expanded(
                                flex: 5,
                                child: TextFormField(
                                  autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && _data.setRTMandatory(3)) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  controller: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerRT,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      filled: true,
                                      fillColor: Colors.black12,
                                      labelText: 'RT',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8))),
                                  enabled: false,
                                ),
                              ),
                            ),
                            Visibility(visible: _data.setRTShow(3),child: SizedBox(width: 8)),
                            Visibility(
                              visible: _data.setRWShow(3),
                              child: Expanded(
                                  flex: 5,
                                  child: TextFormField(
                                    autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty && _data.setRWMandatory(3)) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    controller: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerRW,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        filled: true,
                                        fillColor: Colors.black12,
                                        labelText: 'RW',
                                        labelStyle: TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.circular(8))),
                                    enabled: false,
                                  )),
                            )
                          ],
                        ),
                      ),
                      Visibility(visible: _data.setRTShow(3) || _data.setRWShow(3), child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                      Visibility(
                        visible: _data.setKelurahanShow(3),
                        child: FocusScope(
                          node: FocusScopeNode(),
                          child: TextFormField(
                            autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                            validator: (e) {
                              if (e.isEmpty && _data.setKelurahanMandatory(3)) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            controller: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerKelurahan,
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                                filled: true,
                                fillColor: Colors.black12,
                                labelText: 'Kelurahan',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8))),
                            enabled: false,
                          ),
                        ),
                      ),
                      Visibility(visible: _data.setKelurahanShow(3), child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                      Visibility(
                        visible: _data.setKecamatanShow(3),
                        child: TextFormField(
                          autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                          validator: (e) {
                            if (e.isEmpty && _data.setKecamatanMandatory(3)) {
                              return "Tidak boleh kosong";
                            } else {
                              return null;
                            }
                          },
                          controller: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerKecamatan,
                          style: new TextStyle(color: Colors.black),
                          decoration: new InputDecoration(
                              filled: true,
                              fillColor: Colors.black12,
                              labelText: 'Kecamatan',
                              labelStyle: TextStyle(color: Colors.black),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8))),
                          enabled: false,
                        ),
                      ),
                      Visibility(visible: _data.setKecamatanShow(3),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                      Visibility(
                        visible: _data.setKabKotShow(3),
                        child: TextFormField(
                          autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                          validator: (e) {
                            if (e.isEmpty && _data.setKabKotMandatory(3)) {
                              return "Tidak boleh kosong";
                            } else {
                              return null;
                            }
                          },
                          controller: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerKota,
                          style: TextStyle(color: Colors.black),
                          decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.black12,
                              labelText: 'Kabupaten/Kota',
                              labelStyle: TextStyle(color: Colors.black),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8))),
                          enabled: false,
                        ),
                      ),
                      Visibility(visible: _data.setKabKotShow(3),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                      Visibility(
                        visible: _data.setProvinsiShow(3) || _data.setKodePosShow(3),
                        child: Row(
                          children: [
                            Visibility(
                              visible: _data.setProvinsiShow(3),
                              child: Expanded(
                                flex: 7,
                                child: TextFormField(
                                  autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && _data.setProvinsiMandatory(3)) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  controller: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerProvinsi,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      filled: true,
                                      fillColor: Colors.black12,
                                      labelText: 'Provinsi',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8))),
                                  enabled: false,
                                ),
                              ),
                            ),
                            Visibility(visible: _data.setProvinsiShow(3),child: SizedBox(width: 8)),
                            Visibility(
                              visible: _data.setKodePosShow(3),
                              child: Expanded(
                                flex: 3,
                                child: TextFormField(
                                  autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && _data.setKodePosMandatory(3)) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  controller:
                                  formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerPostalCode,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      filled: true,
                                      fillColor: Colors.black12,
                                      labelText: 'Kode Pos',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8))),
                                  enabled: false,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Visibility(visible: _data.setProvinsiShow(3) || _data.setKodePosShow(3), child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                      Visibility(
                        visible: _data.setTelp1AreaShow(3) || _data.setTelp1Show(3),
                        child: Row(
                          children: [
                            Visibility(
                              visible: _data.setTelp1AreaShow(3),
                              child: Expanded(
                                flex: 4,
                                child: TextFormField(
                                  autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && _data.setTelp1AreaMandatory(3)) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  keyboardType: TextInputType.number,
                                  inputFormatters: [
                                    WhitelistingTextInputFormatter.digitsOnly,
                                    LengthLimitingTextInputFormatter(10),
                                  ],
                                  controller: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerTelepon1Area,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      filled: true,
                                      fillColor: Colors.black12,
                                      labelText: 'Telepon 1 (Area)',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8))),
                                  enabled: false,
                                ),
                              ),
                            ),
                            Visibility(visible: _data.setTelp1AreaShow(3),child: SizedBox(width: MediaQuery.of(context).size.width / 37)),
                            Visibility(
                              visible: _data.setTelp1Show(3),
                              child: Expanded(
                                flex: 6,
                                child: TextFormField(
                                  autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && _data.setTelp1Mandatory(3)) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  keyboardType: TextInputType.number,
                                  inputFormatters: [
                                    WhitelistingTextInputFormatter.digitsOnly,
                                    LengthLimitingTextInputFormatter(10),
                                  ],
                                  controller: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerTelepon1,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      filled: true,
                                      fillColor: Colors.black12,
                                      labelText: 'Telepon 1',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8))),
                                  enabled: false,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Visibility(visible: _data.setTelp1AreaShow(3) || _data.setTelp1Show(3),child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                      Visibility(
                        visible: _data.setTelp2AreaShow(3) || _data.setTelp2Show(3),
                        child: Row(
                          children: [
                            Visibility(
                              visible: _data.setTelp2AreaShow(3),
                              child: Expanded(
                                flex: 4,
                                child: TextFormField(
                                  autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && _data.setTelp2AreaMandatory(3)) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  keyboardType: TextInputType.number,
                                  inputFormatters: [
                                    WhitelistingTextInputFormatter.digitsOnly,
                                    LengthLimitingTextInputFormatter(10),
                                  ],
                                  controller: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerTelepon2Area,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      filled: true,
                                      fillColor: Colors.black12,
                                      labelText: 'Telepon 2 (Area)',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8))),
                                  enabled: false,
                                ),
                              ),
                            ),
                            Visibility(visible: _data.setTelp2AreaShow(3),child: SizedBox(width: MediaQuery.of(context).size.width / 37)),
                            Visibility(
                              visible: _data.setTelp2Show(3),
                              child: Expanded(
                                flex: 6,
                                child: TextFormField(
                                  autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && _data.setTelp2Mandatory(3)) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  keyboardType: TextInputType.number,
                                  inputFormatters: [
                                    WhitelistingTextInputFormatter.digitsOnly,
                                    LengthLimitingTextInputFormatter(10),
                                  ],
                                  controller: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerTelepon2,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      filled: true,
                                      fillColor: Colors.black12,
                                      labelText: 'Telepon 2',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8))),
                                  enabled: false,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Visibility(visible: _data.setTelp2AreaShow(3) || _data.setTelp2Show(3), child: SizedBox(height: MediaQuery.of(context).size.height / 47)),
                      Visibility(
                        visible: _data.setFaxAreaShow(3) || _data.setFaxShow(3),
                        child: Row(
                          children: [
                            Visibility(
                              visible: _data.setFaxAreaShow(3),
                              child: Expanded(
                                flex: 4,
                                child: TextFormField(
                                  autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && _data.setFaxAreaMandatory(3)) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  keyboardType: TextInputType.number,
                                  inputFormatters: [
                                    WhitelistingTextInputFormatter.digitsOnly,
                                    LengthLimitingTextInputFormatter(10),
                                  ],
                                  controller: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerFaxArea,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      filled: true,
                                      fillColor: Colors.black12,
                                      labelText: 'Fax (Area)',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8))),
                                  enabled: false,
                                ),
                              ),
                            ),
                            Visibility(visible: _data.setFaxAreaShow(3), child: SizedBox(width: MediaQuery.of(context).size.width / 37)),
                            Visibility(
                              visible: _data.setFaxShow(3),
                              child: Expanded(
                                flex: 6,
                                child: TextFormField(
                                  autovalidate: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty && _data.setFaxMandatory(3)) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  keyboardType: TextInputType.number,
                                  inputFormatters: [
                                    WhitelistingTextInputFormatter.digitsOnly,
                                    LengthLimitingTextInputFormatter(10),
                                  ],
                                  controller: formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.controllerFax,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      filled: true,
                                      fillColor: Colors.black12,
                                      labelText: 'Fax',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8))),
                                  enabled: false,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            bottomNavigationBar: BottomAppBar(
              elevation: 0.0,
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Consumer<FormMCompanyPemegangSahamKelembagaanAlamatChangeNotifier>(
                    builder: (context, formMCompanyPemegangSahamKelembagaanAlamatChangeNotif, _) {
                      return RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(8.0)),
                          color: myPrimaryColor,
                          onPressed: () {
                            formMCompanyPemegangSahamKelembagaanAlamatChangeNotif.check(context);
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("DONE",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                      letterSpacing: 1.25))
                            ],
                          ));
                    },
                  )),
            ),
          ),
        );
      },
    );
  }
}
