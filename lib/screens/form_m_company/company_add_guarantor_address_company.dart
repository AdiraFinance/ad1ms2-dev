import 'package:ad1ms2_dev/models/address_guarantor_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/screens/form_m_company/form_m_company_add_guarantor_address_company_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

//ga kepake
class CompanyAddGuarantorAddressCompany extends StatefulWidget {
  final int flag;
  final int index;
  final AddressModelCompany addressModel;

  const CompanyAddGuarantorAddressCompany(
      {this.flag, this.index, this.addressModel});
  @override
  _CompanyAddGuarantorAddressCompanyState createState() =>
      _CompanyAddGuarantorAddressCompanyState();
}

class _CompanyAddGuarantorAddressCompanyState
    extends State<CompanyAddGuarantorAddressCompany> {
  Future<void> _setValueForEdit;

  @override
  void initState() {
    super.initState();
    if (widget.flag != 0) {
      if (widget.addressModel.isSameWithIdentity) {
        _setValueForEdit =
            Provider.of<FormMCompanyAddGuarantorAddressCompanyChangeNotifier>(
                    context,
                    listen: false)
                .setValueForEdit(widget.addressModel, context, widget.index,
                    widget.addressModel.isSameWithIdentity);
      } else {
        _setValueForEdit =
            Provider.of<FormMCompanyAddGuarantorAddressCompanyChangeNotifier>(
                    context,
                    listen: false)
                .setValueForEdit(widget.addressModel, context, widget.index,
                    widget.addressModel.isSameWithIdentity);
      }
    } else {
      Provider.of<FormMCompanyAddGuarantorAddressCompanyChangeNotifier>(context,
              listen: false)
          .addDataListAddressType(context, null);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans"),
      child: Scaffold(
        appBar: AppBar(
          title: Text(
              widget.flag == 0
                  ? "Tambah Alamat Penjamin Kelembagaan"
                  : "Edit Alamat Penjamin Kelembagaan",
              style: TextStyle(color: Colors.black)),
          centerTitle: true,
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: SingleChildScrollView(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width / 27,
                vertical: MediaQuery.of(context).size.height / 57),
            child: widget.flag == 0
                ? Consumer<
                    FormMCompanyAddGuarantorAddressCompanyChangeNotifier>(
                    builder: (context, formMCompanyAddGuarantorAddress, _) {
                      return Form(
                        key: formMCompanyAddGuarantorAddress.key,
                        onWillPop: _onWillPop,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            DropdownButtonFormField<JenisAlamatModel>(
                                autovalidate: formMCompanyAddGuarantorAddress
                                    .autoValidate,
                                validator: (e) {
                                  if (e == null) {
                                    return "Silahkan pilih jenis alamat";
                                  } else {
                                    return null;
                                  }
                                },
                                value: formMCompanyAddGuarantorAddress
                                    .jenisAlamatSelected,
                                onChanged: (value) {
                                  formMCompanyAddGuarantorAddress
                                      .jenisAlamatSelected = value;
                                },
                                onTap: () {
                                  FocusManager.instance.primaryFocus.unfocus();
                                },
                                decoration: InputDecoration(
                                  labelText: "Jenis Alamat",
                                  border: OutlineInputBorder(),
                                  contentPadding:
                                      EdgeInsets.symmetric(horizontal: 10),
                                ),
                                items: formMCompanyAddGuarantorAddress
                                    .listJenisAlamat
                                    .map((value) {
                                  return DropdownMenuItem<JenisAlamatModel>(
                                    value: value,
                                    child: Text(
                                      value.DESKRIPSI,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  );
                                }).toList()),
                            formMCompanyAddGuarantorAddress
                                        .jenisAlamatSelected !=
                                    null
                                ? formMCompanyAddGuarantorAddress
                                            .jenisAlamatSelected.KODE ==
                                        "02"
                                    ? Row(
                                        children: [
                                          Checkbox(
                                              value:
                                                  formMCompanyAddGuarantorAddress
                                                      .isSameWithIdentity,
                                              onChanged: (value) {
                                                formMCompanyAddGuarantorAddress
                                                    .isSameWithIdentity = value;
                                                formMCompanyAddGuarantorAddress
                                                    .setValueIsSameWithIdentity(
                                                        value, context, null);
                                              },
                                              activeColor: myPrimaryColor),
                                          SizedBox(width: 8),
                                          Text("Sama dengan identitas")
                                        ],
                                      )
                                    : SizedBox(
                                        height:
                                            MediaQuery.of(context).size.height /
                                                47)
                                : SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                            TextFormField(
                              autovalidate:
                                  formMCompanyAddGuarantorAddress.autoValidate,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              controller: formMCompanyAddGuarantorAddress
                                  .controllerAlamat,
                              enabled: formMCompanyAddGuarantorAddress
                                  .enableTfAddress,
                              style: TextStyle(color: Colors.black),
                              decoration: InputDecoration(
                                  filled: !formMCompanyAddGuarantorAddress
                                      .enableTfAddress,
                                  fillColor: !formMCompanyAddGuarantorAddress
                                          .enableTfAddress
                                      ? Colors.black12
                                      : Colors.white,
                                  labelText: 'Alamat',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              maxLines: 3,
                              keyboardType: TextInputType.text,
                              textCapitalization: TextCapitalization.characters,
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            Row(
                              children: [
                                Expanded(
                                  flex: 5,
                                  child: TextFormField(
                                    autovalidate:
                                        formMCompanyAddGuarantorAddress
                                            .autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    enabled: formMCompanyAddGuarantorAddress
                                        .enableTfRT,
                                    controller: formMCompanyAddGuarantorAddress
                                        .controllerRT,
                                    style: TextStyle(color: Colors.black),
                                    decoration: InputDecoration(
                                        filled: !formMCompanyAddGuarantorAddress
                                            .enableTfRT,
                                        fillColor:
                                            !formMCompanyAddGuarantorAddress
                                                    .enableTfRT
                                                ? Colors.black12
                                                : Colors.white,
                                        labelText: 'RT',
                                        labelStyle:
                                            TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8))),
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter.digitsOnly,
                                      // LengthLimitingTextInputFormatter(10),
                                    ],
                                    keyboardType: TextInputType.number,
                                  ),
                                ),
                                SizedBox(width: 8),
                                Expanded(
                                  flex: 5,
                                  child: TextFormField(
                                    autovalidate:
                                        formMCompanyAddGuarantorAddress
                                            .autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    enabled: formMCompanyAddGuarantorAddress
                                        .enableTfRW,
                                    controller: formMCompanyAddGuarantorAddress
                                        .controllerRW,
                                    style: TextStyle(color: Colors.black),
                                    decoration: InputDecoration(
                                        filled: !formMCompanyAddGuarantorAddress
                                            .enableTfRW,
                                        fillColor:
                                            !formMCompanyAddGuarantorAddress
                                                    .enableTfRW
                                                ? Colors.black12
                                                : Colors.white,
                                        labelText: 'RW',
                                        labelStyle:
                                            TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8))),
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter.digitsOnly,
                                      // LengthLimitingTextInputFormatter(10),
                                    ],
                                    keyboardType: TextInputType.number,
                                  ),
                                )
                              ],
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            FocusScope(
                              node: FocusScopeNode(),
                              child: TextFormField(
                                autovalidate: formMCompanyAddGuarantorAddress
                                    .autoValidate,
                                validator: (e) {
                                  if (e.isEmpty) {
                                    return "Tidak boleh kosong";
                                  } else {
                                    return null;
                                  }
                                },
                                onTap: () {
                                  FocusManager.instance.primaryFocus.unfocus();
                                  formMCompanyAddGuarantorAddress
                                      .searchKelurahan(context);
                                },
                                enabled: formMCompanyAddGuarantorAddress
                                    .enableTfKelurahan,
                                controller: formMCompanyAddGuarantorAddress
                                    .controllerKelurahan,
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                    filled: !formMCompanyAddGuarantorAddress
                                        .enableTfKelurahan,
                                    fillColor: !formMCompanyAddGuarantorAddress
                                            .enableTfKelurahan
                                        ? Colors.black12
                                        : Colors.white,
                                    labelText: 'Kelurahan',
                                    labelStyle: TextStyle(color: Colors.black),
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(8))),
                              ),
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            TextFormField(
                              autovalidate:
                                  formMCompanyAddGuarantorAddress.autoValidate,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              // enabled: formMCompanyAddGuarantorAddress.enableTfKecamatan,
                              controller: formMCompanyAddGuarantorAddress
                                  .controllerKecamatan,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  filled: !formMCompanyAddGuarantorAddress
                                      .enableTfKecamatan,
                                  fillColor: !formMCompanyAddGuarantorAddress
                                          .enableTfKecamatan
                                      ? Colors.black12
                                      : Colors.white,
                                  labelText: 'Kecamatan',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              enabled: false,
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            TextFormField(
                              autovalidate:
                                  formMCompanyAddGuarantorAddress.autoValidate,
                              validator: (e) {
                                if (e.isEmpty) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              // enabled: formMCompanyAddGuarantorAddress.enableTfKota,
                              controller: formMCompanyAddGuarantorAddress
                                  .controllerKota,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  filled: !formMCompanyAddGuarantorAddress
                                      .enableTfKota,
                                  fillColor: !formMCompanyAddGuarantorAddress
                                          .enableTfKota
                                      ? Colors.black12
                                      : Colors.white,
                                  labelText: 'Kabupaten/Kota',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              enabled: false,
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            Row(
                              children: [
                                Expanded(
                                  flex: 7,
                                  child: TextFormField(
                                    autovalidate:
                                        formMCompanyAddGuarantorAddress
                                            .autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    // enabled: formMCompanyAddGuarantorAddress.enableTfProv,
                                    controller: formMCompanyAddGuarantorAddress
                                        .controllerProvinsi,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        filled: !formMCompanyAddGuarantorAddress
                                            .enableTfProv,
                                        fillColor:
                                            !formMCompanyAddGuarantorAddress
                                                    .enableTfProv
                                                ? Colors.black12
                                                : Colors.white,
                                        labelText: 'Provinsi',
                                        labelStyle:
                                            TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8))),
                                    enabled: false,
                                  ),
                                ),
                                SizedBox(width: 8),
                                Expanded(
                                  flex: 3,
                                  child: TextFormField(
                                    autovalidate:
                                        formMCompanyAddGuarantorAddress
                                            .autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    // enabled: formMCompanyAddGuarantorAddress.enableTfPostalCode,
                                    controller: formMCompanyAddGuarantorAddress
                                        .controllerPostalCode,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        filled: !formMCompanyAddGuarantorAddress
                                            .enableTfPostalCode,
                                        fillColor:
                                            !formMCompanyAddGuarantorAddress
                                                    .enableTfPostalCode
                                                ? Colors.black12
                                                : Colors.white,
                                        labelText: 'Kode Pos',
                                        labelStyle:
                                            TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8))),
                                    enabled: false,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            Row(
                              children: [
                                Expanded(
                                  flex: 4,
                                  child: TextFormField(
                                    autovalidate:
                                        formMCompanyAddGuarantorAddress
                                            .autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    onChanged: (e) {
                                      formMCompanyAddGuarantorAddress
                                          .checkValidCodeArea1(e);
                                    },
                                    keyboardType: TextInputType.number,
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter.digitsOnly,
                                      // LengthLimitingTextInputFormatter(10),
                                    ],
                                    enabled: formMCompanyAddGuarantorAddress
                                        .enableTfTelephone1Area,
                                    controller: formMCompanyAddGuarantorAddress
                                        .controllerTelephone1Area,
                                    style: TextStyle(color: Colors.black),
                                    decoration: InputDecoration(
                                        filled: !formMCompanyAddGuarantorAddress
                                            .enableTfTelephone1Area,
                                        fillColor:
                                            !formMCompanyAddGuarantorAddress
                                                    .enableTfTelephone1Area
                                                ? Colors.black12
                                                : Colors.white,
                                        labelText: 'Telepon 1 (Area)',
                                        labelStyle:
                                            TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8))),
                                  ),
                                ),
                                SizedBox(
                                    width:
                                        MediaQuery.of(context).size.width / 37),
                                Expanded(
                                  flex: 6,
                                  child: TextFormField(
                                    autovalidate:
                                        formMCompanyAddGuarantorAddress
                                            .autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    enabled: formMCompanyAddGuarantorAddress
                                        .enableTfPhone1,
                                    keyboardType: TextInputType.number,
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter.digitsOnly,
                                      // LengthLimitingTextInputFormatter(10),
                                    ],
                                    controller: formMCompanyAddGuarantorAddress
                                        .controllerTelephone1,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        filled: !formMCompanyAddGuarantorAddress
                                            .enableTfPhone1,
                                        fillColor:
                                            !formMCompanyAddGuarantorAddress
                                                    .enableTfPhone1
                                                ? Colors.black12
                                                : Colors.white,
                                        labelText: 'Telepon 1',
                                        labelStyle:
                                            TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8))),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            Row(
                              children: [
                                Expanded(
                                  flex: 4,
                                  child: TextFormField(
                                    autovalidate:
                                        formMCompanyAddGuarantorAddress
                                            .autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    onChanged: (e) {
                                      formMCompanyAddGuarantorAddress
                                          .checkValidCodeArea2(e);
                                    },
                                    keyboardType: TextInputType.number,
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter.digitsOnly,
                                      // LengthLimitingTextInputFormatter(10),
                                    ],
                                    enabled: formMCompanyAddGuarantorAddress
                                        .enableTfTelephone2Area,
                                    controller: formMCompanyAddGuarantorAddress
                                        .controllerTelephone2Area,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: InputDecoration(
                                        filled: !formMCompanyAddGuarantorAddress
                                            .enableTfTelephone2Area,
                                        fillColor:
                                            !formMCompanyAddGuarantorAddress
                                                    .enableTfTelephone2Area
                                                ? Colors.black12
                                                : Colors.white,
                                        labelText: 'Telepon 2 (Area)',
                                        labelStyle:
                                            TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8))),
                                  ),
                                ),
                                SizedBox(
                                    width:
                                        MediaQuery.of(context).size.width / 37),
                                Expanded(
                                  flex: 6,
                                  child: TextFormField(
                                    autovalidate:
                                        formMCompanyAddGuarantorAddress
                                            .autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    keyboardType: TextInputType.number,
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter.digitsOnly,
                                      // LengthLimitingTextInputFormatter(10),
                                    ],
                                    enabled: formMCompanyAddGuarantorAddress
                                        .enableTfPhone2,
                                    controller: formMCompanyAddGuarantorAddress
                                        .controllerTelephone2,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        filled: !formMCompanyAddGuarantorAddress
                                            .enableTfPhone2,
                                        fillColor:
                                            !formMCompanyAddGuarantorAddress
                                                    .enableTfPhone2
                                                ? Colors.black12
                                                : Colors.white,
                                        labelText: 'Telepon 2',
                                        labelStyle:
                                            TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8))),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                                height:
                                    MediaQuery.of(context).size.height / 47),
                            Row(
                              children: [
                                Expanded(
                                  flex: 4,
                                  child: TextFormField(
                                    autovalidate:
                                        formMCompanyAddGuarantorAddress
                                            .autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    onChanged: (e) {
                                      formMCompanyAddGuarantorAddress
                                          .checkValidCodeAreaFax(e);
                                    },
                                    keyboardType: TextInputType.number,
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter.digitsOnly,
                                      // LengthLimitingTextInputFormatter(10),
                                    ],
                                    enabled: formMCompanyAddGuarantorAddress
                                        .enableTfFaxArea,
                                    controller: formMCompanyAddGuarantorAddress
                                        .controllerFaxArea,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: InputDecoration(
                                        filled: !formMCompanyAddGuarantorAddress
                                            .enableTfFaxArea,
                                        fillColor:
                                            !formMCompanyAddGuarantorAddress
                                                    .enableTfFaxArea
                                                ? Colors.black12
                                                : Colors.white,
                                        labelText: 'Fax (Area)',
                                        labelStyle:
                                            TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8))),
                                  ),
                                ),
                                SizedBox(
                                    width:
                                        MediaQuery.of(context).size.width / 37),
                                Expanded(
                                  flex: 6,
                                  child: TextFormField(
                                    autovalidate:
                                        formMCompanyAddGuarantorAddress
                                            .autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    keyboardType: TextInputType.number,
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter.digitsOnly,
                                      // LengthLimitingTextInputFormatter(10),
                                    ],
                                    enabled: formMCompanyAddGuarantorAddress
                                        .enableTfFax,
                                    controller: formMCompanyAddGuarantorAddress
                                        .controllerFax,
                                    style: new TextStyle(color: Colors.black),
                                    decoration: new InputDecoration(
                                        filled: !formMCompanyAddGuarantorAddress
                                            .enableTfFax,
                                        fillColor:
                                            !formMCompanyAddGuarantorAddress
                                                    .enableTfFax
                                                ? Colors.black12
                                                : Colors.white,
                                        labelText: 'Fax',
                                        labelStyle:
                                            TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8))),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      );
                    },
                  )
                : FutureBuilder(
                    future: _setValueForEdit,
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                      return Consumer<
                          FormMCompanyAddGuarantorAddressCompanyChangeNotifier>(
                        builder: (context, formMCompanyAddGuarantorAddress, _) {
                          return Form(
                            key: formMCompanyAddGuarantorAddress.key,
                            onWillPop: _onWillPop,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                DropdownButtonFormField<JenisAlamatModel>(
                                    autovalidate:
                                        formMCompanyAddGuarantorAddress
                                            .autoValidate,
                                    validator: (e) {
                                      if (e == null) {
                                        return "Silahkan pilih jenis alamat";
                                      } else {
                                        return null;
                                      }
                                    },
                                    value: formMCompanyAddGuarantorAddress
                                        .jenisAlamatSelected,
                                    onChanged: (value) {
                                      formMCompanyAddGuarantorAddress
                                          .jenisAlamatSelected = value;
                                    },
                                    onTap: () {
                                      FocusManager.instance.primaryFocus.unfocus();
                                    },
                                    decoration: InputDecoration(
                                      labelText: "Jenis Alamat",
                                      border: OutlineInputBorder(),
                                      contentPadding:
                                          EdgeInsets.symmetric(horizontal: 10),
                                    ),
                                    items: formMCompanyAddGuarantorAddress
                                        .listJenisAlamat
                                        .map((value) {
                                      return DropdownMenuItem<JenisAlamatModel>(
                                        value: value,
                                        child: Text(
                                          value.DESKRIPSI,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      );
                                    }).toList()),
                                formMCompanyAddGuarantorAddress
                                            .jenisAlamatSelected !=
                                        null
                                    ? formMCompanyAddGuarantorAddress
                                                .jenisAlamatSelected.KODE ==
                                            "02"
                                        ? Row(
                                            children: [
                                              Checkbox(
                                                  value:
                                                      formMCompanyAddGuarantorAddress
                                                          .isSameWithIdentity,
                                                  onChanged: (value) {
                                                    formMCompanyAddGuarantorAddress
                                                            .isSameWithIdentity =
                                                        value;
                                                    formMCompanyAddGuarantorAddress
                                                        .setValueIsSameWithIdentity(
                                                            value,
                                                            context,
                                                            null);
                                                  },
                                                  activeColor: primaryOrange),
                                              SizedBox(width: 8),
                                              Text("Sama dengan identitas")
                                            ],
                                          )
                                        : SizedBox(
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height /
                                                47)
                                    : SizedBox(
                                        height:
                                            MediaQuery.of(context).size.height /
                                                47),
                                TextFormField(
                                  autovalidate: formMCompanyAddGuarantorAddress
                                      .autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  controller: formMCompanyAddGuarantorAddress
                                      .controllerAlamat,
                                  enabled: formMCompanyAddGuarantorAddress
                                      .enableTfAddress,
                                  style: TextStyle(color: Colors.black),
                                  decoration: InputDecoration(
                                      filled: !formMCompanyAddGuarantorAddress
                                          .enableTfAddress,
                                      fillColor:
                                          !formMCompanyAddGuarantorAddress
                                                  .enableTfAddress
                                              ? Colors.black12
                                              : Colors.white,
                                      labelText: 'Alamat',
                                      labelStyle:
                                          TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(8))),
                                  maxLines: 3,
                                  keyboardType: TextInputType.text,
                                  textCapitalization: TextCapitalization.characters,
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                Row(
                                  children: [
                                    Expanded(
                                      flex: 5,
                                      child: TextFormField(
                                        autovalidate:
                                            formMCompanyAddGuarantorAddress
                                                .autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        enabled: formMCompanyAddGuarantorAddress
                                            .enableTfRT,
                                        controller:
                                            formMCompanyAddGuarantorAddress
                                                .controllerRT,
                                        style: TextStyle(color: Colors.black),
                                        decoration: InputDecoration(
                                            filled:
                                                !formMCompanyAddGuarantorAddress
                                                    .enableTfRT,
                                            fillColor:
                                                !formMCompanyAddGuarantorAddress
                                                        .enableTfRT
                                                    ? Colors.black12
                                                    : Colors.white,
                                            labelText: 'RT',
                                            labelStyle:
                                                TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                        inputFormatters: [
                                          WhitelistingTextInputFormatter.digitsOnly,
                                          // LengthLimitingTextInputFormatter(10),
                                        ],
                                        keyboardType: TextInputType.number,
                                      ),
                                    ),
                                    SizedBox(width: 8),
                                    Expanded(
                                      flex: 5,
                                      child: TextFormField(
                                        autovalidate:
                                            formMCompanyAddGuarantorAddress
                                                .autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        enabled: formMCompanyAddGuarantorAddress
                                            .enableTfRW,
                                        controller:
                                            formMCompanyAddGuarantorAddress
                                                .controllerRW,
                                        style: TextStyle(color: Colors.black),
                                        decoration: InputDecoration(
                                            filled:
                                                !formMCompanyAddGuarantorAddress
                                                    .enableTfRW,
                                            fillColor:
                                                !formMCompanyAddGuarantorAddress
                                                        .enableTfRW
                                                    ? Colors.black12
                                                    : Colors.white,
                                            labelText: 'RW',
                                            labelStyle:
                                                TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                        inputFormatters: [
                                          WhitelistingTextInputFormatter.digitsOnly,
                                          // LengthLimitingTextInputFormatter(10),
                                        ],
                                        keyboardType: TextInputType.number,
                                      ),
                                    )
                                  ],
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                FocusScope(
                                  node: FocusScopeNode(),
                                  child: TextFormField(
                                    autovalidate:
                                        formMCompanyAddGuarantorAddress
                                            .autoValidate,
                                    validator: (e) {
                                      if (e.isEmpty) {
                                        return "Tidak boleh kosong";
                                      } else {
                                        return null;
                                      }
                                    },
                                    onTap: () {
                                      FocusManager.instance.primaryFocus.unfocus();
                                      formMCompanyAddGuarantorAddress
                                          .searchKelurahan(context);
                                    },
                                    enabled: formMCompanyAddGuarantorAddress
                                        .enableTfKelurahan,
                                    controller: formMCompanyAddGuarantorAddress
                                        .controllerKelurahan,
                                    style: TextStyle(color: Colors.black),
                                    decoration: InputDecoration(
                                        filled: !formMCompanyAddGuarantorAddress
                                            .enableTfKelurahan,
                                        fillColor:
                                            !formMCompanyAddGuarantorAddress
                                                    .enableTfKelurahan
                                                ? Colors.black12
                                                : Colors.white,
                                        labelText: 'Kelurahan',
                                        labelStyle:
                                            TextStyle(color: Colors.black),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(8))),
                                  ),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                TextFormField(
                                  autovalidate: formMCompanyAddGuarantorAddress
                                      .autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  enabled: formMCompanyAddGuarantorAddress
                                      .enableTfKecamatan,
                                  controller: formMCompanyAddGuarantorAddress
                                      .controllerKecamatan,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      filled: !formMCompanyAddGuarantorAddress
                                          .enableTfKecamatan,
                                      fillColor:
                                          !formMCompanyAddGuarantorAddress
                                                  .enableTfKecamatan
                                              ? Colors.black12
                                              : Colors.white,
                                      labelText: 'Kecamatan',
                                      labelStyle:
                                          TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(8))),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                TextFormField(
                                  autovalidate: formMCompanyAddGuarantorAddress
                                      .autoValidate,
                                  validator: (e) {
                                    if (e.isEmpty) {
                                      return "Tidak boleh kosong";
                                    } else {
                                      return null;
                                    }
                                  },
                                  enabled: formMCompanyAddGuarantorAddress
                                      .enableTfKota,
                                  controller: formMCompanyAddGuarantorAddress
                                      .controllerKota,
                                  style: new TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      filled: !formMCompanyAddGuarantorAddress
                                          .enableTfKota,
                                      fillColor:
                                          !formMCompanyAddGuarantorAddress
                                                  .enableTfKota
                                              ? Colors.black12
                                              : Colors.white,
                                      labelText: 'Kabupaten/Kota',
                                      labelStyle:
                                          TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(8))),
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                Row(
                                  children: [
                                    Expanded(
                                      flex: 7,
                                      child: TextFormField(
                                        autovalidate:
                                            formMCompanyAddGuarantorAddress
                                                .autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        enabled: formMCompanyAddGuarantorAddress
                                            .enableTfProv,
                                        controller:
                                            formMCompanyAddGuarantorAddress
                                                .controllerProvinsi,
                                        style:
                                            new TextStyle(color: Colors.black),
                                        decoration: new InputDecoration(
                                            filled:
                                                !formMCompanyAddGuarantorAddress
                                                    .enableTfProv,
                                            fillColor:
                                                !formMCompanyAddGuarantorAddress
                                                        .enableTfProv
                                                    ? Colors.black12
                                                    : Colors.white,
                                            labelText: 'Provinsi',
                                            labelStyle:
                                                TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                      ),
                                    ),
                                    SizedBox(width: 8),
                                    Expanded(
                                      flex: 3,
                                      child: TextFormField(
                                        autovalidate:
                                            formMCompanyAddGuarantorAddress
                                                .autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        enabled: formMCompanyAddGuarantorAddress
                                            .enableTfPostalCode,
                                        controller:
                                            formMCompanyAddGuarantorAddress
                                                .controllerPostalCode,
                                        style:
                                            new TextStyle(color: Colors.black),
                                        decoration: new InputDecoration(
                                            filled:
                                                !formMCompanyAddGuarantorAddress
                                                    .enableTfPostalCode,
                                            fillColor:
                                                !formMCompanyAddGuarantorAddress
                                                        .enableTfPostalCode
                                                    ? Colors.black12
                                                    : Colors.white,
                                            labelText: 'Kode Pos',
                                            labelStyle:
                                                TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                TextFormField(
                                  // autovalidate:
                                  // formMAddAlamatKorespon.autoValidate,
                                  // validator: (e) {
                                  //     if (e.isEmpty) {
                                  //         return "Tidak boleh kosong";
                                  //     } else {
                                  //         return null;
                                  //     }
                                  // },
                                  readOnly: true,
                                  onTap: (){
                                    formMCompanyAddGuarantorAddress.setLocationAddressByMap(context);
                                  },
                                  maxLines: 3,
                                  controller: formMCompanyAddGuarantorAddress.controllerAddressFromMap,
                                  style: TextStyle(color: Colors.black),
                                  decoration: new InputDecoration(
                                      labelText: 'Alamat Lokasi',
                                      labelStyle:
                                      TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius:
                                          BorderRadius.circular(8))),
                                  keyboardType: TextInputType.number,
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                Row(
                                  children: [
                                    Expanded(
                                      flex: 4,
                                      child: TextFormField(
                                        autovalidate:
                                            formMCompanyAddGuarantorAddress
                                                .autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        onChanged: (e) {
                                          formMCompanyAddGuarantorAddress
                                              .checkValidCodeArea1(e);
                                        },
                                        keyboardType: TextInputType.number,
                                        inputFormatters: [
                                          WhitelistingTextInputFormatter
                                              .digitsOnly,
                                          // LengthLimitingTextInputFormatter(10),
                                        ],
                                        enabled: formMCompanyAddGuarantorAddress
                                            .enableTfTelephone1Area,
                                        controller:
                                            formMCompanyAddGuarantorAddress
                                                .controllerTelephone1Area,
                                        style: TextStyle(color: Colors.black),
                                        decoration: InputDecoration(
                                            filled:
                                                !formMCompanyAddGuarantorAddress
                                                    .enableTfTelephone1Area,
                                            fillColor:
                                                !formMCompanyAddGuarantorAddress
                                                        .enableTfTelephone1Area
                                                    ? Colors.black12
                                                    : Colors.white,
                                            labelText: 'Telepon 1 (Area)',
                                            labelStyle:
                                                TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                      ),
                                    ),
                                    SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width /
                                                37),
                                    Expanded(
                                      flex: 6,
                                      child: TextFormField(
                                        autovalidate:
                                            formMCompanyAddGuarantorAddress
                                                .autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        enabled: formMCompanyAddGuarantorAddress
                                            .enableTfPhone1,
                                        keyboardType: TextInputType.number,
                                        inputFormatters: [
                                          WhitelistingTextInputFormatter
                                              .digitsOnly,
                                          // LengthLimitingTextInputFormatter(10),
                                        ],
                                        controller:
                                            formMCompanyAddGuarantorAddress
                                                .controllerTelephone1,
                                        style:
                                            new TextStyle(color: Colors.black),
                                        decoration: new InputDecoration(
                                            filled:
                                                !formMCompanyAddGuarantorAddress
                                                    .enableTfPhone1,
                                            fillColor:
                                                !formMCompanyAddGuarantorAddress
                                                        .enableTfPhone1
                                                    ? Colors.black12
                                                    : Colors.white,
                                            labelText: 'Telepon 1',
                                            labelStyle:
                                                TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                Row(
                                  children: [
                                    Expanded(
                                      flex: 4,
                                      child: TextFormField(
                                        autovalidate:
                                            formMCompanyAddGuarantorAddress
                                                .autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        onChanged: (e) {
                                          formMCompanyAddGuarantorAddress
                                              .checkValidCodeArea2(e);
                                        },
                                        keyboardType: TextInputType.number,
                                        inputFormatters: [
                                          WhitelistingTextInputFormatter
                                              .digitsOnly,
                                          // LengthLimitingTextInputFormatter(10),
                                        ],
                                        enabled: formMCompanyAddGuarantorAddress
                                            .enableTfTelephone2Area,
                                        controller:
                                            formMCompanyAddGuarantorAddress
                                                .controllerTelephone2Area,
                                        style:
                                            new TextStyle(color: Colors.black),
                                        decoration: InputDecoration(
                                            filled:
                                                !formMCompanyAddGuarantorAddress
                                                    .enableTfTelephone2Area,
                                            fillColor:
                                                !formMCompanyAddGuarantorAddress
                                                        .enableTfTelephone2Area
                                                    ? Colors.black12
                                                    : Colors.white,
                                            labelText: 'Telepon 2 (Area)',
                                            labelStyle:
                                                TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                      ),
                                    ),
                                    SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width /
                                                37),
                                    Expanded(
                                      flex: 6,
                                      child: TextFormField(
                                        autovalidate:
                                            formMCompanyAddGuarantorAddress
                                                .autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        keyboardType: TextInputType.number,
                                        inputFormatters: [
                                          WhitelistingTextInputFormatter
                                              .digitsOnly,
                                          // LengthLimitingTextInputFormatter(10),
                                        ],
                                        enabled: formMCompanyAddGuarantorAddress
                                            .enableTfPhone2,
                                        controller:
                                            formMCompanyAddGuarantorAddress
                                                .controllerTelephone2,
                                        style:
                                            new TextStyle(color: Colors.black),
                                        decoration: new InputDecoration(
                                            filled:
                                                !formMCompanyAddGuarantorAddress
                                                    .enableTfPhone2,
                                            fillColor:
                                                !formMCompanyAddGuarantorAddress
                                                        .enableTfPhone2
                                                    ? Colors.black12
                                                    : Colors.white,
                                            labelText: 'Telepon 2',
                                            labelStyle:
                                                TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                    height: MediaQuery.of(context).size.height /
                                        47),
                                Row(
                                  children: [
                                    Expanded(
                                      flex: 4,
                                      child: TextFormField(
                                        autovalidate:
                                            formMCompanyAddGuarantorAddress
                                                .autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        onChanged: (e) {
                                          formMCompanyAddGuarantorAddress
                                              .checkValidCodeAreaFax(e);
                                        },
                                        keyboardType: TextInputType.number,
                                        inputFormatters: [
                                          WhitelistingTextInputFormatter
                                              .digitsOnly,
                                          // LengthLimitingTextInputFormatter(10),
                                        ],
                                        enabled: formMCompanyAddGuarantorAddress
                                            .enableTfFaxArea,
                                        controller:
                                            formMCompanyAddGuarantorAddress
                                                .controllerFaxArea,
                                        style:
                                            new TextStyle(color: Colors.black),
                                        decoration: InputDecoration(
                                            filled:
                                                !formMCompanyAddGuarantorAddress
                                                    .enableTfFaxArea,
                                            fillColor:
                                                !formMCompanyAddGuarantorAddress
                                                        .enableTfFaxArea
                                                    ? Colors.black12
                                                    : Colors.white,
                                            labelText: 'Fax (Area)',
                                            labelStyle:
                                                TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                      ),
                                    ),
                                    SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width /
                                                37),
                                    Expanded(
                                      flex: 6,
                                      child: TextFormField(
                                        autovalidate:
                                            formMCompanyAddGuarantorAddress
                                                .autoValidate,
                                        validator: (e) {
                                          if (e.isEmpty) {
                                            return "Tidak boleh kosong";
                                          } else {
                                            return null;
                                          }
                                        },
                                        keyboardType: TextInputType.number,
                                        inputFormatters: [
                                          WhitelistingTextInputFormatter
                                              .digitsOnly,
                                          // LengthLimitingTextInputFormatter(10),
                                        ],
                                        enabled: formMCompanyAddGuarantorAddress
                                            .enableTfFax,
                                        controller:
                                            formMCompanyAddGuarantorAddress
                                                .controllerFax,
                                        style:
                                            new TextStyle(color: Colors.black),
                                        decoration: new InputDecoration(
                                            filled:
                                                !formMCompanyAddGuarantorAddress
                                                    .enableTfFax,
                                            fillColor:
                                                !formMCompanyAddGuarantorAddress
                                                        .enableTfFax
                                                    ? Colors.black12
                                                    : Colors.white,
                                            labelText: 'Fax',
                                            labelStyle:
                                                TextStyle(color: Colors.black),
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          );
                        },
                      );
                    })),
        bottomNavigationBar: BottomAppBar(
          elevation: 0.0,
          child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(8.0)),
                  color: myPrimaryColor,
                  onPressed: () {
                    if (widget.flag == 0) {
                      Provider.of<FormMCompanyAddGuarantorAddressCompanyChangeNotifier>(
                              context,
                              listen: true)
                          .check(context, widget.flag, null);
                    } else {
                      Provider.of<FormMCompanyAddGuarantorAddressCompanyChangeNotifier>(
                              context,
                              listen: true)
                          .check(context, widget.flag, widget.index);
                    }
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(widget.flag == 0 ? "SAVE" : "UPDATE",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 1.25))
                    ],
                  ))),
        ),
      ),
    );
  }

  Future<bool> _onWillPop() async {
    var _provider =
        Provider.of<FormMCompanyAddGuarantorAddressCompanyChangeNotifier>(
            context,
            listen: false);
    if (widget.flag == 0) {
      if (_provider.jenisAlamatSelected != null ||
          _provider.controllerAlamat.text != "" ||
          _provider.controllerRT.text != "" ||
          _provider.controllerRW.text != "" ||
          _provider.controllerKelurahan.text != "" ||
          _provider.controllerKecamatan.text != "" ||
          _provider.controllerKota.text != "" ||
          _provider.controllerProvinsi.text != "" ||
          _provider.controllerPostalCode.text != "" ||
          _provider.controllerTelephone1Area.text != "" ||
          _provider.controllerTelephone1.text != "" ||
          _provider.controllerTelephone2Area.text != "" ||
          _provider.controllerTelephone2.text != "" ||
          _provider.controllerFaxArea.text != "" ||
          _provider.controllerFax.text != "") {
        return (await showDialog(
              context: context,
              builder: (myContext) => AlertDialog(
                title: new Text('Warning'),
                content: new Text('Simpan perubahan?'),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      _provider.check(context, widget.flag, null);
                      Navigator.pop(context);
                    },
                    child: new Text('Ya', style: TextStyle(fontSize: 12, color: Colors.grey),),
                  ),
                  new FlatButton(
                    onPressed: () => Navigator.of(context).pop(true),
                    child: new Text('Tidak'),
                  ),
                ],
              ),
            )) ??
            false;
      } else {
        return true;
      }
    } else {
      if (_provider.jenisAlamatSelectedTemp.KODE !=
              _provider.jenisAlamatSelected.KODE ||
          _provider.alamatTemp != _provider.controllerAlamat.text ||
          _provider.rtTemp != _provider.controllerRT.text ||
          _provider.rwTemp != _provider.controllerRW.text ||
          _provider.kelurahanTemp != _provider.controllerKelurahan.text ||
          _provider.kecamatanTemp != _provider.controllerKecamatan.text ||
          _provider.kotaTemp != _provider.controllerKota.text ||
          _provider.provinsiTemp != _provider.controllerProvinsi.text ||
          _provider.postalCodeTemp != _provider.controllerPostalCode.text ||
          _provider.phone1AreaTemp != _provider.controllerTelephone1Area.text ||
          _provider.phone1Temp != _provider.controllerTelephone1.text ||
          _provider.phone2AreaTemp != _provider.controllerTelephone2Area.text ||
          _provider.phone2Temp != _provider.controllerTelephone2.text ||
          _provider.faxAreaTemp != _provider.controllerFaxArea.text ||
          _provider.faxTemp != _provider.controllerFax.text) {
        return (await showDialog(
              context: context,
              builder: (myContext) => AlertDialog(
                title: new Text('Warning'),
                content: new Text('Simpan perubahan?'),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      _provider.check(context, widget.flag, widget.index);
                      Navigator.pop(context);
                    },
                    child: new Text('Ya', style: TextStyle(fontSize: 12, color: Colors.grey),),
                  ),
                  new FlatButton(
                    onPressed: () => Navigator.of(context).pop(true),
                    child: new Text('Tidak'),
                  ),
                ],
              ),
            )) ??
            false;
      } else {
        return true;
      }
    }
  }
}
