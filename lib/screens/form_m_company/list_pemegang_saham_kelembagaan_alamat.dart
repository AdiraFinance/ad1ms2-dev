import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/form_m_company/add_pemegang_saham_kelembagaan_alamat.dart';
import 'package:ad1ms2_dev/shared/add_pemegang_saham_lembaga_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_company_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_add_pemegang_saham_kelembagaan_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_pemegang_saham_kelembagaan_alamat_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ListPemegangSahamKelembagaanAddress extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: Colors.yellow),
      child: Scaffold(
        appBar: AppBar(
          title: Text("List Alamat Korespondensi", style: TextStyle(color: Colors.black)),
          centerTitle: true,
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        bottomSheet: Container(
            padding: EdgeInsets.all(8),
            height: 100,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                    "∙ Pilih salah satu alamat untuk mengubah detail alamat",
                    style: TextStyle(fontWeight: FontWeight.w400)
                ),
                Text.rich(
                  TextSpan(
                    children: [
                      TextSpan(
                          text: "∙ Klik tanda",
                          style: TextStyle(fontWeight: FontWeight.w400)
                      ),
                      WidgetSpan(
                        child: Icon(Icons.more_vert, color: Colors.grey),
                      ),
                      TextSpan(
                          text: "pada salah satu alamat untuk memilih sebagai Alamat Korespondensi atau menghapus alamat",
                          style: TextStyle(fontWeight: FontWeight.w400)
                      )
                    ],
                  ),
                ),
              ],
            )
        ),
        body: Consumer<AddPemegangSahamLembagaChangeNotif>(
          builder: (context, _provider, _) {
            return _provider.listPemegangSahamKelembagaanAddress.isEmpty
                ?
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset("img/alamat_kosong.png", height: MediaQuery.of(context).size.height / 9,),
                  SizedBox(height: MediaQuery.of(context).size.height / 47,),
                  Text("Tambah Alamat", style: TextStyle(color: Colors.grey, fontSize: 16),)
                ],
              ),
            )
                :
            ListView.builder(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 77,
                  horizontal: MediaQuery.of(context).size.width / 47),
              itemBuilder: (context, index) {
                return InkWell(
                  // onLongPress: () {
                  //   _provider.selectedIndex = index;
                  //   _provider.controllerAddress.clear();
                  //   _provider.setAddress(_provider.listPemegangSahamKelembagaanAddress[index]);
                  //   Navigator.pop(context);
                  // },
                  onTap: () {
                    Navigator.push(context,MaterialPageRoute(
                            builder: (context) => CompanyAddPemegangSahamKelembagaanAddress(
                                  flag: 1,
                                  index: index,
                                  addressModel: _provider.listPemegangSahamKelembagaanAddress[index],
                                  typeAddress: 3,
                                )
                        )
                    );
                  },
                  child: _provider.listPemegangSahamKelembagaanAddress[index].active == 0 ?
                  Card(
                    shape: _provider.selectedIndex == index
                        ? RoundedRectangleBorder(
                            side: BorderSide(color: primaryOrange, width: 2),
                            borderRadius: BorderRadius.all(Radius.circular(4)))
                        : null,
                    elevation: 3.3,
                    child: Padding(
                      padding: const EdgeInsets.all(16),
                      child: Stack(
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text("${_provider.listPemegangSahamKelembagaanAddress[index].jenisAlamatModel.KODE} - ${_provider.listPemegangSahamKelembagaanAddress[index].jenisAlamatModel.DESKRIPSI}",
                                  style: TextStyle(fontWeight: FontWeight.bold)
                              ),
                              SizedBox(height:MediaQuery.of(context).size.height / 97),
                              Text("${_provider.listPemegangSahamKelembagaanAddress[index].address}",
                                style: TextStyle(color: Colors.grey, fontSize: 13),
                              ),
                              Text("${_provider.listPemegangSahamKelembagaanAddress[index].kelurahanModel.KEC_ID} - ${_provider.listPemegangSahamKelembagaanAddress[index].kelurahanModel.KEC_NAME},",
                                style: TextStyle(color: Colors.grey, fontSize: 13),
                              ),
                              Text("${_provider.listPemegangSahamKelembagaanAddress[index].kelurahanModel.KABKOT_ID} - ${_provider.listPemegangSahamKelembagaanAddress[index].kelurahanModel.KABKOT_NAME},",
                                style: TextStyle(color: Colors.grey, fontSize: 13),
                              ),
                              Text("${_provider.listPemegangSahamKelembagaanAddress[index].kelurahanModel.PROV_ID} - ${_provider.listPemegangSahamKelembagaanAddress[index].kelurahanModel.PROV_NAME}",
                                style: TextStyle(color: Colors.grey, fontSize: 13),
                              ),
                              Text("${_provider.listPemegangSahamKelembagaanAddress[index].kelurahanModel.ZIPCODE}",
                                style: TextStyle(color: Colors.grey, fontSize: 13),),
                              SizedBox(height:MediaQuery.of(context).size.height / 97),
                              Text("${_provider.listPemegangSahamKelembagaanAddress[index].phoneArea1} ${_provider.listPemegangSahamKelembagaanAddress[index].phone1}",
                                style: TextStyle(color: Colors.grey, fontSize: 13),)
                            ],
                          ),
                          Align(
                              alignment: Alignment.topRight,
                              child: Column(
                                children: [
                                  GestureDetector(
                                      onTap: () {_provider.moreDialog(context, index);},
                                      child: Icon(Icons.more_vert, color: Colors.grey)
                                  ),
                                  SizedBox(height: 3),
                                  _provider.listPemegangSahamKelembagaanAddress[index].isEditAddress ? Icon(Icons.edit, color: Colors.purple) : Text(""),
                                ],
                              )
                          )
                        ],
                      ),
                    ),
                  )
                      :
                  SizedBox()
                );
              },
              itemCount: _provider.listPemegangSahamKelembagaanAddress.length,
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Provider.of<FormMAddAddressCompanyChangeNotifier>(context, listen: false).clearData();
            Navigator.push(context, MaterialPageRoute(
                    builder: (context) => CompanyAddPemegangSahamKelembagaanAddress(
                          flag: 0,
                          index: null,
                          addressModel: null,
                          typeAddress: 3,
                        )
                )
            );
          },
          backgroundColor: myPrimaryColor,
          child: Icon(Icons.add, color: Colors.black),
        ),
      ),
    );
  }
}
