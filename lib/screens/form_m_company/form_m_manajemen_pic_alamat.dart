import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/form_m_company/list_manajemen_pic_alamat.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_manajemen_pic_alamat_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class FormMCompanyManajemenPICAlamatProvider extends StatefulWidget {
  @override
  _FormMCompanyManajemenPICAlamatProviderState createState() => _FormMCompanyManajemenPICAlamatProviderState();
}

class _FormMCompanyManajemenPICAlamatProviderState extends State<FormMCompanyManajemenPICAlamatProvider> {

  @override
  void initState() {
    super.initState();
    Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context,listen: false).setPreference();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<FormMCompanyManajemenPICAlamatChangeNotifier>(
      builder: (context, formMCompanyManajemenPICAlamatChangeNotif, _) {
        return Theme(
          data: ThemeData(
              primaryColor: Colors.black,
              accentColor: myPrimaryColor,
              fontFamily: "NunitoSans",
              primarySwatch: Colors.yellow),
          child: Scaffold(
            appBar: AppBar(
              title:
              Text("Alamat Manajemen PIC", style: TextStyle(color: Colors.black)),
              centerTitle: true,
              backgroundColor: myPrimaryColor,
              iconTheme: IconThemeData(color: Colors.black),
            ),
            body: Form(
              key: formMCompanyManajemenPICAlamatChangeNotif.keyForm,
              onWillPop: formMCompanyManajemenPICAlamatChangeNotif.onBackPress,
              child: ListView(
                padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height/57,
                    horizontal: MediaQuery.of(context).size.width/37
                ),
                children: [
                  Visibility(
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressTypeShow(4),
                    child: TextFormField(
                      autovalidate: formMCompanyManajemenPICAlamatChangeNotif.autoValidate,
                      validator: (e) {
                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressTypeMan(4)) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: formMCompanyManajemenPICAlamatChangeNotif.controllerAddressType,
                      style: new TextStyle(color: Colors.black),
                      decoration: new InputDecoration(
                          labelText: 'Jenis Alamat',
                          labelStyle: TextStyle(color: Colors.black),
                          filled: true,
                          fillColor: Colors.black12,
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8))),
                      enabled: false,
                    ),
                  ),
                  Visibility(
                      visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressTypeShow(4),
                      child: SizedBox(height: MediaQuery.of(context).size.height / 47)
                  ),
                  Visibility(
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressShow(4),
                    child: TextFormField(
                      autovalidate: formMCompanyManajemenPICAlamatChangeNotif.autoValidate,
                      validator: (e) {
                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressMan(4)) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      // onTap: () {
                      //   Navigator.push(context,
                      //       MaterialPageRoute(
                      //           builder: (context) => ListManajemenPICAlamat()));
                      // },
                      controller: formMCompanyManajemenPICAlamatChangeNotif.controllerAddress,
                      style: new TextStyle(color: Colors.black),
                      decoration: new InputDecoration(
                          labelText: 'Alamat',
                          labelStyle: TextStyle(color: Colors.black),
                          filled: true,
                          fillColor: Colors.black12,
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8)
                          )
                      ),
                      enabled: false,
                      // readOnly: true,
                    ),
                  ),
                  Visibility(
                      child: SizedBox(height: MediaQuery.of(context).size.height / 47),
                      visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setAddressShow(4)
                  ),
                  Row(
                    children: [
                      Expanded(
                        flex: 5,
                        child: Visibility(
                          visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRTShow(4),
                          child: TextFormField(
                            autovalidate: formMCompanyManajemenPICAlamatChangeNotif.autoValidate,
                            validator: (e) {
                              if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRTMan(4)) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            controller: formMCompanyManajemenPICAlamatChangeNotif.controllerRT,
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                                filled: true,
                                fillColor: Colors.black12,
                                labelText: 'RT',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8))),
                            enabled: false,
                          ),
                        ),
                      ),
                      SizedBox(width: 8),
                      Expanded(
                          flex: 5,
                          child: Visibility(
                            visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRWShow(4),
                            child: TextFormField(
                              autovalidate: formMCompanyManajemenPICAlamatChangeNotif.autoValidate,
                              validator: (e) {
                                if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRWMan(4)) {
                                  return "Tidak boleh kosong";
                                } else {
                                  return null;
                                }
                              },
                              controller: formMCompanyManajemenPICAlamatChangeNotif.controllerRW,
                              style: new TextStyle(color: Colors.black),
                              decoration: new InputDecoration(
                                  filled: true,
                                  fillColor: Colors.black12,
                                  labelText: 'RW',
                                  labelStyle: TextStyle(color: Colors.black),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8))),
                              enabled: false,
                            ),
                          ))
                    ],
                  ),
                  Visibility(
                    child: SizedBox(height: MediaQuery.of(context).size.height / 47),
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRWShow(4)
                        && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setRTShow(4),
                  ),
                  Visibility(
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKelurahanShow(4),
                    child: TextFormField(
                      autovalidate: formMCompanyManajemenPICAlamatChangeNotif.autoValidate,
                      validator: (e) {
                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKelurahanMan(4)) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: formMCompanyManajemenPICAlamatChangeNotif.controllerKelurahan,
                      style: new TextStyle(color: Colors.black),
                      decoration: new InputDecoration(
                          filled: true,
                          fillColor: Colors.black12,
                          labelText: 'Kelurahan',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8))),
                      enabled: false,
                    ),
                  ),
                  Visibility(child: SizedBox(height: MediaQuery.of(context).size.height / 47),visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKelurahanShow(4),),
                  Visibility(
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKecamatanShow(4),
                    child: TextFormField(
                      autovalidate: formMCompanyManajemenPICAlamatChangeNotif.autoValidate,
                      validator: (e) {
                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKecamatanMan(4)) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: formMCompanyManajemenPICAlamatChangeNotif.controllerKecamatan,
                      style: new TextStyle(color: Colors.black),
                      decoration: new InputDecoration(
                          filled: true,
                          fillColor: Colors.black12,
                          labelText: 'Kecamatan',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8))),
                      enabled: false,
                    ),
                  ),
                  Visibility(
                    child: SizedBox(height: MediaQuery.of(context).size.height / 47),
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKecamatanShow(4),
                  ),
                  Visibility(
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKabKotShow(4),
                    child: TextFormField(
                      autovalidate: formMCompanyManajemenPICAlamatChangeNotif.autoValidate,
                      validator: (e) {
                        if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKabKotMan(4)) {
                          return "Tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      controller: formMCompanyManajemenPICAlamatChangeNotif.controllerKota,
                      style: TextStyle(color: Colors.black),
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.black12,
                          labelText: 'Kabupaten/Kota',
                          labelStyle: TextStyle(color: Colors.black),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8))),
                      enabled: false,
                    ),
                  ),
                  Visibility(child: SizedBox(height: MediaQuery.of(context).size.height / 47),visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKabKotShow(4)),
                  Row(
                    children: [
                      Expanded(
                        flex: 7,
                        child:Visibility(
                          visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setProvinsiShow(4),
                          child:TextFormField(
                            autovalidate: formMCompanyManajemenPICAlamatChangeNotif.autoValidate,
                            validator: (e) {
                              if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setProvinsiMan(4)) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            controller: formMCompanyManajemenPICAlamatChangeNotif.controllerProvinsi,
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                                filled: true,
                                fillColor: Colors.black12,
                                labelText: 'Provinsi',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8))),
                            enabled: false,
                          ),
                        ),
                      ),
                      SizedBox(width: 8),
                      Expanded(
                        flex: 3,
                        child: Visibility(
                          visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKodePosShow(4),
                          child: TextFormField(
                            autovalidate: formMCompanyManajemenPICAlamatChangeNotif.autoValidate,
                            validator: (e) {
                              if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKodePosMan(4)) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            controller:
                            formMCompanyManajemenPICAlamatChangeNotif.controllerPostalCode,
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                                filled: true,
                                fillColor: Colors.black12,
                                labelText: 'Kode Pos',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8))),
                            enabled: false,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Visibility(
                    child: SizedBox(height: MediaQuery.of(context).size.height / 47),
                    visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setProvinsiShow(4)
                        && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setKodePosShow(4),
                  ),
                  Row(
                    children: [
                      Expanded(
                        flex: 4,
                        child: Visibility(
                          visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setTelp1AreaShow(4),
                          child: TextFormField(
                            autovalidate: formMCompanyManajemenPICAlamatChangeNotif.autoValidate,
                            validator: (e) {
                              if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setTelp1AreaMan(4)) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              WhitelistingTextInputFormatter.digitsOnly,
                              LengthLimitingTextInputFormatter(10),
                            ],
                            controller: formMCompanyManajemenPICAlamatChangeNotif.controllerTeleponArea,
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                                filled: true,
                                fillColor: Colors.black12,
                                labelText: 'Telepon (Area)',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8))),
                            enabled: false,
                          ),
                        ),
                      ),
                      SizedBox(width: MediaQuery.of(context).size.width / 37),
                      Expanded(
                        flex: 6,
                        child: Visibility(
                          visible: Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setTelp1Show(4),
                          child: TextFormField(
                            autovalidate: formMCompanyManajemenPICAlamatChangeNotif.autoValidate,
                            validator: (e) {
                              if (e.isEmpty && Provider.of<FormMAddAddressIndividuChangeNotifier>(context,listen: false).setTelp1Man(4)) {
                                return "Tidak boleh kosong";
                              } else {
                                return null;
                              }
                            },
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              WhitelistingTextInputFormatter.digitsOnly,
                              LengthLimitingTextInputFormatter(10),
                            ],
                            controller: formMCompanyManajemenPICAlamatChangeNotif.controllerTelepon,
                            style: new TextStyle(color: Colors.black),
                            decoration: new InputDecoration(
                                filled: true,
                                fillColor: Colors.black12,
                                labelText: 'Telepon',
                                labelStyle: TextStyle(color: Colors.black),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8))),
                            enabled: false,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            bottomNavigationBar: BottomAppBar(
              elevation: 0.0,
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Consumer<FormMCompanyManajemenPICAlamatChangeNotifier>(
                    builder: (context, formMCompanyManajemenPICAlamatChangeNotif, _) {
                      return Row(
                        children: [
                          Expanded(
                            child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: new BorderRadius.circular(8.0)),
                                color: myPrimaryColor,
                                onPressed: () {
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => ListManajemenPICAlamat()));
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text("EDIT ALAMAT",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500,
                                            letterSpacing: 1.25))
                                  ],
                                )
                            ),
                          ),
                          SizedBox(width: MediaQuery.of(context).size.width / 37),
                          Expanded(
                            child:  RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(8.0)),
                              color: myPrimaryColor,
                              onPressed: () {
                                formMCompanyManajemenPICAlamatChangeNotif.check(context);
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text("DONE",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500,
                                          letterSpacing: 1.25))
                                ],
                              )
                            )
                          )
                        ],
                      );
                    },
                  )),
            ),
          ),
        );
      },
    );
  }
}
