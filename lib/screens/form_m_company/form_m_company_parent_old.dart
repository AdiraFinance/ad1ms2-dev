import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/form_m_company/form_m_manajemen_pic.dart';
import 'package:ad1ms2_dev/screens/form_m_company/form_m_manajemen_pic_alamat.dart';
import 'package:ad1ms2_dev/screens/form_m_company/add_pemegang_saham_lembaga.dart';
import 'package:ad1ms2_dev/screens/form_m_company/form_m_pemegang_saham_kelembagaan_alamat.dart';
import 'package:ad1ms2_dev/screens/form_m_company/add_pemegang_saham_pribadi.dart';
import 'package:ad1ms2_dev/screens/form_m_company/form_m_pemegang_saham_pribadi_alamat.dart';
import 'package:ad1ms2_dev/screens/form_m_company/form_m_pendapatan.dart';
import 'package:ad1ms2_dev/screens/form_m_company/form_m_penjamin.dart';
import 'package:ad1ms2_dev/shared/document_unit_model.dart';
import 'package:ad1ms2_dev/shared/form_m_company_parent_change_notifier_old.dart';
import 'package:ad1ms2_dev/shared/group_unit_object_model.dart';
import 'package:ad1ms2_dev/shared/responsive_screen.dart';
import 'package:fa_stepper/fa_stepper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:provider/provider.dart';

import 'form_m_rincian.dart';
import 'form_m_alamat.dart';

class FormMCompanyParentOld extends StatefulWidget {
  @override
  _FormMCompanyParentOldState createState() => _FormMCompanyParentOldState();
}

class _FormMCompanyParentOldState extends State<FormMCompanyParentOld> {
  int _currentStep = 0;
  VoidCallback _onStepContinue, _onStepCancel;
  bool _validateFotoTempatTinggal = false;
  bool _validateFotoTempatUsaha = false;
  bool _validateGroupObjectUnit = false;
  bool _validateDocumentObjectUnit = false;
  String _occupationCodeSelected,
      _kegiatanUsahaSelected,
      _jenisKegiatanUsahaSelected,
      _jenisKonsepSelected;
  Screen _size;
  List<Map> _fileImageTmptTinggal = [];
  List<Map> _fileImageTmptUsaha = [];
  List<Map> _currentTotalWidget = [];
  List<GroupUnitObjectModel> _groupObjectUnit = [];
  List<DocumentUnitModel> _listDocumentUnit = [];

  @override
  Widget build(BuildContext context) {
    _size = Screen(MediaQuery.of(context).size);
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        accentColor: myPrimaryColor,
      ),
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text("Form M Company", style: TextStyle(color: Colors.black)),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: Consumer<FormMCompanyParentChangeNotifierOld>(
          builder: (context, formMCompanyParentChangeNotif, _) {
            return FAStepper(
                currentStep: formMCompanyParentChangeNotif.currentStep,
                type: FAStepperType.horizontal,
                stepNumberColor: myPrimaryColor,
                onStepTapped: (step) {
                  formMCompanyParentChangeNotif.onTapCheck(step, context);
                },
                controlsBuilder: (BuildContext context,
                    {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
                  formMCompanyParentChangeNotif.onStepContinue = onStepContinue;
                  formMCompanyParentChangeNotif.onStepCancel = onStepCancel;
                  return SizedBox(
                    width: 0.0,
                    height: 0.0,
                  );
                },
                onStepCancel: formMCompanyParentChangeNotif.currentStep > 0
                    ? () => setState(
                        () => formMCompanyParentChangeNotif.currentStep -= 1)
                    : null,
                onStepContinue: () {
                  setState(
                      () => formMCompanyParentChangeNotif.currentStep += 1);
                },
                steps: [
                  FAStep(
                    title: Container(
                        margin: EdgeInsets.only(top: 0.0),
                        child: formMCompanyParentChangeNotif.currentStep == 0
                            ? Text("Rincian",
                                style: TextStyle(color: Colors.black))
                            : Text("")),
                    content: Form(
                        key: formMCompanyParentChangeNotif.formKeys[0],
                        child: FormMCompanyRincianProvider()),
                    state: formMCompanyParentChangeNotif.flagStatusForm1
                        ? FAStepstate.complete
                        : FAStepstate.indexed,
                    isActive: true,
                  ),
                  FAStep(
                    title: Container(
                        margin: EdgeInsets.only(top: 0.0),
                        child: formMCompanyParentChangeNotif.currentStep == 1
                            ? Text("Alamat",
                                style: TextStyle(color: Colors.black))
                            : Text("")),
                    content: Form(
                        key: formMCompanyParentChangeNotif.formKeys[1],
                        child: FormMCompanyAlamat()),
                    state: formMCompanyParentChangeNotif.flagStatusForm2
                        ? FAStepstate.complete
                        : FAStepstate.indexed,
                    isActive: formMCompanyParentChangeNotif.enableTapForm2,
                  ),
                  FAStep(
                    title: Container(
                        margin: EdgeInsets.only(top: 0.0),
                        child: formMCompanyParentChangeNotif.currentStep == 2
                            ? Text("Pendapatan",
                                style: TextStyle(color: Colors.black))
                            : Text("")),
                    content: Form(
                        key: formMCompanyParentChangeNotif.formKeys[2],
                        child: FormMCompanyPendapatanProvider()),
                    state: formMCompanyParentChangeNotif.flagStatusForm3
                        ? FAStepstate.complete
                        : FAStepstate.indexed,
                    isActive: formMCompanyParentChangeNotif.enableTapForm3,
                  ),
                  FAStep(
                    title: Container(
                        margin: EdgeInsets.only(top: 0.0),
                        child: formMCompanyParentChangeNotif.currentStep == 3
                            ? Text("Penjamin",
                                style: TextStyle(color: Colors.black))
                            : Text("")),
                    content: Form(
                        key: formMCompanyParentChangeNotif.formKeys[3],
                        child: FormMCompanyPenjaminProvider()),
                    state: formMCompanyParentChangeNotif.flagStatusForm4
                        ? FAStepstate.complete
                        : FAStepstate.indexed,
                    isActive: formMCompanyParentChangeNotif.enableTapForm4,
                  ),
                  FAStep(
                    title: Container(
                        margin: EdgeInsets.only(top: 0.0),
                        child: formMCompanyParentChangeNotif.currentStep == 4
                            ? Text("Manajemen PIC",
                                style: TextStyle(color: Colors.black))
                            : Text("")),
                    content: Form(
                        key: formMCompanyParentChangeNotif.formKeys[4],
                        child: FormMCompanyManajemenPICProvider()),
                    state: formMCompanyParentChangeNotif.flagStatusForm5
                        ? FAStepstate.complete
                        : FAStepstate.indexed,
                    isActive: formMCompanyParentChangeNotif.enableTapForm5,
                  ),
                  FAStep(
                    title: Container(
                        margin: EdgeInsets.only(top: 0.0),
                        child: formMCompanyParentChangeNotif.currentStep == 5
                            ? Text("Manajemen PIC - Alamat",
                                style: TextStyle(color: Colors.black))
                            : Text("")),
                    content: Form(
                        key: formMCompanyParentChangeNotif.formKeys[5],
                        child: FormMCompanyManajemenPICAlamatProvider()),
                    state: formMCompanyParentChangeNotif.flagStatusForm6
                        ? FAStepstate.complete
                        : FAStepstate.indexed,
                    isActive: formMCompanyParentChangeNotif.enableTapForm6,
                  ),
                  FAStep(
                    title: Container(
                        margin: EdgeInsets.only(top: 0.0),
                        child: formMCompanyParentChangeNotif.currentStep == 6
                            ? Text("Pemegang Saham Pribadi",
                                style: TextStyle(color: Colors.black))
                            : Text("")),
                    content: Form(
                        key: formMCompanyParentChangeNotif.formKeys[6],
                        child: AddPemegangSahamPribadi()),
                    state: formMCompanyParentChangeNotif.flagStatusForm7
                        ? FAStepstate.complete
                        : FAStepstate.indexed,
                    isActive: formMCompanyParentChangeNotif.enableTapForm7,
                  ),
                  FAStep(
                    title: Container(
                        margin: EdgeInsets.only(top: 0.0),
                        child: formMCompanyParentChangeNotif.currentStep == 7
                            ? Text("Pemegang Saham Pribadi - Alamat",
                                style: TextStyle(color: Colors.black))
                            : Text("")),
                    content: Form(
                        key: formMCompanyParentChangeNotif.formKeys[7],
                        child:
                            FormMCompanyPemegangSahamPribadiAlamatProvider()),
                    state: formMCompanyParentChangeNotif.flagStatusForm8
                        ? FAStepstate.complete
                        : FAStepstate.indexed,
                    isActive: formMCompanyParentChangeNotif.enableTapForm8,
                  ),
                  FAStep(
                    title: Container(
                        margin: EdgeInsets.only(top: 0.0),
                        child: formMCompanyParentChangeNotif.currentStep == 8
                            ? Text("Pemegang Saham Kelembagaan",
                                style: TextStyle(color: Colors.black))
                            : Text("")),
                    content: Form(
                        key: formMCompanyParentChangeNotif.formKeys[8],
                        child: AddPemegangSahamLembaga()),
                    state: formMCompanyParentChangeNotif.flagStatusForm9
                        ? FAStepstate.complete
                        : FAStepstate.indexed,
                    isActive: formMCompanyParentChangeNotif.enableTapForm9,
                  ),
                  FAStep(
                    title: Container(
                        margin: EdgeInsets.only(top: 0.0),
                        child: formMCompanyParentChangeNotif.currentStep == 9
                            ? Text("Pemegang Saham Kelembagaan - Alamat",
                                style: TextStyle(color: Colors.black))
                            : Text("")),
                    content: Form(
                        key: formMCompanyParentChangeNotif.formKeys[9],
                        child:
                            FormMCompanyPemegangSahamKelembagaanAlamatProvider()),
                    state: formMCompanyParentChangeNotif.flagStatusForm10
                        ? FAStepstate.complete
                        : FAStepstate.indexed,
                    isActive: formMCompanyParentChangeNotif.enableTapForm10,
                  ),
                ]);
          },
        ),
        bottomNavigationBar: BottomAppBar(
          elevation: 0.0,
          child: Consumer<FormMCompanyParentChangeNotifierOld>(
            builder: (context, formMCompanyParentChangeNotif, _) {
              return Container(
                margin: EdgeInsets.symmetric(
                    horizontal: _size.wp(2), vertical: _size.hp(1.5)),
                child: formMCompanyParentChangeNotif.currentStep == 0
                    ? RaisedButton(
                        padding: EdgeInsets.only(
                            top: _size.hp(1.5), bottom: _size.hp(1.5)),
                        onPressed: () {
                          formMCompanyParentChangeNotif.check(context);
                        },
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(8.0)),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text('NEXT',
                                style: TextStyle(
                                    fontFamily: "NunitoSans",
                                    color: Colors.black,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500,
                                    letterSpacing: 1.25))
                          ],
                        ),
                        color: myPrimaryColor,
                      )
                    : Row(
                        children: <Widget>[
                          Expanded(
                            flex: 5,
                            child: RaisedButton(
                                padding: EdgeInsets.only(
                                    top: _size.hp(1.5), bottom: _size.hp(1.5)),
                                onPressed: () {
                                  formMCompanyParentChangeNotif.onStepCancel();
                                },
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(8.0)),
                                child: Text('BACK',
                                    style: TextStyle(
                                        fontFamily: "NunitoSans",
                                        color: Colors.black,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500,
                                        letterSpacing: 1.25))),
                          ),
                          SizedBox(width: _size.wp(1.5)),
                          Expanded(
                            flex: 5,
                            child: RaisedButton(
                              padding: EdgeInsets.only(
                                  top: _size.hp(1.5), bottom: _size.hp(1.5)),
                              onPressed: () {
                                formMCompanyParentChangeNotif.check(context);
                              },
                              shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(8.0)),
                              child: Text('NEXT',
                                  style: TextStyle(
                                      fontFamily: "NunitoSans",
                                      color: Colors.black,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                      letterSpacing: 1.25)),
                              color: myPrimaryColor,
                            ),
                          )
                        ],
                      ),
              );
            },
          ),
        ),
      ),
    );
  }
}
