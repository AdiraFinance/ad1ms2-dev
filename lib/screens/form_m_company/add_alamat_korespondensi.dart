import 'package:ad1ms2_dev/models/address_guarantor_model.dart';
import 'package:ad1ms2_dev/models/form_m_company_alamat_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_company_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_add_alamat_korespondensi_change_notif.dart';
import 'package:ad1ms2_dev/widgets/widget_address_company.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class CompanyAddAlamatKorespondensi extends StatefulWidget {
  final int flag;
  final int index;
  final AddressModelCompany addressModel;
  final int typeAddress;

  const CompanyAddAlamatKorespondensi(
      {this.flag, this.index, this.addressModel, this.typeAddress});
  @override
  _CompanyAddAlamatKorespondensiState createState() => _CompanyAddAlamatKorespondensiState();
}

class _CompanyAddAlamatKorespondensiState extends State<CompanyAddAlamatKorespondensi> {
  Future<void> _setValueForEdit;

  @override
  void initState() {
    super.initState();
    if (widget.flag != 0) {
      _setValueForEdit = Provider.of<FormMAddAddressCompanyChangeNotifier>(
          context,
          listen: false)
          .addDataListJenisAlamat(widget.addressModel, context, widget.flag,
          widget.addressModel.isSameWithIdentity,widget.index, widget.typeAddress);
    } else {
      Provider.of<FormMAddAddressCompanyChangeNotifier>(context,
          listen: false)
          .addDataListJenisAlamat(null,context,widget.flag,null,null, widget.typeAddress);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange),
      child:
      WidgetAddressCompany(flag: widget.flag, index: widget.index, editVal: _setValueForEdit,
        addressModel: widget.addressModel, typeAddress: widget.typeAddress,)

//      Scaffold(
//        appBar: AppBar(
//          title: Text(
//              widget.flag == 0
//                  ? "Add Alamat Korespondensi"
//                  : "Edit Alamat Korespondensi",
//              style: TextStyle(color: Colors.black)),
//          centerTitle: true,
//          backgroundColor: myPrimaryColor,
//          iconTheme: IconThemeData(color: Colors.black),
//        ),
//        body: SingleChildScrollView(
//            padding: EdgeInsets.symmetric(
//                horizontal: MediaQuery.of(context).size.width / 27,
//                vertical: MediaQuery.of(context).size.height / 57),
//            child: widget.flag == 0
//                ? Consumer<FormMCompanyAddAlamatKorespondensiChangeNotifier>(
//                    builder: (context, formMCompanyAddAlamatKorespon, _) {
//                      return Form(
//                        key: formMCompanyAddAlamatKorespon.key,
//                        onWillPop: _onWillPop,
//                        child: Column(
//                          crossAxisAlignment: CrossAxisAlignment.start,
//                          children: [
//                            DropdownButtonFormField<JenisAlamatModel>(
//                                autovalidate:
//                                    formMCompanyAddAlamatKorespon.autoValidate,
//                                validator: (e) {
//                                  if (e == null) {
//                                    return "Silahkan pilih jenis alamat";
//                                  } else {
//                                    return null;
//                                  }
//                                },
//                                value: formMCompanyAddAlamatKorespon.jenisAlamatSelected,
//                                onChanged: (value) {
//                                  formMCompanyAddAlamatKorespon.jenisAlamatSelected = value;
//                                },
//                                onTap: () {
//                                  FocusManager.instance.primaryFocus.unfocus();
//                                },
//                                decoration: InputDecoration(
//                                  labelText: "Jenis Alamat",
//                                  border: OutlineInputBorder(),
//                                  contentPadding:
//                                      EdgeInsets.symmetric(horizontal: 10),
//                                ),
//                                items: formMCompanyAddAlamatKorespon.listJenisAlamat
//                                    .map((value) {
//                                  return DropdownMenuItem<JenisAlamatModel>(
//                                    value: value,
//                                    child: Text(
//                                      value.DESKRIPSI,
//                                      overflow: TextOverflow.ellipsis,
//                                    ),
//                                  );
//                                }).toList()),
//                            formMCompanyAddAlamatKorespon.jenisAlamatSelected != null
//                            ? formMCompanyAddAlamatKorespon.jenisAlamatSelected.KODE == "02"
//                              ? Row(
//                                children: [
//                                  Checkbox(
//                                      value: formMCompanyAddAlamatKorespon
//                                          .isSameWithIdentity,
//                                      onChanged: (value) {
//                                        formMCompanyAddAlamatKorespon.isSameWithIdentity = value;
//                                        formMCompanyAddAlamatKorespon.setValueIsSameWithIdentity(value, context, null);
//                                      },
//                                      activeColor: myPrimaryColor),
//                                  SizedBox(width: 8),
//                                  Text("Sama dengan identitas")
//                                ],
//                              )
//                              : SizedBox(height: MediaQuery.of(context).size.height / 47)
//                            : SizedBox(height: MediaQuery.of(context).size.height / 47),
//                            TextFormField(
//                              autovalidate: formMCompanyAddAlamatKorespon.autoValidate,
//                              validator: (e) {
//                                if (e.isEmpty) {
//                                  return "Tidak boleh kosong";
//                                } else {
//                                  return null;
//                                }
//                              },
//                              controller: formMCompanyAddAlamatKorespon.controllerAlamat,
//                              enabled: formMCompanyAddAlamatKorespon.enableTfAddress,
//                              style: TextStyle(color: Colors.black),
//                              decoration: InputDecoration(
//                                labelText: 'Alamat',
//                                labelStyle: TextStyle(color: Colors.black),
//                                border: OutlineInputBorder(
//                                    borderRadius: BorderRadius.circular(8)),
//                                filled: !formMCompanyAddAlamatKorespon
//                                    .enableTfAddress,
//                                fillColor: !formMCompanyAddAlamatKorespon
//                                    .enableTfAddress
//                                    ? Colors.black12
//                                    : Colors.white,),
//                              keyboardType: TextInputType.text,
//                              textCapitalization: TextCapitalization.characters,
//                              maxLines: 3,
//                            ),
//                            SizedBox(height: MediaQuery.of(context).size.height / 47),
//                            Row(
//                              children: [
//                                Expanded(
//                                  flex: 5,
//                                  child: TextFormField(
//                                    autovalidate:
//                                    formMCompanyAddAlamatKorespon.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    inputFormatters: [
//                                      WhitelistingTextInputFormatter.digitsOnly,
//                                      // LengthLimitingTextInputFormatter(10),
//                                    ],
//                                    controller:
//                                    formMCompanyAddAlamatKorespon.controllerRT,
//                                    enabled: formMCompanyAddAlamatKorespon.enableTfRT,
//                                    style: TextStyle(color: Colors.black),
//                                    decoration: InputDecoration(
//                                      labelText: 'RT',
//                                      labelStyle:
//                                      TextStyle(color: Colors.black),
//                                      border: OutlineInputBorder(
//                                          borderRadius:
//                                          BorderRadius.circular(8)),
//                                      filled: !formMCompanyAddAlamatKorespon
//                                          .enableTfRT,
//                                      fillColor: !formMCompanyAddAlamatKorespon
//                                          .enableTfRT
//                                          ? Colors.black12
//                                          : Colors.white,),
//                                    keyboardType: TextInputType.number,
//                                  ),
//                                ),
//                                SizedBox(width: 8),
//                                Expanded(
//                                  flex: 5,
//                                  child: TextFormField(
//                                    autovalidate:
//                                    formMCompanyAddAlamatKorespon.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    inputFormatters: [
//                                      WhitelistingTextInputFormatter.digitsOnly,
//                                      // LengthLimitingTextInputFormatter(10),
//                                    ],
//                                    controller:
//                                    formMCompanyAddAlamatKorespon.controllerRW,
//                                    enabled: formMCompanyAddAlamatKorespon.enableTfRW,
//                                    style: TextStyle(color: Colors.black),
//                                    decoration: InputDecoration(
//                                      labelText: 'RW',
//                                      labelStyle:
//                                      TextStyle(color: Colors.black),
//                                      border: OutlineInputBorder(
//                                          borderRadius:
//                                          BorderRadius.circular(8)),
//                                      filled: !formMCompanyAddAlamatKorespon
//                                          .enableTfRW,
//                                      fillColor: !formMCompanyAddAlamatKorespon
//                                          .enableTfRW
//                                          ? Colors.black12
//                                          : Colors.white,),
//                                    keyboardType: TextInputType.number,
//                                  ),
//                                )
//                              ],
//                            ),
//                            SizedBox(height: MediaQuery.of(context).size.height / 47),
//                            FocusScope(
//                              node: FocusScopeNode(),
//                              child: TextFormField(
//                                autovalidate:
//                                formMCompanyAddAlamatKorespon.autoValidate,
//                                validator: (e) {
//                                  if (e.isEmpty) {
//                                    return "Tidak boleh kosong";
//                                  } else {
//                                    return null;
//                                  }
//                                },
//                                onTap: () {
//                                  FocusManager.instance.primaryFocus.unfocus();
//                                  formMCompanyAddAlamatKorespon
//                                      .searchKelurahan(context);
//                                },
//                                controller:
//                                formMCompanyAddAlamatKorespon.controllerKelurahan,
//                                enabled: formMCompanyAddAlamatKorespon.enableTfKelurahan,
//                                style: new TextStyle(color: Colors.black),
//                                decoration: new InputDecoration(
//                                  labelText: 'Kelurahan',
//                                  labelStyle: TextStyle(color: Colors.black),
//                                  border: OutlineInputBorder(
//                                      borderRadius:
//                                      BorderRadius.circular(8)),
//                                  filled: !formMCompanyAddAlamatKorespon
//                                      .enableTfKelurahan,
//                                  fillColor: !formMCompanyAddAlamatKorespon
//                                      .enableTfKelurahan
//                                      ? Colors.black12
//                                      : Colors.white,),
//                              ),
//                            ),
//                            SizedBox(height: MediaQuery.of(context).size.height / 47),
//                            TextFormField(
//                              autovalidate: formMCompanyAddAlamatKorespon.autoValidate,
//                              validator: (e) {
//                                if (e.isEmpty) {
//                                  return "Tidak boleh kosong";
//                                } else {
//                                  return null;
//                                }
//                              },
//                              controller: formMCompanyAddAlamatKorespon.controllerKecamatan,
//                              // enabled: formMCompanyAddAlamatKorespon.enableTfKecamatan,
//                              style: new TextStyle(color: Colors.black),
//                              decoration: new InputDecoration(
//                                  labelText: 'Kecamatan',
//                                  labelStyle: TextStyle(color: Colors.black),
//                                  border: OutlineInputBorder(
//                                      borderRadius: BorderRadius.circular(8)),
//                                filled: !formMCompanyAddAlamatKorespon
//                                    .enableTfKecamatan,
//                                fillColor: !formMCompanyAddAlamatKorespon
//                                    .enableTfKecamatan
//                                    ? Colors.black12
//                                    : Colors.white,),
//                              enabled: false,
//                            ),
//                            SizedBox(height: MediaQuery.of(context).size.height / 47),
//                            TextFormField(
//                              autovalidate: formMCompanyAddAlamatKorespon.autoValidate,
//                              validator: (e) {
//                                if (e.isEmpty) {
//                                  return "Tidak boleh kosong";
//                                } else {
//                                  return null;
//                                }
//                              },
//                              controller: formMCompanyAddAlamatKorespon.controllerKota,
//                              // enabled: formMCompanyAddAlamatKorespon.enableTfKota,
//                              style: new TextStyle(color: Colors.black),
//                              decoration: new InputDecoration(
//                                labelText: 'Kabupaten/Kota',
//                                labelStyle: TextStyle(color: Colors.black),
//                                border: OutlineInputBorder(
//                                    borderRadius: BorderRadius.circular(8)),
//                                filled: !formMCompanyAddAlamatKorespon
//                                    .enableTfKota,
//                                fillColor: !formMCompanyAddAlamatKorespon
//                                    .enableTfKota
//                                    ? Colors.black12
//                                    : Colors.white,),
//                              enabled: false,
//                            ),
//                            SizedBox(height: MediaQuery.of(context).size.height / 47),
//                            Row(
//                              children: [
//                                Expanded(
//                                  flex: 7,
//                                  child: TextFormField(
//                                    autovalidate:
//                                    formMCompanyAddAlamatKorespon.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    controller: formMCompanyAddAlamatKorespon
//                                        .controllerProvinsi,
//                                    // enabled: formMCompanyAddAlamatKorespon.enableTfProv,
//                                    style: new TextStyle(color: Colors.black),
//                                    decoration: new InputDecoration(
//                                      labelText: 'Provinsi',
//                                      labelStyle:
//                                      TextStyle(color: Colors.black),
//                                      border: OutlineInputBorder(
//                                          borderRadius:
//                                          BorderRadius.circular(8)),
//                                      filled: !formMCompanyAddAlamatKorespon
//                                          .enableTfProv,
//                                      fillColor: !formMCompanyAddAlamatKorespon
//                                          .enableTfProv
//                                          ? Colors.black12
//                                          : Colors.white,),
//                                    enabled: false,
//                                  ),
//                                ),
//                                SizedBox(width: 8),
//                                Expanded(
//                                  flex: 3,
//                                  child: TextFormField(
//                                    autovalidate:
//                                    formMCompanyAddAlamatKorespon.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    inputFormatters: [
//                                      WhitelistingTextInputFormatter.digitsOnly,
//                                      // LengthLimitingTextInputFormatter(10),
//                                    ],
//                                    controller: formMCompanyAddAlamatKorespon
//                                        .controllerPostalCode,
//                                    // enabled: formMCompanyAddAlamatKorespon.enableTfPostalCode,
//                                    style: new TextStyle(color: Colors.black),
//                                    decoration: new InputDecoration(
//                                      labelText: 'Kode Pos',
//                                      labelStyle:
//                                      TextStyle(color: Colors.black),
//                                      border: OutlineInputBorder(
//                                          borderRadius:
//                                          BorderRadius.circular(8)),
//                                      filled: !formMCompanyAddAlamatKorespon
//                                          .enableTfPostalCode,
//                                      fillColor: !formMCompanyAddAlamatKorespon
//                                          .enableTfPostalCode
//                                          ? Colors.black12
//                                          : Colors.white,),
//                                    enabled: false,
//                                  ),
//                                ),
//                              ],
//                            ),
//                            SizedBox(height: MediaQuery.of(context).size.height / 47),
//                            Row(
//                              children: [
//                                Expanded(
//                                  flex: 4,
//                                  child: TextFormField(
//                                    autovalidate:
//                                    formMCompanyAddAlamatKorespon.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    onChanged: (e) {
//                                      formMCompanyAddAlamatKorespon
//                                          .checkValidCodeArea1(e);
//                                    },
//                                    keyboardType: TextInputType.number,
//                                    inputFormatters: [
//                                      WhitelistingTextInputFormatter.digitsOnly,
//                                      // LengthLimitingTextInputFormatter(10),
//                                    ],
//                                    controller: formMCompanyAddAlamatKorespon
//                                        .controllerTelepon1Area,
//                                    enabled: formMCompanyAddAlamatKorespon.enableTfAreaCode1,
//                                    style: new TextStyle(color: Colors.black),
//                                    decoration: new InputDecoration(
//                                      labelText: 'Telepon 1 (Area)',
//                                      labelStyle:
//                                      TextStyle(color: Colors.black),
//                                      border: OutlineInputBorder(
//                                          borderRadius:
//                                          BorderRadius.circular(8)),
//                                      filled: !formMCompanyAddAlamatKorespon
//                                          .enableTfAreaCode1,
//                                      fillColor: !formMCompanyAddAlamatKorespon
//                                          .enableTfAreaCode1
//                                          ? Colors.black12
//                                          : Colors.white,),
//                                  ),
//                                ),
//                                SizedBox(width: MediaQuery.of(context).size.width / 37),
//                                Expanded(
//                                  flex: 6,
//                                  child: TextFormField(
//                                    autovalidate:
//                                    formMCompanyAddAlamatKorespon.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    keyboardType: TextInputType.number,
//                                    inputFormatters: [
//                                      WhitelistingTextInputFormatter.digitsOnly,
//                                      // LengthLimitingTextInputFormatter(10),
//                                    ],
//                                    controller:
//                                    formMCompanyAddAlamatKorespon.controllerTelepon1,
//                                    enabled: formMCompanyAddAlamatKorespon.enableTfPhone1,
//                                    style: new TextStyle(color: Colors.black),
//                                    decoration: new InputDecoration(
//                                      labelText: 'Telepon 1',
//                                      labelStyle:
//                                      TextStyle(color: Colors.black),
//                                      border: OutlineInputBorder(
//                                          borderRadius:
//                                          BorderRadius.circular(8)),
//                                      filled: !formMCompanyAddAlamatKorespon
//                                          .enableTfPhone1,
//                                      fillColor: !formMCompanyAddAlamatKorespon
//                                          .enableTfPhone1
//                                          ? Colors.black12
//                                          : Colors.white,),
//                                  ),
//                                ),
//                              ],
//                            ),
//                            SizedBox(height: MediaQuery.of(context).size.height / 47),
//                            Row(
//                              children: [
//                                Expanded(
//                                  flex: 4,
//                                  child: TextFormField(
//                                    autovalidate:
//                                    formMCompanyAddAlamatKorespon.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    onChanged: (e) {
//                                      formMCompanyAddAlamatKorespon.checkValidCodeArea2(e);
//                                    },
//                                    keyboardType: TextInputType.number,
//                                    inputFormatters: [
//                                      WhitelistingTextInputFormatter.digitsOnly,
//                                      // LengthLimitingTextInputFormatter(10),
//                                    ],
//                                    controller: formMCompanyAddAlamatKorespon.controllerTelepon2Area,
//                                    enabled: formMCompanyAddAlamatKorespon.enableTfAreaCode2,
//                                    style: new TextStyle(color: Colors.black),
//                                    decoration: new InputDecoration(
//                                      labelText: 'Telepon 2 (Area)',
//                                      labelStyle:
//                                      TextStyle(color: Colors.black),
//                                      border: OutlineInputBorder(
//                                          borderRadius:
//                                          BorderRadius.circular(8)),
//                                      filled: !formMCompanyAddAlamatKorespon
//                                          .enableTfAreaCode2,
//                                      fillColor: !formMCompanyAddAlamatKorespon
//                                          .enableTfAreaCode2
//                                          ? Colors.black12
//                                          : Colors.white,),
//                                  ),
//                                ),
//                                SizedBox(width: MediaQuery.of(context).size.width / 37),
//                                Expanded(
//                                  flex: 6,
//                                  child: TextFormField(
//                                    autovalidate:
//                                    formMCompanyAddAlamatKorespon.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    inputFormatters: [
//                                      WhitelistingTextInputFormatter.digitsOnly,
//                                      // LengthLimitingTextInputFormatter(10),
//                                    ],
//                                    controller:
//                                    formMCompanyAddAlamatKorespon.controllerTelepon2,
//                                    enabled: formMCompanyAddAlamatKorespon.enableTfPhone2,
//                                    style: new TextStyle(color: Colors.black),
//                                    decoration: new InputDecoration(
//                                      labelText: 'Telepon 2',
//                                      labelStyle:
//                                      TextStyle(color: Colors.black),
//                                      border: OutlineInputBorder(
//                                          borderRadius:
//                                          BorderRadius.circular(8)),
//                                      filled: !formMCompanyAddAlamatKorespon
//                                          .enableTfPhone2,
//                                      fillColor: !formMCompanyAddAlamatKorespon
//                                          .enableTfPhone2
//                                          ? Colors.black12
//                                          : Colors.white,),
//                                    keyboardType: TextInputType.number,
//                                  ),
//                                ),
//                              ],
//                            ),
//                            SizedBox(height: MediaQuery.of(context).size.height / 47),
//                            Row(
//                              children: [
//                                Expanded(
//                                  flex: 4,
//                                  child: TextFormField(
//                                    autovalidate:
//                                    formMCompanyAddAlamatKorespon.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    onChanged: (e) {
//                                      formMCompanyAddAlamatKorespon
//                                          .checkValidCodeAreaFax(e);
//                                    },
//                                    keyboardType: TextInputType.number,
//                                    inputFormatters: [
//                                      WhitelistingTextInputFormatter.digitsOnly,
//                                      // LengthLimitingTextInputFormatter(10),
//                                    ],
//                                    controller: formMCompanyAddAlamatKorespon.controllerFaxArea,
//                                    enabled: formMCompanyAddAlamatKorespon.enableTfAreaCodeFax,
//                                    style: new TextStyle(color: Colors.black),
//                                    decoration: new InputDecoration(
//                                      labelText: 'Fax (Area) ',
//                                      labelStyle:
//                                      TextStyle(color: Colors.black),
//                                      border: OutlineInputBorder(
//                                          borderRadius:
//                                          BorderRadius.circular(8)),
//                                      filled: !formMCompanyAddAlamatKorespon
//                                          .enableTfAreaCodeFax,
//                                      fillColor: !formMCompanyAddAlamatKorespon
//                                          .enableTfAreaCodeFax
//                                          ? Colors.black12
//                                          : Colors.white,),
//                                  ),
//                                ),
//                                SizedBox(width: MediaQuery.of(context).size.width / 37),
//                                Expanded(
//                                  flex: 6,
//                                  child: TextFormField(
//                                    autovalidate:
//                                    formMCompanyAddAlamatKorespon.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    inputFormatters: [
//                                      WhitelistingTextInputFormatter.digitsOnly,
//                                      // LengthLimitingTextInputFormatter(10),
//                                    ],
//                                    controller:
//                                    formMCompanyAddAlamatKorespon.controllerFax,
//                                    enabled: formMCompanyAddAlamatKorespon.enableTfFax,
//                                    style: new TextStyle(color: Colors.black),
//                                    decoration: new InputDecoration(
//                                      labelText: 'Fax',
//                                      labelStyle:
//                                      TextStyle(color: Colors.black),
//                                      border: OutlineInputBorder(
//                                          borderRadius:
//                                          BorderRadius.circular(8)),
//                                      filled: !formMCompanyAddAlamatKorespon
//                                          .enableTfFax,
//                                      fillColor: !formMCompanyAddAlamatKorespon
//                                          .enableTfFax
//                                          ? Colors.black12
//                                          : Colors.white,),
//                                    keyboardType: TextInputType.number,
//                                  ),
//                                ),
//                              ],
//                            ),
//                          ],
//                        ),
//                      );
//                    },
//                  )
//                : FutureBuilder(
//                    future: _setValueForEdit,
//                    builder: (context, snapshot) {
//                      if (snapshot.connectionState == ConnectionState.waiting) {
//                        return Center(
//                          child: CircularProgressIndicator(),
//                        );
//                      }
//                      return Consumer<FormMCompanyAddAlamatKorespondensiChangeNotifier>(
//                        builder: (context, formMCompanyAddAlamatKorespon, _) {
//                          return Form(
//                            key: formMCompanyAddAlamatKorespon.key,
//                            onWillPop: _onWillPop,
//                            child: Column(
//                              crossAxisAlignment: CrossAxisAlignment.start,
//                              children: [
//                                DropdownButtonFormField<JenisAlamatModel>(
//                                    autovalidate:
//                                    formMCompanyAddAlamatKorespon.autoValidate,
//                                    validator: (e) {
//                                      if (e == null) {
//                                        return "Silahkan pilih jenis alamat";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    value: formMCompanyAddAlamatKorespon.jenisAlamatSelected,
//                                    onChanged: (value) {
//                                      formMCompanyAddAlamatKorespon.jenisAlamatSelected = value;
//                                    },
//                                    onTap: () {
//                                      FocusManager.instance.primaryFocus.unfocus();
//                                    },
//                                    decoration: InputDecoration(
//                                      labelText: "Jenis Alamat",
//                                      border: OutlineInputBorder(),
//                                      contentPadding:
//                                          EdgeInsets.symmetric(horizontal: 10),
//                                    ),
//                                    items: formMCompanyAddAlamatKorespon
//                                        .listJenisAlamat
//                                        .map((value) {
//                                      return DropdownMenuItem<JenisAlamatModel>(
//                                        value: value,
//                                        child: Text(
//                                          value.DESKRIPSI,
//                                          overflow: TextOverflow.ellipsis,
//                                        ),
//                                      );
//                                    }).toList()),
//                                formMCompanyAddAlamatKorespon.jenisAlamatSelected != null
//                                  ? formMCompanyAddAlamatKorespon.jenisAlamatSelected.KODE == "02"
//                                    ? Row(
//                                      children: [
//                                        Checkbox(
//                                            value: formMCompanyAddAlamatKorespon.isSameWithIdentity,
//                                            onChanged: (value) {
//                                              formMCompanyAddAlamatKorespon.isSameWithIdentity = value;
//                                              formMCompanyAddAlamatKorespon.setValueIsSameWithIdentity(value, context, null);
//                                            },
//                                            activeColor: myPrimaryColor),
//                                        SizedBox(width: 8),
//                                        Text("Sama dengan identitas")
//                                      ],
//                                    )
//                                  : SizedBox(height: MediaQuery.of(context).size.height / 47)
//                                : SizedBox(height: MediaQuery.of(context).size.height / 47),
//                                TextFormField(
//                                  autovalidate: formMCompanyAddAlamatKorespon.autoValidate,
//                                  validator: (e) {
//                                    if (e.isEmpty) {
//                                      return "Tidak boleh kosong";
//                                    } else {
//                                      return null;
//                                    }
//                                  },
//                                  controller: formMCompanyAddAlamatKorespon.controllerAlamat,
//                                  enabled: formMCompanyAddAlamatKorespon.enableTfAddress,
//                                  style: TextStyle(color: Colors.black),
//                                  decoration: InputDecoration(
//                                    labelText: 'Alamat',
//                                    labelStyle: TextStyle(color: Colors.black),
//                                    border: OutlineInputBorder(
//                                        borderRadius: BorderRadius.circular(8)),
//                                    filled: !formMCompanyAddAlamatKorespon
//                                        .enableTfAddress,
//                                    fillColor: !formMCompanyAddAlamatKorespon
//                                        .enableTfAddress
//                                        ? Colors.black12
//                                        : Colors.white,),
//                                  keyboardType: TextInputType.text,
//                                  textCapitalization: TextCapitalization.characters,
//                                  maxLines: 3,
//                                ),
//                                SizedBox(height: MediaQuery.of(context).size.height / 47),
//                                Row(
//                                  children: [
//                                    Expanded(
//                                      flex: 5,
//                                      child: TextFormField(
//                                        autovalidate:
//                                        formMCompanyAddAlamatKorespon.autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        inputFormatters: [
//                                          WhitelistingTextInputFormatter.digitsOnly,
//                                          // LengthLimitingTextInputFormatter(10),
//                                        ],
//                                        controller:
//                                        formMCompanyAddAlamatKorespon.controllerRT,
//                                        enabled: formMCompanyAddAlamatKorespon.enableTfRT,
//                                        style: TextStyle(color: Colors.black),
//                                        decoration: InputDecoration(
//                                          labelText: 'RT',
//                                          labelStyle:
//                                          TextStyle(color: Colors.black),
//                                          border: OutlineInputBorder(
//                                              borderRadius:
//                                              BorderRadius.circular(8)),
//                                          filled: !formMCompanyAddAlamatKorespon
//                                              .enableTfRT,
//                                          fillColor: !formMCompanyAddAlamatKorespon
//                                              .enableTfRT
//                                              ? Colors.black12
//                                              : Colors.white,),
//                                        keyboardType: TextInputType.number,
//                                      ),
//                                    ),
//                                    SizedBox(width: 8),
//                                    Expanded(
//                                      flex: 5,
//                                      child: TextFormField(
//                                        autovalidate:
//                                        formMCompanyAddAlamatKorespon.autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        inputFormatters: [
//                                          WhitelistingTextInputFormatter.digitsOnly,
//                                          // LengthLimitingTextInputFormatter(10),
//                                        ],
//                                        controller:
//                                        formMCompanyAddAlamatKorespon.controllerRW,
//                                        enabled: formMCompanyAddAlamatKorespon.enableTfRW,
//                                        style: TextStyle(color: Colors.black),
//                                        decoration: InputDecoration(
//                                          labelText: 'RW',
//                                          labelStyle:
//                                          TextStyle(color: Colors.black),
//                                          border: OutlineInputBorder(
//                                              borderRadius:
//                                              BorderRadius.circular(8)),
//                                          filled: !formMCompanyAddAlamatKorespon
//                                              .enableTfRW,
//                                          fillColor: !formMCompanyAddAlamatKorespon
//                                              .enableTfRW
//                                              ? Colors.black12
//                                              : Colors.white,),
//                                        keyboardType: TextInputType.number,
//                                      ),
//                                    )
//                                  ],
//                                ),
//                                SizedBox(height: MediaQuery.of(context).size.height / 47),
//                                FocusScope(
//                                  node: FocusScopeNode(),
//                                  child: TextFormField(
//                                    autovalidate:
//                                    formMCompanyAddAlamatKorespon.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    onTap: () {
//                                      FocusManager.instance.primaryFocus.unfocus();
//                                      formMCompanyAddAlamatKorespon
//                                          .searchKelurahan(context);
//                                    },
//                                    controller:
//                                    formMCompanyAddAlamatKorespon.controllerKelurahan,
//                                    enabled: formMCompanyAddAlamatKorespon.enableTfKelurahan,
//                                    style: new TextStyle(color: Colors.black),
//                                    decoration: new InputDecoration(
//                                      labelText: 'Kelurahan',
//                                      labelStyle: TextStyle(color: Colors.black),
//                                      border: OutlineInputBorder(
//                                          borderRadius:
//                                          BorderRadius.circular(8)),
//                                      filled: !formMCompanyAddAlamatKorespon
//                                          .enableTfKelurahan,
//                                      fillColor: !formMCompanyAddAlamatKorespon
//                                          .enableTfKelurahan
//                                          ? Colors.black12
//                                          : Colors.white,),
//                                    keyboardType: TextInputType.text,
//                                    textCapitalization: TextCapitalization.characters,
//                                  ),
//                                ),
//                                SizedBox(height: MediaQuery.of(context).size.height / 47),
//                                TextFormField(
//                                  autovalidate: formMCompanyAddAlamatKorespon.autoValidate,
//                                  validator: (e) {
//                                    if (e.isEmpty) {
//                                      return "Tidak boleh kosong";
//                                    } else {
//                                      return null;
//                                    }
//                                  },
//                                  controller:
//                                  formMCompanyAddAlamatKorespon.controllerKecamatan,
//                                  // enabled: formMCompanyAddAlamatKorespon.enableTfKecamatan,
//                                  style: new TextStyle(color: Colors.black),
//                                  decoration: new InputDecoration(
//                                    labelText: 'Kecamatan',
//                                    labelStyle: TextStyle(color: Colors.black),
//                                    border: OutlineInputBorder(
//                                        borderRadius: BorderRadius.circular(8)),
//                                    filled: !formMCompanyAddAlamatKorespon
//                                        .enableTfKecamatan,
//                                    fillColor: !formMCompanyAddAlamatKorespon
//                                        .enableTfKecamatan
//                                        ? Colors.black12
//                                        : Colors.white,),
//                                  enabled: false,
//                                ),
//                                SizedBox(height: MediaQuery.of(context).size.height / 47),
//                                TextFormField(
//                                  autovalidate: formMCompanyAddAlamatKorespon.autoValidate,
//                                  validator: (e) {
//                                    if (e.isEmpty) {
//                                      return "Tidak boleh kosong";
//                                    } else {
//                                      return null;
//                                    }
//                                  },
//                                  controller: formMCompanyAddAlamatKorespon.controllerKota,
//                                  // enabled: formMCompanyAddAlamatKorespon.enableTfKota,
//                                  style: new TextStyle(color: Colors.black),
//                                  decoration: new InputDecoration(
//                                    labelText: 'Kabupaten/Kota',
//                                    labelStyle: TextStyle(color: Colors.black),
//                                    border: OutlineInputBorder(
//                                        borderRadius: BorderRadius.circular(8)),
//                                    filled: !formMCompanyAddAlamatKorespon
//                                        .enableTfKota,
//                                    fillColor: !formMCompanyAddAlamatKorespon
//                                        .enableTfKota
//                                        ? Colors.black12
//                                        : Colors.white,),
//                                  enabled: false,
//                                ),
//                                SizedBox(height: MediaQuery.of(context).size.height / 47),
//                                Row(
//                                  children: [
//                                    Expanded(
//                                      flex: 7,
//                                      child: TextFormField(
//                                        autovalidate:
//                                        formMCompanyAddAlamatKorespon.autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        controller: formMCompanyAddAlamatKorespon
//                                            .controllerProvinsi,
//                                        // enabled: formMCompanyAddAlamatKorespon.enableTfProv,
//                                        style: new TextStyle(color: Colors.black),
//                                        decoration: new InputDecoration(
//                                          labelText: 'Provinsi',
//                                          labelStyle:
//                                          TextStyle(color: Colors.black),
//                                          border: OutlineInputBorder(
//                                              borderRadius:
//                                              BorderRadius.circular(8)),
//                                          filled: !formMCompanyAddAlamatKorespon
//                                              .enableTfProv,
//                                          fillColor: !formMCompanyAddAlamatKorespon
//                                              .enableTfProv
//                                              ? Colors.black12
//                                              : Colors.white,),
//                                        enabled: false,
//                                      ),
//                                    ),
//                                    SizedBox(width: 8),
//                                    Expanded(
//                                      flex: 3,
//                                      child: TextFormField(
//                                        autovalidate:
//                                        formMCompanyAddAlamatKorespon.autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        controller: formMCompanyAddAlamatKorespon
//                                            .controllerPostalCode,
//                                        // enabled: formMCompanyAddAlamatKorespon.enableTfPostalCode,
//                                        style: new TextStyle(color: Colors.black),
//                                        decoration: new InputDecoration(
//                                          labelText: 'Kode Pos',
//                                          labelStyle:
//                                          TextStyle(color: Colors.black),
//                                          border: OutlineInputBorder(
//                                              borderRadius:
//                                              BorderRadius.circular(8)),
//                                          filled: !formMCompanyAddAlamatKorespon
//                                              .enableTfPostalCode,
//                                          fillColor: !formMCompanyAddAlamatKorespon
//                                              .enableTfPostalCode
//                                              ? Colors.black12
//                                              : Colors.white,),
//                                        enabled: false,
//                                      ),
//                                    ),
//                                  ],
//                                ),
//                                SizedBox(height: MediaQuery.of(context).size.height / 47),
//                                Row(
//                                  children: [
//                                    Expanded(
//                                      flex: 4,
//                                      child: TextFormField(
//                                        autovalidate:
//                                        formMCompanyAddAlamatKorespon.autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        onChanged: (e) {
//                                          formMCompanyAddAlamatKorespon
//                                              .checkValidCodeArea1(e);
//                                        },
//                                        keyboardType: TextInputType.number,
//                                        inputFormatters: [
//                                          WhitelistingTextInputFormatter.digitsOnly,
//                                          // LengthLimitingTextInputFormatter(10),
//                                        ],
//                                        controller: formMCompanyAddAlamatKorespon
//                                            .controllerTelepon1Area,
//                                        enabled: formMCompanyAddAlamatKorespon.enableTfAreaCode1,
//                                        style: new TextStyle(color: Colors.black),
//                                        decoration: new InputDecoration(
//                                          labelText: 'Telepon 1 (Area)',
//                                          labelStyle:
//                                          TextStyle(color: Colors.black),
//                                          border: OutlineInputBorder(
//                                              borderRadius:
//                                              BorderRadius.circular(8)),
//                                          filled: !formMCompanyAddAlamatKorespon
//                                              .enableTfAreaCode1,
//                                          fillColor: !formMCompanyAddAlamatKorespon
//                                              .enableTfAreaCode1
//                                              ? Colors.black12
//                                              : Colors.white,),
//                                      ),
//                                    ),
//                                    SizedBox(width: MediaQuery.of(context).size.width / 37),
//                                    Expanded(
//                                      flex: 6,
//                                      child: TextFormField(
//                                        autovalidate:
//                                        formMCompanyAddAlamatKorespon.autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        keyboardType: TextInputType.number,
//                                        inputFormatters: [
//                                          WhitelistingTextInputFormatter.digitsOnly,
//                                          // LengthLimitingTextInputFormatter(10),
//                                        ],
//                                        controller:
//                                        formMCompanyAddAlamatKorespon.controllerTelepon1,
//                                        enabled: formMCompanyAddAlamatKorespon.enableTfPhone1,
//                                        style: new TextStyle(color: Colors.black),
//                                        decoration: new InputDecoration(
//                                          labelText: 'Telepon 1',
//                                          labelStyle:
//                                          TextStyle(color: Colors.black),
//                                          border: OutlineInputBorder(
//                                              borderRadius:
//                                              BorderRadius.circular(8)),
//                                          filled: !formMCompanyAddAlamatKorespon
//                                              .enableTfPhone1,
//                                          fillColor: !formMCompanyAddAlamatKorespon
//                                              .enableTfPhone1
//                                              ? Colors.black12
//                                              : Colors.white,),
//                                      ),
//                                    ),
//                                  ],
//                                ),
//                                SizedBox(height: MediaQuery.of(context).size.height / 47),
//                                Row(
//                                  children: [
//                                    Expanded(
//                                      flex: 4,
//                                      child: TextFormField(
//                                        autovalidate:
//                                        formMCompanyAddAlamatKorespon.autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        onChanged: (e) {
//                                          formMCompanyAddAlamatKorespon.checkValidCodeArea2(e);
//                                        },
//                                        keyboardType: TextInputType.number,
//                                        inputFormatters: [
//                                          WhitelistingTextInputFormatter.digitsOnly,
//                                          // LengthLimitingTextInputFormatter(10),
//                                        ],
//                                        controller: formMCompanyAddAlamatKorespon.controllerTelepon2Area,
//                                        enabled: formMCompanyAddAlamatKorespon.enableTfAreaCode2,
//                                        style: new TextStyle(color: Colors.black),
//                                        decoration: new InputDecoration(
//                                          labelText: 'Telepon 2 (Area)',
//                                          labelStyle:
//                                          TextStyle(color: Colors.black),
//                                          border: OutlineInputBorder(
//                                              borderRadius:
//                                              BorderRadius.circular(8)),
//                                          filled: !formMCompanyAddAlamatKorespon
//                                              .enableTfAreaCode2,
//                                          fillColor: !formMCompanyAddAlamatKorespon
//                                              .enableTfAreaCode2
//                                              ? Colors.black12
//                                              : Colors.white,),
//                                      ),
//                                    ),
//                                    SizedBox(width: MediaQuery.of(context).size.width / 37),
//                                    Expanded(
//                                      flex: 6,
//                                      child: TextFormField(
//                                        autovalidate:
//                                        formMCompanyAddAlamatKorespon.autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        inputFormatters: [
//                                          WhitelistingTextInputFormatter.digitsOnly,
//                                          // LengthLimitingTextInputFormatter(10),
//                                        ],
//                                        controller:
//                                        formMCompanyAddAlamatKorespon.controllerTelepon2,
//                                        enabled: formMCompanyAddAlamatKorespon.enableTfPhone2,
//                                        style: new TextStyle(color: Colors.black),
//                                        decoration: new InputDecoration(
//                                          labelText: 'Telepon 2',
//                                          labelStyle:
//                                          TextStyle(color: Colors.black),
//                                          border: OutlineInputBorder(
//                                              borderRadius:
//                                              BorderRadius.circular(8)),
//                                          filled: !formMCompanyAddAlamatKorespon
//                                              .enableTfPhone2,
//                                          fillColor: !formMCompanyAddAlamatKorespon
//                                              .enableTfPhone2
//                                              ? Colors.black12
//                                              : Colors.white,),
//                                        keyboardType: TextInputType.number,
//                                      ),
//                                    ),
//                                  ],
//                                ),
//                                SizedBox(height: MediaQuery.of(context).size.height / 47),
//                                Row(
//                                  children: [
//                                    Expanded(
//                                      flex: 4,
//                                      child: TextFormField(
//                                        autovalidate:
//                                        formMCompanyAddAlamatKorespon.autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        onChanged: (e) {
//                                          formMCompanyAddAlamatKorespon
//                                              .checkValidCodeAreaFax(e);
//                                        },
//                                        keyboardType: TextInputType.number,
//                                        inputFormatters: [
//                                          WhitelistingTextInputFormatter.digitsOnly,
//                                          // LengthLimitingTextInputFormatter(10),
//                                        ],
//                                        controller: formMCompanyAddAlamatKorespon.controllerFaxArea,
//                                        enabled: formMCompanyAddAlamatKorespon.enableTfAreaCodeFax,
//                                        style: new TextStyle(color: Colors.black),
//                                        decoration: new InputDecoration(
//                                          labelText: 'Fax (Area) ',
//                                          labelStyle:
//                                          TextStyle(color: Colors.black),
//                                          border: OutlineInputBorder(
//                                              borderRadius:
//                                              BorderRadius.circular(8)),
//                                          filled: !formMCompanyAddAlamatKorespon
//                                              .enableTfAreaCodeFax,
//                                          fillColor: !formMCompanyAddAlamatKorespon
//                                              .enableTfAreaCodeFax
//                                              ? Colors.black12
//                                              : Colors.white,),
//                                      ),
//                                    ),
//                                    SizedBox(width: MediaQuery.of(context).size.width / 37),
//                                    Expanded(
//                                      flex: 6,
//                                      child: TextFormField(
//                                        autovalidate:
//                                        formMCompanyAddAlamatKorespon.autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        inputFormatters: [
//                                          WhitelistingTextInputFormatter.digitsOnly,
//                                          // LengthLimitingTextInputFormatter(10),
//                                        ],
//                                        controller:
//                                        formMCompanyAddAlamatKorespon.controllerFax,
//                                        enabled: formMCompanyAddAlamatKorespon.enableTfFax,
//                                        style: new TextStyle(color: Colors.black),
//                                        decoration: new InputDecoration(
//                                          labelText: 'Fax',
//                                          labelStyle:
//                                          TextStyle(color: Colors.black),
//                                          border: OutlineInputBorder(
//                                              borderRadius:
//                                              BorderRadius.circular(8)),
//                                          filled: !formMCompanyAddAlamatKorespon
//                                              .enableTfFax,
//                                          fillColor: !formMCompanyAddAlamatKorespon
//                                              .enableTfFax
//                                              ? Colors.black12
//                                              : Colors.white,),
//                                        keyboardType: TextInputType.number,
//                                      ),
//                                    ),
//                                  ],
//                                ),
//                              ],
//                            ),
//                          );
//                        },
//                      );
//                    })),
//        bottomNavigationBar: BottomAppBar(
//          elevation: 0.0,
//          child: Padding(
//              padding: const EdgeInsets.all(8.0),
//              child: RaisedButton(
//                  shape: RoundedRectangleBorder(
//                      borderRadius: new BorderRadius.circular(8.0)),
//                  color: myPrimaryColor,
//                  onPressed: () {
//                    if (widget.flag == 0) {
//                      Provider.of<FormMCompanyAddAlamatKorespondensiChangeNotifier>(
//                              context,
//                              listen: false)
//                          .check(context, widget.flag, null);
//                    } else {
//                      Provider.of<FormMCompanyAddAlamatKorespondensiChangeNotifier>(
//                              context,
//                              listen: false)
//                          .check(context, widget.flag, widget.index);
//                    }
//                  },
//                  child: Row(
//                    mainAxisAlignment: MainAxisAlignment.center,
//                    children: <Widget>[
//                      Text(widget.flag == 0 ? "SAVE" : "UPDATE",
//                          style: TextStyle(
//                              color: Colors.black,
//                              fontSize: 14,
//                              fontWeight: FontWeight.w500,
//                              letterSpacing: 1.25))
//                    ],
//                  ))),
//        ),
//      ),
    );
  }

//  Future<bool> _onWillPop() async {
//    var _provider = Provider.of<FormMCompanyAddAlamatKorespondensiChangeNotifier>(context, listen: false);
//    if (widget.flag == 0) {
//      if (_provider.jenisAlamatSelected != null ||
//          _provider.controllerAlamat.text != "" ||
//          _provider.controllerRT.text != "" ||
//          _provider.controllerRW.text != "" ||
//          _provider.controllerKelurahan.text != "" ||
//          _provider.controllerKecamatan.text != "" ||
//          _provider.controllerKota.text != "" ||
//          _provider.controllerProvinsi.text != "" ||
//          _provider.controllerPostalCode.text != "" ||
//          _provider.controllerTelepon1Area.text != "" ||
//          _provider.controllerTelepon1.text != "" ||
//          _provider.controllerTelepon2Area.text != "" ||
//          _provider.controllerTelepon2.text != "" ||
//          _provider.controllerFaxArea.text != "" ||
//          _provider.controllerFax.text != "") {
//          return (await showDialog(
//              context: context,
//              builder: (myContext) => AlertDialog(
//                title: new Text('Warning'),
//                content: new Text('Simpan perubahan?'),
//                actions: <Widget>[
//                  new FlatButton(
//                    onPressed: () {
//                      _provider.check(context, widget.flag, null);
//                      Navigator.pop(context);
//                    },
//                    child: new Text('Ya', style: TextStyle(fontSize: 12, color: Colors.grey),),
//                  ),
//                  new FlatButton(
//                    onPressed: () => Navigator.of(context).pop(true),
//                    child: new Text('Tidak'),
//                  ),
//                ],
//              ),
//          )) ?? false;
//      } else {
//        return true;
//      }
//    } else {
//      if (_provider.jenisAlamatSelectedTemp.KODE != _provider.jenisAlamatSelected.KODE ||
//          _provider.alamatTemp != _provider.controllerAlamat.text ||
//          _provider.rtTemp != _provider.controllerRT.text ||
//          _provider.rwTemp != _provider.controllerRW.text ||
//          _provider.kelurahanTemp != _provider.controllerKelurahan.text ||
//          _provider.kecamatanTemp != _provider.controllerKecamatan.text ||
//          _provider.kotaTemp != _provider.controllerKota.text ||
//          _provider.provinsiTemp != _provider.controllerProvinsi.text ||
//          _provider.postalCodeTemp != _provider.controllerPostalCode.text ||
//          _provider.telepon1AreaTemp != _provider.controllerTelepon1Area.text ||
//          _provider.telepon1Temp != _provider.controllerTelepon1.text ||
//          _provider.telepon2AreaTemp != _provider.controllerTelepon2Area.text ||
//          _provider.telepon2Temp != _provider.controllerTelepon2.text ||
//          _provider.faxAreaTemp != _provider.controllerFaxArea.text ||
//          _provider.faxTemp != _provider.controllerFax.text) {
//          return (await showDialog(
//            context: context,
//            builder: (myContext) => AlertDialog(
//              title: new Text('Warning'),
//              content: new Text('Simpan perubahan?'),
//              actions: <Widget>[
//                new FlatButton(
//                  onPressed: () {
//                    _provider.check(context, widget.flag, widget.index);
//                    Navigator.pop(context);
//                  },
//                  child: new Text('Ya', style: TextStyle(fontSize: 12, color: Colors.grey),),
//                ),
//                new FlatButton(
//                  onPressed: () => Navigator.of(context).pop(true),
//                  child: new Text('Tidak'),
//                ),
//              ],
//            ),
//          )) ?? false;
//      } else {
//        return true;
//      }
//    }
//  }
}
