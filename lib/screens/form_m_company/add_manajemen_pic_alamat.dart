import 'package:ad1ms2_dev/models/address_guarantor_model.dart';
import 'package:ad1ms2_dev/models/form_m_company_manajemen_pic_alamat_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_company_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_add_manajemen_pic_alamat_change_notif.dart';
import 'package:ad1ms2_dev/widgets/widget_address_company.dart';
import 'package:ad1ms2_dev/widgets/widget_address_individu.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class CompanyAddManajemenPICAlamat extends StatefulWidget {
  final int flag;
  final int index;
  final AddressModel addressModel;
  final int typeAddress;

  const CompanyAddManajemenPICAlamat(
      {this.flag, this.index, this.addressModel, this.typeAddress});
  @override
  _CompanyAddManajemenPICAlamatState createState() => _CompanyAddManajemenPICAlamatState();
}

class _CompanyAddManajemenPICAlamatState extends State<CompanyAddManajemenPICAlamat> {
  Future<void> _setValueForEdit;

  @override
  void initState() {
    super.initState();
    if (widget.flag != 0) {
      _setValueForEdit = Provider.of<FormMAddAddressIndividuChangeNotifier>(
          context,
          listen: false)
          .addDataListJenisAlamat(widget.addressModel, context, widget.flag,
          widget.addressModel.isSameWithIdentity,widget.index, widget.typeAddress);
    } else {
      Provider.of<FormMAddAddressIndividuChangeNotifier>(context,
          listen: false)
          .addDataListJenisAlamat(null,context,widget.flag,null,null, widget.typeAddress);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: Colors.yellow),
      child:
      WidgetAddressIndividu(flag: widget.flag, index: widget.index, editVal: _setValueForEdit,
        addressModel: widget.addressModel, typeAddress: widget.typeAddress,)

//      Scaffold(
//        appBar: AppBar(
//          title: Text(
//              widget.flag == 0
//                  ? "Add Alamat Korespondensi"
//                  : "Edit Alamat Korespondensi",
//              style: TextStyle(color: Colors.black)),
//          centerTitle: true,
//          backgroundColor: myPrimaryColor,
//          iconTheme: IconThemeData(color: Colors.black),
//        ),
//        body: SingleChildScrollView(
//            padding: EdgeInsets.symmetric(
//                horizontal: MediaQuery.of(context).size.width / 27,
//                vertical: MediaQuery.of(context).size.height / 57),
//            child: widget.flag == 0
//                ? Consumer<FormMCompanyAddManajemenPICAlamatChangeNotifier>(
//                    builder: (context, formMCompanyAddManajemenPICAlamatNotif, _) {
//                      return Form(
//                        key: formMCompanyAddManajemenPICAlamatNotif.key,
//                        onWillPop: _onWillPop,
//                        child: Column(
//                          crossAxisAlignment: CrossAxisAlignment.start,
//                          children: [
//                            DropdownButtonFormField<JenisAlamatModel>(
//                                autovalidate:
//                                    formMCompanyAddManajemenPICAlamatNotif.autoValidate,
//                                validator: (e) {
//                                  if (e == null) {
//                                    return "Silahkan pilih jenis alamat";
//                                  } else {
//                                    return null;
//                                  }
//                                },
//                                value: formMCompanyAddManajemenPICAlamatNotif.jenisAlamatSelected,
//                                onChanged: (value) {
//                                  formMCompanyAddManajemenPICAlamatNotif.jenisAlamatSelected = value;
//                                },
//                                onTap: () {
//                                  FocusManager.instance.primaryFocus.unfocus();
//                                },
//                                decoration: InputDecoration(
//                                  labelText: "Jenis Alamat",
//                                  border: OutlineInputBorder(),
//                                  contentPadding:
//                                      EdgeInsets.symmetric(horizontal: 10),
//                                ),
//                                items: formMCompanyAddManajemenPICAlamatNotif.listJenisAlamat
//                                    .map((value) {
//                                  return DropdownMenuItem<JenisAlamatModel>(
//                                    value: value,
//                                    child: Text(
//                                      value.DESKRIPSI,
//                                      overflow: TextOverflow.ellipsis,
//                                    ),
//                                  );
//                                }).toList()),
//                            formMCompanyAddManajemenPICAlamatNotif.jenisAlamatSelected != null
//                            ? formMCompanyAddManajemenPICAlamatNotif.jenisAlamatSelected.KODE == "02"
//                              ? Row(
//                                children: [
//                                  Checkbox(
//                                    value: formMCompanyAddManajemenPICAlamatNotif
//                                        .isSameWithIdentity,
//                                    onChanged: (value) {
//                                      formMCompanyAddManajemenPICAlamatNotif
//                                          .isSameWithIdentity = value;
//                                      formMCompanyAddManajemenPICAlamatNotif
//                                          .setValueIsSameWithIdentity(
//                                          value, context, null);
//                                    },
//                                    activeColor: myPrimaryColor),
//                                  SizedBox(width: 8),
//                                  Text("Sama dengan identitas")
//                                ],
//                              )
//                              : SizedBox(height: MediaQuery.of(context).size.height / 47)
//                            : SizedBox(height: MediaQuery.of(context).size.height / 47),
//                            TextFormField(
//                              autovalidate:
//                              formMCompanyAddManajemenPICAlamatNotif.autoValidate,
//                              validator: (e) {
//                                if (e.isEmpty) {
//                                  return "Tidak boleh kosong";
//                                } else {
//                                  return null;
//                                }
//                              },
//                              controller:
//                              formMCompanyAddManajemenPICAlamatNotif.controllerAlamat,
//                              enabled:
//                              formMCompanyAddManajemenPICAlamatNotif.enableTfAddress,
//                              style: TextStyle(color: Colors.black),
//                              decoration: InputDecoration(
//                                labelText: 'Alamat',
//                                labelStyle:
//                                TextStyle(color: Colors.black),
//                                border: OutlineInputBorder(
//                                    borderRadius:
//                                    BorderRadius.circular(8)),
//                                filled: !formMCompanyAddManajemenPICAlamatNotif
//                                    .enableTfAddress,
//                                fillColor: !formMCompanyAddManajemenPICAlamatNotif
//                                    .enableTfAddress
//                                    ? Colors.black12
//                                    : Colors.white,),
//                              maxLines: 3,
//                              keyboardType: TextInputType.text,
//                              textCapitalization: TextCapitalization.characters,
//                            ),
//                            SizedBox(
//                                height: MediaQuery.of(context).size.height /
//                                    47),
//                            Row(
//                              children: [
//                                Expanded(
//                                  flex: 5,
//                                  child: TextFormField(
//                                    autovalidate:
//                                    formMCompanyAddManajemenPICAlamatNotif.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    inputFormatters: [
//                                      WhitelistingTextInputFormatter.digitsOnly,
//                                      // LengthLimitingTextInputFormatter(10),
//                                    ],
//                                    controller:
//                                    formMCompanyAddManajemenPICAlamatNotif.controllerRT,
//                                    enabled:
//                                    formMCompanyAddManajemenPICAlamatNotif.enableTfRT,
//                                    style: TextStyle(color: Colors.black),
//                                    decoration: InputDecoration(
//                                      labelText: 'RT',
//                                      labelStyle:
//                                      TextStyle(color: Colors.black),
//                                      border: OutlineInputBorder(
//                                          borderRadius:
//                                          BorderRadius.circular(8)),
//                                      filled: !formMCompanyAddManajemenPICAlamatNotif
//                                          .enableTfAddress,
//                                      fillColor: !formMCompanyAddManajemenPICAlamatNotif
//                                          .enableTfAddress
//                                          ? Colors.black12
//                                          : Colors.white,),
//                                    keyboardType: TextInputType.number,
//                                  ),
//                                ),
//                                SizedBox(width: 8),
//                                Expanded(
//                                  flex: 5,
//                                  child: TextFormField(
//                                    autovalidate:
//                                    formMCompanyAddManajemenPICAlamatNotif.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    inputFormatters: [
//                                      WhitelistingTextInputFormatter.digitsOnly,
//                                      // LengthLimitingTextInputFormatter(10),
//                                    ],
//                                    controller:
//                                    formMCompanyAddManajemenPICAlamatNotif.controllerRW,
//                                    enabled: formMCompanyAddManajemenPICAlamatNotif.enableTfRW,
//                                    style: TextStyle(color: Colors.black),
//                                    decoration: InputDecoration(
//                                      labelText: 'RW',
//                                      labelStyle:
//                                      TextStyle(color: Colors.black),
//                                      border: OutlineInputBorder(
//                                          borderRadius:
//                                          BorderRadius.circular(8)),
//                                      filled: !formMCompanyAddManajemenPICAlamatNotif
//                                          .enableTfRW,
//                                      fillColor: !formMCompanyAddManajemenPICAlamatNotif
//                                          .enableTfRW
//                                          ? Colors.black12
//                                          : Colors.white,),
//                                    keyboardType: TextInputType.number,
//                                  ),
//                                )
//                              ],
//                            ),
//                            SizedBox(
//                                height: MediaQuery.of(context).size.height /
//                                    47),
//                            FocusScope(
//                              node: FocusScopeNode(),
//                              child: TextFormField(
//                                autovalidate:
//                                formMCompanyAddManajemenPICAlamatNotif.autoValidate,
//                                validator: (e) {
//                                  if (e.isEmpty) {
//                                    return "Tidak boleh kosong";
//                                  } else {
//                                    return null;
//                                  }
//                                },
//                                onTap: () {
//                                  FocusManager.instance.primaryFocus.unfocus();
//                                  formMCompanyAddManajemenPICAlamatNotif
//                                      .searchKelurahan(context);
//                                },
//                                controller: formMCompanyAddManajemenPICAlamatNotif
//                                    .controllerKelurahan,
//                                enabled: formMCompanyAddManajemenPICAlamatNotif.enableTfKelurahan,
//                                style: new TextStyle(color: Colors.black),
//                                decoration: new InputDecoration(
//                                  labelText: 'Kelurahan',
//                                  labelStyle:
//                                  TextStyle(color: Colors.black),
//                                  border: OutlineInputBorder(
//                                      borderRadius:
//                                      BorderRadius.circular(8)),
//                                  filled: !formMCompanyAddManajemenPICAlamatNotif
//                                      .enableTfKelurahan,
//                                  fillColor: !formMCompanyAddManajemenPICAlamatNotif
//                                      .enableTfKelurahan
//                                      ? Colors.black12
//                                      : Colors.white,),
//                              ),
//                            ),
//                            SizedBox(
//                                height: MediaQuery.of(context).size.height /
//                                    47),
//                            TextFormField(
//                              autovalidate:
//                              formMCompanyAddManajemenPICAlamatNotif.autoValidate,
//                              validator: (e) {
//                                if (e.isEmpty) {
//                                  return "Tidak boleh kosong";
//                                } else {
//                                  return null;
//                                }
//                              },
//                              controller: formMCompanyAddManajemenPICAlamatNotif
//                                  .controllerKecamatan,
//                              // enabled: formMCompanyAddManajemenPICAlamatNotif.enableTfKecamatan,
//                              style: new TextStyle(color: Colors.black),
//                              decoration: new InputDecoration(
//                                labelText: 'Kecamatan',
//                                labelStyle:
//                                TextStyle(color: Colors.black),
//                                border: OutlineInputBorder(
//                                    borderRadius:
//                                    BorderRadius.circular(8)),
//                                filled: !formMCompanyAddManajemenPICAlamatNotif
//                                    .enableTfKecamatan,
//                                fillColor: !formMCompanyAddManajemenPICAlamatNotif
//                                    .enableTfKecamatan
//                                    ? Colors.black12
//                                    : Colors.white,),
//                              enabled: false,
//                            ),
//                            SizedBox(
//                                height: MediaQuery.of(context).size.height /
//                                    47),
//                            TextFormField(
//                              autovalidate:
//                              formMCompanyAddManajemenPICAlamatNotif.autoValidate,
//                              validator: (e) {
//                                if (e.isEmpty) {
//                                  return "Tidak boleh kosong";
//                                } else {
//                                  return null;
//                                }
//                              },
//                              controller:
//                              formMCompanyAddManajemenPICAlamatNotif.controllerKota,
//                              // enabled: formMCompanyAddManajemenPICAlamatNotif.enableTfKota,
//                              style: new TextStyle(color: Colors.black),
//                              decoration: new InputDecoration(
//                                labelText: 'Kabupaten/Kota',
//                                labelStyle:
//                                TextStyle(color: Colors.black),
//                                border: OutlineInputBorder(
//                                    borderRadius:
//                                    BorderRadius.circular(8)),
//                                filled: !formMCompanyAddManajemenPICAlamatNotif
//                                    .enableTfKota,
//                                fillColor: !formMCompanyAddManajemenPICAlamatNotif
//                                    .enableTfKota
//                                    ? Colors.black12
//                                    : Colors.white,),
//                              enabled: false,
//                            ),
//                            SizedBox(
//                                height: MediaQuery.of(context).size.height /
//                                    47),
//                            Row(
//                              children: [
//                                Expanded(
//                                  flex: 7,
//                                  child: TextFormField(
//                                    autovalidate:
//                                    formMCompanyAddManajemenPICAlamatNotif.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    controller: formMCompanyAddManajemenPICAlamatNotif
//                                        .controllerProvinsi,
//                                    // enabled: formMCompanyAddManajemenPICAlamatNotif.enableTfProv,
//                                    style: new TextStyle(color: Colors.black),
//                                    decoration: new InputDecoration(
//                                      labelText: 'Provinsi',
//                                      labelStyle:
//                                      TextStyle(color: Colors.black),
//                                      border: OutlineInputBorder(
//                                          borderRadius:
//                                          BorderRadius.circular(8)),
//                                      filled: !formMCompanyAddManajemenPICAlamatNotif
//                                          .enableTfProv,
//                                      fillColor: !formMCompanyAddManajemenPICAlamatNotif
//                                          .enableTfProv
//                                          ? Colors.black12
//                                          : Colors.white,),
//                                    enabled: false,
//                                  ),
//                                ),
//                                SizedBox(width: 8),
//                                Expanded(
//                                  flex: 3,
//                                  child: TextFormField(
//                                    autovalidate:
//                                    formMCompanyAddManajemenPICAlamatNotif.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    controller: formMCompanyAddManajemenPICAlamatNotif
//                                        .controllerPostalCode,
//                                    // enabled: formMCompanyAddManajemenPICAlamatNotif.enableTfPostalCode,
//                                    style: new TextStyle(color: Colors.black),
//                                    decoration: new InputDecoration(
//                                      labelText: 'Kode Pos',
//                                      labelStyle:
//                                      TextStyle(color: Colors.black),
//                                      border: OutlineInputBorder(
//                                          borderRadius:
//                                          BorderRadius.circular(8)),
//                                      filled: !formMCompanyAddManajemenPICAlamatNotif
//                                          .enableTfPostalCode,
//                                      fillColor: !formMCompanyAddManajemenPICAlamatNotif
//                                          .enableTfPostalCode
//                                          ? Colors.black12
//                                          : Colors.white,),
//                                    enabled: false,
//                                  ),
//                                ),
//                              ],
//                            ),
//                            SizedBox(
//                                height: MediaQuery.of(context).size.height /
//                                    47),
//                            Row(
//                              children: [
//                                Expanded(
//                                  flex: 4,
//                                  child: TextFormField(
//                                    autovalidate:
//                                    formMCompanyAddManajemenPICAlamatNotif.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    onChanged: (e) {
//                                      formMCompanyAddManajemenPICAlamatNotif
//                                          .checkValidCodeArea(e);
//                                    },
//                                    keyboardType: TextInputType.number,
//                                    inputFormatters: [
//                                      WhitelistingTextInputFormatter.digitsOnly,
//                                      // LengthLimitingTextInputFormatter(10),
//                                    ],
//                                    controller: formMCompanyAddManajemenPICAlamatNotif
//                                        .controllerTeleponArea,
//                                    enabled: formMCompanyAddManajemenPICAlamatNotif.enableTfAreaCode,
//                                    style:
//                                    new TextStyle(color: Colors.black),
//                                    decoration: new InputDecoration(
//                                      labelText: 'Telepon (Area)',
//                                      labelStyle:
//                                      TextStyle(color: Colors.black),
//                                      border: OutlineInputBorder(
//                                          borderRadius:
//                                          BorderRadius.circular(8)),
//                                      filled: !formMCompanyAddManajemenPICAlamatNotif
//                                          .enableTfAreaCode,
//                                      fillColor: !formMCompanyAddManajemenPICAlamatNotif
//                                          .enableTfAreaCode
//                                          ? Colors.black12
//                                          : Colors.white,),
//                                  ),
//                                ),
//                                SizedBox(
//                                    width:
//                                    MediaQuery.of(context).size.width /
//                                        37),
//                                Expanded(
//                                  flex: 6,
//                                  child: TextFormField(
//                                    autovalidate:
//                                    formMCompanyAddManajemenPICAlamatNotif.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    keyboardType: TextInputType.number,
//                                    inputFormatters: [
//                                      WhitelistingTextInputFormatter.digitsOnly,
//                                      // LengthLimitingTextInputFormatter(10),
//                                    ],
//                                    controller: formMCompanyAddManajemenPICAlamatNotif
//                                        .controllerTelepon,
//                                    enabled: formMCompanyAddManajemenPICAlamatNotif.enableTfPhone,
//                                    style:
//                                    new TextStyle(color: Colors.black),
//                                    decoration: new InputDecoration(
//                                      labelText: 'Telepon',
//                                      labelStyle:
//                                      TextStyle(color: Colors.black),
//                                      border: OutlineInputBorder(
//                                          borderRadius:
//                                          BorderRadius.circular(8)),
//                                      filled: !formMCompanyAddManajemenPICAlamatNotif
//                                          .enableTfPhone,
//                                      fillColor: !formMCompanyAddManajemenPICAlamatNotif
//                                          .enableTfPhone
//                                          ? Colors.black12
//                                          : Colors.white,),
//                                  ),
//                                ),
//                              ],
//                            ),
//                          ],
//                        ),
//                      );
//                    },
//                  )
//                : FutureBuilder(
//                    future: _setValueForEdit,
//                    builder: (context, snapshot) {
//                      if (snapshot.connectionState == ConnectionState.waiting) {
//                        return Center(
//                          child: CircularProgressIndicator(),
//                        );
//                      }
//                      return Consumer<FormMCompanyAddManajemenPICAlamatChangeNotifier>(
//                        builder: (context, formMCompanyAddManajemenPICAlamatNotif, _) {
//                          return Form(
//                            key: formMCompanyAddManajemenPICAlamatNotif.key,
//                            onWillPop: _onWillPop,
//                            child: Column(
//                              crossAxisAlignment: CrossAxisAlignment.start,
//                              children: [
//                                DropdownButtonFormField<JenisAlamatModel>(
//                                    autovalidate:
//                                    formMCompanyAddManajemenPICAlamatNotif.autoValidate,
//                                    validator: (e) {
//                                      if (e == null) {
//                                        return "Silahkan pilih jenis alamat";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    value: formMCompanyAddManajemenPICAlamatNotif.jenisAlamatSelected,
//                                    onChanged: (value) {
//                                      formMCompanyAddManajemenPICAlamatNotif.jenisAlamatSelected = value;
//                                    },
//                                    onTap: () {
//                                      FocusManager.instance.primaryFocus.unfocus();
//                                    },
//                                    decoration: InputDecoration(
//                                      labelText: "Jenis Alamat",
//                                      border: OutlineInputBorder(),
//                                      contentPadding:
//                                          EdgeInsets.symmetric(horizontal: 10),
//                                    ),
//                                    items: formMCompanyAddManajemenPICAlamatNotif
//                                        .listJenisAlamat
//                                        .map((value) {
//                                      return DropdownMenuItem<JenisAlamatModel>(
//                                        value: value,
//                                        child: Text(
//                                          value.DESKRIPSI,
//                                          overflow: TextOverflow.ellipsis,
//                                        ),
//                                      );
//                                    }).toList()),
//                                formMCompanyAddManajemenPICAlamatNotif.jenisAlamatSelected != null
//                                  ? formMCompanyAddManajemenPICAlamatNotif.jenisAlamatSelected.KODE == "02"
//                                  ? Row(
//                                    children: [
//                                      Checkbox(
//                                        value:
//                                        formMCompanyAddManajemenPICAlamatNotif.isSameWithIdentity,
//                                        onChanged: (value) {
//                                          formMCompanyAddManajemenPICAlamatNotif.isSameWithIdentity = value;
//                                          formMCompanyAddManajemenPICAlamatNotif.setValueIsSameWithIdentity(
//                                              value,
//                                              context,
//                                              null);
//                                        },
//                                        activeColor: myPrimaryColor),
//                                      SizedBox(width: 8),
//                                      Text("Sama dengan identitas")
//                                    ],
//                                  )
//                                : SizedBox(height: MediaQuery.of(context).size.height / 47)
//                                : SizedBox(height: MediaQuery.of(context).size.height / 47),
//                                TextFormField(
//                                  autovalidate:
//                                  formMCompanyAddManajemenPICAlamatNotif.autoValidate,
//                                  validator: (e) {
//                                    if (e.isEmpty) {
//                                      return "Tidak boleh kosong";
//                                    } else {
//                                      return null;
//                                    }
//                                  },
//                                  controller:
//                                  formMCompanyAddManajemenPICAlamatNotif.controllerAlamat,
//                                  enabled:
//                                  formMCompanyAddManajemenPICAlamatNotif.enableTfAddress,
//                                  style: TextStyle(color: Colors.black),
//                                  decoration: InputDecoration(
//                                      labelText: 'Alamat',
//                                      labelStyle:
//                                          TextStyle(color: Colors.black),
//                                      border: OutlineInputBorder(
//                                          borderRadius:
//                                              BorderRadius.circular(8)),
//                                      filled: !formMCompanyAddManajemenPICAlamatNotif
//                                          .enableTfAddress,
//                                      fillColor: !formMCompanyAddManajemenPICAlamatNotif
//                                          .enableTfAddress
//                                          ? Colors.black12
//                                          : Colors.white,),
//                                  maxLines: 3,
//                                  keyboardType: TextInputType.text,
//                                  textCapitalization: TextCapitalization.characters,
//                                ),
//                                SizedBox(
//                                    height: MediaQuery.of(context).size.height /
//                                        47),
//                                Row(
//                                  children: [
//                                    Expanded(
//                                      flex: 5,
//                                      child: TextFormField(
//                                        autovalidate:
//                                        formMCompanyAddManajemenPICAlamatNotif.autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        inputFormatters: [
//                                          WhitelistingTextInputFormatter.digitsOnly,
//                                          // LengthLimitingTextInputFormatter(10),
//                                        ],
//                                        controller:
//                                        formMCompanyAddManajemenPICAlamatNotif.controllerRT,
//                                        enabled:
//                                        formMCompanyAddManajemenPICAlamatNotif.enableTfRT,
//                                        style: TextStyle(color: Colors.black),
//                                        decoration: InputDecoration(
//                                            labelText: 'RT',
//                                            labelStyle:
//                                                TextStyle(color: Colors.black),
//                                            border: OutlineInputBorder(
//                                                borderRadius:
//                                                    BorderRadius.circular(8)),
//                                            filled: !formMCompanyAddManajemenPICAlamatNotif
//                                                .enableTfAddress,
//                                            fillColor: !formMCompanyAddManajemenPICAlamatNotif
//                                                .enableTfAddress
//                                                ? Colors.black12
//                                                : Colors.white,),
//                                        keyboardType: TextInputType.number,
//                                      ),
//                                    ),
//                                    SizedBox(width: 8),
//                                    Expanded(
//                                      flex: 5,
//                                      child: TextFormField(
//                                        autovalidate:
//                                        formMCompanyAddManajemenPICAlamatNotif.autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        inputFormatters: [
//                                          WhitelistingTextInputFormatter.digitsOnly,
//                                          // LengthLimitingTextInputFormatter(10),
//                                        ],
//                                        controller:
//                                        formMCompanyAddManajemenPICAlamatNotif.controllerRW,
//                                        enabled: formMCompanyAddManajemenPICAlamatNotif.enableTfRW,
//                                        style: TextStyle(color: Colors.black),
//                                        decoration: InputDecoration(
//                                            labelText: 'RW',
//                                            labelStyle:
//                                                TextStyle(color: Colors.black),
//                                            border: OutlineInputBorder(
//                                                borderRadius:
//                                                    BorderRadius.circular(8)),
//                                            filled: !formMCompanyAddManajemenPICAlamatNotif
//                                                .enableTfRW,
//                                            fillColor: !formMCompanyAddManajemenPICAlamatNotif
//                                                .enableTfRW
//                                                ? Colors.black12
//                                                : Colors.white,),
//                                        keyboardType: TextInputType.number,
//                                      ),
//                                    )
//                                  ],
//                                ),
//                                SizedBox(
//                                    height: MediaQuery.of(context).size.height /
//                                        47),
//                                FocusScope(
//                                  node: FocusScopeNode(),
//                                  child: TextFormField(
//                                    autovalidate:
//                                    formMCompanyAddManajemenPICAlamatNotif.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    onTap: () {
//                                      FocusManager.instance.primaryFocus.unfocus();
//                                      formMCompanyAddManajemenPICAlamatNotif
//                                          .searchKelurahan(context);
//                                    },
//                                    controller: formMCompanyAddManajemenPICAlamatNotif
//                                        .controllerKelurahan,
//                                    enabled: formMCompanyAddManajemenPICAlamatNotif.enableTfKelurahan,
//                                    style: new TextStyle(color: Colors.black),
//                                    decoration: new InputDecoration(
//                                        labelText: 'Kelurahan',
//                                        labelStyle:
//                                            TextStyle(color: Colors.black),
//                                        border: OutlineInputBorder(
//                                            borderRadius:
//                                                BorderRadius.circular(8)),
//                                        filled: !formMCompanyAddManajemenPICAlamatNotif
//                                            .enableTfKelurahan,
//                                        fillColor: !formMCompanyAddManajemenPICAlamatNotif
//                                            .enableTfKelurahan
//                                            ? Colors.black12
//                                            : Colors.white,),
//                                  ),
//                                ),
//                                SizedBox(
//                                    height: MediaQuery.of(context).size.height /
//                                        47),
//                                TextFormField(
//                                  autovalidate:
//                                  formMCompanyAddManajemenPICAlamatNotif.autoValidate,
//                                  validator: (e) {
//                                    if (e.isEmpty) {
//                                      return "Tidak boleh kosong";
//                                    } else {
//                                      return null;
//                                    }
//                                  },
//                                  controller: formMCompanyAddManajemenPICAlamatNotif
//                                      .controllerKecamatan,
//                                  // enabled: formMCompanyAddManajemenPICAlamatNotif.enableTfKecamatan,
//                                  style: new TextStyle(color: Colors.black),
//                                  decoration: new InputDecoration(
//                                      labelText: 'Kecamatan',
//                                      labelStyle:
//                                          TextStyle(color: Colors.black),
//                                      border: OutlineInputBorder(
//                                          borderRadius:
//                                              BorderRadius.circular(8)),
//                                      filled: !formMCompanyAddManajemenPICAlamatNotif
//                                          .enableTfKecamatan,
//                                      fillColor: !formMCompanyAddManajemenPICAlamatNotif
//                                          .enableTfKecamatan
//                                          ? Colors.black12
//                                          : Colors.white,),
//                                  enabled: false,
//                                ),
//                                SizedBox(
//                                    height: MediaQuery.of(context).size.height /
//                                        47),
//                                TextFormField(
//                                  autovalidate:
//                                  formMCompanyAddManajemenPICAlamatNotif.autoValidate,
//                                  validator: (e) {
//                                    if (e.isEmpty) {
//                                      return "Tidak boleh kosong";
//                                    } else {
//                                      return null;
//                                    }
//                                  },
//                                  controller:
//                                  formMCompanyAddManajemenPICAlamatNotif.controllerKota,
//                                  // enabled: formMCompanyAddManajemenPICAlamatNotif.enableTfKota,
//                                  style: new TextStyle(color: Colors.black),
//                                  decoration: new InputDecoration(
//                                      labelText: 'Kabupaten/Kota',
//                                      labelStyle:
//                                          TextStyle(color: Colors.black),
//                                      border: OutlineInputBorder(
//                                          borderRadius:
//                                              BorderRadius.circular(8)),
//                                      filled: !formMCompanyAddManajemenPICAlamatNotif
//                                          .enableTfKota,
//                                      fillColor: !formMCompanyAddManajemenPICAlamatNotif
//                                          .enableTfKota
//                                          ? Colors.black12
//                                          : Colors.white,),
//                                  enabled: false,
//                                ),
//                                SizedBox(
//                                    height: MediaQuery.of(context).size.height /
//                                        47),
//                                Row(
//                                  children: [
//                                    Expanded(
//                                      flex: 7,
//                                      child: TextFormField(
//                                        autovalidate:
//                                        formMCompanyAddManajemenPICAlamatNotif.autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        controller: formMCompanyAddManajemenPICAlamatNotif
//                                            .controllerProvinsi,
//                                        // enabled: formMCompanyAddManajemenPICAlamatNotif.enableTfProv,
//                                        style: new TextStyle(color: Colors.black),
//                                        decoration: new InputDecoration(
//                                            labelText: 'Provinsi',
//                                            labelStyle:
//                                                TextStyle(color: Colors.black),
//                                            border: OutlineInputBorder(
//                                                borderRadius:
//                                                    BorderRadius.circular(8)),
//                                            filled: !formMCompanyAddManajemenPICAlamatNotif
//                                                .enableTfProv,
//                                            fillColor: !formMCompanyAddManajemenPICAlamatNotif
//                                                .enableTfProv
//                                                ? Colors.black12
//                                                : Colors.white,),
//                                        enabled: false,
//                                      ),
//                                    ),
//                                    SizedBox(width: 8),
//                                    Expanded(
//                                      flex: 3,
//                                      child: TextFormField(
//                                        autovalidate:
//                                        formMCompanyAddManajemenPICAlamatNotif.autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        controller: formMCompanyAddManajemenPICAlamatNotif
//                                            .controllerPostalCode,
//                                        // enabled: formMCompanyAddManajemenPICAlamatNotif.enableTfPostalCode,
//                                        style: new TextStyle(color: Colors.black),
//                                        decoration: new InputDecoration(
//                                            labelText: 'Kode Pos',
//                                            labelStyle:
//                                                TextStyle(color: Colors.black),
//                                            border: OutlineInputBorder(
//                                                borderRadius:
//                                                    BorderRadius.circular(8)),
//                                            filled: !formMCompanyAddManajemenPICAlamatNotif
//                                                .enableTfPostalCode,
//                                            fillColor: !formMCompanyAddManajemenPICAlamatNotif
//                                                .enableTfPostalCode
//                                                ? Colors.black12
//                                                : Colors.white,),
//                                        enabled: false,
//                                      ),
//                                    ),
//                                  ],
//                                ),
//                                SizedBox(
//                                    height: MediaQuery.of(context).size.height /
//                                        47),
//                                Row(
//                                  children: [
//                                    Expanded(
//                                      flex: 4,
//                                      child: TextFormField(
//                                        autovalidate:
//                                        formMCompanyAddManajemenPICAlamatNotif.autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        onChanged: (e) {
//                                          formMCompanyAddManajemenPICAlamatNotif
//                                              .checkValidCodeArea(e);
//                                        },
//                                        keyboardType: TextInputType.number,
//                                        inputFormatters: [
//                                          WhitelistingTextInputFormatter.digitsOnly,
//                                          // LengthLimitingTextInputFormatter(10),
//                                        ],
//                                        controller: formMCompanyAddManajemenPICAlamatNotif
//                                            .controllerTeleponArea,
//                                        enabled: formMCompanyAddManajemenPICAlamatNotif.enableTfAreaCode,
//                                        style:
//                                            new TextStyle(color: Colors.black),
//                                        decoration: new InputDecoration(
//                                            labelText: 'Telepon (Area)',
//                                            labelStyle:
//                                                TextStyle(color: Colors.black),
//                                            border: OutlineInputBorder(
//                                                borderRadius:
//                                                    BorderRadius.circular(8)),
//                                            filled: !formMCompanyAddManajemenPICAlamatNotif
//                                                .enableTfAreaCode,
//                                            fillColor: !formMCompanyAddManajemenPICAlamatNotif
//                                                .enableTfAreaCode
//                                                ? Colors.black12
//                                                : Colors.white,),
//                                      ),
//                                    ),
//                                    SizedBox(
//                                        width:
//                                            MediaQuery.of(context).size.width /
//                                                37),
//                                    Expanded(
//                                      flex: 6,
//                                      child: TextFormField(
//                                        autovalidate:
//                                        formMCompanyAddManajemenPICAlamatNotif.autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        keyboardType: TextInputType.number,
//                                        inputFormatters: [
//                                          WhitelistingTextInputFormatter.digitsOnly,
//                                          // LengthLimitingTextInputFormatter(10),
//                                        ],
//                                        controller: formMCompanyAddManajemenPICAlamatNotif
//                                            .controllerTelepon,
//                                        enabled: formMCompanyAddManajemenPICAlamatNotif.enableTfPhone,
//                                        style:
//                                            new TextStyle(color: Colors.black),
//                                        decoration: new InputDecoration(
//                                            labelText: 'Telepon',
//                                            labelStyle:
//                                                TextStyle(color: Colors.black),
//                                            border: OutlineInputBorder(
//                                                borderRadius:
//                                                    BorderRadius.circular(8)),
//                                            filled: !formMCompanyAddManajemenPICAlamatNotif
//                                                .enableTfPhone,
//                                            fillColor: !formMCompanyAddManajemenPICAlamatNotif
//                                                .enableTfPhone
//                                                ? Colors.black12
//                                                : Colors.white,),
//                                      ),
//                                    ),
//                                  ],
//                                ),
//                              ],
//                            ),
//                          );
//                        },
//                      );
//                    })),
//        bottomNavigationBar: BottomAppBar(
//          elevation: 0.0,
//          child: Padding(
//              padding: const EdgeInsets.all(8.0),
//              child: RaisedButton(
//                  shape: RoundedRectangleBorder(
//                      borderRadius: new BorderRadius.circular(8.0)),
//                  color: myPrimaryColor,
//                  onPressed: () {
//                    if (widget.flag == 0) {
//                      Provider.of<FormMCompanyAddManajemenPICAlamatChangeNotifier>(
//                              context,
//                              listen: false)
//                          .check(context, widget.flag, null);
//                    } else {
//                      Provider.of<FormMCompanyAddManajemenPICAlamatChangeNotifier>(
//                              context,
//                              listen: false)
//                          .check(context, widget.flag, widget.index);
//                    }
//                  },
//                  child: Row(
//                    mainAxisAlignment: MainAxisAlignment.center,
//                    children: <Widget>[
//                      Text(widget.flag == 0 ? "SAVE" : "UPDATE",
//                          style: TextStyle(
//                              color: Colors.black,
//                              fontSize: 14,
//                              fontWeight: FontWeight.w500,
//                              letterSpacing: 1.25))
//                    ],
//                  ))),
//        ),
//      ),
    );
  }

//  Future<bool> _onWillPop() async {
//    var _provider = Provider.of<FormMCompanyAddManajemenPICAlamatChangeNotifier>(context,
//        listen: false);
//    if (widget.flag == 0) {
//      if (_provider.jenisAlamatSelected != null ||
//          _provider.controllerAlamat.text != "" ||
//          _provider.controllerRT.text != "" ||
//          _provider.controllerRW.text != "" ||
//          _provider.controllerKelurahan.text != "" ||
//          _provider.controllerKecamatan.text != "" ||
//          _provider.controllerKota.text != "" ||
//          _provider.controllerProvinsi.text != "" ||
//          _provider.controllerPostalCode.text != "" ||
//          _provider.controllerTeleponArea.text != "" ||
//          _provider.controllerTelepon.text != "") {
//        return (await showDialog(
//              context: context,
//              builder: (myContext) => AlertDialog(
//                title: new Text('Warning'),
//                content: new Text('Simpan perubahan?'),
//                actions: <Widget>[
//                  new FlatButton(
//                    onPressed: () {
//                      _provider.check(context, widget.flag, null);
//                      Navigator.pop(context);
//                    },
//                    child: new Text('Ya', style: TextStyle(fontSize: 12, color: Colors.grey),),
//                  ),
//                  new FlatButton(
//                    onPressed: () => Navigator.of(context).pop(true),
//                    child: new Text('Tidak'),
//                  ),
//                ],
//              ),
//            )) ??
//            false;
//      } else {
//        return true;
//      }
//    } else {
//      if (_provider.jenisAlamatSelectedTemp.KODE != _provider.jenisAlamatSelected.KODE ||
//          _provider.alamatTemp != _provider.controllerAlamat.text ||
//          _provider.rtTemp != _provider.controllerRT.text ||
//          _provider.rwTemp != _provider.controllerRW.text ||
//          _provider.kelurahanTemp != _provider.controllerKelurahan.text ||
//          _provider.kecamatanTemp != _provider.controllerKecamatan.text ||
//          _provider.kotaTemp != _provider.controllerKota.text ||
//          _provider.provinsiTemp != _provider.controllerProvinsi.text ||
//          _provider.postalCodeTemp != _provider.controllerPostalCode.text ||
//          _provider.teleponAreaTemp != _provider.controllerTeleponArea.text ||
//          _provider.teleponTemp != _provider.controllerTelepon.text) {
//          return (await showDialog(
//            context: context,
//            builder: (myContext) => AlertDialog(
//              title: new Text('Warning'),
//              content: new Text('Simpan perubahan?'),
//              actions: <Widget>[
//                new FlatButton(
//                  onPressed: () {
//                    _provider.check(context, widget.flag, widget.index);
//                    Navigator.pop(context);
//                  },
//                  child: new Text('Ya', style: TextStyle(fontSize: 12, color: Colors.grey),),
//                ),
//                new FlatButton(
//                  onPressed: () => Navigator.of(context).pop(true),
//                  child: new Text('Tidak'),
//                ),
//              ],
//            ),
//          )) ?? false;
//      } else {
//        return true;
//      }
//    }
//  }
}
