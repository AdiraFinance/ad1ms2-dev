import 'package:ad1ms2_dev/models/form_m_company_pemegang_saham_pribadi_alamat_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/shared/form_m_add_address_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_add_pemegang_saham_pribadi_alamat_change_notif.dart';
import 'package:ad1ms2_dev/widgets/widget_address_individu.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class CompanyAddPemegangSahamPribadiAlamat extends StatefulWidget {
  final int flag;
  final int index;
  final AddressModel addressModel;
  final int typeAddress;

  const CompanyAddPemegangSahamPribadiAlamat(
      {this.flag, this.index, this.addressModel, this.typeAddress});
  @override
  _CompanyAddPemegangSahamPribadiAlamatState createState() => _CompanyAddPemegangSahamPribadiAlamatState();
}

class _CompanyAddPemegangSahamPribadiAlamatState extends State<CompanyAddPemegangSahamPribadiAlamat> {
  Future<void> _setValueForEdit;

  @override
  void initState() {
    super.initState();
    if (widget.flag != 0) {
        _setValueForEdit = Provider.of<FormMAddAddressIndividuChangeNotifier>(
            context,
            listen: false)
            .addDataListJenisAlamat(widget.addressModel, context, widget.flag,
            widget.addressModel.isSameWithIdentity,widget.index, widget.typeAddress);
    } else {
        Provider.of<FormMAddAddressIndividuChangeNotifier>(context,
            listen: false)
            .addDataListJenisAlamat(null,context,widget.flag,null,null, widget.typeAddress);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: Colors.yellow),
      child:
      WidgetAddressIndividu(flag: widget.flag, index: widget.index, editVal: _setValueForEdit,
          addressModel: widget.addressModel, typeAddress: widget.typeAddress,)

//      Scaffold(
//        appBar: AppBar(
//          title: Text(
//              widget.flag == 0
//                  ? "Add Alamat Korespondensi"
//                  : "Edit Alamat Korespondensi",
//              style: TextStyle(color: Colors.black)),
//          centerTitle: true,
//          backgroundColor: myPrimaryColor,
//          iconTheme: IconThemeData(color: Colors.black),
//        ),
//        body: SingleChildScrollView(
//            padding: EdgeInsets.symmetric(
//                horizontal: MediaQuery.of(context).size.width / 27,
//                vertical: MediaQuery.of(context).size.height / 57),
//            child: widget.flag == 0
//                ? Consumer<FormMCompanyAddPemegangSahamPribadiAlamatChangeNotifier>(
//                    builder: (context, formMCompanyAddPemegangSahamPribadiAlamatNotif, _) {
//                      return Form(
//                        key: formMCompanyAddPemegangSahamPribadiAlamatNotif.key,
//                        onWillPop: _onWillPop,
//                        child: Column(
//                          crossAxisAlignment: CrossAxisAlignment.start,
//                          children: [
//                            DropdownButtonFormField<JenisAlamatModel>(
//                                autovalidate:
//                                formMCompanyAddPemegangSahamPribadiAlamatNotif.autoValidate,
//                                validator: (e) {
//                                  if (e == null) {
//                                    return "Silahkan pilih jenis alamat";
//                                  } else {
//                                    return null;
//                                  }
//                                },
//                                value: formMCompanyAddPemegangSahamPribadiAlamatNotif.jenisAlamatSelected,
//                                onChanged: (value) {
//                                  formMCompanyAddPemegangSahamPribadiAlamatNotif.jenisAlamatSelected = value;
//                                },
//                                onTap: () {
//                                  FocusManager.instance.primaryFocus.unfocus();
//                                },
//                                decoration: InputDecoration(
//                                  labelText: "Jenis Alamat",
//                                  border: OutlineInputBorder(),
//                                  contentPadding:
//                                      EdgeInsets.symmetric(horizontal: 10),
//                                ),
//                                items: formMCompanyAddPemegangSahamPribadiAlamatNotif.listJenisAlamat
//                                    .map((value) {
//                                  return DropdownMenuItem<JenisAlamatModel>(
//                                    value: value,
//                                    child: Text(
//                                      value.DESKRIPSI,
//                                      overflow: TextOverflow.ellipsis,
//                                    ),
//                                  );
//                                }).toList()),
//                            formMCompanyAddPemegangSahamPribadiAlamatNotif.jenisAlamatSelected != null
//                              ? formMCompanyAddPemegangSahamPribadiAlamatNotif.jenisAlamatSelected.KODE == "02"
//                                ? Row(
//                                  children: [
//                                    Checkbox(
//                                        value: formMCompanyAddPemegangSahamPribadiAlamatNotif
//                                            .isSameWithIdentity,
//                                        onChanged: (value) {
//                                          formMCompanyAddPemegangSahamPribadiAlamatNotif
//                                              .isSameWithIdentity = value;
//                                          formMCompanyAddPemegangSahamPribadiAlamatNotif
//                                              .setValueIsSameWithIdentity(
//                                              value, context, null);
//                                        },
//                                        activeColor: myPrimaryColor),
//                                    SizedBox(width: 8),
//                                    Text("Sama dengan identitas")
//                                  ],
//                              )
//                              : SizedBox(height: MediaQuery.of(context).size.height / 47)
//                            : SizedBox(height: MediaQuery.of(context).size.height / 47),
//                            TextFormField(
//                              autovalidate: formMCompanyAddPemegangSahamPribadiAlamatNotif.autoValidate,
//                              validator: (e) {
//                                if (e.isEmpty) {
//                                  return "Tidak boleh kosong";
//                                } else {
//                                  return null;
//                                }
//                              },
//                              controller:
//                              formMCompanyAddPemegangSahamPribadiAlamatNotif.controllerAlamat,
//                              enabled:
//                              formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfAddress,
//                              style: TextStyle(color: Colors.black),
//                              decoration: InputDecoration(
//                                  labelText: 'Alamat',
//                                  labelStyle: TextStyle(color: Colors.black),
//                                  border: OutlineInputBorder(
//                                      borderRadius: BorderRadius.circular(8)),
//                                  filled: !formMCompanyAddPemegangSahamPribadiAlamatNotif
//                                      .enableTfAddress,
//                                  fillColor:
//                                  !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfAddress
//                                      ? Colors.black12
//                                      : Colors.white,),
//                              maxLines: 3,
//                              keyboardType: TextInputType.text,
//                              textCapitalization: TextCapitalization.characters,
//                            ),
//                            SizedBox(height: MediaQuery.of(context).size.height / 47),
//                            Row(
//                              children: [
//                                Expanded(
//                                  flex: 5,
//                                  child: TextFormField(
//                                    autovalidate:
//                                    formMCompanyAddPemegangSahamPribadiAlamatNotif.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    inputFormatters: [
//                                      WhitelistingTextInputFormatter.digitsOnly,
//                                      // LengthLimitingTextInputFormatter(10),
//                                    ],
//                                    controller:
//                                    formMCompanyAddPemegangSahamPribadiAlamatNotif.controllerRT,
//                                    enabled:
//                                    formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfRT,
//                                    style: TextStyle(color: Colors.black),
//                                    decoration: InputDecoration(
//                                        labelText: 'RT',
//                                        labelStyle:
//                                            TextStyle(color: Colors.black),
//                                        border: OutlineInputBorder(
//                                            borderRadius:
//                                                BorderRadius.circular(8)),
//                                        filled: !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfRT,
//                                        fillColor:
//                                        !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfRT
//                                            ? Colors.black12
//                                            : Colors.white,),
//                                    keyboardType: TextInputType.number,
//                                  ),
//                                ),
//                                SizedBox(width: 8),
//                                Expanded(
//                                  flex: 5,
//                                  child: TextFormField(
//                                    autovalidate:
//                                    formMCompanyAddPemegangSahamPribadiAlamatNotif.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    inputFormatters: [
//                                      WhitelistingTextInputFormatter.digitsOnly,
//                                      // LengthLimitingTextInputFormatter(10),
//                                    ],
//                                    controller:
//                                    formMCompanyAddPemegangSahamPribadiAlamatNotif.controllerRW,
//                                    enabled:
//                                    formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfRW,
//                                    style: TextStyle(color: Colors.black),
//                                    decoration: InputDecoration(
//                                        labelText: 'RW',
//                                        labelStyle:
//                                            TextStyle(color: Colors.black),
//                                        border: OutlineInputBorder(
//                                            borderRadius:
//                                                BorderRadius.circular(8)),
//                                        filled: !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfRW,
//                                        fillColor:
//                                        !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfRW
//                                            ? Colors.black12
//                                            : Colors.white,),
//                                    keyboardType: TextInputType.number,
//                                  ),
//                                )
//                              ],
//                            ),
//                            SizedBox(height: MediaQuery.of(context).size.height / 47),
//                            FocusScope(
//                              node: FocusScopeNode(),
//                              child: TextFormField(
//                                autovalidate:
//                                formMCompanyAddPemegangSahamPribadiAlamatNotif.autoValidate,
//                                validator: (e) {
//                                  if (e.isEmpty) {
//                                    return "Tidak boleh kosong";
//                                  } else {
//                                    return null;
//                                  }
//                                },
//                                onTap: () {
//                                  FocusManager.instance.primaryFocus.unfocus();
//                                  formMCompanyAddPemegangSahamPribadiAlamatNotif
//                                      .searchKelurahan(context);
//                                },
//                                controller:
//                                formMCompanyAddPemegangSahamPribadiAlamatNotif.controllerKelurahan,
//                                enabled:
//                                formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfKelurahan,
//                                style: new TextStyle(color: Colors.black),
//                                decoration: new InputDecoration(
//                                    labelText: 'Kelurahan',
//                                    labelStyle: TextStyle(color: Colors.black),
//                                    border: OutlineInputBorder(
//                                        borderRadius:
//                                              BorderRadius.circular(8)),
//                                    filled: !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfKelurahan,
//                                    fillColor:
//                                    !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfKelurahan
//                                        ? Colors.black12
//                                        : Colors.white,),
//                              ),
//                            ),
//                            SizedBox(height: MediaQuery.of(context).size.height / 47),
//                            TextFormField(
//                              autovalidate: formMCompanyAddPemegangSahamPribadiAlamatNotif.autoValidate,
//                              validator: (e) {
//                                if (e.isEmpty) {
//                                  return "Tidak boleh kosong";
//                                } else {
//                                  return null;
//                                }
//                              },
//                              controller:
//                              formMCompanyAddPemegangSahamPribadiAlamatNotif.controllerKecamatan,
//                              // enabled: formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfKecamatan,
//                              style: new TextStyle(color: Colors.black),
//                              decoration: new InputDecoration(
//                                  labelText: 'Kecamatan',
//                                  labelStyle: TextStyle(color: Colors.black),
//                                  border: OutlineInputBorder(
//                                      borderRadius: BorderRadius.circular(8)),
//                                  filled: !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfKecamatan,
//                                  fillColor:
//                                  !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfKecamatan
//                                      ? Colors.black12
//                                      : Colors.white,),
//                              enabled: false,
//                            ),
//                            SizedBox(height: MediaQuery.of(context).size.height / 47),
//                            TextFormField(
//                              autovalidate: formMCompanyAddPemegangSahamPribadiAlamatNotif.autoValidate,
//                              validator: (e) {
//                                if (e.isEmpty) {
//                                  return "Tidak boleh kosong";
//                                } else {
//                                  return null;
//                                }
//                              },
//                              controller: formMCompanyAddPemegangSahamPribadiAlamatNotif.controllerKota,
//                              // enabled: formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfKota,
//                              style: new TextStyle(color: Colors.black),
//                              decoration: new InputDecoration(
//                                  labelText: 'Kabupaten/Kota',
//                                  labelStyle: TextStyle(color: Colors.black),
//                                  border: OutlineInputBorder(
//                                      borderRadius: BorderRadius.circular(8)),
//                                  filled: !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfKota,
//                                  fillColor:
//                                  !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfKota
//                                      ? Colors.black12
//                                      : Colors.white,),
//                              enabled: false,
//                            ),
//                            SizedBox(height: MediaQuery.of(context).size.height / 47),
//                            Row(
//                              children: [
//                                Expanded(
//                                  flex: 7,
//                                  child: TextFormField(
//                                    autovalidate:
//                                    formMCompanyAddPemegangSahamPribadiAlamatNotif.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    controller: formMCompanyAddPemegangSahamPribadiAlamatNotif
//                                        .controllerProvinsi,
//                                    // enabled: formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfProv,
//                                    style: new TextStyle(color: Colors.black),
//                                    decoration: new InputDecoration(
//                                        labelText: 'Provinsi',
//                                        labelStyle:
//                                            TextStyle(color: Colors.black),
//                                        border: OutlineInputBorder(
//                                            borderRadius:
//                                                BorderRadius.circular(8)),
//                                        filled: !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfProv,
//                                        fillColor:
//                                        !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfProv
//                                            ? Colors.black12
//                                            : Colors.white,),
//                                    enabled: false,
//                                  ),
//                                ),
//                                SizedBox(width: 8),
//                                Expanded(
//                                  flex: 3,
//                                  child: TextFormField(
//                                    autovalidate:
//                                    formMCompanyAddPemegangSahamPribadiAlamatNotif.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    controller: formMCompanyAddPemegangSahamPribadiAlamatNotif
//                                        .controllerPostalCode,
//                                    // enabled: formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfPostalCode,
//                                    style: new TextStyle(color: Colors.black),
//                                    decoration: new InputDecoration(
//                                        labelText: 'Kode Pos',
//                                        labelStyle:
//                                            TextStyle(color: Colors.black),
//                                        border: OutlineInputBorder(
//                                            borderRadius:
//                                                BorderRadius.circular(8)),
//                                        filled: !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfPostalCode,
//                                        fillColor:
//                                        !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfPostalCode
//                                            ? Colors.black12
//                                            : Colors.white,),
//                                    enabled: false,
//                                  ),
//                                ),
//                              ],
//                            ),
//                            SizedBox(height: MediaQuery.of(context).size.height / 47),
//                            Row(
//                              children: [
//                                Expanded(
//                                  flex: 4,
//                                  child: TextFormField(
//                                    autovalidate:
//                                    formMCompanyAddPemegangSahamPribadiAlamatNotif.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    onChanged: (e) {
//                                      formMCompanyAddPemegangSahamPribadiAlamatNotif
//                                          .checkValidCodeArea(e);
//                                    },
//                                    keyboardType: TextInputType.number,
//                                    inputFormatters: [
//                                      WhitelistingTextInputFormatter.digitsOnly,
//                                      // LengthLimitingTextInputFormatter(10),
//                                    ],
//                                    controller: formMCompanyAddPemegangSahamPribadiAlamatNotif
//                                        .controllerTeleponArea,
//                                    enabled:
//                                    formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfAreaCode,
//                                    style: new TextStyle(color: Colors.black),
//                                    decoration: new InputDecoration(
//                                        labelText: 'Telepon (Area)',
//                                        labelStyle:
//                                            TextStyle(color: Colors.black),
//                                        border: OutlineInputBorder(
//                                            borderRadius:
//                                                BorderRadius.circular(8)),
//                                        filled: !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfAreaCode,
//                                        fillColor:
//                                        !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfAreaCode
//                                            ? Colors.black12
//                                            : Colors.white,),
//                                  ),
//                                ),
//                                SizedBox(width: MediaQuery.of(context).size.width / 37),
//                                Expanded(
//                                  flex: 6,
//                                  child: TextFormField(
//                                    autovalidate:
//                                    formMCompanyAddPemegangSahamPribadiAlamatNotif.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    keyboardType: TextInputType.number,
//                                    inputFormatters: [
//                                      WhitelistingTextInputFormatter.digitsOnly,
//                                      // LengthLimitingTextInputFormatter(10),
//                                    ],
//                                    controller:
//                                    formMCompanyAddPemegangSahamPribadiAlamatNotif.controllerTelepon,
//                                    enabled:
//                                    formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfPhone,
//                                    style: new TextStyle(color: Colors.black),
//                                    decoration: new InputDecoration(
//                                        labelText: 'Telepon',
//                                        labelStyle:
//                                            TextStyle(color: Colors.black),
//                                        border: OutlineInputBorder(
//                                            borderRadius:
//                                                BorderRadius.circular(8)),
//                                      filled: !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfPhone,
//                                      fillColor:
//                                      !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfPhone
//                                          ? Colors.black12
//                                          : Colors.white,),
//                                  ),
//                                ),
//                              ],
//                            ),
//                          ],
//                        ),
//                      );
//                    },
//                  )
//                : FutureBuilder(
//                    future: _setValueForEdit,
//                    builder: (context, snapshot) {
//                      if (snapshot.connectionState == ConnectionState.waiting) {
//                        return Center(
//                          child: CircularProgressIndicator(),
//                        );
//                      }
//                      return Consumer<FormMCompanyAddPemegangSahamPribadiAlamatChangeNotifier>(
//                        builder: (context, formMCompanyAddPemegangSahamPribadiAlamatNotif, _) {
//                          return Form(
//                            key: formMCompanyAddPemegangSahamPribadiAlamatNotif.key,
//                            onWillPop: _onWillPop,
//                            child: Column(
//                              crossAxisAlignment: CrossAxisAlignment.start,
//                              children: [
//                                DropdownButtonFormField<JenisAlamatModel>(
//                                    autovalidate:
//                                    formMCompanyAddPemegangSahamPribadiAlamatNotif.autoValidate,
//                                    validator: (e) {
//                                      if (e == null) {
//                                        return "Silahkan pilih jenis alamat";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    value: formMCompanyAddPemegangSahamPribadiAlamatNotif.jenisAlamatSelected,
//                                    onChanged: (value) {
//                                      formMCompanyAddPemegangSahamPribadiAlamatNotif.jenisAlamatSelected = value;
//                                    },
//                                    onTap: () {
//                                      FocusManager.instance.primaryFocus.unfocus();
//                                    },
//                                    decoration: InputDecoration(
//                                      labelText: "Jenis Alamat",
//                                      border: OutlineInputBorder(),
//                                      contentPadding:
//                                          EdgeInsets.symmetric(horizontal: 10),
//                                    ),
//                                    items: formMCompanyAddPemegangSahamPribadiAlamatNotif
//                                        .listJenisAlamat
//                                        .map((value) {
//                                      return DropdownMenuItem<JenisAlamatModel>(
//                                        value: value,
//                                        child: Text(
//                                          value.DESKRIPSI,
//                                          overflow: TextOverflow.ellipsis,
//                                        ),
//                                      );
//                                    }).toList()),
//                                formMCompanyAddPemegangSahamPribadiAlamatNotif.jenisAlamatSelected != null
//                                  ? formMCompanyAddPemegangSahamPribadiAlamatNotif.jenisAlamatSelected.KODE == "02"
//                                    ? Row(
//                                      children: [
//                                        Checkbox(
//                                            value: formMCompanyAddPemegangSahamPribadiAlamatNotif
//                                                .isSameWithIdentity,
//                                            onChanged: (value) {
//                                              formMCompanyAddPemegangSahamPribadiAlamatNotif
//                                                  .isSameWithIdentity = value;
//                                              formMCompanyAddPemegangSahamPribadiAlamatNotif
//                                                  .setValueIsSameWithIdentity(
//                                                  value, context, null);
//                                            },
//                                            activeColor: myPrimaryColor),
//                                        SizedBox(width: 8),
//                                        Text("Sama dengan identitas")
//                                      ],
//                                    )
//                                  : SizedBox(height: MediaQuery.of(context).size.height / 47)
//                                : SizedBox(height: MediaQuery.of(context).size.height / 47),
//                                TextFormField(
//                                  autovalidate: formMCompanyAddPemegangSahamPribadiAlamatNotif.autoValidate,
//                                  validator: (e) {
//                                    if (e.isEmpty) {
//                                      return "Tidak boleh kosong";
//                                    } else {
//                                      return null;
//                                    }
//                                  },
//                                  controller:
//                                  formMCompanyAddPemegangSahamPribadiAlamatNotif.controllerAlamat,
//                                  enabled:
//                                  formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfAddress,
//                                  style: TextStyle(color: Colors.black),
//                                  decoration: InputDecoration(
//                                    labelText: 'Alamat',
//                                    labelStyle: TextStyle(color: Colors.black),
//                                    border: OutlineInputBorder(
//                                        borderRadius: BorderRadius.circular(8)),
//                                    filled: !formMCompanyAddPemegangSahamPribadiAlamatNotif
//                                        .enableTfAddress,
//                                    fillColor:
//                                    !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfAddress
//                                        ? Colors.black12
//                                        : Colors.white,),
//                                  maxLines: 3,
//                                  keyboardType: TextInputType.text,
//                                  textCapitalization: TextCapitalization.characters,
//                                ),
//                                SizedBox(height: MediaQuery.of(context).size.height / 47),
//                                Row(
//                                  children: [
//                                    Expanded(
//                                      flex: 5,
//                                      child: TextFormField(
//                                        autovalidate:
//                                        formMCompanyAddPemegangSahamPribadiAlamatNotif.autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        inputFormatters: [
//                                          WhitelistingTextInputFormatter.digitsOnly,
//                                          // LengthLimitingTextInputFormatter(10),
//                                        ],
//                                        controller:
//                                        formMCompanyAddPemegangSahamPribadiAlamatNotif.controllerRT,
//                                        enabled:
//                                        formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfRT,
//                                        style: TextStyle(color: Colors.black),
//                                        decoration: InputDecoration(
//                                          labelText: 'RT',
//                                          labelStyle:
//                                          TextStyle(color: Colors.black),
//                                          border: OutlineInputBorder(
//                                              borderRadius:
//                                              BorderRadius.circular(8)),
//                                          filled: !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfRT,
//                                          fillColor:
//                                          !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfRT
//                                              ? Colors.black12
//                                              : Colors.white,),
//                                        keyboardType: TextInputType.number,
//                                      ),
//                                    ),
//                                    SizedBox(width: 8),
//                                    Expanded(
//                                      flex: 5,
//                                      child: TextFormField(
//                                        autovalidate:
//                                        formMCompanyAddPemegangSahamPribadiAlamatNotif.autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        inputFormatters: [
//                                          WhitelistingTextInputFormatter.digitsOnly,
//                                          // LengthLimitingTextInputFormatter(10),
//                                        ],
//                                        controller:
//                                        formMCompanyAddPemegangSahamPribadiAlamatNotif.controllerRW,
//                                        enabled:
//                                        formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfRW,
//                                        style: TextStyle(color: Colors.black),
//                                        decoration: InputDecoration(
//                                          labelText: 'RW',
//                                          labelStyle:
//                                          TextStyle(color: Colors.black),
//                                          border: OutlineInputBorder(
//                                              borderRadius:
//                                              BorderRadius.circular(8)),
//                                          filled: !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfRW,
//                                          fillColor:
//                                          !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfRW
//                                              ? Colors.black12
//                                              : Colors.white,),
//                                        keyboardType: TextInputType.number,
//                                      ),
//                                    )
//                                  ],
//                                ),
//                                SizedBox(height: MediaQuery.of(context).size.height / 47),
//                                FocusScope(
//                                  node: FocusScopeNode(),
//                                  child: TextFormField(
//                                    autovalidate:
//                                    formMCompanyAddPemegangSahamPribadiAlamatNotif.autoValidate,
//                                    validator: (e) {
//                                      if (e.isEmpty) {
//                                        return "Tidak boleh kosong";
//                                      } else {
//                                        return null;
//                                      }
//                                    },
//                                    onTap: () {
//                                      FocusManager.instance.primaryFocus.unfocus();
//                                      formMCompanyAddPemegangSahamPribadiAlamatNotif
//                                          .searchKelurahan(context);
//                                    },
//                                    controller:
//                                    formMCompanyAddPemegangSahamPribadiAlamatNotif.controllerKelurahan,
//                                    enabled:
//                                    formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfKelurahan,
//                                    style: new TextStyle(color: Colors.black),
//                                    decoration: new InputDecoration(
//                                      labelText: 'Kelurahan',
//                                      labelStyle: TextStyle(color: Colors.black),
//                                      border: OutlineInputBorder(
//                                          borderRadius:
//                                          BorderRadius.circular(8)),
//                                      filled: !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfKelurahan,
//                                      fillColor:
//                                      !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfKelurahan
//                                          ? Colors.black12
//                                          : Colors.white,),
//                                  ),
//                                ),
//                                SizedBox(height: MediaQuery.of(context).size.height / 47),
//                                TextFormField(
//                                  autovalidate: formMCompanyAddPemegangSahamPribadiAlamatNotif.autoValidate,
//                                  validator: (e) {
//                                    if (e.isEmpty) {
//                                      return "Tidak boleh kosong";
//                                    } else {
//                                      return null;
//                                    }
//                                  },
//                                  controller:
//                                  formMCompanyAddPemegangSahamPribadiAlamatNotif.controllerKecamatan,
//                                  // enabled: formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfKecamatan,
//                                  style: new TextStyle(color: Colors.black),
//                                  decoration: new InputDecoration(
//                                    labelText: 'Kecamatan',
//                                    labelStyle: TextStyle(color: Colors.black),
//                                    border: OutlineInputBorder(
//                                        borderRadius: BorderRadius.circular(8)),
//                                    filled: !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfKecamatan,
//                                    fillColor:
//                                    !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfKecamatan
//                                        ? Colors.black12
//                                        : Colors.white,),
//                                  enabled: false,
//                                ),
//                                SizedBox(height: MediaQuery.of(context).size.height / 47),
//                                TextFormField(
//                                  autovalidate: formMCompanyAddPemegangSahamPribadiAlamatNotif.autoValidate,
//                                  validator: (e) {
//                                    if (e.isEmpty) {
//                                      return "Tidak boleh kosong";
//                                    } else {
//                                      return null;
//                                    }
//                                  },
//                                  controller: formMCompanyAddPemegangSahamPribadiAlamatNotif.controllerKota,
//                                  // enabled: formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfKota,
//                                  style: new TextStyle(color: Colors.black),
//                                  decoration: new InputDecoration(
//                                    labelText: 'Kabupaten/Kota',
//                                    labelStyle: TextStyle(color: Colors.black),
//                                    border: OutlineInputBorder(
//                                        borderRadius: BorderRadius.circular(8)),
//                                    filled: !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfKota,
//                                    fillColor:
//                                    !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfKota
//                                        ? Colors.black12
//                                        : Colors.white,),
//                                  enabled: false,
//                                ),
//                                SizedBox(height: MediaQuery.of(context).size.height / 47),
//                                Row(
//                                  children: [
//                                    Expanded(
//                                      flex: 7,
//                                      child: TextFormField(
//                                        autovalidate:
//                                        formMCompanyAddPemegangSahamPribadiAlamatNotif.autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        controller: formMCompanyAddPemegangSahamPribadiAlamatNotif
//                                            .controllerProvinsi,
//                                        // enabled: formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfProv,
//                                        style: new TextStyle(color: Colors.black),
//                                        decoration: new InputDecoration(
//                                          labelText: 'Provinsi',
//                                          labelStyle:
//                                          TextStyle(color: Colors.black),
//                                          border: OutlineInputBorder(
//                                              borderRadius:
//                                              BorderRadius.circular(8)),
//                                          filled: !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfProv,
//                                          fillColor:
//                                          !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfProv
//                                              ? Colors.black12
//                                              : Colors.white,),
//                                        enabled: false,
//                                      ),
//                                    ),
//                                    SizedBox(width: 8),
//                                    Expanded(
//                                      flex: 3,
//                                      child: TextFormField(
//                                        autovalidate:
//                                        formMCompanyAddPemegangSahamPribadiAlamatNotif.autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        controller: formMCompanyAddPemegangSahamPribadiAlamatNotif
//                                            .controllerPostalCode,
//                                        // enabled: formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfPostalCode,
//                                        style: new TextStyle(color: Colors.black),
//                                        decoration: new InputDecoration(
//                                          labelText: 'Kode Pos',
//                                          labelStyle:
//                                          TextStyle(color: Colors.black),
//                                          border: OutlineInputBorder(
//                                              borderRadius:
//                                              BorderRadius.circular(8)),
//                                          filled: !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfPostalCode,
//                                          fillColor:
//                                          !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfPostalCode
//                                              ? Colors.black12
//                                              : Colors.white,),
//                                        enabled: false,
//                                      ),
//                                    ),
//                                  ],
//                                ),
//                                SizedBox(height: MediaQuery.of(context).size.height / 47),
//                                Row(
//                                  children: [
//                                    Expanded(
//                                      flex: 4,
//                                      child: TextFormField(
//                                        autovalidate:
//                                        formMCompanyAddPemegangSahamPribadiAlamatNotif.autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        onChanged: (e) {
//                                          formMCompanyAddPemegangSahamPribadiAlamatNotif
//                                              .checkValidCodeArea(e);
//                                        },
//                                        keyboardType: TextInputType.number,
//                                        inputFormatters: [
//                                          WhitelistingTextInputFormatter.digitsOnly,
//                                          // LengthLimitingTextInputFormatter(10),
//                                        ],
//                                        controller: formMCompanyAddPemegangSahamPribadiAlamatNotif
//                                            .controllerTeleponArea,
//                                        enabled:
//                                        formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfAreaCode,
//                                        style: new TextStyle(color: Colors.black),
//                                        decoration: new InputDecoration(
//                                          labelText: 'Telepon (Area)',
//                                          labelStyle:
//                                          TextStyle(color: Colors.black),
//                                          border: OutlineInputBorder(
//                                              borderRadius:
//                                              BorderRadius.circular(8)),
//                                          filled: !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfAreaCode,
//                                          fillColor:
//                                          !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfAreaCode
//                                              ? Colors.black12
//                                              : Colors.white,),
//                                      ),
//                                    ),
//                                    SizedBox(width: MediaQuery.of(context).size.width / 37),
//                                    Expanded(
//                                      flex: 6,
//                                      child: TextFormField(
//                                        autovalidate:
//                                        formMCompanyAddPemegangSahamPribadiAlamatNotif.autoValidate,
//                                        validator: (e) {
//                                          if (e.isEmpty) {
//                                            return "Tidak boleh kosong";
//                                          } else {
//                                            return null;
//                                          }
//                                        },
//                                        keyboardType: TextInputType.number,
//                                        inputFormatters: [
//                                          WhitelistingTextInputFormatter.digitsOnly,
//                                          // LengthLimitingTextInputFormatter(10),
//                                        ],
//                                        controller:
//                                        formMCompanyAddPemegangSahamPribadiAlamatNotif.controllerTelepon,
//                                        enabled:
//                                        formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfPhone,
//                                        style: new TextStyle(color: Colors.black),
//                                        decoration: new InputDecoration(
//                                          labelText: 'Telepon',
//                                          labelStyle:
//                                          TextStyle(color: Colors.black),
//                                          border: OutlineInputBorder(
//                                              borderRadius:
//                                              BorderRadius.circular(8)),
//                                          filled: !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfPhone,
//                                          fillColor:
//                                          !formMCompanyAddPemegangSahamPribadiAlamatNotif.enableTfPhone
//                                              ? Colors.black12
//                                              : Colors.white,),
//                                      ),
//                                    ),
//                                  ],
//                                ),
//                              ],
//                            ),
//                          );
//                        },
//                      );
//                    })),
//        bottomNavigationBar: BottomAppBar(
//          elevation: 0.0,
//          child: Padding(
//              padding: const EdgeInsets.all(8.0),
//              child: RaisedButton(
//                  shape: RoundedRectangleBorder(
//                      borderRadius: new BorderRadius.circular(8.0)),
//                  color: myPrimaryColor,
//                  onPressed: () {
//                    if (widget.flag == 0) {
//                      Provider.of<FormMCompanyAddPemegangSahamPribadiAlamatChangeNotifier>(
//                              context,
//                              listen: false)
//                          .check(context, widget.flag, null);
//                    } else {
//                      Provider.of<FormMCompanyAddPemegangSahamPribadiAlamatChangeNotifier>(
//                              context,
//                              listen: false)
//                          .check(context, widget.flag, widget.index);
//                    }
//                  },
//                  child: Row(
//                    mainAxisAlignment: MainAxisAlignment.center,
//                    children: <Widget>[
//                      Text(widget.flag == 0 ? "SAVE" : "UPDATE",
//                          style: TextStyle(
//                              color: Colors.black,
//                              fontSize: 14,
//                              fontWeight: FontWeight.w500,
//                              letterSpacing: 1.25))
//                    ],
//                  ))),
//        ),
//      ),
    );
  }

//  Future<bool> _onWillPop() async {
//    var _provider = Provider.of<FormMCompanyAddPemegangSahamPribadiAlamatChangeNotifier>(context,
//        listen: false);
//    if (widget.flag == 0) {
//      if (_provider.jenisAlamatSelected != null ||
//          _provider.controllerAlamat.text != "" ||
//          _provider.controllerRT.text != "" ||
//          _provider.controllerRW.text != "" ||
//          _provider.controllerKelurahan.text != "" ||
//          _provider.controllerKecamatan.text != "" ||
//          _provider.controllerKota.text != "" ||
//          _provider.controllerProvinsi.text != "" ||
//          _provider.controllerPostalCode.text != "" ||
//          _provider.controllerTeleponArea.text != "" ||
//          _provider.controllerTelepon.text != "") {
//        return (await showDialog(
//              context: context,
//              builder: (myContext) => AlertDialog(
//                title: new Text('Warning'),
//                content: new Text('Simpan perubahan?'),
//                actions: <Widget>[
//                  new FlatButton(
//                    onPressed: () {
//                      _provider.check(context, widget.flag, null);
//                      Navigator.pop(context);
//                    },
//                    child: new Text('Ya', style: TextStyle(fontSize: 12, color: Colors.grey),),
//                  ),
//                  new FlatButton(
//                    onPressed: () => Navigator.of(context).pop(true),
//                    child: new Text('Tidak'),
//                  ),
//                ],
//              ),
//            )) ??
//            false;
//      } else {
//        return true;
//      }
//    } else {
//      if (_provider.jenisAlamatSelectedTemp.KODE !=
//              _provider.jenisAlamatSelected.KODE ||
//          _provider.alamatTemp != _provider.controllerAlamat.text ||
//          _provider.rtTemp != _provider.controllerRT.text ||
//          _provider.rwTemp != _provider.controllerRW.text ||
//          _provider.kelurahanTemp != _provider.controllerKelurahan.text ||
//          _provider.kecamatanTemp != _provider.controllerKecamatan.text ||
//          _provider.kotaTemp != _provider.controllerKota.text ||
//          _provider.provinsiTemp != _provider.controllerProvinsi.text ||
//          _provider.postalCodeTemp != _provider.controllerPostalCode.text ||
//          _provider.teleponAreaTemp != _provider.controllerTeleponArea.text ||
//          _provider.teleponTemp != _provider.controllerTelepon.text) {
//        return (await showDialog(
//              context: context,
//              builder: (myContext) => AlertDialog(
//                title: new Text('Warning'),
//                content: new Text('Simpan perubahan?'),
//                actions: <Widget>[
//                  new FlatButton(
//                    onPressed: () {
//                      _provider.check(context, widget.flag, widget.index);
//                      Navigator.pop(context);
//                    },
//                    child: new Text('Ya', style: TextStyle(fontSize: 12, color: Colors.grey),),
//                  ),
//                  new FlatButton(
//                    onPressed: () => Navigator.of(context).pop(true),
//                    child: new Text('Tidak'),
//                  ),
//                ],
//              ),
//            )) ??
//            false;
//      } else {
//        return true;
//      }
//    }
//  }
}
