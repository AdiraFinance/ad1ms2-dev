import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/address_guarantor_model.dart';
import 'package:ad1ms2_dev/shared/form_m_company_guarantor_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'company_add_guarantor_company.dart';

//ga kepake
class ListCompanyGuarantorCompany extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans"),
      child: Scaffold(
        appBar: AppBar(
          title: Text("List Penjamin Kelembagaan",
              style: TextStyle(color: Colors.black)),
          centerTitle: true,
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: Consumer<FormMCompanyGuarantorChangeNotifier>(
          builder: (context, formMGuarantorChangeNotifier, _) {
            return ListView.builder(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 77,
                  horizontal: MediaQuery.of(context).size.width / 47),
              itemBuilder: (context, index) {
                return InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => CompanyAddGuarantorCompany(
                                flag: 1,
                                index: index,
                                model: formMGuarantorChangeNotifier
                                    .listGuarantorCompany[index])
//                              ChangeNotifierProvider(
//                              create: (context) =>
//                                  FormMAddAddressGuarantorIndividualChangeNotifier(),
//                              child: AddGuarantorAddressIndividual(
//                                  flag: 1,
//                                  index: index,
//                                  addressModel:
//                                  formMAddGuarantorChangeNotifier
//                                      .listGuarantorAddress[
//                                  index]))
                            ));
                  },
                  child: Card(
//                  shape: formMAddGuarantorChangeNotifier.selectedIndex ==
//                      index
//                      ? RoundedRectangleBorder(
//                      side:
//                      BorderSide(color: myPrimaryColor, width: 2),
//                      borderRadius:
//                      BorderRadius.all(Radius.circular(4)))
//                      : null,
                    elevation: 3.3,
                    child: Padding(
                        padding: const EdgeInsets.all(16),
                        child: Stack(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  children: [
                                    Text(formMGuarantorChangeNotifier.listGuarantorCompany[index].profilModel.text,
                                      style: TextStyle(fontWeight: FontWeight.bold),),
                                    Text(" ${formMGuarantorChangeNotifier.listGuarantorCompany[index].institutionName}",
                                      style: TextStyle(fontWeight: FontWeight.bold),
                                    ),
                                    Text(" (${formMGuarantorChangeNotifier.listGuarantorCompany[index].typeInstitutionModel.PARA_NAME})",
                                      style: TextStyle(color: Colors.grey, fontSize: 13),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height:
                                  MediaQuery.of(context).size.height /
                                      97,
                                ),
                                Text("${formMGuarantorChangeNotifier.listGuarantorCompany[index].npwp}",
                                  style: TextStyle(color: Colors.grey, fontSize: 13),
                                ),
                              ],
                            ),
                            Align(
                                alignment: Alignment.topRight,
                                child: GestureDetector(
                                    onTap: () {formMGuarantorChangeNotifier.deleteListGuarantorIndividual(context, index);},
                                    child: Icon(Icons.delete, color: Colors.red)
                                )
                              // IconButton(icon: Icon(Icons.delete, color: Colors.red),
                              //     onPressed: () {
                              //       // formMinfoAlamat.deleteAlamatKorespondensi(index);
                              //     })
                              //     : SizedBox(),
                            )
                          ],
                        )
                    ),
                    // child: Padding(
                    //   padding: const EdgeInsets.all(13.0),
                    //   child: Stack(
                    //     children: [
                    //       Column(
                    //         children: [
                    //           Row(
                    //             children: [
                    //               Expanded(
                    //                   flex: 4, child: Text("Nama Kelembagaan")),
                    //               Text(" : "),
                    //               Expanded(
                    //                   flex: 6,
                    //                   child: Text(formMGuarantorChangeNotifier
                    //                       .listGuarantorCompany[index]
                    //                       .institutionName))
                    //             ],
                    //           ),
                    //           Row(
                    //             children: [
                    //               Expanded(flex: 4, child: Text("Alamat")),
                    //               Text(" : "),
                    //               Expanded(
                    //                   flex: 6,
                    //                   child: _widget(
                    //                       formMGuarantorChangeNotifier
                    //                           .listGuarantorCompany[index]
                    //                           .listAddressGuarantorModel))
                    //             ],
                    //           ),
                    //         ],
                    //       ),
                    //       Align(
                    //           alignment: Alignment.topRight,
                    //           child: IconButton(
                    //               icon: Icon(Icons.delete, color: Colors.red),
                    //               onPressed: () {
                    //                 formMGuarantorChangeNotifier
                    //                     .deleteListGuarantorIndividual(context, index);
                    //               }))
                    //     ],
                    //   ),
                    // ),
                  ),
                );
              },
              itemCount:
                  formMGuarantorChangeNotifier.listGuarantorCompany.length,
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CompanyAddGuarantorCompany(
                            model: null,
                            index: null,
                            flag: 0,
                          )));
            },
            child: Icon(Icons.add),
            backgroundColor: myPrimaryColor),
      ),
    );
  }

  Widget _widget(List<AddressModelCompany> model) {
    String _data = "";
    for (int i = 0; i < model.length; i++) {
      if (model[i].isCorrespondence) {
        _data = model[i].address;
      }
    }
    return Text(_data);
  }
}
