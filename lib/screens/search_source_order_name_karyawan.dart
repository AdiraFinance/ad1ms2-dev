import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/search_source_order_name_karyawan_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchSourceOrderNameKaryawan extends StatefulWidget {
  @override
  _SearchSourceOrderNameKaryawanState createState() => _SearchSourceOrderNameKaryawanState();
}

class _SearchSourceOrderNameKaryawanState extends State<SearchSourceOrderNameKaryawan> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<SearchSourceOrderNameKaryawanChangeNotifier>(context,listen: false).scaffoldKey,
        appBar: AppBar(
          title: Consumer<SearchSourceOrderNameKaryawanChangeNotifier>(
            builder: (context, searchSourceOrderNameKaryawanChangeNotifier, _) {
              return TextFormField(
                controller: searchSourceOrderNameKaryawanChangeNotifier.controllerSearch,
                style: TextStyle(color: Colors.black),
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (query) {
                  if(query.length >= 3) {
                    searchSourceOrderNameKaryawanChangeNotifier.getSourceOrderNameKaryawan(query);
                  } else {
                    searchSourceOrderNameKaryawanChangeNotifier.showSnackBar("Input minimal 3 karakter");
                  }
                },
                onChanged: (e) {
                  searchSourceOrderNameKaryawanChangeNotifier.changeAction(e);
                },
                textCapitalization: TextCapitalization.characters,
                cursorColor: Colors.black,
                decoration: new InputDecoration(
                  hintText: "Cari Nama Sumber Order (minimal 3 karakter)",
                  hintStyle: TextStyle(color: Colors.black),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                ),
                autofocus: true,
              );
            },
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            Provider.of<SearchSourceOrderNameKaryawanChangeNotifier>(context,
                        listen: true)
                    .showClear
                ? IconButton(
                    icon: Icon(Icons.clear),
                    onPressed: () {
                      Provider.of<SearchSourceOrderNameKaryawanChangeNotifier>(context,
                          listen: false).clearSearchTemp();
                      Provider.of<SearchSourceOrderNameKaryawanChangeNotifier>(context,
                              listen: false)
                          .controllerSearch
                          .clear();
                      Provider.of<SearchSourceOrderNameKaryawanChangeNotifier>(context,
                              listen: false)
                          .changeAction(
                              Provider.of<SearchSourceOrderNameKaryawanChangeNotifier>(
                                      context,
                                      listen: false)
                                  .controllerSearch
                                  .text);
                    })
                : SizedBox(
                    width: 0.0,
                    height: 0.0,
                  )
          ],
        ),
        body: Consumer<SearchSourceOrderNameKaryawanChangeNotifier>(
          builder: (context, searchSourceOrderNameKaryawanChangeNotifier, _) {
            return searchSourceOrderNameKaryawanChangeNotifier.loadData
                ?
            Center(
              child: CircularProgressIndicator(),
            )
                :
            searchSourceOrderNameKaryawanChangeNotifier.listSourceOrderNameKaryawanTemp.isNotEmpty
                ?
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount:
              searchSourceOrderNameKaryawanChangeNotifier.listSourceOrderNameKaryawanTemp.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(
                        context,
                        searchSourceOrderNameKaryawanChangeNotifier
                            .listSourceOrderNameKaryawanTemp[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchSourceOrderNameKaryawanChangeNotifier.listSourceOrderNameKaryawanTemp[index].kode} - "
                              "${searchSourceOrderNameKaryawanChangeNotifier.listSourceOrderNameKaryawanTemp[index].deskripsi}",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            )
                :
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount:
                  searchSourceOrderNameKaryawanChangeNotifier.listSourceOrderNameKaryawan.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(
                        context,
                        searchSourceOrderNameKaryawanChangeNotifier
                            .listSourceOrderNameKaryawan[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchSourceOrderNameKaryawanChangeNotifier
                              .listSourceOrderNameKaryawan[index].kode} - "
                              "${searchSourceOrderNameKaryawanChangeNotifier
                              .listSourceOrderNameKaryawan[index].deskripsi}",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          },
        ),
      ),
    );
  }
}
