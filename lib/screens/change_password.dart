import 'package:ad1ms2_dev/shared/change_password_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../main.dart';

class ChangePassword extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text("Change Password", style: TextStyle(color: Colors.black)),
            centerTitle: true,
            backgroundColor: myPrimaryColor,
            iconTheme: IconThemeData(color: Colors.black),
        ),
        key: Provider.of<ChangePasswordChangeNotifier>(context,listen: false).scaffoldKey,
        body: Consumer<ChangePasswordChangeNotifier>(
            builder: (context, changePassword, _){
                return changePassword.changePasswordProcess
                    ?
                Center(child: CircularProgressIndicator())
                    :
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width/37,
                      vertical: MediaQuery.of(context).size.height/57,
                  ),
                  child: Form(
                      key: changePassword.key,
                      child: Column(
                          children: [
                              TextFormField(
                                  validator: (e) {
                                      if (e.isEmpty) {
                                          return "Tidak boleh kosong";
                                      } else {
                                          return null;
                                      }
                                  },
                                  obscureText: changePassword.secureText,
                                  controller: changePassword.controllerOldPassword,
                                  autovalidate: changePassword.autoValidate,
                                  decoration: new InputDecoration(
                                      labelText: 'Password Lama',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)
                                      ),
                                      suffixIcon: IconButton(
                                          icon: Icon(
                                              changePassword.secureText
                                                  ? Icons.visibility_off
                                                  : Icons.visibility,
                                              color: Colors.black,
                                          ),
                                          onPressed: changePassword.showHidePass
                                      ),
                                  )
                              ),
                              SizedBox(height: MediaQuery.of(context).size.height / 47),
                              TextFormField(
                                  validator: (e) {
                                      if (e.isEmpty) {
                                          return "Tidak boleh kosong";
                                      } else {
                                          return null;
                                      }
                                  },
                                  obscureText: changePassword.secureTextNewPass,
                                  controller: changePassword.controllerNewPassword,
                                  autovalidate: changePassword.autoValidate,
                                  decoration: new InputDecoration(
                                      labelText: 'Password Baru',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      suffixIcon: IconButton(
                                          icon: Icon(
                                              changePassword.secureTextNewPass
                                                  ? Icons.visibility_off
                                                  : Icons.visibility,
                                              color: Colors.black,
                                          ),
                                          onPressed: changePassword.showHidePassNewPass
                                      ),
                                  )
                              ),
                              SizedBox(height: MediaQuery.of(context).size.height / 47),
                              TextFormField(
                                  validator: (e) {
                                      if (e.isEmpty) {
                                          return "Tidak boleh kosong";
                                      } else {
                                          return null;
                                      }
                                  },
                                  obscureText: changePassword.secureTextConfirm,
                                  controller: changePassword.controllerConfirmNewPassword,
                                  autovalidate: changePassword.autoValidate,
                                  decoration: new InputDecoration(
                                      labelText: 'Confirm Password Baru',
                                      labelStyle: TextStyle(color: Colors.black),
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(8)),
                                      suffixIcon: IconButton(
                                          icon: Icon(
                                              changePassword.secureTextConfirm
                                                  ? Icons.visibility_off
                                                  : Icons.visibility,
                                              color: Colors.black,
                                          ),
                                          onPressed: changePassword.showHidePassConfirm
                                      ),
                                  )
                              ),
                          ],
                      ),
                  ),
                );
            },
        ),
        bottomNavigationBar: BottomAppBar(
            elevation: 0.0,
            child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Consumer<ChangePasswordChangeNotifier>(
                    builder: (context, changePassword, _) {
                        return RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(8.0)),
                            color: changePassword.changePasswordProcess
                                ?
                            Colors.black12
                                :
                            myPrimaryColor,
                            onPressed: () {
                                changePassword.changePasswordProcess
                                    ?
                                null
                                    :
                                changePassword.check(context);
                            },
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                    Text("SUBMIT",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500,
                                            letterSpacing: 1.25))
                                ],
                            )
                        );
                    },
                )),
        ),
    );
  }
}
