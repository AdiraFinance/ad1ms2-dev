import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/task_list/task_list.dart';
import 'package:ad1ms2_dev/shared/form_AOS_change_notifier/form_AOS_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FormAOS extends StatefulWidget {
  @override
  _FormAOSState createState() => _FormAOSState();
}

class _FormAOSState extends State<FormAOS> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "Form AOS",
            style: TextStyle(
                color: Colors.black
            ),
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(
              color: Colors.black
          ),
        ),
        body: Consumer<FormAOSChangeNotifier>(
          builder: (context, value, child) {
            return SingleChildScrollView(
              padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width / 27,
                  vertical: MediaQuery.of(context).size.height / 57
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextFormField(
//              autovalidate: value.autoValidate,
                    validator: (e) {
                      if (e.isEmpty) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
//              keyboardType: TextInputType.number,
//              inputFormatters: [
//                WhitelistingTextInputFormatter.digitsOnly,
//              ],
                    controller: value.controllerName,
                    style: new TextStyle(color: Colors.black),
                    enabled: false,
                    decoration: new InputDecoration(
                        labelText: 'Nama',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8))),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  TextFormField(
//              autovalidate: value.autoValidate,
                    validator: (e) {
                      if (e.isEmpty) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
//              keyboardType: TextInputType.number,
//              inputFormatters: [
//                WhitelistingTextInputFormatter.digitsOnly,
//              ],
                    controller: value.controllerAddress,
                    style: new TextStyle(color: Colors.black),
                    enabled: false,
                    decoration: new InputDecoration(
                        labelText: 'Alamat',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)
                        )
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  TextFormField(
//              autovalidate: value.autoValidate,
                    validator: (e) {
                      if (e.isEmpty) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
//              keyboardType: TextInputType.number,
//              inputFormatters: [
//                WhitelistingTextInputFormatter.digitsOnly,
//              ],
                    controller: value.controllerAppNumber,
                    style: new TextStyle(color: Colors.black),
                    enabled: false,
                    decoration: new InputDecoration(
                        labelText: 'No Aplikasi',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)
                        )
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  TextFormField(
//              autovalidate: value.autoValidate,
                    validator: (e) {
                      if (e.isEmpty) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
//              keyboardType: TextInputType.number,
//              inputFormatters: [
//                WhitelistingTextInputFormatter.digitsOnly,
//              ],
                    controller: value.controllerModelObject,
                    style: new TextStyle(color: Colors.black),
                    enabled: false,
                    decoration: new InputDecoration(
                        labelText: 'Model Objek',
                        labelStyle: TextStyle(color: Colors.black),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)
                        )
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height / 47),
                  Text("Approve", style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                      letterSpacing: 0.15),),
                  // SizedBox(height: MediaQuery.of(context).size.height / 47),
                  Row(
                    children: [
                      Row(
                        children: [
                          Radio(
                              activeColor: primaryOrange,
                              value: '01',
                              groupValue: value.radioValueApproved,
                              onChanged: (data) {
                                value.radioValueApproved = data;
                              }),
                          Text("Ya")
                        ],
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      Row(
                        children: [
                          Radio(
                              activeColor: primaryOrange,
                              value: '02',
                              groupValue: value.radioValueApproved,
                              onChanged: (data) {
                                value.radioValueApproved = data;
                              }),
                          Text("Tidak")
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            );
          },
        ),
        bottomNavigationBar: BottomAppBar(
          elevation: 0.0,
          child: Container(
            padding: const EdgeInsets.all(8.0),
            child: RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(8.0)),
                color: myPrimaryColor,
                onPressed: () {
                  Provider.of<FormAOSChangeNotifier>(context, listen: false).generateAplikasiPayung(context);
                  // Navigator.push(context, MaterialPageRoute(builder: (context) => TaskList()));
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("SUBMIT",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 1.25))
                  ],
                )),
          ),
        ),
      ),
    );
  }
}
