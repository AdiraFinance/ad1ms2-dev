import 'package:ad1ms2_dev/shared/search_company_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_employee_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_karoseri_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../main.dart';

class SearchKaroseri extends StatefulWidget {
    final int radioPKSKaroseri;
    final String company;
    const SearchKaroseri({this.radioPKSKaroseri, this.company});

    @override
    _SearchKaroseriState createState() => _SearchKaroseriState();
}

class _SearchKaroseriState extends State<SearchKaroseri> {

    @override
  void initState() {
    super.initState();
    Provider.of<SearchKaroseriChangeNotifier>(context,listen: false).getKaroseri(context, widget.radioPKSKaroseri, widget.company);
  }
    @override
    Widget build(BuildContext context) {
        return Theme(
            data: ThemeData(
                primaryColor: Colors.black,
                fontFamily: "NunitoSans",
                accentColor: myPrimaryColor,
                primarySwatch: primaryOrange
            ),
          child: Scaffold(
              key: Provider.of<SearchKaroseriChangeNotifier>(context,listen: false).scaffoldKey,
              appBar: AppBar(
                  title: Consumer<SearchKaroseriChangeNotifier>(
                      builder: (context, searchKaroseriChangeNotifier, _) {
                          return TextFormField(
                              controller: searchKaroseriChangeNotifier.controllerSearch,
                              style: TextStyle(color: Colors.black),
                              textInputAction: TextInputAction.search,
                              onFieldSubmitted: (query) {
                                  searchKaroseriChangeNotifier.searchKaroseri(query);
                              },
                              onChanged: (e) {
                                  searchKaroseriChangeNotifier.changeAction(e);
                              },
                              cursorColor: Colors.black,
                              decoration: new InputDecoration(
                                  hintText: "Cari Karoseri (minimal 3 karakter)",
                                  hintStyle: TextStyle(color: Colors.black),
                                  enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: myPrimaryColor),
                                  ),
                                  focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: myPrimaryColor),
                                  ),
                              ),
                              autofocus: true,
                          );
                      },
                  ),
                  backgroundColor: myPrimaryColor,
                  iconTheme: IconThemeData(color: Colors.black),
                  actions: <Widget>[
                      Provider.of<SearchKaroseriChangeNotifier>(context, listen: true)
                          .showClear
                          ? IconButton(
                          icon: Icon(Icons.clear),
                          onPressed: () {
                              Provider.of<SearchKaroseriChangeNotifier>(context,
                                  listen: false).clearSearchTemp();
                              Provider.of<SearchKaroseriChangeNotifier>(context,
                                  listen: false)
                                  .controllerSearch
                                  .clear();
                              Provider.of<SearchKaroseriChangeNotifier>(context,
                                  listen: false)
                                  .changeAction(Provider.of<SearchKaroseriChangeNotifier>(
                                  context,
                                  listen: false)
                                  .controllerSearch
                                  .text);
                          })
                          : SizedBox(
                          width: 0.0,
                          height: 0.0,
                      )
                  ],
              ),
              body: Consumer<SearchKaroseriChangeNotifier>(
                  builder: (context, searchKaroseriChangeNotifier, _) {
                      return searchKaroseriChangeNotifier.loadData
                          ?
                      Center(child: CircularProgressIndicator())
                          :
                      searchKaroseriChangeNotifier.listKaroseriTemp.isNotEmpty
                          ?
                      ListView.separated(
                          padding: EdgeInsets.symmetric(
                              vertical: MediaQuery.of(context).size.height / 57,
                              horizontal: MediaQuery.of(context).size.width / 27),
                          itemCount: searchKaroseriChangeNotifier.listKaroseriTemp.length,
                          itemBuilder: (listContext, index) {
                              return InkWell(
                                  onTap: () {
                                      Navigator.pop(context,
                                          searchKaroseriChangeNotifier.listKaroseriTemp[index]);
                                  },
                                  child: Container(
                                      child: Row(
                                          mainAxisSize: MainAxisSize.max,
                                          children: [
                                              Text(
                                                  "${searchKaroseriChangeNotifier.listKaroseriTemp[index].kode} - "
                                                      "${searchKaroseriChangeNotifier.listKaroseriTemp[index].deskripsi} ",
                                                  style: TextStyle(fontSize: 16),
                                              )
                                          ],
                                      ),
                                  ),
                              );
                          },
                          separatorBuilder: (context, index) {
                              return Divider();
                          },
                      )
                          :
                      ListView.separated(
                          padding: EdgeInsets.symmetric(
                              vertical: MediaQuery.of(context).size.height / 57,
                              horizontal: MediaQuery.of(context).size.width / 27),
                          itemCount: searchKaroseriChangeNotifier.listKaroseriModel.length,
                          itemBuilder: (listContext, index) {
                              return InkWell(
                                  onTap: () {
                                      Navigator.pop(context,
                                          searchKaroseriChangeNotifier.listKaroseriModel[index]);
                                  },
                                  child: Container(
                                      child: Row(
                                          mainAxisSize: MainAxisSize.max,
                                          children: [
                                              Text(
                                                  "${searchKaroseriChangeNotifier.listKaroseriModel[index].kode} - "
                                                      "${searchKaroseriChangeNotifier.listKaroseriModel[index].deskripsi} ",
                                                  style: TextStyle(fontSize: 16),
                                              )
                                          ],
                                      ),
                                  ),
                              );
                          },
                          separatorBuilder: (context, index) {
                              return Divider();
                          },
                      );
                  },
              ),
          ),
        );
    }
}
