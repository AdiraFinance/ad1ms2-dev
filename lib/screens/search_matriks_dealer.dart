import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/search_matriks_dealer_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_third_party_type_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchMatriksDealer extends StatefulWidget {
  @override
  _SearchMatriksDealerState createState() => _SearchMatriksDealerState();
}

class _SearchMatriksDealerState extends State<SearchMatriksDealer> {

  @override
  void initState() {
    super.initState();
    Provider.of<SearchMatriksDealerChangeNotifier>(context,listen: false).getMatriksDealer(context);
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<SearchMatriksDealerChangeNotifier>(context,listen: false).scaffoldKey,
        appBar: AppBar(
          title: Consumer<SearchMatriksDealerChangeNotifier>(
            builder: (context, searchMatriksDealerChangeNotifier, _) {
              return TextFormField(
                controller: searchMatriksDealerChangeNotifier.controllerSearch,
                style: TextStyle(color: Colors.black),
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (query) {
                  searchMatriksDealerChangeNotifier.searchMatriksDealer(query);
                },
                onChanged: (e) {
                  searchMatriksDealerChangeNotifier.changeAction(e);
                },
                cursorColor: Colors.black,
                decoration: new InputDecoration(
                  hintText: "Cari Matriks Dealer (minimal 3 karakter)",
                  hintStyle: TextStyle(color: Colors.black),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                ),
                autofocus: true,
                textCapitalization: TextCapitalization.characters,
              );
            },
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            Provider.of<SearchMatriksDealerChangeNotifier>(context, listen: true)
                .showClear
                ? IconButton(
                icon: Icon(Icons.clear),
                onPressed: () {
                  Provider.of<SearchMatriksDealerChangeNotifier>(context,
                      listen: false)
                      .controllerSearch
                      .clear();
                  Provider.of<SearchMatriksDealerChangeNotifier>(context,
                      listen: false)
                      .changeAction(
                      Provider.of<SearchMatriksDealerChangeNotifier>(
                          context,
                          listen: false)
                          .controllerSearch
                          .text);
                })
                : SizedBox(
              width: 0.0,
              height: 0.0,
            )
          ],
        ),
        body: Consumer<SearchMatriksDealerChangeNotifier>(
          builder: (context, searchMatriksDealerChangeNotifier, _) {
            return searchMatriksDealerChangeNotifier.loadData
                ?
            Center(
              child: CircularProgressIndicator(),
            )
                :
            searchMatriksDealerChangeNotifier.listMatriksDealerTemp.isNotEmpty
                ?
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount:
              searchMatriksDealerChangeNotifier.listMatriksDealerTemp.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(
                        context,
                        searchMatriksDealerChangeNotifier.listMatriksDealerTemp[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchMatriksDealerChangeNotifier.listMatriksDealerTemp[index].kode} - "
                              "${searchMatriksDealerChangeNotifier.listMatriksDealerTemp[index].deskripsi} ",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            )
                :
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount:
              searchMatriksDealerChangeNotifier.listMatriksDealer.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(
                        context,
                        searchMatriksDealerChangeNotifier
                            .listMatriksDealer[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchMatriksDealerChangeNotifier.listMatriksDealer[index].kode} - "
                              "${searchMatriksDealerChangeNotifier.listMatriksDealer[index].deskripsi} ",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          },
        ),
      ),
    );
  }
}
