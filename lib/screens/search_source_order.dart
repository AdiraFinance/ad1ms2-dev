import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/search_source_order_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchSourceOrder extends StatefulWidget {
  @override
  _SearchSourceOrderState createState() => _SearchSourceOrderState();
}

class _SearchSourceOrderState extends State<SearchSourceOrder> {
  @override
  void initState() {
    super.initState();
    Provider.of<SearchSourceOrderChangeNotifier>(context,listen: false).getSourceOrder(context);
  }
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<SearchSourceOrderChangeNotifier>(context,listen: false).scaffoldKey,
        appBar: AppBar(
          title: Consumer<SearchSourceOrderChangeNotifier>(
            builder: (context, searchSourceOrderChangeNotifier, _) {
              return TextFormField(
                controller: searchSourceOrderChangeNotifier.controllerSearch,
                style: TextStyle(color: Colors.black),
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (query) {
                  searchSourceOrderChangeNotifier.searchSourceOrder(query);
              },
              onChanged: (e) {
                searchSourceOrderChangeNotifier.changeAction(e);
              },
              textCapitalization: TextCapitalization.characters,
              cursorColor: Colors.black,
              decoration: new InputDecoration(
                hintText: "Cari Sumber Order (minimal 3 karakter)",
                hintStyle: TextStyle(color: Colors.black),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: myPrimaryColor),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: myPrimaryColor),
                ),
              ),
              autofocus: true,
            );
          },
        ),
        backgroundColor: myPrimaryColor,
        iconTheme: IconThemeData(color: Colors.black),
        actions: <Widget>[
          Provider.of<SearchSourceOrderChangeNotifier>(context, listen: true)
                  .showClear
              ? IconButton(
                  icon: Icon(Icons.clear),
                  onPressed: () {
                    Provider.of<SearchSourceOrderChangeNotifier>(context,
                            listen: false)
                        .controllerSearch
                        .clear();
                    Provider.of<SearchSourceOrderChangeNotifier>(context,
                            listen: false)
                        .changeAction(
                            Provider.of<SearchSourceOrderChangeNotifier>(
                                    context,
                                    listen: false)
                                .controllerSearch
                                .text);
                  })
              : SizedBox(
                  width: 0.0,
                  height: 0.0,
                )
        ],
      ),
      body: Consumer<SearchSourceOrderChangeNotifier>(
        builder: (context, searchSourceOrderChangeNotifier, _) {
          return searchSourceOrderChangeNotifier.loadData
              ?
          Center(
            child: CircularProgressIndicator(),
          )
              :
          searchSourceOrderChangeNotifier.listSourceOrderTemp.isNotEmpty
              ?
          ListView.separated(
                padding: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height / 57,
                    horizontal: MediaQuery.of(context).size.width / 27),
                itemCount: searchSourceOrderChangeNotifier.listSourceOrderTemp.length,
                itemBuilder: (listContext, index) {
                  return InkWell(
                    onTap: () {
                      Navigator.pop(context,
                          searchSourceOrderChangeNotifier.listSourceOrderTemp[index]);
                    },
                    child: Container(
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Text(
                            "${searchSourceOrderChangeNotifier.listSourceOrderTemp[index].kode} - "
                                "${searchSourceOrderChangeNotifier.listSourceOrderTemp[index].deskripsi} ",
                            style: TextStyle(fontSize: 16),
                          )
                        ],
                      ),
                    ),
                  );
                },
                separatorBuilder: (context, index) {
                  return Divider();
                },
              )
                  :
          ListView.separated(
            padding: EdgeInsets.symmetric(
                vertical: MediaQuery.of(context).size.height / 57,
                horizontal: MediaQuery.of(context).size.width / 27),
            itemCount: searchSourceOrderChangeNotifier.listSourceOrder.length,
            itemBuilder: (listContext, index) {
              return InkWell(
                onTap: () {
                  Navigator.pop(context,
                      searchSourceOrderChangeNotifier.listSourceOrder[index]);
                },
                child: Container(
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Text(
                        "${searchSourceOrderChangeNotifier.listSourceOrder[index].kode} - "
                        "${searchSourceOrderChangeNotifier.listSourceOrder[index].deskripsi} ",
                        style: TextStyle(fontSize: 16),
                      )
                    ],
                  ),
                ),
              );
            },
            separatorBuilder: (context, index) {
              return Divider();
            },
          );
        },
      ))
    );
  }
}
