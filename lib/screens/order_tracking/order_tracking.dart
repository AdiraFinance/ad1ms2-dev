import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/list_order_tracking.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/date_picker.dart';
import 'package:ad1ms2_dev/shared/responsive_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OrderTracking extends StatefulWidget {
  @override
  _FormDedupIndividuState createState() => _FormDedupIndividuState();
}

class _FormDedupIndividuState extends State<OrderTracking> {
  Screen _size;

  final _controllerStartDate = TextEditingController();
  final _controllerEndDate = TextEditingController();

  DateTime _startDate;
  DateTime _endDate;
  final _key = new GlobalKey<FormState>();
  bool _autoValidate = false;
  bool _loadData = false;

  @override
  void initState() {
    super.initState();
    _startDate = DateTime(dateNow.year, dateNow.month, dateNow.day);
    _endDate = DateTime(dateNow.year, dateNow.month, dateNow.day);
  }

  @override
  Widget build(BuildContext context) {
    _size = Screen(MediaQuery.of(context).size);
    return Theme(
      data: ThemeData(
          fontFamily: "NunitoSans",
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          primarySwatch: primaryOrange),
      child: Scaffold(
        // appBar: AppBar(
        //   backgroundColor: myPrimaryColor,
        //   //centerTitle: true,
        //   //title: Text("Order Tracking", style: TextStyle(color: Colors.black)),
        //   //iconTheme: IconThemeData(color: Colors.black),
        // ),
        body: _loadData
            ? Center(child: CircularProgressIndicator())
            : Form(
                key: _key,
                child: SingleChildScrollView(
                  padding: EdgeInsets.symmetric(
                      vertical: _size.hp(2.5), horizontal: _size.wp(3)),
                  child: Column(
                    children: <Widget>[
                      TextFormField(
                        autovalidate: _autoValidate,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        controller: _controllerStartDate,
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            labelText: 'Start Date',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))),
                        onTap: () {
                          _selectStartDate(context);
                        },
                        readOnly: true,
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      TextFormField(
                        autovalidate: _autoValidate,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Tidak boleh kosong";
                          } else {
                            return null;
                          }
                        },
                        controller: _controllerEndDate,
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            labelText: 'End Date',
                            labelStyle: TextStyle(color: Colors.black),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))),
                        onTap: () {
                          // _showDatePicker();
                          _selectEndDate(context);
                        },
                        readOnly: true,
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height / 47),
                      RaisedButton(
                          onPressed: () {
                            // _insertDataDedup();
                            _check();
                          },
                          padding:
                              EdgeInsets.symmetric(vertical: _size.hp(1.5)),
                          color: myPrimaryColor,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8)),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  "CARI",
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                      letterSpacing: 1.25),
                                )
                              ]))
                    ],
                  ),
                ),
              ),
      ),
    );
  }


  void _selectStartDate(BuildContext context) async {
    var _datePickerSelected = await selectDate(context, this._startDate);
    if (_datePickerSelected != null) {
      setState(() {
        this._controllerStartDate.text = dateFormat.format(_datePickerSelected);
        this._startDate = _datePickerSelected;
      });
    } else {
      return;
    }
  }

  void _selectEndDate(BuildContext context) async {
    var _datePickerSelected = await selectDate(context, this._endDate);
    if (_datePickerSelected != null) {
      setState(() {
        this._controllerEndDate.text = dateFormat.format(_datePickerSelected);
        this._endDate = _datePickerSelected;
      });
    } else {
      return;
    }
  }

  _showDatePicker() async {
    DatePickerShared _datePicker = DatePickerShared();
    var _startDateSelected = await _datePicker.selectStartDate(context, _startDate, canAccessNextDay: false);
    var _endDateSelected = await _datePicker.selectStartDate(context, _endDate, canAccessNextDay: false);
    if (_startDateSelected != null) {
      setState(() {
        _startDate = _startDateSelected;
        _controllerStartDate.text = dateFormat.format(_startDateSelected);
      });
    } else {
      return;
    }

    if (_endDateSelected != null) {
      setState(() {
        _endDate = _endDateSelected;
        _controllerEndDate.text = dateFormat.format(_endDateSelected);
      });
    } else {
      return;
    }
  }

  _check() {
    final _form = _key.currentState;
    if (_form.validate()) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ListOrderTracking(
                    startDate: _controllerStartDate.text,
                    endDate: _controllerEndDate.text,
                  )));
    } else {
      setState(() {
        _autoValidate = true;
      });
    }
  }
}
