import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/search_brand_object_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchBrandObject extends StatefulWidget {
  final int flagByBrandModelType;
  final String flag;
  final String kodeGroupObject;
  final String kodeObject;
  final String prodMatrix;

  const SearchBrandObject({this.flagByBrandModelType, this.flag, this.kodeGroupObject, this.kodeObject, this.prodMatrix});
  @override
  _SearchBrandObjectState createState() => _SearchBrandObjectState();
}

class _SearchBrandObjectState extends State<SearchBrandObject> {
  @override
  void initState() {
    super.initState();
    // Provider.of<SearchBrandObjectChangeNotifier>(context, listen: false)
    //     .addDataBrandObject(widget.id);
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<SearchBrandObjectChangeNotifier>(context, listen: false).scaffoldKey,
        appBar: AppBar(
          title: Consumer<SearchBrandObjectChangeNotifier>(
            builder: (context, searchModelObjectChangeNotifier, _) {
              return TextFormField(
                controller: searchModelObjectChangeNotifier.controllerSearch,
                style: TextStyle(color: Colors.black),
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (query) {
                  searchModelObjectChangeNotifier.searchBrandObject(context, widget.flagByBrandModelType, widget.flag, query, widget.kodeGroupObject, widget.kodeObject, widget.prodMatrix);
                },
                onChanged: (e) {
                  searchModelObjectChangeNotifier.changeAction(e);
                },
                textCapitalization: TextCapitalization.characters,
                cursorColor: Colors.black,
                decoration: new InputDecoration(
                  hintText: "Cari Merk Objek (minimal 3 karakter)",
                  hintStyle: TextStyle(color: Colors.black),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                ),
                autofocus: true,
              );
            },
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            Provider.of<SearchBrandObjectChangeNotifier>(context, listen: true)
                    .showClear
                ? IconButton(
                    icon: Icon(Icons.clear),
                    onPressed: () {
                      Provider.of<SearchBrandObjectChangeNotifier>(context,
                              listen: false)
                          .controllerSearch
                          .clear();
                      Provider.of<SearchBrandObjectChangeNotifier>(context,
                              listen: false)
                          .changeAction(
                              Provider.of<SearchBrandObjectChangeNotifier>(
                                      context,
                                      listen: false)
                                  .controllerSearch
                                  .text);
                    })
                : SizedBox(
                    width: 0.0,
                    height: 0.0,
                  )
          ],
        ),
        body: Consumer<SearchBrandObjectChangeNotifier>(
          builder: (context, searchModelObject, _) {
            return ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount: searchModelObject.listBrandTypeModelGenreModel.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(
                        context, searchModelObject.listBrandTypeModelGenreModel[index]);
                  },
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: MediaQuery.of(context).size.height / 57,
                        ),
                        Row(
                          children: [
                            Expanded(
                                child: Text(
                              "${searchModelObject.listBrandTypeModelGenreModel[index].brandObjectModel.id} - "
                              "${searchModelObject.listBrandTypeModelGenreModel[index].brandObjectModel.name}",
                              style: TextStyle(fontSize: 16),
                            ))
                          ],
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height / 57,
                        ),
                        Row(
                          children: [
                            Expanded(
                                child: Text(
                                  "${searchModelObject.listBrandTypeModelGenreModel[index].objectTypeModel.id} - "
                                      "${searchModelObject.listBrandTypeModelGenreModel[index].objectTypeModel.name}",
                                  style: TextStyle(fontSize: 16),
                                ))
                          ],
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height / 57,
                        ),
                        Row(
                          children: [
                            Expanded(
                                child: Text(
                                  "${searchModelObject.listBrandTypeModelGenreModel[index].modelObjectModel.id} - "
                                      "${searchModelObject.listBrandTypeModelGenreModel[index].modelObjectModel.name}",
                                  style: TextStyle(fontSize: 16),
                                ))
                          ],
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height / 57,
                        ),
                        // Row(
                        //   children: [
                        //     Expanded(
                        //         child: Text(
                        //           "${searchModelObject.listBrandTypeModelGenreModel[index].objectUsageModel.id} - "
                        //               "${searchModelObject.listBrandTypeModelGenreModel[index].objectUsageModel.name}",
                        //           style: TextStyle(fontSize: 16),
                        //         ))
                        //   ],
                        // ),
                        // SizedBox(
                        //   height: MediaQuery.of(context).size.height / 57,
                        // ),
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          },
        ),
      ),
    );
  }
}
