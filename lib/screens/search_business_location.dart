import 'package:ad1ms2_dev/shared/search_business_location_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../main.dart';

class SearchBusinessLocation extends StatefulWidget {
    @override
    _SearchBusinessLocationState createState() => _SearchBusinessLocationState();
}

class _SearchBusinessLocationState extends State<SearchBusinessLocation> {
    @override
    void initState() {
        Provider.of<SearchBusinessLocationChangeNotif>(context, listen: false).getBusinessLocationField();
        super.initState();
    }
    @override
    Widget build(BuildContext context) {
        return Theme(
            data: ThemeData(
                primaryColor: Colors.black,
                fontFamily: "NunitoSans",
                accentColor: myPrimaryColor,
                primarySwatch: primaryOrange
            ),
            child: Scaffold(
                key: Provider.of<SearchBusinessLocationChangeNotif>(context, listen: false).scaffoldKey,
                appBar: AppBar(
                    title: Consumer<SearchBusinessLocationChangeNotif>(
                        builder: (context, searchBusinessChangeNotif, _) {
                            return TextFormField(
                                controller: searchBusinessChangeNotif.controllerSearch,
                                style: TextStyle(color: Colors.black),
                                textInputAction: TextInputAction.search,
                                onFieldSubmitted: (e) {
                                    searchBusinessChangeNotif.searchBusinessLocationField(e);
                                },
                                onChanged: (e) {
                                    searchBusinessChangeNotif.changeAction(e);
                                },
                                cursorColor: Colors.black,
                                textCapitalization: TextCapitalization.characters,
                                decoration: new InputDecoration(
                                    hintText: "Cari Lokasi Usaha (minimal 3 karakter)",
                                    hintStyle: TextStyle(color: Colors.black),
                                    enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(color: myPrimaryColor),
                                        //  when the TextFormField in unfocused
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(color: myPrimaryColor),
                                        //  when the TextFormField in focused
                                    ),
                                ),
                                autofocus: true,
                            );
                        },
                    ),
                    backgroundColor: myPrimaryColor,
                    iconTheme: IconThemeData(color: Colors.black),
                    actions: <Widget>[
                        Provider.of<SearchBusinessLocationChangeNotif>(context, listen: true)
                            .showClear
                            ? IconButton(
                            icon: Icon(Icons.clear),
                            onPressed: () {
                                Provider.of<SearchBusinessLocationChangeNotif>(context,listen: false).clearSearchTemp();
                                Provider.of<SearchBusinessLocationChangeNotif>(context,listen: false).controllerSearch.clear();
                                Provider.of<SearchBusinessLocationChangeNotif>(context,listen: false).changeAction(
                                    Provider.of<SearchBusinessLocationChangeNotif>(context,listen: false).controllerSearch.text);
                            })
                            : SizedBox(
                            width: 0.0,
                            height: 0.0,
                        )
                    ],
                ),
                body: Consumer<SearchBusinessLocationChangeNotif>(
                    builder: (context, _provider, _) {
                        return _provider.listBusinessFieldTemp.isNotEmpty
                            ? ListView.separated(
                            padding: EdgeInsets.symmetric(
                                vertical: MediaQuery.of(context).size.height / 57,
                                horizontal: MediaQuery.of(context).size.width / 27),
                            itemCount: _provider.listBusinessFieldTemp.length,
                            itemBuilder: (listContext, index) {
                                return InkWell(
                                    onTap: () {
                                        Navigator.pop(context,
                                            _provider.listBusinessFieldTemp[index]);
                                    },
                                    child: Container(
                                        child: Row(
                                            children: [
                                                Expanded(
                                                    child: Text(
                                                        "${_provider.listBusinessFieldTemp[index].id} - "
                                                            "${_provider.listBusinessFieldTemp[index].desc}",
                                                        style: TextStyle(fontSize: 16),
                                                    ))
                                            ],
                                        ),
                                    ),
                                );
                            },
                            separatorBuilder: (context, index) {
                                return Divider();
                            },
                        )
                            : ListView.separated(
                            padding: EdgeInsets.symmetric(
                                vertical: MediaQuery.of(context).size.height / 57,
                                horizontal: MediaQuery.of(context).size.width / 27),
                            itemCount: _provider.listBusinessField.length,
                            itemBuilder: (listContext, index) {
                                return InkWell(
                                    onTap: () {
                                        Navigator.pop(context, _provider.listBusinessField[index]);
                                    },
                                    child: Container(
                                        child: Row(
                                            children: [
                                                Expanded(
                                                    child: Text(
                                                        "${_provider.listBusinessField[index].id} - "
                                                            "${_provider.listBusinessField[index].desc}",
                                                        style: TextStyle(fontSize: 16),
                                                    ))
                                            ],
                                        ),
                                    ),
                                );
                            },
                            separatorBuilder: (context, index) {
                                return Divider();
                            },
                        );
                    },
                )),
        );
    }
}
