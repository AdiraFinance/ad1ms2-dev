import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/search_collateral_usage_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchCollateralUsage extends StatefulWidget {
  @override
  _SearchCollateralUsageState createState() => _SearchCollateralUsageState();
}

class _SearchCollateralUsageState extends State<SearchCollateralUsage> {
  @override
  void initState() {
    super.initState();
    Provider.of<SearchCollateralUsageChangeNotifier>(context,listen: false).getObjectPurpose(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Consumer<SearchCollateralUsageChangeNotifier>(
          builder: (context, searchCollateralUsageChangeNotifier, _) {
            return TextFormField(
              controller: searchCollateralUsageChangeNotifier.controllerSearch,
              style: TextStyle(color: Colors.black),
              textInputAction: TextInputAction.search,
              onFieldSubmitted: (e) {
//            _getCustomer(e);
              },
              onChanged: (e) {
                searchCollateralUsageChangeNotifier.changeAction(e);
              },
              cursorColor: Colors.black,
              decoration: new InputDecoration(
                hintText: "Cari Tujuan Penggunaan Collateral (minimal 3 karakter)",
                hintStyle: TextStyle(color: Colors.black),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: myPrimaryColor),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: myPrimaryColor),
                ),
              ),
              autofocus: true,
            );
          },
        ),
        backgroundColor: myPrimaryColor,
        iconTheme: IconThemeData(color: Colors.black),
        actions: <Widget>[
          Provider.of<SearchCollateralUsageChangeNotifier>(context, listen: true)
              .showClear
              ? IconButton(
              icon: Icon(Icons.clear),
              onPressed: () {
                Provider.of<SearchCollateralUsageChangeNotifier>(context,
                    listen: false)
                    .controllerSearch
                    .clear();
                Provider.of<SearchCollateralUsageChangeNotifier>(context,
                    listen: false)
                    .changeAction(
                    Provider.of<SearchCollateralUsageChangeNotifier>(
                        context,
                        listen: false)
                        .controllerSearch
                        .text);
              })
              : SizedBox(
            width: 0.0,
            height: 0.0,
          )
        ],
      ),
      body: Consumer<SearchCollateralUsageChangeNotifier>(
        builder: (context, searchCollateralUsageChangeNotifier, _) {
          return searchCollateralUsageChangeNotifier.loadData
              ?
          Center(child: CircularProgressIndicator())
              :
          searchCollateralUsageChangeNotifier.listCollateralUsageTemp.isEmpty
              ?
          ListView.separated(
            padding: EdgeInsets.symmetric(
                vertical: MediaQuery.of(context).size.height / 57,
                horizontal: MediaQuery.of(context).size.width / 27),
            itemCount:
            searchCollateralUsageChangeNotifier.listCollateralUsage.length,
            itemBuilder: (listContext, index) {
              return InkWell(
                onTap: () {
                  Navigator.pop(
                      context,
                      searchCollateralUsageChangeNotifier
                          .listCollateralUsage[index]);
                },
                child: Container(
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Text(
                        "${searchCollateralUsageChangeNotifier.listCollateralUsage[index].id} - "
                            "${searchCollateralUsageChangeNotifier.listCollateralUsage[index].name} ",
                        style: TextStyle(fontSize: 16),
                      )
                    ],
                  ),
                ),
              );
            },
            separatorBuilder: (context, index) {
              return Divider();
            },
          )
              :
          ListView.separated(
            padding: EdgeInsets.symmetric(
                vertical: MediaQuery.of(context).size.height / 57,
                horizontal: MediaQuery.of(context).size.width / 27),
            itemCount:
            searchCollateralUsageChangeNotifier.listCollateralUsageTemp.length,
            itemBuilder: (listContext, index) {
              return InkWell(
                onTap: () {
                  Navigator.pop(
                      context,
                      searchCollateralUsageChangeNotifier
                          .listCollateralUsageTemp[index]);
                },
                child: Container(
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Text(
                        "${searchCollateralUsageChangeNotifier.listCollateralUsageTemp[index].id} - "
                            "${searchCollateralUsageChangeNotifier.listCollateralUsageTemp[index].name} ",
                        style: TextStyle(fontSize: 16),
                      )
                    ],
                  ),
                ),
              );
            },
            separatorBuilder: (context, index) {
              return Divider();
            },
          );
        },
      ),
    );
  }
}
