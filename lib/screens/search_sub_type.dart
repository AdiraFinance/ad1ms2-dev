import 'package:ad1ms2_dev/shared/search_sub_type_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../main.dart';

class SearchSubType extends StatefulWidget {
    final String parentCode;

    SearchSubType(this.parentCode);
    @override
    _SearchSubTypeState createState() => _SearchSubTypeState();
}

class _SearchSubTypeState extends State<SearchSubType> {

    @override
    void initState() {
        super.initState();
        Provider.of<SearchSubTypeChangeNotifier>(context,listen: false).getSubTypeModel(context, widget.parentCode);
    }
    @override
    Widget build(BuildContext context) {
        return Theme(
            data: ThemeData(
                primaryColor: Colors.black,
                fontFamily: "NunitoSans",
                accentColor: myPrimaryColor,
                primarySwatch: primaryOrange
            ),
            child: Scaffold(
                key: Provider.of<SearchSubTypeChangeNotifier>(context,listen: false).scaffoldKey,
                appBar: AppBar(
                    title: Consumer<SearchSubTypeChangeNotifier>(
                        builder: (context, _provider, _) {
                            return TextFormField(
                                controller: _provider.controllerSearch,
                                style: TextStyle(color: Colors.black),
                                textInputAction: TextInputAction.search,
                                onFieldSubmitted: (query) {
                                    _provider.searchSubType(query);
                                },
                                onChanged: (e) {
                                    _provider.changeAction(e);
                                },
                                textCapitalization: TextCapitalization.characters,
                                cursorColor: Colors.black,
                                decoration: new InputDecoration(
                                    hintText: "Cari Sub Type (minimal 3 karakter)",
                                    hintStyle: TextStyle(color: Colors.black),
                                    enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(color: myPrimaryColor),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(color: myPrimaryColor),
                                    ),
                                ),
                                autofocus: true,
                            );
                        },
                    ),
                    backgroundColor: myPrimaryColor,
                    iconTheme: IconThemeData(color: Colors.black),
                    actions: <Widget>[
                        Provider.of<SearchSubTypeChangeNotifier>(context, listen: true)
                            .showClear
                            ? IconButton(
                            icon: Icon(Icons.clear),
                            onPressed: () {
                                Provider.of<SearchSubTypeChangeNotifier>(context,listen: false).clearSubTypeTemp();
                                Provider.of<SearchSubTypeChangeNotifier>(context,listen: false).controllerSearch.clear();
                                Provider.of<SearchSubTypeChangeNotifier>(context,listen: false).
                                changeAction(Provider.of<SearchSubTypeChangeNotifier>(context,listen: false).controllerSearch.text);
                            })
                            : SizedBox(
                            width: 0.0,
                            height: 0.0,
                        )
                    ],
                ),
                body: Consumer<SearchSubTypeChangeNotifier>(
                    builder: (context, _provider, _) {
                        return _provider.loadData
                            ?
                        Center(child: CircularProgressIndicator())
                            :
                        _provider.listSubTypeTemp.isNotEmpty
                            ?
                        ListView.separated(
                            padding: EdgeInsets.symmetric(
                                vertical: MediaQuery.of(context).size.height / 57,
                                horizontal: MediaQuery.of(context).size.width / 27),
                            itemCount: _provider.listSubTypeTemp.length,
                            itemBuilder: (listContext, index) {
                                return InkWell(
                                    onTap: () {
                                        Navigator.pop(context, _provider.listSubTypeModel[index]);
                                    },
                                    child: Container(
                                        child: Row(
                                            mainAxisSize: MainAxisSize.max,
                                            children: [
                                                Text("${_provider.listSubTypeTemp[index].KODE} - ${_provider.listSubTypeTemp[index].DESKRIPSI} ",
                                                    style: TextStyle(fontSize: 16),
                                                )
                                            ],
                                        ),
                                    ),
                                );
                            },
                            separatorBuilder: (context, index) {
                                return Divider();
                            },
                        )
                            :
                        ListView.separated(
                            padding: EdgeInsets.symmetric(
                                vertical: MediaQuery.of(context).size.height / 57,
                                horizontal: MediaQuery.of(context).size.width / 27),
                            itemCount: _provider.listSubTypeModel.length,
                            itemBuilder: (listContext, index) {
                                return InkWell(
                                    onTap: () {
                                        Navigator.pop(context,
                                            _provider.listSubTypeModel[index]);
                                    },
                                    child: Container(
                                        child: Row(
                                            mainAxisSize: MainAxisSize.max,
                                            children: [
                                                Text("${_provider.listSubTypeModel[index].KODE} - ${_provider.listSubTypeModel[index].DESKRIPSI} ",
                                                    style: TextStyle(fontSize: 16),
                                                )
                                            ],
                                        ),
                                    ),
                                );
                            },
                            separatorBuilder: (context, index) {
                                return Divider();
                            },
                        );
                    },
                ),
            ),
        );
    }
}
