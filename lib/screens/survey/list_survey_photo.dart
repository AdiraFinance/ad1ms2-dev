import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/survey/list_survey_photo_change_notifier.dart';
import 'package:ad1ms2_dev/shared/survey/result_survey_change_notifier.dart';
import 'package:ad1ms2_dev/widgets/widget_result_survey_photo.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ListSurveyPhoto extends StatefulWidget {
  @override
  _ListSurveyPhotoState createState() => _ListSurveyPhotoState();
}

class _ListSurveyPhotoState extends State<ListSurveyPhoto> {
  @override
  void initState() {
    super.initState();
    Provider.of<ListSurveyPhotoChangeNotifier>(context, listen: false).setPreference(context);
  }
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        primaryColor: Colors.black,
        primarySwatch: primaryOrange,
        accentColor: myPrimaryColor,
      ),
      child: Scaffold(
        appBar: AppBar(
          title: Text("List Survey Photo", style: TextStyle(color: Colors.black)),
          centerTitle: true,
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        body: Consumer<ListSurveyPhotoChangeNotifier>(
          builder: (context, _provider, _) {
            return Form(
              onWillPop: _provider.onBackPress,
              child: _provider.listSurveyPhoto.isEmpty
                  ?
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Add Survey Photo", style: TextStyle(color: Colors.grey, fontSize: 16))
                  ],
                ),
              )
                  :
              ListView.builder(
                padding: EdgeInsets.symmetric(
                    horizontal: MediaQuery.of(context).size.width / 27,
                    vertical: MediaQuery.of(context).size.height / 47),
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(
                            builder: (context) => ChangeNotifierProvider(
                                create: (context) => ResultSurveyChangeNotifier(),
                                child: WidgetResultSurveyPhoto(
                                  flag: 1,
                                  model: _provider.listSurveyPhoto[index],
                                  index: index,
                                )
                            ),
                          )
                      );
                    },
                    child: Card(
                      shape: _provider.listSurveyPhoto[index].isEdit
                          ?
                      RoundedRectangleBorder(
                          side: BorderSide(color: Colors.purple, width: 2),
                          borderRadius: BorderRadius.all(Radius.circular(4))
                      )
                          :
                      null,
                      elevation: 3.3,
                      child: Padding(
                        padding: EdgeInsets.all(13.0),
                        child: Stack(
                          children: [
                            Row(
                              children: <Widget>[
                                Expanded(
                                  child: Text("Survey Photo"),
                                  flex: 4,
                                ),
                                Expanded(child: Text(" : "), flex: 0),
                                Expanded(
                                  child: Text(
                                      "${_provider.listSurveyPhoto[index].photoTypeModel.id} - ${_provider.listSurveyPhoto[index].photoTypeModel.name}"),
                                  flex: 6,
                                )
                              ],
                            ),
                            Align(
                              alignment: Alignment.topRight,
                              child: IconButton(
                                  icon: Icon(
                                    Icons.delete,
                                    color: Colors.red,
                                  ),
                                  onPressed: () {
                                    Provider.of<ListSurveyPhotoChangeNotifier>(context,listen: false).deleteListSurveyPhoto(context, index);
                                  }),
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                },
                itemCount: _provider.listSurveyPhoto.length,
              ),
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(
                  builder: (context) => ChangeNotifierProvider(
                    create: (context) => ResultSurveyChangeNotifier(),
                    child: WidgetResultSurveyPhoto(
                      flag: 0,
                      index: null,
                      model: null,
                    ),
                  ),
                ));
          },
          child: Icon(
            Icons.add,
          ),
          backgroundColor: myPrimaryColor,
        ),
      ),
    );
  }
}
