import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/list_order_tracking_change_notif.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ListOrderTracking extends StatefulWidget {
  final String startDate;
  final String endDate;

  const ListOrderTracking({this.startDate, this.endDate});

  @override
  _ListOrderTrackingState createState() => _ListOrderTrackingState();
}

class _ListOrderTrackingState extends State<ListOrderTracking> {
  @override
  void initState() {
    super.initState();
    Provider.of<ListOrderTrackingChangeNotifier>(context, listen: false).getListOrderTracking(widget.startDate, widget.endDate);
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        fontFamily: "NunitoSans",
        accentColor: myPrimaryColor,
        primaryColor: myPrimaryColor,
      ),
      child: Consumer<ListOrderTrackingChangeNotifier>(
          builder: (context, listOrderTrackingChangeNotif, _) {
        return Scaffold(
            key: listOrderTrackingChangeNotif.scaffoldKey,
            appBar: AppBar(
              title: Text("Order Tracking"),
              centerTitle: true,
              actions: <Widget>[
                // IconButton(
                //     icon: Icon(Icons.info_outline, color: Colors.black),
                //     onPressed: () {
                //       _showDialog();
                //     })
              ],
            ),
            body: listOrderTrackingChangeNotif.loadData
                ? Center(child: CircularProgressIndicator())
                : listOrderTrackingChangeNotif.listOrderTracking.isEmpty
                    ? Center(
                        child: Text("Tidak ada data Order Tracking",
                            style: TextStyle(color: Colors.grey, fontSize: 16)))
                    : ListView.builder(
                        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                        itemBuilder: (context, index) {
                          return InkWell(
                            // onTap: () {
                            //   Navigator.push(
                            //       context,
                            //       MaterialPageRoute(
                            //           builder: (context) =>
                            //               DetailSimilarity()));
                            // },
                            child: Card(
                                elevation: 3.3,
                                // shape: (listOrderTrackingChangeNotif.selectedIndex == index)
                                //     ? RoundedRectangleBorder(
                                //     side: BorderSide(color: primaryOrange, width: 2),
                                //     borderRadius:
                                //     BorderRadius.all(Radius.circular(4)))
                                //     : null,
                                child: Padding(
                                    padding: EdgeInsets.all(13.0),
                                    child: Stack(
                                      children: [
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              listOrderTrackingChangeNotif.listOrderTracking[index].NAMA_NASABAH != null ? listOrderTrackingChangeNotif.listOrderTracking[index].NAMA_NASABAH : "-",
                                              style: TextStyle(color: Colors.black),
                                            ),
                                            SizedBox(height: MediaQuery.of(context).size.height/100),
                                            Text(
                                              "Application No : ${listOrderTrackingChangeNotif.listOrderTracking[index].NO_APLIKASI != null ? listOrderTrackingChangeNotif.listOrderTracking[index].NO_APLIKASI : "-"}",
                                              style: TextStyle(color: Colors.black, fontSize: 14),
                                            ),
                                            Text(
                                              "Order Date : ${listOrderTrackingChangeNotif.listOrderTracking[index].TANGGAL_ORDER != null ? listOrderTrackingChangeNotif.listOrderTracking[index].TANGGAL_ORDER : "-"}",
                                              style: TextStyle(color: Colors.black, fontSize: 13),
                                            ),
                                            Text(
                                              "Branch : ${listOrderTrackingChangeNotif.listOrderTracking[index].KODE_CABANG != null ? listOrderTrackingChangeNotif.listOrderTracking[index].KODE_CABANG : "-"}",
                                              style: TextStyle(color: Colors.black, fontSize: 13),
                                            ),
                                            Text(
                                              "On Going Process : ${listOrderTrackingChangeNotif.listOrderTracking[index].LAST_KNOWN_STATE != null ? listOrderTrackingChangeNotif.listOrderTracking[index].LAST_KNOWN_STATE : "-"}",
                                              style: TextStyle(color: Colors.black, fontSize: 13),
                                            ),
                                            Text(
                                               "Initial Scoring : ${listOrderTrackingChangeNotif.listOrderTracking[index].INITIAL_RECOMENDATION != null ? listOrderTrackingChangeNotif.listOrderTracking[index].INITIAL_RECOMENDATION : "-"}",
                                              style: TextStyle(color: Colors.black, fontSize: 13),
                                            ),
                                            Text(
                                               "Type Survey : ${listOrderTrackingChangeNotif.listOrderTracking[index].FINAL_RECOMENDATION != null ? listOrderTrackingChangeNotif.listOrderTracking[index].FINAL_RECOMENDATION : "-"}",
                                              style: TextStyle(color: Colors.black, fontSize: 13),
                                            ),
                                            Text(
                                              "Dealer : ${listOrderTrackingChangeNotif.listOrderTracking[index].DEALER != null ? listOrderTrackingChangeNotif.listOrderTracking[index].DEALER : "-"}",
                                              style: TextStyle(color: Colors.black, fontSize: 13),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ))),
                          );
                        },
                        itemCount: listOrderTrackingChangeNotif.listOrderTracking.length));
      }),
    );
  }
}
