import 'package:ad1ms2_dev/main.dart';
import 'package:flutter/material.dart';

class DetailSimilarity extends StatefulWidget {
  final String flag;
  final String identityNumber;
  final String fullname;
  final String birthDate;
  final String birthPlace;
  final String motherName;
  final String identityAddress;

  const DetailSimilarity({this.flag, this.identityNumber, this.fullname, this.birthDate, this.birthPlace, this.motherName, this.identityAddress});

  @override
  _DetailSimilarityState createState() => _DetailSimilarityState();
}

class _DetailSimilarityState extends State<DetailSimilarity> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: myPrimaryColor,
        iconTheme: IconThemeData(
            color: Colors.black
        ),
        title: Text(
          "Detail Kemiripan",
          style: TextStyle(
              color: Colors.black
          ),
        ),
        centerTitle: true,
      ),
      body: widget.flag == "PER" ? Container(
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(child: Text("Nomor Identitas"),flex: 4,),
                Text(" : "),
                Expanded(child: Text(widget.identityNumber),flex: 6,),
              ],
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            Row(
              children: <Widget>[
                Expanded(child: Text("Nama Lengkap"),flex: 4,),
                Text(" : "),
                Expanded(child: Text(widget.fullname),flex: 6,),
              ],
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            Row(
              children: <Widget>[
                Expanded(child: Text("Tanggal Lahir"),flex: 4,),
                Text(" : "),
                Expanded(child: Text(widget.birthDate),flex: 6,),
              ],
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            Row(
              children: <Widget>[
                Expanded(child: Text("Tempat Lahir"),flex: 4,),
                Text(" : "),
                Expanded(child: Text(widget.birthPlace),flex: 6,),
              ],
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            Row(
              children: <Widget>[
                Expanded(child: Text("Nama Gadis Ibu Kandung"),flex: 4,),
                Text(" : "),
                Expanded(child: Text(widget.motherName),flex: 6,),
              ],
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            Row(
              children: <Widget>[
                Expanded(child: Text("Alamat"),flex: 4,),
                Text(" : "),
                Expanded(child: Text(widget.identityAddress),flex: 6,),
              ],
            ),
          ],
        ),
      )
      : Container(
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(child: Text("NPWP"),flex: 4,),
                Text(" : "),
                Expanded(child: Text(widget.identityNumber),flex: 6,),
              ],
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            Row(
              children: <Widget>[
                Expanded(child: Text("Nama Lembaga"),flex: 4,),
                Text(" : "),
                Expanded(child: Text(widget.fullname),flex: 6,),
              ],
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            Row(
              children: <Widget>[
                Expanded(child: Text("Tanggal Pendirian"),flex: 4,),
                Text(" : "),
                Expanded(child: Text(widget.birthDate),flex: 6,),
              ],
            ),
            SizedBox(height: MediaQuery.of(context).size.height / 47),
            Row(
              children: <Widget>[
                Expanded(child: Text("Alamat NPWP"),flex: 4,),
                Text(" : "),
                Expanded(child: Text(widget.identityAddress),flex: 6,),
              ],
            ),
          ],
        ),
      )
    );
  }
}
