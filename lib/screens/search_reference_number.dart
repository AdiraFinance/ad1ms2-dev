import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/search_reference_number_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchReferenceNumber extends StatefulWidget {
  @override
  _SearchReferenceNumberState createState() => _SearchReferenceNumberState();
}

class _SearchReferenceNumberState extends State<SearchReferenceNumber> {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<SearchReferenceNumberChangeNotifier>(context,listen: false).scaffoldKey,
        appBar: AppBar(
          title: Consumer<SearchReferenceNumberChangeNotifier>(
            builder: (context, searchReferenceNumberChangeNotifier, _) {
              return TextFormField(
                controller: searchReferenceNumberChangeNotifier.controllerSearch,
                style: TextStyle(color: Colors.black),
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (query) {
                  searchReferenceNumberChangeNotifier.getReferenceNumber(query, context);
                },
                onChanged: (e) {
                  searchReferenceNumberChangeNotifier.changeAction(e);
                },
                cursorColor: Colors.black,
                decoration: new InputDecoration(
                  hintText: "Masukkan No Reference (minimal 3 karakter)",
                  hintStyle: TextStyle(color: Colors.black),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                ),
                autofocus: true,
                textCapitalization: TextCapitalization.characters,
              );
            },
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            Provider.of<SearchReferenceNumberChangeNotifier>(context,
                        listen: true)
                    .showClear
                ? IconButton(
                    icon: Icon(Icons.clear),
                    onPressed: () {
                      Provider.of<SearchReferenceNumberChangeNotifier>(context,
                              listen: false)
                          .controllerSearch
                          .clear();
                      Provider.of<SearchReferenceNumberChangeNotifier>(context,
                              listen: false)
                          .changeAction(
                              Provider.of<SearchReferenceNumberChangeNotifier>(
                                      context,
                                      listen: false)
                                  .controllerSearch
                                  .text);
                    })
                : SizedBox(
                    width: 0.0,
                    height: 0.0,
                  )
          ],
        ),
        body: Column(
          children: [
            Container(
              padding: EdgeInsets.symmetric(vertical: 20),
              child: GestureDetector(
                onTap: () {
                  Provider.of<SearchReferenceNumberChangeNotifier>(context,listen: false).filterDialog(context);
                },
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.filter_list),
                    SizedBox(width: 16),
                    Text(
                      "FILTER",
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          letterSpacing: 1.25),
                    )
                  ],
                ),
              ),
            ),
            Expanded(
              child: Consumer<SearchReferenceNumberChangeNotifier>(
                builder: (context, searchReferenceNumberChangeNotifier, _) {
                  return ListView.separated(
                    padding: EdgeInsets.symmetric(
                        vertical: MediaQuery.of(context).size.height / 57,
                        horizontal: MediaQuery.of(context).size.width / 27),
                    itemCount:
                        searchReferenceNumberChangeNotifier.listReferenceNumber.length,
                    itemBuilder: (listContext, index) {
                      return InkWell(
                        onTap: () {
                          Navigator.pop(
                              context,
                              searchReferenceNumberChangeNotifier
                                  .listReferenceNumber[index]);
                        },
                        child: Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "${searchReferenceNumberChangeNotifier.listReferenceNumber[index].noPK}",
                                style: TextStyle(fontSize: 16),
                              ),
                              SizedBox(height: MediaQuery.of(context).size.height/77),
                              Text(
                                "${searchReferenceNumberChangeNotifier.listReferenceNumber[index].custName}",
                                style: TextStyle(fontSize: 16),
                              )
                            ],
                          ),
                        ),
                      );
                    },
                    separatorBuilder: (context, index) {
                      return Divider();
                    },
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
