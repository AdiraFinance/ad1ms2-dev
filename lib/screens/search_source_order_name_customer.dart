import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/shared/search_source_order_name_customer_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_source_order_name_karyawan_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchSourceOrderNameCustomer extends StatefulWidget {
  @override
  _SearchSourceOrderNameCustomerState createState() => _SearchSourceOrderNameCustomerState();
}

class _SearchSourceOrderNameCustomerState extends State<SearchSourceOrderNameCustomer> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          primaryColor: Colors.black,
          accentColor: myPrimaryColor,
          fontFamily: "NunitoSans",
          primarySwatch: primaryOrange
      ),
      child: Scaffold(
        key: Provider.of<SearchSourceOrderNameCustomerChangeNotifier>(context,listen: false).scaffoldKey,
        appBar: AppBar(
          title: Consumer<SearchSourceOrderNameCustomerChangeNotifier>(
            builder: (context, searchSourceOrderNameCustomerChangeNotifier, _) {
              return TextFormField(
                controller: searchSourceOrderNameCustomerChangeNotifier.controllerSearch,
                style: TextStyle(color: Colors.black),
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (query) {
                  if(query.length >= 3) {
                    searchSourceOrderNameCustomerChangeNotifier.getSourceOrderNameCustomer(query);
                  } else {
                    searchSourceOrderNameCustomerChangeNotifier.showSnackBar("Input minimal 3 karakter");
                  }
                },
                onChanged: (e) {
                  searchSourceOrderNameCustomerChangeNotifier.changeAction(e);
                },
                textCapitalization: TextCapitalization.characters,
                cursorColor: Colors.black,
                decoration: new InputDecoration(
                  hintText: "Cari Nama Sumber Order (minimal 3 karakter)",
                  hintStyle: TextStyle(color: Colors.black),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: myPrimaryColor),
                  ),
                ),
                autofocus: true,
              );
            },
          ),
          backgroundColor: myPrimaryColor,
          iconTheme: IconThemeData(color: Colors.black),
          actions: <Widget>[
            Provider.of<SearchSourceOrderNameCustomerChangeNotifier>(context,
                        listen: true)
                    .showClear
                ? IconButton(
                    icon: Icon(Icons.clear),
                    onPressed: () {
                      Provider.of<SearchSourceOrderNameCustomerChangeNotifier>(context,
                          listen: false).clearSearchTemp();
                      Provider.of<SearchSourceOrderNameCustomerChangeNotifier>(context,
                              listen: false)
                          .controllerSearch
                          .clear();
                      Provider.of<SearchSourceOrderNameCustomerChangeNotifier>(context,
                              listen: false)
                          .changeAction(
                              Provider.of<SearchSourceOrderNameCustomerChangeNotifier>(
                                      context,
                                      listen: false)
                                  .controllerSearch
                                  .text);
                    })
                : SizedBox(
                    width: 0.0,
                    height: 0.0,
                  )
          ],
        ),
        body: Consumer<SearchSourceOrderNameCustomerChangeNotifier>(
          builder: (context, searchSourceOrderNameCustomerChangeNotifier, _) {
            return searchSourceOrderNameCustomerChangeNotifier.loadData
                ?
            Center(
              child: CircularProgressIndicator(),
            )
                :
            searchSourceOrderNameCustomerChangeNotifier.listSourceOrderNameCustomerTemp.isNotEmpty
                ?
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount:
              searchSourceOrderNameCustomerChangeNotifier.listSourceOrderNameCustomerTemp.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(
                        context,
                        searchSourceOrderNameCustomerChangeNotifier
                            .listSourceOrderNameCustomerTemp[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchSourceOrderNameCustomerChangeNotifier.listSourceOrderNameCustomerTemp[index].kode} - "
                              "${searchSourceOrderNameCustomerChangeNotifier.listSourceOrderNameCustomerTemp[index].deskripsi}",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            )
                :
            ListView.separated(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height / 57,
                  horizontal: MediaQuery.of(context).size.width / 27),
              itemCount:
                  searchSourceOrderNameCustomerChangeNotifier.listSourceOrderNameCustomer.length,
              itemBuilder: (listContext, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pop(
                        context,
                        searchSourceOrderNameCustomerChangeNotifier
                            .listSourceOrderNameCustomer[index]);
                  },
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "${searchSourceOrderNameCustomerChangeNotifier
                              .listSourceOrderNameCustomer[index].kode} - "
                              "${searchSourceOrderNameCustomerChangeNotifier
                              .listSourceOrderNameCustomer[index].deskripsi}",
                          style: TextStyle(fontSize: 16),
                        )
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          },
        ),
      ),
    );
  }
}
