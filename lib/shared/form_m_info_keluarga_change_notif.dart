import 'dart:convert';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/birth_place_model.dart';
import 'package:ad1ms2_dev/models/form_m_info_keluarga_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_family_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../main.dart';
import 'change_notifier_app/information_object_unit_change_notifier.dart';
import 'form_m_info_nasabah_change_notif.dart';

class FormMInfoKeluargaChangeNotif with ChangeNotifier {
  List<FormMInfoKelModel> _listFormInfoKel = [];
  bool _autoValidate = false;
  bool _flag = false;
  DbHelper _dbHelper = DbHelper();
  bool _isPasanganExist = false;
  bool _checkGroupObject = false;
  String _status = '';
  String _custType;
  String _lastKnownState;
  bool _isCheckData = false;

  bool get isCheckData => _isCheckData;

  set isCheckData(bool value) {
    this._isCheckData = value;
    notifyListeners();
  }

  String get custType => _custType;

  set custType(String value) {
    this._custType = value;
    notifyListeners();
  }

  String get lastKnownState => _lastKnownState;

  set lastKnownState(String value) {
    this._lastKnownState = value;
    notifyListeners();
  }

  List<FormMInfoKelModel> get listFormInfoKel => _listFormInfoKel;

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  bool _isInfoKeluargaCardChanges = false;
  bool get isInfoKeluargaCardChanges => _isInfoKeluargaCardChanges;
  set isInfoKeluargaCardChanges(bool value) {
    this._isInfoKeluargaCardChanges = value;
    notifyListeners();
  }

  bool get isPasanganExist => _isPasanganExist;

  set isPasanganExist(bool value) {
    this._isPasanganExist = value;
  }

  void addListInfoKel(FormMInfoKelModel value) {
    _listFormInfoKel.add(value);
    if (this._autoValidate) autoValidate = false;
    notifyListeners();
  }

  void deleteListInfoKel(BuildContext context, int index) {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context){
        return Theme(
          data: ThemeData(
              fontFamily: "NunitoSans",
              primaryColor: Colors.black,
              primarySwatch: primaryOrange,
              accentColor: myPrimaryColor
          ),
          child: AlertDialog(
            title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
            content: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text("Apakah kamu yakin menghapus data ini?",),
              ],
            ),
            actions: <Widget>[
              new FlatButton(
                onPressed: () {
                  this._listFormInfoKel.removeAt(index);
                  notifyListeners();
                  Navigator.pop(context);
                },
                child: new Text('Ya', style: TextStyle(color: Colors.grey),),
              ),
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: new Text('Tidak'),
              ),
            ],
          ),
        );
      }
    );
  }

  void updateListInfoKel(FormMInfoKelModel mInfoKelModel, BuildContext context, int index) {
    this._listFormInfoKel[index] = mInfoKelModel;
    notifyListeners();
    Navigator.pop(context);
  }

  bool get flag => _flag;

  set flag(bool value) {
    this._flag = value;
    notifyListeners();
  }

  bool get checkGroupObject => _checkGroupObject;

  set checkGroupObject(bool value) {
    this._checkGroupObject = value;
    notifyListeners();
  }

  String get status => _status;

  set status(String value) {
    this._status = value;
    notifyListeners();
  }

  void checkWarning(BuildContext context){
    _isCheckData = true;
    var _providerRincianNasabah = Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false);
    var _providerInfoObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
    if(_providerRincianNasabah.maritalStatusSelected != null){
      this._status = _providerRincianNasabah.maritalStatusSelected.id;
    }
    if(_providerInfoObjectUnit.groupObjectSelected != null){
      this._checkGroupObject = _providerInfoObjectUnit.groupObjectSelected.KODE != "003" ? true : false;
    }
  }

  Future<bool> onBackPress() async{
    if (_listFormInfoKel.length != 0) {
      flag = false;
      if(this._checkGroupObject && this._status == "01"){
        this._isCheckData = false;
        isPasanganExist = true;
        for(int i = 0; i < _listFormInfoKel.length; i++) {
          if(_listFormInfoKel[i].relationshipStatusModel.PARA_FAMILY_TYPE_ID == "01" || _listFormInfoKel[i].relationshipStatusModel.PARA_FAMILY_TYPE_ID == "02") {
            this._isCheckData = true;
            isPasanganExist = false;
          }
        }
      }
    }
    else {
      flag = true;
    }
    return true;
  }

  // Future<void> clearDataKeluarga() async {
  void clearDataKeluarga() {
    this._listFormInfoKel.clear();
    this._isCheckData = false;
  }

  void saveToSQLite() async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    // var checkID = await _dbHelper.selectDataInfoKeluargaID("");
    // print("id keluarga: ${checkID[0]['familyInfoID']}");
    for(int i=0; i < _listFormInfoKel.length; i++){
      _dbHelper.insertMS2CustFamily(
          MS2CustFamilyModel(
              "123",
              _listFormInfoKel[i].familyInfoID != null ? _listFormInfoKel[i].familyInfoID : "NEW",
              _listFormInfoKel[i].relationshipStatusModel != null ? _listFormInfoKel[i].relationshipStatusModel.PARA_FAMILY_TYPE_ID : "",
              _listFormInfoKel[i].relationshipStatusModel != null ? _listFormInfoKel[i].relationshipStatusModel.PARA_FAMILY_TYPE_NAME : "",
              _listFormInfoKel[i].namaLengkapSesuaiIdentitas,
              _listFormInfoKel[i].namaLengkap,
              _listFormInfoKel[i].noIdentitas,
              _listFormInfoKel[i].birthDate,
              _listFormInfoKel[i].tmptLahirSesuaiIdentitas,
              _listFormInfoKel[i].birthPlaceModel != null ? _listFormInfoKel[i].birthPlaceModel.KABKOT_ID : "",
              _listFormInfoKel[i].birthPlaceModel != null ? _listFormInfoKel[i].birthPlaceModel.KABKOT_NAME : "",
              _listFormInfoKel[i].gender,
              _listFormInfoKel[i].gender == "01"? "Laki laki" : "Perempuan",
              null,
              _listFormInfoKel[i].identityModel != null ? _listFormInfoKel[i].identityModel.id:"",
              _listFormInfoKel[i].identityModel != null ? _listFormInfoKel[i].identityModel.name:"",
              null,
              null,
              null,
              null,
              1,
              null,
              _listFormInfoKel[i].noTlpn,
              _listFormInfoKel[i].kodeArea,
              _listFormInfoKel[i].noHp,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
          )
      );
    }

    // var printSelect = await _dbHelper.selectDataInfoKeluarga("");
    // await http.post(
    //     "http://192.168.57.122/orca_api/public/login",
    //     body: jsonEncode(printSelect),
    //     headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    // );
  }

  Future<bool> deleteSQLite() async{
    return await _dbHelper.deleteMS2CustFamily();
  }

  Future<void> setDataFromSQLite() async{
    await setPreference();
    if(this._listFormInfoKel.isEmpty){
      this._listFormInfoKel.clear();
      List _data =  await _dbHelper.selectDataInfoKeluarga("");
      print("cek db info keluarga $_data");
      if(_data.isNotEmpty){
        for(int i=0; i <_data.length; i++){
          print("cek relation status: ${_data[i]['relation_status']}");
          this._listFormInfoKel.add(
              FormMInfoKelModel(
                IdentityModel(_data[i]['id_type'], _data[i]['id_desc']),
                _data[i]['familyInfoID'],
                RelationshipStatusModel(_data[i]['relation_status'], _data[i]['relation_status_desc']),
                _data[i]['id_no'],
                _data[i]['full_name_id'],
                _data[i]['full_name'],
                _data[i]['date_of_birth'],
                _data[i]['gender'],
                _data[i]['phone1_area'],
                _data[i]['phone1'],
                _data[i]['place_of_birth'],
                _data[i]['handphone_no'],
                BirthPlaceModel(_data[i]['place_of_birth_kabkota'], _data[i]['place_of_birth_kabkota_desc']),
                _data[i]['edit_id_type'] == "1",
                _data[i]['edit_relation_stauts'] == "1",
                _data[i]['edit_id_no'] == "1",
                _data[i]['edit_full_name_id'] == "1",
                _data[i]['edit_full_name'] == "1",
                _data[i]['edit_date_of_birth'] == "1",
                _data[i]['edit_gender'] == "1",
                _data[i]['edit_phone1_area'] == "1",
                _data[i]['edit_phone1'] == "1",
                _data[i]['edit_place_of_birth'] == "1",
                _data[i]['edit_handphone_no'] == "1",
                _data[i]['edit_place_of_birth_kabkota'] == "1",
              )
          );
        }
      }
      notifyListeners();
    }
  }

  Future<void> setPreference() async {
    SharedPreferences _preference = await SharedPreferences.getInstance();
    this._custType = _preference.getString("cust_type");
    this._lastKnownState = _preference.getString("last_known_state");
  }
}
