import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/branch_marketing_model.dart';
import 'package:ad1ms2_dev/screens/home.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/add_credit_subsidy_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_app_document_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_application_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_income_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_type_installment_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_subsidy_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_major_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_object_karoseri_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_collatelar_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_salesman_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_company_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_manajemen_pic_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_pendapatan_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_rincian_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_guarantor_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_income_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_info_keluarga_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_keluarga(ibu)_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_occupation_change_notif.dart';
import 'package:ad1ms2_dev/shared/info_wmp_change_notifier.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SAJobMayorChangeNotifier with ChangeNotifier{
  TextEditingController _controllerAppNo = TextEditingController();
  TextEditingController _controllerNumberOrder = TextEditingController();
  TextEditingController _controllerCustomerName = TextEditingController();
  TextEditingController _controllerCellPhoneNumber = TextEditingController();
  TextEditingController _controllerPhoneNumber = TextEditingController();
  List<BranchMarketingModel> _listBranchMarketing = [
    BranchMarketingModel("001", "MARKETING CABANG 1"),
    BranchMarketingModel("002", "MARKETING CABANG 2"),
    BranchMarketingModel("003", "MARKETING CABANG 3"),
  ];
  TextEditingController _controllerAppointmentSurveyDate = TextEditingController();
  bool _autoValidate = false;
  BranchMarketingModel _branchMarketingSelected;
  DateTime _initialAppointmentSurveyDate = DateTime(dateNow.year,dateNow.month,dateNow.day);
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  var storage = FlutterSecureStorage();

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  TextEditingController get controllerAppNo => _controllerAppNo;

  TextEditingController get controllerNumberOrder => _controllerNumberOrder;

  TextEditingController get controllerCustomerName => _controllerCustomerName;

  TextEditingController get controllerCellPhoneNumber =>
      _controllerCellPhoneNumber;

  TextEditingController get controllerPhoneNumber => _controllerPhoneNumber;

  TextEditingController get controllerAppointmentSurveyDate =>
      _controllerAppointmentSurveyDate;

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  List<BranchMarketingModel> get listBranchMarketing => _listBranchMarketing;

  BranchMarketingModel get branchMarketingSelected => _branchMarketingSelected;

  set branchMarketingSelected(BranchMarketingModel value) {
    this._branchMarketingSelected = value;
    notifyListeners();
  }

  void selectAppointmentSurveyDate(BuildContext context) async {
    final DateTime _picked = await showDatePicker(
        context: context,
        initialDate: _initialAppointmentSurveyDate,
        firstDate: DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day),
        lastDate: DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day));
    if (_picked != null) {
      this._initialAppointmentSurveyDate = _picked;
      this._controllerAppointmentSurveyDate.text = dateFormat.format(_picked);
      notifyListeners();
    } else {
      return;
    }
  }

  // List mappingCustomerIncomeWiraswastaBelumMenikah(BuildContext context) {
  //   var _providerIncome = Provider.of<FormMIncomeChangeNotifier>(context, listen: false);
  //
  //   // Wiraswasta Belum Menikah
  //   var _customerIncomes = [];
  //   for(int i=0; i < 12; i++) {
  //     _customerIncomes.add(
  //       {
  //         "customerIncomeID": "NEW",
  //         "incomeFrmlType": i == 0
  //             ? "S" : i == 1
  //             ? "+" : i == 2
  //             ? "=" : i == 3
  //             ? "-" : i == 4
  //             ? "=" : i == 5
  //             ? "-" : i == 6
  //             ? "-" : i == 7
  //             ? "=" : i == 8
  //             ? "-" : i == 9
  //             ? "=" : i == 10
  //             ? "-" : i == 11
  //             ? "="
  //             : "",
  //         "incomeType": i == 0
  //             ? "001" : i == 1
  //             ? "002" : i == 2
  //             ? "003" : i == 3
  //             ? "004" : i == 4
  //             ? "005" : i == 5
  //             ? "006" : i == 6
  //             ? "007" : i == 7
  //             ? "008" : i == 8
  //             ? "009" : i == 9
  //             ? "010" : i == 10
  //             ? "011" : i == 11
  //             ? "017"
  //             : "",
  //         "incomeValue": i == 0
  //             ? _providerIncome.controllerMonthlyIncome.text : i == 1
  //             ? _providerIncome.controllerOtherIncome.text : i == 2
  //             ? _providerIncome.controllerTotalIncome.text : i == 3
  //             ? _providerIncome.controllerCostOfGoodsSold.text : i == 4
  //             ? _providerIncome.controllerGrossProfit.text : i == 5
  //             ? _providerIncome.controllerOperatingCosts.text : i == 6
  //             ? _providerIncome.controllerOtherCosts.text : i == 7
  //             ? _providerIncome.controllerNetProfitBeforeTax.text : i == 8
  //             ? _providerIncome.controllerTax.text : i == 9
  //             ? _providerIncome.controllerNetProfitAfterTax.text : i == 10
  //             ? _providerIncome.controllerCostOfLiving.text : i == 11
  //             ? _providerIncome.controllerRestIncome.text
  //             : _providerIncome.controllerOtherInstallments.text,
  //         "dataStatus": "ACTIVE",
  //         "creationalSpecification": {
  //           "createdAt": DateTime.now(),
  //           "createdBy": _preferences.getString("username"),
  //           "modifiedAt": DateTime.now(),
  //           "modifiedBy": _preferences.getString("username"),
  //         },
  //       }
  //     );
  //   }
  //
  //   return _customerIncomes;
  // }
  //
  // List mappingCustomerIncomeWiraswastaMenikah(BuildContext context) {
  //   var _providerIncome = Provider.of<FormMIncomeChangeNotifier>(context, listen: false);
  //
  //   // Wiraswasta Menikah
  //   var _customerIncomes = [];
  //   for(int i=0; i < 13; i++) {
  //     _customerIncomes.add(
  //       {
  //         "customerIncomeID": "NEW",
  //         "incomeFrmlType": i == 0
  //             ? "S" : i == 1
  //             ? "+" : i == 2
  //             ? "=" : i == 3
  //             ? "-" : i == 4
  //             ? "=" : i == 5
  //             ? "-" : i == 6
  //             ? "-" : i == 7
  //             ? "=" : i == 8
  //             ? "-" : i == 9
  //             ? "=" : i == 10
  //             ? "-" : i == 11
  //             ? "=" : i == 12
  //             ? ""
  //             : "",
  //         "incomeType": i == 0
  //             ? "001" : i == 1
  //             ? "002" : i == 2
  //             ? "003" : i == 3
  //             ? "004" : i == 4
  //             ? "005" : i == 5
  //             ? "006" : i == 6
  //             ? "007" : i == 7
  //             ? "008" : i == 8
  //             ? "009" : i == 9
  //             ? "010" : i == 10
  //             ? "011" : i == 11
  //             ? "017" : i == 12
  //             ? ""
  //             : "",
  //         "incomeValue": i == 0
  //             ? _providerIncome.controllerMonthlyIncome.text : i == 1
  //             ? _providerIncome.controllerOtherIncome.text : i == 2
  //             ? _providerIncome.controllerTotalIncome.text : i == 3
  //             ? _providerIncome.controllerCostOfGoodsSold.text : i == 4
  //             ? _providerIncome.controllerGrossProfit.text : i == 5
  //             ? _providerIncome.controllerOperatingCosts.text : i == 6
  //             ? _providerIncome.controllerOtherCosts.text : i == 7
  //             ? _providerIncome.controllerNetProfitBeforeTax.text : i == 8
  //             ? _providerIncome.controllerTax.text : i == 9
  //             ? _providerIncome.controllerNetProfitAfterTax.text : i == 10
  //             ? _providerIncome.controllerCostOfLiving.text : i == 11
  //             ? _providerIncome.controllerRestIncome.text : i == 12
  //             ? _providerIncome.controllerSpouseIncome.text
  //             : _providerIncome.controllerOtherInstallments.text,
  //         "dataStatus": "ACTIVE",
  //         "creationalSpecification": {
  //           "createdAt": DateTime.now(),
  //           "createdBy": _preferences.getString("username"),
  //           "modifiedAt": DateTime.now(),
  //           "modifiedBy": _preferences.getString("username"),
  //         },
  //       }
  //     );
  //   }
  //
  //   return _customerIncomes;
  // }
  //
  // List mappingCustomerIncomeProfesionalBelumMenikah(BuildContext context) {
  //   var _providerIncome = Provider.of<FormMIncomeChangeNotifier>(context, listen: false);
  //
  //   var _customerIncomes = [];
  //   // Profesional Belum Menikah
  //   for(int i=0; i < 5; i++) {
  //     _customerIncomes.add(
  //         {
  //           "customerIncomeID": "NEW",
  //           "incomeFrmlType": i == 0
  //               ? "S" : i == 1
  //               ? "+" : i == 2
  //               ? "=" : i == 3
  //               ? "-" : i == 4
  //               ? "="
  //               : "",
  //           "incomeType": i == 0
  //               ? "001" : i == 1
  //               ? "002" : i == 2
  //               ? "003" : i == 3
  //               ? "011" : i == 4
  //               ? "017"
  //               : "",
  //           "incomeValue": i == 0
  //               ? _providerIncome.controllerIncome.text : i == 1
  //               ? _providerIncome.controllerOtherIncome.text : i == 2
  //               ? _providerIncome.controllerTotalIncome.text : i == 3
  //               ? _providerIncome.controllerCostOfLiving.text : i == 4
  //               ? _providerIncome.controllerRestIncome.text
  //               : _providerIncome.controllerOtherInstallments.text,
  //           "dataStatus": "ACTIVE",
  //           "creationalSpecification": {
  //             "createdAt": DateTime.now(),
  //             "createdBy": _preferences.getString("username"),
  //             "modifiedAt": DateTime.now(),
  //             "modifiedBy": _preferences.getString("username"),
  //           },
  //         }
  //     );
  //   }
  //
  //   return _customerIncomes;
  // }
  //
  // List mappingCustomerIncomeProfesionalMenikah(BuildContext context) {
  //   var _providerIncome = Provider.of<FormMIncomeChangeNotifier>(context, listen: false);
  //
  //   var _customerIncomes = [];
  //   // Profesional Menikah
  //   for(int i=0; i < 6; i++) {
  //     _customerIncomes.add(
  //         {
  //           "customerIncomeID": "NEW",
  //           "incomeFrmlType": i == 0
  //               ? "S" : i == 1
  //               ? "+" : i == 2
  //               ? "+" : i == 3
  //               ? "=" : i == 4
  //               ? "-" : i == 5
  //               ? "="
  //               : "",
  //           "incomeType": i == 0
  //               ? "001" : i == 1
  //               ? "002" : i == 2
  //               ? "?" : i == 3
  //               ? "003" : i == 4
  //               ? "011" : i == 5
  //               ? "017"
  //               : "",
  //           "incomeValue": i == 0
  //               ? _providerIncome.controllerIncome.text : i == 1
  //               ? _providerIncome.controllerSpouseIncome.text : i == 2
  //               ? _providerIncome.controllerOtherIncome.text : i == 3
  //               ? _providerIncome.controllerTotalIncome.text : i == 4
  //               ? _providerIncome.controllerCostOfLiving.text : i == 5
  //               ? _providerIncome.controllerRestIncome.text
  //               : _providerIncome.controllerOtherInstallments.text,
  //           "dataStatus": "ACTIVE",
  //           "creationalSpecification": {
  //             "createdAt": DateTime.now(),
  //             "createdBy": _preferences.getString("username"),
  //             "modifiedAt": DateTime.now(),
  //             "modifiedBy": _preferences.getString("username"),
  //           },
  //         }
  //     );
  //   }
  //
  //   return _customerIncomes;
  // }
  //
  // List mappingCustomerIncomeCompany (BuildContext context) {
  //   var _providerIncome = Provider.of<FormMCompanyPendapatanChangeNotifier>(context, listen: false);
  //
  //   var _customerIncomes = [];
  //   for(int i=0; i < 10; i++) {
  //     _customerIncomes.add(
  //         {
  //           "customerIncomeID": "NEW",
  //           "incomeFrmlType": i == 0
  //               ? "S" : i == 1
  //               ? "+" : i == 2
  //               ? "=" : i == 3
  //               ? "-" : i == 4
  //               ? "=" : i == 5
  //               ? "-" : i == 6
  //               ? "-" : i == 7
  //               ? "=" : i == 8
  //               ? "-" : i == 9
  //               ? "="
  //               : "",
  //           "incomeType": i == 0
  //               ? "001" : i == 1
  //               ? "002" : i == 2
  //               ? "003" : i == 3
  //               ? "004" : i == 4
  //               ? "005" : i == 5
  //               ? "006" : i == 6
  //               ? "007" : i == 7
  //               ? "008" : i == 8
  //               ? "" : i == 9
  //               ? ""
  //               : "",
  //           "incomeValue": i == 0
  //               ? _providerIncome.controllerMonthlyIncome.text : i == 1
  //               ? _providerIncome.controllerOtherIncome.text : i == 2
  //               ? _providerIncome.controllerTotalIncome.text : i == 3
  //               ? _providerIncome.controllerCostOfRevenue.text : i == 4
  //               ? _providerIncome.controllerGrossProfit.text : i == 5
  //               ? _providerIncome.controllerOperatingCosts.text : i == 6
  //               ? _providerIncome.controllerOtherCosts.text : i == 7
  //               ? _providerIncome.controllerNetProfitBeforeTax.text : i == 8
  //               ? _providerIncome.controllerTax.text : i == 9
  //               ? _providerIncome.controllerNetProfitAfterTax.text
  //               : _providerIncome.controllerOtherInstallments.text,
  //           "dataStatus": "ACTIVE",
  //           "creationalSpecification": {
  //             "createdAt": DateTime.now(),
  //             "createdBy": _preferences.getString("username"),
  //             "modifiedAt": DateTime.now(),
  //             "modifiedBy": _preferences.getString("username"),
  //           },
  //         }
  //     );
  //   }
  //
  //   return _customerIncomes;
  // }

  void generateAplikasiPayung(BuildContext context) async {
    var _listOID = Provider.of<ListOidChangeNotifier>(context,listen: false);
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
    var _providerUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);

    SharedPreferences _preferences = await SharedPreferences.getInstance();
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var _body = jsonEncode({
      "unitId" : _preferences.getString("branchid"),
      "columnName" : "AC_APPL_NO",
      "objectGroupId" : _providerUnitObject.groupObjectSelected != null ? _providerUnitObject.groupObjectSelected.KODE : "",
      "finTypeTid" : _listOID.customerType != "COM" ? _providerFoto.typeOfFinancingModelSelected.financingTypeId : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId,
      "noAppUnit" : ""
    });
    String urlAcction = await storage.read(key: "urlAcction");
    final _response = await _http.post(
        "${urlAcction}adira-acction-062036/acction/service/order/generateID",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );

    if(_response.statusCode == 200){
      // print(_response.body);
      _showSnackBar("Berhasil generate nomer aplikasi payung");
      final _result = jsonDecode(_response.body);
      var _orderId = _result['message'];
      generateAplikasiUnit(context, _orderId);
    }
    else{
      print(_response.statusCode);
      _showSnackBar("Error ${_response.statusCode}");
    }
  }

  void generateAplikasiUnit(BuildContext context, String orderId) async {
    var _listOID = Provider.of<ListOidChangeNotifier>(context,listen: false);
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
    var _providerUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);

    SharedPreferences _preferences = await SharedPreferences.getInstance();
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var _body = jsonEncode({
      "unitId" : _preferences.getString("branchid"),
      "columnName" : "AC_APPL_NO",
      "objectGroupId" : _providerUnitObject.groupObjectSelected != null ? _providerUnitObject.groupObjectSelected.KODE : "",
      "finTypeTid" : _listOID.customerType != "COM" ? _providerFoto.typeOfFinancingModelSelected.financingTypeId : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId,
      "noAppUnit" : ""
    });
    String urlAcction = await storage.read(key: "urlAcction");
    final _response = await _http.post(
        "${urlAcction}adira-acction-062036/acction/service/order/generateID",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );

    if(_response.statusCode == 200){
      // print(_response.body);
      _showSnackBar("Berhasil generate nomer aplikasi unit");
      final _result = jsonDecode(_response.body);
      var _orderProductId = _result['message'];
      // if(_radioValueApproved == 0) {
      validateData(context, orderId, _orderProductId);
      // } else {
      //   submitDataUnapprove(context, orderId, _orderProductId);
      // }
    }
    else{
      print(_response.statusCode);
      _showSnackBar("Error ${_response.statusCode}");
    }
  }

  void validateData(BuildContext context, String orderId, String orderProductId) async {
    // Dedup
    var _listOID = Provider.of<ListOidChangeNotifier>(context,listen: false);

    // Form IDE Individu
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
    var _providerInfoNasabah = Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false);
    var _providerInfoAlamat = Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false);
    var _providerIbu = Provider.of<FormMInformasiKeluargaIBUChangeNotif>(context, listen: false);
    var _providerKeluarga = Provider.of<FormMInfoKeluargaChangeNotif>(context, listen: false);
    var _providerOccupation = Provider.of<FormMOccupationChangeNotif>(context,listen: false);
    var _providerIncome = Provider.of<FormMIncomeChangeNotifier>(context, listen: false);
    var _providerGuarantor = Provider.of<FormMGuarantorChangeNotifier>(context, listen: false);

    // Form IDE Company
    var _providerRincian = Provider.of<FormMCompanyRincianChangeNotifier>(context, listen: false);
    var _providerInfoAlamatCompany = Provider.of<FormMCompanyAlamatChangeNotifier>(context, listen: false);
    var _providerIncomeCompany = Provider.of<FormMCompanyPendapatanChangeNotifier>(context, listen: false);
    var _providerManajemenPIC = Provider.of<FormMCompanyManajemenPICChangeNotifier>(context, listen: false);

    // Form App
    var _providerApp = Provider.of<InfoAppChangeNotifier>(context, listen: false);
    var _providerUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
    var _providerKolateral = Provider.of<InformationCollateralChangeNotifier>(context, listen: false);
    var _providerSales = Provider.of<InformationSalesmanChangeNotifier>(context, listen: false);
    var _providerInsurance = Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false);
    var _providerKaroseri = Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false);
    var _providerCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false);
    var _providerCreditStructureType = Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context, listen: false);
    var _providerCreditIncome = Provider.of<InfoCreditIncomeChangeNotifier>(context, listen: false);
    var _providerWmp = Provider.of<InfoWMPChangeNotifier>(context,listen: false);
    var _providerSubsidy = Provider.of<InfoCreditSubsidyChangeNotifier>(context,listen: false);
    var _providerSubsidyDetail = Provider.of<AddCreditSubsidyChangeNotifier>(context, listen: false);
    // var _providerCreditStructureTypeInstallment = Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false);
    var _providerDocument = Provider.of<InfoDocumentChangeNotifier>(context,listen: false);

    var occupation = Provider.of<FormMFotoChangeNotifier>(context, listen: false).occupationSelected.KODE;
    var _providerInfoDocument = Provider.of<InfoDocumentChangeNotifier>(context, listen: false);

    SharedPreferences _preferences = await SharedPreferences.getInstance();

    var _customerIncomes = [];
    if(_listOID.customerType != "COM") {
      if(_providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07") {
        if(_providerInfoNasabah.maritalStatusSelected.id != "01") {
          for(int i=0; i < 12; i++) {
            _customerIncomes.add(
                {
                  "customerIncomeID": "NEW",
                  "incomeFrmlType": i == 0
                      ? "S" : i == 1
                      ? "+" : i == 2
                      ? "=" : i == 3
                      ? "-" : i == 4
                      ? "=" : i == 5
                      ? "-" : i == 6
                      ? "-" : i == 7
                      ? "=" : i == 8
                      ? "-" : i == 9
                      ? "=" : i == 10
                      ? "-" : i == 11
                      ? "="
                      : "",
                  "incomeType": i == 0
                      ? "001" : i == 1
                      ? "002" : i == 2
                      ? "003" : i == 3
                      ? "004" : i == 4
                      ? "005" : i == 5
                      ? "006" : i == 6
                      ? "007" : i == 7
                      ? "008" : i == 8
                      ? "009" : i == 9
                      ? "010" : i == 10
                      ? "011" : i == 11
                      ? "017"
                      : "",
                  "incomeValue": i == 0
                      ? _providerIncome.controllerMonthlyIncome.text : i == 1
                      ? _providerIncome.controllerOtherIncome.text : i == 2
                      ? _providerIncome.controllerTotalIncome.text : i == 3
                      ? _providerIncome.controllerCostOfGoodsSold.text : i == 4
                      ? _providerIncome.controllerGrossProfit.text : i == 5
                      ? _providerIncome.controllerOperatingCosts.text : i == 6
                      ? _providerIncome.controllerOtherCosts.text : i == 7
                      ? _providerIncome.controllerNetProfitBeforeTax.text : i == 8
                      ? _providerIncome.controllerTax.text : i == 9
                      ? _providerIncome.controllerNetProfitAfterTax.text : i == 10
                      ? _providerIncome.controllerCostOfLiving.text : i == 11
                      ? _providerIncome.controllerRestIncome.text
                      : _providerIncome.controllerOtherInstallments.text,
                  "dataStatus": "ACTIVE",
                  "creationalSpecification": {
                    "createdAt": DateTime.now(),
                    "createdBy": _preferences.getString("username"),
                    "modifiedAt": DateTime.now(),
                    "modifiedBy": _preferences.getString("username"),
                  },
                }
            );
          }
        } else {
          for(int i=0; i < 13; i++) {
            _customerIncomes.add(
                {
                  "customerIncomeID": "NEW",
                  "incomeFrmlType": i == 0
                      ? "S" : i == 1
                      ? "+" : i == 2
                      ? "=" : i == 3
                      ? "-" : i == 4
                      ? "=" : i == 5
                      ? "-" : i == 6
                      ? "-" : i == 7
                      ? "=" : i == 8
                      ? "-" : i == 9
                      ? "=" : i == 10
                      ? "-" : i == 11
                      ? "=" : i == 12
                      ? ""
                      : "",
                  "incomeType": i == 0
                      ? "001" : i == 1
                      ? "002" : i == 2
                      ? "003" : i == 3
                      ? "004" : i == 4
                      ? "005" : i == 5
                      ? "006" : i == 6
                      ? "007" : i == 7
                      ? "008" : i == 8
                      ? "009" : i == 9
                      ? "010" : i == 10
                      ? "011" : i == 11
                      ? "017" : i == 12
                      ? ""
                      : "",
                  "incomeValue": i == 0
                      ? _providerIncome.controllerMonthlyIncome.text : i == 1
                      ? _providerIncome.controllerOtherIncome.text : i == 2
                      ? _providerIncome.controllerTotalIncome.text : i == 3
                      ? _providerIncome.controllerCostOfGoodsSold.text : i == 4
                      ? _providerIncome.controllerGrossProfit.text : i == 5
                      ? _providerIncome.controllerOperatingCosts.text : i == 6
                      ? _providerIncome.controllerOtherCosts.text : i == 7
                      ? _providerIncome.controllerNetProfitBeforeTax.text : i == 8
                      ? _providerIncome.controllerTax.text : i == 9
                      ? _providerIncome.controllerNetProfitAfterTax.text : i == 10
                      ? _providerIncome.controllerCostOfLiving.text : i == 11
                      ? _providerIncome.controllerRestIncome.text : i == 12
                      ? _providerIncome.controllerSpouseIncome.text
                      : _providerIncome.controllerOtherInstallments.text,
                  "dataStatus": "ACTIVE",
                  "creationalSpecification": {
                    "createdAt": DateTime.now(),
                    "createdBy": _preferences.getString("username"),
                    "modifiedAt": DateTime.now(),
                    "modifiedBy": _preferences.getString("username"),
                  },
                }
            );
          }
        }
      } else {
        if(_providerInfoNasabah.maritalStatusSelected.id != "01") {
          for(int i=0; i < 5; i++) {
            _customerIncomes.add(
                {
                  "customerIncomeID": "NEW",
                  "incomeFrmlType": i == 0
                      ? "S" : i == 1
                      ? "+" : i == 2
                      ? "=" : i == 3
                      ? "-" : i == 4
                      ? "="
                      : "",
                  "incomeType": i == 0
                      ? "001" : i == 1
                      ? "002" : i == 2
                      ? "003" : i == 3
                      ? "011" : i == 4
                      ? "017"
                      : "",
                  "incomeValue": i == 0
                      ? _providerIncome.controllerIncome.text : i == 1
                      ? _providerIncome.controllerOtherIncome.text : i == 2
                      ? _providerIncome.controllerTotalIncome.text : i == 3
                      ? _providerIncome.controllerCostOfLiving.text : i == 4
                      ? _providerIncome.controllerRestIncome.text
                      : _providerIncome.controllerOtherInstallments.text,
                  "dataStatus": "ACTIVE",
                  "creationalSpecification": {
                    "createdAt": DateTime.now(),
                    "createdBy": _preferences.getString("username"),
                    "modifiedAt": DateTime.now(),
                    "modifiedBy": _preferences.getString("username"),
                  },
                }
            );
          }
        } else {
          for(int i=0; i < 6; i++) {
            _customerIncomes.add(
                {
                  "customerIncomeID": "NEW",
                  "incomeFrmlType": i == 0
                      ? "S" : i == 1
                      ? "+" : i == 2
                      ? "+" : i == 3
                      ? "=" : i == 4
                      ? "-" : i == 5
                      ? "="
                      : "",
                  "incomeType": i == 0
                      ? "001" : i == 1
                      ? "002" : i == 2
                      ? "?" : i == 3
                      ? "003" : i == 4
                      ? "011" : i == 5
                      ? "017"
                      : "",
                  "incomeValue": i == 0
                      ? _providerIncome.controllerIncome.text : i == 1
                      ? _providerIncome.controllerSpouseIncome.text : i == 2
                      ? _providerIncome.controllerOtherIncome.text : i == 3
                      ? _providerIncome.controllerTotalIncome.text : i == 4
                      ? _providerIncome.controllerCostOfLiving.text : i == 5
                      ? _providerIncome.controllerRestIncome.text
                      : _providerIncome.controllerOtherInstallments.text,
                  "dataStatus": "ACTIVE",
                  "creationalSpecification": {
                    "createdAt": DateTime.now(),
                    "createdBy": _preferences.getString("username"),
                    "modifiedAt": DateTime.now(),
                    "modifiedBy": _preferences.getString("username"),
                  },
                }
            );
          }
        }
      }
    } else {
      for(int i=0; i < 10; i++) {
        _customerIncomes.add(
            {
              "customerIncomeID": "NEW",
              "incomeFrmlType": i == 0
                  ? "S" : i == 1
                  ? "+" : i == 2
                  ? "=" : i == 3
                  ? "-" : i == 4
                  ? "=" : i == 5
                  ? "-" : i == 6
                  ? "-" : i == 7
                  ? "=" : i == 8
                  ? "-" : i == 9
                  ? "="
                  : "",
              "incomeType": i == 0
                  ? "001" : i == 1
                  ? "002" : i == 2
                  ? "003" : i == 3
                  ? "004" : i == 4
                  ? "005" : i == 5
                  ? "006" : i == 6
                  ? "007" : i == 7
                  ? "008" : i == 8
                  ? "" : i == 9
                  ? ""
                  : "",
              "incomeValue": i == 0
                  ? _providerIncomeCompany.controllerMonthlyIncome.text : i == 1
                  ? _providerIncomeCompany.controllerOtherIncome.text : i == 2
                  ? _providerIncomeCompany.controllerTotalIncome.text : i == 3
                  ? _providerIncomeCompany.controllerCostOfRevenue.text : i == 4
                  ? _providerIncomeCompany.controllerGrossProfit.text : i == 5
                  ? _providerIncomeCompany.controllerOperatingCosts.text : i == 6
                  ? _providerIncomeCompany.controllerOtherCosts.text : i == 7
                  ? _providerIncomeCompany.controllerNetProfitBeforeTax.text : i == 8
                  ? _providerIncomeCompany.controllerTax.text : i == 9
                  ? _providerIncomeCompany.controllerNetProfitAfterTax.text
                  : _providerIncomeCompany.controllerOtherInstallments.text,
              "dataStatus": "ACTIVE",
              "creationalSpecification": {
                "createdAt": DateTime.now(),
                "createdBy": _preferences.getString("username"),
                "modifiedAt": DateTime.now(),
                "modifiedBy": _preferences.getString("username"),
              },
            }
        );
      }
    }

    var _detailAddresses = [];
    if(_listOID.customerType != "COM") {
      for (int i = 0; i <_providerInfoAlamat.listAlamatKorespondensi.length; i++) {
        _detailAddresses.add({
          "addressID": "NEW",
          "addressSpecification": {
            "koresponden": _providerInfoAlamat.listAlamatKorespondensi[i].isCorrespondence,
            "matrixAddr": _providerInfoAlamat.listAlamatKorespondensi[i].address,
            "address": _providerInfoAlamat.listAlamatKorespondensi[i].address,
            "rt": _providerInfoAlamat.listAlamatKorespondensi[i].rt,
            "rw": _providerInfoAlamat.listAlamatKorespondensi[i].rw,
            "provinsiID": _providerInfoAlamat.listAlamatKorespondensi[i].kelurahanModel.PROV_ID,
            "kabkotID": _providerInfoAlamat.listAlamatKorespondensi[i].kelurahanModel.KABKOT_ID,
            "kecamatanID": _providerInfoAlamat.listAlamatKorespondensi[i].kelurahanModel.KEC_ID,
            "kelurahanID": _providerInfoAlamat.listAlamatKorespondensi[i].kelurahanModel.KEL_ID,
            "zipcode": _providerInfoAlamat.listAlamatKorespondensi[i].kelurahanModel.ZIPCODE
          },
          "contactSpecification": {
            "telephoneArea1": _providerInfoAlamat.listAlamatKorespondensi[i].phone,
            "telephone1": _providerInfoAlamat.listAlamatKorespondensi[i].areaCode,
            "telephoneArea2": "",
            "telephone2": null,
            "faxArea": null,
            "fax": null,
            "handphone": null,
            "email": null
          },
          "addressType": _providerInfoAlamat.listAlamatKorespondensi[i].jenisAlamatModel.KODE,
          "creationalSpecification": {
            "createdAt": DateTime.now(),
            "createdBy": _preferences.getString("username"),
            "modifiedAt": DateTime.now(),
            "modifiedBy": _preferences.getString("username"),
          },
          "status": "ACTIVE",
          "foreignBusinessID": ""
        });
      }
    } else {
      for (int i = 0; i < _providerInfoAlamatCompany.listAlamatKorespondensi.length; i++) {
        _detailAddresses.add({
          "addressID": "NEW",
          "addressSpecification": {
            "koresponden": _providerInfoAlamatCompany.listAlamatKorespondensi[i].isCorrespondence,
            "matrixAddr": _providerInfoAlamatCompany.listAlamatKorespondensi[i].address,
            "address": _providerInfoAlamatCompany.listAlamatKorespondensi[i].address,
            "rt": _providerInfoAlamatCompany.listAlamatKorespondensi[i].rt,
            "rw": _providerInfoAlamatCompany.listAlamatKorespondensi[i].rw,
            "provinsiID": _providerInfoAlamatCompany.listAlamatKorespondensi[i].kelurahanModel.PROV_ID,
            "kabkotID": _providerInfoAlamatCompany.listAlamatKorespondensi[i].kelurahanModel.KABKOT_ID,
            "kecamatanID": _providerInfoAlamatCompany.listAlamatKorespondensi[i].kelurahanModel.KEC_ID,
            "kelurahanID": _providerInfoAlamatCompany.listAlamatKorespondensi[i].kelurahanModel.KEL_ID,
            "zipcode": _providerInfoAlamatCompany.listAlamatKorespondensi[i].kelurahanModel.ZIPCODE
          },
          "contactSpecification": {
            "telephoneArea1": _providerInfoAlamatCompany.listAlamatKorespondensi[i].phoneArea1,
            "telephone1": _providerInfoAlamatCompany.listAlamatKorespondensi[i].phone1,
            "telephoneArea2": _providerInfoAlamatCompany.listAlamatKorespondensi[i].phoneArea2,
            "telephone2": _providerInfoAlamatCompany.listAlamatKorespondensi[i].phone2,
            "faxArea": _providerInfoAlamatCompany.listAlamatKorespondensi[i].faxArea,
            "fax": _providerInfoAlamatCompany.listAlamatKorespondensi[i].fax,
            "handphone": null,
            "email": null
          },
          "addressType": _providerInfoAlamat.listAlamatKorespondensi[i].jenisAlamatModel.KODE,
          "creationalSpecification": {
            "createdAt": DateTime.now(),
            "createdBy": _preferences.getString("username"),
            "modifiedAt": DateTime.now(),
            "modifiedBy": _preferences.getString("username"),
          },
          "status": "ACTIVE",
          "foreignBusinessID": "IP001"
        });
      }
    }

    var _orderSupportingDocuments = [];
    if(_providerDocument.listInfoDocument.isNotEmpty) {
      for(int i=0; i < _providerDocument.listInfoDocument.length; i++) {
        _orderSupportingDocuments.add({
          "orderSupportingDocumentID": "NEW",
          "documentSpecification": {
            "documentTypeID": _providerDocument.listInfoDocument[i].documentType.docTypeId,
            "isMandatory": false,
            "isDisplay": false,
            "isUnit": false,
            "receivedDate": _providerDocument.listInfoDocument[i].date,
          },
          "fileSpecification": {
            "fileName": _providerDocument.listInfoDocument[i].documentDetail.fileName,
            "fileHeaderID": "",
            "uploadDate": DateTime.now()
          },
          "creationalSpecification": {
            "createdAt": DateTime.now(),
            "createdBy": _preferences.getString("username"),
            "modifiedAt": DateTime.now(),
            "modifiedBy": _preferences.getString("username"),
          },
          "status": "ACTIVE"
        });
      }
    }

    var _installmentDetails = [];
    if(_providerCreditStructure.installmentTypeSelected.id == "06") {
      // Stepping
      if(_providerCreditStructureType.listInfoCreditStructureStepping.isNotEmpty) {
        for(int i=0; i < _providerCreditStructureType.listInfoCreditStructureStepping.length; i++) {
          _installmentDetails.add({
            "installmentID": null,
            "installmentNumber": 0,
            "percentage": _providerCreditStructureType.listInfoCreditStructureStepping[i].controllerPercentage.text != "" ? double.parse(_providerCreditStructureType.listInfoCreditStructureStepping[i].controllerPercentage.text.replaceAll(",", "")) : 0,
            "amount": _providerCreditStructureType.listInfoCreditStructureStepping[i].controllerValue.text != "" ? double.parse(_providerCreditStructureType.listInfoCreditStructureStepping[i].controllerValue.text.replaceAll(",", "")) : 0,
            "creationalSpecification": {
              "createdAt": DateTime.now(),
              "createdBy": _preferences.getString("username"),
              "modifiedAt": DateTime.now(),
              "modifiedBy": _preferences.getString("username"),
            },
            "status": "ACTIVE"
          });
        }
      }
    } else if(_providerCreditStructure.installmentTypeSelected.id == "07") {
      // Irreguler
      if(_providerCreditStructureType.listInfoCreditStructureIrregularModel.isNotEmpty) {
        for(int i=0; i < _providerCreditStructureType.listInfoCreditStructureIrregularModel.length; i++) {
          _installmentDetails.add({
            "installmentID": "NEW",
            "installmentNumber": i+1,
            "percentage": 0,
            "amount": _providerCreditStructureType.listInfoCreditStructureIrregularModel[i].controller.text != "" ? double.parse(_providerCreditStructureType.listInfoCreditStructureIrregularModel[i].controller.text.replaceAll(",", "")) : 0,
            "creationalSpecification": {
              "createdAt": DateTime.now(),
              "createdBy": _preferences.getString("username"),
              "modifiedAt": DateTime.now(),
              "modifiedBy": _preferences.getString("username"),
            },
            "status": "ACTIVE"
          });
        }
      }
    }

    var _guarantorIndividuals = [];
    if(_providerGuarantor.listGuarantorIndividual.isNotEmpty) {
      for(int i=0; i < _providerGuarantor.listGuarantorIndividual.length; i++) {
        _guarantorIndividuals.add(
            {
              "guarantorIndividualID": "NEW",
              "relationshipStatus": _providerGuarantor.listGuarantorIndividual[i].relationshipStatusModel.PARA_FAMILY_TYPE_ID,
              "dedupScore": 0.0,
              "guarantorIdentity": {
                "identityType": _providerGuarantor.listGuarantorIndividual[i].identityModel.id,
                "identityNumber": _providerGuarantor.listGuarantorIndividual[i].identityNumber,
                "identityName": _providerGuarantor.listGuarantorIndividual[i].fullNameIdentity,
                "fullName": _providerGuarantor.listGuarantorIndividual[i].fullName,
                "alias": "",
                "title": "",
                "dateOfBirth": _providerGuarantor.listGuarantorIndividual[i].birthDate,
                "placeOfBirth": _providerGuarantor.listGuarantorIndividual[i].birthPlaceIdentity1,
                "placeOfBirthKabKota": _providerGuarantor.listGuarantorIndividual[i].birthPlaceIdentity2,
                "gender": _providerGuarantor.listGuarantorIndividual[i].gender,
                "identityActiveStart": null,
                "identityActiveEnd": null,
                "isLifetime": null,
                "religion": null,
                "occupationID": null,
                "positionID": null,
                "maritalStatusID": null
              },
              "guarantorContact": {
                "telephoneArea1": null,
                "telephone1": null,
                "telephoneArea2": null,
                "telephone2": null,
                "faxArea": null,
                "fax": null,
                "handphone": _providerGuarantor.listGuarantorIndividual[i].cellPhoneNumber,
                "email": null,
                "noWa": null,
                "noWa2": null,
                "noWa3": null
              },
              "guarantorIndividualAddresses": [],
              "creationalSpecification": {
                "createdAt": DateTime.now(),
                "createdBy": _preferences.getString("username"),
                "modifiedAt": DateTime.now(),
                "modifiedBy": _preferences.getString("username"),
              },
              "status": "ACTIVE"
            }
        );
      }
    }

    var _guarantorCorporates = [];
    if(_providerGuarantor.listGuarantorCompany.isNotEmpty) {
      for(int i=0; i < _providerGuarantor.listGuarantorCompany.length; i++) {
        _guarantorCorporates.add(
            {
              "guarantorCorporateID": "NEW",
              "dedupScore": 0.0,
              "businessPermitSpecification": {
                "institutionType": _providerGuarantor.listGuarantorCompany[i].typeInstitutionModel.PARA_ID,
                "profile": _listOID.customerType != "COM" ? _providerGuarantor.listGuarantorCompany[i].profilModel.id : "",
                "fullName": _providerGuarantor.listGuarantorCompany[i].institutionName,
                "deedOfIncorporationNumber": null,
                "deedEndDate": null,
                "siupNumber": null,
                "siupStartDate": null,
                "tdpNumber": null,
                "tdpStartDate": null,
                "establishmentDate": _listOID.customerType != "COM" ? "" : _providerGuarantor.listGuarantorCompany[i].establishDate,
                "isCompany": false,
                "authorizedCapital": 0,
                "paidCapital": 0
              },
              "businessSpecification": {
                "economySector": null,
                "businessField": null,
                "locationStatus": null,
                "businessLocation": null,
                "employeeTotal": 0,
                "bussinessLengthInMonth": 0,
                "totalBussinessLengthInMonth": 0
              },
              "customerNpwpSpecification": {
                "hasNpwp": true,
                "npwpAddress": null,
                "npwpFullname": null,
                "npwpNumber": _providerGuarantor.listGuarantorCompany[i].npwp,
                "npwpType": null,
                "pkpIdentifier": null
              },
              "guarantorCorporateAddresses": [],
              "creationalSpecification": {
                "createdAt": DateTime.now(),
                "createdBy": _preferences.getString("username"),
                "modifiedAt": DateTime.now(),
                "modifiedBy": _preferences.getString("username"),
              },
              "status": "ACTIVE"
            }
        );
      }
    }

    var _orderKaroseris = [];
    if(_providerKaroseri.listFormKaroseriObject.isNotEmpty) {
      for(int i=0; i < _providerKaroseri.listFormKaroseriObject.length; i++) {
        _orderKaroseris.add(
            {
              "orderKaroseriID": "NEW",
              "isPksKaroseri": _providerKaroseri.listFormKaroseriObject[i].PKSKaroseri,
              "karoseriCompanyID": _providerKaroseri.listFormKaroseriObject[i].company.id,
              "karoseriID": _providerKaroseri.listFormKaroseriObject[i].karoseri.kode,
              "karoseriPrice": _providerKaroseri.listFormKaroseriObject[i].price,
              "karoseriQty": _providerKaroseri.listFormKaroseriObject[i].jumlahKaroseri,
              "karoseriTotalPrice": _providerKaroseri.listFormKaroseriObject[i].totalPrice,
              "creationalSpecification": {
                "createdAt": DateTime.now(),
                "createdBy": _preferences.getString("username"),
                "modifiedAt": DateTime.now(),
                "modifiedBy": _preferences.getString("username"),
              },
              "status": "ACTIVE"
            }
        );
      }
    }

    var _orderWmps = [];
    if(_providerWmp.listWMP.isNotEmpty) {
      for(int i=0; i < _providerWmp.listWMP.length; i++) {
        _orderWmps.add(
          {
            "orderWmpID": "NEW",
            "wmpNumber": _providerWmp.listWMP[i].noProposal,
            "wmpType": _providerWmp.listWMP[i].type,
            "wmpJob": "",
            "wmpSubsidyTypeID": _providerWmp.listWMP[i].typeSubsidi,
            "maxRefundAmount": _providerWmp.listWMP[i].amount,
            "status": "ACTIVE",
            "creationalSpecification": {
              "createdAt": DateTime.now(),
              "createdBy": _preferences.getString("username"),
              "modifiedAt": DateTime.now(),
              "modifiedBy": _preferences.getString("username"),
            },
          },
        );
      }
    }

    var _orderSubsidies = [];
    if(_providerSubsidy.listInfoCreditSubsidy.isNotEmpty) {
      for(int i=0; i < _providerSubsidy.listInfoCreditSubsidy.length; i++) {
        _orderSubsidies.add(
            {
              "orderSubsidyID": "NEW",
              "subsidyProviderID": "1",
              "subsidyTypeID": _providerSubsidy.listInfoCreditSubsidy[i].type != null ? _providerSubsidy.listInfoCreditSubsidy[i].type.id : "",
              "subsidyMethodTypeID": _providerSubsidy.listInfoCreditSubsidy[i].cuttingMethod != null ? _providerSubsidy.listInfoCreditSubsidy[i].cuttingMethod.id : "",
              "refundAmount": _providerSubsidy.listInfoCreditSubsidy[i].claimValue != null ? double.parse(_providerSubsidy.listInfoCreditSubsidy[i].claimValue.replaceAll(",", "")) : 0,
              "effectiveRate": _providerSubsidy.listInfoCreditSubsidy[i].rateBeforeEff != null ? double.parse(_providerSubsidy.listInfoCreditSubsidy[i].rateBeforeEff.replaceAll(",", "")) : 0,
              "flatRate": _providerSubsidy.listInfoCreditSubsidy[i].rateBeforeFlat != null ? double.parse(_providerSubsidy.listInfoCreditSubsidy[i].rateBeforeFlat.replaceAll(",", "")) : 0,
              "dpReal": 1,
              "totalInstallment": _providerSubsidy.listInfoCreditSubsidy[i].totalInstallment != null ? double.parse(_providerSubsidy.listInfoCreditSubsidy[i].totalInstallment.replaceAll(",", "")) : 0,
              "interestRate": 0,
              "creationalSpecification": {
                "createdAt": DateTime.now(),
                "createdBy": _preferences.getString("username"),
                "modifiedAt": DateTime.now(),
                "modifiedBy": _preferences.getString("username"),
              },
              "status": "ACTIVE",
              "orderSubsidyDetails": [
                for(int i=0; i < _providerSubsidyDetail.listDetail.length; i++) {
                  {
                    "orderSubsidyDetailID": "NEW",
                    "installmentNumber": _providerSubsidyDetail.listDetail[i].installmentIndex != null ? double.parse(_providerSubsidyDetail.listDetail[i].installmentIndex.replaceAll(",", "")) : 0,
                    "installmentAmount": _providerSubsidyDetail.listDetail[i].installmentSubsidy != null ? double.parse(_providerSubsidyDetail.listDetail[i].installmentSubsidy.replaceAll(",", "")) : 0,
                    "creationalSpecification": {
                      "createdAt": DateTime.now(),
                      "createdBy": _preferences.getString("username"),
                      "modifiedAt": DateTime.now(),
                      "modifiedBy": _preferences.getString("username"),
                    },
                    "status": "ACTIVE"
                  }
                }
              ]
            }
        );
      }
    }

    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var _body = jsonEncode({
      "applicationDTO": {
        "customerDTO": {
          "customerID": "NEW",
          "customerType":  _listOID.customerType != "COM" ? "PER" : "COM",
          "customerSpecification": {
            "customerNPWP": {
              "npwpAddress": _providerInfoNasabah.radioValueIsHaveNPWP == 1 ? _providerInfoNasabah.controllerAlamatSesuaiNPWP.text : _providerInfoAlamat.listAlamatKorespondensi[0].address,
              "npwpFullname": _providerInfoNasabah.radioValueIsHaveNPWP == 1 ? _providerInfoNasabah.controllerNamaSesuaiNPWP.text  : _providerInfoNasabah.controllerNamaLengkapSesuaiIdentitas.text,
              "npwpNumber": _providerInfoNasabah.radioValueIsHaveNPWP == 1 ? _providerInfoNasabah.controllerNoNPWP.text : 00000000000000,
              "npwpType": null,
              "hasNpwp": _providerInfoNasabah.radioValueIsHaveNPWP == 1 ? true : false,
              "pkpIdentifier": null
            },
            "detailAddresses": _detailAddresses,
            "customerGuarantor": {
              "isGuarantor": _providerGuarantor.radioValueIsWithGuarantor == 0 ? true : false,
              "guarantorCorporates": _guarantorCorporates,
              "guarantorIndividuals": _guarantorIndividuals
            }
          },
          "customerIndividualDTO": [
            {
              "customerIndividualID": "NEW",
              "numberOfDependents": 0,
              "groupCustomer": _providerInfoNasabah.gcModel.id,
              "statusRelationship": _providerInfoNasabah.maritalStatusSelected.id,
              "educationID": _providerInfoNasabah.educationSelected.id,
              "customerIdentity": {
                "identityType": _providerInfoNasabah.identitasModel.id,
                "identityNumber": _providerInfoNasabah.controllerNoIdentitas.text,
                "identityName": _providerInfoNasabah.controllerNamaLengkapSesuaiIdentitas,
                "fullName": _providerInfoNasabah.controllerNamaLengkap.text,
                "alias": null,
                "title": null,
                "dateOfBirth": _providerInfoNasabah.controllerTglLahir.text,
                "placeOfBirth": _providerInfoNasabah.controllerTempatLahirSesuaiIdentitas.text,
                "placeOfBirthKabKota": _providerInfoNasabah.birthPlaceSelected.KABKOT_ID,
                "gender": _providerInfoNasabah.radioValueGender,
                "identityActiveStart": _listOID.customerType != "COM" ? _providerInfoNasabah.controllerTglIdentitas.text != "" ? _providerInfoNasabah.controllerTglIdentitas.text : "" : "",
                "identityActiveEnd": _listOID.customerType != "COM" ? _providerInfoNasabah.valueCheckBox == true ? "" : _providerInfoNasabah.controllerIdentitasBerlakuSampai.text != "" ? _providerInfoNasabah.controllerIdentitasBerlakuSampai.text : "" : "",
                "religion": _providerInfoNasabah.religionSelected.id,
                "occupationID": _providerFoto.occupationSelected.KODE,
                "positionID": null,
                "isLifetime": _providerInfoNasabah.valueCheckBox == true ? true : false
              },
              "customerContact": {
                "telephoneArea1": "",
                "telephone1": "",
                "telephoneArea2": null,
                "telephone2": null,
                "faxArea": null,
                "fax": null,
                "handphone": _providerInfoNasabah.controllerNoHp.text != "" ?  _providerInfoNasabah.controllerNoHp.text : "",
                "email": _providerInfoNasabah.controllerEmail.text != "" ? _providerInfoNasabah.controllerEmail.text : ""
              },
              "customerOccupation": [
                {
                  "customerOccupationID": "NEW",
                  "occupationID": _listOID.customerType != "COM" ? _providerFoto.occupationSelected.KODE : "",
                  "businessName": _listOID.customerType != "COM" ? _providerOccupation.controllerBusinessName.text : "",
                  "businessType": _listOID.customerType != "COM" ? _providerRincian.typeInstitutionSelected.PARA_ID : "",
                  "professionType": _listOID.customerType != "COM" ? _providerOccupation.occupationSelected.KODE == "08" ? _providerOccupation.professionTypeModelSelected.id : "" : "",
                  "pepType": _providerFoto.occupationSelected.KODE != "05" || _providerFoto.occupationSelected.KODE != "07" || _providerFoto.occupationSelected.KODE != "08" ? _providerOccupation.pepModelSelected.KODE : null,
                  "employeeStatus": _listOID.customerType != "COM" ? _providerOccupation.employeeStatusModelSelected.id : "",
                  "businessSpecification": {
                    "economySector": _providerOccupation.sectorEconomicModelSelected.KODE,
                    "businessField": _listOID.customerType != "COM" ? _providerOccupation.businessFieldModelSelected.KODE : "",
                    "locationStatus": _listOID.customerType != "COM" ? _providerOccupation.statusLocationModelSelected.id : "",
                    "businessLocation": _listOID.customerType != "COM" ? _providerOccupation.businessLocationModelSelected.id : "",
                    "employeeTotal": _listOID.customerType != "COM" ? double.parse(_providerOccupation.controllerEmployeeTotal.text.replaceAll(",", "")) : 0,
                    "bussinessLengthInMonth": _listOID.customerType != "COM" ? double.parse(_providerOccupation.controllerEstablishedDate.text.replaceAll(",", "")) : 0,
                    "totalBussinessLengthInMonth": _listOID.customerType != "COM" ? _providerOccupation.controllerTotalEstablishedDate.text : 0
                  },
                  "creationalSpecification": {
                    "createdAt": DateTime.now(),
                    "createdBy": _preferences.getString("username"),
                    "modifiedAt": DateTime.now(),
                    "modifiedBy": _preferences.getString("username"),
                  },
                  "status": "ACTIVE"
                }
              ],
              "customerIndividualAdditionalSpecification": null,
              "customerFamilies": [
                if(_listOID.customerType != "COM") {
                  {
                    "familyInfoID": "NEW",
                    "relationshipStatus": _listOID.customerType != "COM" ? _providerIbu.relationshipStatusSelected.PARA_FAMILY_TYPE_ID : "",
                    "familyIdentity": {
                      "identityType": _listOID.customerType != "COM" ? _providerIbu.identitySelected.id : "",
                      "identityNumber": _listOID.customerType != "COM" ? _providerIbu.controllerNoIdentitas.text : "",
                      "identityName": _listOID.customerType != "COM" ? _providerIbu.controllerNamaIdentitas.text : "",
                      "fullName": _listOID.customerType != "COM" ? _providerIbu.controllerNamaLengkapdentitas.text : "",
                      "alias": null,
                      "title": null,
                      "dateOfBirth": _listOID.customerType != "COM" ? _providerIbu.controllerTglLahir.text : "",
                      "placeOfBirth": _listOID.customerType != "COM" ? _providerIbu.controllerTempatLahirSesuaiIdentitas.text : "",
                      "placeOfBirthKabKota": _listOID.customerType != "COM" ? _providerIbu.controllerTempatLahirSesuaiIdentitasLOV.text : "",
                      "gender": "02",
                      "identityActiveStart": null,
                      "identityActiveEnd": null,
                      "religion": null,
                      "occupationID": "0",
                      "positionID": null
                    },
                    "creationalSpecification": {
                      "createdAt": DateTime.now(),
                      "createdBy": _preferences.getString("username"),
                      "modifiedAt": DateTime.now(),
                      "modifiedBy": _preferences.getString("username"),
                    },
                    "contactSpecification": {
                      "telephoneArea1": _listOID.customerType != "COM" ? _providerIbu.controllerKodeArea.text : "",
                      "telephone1": _listOID.customerType != "COM" ? _providerIbu.controllerTlpn.text : "",
                      "telephoneArea2": null,
                      "telephone2": null,
                      "faxArea": null,
                      "fax": null,
                      "handphone": _listOID.customerType != "COM" ? _providerIbu.controllerNoHp.text : "",
                      "email": null
                    },
                    "dedupScore": 0.0,
                    "status": "ACTIVE"
                  },
                  if(_providerKeluarga.listFormInfoKel.isNotEmpty) {
                    for(int i=0; i<_providerKeluarga.listFormInfoKel.length; i++){
                      {
                        "familyInfoID": "NEW",
                        "relationshipStatus": _listOID.customerType != "COM" ? _providerKeluarga.listFormInfoKel[i].relationshipStatusModel.PARA_FAMILY_TYPE_ID : "",
                        "familyIdentity": {
                          "identityType": _listOID.customerType != "COM" ? _providerKeluarga.listFormInfoKel[i].identityModel.id : "",
                          "identityNumber": _listOID.customerType != "COM" ? _providerKeluarga.listFormInfoKel[i].noIdentitas : "",
                          "identityName": _listOID.customerType != "COM" ? _providerKeluarga.listFormInfoKel[i].namaLengkapSesuaiIdentitas : "",
                          "fullName": _listOID.customerType != "COM" ? _providerKeluarga.listFormInfoKel[i].namaLengkap : "",
                          "alias": null,
                          "title": null,
                          "dateOfBirth": _listOID.customerType != "COM" ? _providerKeluarga.listFormInfoKel[i].birthDate : "",
                          "placeOfBirth": _listOID.customerType != "COM" ? _providerKeluarga.listFormInfoKel[i].tmptLahirSesuaiIdentitas : "",
                          "placeOfBirthKabKota": _listOID.customerType != "COM" ? _providerKeluarga.listFormInfoKel[i].birthPlaceModel.KABKOT_ID : "",
                          "gender": _listOID.customerType != "COM" ? _providerKeluarga.listFormInfoKel[i].gender == "01" ? "Laki Laki" : "Perempuan" : "",
                          "identityActiveStart": null,
                          "identityActiveEnd": null,
                          "religion": null,
                          "occupationID": null,
                          "positionID": null
                        },
                        "creationalSpecification": {
                          "createdAt": DateTime.now(),
                          "createdBy": _preferences.getString("username"),
                          "modifiedAt": DateTime.now(),
                          "modifiedBy": _preferences.getString("username"),
                        },
                        "contactSpecification": {
                          "telephoneArea1": _listOID.customerType != "COM" ? _providerKeluarga.listFormInfoKel[i].kodeArea : "",
                          "telephone1": _listOID.customerType != "COM" ? _providerKeluarga.listFormInfoKel[i].noTlpn : "",
                          "telephoneArea2": null,
                          "telephone2": null,
                          "faxArea": null,
                          "fax": null,
                          "handphone": _listOID.customerType != "COM" ? _providerKeluarga.listFormInfoKel[i].noHp : "",
                          "email": null
                        },
                        "dedupScore": 0.0,
                        "status": "ACTIVE"
                      }
                    }
                  }
                }
              ],
              "creationalSpecification": {
                "createdAt": DateTime.now(),
                "createdBy": _preferences.getString("username"),
                "modifiedAt": DateTime.now(),
                "modifiedBy": _preferences.getString("username"),
              },
              "status": "ACTIVE"
            }
          ],
          "customerCorporateDTO": [
            {
              "customerCorporateID": "NEW",
              "businessSpecification": {
                "economySector": _listOID.customerType != "COM" ? "" : _providerRincian.sectorEconomicModelSelected.KODE,
                "businessField": _listOID.customerType != "COM" ? "" : _providerRincian.businessFieldModelSelected.KODE,
                "locationStatus": _listOID.customerType != "COM" ? "" : _providerRincian.locationStatusSelected.id,
                "businessLocation": _listOID.customerType != "COM" ? "" : _providerRincian.businessLocationSelected.id,
                "employeeTotal": _listOID.customerType != "COM" ? 0 : double.parse(_providerRincian.controllerTotalEmployees.text.replaceAll(",", "")),
                "bussinessLengthInMonth": _listOID.customerType != "COM" ? 0 : double.parse(_providerRincian.controllerLamaUsahaBerjalan.text.replaceAll(",", "")),
                "totalBussinessLengthInMonth": _listOID.customerType != "COM" ? 0 : double.parse(_providerRincian.controllerTotalLamaUsahaBerjalan.text.replaceAll(",", "")),
              },
              "businessPermitSpecification": {
                "institutionType": _listOID.customerType != "COM" ? "" : _providerRincian.typeInstitutionSelected.PARA_ID,
                "profile": _listOID.customerType != "COM" ? "" : _providerRincian.profilSelected.id,
                "fullName": _listOID.customerType != "COM" ? "" : _providerRincian.controllerInstitutionName.text,
                "deedOfIncorporationNumber": null,
                "deedEndDate": null,
                "siupNumber": null,
                "siupStartDate": null,
                "tdpNumber": null,
                "tdpStartDate": null,
                "establishmentDate": _listOID.customerType != "COM" ? "" : _providerRincian.controllerDateEstablishment.text,
                "isCompany": false,
                "authorizedCapital": 0,
                "paidCapital": 0
              },
              "managementPIC": {
                "picIdentity": {
                  "identityType": _listOID.customerType != "COM" ? "" : _providerManajemenPIC.typeIdentitySelected.id,
                  "identityNumber": _listOID.customerType != "COM" ? "" : _providerManajemenPIC.controllerIdentityNumber.text,
                  "identityName": _listOID.customerType != "COM" ? "" : _providerManajemenPIC.controllerFullNameIdentity.text,
                  "fullName": _listOID.customerType != "COM" ? "" : _providerManajemenPIC.controllerFullName,
                  "alias": null,
                  "title": "",
                  "dateOfBirth": _listOID.customerType != "COM" ? "" : _providerManajemenPIC.controllerBirthOfDate.text,
                  "placeOfBirth": _listOID.customerType != "COM" ? "" : _providerManajemenPIC.controllerPlaceOfBirthIdentity.text,
                  "placeOfBirthKabKota": _listOID.customerType != "COM" ? "" : _providerManajemenPIC.birthPlaceSelected.KABKOT_ID,
                  "gender": null,
                  "identityActiveStart": null,
                  "identityActiveEnd": null,
                  "isLifetime": null,
                  "religion": null,
                  "occupationID": null,
                  "positionID": "02",
                  "maritalStatusID": "02"
                },
                "picContact": {
                  "telephoneArea1": null,
                  "telephone1": null,
                  "telephoneArea2": null,
                  "telephone2": null,
                  "faxArea": null,
                  "fax": null,
                  "handphone": _listOID.customerType != "COM" ? "" : _providerManajemenPIC.controllerHandphoneNumber.text,
                  "email": _listOID.customerType != "COM" ? "" : _providerManajemenPIC.controllerEmail.text,
                  "noWa": null,
                  "noWa2": null,
                  "noWa3": null
                },
                "picAddresses": [],
                "shareholding": 0
              },
              "shareholdersCorporates": [],
              "shareholdersIndividuals": [],
              "dedupScore": 0.0,
              "status": "ACTIVE",
              "creationalSpecification": {
                "createdAt": DateTime.now(),
                "createdBy": _preferences.getString("username"),
                "modifiedAt": DateTime.now(),
                "modifiedBy": _preferences.getString("username"),
              },
            }
          ],
          "customerAdditionalInformation": null,
          "customerProcessingCompletion": {
            "lastKnownCustomerState": "CUSTOMER_GUARANTOR",
            "nextKnownCustomerState": "CUSTOMER_GUARANTOR"
          },
          "creationalSpecification": {
            "createdAt": DateTime.now(),
            "createdBy": _preferences.getString("username"),
            "modifiedAt": DateTime.now(),
            "modifiedBy": _preferences.getString("username"),
          },
          "status": "ACTIVE",
          "obligorID": "",
          "exposureInformations": [
            {
              "exposureInformationID": "NEW",
              "obligorID": "",
              "groupObjectID": "",
              "osAr": 0,
              "arOnProcess": 0,
              "arNew": 0,
              "exposureOthers": 0,
              "exposureTotal": 0,
              "status": "ACTIVE",
              "creationalSpecification": {
                "createdAt": DateTime.now(),
                "createdBy": _preferences.getString("username"),
                "modifiedAt": DateTime.now(),
                "modifiedBy": _preferences.getString("username"),
              },
            }
          ],
          "customerIncomes": _customerIncomes
          // _listOID.customerType != "COM"
          //   ? _providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07"
          //     ? _providerInfoNasabah.maritalStatusSelected.id != "02"
          //       ? mappingCustomerIncomeWiraswastaBelumMenikah(context)
          //       : mappingCustomerIncomeWiraswastaMenikah(context)
          //     : _providerInfoNasabah.maritalStatusSelected.id != "02"
          //       ? mappingCustomerIncomeProfesionalBelumMenikah(context)
          //       : mappingCustomerIncomeProfesionalMenikah(context)
          //   : mappingCustomerIncomeCompany(context)
          // if(_listOID.customerType != "COM") {
          //   if(_providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07") {
          //     if(_providerInfoNasabah.maritalStatusSelected.id != "02") {
          //       mappingCustomerIncomeWiraswastaBelumMenikah(context)
          //     } else {
          //       mappingCustomerIncomeWiraswastaMenikah(context)
          //     }
          //   } else {
          //     if(_providerInfoNasabah.maritalStatusSelected.id != "02") {
          //       mappingCustomerIncomeProfesionalBelumMenikah(context)
          //     } else {
          //       mappingCustomerIncomeProfesionalMenikah(context)
          //     }
          //   }
          // } else {
          //   mappingCustomerIncomeCompany(context)
          // }
        },
        "orderDTO": {
          "orderID": orderId,
          "customerID": "NEW",
          "orderSpecification": {
            "isMayor": _preferences.getString("is_mayor") == "0" ? false : true,
            "hasFiducia": false,
            "applicationSource": "003",
            "orderDate": _providerApp.controllerOrderDate.text,
            "applicationDate": _providerApp.controllerOrderDate.text,
            "surveyAppointmentDate": _providerApp.controllerSurveyAppointmentDate.text,
            "orderType": "",
            "isOpenAccount": false,
            "accountFormNumber": null,
            "sentraCID": _preferences.getString("SentraD"),
            "unitCID": _preferences.getString("UnitD"),
            "initialRecommendation": "",
            "finalRecommendation": "",
            "brmsScoring": 0,
            "isInPlaceApproval": false,
            "isPkSigned": _providerApp.isSignedPK,
            "maxApprovalLevel": null,
            "jenisKonsep": _providerFoto.jenisKonsepSelected.id,
            "jumlahObjek": _providerApp.controllerTotalObject.text,
            "jenisProporsionalAsuransi": _providerApp.proportionalTypeOfInsuranceModelSelected != null ? _providerApp.proportionalTypeOfInsuranceModelSelected.kode : "",
            "unitKe": _providerApp.numberOfUnitSelected,
            "isWillingToAcceptInfo": false,
            "applicationContractNumber": null,
            "dealerNote": "",
            "userDealer": "",
            "orderDealer": "",
            "flagSourceMS2": "006",
            "orderSupportingDocuments": _orderSupportingDocuments,
            "collateralTypeID": null,
            "applicationDocVerStatus": null
          },
          "orderProducts": [
            {
              "orderProductID": orderProductId,
              "orderProductSpecification": {
                "financingTypeID": _listOID.customerType != "COM" ? _providerFoto.typeOfFinancingModelSelected.financingTypeId : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId,
                "ojkBusinessTypeID": _listOID.customerType != "COM" ? _providerFoto.kegiatanUsahaSelected.id : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId,
                "ojkBussinessDetailID": _listOID.customerType != "COM" ? _providerFoto.jenisKegiatanUsahaSelected.id : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId,
                "orderUnitSpecification": {
                  "objectGroupID": _providerUnitObject.groupObjectSelected.KODE,
                  "objectID": _providerUnitObject.objectSelected.id,
                  "productTypeID": _providerUnitObject.productTypeSelected.id,
                  "objectBrandID": _providerUnitObject.brandObjectSelected.id,
                  "objectTypeID": _providerUnitObject.objectTypeSelected.id,
                  "objectModelID": _providerUnitObject.controllerUsageObjectModel.text,
                  "objectUsageID": _providerUnitObject.objectUsageModel.id,
                  "objectPurposeID": _providerUnitObject.objectPurposeSelected.id,
                  "modelDetail": _providerUnitObject.controllerDetailModel.text
                },
                "salesGroupID": _providerUnitObject.groupSalesSelected.kode,
                "orderSourceID": _providerUnitObject.sourceOrderNameSelected.kode,
                "orderSourceName": _providerUnitObject.sourceOrderNameSelected.deskripsi,
                "orderProductDealerSpecification": {
                  "isThirdParty": false,
                  "thirdPartyTypeID": _providerUnitObject.thirdPartyTypeSelected.kode,
                  "thirdPartyID": _providerUnitObject.thirdPartySelected.kode,
                  "thirdPartyActivity": _providerUnitObject.activitiesModel.kode,
                  "sentradID": _providerUnitObject.thirdPartySelected.sentraId,
                  "unitdID": _providerUnitObject.thirdPartySelected.unitId,
                },
                "programID": _providerUnitObject.programSelected != null ? _providerUnitObject.programSelected.kode : "",
                "rehabType": _providerUnitObject.rehabTypeSelected != null ? _providerUnitObject.rehabTypeSelected.id : "",
                "referenceNumber": _providerUnitObject.referenceNumberModel != null ? _providerUnitObject.referenceNumberModel.noPK : "",
                "applicationUnitContractNumber": null,
                "orderProductSaleses": [
                  if(_providerSales.listInfoSales.isNotEmpty) {
                    for(int i=0; i<_providerSales.listInfoSales.length; i++){
                      {
                        "orderProductSalesID": "NEW",
                        "salesType": _providerSales.listInfoSales[i].salesmanTypeModel.id,
                        "employeeJob": _providerSales.listInfoSales[i].positionModel.id,
                        "employeeID": _providerSales.listInfoSales[i].employeeModel.NIK,
                        "employeeHeadID": _providerSales.listInfoSales[i].employeeHeadModel.NIK,
                        "referalContractNumber": null,
                        "creationalSpecification": {
                          "createdAt": DateTime.now(),
                          "createdBy": _preferences.getString("username"),
                          "modifiedAt": DateTime.now(),
                          "modifiedBy": _preferences.getString("username"),
                        },
                        "status": "ACTIVE"
                      }
                    }
                  }
                ],
                "orderKaroseris": _orderKaroseris,
                "orderProductInsurances": [
                  if(_providerInsurance.listFormMajorInsurance.isNotEmpty) {
                    for(int i=0; i<_providerInsurance.listFormMajorInsurance.length; i++){
                      {
                        "orderProductInsuranceID": "NEW",
                        "insuranceCollateralReferenceSpecification": {
                          "collateralID": "NEW",
                          "collateralTypeID": ""
                        },
                        "insuranceTypeID": _providerInsurance.listFormMajorInsurance[i].insuranceType.KODE,
                        "insuranceCompanyID": _providerInsurance.listFormMajorInsurance[i].company.KODE,
                        "insuranceProductID": _providerInsurance.listFormMajorInsurance[i].product.KODE,
                        "period": _providerInsurance.listFormMajorInsurance[i].periodType,
                        "type": _providerInsurance.listFormMajorInsurance[i].type,
                        "type1": _providerInsurance.listFormMajorInsurance[i].coverage1.KODE,
                        "type2": _providerInsurance.listFormMajorInsurance[i].coverage2.KODE,
                        "subType": _providerInsurance.listFormMajorInsurance[i].coverageType,
                        "subTypeCoverage": null,
                        "insuranceSourceTypeID": "",
                        "insuranceValue": _providerInsurance.listFormMajorInsurance[i].coverageValue,
                        "upperLimitPercentage": _providerInsurance.listFormMajorInsurance[i].upperLimitRate,
                        "upperLimitAmount": _providerInsurance.listFormMajorInsurance[i].upperLimit,
                        "lowerLimitPercentage": _providerInsurance.listFormMajorInsurance[i].lowerLimitRate,
                        "lowerLimitAmount": _providerInsurance.listFormMajorInsurance[i].lowerLimit,
                        "insuranceCashFee": _providerInsurance.listFormMajorInsurance[i].priceCash,
                        "insuranceCreditFee": _providerInsurance.listFormMajorInsurance[i].priceCredit,
                        "totalSplitFee": _providerInsurance.listFormMajorInsurance[i].totalPriceSplit,
                        "totalInsuranceFeePercentage": _providerInsurance.listFormMajorInsurance[i].totalPriceRate,
                        "totalInsuranceFeeAmount": _providerInsurance.listFormMajorInsurance[i].totalPrice,
                        "creationalSpecification": {
                          "createdAt": DateTime.now(),
                          "createdBy": _preferences.getString("username"),
                          "modifiedAt": DateTime.now(),
                          "modifiedBy": _preferences.getString("username"),
                        },
                        "status": "ACTIVE",
                        "insurancePaymentType": null
                      }
                    }
                  }
                ],
                "orderWmps": _orderWmps
              },
              "orderCreditStructure": {
                "installmentTypeID": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.id : "",
                "tenor": _providerCreditStructure.periodOfTimeSelected != null ? _providerCreditStructure.periodOfTimeSelected : "",
                "paymentMethodID": _providerCreditStructure.paymentMethodSelected != null ? _providerCreditStructure.paymentMethodSelected.id : "",
                "effectiveRate": _providerCreditStructure.controllerInterestRateEffective.text != "" ? _providerCreditStructure.controllerInterestRateEffective.text : "",
                "flatRate": _providerCreditStructure.controllerInterestRateFlat.text != "" ? _providerCreditStructure.controllerInterestRateFlat.text : "",
                "objectPrice": _providerCreditStructure.controllerObjectPrice.text != "" ? double.parse(_providerCreditStructure.controllerObjectPrice.text.replaceAll(",", "")) : 0,
                "karoseriTotalPrice": _providerCreditStructure.controllerKaroseriTotalPrice.text != "" ? double.parse(_providerCreditStructure.controllerKaroseriTotalPrice.text.replaceAll(",", "")) : 0,
                "totalPrice": _providerCreditStructure.controllerTotalPrice.text != "" ? double.parse(_providerCreditStructure.controllerTotalPrice.text.replaceAll(",", "")) : 0,
                "nettDownPayment": _providerCreditStructure.controllerNetDP.text != "" ? double.parse(_providerCreditStructure.controllerNetDP.text.replaceAll(",", "")) : 0,
                "declineNInstallment": 0,
                "paymentOfYear": 0,
                "branchDownPayment": _providerCreditStructure.controllerBranchDP.text != "" ? double.parse(_providerCreditStructure.controllerBranchDP.text.replaceAll(",", "")) : 0,
                "grossDownPayment": _providerCreditStructure.controllerGrossDP.text != "" ? double.parse(_providerCreditStructure.controllerGrossDP.text.replaceAll(",", "")) : 0,
                "totalLoan": _providerCreditStructure.controllerTotalLoan.text != "" ? double.parse(_providerCreditStructure.controllerTotalLoan.text.replaceAll(",", "")) : 0,
                "installmentAmount": _providerCreditStructure.controllerInstallment.text != "" ? double.parse(_providerCreditStructure.controllerInstallment.text.replaceAll(",", "")) : 0,
                "ltvRatio": _providerCreditStructure.controllerLTV.text != "" ? double.parse(_providerCreditStructure.controllerLTV.text.replaceAll(",", "")) : 0,
                "pencairan": 0,
                "interestAmount": 0,
                "gpType": null,
                "newTenor": 0,
                "totalStepping": 0,
                "installmentBalloonPayment": {
                  "balloonType": _providerCreditStructureType.radioValueBalloonPHOrBalloonInstallment,
                  "lastInstallmentPercentage": _providerCreditStructureType.controllerInstallmentPercentage.text != "" ? double.parse(_providerCreditStructureType.controllerInstallmentPercentage.text.replaceAll(",", "")) : 0,
                  "lastInstallmentValue": _providerCreditStructureType.controllerInstallmentValue.text != "" ? double.parse(_providerCreditStructureType.controllerInstallmentValue.text.replaceAll(",", "")) : 0
                },
                "installmentSchedule": {
                  "installmentType": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.id : "",
                  "paymentMethodType": _providerCreditStructure.paymentMethodSelected != null ? _providerCreditStructure.paymentMethodSelected.id : "",
                  "installment": _providerCreditStructure.controllerInstallment.text != "" ? double.parse(_providerCreditStructure.controllerInstallment.text.replaceAll(",", "")) : 0,
                  "installmentRound": 0,
                  "installmentMethod": 0,
                  "lastKnownOutstanding": 0,
                  "minInterest": 0,
                  "maxInterest": 0,
                  "futureValue": 0,
                  "isInstallment": false,
                  "roundingValue": null,
                  "balloonInstallment": 0,
                  "gracePeriode": 0,
                  "gracePeriodes": [],
                  "seasonal": 0,
                  "steppings": [],
                  "installmentScheduleDetails": []
                },
                "dsr": _providerCreditIncome.controllerDebtComparison.text != "" ? double.parse(_providerCreditIncome.controllerDebtComparison.text.replaceAll(",", "")) : 0,
                "dir": _providerCreditIncome.controllerIncomeComparison.text != "" ? double.parse(_providerCreditIncome.controllerIncomeComparison.text.replaceAll(",", "")) : 0,
                "dsc": _providerCreditIncome.controllerDebtComparison.text != "" ? double.parse(_providerCreditIncome.controllerDebtComparison.text.replaceAll(",", "")) : 0,
                "irr": _providerCreditIncome.controllerIRR.text != "" ? double.parse(_providerCreditIncome.controllerIRR.text.replaceAll(",", "")) : 0,
                "installmentDetails": [_installmentDetails],
                "realDSR": 0
              },
              "orderFees": [
                if(_providerCreditStructure.listInfoFeeCreditStructure.isNotEmpty) {
                  for(int i=0; i<_providerCreditStructure.listInfoFeeCreditStructure.length; i++){
                    {
                      "orderFeeID": "NEW",
                      "feeTypeID": _providerCreditStructure.listInfoFeeCreditStructure[i].feeTypeModel.id,
                      "cashFee": _providerCreditStructure.listInfoFeeCreditStructure[i].cashCost,
                      "creditFee": _providerCreditStructure.listInfoFeeCreditStructure[i].creditCost,
                      "totalFee": _providerCreditStructure.listInfoFeeCreditStructure[i].totalCost,
                      "creationalSpecification": {
                        "createdAt": DateTime.now(),
                        "createdBy": _preferences.getString("username"),
                        "modifiedAt": DateTime.now(),
                        "modifiedBy": _preferences.getString("username"),
                      },
                      "status": "ACTIVE"
                    }
                  }
                }
              ],
              "orderSubsidies": [],
              "orderProductAdditionalSpecification": {
                "virtualAccountID": null,
                "virtualAccountValue": 0,
                "cashingPurposeID": null,
                "cashingTypeID": null
              },
              "creationalSpecification": {
                "createdAt": DateTime.now(),
                "createdBy": _preferences.getString("username"),
                "modifiedAt": DateTime.now(),
                "modifiedBy": _preferences.getString("username"),
              },
              "status": "ACTIVE",
              "collateralAutomotives": [
                {
                  "collateralID": _providerKolateral.collateralTypeModel != null ? _providerKolateral.collateralTypeModel.id : "",
                  "isCollaSameUnit": _providerKolateral.radioValueIsCollateralSameWithUnitOto == 0 ? true : false,
                  "isMultiUnitCollateral": false,
                  "isCollaNameSameWithCustomerName": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0 ? true : false,
                  "identitySpecification": {
                    "identityType": _providerKolateral.identityTypeSelectedAuto != null ? _providerKolateral.identityTypeSelectedAuto.id : "",
                    "identityNumber": _providerKolateral.controllerIdentityNumberAuto.text != "" ? _providerKolateral.controllerIdentityNumberAuto.text : "",
                    "identityName": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1 ? _providerInfoNasabah.controllerNamaLengkapSesuaiIdentitas.text : null,
                    "fullName": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1 ? _providerInfoNasabah.controllerNamaLengkap : _providerKolateral.controllerNameOnCollateralAuto.text,
                    "alias": null,
                    "title": null,
                    "dateOfBirth": _providerKolateral.controllerBirthDateAuto.text != "" ? _providerKolateral.controllerBirthDateAuto.text : "",
                    "placeOfBirth": _providerKolateral.controllerBirthPlaceValidWithIdentityAuto.text != "" ? _providerKolateral.controllerBirthPlaceValidWithIdentityAuto.text : "",
                    "placeOfBirthKabKota": _providerKolateral.controllerBirthPlaceValidWithIdentityLOVAuto.text != "" ? _providerKolateral.controllerBirthPlaceValidWithIdentityLOVAuto.text : "",
                    "gender": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1 ? _providerInfoNasabah.radioValueGender == "01" ? "Laki Laki" : "Perempuan" : null,
                    "identityActiveStart": null,
                    "identityActiveEnd": null,
                    "religion": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1 ? _providerInfoNasabah.religionSelected.id : null,
                    "occupationID": _providerFoto.occupationSelected != null ? _providerFoto.occupationSelected.KODE : "",
                    "positionID": null
                  },
                  "collateralAutomotiveSpecification": {
                    "orderUnitSpecification": {
                      "objectGroupID": _providerKolateral.groupObjectSelected != null ? _providerKolateral.groupObjectSelected.KODE : "",
                      "objectID": _providerKolateral.objectSelected != null ? _providerKolateral.objectSelected.id : "",
                      "productTypeID": _providerKolateral.productTypeSelected != null ? _providerKolateral.productTypeSelected.id : "",
                      "objectBrandID": _providerKolateral.brandObjectSelected != null ? _providerKolateral.brandObjectSelected.id : "",
                      "objectTypeID": _providerKolateral.objectTypeSelected != null ? _providerKolateral.objectTypeSelected.id : "",
                      "objectModelID": _providerKolateral.controllerUsageObjectModel.text != "" ? _providerKolateral.controllerUsageObjectModel.text : "",
                      "objectUsageID": _providerKolateral.objectUsageSelected != null ? _providerKolateral.objectUsageSelected.id : "",
                      "objectPurposeID": ""
                    },
                    "productionYear": _providerKolateral.yearProductionSelected != null ? _providerKolateral.yearProductionSelected : "",
                    "registrationYear": _providerKolateral.yearRegistrationSelected != null ? _providerKolateral.yearRegistrationSelected : "",
                    "isYellowPlate": _providerKolateral.radioValueYellowPlat == 0 ? true : false,
                    "isBuiltUp": _providerKolateral.radioValueBuiltUpNonATPM == 0 ? true : false,
                    "utjSpecification": {
                      "bpkbNumber": _providerKolateral.controllerBPKPNumber.text != "" ? _providerKolateral.controllerBPKPNumber.text : "",
                      "chassisNumber": _providerKolateral.controllerFrameNumber.text != "" ? _providerKolateral.controllerFrameNumber.text : "",
                      "engineNumber": _providerKolateral.controllerMachineNumber.text != "" ? _providerKolateral.controllerMachineNumber.text : "",
                      "policeNumber": _providerKolateral.controllerPoliceNumber.text != "" ? _providerKolateral.controllerPoliceNumber.text : ""
                    },
                    "gradeUnit": _providerKolateral.controllerGradeUnit.text != "" ? _providerKolateral.controllerGradeUnit.text : "",
                    "utjFacility": _providerKolateral.controllerFasilitasUTJ.text != "" ? _providerKolateral.controllerFasilitasUTJ.text : "",
                    "bidderName": _providerKolateral.controllerNamaBidder.text != "" ? _providerKolateral.controllerNamaBidder.text : "",
                    "showroomPrice": _providerKolateral.controllerHargaJualShowroom.text != "" ? _providerKolateral.controllerHargaJualShowroom.text : "",
                    "additionalEquipment": _providerKolateral.controllerPerlengkapanTambahan.text != "" ? _providerKolateral.controllerPerlengkapanTambahan.text : "",
                    "mpAdira": _providerKolateral.controllerMPAdira.text != "" ? _providerKolateral.controllerMPAdira.text : "",
                    "physicalRecondition": _providerKolateral.controllerRekondisiFisik.text != "" ? double.parse(_providerKolateral.controllerRekondisiFisik.text.replaceAll(",", "")) : 0,
                    "isProper": _providerKolateral.radioValueWorthyOrUnworthy == 0 ? false : true,
                    "ltvRatio": 0,
                    "appraisalSpecification": {
                      "collateralDp": double.parse(_providerKolateral.controllerDPJaminan.text.replaceAll(",", "")),
                      "maximumPh": double.parse(_providerKolateral.controllerPHMax.text.replaceAll(",", "")),
                      "appraisalPrice": double.parse(_providerKolateral.controllerTaksasiPriceAutomotive.text.replaceAll(",", "")),
                      "collateralUtilizationID": ""
                    },
                    "collateralAutomotiveAdditionalSpecification": {
                      "capacity": 0,
                      "color": null,
                      "manufacturer": null,
                      "serialNumber": null,
                      "invoiceNumber": null,
                      "stnkActiveDate": null,
                      "bpkbAddress": null,
                      "bpkbIdentityTypeID": null
                    },
                  },
                  "status": "ACTIVE",
                  "creationalSpecification": {
                    "createdAt": DateTime.now(),
                    "createdBy": _preferences.getString("username"),
                    "modifiedAt": DateTime.now(),
                    "modifiedBy": _preferences.getString("username"),
                  }
                }
              ],
              "collateralProperties": [],
              "orderProductProcessingCompletion": {
                "lastKnownOrderProductState": "APPLICATION_DOCUMENT",
                "nextKnownOrderProductState": "APPLICATION_DOCUMENT"
              },
              "isWithoutColla": _providerUnitObject.groupObjectSelected.KODE == "003" ? false : true,
              "isSaveKaroseri": false
            }
          ],
          "orderProcessingCompletion": {
            "calculatedAt": 0,
            "lastKnownOrderState": null,
            "lastKnownOrderStatePosition": null,
            "lastKnownOrderStatus": null,
            "lastKnownOrderHandledBy": null
          },
          "creationalSpecification": {
            "createdAt": DateTime.now(),
            "createdBy": _preferences.getString("username"),
            "modifiedAt": DateTime.now(),
            "modifiedBy": _preferences.getString("username"),
          },
          "status": "ACTIVE"
        },
        "surveyDTO": {
          "surveyID": "NEW",
          "orderID": orderId,
          "surveyType": "",
          "employeeID": "NEW",
          "phoneNumber": "",
          "janjiSurveyDate": _providerApp.controllerSurveyAppointmentDate.text != "" ? _providerApp.controllerSurveyAppointmentDate.text : "",
          "reason": "",
          "flagBRMS": "BRMS",
          "surveyStatus": "",
          "status": "ACTIVE",
          "surveyProcess": "",
          "cfoEligibilityStatus": "",
          "surveyResultSpecification": {
            "surveyResultType": "",
            "recomendationType": "",
            "notes": "",
            "surveyResultDate": "",
            "distanceWithSentra": 0,
            "distanceWithDealer": 0,
            "distanceObjWithSentra": 0,
            "surveyResultAssets": null,
            "surveyResultDetails": null,
            "surveyResultTelesurveys": null
          },
          "creationalSpecification": {
            "createdAt": DateTime.now(),
            "createdBy": _preferences.getString("username"),
            "modifiedAt": DateTime.now(),
            "modifiedBy": _preferences.getString("username"),
          },
          "jobSurveyor": ""
        },
        "userCredentialMS2": {
          "nik": _preferences.getString("username"),
          "branchCode": _preferences.getString("branchid"),
          "fullName": _preferences.getString("fullname"),
          "role": _preferences.getString("job_name")
        },
      },
      "ms2TaskID": _preferences.getString("order_no")
    });
    String urlAcction = await storage.read(key: "urlAcction");
    final _response = await _http.post(
        "${urlAcction}adira-acction-071043/acction/service/mss/validateData",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );

    if(_response.statusCode == 200){
      _showSnackBar("Berhasil validate");
      submitData(_body, context);
    }
    else{
      print(_response.statusCode);
      _showSnackBar("Error ${_response.statusCode}");
    }
  }

  void submitData(String body, BuildContext context) async{
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);
    String urlAcction = await storage.read(key: "urlAcction");
    final _response = await _http.post(
        "${urlAcction}adira-acction-071043/acction/service/mss/ide",
        body: body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );

    if(_response.statusCode == 200){
      print(_response.body);
      _showSnackBar("Berhasil Submit");
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => HomePage()), (Route <dynamic> route) => false);
    }
    else{
      print(_response.statusCode);
      _showSnackBar("Error ${_response.statusCode}");
    }
  }

  void _showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor, duration: Duration(seconds: 2)));
  }
}