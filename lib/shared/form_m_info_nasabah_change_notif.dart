import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/birth_place_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/models/ms2_customer_individu_model.dart';
import 'package:ad1ms2_dev/models/ms2_customer_model.dart';
import 'package:ad1ms2_dev/screens/search_birth_place.dart';
import 'package:ad1ms2_dev/shared/date_picker.dart';
import 'package:ad1ms2_dev/shared/search_birth_place_change_notifier.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';
import 'constants.dart';
import 'resource/validate_data.dart';

class FormMInfoNasabahChangeNotif with ChangeNotifier {
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  var storage = FlutterSecureStorage();
  TextEditingController _controllerNoIdentitas = TextEditingController();
  TextEditingController _controllerTglIdentitas = TextEditingController();
  TextEditingController _controllerIdentitasBerlakuSampai = TextEditingController();
  TextEditingController _controllerNamaLengkapSesuaiIdentitas = TextEditingController();
  TextEditingController _controllerNamaLengkap = TextEditingController();
  TextEditingController _controllerTglLahir = TextEditingController();
  TextEditingController _controllerTempatLahirSesuaiIdentitas = TextEditingController();
  TextEditingController _controllerTempatLahirSesuaiIdentitasLOV = TextEditingController();
  TextEditingController _controllerJumlahTanggungan = TextEditingController();
  TextEditingController _controllerEmail = TextEditingController();
  TextEditingController _controllerNoHp = TextEditingController();
  TextEditingController _controllerNoHp2 = TextEditingController();
  TextEditingController _controllerNoHp3 = TextEditingController();
  TextEditingController _controllerNoNPWP = TextEditingController();
  TextEditingController _controllerNamaSesuaiNPWP = TextEditingController();
  TextEditingController _controllerAlamatSesuaiNPWP = TextEditingController();
  DateTime _initialDateForTglIdentitas = DateTime(dateNow.year, dateNow.month, dateNow.day);
  DateTime _initialDateForTglLahir = DateTime(dateNow.year, dateNow.month, dateNow.day);
  DateTime _initialDateForTglIdentitasBerlakuSampai = DateTime(dateNow.year, dateNow.month, dateNow.day);
  bool _valueCheckBox = true;
  bool _autoValidate = false;
  bool _flag = false;
  bool _isNoHp1WA = false;
  bool _isNoHp2WA = false;
  bool _isNoHp3WA = false;
  String _radioValueGender = "01";
  int _radioValueIsHaveNPWP = 0;
  IdentityModel _identitasModel;
  GCModel _gcModel;
  ReligionModel _religionSelected;
  EducationModel _educationSelected;
  MaritalStatusModel _maritalStatusSelected;
  JenisNPWPModel _jenisNPWPSelected;
  TandaPKPModel _tandaPKPSelected;
  BirthPlaceModel _birthPlaceSelected;
  bool _isEnableFieldFullName = true;
  bool _isEnableFieldBirthDate = true;
  bool _isEnableFieldBirthPlace = true;
  bool _isEnableFieldIdentityNumber = true;
  bool _isGuarantorVisible = false;
  DbHelper _dbHelper = DbHelper();
  bool _loadData = false;
  String _customerIndividualID;
  String _customerID;
  String _obligorID;
  bool _disableJenisPenawaran = false;
  bool _enableDataDedup = false;
  bool _isDisablePACIAAOSCONA = false;

  bool _isGCVisible  = false;
  bool _isIdentitasModelVisible = false;
  bool _isNoIdentitasVisible = false;
  bool _isTglIdentitasVisible = false;
  bool _isidentitasModelVisible = false;
  bool _isIdentitasBerlakuSampaiVisible = false;
  bool _isNamaLengkapSesuaiIdentitasVisible = false;
  bool _isNamaLengkapVisible = false;
  bool _isTglLahirVisible = false;
  bool _isTempatLahirSesuaiIdentitasVisible = false;
  bool _isTempatLahirSesuaiIdentitasLOVVisible = false;
  bool _isGenderVisible = false;
  bool _isReligionVisible = false;
  bool _isEducationSelectedVisible = false;
  bool _isJumlahTanggunganVisible = false;
  bool _isMaritalStatusSelectedVisible = false;
  bool _isEmailVisible = false;
  bool _isNoHpVisible = false;
  bool _isNoHp1WAVisible = false;
  bool _isNoHp2Visible = false;
  bool _isNoHp2WAVisible = false;
  bool _isNoHp3Visible = false;
  bool _isNoHp3WAVisible = false;
  bool _isHaveNPWPVisible = false;
  bool _isNamaSesuaiNPWPVisible = false;
  bool _isJenisNPWPVisible = false;
  bool _isTandaPKPVisible = false;
  bool _isAlamatSesuaiNPWPVisible = false;
  bool _isNoNPWPVisible = false;

  bool _isGCMandatory = false;
  bool _isIdentitasModelMandatory = false;
  bool _isNoIdentitasMandatory = false;
  bool _isTglIdentitasMandatory = false;
  bool _isidentitasModelMandatory = false;
  bool _isIdentitasBerlakuSampaiMandatory = false;
  bool _isNamaLengkapSesuaiIdentitasMandatory = false;
  bool _isNamaLengkapMandatory = false;
  bool _isTglLahirMandatory = false;
  bool _isTempatLahirSesuaiIdentitasMandatory = false;
  bool _isTempatLahirSesuaiIdentitasLOVMandatory = false;
  bool _isGenderMandatory = false;
  bool _isGenderMandatoryShowError = false;
  bool _isReligionMandatory = false;
  bool _isEducationSelectedMandatory = false;
  bool _isJumlahTanggunganMandatory = false;
  bool _isMaritalStatusSelectedMandatory = false;
  bool _isEmailMandatory = false;
  bool _isNoHpMandatory = false;
  bool _isNoHp1WAMandatory = false;
  bool _isNoHp2Mandatory = false;
  bool _isNoHp2WAMandatory = false;
  bool _isNoHp3Mandatory = false;
  bool _isNoHp3WAMandatory = false;
  bool _isHaveNPWPMandatory = false;
  bool _isNPWPMMandatoryShowError = false;
  bool _isNamaSesuaiNPWPMandatory = false;
  bool _isJenisNPWPMandatory = false;
  bool _isTandaPKPMandatory = false;
  bool _isAlamatSesuaiNPWPMandatory = false;
  bool _isNoNPWPMandatory = false;

  // bool _isGCEnable = false;
  // bool _isIdentitasModelEnable = false;
  // bool _isNoIdentitasEnable = false;
  // bool _isTglIdentitasEnable = false;
  // bool _isidentitasModelEnable = false;
  // bool _isIdentitasBerlakuSampaiEnable = false;
  // bool _isNamaLengkapSesuaiIdentitasEnable = false;
  // bool _isNamaLengkapEnable = false;
  // bool _isTglLahirEnable = false;
  // bool _isTempatLahirSesuaiIdentitasEnable = false;
  // bool _isTempatLahirSesuaiIdentitasLOVEnable = false;
  // bool _isGenderEnable = false;
  // bool _isEducationSelectedEnable = false;
  // bool _isMaritalStatusSelectedEnable = false;
  // bool _isNoHpEnable = false;
  // bool _isNoHp1WAEnable = false;
  // bool _isHaveNPWPEnable = false;
  // bool _isAlamatSesuaiNPWPEnable = false;
  // bool _isNoNPWPEnable = false;

  bool _isGCChanges  = false;
  bool _isIdentitasModelChanges = false;
  bool _isNoIdentitasChanges = false;
  bool _isTglIdentitasChanges = false;
  bool _isKTPBerlakuSeumurHidupChanges = false;
  bool _isidentitasModelChanges = false;
  bool _isIdentitasBerlakuSampaiChanges = false;
  bool _isNamaLengkapSesuaiIdentitasChanges = false;
  bool _isNamaLengkapChanges = false;
  bool _isTglLahirChanges = false;
  bool _isTempatLahirSesuaiIdentitasChanges = false;
  bool _isTempatLahirSesuaiIdentitasLOVChanges = false;
  bool _isGenderChanges = false;
  bool _isReligionChanges = false;
  bool _isEducationSelectedChanges = false;
  bool _isJumlahTanggunganChanges = false;
  bool _isMaritalStatusSelectedChanges = false;
  bool _isEmailChanges = false;
  bool _isNoHpChanges = false;
  bool _isNoHp1WAChanges = false;
  bool _isNoHp2Changes = false;
  bool _isNoHp2WAChanges = false;
  bool _isNoHp3Changes = false;
  bool _isNoHp3WAChanges = false;
  bool _isHaveNPWPChanges = false;
  bool _isNamaSesuaiNPWPChanges = false;
  bool _isJenisNPWPChanges = false;
  bool _isTandaPKPChanges = false;
  bool _isAlamatSesuaiNPWPChanges = false;
  bool _isNoNPWPChanges = false;

  bool get isGCVisible => _isGCVisible;
  bool get isIdentitasModelVisible => _isIdentitasModelVisible;
  bool get isNoIdentitasVisible => _isNoIdentitasVisible;
  bool get isTglIdentitasVisible => _isTglIdentitasVisible;
  bool get isidentitasModelVisible => _isidentitasModelVisible;
  bool get isIdentitasBerlakuSampaiVisible => _isIdentitasBerlakuSampaiVisible;
  bool get isNamaLengkapSesuaiIdentitasVisible => _isNamaLengkapSesuaiIdentitasVisible;
  bool get isNamaLengkapVisible => _isNamaLengkapVisible;
  bool get isTglLahirVisible => _isTglLahirVisible;
  bool get isTempatLahirSesuaiIdentitasVisible => _isTempatLahirSesuaiIdentitasVisible;
  bool get isTempatLahirSesuaiIdentitasLOVVisible => _isTempatLahirSesuaiIdentitasLOVVisible;
  bool get isGenderVisible => _isGenderVisible;
  bool get isReligionVisible => _isReligionVisible;
  bool get isEducationSelectedVisible => _isEducationSelectedVisible;
  bool get isJumlahTanggunganVisible => _isJumlahTanggunganVisible;
  bool get isMaritalStatusSelectedVisible => _isMaritalStatusSelectedVisible;
  bool get isEmailVisible => _isEmailVisible;
  bool get isNoHpVisible => _isNoHpVisible;
  bool get isNoHp1WAVisible => _isNoHp1WAVisible;
  bool get isNoHp2Visible => _isNoHp2Visible;
  bool get isNoHp2WAVisible => _isNoHp2WAVisible;
  bool get isNoHp3Visible => _isNoHp3Visible;
  bool get isNoHp3WAVisible => _isNoHp3WAVisible;
  bool get isHaveNPWPVisible => _isHaveNPWPVisible;
  bool get isNamaSesuaiNPWPVisible => _isNamaSesuaiNPWPVisible;
  bool get isJenisNPWPVisible => _isJenisNPWPVisible;
  bool get isTandaPKPVisible => _isTandaPKPVisible;
  bool get isAlamatSesuaiNPWPVisible => _isAlamatSesuaiNPWPVisible;
  bool get isNoNPWPVisible => _isNoNPWPVisible;

  bool get isGCMandatory => _isGCMandatory;
  bool get isIdentitasModelMandatory => _isIdentitasModelMandatory;
  bool get isNoIdentitasMandatory => _isNoIdentitasMandatory;
  bool get isTglIdentitasMandatory => _isTglIdentitasMandatory;
  bool get isidentitasModelMandatory => _isidentitasModelMandatory;
  bool get isIdentitasBerlakuSampaiMandatory => _isIdentitasBerlakuSampaiMandatory;
  bool get isNamaLengkapSesuaiIdentitasMandatory => _isNamaLengkapSesuaiIdentitasMandatory;
  bool get isNamaLengkapMandatory => _isNamaLengkapMandatory;
  bool get isTglLahirMandatory => _isTglLahirMandatory;
  bool get isTempatLahirSesuaiIdentitasMandatory => _isTempatLahirSesuaiIdentitasMandatory;
  bool get isTempatLahirSesuaiIdentitasLOVMandatory => _isTempatLahirSesuaiIdentitasLOVMandatory;
  bool get isGenderMandatory => _isGenderMandatory;
  bool get isGenderMandatoryShowError => _isGenderMandatoryShowError;
  bool get isReligionMandatory => _isReligionMandatory;
  bool get isEducationSelectedMandatory => _isEducationSelectedMandatory;
  bool get isJumlahTanggunganMandatory => _isJumlahTanggunganMandatory;
  bool get isMaritalStatusSelectedMandatory => _isMaritalStatusSelectedMandatory;
  bool get isEmailMandatory => _isEmailMandatory;
  bool get isNoHpMandatory => _isNoHpMandatory;
  bool get isNoHp1WAMandatory => _isNoHp1WAMandatory;
  bool get isNoHp2Mandatory => _isNoHp2Mandatory;
  bool get isNoHp2WAMandatory => _isNoHp2WAMandatory;
  bool get isNoHp3Mandatory => _isNoHp3Mandatory;
  bool get isNoHp3WAMandatory => _isNoHp3WAMandatory;
  bool get isHaveNPWPMandatory => _isHaveNPWPMandatory;
  bool get isHaveNPWPMandatoryShowError => _isNPWPMMandatoryShowError;
  bool get isNamaSesuaiNPWPMandatory => _isNamaSesuaiNPWPMandatory;
  bool get isJenisNPWPMandatory => _isJenisNPWPMandatory;
  bool get isTandaPKPMandatory => _isTandaPKPMandatory;
  bool get isAlamatSesuaiNPWPMandatory => _isAlamatSesuaiNPWPMandatory;
  bool get isNoNPWPMandatory => _isNoNPWPMandatory;

  // bool get isGCEnable => _isGCEnable;
  // bool get isIdentitasModelEnable => _isIdentitasModelEnable;
  // bool get isNoIdentitasEnable => _isNoIdentitasEnable;
  // bool get isTglIdentitasEnable => _isTglIdentitasEnable = false;
  // bool get isidentitasModelEnable => _isidentitasModelEnable = false;
  // bool get isIdentitasBerlakuSampaiEnable => _isIdentitasBerlakuSampaiEnable = false;
  // bool get isNamaLengkapSesuaiIdentitasEnable => _isNamaLengkapSesuaiIdentitasEnable;
  // bool get isNamaLengkapEnable => _isNamaLengkapEnable;
  // bool get isTglLahirEnable => _isTglLahirEnable;
  // bool get isTempatLahirSesuaiIdentitasEnable => _isTempatLahirSesuaiIdentitasEnable;
  // bool get isTempatLahirSesuaiIdentitasLOVEnable => _isTempatLahirSesuaiIdentitasLOVEnable;
  // bool get isGenderEnable => _isGenderEnable;
  // bool get isEducationSelectedEnable => _isEducationSelectedEnable;
  // bool get isMaritalStatusSelectedEnable => _isMaritalStatusSelectedEnable;
  // bool get isNoHpEnable => _isNoHpEnable;
  // bool get isNoHp1WAEnable => _isNoHp1WAEnable;
  // bool get isHaveNPWPEnable => _isHaveNPWPEnable;
  // bool get isAlamatSesuaiNPWPEnable => _isAlamatSesuaiNPWPEnable;
  // bool get isNoNPWPEnable => _isNoNPWPEnable;

  bool get isGCChanges => _isGCChanges;
  bool get isIdentitasModelChanges => _isIdentitasModelChanges;
  bool get isNoIdentitasChanges => _isNoIdentitasChanges;
  bool get isTglIdentitasChanges => _isTglIdentitasChanges;
  bool get isKTPBerlakuSeumurHidupChanges => _isKTPBerlakuSeumurHidupChanges;
  bool get isidentitasModelChanges => _isidentitasModelChanges;
  bool get isIdentitasBerlakuSampaiChanges => _isIdentitasBerlakuSampaiChanges;
  bool get isNamaLengkapSesuaiIdentitasChanges => _isNamaLengkapSesuaiIdentitasChanges;
  bool get isNamaLengkapChanges => _isNamaLengkapChanges;
  bool get isTglLahirChanges => _isTglLahirChanges;
  bool get isTempatLahirSesuaiIdentitasChanges => _isTempatLahirSesuaiIdentitasChanges;
  bool get isTempatLahirSesuaiIdentitasLOVChanges => _isTempatLahirSesuaiIdentitasLOVChanges;
  bool get isGenderChanges => _isGenderChanges;
  bool get isReligionChanges => _isReligionChanges;
  bool get isEducationSelectedChanges => _isEducationSelectedChanges;
  bool get isJumlahTanggunganChanges => _isJumlahTanggunganChanges;
  bool get isMaritalStatusSelectedChanges => _isMaritalStatusSelectedChanges;
  bool get isEmailChanges => _isEmailChanges;
  bool get isNoHpChanges => _isNoHpChanges;
  bool get isNoHp1WAChanges => _isNoHp1WAChanges;
  bool get isNoHp2Changes => _isNoHp2Changes;
  bool get isNoHp2WAChanges => _isNoHp2WAChanges;
  bool get isNoHp3Changes => _isNoHp3Changes;
  bool get isNoHp3WAChanges => _isNoHp3WAChanges;
  bool get isHaveNPWPChanges => _isHaveNPWPChanges;
  bool get isNamaSesuaiNPWPChanges => _isNamaSesuaiNPWPChanges;
  bool get isJenisNPWPChanges => _isJenisNPWPChanges;
  bool get isTandaPKPChanges => _isTandaPKPChanges;
  bool get isAlamatSesuaiNPWPChanges => _isAlamatSesuaiNPWPChanges;
  bool get isNoNPWPChanges => _isNoNPWPChanges;

  bool get isDisablePACIAAOSCONA => _isDisablePACIAAOSCONA;

  set isDisablePACIAAOSCONA(bool value) {
    this._isDisablePACIAAOSCONA = value;
    notifyListeners();
  }

  bool get enableDataDedup => _enableDataDedup;

  set enableDataDedup(bool value) {
    this._enableDataDedup = value;
    notifyListeners();
  }

  String get customerIndividualID => _customerIndividualID;

  set customerIndividualID(String value) {
    this._customerIndividualID = value;
    notifyListeners();
  }

  String get customerID => _customerID;

  set customerID(String value) {
    this._customerID = value;
  }

  set isGCVisible(bool value) {
    this._isGCVisible = value;
    notifyListeners();
  }
  set isIdentitasModelVisible(bool value) {
    this._isIdentitasModelVisible = value;
    notifyListeners();
  }
  set isNoIdentitasVisible(bool value) {
    this._isNoIdentitasVisible = value;
    notifyListeners();
  }
  set isTglIdentitasVisible(bool value) {
    this._isTglIdentitasVisible = value;
    notifyListeners();
  }
  set isidentitasModelVisible(bool value) {
    this._isidentitasModelVisible = value;
    notifyListeners();
  }
  set isIdentitasBerlakuSampaiVisible(bool value) {
    this._isIdentitasBerlakuSampaiVisible = value;
    notifyListeners();
  }
  set isNamaLengkapSesuaiIdentitasVisible(bool value) {
    this._isNamaLengkapSesuaiIdentitasVisible = value;
    notifyListeners();
  }
  set isNamaLengkapVisible(bool value) {
    this._isNamaLengkapVisible = value;
    notifyListeners();
  }
  set isTglLahirVisible(bool value) {
    this._isTglLahirVisible = value;
    notifyListeners();
  }
  set isTempatLahirSesuaiIdentitasVisible(bool value) {
    this._isTempatLahirSesuaiIdentitasVisible = value;
    notifyListeners();
  }
  set isTempatLahirSesuaiIdentitasLOVVisible(bool value) {
    this._isTempatLahirSesuaiIdentitasLOVVisible = value;
    notifyListeners();
  }
  set isGenderVisible(bool value) {
    this._isGenderVisible = value;
    notifyListeners();
  }
  set isReligionVisible(bool value) {
    this._isReligionVisible = value;
    notifyListeners();
  }
  set isEducationSelectedVisible(bool value) {
    this._isEducationSelectedVisible = value;
    notifyListeners();
  }
  set isJumlahTanggunganVisible(bool value) {
    this._isJumlahTanggunganVisible = value;
    notifyListeners();
  }
  set isMaritalStatusSelectedVisible(bool value) {
    this._isMaritalStatusSelectedVisible = value;
    notifyListeners();
  }
  set isEmailVisible(bool value) {
    this._isEmailVisible = value;
    notifyListeners();
  }
  set isNoHpVisible(bool value) {
    this._isNoHpVisible = value;
    notifyListeners();
  }
  set isNoHp1WAVisible(bool value) {
    this._isNoHp1WAVisible = value;
    notifyListeners();
  }
  set isNoHp2Visible(bool value) {
    this._isNoHp2Visible = value;
    notifyListeners();
  }
  set isNoHp2WAVisible(bool value) {
    this._isNoHp2WAVisible = value;
    notifyListeners();
  }
  set isNoHp3Visible(bool value) {
    this._isNoHp3Visible = value;
    notifyListeners();
  }
  set isNoHp3WAVisible(bool value) {
    this._isNoHp3WAVisible = value;
    notifyListeners();
  }
  set isHaveNPWPVisible(bool value) {
    this._isHaveNPWPVisible = value;
    notifyListeners();
  }
  set isNamaSesuaiNPWPVisible(bool value) {
    this._isNamaSesuaiNPWPVisible = value;
    notifyListeners();
  }
  set isJenisNPWPVisible(bool value) {
    this._isJenisNPWPVisible = value;
    notifyListeners();
  }
  set isTandaPKPVisible(bool value) {
    this._isTandaPKPVisible = value;
    notifyListeners();
  }
  set isAlamatSesuaiNPWPVisible(bool value) {
    this._isAlamatSesuaiNPWPVisible = value;
    notifyListeners();
  }
  set isNoNPWPVisible(bool value) {
    this._isNoNPWPVisible = value;
    notifyListeners();
  }

//
  set isGCMandatory(bool value) {
    this._isGCMandatory = value;
    notifyListeners();
  }
  set isIdentitasModelMandatory(bool value) {
    this._isIdentitasModelMandatory = value;
    notifyListeners();
  }
  set isNoIdentitasMandatory(bool value) {
    this._isNoIdentitasMandatory = value;
    notifyListeners();
  }
  set isTglIdentitasMandatory(bool value) {
    this._isTglIdentitasMandatory = value;
    notifyListeners();
  }
  set isidentitasModelMandatory(bool value) {
    this._isidentitasModelMandatory = value;
    notifyListeners();
  }
  set isIdentitasBerlakuSampaiMandatory(bool value) {
    this._isIdentitasBerlakuSampaiMandatory = value;
    notifyListeners();
  }
  set isNamaLengkapSesuaiIdentitasMandatory(bool value) {
    this._isNamaLengkapSesuaiIdentitasMandatory = value;
    notifyListeners();
  }
  set isNamaLengkapMandatory(bool value) {
    this._isNamaLengkapMandatory = value;
    notifyListeners();
  }
  set isTglLahirMandatory(bool value) {
    this._isTglLahirMandatory = value;
    notifyListeners();
  }
  set isTempatLahirSesuaiIdentitasMandatory(bool value) {
    this._isTempatLahirSesuaiIdentitasMandatory = value;
    notifyListeners();
  }
  set isTempatLahirSesuaiIdentitasLOVMandatory(bool value) {
    this._isTempatLahirSesuaiIdentitasLOVMandatory = value;
    notifyListeners();
  }
  set isGenderMandatory(bool value) {
    this._isGenderMandatory = value;
    notifyListeners();
  }
  set isReligionMandatory(bool value) {
    this._isReligionMandatory = value;
    notifyListeners();
  }
  set isEducationSelectedMandatory(bool value) {
    this._isEducationSelectedMandatory = value;
    notifyListeners();
  }
  set isJumlahTanggunganMandatory(bool value) {
    this._isJumlahTanggunganMandatory = value;
    notifyListeners();
  }
  set isMaritalStatusSelectedMandatory(bool value) {
    this._isMaritalStatusSelectedMandatory = value;
    notifyListeners();
  }
  set isEmailMandatory(bool value) {
    this._isEmailMandatory = value;
    notifyListeners();
  }
  set isNoHpMandatory(bool value) {
    this._isNoHpMandatory = value;
    notifyListeners();
  }
  set isNoHp1WAMandatory(bool value) {
    this._isNoHp1WAMandatory = value;
    notifyListeners();
  }
  set isNoHp2Mandatory(bool value) {
    this._isNoHp2Mandatory = value;
    notifyListeners();
  }
  set isNoHp2WAMandatory(bool value) {
    this._isNoHp2WAMandatory = value;
    notifyListeners();
  }
  set isNoHp3Mandatory(bool value) {
    this._isNoHp3Mandatory = value;
    notifyListeners();
  }
  set isNoHp3WAMandatory(bool value) {
    this._isNoHp3WAMandatory = value;
    notifyListeners();
  }
  set isHaveNPWPMandatory(bool value) {
    this._isHaveNPWPMandatory = value;
    notifyListeners();
  }
  set isNamaSesuaiNPWPMandatory(bool value) {
    this._isNamaSesuaiNPWPMandatory = value;
    notifyListeners();
  }
  set isJenisNPWPMandatory(bool value) {
    this._isJenisNPWPMandatory = value;
    notifyListeners();
  }
  set isTandaPKPMandatory(bool value) {
    this._isTandaPKPMandatory = value;
    notifyListeners();
  }
  set isAlamatSesuaiNPWPMandatory(bool value) {
    this._isAlamatSesuaiNPWPMandatory = value;
    notifyListeners();
  }
  set isNoNPWPMandatory(bool value) {
    this._isNoNPWPMandatory = value;
    notifyListeners();
  }
//

  set isGCChanges(bool value) {
    this._isGCChanges = value;
    notifyListeners();
  }
  set isIdentitasModelChanges(bool value) {
    this._isIdentitasModelChanges = value;
    notifyListeners();
  }
  set isNoIdentitasChanges(bool value) {
    this._isNoIdentitasChanges = value;
    notifyListeners();
  }
  set isTglIdentitasChanges(bool value) {
    this._isTglIdentitasChanges = value;
    notifyListeners();
  }
  set isKTPBerlakuSeumurHidupChanges(bool value) {
    this._isKTPBerlakuSeumurHidupChanges = value;
    notifyListeners();
  }
  set isidentitasModelChanges(bool value) {
    this._isidentitasModelChanges = value;
    notifyListeners();
  }
  set isIdentitasBerlakuSampaiChanges(bool value) {
    this._isIdentitasBerlakuSampaiChanges = value;
    notifyListeners();
  }
  set isNamaLengkapSesuaiIdentitasChanges(bool value) {
    this._isNamaLengkapSesuaiIdentitasChanges = value;
    notifyListeners();
  }
  set isNamaLengkapChanges(bool value) {
    this._isNamaLengkapChanges = value;
    notifyListeners();
  }
  set isTglLahirChanges(bool value) {
    this._isTglLahirChanges = value;
    notifyListeners();
  }
  set isTempatLahirSesuaiIdentitasChanges(bool value) {
    this._isTempatLahirSesuaiIdentitasChanges = value;
    notifyListeners();
  }
  set isTempatLahirSesuaiIdentitasLOVChanges(bool value) {
    this._isTempatLahirSesuaiIdentitasLOVChanges = value;
    notifyListeners();
  }
  set isGenderChanges(bool value) {
    this._isGenderChanges = value;
    notifyListeners();
  }
  set isReligionChanges(bool value) {
    this._isReligionChanges = value;
    notifyListeners();
  }
  set isEducationSelectedChanges(bool value) {
    this._isEducationSelectedChanges = value;
    notifyListeners();
  }
  set isJumlahTanggunganChanges(bool value) {
    this._isJumlahTanggunganChanges = value;
    notifyListeners();
  }
  set isMaritalStatusSelectedChanges(bool value) {
    this._isMaritalStatusSelectedChanges = value;
    notifyListeners();
  }
  set isEmailChanges(bool value) {
    this._isEmailChanges = value;
    notifyListeners();
  }
  set isNoHpChanges(bool value) {
    this._isNoHpChanges = value;
    notifyListeners();
  }
  set isNoHp1WAChanges(bool value) {
    this._isNoHp1WAChanges = value;
    notifyListeners();
  }
  set isNoHp2Changes(bool value) {
    this._isNoHp2Changes = value;
    notifyListeners();
  }
  set isNoHp2WAChanges(bool value) {
    this._isNoHp2WAChanges = value;
    notifyListeners();
  }
  set isNoHp3Changes(bool value) {
    this._isNoHp3Changes = value;
    notifyListeners();
  }
  set isNoHp3WAChanges(bool value) {
    this._isNoHp3WAChanges = value;
    notifyListeners();
  }
  set isHaveNPWPChanges(bool value) {
    this._isHaveNPWPChanges = value;
    notifyListeners();
  }
  set isNamaSesuaiNPWPChanges(bool value) {
    this._isNamaSesuaiNPWPChanges = value;
    notifyListeners();
  }
  set isJenisNPWPChanges(bool value) {
    this._isJenisNPWPChanges = value;
    notifyListeners();
  }
  set isTandaPKPChanges(bool value) {
    this._isTandaPKPChanges = value;
    notifyListeners();
  }
  set isAlamatSesuaiNPWPChanges(bool value) {
    this._isAlamatSesuaiNPWPChanges = value;
    notifyListeners();
  }
  set isNoNPWPChanges(bool value) {
    this._isNoNPWPChanges = value;
    notifyListeners();
  }

//   set isGCEnable(bool value) {
//     this._isGCEnable = value;
//     notifyListeners();
//   }
//   set isIdentitasModelEnable(bool value) {
//     this._isIdentitasModelEnable = value;
//     notifyListeners();
//   }
//   set isNoIdentitasEnable(bool value) {
//     this._isNoIdentitasEnable = value;
//     notifyListeners();
//   }
//   set isNamaLengkapSesuaiIdentitasEnable(bool value) {
//     this._isNamaLengkapSesuaiIdentitasEnable = value;
//     notifyListeners();
//   }
//   set isNamaLengkapEnable(bool value) {
//     this._isNamaLengkapEnable = value;
//     notifyListeners();
//   }
//   set isTglLahirEnable(bool value) {
//     this._isTglLahirEnable = value;
//     notifyListeners();
//   }
//   set isTempatLahirSesuaiIdentitasEnable(bool value) {
//     this._isTempatLahirSesuaiIdentitasEnable = value;
//     notifyListeners();
//   }
//   set isTempatLahirSesuaiIdentitasLOVEnable(bool value) {
//     this._isTempatLahirSesuaiIdentitasLOVEnable = value;
//     notifyListeners();
//   }
//   set isGenderEnable(bool value) {
//     this._isGenderEnable = value;
//     notifyListeners();
//   }
//   set isEducationSelectedEnable(bool value) {
//     this._isEducationSelectedEnable = value;
//     notifyListeners();
//   }
//   set isMaritalStatusSelectedEnable(bool value) {
//     this._isMaritalStatusSelectedEnable = value;
//     notifyListeners();
//   }
//   set isNoHpEnable(bool value) {
//     this._isNoHpEnable = value;
//     notifyListeners();
//   }
//   set isNoHp1WAEnable(bool value) {
//     this._isNoHp1WAEnable = value;
//     notifyListeners();
//   }
//   set isHaveNPWPEnable(bool value) {
//     this._isHaveNPWPEnable = value;
//     notifyListeners();
//   }
//   set isAlamatSesuaiNPWPEnable(bool value) {
//     this._isAlamatSesuaiNPWPEnable = value;
//     notifyListeners();
//   }
//   set isNoNPWPEnable(bool value) {
//     this._isNoNPWPEnable = value;
//     notifyListeners();
//   }


  // Future<void> getInfoNasabahShowHide() async{
  //   final ioc = new HttpClient();
  //   ioc.badCertificateCallback =
  //       (X509Certificate cert, String host, int port) => true;
  //
  //   final _http = IOClient(ioc);
  //   String urlPublic = await storage.read(key: "urlPublic");
  //   final _response = await _http.get(
  //       "${urlPublic}api-form-web/get_mide_individuhs",
  //       headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  //   );
  //   if(_response.statusCode == 200){
  //     final _result = jsonDecode(_response.body);
  //     final _data = _result['hideshow'];
  //
  //     this._isGCVisible  = _data[0]['gc'] == "1";
  //     this._isIdentitasModelVisible = _data[0]['jenisidentitaspribadi'] == "1";
  //     this._isNoIdentitasVisible = _data[0]['noidentitaspribadi'] == "1";
  //     this._isNamaLengkapSesuaiIdentitasVisible = _data[0]['namalengkapidpribadi'] == "1";
  //     this._isNamaLengkapVisible = _data[0]['namalengkappribadi'] == "1";
  //     this._isTglLahirVisible = _data[0]['tanggallahirpribadi'] == "1";
  //     this._isTempatLahirSesuaiIdentitasVisible = _data[0]['tempatlahiridpribadi'] == "1";
  //     this._isTempatLahirSesuaiIdentitasLOVVisible = _data[0]['tempatlahiridlovpribadi'] == "1";
  //     this._isGenderVisible = _data[0]['jeniskelaminpribadi'] == "1";
  //     this._isEducationSelectedVisible = _data[0]['pendidikan'] == "1";
  //     this._isMaritalStatusSelectedVisible = _data[0]['statuspernikahan'] == "1";
  //     this._isNoHpVisible = _data[0]['handphone1'] == "1";
  //     this._isNoHp1WAVisible = _data[0]['nowa1'] == "1";
  //     this._isHaveNPWPVisible = _data[0]['punyanpwp'] == "1";
  //     this._isAlamatSesuaiNPWPVisible = _data[0]['alamatnpwp'] == "1";
  //
  //   }
  //   else{
  //     throw Exception("Failed get data error ${_response.statusCode}");
  //   }
  // }


  List<IdentityModel> _itemsIdentitas = IdentityType().lisIdentityModel;
//  [
//    IdentityModel("01", "KTP"),
//    IdentityModel("03", "PASSPORT"),
//    IdentityModel("04", "SIM"),
//    IdentityModel("05", "KTP Sementara"),
//    IdentityModel("06", "Resi KTP"),
//    IdentityModel("07", "Ket. Domisili"),
//  ];

  List<GCModel> _itemsGC = [
    GCModel("001", "Fleet"),
    GCModel("002", "Retail"),
    GCModel("003", "RETAIL TAC NOL"),
    GCModel("004", "FLEET TAC NOL"),
  ];

  List<ReligionModel> _itemsReligion = [
    ReligionModel("01", "ISLAM"),
    ReligionModel("02", "KATHOLIK"),
    ReligionModel("03", "KRISTEN"),
    ReligionModel("04", "BUDHA"),
    ReligionModel("05", "HINDU"),
    ReligionModel("06", "KONGHUCU"),
    ReligionModel("07", "KEPERCAYAAN TERHADAP TUHAN YME"),
    ReligionModel("99", "-"),
  ];

  List<EducationModel> _listEducation = [
    EducationModel("01", "PASCASARJANA"),
    EducationModel("02", "SARJANA"),
    EducationModel("03", "DIPLOMA"),
    EducationModel("04", "SMTA/SEDERAJAT"),
    EducationModel("05", "SMTP/SEDERAJAT"),
    EducationModel("06", "DIBAWAH SMTP"),
    EducationModel("07", "TIDAKSEKOLAH"),
  ];

  List<MaritalStatusModel> _listMaritalStatus = [
    MaritalStatusModel("01", "KAWIN"),
    MaritalStatusModel("02", "SINGLE"),
    MaritalStatusModel("03", "DUDA/JANDA TANPA ANAK"),
    MaritalStatusModel("04", "DUDA/JANDA DGN ANAK")
  ];

  List<JenisNPWPModel> _listJenisNPWP = [
    JenisNPWPModel("1", "BADAN USAHA"),
    JenisNPWPModel("2", "PERORANGAN"),
  ];

  List<TandaPKPModel> _listTandaPKP = [
    TandaPKPModel("1", "PKP"),
    TandaPKPModel("2", "NON PKP"),
  ];

  TextEditingController get controllerTglIdentitas {
    return this._controllerTglIdentitas;
  }

  set controllerTglIdentitas(TextEditingController value) {
    this._controllerTglIdentitas = value;
    this.notifyListeners();
  }

  TextEditingController get controllerNoIdentitas {
    return this._controllerNoIdentitas;
  }

  set controllerNoIdentitas(value) {
    this._controllerNoIdentitas = value;
    this.notifyListeners();
  }

  IdentityModel get identitasModel {
    return this._identitasModel;
  }

  set identitasModel(IdentityModel value) {
    if (_valueCheckBox) {
      this._valueCheckBox = false;
    }
    this._identitasModel = value;
    this._controllerNoIdentitas.clear();
    this.notifyListeners();
  }

  UnmodifiableListView<IdentityModel> get items {
    return UnmodifiableListView(this._itemsIdentitas);
  }

  UnmodifiableListView<GCModel> get itemsGC {
    return UnmodifiableListView(this._itemsGC);
  }

  DateTime get initialDateForTglIdentitas => _initialDateForTglIdentitas;

  void selectTglIdentitas(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      // builder: (BuildContext context, Widget child) {
      //  return Theme(
      //    data: ThemeData.light().copyWith(
      //      buttonTheme: ButtonThemeData(
      //          textTheme: ButtonTextTheme.normal
      //      ),
      //      colorScheme: ColorScheme.light(
      //        primary: const Color(0xFFec7e00),
      //        surface: const Color(0xFFec7e00),
      //        onSurface: Colors.black, // tanggal
      //      ),
      //    ),
      //    child: child,
      //  );
      // },
      context: context,
      initialDate: this._initialDateForTglIdentitas,
      firstDate: DateTime(dateNow.year - 5, dateNow.month, dateNow.day),
      lastDate: DateTime(dateNow.year + 5, dateNow.month, dateNow.day));
    if (picked != null) {
      this.controllerTglIdentitas.text = dateFormat.format(picked);
      this._initialDateForTglIdentitas = picked;
      this.notifyListeners();
    } else {
      return;
    }
  }

  void selectTglIdentitasBerlakuSampai(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      // builder: (BuildContext context, Widget child) {
      //   return Theme(
      //     data: ThemeData.light().copyWith(
      //       buttonTheme: ButtonThemeData(
      //           textTheme: ButtonTextTheme.normal
      //       ),
      //       colorScheme: ColorScheme.light(
      //         primary: const Color(0xFF3f4f7f),
      //         surface: const Color(0xFF3f4f7f),
      //         onSurface: Colors.black, // tanggal
      //       ),
      //     ),
      //     child: child,
      //   );
      // },
      context: context,
      initialDate: this._initialDateForTglIdentitasBerlakuSampai,
      firstDate: DateTime(dateNow.year, dateNow.month, dateNow.day),
      lastDate: DateTime(dateNow.year + 7, dateNow.month, dateNow.day)
    );
    if (picked != null) {
      this.controllerIdentitasBerlakuSampai.text = dateFormat.format(picked);
      this._initialDateForTglIdentitasBerlakuSampai = picked;
      this.notifyListeners();
    } else {
      return;
    }
  }

  TextEditingController get controllerNamaLengkapSesuaiIdentitas {
    return this._controllerNamaLengkapSesuaiIdentitas;
  }

  set controllerNamaLengkapSesuaiIdentitas(TextEditingController value) {
    this._controllerNamaLengkapSesuaiIdentitas = value;
    notifyListeners();
  }

  TextEditingController get controllerNamaLengkap {
    return this._controllerNamaLengkap;
  }

  set controllerNamaLengkap(TextEditingController value) {
    this._controllerNamaLengkap = value;
    notifyListeners();
  }

  TextEditingController get controllerTglLahir => _controllerTglLahir;

  DateTime get initialDateForTglLahir => _initialDateForTglLahir;

  set initialDateForTglLahir(DateTime value) {
    this._initialDateForTglLahir = value;
  }

  String get obligorID => _obligorID;

  set obligorID(String value) {
    this._obligorID = value;
  }

  // void selectBirthDate(BuildContext context) async {
  //   DatePickerShared _datePickerShared = DatePickerShared();
  //   var _datePickerSelected = await _datePickerShared.selectStartDate(
  //       context, this._initialDateForTglLahir,
  //       canAccessNextDay: false);
  //   if (_datePickerSelected != null) {
  //     this.controllerTglLahir.text = dateFormat.format(_datePickerSelected);
  //     this._initialDateForTglLahir = _datePickerSelected;
  //     notifyListeners();
  //   } else {
  //     return;
  //   }
  // }

  void selectBirthDate(BuildContext context) async {
    // final DateTime _picked = await showDatePicker(
    //     context: context,
    //     initialDate: _initialSurveyDate,
    //     firstDate: DateTime(
    //         DateTime.now().year, DateTime.now().month, DateTime.now().day),
    //     lastDate: DateTime(
    //         DateTime.now().year + 2, DateTime.now().month, DateTime.now().day));
    // if (_picked != null) {
    //   this._initialSurveyDate = _picked;
    //   this._controllerSurveyAppointmentDate.text = _formatter.format(_picked);
    //   notifyListeners();
    // } else {
    //   return;
    // }
    var _picked = await selectDate(context,  this._initialDateForTglLahir);
    if (_picked != null) {
      this.controllerTglLahir.text = dateFormat.format(_picked);
      this._initialDateForTglLahir = _picked;
      notifyListeners();
    } else {
      return;
    }
  }

  GlobalKey<FormState> get keyForm => _key;

  TextEditingController get controllerTempatLahirSesuaiIdentitas =>
      _controllerTempatLahirSesuaiIdentitas;

  GCModel get gcModel => _gcModel;

  set gcModel(GCModel value) {
    this._gcModel = value;
    notifyListeners();
  }

  bool get autoValidate {
    return _autoValidate;
  }

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  bool get flag {
    return _flag;
  }

  set flag(bool value) {
    this._flag = value;
    notifyListeners();
  }

  String validateEmail(String value) {
    if (value.isEmpty && isEmailMandatory) {
      // The form is empty
      return "Enter email address";
    }
    // This is just a regular expression for email addresses
    String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
        "\\@" +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
        "(" +
        "\\." +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
        ")+";
    RegExp regExp = new RegExp(p);

    if (regExp.hasMatch(value)) {
      // So, the email is valid
      return null;
    }
    // The pattern of the email didn't match the regex above.
    return 'Email is not valid';
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    _loadData = value;
    notifyListeners();
  }

  // fungsi untuk cek validasi name dan npwp
  Future<void> validateNameOrNPWP(BuildContext context,String typeValidate) async{
    try{
      if(typeValidate == "NAME"){
        print("NAME");
        if(this._controllerNamaSesuaiNPWP.text != ""){
          if(await _validateData.validateName(this._controllerNamaSesuaiNPWP.text,"PER") != "SUCCESS"){
            _showSnackBar(await _validateData.validateName(this._controllerNamaSesuaiNPWP.text,"PER"));
          }
        }
      }
      else if(typeValidate == "NPWP"){
        print("NPWP");
        if(this._controllerNoNPWP.text != ""){
          if(await _validateData.validateNPWP(this._controllerNoNPWP.text) != "SUCCESS"){
            _showSnackBar("${await _validateData.validateNPWP(this._controllerNoNPWP.text)}");
          }
        }
      }
      else{
        print("ELSE");
        loadData = true;
        if(await _validateData.validateName(this._controllerNamaSesuaiNPWP.text,"PER") != "SUCCESS"){
          _showSnackBar(await _validateData.validateName(this._controllerNamaSesuaiNPWP.text,"COM"));
        }
        else if(await _validateData.validateNPWP(this._controllerNoNPWP.text) != "SUCCESS"){
          _showSnackBar("${await _validateData.validateNPWP(this._controllerNoNPWP.text)}");
        }
        else{
          Navigator.pop(context);
        }
        loadData = false;
      }
    }
    catch(e){
      _showSnackBar(e);
    }
  }

  void check(BuildContext context) {
    final _form = _key.currentState;
    if (_form.validate() && !isGenderMandatoryShowError) {
      checkNeedGuarantor();
      flag = true;
      autoValidate = false;
      // await validateNameOrNPWP(context, "");
      Navigator.pop(context);
    } else {
      flag = false;
      autoValidate = true;
    }
  }

  Future<bool> onBackPress() async{
    final _form = _key.currentState;
    if (_form.validate()) {
      checkNeedGuarantor();
      flag = true;
      autoValidate = false;
    } else {
      flag = false;
      autoValidate = true;
    }
    return true;
  }

  TextEditingController get controllerIdentitasBerlakuSampai =>
      _controllerIdentitasBerlakuSampai;

  bool get valueCheckBox {
    return _valueCheckBox;
  }

  set valueCheckBox(bool value) {
    this._valueCheckBox = value;
    this.controllerIdentitasBerlakuSampai.text = "";
    notifyListeners();
  }

  String get radioValueGender {
    return _radioValueGender;
  }

  set radioValueGender(String value) {
    if(value == "01" || value == "02"){
      _isGenderMandatoryShowError = false;
    }else{
      _isGenderMandatoryShowError = true;
    }
    this._radioValueGender = value;
    notifyListeners();
  }

  UnmodifiableListView<ReligionModel> get itemsReligion {
    return UnmodifiableListView(this._itemsReligion);
  }

  ReligionModel get religionSelected {
    return this._religionSelected;
  }

  set religionSelected(ReligionModel value) {
    this._religionSelected = value;
    notifyListeners();
  }

  UnmodifiableListView<EducationModel> get listEducation {
    return UnmodifiableListView(this._listEducation);
  }

  EducationModel get educationSelected {
    return this._educationSelected;
  }

  set educationSelected(EducationModel value) {
    this._educationSelected = value;
    notifyListeners();
  }

  TextEditingController get controllerJumlahTanggungan =>
      _controllerJumlahTanggungan;

  UnmodifiableListView<MaritalStatusModel> get listMaritalStatus {
    return UnmodifiableListView(this._listMaritalStatus);
  }

  MaritalStatusModel get maritalStatusSelected {
    return this._maritalStatusSelected;
  }

  set maritalStatusSelected(MaritalStatusModel value) {
    this._maritalStatusSelected = value;
    notifyListeners();
  }

  TextEditingController get controllerEmail => _controllerEmail;

  TextEditingController get controllerNoHp => _controllerNoHp;

  TextEditingController get controllerNoHp2 => _controllerNoHp2;

  TextEditingController get controllerNoHp3 => _controllerNoHp3;

  int get radioValueIsHaveNPWP => _radioValueIsHaveNPWP;

  set radioValueIsHaveNPWP(int value) {
    this._controllerNoNPWP.clear();
    if(value == 0 || value == 1){
      _isNPWPMMandatoryShowError = true;
    }else{
      _isNPWPMMandatoryShowError = false;
    }
    this._radioValueIsHaveNPWP = value;
    notifyListeners();
  }

  bool get isNoHp1WA => _isNoHp1WA;

  set isNoHp1WA(bool value) {
    this._isNoHp1WA = value;
    notifyListeners();
  }

  bool get isNoHp2WA => _isNoHp2WA;

  set isNoHp2WA(bool value) {
    this._isNoHp2WA = value;
    notifyListeners();
  }

  bool get isNoHp3WA => _isNoHp3WA;

  set isNoHp3WA(bool value) {
    this._isNoHp3WA = value;
    notifyListeners();
  }

  TextEditingController get controllerNoNPWP => _controllerNoNPWP;

  TextEditingController get controllerNamaSesuaiNPWP =>
      _controllerNamaSesuaiNPWP;

  UnmodifiableListView<JenisNPWPModel> get listJenisNPWP {
    return UnmodifiableListView(this._listJenisNPWP);
  }

  UnmodifiableListView<TandaPKPModel> get listTandaPKP {
    return UnmodifiableListView(this._listTandaPKP);
  }

  JenisNPWPModel get jenisNPWPSelected {
    return this._jenisNPWPSelected;
  }

  set jenisNPWPSelected(JenisNPWPModel value) {
    this._jenisNPWPSelected = value;
    notifyListeners();
  }

  TandaPKPModel get tandaPKPSelected {
    return this._tandaPKPSelected;
  }

  set tandaPKPSelected(TandaPKPModel value) {
    this._tandaPKPSelected = value;
    notifyListeners();
  }

  TextEditingController get controllerAlamatSesuaiNPWP =>
      _controllerAlamatSesuaiNPWP;

  BirthPlaceModel get birthPlaceSelected => _birthPlaceSelected;

  set birthPlaceSelected(BirthPlaceModel value) {
    this._birthPlaceSelected = value;
    notifyListeners();
  }

  TextEditingController get controllerTempatLahirSesuaiIdentitasLOV =>
      _controllerTempatLahirSesuaiIdentitasLOV;

  //function untuk pindah ke halaman search tempat lahir
  void searchBirthPlace(BuildContext context) async{
    BirthPlaceModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchBirthPlaceChangeNotifier(),
                child: SearchBirthPlace())));
    if (data != null) {
      this._birthPlaceSelected = data;
      this._controllerTempatLahirSesuaiIdentitasLOV.text = "${data.KABKOT_ID} - ${data.KABKOT_NAME}";
      notifyListeners();
    } else {
      return;
    }
  }

  bool get isEnableFieldFullName => _isEnableFieldFullName;

  set isEnableFieldFullName(bool value) {
    this._isEnableFieldFullName = value;
    notifyListeners();
  }

  bool get isEnableFieldBirthDate => _isEnableFieldBirthDate;

  set isEnableFieldBirthDate(bool value) {
    this._isEnableFieldBirthDate = value;
    notifyListeners();
  }

  bool get isEnableFieldBirthPlace => _isEnableFieldBirthPlace;

  set isEnableFieldBirthPlace(bool value) {
    this._isEnableFieldBirthPlace = value;
    notifyListeners();
  }

  bool get isEnableFieldIdentityNumber => _isEnableFieldIdentityNumber;

  set isEnableFieldIdentityNumber(bool value) {
    this._isEnableFieldIdentityNumber = value;
    notifyListeners();
  }

  void clearFormInfoNasabah(){
    this._disableJenisPenawaran = false;
    this._enableDataDedup = false;
    this._isDisablePACIAAOSCONA = false;
    this._flag = false;
    this._autoValidate = false;
    this._gcModel = null;
    this._identitasModel = null;
    this._controllerTglIdentitas.clear();
    this._valueCheckBox = true;
    this._controllerIdentitasBerlakuSampai.clear();
    this._controllerNoIdentitas.clear();
    this._controllerNamaLengkapSesuaiIdentitas.clear();
    this._controllerTempatLahirSesuaiIdentitasLOV.clear();
    this._controllerJumlahTanggungan.clear();
    this._birthPlaceSelected = null;
    this._educationSelected = null;
    this._religionSelected = null;
    this._maritalStatusSelected = null;
    this._controllerNoHp.clear();
    this._isNoHp1WA = false;
    this._initialDateForTglLahir = DateTime(dateNow.year, dateNow.month, dateNow.day);
    this._controllerNoNPWP.clear();
    this._radioValueIsHaveNPWP = 0;
    clearDataNPWP();
  }

  void clearDataNPWP() {
    this._controllerNoNPWP.clear();
    this._controllerNamaSesuaiNPWP.clear();
    this._jenisNPWPSelected = null;
    this._tandaPKPSelected = null;
    this._controllerAlamatSesuaiNPWP.clear();
  }

  void setDefaultValue(){
    this._identitasModel = this._itemsIdentitas[0];
  }

  bool get isGuarantorVisible => _isGuarantorVisible;

  set isGuarantorVisible(bool value) {
    this._isGuarantorVisible = value;
    notifyListeners();
  }

  //function untuk cek kondisi membutuhkan penjamin jika kondisi menikah dan umur < 21
  void checkNeedGuarantor(){
    print("checkNeedGuarantor");
    if(this._maritalStatusSelected.id == "02" && _isNotValidAge()){
      this._isGuarantorVisible = true;
    }
    else{
      this._isGuarantorVisible = false;
    }
    print("cek guarantor (false = hide, true = show) = ${this._isGuarantorVisible}");
    notifyListeners();
  }

  // fungsi untuk menghitung umur berdasarkan tgl lahir dengan tanggal sekarang
  bool _isNotValidAge(){
    Duration difference = dateNow.difference(_initialDateForTglLahir);
    // Duration difference = dateNow.difference(DateTime.parse(_controllerTglIdentitas.text));
    print(difference.inDays);
    if (difference.inDays < 7670) {
      return true;
    }
    else{
      return false;
    }
  }

  //function save data customer nasabah ke SQLite
  void saveToSQLite() async{
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _custType = _preferences.getString("cust_type");

    await _dbHelper.insertMS2CustomerPersonal(MS2CustomerIndividuModel(null, _customerIndividualID,
        this._controllerJumlahTanggungan.text != "" ? int.parse(this._controllerJumlahTanggungan.text) : null, _gcModel, _educationSelected, _identitasModel,
        _controllerNoIdentitas.text, _controllerNamaLengkapSesuaiIdentitas.text, _controllerNamaLengkap.text, "nama alias",
        "degree", _initialDateForTglLahir.toString(), _controllerTempatLahirSesuaiIdentitas.text, _birthPlaceSelected, _radioValueGender,
        _radioValueGender == "01"? "Laki laki" : "Perempuan", "id_date", "id_expired_date", _religionSelected, _maritalStatusSelected,
        _controllerNoHp.text, "email", "fb", "bb", "kk", "fav_color", "fav_brand", "relation_status_emg", "full_name_id_emg",
        "full_name_emg", "degree_emg", "email_emg", "handphone_emg", "created_date", "created_by", "modified_date", "modified_by",
        1, 2, _isNoHp1WA ? "1" : "0", _isNoHp2WA ? "1" : "0", _isNoHp3WA ? "1" : "0",
        _isGCChanges,
        _isIdentitasModelChanges,
        _isNoIdentitasChanges,
        _isTglIdentitasChanges,
        _isKTPBerlakuSeumurHidupChanges,
        _isIdentitasBerlakuSampaiChanges,
        _isNamaLengkapSesuaiIdentitasChanges,
        _isNamaLengkapChanges,
        _isTglLahirChanges,
        _isTempatLahirSesuaiIdentitasChanges,
        _isTempatLahirSesuaiIdentitasLOVChanges,
        _isGenderChanges,
        _isReligionChanges,
        _isEducationSelectedChanges,
        _isJumlahTanggunganChanges,
        _isMaritalStatusSelectedChanges,
        _isEmailChanges,
        _isNoHpChanges,
        _isNoHp1WAChanges,
        _isNoHp2Changes,
        _isNoHp2WAChanges,
        _isNoHp3Changes,
        _isNoHp3WAChanges
    ));

    print("cek nama npwp ${ _controllerNamaSesuaiNPWP.text}");
    // if(_radioValueIsHaveNPWP == 1){
      await _dbHelper.insertMS2Customer(MS2CustomerModel(
          null,
          customerID,//customerID
          obligorID,
          _custType,
          _controllerAlamatSesuaiNPWP.text,
          _controllerNamaSesuaiNPWP.text,
          _controllerNoNPWP.text,
          this._jenisNPWPSelected,
          this._radioValueIsHaveNPWP,
          this._tandaPKPSelected != null ? this._tandaPKPSelected.id : null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          1,
          _isHaveNPWPChanges ? '1' : '0',
          _isNoNPWPChanges ? '1' : '0',
          _isNamaSesuaiNPWPChanges ? '1' : '0',
          _isJenisNPWPChanges ? '1' : '0',
          _isTandaPKPChanges ? '1' : '0',
          _isAlamatSesuaiNPWPChanges ? '1' : '0'
      ));
    // }
  }

  Future<bool> deleteSQLite() async{
    return await _dbHelper.deleteMS2CustomerPersonal();
  }

  bool _setupData = false;

  bool get setupData => _setupData;

  set setupData(bool value) {
    this._setupData = value;
  }

  String _custType;
  String _lastKnownState;

  String get custType => _custType;

  set custType(String value) {
    this._custType = value;
    notifyListeners();
  }

  String get lastKnownState => _lastKnownState;

  set lastKnownState(String value) {
    this._lastKnownState = value;
    notifyListeners();
  }

  bool get disableJenisPenawaran => _disableJenisPenawaran;

  set disableJenisPenawaran(bool value) {
    this._disableJenisPenawaran = value;
  }

  Future<void> setPreference() async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    this._custType = _preferences.getString("cust_type");
    this._lastKnownState = _preferences.getString("last_known_state");
    if(_preferences.getString("jenis_penawaran") == "002"){
      this._disableJenisPenawaran = true;
    }
    // if(this._lastKnownState == "SRE" || this._lastKnownState == "RSVY" || this._lastKnownState == "DKR") {
    //   this._enableDataDedup = true;
    // }
    if(this._lastKnownState == "IDE") {
      // saat IDE tidak boleh diedit
      this._enableDataDedup = true;
    }
    if(this._lastKnownState == "AOS" || this._lastKnownState == "CONA" || this._lastKnownState == "IA" || this._lastKnownState == "PAC") {
      this._isDisablePACIAAOSCONA = true;
    }
  }

  // function untuk pengecekan data jika setiap form ada perubahan
  Future<void> checkDataChanges(int index) async{
    List _data = await _dbHelper.selectDataInfoNasabah();
    List _dedup = await _dbHelper.selectDataInfoNasabahFromDataDedup();
    List _dataNPWP = await _dbHelper.selectDataInfoNasabahNPWP();

      if(_dedup.isNotEmpty && _lastKnownState == "DKR") {
            _isNamaLengkapChanges = this._controllerNamaLengkap.text != _dedup[index]['nama'] || _dedup[index]['edit_nama'] == '1';
            _isTglLahirChanges = this._controllerTglLahir.text != dateFormat.format(DateTime.parse(_dedup[index]['tanggal_lahir'])) || _dedup[index]['edit_tanggal_lahir'] == '1';
            _isNoIdentitasChanges = this._controllerNoIdentitas.text != _dedup[index]['no_identitas'] || _dedup[index]['edit_no_identitas'] == '1';
            _isTempatLahirSesuaiIdentitasChanges = this._controllerTempatLahirSesuaiIdentitas.text != _dedup[index]['tempat_lahir_sesuai_identitas'] ||  _dedup[index]['edit_tempat_lahir_sesuai_identitas'] == '1';
      }
      if(_data.isNotEmpty && _lastKnownState == "DKR"){
            _isGCChanges = this._gcModel.id != _data[index]['cust_group'] || _data[index]['edit_cust_group'] == '1';
            _isidentitasModelChanges = this._identitasModel.id != _data[index]['id_type'] || _data[index]['edit_id_type'] == '1';
            _isTglIdentitasChanges = this._controllerTglIdentitas.text != _data[index][''] || _data[index][''] == '1';
            _isIdentitasBerlakuSampaiChanges = this._controllerIdentitasBerlakuSampai.text != _data[index]['id_expired_date'] || _data[index]['edit_id_expired_date'] == '1';
            _isNamaLengkapSesuaiIdentitasChanges = this._controllerNamaLengkapSesuaiIdentitas.text != _data[index]['full_name_id'] || _data[index]['edit_full_name_id'] == '1';
            _isTempatLahirSesuaiIdentitasLOVChanges = this._controllerTempatLahirSesuaiIdentitasLOV.text != _data[index]['place_of_birth_kabkota'] || _data[index]['edit_place_of_birth_kabkota'] == '1';
            _isGenderChanges = this._radioValueGender != _data[index]['gender'] || _data[index]['edit_gender'] == '1';
            _isReligionChanges = this._religionSelected.id != _data[index]['religion'] || _data[index]['edit_religion'] == '1';
            _isEducationSelectedChanges = this._educationSelected.id != _data[index]['education'] || _data[index]['edit_education']== '1';
            _isJumlahTanggunganChanges = this._controllerJumlahTanggungan.text != _data[index]['no_of_liability'] || _data[index]['edit_no_of_liability'] == '1';
            _isMaritalStatusSelectedChanges = this._maritalStatusSelected.id != _data[index]['marital_status'] || _data[index]['edit_marital_status'] == '1';
            _isEmailChanges = this._controllerEmail.text != _data[index]['email'] || _data[index]['edit_email'] == '1';
            _isNoHpChanges = this._controllerNoHp.text != _data[index]['handphone_no'] || _data[index]['edit_handphone_no'] == '1';
            _isNoHp1WAChanges = this._isNoHp1WA != _data[index][''] || _data[index][''] == '1';
            _isNoHp2Changes = this._controllerNoHp2.text != _data[index]['no_wa_2'] || _data[index]['edit_no_wa_2'] == '1';
            _isNoHp2WAChanges = this._isNoHp2WA != _data[index][''] || _data[index][''] == '1';
            _isNoHp3Changes = this._controllerNoHp3.text != _data[index]['no_wa_3'] || _data[index]['edit_no_wa_3'] == '1';
            _isNoHp3WAChanges = this._isNoHp3WA != _data[index][''] || _data[index][''] == '1';
      }

      if(_dataNPWP.isNotEmpty && _lastKnownState == "DKR"){
            _isHaveNPWPChanges = this._radioValueIsHaveNPWP != _dataNPWP[index]['flag_npwp'] || _dataNPWP[index]['edit_flag_npwp'] == '1';
            _isNamaSesuaiNPWPChanges = this._controllerNamaSesuaiNPWP.text != _dataNPWP[index]['npwp_name'] || _dataNPWP[index]['edit_npwp_name'] == '1';
            _isJenisNPWPChanges = this._jenisNPWPSelected.id != _dataNPWP[index]['npwp_type'] || _dataNPWP[index]['edit_npwp_type'] == '1';
            _isTandaPKPChanges = this._tandaPKPSelected.id != _dataNPWP[index]['pkp_flag'] || _dataNPWP[index]['edit_pkp_flag'] == '1';
            _isNoNPWPChanges = this._controllerNoNPWP.text != _dataNPWP[index]['npwp_no'] || _dataNPWP[index]['edit_npwp_no'] == '1';
            _isAlamatSesuaiNPWPChanges = this._controllerAlamatSesuaiNPWP.text != _dataNPWP[index]['npwp_address'] || _dataNPWP[index]['edit_npwp_address'] == '1';
      }
  }

  // function set data di setiap form atau variable dari SQLite
  Future<void> setDataSQLite() async{
    debugPrint("SETUP_DATA_INFO_NASABAH");
    // setupData = true;
    List _data = await _dbHelper.selectDataInfoNasabah();
    if(_data.isNotEmpty && this._controllerNoIdentitas.text.isEmpty){
      // List _dedup = await _dbHelper.selectDataInfoNasabahFromDataDedup();
      // if(_dedup.isNotEmpty){
      //   this._controllerNamaLengkap.text = _dedup[0]['nama'];
      //   this._controllerTglLahir.text = dateFormat.format(DateTime.parse(_dedup[0]['tanggal_lahir']));
      //   this._controllerNoIdentitas.text = _dedup[0]['no_identitas'];
      //   this._controllerTempatLahirSesuaiIdentitas.text = _dedup[0]['tempat_lahir_sesuai_identitas'];
      // }
      // setupData = false;
      customerIndividualID = _data[0]['customerIndividualID'];
      debugPrint("customerIndividualID ${_data[0]['customerIndividualID']}");
      for(int i=0; i < this._itemsGC.length; i++){
        if(_data[0]['cust_group'] == _itemsGC[i].id){
          _gcModel = _itemsGC[i];
        }
      }
      this._controllerJumlahTanggungan.text = _data[0]['no_of_liability'] != "null" && _data[0]['no_of_liability'] != "" ? _data[0]['no_of_liability'].toString() : "";
      _data[0]['full_name'] != "null" ? this._controllerNamaLengkap.text = _data[0]['full_name'] : this._controllerNamaLengkap.text = "";
      _data[0]['full_name_id'] != "null" && _data[0]['full_name_id'] != "" ? this._controllerNamaLengkapSesuaiIdentitas.text = _data[0]['full_name_id'] : this._controllerNamaLengkapSesuaiIdentitas.text = "";
      _data[0]['date_of_birth'] != "null" ? this._controllerTglLahir.text = dateFormat.format(DateTime.parse(_data[0]['date_of_birth'])) : this._controllerTglLahir.text = "";
      this._initialDateForTglLahir = _data[0]['date_of_birth'] != "null" ? DateTime.parse(_data[0]['date_of_birth']) : DateTime(dateNow.year, dateNow.month, dateNow.day);
      _data[0]['id_no'] != "null" ? this._controllerNoIdentitas.text = _data[0]['id_no'] : this._controllerNoIdentitas.text = "";
      _data[0]['place_of_birth'] != "null" ? this._controllerTempatLahirSesuaiIdentitas.text = "${_data[0]['place_of_birth']}" : this._controllerTempatLahirSesuaiIdentitas.text = "";

      if(_data[0]['place_of_birth_kabkota'] != "" && _data[0]['place_of_birth_kabkota'] != "null"){
        this._controllerTempatLahirSesuaiIdentitasLOV.text = "${_data[0]['place_of_birth_kabkota']} - ${_data[0]['place_of_birth_kabkota_desc']}";
        this._birthPlaceSelected = BirthPlaceModel(_data[0]['place_of_birth_kabkota'], _data[0]['place_of_birth_kabkota_desc']);
      }
      _data[0]['gender'] == "01" ? _radioValueGender = "01" : _radioValueGender = "02";
      if(_data[0]['id_type'] != "" && _data[0]['id_type'] != null){
        this._identitasModel = IdentityModel(_data[0]['id_type'], _data[0]['id_desc']);
        _data[0]['handphone_no'] != "null" &&  _data[0]['handphone_no'] != ""? this._controllerNoHp.text = _data[0]['handphone_no'] : this._controllerNoHp.text = "";
      }
      if(_data[0]['no_wa'] != "null"){
        isNoHp1WA = _data[0]['no_wa'] == "1";
      }
      if(_data[0]['no_wa'] != "null"){
        isNoHp2WA = _data[0]['no_wa_2'] == "1";
      }
      if(_data[0]['no_wa'] != "null"){
        isNoHp3WA = _data[0]['no_wa_3'] == "1";
      }
      for(int i=0; i < _listEducation.length; i++){
        if(_data[0]['education'] == _listEducation[i].id){
          _educationSelected = _listEducation[i];
        }
      }
      for(int i=0; i<_itemsReligion.length; i++){
        if(_data[0]['religion'] ==  _itemsReligion[i].id){
          _religionSelected = _itemsReligion[i];
        }
      }
      for(int i=0; i < _listMaritalStatus.length; i++){
        if(_data[0]['marital_status'] == _listMaritalStatus[i].id){
          _maritalStatusSelected = _listMaritalStatus[i];
        }
      }
      if(_maritalStatusSelected != null) checkNeedGuarantor();
      List _dataNPWP = await _dbHelper.selectDataInfoNasabahNPWP();
      // kalau FLAG_NPWP 0 nggak punya, kalau 1 ada npwp
      print("cek data nasabah npwp $_dataNPWP");
      if(_dataNPWP.isNotEmpty){
        if(_dataNPWP[0]['npwp_name'] != "null" && _dataNPWP[0]['npwp_name'] != ""){
          this._controllerNamaSesuaiNPWP.text = _dataNPWP[0]['npwp_name'];
        }
        if(_dataNPWP[0]['npwp_no'] != "null" && _dataNPWP[0]['npwp_no'] != ""){
          this._controllerAlamatSesuaiNPWP.text = _dataNPWP[0]['npwp_address'];
          this._controllerNoNPWP.text = _dataNPWP[0]['npwp_no'];
        }
        if(_dataNPWP[0]['flag_npwp'] != "null"){
          this._radioValueIsHaveNPWP = _dataNPWP[0]['flag_npwp'];
        }
        if(_dataNPWP[0]['npwp_type'] != "null" && _dataNPWP[0]['npwp_type'] != ""){
          for(int i=0; i < this._listJenisNPWP.length; i++){
            if(_dataNPWP[0]['npwp_type']== this._listJenisNPWP[i].id){
              jenisNPWPSelected = this._listJenisNPWP[i];
            }
          }
        }
        if(_dataNPWP[0]['pkp_flag'] != "null" && _dataNPWP[0]['pkp_flag'] != ""){
          for(int i=0; i < this._listTandaPKP.length; i++){
            if(_dataNPWP[0]['pkp_flag']== this._listTandaPKP[i].id){
              tandaPKPSelected = this._listTandaPKP[i];
            }
          }
        }
        customerID = _dataNPWP[0]['customerID'];
        obligorID = _dataNPWP[0]['id_obligor'];
        debugPrint("OBLIGOR_ID NASABAH $obligorID");
        debugPrint("OBLIGOR_ID NASABAH ${_dataNPWP[0]['flag_npwp'] == "null"}");
      }
    }
    else{
      debugPrint("data kosong");
      // setupData = false;
    }
    this._initialDateForTglLahir = _data[0]['date_of_birth'] != "null" ? DateTime.parse(_data[0]['date_of_birth']) : DateTime(dateNow.year, dateNow.month, dateNow.day);
    await checkDataChanges(0);
    notifyListeners();
  }

  ValidateData _validateData = ValidateData();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void _showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating,backgroundColor: snackbarColor, duration: Duration(seconds: 4)));
  }

  void checkValidNoHP(String value) {
    if (value == "08") {
      Future.delayed(Duration(milliseconds: 100), () {
        this._controllerNoHp.clear();
      });
    } else {
      return;
    }
  }
}
