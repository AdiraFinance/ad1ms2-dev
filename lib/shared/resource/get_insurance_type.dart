
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/models/coverage1_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_collatelar_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

Future<List<CoverageModel>> getInsuranceType(BuildContext context, String company, String product) async{
  var storage = FlutterSecureStorage();
  print("di eksekusi");
  List<CoverageModel> _listCoverage = [];
  try{
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
    var _body = jsonEncode({
      "P_INSR_COMPANY_ID" : company,
      "P_INSR_LEVEL" : "2",
      "P_INSR_PRODUCT_ID" : product,
      "P_PARA_OBJECT_ID" : Provider.of<InformationCollateralChangeNotifier>(context, listen: false).objectSelected.id
    });
    print("body coverage = $_body");

    String _fieldCoverage1 = await storage.read(key: "FieldCoverage1");
    final _response = await _http.post(
      "${BaseUrl.urlGeneral}$_fieldCoverage1",
        // "${urlPublic}api/asuransi/get-insurance-type",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    ).timeout(Duration(seconds: 10));

    final _result = jsonDecode(_response.body);
    List _data = _result['data'];
    print("result coverage = $_data");
    if(_data.isNotEmpty){
      for(int i=0; i < _data.length; i++){
        _listCoverage.add(CoverageModel(_data[i]['PARENT_KODE'], _data[i]['KODE'], _data[i]['DESKRIPSI'].toString().trim()));
      }
      return _listCoverage;
    }
    else{
      throw Exception("Data tidak ditemukan");
    }
  }
  on TimeoutException catch(_){
    throw Exception("Timeout connection");
  }
  catch (e) {
    throw Exception("${e.toString()}");
  }
}