import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';

import '../../main.dart';

var storage = FlutterSecureStorage();
Future<Map> getCustInfoPer(String orderNo) async{
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  var _body = jsonEncode(
      {
        "P_ORDERNO": orderNo
      }
  );
  String _getDataFromDB = await storage.read(key: "GetDataFromDB(Individu)");
  print("${BaseUrl.urlGeneral}$_getDataFromDB");
  try{
    final _response = await _http.post(
        "${BaseUrl.urlGeneral}$_getDataFromDB",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    ).timeout(Duration(seconds: 10));
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      return _result;
    }
    else{
      throw "Failed get data error ${_response.statusCode}";
    }
  }
  on TimeoutException catch(_){
    throw "Timeout connection";
  }
  catch (e) {
    throw "${e.toString()}";
  }
}

Future<Map> getCustInfoCom(String orderNo) async{
  final ioc = new HttpClient();
  ioc.badCertificateCallback =
      (X509Certificate cert, String host, int port) => true;

  final _http = IOClient(ioc);
  var _body = jsonEncode(
      {
        "P_ORDERNO": orderNo
      }
  );
  String _getDataFromDB = await storage.read(key: "GetDataFromDB(Company)");
  try{
    final _response = await _http.post(
        "${BaseUrl.urlGeneral}$_getDataFromDB",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    ).timeout(Duration(seconds: 10));
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      return _result;
    }
    else{
      throw "Failed get data error ${_response.statusCode}";
    }
  }
  on TimeoutException catch(_){
    throw "Timeout connection";
  }
  catch (e) {
    throw "${e.toString()}";
  }
}