import 'dart:async';
import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

import '../../main.dart';
import '../constants.dart';

class ValidateData{
  var storage = FlutterSecureStorage();
  Future<String> validateAge(String birthDate) async{
    String _validateCustAge = await storage.read(key: "ValidateCustAge");
    var _body = jsonEncode(
      {
        "P_AC_DATE_BIRTH": birthDate
      }
    );

    try{
      final _response = await http.post(
        "${BaseUrl.urlGeneral}$_validateCustAge",
          body: _body,
          headers: {'Content-type': 'application/json', "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));
      print(_response.statusCode);
      print("${_response.body}");
      final _data = jsonDecode(_response.body);
      if(_response.statusCode == 200){
        return _data['message'];
      }
      else{
        throw "Error ${_response.statusCode} validate age";
      }
    }
    on TimeoutException catch(_){
      throw "Request timeout validate age";
    }
    catch(e){
      throw "${e.toString()}";
    }
  }

  Future<String> validateName(String name,String custType) async{
    String _validateCustName = await storage.read(key: "ValidateCustName");
    var _body = jsonEncode(
        {
          "P_CUST_NAME": name,
          "P_CUST_TYPE": custType
        }
    );

    try{
      final _response = await http.post(
          "${BaseUrl.urlGeneral}$_validateCustName",
          body: _body,
          headers: {'Content-type': 'application/json', "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));
      print(_response.statusCode);
      print("${_response.body}");
      final _data = jsonDecode(_response.body);
      if(_response.statusCode == 200){
        return _data['message'];
      }
      else{
        throw "Error ${_response.statusCode} validate name";
      }
    }
    on TimeoutException catch(_){
      throw "Request timeout validate age";
    }
    catch(e){
      throw "${e.toString()}";
    }
  }

  Future<String> validateNPWP(String npwp) async{
    String _validateNPWP = await storage.read(key: "ValidateNPWP");
    var _body = jsonEncode(
        {
          "NPWP_NO": npwp
        }
    );

    try{
      final _response = await http.post(
          "${BaseUrl.urlGeneral}$_validateNPWP",
          body: _body,
          headers: {'Content-type': 'application/json', "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));
      print("status validate npwp/nama = ${_response.statusCode}");
      print("${_response.body}");
      final _data = jsonDecode(_response.body);
      if(_response.statusCode == 200){
        return _data['message'];
      }
      else{
        throw "Error ${_response.statusCode} validate NPWP";
      }
    }
    on TimeoutException catch(_){
      throw "Request timeout validate age";
    }
    catch(e){
      throw "${e.toString()}";
    }
  }
}