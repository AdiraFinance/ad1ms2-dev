import 'dart:async';
import 'dart:convert';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/guarantor_idividual_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_objt_coll_oto_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_objt_coll_prop_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_taksasi_model.dart';
import 'package:ad1ms2_dev/models/ms2_application_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_addr_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_income_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_additional_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_app_document_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_application_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_income_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_type_installment_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_subsidy_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_major_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_object_karoseri_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_collatelar_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_salesman_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_company_manajemen_pic_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_pendapatan_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_rincian_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_credit_limit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_guarantor_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_income_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_individu_change_notifier/form_m_parent_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_info_keluarga_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_keluarga(ibu)_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_occupation_change_notif.dart';
import 'package:ad1ms2_dev/shared/info_wmp_change_notifier.dart';
import 'package:ad1ms2_dev/shared/pemegang_saham_lembaga_change_notif.dart';
import 'package:ad1ms2_dev/shared/pemegang_saham_pribadi_change_notifier.dart';
import 'package:ad1ms2_dev/shared/survey/list_survey_photo_change_notifier.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../../main.dart';
import '../constants.dart';
import '../form_m_company_alamat_change_notif.dart';
import '../form_m_company_manajemen_pic_change_notif.dart';

// class untuk kumpulan fungsi save partial
class SubmitDataPartial{
  var _storage = FlutterSecureStorage();

  Future<String> submitCreditLimit(BuildContext context) async{
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _providerCreditLimit = Provider.of<FormMCreditLimitChangeNotifier>(context,listen: false);
    var _body = jsonEncode({
      "P_ACTIVATION_DATE": _providerCreditLimit.activationDate,
      "P_APPL_NO": "",
      "P_CREATED_BY": _preferences.getString("username"),
      "P_CUST_ADDRESS": _providerCreditLimit.controllerConsumerAddress.text,
      "P_CUST_NAME": _providerCreditLimit.controllerConsumerName.text,
      "P_CUST_TYPE": _providerCreditLimit.statusAoro,
      "P_DISBURSE_TYPE": int.parse(_providerCreditLimit.disburseType.toString()),
      "P_FLAG_ELIGIBLE": _providerCreditLimit.flagEligible,
      "P_GRADING": _providerCreditLimit.controllerGrading.text,
      "P_INSTALLMENT_AMOUNT": int.parse(_providerCreditLimit.installmentAmount.toString()),
      "P_JENIS_PENAWARAN": _providerCreditLimit.typeOfferSelected.KODE,
      "P_JENIS_PENAWARAN_DESC": _providerCreditLimit.typeOfferSelected.DESCRIPTION,
      "P_LME_ID": _providerCreditLimit.lmeId,
      "P_MAX_INSTALLMENT": double.parse(_providerCreditLimit.controllerMaxInstallment.toString()),
      "P_MAX_PH": double.parse(_providerCreditLimit.controllerMaxPH.toString()),
      "P_MAX_TENOR": int.parse(_providerCreditLimit.controllerTenor.toString()),
      "P_NOREFF": _providerCreditLimit.noReferensi,
      "P_NOTES": _providerCreditLimit.controllerNotes.text,
      "P_OID": _providerCreditLimit.oid,
      "P_OPSI_MULTIDISBURSE": _providerCreditLimit.opsiMultidisburse,
      "P_ORDER_NO": _preferences.getString("order_no"),
      "P_PENCAIRAN_KE": int.parse(_providerCreditLimit.controllerDisbursementNo.toString()),
      "P_PH_AMOUNT": double.parse(_providerCreditLimit.controllerMaxPH.toString()),
      "P_PLAFOND_MRP_RISK": 0,
      "P_PORTFOLIO": _providerCreditLimit.portofolio,
      "P_PRODUCT_ID": _providerCreditLimit.productId,
      "P_PRODUCT_ID_DESC": _providerCreditLimit.productIdDesc,
      "P_REMAINING_TENOR": _providerCreditLimit.remainingTenor,
      "P_TENOR": int.parse(_providerCreditLimit.controllerTenor.toString()),
      "P_VOUCHER_CODE": _providerCreditLimit.voucherCode
    });
    print("print body save partial credit limit : $_body");

    // String _creditLimitPartial = await _storage.read(key: "CreditLimitPartial");
    // try{
    //   var _timeStart = DateTime.now();
    //   final _response = await http.post(
    //       "${BaseUrl.urlGeneral}$_creditLimitPartial",
    //       // "http://192.168.1.105/orca_api/public/login",
    //       body: _body,
    //       headers: {'Content-type': 'application/json', "Authorization":"bearer $token"}
    //   ).timeout(Duration(seconds: 30));
    //
    //   var _data = jsonDecode(_response.body);
    //   Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, _response.body, "Save partial credit limit");
    //   debugPrint("save credit limit $_data");
    //
    //   if(_response.statusCode == 200){
    //     if(_data['status'] != "1"){
    //       throw "${_data['message']}";
    //     }
    //     else{
    //       return _data['message'];
    //     }
    //   }
    //   else{
    //     throw "Save partial credit limit Error ${_response.statusCode}";
    //   }
    // }
    // on TimeoutException catch(_){
    //   throw "Save partial credit limit timeout connection";
    // }
    // catch(e){
    //   Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, DateTime.now(), DateTime.now(), _body, e.toString(), "Save partial credit limit");
    //   throw "Save partial credit limit ${e.toString()}";
    // }
  }

  Future<String> submitCreditLimitDetail(BuildContext context) async{
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _providerCreditLimit = Provider.of<FormMCreditLimitChangeNotifier>(context,listen: false);
    var _body = jsonEncode({
      "P_APPL_OBJT_ID": "string",
      "P_CREATED_BY": "string",
      "P_FLAG_LME": "string",
      "P_ORDER_NO": "string",
      "P_SOURCE_REFF_ID": "string"
    });

    String _creditLimitPartial = await _storage.read(key: "CreditLimitDetailPartial");
    try{
      var _timeStart = DateTime.now();
      final _response = await http.post(
          "${BaseUrl.urlGeneral}$_creditLimitPartial",
          // "http://192.168.1.105/orca_api/public/login",
          body: _body,
          headers: {'Content-type': 'application/json', "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));

      var _data = jsonDecode(_response.body);
      Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, _response.body, "Save partial credit limit");
      debugPrint("save credit limit $_data");

      if(_response.statusCode == 200){
        if(_data['status'] != "1"){
          throw "${_data['message']}";
        }
        else{
          return _data['message'];
        }
      }
      else{
        throw "Save partial credit limit Error ${_response.statusCode}";
      }
    }
    on TimeoutException catch(_){
      throw "Save partial credit limit timeout connection";
    }
    catch(e){
      Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, DateTime.now(), DateTime.now(), _body, e.toString(), "Save partial credit limit");
      throw "Save partial credit limit ${e.toString()}";
    }
  }

  // save partial untuk customer PER
  Future<String> submitCustomerIndividu(BuildContext context,int idx) async{
    String _rincianIndividuPartial = await _storage.read(key: "RincianIndividuPartial");
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _providerInfoNasabah = Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false);
    var _providerAlamatInfoNasabah = Provider.of<FormMInfoAlamatChangeNotif>(context,listen: false);
    var _providerInfoKeluargaIbu = Provider.of<FormMInformasiKeluargaIBUChangeNotif>(context,listen: false);
    var _providerInfoKeluarga = Provider.of<FormMInfoKeluargaChangeNotif>(context,listen: false);
    var checkID = await DbHelper().selectDataInfoNasabah();
    var checkIDIbu = await DbHelper().selectDataInfoKeluarga("05");
    var checkIDFamily = await DbHelper().selectDataInfoKeluarga("");
    List _custAddress = [];
    List _custFamily = [];
    for(int i=0; i < _providerAlamatInfoNasabah.listAlamatKorespondensi.length; i++){
      _custAddress.add({
        "P_ADDRESS": "${_providerAlamatInfoNasabah.listAlamatKorespondensi[i].address}",
        "P_ADDRESS_ID": _preferences.getString("last_known_state") != "IDE" ? _providerAlamatInfoNasabah.listAlamatKorespondensi[i].addressID : "NEW",
        "P_ADDR_DESC": "${_providerAlamatInfoNasabah.listAlamatKorespondensi[i].jenisAlamatModel.DESKRIPSI}",
        "P_ADDR_TYPE": "${_providerAlamatInfoNasabah.listAlamatKorespondensi[i].jenisAlamatModel.KODE}",
        "P_CREATED_BY": "${_preferences.getString("username")}",
        "P_FAX": "",
        "P_FAX_AREA": "",
        "P_HANDPHONE_NO": "",
        "P_KABKOT": "${_providerAlamatInfoNasabah.listAlamatKorespondensi[i].kelurahanModel.KABKOT_ID}",
        "P_KABKOT_DESC": "${_providerAlamatInfoNasabah.listAlamatKorespondensi[i].kelurahanModel.KABKOT_NAME}",
        "P_KECAMATAN": "${_providerAlamatInfoNasabah.listAlamatKorespondensi[i].kelurahanModel.KEC_ID}",
        "P_KECAMATAN_DESC": "${_providerAlamatInfoNasabah.listAlamatKorespondensi[i].kelurahanModel.KEC_NAME}",
        "P_KELURAHAN": "${_providerAlamatInfoNasabah.listAlamatKorespondensi[i].kelurahanModel.KEL_ID}",
        "P_KELURAHAN_DESC": "${_providerAlamatInfoNasabah.listAlamatKorespondensi[i].kelurahanModel.KEL_NAME}",
        "P_KORESPONDEN": _providerAlamatInfoNasabah.listAlamatKorespondensi[i].isCorrespondence ? "1" : "0",
        "P_MATRIX_ADDR": "5",
        "P_ORDER_NO": "${_preferences.getString("order_no")}",
        "P_PHONE1": "${_providerAlamatInfoNasabah.listAlamatKorespondensi[i].phone}",
        "P_PHONE1_AREA": "${_providerAlamatInfoNasabah.listAlamatKorespondensi[i].areaCode}",
        "P_PHONE2": "",
        "P_PHONE2_AREA": "",
        "P_PROVINSI": "${_providerAlamatInfoNasabah.listAlamatKorespondensi[i].kelurahanModel.PROV_ID}",
        "P_PROVINSI_DESC": "${_providerAlamatInfoNasabah.listAlamatKorespondensi[i].kelurahanModel.PROV_NAME}",
        "P_RT": "${_providerAlamatInfoNasabah.listAlamatKorespondensi[i].rt}",
        "P_RT_DESC": "",
        "P_RW": "${_providerAlamatInfoNasabah.listAlamatKorespondensi[i].rw}",
        "P_RW_DESC": "",
        "P_ZIP_CODE": "${_providerAlamatInfoNasabah.listAlamatKorespondensi[i].kelurahanModel.ZIPCODE}",
        "P_FOREIGN_BK": _preferences.getString("last_known_state") != "IDE"
            ? _providerAlamatInfoNasabah.listAlamatKorespondensi[i].foreignBusinessID != "NEW"
            ? _providerAlamatInfoNasabah.listAlamatKorespondensi[i].foreignBusinessID : "NEW" : "NEW",
        "P_ID_NO":"",
      });
    }
    debugPrint("CEK tgl lahir ${_providerInfoKeluargaIbu.controllerTglLahir.text}");
    _custFamily.add({
        "P_CREATED_BY": "${_preferences.getString("username")}",
        "P_DATE_OF_BIRTH": _providerInfoKeluargaIbu.controllerTglLahir.text,
        "P_DEGREE": "",
        "P_FAMILY_ID":_preferences.getString("last_known_state") != "IDE" ? _providerInfoKeluargaIbu.familyInfoID : "NEW",
        "P_FULL_NAME": _providerInfoKeluargaIbu.controllerNamaIdentitas.text,
        "P_FULL_NAME_ID": _providerInfoKeluargaIbu.controllerNamaLengkapdentitas.text,
        "P_GENDER": _providerInfoKeluargaIbu.radioValueGender,
        "P_GENDER_DESC": _providerInfoKeluargaIbu.radioValueGender == "01" ? "Laki Laki" : "Perempuan",
        "P_HANDPHONE_NO": _providerInfoKeluargaIbu.controllerNoHp.text != "" ? "08${_providerInfoKeluargaIbu.controllerNoHp.text}":"",
        "P_ID_DESC": _providerInfoKeluargaIbu.identitySelected != null ? _providerInfoKeluargaIbu.identitySelected.name : "",
        "P_ID_NO": _providerInfoKeluargaIbu.controllerNoIdentitas.text,
        "P_ID_TYPE": _providerInfoKeluargaIbu.identitySelected != null ? _providerInfoKeluargaIbu.identitySelected.id : "",
        "P_ORDER_NO": _preferences.getString("order_no"),
        "P_PHONE1":_providerInfoKeluargaIbu.controllerTlpn.text,
        "P_PHONE1_AREA": _providerInfoKeluargaIbu.controllerKodeArea.text,
        "P_PLACE_OF_BIRTH": _providerInfoKeluargaIbu.controllerTempatLahirSesuaiIdentitas.text,
        "P_PLACE_OF_BIRTH_KABKOTA": _providerInfoKeluargaIbu.birthPlaceSelected != null ? _providerInfoKeluargaIbu.birthPlaceSelected.KABKOT_ID : "",
        "P_PLACE_OF_BIRTH_KABKOTA_DESC": _providerInfoKeluargaIbu.birthPlaceSelected != null ? _providerInfoKeluargaIbu.birthPlaceSelected.KABKOT_NAME : "",
        "P_RELATION_STATUS": "05",
        "P_RELATION_STATUS_DESC": "",
    });
    print("panjang list keluarga ${_custFamily.length}");
    for(int i=0; i < _providerInfoKeluarga.listFormInfoKel.length; i++){
      print("FAMILY ID: ${_preferences.getString("last_known_state") != "IDE" ? _providerInfoKeluarga.listFormInfoKel[i].familyInfoID != null ? _providerInfoKeluarga.listFormInfoKel[i].familyInfoID : "NEW" : "NEW"}");
      print("TANGGAL LAHIR: ${_providerInfoKeluarga.listFormInfoKel[i].birthDate}");
      _custFamily.add({
        "P_CREATED_BY": "${_preferences.getString("username")}",
        "P_DATE_OF_BIRTH": _providerInfoKeluarga.listFormInfoKel[i].birthDate != null ? formatDateDedup(DateTime.parse(_providerInfoKeluarga.listFormInfoKel[i].birthDate)) : "", //_providerInfoKeluarga.listFormInfoKel[i].birthDate,
        "P_DEGREE": "",
        "P_FAMILY_ID": _preferences.getString("last_known_state") != "IDE" ? _providerInfoKeluarga.listFormInfoKel[i].familyInfoID != null ? _providerInfoKeluarga.listFormInfoKel[i].familyInfoID : "NEW" : "NEW",
        "P_FULL_NAME": _providerInfoKeluarga.listFormInfoKel[i].namaLengkap,
        "P_FULL_NAME_ID": _providerInfoKeluarga.listFormInfoKel[i].namaLengkapSesuaiIdentitas,
        "P_GENDER": _providerInfoKeluarga.listFormInfoKel[i].gender,
        "P_GENDER_DESC": _providerInfoKeluarga.listFormInfoKel[i].gender == "01" ? "Laki Laki" : "Perempuan",
        "P_HANDPHONE_NO": _providerInfoKeluarga.listFormInfoKel[i].noHp != "" ? "08${_providerInfoKeluarga.listFormInfoKel[i].noHp}" : "",
        "P_ID_DESC": _providerInfoKeluarga.listFormInfoKel[i].identityModel != null ? _providerInfoKeluarga.listFormInfoKel[i].identityModel.name : "",
        "P_ID_NO": _providerInfoKeluarga.listFormInfoKel[i].noIdentitas,
        "P_ID_TYPE": _providerInfoKeluarga.listFormInfoKel[i].identityModel != null ? _providerInfoKeluarga.listFormInfoKel[i].identityModel.id : "",
        "P_ORDER_NO": _preferences.getString("order_no"),
        "P_PHONE1": _providerInfoKeluarga.listFormInfoKel[i].noTlpn,
        "P_PHONE1_AREA": _providerInfoKeluarga.listFormInfoKel[i].kodeArea,
        "P_PLACE_OF_BIRTH": _providerInfoKeluarga.listFormInfoKel[i].tmptLahirSesuaiIdentitas,
        "P_PLACE_OF_BIRTH_KABKOTA": _providerInfoKeluarga.listFormInfoKel[i].birthPlaceModel != null ? _providerInfoKeluarga.listFormInfoKel[i].birthPlaceModel.KABKOT_ID : "",
        "P_PLACE_OF_BIRTH_KABKOTA_DESC": _providerInfoKeluarga.listFormInfoKel[i].birthPlaceModel != null ? _providerInfoKeluarga.listFormInfoKel[i].birthPlaceModel.KABKOT_NAME : "",
        "P_RELATION_STATUS": _providerInfoKeluarga.listFormInfoKel[i].relationshipStatusModel.PARA_FAMILY_TYPE_ID,
        "P_RELATION_STATUS_DESC": _providerInfoKeluarga.listFormInfoKel[i].relationshipStatusModel.PARA_FAMILY_TYPE_NAME,
      });
    }
    debugPrint("CEK_P_CUST_IND_ID_SAVE_PARTIAL ${_preferences.getString("last_known_state") != "IDE" ? _providerInfoNasabah.customerIndividualID : "NEW"}");
    var _body = jsonEncode({
      "custAddress": _custAddress,
      "custDetailInfoPersonal": [
        {
          "P_ALIAS_NAME": "",
          "P_CORESPONDENCE ": "",
          "P_CREATED_BY": "${_preferences.getString("username")}",
          "P_CUST_GROUP": "${_providerInfoNasabah.gcModel.id}",
          "P_CUST_GROUP_DESC": "${_providerInfoNasabah.gcModel.name}",
          "P_CUST_ID": _preferences.getString("last_known_state") != "IDE" ? _providerInfoNasabah.customerID : "NEW",
          "P_CUST_IND_ID": _preferences.getString("last_known_state") != "IDE" ? _providerInfoNasabah.customerIndividualID : "NEW",
          "P_CUST_TYPE": "PER",
          "P_DATE_OF_BIRTH": "${_providerInfoNasabah.controllerTglLahir.text}",
          "P_DEGREE": "",
          "P_EDUCATION": "${_providerInfoNasabah.educationSelected.id}",
          "P_EDUCATION_DESC": "${_providerInfoNasabah.educationSelected.text}",
          "P_EMAIL": "${_providerInfoNasabah.controllerEmail.text}",
          "P_FLAG_ID_LIFETIME": _providerInfoNasabah.isKTPBerlakuSeumurHidupChanges ? "1" : "0",
          "P_FLAG_NPWP": "${_providerInfoNasabah.radioValueIsHaveNPWP}",
          "P_FULL_NAME": "${_providerInfoNasabah.controllerNamaLengkap.text}",
          "P_FULL_NAME_ID": "${_providerInfoNasabah.controllerNamaLengkapSesuaiIdentitas.text}",
          "P_GENDER": "${_providerInfoNasabah.radioValueGender}",
          "P_GENDER_DESC": _providerInfoNasabah.radioValueGender == "01" ? "Laki Laki" : "Perempuan",
          "P_HANDPHONE_NO": _providerInfoNasabah.controllerNoHp.text != "" ?  "08${_providerInfoNasabah.controllerNoHp.text}" : "",
          "P_ID_DATE": _providerInfoNasabah.controllerTglIdentitas.text,
          "P_ID_DESC": _providerInfoNasabah.identitasModel.name,
          "P_ID_EXPIRE_DATE": _providerInfoNasabah.controllerIdentitasBerlakuSampai.text,
          "P_ID_NO": _providerInfoNasabah.controllerNoIdentitas.text,
          "P_ID_OBLIGOR": _preferences.getString("last_known_state") != "IDE"
              ? _providerInfoNasabah.obligorID != "NEW"
              ? _providerInfoNasabah.obligorID : _preferences.getString("oid").toString() : _preferences.getString("oid").toString(),
          "P_ID_TYPE": _providerInfoNasabah.identitasModel.id,
          "P_IS_GUARANTOR": "",
          "P_MARITAL_STATUS": _providerInfoNasabah.maritalStatusSelected.id,
          "P_MARITAL_STATUS_DESC": _providerInfoNasabah.maritalStatusSelected.text,
          "P_NO_OF_LIABILITY": _providerInfoNasabah.controllerJumlahTanggungan.text,
          "P_NO_WA": _providerInfoNasabah.isNoHp1WA ? "1" : "0",
          "P_NO_WA_2": "0",
          "P_NO_WA_3": "0",
          "P_NPWP_ADDRESS": _providerInfoNasabah.controllerAlamatSesuaiNPWP.text,
          "P_NPWP_NAME": _providerInfoNasabah.controllerNamaSesuaiNPWP.text,
          "P_NPWP_NO": _providerInfoNasabah.controllerNoNPWP.text,
          "P_NPWP_TYPE": _providerInfoNasabah.jenisNPWPSelected != null ? _providerInfoNasabah.jenisNPWPSelected.id : "",
          "P_NPWP_TYPE_DESC": _providerInfoNasabah.jenisNPWPSelected != null ? _providerInfoNasabah.jenisNPWPSelected.text : "",
          "P_ORDER_NO": _preferences.getString("order_no"),
          "P_PKP_FLAG": _providerInfoNasabah.tandaPKPSelected != null ? _providerInfoNasabah.tandaPKPSelected.id : "",
          "P_PLACE_OF_BIRTH": _providerInfoNasabah.controllerTempatLahirSesuaiIdentitas.text,
          "P_PLACE_OF_BIRTH_KABKOTA": _providerInfoNasabah.birthPlaceSelected != null ? _providerInfoNasabah.birthPlaceSelected.KABKOT_ID : "",
          "P_PLACE_OF_BIRTH_KABKOTA_DESC": _providerInfoNasabah.birthPlaceSelected != null ? _providerInfoNasabah.birthPlaceSelected.KABKOT_NAME : "",
          "P_RELIGION": _providerInfoNasabah.religionSelected != null ? _providerInfoNasabah.religionSelected.id : "",
          "P_RELIGION_DESC": _providerInfoNasabah.religionSelected != null ? _providerInfoNasabah.religionSelected.text : ""
        }
      ],
      "custFamily": _custFamily,
      "idx": idx
    });
    // await http.post(
    //     "http://192.168.57.122/orca_api/public/login",
    //     body: _body,
    //     headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    // );
    var _timeStart = DateTime.now();
    Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, "Start Save Partial Customer Personal ${_preferences.getString("order_no")}","Start Save Partial Customer Personal ${_preferences.getString("last_known_state")} ${_preferences.getString("order_no")}");
    try{
      final _response = await http.post(
          "${BaseUrl.urlGeneral}$_rincianIndividuPartial",
          // "http://192.168.57.84/orca_api/public/login",
          body: _body,
          headers: {'Content-type': 'application/json', "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));

      var _data = jsonDecode(_response.body);
      Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, _response.body, "Save partial customer personal ${_preferences.getString("last_known_state")}");
      debugPrint("save customer $_data");

      if(_response.statusCode == 200){
        if(_data['status'] != "1"){
          throw "${_data['message']}";
        }
        else{
          return _data['message'];
        }
      }
      else{
        throw "Save partial customer info Error ${_response.statusCode}";
      }
    }
    on TimeoutException catch(_){
      throw "Save partial customer info timeout connection";
    }
    catch(e){
      Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, DateTime.now(), DateTime.now(), _body, e.toString(), "Save partial customer personal ${_preferences.getString("last_known_state")}");
      throw "Save partial customer info ${e.toString()}";
    }
  }

  Future<String> submitPhoto(BuildContext context, String opsi) async{
    //SC1 Opsi 1 = foto tempat tinggal, foto tempat usaha, foto group unit.
    //SC2 Opsi 2 = foto survey
    //SC3 Opsi 3 = foto dokumen (Rincian Foto)
    //SC4 Opsi 4 = foto dokumen (Inf Dokumen)
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
    var _providerInfoDocument = Provider.of<InfoDocumentChangeNotifier>(context, listen: false);
    var _providerSurveyPhoto = Provider.of<ListSurveyPhotoChangeNotifier>(context, listen: false);
    List _bodyData = [];
    if(opsi == "1"){
      // tempat tinggal
      for(int i=0; i<_providerFoto.listFotoTempatTinggal.length; i++){
        _bodyData.add({
          "P_APPL_NO": "string",
          "P_CREATED_BY": "${_preferences.getString("username")}",
          "P_DOC_ID": "NEW",
          "P_DOC_NAME": "${_providerFoto.listFotoTempatTinggal[i].path.split("/").last}",
          "P_DOC_PATH": _providerFoto.listFotoTempatTinggal[i].fileHeaderID != "" ? "${_providerFoto.listFotoTempatTinggal[i].fileHeaderID}" : "${_providerFoto.listFotoTempatTinggal[i].path}",
          "P_DOC_TYPE_DESC": "",
          "P_DOC_TYPE_ID": "SC2",
          "P_FLAG_DISPLAY": "1",
          "P_FLAG_MANDATORY": "1",
          "P_FLAG_UNIT": "1",
          "P_FLAG_MANDATORY_CA": "",
          "P_LATITUDE": "${_providerFoto.listFotoTempatTinggal[i].latitude}",
          "P_LONGITUDE": "${_providerFoto.listFotoTempatTinggal[i].longitude}",
          "P_OBJECT": "",
          "P_OBJECT_DESC": "",
          "P_OBJECT_GROUP": "",
          "P_OBJECT_GROUP_DESC": "",
          "P_RECEIVED_DATE": "",
          "P_FLAG": "0",
          "P_OPSI": opsi,
          "P_ORDER_NO": "${_preferences.getString("order_no")}",
        });
      }
      // usaha
      for(int i=0; i<_providerFoto.listFotoTempatUsaha.length; i++){
        _bodyData.add({
          "P_APPL_NO": "string",
          "P_CREATED_BY": "${_preferences.getString("username")}",
          "P_DOC_ID": "NEW",
          "P_DOC_NAME": "${_providerFoto.listFotoTempatUsaha[i].path.split("/").last}",
          "P_DOC_PATH": _providerFoto.listFotoTempatUsaha[i].fileHeaderID != "" ? "${_providerFoto.listFotoTempatUsaha[i].fileHeaderID}" : "${_providerFoto.listFotoTempatUsaha[i].path}",
          "P_DOC_TYPE_DESC": "",
          "P_DOC_TYPE_ID": "SC3",
          "P_FLAG_DISPLAY": "1",
          "P_FLAG_MANDATORY": "1",
          "P_FLAG_UNIT": "1",
          "P_FLAG_MANDATORY_CA": "",
          "P_LATITUDE": "${_providerFoto.listFotoTempatUsaha[i].latitude}",
          "P_LONGITUDE": "${_providerFoto.listFotoTempatUsaha[i].longitude}",
          "P_OBJECT": "",
          "P_OBJECT_DESC": "",
          "P_OBJECT_GROUP": "",
          "P_OBJECT_GROUP_DESC": "",
          "P_RECEIVED_DATE": "",
          "P_FLAG": "0",
          "P_OPSI": opsi,
          "P_ORDER_NO": "${_preferences.getString("order_no")}",
        });
      }
      // grup unit
      for(int i=0; i<_providerFoto.listGroupUnitObject.length; i++){
        for(int j=0; j<_providerFoto.listGroupUnitObject[i].listImageGroupObjectUnit.length; j++){
          _bodyData.add({
            "P_APPL_NO": "string",
            "P_CREATED_BY": "${_preferences.getString("username")}",
            "P_DOC_ID": "NEW",
            "P_DOC_NAME": "${_providerFoto.listGroupUnitObject[i].listImageGroupObjectUnit[j].path.split("/").last}",
            "P_DOC_PATH": _providerFoto.listGroupUnitObject[i].listImageGroupObjectUnit[j].fileHeaderID != "" ? "${_providerFoto.listGroupUnitObject[i].listImageGroupObjectUnit[j].fileHeaderID}" : "${_providerFoto.listGroupUnitObject[i].listImageGroupObjectUnit[j].path}",
            "P_DOC_TYPE_DESC": "",
            "P_DOC_TYPE_ID": "SC1",
            "P_FLAG_DISPLAY": "1",
            "P_FLAG_MANDATORY": "1",
            "P_FLAG_UNIT": "1",
            "P_FLAG_MANDATORY_CA": "",
            "P_LATITUDE": "${_providerFoto.listGroupUnitObject[i].listImageGroupObjectUnit[j].latitude}",
            "P_LONGITUDE": "${_providerFoto.listGroupUnitObject[i].listImageGroupObjectUnit[j].longitude}",
            "P_OBJECT": "${_providerFoto.listGroupUnitObject[i].objectUnit.id}",
            "P_OBJECT_DESC": "${_providerFoto.listGroupUnitObject[i].objectUnit.objectUnit}",
            "P_OBJECT_GROUP": "${_providerFoto.listGroupUnitObject[i].groupObjectUnit.id}",
            "P_OBJECT_GROUP_DESC": "${_providerFoto.listGroupUnitObject[i].groupObjectUnit.groupObject}",
            "P_RECEIVED_DATE": "",
            "P_FLAG": "0",
            "P_OPSI": opsi,
            "P_ORDER_NO": "${_preferences.getString("order_no")}",
          });
        }
      }
    }
    else if(opsi == "2"){
      for(int i=0; i<_providerSurveyPhoto.listSurveyPhoto.length; i++){
        for(int j=0; j<_providerSurveyPhoto.listSurveyPhoto[i].listImageModel.length; j++){
          _bodyData.add({
            "P_APPL_NO": "string",
            "P_CREATED_BY": "${_preferences.getString("username")}",
            "P_DOC_ID": "NEW",
            "P_DOC_NAME": "${_providerSurveyPhoto.listSurveyPhoto[i].listImageModel[j].path.split("/").last}",
            "P_DOC_PATH": _providerSurveyPhoto.listSurveyPhoto[i].listImageModel[j].fileHeaderID != "" ? "${_providerSurveyPhoto.listSurveyPhoto[i].listImageModel[j].fileHeaderID}" : "${_providerSurveyPhoto.listSurveyPhoto[i].listImageModel[j].path}",
            "P_DOC_TYPE_DESC": _providerSurveyPhoto.listSurveyPhoto[i].photoTypeModel.name,
            "P_DOC_TYPE_ID": _providerSurveyPhoto.listSurveyPhoto[i].photoTypeModel.id,
            "P_FLAG_MANDATORY": "1",
            "P_FLAG_DISPLAY": "1",
            "P_FLAG_UNIT": "1",
            "P_FLAG_MANDATORY_CA": "",
            "P_LATITUDE": "${_providerSurveyPhoto.listSurveyPhoto[i].listImageModel[j].latitude}",
            "P_LONGITUDE": "${_providerSurveyPhoto.listSurveyPhoto[i].listImageModel[j].longitude}",
            "P_OBJECT": "",
            "P_OBJECT_DESC": "",
            "P_OBJECT_GROUP": "",
            "P_OBJECT_GROUP_DESC": "",
            "P_RECEIVED_DATE": "",
            "P_FLAG": "1",
            "P_OPSI": opsi,
            "P_ORDER_NO": "${_preferences.getString("order_no")}",
          });
        }
      }
    }
    else if(opsi == "3"){
      for(int i=0; i<_providerFoto.listDocument.length; i++){
        _bodyData.add({
          "P_APPL_NO": "",
          "P_CREATED_BY": "${_preferences.getString("username")}",
          "P_DOC_ID": "${_providerFoto.listDocument[i].orderSupportingDocumentID}",
          "P_DOC_NAME": "${_providerFoto.listDocument[i].path.split("/").last}",
          "P_DOC_PATH": _providerFoto.listDocument[i].fileHeaderID != "" ? "${_providerFoto.listDocument[i].fileHeaderID}" : "${_providerFoto.listDocument[i].path}",
          "P_DOC_TYPE_DESC": "${_providerFoto.listDocument[i].jenisDocument.docTypeName}",
          "P_DOC_TYPE_ID": "${_providerFoto.listDocument[i].jenisDocument.docTypeId}",
          "P_FLAG_MANDATORY": "${_providerFoto.listDocument[i].jenisDocument.mandatory}",
          "P_FLAG_DISPLAY": "${_providerFoto.listDocument[i].jenisDocument.display}",
          "P_FLAG_UNIT": "${_providerFoto.listDocument[i].jenisDocument.flag_unit}",
          "P_FLAG_MANDATORY_CA": "",
          "P_LATITUDE": "${_providerFoto.listDocument[i].latitude}",
          "P_LONGITUDE": "${_providerFoto.listDocument[i].longitude}",
          "P_OBJECT": "",
          "P_OBJECT_DESC": "",
          "P_OBJECT_GROUP": "",
          "P_OBJECT_GROUP_DESC": "",
          "P_RECEIVED_DATE": dateFormat.format(DateTime.parse(_providerFoto.listDocument[i].dateTime.toString())),
          "P_FLAG": "0",
          "P_OPSI": opsi,
          "P_ORDER_NO": "${_preferences.getString("order_no")}",
        });
      }
    }
    else if(opsi == "4"){
      for(int i=0; i<_providerInfoDocument.listInfoDocument.length; i++){
        _bodyData.add({
          "P_APPL_NO": "",
          "P_CREATED_BY": "${_preferences.getString("username")}",
          "P_DOC_ID": "${_providerInfoDocument.listInfoDocument[i].orderSupportingDocumentID}",
          "P_DOC_NAME": "${_providerInfoDocument.listInfoDocument[i].path.split("/").last}",
          "P_DOC_PATH": _providerInfoDocument.listInfoDocument[i].fileHeaderID != "" ? "${_providerInfoDocument.listInfoDocument[i].fileHeaderID}" : "${_providerInfoDocument.listInfoDocument[i].path}",
          "P_DOC_TYPE_DESC": "${_providerInfoDocument.listInfoDocument[i].documentType.docTypeName}",
          "P_DOC_TYPE_ID": "${_providerInfoDocument.listInfoDocument[i].documentType.docTypeId}",
          "P_FLAG_MANDATORY": "${_providerInfoDocument.listInfoDocument[i].documentType.mandatory}",
          "P_FLAG_DISPLAY": "${_providerInfoDocument.listInfoDocument[i].documentType.display}",
          "P_FLAG_UNIT": "${_providerInfoDocument.listInfoDocument[i].documentType.flag_unit}",
          "P_FLAG_MANDATORY_CA": "",
          "P_LATITUDE": "${_providerInfoDocument.listInfoDocument[i].latitude}",
          "P_LONGITUDE": "${_providerInfoDocument.listInfoDocument[i].longitude}",
          "P_OBJECT": "",
          "P_OBJECT_DESC": "",
          "P_OBJECT_GROUP": "",
          "P_OBJECT_GROUP_DESC": "",
          "P_RECEIVED_DATE": dateFormat.format(DateTime.parse(_providerInfoDocument.listInfoDocument[i].date)),
          "P_FLAG": "1",
          "P_OPSI": opsi,
          "P_ORDER_NO": "${_preferences.getString("order_no")}",
        });
      }
    }

    var _body = jsonEncode(_bodyData);
    print("cek body foto partial $_body");

    String _fotoPartial = await _storage.read(key: "FotoPartial");
    // if(_preferences.getString("last_known_state") == "IDE"){
    if(_bodyData.isNotEmpty){
      try{
        var _timeStart = DateTime.now();
        final _response = await http.post(
          // "http://192.168.57.122/orca_api/public/login",
            "${BaseUrl.urlGeneral}$_fotoPartial",
            body: _body,
            headers: {'Content-type': 'application/json', "Authorization":"bearer $token"}
        ).timeout(Duration(seconds: 30));

        var _data = jsonDecode(_response.body);
        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, _response.body, "Save partial foto $opsi");
        debugPrint("save foto $opsi $_data");

        if(_response.statusCode == 200){
          if(_data['status'] != "1"){
            throw "${_data['message']}";
          }
          else{
            return _data['message'];
          }
        }
        else{
          throw "Save partial foto $opsi Error ${_response.statusCode}";
        }
      }
      on TimeoutException catch(_){
        throw "Save partial foto $opsi timeout connection";
      }
      catch(e){
        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, DateTime.now(), DateTime.now(), _body, e.toString(), "Save partial foto $opsi");
        throw "Save partial foto opsi $opsi ${e.toString()}";
      }
    }
    // }
  }

  // save partial untuk customer COM
  Future<void> submitCustomerCompany(BuildContext context,int idx) async{
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _timeStart = DateTime.now();
    var _providerInfoNasabahCompany = Provider.of<FormMCompanyRincianChangeNotifier>(context,listen: false);
    var _providerAddressCompany = Provider.of<FormMCompanyAlamatChangeNotifier>(context,listen: false);
    var _custAddress = [];
    for(int i=0; i < _providerAddressCompany.listAlamatKorespondensi.length; i++){
      _custAddress.add( {
        "P_ADDRESS": _providerAddressCompany.listAlamatKorespondensi[i].address,
        "P_ADDRESS_ID": _preferences.getString("last_known_state") != "IDE" ? _providerAddressCompany.listAlamatKorespondensi[i].addressID : "NEW",
        "P_ADDR_DESC": _providerAddressCompany.listAlamatKorespondensi[i].jenisAlamatModel != null ? _providerAddressCompany.listAlamatKorespondensi[i].jenisAlamatModel.DESKRIPSI : "",
        "P_ADDR_TYPE": _providerAddressCompany.listAlamatKorespondensi[i].jenisAlamatModel != null ? _providerAddressCompany.listAlamatKorespondensi[i].jenisAlamatModel.KODE : "",
        "P_CREATED_BY": "${_preferences.getString("username")}",
        "P_FAX": _providerAddressCompany.listAlamatKorespondensi[i].fax,
        "P_FAX_AREA": _providerAddressCompany.listAlamatKorespondensi[i].faxArea,
        "P_HANDPHONE_NO": "",
        "P_KABKOT": _providerAddressCompany.listAlamatKorespondensi[i].kelurahanModel != null ? _providerAddressCompany.listAlamatKorespondensi[i].kelurahanModel.KABKOT_ID : "",
        "P_KABKOT_DESC": _providerAddressCompany.listAlamatKorespondensi[i].kelurahanModel != null? _providerAddressCompany.listAlamatKorespondensi[i].kelurahanModel.KABKOT_NAME : "",
        "P_KECAMATAN": _providerAddressCompany.listAlamatKorespondensi[i].kelurahanModel != null? _providerAddressCompany.listAlamatKorespondensi[i].kelurahanModel.KEC_ID : "",
        "P_KECAMATAN_DESC": _providerAddressCompany.listAlamatKorespondensi[i].kelurahanModel != null? _providerAddressCompany.listAlamatKorespondensi[i].kelurahanModel.KEC_NAME : "",
        "P_KELURAHAN": _providerAddressCompany.listAlamatKorespondensi[i].kelurahanModel != null? _providerAddressCompany.listAlamatKorespondensi[i].kelurahanModel.KEL_ID : "",
        "P_KELURAHAN_DESC": _providerAddressCompany.listAlamatKorespondensi[i].kelurahanModel != null? _providerAddressCompany.listAlamatKorespondensi[i].kelurahanModel.KEL_NAME : "",
        "P_KORESPONDEN": _providerAddressCompany.listAlamatKorespondensi[i].isCorrespondence ? "1" : "0" ,
        "P_MATRIX_ADDR": "10",
        "P_ORDER_NO": "${_preferences.getString("order_no")}",
        "P_PHONE1": _providerAddressCompany.listAlamatKorespondensi[i].phone1,
        "P_PHONE1_AREA": _providerAddressCompany.listAlamatKorespondensi[i].phoneArea1,
        "P_PHONE2": _providerAddressCompany.listAlamatKorespondensi[i].phone2,
        "P_PHONE2_AREA": _providerAddressCompany.listAlamatKorespondensi[i].phoneArea2,
        "P_PROVINSI": _providerAddressCompany.listAlamatKorespondensi[i].kelurahanModel != null ? _providerAddressCompany.listAlamatKorespondensi[i].kelurahanModel.PROV_ID : "",
        "P_PROVINSI_DESC": _providerAddressCompany.listAlamatKorespondensi[i].kelurahanModel != null ? _providerAddressCompany.listAlamatKorespondensi[i].kelurahanModel.PROV_NAME : "",
        "P_RT": _providerAddressCompany.listAlamatKorespondensi[i].rt,
        "P_RT_DESC": "",
        "P_RW": _providerAddressCompany.listAlamatKorespondensi[i].rw,
        "P_RW_DESC": "",
        "P_ZIP_CODE":_providerAddressCompany.listAlamatKorespondensi[i].kelurahanModel != null ? _providerAddressCompany.listAlamatKorespondensi[i].kelurahanModel.ZIPCODE : "",
        "P_FOREIGN_BK": _preferences.getString("last_known_state") != "IDE"
            ? _providerAddressCompany.listAlamatKorespondensi[i].foreignBusinessID != "NEW"
            ? _providerAddressCompany.listAlamatKorespondensi[i].foreignBusinessID : "NEW" : "NEW",
        "P_ID_NO":""
      });
    }

    var _body = jsonEncode({
      "custAddress": _custAddress,
      "custDetailInfoCompany": [
        {
          "P_ORDER_NO": _preferences.getString("order_no"),
          "P_COMP_TYPE": _providerInfoNasabahCompany.typeInstitutionSelected != null ? _providerInfoNasabahCompany.typeInstitutionSelected.PARA_ID : "",
          "P_COMP_DESC": _providerInfoNasabahCompany.typeInstitutionSelected != null ? _providerInfoNasabahCompany.typeInstitutionSelected.PARA_NAME : "",
          "P_PROFIL": _providerInfoNasabahCompany.profilSelected != null ? _providerInfoNasabahCompany.profilSelected.id : "",
          "P_PROFIL_DESC": _providerInfoNasabahCompany.profilSelected != null ? _providerInfoNasabahCompany.profilSelected.text : "",
          "P_COMP_NAME": _providerInfoNasabahCompany.controllerInstitutionName.text,
          "P_ESTABLISH_DATE": dateFormat2.format(_providerInfoNasabahCompany.initialDateForDateEstablishment),
          "P_SECTOR_ECONOMIC": _providerInfoNasabahCompany.sectorEconomicModelSelected != null ? _providerInfoNasabahCompany.sectorEconomicModelSelected.KODE : "",
          "P_SECTOR_ECONOMIC_DESC": _providerInfoNasabahCompany.sectorEconomicModelSelected != null ? _providerInfoNasabahCompany.sectorEconomicModelSelected.DESKRIPSI : "",
          "P_NATURE_OF_BUSS": _providerInfoNasabahCompany.businessFieldModelSelected != null ? _providerInfoNasabahCompany.businessFieldModelSelected.KODE : "",
          "P_NATURE_OF_BUSS_DESC": _providerInfoNasabahCompany.businessFieldModelSelected != null ? _providerInfoNasabahCompany.businessFieldModelSelected.DESKRIPSI : "",
          "P_LOCATION_STATUS": _providerInfoNasabahCompany.locationStatusSelected != null ? _providerInfoNasabahCompany.locationStatusSelected.id : "",
          "P_LOCATION_STATUS_DESC": _providerInfoNasabahCompany.locationStatusSelected != null ? _providerInfoNasabahCompany.locationStatusSelected.desc : "",
          "P_BUSS_LOCATION": _providerInfoNasabahCompany.businessLocationSelected != null ? _providerInfoNasabahCompany.businessLocationSelected.id : "",
          "P_BUSS_LOCATION_DESC":_providerInfoNasabahCompany.businessLocationSelected != null ? _providerInfoNasabahCompany.businessLocationSelected.desc : "",
          "P_TOTAL_EMP": _providerInfoNasabahCompany.controllerTotalEmployees.text,
          "P_NO_OF_YEAR_BUSS": _providerInfoNasabahCompany.controllerTotalLamaUsahaBerjalan.text, // Total Lama Usaha
          "P_NO_OF_YEAR_WORK": _providerInfoNasabahCompany.controllerLamaUsahaBerjalan.text, // Lama Usaha
          "P_CREATED_BY": _preferences.getString("username"),
          "P_ID_OBLIGOR": _preferences.getString("last_known_state") != "IDE"
              ? _providerInfoNasabahCompany.obligorID != "NEW"
              ? _providerInfoNasabahCompany.obligorID : _preferences.getString("oid").toString() : _preferences.getString("oid").toString(),// _providerInfoNasabahCompany.obligorID != "NEW" && _providerInfoNasabahCompany.obligorID != "null" ? _providerInfoNasabahCompany.obligorID : _preferences.getString("oid").toString(), // _preferences.getString("last_known_state") != "IDE" ? _providerInfoNasabahCompany.obligorID : "NEW", | _providerInfoNasabahCompany.obligorID != "NEW" && _providerInfoNasabahCompany.obligorID != "null" ? _providerInfoNasabahCompany.obligorID : _preferences.getString("oid").toString()
          "P_CUST_TYPE": "COM",
          "P_IS_GUARANTOR": "0",
          "P_FLAG_NPWP": "${_providerInfoNasabahCompany.radioValueIsHaveNPWP}",
          "P_NPWP_NO": _providerInfoNasabahCompany.controllerNPWP.text,
          "P_NPWP_NAME": _providerInfoNasabahCompany.controllerFullNameNPWP.text,
          "P_NPWP_TYPE": _providerInfoNasabahCompany.typeNPWPSelected != null ? _providerInfoNasabahCompany.typeNPWPSelected.id : "",
          "P_NPWP_TYPE_DESC": _providerInfoNasabahCompany.typeNPWPSelected != null ? _providerInfoNasabahCompany.typeNPWPSelected.text : "",
          "P_PKP_FLAG": _providerInfoNasabahCompany.signPKPSelected != null ? _providerInfoNasabahCompany.signPKPSelected.id : "",
          "P_NPWP_ADDRESS": _providerInfoNasabahCompany.controllerNPWPAddress.text,
          "P_CORESPONDENCE": "",
          "P_CUST_COMP_ID": _preferences.getString("last_known_state") != "IDE" ? _providerInfoNasabahCompany.customerCorporateID : "NEW",
          "P_CUST_ID": _preferences.getString("last_known_state") != "IDE" ? _providerInfoNasabahCompany.customerID : "NEW"
        }
      ],
      "idx": idx
    });
    String _companyPartial = await _storage.read(key: "CompanyPartial");
    try{
      final _response = await http.post(
          "${BaseUrl.urlGeneral}$_companyPartial",
          // "${BaseUrl.urlGeneral}ms2/api/save-draft/detail-cust-company",
          // "http://192.168.57.84/orca_api/public/login",
          body: _body,
          headers: {'Content-type': 'application/json', "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));
      var _data = jsonDecode(_response.body);
      Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, _response.body, "Save partial nasabah com ${_preferences.getString("last_known_state")}");
      if(_response.statusCode == 200){
        if(_data['status'] != "1"){
          throw "${_data['message']}";
        }
        else{
          return _data['message'];
        }
      }
      else{
        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, _response.body, "Save partial nasabah com ${_preferences.getString("last_known_state")}");
        throw "Submit partial info nasabah company error ${_response.statusCode}";
      }
    }
    on TimeoutException catch(_){
      throw "Timeout save partial info nasabah company";
    }
    catch(e){
      Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, e.toString(), "Save partial nasabah com ${_preferences.getString("last_known_state")}");
      throw e.toString();
    }
  }

  // save partial untuk pekerjaan PER
  Future<String> submitOccupationPersonal(BuildContext context,int idx) async{
    String _occupationPartial = await _storage.read(key: "OccupationPartial");
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _providerOccupation = Provider.of<FormMOccupationChangeNotif>(context,listen: false);
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);

    List _custAddress = [];
    for(int i=0; i < _providerOccupation.listOccupationAddress.length; i++){
      _custAddress.add({
        "P_ADDRESS": _providerOccupation.listOccupationAddress[i].address,
        "P_ADDRESS_ID": _preferences.getString("last_known_state") != "IDE" ? _providerOccupation.listOccupationAddress[i].addressID : "NEW",
        "P_ADDR_DESC": _providerOccupation.listOccupationAddress[i].jenisAlamatModel.DESKRIPSI,
        "P_ADDR_TYPE": _providerOccupation.listOccupationAddress[i].jenisAlamatModel.KODE,
        "P_CREATED_BY": _preferences.getString("username"),
        "P_FAX": "",
        "P_FAX_AREA": "",
        "P_HANDPHONE_NO": "",
        "P_KABKOT": _providerOccupation.listOccupationAddress[i].kelurahanModel.KABKOT_ID,
        "P_KABKOT_DESC": _providerOccupation.listOccupationAddress[i].kelurahanModel.KABKOT_NAME,
        "P_KECAMATAN": _providerOccupation.listOccupationAddress[i].kelurahanModel.KEC_ID,
        "P_KECAMATAN_DESC": _providerOccupation.listOccupationAddress[i].kelurahanModel.KEC_NAME,
        "P_KELURAHAN": _providerOccupation.listOccupationAddress[i].kelurahanModel.KEL_ID,
        "P_KELURAHAN_DESC": _providerOccupation.listOccupationAddress[i].kelurahanModel.KEL_NAME,
        "P_KORESPONDEN": _providerOccupation.listOccupationAddress[i].isCorrespondence ? "1" :  "0",
        "P_MATRIX_ADDR": "4",
        "P_ORDER_NO": _preferences.getString("order_no"),
        "P_PHONE1": _providerOccupation.listOccupationAddress[i].phone,
        "P_PHONE1_AREA": _providerOccupation.listOccupationAddress[i].areaCode,
        "P_PHONE2": "",
        "P_PHONE2_AREA": "",
        "P_PROVINSI": _providerOccupation.listOccupationAddress[i].kelurahanModel.PROV_ID,
        "P_PROVINSI_DESC": _providerOccupation.listOccupationAddress[i].kelurahanModel.PROV_NAME,
        "P_RT": _providerOccupation.listOccupationAddress[i].rt,
        "P_RT_DESC": "",
        "P_RW": _providerOccupation.listOccupationAddress[i].rw,
        "P_RW_DESC": "",
        "P_ZIP_CODE": _providerOccupation.listOccupationAddress[i].kelurahanModel.ZIPCODE,
        "P_FOREIGN_BK": _preferences.getString("last_known_state") != "IDE"
            ? _providerOccupation.listOccupationAddress[i].foreignBusinessID != "NEW"
            ? _providerOccupation.listOccupationAddress[i].foreignBusinessID : "NEW" : "NEW",
        "P_ID_NO":"",
      });
    }
    String _bussType = "";
    String _bussTypeDesc = "";
    if(_providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07"){
      if(_providerOccupation.typeOfBusinessModelSelected != null){
        _bussType =_providerOccupation.typeOfBusinessModelSelected.id;
      }
    }
    if(_providerFoto.occupationSelected.KODE != "08"){
      if(_providerOccupation.companyTypeModelSelected != null){
        _bussType = _providerOccupation.companyTypeModelSelected.id;
      }
    }

    if(_providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07"){
      if(_providerOccupation.typeOfBusinessModelSelected != null){
        _bussTypeDesc =_providerOccupation.typeOfBusinessModelSelected.text;
      }
    }
    if(_providerFoto.occupationSelected.KODE != "08"){
      if(_providerOccupation.companyTypeModelSelected != null){
        _bussTypeDesc = _providerOccupation.companyTypeModelSelected.desc;
      }
    }
    print("cek statusLocationModelSelected ${_providerOccupation.statusLocationModelSelected != null} ${_providerFoto.occupationSelected.KODE}");

    var _body = jsonEncode({
      "custAddress": _custAddress,
      "custDetailOccupation": [
        {
          "P_BUSS_LOCATION": _providerOccupation.businessLocationModelSelected != null ? _providerOccupation.businessLocationModelSelected.id : "",
          "P_BUSS_LOCATION_DESC": _providerOccupation.businessLocationModelSelected != null ? _providerOccupation.businessLocationModelSelected.desc : "",
          "P_BUSS_NAME": _providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07" ? _providerOccupation.controllerBusinessName.text : _providerFoto.occupationSelected.KODE != "08" ? _providerOccupation.controllerCompanyName.text : "",// _preferences.getString("cust_type") == "PER" ? _providerOccupation.controllerBusinessName.text : null,
          "P_BUSS_TYPE": _bussType,
          // _providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07"
          //     ? _providerOccupation.typeOfBusinessModelSelected != null ? _providerOccupation.typeOfBusinessModelSelected.id
          //     : _providerFoto.occupationSelected.KODE != "08"
          //     ? _providerOccupation.companyTypeModelSelected != null ? _providerOccupation.companyTypeModelSelected.id : "" : "" : "",
          "P_BUSS_DESC": _bussTypeDesc,
          // _providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07"
          //     ? _providerOccupation.typeOfBusinessModelSelected != null ? _providerOccupation.typeOfBusinessModelSelected.text
          //     : _providerFoto.occupationSelected.KODE != "08"
          //     ? _providerOccupation.companyTypeModelSelected != null ? _providerOccupation.companyTypeModelSelected.desc : "" : "" : "",
          "P_CREATED_BY": _preferences.getString("username"),
          "P_CUST_OCCUPATION_ID":_preferences.getString("last_known_state") != "IDE" ? _providerOccupation.customerOccupationID : "NEW",
          "P_EMP_STATUS": _providerOccupation.employeeStatusModelSelected != null ? _providerOccupation.employeeStatusModelSelected.id : "",
          "P_EMP_STATUS_DESC": _providerOccupation.employeeStatusModelSelected != null ? _providerOccupation.employeeStatusModelSelected.desc : "",
          "P_LOCATION_STATUS": _providerOccupation.statusLocationModelSelected != null ? _providerOccupation.statusLocationModelSelected.id : "",
          "P_LOCATION_STATUS_DESC": _providerOccupation.statusLocationModelSelected != null ? _providerOccupation.statusLocationModelSelected.desc : "",
          "P_MODAL_USAHA": "",
          "P_NATURE_OF_BUSS": _providerOccupation.businessFieldModelSelected != null ? _providerOccupation.businessFieldModelSelected.KODE : "",
          "P_NATURE_OF_BUSS_DESC": _providerOccupation.businessFieldModelSelected != null ? _providerOccupation.businessFieldModelSelected.DESKRIPSI : "",
          "P_NO_OF_YEAR_BUSS":_providerOccupation.controllerTotalEstablishedDate.text,//_providerOccupation.controllerEstablishedDate.text,
          "P_NO_OF_YEAR_WORK": _providerOccupation.controllerEstablishedDate.text,//_providerOccupation.controllerTotalEstablishedDate.text,
          "P_OCCUPATION": _providerFoto.occupationSelected != null ? _providerFoto.occupationSelected.KODE : "",
          "P_OCCUPATION_DESC": _providerFoto.occupationSelected != null ? _providerFoto.occupationSelected.DESKRIPSI : "",
          "P_ORDER_NO": _preferences.getString("order_no"),
          "P_PEP_DESC": _providerOccupation.pepModelSelected != null ? _providerOccupation.pepModelSelected.DESKRIPSI : "",
          "P_PEP_TYPE": _providerOccupation.pepModelSelected != null ? _providerOccupation.pepModelSelected.KODE : "",
          "P_PROFESSION_DESC": _providerOccupation.professionTypeModelSelected != null ? _providerOccupation.professionTypeModelSelected.desc : "",
          "P_PROFESSION_TYPE": _providerOccupation.professionTypeModelSelected != null ? _providerOccupation.professionTypeModelSelected.id : "",
          "P_SCHEME_KUR": "",
          "P_SECTOR_ECONOMIC": _providerOccupation.sectorEconomicModelSelected != null ? _providerOccupation.sectorEconomicModelSelected.KODE : "",
          "P_SECTOR_ECONOMIC_DESC": _providerOccupation.sectorEconomicModelSelected != null ? _providerOccupation.sectorEconomicModelSelected.DESKRIPSI.trim() : "",
          "P_SIUP_NO": "",
          "P_TOTAL_EMP": _providerOccupation.controllerEmployeeTotal.text != "" ? _providerOccupation.controllerEmployeeTotal.text : "0"
        }
      ],
      "idx": idx
    });
    print("cek body submit partial $_body");

    // await http.post(
    //     "http://192.168.57.122/orca_api/public/login",
    //     body: _body,
    //     headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    // );

    var _timeStart = DateTime.now();
    Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, "Start Save Partial Occupation ${_preferences.getString("order_no")}","Start Save Partial Occupation ${_preferences.getString("last_known_state")} ${_preferences.getString("order_no")}");
    try{
      final _response = await http.post(
          "${BaseUrl.urlGeneral}$_occupationPartial",
          // "http://192.168.57.84/orca_api/public/login",
          body: _body,
          headers: {'Content-type': 'application/json', "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));
      var _data = jsonDecode(_response.body);
      Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, _response.body, "Save partial occupation ${_preferences.getString("last_known_state")}");
      print("cek save partial occupation $_data");
      if(_response.statusCode == 200){
        if(_data['status'] != "1"){
          throw "${_data['message']}";
        }
        else{
          return _data['message'];
        }
      }
      else{
        throw "Submit partial perkerjaan error ${_response.statusCode}";
      }
    }
    on TimeoutException catch(_){
      throw "Timeout submit partial perkerjaan";
    }
    catch(e){
      throw e.toString();
    }
  }

  // save partial untuk pendapatan PER dan COM
  Future<String> submitIncome(BuildContext context,int idx,bool isCompany) async{
    String _incomePartial = await _storage.read(key: "IncomePartial");
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _providerIncome = Provider.of<FormMIncomeChangeNotifier>(context,listen: false);
    var _providerIncomeCompany = Provider.of<FormMCompanyPendapatanChangeNotifier>(context,listen: false);
    List<MS2CustIncomeModel> _listIncome = _providerIncome.listIncome;
    List<MS2CustIncomeModel> _listIncomeCom = _providerIncomeCompany.listIncome;
    List _custIncome = [];
    List _custIncomeCom = [];
    for(int i=0;i < _listIncome.length; i++){
      _custIncome.add({
        "P_CREATED_BY": _preferences.getString("username"),
        "P_INCOME_DESC": _listIncome[i].income_desc,
        "P_INCOME_TYPE": _listIncome[i].income_type,
        "P_INCOME_VALUE": double.parse(_listIncome[i].income_value.replaceAll(",", "")).toInt(),
        "P_ORDER_NO": _preferences.getString("order_no"),
        "P_CUST_INCOME_ID": _preferences.getString("last_known_state") != "IDE" ? _providerIncome.listIncome[i].customerIncomeID : "NEW",
        "P_TYPE_INCOME_FRML": _listIncome[i].type_income_frml == null ? "" : _listIncome[i].type_income_frml
      });
      // _custIncome.add({
      //     "P_CREATED_BY": _preferences.getString("username"),
      //     "P_CUST_TYPE": _preferences.getString("cust_type"),
      //     "P_INCOME_DESC": listIncome[i].income_desc,
      //     "P_INCOME_TYPE": listIncome[i].income_type,
      //     "P_INCOME_VALUE": listIncome[i].income_value.replaceAll(",", ""),
      //     "P_ORDER_NO": _preferences.getString("order_no"),
      // });
    }

    for(int i=0;i < _listIncomeCom.length; i++){
      _custIncomeCom.add({
        "P_CREATED_BY": _preferences.getString("username"),
        "P_INCOME_DESC": _listIncomeCom[i].income_desc,
        "P_INCOME_TYPE": _listIncomeCom[i].income_type,
        "P_INCOME_VALUE": double.parse(_listIncomeCom[i].income_value.replaceAll(",", "")).toInt(),
        "P_ORDER_NO": _preferences.getString("order_no"),
        "P_CUST_INCOME_ID": _preferences.getString("last_known_state") != "IDE" ? _providerIncomeCompany.listIncome[i].customerIncomeID : "NEW",
        "P_TYPE_INCOME_FRML": _listIncomeCom[i].type_income_frml == null ? "" : _listIncomeCom[i].type_income_frml
      });
    }

    var _body = jsonEncode(
      {
        "custIncome": isCompany ? _custIncomeCom : _custIncome,
        "idx": idx
      }
    );
    print("BODY SAVE PARTIAL PENDAPAATAN $_body");
    // await http.post(
    //     "http://192.168.57.122/orca_api/public/login",
    //     body: _body,
    //     headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    // );
    var _timeStart = DateTime.now();
    Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, "Start Save Partial Pendapatan ${_preferences.getString("order_no")}","Start Save Partial Pendapatan ${_preferences.getString("last_known_state")} ${_preferences.getString("order_no")}");
    try{
      final _response = await http.post(
          "${BaseUrl.urlGeneral}$_incomePartial",
          // "http://192.168.57.84/orca_api/public/login",
          body: _body,
          headers: {'Content-type': 'application/json', "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));

      if(_response.statusCode == 200){
        var _data = jsonDecode(_response.body);
        print("cek data income $_data");
        if(_data['status'] != "1"){
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, jsonEncode(_response.body),"Save Partial Pendapatan ${_preferences.getString("last_known_state")} ${_preferences.getString("order_no")}");
          throw "${_data['message']}";
        }
        else{
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, jsonEncode(_response.body),"Save Partial Pendapatan ${_preferences.getString("last_known_state")} ${_preferences.getString("order_no")}");
          return _data['message'];
        }
      }
      else{
        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, jsonEncode(_response.body),"Save Partial Pendapatan ${_preferences.getString("last_known_state")} ${_preferences.getString("order_no")}");
        throw "Save partial pendapatan error ${_response.statusCode}";
      }
    }
    on TimeoutException catch(_){
      throw "Save partial pendapatan timeout connection";
    }
    catch(e){
      throw "Save partial pendapatan ${e.toString()}";
    }
  }

  // save partial untuk penjamin PER dan COM
  Future<String> submitGuarantor(BuildContext context,int idx) async{
    String _guarantorPartial = await _storage.read(key: "GuarantorPartial");
    SharedPreferences _preferences = await SharedPreferences.getInstance();

    var _providerInfoGuarantor = Provider.of<FormMGuarantorChangeNotifier>(context,listen: false);

    List<GuarantorIndividualModel> _listDataIndividu = _providerInfoGuarantor.listGuarantorIndividual;
    List<GuarantorCompanyModel> _listDataCompany = _providerInfoGuarantor.listGuarantorCompany;

    List<MS2CustAddrModel> _listAddressIndividu = _providerInfoGuarantor.listAddressIndividu;
    List<MS2CustAddrModel> _listAddressCompany = _providerInfoGuarantor.listAddressCompany;

    List _custDetailGuarantorCom = [];
    List _custDetailGuarantorInd = [];
    List _custGuarantorIndAddress = [];
    List _custGuarantorComAddress = [];
    for(int i=0; i < _listDataCompany.length; i++){
      _custDetailGuarantorCom.add(
          {
            "P_COMP_DESC": _listDataCompany[i].typeInstitutionModel.PARA_NAME,
            "P_COMP_NAME": _listDataCompany[i].institutionName,
            "P_COMP_TYPE": _listDataCompany[i].typeInstitutionModel.PARA_ID,
            "P_CREATED_BY": _preferences.getString("username"),
            "P_GRNTR_COMP_ID": _preferences.getString("last_known_state") != "IDE"
                ? _listDataCompany[i].guarantorCorporateID != null
                ? _listDataCompany[i].guarantorCorporateID : "NEW" : "NEW",
            "P_NPWP_NO": _listDataCompany[i].npwp,
            "P_ORDER_NO": _preferences.getString("order_no"),
            "P_PROFIL": _listDataCompany[i].profilModel  != null ? _listDataCompany[i].profilModel.id : "",
            "P_PROFIL_DESC": _listDataCompany[i].profilModel != null ? _listDataCompany[i].profilModel.text : "",
            "P_ESTABLISH_DATE": formatDateDedup(DateTime.parse(_listDataCompany[i].establishDate)),
          }
      );
    }

    for(int i=0; i < _listDataIndividu.length; i++){
      _custDetailGuarantorInd.add(
          {
            "P_ALIAS_NAME": "",
            "P_CREATED_BY": _preferences.getString("username"),
            "P_DATE_OF_BIRTH": _listDataIndividu[i].birthDate != "" ? dateFormat.format(DateTime.parse(_listDataIndividu[i].birthDate)) : "01-01-1900",
            "P_DEGREE": "",
            "P_FULL_NAME": _listDataIndividu[i].fullName,
            "P_FULL_NAME_ID": _listDataIndividu[i].fullNameIdentity,
            "P_GENDER": _listDataIndividu[i].gender,
            "P_GENDER_DESC": _listDataIndividu[i].gender == "01" ? "Laki Laki" : 'Perempuan',
            "P_GRNTR_IND_ID": _preferences.getString("last_known_state") != "IDE"
                ? _listDataIndividu[i].guarantorIndividualID != null
                ? _listDataIndividu[i].guarantorIndividualID : "NEW" : "NEW",
            "P_HANDPHONE_NO": "08${_listDataIndividu[i].cellPhoneNumber}",
            "P_ID_DESC": _listDataIndividu[i].identityModel.name,
            "P_ID_NO": _listDataIndividu[i].identityNumber,
            "P_ID_TYPE": _listDataIndividu[i].identityModel.id,
            "P_ORDER_NO": _preferences.getString("order_no"),
            "P_PLACE_OF_BIRTH": _listDataIndividu[i].birthPlaceIdentity1,
            "P_PLACE_OF_BIRTH_KABKOTA": _listDataIndividu[i].birthPlaceIdentity2.KABKOT_ID,
            "P_PLACE_OF_BIRTH_KABKOTA_DESC": _listDataIndividu[i].birthPlaceIdentity2.KABKOT_NAME,
            "P_RELATION_STATUS": _listDataIndividu[i].relationshipStatusModel.PARA_FAMILY_TYPE_ID,
            "P_RELATION_STATUS_DESC": _listDataIndividu[i].relationshipStatusModel.PARA_FAMILY_TYPE_NAME
          }
      );
    }

    for(int i=0; i < _listDataIndividu.length; i++){
      debugPrint("CEK_KORESPONDEN_GUA_INDI ${_listAddressIndividu[i].koresponden == "1"}");
      for(int j=0; j < _listDataIndividu[i].listAddressGuarantorModel.length; j++){
        _custGuarantorIndAddress.add(
            {
              "P_ADDRESS": _listDataIndividu[i].listAddressGuarantorModel[j].address,
              "P_ADDRESS_ID": _preferences.getString("last_known_state") != "IDE" ? _listDataIndividu[i].listAddressGuarantorModel[j].addressID : "NEW",
              "P_ADDR_DESC": _listDataIndividu[i].listAddressGuarantorModel[j].jenisAlamatModel.DESKRIPSI,
              "P_ADDR_TYPE": _listDataIndividu[i].listAddressGuarantorModel[j].jenisAlamatModel.KODE,
              "P_CREATED_BY": _preferences.getString("username"),
              "P_FAX": "",
              "P_FAX_AREA": "",
              "P_HANDPHONE_NO": "",//_listAddressIndividu[i].handphone_no,
              "P_KABKOT": _listDataIndividu[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_ID,
              "P_KABKOT_DESC": _listDataIndividu[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_NAME,
              "P_KECAMATAN": _listDataIndividu[i].listAddressGuarantorModel[j].kelurahanModel.KEC_ID,
              "P_KECAMATAN_DESC": _listDataIndividu[i].listAddressGuarantorModel[j].kelurahanModel.KEC_NAME,
              "P_KELURAHAN": _listDataIndividu[i].listAddressGuarantorModel[j].kelurahanModel.KEL_ID,
              "P_KELURAHAN_DESC": _listDataIndividu[i].listAddressGuarantorModel[j].kelurahanModel.KEL_NAME,
              "P_KORESPONDEN": _listDataIndividu[i].listAddressGuarantorModel[j].isCorrespondence ? "1" :  "0", //perlu dipastikan bentuk nya apa, string "true"/"false" atau sudah betul "0"/"1"
              "P_MATRIX_ADDR": _preferences.getString("cust_type") == "PER" ? "1" : "7",
              "P_ORDER_NO": _preferences.getString("order_no"),
              "P_PHONE1": _listDataIndividu[i].listAddressGuarantorModel[j].phone != "" && _listDataIndividu[i].listAddressGuarantorModel[j].phone != null ?  _listDataIndividu[i].listAddressGuarantorModel[j].phone : "",
              "P_PHONE1_AREA": _listDataIndividu[i].listAddressGuarantorModel[j].areaCode != "" && _listDataIndividu[i].listAddressGuarantorModel[j].areaCode != null ? _listDataIndividu[i].listAddressGuarantorModel[j].areaCode : "",
              "P_PHONE2": "",
              "P_PHONE2_AREA": "",
              "P_PROVINSI": _listDataIndividu[i].listAddressGuarantorModel[j].kelurahanModel.PROV_ID,
              "P_PROVINSI_DESC": _listDataIndividu[i].listAddressGuarantorModel[j].kelurahanModel.PROV_NAME,
              "P_RT": _listDataIndividu[i].listAddressGuarantorModel[j].rt,
              "P_RT_DESC": "",
              "P_RW": _listDataIndividu[i].listAddressGuarantorModel[j].rw,
              "P_RW_DESC": "",
              "P_ZIP_CODE": _listDataIndividu[i].listAddressGuarantorModel[j].kelurahanModel.ZIPCODE,
              "P_FOREIGN_BK": _preferences.getString("last_known_state") != "IDE"
                  ? _listDataIndividu[i].listAddressGuarantorModel[j].foreignBusinessID != "NEW"
                  ? _listDataIndividu[i].listAddressGuarantorModel[j].foreignBusinessID : "NEW" : "NEW",
              "P_ID_NO": _listDataIndividu[i].identityNumber,
            }
        );
      }
    }

    for(int i=0; i < _listDataCompany.length; i++){
      for(int j=0; j < _listDataCompany[i].listAddressGuarantorModel.length; j++){
        _custGuarantorComAddress.add(
            {
              "P_ADDRESS": _listDataCompany[i].listAddressGuarantorModel[j].address,
              "P_ADDRESS_ID": _preferences.getString("last_known_state") != "IDE" ? _listDataCompany[i].listAddressGuarantorModel[j].addressID : "NEW",
              "P_ADDR_DESC": _listDataCompany[i].listAddressGuarantorModel[j].jenisAlamatModel.DESKRIPSI,
              "P_ADDR_TYPE": _listDataCompany[i].listAddressGuarantorModel[j].jenisAlamatModel.KODE,
              "P_CREATED_BY": _preferences.getString("username"),
              "P_FAX": _listDataCompany[i].listAddressGuarantorModel[j].fax != "" && _listDataCompany[i].listAddressGuarantorModel[j].fax != null ? _listDataCompany[i].listAddressGuarantorModel[j].fax : "",
              "P_FAX_AREA": _listDataCompany[i].listAddressGuarantorModel[j].faxArea != "" && _listDataCompany[i].listAddressGuarantorModel[j].faxArea != null ? _listDataCompany[i].listAddressGuarantorModel[j].faxArea : "",
              "P_HANDPHONE_NO": "",//_listAddressCompany[i].handphone_no,
              "P_KABKOT": _listDataCompany[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_ID,
              "P_KABKOT_DESC": _listDataCompany[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_NAME,
              "P_KECAMATAN": _listDataCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEC_ID,
              "P_KECAMATAN_DESC": _listDataCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEC_NAME,
              "P_KELURAHAN": _listDataCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEC_ID,
              "P_KELURAHAN_DESC": _listDataCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEL_NAME,
              "P_KORESPONDEN": _listDataCompany[i].listAddressGuarantorModel[j].isCorrespondence ? "1" :  "0",// perlu dipastikan bentuk nya apa, string "true"/"false" atau sudah betul "0"/"1"
              "P_MATRIX_ADDR": _preferences.getString("cust_type") == "PER" ? "2" : "8",
              "P_ORDER_NO": _preferences.getString("order_no"),
              "P_PHONE1": _listDataCompany[i].listAddressGuarantorModel[j].phone1 != "" && _listDataCompany[i].listAddressGuarantorModel[j].phone1 != null ? _listDataCompany[i].listAddressGuarantorModel[j].phone1 : "",
              "P_PHONE1_AREA": _listDataCompany[i].listAddressGuarantorModel[j].phoneArea1 != "" && _listDataCompany[i].listAddressGuarantorModel[j].phoneArea1 != null ? _listDataCompany[i].listAddressGuarantorModel[j].phoneArea1 : "",
              "P_PHONE2": _listDataCompany[i].listAddressGuarantorModel[j].phone2 != "" && _listDataCompany[i].listAddressGuarantorModel[j].phone2 != null ? _listDataCompany[i].listAddressGuarantorModel[j].phone2 : "",
              "P_PHONE2_AREA": _listDataCompany[i].listAddressGuarantorModel[j].phoneArea2 != "" && _listDataCompany[i].listAddressGuarantorModel[j].phoneArea2 != null ? _listDataCompany[i].listAddressGuarantorModel[j].phoneArea2 : "",
              "P_PROVINSI": _listDataCompany[i].listAddressGuarantorModel[j].kelurahanModel.PROV_ID,
              "P_PROVINSI_DESC": _listDataCompany[i].listAddressGuarantorModel[j].kelurahanModel.PROV_NAME,
              "P_RT": _listDataCompany[i].listAddressGuarantorModel[j].rt,
              "P_RT_DESC": "",
              "P_RW": _listDataCompany[i].listAddressGuarantorModel[j].rw,
              "P_RW_DESC": "",
              "P_ZIP_CODE": _listDataCompany[i].listAddressGuarantorModel[j].kelurahanModel.ZIPCODE,
              "P_FOREIGN_BK": _preferences.getString("last_known_state") != "IDE"
                  ? _listDataCompany[i].listAddressGuarantorModel[j].foreignBusinessID != "NEW"
                  ? _listDataCompany[i].listAddressGuarantorModel[j].foreignBusinessID : "NEW" : "NEW",
              "P_ID_NO": _listDataCompany[i].npwp,
            }
        );
      }
    }

    var _body = jsonEncode(
        {
          "custDetailGuarantorCom": _custDetailGuarantorCom,
          "custDetailGuarantorInd": _custDetailGuarantorInd,
          "custGuarantorComAddress": _custGuarantorComAddress,
          "custGuarantorIndAddress": _custGuarantorIndAddress,
          "idx": idx
        }
    );

    // await http.post(
    //     "http://192.168.57.122/orca_api/public/login",
    //     body: _body,
    //     headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    // );

    var _timeStart = DateTime.now();
    try{
      final _response = await http.post(
          "${BaseUrl.urlGeneral}$_guarantorPartial",
          // "http://192.168.57.84/orca_api/public/login",
          body: _body,
          headers: {'Content-type': 'application/json', "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));
      var _data = jsonDecode(_response.body);
      if(_response.statusCode == 200){
        if(_data['status'] != "1"){
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, jsonEncode(_response.body),"Save Partial Guarantor ${_preferences.getString("last_known_state")} ${_preferences.getString("order_no")}");
          throw "${_data['message']}";
        }
        else{
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, jsonEncode(_response.body),"Save Partial Guarantor ${_preferences.getString("last_known_state")} ${_preferences.getString("order_no")}");
          return _data['message'];
        }
      }
      else{
        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, jsonEncode(_response.body),"Save Partial Guarantor ${_preferences.getString("last_known_state")} ${_preferences.getString("order_no")}");
        throw "Save partial penjamin error ${_response.statusCode}";
      }
    }
    on TimeoutException catch(_){
      throw "Save partial penjamin timeout connection";
    }
    catch(e){
      Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, e.toString(),"Save Partial Guarantor ${_preferences.getString("last_known_state")} ${_preferences.getString("order_no")}");
      throw "Save partial penjamin ${e.toString()}";
    }
  }

  // save partial untuk application PER dan COM
  Future<String> submitApplication(BuildContext context,int idx) async{
    String _applicationPartial = await _storage.read(key: "ApplicationPartial");
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
    var _providerInfoApplication = Provider.of<InfoAppChangeNotifier>(context,listen: false);
    var _surveyAppointmentDateTime = "";
    if(_providerInfoApplication.controllerSurveyAppointmentDate.text != "" && _providerInfoApplication.controllerSurveyAppointmentTime.text != ""){
      print("cek save survey ${"${dateFormatSurveyApp.format(_providerInfoApplication.initialSurveyDate)}T${_providerInfoApplication.controllerSurveyAppointmentTime.text}"}");
      _surveyAppointmentDateTime = "${dateFormatSurveyApp.format(_providerInfoApplication.initialSurveyDate)}T${_providerInfoApplication.controllerSurveyAppointmentTime.text}";
    }
    var _model = MS2ApplicationModel(
      null,
      _providerInfoApplication.applNo,
      null,
      null,
      null,
      _providerInfoApplication.initialDateOrder.toString(),//this._controllerOrderDate.text != "" ? this._controllerOrderDate.text : "",
      null,
      _surveyAppointmentDateTime,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      _providerInfoApplication.isSignedPK ? 1 : 0,
      null,
      null,
      _preferences.getString("cust_type") == "COM" ? _providerInfoApplication.conceptTypeModelSelected.id : _providerFoto.jenisKonsepSelected.id,
      _preferences.getString("cust_type") == "COM" ? _providerInfoApplication.conceptTypeModelSelected.text : _providerFoto.jenisKonsepSelected.text,
      _providerInfoApplication.controllerTotalObject.text != "" ? int.parse(_providerInfoApplication.controllerTotalObject.text) : null,
      _providerInfoApplication.proportionalTypeOfInsuranceModelSelected != null ? _providerInfoApplication.proportionalTypeOfInsuranceModelSelected.kode : "",
      _providerInfoApplication.proportionalTypeOfInsuranceModelSelected != null ? _providerInfoApplication.proportionalTypeOfInsuranceModelSelected.description : "",
      _providerInfoApplication.numberOfUnitSelected != null ? int.parse(_providerInfoApplication.numberOfUnitSelected) : null,
      null,
      null,
      null,
      null,
      null, // marketing notes
      null,
      null,
      null,
      null,
      null,
      _preferences.getString("username"),
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      _providerInfoApplication.isOrderDateChanges ? "1" : "0",
      _providerInfoApplication.isSurveyAppointmentDateChanges ? "1" : "0",
      _providerInfoApplication.isIsSignedPKChanges ? "1" : "0",
      _providerInfoApplication.isConceptTypeModelChanges ? "1" : "0",
      _providerInfoApplication.isTotalObjectChanges ? "1" : "0",
      _providerInfoApplication.isPropotionalObjectChanges ? "1" : "0",
      _providerInfoApplication.isNumberOfUnitChanges ? "1" : "0",
      null, // marketing notes
    );

    debugPrint("CEK_TGL_SAVE_PARTIAL ${_model.svy_appointment_date}");
    String _dateTimeSurvey = "";
    if(_model.svy_appointment_date != ""){
      _dateTimeSurvey =  dateFormatSurveyAppWithTime.format(DateTime.parse(_model.svy_appointment_date.replaceAll("T", " ")));
      debugPrint("cek data $_dateTimeSurvey");
    }
    var _body = jsonEncode(
        {
          "P_APPL_NO": _preferences.getString("last_known_state") != "IDE" ? _model.applNo : "NEW",
          "P_CREATED_BY": _preferences.getString("username"),
          "P_KONSEP_TYPE": _model.konsep_type,
          "P_KONSEP_TYPE_DESC": _model.konsep_type_desc,
          "P_OBJT_QTY": _model.objt_qty != null ? _model.objt_qty : "",
          "P_ORDER_DATE": dateFormat.format(DateTime.parse(_model.order_date)),
          "P_ORDER_NO": _preferences.getString("order_no"),
          "P_PROP_INSR_TYPE": _model.prop_insr_type,
          "P_PROP_INSR_TYPE_DESC": _model.prop_insr_type_desc,
          "P_SIGN_PK": _model.sign_pk,
          "P_SVY_APPOINTMENT_DATE": _dateTimeSurvey,
          "P_UNIT": _model.unit != null ? _model.unit : 0,
          "idx": idx
        }
    );
    debugPrint("body app = $_body");

    // await http.post(
    //     "http://192.168.57.122/orca_api/public/login",
    //     body: _body,
    //     headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    // );

    var _timeStart = DateTime.now();

    try{
      final _response = await http.post(
          "${BaseUrl.urlGeneral}$_applicationPartial",
          // "http://192.168.57.84/orca_api/public/login",
          body: _body,
          headers: {'Content-type': 'application/json', "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));
      var _data = jsonDecode(_response.body);
      print("result app = $_data");
      Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, "Start Save Partial Application ${_preferences.getString("order_no")}","Start Save Partial Application ${_preferences.getString("last_known_state")} ${_preferences.getString("order_no")}");
      if(_response.statusCode == 200){
        if(_data['status'] != "1"){
          throw "${_data['message']}";
        }
        else{
          return _data['message'];
        }
      }
      else{
        throw "Save partial aplikasi error ${_response.statusCode}";
      }
    }
    on TimeoutException catch(_){
      throw "Save partial aplikasi timeout connection";
    }
    catch(e){
      Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, "Start Save Partial Application ${_preferences.getString("last_known_state")} ${_preferences.getString("order_no")}","Start Save Partial Application ${_preferences.getString("last_known_state")} ${_preferences.getString("order_no")}");
      throw "Save partial aplikasi ${e.toString()}";
    }
  }

  // save partial untuk collateral PER dan COM
  Future<String> submitColla(BuildContext context, int idx) async{
    debugPrint("submit colla");
    var _providerPhoto = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
    var _providerUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
    var _providerSales = Provider.of<InformationSalesmanChangeNotifier>(context,listen: false);
    var _providerCreditIncome = Provider.of<InfoCreditIncomeChangeNotifier>(context,listen: false);
    var _providerInfoColla = Provider.of<InformationCollateralChangeNotifier>(context,listen: false);
    var _providerDetailCustomer = Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false);
    var _providerDetailPIC = Provider.of<FormMCompanyManajemenPICChangeNotifier>(context,listen: false);
    String _collaPartial = await _storage.read(key: "CollaPartial");
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    List _sales = [];
    List _addressProp = [];

    for(int i=0;i < _providerSales.listInfoSales.length; i++){
      debugPrint("cek partial colla listInfoSales ${_providerSales.listInfoSales[i].salesmanTypeModel.id}");
      _sales.add({
        "P_APPL_SALES_ID": _preferences.getString("last_known_state") != "IDE" ? _providerSales.listInfoSales[i].orderProductSalesID : "NEW",
        "P_CREATED_BY": _preferences.getString("username"),
        "P_EMPL_HEAD_ID": _providerSales.listInfoSales[i].employeeHeadModel.NIK,
        "P_EMPL_HEAD_JOB": "",//_providerSales.listInfoSales[i].employeeHeadModel.Jabatan,
        "P_EMPL_HEAD_JOB_DESC": "",//_providerSales.listInfoSales[i].employeeHeadModel.AliasJabatan,
        "P_EMPL_HEAD_NAME":_providerSales.listInfoSales[i].employeeHeadModel.Nama,
        "P_EMPL_ID": _providerSales.listInfoSales[i].employeeModel.NIK,
        "P_EMPL_JOB": _providerSales.listInfoSales[i].positionModel.id,
        "P_EMPL_JOB_DESC": _providerSales.listInfoSales[i].positionModel.positionName,
        "P_EMPL_NAME": _providerSales.listInfoSales[i].employeeModel.Nama,
        "P_ORDER_NO": _preferences.getString("order_no"),
        "P_REF_CONTRACT_NO": _providerUnitObject.referenceNumberModel != null ? _providerUnitObject.referenceNumberModel.noPK : "",
        "P_SALES_TYPE": _providerSales.listInfoSales[i].salesmanTypeModel.id,
        "P_SALES_TYPE_DESC": _providerSales.listInfoSales[i].salesmanTypeModel.salesmanType
      });
    }

    if(_providerInfoColla.listAddressPropColla.isNotEmpty) {
      for(int i=0; i< _providerInfoColla.listAddressPropColla.length; i++){
        _addressProp.add({
          "P_ADDRESS": _providerInfoColla.listAddressPropColla[i].address,
          "P_ADDRESS_ID": _preferences.getString("last_known_state") != "IDE" ? _providerInfoColla.listAddressPropColla[i].addressID : "NEW",
          "P_ADDR_DESC": _providerInfoColla.listAddressPropColla[i].addr_desc,
          "P_ADDR_TYPE": _providerInfoColla.listAddressPropColla[i].addr_type,
          "P_CREATED_BY": "${_preferences.getString("username")}",
          "P_FAX": "",
          "P_FAX_AREA": "",
          "P_HANDPHONE_NO": "",
          "P_KABKOT": _providerInfoColla.listAddressPropColla[i].kabkot,
          "P_KABKOT_DESC": _providerInfoColla.listAddressPropColla[i].kabkot_desc,
          "P_KECAMATAN": _providerInfoColla.listAddressPropColla[i].kecamatan,
          "P_KECAMATAN_DESC": _providerInfoColla.listAddressPropColla[i].kecamatan_desc,
          "P_KELURAHAN": _providerInfoColla.listAddressPropColla[i].kelurahan,
          "P_KELURAHAN_DESC": _providerInfoColla.listAddressPropColla[i].kelurahan_desc,
          "P_KORESPONDEN": _providerInfoColla.listAddressPropColla[i].koresponden == "true" ? "1" : "0",
          "P_MATRIX_ADDR": "6",
          "P_ORDER_NO": "${_preferences.getString("order_no")}",
          "P_PHONE1": _providerInfoColla.listAddressPropColla[i].phone1 != null ? _providerInfoColla.listAddressPropColla[i].phone1 : "",
          "P_PHONE1_AREA": _providerInfoColla.listAddressPropColla[i].phone1_area != null ? _providerInfoColla.listAddressPropColla[i].phone1_area : "",
          "P_PHONE2": "",
          "P_PHONE2_AREA": "",
          "P_PROVINSI": _providerInfoColla.listAddressPropColla[i].provinsi,
          "P_PROVINSI_DESC": _providerInfoColla.listAddressPropColla[i].provinsi_desc,
          "P_RT": _providerInfoColla.listAddressPropColla[i].rt,
          "P_RT_DESC": "",
          "P_RW": _providerInfoColla.listAddressPropColla[i].rw,
          "P_RW_DESC": "",
          "P_ZIP_CODE": _providerInfoColla.listAddressPropColla[i].zip_code
        });
      }
    }

    MS2ApplObjtCollOtoModel _modelOto = _providerInfoColla.modelOto;
    MS2ApplObjtCollPropModel _modelProp = _providerInfoColla.modelProp;
    List _custDetailCollOto = [];
    List _custDetailCollProp = [];
    if(_providerInfoColla.collateralTypeModel != null){
      debugPrint("${_providerInfoColla.collateralTypeModel.id}");
      if(_providerInfoColla.collateralTypeModel.id != "003" && _providerInfoColla.collateralTypeModel.id != "002"){
        _custDetailCollOto.add(
            {
              "P_ADD_EQUIP": _providerInfoColla.controllerPerlengkapanTambahan.text != "" ? _providerInfoColla.controllerPerlengkapanTambahan.text : "",
              "P_BIDDER_NAME": _providerInfoColla.controllerNamaBidder.text != "" ? _providerInfoColla.controllerNamaBidder.text : "",
              "P_BPKB_NO": _providerInfoColla.controllerBPKPNumber.text != "" ? _providerInfoColla.controllerBPKPNumber.text : "",
              "P_BRAND_OBJECT": _providerInfoColla.brandObjectSelected != null ? _providerInfoColla.brandObjectSelected.id : "",
              "P_BRAND_OBJECT_DESC": _providerInfoColla.brandObjectSelected != null ? _providerInfoColla.brandObjectSelected.name : "",
              "P_BUILT_UP": _providerInfoColla.radioValueBuiltUpNonATPM,
              "P_COLA_PURPOSE": _providerInfoColla.collateralUsageOtoSelected != null ? _providerInfoColla.collateralUsageOtoSelected.id : "",
              "P_COLA_PURPOSE_DESC": _providerInfoColla.collateralUsageOtoSelected != null ? _providerInfoColla.collateralUsageOtoSelected.name : "",
              "P_COLLA_NAME": _providerInfoColla.controllerNameOnCollateralAuto.text != "" ? _providerInfoColla.controllerNameOnCollateralAuto.text : "",
              "P_COLL_OTO_ID": _preferences.getString("last_known_state") != "IDE" ? _providerInfoColla.collateralID : "NEW",
              "P_CREATED_BY": _preferences.getString("username"),
              "P_DATE_OF_BIRTH": _providerInfoColla.initialDateForBirthDateAuto != null ? formatDateDedup(DateTime.parse(_providerInfoColla.initialDateForBirthDateAuto.toString())) : "",
              "P_DP_JAMINAN": _providerInfoColla.controllerDPJaminan.text != "" ? double.parse(_providerInfoColla.controllerDPJaminan.text.replaceAll(",", "")) : "",
              "P_ENGINE_NO": _providerInfoColla.controllerMachineNumber.text != "" ? _providerInfoColla.controllerMachineNumber.text : "",
              "P_FLAG_COLL_IS_UNIT": _providerInfoColla.radioValueIsCollateralSameWithUnitOto,
              "P_FLAG_COLL_NAME_IS_APPL": _providerInfoColla.radioValueIsCollaNameSameWithApplicantOto,
              "P_FLAG_MULTI_COLL": "",
              "P_FRAME_NO": _providerInfoColla.controllerFrameNumber.text != "" ? _providerInfoColla.controllerFrameNumber.text : "",
              "P_GRADE_UNIT": _providerInfoColla.controllerGradeUnit.text != "" ? _providerInfoColla.controllerGradeUnit.text : "",
              "P_GROUP_OBJECT": _providerInfoColla.groupObjectSelected != null ? _providerInfoColla.groupObjectSelected.KODE : "",
              "P_GROUP_OBJECT_DESC": _providerInfoColla.groupObjectSelected != null ? _providerInfoColla.groupObjectSelected.DESKRIPSI : "",
              "P_ID_TYPE": _providerInfoColla.identityTypeSelectedAuto != null ? _providerInfoColla.identityTypeSelectedAuto.id : "",
              "P_ID_TYPE_DESC": _providerInfoColla.identityTypeSelectedAuto != null ? _providerInfoColla.identityTypeSelectedAuto.name : "",
              "P_ID_NO": _providerInfoColla.controllerIdentityNumberAuto.text != "" ? _providerInfoColla.controllerIdentityNumberAuto.text : "",
              "P_MAX_PH": _providerInfoColla.controllerPHMaxAutomotive.text != "" ? double.parse(_providerInfoColla.controllerPHMaxAutomotive.text.replaceAll(",", "")) : "",
              "P_MFG_YEAR": _providerInfoColla.yearProductionSelected != null ? int.parse(_providerInfoColla.yearProductionSelected) : "",
              "P_MP_ADIRA": _providerInfoColla.controllerMPAdira.text != "" ? double.parse(_providerInfoColla.controllerMPAdira.text.replaceAll(",", "")) : "",
              "P_MP_ADIRA_UPLD": _providerInfoColla.controllerMPAdiraUpload.text != "" ? int.parse(_providerInfoColla.controllerMPAdiraUpload.text.replaceAll(",", "").split(".")[0]) : "",
              "P_OBJECT": _providerInfoColla.objectSelected != null ? _providerInfoColla.objectSelected.id : "",
              "P_OBJECT_DESC": _providerInfoColla.objectSelected != null ? _providerInfoColla.objectSelected.name : "",
              "P_OBJECT_MODEL": _providerInfoColla.modelObjectSelected != null ? _providerInfoColla.modelObjectSelected.id :"",
              "P_OBJECT_MODEL_DESC": _providerInfoColla.modelObjectSelected != null ? _providerInfoColla.modelObjectSelected.name :"",
              "P_OBJECT_PURPOSE": _providerInfoColla.objectUsageSelected != null ? _providerInfoColla.objectUsageSelected.id : "",
              "P_OBJECT_PURPOSE_DESC": _providerInfoColla.objectUsageSelected != null ? _providerInfoColla.objectUsageSelected.name : "",
              "P_OBJECT_TYPE": _providerInfoColla.objectTypeSelected != null ? _providerInfoColla.objectTypeSelected.id : "",
              "P_OBJECT_TYPE_DESC": _providerInfoColla.objectTypeSelected != null ? _providerInfoColla.objectTypeSelected.name : "",
              "P_ORDER_NO": _preferences.getString("order_no"),
              "P_PLACE_OF_BIRTH": _providerInfoColla.controllerBirthPlaceValidWithIdentityAuto.text != "" ? _providerInfoColla.controllerBirthPlaceValidWithIdentityAuto.text : "",
              "P_PLACE_OF_BIRTH_KABKOTA": _providerInfoColla.birthPlaceAutoSelected != null ? _providerInfoColla.birthPlaceAutoSelected.KABKOT_ID : "",
              "P_PLACE_OF_BIRTH_KABKOTA_DESC": _providerInfoColla.birthPlaceAutoSelected != null ? _providerInfoColla.birthPlaceAutoSelected.KABKOT_NAME : "",
              "P_POLICE_NO": _providerInfoColla.controllerPoliceNumber.text != "" ? _providerInfoColla.controllerPoliceNumber.text : "",
              "P_REGISTRATION_YEAR": _providerInfoColla.yearRegistrationSelected != null ? int.parse(_providerInfoColla.yearRegistrationSelected) : "",
              "P_REKONDISI_FISIK": _providerInfoColla.controllerRekondisiFisik.text != "" ? double.parse(_providerInfoColla.controllerRekondisiFisik.text.replaceAll(",", "")) : "",
              "P_RESULT": _providerInfoColla.radioValueWorthyOrUnworthy,
              "P_SHOWROOM_PRICE": _providerInfoColla.controllerHargaJualShowroom.text != "" ? double.parse(_providerInfoColla.controllerHargaJualShowroom.text.replaceAll(",", "")) : "",
              "P_TAKSASI_PRICE": _providerInfoColla.controllerTaksasiPriceAutomotive.text != "" ? double.parse(_providerInfoColla.controllerTaksasiPriceAutomotive.text.replaceAll(",", "")) : "",
              "P_UTJ_FACILITIES": _providerInfoColla.controllerFasilitasUTJ.text != "" ? _providerInfoColla.controllerFasilitasUTJ.text == "TUNAI" ? "1" : "0" : "",
              "P_YELLOW_PLATE": _providerInfoColla.radioValueYellowPlat,
            }
        );
      }
      else if(_providerInfoColla.collateralTypeModel.id != "003" && _providerInfoColla.collateralTypeModel.id != "001"){
        _custDetailCollProp.add({
          "P_BUILDING_AREA": _providerInfoColla.controllerBuildingArea.text != "" ? double.parse(_providerInfoColla.controllerBuildingArea.text.replaceAll(",", "")) : 0,
          "P_BUILDING_PRICE": _providerInfoColla.controllerHargaBangunan.text != "" ? double.parse(_providerInfoColla.controllerHargaBangunan.text.replaceAll(",", "")) : 0,
          "P_COLA_NAME": _providerInfoColla.controllerNameOnCollateral.text != "" ? _providerInfoColla.controllerNameOnCollateral.text : "",
          "P_COLA_PURPOSE": _providerInfoColla.collateralUsagePropertySelected != null ? _providerInfoColla.collateralUsagePropertySelected.id : "",
          "P_COLA_PURPOSE_DESC": _providerInfoColla.collateralUsagePropertySelected != null ? _providerInfoColla.collateralUsagePropertySelected.name : "",
          "P_COLL_PROP_ID": _preferences.getString("last_known_state") != "IDE" ? _providerInfoColla.collateralID : "NEW",
          "P_CREATED_BY": _preferences.getString("username"),
          "P_DATE_OF_BIRTH":  _providerInfoColla.controllerBirthDateProp.text != "" ? _providerInfoColla.controllerBirthDateProp.text : "",
          "P_DP_GUARANTEE": _providerInfoColla.controllerDPJaminan.text != "" ? double.parse(_providerInfoColla.controllerDPJaminan.text.replaceAll(",", "")) : "",
          "P_EXPIRE_RIGHT": _providerInfoColla.controllerMasaHakBerlaku.text != "" ? _providerInfoColla.controllerMasaHakBerlaku.text : "",
          "P_FLAG_CAR_PAS": _providerInfoColla.radioValueAccessCar,
          "P_FLAG_COLL_NAME_IS_APPL": _providerInfoColla.radioValueIsCollateralSameWithApplicantProperty,
          "P_FLAG_MULTI_COLL": _providerInfoColla.radioValueForAllUnitProperty,
          "P_FLOOR_TYPE": _providerInfoColla.floorTypeSelected != null ? _providerInfoColla.floorTypeSelected.id : "",
          "P_FLOOR_TYPE_DESC": _providerInfoColla.floorTypeSelected != null ? _providerInfoColla.floorTypeSelected.name : "",
          "P_FOUNDATION_TYPE": _providerInfoColla.foundationTypeSelected != null ? _providerInfoColla.foundationTypeSelected.id:"",
          "P_FOUNDATION_TYPE_DESC": _providerInfoColla.foundationTypeSelected != null ? _providerInfoColla.foundationTypeSelected.name:"",
          "P_GRNTR_EVDNCE_OWNR": _providerInfoColla.controllerBuktiKepemilikan.text != "" ? _providerInfoColla.controllerBuktiKepemilikan.text : "",
          "P_GUARANTEE_CHARACTER": _providerInfoColla.controllerSifatJaminan.text != "" ? _providerInfoColla.controllerSifatJaminan.text : "",
          "P_ID_NO": _providerInfoColla.radioValueIsCollateralSameWithApplicantProperty == 1 ? _providerInfoColla.identityModel != null ? _providerInfoColla.identityModel.id : null : _preferences.getString("cust_type") != "COM" ? _providerDetailCustomer.identitasModel.id : _providerDetailPIC.typeIdentitySelected.id,
          "P_ID_TYPE": _providerInfoColla.radioValueIsCollateralSameWithApplicantProperty == 1 ? _providerInfoColla.identityModel != null ? _providerInfoColla.identityModel.name : null : _preferences.getString("cust_type") != "COM" ? _providerDetailCustomer.identitasModel.name : _providerDetailPIC.typeIdentitySelected.text,
          "P_ID_TYPE_DESC": _providerInfoColla.controllerIdentityNumber.text != "" ? _providerInfoColla.controllerIdentityNumber.text : "",
          "P_IMB_DATE": _providerInfoColla.controllerNoIMB.text != "" ? _providerInfoColla.controllerNoIMB.text : "",
          "P_IMB_NO": _providerInfoColla.controllerDateOfIMB.text != "" ? _providerInfoColla.controllerDateOfIMB.text : "",
          "P_JARAK_FASUM_POSITIF": _providerInfoColla.controllerJarakFasumPositif.text != "" ? double.parse(_providerInfoColla.controllerJarakFasumPositif.text.replaceAll(",", "")) : 0,
          "P_JARAK_FASUM_NEGATIF": _providerInfoColla.controllerJarakFasumNegatif.text != "" ? double.parse(_providerInfoColla.controllerJarakFasumNegatif.text.replaceAll(",", "")) : 0,
          "P_LAND_AREA": _providerInfoColla.controllerSurfaceArea.text != "" ? double.parse(_providerInfoColla.controllerSurfaceArea.text.replaceAll(",", "")) : 0,
          "P_LAND_PRICE": _providerInfoColla.controllerHargaTanah.text != "" ? double.parse(_providerInfoColla.controllerHargaTanah.text.replaceAll(",", "")) : 0,
          "P_LICENSE_NAME": _providerInfoColla.controllerNamaPemegangHak.text != "" ? _providerInfoColla.controllerNamaPemegangHak.text : "",
          "P_LTV": _providerInfoColla.controllerLTV.text != "" ? double.parse(_providerInfoColla.controllerLTV.text.replaceAll(",", "")): 0,
          "P_MAX_PH": _providerInfoColla.controllerPHMax.text != "" ? double.parse(_providerInfoColla.controllerPHMax.text.replaceAll(",", "")) : "",
          "P_NJOP_PRICE": _providerInfoColla.controllerHargaNJOP.text != "" ? double.parse(_providerInfoColla.controllerHargaNJOP.text.replaceAll(",", "")) : 0,
          "P_NO_OF_HOUSE": _providerInfoColla.controllerJumlahRumahDalamRadius.text != "" ? int.parse(_providerInfoColla.controllerJumlahRumahDalamRadius.text.replaceAll(",", "")) : 0,
          "P_NO_SRT_UKUR": _providerInfoColla.controllerNoSuratUkur.text != "" ? _providerInfoColla.controllerNoSuratUkur.text : "",
          "P_ORDER_NO": _preferences.getString("order_no"),
          "P_PBLSH_SERTIFIKAT_DATE": _providerInfoColla.controllerCertificateReleaseDate.text != "" ? _providerInfoColla.controllerCertificateReleaseDate.text : "",
          "P_PBLSH_SERTIFIKAT_YEAR": _providerInfoColla.controllerCertificateReleaseYear.text != "" ? int.parse(_providerInfoColla.controllerCertificateReleaseYear.text.replaceAll(",", "")) : 0,
          "P_PLACE_OF_BIRTH": _providerInfoColla.controllerBirthPlaceValidWithIdentity1.text != "" ? _providerInfoColla.controllerBirthPlaceValidWithIdentity1.text : "",
          "P_PLACE_OF_BIRTH_KABKOTA": _providerInfoColla.birthPlaceSelected != null ? _providerInfoColla.birthPlaceSelected.KABKOT_ID : "",
          "P_PLACE_OF_BIRTH_KABKOTA_DESC": _providerInfoColla.birthPlaceSelected != null ? _providerInfoColla.birthPlaceSelected.KABKOT_NAME : "",
          "P_PROPERTY_TYPE": _providerInfoColla.propertyTypeSelected != null ? _providerInfoColla.propertyTypeSelected.id : "",
          "P_PROPERTY_TYPE_DESC": _providerInfoColla.propertyTypeSelected != null ? _providerInfoColla.propertyTypeSelected.name : "",
          "P_ROAD_TYPE": _providerInfoColla.streetTypeSelected != null ? _providerInfoColla.streetTypeSelected.id : "",
          "P_ROAD_TYPE_DESC": _providerInfoColla.streetTypeSelected != null ? _providerInfoColla.streetTypeSelected.name : "",
          "P_ROOF_TYPE": _providerInfoColla.typeOfRoofSelected != null ? _providerInfoColla.typeOfRoofSelected.id : "",
          "P_ROOF_TYPE_DESC": _providerInfoColla.typeOfRoofSelected != null ? _providerInfoColla.typeOfRoofSelected.name : "",
          "P_SERTIFIKAT_NO": _providerInfoColla.controllerCertificateNumber.text != "" ? _providerInfoColla.controllerCertificateNumber.text : "",
          "P_SERTIFIKAT_TYPE": _providerInfoColla.certificateTypeSelected != null ? _providerInfoColla.certificateTypeSelected.id:"",
          "P_SERTIFIKAT_TYPE_DESC": _providerInfoColla.certificateTypeSelected != null ? _providerInfoColla.certificateTypeSelected.name:"",
          "P_SIZE_OF_IMB": _providerInfoColla.controllerLuasBangunanIMB.text != "" ? double.parse(_providerInfoColla.controllerLuasBangunanIMB.text.replaceAll(",", "")) : 0,
          "P_SRTFKT_PBLSH_BY": _providerInfoColla.controllerCertificateOfMeasuringLetter.text != "" ? _providerInfoColla.controllerCertificateOfMeasuringLetter.text : "",
          "P_TAKSASI_PRICE": _providerInfoColla.controllerTaksasiPrice.text != "" ? double.parse(_providerInfoColla.controllerTaksasiPrice.text.replaceAll(",", "")) : "",
          "P_TGL_SRT_UKUR": _providerInfoColla.controllerDateOfMeasuringLetter.text != "" ? _providerInfoColla.controllerDateOfMeasuringLetter.text : "",
          "P_WALL_TYPE": _providerInfoColla.wallTypeSelected != null ? _providerInfoColla.wallTypeSelected.id : "",
          "P_WALL_TYPE_DESC": _providerInfoColla.wallTypeSelected != null ? _providerInfoColla.wallTypeSelected.name : "",
        });
      }
    }

    var _body = jsonEncode({
      "custDetailCollOto": _custDetailCollOto,
      // _modelOto != null
      //     ?
      // [
      //   {
      //     "P_ADD_EQUIP": _modelOto != null ? _modelOto.add_equip : "",
      //     "P_BIDDER_NAME": _modelOto != null ? _modelOto.bidder_name : "",
      //     "P_BPKB_NO": _modelOto != null ? _modelOto.bpkp_no : "",
      //     "P_BRAND_OBJECT": _modelOto != null ? _modelOto.brand_object : "",
      //     "P_BRAND_OBJECT_DESC": _modelOto != null ? _modelOto.brand_object_desc : "",
      //     "P_BUILT_UP": _modelOto != null ? _modelOto.built_up : "0",
      //     "P_COLA_PURPOSE": _modelOto != null ? _modelOto.cola_purpose : "",
      //     "P_COLA_PURPOSE_DESC": _modelOto != null ? _modelOto.cola_purpose_desc : "",
      //     "P_COLLA_NAME": _modelOto != null ? _modelOto.colla_name : "",
      //     "P_COLL_OTO_ID": _preferences.getString("last_known_state") != "IDE" ? _modelOto.collateralID : "NEW",
      //     "P_CREATED_BY": _preferences.getString("username"),
      //     "P_DATE_OF_BIRTH": _modelOto != null ? formatDateDedup(DateTime.parse(_modelOto.date_of_birth)) : "",
      //     "P_DP_JAMINAN": _modelOto != null ? _modelOto.dp_jaminan : "",
      //     "P_ENGINE_NO": _modelOto != null ? _modelOto.engine_no : "",
      //     "P_FLAG_COLL_IS_UNIT": _modelOto != null ? _modelOto.flag_coll_is_unit : "",
      //     "P_FLAG_COLL_NAME_IS_APPL": _modelOto != null ? _modelOto.flag_coll_name_is_appl : "",
      //     "P_FLAG_MULTI_COLL": _modelOto != null ? _modelOto.flag_multi_coll : "",
      //     "P_FRAME_NO": _modelOto != null ? _modelOto.frame_no : "",
      //     "P_GRADE_UNIT": _modelOto != null ? _modelOto.grade_unit : "",
      //     "P_GROUP_OBJECT": _modelOto != null ? _modelOto.group_object : "",
      //     "P_GROUP_OBJECT_DESC": _modelOto != null ? _modelOto.group_object_desc : "",
      //     "P_ID_NO": _modelOto != null ? _modelOto.id_no : "",
      //     "P_ID_TYPE": _modelOto != null ? _modelOto.id_type : "",
      //     "P_ID_TYPE_DESC": _modelOto != null ? _modelOto.id_type_desc : "",
      //     "P_MAX_PH": _modelOto != null ? _modelOto.max_ph : "",
      //     "P_MFG_YEAR": _modelOto != null ? _modelOto.mfg_year : "",
      //     "P_MP_ADIRA": _modelOto != null ? _modelOto.mp_adira : "",
      //     "P_MP_ADIRA_UPLD": _modelOto != null ? _modelOto.mp_adira_upld : "",
      //     "P_OBJECT": _modelOto != null ? _modelOto.object : "",
      //     "P_OBJECT_DESC": _modelOto != null ? _modelOto.object_desc : "",
      //     "P_OBJECT_MODEL": _modelOto != null ? _modelOto.object_model : "",
      //     "P_OBJECT_MODEL_DESC": _modelOto != null ? _modelOto.object_model_desc : "",
      //     "P_OBJECT_PURPOSE": _modelOto != null ? _modelOto.object_purpose : "",
      //     "P_OBJECT_PURPOSE_DESC": _modelOto != null ? _modelOto.object_purpose_desc : "",
      //     "P_OBJECT_TYPE": _modelOto != null ? _modelOto.object_type : "",
      //     "P_OBJECT_TYPE_DESC": _modelOto != null ? _modelOto.object_type_desc : "",
      //     "P_ORDER_NO": _preferences.getString("order_no"),
      //     "P_PLACE_OF_BIRTH": _modelOto != null ? _modelOto.place_of_birth : "",
      //     "P_PLACE_OF_BIRTH_KABKOTA": _modelOto != null ? _modelOto.place_of_birth_kabkota : "",
      //     "P_PLACE_OF_BIRTH_KABKOTA_DESC": _modelOto != null ? _modelOto.place_of_birth_kabkota_desc : "",
      //     "P_POLICE_NO": _modelOto != null ? _modelOto.police_no : "",
      //     "P_REGISTRATION_YEAR": _modelOto != null ? _modelOto.registration_year : "",
      //     "P_REKONDISI_FISIK": _modelOto != null ? _modelOto.rekondisi_fisik : "",
      //     "P_RESULT": _modelOto != null ? _modelOto.result : "",
      //     "P_SHOWROOM_PRICE": _modelOto != null ? _modelOto.showroom_price : "",
      //     "P_TAKSASI_PRICE": _modelOto != null ? _modelOto.taksasi_price : "",
      //     "P_UTJ_FACILITIES": _modelOto != null ? _modelOto.utj_facilities : "",
      //     "P_YELLOW_PLATE": _modelOto != null ? _modelOto.yellow_plate : ""
      //   }
      // ]
      //     :
      // [],
      "custDetailCollProp": _custDetailCollProp,
      // _modelProp != null
      //     ?
      // [
      //   {
      //     "P_BUILDING_AREA": _modelProp != null ? _modelProp.building_area : "",
      //     "P_BUILDING_PRICE": _modelProp != null ? _modelProp.building_price : "",
      //     "P_COLA_NAME": _modelProp != null ? _modelProp.cola_name : "",
      //     "P_COLA_PURPOSE": _modelProp != null ? _modelProp.cola_purpose : "",
      //     "P_COLA_PURPOSE_DESC": _modelProp != null ? _modelProp.cola_purpose_desc : "",
      //     "P_COLL_PROP_ID": _preferences.getString("last_known_state") != "IDE" ? _modelProp.collateralID : "NEW",
      //     "P_CREATED_BY": _preferences.getString("username"),
      //     "P_DATE_OF_BIRTH": _modelProp != null ? _modelProp.date_of_birth : "",
      //     "P_DP_GUARANTEE": _modelProp != null ? _modelProp.dp_guarantee : "",
      //     "P_EXPIRE_RIGHT": _modelProp != null ? _modelProp.expire_right : "",
      //     "P_FLAG_CAR_PAS": _modelProp != null ? _modelProp.flag_car_pas : "",
      //     "P_FLAG_COLL_NAME_IS_APPL": _modelProp != null ? _modelProp.flag_coll_name_is_appl : "",
      //     "P_FLAG_MULTI_COLL": _modelProp != null ? _modelProp.flag_multi_coll : "",
      //     "P_FLOOR_TYPE": _modelProp != null ? _modelProp.floor_type : "",
      //     "P_FLOOR_TYPE_DESC": _modelProp != null ? _modelProp.floor_type_desc : "",
      //     "P_FOUNDATION_TYPE": _modelProp != null ? _modelProp.foundation_type : "",
      //     "P_FOUNDATION_TYPE_DESC": _modelProp != null ? _modelProp.foundation_type_desc : "",
      //     "P_GRNTR_EVDNCE_OWNR": _modelProp != null ? _modelProp.grntr_evdnce_ownr : "",
      //     "P_GUARANTEE_CHARACTER": _modelProp != null ? _modelProp.guarantee_character : "",
      //     "P_ID_NO": _modelProp != null ? _modelProp.id_no : "",
      //     "P_ID_TYPE": _modelProp != null ? _modelProp.id_type : "",
      //     "P_ID_TYPE_DESC": _modelProp != null ? _modelProp.id_type_desc : "",
      //     "P_IMB_DATE": _modelProp != null ? _modelProp.imb_date : "",
      //     "P_IMB_NO": _modelProp != null ? _modelProp.imb_no : "",
      //     "P_JARAK_FASUM_NEGATIF": _modelProp != null ? _modelProp.jarak_fasum_negatif : "",
      //     "P_JARAK_FASUM_POSITIF": _modelProp != null ? _modelProp.jarak_fasum_positif : "",
      //     "P_LAND_AREA": _modelProp != null ? _modelProp.land_area : "",
      //     "P_LAND_PRICE": _modelProp != null ? _modelProp.land_price : "",
      //     "P_LICENSE_NAME": _modelProp != null ? _modelProp.license_name : "",
      //     "P_LTV": _modelProp != null ? _modelProp.ltv : "",
      //     "P_MAX_PH": _modelProp != null ? _modelProp.max_ph : "",
      //     "P_NJOP_PRICE": _modelProp != null ? _modelProp.njop_price : "",
      //     "P_NO_OF_HOUSE": _modelProp != null ? _modelProp.no_of_house : "",
      //     "P_NO_SRT_UKUR": _modelProp != null ? _modelProp.no_srt_ukur : "",
      //     "P_ORDER_NO": _preferences.getString("order_no"),
      //     "P_PBLSH_SERTIFIKAT_DATE": _modelProp != null ? _modelProp.pblsh_sertifikat_date : "",
      //     "P_PBLSH_SERTIFIKAT_YEAR": _modelProp != null ? _modelProp.pblsh_sertifikat_year : "",
      //     "P_PLACE_OF_BIRTH": _modelProp != null ? _modelProp.place_of_birth : "",
      //     "P_PLACE_OF_BIRTH_KABKOTA": _modelProp != null ? _modelProp.place_of_birth_kabkota : "",
      //     "P_PLACE_OF_BIRTH_KABKOTA_DESC": _modelProp != null ? _modelProp.place_of_birth_kabkota_desc : "",
      //     "P_PROPERTY_TYPE": _modelProp != null ? _modelProp.property_type : "",
      //     "P_PROPERTY_TYPE_DESC": _modelProp != null ? _modelProp.property_type_desc : "",
      //     "P_ROAD_TYPE": _modelProp != null ? _modelProp.road_type : "",
      //     "P_ROAD_TYPE_DESC": _modelProp != null ? _modelProp.road_type_desc : "",
      //     "P_ROOF_TYPE": _modelProp != null ? _modelProp.roof_type : "",
      //     "P_ROOF_TYPE_DESC": _modelProp != null ? _modelProp.roof_type_desc : "",
      //     "P_SERTIFIKAT_NO": _modelProp != null ? _modelProp.sertifikat_no : "",
      //     "P_SERTIFIKAT_TYPE": _modelProp != null ? _modelProp.seritifikat_type : "",
      //     "P_SERTIFIKAT_TYPE_DESC": _modelProp != null ? _modelProp.seritifikat_type_desc : "",
      //     "P_SIZE_OF_IMB": _modelProp != null ? _modelProp.size_of_imb : "",
      //     "P_SRTFKT_PBLSH_BY": _modelProp != null ? _modelProp.srtfkt_pblsh_by : "",
      //     "P_TAKSASI_PRICE": _modelProp != null ? _modelProp.taksasi_price : "",
      //     "P_TGL_SRT_UKUR": _modelProp != null ? _modelProp.tgl_srt_ukur : "",
      //     "P_WALL_TYPE": _modelProp != null ? _modelProp.wall_type : "",
      //     "P_WALL_TYPE_DESC": _modelProp != null ? _modelProp.wall_type_desc : "",
      //   }
      // ]
      //     :
      // [],
      "custDetailCollPropAddr":_addressProp,
      "custDetailObject":[
        {
          "P_APPL_MODEL_DTL": _providerUnitObject.controllerDetailModel.text,
          "P_APPL_OBJT_ID": _preferences.getString("last_known_state") != "IDE" ? _providerUnitObject.applObjtID : "NEW",
          "P_BRAND_OBJECT": _providerUnitObject.brandObjectSelected != null ? _providerUnitObject.brandObjectSelected.id : "",
          "P_BRAND_OBJECT_DESC": _providerUnitObject.brandObjectSelected != null ? _providerUnitObject.brandObjectSelected.name : "",
          "P_BUSS_ACTIVITIES": _preferences.getString("cust_type") == "COM" ? _providerUnitObject.businessActivitiesModelSelected != null ? _providerUnitObject.businessActivitiesModelSelected.id.toString() : "" : _providerPhoto.kegiatanUsahaSelected.id,
          "P_BUSS_ACTIVITIES_DESC": _preferences.getString("cust_type") == "COM" ? _providerUnitObject.businessActivitiesModelSelected != null ? _providerUnitObject.businessActivitiesModelSelected.text : "" : _providerPhoto.kegiatanUsahaSelected.text,
          "P_BUSS_ACTIVITIES_TYPE": _preferences.getString("cust_type") == "COM" ? _providerUnitObject.businessActivitiesTypeModelSelected != null ? _providerUnitObject.businessActivitiesTypeModelSelected.id.toString() : "" : _providerPhoto.jenisKegiatanUsahaSelected.id,
          "P_BUSS_ACTIVITIES_TYPE_DESC": _preferences.getString("cust_type") == "COM" ? _providerUnitObject.businessActivitiesTypeModelSelected != null ? _providerUnitObject.businessActivitiesTypeModelSelected.text : "" : _providerPhoto.jenisKegiatanUsahaSelected.text,
          "P_CREATED_BY": _preferences.getString("username"),
          // "P_DIR": _providerCreditIncome.controllerIncomeComparison.text.replaceAll(",", ""),
          // "P_DSC": _providerCreditIncome.controllerDSC.text.replaceAll(",", ""),
          // "P_DSR": _providerCreditIncome.controllerDebtComparison.text.replaceAll(",", ""),
          // "P_IRR": _providerCreditIncome.controllerIRR.text.replaceAll(",", ""),
          "P_FINANCING_TYPE": _preferences.getString("cust_type") == "COM" ? _providerUnitObject.typeOfFinancingModelSelected.financingTypeId : _providerPhoto.typeOfFinancingModelSelected.financingTypeId,
          "P_GROUP_OBJECT": _providerUnitObject.groupObjectSelected != null ? _providerUnitObject.groupObjectSelected.KODE : "",
          "P_GROUP_OBJECT_DESC": _providerUnitObject.groupObjectSelected != null ? _providerUnitObject.groupObjectSelected.DESKRIPSI : "",
          "P_GROUP_SALES": _providerUnitObject.groupSalesSelected != null ? _providerUnitObject.groupSalesSelected.kode : "",
          "P_GROUP_SALES_DESC": _providerUnitObject.groupSalesSelected != null ? _providerUnitObject.groupSalesSelected.deskripsi : "",
          "P_OBJECT": _providerUnitObject.objectSelected != null ? _providerUnitObject.objectSelected.id : "",
          "P_OBJECT_DESC": _providerUnitObject.objectSelected != null ? _providerUnitObject.objectSelected.name : "",
          "P_OBJECT_MODEL": _providerUnitObject.modelObjectSelected != null ? _providerUnitObject.modelObjectSelected.id : "",
          "P_OBJECT_MODEL_DESC": _providerUnitObject.modelObjectSelected != null ? _providerUnitObject.modelObjectSelected.name : "",
          "P_OBJECT_PURPOSE": _providerUnitObject.objectPurposeSelected != null ? _providerUnitObject.objectPurposeSelected.id : "",
          "P_OBJECT_PURPOSE_DESC": _providerUnitObject.objectPurposeSelected != null ? _providerUnitObject.objectPurposeSelected.name : "",
          "P_OBJECT_TYPE": _providerUnitObject.objectTypeSelected != null ? _providerUnitObject.objectTypeSelected.id : "",
          "P_OBJECT_TYPE_DESC": _providerUnitObject.objectTypeSelected != null ? _providerUnitObject.objectTypeSelected.name : "",
          "P_OBJECT_USED": _providerUnitObject.objectUsageModel != null ? _providerUnitObject.objectUsageModel.id : "",
          "P_OBJECT_USED_DESC": _providerUnitObject.objectUsageModel != null ? _providerUnitObject.objectUsageModel.name : "",
          "P_ORDER_NO": _preferences.getString("order_no"),
          "P_ORDER_SOURCE": _providerUnitObject.sourceOrderSelected != null ? _providerUnitObject.sourceOrderSelected.kode : "",
          "P_ORDER_SOURCE_NAME": _providerUnitObject.sourceOrderNameSelected != null ? _providerUnitObject.sourceOrderNameSelected.kode : "",//_providerUnitObject.sourceOrderSelected != null ? _providerUnitObject.sourceOrderSelected.deskripsi : "",
          "P_PRODUCT_TYPE": _providerUnitObject.productTypeSelected != null ? _providerUnitObject.productTypeSelected.id : "",
          "P_PRODUCT_TYPE_DESC": _providerUnitObject.productTypeSelected != null ? _providerUnitObject.productTypeSelected.name : "",
          "P_PROGRAM": _providerUnitObject.programSelected != null ? _providerUnitObject.programSelected.kode : "",
          "P_PROGRAM_DESC": _providerUnitObject.programSelected != null ? _providerUnitObject.programSelected.deskripsi : "",
          "P_REFERENCE_NO": _providerUnitObject.referenceNumberModel != null ? _providerUnitObject.referenceNumberModel.noPK : "",
          "P_REHAB_TYPE": _providerUnitObject.rehabTypeSelected != null ? _providerUnitObject.rehabTypeSelected.id : "",
          "P_REHAB_TYPE_DESC": _providerUnitObject.rehabTypeSelected != null ? _providerUnitObject.rehabTypeSelected.name : "",
          "P_SENTRA_D": _preferences.getString("SentraD"),
          "P_SENTRA_D_DESC": _providerUnitObject.controllerSentraD.text != "" ? _providerUnitObject.controllerSentraD.text : "",
          "P_THIRD_PARTY_ID": _providerUnitObject.thirdPartySelected != null ? _providerUnitObject.thirdPartySelected.kode : "",
          "P_THIRD_PARTY_TYPE": _providerUnitObject.thirdPartyTypeSelected != null ? _providerUnitObject.thirdPartyTypeSelected.kode : "",
          "P_THIRD_PARTY_TYPE_DESC": _providerUnitObject.thirdPartyTypeSelected != null ? _providerUnitObject.thirdPartyTypeSelected.deskripsi : "",
          "P_UNIT_D": _preferences.getString("UnitD"),
          "P_UNIT_D_DESC": _providerUnitObject.controllerUnitD.text != "" ?_providerUnitObject.controllerUnitD.text :"",
          "P_WORK": _providerUnitObject.activitiesModel != null ? _providerUnitObject.activitiesModel.kode : "",
          "P_WORK_DESC": _providerUnitObject.activitiesModel != null ? _providerUnitObject.activitiesModel.deskripsi : "",
          "P_ORDER_SOURCE_DESC": _providerUnitObject.sourceOrderSelected != null ? _providerUnitObject.sourceOrderSelected.deskripsi : "",
          "P_ORDER_SOURCE_NAME_DESC": _providerUnitObject.sourceOrderNameSelected != null ? _providerUnitObject.sourceOrderNameSelected.deskripsi : ""
        }
      ],
      "custDetailSales": _sales,
      "idx": idx
    });

    // await http.post(
    //     "http://192.168.57.122/orca_api/public/login",
    //     body: _body,
    //     headers: {"Content-Type":"application/json"}
    // );
    var _timeStart = DateTime.now();
    try{
      final _response = await http.post(
          "${BaseUrl.urlGeneral}$_collaPartial",
          // "http://192.168.57.84/orca_api/public/login",
          body: _body,
          headers: {'Content-type': 'application/json', "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));
      var _data = jsonDecode(_response.body);
      debugPrint("result colla = $_data");
      if(_response.statusCode == 200){
        if(_data['status'] != "1"){
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, jsonEncode(_response.body), "Save partial colla ${_preferences.getString("last_known_state")}");
          throw "${_data['message']}";
        }
        else{
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, jsonEncode(_response.body), "Save partial colla ${_preferences.getString("last_known_state")}");
          return _data['message'];
        }
      }
      else{
        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, jsonEncode(_response.body), "Save partial colla ${_preferences.getString("last_known_state")}");
        throw "Save partial colla error ${_response.statusCode}";
      }
    }
    on TimeoutException catch(_){
      throw "Save partial colla timeout connection";
    }
    catch(e){
      Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, e.toString(), "Save partial colla ${_preferences.getString("last_known_state")}");
      throw "Save partial colla ${e.toString()}";
    }
  }

  // save partial untuk karoseri PER dan COM
  Future<String> submitKaroseri(BuildContext context,int idx) async{
    String _karoseriPartial = await _storage.read(key: "KaroseriPartial");
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _providerKaroseri = Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false);
    List _karoseri = [];
    for(int i=0;i < _providerKaroseri.listFormKaroseriObject.length; i++){
      _karoseri.add({
        "P_APPL_KAROSERI_ID": _preferences.getString("last_known_state") != "IDE" ? _providerKaroseri.listFormKaroseriObject[i].orderKaroseriID : "NEW",
        "P_COMP_KAROSERI": _providerKaroseri.listFormKaroseriObject[i].company.id,
        "P_COMP_KAROSERI_DESC": _providerKaroseri.listFormKaroseriObject[i].company.desc,
        "P_CREATED_BY": _preferences.getString("username"),
        "P_KAROSERI": _providerKaroseri.listFormKaroseriObject[i].karoseri.kode,
        "P_KAROSERI_DESC": _providerKaroseri.listFormKaroseriObject[i].karoseri.deskripsi,
        "P_KAROSERI_QTY": _providerKaroseri.listFormKaroseriObject[i].jumlahKaroseri,
        "P_ORDER_NO": _preferences.getString("order_no"),
        "P_PKS_KAROSERI": _providerKaroseri.listFormKaroseriObject[i].PKSKaroseri.toString(),
        "P_PRICE": double.parse(_providerKaroseri.listFormKaroseriObject[i].price.replaceAll(",", "")),//_providerKaroseri.listFormKaroseriObject[i].price,
        "P_TOTAL_KAROSERI": double.parse(_providerKaroseri.listFormKaroseriObject[i].totalPrice.replaceAll(",", "")),//_providerKaroseri.listFormKaroseriObject[i].totalPrice
      });
    }
    var _body = jsonEncode(
        {
          "custKaroseri": _karoseri,
          "idx": idx
        }
    );

    // await http.post(
    //     "http://192.168.57.122/orca_api/public/login",
    //     body: _body,
    //     headers: {"Content-Type":"application/json"}
    // );

    debugPrint("body karoseri = $_body");
    var _timeStart = DateTime.now();
    try{
      final _response = await http.post(
          "${BaseUrl.urlGeneral}$_karoseriPartial",
          // "http://192.168.57.84/orca_api/public/login",
          body: _body,
          headers: {'Content-type': 'application/json', "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));
      var _data = jsonDecode(_response.body);
      debugPrint("result karoseri save partial = $_data");
      if(_response.statusCode == 200){
        if(_data['status'] == "1"){
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, jsonEncode(_response.body), "Save partial karoseri ${_preferences.getString("last_known_state")}");
          return _data['message'];
        }
        else{
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, jsonEncode(_response.body), "Save partial karoseri ${_preferences.getString("last_known_state")}");
          throw "${_data['message']}";
        }
      }
      else{
        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, jsonEncode(_response.body), "Save partial karoseri ${_preferences.getString("last_known_state")}");
        throw "Save partial karoseri error ${_response.statusCode}";
      }
    }
    on TimeoutException catch(_){
      throw "Save partial karoseri timeout connection";
    }
    catch(e){
      Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, e.toString(), "Save partial karoseri ${_preferences.getString("last_known_state")}");
      throw "Save partial karoseri ${e.toString()}";
    }
  }

  // save partial untuk strucuture credit PER dan COM
  Future<String> submitStructureCredit(int idx,BuildContext context) async{
    String _structureCreditPartial = await _storage.read(key: "");
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _providerInfoStructureCredit = Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false);
    var _providerInfoStructureCreditTypeInstallment = Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false);
    var _providerInfoCreditIncome = Provider.of<InfoCreditIncomeChangeNotifier>(context,listen: false);

    List _custDetailStructureFee = [];
    for(int i=0; i < _providerInfoStructureCredit.listInfoFeeCreditStructure.length; i++){
      _custDetailStructureFee.add(
          {
            "P_CREATED_BY": _preferences.getString("username"),
            "P_FEE_CASH": _providerInfoStructureCredit.listInfoFeeCreditStructure[i].cashCost == null ||
          _providerInfoStructureCredit.listInfoFeeCreditStructure[i].cashCost == ""
                ? 0
                : double.parse(_providerInfoStructureCredit.listInfoFeeCreditStructure[i].cashCost.replaceAll(",", "")),
            "P_FEE_CREDIT": _providerInfoStructureCredit.listInfoFeeCreditStructure[i].creditCost != null
                ? double.parse(_providerInfoStructureCredit.listInfoFeeCreditStructure[i].creditCost.replaceAll(",", "")) : 0,
            "P_FEE_ID": _preferences.getString("last_known_state") != "IDE" ? _providerInfoStructureCredit.listInfoFeeCreditStructure[i].orderFeeID : "NEW",
            "P_FEE_TYPE": _providerInfoStructureCredit.listInfoFeeCreditStructure[i].feeTypeModel != null
                ? _providerInfoStructureCredit.listInfoFeeCreditStructure[i].feeTypeModel.id : "",
            "P_FEE_TYPE_DESC": _providerInfoStructureCredit.listInfoFeeCreditStructure[i].feeTypeModel != null
                ? _providerInfoStructureCredit.listInfoFeeCreditStructure[i].feeTypeModel.name : "",
            "P_ORDER_NO": _preferences.getString("order_no"),
            "P_TOTAL_FEE": _providerInfoStructureCredit.listInfoFeeCreditStructure[i].totalCost == null
                || _providerInfoStructureCredit.listInfoFeeCreditStructure[i].totalCost == ""
                ? 0
                : double.parse(_providerInfoStructureCredit.listInfoFeeCreditStructure[i].totalCost.replaceAll(",", "")),
          }
      );
    }

    var _body = jsonEncode(
        {
          "custDetailStructureCredit": {
            "P_APPL_TOP": _providerInfoStructureCredit.periodOfTimeSelected,
            "P_BALLOON_TYPE": _providerInfoStructureCreditTypeInstallment.radioValueBalloonPHOrBalloonInstallment == 0 ? "0" : "1",
            "P_CREATED_BY": _preferences.getString("username"),
            "P_DIR": _providerInfoCreditIncome.controllerIncomeComparison.text != ""
                ? double.parse(_providerInfoCreditIncome.controllerIncomeComparison.text.replaceAll(",", "")) : "",
            "P_DP_BRANCH": _providerInfoStructureCredit.controllerBranchDP.text != ""
                ? double.parse(_providerInfoStructureCredit.controllerBranchDP.text.replaceAll(",", "")) : "",
            "P_DP_GROSS": _providerInfoStructureCredit.controllerGrossDP.text == "" || _providerInfoStructureCredit.controllerGrossDP.text == null
                ? "" : double.parse(_providerInfoStructureCredit.controllerGrossDP.text.replaceAll(",", "")),
            "P_DP_NET": _providerInfoStructureCredit.controllerNetDP.text == "" || _providerInfoStructureCredit.controllerNetDP.text == null
                ? "" : double.parse(_providerInfoStructureCredit.controllerNetDP.text.replaceAll(",", "")),
            "P_DSC": _providerInfoCreditIncome.controllerDSC.text.isNotEmpty ? double.parse(_providerInfoCreditIncome.controllerDSC.text.replaceAll(",", "")) : "",
            "P_DSR": _providerInfoCreditIncome.controllerDebtComparison.text.isNotEmpty ? double.parse(_providerInfoCreditIncome.controllerDebtComparison.text.replaceAll(",", "")) : "",
            "P_EFF_RATE": _providerInfoStructureCredit.controllerInterestRateEffective.text == "" || _providerInfoStructureCredit.controllerInterestRateEffective.text == null ? "" : double.parse(_providerInfoStructureCredit.controllerInterestRateEffective.text.replaceAll(",", "")) ,
            "P_FLAT_RATE": _providerInfoStructureCredit.controllerInterestRateFlat.text == "" || _providerInfoStructureCredit.controllerInterestRateFlat.text == null ? "" : double.parse(_providerInfoStructureCredit.controllerInterestRateFlat.text.replaceAll(",", "")),
            "P_INSTALLMENT_PAID": _providerInfoStructureCredit.controllerInstallment.text == "" || _providerInfoStructureCredit.controllerInstallment.text == null ? "" : double.parse(_providerInfoStructureCredit.controllerInstallment.text.replaceAll(",", "")),
            "P_INSTALLMENT_TYPE": _providerInfoStructureCredit.installmentTypeSelected != null ? _providerInfoStructureCredit.installmentTypeSelected.id : "",
            "P_INSTALLMENT_TYPE_DESC": _providerInfoStructureCredit.installmentTypeSelected != null ? _providerInfoStructureCredit.installmentTypeSelected.name : "",
            "P_INTEREST_AMT": 0,
            "P_IRR": _providerInfoCreditIncome.controllerIRR.text.isNotEmpty ? double.parse(_providerInfoCreditIncome.controllerIRR.text.replaceAll(",", "")) : "",
            "P_KAROSERI_PRICE": _providerInfoStructureCredit.controllerKaroseriTotalPrice.text != "" ? double.parse(_providerInfoStructureCredit.controllerKaroseriTotalPrice.text.replaceAll(",", "")) : "",
            "P_LAST_INST_AMT": _providerInfoStructureCreditTypeInstallment.controllerInstallmentValue.text != "" ? double.parse(_providerInfoStructureCreditTypeInstallment.controllerInstallmentValue.text.replaceAll(",", "")) : 0,
            "P_LAST_PERCEN_INST": _providerInfoStructureCreditTypeInstallment.controllerInstallmentPercentage.text != "" ? double.parse(_providerInfoStructureCreditTypeInstallment.controllerInstallmentPercentage.text.replaceAll(",", "")) : 0,
            "P_LTV": _providerInfoStructureCredit.controllerLTV.text == "" ? "" : double.parse(_providerInfoStructureCredit.controllerLTV.text.replaceAll(",", "")),
            "P_OBJECT_PRICE": _providerInfoStructureCredit.controllerObjectPrice.text != "" ? double.parse(_providerInfoStructureCredit.controllerObjectPrice.text.replaceAll(",", "")) : "",
            "P_ORDER_NO": _preferences.getString("order_no"),
            "P_PAYMENT_METHOD": _providerInfoStructureCredit.paymentMethodSelected != null ? _providerInfoStructureCredit.paymentMethodSelected.id : "",
            "P_PAYMENT_METHOD_DESC": _providerInfoStructureCredit.paymentMethodSelected != null ? _providerInfoStructureCredit.paymentMethodSelected.name : "",
            "P_PRINCIPAL_AMT": _providerInfoStructureCredit.controllerTotalLoan.text == "" || _providerInfoStructureCredit.controllerTotalLoan.text == null ? "" : double.parse(_providerInfoStructureCredit.controllerTotalLoan.text.replaceAll(",", "")),
            "P_TOTAL_AMT": _providerInfoStructureCredit.controllerTotalPrice.text == "" || _providerInfoStructureCredit.controllerTotalPrice.text == null ? "" : double.parse(_providerInfoStructureCredit.controllerTotalPrice.text.replaceAll(",", "")) ,
          },
          "custDetailStructureFee": _custDetailStructureFee,
          "idx": idx
        }
    );

    // await http.post(
    //     "http://192.168.57.122/orca_api/public/login",
    //     body: _body,
    //     headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    // );

    var _timeStart = DateTime.now();
    String _struktureCreditPartial = await _storage.read(key: "StructureCreditPartial");
    try{
      final _response = await http.post(
          "${BaseUrl.urlGeneral}$_struktureCreditPartial",
          // "${BaseUrl.urlGeneral}ms2/api/save-draft/detail-structurecredit",
          // "http://192.168.57.84/orca_api/public/login",
          body: _body,
          headers: {'Content-type': 'application/json', "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));
      var _data = jsonDecode(_response.body);
      print("result structure credit save partial = $_data");
      if(_response.statusCode == 200){
        if(_data['status'] == "1"){
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, jsonEncode(_response.body), "Save partial structure credit ${_preferences.getString("last_known_state")}");
          return _data['message'];
        }
        else{
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, jsonEncode(_response.body), "Save partial structure credit ${_preferences.getString("last_known_state")}");
          throw "${_data['message']}";
        }
      }
      else{
        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, jsonEncode(_response.body), "Save partial structure credit ${_preferences.getString("last_known_state")}");
        throw "Save partial structure credit error ${_response.statusCode}";
      }
    }
    on TimeoutException catch(_){
      throw "Save partial structure credit timeout connection";
    }
    catch(e){
      Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, e.toString(), "Save partial structure credit ${_preferences.getString("last_known_state")}");
      throw "Save partial structure credit ${e.toString()}";
    }
  }

  // save partial untuk asuransi PER dan COM
  Future<String> submitInsurance(int idx,BuildContext context) async{
    String _structureCreditPartial = await _storage.read(key: "");
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _providerMajorInsurance = Provider.of<InfoMajorInsuranceChangeNotifier>(context,listen: false);
    var _providerAdditionalInsurance = Provider.of<InfoAdditionalInsuranceChangeNotifier>(context,listen: false);

    List _custInsurance = [];
    for(int i=0; i < _providerMajorInsurance.listFormMajorInsurance.length; i++){
      debugPrint("ASURANSI_UTAMA_SPLIT ${_providerMajorInsurance.listFormMajorInsurance[i].totalPriceSplit}");
      _custInsurance.add({
        "P_CASH_AMT": _providerMajorInsurance.listFormMajorInsurance[i].priceCash != null || _providerMajorInsurance.listFormMajorInsurance[i].priceCash != "" ? double.parse(_providerMajorInsurance.listFormMajorInsurance[i].priceCash.replaceAll(",", "")) : "",
        "P_COMPANY_DESC": _providerMajorInsurance.listFormMajorInsurance[i].company != null ? _providerMajorInsurance.listFormMajorInsurance[i].company.DESKRIPSI : "",
        "P_COMPANY_ID": _providerMajorInsurance.listFormMajorInsurance[i].company != null ? _providerMajorInsurance.listFormMajorInsurance[i].company.KODE : "",
        "P_COVERAGE_AMT": _providerMajorInsurance.listFormMajorInsurance[i].coverageValue != "" ? double.parse(_providerMajorInsurance.listFormMajorInsurance[i].coverageValue.replaceAll(",", "")) : "",
        "P_COVERAGE_TYPE": _providerMajorInsurance.listFormMajorInsurance[i].coverageType != null ? _providerMajorInsurance.listFormMajorInsurance[i].coverageType.KODE : "",
        "P_CREATED_BY": _preferences.getString("username"),
        "P_CREDIT_AMT": _providerMajorInsurance.listFormMajorInsurance[i].priceCredit != "" ? double.parse(_providerMajorInsurance.listFormMajorInsurance[i].priceCredit.replaceAll(",", "")) : "",
        "P_INSURANCE_ID": _providerMajorInsurance.listFormMajorInsurance[i].orderProductInsuranceID,
        "P_INSURANCE_TYPE": _providerMajorInsurance.listFormMajorInsurance[i].insuranceType != null ? _providerMajorInsurance.listFormMajorInsurance[i].insuranceType.KODE : "",
        "P_INSURANCE_TYPE_DESC": _providerMajorInsurance.listFormMajorInsurance[i].insuranceType != null ? _providerMajorInsurance.listFormMajorInsurance[i].insuranceType.DESKRIPSI : "",
        "P_JAMINAN_KE": _providerMajorInsurance.listFormMajorInsurance[i].numberOfColla,
        "P_LOWER_LIMIT_AMT": _providerMajorInsurance.listFormMajorInsurance[i].lowerLimit != "" ? double.parse(_providerMajorInsurance.listFormMajorInsurance[i].lowerLimit.replaceAll(",", "")) : "",
        "P_LOWER_LIMIT_PCT": _providerMajorInsurance.listFormMajorInsurance[i].lowerLimitRate != "" ? double.parse(_providerMajorInsurance.listFormMajorInsurance[i].lowerLimitRate.replaceAll(",", "")) : "",
        "P_ORDER_NO": _preferences.getString("order_no"),
        "P_PERIOD": _providerMajorInsurance.listFormMajorInsurance[i].periodType != "" ? int.parse(_providerMajorInsurance.listFormMajorInsurance[i].periodType) : "",
        "P_PRODUCT": _providerMajorInsurance.listFormMajorInsurance[i].product != null ? _providerMajorInsurance.listFormMajorInsurance[i].product.KODE : "",
        "P_PRODUCT_DESC":_providerMajorInsurance.listFormMajorInsurance[i].product != null ? _providerMajorInsurance.listFormMajorInsurance[i].product.DESKRIPSI : "",
        "P_SUB_TYPE": _providerMajorInsurance.listFormMajorInsurance[i].subType != null ? _providerMajorInsurance.listFormMajorInsurance[i].subType.KODE : "",
        "P_SUB_TYPE_DESC": _providerMajorInsurance.listFormMajorInsurance[i].subType != null ? _providerMajorInsurance.listFormMajorInsurance[i].subType.DESKRIPSI : "",
        "P_TOTAL_AMT": _providerMajorInsurance.listFormMajorInsurance[i].totalPrice != "" ? double.parse(_providerMajorInsurance.listFormMajorInsurance[i].totalPrice.replaceAll(",", "")) : "",
        "P_TOTAL_PCT": _providerMajorInsurance.listFormMajorInsurance[i].totalPriceRate != "" ? double.parse(_providerMajorInsurance.listFormMajorInsurance[i].totalPriceRate.replaceAll(",", "")) : "",
        "P_TOTAL_SPLIT": _providerMajorInsurance.listFormMajorInsurance[i].totalPriceSplit != "" ? double.parse(_providerMajorInsurance.listFormMajorInsurance[i].totalPriceSplit.replaceAll(",", "")) : "",
        "P_TYPE": _providerMajorInsurance.listFormMajorInsurance[i].type != null ?  _providerMajorInsurance.listFormMajorInsurance[i].type : "",
        "P_TYPE_1": _providerMajorInsurance.listFormMajorInsurance[i].coverage1 != null ? _providerMajorInsurance.listFormMajorInsurance[i].coverage1.KODE : "",
        "P_TYPE_1_DESC": _providerMajorInsurance.listFormMajorInsurance[i].coverage1 != null ? _providerMajorInsurance.listFormMajorInsurance[i].coverage1.DESKRIPSI : "",
        "P_TYPE_2": _providerMajorInsurance.listFormMajorInsurance[i].coverage2 != null ? _providerMajorInsurance.listFormMajorInsurance[i].coverage2.KODE : "",
        "P_TYPE_2_DESC": _providerMajorInsurance.listFormMajorInsurance[i].coverage2 != null ? _providerMajorInsurance.listFormMajorInsurance[i].coverage2.DESKRIPSI : "",
        "P_TYPE_DESC": "",
        "P_UPPER_LIMIT_PCT": _providerMajorInsurance.listFormMajorInsurance[i].upperLimitRate == "" || _providerMajorInsurance.listFormMajorInsurance[i].upperLimitRate == null? "" : double.parse(_providerMajorInsurance.listFormMajorInsurance[i].upperLimitRate),
        "P_UPPER_LIMIT_AMT": _providerMajorInsurance.listFormMajorInsurance[i].upperLimit == "" ? 0 : double.parse(_providerMajorInsurance.listFormMajorInsurance[i].upperLimit.replaceAll(",", "")),
      });
    }

    for(int i=0; i < _providerAdditionalInsurance.listFormAdditionalInsurance.length; i++){
      _custInsurance.add({
        "P_CASH_AMT": _providerAdditionalInsurance.listFormAdditionalInsurance[i].priceCash != null
            || _providerAdditionalInsurance.listFormAdditionalInsurance[i].priceCash != "" ? _providerAdditionalInsurance.listFormAdditionalInsurance[i].priceCash : "",
        "P_COMPANY_DESC": _providerAdditionalInsurance.listFormAdditionalInsurance[i].company != null ? _providerAdditionalInsurance.listFormAdditionalInsurance[i].company.DESKRIPSI : "",
        "P_COMPANY_ID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].company != null ? _providerAdditionalInsurance.listFormAdditionalInsurance[i].company.KODE : "",
        "P_COVERAGE_AMT": _providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageValue != "" ? double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageValue.replaceAll(",", "")) : "",
        "P_COVERAGE_TYPE": _providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageType != null ? _providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageType.KODE : "",
        "P_CREATED_BY": _preferences.getString("username"),
        "P_CREDIT_AMT": _providerAdditionalInsurance.listFormAdditionalInsurance[i].priceCredit != "" ? double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].priceCredit.replaceAll(",", "")) : "",
        "P_INSURANCE_ID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].orderProductInsuranceID,
        "P_INSURANCE_TYPE": _providerAdditionalInsurance.listFormAdditionalInsurance[i].insuranceType != null ? _providerAdditionalInsurance.listFormAdditionalInsurance[i].insuranceType.KODE : "",
        "P_INSURANCE_TYPE_DESC": _providerAdditionalInsurance.listFormAdditionalInsurance[i].insuranceType != null ? _providerAdditionalInsurance.listFormAdditionalInsurance[i].insuranceType.DESKRIPSI : "",
        "P_JAMINAN_KE": "",
        "P_LOWER_LIMIT_AMT": _providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimit != "" ? double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimit.replaceAll(",", "")) : "",
        "P_LOWER_LIMIT_PCT": _providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimitRate != "" ? double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimitRate.replaceAll(",", "")) : "",
        "P_ORDER_NO": _preferences.getString("order_no"),
        "P_PERIOD": _providerAdditionalInsurance.listFormAdditionalInsurance[i].periodType != "" ? int.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].periodType) : "",
        "P_PRODUCT": _providerAdditionalInsurance.listFormAdditionalInsurance[i].product != null ? _providerAdditionalInsurance.listFormAdditionalInsurance[i].product.KODE : "",
        "P_PRODUCT_DESC":_providerAdditionalInsurance.listFormAdditionalInsurance[i].product != null ? _providerAdditionalInsurance.listFormAdditionalInsurance[i].product.DESKRIPSI : "",
        "P_SUB_TYPE": "",
        "P_SUB_TYPE_DESC": "",
        "P_TOTAL_AMT": _providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPrice != "" ? double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPrice.replaceAll(",", "")) : "",
        "P_TOTAL_PCT": _providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPriceRate != "" ? double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPriceRate.replaceAll(",", "")) : "",
        "P_TOTAL_SPLIT": 0,
        "P_TYPE": "",
        "P_TYPE_1": "",
        "P_TYPE_1_DESC": "",
        "P_TYPE_2": "",
        "P_TYPE_2_DESC": "",
        "P_TYPE_DESC": "",
        "P_UPPER_LIMIT_PCT": _providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimitRate == "" || _providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimitRate == null? "" : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimitRate),
        "P_UPPER_LIMIT_AMT": _providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimit == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimit.replaceAll(",", "")),
      });
    }

    var _body = jsonEncode(
        {
          "custInsurance": _custInsurance,
          "idx": 8
        }
    );

    // await http.post(
    //     "http://192.168.57.122/orca_api/public/login",
    //     body: _body,
    //     headers: {"Content-Type":"application/json"}
    // );

    var _timeStart = DateTime.now();
    String _insurancePartial = await _storage.read(key: "InsurancePartial");
    try{
      final _response = await http.post(
          "${BaseUrl.urlGeneral}$_insurancePartial",
          // "${BaseUrl.urlGeneral}ms2/api/save-draft/detail-insurance",
          // "http://192.168.57.84/orca_api/public/login",
          body: _body,
          headers: {'Content-type': 'application/json', "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));
      var _data = jsonDecode(_response.body);
      print("result insurance save partial = $_data");
      if(_response.statusCode == 200){
        if(_data['status'] == "1"){
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, jsonEncode(_response.body), "Save partial insurance ${_preferences.getString("last_known_state")}");
          return _data['message'];
        }
        else{
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, jsonEncode(_response.body), "Save partial insurance ${_preferences.getString("last_known_state")}");
          throw "${_data['message']}";
        }
      }
      else{
        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, jsonEncode(_response.body), "Save partial insurance ${_preferences.getString("last_known_state")}");
        throw "Save partial insurance error ${_response.statusCode}";
      }
    }
    on TimeoutException catch(_){
      throw "Save partial insurance timeout connection";
    }
    catch(e){
      Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, e.toString(), "Save partial insurance ${_preferences.getString("last_known_state")}");
      throw "Save partial insurance ${e.toString()}";
    }
  }

  // save partial untuk WMP PER dan COM
  Future<String> submitWMP(int idx,BuildContext context) async{
    String _structureCreditPartial = await _storage.read(key: "");
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _providerWMP = Provider.of<InfoWMPChangeNotifier>(context,listen: false);

    List _wmp = [];
    for(int i=0; i < _providerWMP.listWMP.length; i++){
      _wmp.add({
        "P_CREATED_BY": _preferences.getString("username"),
        "P_MAX_REFUND_AMT": _providerWMP.listWMP[i].amount,
        "P_ORDER_NO": _preferences.getString("order_no"),
        "P_WMP_ID": _providerWMP.listWMP[i].orderWmpID,
        "P_WMP_JOB": "",
        "P_WMP_JOB_DESC": "",
        "P_WMP_NO": _providerWMP.listWMP[i].noProposal,
        "P_WMP_SUBSIDY_TYPE_ID": _providerWMP.listWMP[i].typeSubsidi,
        "P_WMP_SUBSIDY_TYPE_ID_DESC": _providerWMP.listWMP[i].deskripsi,
        "P_WMP_TYPE": _providerWMP.listWMP[i].type,
        "P_WMP_TYPE_DESC": _providerWMP.listWMP[i].deskripsiProposal
      });
    }

    var _body = jsonEncode(_wmp);

    // await http.post(
    //     "http://192.168.57.122/orca_api/public/login",
    //     body: _body,
    //     headers: {"Content-Type":"application/json"}
    // );

    var _timeStart = DateTime.now();
    String _wmpPartial = await _storage.read(key: "WMPPartial");
    try{
      final _response = await http.post(
          "${BaseUrl.urlGeneral}$_wmpPartial",
          // "${BaseUrl.urlGeneral}ms2/api/save-draft/detail-wmp",
          // "http://192.168.57.84/orca_api/public/login",
          body: _body,
          headers: {'Content-type': 'application/json', "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));
      var _data = jsonDecode(_response.body);
      print("result wmp save partial = $_data");
      if(_response.statusCode == 200){
        if(_data['status'] == "1"){
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, jsonEncode(_response.body), "Save partial WMP ${_preferences.getString("last_known_state")}");
          return _data['message'];
        }
        else{
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, jsonEncode(_response.body), "Save partial WMP ${_preferences.getString("last_known_state")}");
          throw "${_data['message']}";
        }
      }
      else{
        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, jsonEncode(_response.body), "Save partial WMP ${_preferences.getString("last_known_state")}");
        throw "Save partial wmp error ${_response.statusCode}";
      }
    }
    on TimeoutException catch(_){
      throw "Save partial wmp timeout connection";
    }
    catch(e){
      Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, e.toString(), "Save partial WMP ${_preferences.getString("last_known_state")}");
      throw "Save partial wmp ${e.toString()}";
    }
  }

  // //blm cek
  // Future<String> submitDocument(int idx,List<MS2DocumentModel> model) async{
  //   String _documentPartial = await _storage.read(key: "DocumentPartial");
  //   SharedPreferences _preferences = await SharedPreferences.getInstance();
  //   List _document = [];
  //   for(int i=0;i < model.length; i++){
  //     _document.add({
  //       "P_CREATED_BY": _preferences.getString("username"),
  //       "P_DOCUMENT_TYPE_DESC": model[i].document_type_desc,
  //       "P_DOC_LINK": "string",
  //       "P_DOC_NAME": model[i].file_name,
  //       "P_FLAG_DISPLAY": model[i].display,
  //       "P_FLAG_MANDATORY": model[i].mandatory,
  //       "P_FLAG_MANDATORY_CA": "string",
  //       "P_FLAG_UNIT": model[i].flag_unit,
  //       "P_INPUT_SOURCE": "string",
  //       "P_ORDER_NO": _preferences.getString("order_no"),
  //       "P_PARA_DOCUMENT_TYPE_ID": model[i].document_type_id,
  //       "P_RECEIVED_DATE": model[i].upload_date,
  //       "P_UPLOAD_DATE": dateFormat.format(DateTime.now())
  //     });
  //   }
  //   var _body = jsonEncode(
  //       {
  //         "custDocument": _document,
  //         "idx": idx
  //       }
  //   );
  //   print("body document = $_body");
  //   try{
  //     final _response = await http.post(
  //         "${BaseUrl.urlGeneral}$_documentPartial",
  //         // "http://192.168.57.84/orca_api/public/login",
  //         body: _body,
  //         headers: {'Content-type': 'application/json', "Authorization":"bearer $token"}
  //     ).timeout(Duration(seconds: 30));
  //     var _data = jsonDecode(_response.body);
  //     if(_response.statusCode == 200){
  //       if(_data['status'] == "1"){
  //         return _data['message'];
  //       }
  //       else{
  //         throw "${_data['message']}";
  //       }
  //     }
  //     else{
  //       throw "Save partial document error ${_response.statusCode}";
  //     }
  //   }
  //   on TimeoutException catch(_){
  //     throw "Save partial document timeout connection";
  //   }
  //   catch(e){
  //     throw "Save partial document ${e.toString()}";
  //   }
  // }

  //blm cek

  // save partial untuk takasasi PER dan COM
  Future<String> submitTaksasi(int idx,List<MS2ApplTaksasiModel> model) async{
    String _taksasiPartial = await _storage.read(key: "TaksasiPartial");
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    List _taksasi = [];
    for(int i=0;i < model.length; i++){
      _taksasi.add({
        "P_CREATED_BY": _preferences.getString("username"),
        "P_NILAI_INPUT_REKONDISI": model[i].nilai_input_rekondisi,
        "P_ORDER_NO": _preferences.getString("order_no"),
        "P_TAKSASI_CODE": model[i].taksasi_code,
        "P_TAKSASI_DESC": model[i].taksasi_desc,
        "P_UNIT_TYPE": model[i].unit_type,
        "P_UNIT_TYPE_DESC": model[i].unit_type_desc
      });
    }
    var _body = jsonEncode(
        {
          "custTaksasi": _taksasi,
          "idx": idx
        }
    );
    print("body document = $_body");
    try{
      final _response = await http.post(
          "${BaseUrl.urlGeneral}$_taksasiPartial",
          // "http://192.168.57.84/orca_api/public/login",
          body: _body,
          headers: {'Content-type': 'application/json', "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));
      var _data = jsonDecode(_response.body);
      if(_response.statusCode == 200){
        if(_data['status'] == "1"){
          return _data['message'];
        }
        else{
          throw "${_data['message']}";
        }
      }
      else{
        throw "Save partial taksasi error ${_response.statusCode}";
      }
    }
    on TimeoutException catch(_){
      throw "Save partial taksasi timeout connection";
    }
    catch(e){
      throw "Save partial taksasi ${e.toString()}";
    }
  }

  // save partial untuk subsidy PER dan COM
  Future<String> submitSubsidy(int idx,BuildContext context) async{
    String _subsidiPartial = await _storage.read(key: "SubsidiPartial");
    String _subsidiPartialDetail = await _storage.read(key: "SubsidiDetailPartial");
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _providerSubsidy = Provider.of<InfoCreditSubsidyChangeNotifier>(context,listen: false);
    List _subsidi = [];
    List _orderSubsidyDetails = [];
    for(int i=0;i < _providerSubsidy.listInfoCreditSubsidy.length; i++){
      for(int j=0; j < _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail.length; j++) {
        _orderSubsidyDetails.add(
            {
              "P_CREATED_BY": _preferences.getString("username"),
              "P_INSTALLMENT_AMT": _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentSubsidy != null ? _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentSubsidy.replaceAll(",", "") : "0",
              "P_INSTALLMENT_NO": _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentIndex != null ? _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentIndex.replaceAll(",", "") : "0",
              "P_ORDER_NO": _preferences.getString("order_no"),
              "P_SUBSIDY_DTL_ID": _preferences.getString("last_known_state") != "IDE" ? _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].orderSubsidyDetailID : "NEW",
            }
        );
      }
      _subsidi.add({
        "P_SUBSIDY_ID": _preferences.getString("last_known_state") != "IDE" ? _providerSubsidy.listInfoCreditSubsidy[i].orderSubsidyID : "NEW",
        "P_CREATED_BY": _preferences.getString("username"),
        "P_DEDUCTION_METHOD": _providerSubsidy.listInfoCreditSubsidy[i].cuttingMethod != null
            ? _providerSubsidy.listInfoCreditSubsidy[i].cuttingMethod.id : "02",
        "P_EFF_RATE_BEF": _providerSubsidy.listInfoCreditSubsidy[i].rateBeforeEff != ""
            ? _providerSubsidy.listInfoCreditSubsidy[i].rateBeforeEff.replaceAll(",", ""): "0",
        "P_FLAT_RATE_BEF": _providerSubsidy.listInfoCreditSubsidy[i].rateBeforeFlat != ""
            ? _providerSubsidy.listInfoCreditSubsidy[i].rateBeforeFlat.replaceAll(",", "") : "0",
        "P_GIVER_REFUND": _providerSubsidy.listInfoCreditSubsidy[i].giver,
        "P_INSTALLMENT_AMT":_providerSubsidy.listInfoCreditSubsidy[i].totalInstallment != "" && _providerSubsidy.listInfoCreditSubsidy[i].totalInstallment != null
            ? _providerSubsidy.listInfoCreditSubsidy[i].totalInstallment.replaceAll(",", "") : "0",
        "P_ORDER_NO": _preferences.getString("order_no"),
        "P_REFUND_AMT": _providerSubsidy.listInfoCreditSubsidy[i].claimValue != ""
            ? _providerSubsidy.listInfoCreditSubsidy[i].claimValue.replaceAll(",", "") : "0",
        "P_TYPE_REFUND": _providerSubsidy.listInfoCreditSubsidy[i].type != null ? _providerSubsidy.listInfoCreditSubsidy[i].type.id : "",
      });
    }
    var _body = jsonEncode(
        {
          "custSubsidi": _subsidi,
          "idx": idx
        }
    );

    var _bodyDetail = jsonEncode(
        {
          "custSubsidiDetail":_orderSubsidyDetails,
          "idx": idx
        }
    );
    print("body header = $_body");
    print("body detail = $_bodyDetail");
    try{
      final _response = await http.post(
          "${BaseUrl.urlGeneral}$_subsidiPartial",
          // "http://192.168.57.84/orca_api/public/login",
          body: _body,
          headers: {'Content-type': 'application/json', "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));

      final _responseDetail = await http.post(
          "${BaseUrl.urlGeneral}$_subsidiPartialDetail",
          // "http://192.168.57.84/orca_api/public/login",
          body: _bodyDetail,
          headers: {'Content-type': 'application/json', "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));

      var _data = jsonDecode(_response.body);
      var _dataDetail = jsonDecode(_responseDetail.body);
      if(_response.statusCode == 200){
        if(_data['status'] == "1"){
          return _data['message'];
        }
        else{
          throw "${_data['message']}";
        }
      }
      if(_responseDetail.statusCode == 200){
        if(_dataDetail['status'] == "1"){
          return _dataDetail['message'];
        }
        else{
          throw "${_dataDetail['message']}";
        }
      }
      else{
        throw "Save partial subsidi error ${_response.statusCode}";
      }
    }
    on TimeoutException catch(_){
      throw "Save partial subsidi timeout connection";
    }
    catch(e){
      throw "Save partial subsidi ${e.toString()}";
    }
  }

  // save partial untuk manajemen PIC COM
  Future<String> submitManagementPIC(int idx,BuildContext context) async{
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _providerManajemenPIC = Provider.of<FormMCompanyManajemenPICChangeNotifier>(context,listen: false);
    var _providerManajemenPICAddress = Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context,listen: false);
    List _custDetailMngmntPicAddr = [];

    for(int i=0; i < _providerManajemenPICAddress.listManajemenPICAddress.length; i++){
      _custDetailMngmntPicAddr.add({
        "P_ADDRESS": _providerManajemenPICAddress.listManajemenPICAddress[i].address,
        "P_ADDRESS_ID": _preferences.getString("last_known_state") != "IDE" ? _providerManajemenPICAddress.listManajemenPICAddress[i].addressID : "NEW",
        "P_ADDR_DESC": _providerManajemenPICAddress.listManajemenPICAddress[i].jenisAlamatModel.DESKRIPSI,
        "P_ADDR_TYPE": _providerManajemenPICAddress.listManajemenPICAddress[i].jenisAlamatModel.KODE,
        "P_CREATED_BY": _preferences.getString("username"),
        "P_FAX": "",
        "P_FAX_AREA": "",
        "P_HANDPHONE_NO": "",
        "P_KABKOT": _providerManajemenPICAddress.listManajemenPICAddress[i].kelurahanModel.KABKOT_ID,
        "P_KABKOT_DESC": _providerManajemenPICAddress.listManajemenPICAddress[i].kelurahanModel.KABKOT_NAME,
        "P_KECAMATAN": _providerManajemenPICAddress.listManajemenPICAddress[i].kelurahanModel.KEC_ID,
        "P_KECAMATAN_DESC": _providerManajemenPICAddress.listManajemenPICAddress[i].kelurahanModel.KEC_NAME,
        "P_KELURAHAN": _providerManajemenPICAddress.listManajemenPICAddress[i].kelurahanModel.KEL_ID,
        "P_KELURAHAN_DESC": _providerManajemenPICAddress.listManajemenPICAddress[i].kelurahanModel.KEL_NAME,
        "P_KORESPONDEN": _providerManajemenPICAddress.listManajemenPICAddress[i].isCorrespondence ? "1" : "0",
        "P_MATRIX_ADDR": "9",
        "P_ORDER_NO": _preferences.getString("order_no"),
        "P_PHONE1": _providerManajemenPICAddress.listManajemenPICAddress[i].phone,
        "P_PHONE1_AREA": _providerManajemenPICAddress.listManajemenPICAddress[i].areaCode,
        "P_PHONE2": "",
        "P_PHONE2_AREA": "",
        "P_PROVINSI": _providerManajemenPICAddress.listManajemenPICAddress[i].kelurahanModel.PROV_ID,
        "P_PROVINSI_DESC": _providerManajemenPICAddress.listManajemenPICAddress[i].kelurahanModel.PROV_NAME,
        "P_RT": _providerManajemenPICAddress.listManajemenPICAddress[i].rt,
        "P_RT_DESC": "",
        "P_RW": _providerManajemenPICAddress.listManajemenPICAddress[i].rw,
        "P_RW_DESC": "",
        "P_ZIP_CODE": _providerManajemenPICAddress.listManajemenPICAddress[i].kelurahanModel.ZIPCODE,
        "P_FOREIGN_BK": _preferences.getString("last_known_state") != "IDE"
            ? _providerManajemenPICAddress.listManajemenPICAddress[i].foreignBusinessID != "NEW"
            ? _providerManajemenPICAddress.listManajemenPICAddress[i].foreignBusinessID : "NEW" : "NEW",
        "P_ID_NO": _providerManajemenPIC.controllerIdentityNumber.text != "" ? _providerManajemenPIC.controllerIdentityNumber.text : "",
      });
    }

    var _body = jsonEncode(
        {
          "custDetailMngmntPic": [
            {
              "P_AC_LEVEL": _providerManajemenPIC.positionSelected != null ? _providerManajemenPIC.positionSelected.PARA_FAMILY_TYPE_ID : "",
              "P_ALIAS_NAME": "",
              "P_CREATED_BY": _preferences.getString("username"),
              "P_DATE_OF_BIRTH": dateFormat2.format(_providerManajemenPIC.initialDateForBirthOfDate),
              "P_DEGREE": "",
              "P_EMAIL": "",
              "P_FULL_NAME": _providerManajemenPIC.controllerFullName.text != "" ? _providerManajemenPIC.controllerFullName.text : "",
              "P_FULL_NAME_ID": _providerManajemenPIC.controllerFullNameIdentity.text != "" ? _providerManajemenPIC.controllerFullNameIdentity.text : "",
              "P_HANDPHONE_NO": _providerManajemenPIC.controllerHandphoneNumber.text != "" ? _providerManajemenPIC.controllerHandphoneNumber.text : "",
              "P_ID_DESC": _providerManajemenPIC.typeIdentitySelected != null ? _providerManajemenPIC.typeIdentitySelected.text : "",
              "P_ID_NO": _providerManajemenPIC.controllerIdentityNumber.text != "" ? _providerManajemenPIC.controllerIdentityNumber.text : "",
              "P_ID_TYPE": _providerManajemenPIC.typeIdentitySelected != null ? _providerManajemenPIC.typeIdentitySelected.id : "",
              "P_ORDER_NO ": _preferences.getString("order_no"),
              "P_PLACE_OF_BIRTH": _providerManajemenPIC.controllerPlaceOfBirthIdentity.text != "" ? _providerManajemenPIC.controllerPlaceOfBirthIdentity.text : "",
              "P_PLACE_OF_BIRTH_KABKOTA": _providerManajemenPIC.birthPlaceSelected != null ? _providerManajemenPIC.birthPlaceSelected.KABKOT_ID : "",
              "P_PLACE_OF_BIRTH_KABKOTA_DESC": _providerManajemenPIC.birthPlaceSelected != null ? _providerManajemenPIC.birthPlaceSelected.KABKOT_NAME : "",
            }
          ],
          "custDetailMngmntPicAddr": _custDetailMngmntPicAddr,
          "idx": idx
        }
    );

    // await http.post(
    //     "http://192.168.57.122/orca_api/public/login",
    //     body: _body,
    //     headers: {"Content-Type":"application/json"}
    // );

    var _timeStart = DateTime.now();
    Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, "Start Save Partial Manage. PIC ${_preferences.getString("order_no")}","Start Save Partial Manage. PIC ${_preferences.getString("last_known_state")} ${_preferences.getString("order_no")}");
    String _managementPicPartial = await _storage.read(key: "ManagementPICPartial");
    try{
      final _response = await http.post(
          "${BaseUrl.urlGeneral}$_managementPicPartial",
          // "${BaseUrl.urlGeneral}ms2/api/save-draft/detail-managementpic",
          // "http://192.168.57.84/orca_api/public/login",
          body: _body,
          headers: {'Content-type': 'application/json', "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));

      var _data = jsonDecode(_response.body);
      if(_response.statusCode == 200){
        if(_data['status'] == "1"){
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, jsonEncode(_response.body), "Start Save Partial Manage. PIC ${_preferences.getString("last_known_state")}");
          return _data['message'];
        }
        else{
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, jsonEncode(_response.body), "Start Save Partial Manage. PIC ${_preferences.getString("last_known_state")}");
          throw "${_data['message']}";
        }
      }
      else{
        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, jsonEncode(_response.body), "Start Save Partial Manage. PIC ${_preferences.getString("last_known_state")}");
        throw "Save partial management PIC error ${_response.statusCode}";
      }
    }
    on TimeoutException catch(_){
      throw "Save partial management PIC timeout connection";
    }
    catch(e){
      Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, e.toString(), "Start Save Partial Management PIC ${_preferences.getString("last_known_state")}");
      throw "Save partial management PIC ${e.toString()}";
    }
  }

  // save partial untuk pemegang saham COM
  Future<String> submitShareHolder(int idx,BuildContext context) async{
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _providerShareHolderPer = Provider.of<PemegangSahamPribadiChangeNotifier>(context,listen: false);
    var _providerShareHolderCom = Provider.of<PemegangSahamLembagaChangeNotifier>(context,listen: false);
    List _custDetailShrhldrCom = [];
    List _custDetailShrhldrInd = [];
    List _custDetailShrhldrComAddr = [];
    List _custDetailShrhldrIndAddr = [];

    for(int i=0; i < _providerShareHolderCom.listPemegangSahamLembaga.length; i++){
      _custDetailShrhldrCom.add(
          {
            "P_COMP_NAME": _providerShareHolderCom.listPemegangSahamLembaga[i].institutionName,
            "P_COMP_TYPE": _providerShareHolderCom.listPemegangSahamLembaga[i].typeInstitutionModel.PARA_ID,
            "P_COMP_TYPE_DESC": _providerShareHolderCom.listPemegangSahamLembaga[i].typeInstitutionModel.PARA_NAME,
            "P_CREATED_BY": _preferences.getString("username"),
            "P_ESTABLISH_DATE": dateFormat2.format(DateTime.parse(_providerShareHolderCom.listPemegangSahamLembaga[i].initialDateForDateOfEstablishment)),
            "P_NPWP_NO": _providerShareHolderCom.listPemegangSahamLembaga[i].npwp,
            "P_ORDER_NO": _preferences.getString("order_no"),
            "P_SHRHLDNG_PERCENT": _providerShareHolderCom.listPemegangSahamLembaga[i].percentShare,
            "P_SHRHLDR_COMP_ID": _preferences.getString("last_known_state") != "IDE"
                ? _providerShareHolderCom.listPemegangSahamLembaga[i].shareholdersCorporateID != null
                ? _providerShareHolderCom.listPemegangSahamLembaga[i].shareholdersCorporateID : "NEW" : "NEW",
          }
      );
    }

    for(int i=0; i < _providerShareHolderCom.listPemegangSahamLembaga.length; i++){
      for(int j=0; j < _providerShareHolderCom.listPemegangSahamLembaga[i].listAddress.length; j++){
        _custDetailShrhldrComAddr.add({
          "P_ADDRESS": _providerShareHolderCom.listPemegangSahamLembaga[i].listAddress[j].address,
          "P_ADDRESS_ID": _preferences.getString("last_known_state") != "IDE" ? _providerShareHolderCom.listPemegangSahamLembaga[i].listAddress[j].addressID : "NEW",
          "P_ADDR_DESC": _providerShareHolderCom.listPemegangSahamLembaga[i].listAddress[j].jenisAlamatModel.DESKRIPSI,
          "P_ADDR_TYPE": _providerShareHolderCom.listPemegangSahamLembaga[i].listAddress[j].jenisAlamatModel.KODE,
          "P_CREATED_BY": _preferences.getString("username"),
          "P_FAX": _providerShareHolderCom.listPemegangSahamLembaga[i].listAddress[j].fax,
          "P_FAX_AREA": _providerShareHolderCom.listPemegangSahamLembaga[i].listAddress[j].faxArea,
          "P_HANDPHONE_NO": "",
          "P_KABKOT": _providerShareHolderCom.listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.KABKOT_ID,
          "P_KABKOT_DESC": _providerShareHolderCom.listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.KABKOT_NAME,
          "P_KECAMATAN": _providerShareHolderCom.listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.KEC_ID,
          "P_KECAMATAN_DESC": _providerShareHolderCom.listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.KEC_NAME,
          "P_KELURAHAN": _providerShareHolderCom.listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.KEL_ID,
          "P_KELURAHAN_DESC": _providerShareHolderCom.listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.KEL_NAME,
          "P_KORESPONDEN": _providerShareHolderCom.listPemegangSahamLembaga[i].listAddress[j].isCorrespondence ? "1" : "0",
          "P_MATRIX_ADDR": "12",
          "P_ORDER_NO": _preferences.getString("order_no"),
          "P_PHONE1": _providerShareHolderCom.listPemegangSahamLembaga[i].listAddress[j].phone1,
          "P_PHONE1_AREA": _providerShareHolderCom.listPemegangSahamLembaga[i].listAddress[j].phoneArea1,
          "P_PHONE2": _providerShareHolderCom.listPemegangSahamLembaga[i].listAddress[j].phone2,
          "P_PHONE2_AREA": _providerShareHolderCom.listPemegangSahamLembaga[i].listAddress[j].phoneArea2,
          "P_PROVINSI": _providerShareHolderCom.listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.PROV_ID,
          "P_PROVINSI_DESC": _providerShareHolderCom.listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.PROV_NAME,
          "P_RT": _providerShareHolderCom.listPemegangSahamLembaga[i].listAddress[j].rt,
          "P_RT_DESC": "",
          "P_RW": _providerShareHolderCom.listPemegangSahamLembaga[i].listAddress[j].rw,
          "P_RW_DESC": "",
          "P_ZIP_CODE": _providerShareHolderCom.listPemegangSahamLembaga[i].listAddress[j].kelurahanModel.ZIPCODE,
          "P_FOREIGN_BK": _preferences.getString("last_known_state") != "IDE"
              ? _providerShareHolderCom.listPemegangSahamLembaga[i].listAddress[j].foreignBusinessID != "NEW"
              ? _providerShareHolderCom.listPemegangSahamLembaga[i].listAddress[j].foreignBusinessID : "NEW" : "NEW",
          "P_ID_NO": _providerShareHolderCom.listPemegangSahamLembaga[i].npwp,
        });
      }
    }

    for(int i=0; i < _providerShareHolderPer.listPemegangSahamPribadi.length; i++){
      _custDetailShrhldrInd.add(
          {
            "P_ALIAS_NAME": "",
            "P_CREATED_BY": _preferences.getString("username"),
            "P_DATE_OF_BIRTH": dateFormat2.format(DateTime.parse(_providerShareHolderPer.listPemegangSahamPribadi[i].birthOfDate)),
            "P_DEGREE": "",
            "P_FULL_NAME": _providerShareHolderPer.listPemegangSahamPribadi[i].fullName,
            "P_FULL_NAME_ID": _providerShareHolderPer.listPemegangSahamPribadi[i].fullNameIdentity,
            "P_GENDER": "",
            "P_GENDER_DESC": "",
            "P_ID_NO": _providerShareHolderPer.listPemegangSahamPribadi[i].identityNumber,
            "P_ID_TYPE": _providerShareHolderPer.listPemegangSahamPribadi[i].typeIdentitySelected.id,
            "P_ID_TYPE_DESC": _providerShareHolderPer.listPemegangSahamPribadi[i].typeIdentitySelected.name,
            "P_ORDER_NO": _preferences.getString("order_no"),
            "P_PLACE_OF_BIRTH": _providerShareHolderPer.listPemegangSahamPribadi[i].placeOfBirthIdentity,
            "P_PLACE_OF_BIRTH_KABKOTA": _providerShareHolderPer.listPemegangSahamPribadi[i].birthPlaceIdentityLOV.KABKOT_ID,
            "P_PLACE_OF_BIRTH_KABKOTA_DESC": _providerShareHolderPer.listPemegangSahamPribadi[i].birthPlaceIdentityLOV.KABKOT_NAME,
            "P_RELATION_STATUS": _providerShareHolderPer.listPemegangSahamPribadi[i].relationshipStatusSelected.PARA_FAMILY_TYPE_ID,
            "P_RELATION_STATUS_DESC": _providerShareHolderPer.listPemegangSahamPribadi[i].relationshipStatusSelected.PARA_FAMILY_TYPE_NAME,
            "P_SHRHLDNG_PERCENT": _providerShareHolderPer.listPemegangSahamPribadi[i].percentShare,
            "P_SHRHLDR_IND_ID": _preferences.getString("last_known_state") != "IDE"
                ? _providerShareHolderPer.listPemegangSahamPribadi[i].shareholdersIndividualID != null
                ? _providerShareHolderPer.listPemegangSahamPribadi[i].shareholdersIndividualID : "NEW" : "NEW",
          }
      );
    }

    for(int i=0; i < _providerShareHolderPer.listPemegangSahamPribadi.length; i++){
      for(int j=0; j < _providerShareHolderPer.listPemegangSahamPribadi[i].listAddress.length; j++){
        _custDetailShrhldrIndAddr.add({
          "P_ADDRESS": _providerShareHolderPer.listPemegangSahamPribadi[i].listAddress[j].address,
          "P_ADDRESS_ID": _preferences.getString("last_known_state") != "IDE" ? _providerShareHolderPer.listPemegangSahamPribadi[i].listAddress[j].addressID : "NEW",
          "P_ADDR_DESC": _providerShareHolderPer.listPemegangSahamPribadi[i].listAddress[j].jenisAlamatModel.DESKRIPSI,
          "P_ADDR_TYPE": _providerShareHolderPer.listPemegangSahamPribadi[i].listAddress[j].jenisAlamatModel.KODE,
          "P_CREATED_BY": _preferences.getString("username"),
          "P_FAX": "",
          "P_FAX_AREA": "",
          "P_HANDPHONE_NO": "",
          "P_KABKOT": _providerShareHolderPer.listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.KABKOT_ID,
          "P_KABKOT_DESC": _providerShareHolderPer.listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.KABKOT_NAME,
          "P_KECAMATAN": _providerShareHolderPer.listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.KEC_ID,
          "P_KECAMATAN_DESC": _providerShareHolderPer.listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.KEC_NAME,
          "P_KELURAHAN": _providerShareHolderPer.listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.KEL_ID,
          "P_KELURAHAN_DESC": _providerShareHolderPer.listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.KEL_NAME,
          "P_KORESPONDEN": _providerShareHolderPer.listPemegangSahamPribadi[i].listAddress[j].isCorrespondence ? "1" : "0",
          "P_MATRIX_ADDR": "11",
          "P_ORDER_NO": _preferences.getString("order_no"),
          "P_PHONE1": _providerShareHolderPer.listPemegangSahamPribadi[i].listAddress[j].phone,
          "P_PHONE1_AREA": _providerShareHolderPer.listPemegangSahamPribadi[i].listAddress[j].areaCode,
          "P_PHONE2": "",
          "P_PHONE2_AREA": "",
          "P_PROVINSI": _providerShareHolderPer.listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.PROV_ID,
          "P_PROVINSI_DESC": _providerShareHolderPer.listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.PROV_NAME,
          "P_RT": _providerShareHolderPer.listPemegangSahamPribadi[i].listAddress[j].rt,
          "P_RT_DESC": "",
          "P_RW": _providerShareHolderPer.listPemegangSahamPribadi[i].listAddress[j].rw,
          "P_RW_DESC": "",
          "P_ZIP_CODE": _providerShareHolderPer.listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.ZIPCODE,
          "P_FOREIGN_BK": _preferences.getString("last_known_state") != "IDE"
              ? _providerShareHolderPer.listPemegangSahamPribadi[i].listAddress[j].foreignBusinessID != "NEW"
              ? _providerShareHolderPer.listPemegangSahamPribadi[i].listAddress[j].foreignBusinessID : "NEW" : "NEW",
          "P_ID_NO": _providerShareHolderPer.listPemegangSahamPribadi[i].identityNumber,
        });
      }
    }

    var _body = jsonEncode(
        {
          "custDetailShrhldrCom": _custDetailShrhldrCom,
          "custDetailShrhldrComAddr": _custDetailShrhldrComAddr,
          "custDetailShrhldrInd": _custDetailShrhldrInd,
          "custDetailShrhldrIndAddr": _custDetailShrhldrIndAddr,
          "idx": idx
        }
    );

    // await http.post(
    //     "http://192.168.57.122/orca_api/public/login",
    //     body: _body,
    //     headers: {"Content-Type":"application/json"}
    // );

    var _timeStart = DateTime.now();
    Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, "Start Save Partial Pem. Saham ${_preferences.getString("order_no")}","Start Save Partial Pem. Saham ${_preferences.getString("last_known_state")} ${_preferences.getString("order_no")}");
    String _shareholderPartial = await _storage.read(key: "ShareholderPartial");
    try{
      final _response = await http.post(
          "${BaseUrl.urlGeneral}$_shareholderPartial",
          // "${BaseUrl.urlGeneral}ms2/api/save-draft/detail-shareholder",
          body: _body,
          headers: {'Content-type': 'application/json', "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));

      var _data = jsonDecode(_response.body);
      if(_response.statusCode == 200){
        if(_data['status'] == "1"){
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, jsonEncode(_response.body), "Save Partial Pem. Saham ${_preferences.getString("last_known_state")}");
          return _data['message'];
        }
        else{
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, jsonEncode(_response.body), "Save Partial Pem. Saham ${_preferences.getString("last_known_state")}");
          throw "${_data['message']}";
        }
      }
      else{
        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, jsonEncode(_response.body), "Save Partial Pem. Saham ${_preferences.getString("last_known_state")}");
        throw "Save Partial Pemegang Saham error ${_response.statusCode}";
      }
    }
    on TimeoutException catch(_){
      throw "Save Partial Pemegang Saham timeout connection";
    }
    catch(e){
      Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, e.toString(), "Save Partial Pem. Saham ${_preferences.getString("last_known_state")}");
      throw "Save Partial Pemegang Saham ${e.toString()}";
    }
  }
}