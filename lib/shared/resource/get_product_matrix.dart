import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/shared/change_notifier_app/information_collatelar_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';

import '../../main.dart';
import '../form_m_foto_change_notif.dart';

Future<Map> getProdMatrixID(BuildContext context, String flag) async{
    var storage = FlutterSecureStorage();
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
    var _providerObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    var _providerInfoCollateral = Provider.of<InformationCollateralChangeNotifier>(context,listen: false);
    var _finalResult;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
    var _body = jsonEncode(
        {
            "P_JENIS_PRODUK_ID": "${_providerInfoCollateral.productTypeSelected.id}", // Jenis produk
            "P_OBJECT_GROUP_ID": "${_providerInfoCollateral.groupObjectSelected.KODE}",// Group Objek
            "P_OBJECT_ID": "${_providerInfoCollateral.objectSelected.id}", // Objek
            "P_OJK_BUSS_DETAIL_ID" : flag == "COM" ? "${_providerObjectUnit.businessActivitiesModelSelected.id}" : "${_providerFoto.jenisKegiatanUsahaSelected.id}",
            "P_OJK_BUSS_ID" : flag == "COM" ? "${_providerObjectUnit.businessActivitiesTypeModelSelected.id}" : "${_providerFoto.kegiatanUsahaSelected.id}"
        }
    );
    String _productMatrix = await storage.read(key: "ProductMatrix");
    try{
        final _response = await _http.post(
            "${BaseUrl.urlGeneral}$_productMatrix",
            body: _body,
            headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
        ).timeout(Duration(seconds: 30));

        if(_response.statusCode == 200){
            final _result = jsonDecode(_response.body);
            final _data = _result['data'];
            if(_data.isEmpty){
                _finalResult = {
                    "status": false,
                    "message": "Gagal mendapat Prod Matrix ID"
                };
            }
            else{
                print(_result['data']);
                _finalResult = {
                    "status": true,
                    "message": "Berhasil mendapatkan Prod Matrix ID",
                    "value" : _data[0]['PROD_MATRIX_ID']
                };
            }
        }else{
            _finalResult = {
                "status": false,
                "message": "Error ${_response.statusCode}"
            };
        }
    }
    on TimeoutException catch(_){
        _finalResult = {
            "status": false,
            "message": "Timeout connection"
        };
    }
    catch(e){
        _finalResult = {
            "status": false,
            "message": "Error ${e.toString()}"
        };
    }
    return _finalResult;
}