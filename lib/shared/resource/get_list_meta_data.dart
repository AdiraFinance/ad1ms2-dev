import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:dio/adapter.dart';
import 'package:dio/dio.dart' as dio;
import 'dart:io';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:async';
import 'package:http/io_client.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';

Future<List> getListMetadata(String id) async{
  var storage = FlutterSecureStorage();
  try{
    String _metadataTipeDokumen = await storage.read(key: "METADATA");
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
      // var _dio = dio.Dio();
      // (_dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate  = (client) {
      //   client.badCertificateCallback=(X509Certificate cert, String host, int port){
      //     return true;
      //   };
      // };
    var _body = jsonEncode({"refOne": id});
    var _request = await _http.post(
      "${BaseUrl.urlGeneral}$_metadataTipeDokumen",
        // urlPublic+"proses-dokumen/get_list_metadata",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    ).timeout(Duration(seconds: 10));
    print("cek data ${_request.body}");
    List _data = jsonDecode(_request.body);
    if(_data.isNotEmpty){
      return _data;
    }
    else{
      throw "Meta data tidak ditemukan";
    }
  }
  on TimeoutException catch(_){
    throw "Timeout connection";
  }
  catch(e){
   throw e.toString();
  }
}