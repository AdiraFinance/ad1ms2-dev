import 'dart:async';

import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/resource/get_data_cust_info.dart';
import 'package:dio/dio.dart' as dio;
import 'package:flutter/material.dart';
import '../../main.dart';

class OldApplicationDTOProvider{
  Future<Map> oldApplication(String applNo) async{
    String _getMS2ApplicationDTO = await storage.read(key: "GetMS2ApplicationDTO");
    try{

      var _response = await dio.Dio().get("${BaseUrl.urlGeneral}$_getMS2ApplicationDTO"+"applno=$applNo",
        options: dio.Options(
            headers: {"Authorization":"bearer $token"},
            followRedirects: false,
            validateStatus: (status) { return status < 500; }
        ),
      ).timeout(Duration(seconds: 30));

      if(_response.statusCode == 302){
        final _data = _response.data;
        Map _applicationDTO;
        _applicationDTO = _data['applicationDTO'];
        return _applicationDTO;
      }
      else{
        throw "Gagal mendapatkan oldApplication response ${_response.statusCode}";
      }
    }
    on TimeoutException catch(_){
      throw "Gagal mendapatkan oldApplication request timeout";
    }
    catch(e){
      throw "Gagal mendapatkan oldApplication ${e.toString()}";
    }
  }
}