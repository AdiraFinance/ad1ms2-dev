import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';

import '../../main.dart';

 Future<List<JenisAlamatModel>> getAddressType(int flag) async{
     List<JenisAlamatModel> _listJenisAlamatAPI = [];
     var storage = FlutterSecureStorage();
     String _fieldJenisAlamat = await storage.read(key: "FieldJenisAlamat");
     final ioc = new HttpClient();
     ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
     final _http = IOClient(ioc);
     final _response = await _http.get(
         "${BaseUrl.urlGeneral}$_fieldJenisAlamat",
         headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
         // "${urlPublic}api/address/get-address-type"
     );
     final _data = jsonDecode(_response.body);

//     identitas
     if(flag == 1){
         for(int i=0; i < _data['data'].length;i++){
             if(_data['data'][i]['KODE'] == "03"){
                 _listJenisAlamatAPI.add(JenisAlamatModel(_data['data'][i]['KODE'], _data['data'][i]['DESKRIPSI']));
             }
         }
     }
//     domisili
     else if(flag == 2){
         for(int i=0; i < _data['data'].length;i++){
             if(_data['data'][i]['KODE'] == "01"){
                 _listJenisAlamatAPI.add(JenisAlamatModel(_data['data'][i]['KODE'], _data['data'][i]['DESKRIPSI']));
             }
         }
     }
     //kantor 1
     else if(flag == 7){
         for(int i=0; i < _data['data'].length;i++){
             if(_data['data'][i]['KODE'] == "02"){
                 _listJenisAlamatAPI.add(JenisAlamatModel(_data['data'][i]['KODE'], _data['data'][i]['DESKRIPSI']));
             }
         }
     }
     //selain kantor 1
     else if(flag == 8){
         for(int i=0; i < _data['data'].length;i++){
             if(_data['data'][i]['KODE'] == "04" || _data['data'][i]['KODE'] == "05"){
                 _listJenisAlamatAPI.add(JenisAlamatModel(_data['data'][i]['KODE'], _data['data'][i]['DESKRIPSI']));
             }
         }
     }
     //semua kantor
     else if(flag == 9){
         for(int i=0; i < _data['data'].length;i++){
             if(_data['data'][i]['KODE'] == "02" || _data['data'][i]['KODE'] == "04" || _data['data'][i]['KODE'] == "05"){
                 _listJenisAlamatAPI.add(JenisAlamatModel(_data['data'][i]['KODE'], _data['data'][i]['DESKRIPSI']));
             }
         }
     }
     //semua alamat
     else if(flag == 10){
         for(int i=0; i < _data['data'].length;i++){
             _listJenisAlamatAPI.add(JenisAlamatModel(_data['data'][i]['KODE'], _data['data'][i]['DESKRIPSI']));
         }
     }
     // selain identitas
     else {
         for(int i=0; i < _data['data'].length;i++){
             if(_data['data'][i]['KODE'] != "03"){
                 _listJenisAlamatAPI.add(JenisAlamatModel(_data['data'][i]['KODE'], _data['data'][i]['DESKRIPSI']));
             }
         }
     }

     return _listJenisAlamatAPI;
 }