import 'dart:convert';
import 'dart:io';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';

import '../../main.dart';

Future<Map> getAsuransiPerluasan(String  body) async{
  var storage = FlutterSecureStorage();
  try{
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);

    String _getRate = await storage.read(key: "JenisPertanggungan");
    final _response = await _http.post(
        // "${urlPublic}api/asuransi/get-rate-asuransi-utama",
        "${BaseUrl.urlGeneral}$_getRate",
        body: body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      print("result nih $_result");
      List _data = _result['data'];
      if(_data.isNotEmpty){
        return _data[0];
      }
      else{
        throw "Data tidak ditemukan";
      }
    }
    else{
      throw "Error response ${_response.statusCode}";
    }
  }
  catch (e) {
    throw "Error ${e.toString()}";
  }
}