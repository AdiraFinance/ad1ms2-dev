import 'dart:convert';
import 'dart:io';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';

import '../../main.dart';

class GetCompanyType{
  Future<Map> getCompanyTypeData() async{
    var storage = FlutterSecureStorage();
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);
    String urlPublic = await storage.read(key: "urlPublic");
    final _response = await _http.get(
      // "${BaseUrl.unit}api/parameter/get-group-object",
        "${urlPublic}jenis-rehab/get_jenis_perusahaan",
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      return _result;
    }
    else{
      throw Exception("Failed get data error ${_response.statusCode}");
    }
  }
}