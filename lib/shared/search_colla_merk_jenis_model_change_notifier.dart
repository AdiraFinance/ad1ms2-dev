import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/brand_object_model.dart';
import 'package:ad1ms2_dev/models/brand_type_model_genre_model.dart';
import 'package:ad1ms2_dev/models/model_object_model.dart';
import 'package:ad1ms2_dev/models/object_type_model.dart';
import 'package:ad1ms2_dev/models/object_usage_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';

import 'change_notifier_app/information_collatelar_change_notifier.dart';

class SearchCollaMerkJenisModelChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  TextEditingController _controllerSearch = TextEditingController();
  List<BrandTypeModelGenreModel> _listData = [];
  List<BrandTypeModelGenreModel> _listDataSearch = [];
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  void getData(BuildContext context, int type, String query, String groupObject) async{
    // type 1 = merk, 2 = jenis, 3 = model
    var _providerColla = Provider.of<InformationCollateralChangeNotifier>(context, listen: false);
    this._listData.clear();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);
    var _body = type != 3 ?
    jsonEncode({
      "PARA_KODE_OR_NAME": "$query",
      "PARA_OBJECT_GROUP_ID": "$groupObject"
    })
    :
    jsonEncode({
      "P_OBJECT_BRAND_ID": _providerColla.brandObjectSelected != null ? _providerColla.brandObjectSelected.id : "",
      "P_OBJECT_TYPE_ID": _providerColla.objectTypeSelected != null ? _providerColla.objectTypeSelected.id : ""
    });

    var storage = FlutterSecureStorage();
    String _fieldColla = await storage.read(key: type == 1 ? "FieldMerekObjekCollateral" : type == 2 ? "FieldTipeObjekCollateral" : "FieldModelObjekCollateral");
    final _response = await _http.post(
        "${BaseUrl.urlGeneral}$_fieldColla",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );

    if(type == 3 && (_providerColla.brandObjectSelected == null || _providerColla.objectTypeSelected == null)){
      showSnackBar("Merk Objek dan Jenis Objek tidak boleh kosong");
      loadData = false;
    }
    else if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      final _data = _result['data'];
      if(_data.isEmpty){
        showSnackBar("Data tidak ditemukan");
        loadData = false;
      }
      else{
        for(int i=0; i <_data.length; i++){
          this._listData.add(BrandTypeModelGenreModel(
                  BrandObjectModel(_data[i]['KODE'],_data[i]['DESKRIPSI']),
                  ObjectTypeModel(_data[i]['KODE'],_data[i]['DESKRIPSI']),
                  ModelObjectModel(_data[i]['KODE_MODEL'],_data[i]['DESKRIPSI_MODEL']),
                  ObjectUsageModel(_data[i]['KODE_GENRE'],_data[i]['DESKRIPSI_GENRE'])
              )
          );
        }
        loadData = false;
      }
    }
    else{
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  void searchData(BuildContext context, int type, String query, String groupObject) {
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    }
    else {
      if(type != 3){
        getData(context, type, query, groupObject);
      }
      else{
        _listDataSearch.clear();
        if (query.isEmpty) {
          return;
        }
        _listData.forEach((data) {
          if (data.modelObjectModel.id.contains(query) || data.modelObjectModel.name.contains(query)) {
            _listDataSearch.add(data);
          }
        });
        notifyListeners();
      }
    }
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  UnmodifiableListView<BrandTypeModelGenreModel> get listData {
    return UnmodifiableListView(this._listData);
  }

  UnmodifiableListView<BrandTypeModelGenreModel> get listDataSearch {
    return UnmodifiableListView(this._listDataSearch);
  }
}
