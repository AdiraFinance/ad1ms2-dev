import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/product_type_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';

class SearchProductTypeChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController _controllerSearch = TextEditingController();
  List<ProductTypeModel> _listProductType = [
    // ProductTypeModel("00035", "SALE AND LEASEBACK"),
    // ProductTypeModel("00034", "REFINANCING SYARIAH"),
    // ProductTypeModel("00031", "C2C DIGITAL MOTOR"),
    // ProductTypeModel("00033", "MAXI USAHA"),
    // ProductTypeModel("00032", "C2C DIGITAL MOBIL"),
  ];
  List<ProductTypeModel> _listProductTypeTemp = [];

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<ProductTypeModel> get listProductType {
    return UnmodifiableListView(this._listProductType);
  }
  UnmodifiableListView<ProductTypeModel> get listProductTypeTemp {
    return UnmodifiableListView(this._listProductTypeTemp);
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void getProductType(BuildContext context, String flag, String kodeGroupObject, String kodeObject) async{
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
    var _providerObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    this._listProductType.clear();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var _body = jsonEncode({
        "P_OBJECT_GROUP_ID": kodeGroupObject, //"${_providerObjectUnit.groupObjectSelected.KODE}"
        "P_OBJECT_ID": kodeObject, // "${_providerObjectUnit.objectSelected.id}"
        "P_OJK_BUSS_DETAIL_ID" : flag == "COM" ? "${_providerObjectUnit.businessActivitiesTypeModelSelected.id}" : "${_providerFoto.jenisKegiatanUsahaSelected.id}",
        "P_OJK_BUSS_ID" : flag == "COM" ? "${_providerObjectUnit.businessActivitiesModelSelected.id}" : "${_providerFoto.kegiatanUsahaSelected.id}"
    });

    print(_body);

    var storage = FlutterSecureStorage();
    String _fieldJenisProduk = await storage.read(key: "FieldJenisProduk");
    final _response = await _http.post(
        "${BaseUrl.urlGeneral}$_fieldJenisProduk",
        // "${urlPublic}api/parameter/get-jenis-produk",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      final _data = _result['data'];
      if(_data.isEmpty){
        showSnackBar("Product Type not found");
        loadData = false;
      }
      else{
        for(int i=0; i <_data.length; i++){
          this._listProductType.add(
              ProductTypeModel(_data[i]['KODE'], _data[i]['DESKRIPSI'])
          );
        }
        loadData = false;
      }
    }
    else{
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  void searchProductType(String query) {
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    } else {
      _listProductTypeTemp.clear();
      if (query.isEmpty) {
        return;
      }

      _listProductType.forEach((dataGroupObject) {
        if (dataGroupObject.id.contains(query) || dataGroupObject.name.contains(query)) {
          _listProductTypeTemp.add(dataGroupObject);
        }
      });
    }
    notifyListeners();
  }

  void clearSearchTemp() {
    _listProductTypeTemp.clear();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }
}
