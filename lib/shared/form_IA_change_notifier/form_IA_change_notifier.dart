import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/form_m/form_m_parent.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FormIAChangeNotifier with ChangeNotifier{
  TextEditingController _controllerName = TextEditingController();
  TextEditingController _controllerAddress = TextEditingController();
  TextEditingController _controllerAppNumber = TextEditingController();
  TextEditingController _controllerModelObject = TextEditingController();
  int _radioValueIsSignPK = -1;
  DbHelper _dbHelper = DbHelper();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  TextEditingController get controllerName => _controllerName;

  TextEditingController get controllerAddress => _controllerAddress;

  TextEditingController get controllerAppNumber => _controllerAppNumber;

  TextEditingController get controllerModelObject => _controllerModelObject;

  int get radioValueIsSignPK => _radioValueIsSignPK;

  set radioValueIsSignPK(int value) {
    this._radioValueIsSignPK = value;
    notifyListeners();
  }

  void check(BuildContext context) {
    if(_radioValueIsSignPK == 0) {
      showSnackBar("Wajib Sign PK.");
    } else {
      saveStatusIA(context);
    }
  }

  void saveStatusIA(BuildContext context) async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    _preferences.setString("status_ia", _radioValueIsSignPK.toString());
    Navigator.pushReplacement(context, MaterialPageRoute(builder:  (context) => FormMParent()));
  }

  Future<void> setDataFromSQLite() async{
    List _dataNasabah = await _dbHelper.selectDataInfoNasabah();
    var _dataAlamat = await _dbHelper.selectMS2CustAddr("5");
    var _dataApplNo = await _dbHelper.selectMS2Application();
    var _dataObject = await _dbHelper.selectMS2ApplObject();
    if(_dataNasabah.isNotEmpty){
      this._controllerName.text = _dataNasabah[0]['full_name'];
    }
    if(_dataAlamat.isNotEmpty){
      for(int i=0; i < _dataAlamat.length; i++){
        debugPrint("CEK ALAMAT IA CHANGE NOTIF ${_dataAlamat[i]['koresponden']}");
        if(_dataAlamat[i]['koresponden'] == "1")
          this._controllerAddress.text = _dataAlamat[i]['address'];
      }
    }
    if(_dataApplNo.isNotEmpty){
      this._controllerAppNumber.text = _dataApplNo[0]['applNo'];
      this._radioValueIsSignPK = _dataApplNo[0]['sign_pk'];
    }
    if(_dataObject.isNotEmpty){
      this._controllerModelObject.text = "${_dataObject[0]['object_model']} - ${_dataObject[0]['object_model_desc']}";
    }
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }
}