import 'package:ad1ms2_dev/models/form_m_company_alamat_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';

class FormMCompanyPenjaminPribadiAlamatChangeNotifier with ChangeNotifier {
  int _selectedIndex = -1;
  List<CompanyAlamatKorespondensiModel> _listAlamatKorespondensi = [];
  bool _autoValidate = false;
  TextEditingController _controllerInfoAlamat = TextEditingController();

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  int get selectedIndex => _selectedIndex;

  set selectedIndex(int value) {
    this._selectedIndex = value;
    notifyListeners();
  }

  List<CompanyAlamatKorespondensiModel> get listAlamatKorespondensi =>
      _listAlamatKorespondensi;

  TextEditingController get controllerInfoAlamat => _controllerInfoAlamat;

  void addAlamatKorespondensi(CompanyAlamatKorespondensiModel value) {
    this._listAlamatKorespondensi.add(value);
    notifyListeners();
  }

  void updateAlamatKorespondensi(CompanyAlamatKorespondensiModel value, int index) {
    this._listAlamatKorespondensi[index] = value;
    notifyListeners();
  }

  void deleteAlamatKorespondensi(int index) {
    this._listAlamatKorespondensi.removeAt(index);
    notifyListeners();
  }
}
