import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/third_party_type_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';

class SearchThirdPartyTypeChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  TextEditingController _controllerSearch = TextEditingController();
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<ThirdPartyTypeModel> _listThirdPartyType = [
    // ThirdPartyTypeModel("001", "DEALER"),
    // ThirdPartyTypeModel("001", "MERCHANT"),
    // ThirdPartyTypeModel("001", "MITRA MAXI PLUS UMROH"),
    // ThirdPartyTypeModel("001", "RETAIL CHANNEL"),
    // ThirdPartyTypeModel("001", "NON DEALER"),
  ];
  List<ThirdPartyTypeModel>  _listThirdPartyTypeTemp = [];

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<ThirdPartyTypeModel> get listThirdPartyType {
    return UnmodifiableListView(this._listThirdPartyType);
  }

  UnmodifiableListView<ThirdPartyTypeModel> get listThirdPartyTypeTemp {
    return UnmodifiableListView(this._listThirdPartyTypeTemp);
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  void getThirdPartyType(BuildContext context) async{
    var _providerObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    this._listThirdPartyType.clear();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var _body = jsonEncode({
      "refOne": "${_providerObjectUnit.brandObjectSelected.id}",
      "refTwo": ""
    });
    print("jenis pihak ketiga = $_body");

    var storage = FlutterSecureStorage();
    String _fieldJenisPihakKetiga = await storage.read(key: "FieldJenisPihakKetiga");
    final _response = await _http.post(
      "${BaseUrl.urlGeneral}$_fieldJenisPihakKetiga",
        // "${urlPublic}pihak-ketiga/get_pihakketiga",
        // "https://103.110.89.34/public/ms2dev/pihak-ketiga/get_pihakketiga",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      if(_result.isEmpty){
        showSnackBar("Jenis pihak ketiga tidak ditemukan");
        loadData = false;
      }
      else{
        for(int i=0; i <_result.length; i++){
          this._listThirdPartyType.add(
              ThirdPartyTypeModel(_result[i]['kode'], _result[i]['deskripsi'])
          );
        }
        loadData = false;
      }
    }
    else{
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void searchThirdPartyType(String query) {
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    } else {
      _listThirdPartyTypeTemp.clear();
      if (query.isEmpty) {
        return;
      }

      _listThirdPartyType.forEach((dataSourceOrder) {
        if (dataSourceOrder.kode.contains(query) || dataSourceOrder.deskripsi.contains(query)) {
          _listThirdPartyTypeTemp.add(dataSourceOrder);
        }
      });
    }
    notifyListeners();
  }

  void clearSearchTemp() {
    _listThirdPartyTypeTemp.clear();
  }
}
