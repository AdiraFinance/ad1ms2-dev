import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/form_m_company_pemegang_saham_pribadi_alamat_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_addr_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

//ga kepake
class FormMCompanyPemegangSahamPribadiAlamatChangeNotifier with ChangeNotifier {
  int _oldListSize = 0;
  int _selectedIndex = -1;
  bool _autoValidate = false;
  List<AddressModel> _listPemegangSahamPribadiAddress = [];
  TextEditingController _controllerAddress = TextEditingController();
  TextEditingController _controllerAddressType = TextEditingController();
  TextEditingController _controllerRT = TextEditingController();
  TextEditingController _controllerRW = TextEditingController();
  TextEditingController _controllerKelurahan = TextEditingController();
  TextEditingController _controllerKecamatan = TextEditingController();
  TextEditingController _controllerKota = TextEditingController();
  TextEditingController _controllerProvinsi = TextEditingController();
  TextEditingController _controllerPostalCode = TextEditingController();
  TextEditingController _controllerTeleponArea = TextEditingController();
  TextEditingController _controllerTelepon = TextEditingController();
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  bool _flag = false;
  DbHelper _dbHelper = DbHelper();


  int get oldListSize => _oldListSize;

  set oldListSize(int value) {
    this._oldListSize = value;
    notifyListeners();
  }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  int get selectedIndex => _selectedIndex;

  set selectedIndex(int value) {
    this._selectedIndex = value;
    notifyListeners();
  }

  List<AddressModel> get listPemegangSahamPribadiAddress => _listPemegangSahamPribadiAddress;

  // Alamat
  TextEditingController get controllerAddress => _controllerAddress;

  // Jenis Alamat
  TextEditingController get controllerAddressType => _controllerAddressType;

  // RT
  TextEditingController get controllerRT => _controllerRT;

  // RW
  TextEditingController get controllerRW => _controllerRW;

  // Kelurahan
  TextEditingController get controllerKelurahan => _controllerKelurahan;

  // Kecamatan
  TextEditingController get controllerKecamatan => _controllerKecamatan;

  // Kabupaten/Kota
  TextEditingController get controllerKota => _controllerKota;

  // Provinsi
  TextEditingController get controllerProvinsi => _controllerProvinsi;

  // Kode Pos
  TextEditingController get controllerPostalCode => _controllerPostalCode;

  // Telepon 1 Area
  TextEditingController get controllerTeleponArea => _controllerTeleponArea;

  // Telepon 1
  TextEditingController get controllerTelepon => _controllerTelepon;

  void addPemegangSahamPribadiAddress(AddressModel value) {
    this._listPemegangSahamPribadiAddress.add(value);
    notifyListeners();
  }

  void updatePemegangSahamPribadiAddress(AddressModel value, int index) {
    this._listPemegangSahamPribadiAddress[index] = value;
    notifyListeners();
  }

  void deletePemegangSahamPribadiAddress(BuildContext context, int index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                primaryColor: Colors.black,
                primarySwatch: primaryOrange,
                accentColor: myPrimaryColor
            ),
            child: AlertDialog(
              title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Apakah kamu yakin menghapus alamat ini?",),
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    this._listPemegangSahamPribadiAddress.removeAt(index);
                    if (selectedIndex == index) {
                      selectedIndex = -1;
                    }
                    this._controllerAddress.clear();
                    this._controllerAddressType.clear();
                    this._controllerRT.clear();
                    this._controllerRW.clear();
                    this._controllerKelurahan.clear();
                    this._controllerKecamatan.clear();
                    this._controllerKota.clear();
                    this._controllerProvinsi.clear();
                    this._controllerPostalCode.clear();
                    this._controllerTeleponArea.clear();
                    this._controllerTelepon.clear();
                    notifyListeners();
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                ),
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Tidak'),
                ),
              ],
            ),
          );
        }
    );
  }

  void setAddress(AddressModel value) {
    this._controllerAddress.text = value.address;
    this._controllerAddressType.text = value.jenisAlamatModel.KODE + " - " + value.jenisAlamatModel.DESKRIPSI;
    this._controllerRT.text = value.rt;
    this._controllerRW.text = value.rw;
    this._controllerKelurahan.text = value.kelurahanModel.KEL_ID + " - " + value.kelurahanModel.KEL_NAME;
    this._controllerKecamatan.text = value.kelurahanModel.KEC_ID + " - " + value.kelurahanModel.KEC_NAME;
    this._controllerKota.text = value.kelurahanModel.KABKOT_ID + " - " + value.kelurahanModel.KABKOT_NAME;
    this._controllerProvinsi.text = value.kelurahanModel.PROV_ID + " - " + value.kelurahanModel.PROV_NAME;
    this._controllerPostalCode.text = value.kelurahanModel.ZIPCODE;
    this._controllerTeleponArea.text = value.areaCode;
    this._controllerTelepon.text = value.phone;
    notifyListeners();
  }

  // void iconShowDialog(BuildContext context) {
  //   showDialog(
  //       context: context,
  //       barrierDismissible: false,
  //       builder: (BuildContext context){
  //         return Theme(
  //           data: ThemeData(
  //               fontFamily: "NunitoSans"
  //           ),
  //           child: AlertDialog(
  //             title: Text("Information", style: TextStyle(fontWeight: FontWeight.bold)),
  //             content: Column(
  //               crossAxisAlignment: CrossAxisAlignment.start,
  //               mainAxisSize: MainAxisSize.min,
  //               children: <Widget>[
  //                 Text(
  //                   "∙ Tekan 1x untuk edit",
  //                 ),
  //                 Text(
  //                   "∙ Tekan lama untuk memilih alamat korespondensi",
  //                 ),
  //                 SizedBox(height: MediaQuery.of(context).size.height/37,),
  //                 Text("* Tekan icon pada pojok kanan atas untuk melihat kembali",style: TextStyle(fontSize: 12, color: Colors.grey))
  //               ],
  //             ),
  //             actions: <Widget>[
  //               FlatButton(
  //                   onPressed: (){
  //                     Navigator.pop(context);
  //                     // _updateStatusShowDialogSimilarity();
  //                   },
  //                   child: Text(
  //                       "CLOSE",
  //                       style: TextStyle(
  //                           color: primaryOrange,
  //                           fontSize: 14,
  //                           fontWeight: FontWeight.w500,
  //                           letterSpacing: 1.25
  //                       )
  //                   )
  //               )
  //             ],
  //           ),
  //         );
  //       }
  //   );
  // }

  // void isShowDialog(BuildContext context) {
  //   if(this.listPemegangSahamPribadiAddress.length == 1 || this.listPemegangSahamPribadiAddress.length == 2) {
  //     showDialog(
  //         context: context,
  //         barrierDismissible: false,
  //         builder: (BuildContext context){
  //           return Theme(
  //             data: ThemeData(
  //                 fontFamily: "NunitoSans"
  //             ),
  //             child: AlertDialog(
  //               title: Text("Information", style: TextStyle(fontWeight: FontWeight.bold)),
  //               content: Column(
  //                 crossAxisAlignment: CrossAxisAlignment.start,
  //                 mainAxisSize: MainAxisSize.min,
  //                 children: <Widget>[
  //                   Text(
  //                     "∙ Tekan 1x untuk edit",
  //                   ),
  //                   Text(
  //                     "∙ Tekan lama untuk memilih alamat korespondensi",
  //                   ),
  //                   SizedBox(height: MediaQuery.of(context).size.height/37,),
  //                   Text("* Tekan icon pada pojok kanan atas untuk melihat kembali",style: TextStyle(fontSize: 12, color: Colors.grey))
  //                 ],
  //               ),
  //               actions: <Widget>[
  //                 FlatButton(
  //                     onPressed: (){
  //                       Navigator.pop(context);
  //                       // _updateStatusShowDialogSimilarity();
  //                     },
  //                     child: Text(
  //                         "CLOSE",
  //                         style: TextStyle(
  //                             color: primaryOrange,
  //                             fontSize: 14,
  //                             fontWeight: FontWeight.w500,
  //                             letterSpacing: 1.25
  //                         )
  //                     )
  //                 )
  //               ],
  //             ),
  //           );
  //         }
  //     );
  //   }
  // }

  void setCorrespondence(int index){
    for(int i=0; i < this._listPemegangSahamPribadiAddress.length; i++){
      if(this._listPemegangSahamPribadiAddress[i].isCorrespondence){
        this._listPemegangSahamPribadiAddress[i].isCorrespondence = false;
      }
    }
    this._listPemegangSahamPribadiAddress[index].isCorrespondence = true;
    notifyListeners();
  }

  void moreDialog(BuildContext context, index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans"
            ),
            child: AlertDialog(
              title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.check,
                          color: Colors.green,
                          size: 22.0,
                        ),
                        SizedBox(width: 12.0),
                        GestureDetector(
                          onTap: (){
                            selectedIndex = index;
                            _controllerAddress.clear();
                            setAddress(listPemegangSahamPribadiAddress[index]);
                            setCorrespondence(index);
                            Navigator.pop(context);
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Pilih sebagai Alamat Korespondensi",
                            style: TextStyle(fontSize: 14.0),
                          ),
                        )
                      ]
                  ),
                  SizedBox(height: 12.0),
                  listPemegangSahamPribadiAddress[index].jenisAlamatModel.KODE != "03"
                      ? listPemegangSahamPribadiAddress[index].isSameWithIdentity
                      ? SizedBox()
                      : Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.delete,
                          color: Colors.red,
                          size: 22.0,
                        ),
                        SizedBox(
                          width: 12.0,
                        ),
                        GestureDetector(
                          onTap: (){
                            Navigator.pop(context);
                            deletePemegangSahamPribadiAddress(context, index);
                          },
                          child: Text(
                            "Hapus",
                            style: TextStyle(fontSize: 14.0, color: Colors.red),
                          ),
                        )
                      ]
                  )
                      : SizedBox(),
                ],
              ),
              actions: <Widget>[
                FlatButton(
                    onPressed: (){
                      Navigator.pop(context);
                      // _updateStatusShowDialogSimilarity();
                    },
                    child: Text(
                        "CLOSE",
                        style: TextStyle(
                            color: primaryOrange,
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 1.25
                        )
                    )
                )
              ],
            ),
          );
        }
    );
  }

  GlobalKey<FormState> get keyForm => _key;

  bool get flag => _flag;

  set flag(bool value) {
    _flag = value;
    notifyListeners();
  }

  void check(BuildContext context) {
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
      Navigator.pop(context);
    } else {
      flag = false;
      autoValidate = true;
    }
  }

  Future<bool> onBackPress() async{
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
    } else {
      flag = false;
      autoValidate = true;
    }
    return true;
  }

  void clearDataAlamat() {
    this._autoValidate = false;
    this._flag = false;
    this._selectedIndex = -1;
    this._controllerAddress.clear();
    this._controllerAddressType.clear();
    this._controllerRT.clear();
    this._controllerRW.clear();
    this._controllerKelurahan.clear();
    this._controllerKecamatan.clear();
    this._controllerKota.clear();
    this._controllerProvinsi.clear();
    this._controllerPostalCode.clear();
    this._controllerTelepon.clear();
    this._controllerTeleponArea.clear();
    this._listPemegangSahamPribadiAddress = [];
  }

  Future<bool> deleteSQLite(String type) async{
    return await _dbHelper.deleteMS2CustAddr(type);
  }

  void setDataSQLite() async{
    var _check = await _dbHelper.selectMS2CustAddr("11");
    if(this._listPemegangSahamPribadiAddress.isNotEmpty){
    }
    else{
      if(_check.isNotEmpty){
        for(int i=0; i<_check.length; i++){
          var jenisAlamatModel = JenisAlamatModel(_check[i]['addr_type'], _check[i]['addr_desc']);

          var kelurahanModel = KelurahanModel(_check[i]['kelurahan'], _check[i]['kelurahan_desc'],
              _check[i]['kecamatan_desc'], _check[i]['kabkot_desc'], _check[i]['provinsi_desc'], _check[i]['zip_code'],
              _check[i]['kecamatan'], _check[i]['kabkot'], _check[i]['provinsi']);

          var _addressFromMap = {
            "address": _check[i]['address_from_map'],
            "latitude": _check[i]['latitude'],
            "longitude": _check[i]['longitude']
          };
          var _koresponden = _check[i]['koresponden'].toString().toLowerCase();
          _listPemegangSahamPribadiAddress.add(
              AddressModel(
                jenisAlamatModel,
                _check[i]['addressID'],
                _check[i]['foreignBusinessID'],
                null,
                kelurahanModel,
                _check[i]['address'].toString(),
                _check[i]['rt'].toString(),
                _check[i]['rw'].toString(),
                _check[i]['phone1'].toString(),
                _check[i]['phone1_area'].toString(),
                false,
                _addressFromMap,
                _koresponden == '1' ? true : false,
                int.parse(_check[i]['active']),
                _check[i]['edit_address'] == "1" ||
                    _check[i]['edit_addr_type'] == "1" ||
                    _check[i]['edit_rt'] == "1" ||
                    _check[i]['edit_rw'] == "1" ||
                    _check[i]['edit_kelurahan'] == "1" ||
                    _check[i]['edit_kecamatan'] == "1" ||
                    _check[i]['edit_kabkot'] == "1" ||
                    _check[i]['edit_provinsi'] == "1" ||
                    _check[i]['edit_zip_code'] == "1" ||
                    _check[i]['edit_address_from_map'] == "1" ||
                    _check[i]['edit_phone1_area'] == "1" ||
                    _check[i]['edit_phone1'] == "1" ? true : false,
                _check[i]['edit_address'] == "1",
                _check[i]['edit_addr_type'] == "1",
                _check[i]['edit_rt'] == "1",
                _check[i]['edit_rw'] == "1",
                _check[i]['edit_kelurahan'] == "1",
                _check[i]['edit_kecamatan'] == "1",
                _check[i]['edit_kabkot'] == "1",
                _check[i]['edit_provinsi'] == "1",
                _check[i]['edit_zip_code'] == "1",
                _check[i]['edit_address_from_map'] == "1",
                _check[i]['edit_phone1_area'] == "1",
                _check[i]['edit_phone1'] == "1",
              )
          );

          if(_check[i]['koresponden'] == "1"){
            selectedIndex = i;
            _controllerAddress.clear();
            setAddress(listPemegangSahamPribadiAddress[i]);
            setCorrespondence(i);
          }
        }
      }
    }
    notifyListeners();
  }

  void saveToSQLite(String type) async{
    List<MS2CustAddrModel> _listAddress = [];
    for(int i=0; i<_listPemegangSahamPribadiAddress.length; i++){
      _listAddress.add(MS2CustAddrModel(
        "123",
        _listPemegangSahamPribadiAddress[i].addressID,
        _listPemegangSahamPribadiAddress[i].foreignBusinessID,
        null,
        _listPemegangSahamPribadiAddress[i].isCorrespondence ? "1" : "0",
        type,
        _listPemegangSahamPribadiAddress[i].address,
        _listPemegangSahamPribadiAddress[i].rt,
        null,
        _listPemegangSahamPribadiAddress[i].rw,
        null,
        _listPemegangSahamPribadiAddress[i].kelurahanModel.PROV_ID,
        _listPemegangSahamPribadiAddress[i].kelurahanModel.PROV_NAME,
        _listPemegangSahamPribadiAddress[i].kelurahanModel.KABKOT_ID,
        _listPemegangSahamPribadiAddress[i].kelurahanModel.KABKOT_NAME,
        _listPemegangSahamPribadiAddress[i].kelurahanModel.KEC_ID,
        _listPemegangSahamPribadiAddress[i].kelurahanModel.KEC_NAME,
        _listPemegangSahamPribadiAddress[i].kelurahanModel.KEL_ID,
        _listPemegangSahamPribadiAddress[i].kelurahanModel.KEL_NAME,
        _listPemegangSahamPribadiAddress[i].kelurahanModel.ZIPCODE,
        null,
        _listPemegangSahamPribadiAddress[i].phone,
        _listPemegangSahamPribadiAddress[i].areaCode,
        null,
        null,
        null,
        null,
        _listPemegangSahamPribadiAddress[i].jenisAlamatModel.KODE,
        _listPemegangSahamPribadiAddress[i].jenisAlamatModel.DESKRIPSI,
        _listPemegangSahamPribadiAddress[i].addressLatLong != null ? _listPemegangSahamPribadiAddress[i].addressLatLong['latitude'].toString() : "",
        _listPemegangSahamPribadiAddress[i].addressLatLong != null ? _listPemegangSahamPribadiAddress[i].addressLatLong['longitude'].toString() : "",
        _listPemegangSahamPribadiAddress[i].addressLatLong != null ? _listPemegangSahamPribadiAddress[i].addressLatLong['address'].toString() : "",
        null,
        null,
        null,
        null,
        1,
        _listPemegangSahamPribadiAddress[i].isAddressTypeChanges ? "1" : "0",
        _listPemegangSahamPribadiAddress[i].isAlamatChanges ? "1" : "0",
        _listPemegangSahamPribadiAddress[i].isRTChanges ? "1" : "0",
        _listPemegangSahamPribadiAddress[i].isRWChanges ? "1" : "0",
        _listPemegangSahamPribadiAddress[i].isKelurahanChanges ? "1" : "0",
        _listPemegangSahamPribadiAddress[i].isKecamatanChanges ? "1" : "0",
        _listPemegangSahamPribadiAddress[i].isKotaChanges ? "1" : "0",
        _listPemegangSahamPribadiAddress[i].isProvinsiChanges ? "1" : "0",
        _listPemegangSahamPribadiAddress[i].isPostalCodeChanges ? "1" : "0",
        _listPemegangSahamPribadiAddress[i].isTeleponAreaChanges ? "1" : "0",
        _listPemegangSahamPribadiAddress[i].isTeleponChanges ? "1" : "0",
        null,
        null,
        null,
        null,
        _listPemegangSahamPribadiAddress[i].isAddressFromMapChanges ? "1" : "0",
      ));
    }
    _dbHelper.insertMS2CustAddr(_listAddress);
  }
}
