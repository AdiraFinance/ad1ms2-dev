import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/form_m_foto_model.dart';
import 'package:ad1ms2_dev/models/photo_type_model.dart';
import 'package:ad1ms2_dev/models/survey_photo_model.dart';
import 'package:ad1ms2_dev/shared/survey/result_survey_change_notifier.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ListSurveyPhotoChangeNotifier with ChangeNotifier {
  List<SurveyPhotoModel> _listSurveyPhoto = [];
  bool _isFlagSurveyPhoto = false;
  bool _autoValidate = false;
  String _custType;
  String _lastKnownState;
  bool _mandatoryFieldResultSurveyBerhasil = false;
  String _messageError = '';
  bool _checkDataSurveyPhoto = false;
  DbHelper _dbHelper = DbHelper();

  bool get checkDataSurveyPhoto => _checkDataSurveyPhoto;

  set checkDataSurveyPhoto(bool value) {
    this._checkDataSurveyPhoto = value;
    notifyListeners();
  }

  String get messageError => _messageError;

  set messageError(String value) {
    this._messageError = value;
  }

  bool get mandatoryFieldResultSurveyBerhasil => _mandatoryFieldResultSurveyBerhasil;

  set mandatoryFieldResultSurveyBerhasil(bool value) {
    this._mandatoryFieldResultSurveyBerhasil = value;
    notifyListeners();
  }

  List<SurveyPhotoModel> get listSurveyPhoto => _listSurveyPhoto;

  set listSurveyPhoto(List<SurveyPhotoModel> value) {
    this._listSurveyPhoto = value;
  }

  bool get isFlagSurveyPhoto => _isFlagSurveyPhoto;

  set isFlagSurveyPhoto(bool value) {
    this._isFlagSurveyPhoto = value;
  }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    _autoValidate = value;
    notifyListeners();
  }

  String get custType => _custType;

  set custType(String value) {
    this._custType = value;
    notifyListeners();
  }

  String get lastKnownState => _lastKnownState;

  set lastKnownState(String value) {
    this._lastKnownState = value;
    notifyListeners();
  }

  void addSurveyPhoto(SurveyPhotoModel value) {
    this._listSurveyPhoto.add(value);
    notifyListeners();
  }

  Future<void> setPreference(BuildContext context) async {
    SharedPreferences _preference = await SharedPreferences.getInstance();
    this._custType = _preference.getString("cust_type");
    this._lastKnownState = _preference.getString("last_known_state");
    var _providerResultSurvey = Provider.of<ResultSurveyChangeNotifier>(context, listen: false);
    this._mandatoryFieldResultSurveyBerhasil = _providerResultSurvey.mandatoryFieldResultSurveyBerhasil;
  }

  void deleteListSurveyPhoto(BuildContext context, int index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                primaryColor: Colors.black,
                primarySwatch: primaryOrange,
                accentColor: myPrimaryColor
            ),
            child: AlertDialog(
              title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Apakah kamu yakin menghapus foto ini?"),
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    this._listSurveyPhoto.removeAt(index);
                    notifyListeners();
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(color: Colors.grey)),
                ),
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Tidak'),
                ),
              ],
            ),
          );
        }
    );
  }

  void updateListSurveyPhoto(int index, SurveyPhotoModel value) {
    this._listSurveyPhoto[index] = value;
    notifyListeners();
  }

  Future<bool> onBackPress() async{
    if(this._mandatoryFieldResultSurveyBerhasil) {
      if(this._listSurveyPhoto.isNotEmpty){
        for(int i = 0; i < this._listSurveyPhoto.length; i++) {
          if(this._listSurveyPhoto[i].photoTypeModel.id == "SC4") {
            this._isFlagSurveyPhoto = true;
            this._checkDataSurveyPhoto = false;
            break;
          } else {
            this._isFlagSurveyPhoto = false;
            this._checkDataSurveyPhoto = true;
          }
        }
      }
      else{
        this._isFlagSurveyPhoto = false;
        this._checkDataSurveyPhoto = true;
      }
    }
    else {
      this._isFlagSurveyPhoto = true;
      this._checkDataSurveyPhoto = false;
    }
    if(this._checkDataSurveyPhoto) {
      this._messageError = "Foto Selfie wajib ditambahkan.";
    } else {
      this._messageError = "";
    }
    notifyListeners();
    return true;
  }

  void clearData(){
    this._autoValidate = false;
    this._isFlagSurveyPhoto = false;
    this._checkDataSurveyPhoto = false;
    this._listSurveyPhoto.clear();
  }

  Future<void> saveToSQLite(BuildContext context) async{
    if(await _dbHelper.deletePhotoSurvey()){
      await _dbHelper.insertMS2PhotoSurvey(_listSurveyPhoto);
    }
  }

  Future<void> setDataSQLite() async{
    var _check1 = await _dbHelper.selectPhotoSurvey();
    var _check2 = await _dbHelper.selectPhotoSurveyDetail();
    print("cek header foto survey $_check1");
    print("cek detail foto survey $_check2");
    //detail list
    if(_check1.isNotEmpty){
      for(int i=0; i<_check1.length; i++){
        var _photoTypeModel = PhotoTypeModel(_check1[i]['document_type'], _check1[i]['document_type_desc']);
        List<ImageFileModel> listImageGroupObjectUnit = [];
        for(int j=0; j<_check2.length; j++){
          if(_check1[i]['detail_id'] == _check2[j]['detail_id']){
            File _imageFile = File(_check2[j]['path_photo']);
            listImageGroupObjectUnit.add(ImageFileModel(
                _imageFile,
                _check2[j]['latitude'] != "null" && _check2[j]['latitude'] != "" ? double.parse(_check2[j]['latitude']) : null,
                _check2[j]['longitude'] != "null" && _check2[j]['longitude'] != "" ? double.parse(_check2[j]['longitude']) : null,
                _check2[j]['path_photo'],
                _check2[j]['file_header_id']
            ));
          }
        }
        _listSurveyPhoto.add(SurveyPhotoModel(
            _photoTypeModel, listImageGroupObjectUnit, false,
            false, false
        ));
      }
    }
    notifyListeners();
  }

}
