import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/birth_place_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_addr_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_shrhldr_ind_model.dart';
import 'package:ad1ms2_dev/models/pemegang_saham_model.dart';
import 'package:ad1ms2_dev/shared/add_pemegang_saham_pribadi_change_notif.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

import 'constants.dart';

class PemegangSahamPribadiChangeNotifier with ChangeNotifier {
  List<PemegangSahamPribadiModel> _listPemegangSahamPribadi = [];
  bool _autoValidate = false;
  bool _flag = false;
  DbHelper _dbHelper = DbHelper();

  List<PemegangSahamPribadiModel> get listPemegangSahamPribadi => _listPemegangSahamPribadi;

  void addPemegangSahamPribadi(PemegangSahamPribadiModel value) {
    this._listPemegangSahamPribadi.add(value);
    notifyListeners();
  }

  void deleteListPemegangSahamPribadi(BuildContext context, int index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                primaryColor: Colors.black,
                primarySwatch: primaryOrange,
                accentColor: myPrimaryColor
            ),
            child: AlertDialog(
              title: Text(
                  "Warning", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Apakah kamu yakin menghapus data ini?",),
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    this._listPemegangSahamPribadi.removeAt(index);
                    notifyListeners();
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                ),
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Tidak'),
                ),
              ],
            ),
          );
        }
    );
  }

  void updateListPemegangSahamPribadi(int index, PemegangSahamPribadiModel value) {
    this._listPemegangSahamPribadi[index] = value;
    notifyListeners();
  }

  void clearDataPemegangSahamPribadi() {
    this._autoValidate = false;
    this._flag = false;
    this._listPemegangSahamPribadi.clear();
  }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    _autoValidate = value;
    notifyListeners();
  }

  bool get flag => _flag;

  set flag(bool value) {
    _flag = value;
    notifyListeners();
  }

  Future<bool> onBackPress() async {
    autoValidate = false;
    flag = true;


    //ini kodingan asli edit by izha karena pengecekan dipindah di next
    // if (_listPemegangSahamPribadi.isNotEmpty) {
    //   autoValidate = false;
    //   flag = true;
    // } else {
    //   autoValidate = true;
    //   flag = false;
    // }
    return true;
  }

  void saveToSQLite() async {
    print("save sqlite saham pribadi");
    List<MS2CustShrhldrIndModel> _listData = [];
    for (int i = 0; i < _listPemegangSahamPribadi.length; i++) {
      _listData.add(MS2CustShrhldrIndModel(
        "123",
        this._listPemegangSahamPribadi[i].shareholdersIndividualID,
        this._listPemegangSahamPribadi[i].percentShare.isEmpty ? 0 : int.parse(this._listPemegangSahamPribadi[i].percentShare),
        0,
        this._listPemegangSahamPribadi[i].relationshipStatusSelected.PARA_FAMILY_TYPE_ID,
        this._listPemegangSahamPribadi[i].relationshipStatusSelected.PARA_FAMILY_TYPE_NAME,
        this._listPemegangSahamPribadi[i].typeIdentitySelected.id,
        this._listPemegangSahamPribadi[i].typeIdentitySelected.name,
        this._listPemegangSahamPribadi[i].identityNumber,
        this._listPemegangSahamPribadi[i].fullNameIdentity,
        this._listPemegangSahamPribadi[i].fullName,
        null,
        null,
        this._listPemegangSahamPribadi[i].birthOfDate.toString(),
        this._listPemegangSahamPribadi[i].placeOfBirthIdentity,
        this._listPemegangSahamPribadi[i].birthPlaceIdentityLOV != null ? this._listPemegangSahamPribadi[i].birthPlaceIdentityLOV.KABKOT_ID : null,
        this._listPemegangSahamPribadi[i].birthPlaceIdentityLOV != null ? this._listPemegangSahamPribadi[i].birthPlaceIdentityLOV.KABKOT_NAME : null,
        this._listPemegangSahamPribadi[i].gender,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        this._listPemegangSahamPribadi[i].isRelationshipStatusChange ? "1" : "0",
        this._listPemegangSahamPribadi[i].isIdentityTypeChange ? "1" : "0",
        this._listPemegangSahamPribadi[i].isIdentityNoChange ? "1" : "0",
        this._listPemegangSahamPribadi[i].isFullnameIDChange ? "1" : "0",
        this._listPemegangSahamPribadi[i].isFullnameChange ? "1" : "0",
        this._listPemegangSahamPribadi[i].isDateOfBirthChange ? "1" : "0",
        this._listPemegangSahamPribadi[i].isPlaceOfBirthChange ? "1" : "0",
        this._listPemegangSahamPribadi[i].isPlaceOfBirthLOVChange ? "1" : "0",
        this._listPemegangSahamPribadi[i].isGenderChange ? "1" : "0",
        this._listPemegangSahamPribadi[i].isShareChange ? "1" : "0",
      ));
    }
    _dbHelper.insertMS2CustShrhldrInd(_listData);

    //alamat
    List<MS2CustAddrModel> _listAddressSahamPribadi = [];
    for(int i=0; i<_listPemegangSahamPribadi.length; i++){
      for(int j=0; j<_listPemegangSahamPribadi[i].listAddress.length; j++){
        _listAddressSahamPribadi.add(MS2CustAddrModel(
          "123",
          _listPemegangSahamPribadi[i].listAddress[j].addressID,
          _listPemegangSahamPribadi[i].listAddress[j].foreignBusinessID,
          _listPemegangSahamPribadi[i].identityNumber,
          _listPemegangSahamPribadi[i].listAddress[j].isCorrespondence ? "1" : "0",
          "11",
          _listPemegangSahamPribadi[i].listAddress[j].address,
          _listPemegangSahamPribadi[i].listAddress[j].rt,
          null,
          _listPemegangSahamPribadi[i].listAddress[j].rw,
          null,
          _listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.PROV_ID,
          _listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.PROV_NAME,
          _listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.KABKOT_ID,
          _listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.KABKOT_NAME,
          _listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.KEC_ID,
          _listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.KEC_NAME,
          _listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.KEL_ID,
          _listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.KEL_NAME,
          _listPemegangSahamPribadi[i].listAddress[j].kelurahanModel.ZIPCODE,
          null,
          _listPemegangSahamPribadi[i].listAddress[j].phone,
          _listPemegangSahamPribadi[i].listAddress[j].areaCode,
          null,
          null,
          null,
          null,
          _listPemegangSahamPribadi[i].listAddress[j].jenisAlamatModel.KODE,
          _listPemegangSahamPribadi[i].listAddress[j].jenisAlamatModel.DESKRIPSI,
          _listPemegangSahamPribadi[i].listAddress[j].addressLatLong != null ? _listPemegangSahamPribadi[i].listAddress[j].addressLatLong['latitude'].toString() : null,
          _listPemegangSahamPribadi[i].listAddress[j].addressLatLong != null ? _listPemegangSahamPribadi[i].listAddress[j].addressLatLong['longitude'].toString() : null,
          _listPemegangSahamPribadi[i].listAddress[j].addressLatLong != null ? _listPemegangSahamPribadi[i].listAddress[j].addressLatLong['address'].toString() : null,
          null,
          null,
          null,
          null,
          _listPemegangSahamPribadi[i].listAddress[j].active,
          _listPemegangSahamPribadi[i].listAddress[j].isAddressTypeChanges ? "1" : "0",
          _listPemegangSahamPribadi[i].listAddress[j].isAlamatChanges ? "1" : "0",
          _listPemegangSahamPribadi[i].listAddress[j].isRTChanges ? "1" : "0",
          _listPemegangSahamPribadi[i].listAddress[j].isRWChanges ? "1" : "0",
          _listPemegangSahamPribadi[i].listAddress[j].isKelurahanChanges ? "1" : "0",
          _listPemegangSahamPribadi[i].listAddress[j].isKecamatanChanges ? "1" : "0",
          _listPemegangSahamPribadi[i].listAddress[j].isKotaChanges ? "1" : "0",
          _listPemegangSahamPribadi[i].listAddress[j].isProvinsiChanges ? "1" : "0",
          _listPemegangSahamPribadi[i].listAddress[j].isPostalCodeChanges ? "1" : "0",
          _listPemegangSahamPribadi[i].listAddress[j].isTeleponAreaChanges ? "1" : "0",
          _listPemegangSahamPribadi[i].listAddress[j].isTeleponChanges ? "1" : "0",
          null,
          null,
          null,
          null,
          _listPemegangSahamPribadi[i].listAddress[j].isAddressFromMapChanges ? "1" : "0",
        ));
      }
    }
    _dbHelper.insertMS2CustAddr(_listAddressSahamPribadi);
  }

  Future<bool> deleteSQLite() async {
    if(await _dbHelper.deleteMS2CustShrhldrInd() && await _dbHelper.deleteMS2CustAddr("11")){
      return true;
    }
    else{
      return false;
    }
  }

  Future<void> setDataFromSQLite() async {
    List _dataCustomerCompanyShrhldrInd = await _dbHelper.selectMS2CustShrhldrInd();
    List _addressIndividu = await _dbHelper.selectMS2CustAddr("11");
    List<AddressModel> _listAddress = [];

    if(_addressIndividu.isNotEmpty){
      for(int i=0; i<_addressIndividu.length; i++){
        var jenisAlamatModel = JenisAlamatModel(_addressIndividu[i]['addr_type'], _addressIndividu[i]['addr_desc']);
        var kelurahanModel = KelurahanModel(
            _addressIndividu[i]['kelurahan'],
            _addressIndividu[i]['kelurahan_desc'],
            _addressIndividu[i]['kecamatan_desc'],
            _addressIndividu[i]['kabkot_desc'],
            _addressIndividu[i]['provinsi_desc'],
            _addressIndividu[i]['zip_code'],
            _addressIndividu[i]['kecamatan'],
            _addressIndividu[i]['kabkot'],
            _addressIndividu[i]['provinsi']);
        var _addressFromMap = {
          "address": _addressIndividu[i]['address_from_map'] != "" && _addressIndividu[i]['address_from_map'] != "null" ? _addressIndividu[i]['address_from_map'] : '',
          "latitude": _addressIndividu[i]['latitude'],
          "longitude": _addressIndividu[i]['longitude']
        };
        var _koresponden = _addressIndividu[i]['koresponden'].toString().toLowerCase();
        _listAddress.add(AddressModel(
          jenisAlamatModel,
          _addressIndividu[i]['addressID'],
          _addressIndividu[i]['foreignBusinessID'],
          _addressIndividu[i]['id_no'],
          kelurahanModel,
          _addressIndividu[i]['address'] != "" && _addressIndividu[i]['address'] != "null" ? _addressIndividu[i]['address'] : '',
          _addressIndividu[i]['rt'] != "" && _addressIndividu[i]['rt'] != "null" ? _addressIndividu[i]['rt'] : '',
          _addressIndividu[i]['rw'] != "" && _addressIndividu[i]['rw'] != "null" ? _addressIndividu[i]['rw'] : '',
          _addressIndividu[i]['phone1_area'] != "" && _addressIndividu[i]['phone1_area'] != "null" ? _addressIndividu[i]['phone1_area'] : '',
          _addressIndividu[i]['phone1'] != "" && _addressIndividu[i]['phone1'] != "null" ? _addressIndividu[i]['phone1'] : '',
          false,
          _addressFromMap,
          _koresponden == '1' ? true : false,
          _addressIndividu[i]['active'],
          _addressIndividu[i]['edit_address'] == "1" ||
              _addressIndividu[i]['edit_addr_type'] == "1" ||
              _addressIndividu[i]['edit_rt'] == "1" ||
              _addressIndividu[i]['edit_rw'] == "1" ||
              _addressIndividu[i]['edit_kelurahan'] == "1" ||
              _addressIndividu[i]['edit_kecamatan'] == "1" ||
              _addressIndividu[i]['edit_kabkot'] == "1" ||
              _addressIndividu[i]['edit_provinsi'] == "1" ||
              _addressIndividu[i]['edit_zip_code'] == "1" ||
              _addressIndividu[i]['edit_address_from_map'] == "1" ||
              _addressIndividu[i]['edit_phone1_area'] == "1" ||
              _addressIndividu[i]['edit_phone1'] == "1" ? true : false,
          _addressIndividu[i]['edit_address'] == "1",
          _addressIndividu[i]['edit_addr_type'] == "1",
          _addressIndividu[i]['edit_rt'] == "1",
          _addressIndividu[i]['edit_rw'] == "1",
          _addressIndividu[i]['edit_kelurahan'] == "1",
          _addressIndividu[i]['edit_kecamatan'] == "1",
          _addressIndividu[i]['edit_kabkot'] == "1",
          _addressIndividu[i]['edit_provinsi'] == "1",
          _addressIndividu[i]['edit_zip_code'] == "1",
          _addressIndividu[i]['edit_address_from_map'] == "1",
          _addressIndividu[i]['edit_phone1_area'] == "1",
          _addressIndividu[i]['edit_phone1'] == "1",
        ));
      }
    }
    if(_dataCustomerCompanyShrhldrInd.isNotEmpty){
      for (int i = 0; i < _dataCustomerCompanyShrhldrInd.length; i++) {
        List<AddressModel> _listAddressShareholder = [];
        for(int j=0; j<_listAddress.length; j++){
          if(_dataCustomerCompanyShrhldrInd[i]['id_no'] == _listAddress[j].numberID){
            _listAddressShareholder.add(_listAddress[j]);
          }
        }
        this._listPemegangSahamPribadi.add(PemegangSahamPribadiModel(
          RelationshipStatusModel(_dataCustomerCompanyShrhldrInd[i]['relation_status'], _dataCustomerCompanyShrhldrInd[i]['relation_status_desc']),
          IdentityModel(_dataCustomerCompanyShrhldrInd[i]['id_type'], _dataCustomerCompanyShrhldrInd[i]['id_type_desc']),
          _dataCustomerCompanyShrhldrInd[i]['id_no'],
          _dataCustomerCompanyShrhldrInd[i]['full_name_id'],
          _dataCustomerCompanyShrhldrInd[i]['full_name'],
          _dataCustomerCompanyShrhldrInd[i]['date_of_birth'],
          _dataCustomerCompanyShrhldrInd[i]['place_of_birth'],
          BirthPlaceModel(_dataCustomerCompanyShrhldrInd[i]['place_of_birth_kabkota'], _dataCustomerCompanyShrhldrInd[i]['place_of_birth_kabkota_desc']),
          _dataCustomerCompanyShrhldrInd[i]['gender'],
          _dataCustomerCompanyShrhldrInd[i]['shrhldng_percent'].toString(),
          _listAddressShareholder, //alamat
          _dataCustomerCompanyShrhldrInd[i]['edit_relation_status'] == "1" || _dataCustomerCompanyShrhldrInd[i]['edit_id_type'] == "1" ||
          _dataCustomerCompanyShrhldrInd[i]['edit_id_no'] == "1" || _dataCustomerCompanyShrhldrInd[i]['edit_full_name_id'] == "1" ||
          _dataCustomerCompanyShrhldrInd[i]['edit_full_name'] == "1" || _dataCustomerCompanyShrhldrInd[i]['edit_date_of_birth'] == "1" ||
          _dataCustomerCompanyShrhldrInd[i]['edit_place_of_birth'] == "1" || _dataCustomerCompanyShrhldrInd[i]['edit_place_of_birth_kabkota'] == "1" ||
          _dataCustomerCompanyShrhldrInd[i]['edit_gender'] == "1" || _dataCustomerCompanyShrhldrInd[i]['edit_shrhldng_percent'] == "1",
          _dataCustomerCompanyShrhldrInd[i]['edit_relation_status'] == "1",
          _dataCustomerCompanyShrhldrInd[i]['edit_id_type'] == "1",
          _dataCustomerCompanyShrhldrInd[i]['edit_id_no'] == "1",
          _dataCustomerCompanyShrhldrInd[i]['edit_full_name_id'] == "1",
          _dataCustomerCompanyShrhldrInd[i]['edit_full_name'] == "1",
          _dataCustomerCompanyShrhldrInd[i]['edit_date_of_birth'] == "1",
          _dataCustomerCompanyShrhldrInd[i]['edit_place_of_birth'] == "1",
          _dataCustomerCompanyShrhldrInd[i]['edit_place_of_birth_kabkota'] == "1",
          _dataCustomerCompanyShrhldrInd[i]['edit_gender'] == "1",
          _dataCustomerCompanyShrhldrInd[i]['edit_shrhldng_percent'] == "1",
            _dataCustomerCompanyShrhldrInd[i]['shareholdersIndividualID']
        ));
      }
    }
    notifyListeners();
  }

}
