import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/screens/list_oid.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/resource/get_set_vme.dart';
import 'package:ad1ms2_dev/shared/resource/validate_data.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class DedupCompanyChangeNotifier with ChangeNotifier{
  TextEditingController _controllerTglPendirian = TextEditingController();
  TextEditingController _controllerNPWPNumber = TextEditingController();
  TextEditingController _controllerName = TextEditingController();
  TextEditingController _controllerAddress = TextEditingController();
  bool _autoValidate = false;
  DateTime  _initialDate = DateTime(dateNow.year, dateNow.month, dateNow.day);
  final _key = new GlobalKey<FormState>();
  ValidateData _validateData = ValidateData();
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _isEstablishDateVisible = false;
  bool _isNoNPWPVisible = false;
  bool _isNameVisible = false;
  bool _isAddressVisible = false;
  bool _isEstablishDateMandatory = false;
  bool _isNoNPWPMandatory = false;
  bool _isNameMandatory = false;
  bool _isAddressMandatory = false;

  TextEditingController get controllerTglPendirian => _controllerTglPendirian;

  TextEditingController get controllerNPWPNumber => _controllerNPWPNumber;

  TextEditingController get controllerName => _controllerName;

  TextEditingController get controllerAddress => _controllerAddress;

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  DateTime get initialDate => _initialDate;

  GlobalKey get key => _key;

  bool get isAddressMandatory => _isAddressMandatory;

  set isAddressMandatory(bool value) {
    this._isAddressMandatory = value;
  }

  bool get isNameMandatory => _isNameMandatory;

  set isNameMandatory(bool value) {
    this._isNameMandatory = value;
  }

  bool get isNoNPWPMandatory => _isNoNPWPMandatory;

  set isNoNPWPMandatory(bool value) {
    this._isNoNPWPMandatory = value;
  }

  bool get isEstablishDateMandatory => _isEstablishDateMandatory;

  set isEstablishDateMandatory(bool value) {
    this._isEstablishDateMandatory = value;
  }

  bool get isAddressVisible => _isAddressVisible;

  set isAddressVisible(bool value) {
    this._isAddressVisible = value;
  }

  bool get isNameVisible => _isNameVisible;

  set isNameVisible(bool value) {
    this._isNameVisible = value;
  }

  bool get isNoNPWPVisible => _isNoNPWPVisible;

  set isNoNPWPVisible(bool value) {
    this._isNoNPWPVisible = value;
  }

  bool get isEstablishDateVisible => _isEstablishDateVisible;

  set isEstablishDateVisible(bool value) {
    this._isEstablishDateVisible = value;
  }

  check(BuildContext context) {
    final _form = _key.currentState;
    if (_form.validate()) {
      validateNameOrNPWP(context, "NAME_AND_NPWP");
    }
    else {
      _autoValidate = true;
    }
  }

  showDatePicker(BuildContext context) async {
    // DatePickerShared _datePicker = DatePickerShared();
    // var _dateSelected = await _datePicker.selectStartDate(context, _initialDate,
    //     canAccessNextDay: false);
    // if (_dateSelected != null) {
    //   setState(() {
    //     _initialDate = _dateSelected;
    //     _controllerTglPendirian.text = dateFormat.format(_dateSelected);
    //   });
    // } else {
    //   return;
    // }
    var _dateSelected = await selectDateLastToday(context, _initialDate);
    if (_dateSelected != null) {
      _initialDate = _dateSelected;
      _controllerTglPendirian.text = dateFormat.format(_dateSelected);
    }
    else {
      return;
    }
  }

  Future<void> setValueMandatoryVisible() async{
    this._autoValidate = false;
    _controllerTglPendirian.clear();
    _controllerNPWPNumber.clear();
    _controllerName.clear();
    _controllerAddress.clear();
    try{
      List _dataMandatory = await getMandatoryDedupComp();
      print(_dataMandatory[0]);
      List _dataVisible = await getVisibleDedupComp();
      print(_dataVisible[0]);
      isNameMandatory = _dataMandatory[0]['namaLembaga'] == "1";
      isNoNPWPMandatory = _dataMandatory[0]['npwp'] == "1";
      isAddressMandatory = _dataMandatory[0]['alamatNpwp'] == "1";
      isEstablishDateMandatory = _dataMandatory[0]['tglPendirian'] == "1";
      isNameVisible = _dataMandatory[0]['namaLembaga'] == "1";
      isNoNPWPVisible = _dataMandatory[0]['npwp'] == "1";
      isAddressVisible = _dataMandatory[0]['alamatNpwp'] == "1";
      isEstablishDateVisible = _dataMandatory[0]['tglPendirian'] == "1";
    }
    catch(e){
      print("${e.toString()}");
    }
    notifyListeners();
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  bool get loadData => _loadData;

  set loadData(bool value) {
    _loadData = value;
    notifyListeners();
  }

  validateNameOrNPWP(BuildContext context,String typeValidate) async{
    try{
      if(typeValidate == "NAME"){
        print("NAME");
        if(this._controllerName.text != ""){
          if(await _validateData.validateName(this._controllerName.text,"COM") != "SUCCESS"){
            _showSnackBar(await _validateData.validateName(this._controllerName.text,"COM"));
          }
        }
      }
      else if(typeValidate == "NPWP"){
        print("NPWP");
        if(this._controllerNPWPNumber.text != ""){
          if(await _validateData.validateNPWP(this._controllerNPWPNumber.text) != "SUCCESS"){
            _showSnackBar("${await _validateData.validateNPWP(this._controllerNPWPNumber.text)}");
          }
        }
      }
      else{
        print("ELSE");
        loadData = true;
        if(await _validateData.validateName(this._controllerName.text,"COM") != "SUCCESS"){
          _showSnackBar(await _validateData.validateName(this._controllerName.text,"COM"));
        }
        else if(await _validateData.validateNPWP(this._controllerNPWPNumber.text) != "SUCCESS"){
          _showSnackBar("${await _validateData.validateNPWP(this._controllerNPWPNumber.text)}");
        }
        else{
          Navigator.push(
              context, MaterialPageRoute(
              builder: (context) => ListOid(
                flag: "COM",
                initialDateBirthDate: _initialDate,
                fullname: _controllerName.text,
                identityNumber: _controllerNPWPNumber.text,
                identityAddress: _controllerAddress.text,
                birthDate: _controllerTglPendirian.text,
              )
          ));
        }
        loadData = false;
      }
    }
    catch(e){
      _showSnackBar(e);
    }
  }

  void _showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating,backgroundColor: snackbarColor, duration: Duration(seconds: 4)));
  }
}