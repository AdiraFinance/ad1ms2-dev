import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/screens/app/menu_customer_detail.dart';
import 'package:ad1ms2_dev/screens/dashboard.dart';
import 'package:ad1ms2_dev/screens/form_m/menu_detail_loan.dart';
import 'package:ad1ms2_dev/screens/form_m/menu_object_information.dart';
import 'package:ad1ms2_dev/screens/survey/result_survey.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/form_m_add_additional_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/form_m_add_major_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_additional_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_app_document_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_application_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_income_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_type_installment_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_subsidy_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_major_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_object_karoseri_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_collatelar_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_salesman_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/marketing_notes_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/taksasi_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_company_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_manajemen_pic_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_rincian_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_guarantor_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_income_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_info_keluarga_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_keluarga(ibu)_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_occupation_change_notif.dart';
import 'package:ad1ms2_dev/shared/resource/get_list_meta_data.dart';
import 'package:ad1ms2_dev/shared/resource/old_applicationdto_provider.dart';
import 'package:ad1ms2_dev/shared/resource/submit_data_partial.dart';
import 'package:ad1ms2_dev/shared/survey/list_survey_photo_change_notifier.dart';
import 'package:ad1ms2_dev/shared/survey/result_survey_change_notifier.dart';
import 'package:dio/adapter.dart';
import 'package:dio/dio.dart' as dio;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:http_parser/http_parser.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../../main.dart';
import '../info_wmp_change_notifier.dart';

class FormMParentIndividualChangeNotifier with ChangeNotifier {
  List<GlobalKey<FormState>> formKeys = [
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>()
  ];

  var storage = FlutterSecureStorage();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  DbHelper _dbHelper = DbHelper();
  SubmitDataPartial _submitDataPartial = SubmitDataPartial();
  var _orderSupportingDocuments = [];
  List _customerIncomes = [];
  int _lastStepIndex = 0;
  int _selectedIndex = 0;
  bool _isCreditLimitDone = false;
  bool _isPhotoDone = false;
  bool _isMenuCustomerDetailDone = false;
//  bool _isCustomerInfoDone = false;
//  bool _isAddressInfoDone = false;
//  bool _isFamilyInformationMotherDone = false;
//  bool _isFamilyInformationDone = false;
  bool _isOccupationDone = false;
  bool _isIncomeDone = false;
  bool _isGuarantorDone = false;
  bool _isInfoAppDone = false;

  bool _isMenuObjectInformationDone = false;

//  bool _isUnitObjectInfoDone = false;
//  bool _isSalesmanInfoDone = false;
//  bool _isCollateralInfoDone = false;

  bool _isInfoObjKaroseriDone = false;
  bool _isMenuDetailLoanDone = false;

//  bool _isCreditStructureDone = false;
//  bool _isCreditStructureTypeInstallmentDone = false;
//  bool _isMajorInsuranceDone = false;
//  bool _isAdditionalInsuranceDone = false;
//  bool _isInfoWMPDone = false;

//  bool _isCreditIncomeDone = false;
  bool _isCreditSubsidyDone = false;
  bool _isDocumentInfoDone = false;
  bool _isTaksasiUnitDone = false;
  bool _isMarketingNotesDone = false;
  String _urlUploadFile;
  // List<String> _listObjectIdTempatTinggal = [];
  // List<String> _listObjectIdTempatUsaha = [];
  // List<String> _listObjectIdGroupUnitObject = [];
  // List<String> _listObjectIdDocumentUnitObject = [];
  // List<String> _listObjectIdInfoDocument = [];
  // List<String> _listObjectIdPhotoSurvey = [];

  bool _loadData = false;

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
    notifyListeners();
  }

  String _messageProcess;

  String get messageProcess => _messageProcess;

  set messageProcess(String value) {
    this._messageProcess = value;
  }

  List get customerIncomes => _customerIncomes;

  get orderSupportingDocuments => _orderSupportingDocuments;

  set orderSupportingDocuments(value) {
    this._orderSupportingDocuments = value;
    notifyListeners();
  }

  int get selectedIndex => _selectedIndex;

  set selectedIndex(int value) {
    this._selectedIndex = value;
    notifyListeners();
  }

  bool get isCreditLimitDone => _isCreditLimitDone;

  set isCreditLimitDone(bool value) {
    _isCreditLimitDone = value;
    notifyListeners();
  }

  bool get isPhotoDone => _isPhotoDone;

  set isPhotoDone(bool value) {
    this._isPhotoDone = value;
    notifyListeners();
  }

//  bool get isCustomerInfoDone => _isCustomerInfoDone;
//
//  set isCustomerInfoDone(bool value) {
//    this._isCustomerInfoDone = value;
//    notifyListeners();
//  }
//
//  bool get isAddressInfoDone => _isAddressInfoDone;
//
//  set isAddressInfoDone(bool value) {
//    this._isAddressInfoDone = value;
//    notifyListeners();
//  }
//
//  bool get isFamilyInformationMotherDone => _isFamilyInformationMotherDone;
//
//  set isFamilyInformationMotherDone(bool value) {
//    this._isFamilyInformationMotherDone = value;
//    notifyListeners();
//  }
//
//  bool get isFamilyInformationDone => _isFamilyInformationDone;
//
//  set isFamilyInformationDone(bool value) {
//    this._isFamilyInformationDone = value;
//    notifyListeners();
//  }

  bool get isOccupationDone => _isOccupationDone;

  set isOccupationDone(bool value) {
    this._isOccupationDone = value;
    notifyListeners();
  }

  bool get isIncomeDone => _isIncomeDone;

  set isIncomeDone(bool value) {
    this._isIncomeDone = value;
    notifyListeners();
  }

  bool get isGuarantorDone => _isGuarantorDone;

  set isGuarantorDone(bool value) {
    this._isGuarantorDone = value;
    notifyListeners();
  }

  bool get isInfoAppDone => _isInfoAppDone;

  set isInfoAppDone(bool value) {
    this._isInfoAppDone = value;
    notifyListeners();
  }

  bool get isMenuObjectInformationDone => _isMenuObjectInformationDone;

  set isMenuObjectInformationDone(bool value) {
    this._isMenuObjectInformationDone = value;
    notifyListeners();
  }

//  bool get isUnitObjectInfoDone => _isUnitObjectInfoDone;
//
//  set isUnitObjectInfoDone(bool value) {
//    this._isUnitObjectInfoDone = value;
//    notifyListeners();
//  }
//
//
//  bool get isSalesmanInfoDone => _isSalesmanInfoDone;
//
//  set isSalesmanInfoDone(bool value) {
//    this._isSalesmanInfoDone = value;
//    notifyListeners();
//  }
//
//
//  bool get isCollateralInfoDone => _isCollateralInfoDone;
//
//  set isCollateralInfoDone(bool value) {
//    this._isCollateralInfoDone = value;
//    notifyListeners();
//  }

  bool get isInfoObjKaroseriDone => _isInfoObjKaroseriDone;

  set isInfoObjKaroseriDone(bool value) {
    this._isInfoObjKaroseriDone = value;
    notifyListeners();
  }

  bool get isMenuDetailLoanDone => _isMenuDetailLoanDone;

  set isMenuDetailLoanDone(bool value) {
    _isMenuDetailLoanDone = value;
    notifyListeners();
  }

//  bool get isMajorInsuranceDone => _isMajorInsuranceDone;
//
//  set isMajorInsuranceDone(bool value) {
//    this._isMajorInsuranceDone = value;
//    notifyListeners();
//  }
//
//  bool get isAdditionalInsuranceDone => _isAdditionalInsuranceDone;
//
//  set isAdditionalInsuranceDone(bool value) {
//    this._isAdditionalInsuranceDone = value;
//    notifyListeners();
//  }
//
//  bool get isInfoWMPDone => _isInfoWMPDone;
//
//  set isInfoWMPDone(bool value) {
//    this._isInfoWMPDone = value;
//    notifyListeners();
//  }
//
//  bool get isCreditStructureDone => _isCreditStructureDone;
//
//  set isCreditStructureDone(bool value) {
//    this._isCreditStructureDone = value;
//    notifyListeners();
//  }
//
//  bool get isCreditStructureTypeInstallmentDone => _isCreditStructureTypeInstallmentDone;
//
//  set isCreditStructureTypeInstallmentDone(bool value) {
//    this._isCreditStructureTypeInstallmentDone = value;
//    notifyListeners();
//  }

//  bool get isCreditIncomeDone => _isCreditIncomeDone;
//
//  set isCreditIncomeDone(bool value) {
//    this._isCreditIncomeDone = value;
//    notifyListeners();
//  }

  bool get isDocumentInfoDone => _isDocumentInfoDone;

  set isDocumentInfoDone(bool value) {
    this._isDocumentInfoDone = value;
    notifyListeners();
  }

  bool get isCreditSubsidyDone => _isCreditSubsidyDone;

  set isCreditSubsidyDone(bool value) {
    this._isCreditSubsidyDone = value;
    notifyListeners();
  }

  bool get isTaksasiUnitDone => _isTaksasiUnitDone;

  set isTaksasiUnitDone(bool value) {
    this._isTaksasiUnitDone = value;
    notifyListeners();
  }

  bool get isMenuCustomerDetailDone => _isMenuCustomerDetailDone;

  set isMenuCustomerDetailDone(bool value) {
    this._isMenuCustomerDetailDone = value;
    notifyListeners();
  }

  bool get isMarketingNotesDone => _isMarketingNotesDone;

  set isMarketingNotesDone(bool value) {
    this._isMarketingNotesDone = value;
    notifyListeners();
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void checkBtnNext(BuildContext context) async{
    messageProcess = "";
    final _form = formKeys[this._selectedIndex].currentState;
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    List _dataCL = await _dbHelper.selectMS2LME();
    var _providerFormMFotoChangeNotif = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
    var _providerFormMCustomerInfoChangeNotifier = Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false);
    var _providerOccupationChangeNotif = Provider.of<FormMOccupationChangeNotif>(context, listen: false);
    var _providerInfoAlamat = Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false);
    var _providerInfoKeluargaIbu = Provider.of<FormMInformasiKeluargaIBUChangeNotif>(context, listen: false);
    var _providerInfoKeluarga = Provider.of<FormMInfoKeluargaChangeNotif>(context, listen: false);
    var _providerInfoApp = Provider.of<InfoAppChangeNotifier>(context, listen: false);
    var _providerIncomeChangeNotif = Provider.of<FormMIncomeChangeNotifier>(context, listen: false);
    var _providerKaroseri = Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false);
    var _providerCreditSubsidy = Provider.of<InfoCreditSubsidyChangeNotifier>(context, listen: false);
    var _providerInfoDocument = Provider.of<InfoDocumentChangeNotifier>(context, listen: false);

    var _providerInfoObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
    var _providerInfoSales = Provider.of<InformationSalesmanChangeNotifier>(context, listen: false);
    var _providerKolateral = Provider.of<InformationCollateralChangeNotifier>(context, listen: false);
    var _providerWMP = Provider.of<InfoWMPChangeNotifier>(context, listen: false);
    var _providerCreditIncome = Provider.of<InfoCreditIncomeChangeNotifier>(context, listen: false);

    var _providerMajorInsurance = Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false);
    var _providerAddMajorInsurance = Provider.of<FormMAddMajorInsuranceChangeNotifier>(context, listen: false);
    var _providerAdditionalInsurance = Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false);
    var _providerAddAdditionalInsurance = Provider.of<FormMAddAdditionalInsuranceChangeNotifier>(context, listen: false);

    var _providerInfoCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false);
    var _providerInfoCreditStructureTypeInstallment = Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context, listen: false);
    var _providerTaksasiUnit = Provider.of<TaksasiUnitChangeNotifier>(context, listen: false);
    var _providerMarketingNotes = Provider.of<MarketingNotesChangeNotifier>(context,listen: false);
    var _providerGuarantor = Provider.of<FormMGuarantorChangeNotifier>(context, listen: false);

//   selectedIndex += 1;

    // if (this._selectedIndex == 0) {
    //   if(_form.validate()){
    //     selectedIndex += 1;
    //     isCreditLimitDone = true;
    //   }
    //   else{
    //     Provider.of<FormMCreditLimitChangeNotifier>(context,listen: false).autoValidate = true;
    //   }
    // }
    if(this._selectedIndex == 0){
      await _providerFormMFotoChangeNotif.checkDataChanges(); //untuk DKR
      if (_providerFormMFotoChangeNotif.listFotoTempatTinggal.isEmpty && _providerFormMFotoChangeNotif.isFotoTempatTinggalMandatory()) {
        _providerFormMFotoChangeNotif.autoValidateFotoTempatTinggal = true;
      }
      // if (_providerFormMFotoChangeNotif.listGroupUnitObject.isEmpty) {
      //   _providerFormMFotoChangeNotif.autoValidateGroupObjectUnit = true;
      // }
      if (!_form.validate()) {
        _providerFormMFotoChangeNotif.autoValidate = true;
      }
      // if (_providerFormMFotoChangeNotif.listDocument.isEmpty) {
      //   _providerFormMFotoChangeNotif.autoValidateDocumentObjectUnit = true;
      // }
      if (_providerFormMFotoChangeNotif.occupationSelected != null) {
        if (_providerFormMFotoChangeNotif.occupationSelected.KODE == "05" ||
            _providerFormMFotoChangeNotif.occupationSelected.KODE == "06" ||
            _providerFormMFotoChangeNotif.occupationSelected.KODE == "07") {
          if (_providerFormMFotoChangeNotif.listFotoTempatUsaha.isEmpty && _providerFormMFotoChangeNotif.isFotoTempatUsahaMandatory()) {
            _providerFormMFotoChangeNotif.autoValidateTempatUsaha = true;
          }
        }
      }

      if (_form.validate() && !_providerFormMFotoChangeNotif.autoValidateFotoTempatTinggal
          // &&
          // _providerFormMFotoChangeNotif.listFotoTempatTinggal.isNotEmpty
          // &&
          // _providerFormMFotoChangeNotif.listGroupUnitObject.isNotEmpty
          // && _providerFormMFotoChangeNotif.listDocument.isNotEmpty
      ) {
        if (_providerFormMFotoChangeNotif.occupationSelected.KODE == "05" ||
            _providerFormMFotoChangeNotif.occupationSelected.KODE == "06" ||
            _providerFormMFotoChangeNotif.occupationSelected.KODE == "07") {
          if (!_providerFormMFotoChangeNotif.autoValidateTempatUsaha) {
            if(await _providerFormMFotoChangeNotif.deleteSQLite()){
              _providerFormMFotoChangeNotif.saveToSQLite(context);
            }
            await _setLastStep(this._selectedIndex);
            selectedIndex += 1;
            isPhotoDone = true;
          }
        }
        else {
          if(await _providerFormMFotoChangeNotif.deleteSQLite()){
            await _providerFormMFotoChangeNotif.saveToSQLite(context);
          }
          await _setLastStep(this._selectedIndex);
          selectedIndex += 1;
          isPhotoDone = true;
        }
        // if(_preferences.getString("last_known_state") != "DKR"){
          try {
            loadData = true;
            await _submitDataPartial.submitOccupationPersonal(context, 0);
            await _submitDataPartial.submitApplication(context, 0);
            await _submitDataPartial.submitColla(context, 0);
            loadData = false;
          }
          catch(e){
            loadData = false;
            dialogFailedSavePartial(context, e.toString());
          }
        // }
      }
    }
    else if (this._selectedIndex == 1) {
      if(!_providerFormMCustomerInfoChangeNotifier.flag){
        _providerFormMCustomerInfoChangeNotifier.autoValidate = true;
      }
      if(!_providerInfoAlamat.flag){
        _providerInfoAlamat.autoValidate = true;
      }
      if(!_providerInfoKeluargaIbu.flag){
        _providerInfoKeluargaIbu.autoValidate = true;
      }
      // if(_providerFormMCustomerInfoChangeNotifier.maritalStatusSelected.id == "02") {
      //   if(_providerInfoKeluarga.listFormInfoKel.length == 0){
      //     if(!_providerInfoKeluarga.flag){
      //       _providerInfoKeluarga.autoValidate = true;
      //     }
      //   }
      // }

      // if(_providerFormMCustomerInfoChangeNotifier.maritalStatusSelected.id == "02") {
      //   print('masuk sini cuy');
      //   if(_providerInfoKeluarga.listFormInfoKel.length != 0){
      //     print('keluarga udah ada isi');
      //     if(_providerFormMCustomerInfoChangeNotifier.flag && _providerInfoAlamat.flag && _providerInfoKeluargaIbu.flag && _providerInfoKeluarga.listFormInfoKel.length != 0){
      //       print('pake keluarga');
      //       if(await _providerFormMCustomerInfoChangeNotifier.deleteSQLite() && await _providerInfoKeluargaIbu.deleteSQLite() && await _providerInfoAlamat.deleteSQLite("5")){
      //         _providerFormMCustomerInfoChangeNotifier.saveToSQLite();
      //         _providerInfoKeluargaIbu.saveToSQLite();
      //         _providerInfoKeluarga.saveToSQLite();
      //         _providerInfoAlamat.saveToSQLite("5");
      //       }
      //       await _setLastStep(this._selectedIndex);
      //       selectedIndex += 1;
      //       isMenuCustomerDetailDone = true;
      //     }
      //   }
      // }
      // else {
      if(_providerFormMCustomerInfoChangeNotifier.flag && _providerInfoAlamat.flag && _providerInfoKeluargaIbu.flag){
        await _providerFormMCustomerInfoChangeNotifier.checkDataChanges(0);
        if(await _providerFormMCustomerInfoChangeNotifier.deleteSQLite() && await _providerInfoKeluargaIbu.deleteSQLite()
            && await _providerInfoAlamat.deleteSQLite("5")){
          _providerInfoKeluarga.isCheckData = true;
          _providerFormMCustomerInfoChangeNotifier.saveToSQLite();
          _providerInfoKeluargaIbu.saveToSQLite();
          _providerInfoKeluarga.saveToSQLite();
          _providerInfoAlamat.saveToSQLite("5");
        }
        await _setLastStep(this._selectedIndex);
        selectedIndex += 1;
        isMenuCustomerDetailDone = true;
        // if(_preferences.getString("last_known_state") != "DKR"){
          try{
            loadData = true;
            await _submitDataPartial.submitCustomerIndividu(context,1);
            if(_preferences.getString("order_date") == null){
              _preferences.setString("order_date", DateTime.now().toString());
            }
            debugPrint("CEK_TANGGAL ORDER_DATE PARENT INDIVIDU ${ _preferences.getString("order_date")}");
            loadData = false;
          }
          catch(e){
            loadData = false;
            dialogFailedSavePartial(context, e.toString());
          }
        // }
      }
      // }
    }
    else if (this._selectedIndex == 2) {
      if (_form.validate()) {
        if(int.parse(_providerOccupationChangeNotif.controllerTotalEstablishedDate.text) < int.parse(_providerOccupationChangeNotif.controllerEstablishedDate.text)) {
          _providerOccupationChangeNotif.controllerEstablishedDate.clear();
          _showSnackBar("Lama Usaha tidak boleh lebih besar dari Total Lama Usaha");
        }
        else{
          await _providerOccupationChangeNotif.checkDataDakor();
          await _setLastStep(this._selectedIndex);
          if(await _providerOccupationChangeNotif.deleteSQLite()){
            if(await _providerInfoAlamat.deleteSQLite("4"))
              _providerOccupationChangeNotif.saveToSQLite(context, "4");
          }
          isOccupationDone = true;
          selectedIndex += 1;
          // if(_preferences.getString("last_known_state") != "DKR"){
          try{
            loadData = true;
            await _submitDataPartial.submitCustomerIndividu(context,1);
            await _submitDataPartial.submitOccupationPersonal(context,2);
            loadData = false;
          }
          catch(e){
            loadData = false;
            dialogFailedSavePartial(context, e.toString());
          }
          // }
          notifyListeners();
        }
      }
      else {
        _providerOccupationChangeNotif.autoValidate = true;
      }
    }
    else if (this._selectedIndex == 3) {
      // var _msgMinusLabaKotor = "";
      // var _msgMinusLabaBersihSebelumPajak = "";
      // var _msgMinusLabaBersihSetelahPajak = "";
      // var _msgMinusSisaPendapatan = "";
      // var _msgMinusSisaPendapatanProfesional = "";
      // if(_providerFormMFotoChangeNotif.occupationSelected != null) {
      //     if(_providerFormMFotoChangeNotif.occupationSelected.KODE == "05" || _providerFormMFotoChangeNotif.occupationSelected.KODE == "07") {
      //       // Wiraswasta
      //       // minus laba kotor
      //       if(double.parse(_providerIncomeChangeNotif.controllerGrossProfit.text.replaceAll(",", "")) < 0) {
      //         _providerIncomeChangeNotif.controllerCostOfGoodsSold.clear();
      //         _msgMinusLabaKotor = "- Laba Kotor tidak boleh minus.";
      //       }
      //       // minus laba bersih sebelum pajak
      //       if(double.parse(_providerIncomeChangeNotif.controllerNetProfitBeforeTax.text.replaceAll(",", "")) < 0) {
      //         _providerIncomeChangeNotif.controllerOperatingCosts.clear();
      //         _providerIncomeChangeNotif.controllerOtherCosts.clear();
      //         _msgMinusLabaBersihSebelumPajak = "- Laba Bersih Sebelum Pajak tidak boleh minus.";
      //       }
      //       // minus laba bersih setelah pajak
      //       if(double.parse(_providerIncomeChangeNotif.controllerNetProfitAfterTax.text.replaceAll(",", "")) < 0) {
      //         _providerIncomeChangeNotif.controllerTax.clear();
      //         _msgMinusLabaBersihSetelahPajak = "- Laba Bersih Setelah Pajak tidak boleh minus.";
      //       }
      //       // minus sisa pendapatan
      //       if(double.parse(_providerIncomeChangeNotif.controllerRestIncome.text.replaceAll(",", "")) < 0) {
      //         _providerIncomeChangeNotif.controllerCostOfLiving.clear();
      //         _msgMinusSisaPendapatan = "- Sisa Pendapatan tidak boleh minus.";
      //       }
      //
      //       if(double.parse(_providerIncomeChangeNotif.controllerGrossProfit.text.replaceAll(",", "")) < 0 ||
      //           double.parse(_providerIncomeChangeNotif.controllerNetProfitBeforeTax.text.replaceAll(",", "")) < 0 ||
      //           double.parse(_providerIncomeChangeNotif.controllerNetProfitAfterTax.text.replaceAll(",", "")) < 0 ||
      //           double.parse(_providerIncomeChangeNotif.controllerRestIncome.text.replaceAll(",", "")) < 0) {
      //         print("masuk if");
      //         if(double.parse(_providerIncomeChangeNotif.controllerGrossProfit.text.replaceAll(",", "")) < 0) {
      //           _providerIncomeChangeNotif.controllerGrossProfit.clear();
      //         }
      //         if(double.parse(_providerIncomeChangeNotif.controllerNetProfitBeforeTax.text.replaceAll(",", "")) < 0) {
      //           _providerIncomeChangeNotif.controllerNetProfitBeforeTax.clear();
      //         }
      //         if(double.parse(_providerIncomeChangeNotif.controllerNetProfitAfterTax.text.replaceAll(",", "")) < 0) {
      //           _providerIncomeChangeNotif.controllerNetProfitAfterTax.clear();
      //         }
      //         if(double.parse(_providerIncomeChangeNotif.controllerRestIncome.text.replaceAll(",", "")) < 0) {
      //           _providerIncomeChangeNotif.controllerRestIncome.clear();
      //         }
      //         showDialog(
      //             context: context,
      //             barrierDismissible: true,
      //             builder: (BuildContext context){
      //               return Theme(
      //                 data: ThemeData(
      //                     fontFamily: "NunitoSans",
      //                     primaryColor: Colors.black,
      //                     primarySwatch: primaryOrange,
      //                     accentColor: myPrimaryColor
      //                 ),
      //                 child: AlertDialog(
      //                   title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
      //                   content: Column(
      //                     crossAxisAlignment: CrossAxisAlignment.start,
      //                     mainAxisSize: MainAxisSize.min,
      //                     children: <Widget>[
      //                       Visibility(
      //                         visible: _msgMinusLabaKotor != "" ? true : false,
      //                         child: Text("$_msgMinusLabaKotor",)
      //                       ),
      //                       Visibility(
      //                           visible: _msgMinusLabaKotor != "" ? true : false,
      //                           child: SizedBox(height: MediaQuery.of(context).size.height / 117)
      //                       ),
      //                       Visibility(
      //                           visible: _msgMinusLabaBersihSebelumPajak != "" ? true : false,
      //                           child: Text("$_msgMinusLabaBersihSebelumPajak",)
      //                       ),
      //                       Visibility(
      //                           visible: _msgMinusLabaBersihSebelumPajak != "" ? true : false,
      //                           child: SizedBox(height: MediaQuery.of(context).size.height / 117)
      //                       ),
      //                       Visibility(
      //                           visible: _msgMinusLabaBersihSetelahPajak != "" ? true : false,
      //                           child: Text("$_msgMinusLabaBersihSetelahPajak",)
      //                       ),
      //                       Visibility(
      //                           visible: _msgMinusLabaBersihSetelahPajak != "" ? true : false,
      //                           child: SizedBox(height: MediaQuery.of(context).size.height / 117)
      //                       ),
      //                       Visibility(
      //                           visible: _msgMinusSisaPendapatan != "" ? true : false,
      //                           child: Text("$_msgMinusSisaPendapatan",)
      //                       ),
      //                     ],
      //                   ),
      //                   actions: <Widget>[
      //                     new FlatButton(
      //                       onPressed: () {
      //                         Navigator.of(context).pop(true);
      //                       },
      //                       child: new Text('Close'),
      //                     ),
      //                   ],
      //                 ),
      //               );
      //             }
      //         );
      //         _providerIncomeChangeNotif.incomeNotMinus = false;
      //       }
      //       else if(double.parse(_providerIncomeChangeNotif.controllerGrossProfit.text.replaceAll(",", "")) > 0 &&
      //           double.parse(_providerIncomeChangeNotif.controllerNetProfitBeforeTax.text.replaceAll(",", "")) > 0 &&
      //           double.parse(_providerIncomeChangeNotif.controllerNetProfitAfterTax.text.replaceAll(",", "")) > 0 &&
      //           double.parse(_providerIncomeChangeNotif.controllerRestIncome.text.replaceAll(",", "")) > 0) {
      //         print("masuk else if");
      //         _providerIncomeChangeNotif.incomeNotMinus = true;
      //       }
      //     }
      //     else {
      //       // Profesional
      //       // minus sisa pendapatan
      //       if(double.parse(_providerIncomeChangeNotif.controllerRestIncome.text.replaceAll(",", "")) < 0) {
      //         _providerIncomeChangeNotif.controllerCostOfLiving.clear();
      //         _msgMinusSisaPendapatanProfesional = "Sisa Pendapatan tidak boleh minus.";
      //       }
      //       if(double.parse(_providerIncomeChangeNotif.controllerRestIncome.text.replaceAll(",", "")) < 0) {
      //         print("masuk if");
      //         if(double.parse(_providerIncomeChangeNotif.controllerRestIncome.text.replaceAll(",", "")) < 0) {
      //           _providerIncomeChangeNotif.controllerRestIncome.clear();
      //         }
      //         showDialog(
      //             context: context,
      //             barrierDismissible: true,
      //             builder: (BuildContext context){
      //               return Theme(
      //                 data: ThemeData(
      //                     fontFamily: "NunitoSans",
      //                     primaryColor: Colors.black,
      //                     primarySwatch: primaryOrange,
      //                     accentColor: myPrimaryColor
      //                 ),
      //                 child: AlertDialog(
      //                   title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
      //                   content: Column(
      //                     crossAxisAlignment: CrossAxisAlignment.start,
      //                     mainAxisSize: MainAxisSize.min,
      //                     children: <Widget>[
      //                       Visibility(
      //                           visible: _msgMinusSisaPendapatanProfesional != "" ? true : false,
      //                           child: Text("$_msgMinusSisaPendapatanProfesional",)
      //                       ),
      //                     ],
      //                   ),
      //                   actions: <Widget>[
      //                     new FlatButton(
      //                       onPressed: () {
      //                         Navigator.of(context).pop(true);
      //                       },
      //                       child: new Text('Close'),
      //                     ),
      //                   ],
      //                 ),
      //               );
      //             }
      //         );
      //         _providerIncomeChangeNotif.incomeNotMinus = false;
      //       }
      //       else if(double.parse(_providerIncomeChangeNotif.controllerRestIncome.text.replaceAll(",", "")) > 0) {
      //         print("masuk else if");
      //         _providerIncomeChangeNotif.incomeNotMinus = true;
      //       }
      //     }
      //   }
      if (_form.validate()) {
        await _providerIncomeChangeNotif.checkDataDakor();
        if(await _providerIncomeChangeNotif.deleteSQLite()){
          await _providerIncomeChangeNotif.saveToSQLite(context);
          await _setLastStep(this._selectedIndex);
          if(Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false).isGuarantorVisible){
            selectedIndex += 1;
            isIncomeDone = true;
          }
          else{
            selectedIndex += 2;
            isIncomeDone = true;
          }
        }
        // if(_preferences.getString("last_known_state") != "DKR"){
          try{
            loadData = true;
            await _submitDataPartial.submitCustomerIndividu(context,1);
            await _submitDataPartial.submitOccupationPersonal(context,2);
            await _submitDataPartial.submitIncome(context,3,false);
            loadData = false;
          }
          catch(e){
            loadData = false;
            dialogFailedSavePartial(context, e.toString());
          }
        // }
      }
      else {
        isIncomeDone = false;
        _providerIncomeChangeNotif.autoValidate = true;
      }
    }
    else if (this._selectedIndex == 4) {
      if(_providerGuarantor.radioValueIsWithGuarantor == 0){
        if(_providerGuarantor.listGuarantorIndividual.isNotEmpty || _providerGuarantor.listGuarantorCompany.isNotEmpty){
          if(await _providerGuarantor.deleteSQLite()){
            // await _provider.saveToSQLite();
            await _providerGuarantor.saveToSQLite(context);
            await _setLastStep(this._selectedIndex);
            selectedIndex += 1;
            isGuarantorDone = true;
          }
          // if(_preferences.getString("last_known_state") != "DKR"){
            try{
              loadData = true;
              await _submitDataPartial.submitCustomerIndividu(context,1);
              await _submitDataPartial.submitOccupationPersonal(context,2);
              await _submitDataPartial.submitIncome(context,3,false);
              await _submitDataPartial.submitGuarantor(context,4);
              loadData = false;
            }
            catch(e){
              loadData = false;
              dialogFailedSavePartial(context, e.toString());
            }
          // }
        }
        else{
          _providerGuarantor.autoValidate = true;
        }
      }
      else{
        await _setLastStep(this._selectedIndex);
        selectedIndex += 1;
        isGuarantorDone = true;
      }
    }
    else if(this._selectedIndex == 5){
      if (_form.validate()) {
        await _providerInfoApp.checkDataDakor();
        if(await _providerInfoApp.deleteSQLite()){
          await _providerInfoApp.saveToSQLite(context);
        }
        await _setLastStep(this._selectedIndex);
        selectedIndex += 1;
        isInfoAppDone = true;
      } else {
        _providerInfoApp.autoValidate = true;
      }
      // if(_preferences.getString("last_known_state") != "DKR"){
        try{
          loadData = true;
          await _submitDataPartial.submitCustomerIndividu(context,1);
          await _submitDataPartial.submitOccupationPersonal(context,2);
          await _submitDataPartial.submitIncome(context,3,false);
          if(_providerGuarantor.listGuarantorIndividual.isNotEmpty || _providerGuarantor.listGuarantorCompany.isNotEmpty){
            await _submitDataPartial.submitGuarantor(context,4);
          }
          await _submitDataPartial.submitApplication(context,5);
          loadData = false;
        }
        catch(e){
          loadData = false;
          dialogFailedSavePartial(context, e.toString());
        }
      // }
    }
    else if(this._selectedIndex == 6){
      var _providerRincianNasabah = Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false);
      var _providerRincianKeluarga = Provider.of<FormMInfoKeluargaChangeNotif>(context, listen: false);
      var _providerInfoObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
      if(_providerRincianNasabah.maritalStatusSelected != null){
        if (_providerRincianNasabah.maritalStatusSelected.id == "01" && _providerInfoObjectUnit.groupObjectSelected.KODE != "003") { // Sudah Menikah dan Group Objek Selain Durable
          if(_providerRincianKeluarga.listFormInfoKel.isEmpty){
            _providerRincianKeluarga.isPasanganExist = true;
          }
          else {
            for(int i = 0; i < _providerRincianKeluarga.listFormInfoKel.length; i++) {
              if(_providerRincianKeluarga.listFormInfoKel[i].relationshipStatusModel.PARA_FAMILY_TYPE_ID == "01" || _providerRincianKeluarga.listFormInfoKel[i].relationshipStatusModel.PARA_FAMILY_TYPE_ID == "02") {
                _providerRincianKeluarga.isPasanganExist = false;
              }
            }
          }
          if(_providerRincianKeluarga.isPasanganExist){
            showDialog(
                context: context,
                barrierDismissible: true,
                builder: (BuildContext context){
                  return Theme(
                    data: ThemeData(
                        fontFamily: "NunitoSans",
                        primaryColor: Colors.black,
                        primarySwatch: primaryOrange,
                        accentColor: myPrimaryColor
                    ),
                    child: AlertDialog(
                      title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
                      content: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text("Wajib mengisi data pasangan terlebih dahulu pada Rincian Keluarga.",),
                        ],
                      ),
                      actions: <Widget>[
                        new FlatButton(
                          onPressed: () {
                            Navigator.of(context).pop(true);
                          },
                          child: new Text('Close'),
                        ),
                      ],
                    ),
                  );
                }
            );
          }
        }
      }
      else{
        showDialog(
            context: context,
            barrierDismissible: true,
            builder: (BuildContext context){
              return Theme(
                data: ThemeData(
                    fontFamily: "NunitoSans",
                    primaryColor: Colors.black,
                    primarySwatch: primaryOrange,
                    accentColor: myPrimaryColor
                ),
                child: AlertDialog(
                  title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
                  content: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text("Harap isi Status Pernikahan.",),
                    ],
                  ),
                  actions: <Widget>[
                    new FlatButton(
                      onPressed: () {
                        Navigator.of(context).pop(true);
                      },
                      child: new Text('Close'),
                    ),
                  ],
                ),
              );
            }
        );
      }
      if(!_providerRincianKeluarga.isPasanganExist && _providerRincianNasabah.maritalStatusSelected != null){
        if(!_providerInfoObjectUnit.flag){
          _providerInfoObjectUnit.autoValidate = true;
        }
        if((_providerInfoSales.listInfoSales.length == 0 || _providerInfoSales.listInfoSales.length != 0)  && !_providerInfoSales.isCheckData){
          _providerInfoSales.autoValidate = true;
        }
        if(!_providerKolateral.flag){
          if(_providerKolateral.collateralTypeModel == null){
            _providerKolateral.autoValidateAuto = true;
            _providerKolateral.autoValidateProp = false;
          } else {
            if(_providerKolateral.collateralTypeModel.id == "001"){
              _providerKolateral.autoValidateAuto = true;
              _providerKolateral.autoValidateProp = false;
            } else {
              _providerKolateral.autoValidateAuto = false;
              _providerKolateral.autoValidateProp = true;
            }
          }
        }
        if(_providerInfoObjectUnit.flag && _providerInfoSales.listInfoSales.length != 0 && _providerInfoSales.isCheckData && _providerKolateral.flag){
          if(await _providerInfoObjectUnit.deleteSQLite() && await _providerInfoSales.deleteSQLite() && await _providerWMP.deleteSQLite()){
            _providerInfoObjectUnit.saveToSQLite(context);
            _providerInfoSales.saveToSQLite(context);
          }
          if(_providerKolateral.collateralTypeModel.id == "001"){
            if(await _providerKolateral.deleteSQLiteCollaOto()){
              await _providerKolateral.saveToSQLiteOto(context);
              await _providerKolateral.deleteSQLiteCollaProp();
            }
          }
          else if(_providerKolateral.collateralTypeModel.id == "002"){
            if(await _providerKolateral.deleteSQLiteCollaProp()){
              await _providerKolateral.saveToSQLiteProperti(context, "6");
              await _providerKolateral.deleteSQLiteCollaOto();
            }
          }
          // selectedIndex +=1;
          _providerTaksasiUnit.setIsVisible(context);
          await _setLastStep(this._selectedIndex);
          Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: true).isVisible ? selectedIndex +=1 : selectedIndex +=2;
          isMenuObjectInformationDone = true;

          // if(_preferences.getString("last_known_state") != "DKR"){
            try{
              loadData = true;
              await _submitDataPartial.submitCustomerIndividu(context,1);
              await _submitDataPartial.submitOccupationPersonal(context,2);
              await _submitDataPartial.submitIncome(context,3,false);
              if(_providerGuarantor.listGuarantorIndividual.isNotEmpty || _providerGuarantor.listGuarantorCompany.isNotEmpty){
                await _submitDataPartial.submitGuarantor(context,4);
              }
              await _submitDataPartial.submitApplication(context,5);
              await _submitDataPartial.submitColla(context,6);
              loadData = false;
            }
            catch(e){
              loadData = false;
              dialogFailedSavePartial(context, e.toString());
            }
          // }
        }
      }
    }
    else if(this._selectedIndex == 7){
      if(await _providerKaroseri.deleteSQLite()) {
        await _providerKaroseri.saveToSQLite();
      }
      await _setLastStep(this._selectedIndex);
      List _step = await _dbHelper.selectLastStep();
      print("cek last step $_step");
      selectedIndex += 1;
      isInfoObjKaroseriDone = true;
      // if(_preferences.getString("last_known_state") != "DKR"){
        try{
          loadData = true;
          await _submitDataPartial.submitCustomerIndividu(context,1);
          await _submitDataPartial.submitOccupationPersonal(context,2);
          await _submitDataPartial.submitIncome(context,3,false);
          if(_providerGuarantor.listGuarantorIndividual.isNotEmpty || _providerGuarantor.listGuarantorCompany.isNotEmpty){
            await _submitDataPartial.submitGuarantor(context,4);
          }
          await _submitDataPartial.submitApplication(context,5);
          await _submitDataPartial.submitColla(context,6);
          if(_providerKaroseri.listFormKaroseriObject.isNotEmpty) await _submitDataPartial.submitKaroseri(context,7);
          loadData = false;
        }
        catch(e){
          loadData = false;
          dialogFailedSavePartial(context, e.toString());
        }
      // }
      /**
       *
       */
      // if(_providerKaroseri.listFormKaroseriObject.isNotEmpty){
      //   _providerKaroseri.saveToSQLite();
      //   await _setLastStep(this._selectedIndex);
      //   selectedIndex += 1;
      //   isInfoObjKaroseriDone = true;
      // } else {
      //   _showSnackBar("Info Karoseri tidak boleh kosong");
      //   _providerKaroseri.autoValidate = true;
      // }
    }
    else if(this._selectedIndex == 8){
      if(_preferences.getString("jenis_penawaran") == "002") {
        if(double.parse(_providerInfoCreditStructure.controllerInstallment.text.replaceAll(",", "")) > double.parse(_dataCL[0]['max_installment'])) {
          _providerInfoCreditStructure.flag = false;
          _providerInfoCreditStructure.messageError = "PH pengajuan lebih besar dari PH aktivasi.";
          showDialog(
              context: context,
              barrierDismissible: true,
              builder: (BuildContext context){
                return Theme(
                  data: ThemeData(
                      fontFamily: "NunitoSans",
                      primaryColor: Colors.black,
                      primarySwatch: primaryOrange,
                      accentColor: myPrimaryColor
                  ),
                  child: AlertDialog(
                    title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
                    content: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text("PH pengajuan lebih besar dari PH aktivasi. PH aktivasi Rp${_providerInfoCreditStructure.formatCurrency.formatCurrency2(_dataCL[0]['max_installment'])}",),
                        SizedBox(height: MediaQuery.of(context).size.height / 117,),
                        Text("Mohon melakukan penyesuaian",),
                      ],
                    ),
                    actions: <Widget>[
                      FlatButton(
                        onPressed: () {
                          Navigator.of(context).pop(true);
                        },
                        child: Text('Close'),
                      ),
                    ],
                  ),
                );
              }
          );
        }
      }
      else if(_preferences.getString("jenis_penawaran") == "003") {
        if(double.parse(_providerInfoCreditStructure.controllerInstallment.text.replaceAll(",", "")) > double.parse(_dataCL[0]['max_installment'])) {
          showDialog(
              context: context,
              barrierDismissible: true,
              builder: (BuildContext context){
                return Theme(
                  data: ThemeData(
                      fontFamily: "NunitoSans",
                      primaryColor: Colors.black,
                      primarySwatch: primaryOrange,
                      accentColor: myPrimaryColor
                  ),
                  child: AlertDialog(
                    title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
                    content: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text("Aplikasi melewati limit nasabah.\n\nNilai Angsuran Rp${_providerInfoCreditStructure.formatCurrency.formatCurrency2((_providerInfoCreditStructure.controllerInstallment.text))}.\nLimit Angsuran Rp${_providerInfoCreditStructure.formatCurrency.formatCurrency2((_dataCL[0]['max_installment'].toString()))}.\n\nSilahkan rubah struktur kredit untuk meningkatkan potensi IA nasabah",),
                      ],
                    ),
                    actions: <Widget>[
                      FlatButton(
                        onPressed: () {
                          Navigator.of(context).pop(true);
                        },
                        child: Text('Close'),
                      ),
                    ],
                  ),
                );
              }
          );
        }
      }
      if(_providerInfoCreditStructure.controllerTotalPrice.text.isNotEmpty){
        if(int.parse(_providerInfoCreditStructure.controllerTotalPrice.text.replaceAll(",", "").split(".")[0]) >= 50000000 && _providerFormMCustomerInfoChangeNotifier.radioValueIsHaveNPWP == 0) {
          _providerInfoCreditStructure.flag = false;
          _providerInfoCreditStructure.messageError = "Wajib NPWP pada Rincian Nasabah karena Total Harga lebih dari 50,000,000.00.";
          showDialog(
              context: context,
              barrierDismissible: true,
              builder: (BuildContext context){
                return Theme(
                  data: ThemeData(
                      fontFamily: "NunitoSans",
                      primaryColor: Colors.black,
                      primarySwatch: primaryOrange,
                      accentColor: myPrimaryColor
                  ),
                  child: AlertDialog(
                    title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
                    content: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text("Wajib NPWP pada Rincian Nasabah karena Total Harga lebih dari 50,000,000.00.",),
                      ],
                    ),
                    actions: <Widget>[
                      FlatButton(
                        onPressed: () {
                          Navigator.of(context).pop(true);
                        },
                        child: Text('Close'),
                      ),
                    ],
                  ),
                );
              }
          );
        }
      }
      if(!_providerInfoCreditStructure.flag){
        _providerInfoCreditStructure.autoValidate = true;
      }
      if(_providerInfoObjectUnit.productTypeSelected != null) {
        if((_providerInfoObjectUnit.productTypeSelected.id == "0002F" || _providerInfoObjectUnit.productTypeSelected.id == "0002G") && _providerWMP.listWMP.isEmpty) {
          _providerWMP.flag = true;
        }
      }
      await _providerMajorInsurance.onBackPress();
      await _providerAdditionalInsurance.onBackPress();
      if(_preferences.getString("last_known_state") != "IDE") {
        if(_providerMajorInsurance.listFormMajorInsurance.isNotEmpty) {
          for(int i=0; i<_providerMajorInsurance.listFormMajorInsurance.length; i++) {
            if(_providerMajorInsurance.listFormMajorInsurance[i].insuranceType.KODE == "2") {
              await _providerAddMajorInsurance.getBatasAtasBawahUtamaUpdated(context, _providerMajorInsurance.listFormMajorInsurance[i], i);
            } else if(_providerMajorInsurance.listFormMajorInsurance[i].insuranceType.KODE == "3") {
              await _providerAddMajorInsurance.getBatasAtasBawahPerluasanUpdated(context, _providerMajorInsurance.listFormMajorInsurance[i], i);
            }
          }
        }
        if(_providerAdditionalInsurance.listFormAdditionalInsurance.isNotEmpty) {
          for(int i=0; i<_providerAdditionalInsurance.listFormAdditionalInsurance.length; i++) {
            await _providerAddAdditionalInsurance.getLowerUpperLimitUpdated(context, _providerAdditionalInsurance.listFormAdditionalInsurance[i], i);
          }
        }
      }
      // if(_providerMajorInsurance.listFormMajorInsurance.length == 0 && _providerKolateral.collateralTypeModel.id != "003"){
      //   _providerMajorInsurance.flag = true;
      // }
      // if(_providerAdditionalInsurance.listFormAdditionalInsurance.length == 0 && _providerInfoObjectUnit.groupObjectSelected.KODE == "002"){
      //   _providerAdditionalInsurance.flag = true;
      // }
      if(_providerInfoCreditStructure.flag && !_providerMajorInsurance.flag && !_providerAdditionalInsurance.flag && !_providerWMP.flag){
        _providerInfoCreditStructure.updateMS2ApplObjectInfStrukturKredit();
        _providerInfoCreditStructureTypeInstallment.updateMS2ApplObjectInfStrukturKreditBP();
        if(await _providerMajorInsurance.deleteSQLite()){
          await _providerMajorInsurance.saveToSQLite();
        }
        if(await _providerAdditionalInsurance.deleteSQLite()){
          await _providerAdditionalInsurance.saveToSQLite();
        }
        if(await _providerWMP.deleteSQLite()){
          _providerWMP.saveToSQLite();
        }
        _providerCreditIncome.updateMS2ApplObjectInfStrukturKreditIncome();
        if(await _providerInfoCreditStructure.deleteSQLite()){
          _providerInfoCreditStructure.saveToSQLiteInfStrukturKreditBiaya();
        }
        if(!_providerInfoCreditStructure.checkIsAdminFeeExist(context)){
          dialogFailedSavePartial(context, "Harap isi biaya admin");
        }
        else{
          try{
            loadData = true;
            await Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).calculateCreditNew(context);
            await Provider.of<InfoCreditIncomeChangeNotifier>(context, listen: false).getDSR(context);
            await Provider.of<InfoCreditSubsidyChangeNotifier>(context, listen: false).getSukuBungaSebelumEffFlat(context);
            await _setLastStep(this._selectedIndex);
            loadData = false;
            selectedIndex += 1;
            isMenuDetailLoanDone = true;
          }
          catch(e){
            loadData = false;
            dialogFailedSavePartial(context, e.toString());
          }
        }
        // if(_preferences.getString("last_known_state") != "DKR"){
          try{
            loadData = true;
            await _submitDataPartial.submitCustomerIndividu(context,1);
            await _submitDataPartial.submitOccupationPersonal(context,2);
            await _submitDataPartial.submitIncome(context,3,false);
            if(_providerGuarantor.listGuarantorIndividual.isNotEmpty || _providerGuarantor.listGuarantorCompany.isNotEmpty){
              await _submitDataPartial.submitGuarantor(context,4);
            }
            await _submitDataPartial.submitApplication(context,5);
            await _submitDataPartial.submitColla(context,6);
            if(_providerKaroseri.listFormKaroseriObject.isNotEmpty) await _submitDataPartial.submitKaroseri(context,7);
            await _submitDataPartial.submitStructureCredit(8,context);
            if(_providerMajorInsurance.listFormMajorInsurance.isNotEmpty
                ||  _providerAdditionalInsurance.listFormAdditionalInsurance.isNotEmpty) await _submitDataPartial.submitInsurance(8,context);
            if(_providerWMP.listWMP.isNotEmpty) await _submitDataPartial.submitWMP(8,context);
            loadData = false;
          }
          catch(e){
            loadData = false;
            dialogFailedSavePartial(context, e.toString());
          }
        // }
      }
      else{
        print("masuk else");
      }
    }
    else if(this._selectedIndex == 9){
      // if(_providerCreditSubsidy.listInfoCreditSubsidy.isNotEmpty){
        if(await _providerCreditSubsidy.deleteSQLIte()){
          _providerCreditSubsidy.saveToSQLite();
        }
        await _setLastStep(this._selectedIndex);
        selectedIndex += 1;
        _isCreditSubsidyDone = true;
        // if(_preferences.getString("last_known_state") != "DKR"){
          try{
            loadData = true;
            await _submitDataPartial.submitCustomerIndividu(context,1);
            await _submitDataPartial.submitOccupationPersonal(context,2);
            await _submitDataPartial.submitIncome(context,3,false);
            if(_providerGuarantor.listGuarantorIndividual.isNotEmpty || _providerGuarantor.listGuarantorCompany.isNotEmpty){
              await _submitDataPartial.submitGuarantor(context,4);
            }
            await _submitDataPartial.submitApplication(context,5);
            await _submitDataPartial.submitColla(context,6);
            if(_providerKaroseri.listFormKaroseriObject.isNotEmpty) await _submitDataPartial.submitKaroseri(context,7);
            await _submitDataPartial.submitStructureCredit(8,context);
            if(_providerMajorInsurance.listFormMajorInsurance.isNotEmpty
                ||  _providerAdditionalInsurance.listFormAdditionalInsurance.isNotEmpty) await _submitDataPartial.submitInsurance(8,context);
            if(_providerWMP.listWMP.isNotEmpty) await _submitDataPartial.submitWMP(8,context);
            if(_providerCreditSubsidy.listInfoCreditSubsidy.isNotEmpty) await _submitDataPartial.submitSubsidy(9,context);
            loadData = false;
          }
          catch(e){
            loadData = false;
            dialogFailedSavePartial(context, e.toString());
          }
        // }
      // } else {
      //   _showSnackBar("Info Kredit Subsidi tidak boleh kosong");
      // }
    }
    else if(this._selectedIndex == 10){
      if(await _providerInfoDocument.deleteSQLite()){
        _providerInfoDocument.saveToSQLite(context);
      }
      isDocumentInfoDone = true;
      if(_providerTaksasiUnit.isVisible){
        await _setLastStep(this._selectedIndex);
        selectedIndex += 1;
      }
      else {
        await _setLastStep(this._selectedIndex);
        selectedIndex += 2;
      }
      // if(_providerInfoDocument.listInfoDocument.isNotEmpty){
      //   if(await _providerInfoDocument.deleteSQLite()){
      //     _providerInfoDocument.saveToSQLite(context);
      //   }
      //   if(_providerTaksasiUnit.isVisible){
      //     selectedIndex += 1;
      //   }
      //   else {
      //     selectedIndex += 2;
      //   }
      //   _isDocumentInfoDone = true;
      // } else {
      //   _showSnackBar("Info Dokumen tidak boleh kosong");
      // }
    }
    else if(this._selectedIndex == 11){
      if(_providerKolateral.groupObjectSelected.KODE == "002") {
        if(_providerKolateral.objectSelected.id != "003") {
          _providerTaksasiUnit.calculatedCar(context);
        }
      } else {
        if(_providerKolateral.objectSelected.id != "001") {
          _providerTaksasiUnit.calculatedMotorCycle(context);
        }
      }
      // if(_providerInfoObjectUnit.groupObjectSelected != null){
        if(_form.validate()){
          await _setLastStep(this._selectedIndex);
          selectedIndex += 1;
          isTaksasiUnitDone = true;
          if(await _providerTaksasiUnit.deleteSQLite()){
            _providerTaksasiUnit.saveToSQLite(context);
          }
        }
        else{
          if(_providerKolateral.groupObjectSelected.KODE == "001"){
            _providerTaksasiUnit.autoValidateMotor;
          }
          else if(_providerKolateral.groupObjectSelected.KODE == "002"){
            _providerTaksasiUnit.autoValidateCar;
          }
        }
        // if(_providerInfoObjectUnit.groupObjectSelected.KODE == "001"){
        //
        //   _providerTaksasiUnit.autoValidateMotor;
        // }
        // else if(_providerInfoObjectUnit.groupObjectSelected.KODE == "002"){
        //   if(_form.validate()){
        //     selectedIndex += 1;
        //     isTaksasiUnitDone = true;
        //     _providerTaksasiUnit.saveToSQLite(context);
        //   }
        //   _providerTaksasiUnit.autoValidateCar;
        // }
      // }
    }
    else if(this._selectedIndex == 12){
      if(_form.validate()){
        await _setLastStep(this._selectedIndex);
        selectedIndex += 1;
        isMarketingNotesDone = true;
      }
      else{
        _providerMarketingNotes.autoValidate = true;
      }
    }
    // print("cek selected index next btn ${this._selectedIndex}");
    // _setLastStep(this._selectedIndex);
  }

  void checkTabDrawer(BuildContext context, int index) async{
    if(this._selectedIndex < index){
      for(int i=this._selectedIndex; i < index; i++){
        if(await isValidateTabDrawer(context, i)){
          break;
        }
      }
    }
    else{
      this.selectedIndex = index;
    }
    notifyListeners();
  }

  void backBtn(BuildContext context) {
    // var _providerInfoCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false);
    var _providerTaksasiUnit = Provider.of<TaksasiUnitChangeNotifier>(context, listen: false);
    // var _providerFormMFotoChangeNotif = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
//    if(this._selectedIndex == 9){
//      if(_providerInfoCreditStructure.isVisible){
//         this._selectedIndex -= 1;
//         notifyListeners();
//      }
//      else{
//        this._selectedIndex -= 2;
//        notifyListeners();
//      }
//    }

    if(this._selectedIndex == 5){
      print(Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false).isGuarantorVisible);
      if(Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false).isGuarantorVisible){
        this._selectedIndex -= 1;
        notifyListeners();
      }
      else{
        this._selectedIndex -= 2;
        notifyListeners();
      }
    }


    else if(this._selectedIndex == 12){
      if(_providerTaksasiUnit.isVisible){
        this._selectedIndex -= 1;
        notifyListeners();
      }
      else{
        this._selectedIndex -= 2;
        notifyListeners();
      }
    }
    else{
      this._selectedIndex -= 1;
      notifyListeners();
    }
  }

  void _showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor, duration: Duration(seconds: 3)));
  }

  void _showSnackBarSuccess(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: Colors.green, duration: Duration(seconds: 3)));
  }

  // fungsi untuk clear semua form sebelum set semua data ke setiap form
  void clearData(BuildContext context){
    this._selectedIndex = 0;
    this._isPhotoDone = false;
    this._isMenuCustomerDetailDone = false;
    this._isOccupationDone = false;
    this._isIncomeDone = false;
    this._isGuarantorDone = false;
    this._isInfoAppDone = false;
    this._isMenuObjectInformationDone = false;
    this._isInfoObjKaroseriDone = false;
    this._isMenuDetailLoanDone = false;
    this._isCreditSubsidyDone = false;
    this._isDocumentInfoDone = false;
    this._isTaksasiUnitDone = false;
    this._isMarketingNotesDone = false;

    // Form M Individu
    Provider.of<FormMFotoChangeNotifier>(context, listen: false).clearFormMFoto();
    Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false).clearFormInfoNasabah();
    Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false).clearDataAlamat();
    Provider.of<FormMInformasiKeluargaIBUChangeNotif>(context, listen: false).clearDataKeluargaIbu();
    Provider.of<FormMInfoKeluargaChangeNotif>(context, listen: false).clearDataKeluarga();
    Provider.of<FormMOccupationChangeNotif>(context, listen: false).clearDataOccupation();
    Provider.of<FormMIncomeChangeNotifier>(context, listen: false).clearDataIncome();
    Provider.of<FormMGuarantorChangeNotifier>(context, listen: false).clearDataGuarantor();
    // Form App
    Provider.of<InfoAppChangeNotifier>(context, listen: false).clearDataInfoApp();
    Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).clearDataInfoUnitObject();
    Provider.of<InformationSalesmanChangeNotifier>(context, listen: false).clearDataInfoSales();
    Provider.of<InformationCollateralChangeNotifier>(context, listen: false).clearDataInfoCollateral();
    Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false).clearDataInfoObjectKaroseri();
    Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false).clearInfoCreditStructure();
    Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context, listen: false).clearData();
    Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false).clearMajorInsurance();
    Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false).clearListAdditionalInsurance();
    Provider.of<InfoWMPChangeNotifier>(context, listen: false).clearDataWMP();
    Provider.of<InfoCreditIncomeChangeNotifier>(context, listen: false).clearDataInfoCreditIncome();
    Provider.of<InfoCreditSubsidyChangeNotifier>(context, listen: false).clearInfoCreditSubsidy();
    Provider.of<InfoDocumentChangeNotifier>(context, listen: false).clearInfoDocument();
    Provider.of<TaksasiUnitChangeNotifier>(context, listen: false).clearTaksasiUnit();
    Provider.of<MarketingNotesChangeNotifier>(context, listen: false).clearMarketingNotes();
    // Form Survey
    Provider.of<ResultSurveyChangeNotifier>(context, listen: false).clearDataSurvey();
    Provider.of<ListSurveyPhotoChangeNotifier>(context, listen: false).clearData();
    _setupDataFromSQLite(context);
  }

  // List mappingCustomerIncomeWiraswastaBelumMenikah(BuildContext context) {
  //   var _providerIncome = Provider.of<FormMIncomeChangeNotifier>(context, listen: false);
  //
  //   // Wiraswasta Belum Menikah
  //   var _customerIncomes = [];
  //   for(int i=0; i < 12; i++) {
  //     _customerIncomes.add(
  //       {
  //         "customerIncomeID": "NEW",
  //         "incomeFrmlType": i == 0
  //             ? "S" : i == 1
  //             ? "+" : i == 2
  //             ? "=" : i == 3
  //             ? "-" : i == 4
  //             ? "=" : i == 5
  //             ? "-" : i == 6
  //             ? "-" : i == 7
  //             ? "=" : i == 8
  //             ? "-" : i == 9
  //             ? "=" : i == 10
  //             ? "-" : i == 11
  //             ? "="
  //             : "",
  //         "incomeType": i == 0
  //             ? "001" : i == 1
  //             ? "002" : i == 2
  //             ? "003" : i == 3
  //             ? "004" : i == 4
  //             ? "005" : i == 5
  //             ? "006" : i == 6
  //             ? "007" : i == 7
  //             ? "008" : i == 8
  //             ? "009" : i == 9
  //             ? "010" : i == 10
  //             ? "011" : i == 11
  //             ? "017"
  //             : "",
  //         "incomeValue": i == 0
  //             ? _providerIncome.controllerMonthlyIncome.text : i == 1
  //             ? _providerIncome.controllerOtherIncome.text : i == 2
  //             ? _providerIncome.controllerTotalIncome.text : i == 3
  //             ? _providerIncome.controllerCostOfGoodsSold.text : i == 4
  //             ? _providerIncome.controllerGrossProfit.text : i == 5
  //             ? _providerIncome.controllerOperatingCosts.text : i == 6
  //             ? _providerIncome.controllerOtherCosts.text : i == 7
  //             ? _providerIncome.controllerNetProfitBeforeTax.text : i == 8
  //             ? _providerIncome.controllerTax.text : i == 9
  //             ? _providerIncome.controllerNetProfitAfterTax.text : i == 10
  //             ? _providerIncome.controllerCostOfLiving.text : i == 11
  //             ? _providerIncome.controllerRestIncome.text
  //             : _providerIncome.controllerOtherInstallments.text,
  //         "dataStatus": "ACTIVE",
  //         "creationalSpecification": {
  //           "createdAt": DateTime.now(),
  //           "createdBy": _preferences.getString("username"),
  //           "modifiedAt": DateTime.now(),
  //           "modifiedBy": _preferences.getString("username"),
  //         },
  //       }
  //     );
  //   }
  //
  //   return _customerIncomes;
  // }
  //
  // List mappingCustomerIncomeWiraswastaMenikah(BuildContext context) {
  //   var _providerIncome = Provider.of<FormMIncomeChangeNotifier>(context, listen: false);
  //
  //   // Wiraswasta Menikah
  //   var _customerIncomes = [];
  //   for(int i=0; i < 13; i++) {
  //     _customerIncomes.add(
  //       {
  //         "customerIncomeID": "NEW",
  //         "incomeFrmlType": i == 0
  //             ? "S" : i == 1
  //             ? "+" : i == 2
  //             ? "=" : i == 3
  //             ? "-" : i == 4
  //             ? "=" : i == 5
  //             ? "-" : i == 6
  //             ? "-" : i == 7
  //             ? "=" : i == 8
  //             ? "-" : i == 9
  //             ? "=" : i == 10
  //             ? "-" : i == 11
  //             ? "=" : i == 12
  //             ? ""
  //             : "",
  //         "incomeType": i == 0
  //             ? "001" : i == 1
  //             ? "002" : i == 2
  //             ? "003" : i == 3
  //             ? "004" : i == 4
  //             ? "005" : i == 5
  //             ? "006" : i == 6
  //             ? "007" : i == 7
  //             ? "008" : i == 8
  //             ? "009" : i == 9
  //             ? "010" : i == 10
  //             ? "011" : i == 11
  //             ? "017" : i == 12
  //             ? ""
  //             : "",
  //         "incomeValue": i == 0
  //             ? _providerIncome.controllerMonthlyIncome.text : i == 1
  //             ? _providerIncome.controllerOtherIncome.text : i == 2
  //             ? _providerIncome.controllerTotalIncome.text : i == 3
  //             ? _providerIncome.controllerCostOfGoodsSold.text : i == 4
  //             ? _providerIncome.controllerGrossProfit.text : i == 5
  //             ? _providerIncome.controllerOperatingCosts.text : i == 6
  //             ? _providerIncome.controllerOtherCosts.text : i == 7
  //             ? _providerIncome.controllerNetProfitBeforeTax.text : i == 8
  //             ? _providerIncome.controllerTax.text : i == 9
  //             ? _providerIncome.controllerNetProfitAfterTax.text : i == 10
  //             ? _providerIncome.controllerCostOfLiving.text : i == 11
  //             ? _providerIncome.controllerRestIncome.text : i == 12
  //             ? _providerIncome.controllerSpouseIncome.text
  //             : _providerIncome.controllerOtherInstallments.text,
  //         "dataStatus": "ACTIVE",
  //         "creationalSpecification": {
  //           "createdAt": DateTime.now(),
  //           "createdBy": _preferences.getString("username"),
  //           "modifiedAt": DateTime.now(),
  //           "modifiedBy": _preferences.getString("username"),
  //         },
  //       }
  //     );
  //   }
  //
  //   return _customerIncomes;
  // }
  //
  // List mappingCustomerIncomeProfesionalBelumMenikah(BuildContext context) {
  //   var _providerIncome = Provider.of<FormMIncomeChangeNotifier>(context, listen: false);
  //
  //   var _customerIncomes = [];
  //   // Profesional Belum Menikah
  //   for(int i=0; i < 5; i++) {
  //     _customerIncomes.add(
  //         {
  //           "customerIncomeID": "NEW",
  //           "incomeFrmlType": i == 0
  //               ? "S" : i == 1
  //               ? "+" : i == 2
  //               ? "=" : i == 3
  //               ? "-" : i == 4
  //               ? "="
  //               : "",
  //           "incomeType": i == 0
  //               ? "001" : i == 1
  //               ? "002" : i == 2
  //               ? "003" : i == 3
  //               ? "011" : i == 4
  //               ? "017"
  //               : "",
  //           "incomeValue": i == 0
  //               ? _providerIncome.controllerIncome.text : i == 1
  //               ? _providerIncome.controllerOtherIncome.text : i == 2
  //               ? _providerIncome.controllerTotalIncome.text : i == 3
  //               ? _providerIncome.controllerCostOfLiving.text : i == 4
  //               ? _providerIncome.controllerRestIncome.text
  //               : _providerIncome.controllerOtherInstallments.text,
  //           "dataStatus": "ACTIVE",
  //           "creationalSpecification": {
  //             "createdAt": DateTime.now(),
  //             "createdBy": _preferences.getString("username"),
  //             "modifiedAt": DateTime.now(),
  //             "modifiedBy": _preferences.getString("username"),
  //           },
  //         }
  //     );
  //   }
  //
  //   return _customerIncomes;
  // }
  //
  // List mappingCustomerIncomeProfesionalMenikah(BuildContext context) {
  //   var _providerIncome = Provider.of<FormMIncomeChangeNotifier>(context, listen: false);
  //
  //   var _customerIncomes = [];
  //   // Profesional Menikah
  //   for(int i=0; i < 6; i++) {
  //     _customerIncomes.add(
  //         {
  //           "customerIncomeID": "NEW",
  //           "incomeFrmlType": i == 0
  //               ? "S" : i == 1
  //               ? "+" : i == 2
  //               ? "+" : i == 3
  //               ? "=" : i == 4
  //               ? "-" : i == 5
  //               ? "="
  //               : "",
  //           "incomeType": i == 0
  //               ? "001" : i == 1
  //               ? "002" : i == 2
  //               ? "?" : i == 3
  //               ? "003" : i == 4
  //               ? "011" : i == 5
  //               ? "017"
  //               : "",
  //           "incomeValue": i == 0
  //               ? _providerIncome.controllerIncome.text : i == 1
  //               ? _providerIncome.controllerSpouseIncome.text : i == 2
  //               ? _providerIncome.controllerOtherIncome.text : i == 3
  //               ? _providerIncome.controllerTotalIncome.text : i == 4
  //               ? _providerIncome.controllerCostOfLiving.text : i == 5
  //               ? _providerIncome.controllerRestIncome.text
  //               : _providerIncome.controllerOtherInstallments.text,
  //           "dataStatus": "ACTIVE",
  //           "creationalSpecification": {
  //             "createdAt": DateTime.now(),
  //             "createdBy": _preferences.getString("username"),
  //             "modifiedAt": DateTime.now(),
  //             "modifiedBy": _preferences.getString("username"),
  //           },
  //         }
  //     );
  //   }
  //
  //   return _customerIncomes;
  // }
  //
  // List mappingCustomerIncomeCompany (BuildContext context) {
  //   var _providerIncome = Provider.of<FormMCompanyPendapatanChangeNotifier>(context, listen: false);
  //
  //   var _customerIncomes = [];
  //   for(int i=0; i < 10; i++) {
  //     _customerIncomes.add(
  //         {
  //           "customerIncomeID": "NEW",
  //           "incomeFrmlType": i == 0
  //               ? "S" : i == 1
  //               ? "+" : i == 2
  //               ? "=" : i == 3
  //               ? "-" : i == 4
  //               ? "=" : i == 5
  //               ? "-" : i == 6
  //               ? "-" : i == 7
  //               ? "=" : i == 8
  //               ? "-" : i == 9
  //               ? "="
  //               : "",
  //           "incomeType": i == 0
  //               ? "001" : i == 1
  //               ? "002" : i == 2
  //               ? "003" : i == 3
  //               ? "004" : i == 4
  //               ? "005" : i == 5
  //               ? "006" : i == 6
  //               ? "007" : i == 7
  //               ? "008" : i == 8
  //               ? "" : i == 9
  //               ? ""
  //               : "",
  //           "incomeValue": i == 0
  //               ? _providerIncome.controllerMonthlyIncome.text : i == 1
  //               ? _providerIncome.controllerOtherIncome.text : i == 2
  //               ? _providerIncome.controllerTotalIncome.text : i == 3
  //               ? _providerIncome.controllerCostOfRevenue.text : i == 4
  //               ? _providerIncome.controllerGrossProfit.text : i == 5
  //               ? _providerIncome.controllerOperatingCosts.text : i == 6
  //               ? _providerIncome.controllerOtherCosts.text : i == 7
  //               ? _providerIncome.controllerNetProfitBeforeTax.text : i == 8
  //               ? _providerIncome.controllerTax.text : i == 9
  //               ? _providerIncome.controllerNetProfitAfterTax.text
  //               : _providerIncome.controllerOtherInstallments.text,
  //           "dataStatus": "ACTIVE",
  //           "creationalSpecification": {
  //             "createdAt": DateTime.now(),
  //             "createdBy": _preferences.getString("username"),
  //             "modifiedAt": DateTime.now(),
  //             "modifiedBy": _preferences.getString("username"),
  //           },
  //         }
  //     );
  //   }
  //
  //   return _customerIncomes;
  // }

  // url masih belum dr getConfig terbaru
  void generateAplikasiPayung(BuildContext context) async {
    var _providerMarketingNotes = Provider.of<MarketingNotesChangeNotifier>(context, listen: false);
    var _providerSurvey = Provider.of<ResultSurveyChangeNotifier>(context, listen: false);
    var _providerPhotoSurvey = Provider.of<ListSurveyPhotoChangeNotifier>(context, listen: false);
    _providerMarketingNotes.saveToSQLite();
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    if(_preferences.getString("last_known_state") == "IDE"){
      loadData = true;
      messageProcess = "Proses generate payung...";
      List _dataNoPayung = await _dbHelper.selectDataNoPayung();
      print("cek no payung $_dataNoPayung");
      if(_dataNoPayung.isEmpty){
        final ioc = new HttpClient();
        ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
        final _http = IOClient(ioc);

        var _body = jsonEncode({
          "unitId" : _preferences.getString("branchid"),
          "columnName" : "AC_APPL_NO",
          "objectGroupId" : "",
          "finTypeId" : "",
          "noAppUnit" : ""
          // "unitId":_preferences.getString("branchid"),
          // "columnName":"AC_APPL_NO",
          // "objectGroupId":"",//_providerUnitObject.groupObjectSelected != null ? _providerUnitObject.groupObjectSelected.KODE : ""
          // "finTypeTid":"",//_preferences.getString("cust_type") == "PER" ? _providerFoto.typeOfFinancingModelSelected.financingTypeId : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId
          // "noAppUnit":""
        });

        // String urlAcction = await storage.read(key: "urlAcction");
        String _payungDanNomerUnit = await storage.read(key: "PayungDanNomerUnit");
        // print("${urlAcction}adira-acction-prod/acction/service/order/generateID");
        final _response = await _http.post(
          // "${BaseUrl.urlAcction}adira-acction-062036/acction/service/order/generateID",
          //   "${BaseUrl.urlGeneral}adira-acction-prod/acction/service/order/generateID",
          "${BaseUrl.urlGeneral}$_payungDanNomerUnit",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
        );

        if(_response.statusCode == 200 || _response.statusCode == 201){
          print(_response.body);
          print("payung masuk if response");
          loadData = false;
          _showSnackBarSuccess("Berhasil generate nomer aplikasi payung");
          final _result = jsonDecode(_response.body);
          var _orderId = _result['message'];
          await _dbHelper.insertDataNoPayung(_orderId);
          generateAplikasiUnit(context, _orderId);
        }
        else{
          loadData = false;
          print(_response.statusCode);
          print("payung masuk else response");
          _showSnackBar("Error generate no payung${_response.statusCode}");
        }
      }
      else{
        loadData = false;
        print("payung masuk else");
        generateAplikasiUnit(context, _dataNoPayung[0]['no_aplikasi_payung']);
      }
    }
    else{
      if((_preferences.getString("last_known_state") == "AOS" || _preferences.getString("last_known_state") == "CONA") && _providerSurvey.radioValueApproved == "0"){
        calculate(context, "", "");
      }
      else {
        if(_providerSurvey.resultSurveySelected.KODE == "000") {
          if(_providerSurvey.flagResultSurveyGroupNotes && _providerSurvey.flagResultSurveyGroupLocation && _providerSurvey.flagResultSurveyCreateEditDetailSurvey && _providerSurvey.flagResultSurveyAsset && _providerPhotoSurvey.isFlagSurveyPhoto){
            calculate(context, "", "");
          }
          else{
            if(!_providerSurvey.flagResultSurveyGroupNotes) {
              _providerSurvey.autoValidateResultSurveyGroupNote = true;
            }
            if(!_providerSurvey.flagResultSurveyGroupLocation) {
              _providerSurvey.autoValidateResultSurveyGroupLocation = true;
            }
            if(!_providerSurvey.flagResultSurveyCreateEditDetailSurvey) {
              _providerSurvey.checkDataResultSurveyCreateEditDetailSurvey = true;
            }
            if(!_providerSurvey.flagResultSurveyAsset) {
              _providerSurvey.checkDataResultSurveyAsset = true;
            }
            if(!_providerPhotoSurvey.isFlagSurveyPhoto) {
              _providerPhotoSurvey.checkDataSurveyPhoto = true;
            }
          }
        } else {
          if(_providerSurvey.flagResultSurveyGroupNotes) {
            calculate(context, "", "");
          } else {
            _providerSurvey.autoValidateResultSurveyGroupNote = true;
          }
        }
      }
    }
  }

  void generateAplikasiUnit(BuildContext context, String orderId) async {
    loadData = true;
    messageProcess = "Proses generate no unit...";
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    List _dataNoUnit = await _dbHelper.selectDataNoUnit();
    print("cek no unit $_dataNoUnit");
    print("cek no unit data ${_dataNoUnit[0]['no_unit'] == null}");
    if(_dataNoUnit[0]['no_unit'] == null){
      print("unit masuk if");
      final ioc = new HttpClient();
      ioc.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;

      final _http = IOClient(ioc);

      var _body = jsonEncode({
        "unitId" : _preferences.getString("branchid"),
        "columnName" : "AC_APPL_NO",
        "objectGroupId" : "",
        "finTypeId" : "",
        "noAppUnit" : ""
        // "unitId" : _preferences.getString("branchid"),
        // "columnName" : "AC_APPL_NO",
        // "objectGroupId" : "", //_providerUnitObject.groupObjectSelected != null ? _providerUnitObject.groupObjectSelected.KODE : ""
        // "finTypeTid" : "", //_preferences.getString("cust_type") == "PER" ? _providerFoto.typeOfFinancingModelSelected.financingTypeId : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId
        // "noAppUnit" : ""
      });
      // String urlAcction = await storage.read(key: "urlAcction");
      String _payungDanNomerUnit = await storage.read(key: "PayungDanNomerUnit");
      final _response = await _http.post(
        // "${BaseUrl.urlAcction}adira-acction-062036/acction/service/order/generateID",
        //   "${BaseUrl.urlGeneral}adira-acction-prod/acction/service/order/generateID",
        "${BaseUrl.urlGeneral}$_payungDanNomerUnit",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      );

      print("unit ${_response.statusCode}");
      if(_response.statusCode == 200 || _response.statusCode == 201){
        print("unit ${_response.body}");
        print("unit masuk if response");
        loadData = false;
        _showSnackBarSuccess("Berhasil generate nomer aplikasi unit");
        final _result = jsonDecode(_response.body);
        var _orderProductId = _result['message'];
        await _dbHelper.updateDataNoUnit(_orderProductId);
        calculate(context,orderId,_orderProductId);
        // submitFotoToECM(context,orderId,_orderProductId);
      }
      else{
        loadData = false;
        // print(_response.statusCode);
        print("payung masuk else response");
        _showSnackBar("Error generate no unit ${_response.statusCode}");
      }
    }
    else{
      loadData = false;
      submitFotoToECM(context, orderId, _dataNoUnit[0]['no_unit']);
    }
  }

  void calculate(BuildContext context,String orderId,String orderProductId) async {
    try{
      loadData = true;
      await Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).calculateCreditNew(context);
      await Provider.of<InfoCreditIncomeChangeNotifier>(context, listen: false).getDSR(context);
      loadData = false;
      submitFotoToECM(context, orderId, orderProductId);
    }
    catch(e){
      loadData = false;
      dialogFailedSavePartial(context, e.toString());
    }
  }

  void submitFotoToECM(BuildContext context, String orderId, String orderProductId) async {
    loadData = true;
    messageProcess = "Proses submit ECM...";
    _urlUploadFile = await storage.read(key: "UploadFile");
    this._orderSupportingDocuments.clear();
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    DateTime _timeStartValidate = DateTime.now();
    String _orderNo = _preferences.getString("order_no");
    var _providerDocument = Provider.of<InfoDocumentChangeNotifier>(context,listen: false);
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
    var _providerResultSurvey = Provider.of<ListSurveyPhotoChangeNotifier>(context,listen: false);
    var _dio = dio.Dio();
    (_dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate  = (client) {
      client.badCertificateCallback=(X509Certificate cert, String host, int port){
        return true;
      };
    };
    //error jika documentTitle menggunakan ":"
    //dibuat langsung masuk else
    if(_preferences.getString("last_known_state") == ""){
      // List _data = await _dbHelper.selectMS2Document();
      // // debugPrint("DATA_DOKUMEN$_data");
      // if(_data.isNotEmpty){
      //
      //   for(int i=0; i < _data.length; i++){
      //     // debugPrint("CEK ID DOCUMENT ${_data[i]['orderSupportingDocumentID']}");
      //     _orderSupportingDocuments.add({
      //       "orderSupportingDocumentID": _data[i]['orderSupportingDocumentID'],
      //       "documentSpecification": {
      //         "documentTypeID": _data[i]['document_type_id'],// harcode
      //         "isMandatory": _data[i]['mandatory'], // 0 = true | 1 = false
      //         "isDisplay": _data[i]['display'], // 0 = true | 1 = false
      //         "isUnit": _data[i]['flag_unit'], // 0 = true | 1 = false
      //         "receivedDate": formatDateValidateAndSubmit(DateTime.now()),// tgl terima dokumen tidak ada di bagian photo tempat tiggal,tempat usaha dan group object unit
      //       },
      //       "fileSpecification": {
      //         "fileName": _data[i]['file_name'],
      //         "fileHeaderID": _data[i]['file_header_id'],
      //         "uploadDate": formatDateValidateAndSubmit(DateTime.parse(_data[i]['upload_date'])),
      //         "longitude": "",
      //         "latitude": ""
      //       },
      //       "creationalSpecification": {
      //         "createdAt": formatDateValidateAndSubmit(DateTime.now()),
      //         "createdBy": _preferences.getString("username"),
      //         "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
      //         "modifiedBy": _preferences.getString("username"),
      //       },
      //       "status": "ACTIVE"
      //     });
      //   }
      // }

      // tempat tinggal
      if(_providerFoto.listFotoTempatTinggal.isNotEmpty){
        for(int i=0; i < _providerFoto.listFotoTempatTinggal.length; i++){
          String _docTitle = _providerFoto.listFotoTempatTinggal[i].path.split("/")[_providerFoto.listFotoTempatTinggal[i].path.split("/").length - 1];
          _orderSupportingDocuments.add({
            "orderSupportingDocumentID": "NEW",
            "documentSpecification": {
              "documentTypeID": "SC2",// harcode
              "isMandatory": 1, // 0 = true | 1 = false
              "isDisplay": 1, // 0 = true | 1 = false
              "isUnit": 1, // 0 = true | 1 = false
              "receivedDate": formatDateValidateAndSubmit(DateTime.now()),// tgl terima dokumen tidak ada di bagian photo tempat tiggal,tempat usaha dan group object unit
            },
            "fileSpecification": {
              "fileName": _docTitle,
              "fileHeaderID": _providerFoto.listFotoTempatTinggal[i].fileHeaderID,
              "uploadDate": formatDateValidateAndSubmit(DateTime.now()),
              "longitude": "${_providerFoto.listFotoTempatTinggal[i].longitude}",
              "latitude": "${_providerFoto.listFotoTempatTinggal[i].latitude}"
            },
            "creationalSpecification": {
              "createdAt": formatDateValidateAndSubmit(DateTime.now()),
              "createdBy": _preferences.getString("username"),
              "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
              "modifiedBy": _preferences.getString("username"),
            },
            "status": "ACTIVE"
          });
        }
      }

      // tempat usaha
      if(_providerFoto.listFotoTempatUsaha.isNotEmpty){
        for(int i=0; i < _providerFoto.listFotoTempatUsaha.length; i++){
          String _docTitle = _providerFoto.listFotoTempatUsaha[i].path.split("/")[_providerFoto.listFotoTempatUsaha[i].path.split("/").length - 1];
          _orderSupportingDocuments.add({
            "orderSupportingDocumentID": "NEW",
            "documentSpecification": {
              "documentTypeID": "SC3",// harcode
              "isMandatory": 1, // 0 = true | 1 = false
              "isDisplay": 1, // 0 = true | 1 = false
              "isUnit": 1, // 0 = true | 1 = false
              "receivedDate": formatDateValidateAndSubmit(DateTime.now()),// tgl terima dokumen tidak ada di bagian photo tempat tiggal,tempat usaha dan group object unit
            },
            "fileSpecification": {
              "fileName": _docTitle,
              "fileHeaderID": _providerFoto.listFotoTempatUsaha[i].fileHeaderID,
              "uploadDate": formatDateValidateAndSubmit(DateTime.now()),
              "longitude": "${_providerFoto.listFotoTempatUsaha[i].longitude}",
              "latitude": "${_providerFoto.listFotoTempatUsaha[i].latitude}"
            },
            "creationalSpecification": {
              "createdAt": formatDateValidateAndSubmit(DateTime.now()),
              "createdBy": _preferences.getString("username"),
              "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
              "modifiedBy": _preferences.getString("username"),
            },
            "status": "ACTIVE"
          });
        }
      }

      // group object unit
      if(_providerFoto.listGroupUnitObject.isNotEmpty){
        for(int i=0; i < _providerFoto.listGroupUnitObject.length; i++){
          if(_providerFoto.listGroupUnitObject[i].listImageGroupObjectUnit.isNotEmpty){
            for(int j=0; j < _providerFoto.listGroupUnitObject[i].listImageGroupObjectUnit.length; j++){
              String _docTitle = _providerFoto.listGroupUnitObject[i].listImageGroupObjectUnit[j].path.split("/")[
              _providerFoto.listGroupUnitObject[i].listImageGroupObjectUnit[j].path.split("/").length - 1];
              _orderSupportingDocuments.add({
                "orderSupportingDocumentID": "NEW",
                "documentSpecification": {
                  "documentTypeID": "SC1",// harcode
                  "isMandatory": 1, // 0 = true | 1 = false
                  "isDisplay": 1, // 0 = true | 1 = false
                  "isUnit": 1, // 0 = true | 1 = false
                  "receivedDate": formatDateValidateAndSubmit(DateTime.now()),// tgl terima dokumen tidak ada di bagian photo tempat tiggal,tempat usaha dan group object unit
                },
                "fileSpecification": {
                  "fileName": _docTitle,
                  "fileHeaderID": _providerFoto.listGroupUnitObject[i].listImageGroupObjectUnit[j].fileHeaderID,
                  "uploadDate": formatDateValidateAndSubmit(DateTime.now()),
                  "longitude": "${_providerFoto.listGroupUnitObject[i].listImageGroupObjectUnit[j].longitude}",
                  "latitude": "${_providerFoto.listGroupUnitObject[i].listImageGroupObjectUnit[j].latitude}"
                },
                "creationalSpecification": {
                  "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                  "createdBy": _preferences.getString("username"),
                  "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                  "modifiedBy": _preferences.getString("username"),
                },
                "status": "ACTIVE"
              });
            }
          }
        }
      }

      // document group object unit
      if(_providerFoto.listDocument.isNotEmpty){
        for(int i=0; i < _providerFoto.listDocument.length; i++){
          print("cek docTypeId document group object unit ${"${_providerFoto.listDocument[i].jenisDocument.docTypeId}"}");
          // List _data = await getListMetadata("${_providerFoto.listDocument[i].jenisDocument.docTypeId}");
          String _docTitle = _providerFoto.listDocument[i].path.split("/")[_providerFoto.listDocument[i].path.split("/").length - 1];
          _orderSupportingDocuments.add({
            "orderSupportingDocumentID": _providerFoto.listDocument[i].orderSupportingDocumentID,
            "documentSpecification": {
              "documentTypeID": _providerFoto.listDocument[i].jenisDocument.docTypeId,
              "isMandatory": _providerFoto.listDocument[i].jenisDocument.mandatory,
              "isDisplay": _providerFoto.listDocument[i].jenisDocument.display,
              "isUnit": _providerFoto.listDocument[i].jenisDocument.flag_unit,
              "receivedDate": formatDateValidateAndSubmit(_providerFoto.listDocument[i].dateTime),
            },
            "fileSpecification": {
              "fileName": _docTitle,
              "fileHeaderID": _providerFoto.listDocument[i].fileHeaderID,
              "uploadDate": formatDateValidateAndSubmit(DateTime.now()),
              "longitude": "${_providerFoto.listDocument[i].longitude}",
              "latitude": "${_providerFoto.listDocument[i].latitude}"
            },
            "creationalSpecification": {
              "createdAt": formatDateValidateAndSubmit(DateTime.now()),
              "createdBy": _preferences.getString("username"),
              "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
              "modifiedBy": _preferences.getString("username"),
            },
            "status": "ACTIVE"
          });
        }
      }

      // info document
      if(_providerDocument.listInfoDocument.isNotEmpty){
        print("masuk if info document");
        for(int i=0; i < _providerDocument.listInfoDocument.length; i++){
          print("cek docTypeId infoDocument ${_providerDocument.listInfoDocument[i].documentType.docTypeId}");
          String _docTitle = _providerDocument.listInfoDocument[i].fileName.split("/").last;
          _orderSupportingDocuments.add({
            "orderSupportingDocumentID": _providerDocument.listInfoDocument[i].orderSupportingDocumentID,
            "documentSpecification": {
              "documentTypeID": _providerDocument.listInfoDocument[i].documentType.docTypeId,
              "isMandatory": _providerDocument.listInfoDocument[i].documentType.mandatory,
              "isDisplay": _providerDocument.listInfoDocument[i].documentType.display,
              "isUnit": _providerDocument.listInfoDocument[i].documentType.flag_unit,
              "receivedDate": formatDateValidateAndSubmit(DateTime.parse(_providerDocument.listInfoDocument[i].date)),//_providerDocument.listInfoDocument[i].date,
            },
            "fileSpecification": {
              "fileName": _docTitle,
              "fileHeaderID": _providerDocument.listInfoDocument[i].fileHeaderID,
              "uploadDate": formatDateValidateAndSubmit(DateTime.now()),
              "longitude": "${_providerDocument.listInfoDocument[i].longitude}",
              "latitude": "${_providerDocument.listInfoDocument[i].latitude}"
            },
            "creationalSpecification": {
              "createdAt": formatDateValidateAndSubmit(DateTime.now()),
              "createdBy": _preferences.getString("username"),
              "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
              "modifiedBy": _preferences.getString("username"),
            },
            "status": "ACTIVE"
          });
        }
      }

      // photo survey
      if(_providerResultSurvey.listSurveyPhoto.isNotEmpty){
        List _data = await getListMetadata("A8C");
        if(_data.isNotEmpty){
          for(int i=0; i < _providerResultSurvey.listSurveyPhoto.length; i++){
            if(_providerResultSurvey.listSurveyPhoto[i].listImageModel.isNotEmpty){
              for(int j=0; j<_providerResultSurvey.listSurveyPhoto[i].listImageModel.length; j++){
                String _docTitle = _providerResultSurvey.listSurveyPhoto[i].listImageModel[j].path.split("/").last;
                dio.FormData formData = dio.FormData.fromMap({
                  "file" :  await dio.MultipartFile.fromFile(_providerResultSurvey.listSurveyPhoto[i].listImageModel[j].path, filename: _docTitle),
                  "documentTitle" : "${DateFormat("dd-MM-yyyy HH-mm-ss").format(DateTime.now())}-$_orderNo-$_docTitle",
                  "documentType" : _data[0]['symbolName'],
                  "application" : "REVAMPMS2",
                  "objectStore" : "ADIRAOS",
                  "requestId" : "2020369"
                  // "nama" : _preferences.getString("fullname"),
                  // "region" : _preferences.getString("branchid"),
                  // "NIKPIC" : _preferences.getString("username")
                });
                var _bodyPhotoSurvey = jsonEncode({
                  "file" :  await dio.MultipartFile.fromFile(_providerResultSurvey.listSurveyPhoto[i].listImageModel[j].path, filename: _docTitle),
                  "documentTitle" : "${DateFormat("dd-MM-yyyy HH-mm-ss").format(DateTime.now())}-$_orderNo-$_docTitle",
                  "documentType" : _data[0]['symbolName'],
                  "application" : "REVAMPMS2",
                  "objectStore" : "ADIRAOS",
                  "requestId" : "2020369"
                });
                try{
                  var _response = await _dio.post(
                      "${BaseUrl.urlECM}$_urlUploadFile",
                      // "http://10.50.3.123:8080/api/v1/files",
                      data: formData,
                      options: dio.Options(
                        headers: {"x-api-key": xApiKey, "Authorization":"bearer $token"}, // M1I02YSVDX3ZCV4NF1CBUSXWSKD204
                      )
                  );
                  insertLog(context,_timeStartValidate,DateTime.now(),_bodyPhotoSurvey,"Start Upload Foto ${_data[0]['symbolName']}","Start Upload Foto ${_data[0]['symbolName']}");
                  if(_response.statusCode != 200){
                    loadData = false;
                    // _showSnackBar("Error ECM photo survey ${_response.statusCode}");
                    _showSnackBar("Problem koneksi ke ECM. Mohon ditunggu beberapa saat untuk submit kembali");
                    messageProcess = "";
                    return;
                  }
                  else{
                    _orderSupportingDocuments.add({
                      "orderSupportingDocumentID": "NEW",
                      "documentSpecification": {
                        "documentTypeID": "A8C",
                        "isMandatory": 1, // 0 = true | 1 = false
                        "isDisplay": 1, // 0 = true | 1 = false
                        "isUnit": 1, // 0 = true | 1 = false
                        "receivedDate": formatDateValidateAndSubmit(DateTime.now()),// tidak ada field tgl terima dokumen
                      },
                      "fileSpecification": {
                        "fileName": _docTitle,
                        "fileHeaderID": _response.data['objectId'].toString().replaceAll("{}", ""),
                        "uploadDate": formatDateValidateAndSubmit(DateTime.now()),
                        "longitude": "${_providerResultSurvey.listSurveyPhoto[i].listImageModel[j].longitude}",
                        "latitude": "${_providerResultSurvey.listSurveyPhoto[i].listImageModel[j].latitude}"
                      },
                      "creationalSpecification": {
                        "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                        "createdBy": _preferences.getString("username"),
                        "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                        "modifiedBy": _preferences.getString("username"),
                      },
                      "status": "ACTIVE"
                    });
                  }
                }
                catch(e){
                  loadData = false;
                  _showSnackBar("Error submit ECM foto survey ${e.toString()}");
                  return;
                }
              }
            }
          }
        }
        else{
          loadData = false;
          _showSnackBar("Error get metadata hasil survey");
          return;
        }
      }
    }
    else{
      // tempat tinggal
      if(_providerFoto.listFotoTempatTinggal.isNotEmpty){
        List _data = await getListMetadata("SC2");
        if(_data.isNotEmpty){
          for(int i=0; i < _providerFoto.listFotoTempatTinggal.length; i++){
            String _docTitle = _providerFoto.listFotoTempatTinggal[i].path.split("/")[_providerFoto.listFotoTempatTinggal[i].path.split("/").length - 1];
            dio.FormData formData = dio.FormData.fromMap({
              "file" :  await dio.MultipartFile.fromFile(_providerFoto.listFotoTempatTinggal[i].path, filename: _providerFoto.listFotoTempatTinggal[i].path.split("/").last),
              "documentTitle" : "${DateFormat("dd-MM-yyyy HH-mm-ss").format(DateTime.now())}-$_orderNo-$_docTitle",
              "documentType" : _data[0]['symbolName'],//hardcode symbolicName
              "application" : "REVAMPMS2",
              "objectStore" : "ADIRAOS",
              "requestId" : "2020369"
            });
            var _bodyTempatTinggal = jsonEncode({
              "file" :  "",
              "documentTitle" : "${DateFormat("dd-MM-yyyy HH-mm-ss").format(DateTime.now())}-$_orderNo-$_docTitle",
              "documentType" : _data[0]['symbolName'],//hardcode symbolicName
              "application" : "REVAMPMS2",
              "objectStore" : "ADIRAOS",
              "requestId" : "2020369"
            });
            if(_providerFoto.listFotoTempatTinggal[i].fileHeaderID != ""){
              _orderSupportingDocuments.add({
                "orderSupportingDocumentID": "NEW",
                "documentSpecification": {
                  "documentTypeID": "SC2",// harcode
                  "isMandatory": 1, // 0 = true | 1 = false
                  "isDisplay": 1, // 0 = true | 1 = false
                  "isUnit": 1, // 0 = true | 1 = false
                  "receivedDate": formatDateValidateAndSubmit(DateTime.now()),// tgl terima dokumen tidak ada di bagian photo tempat tiggal,tempat usaha dan group object unit
                },
                "fileSpecification": {
                  "fileName": _docTitle,
                  "fileHeaderID": _providerFoto.listFotoTempatTinggal[i].fileHeaderID,
                  "uploadDate": formatDateValidateAndSubmit(DateTime.now()),
                  "longitude": "${_providerFoto.listFotoTempatTinggal[i].longitude}",
                  "latitude": "${_providerFoto.listFotoTempatTinggal[i].latitude}"
                },
                "creationalSpecification": {
                  "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                  "createdBy": _preferences.getString("username"),
                  "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                  "modifiedBy": _preferences.getString("username"),
                },
                "status": "ACTIVE"
              });
            }
            else{
              try{
                print("cek url storage ${BaseUrl.urlECM}$_urlUploadFile");
                var _response = await _dio.post(
                    "${BaseUrl.urlECM}$_urlUploadFile",
                    // "http://10.50.3.123:8080/api/v1/files",
                    data: formData,
                    options: dio.Options(
                      headers: {"Accept":"application/json", "x-api-key": xApiKey, "Authorization":"bearer $token"}, // M1I02YSVDX3ZCV4NF1CBUSXWSKD204
                    )
                );
                insertLog(context,_timeStartValidate,DateTime.now(),_bodyTempatTinggal,"Start Upload Foto ${_data[0]['symbolName']}","Start Upload Foto ${_data[0]['symbolName']}");
                if(_response.statusCode != 200){
                  loadData = false;
                  // _showSnackBar("Error ECM tempat tinggal response ${_response.statusCode}");
                  _showSnackBar("Problem koneksi ke ECM. Mohon ditunggu beberapa saat untuk submit kembali");
                  messageProcess = "";
                  return;
                }
                else{
                  print("result tempat tinggal = ${_response.data}");
                  if(_response.data['status'] == 200){
                    _orderSupportingDocuments.add({
                      "orderSupportingDocumentID": "NEW",
                      "documentSpecification": {
                        "documentTypeID": "SC2",// harcode
                        "isMandatory": 1, // 0 = true | 1 = false
                        "isDisplay": 1, // 0 = true | 1 = false
                        "isUnit": 1, // 0 = true | 1 = false
                        "receivedDate": formatDateValidateAndSubmit(DateTime.now()),// tgl terima dokumen tidak ada di bagian photo tempat tiggal,tempat usaha dan group object unit
                      },
                      "fileSpecification": {
                        "fileName": _docTitle,
                        "fileHeaderID": _response.data['objectId'].toString().replaceAll("{}", ""),
                        "uploadDate": formatDateValidateAndSubmit(DateTime.now()),
                        "longitude": "${_providerFoto.listFotoTempatTinggal[i].longitude}",
                        "latitude": "${_providerFoto.listFotoTempatTinggal[i].latitude}"
                      },
                      "creationalSpecification": {
                        "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                        "createdBy": _preferences.getString("username"),
                        "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                        "modifiedBy": _preferences.getString("username"),
                      },
                      "status": "ACTIVE"
                    });
                  }
                  else{
                    loadData = false;
                    // _showSnackBar("Error ECM tempat tinggal response ${_response.statusCode}");
                    _showSnackBar("Problem koneksi ke ECM. Mohon ditunggu beberapa saat untuk submit kembali");
                    messageProcess = "";
                    return;
                  }
                  // _listObjectIdTempatTinggal.add(_response.data['objectId'].toString().replaceAll("{}", ""));
                }
              }
              catch(e){
                loadData = false;
                _showSnackBar("Error submit ECM tempat tinggal ${e.toString()}");
                return;
              }
            }
          }
        }
        else{
          loadData = false;
          _showSnackBar("Error get metadata tempat tinggal");
          return;
        }
      }

      // tempat usaha
      if(_providerFoto.listFotoTempatUsaha.isNotEmpty){
        List _data = await getListMetadata("SC3");
        if(_data.isNotEmpty){
          for(int i=0; i < _providerFoto.listFotoTempatUsaha.length; i++){
            String _docTitle = _providerFoto.listFotoTempatUsaha[i].path.split("/")[_providerFoto.listFotoTempatUsaha[i].path.split("/").length - 1];
            dio.FormData formData = dio.FormData.fromMap({
              "file" :  await dio.MultipartFile.fromFile(_providerFoto.listFotoTempatUsaha[i].path, filename: _providerFoto.listFotoTempatUsaha[i].path.split("/").last),
              "documentTitle" : "${DateFormat("dd-MM-yyyy HH-mm-ss").format(DateTime.now())}-$_orderNo-$_docTitle",
              "documentType" : _data[0]['symbolName'],
              "application" : "REVAMPMS2",
              "objectStore" : "ADIRAOS",
              "requestId" : "2020369"
              // "nama" : _preferences.getString("fullname"),
              // "region" : _preferences.getString("branchid"),
              // "NIKPIC" : _preferences.getString("username")
            });
            var _bodyTempatUsaha = jsonEncode({
              "documentTitle" : "${DateFormat("dd-MM-yyyy HH-mm-ss").format(DateTime.now())}-$_orderNo-$_docTitle",
              "documentType" : _data[0]['symbolName'],
              "application" : "REVAMPMS2",
              "objectStore" : "ADIRAOS",
              "requestId" : "2020369"
            });
            if(_providerFoto.listFotoTempatUsaha[i].fileHeaderID != ""){
              _orderSupportingDocuments.add({
                "orderSupportingDocumentID": "NEW",
                "documentSpecification": {
                  "documentTypeID": "SC3",// harcode
                  "isMandatory": 1, // 0 = true | 1 = false
                  "isDisplay": 1, // 0 = true | 1 = false
                  "isUnit": 1, // 0 = true | 1 = false
                  "receivedDate": formatDateValidateAndSubmit(DateTime.now()),// tgl terima dokumen tidak ada di bagian photo tempat tiggal,tempat usaha dan group object unit
                },
                "fileSpecification": {
                  "fileName": _docTitle,
                  "fileHeaderID": _providerFoto.listFotoTempatUsaha[i].fileHeaderID,
                  "uploadDate": formatDateValidateAndSubmit(DateTime.now()),
                  "longitude": "${_providerFoto.listFotoTempatUsaha[i].longitude}",
                  "latitude": "${_providerFoto.listFotoTempatUsaha[i].latitude}"
                },
                "creationalSpecification": {
                  "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                  "createdBy": _preferences.getString("username"),
                  "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                  "modifiedBy": _preferences.getString("username"),
                },
                "status": "ACTIVE"
              });
            }
            else{
              try{
                var _response = await _dio.post(
                  // "http://10.50.3.123:8080/api/v1/files",
                    "${BaseUrl.urlECM}$_urlUploadFile",
                    data: formData,
                    options: dio.Options(
                      headers: {"x-api-key": xApiKey, "Authorization":"bearer $token"}, // M1I02YSVDX3ZCV4NF1CBUSXWSKD204
                    )
                );
                insertLog(context,_timeStartValidate,DateTime.now(),_bodyTempatUsaha,"Start Upload Foto ${_data[0]['symbolName']}","Start Upload Foto ${_data[0]['symbolName']}");
                if(_response.statusCode != 200){
                  loadData = false;
                  // _showSnackBar("Error ECM tempat usaha response ${_response.statusCode}");
                  _showSnackBar("Problem koneksi ke ECM tempat usaha. Mohon ditunggu beberapa saat untuk submit kembali");
                  messageProcess = "";
                  return;
                }
                else{
                  print("result tempat usaha = ${_response.data}");
                  if(_response.data['status'] == 200){
                    _orderSupportingDocuments.add({
                      "orderSupportingDocumentID": "NEW",
                      "documentSpecification": {
                        "documentTypeID": "SC3",// harcode
                        "isMandatory": 1, // 0 = true | 1 = false
                        "isDisplay": 1, // 0 = true | 1 = false
                        "isUnit": 1, // 0 = true | 1 = false
                        "receivedDate": formatDateValidateAndSubmit(DateTime.now()),// tgl terima dokumen tidak ada di bagian photo tempat tiggal,tempat usaha dan group object unit
                      },
                      "fileSpecification": {
                        "fileName": _docTitle,
                        "fileHeaderID": _response.data['objectId'].toString().replaceAll("{}", ""),
                        "uploadDate": formatDateValidateAndSubmit(DateTime.now()),
                        "longitude": "${_providerFoto.listFotoTempatUsaha[i].longitude}",
                        "latitude": "${_providerFoto.listFotoTempatUsaha[i].latitude}"
                      },
                      "creationalSpecification": {
                        "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                        "createdBy": _preferences.getString("username"),
                        "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                        "modifiedBy": _preferences.getString("username"),
                      },
                      "status": "ACTIVE"
                    });
                  }
                  else{
                    loadData = false;
                    // _showSnackBar("Error ECM tempat tinggal response ${_response.statusCode}");
                    _showSnackBar("Problem koneksi ke ECM. Mohon ditunggu beberapa saat untuk submit kembali");
                    messageProcess = "";
                    return;
                  }
                  // _listObjectIdTempatUsaha.add(_response.data['objectId'].toString().replaceAll("{}", ""));
                }
              }
              catch(e){
                loadData = false;
                _showSnackBar("Error submit ECM tempat usaha ${e.toString()}");
                return;
              }
            }
          }
        }
        else{
          loadData = false;
          _showSnackBar("Error get metadata tempat usaha");
          return;
        }
      }

      // group object unit
      if(_providerFoto.listGroupUnitObject.isNotEmpty){
        for(int i=0; i < _providerFoto.listGroupUnitObject.length; i++){
          if(_providerFoto.listGroupUnitObject[i].listImageGroupObjectUnit.isNotEmpty){
            for(int j=0; j < _providerFoto.listGroupUnitObject[i].listImageGroupObjectUnit.length; j++){
              List _data = await getListMetadata("SC1");
              if(_data.isNotEmpty){
                String _docTitle = _providerFoto.listGroupUnitObject[i].listImageGroupObjectUnit[j].path.split("/")[
                _providerFoto.listGroupUnitObject[i].listImageGroupObjectUnit[j].path.split("/").length - 1];
                dio.FormData formData = dio.FormData.fromMap({
                  "file" :  await dio.MultipartFile.fromFile(_providerFoto.listGroupUnitObject[i].listImageGroupObjectUnit[j].path, filename: _providerFoto.listGroupUnitObject[i].listImageGroupObjectUnit[j].path.split("/").last,contentType: new MediaType("image", "jpeg"),),
                  "documentTitle" : "${DateFormat("dd-MM-yyyy HH-mm-ss").format(DateTime.now())}-$_orderNo-$_docTitle",
                  "documentType" : _data[0]['symbolName'],//hardcode symbolicName
                  "application" : "REVAMPMS2",
                  "objectStore" : "ADIRAOS",
                  "requestId" : "2020369"
                  // "nama" : _preferences.getString("fullname"),
                  // "region" : _preferences.getString("branchid"),
                  // "NIKPIC" : _preferences.getString("username")
                });
                var _bodyGroupObjectUnit = jsonEncode({
                  "documentTitle" : "${DateFormat("dd-MM-yyyy HH-mm-ss").format(DateTime.now())}-$_orderNo-$_docTitle",
                  "documentType" : _data[0]['symbolName'],//hardcode symbolicName
                  "application" : "REVAMPMS2",
                  "objectStore" : "ADIRAOS",
                  "requestId" : "2020369"
                });
                if(_providerFoto.listGroupUnitObject[i].listImageGroupObjectUnit[j].fileHeaderID != ""){
                  _orderSupportingDocuments.add({
                    "orderSupportingDocumentID": "NEW",
                    "documentSpecification": {
                      "documentTypeID": "SC1",// harcode
                      "isMandatory": 1, // 0 = true | 1 = false
                      "isDisplay": 1, // 0 = true | 1 = false
                      "isUnit": 1, // 0 = true | 1 = false
                      "receivedDate": formatDateValidateAndSubmit(DateTime.now()),// tgl terima dokumen tidak ada di bagian photo tempat tiggal,tempat usaha dan group object unit
                    },
                    "fileSpecification": {
                      "fileName": _docTitle,
                      "fileHeaderID": _providerFoto.listGroupUnitObject[i].listImageGroupObjectUnit[j].fileHeaderID,
                      "uploadDate": formatDateValidateAndSubmit(DateTime.now()),
                      "longitude": "${_providerFoto.listGroupUnitObject[i].listImageGroupObjectUnit[j].longitude}",
                      "latitude": "${_providerFoto.listGroupUnitObject[i].listImageGroupObjectUnit[j].latitude}"
                    },
                    "creationalSpecification": {
                      "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                      "createdBy": _preferences.getString("username"),
                      "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                      "modifiedBy": _preferences.getString("username"),
                    },
                    "status": "ACTIVE"
                  });
                }
                else{
                  try{
                    var _response = await _dio.post(
                        "${BaseUrl.urlECM}$_urlUploadFile",
                        // "http://10.50.3.123:8080/api/v1/files",
                        data: formData,
                        options: dio.Options(
                          headers: {"x-api-key": xApiKey, "Authorization":"bearer $token"}, // M1I02YSVDX3ZCV4NF1CBUSXWSKD204
                        )
                    );
                    print("cek reponse ECM ${_response.data}");
                    insertLog(context,_timeStartValidate,DateTime.now(),_bodyGroupObjectUnit,"Start Upload Foto ${_data[0]['symbolName']}","Start Upload Foto ${_data[0]['symbolName']}");
                    if(_response.statusCode != 200){
                      loadData = false;
                      // _showSnackBar("Error ECM group object unit response ${_response.statusCode}");
                      _showSnackBar("Problem koneksi ke ECM. Mohon ditunggu beberapa saat untuk submit kembali");
                      messageProcess = "";
                      return;
                    }
                    else{
                      print("result grup objek unit = ${_response.data}");
                      if(_response.data['status'] == 200){
                        _orderSupportingDocuments.add({
                          "orderSupportingDocumentID": "NEW",
                          "documentSpecification": {
                            "documentTypeID": "SC1",// harcode
                            "isMandatory": 1, // 0 = true | 1 = false
                            "isDisplay": 1, // 0 = true | 1 = false
                            "isUnit": 1, // 0 = true | 1 = false
                            "receivedDate": formatDateValidateAndSubmit(DateTime.now()),// tgl terima dokumen tidak ada di bagian photo tempat tiggal,tempat usaha dan group object unit
                          },
                          "fileSpecification": {
                            "fileName": _docTitle,
                            "fileHeaderID": _response.data['objectId'].toString().replaceAll("{}", ""),
                            "uploadDate": formatDateValidateAndSubmit(DateTime.now()),
                            "longitude": "${_providerFoto.listGroupUnitObject[i].listImageGroupObjectUnit[j].longitude}",
                            "latitude": "${_providerFoto.listGroupUnitObject[i].listImageGroupObjectUnit[j].latitude}"
                          },
                          "creationalSpecification": {
                            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                            "createdBy": _preferences.getString("username"),
                            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                            "modifiedBy": _preferences.getString("username"),
                          },
                          "status": "ACTIVE"
                        });
                      }
                      else{
                        loadData = false;
                        // _showSnackBar("Error ECM tempat tinggal response ${_response.statusCode}");
                        _showSnackBar("Problem koneksi ke ECM. Mohon ditunggu beberapa saat untuk submit kembali");
                        messageProcess = "";
                        return;
                      }
                      // _listObjectIdGroupUnitObject.add(_response.data['objectId'].toString().replaceAll("{}", ""));
                    }
                  }
                  catch(e){
                    loadData = false;
                    _showSnackBar("Error submit ECM grup objek unit ${e.toString()}");
                    return;
                  }
                }
              }
              else{
                loadData = false;
                _showSnackBar("Error get metadata group object unit");
                return;
              }
            }
          }
        }
      }

      // document group object unit
      if(_providerFoto.listDocument.isNotEmpty){
        for(int i=0; i < _providerFoto.listDocument.length; i++){
          print("cek docTypeId document group object unit s ${"${_providerFoto.listDocument[i].jenisDocument.docTypeId}"} = ${_providerFoto.listDocument[i].orderSupportingDocumentID}");
          List _data = await getListMetadata("${_providerFoto.listDocument[i].jenisDocument.docTypeId}");
          if(_data.isNotEmpty){
            print("cek path = ${_providerFoto.listDocument[i].path}");
            String _docTitle = _providerFoto.listDocument[i].path.split("/")[_providerFoto.listDocument[i].path.split("/").length - 1];
            print("nama = $_docTitle");
            dio.FormData formData = dio.FormData.fromMap({
              "file" :  await dio.MultipartFile.fromFile(_providerFoto.listDocument[i].path, filename: _docTitle),
              "documentTitle" : "${DateFormat("dd-MM-yyyy HH-mm-ss").format(DateTime.now())}-$_orderNo-$_docTitle",
              "documentType" : _data[0]['symbolName'],
              "application" : "REVAMPMS2",
              "objectStore" : "ADIRAOS",
              "requestId" : "2020369"
            });
            var _bodyDocumentGroupObjectUnit = jsonEncode({
              "documentTitle" : "${DateFormat("dd-MM-yyyy HH-mm-ss").format(DateTime.now())}-$_orderNo-$_docTitle",
              "documentType" : _data[0]['symbolName'],
              "application" : "REVAMPMS2",
              "objectStore" : "ADIRAOS",
              "requestId" : "2020369"
            });
            print("cek dokumen depann = ${_providerFoto.listDocument[i].fileHeaderID}  ==== ${_providerFoto.listDocument[i].orderSupportingDocumentID}");
            if(_providerFoto.listDocument[i].orderSupportingDocumentID != "NEW"){
              _orderSupportingDocuments.add({
                "orderSupportingDocumentID": _providerFoto.listDocument[i].orderSupportingDocumentID,
                "documentSpecification": {
                  "documentTypeID": _providerFoto.listDocument[i].jenisDocument.docTypeId,
                  "isMandatory": _providerFoto.listDocument[i].jenisDocument.mandatory,
                  "isDisplay": _providerFoto.listDocument[i].jenisDocument.display,
                  "isUnit": _providerFoto.listDocument[i].jenisDocument.flag_unit,
                  "receivedDate": formatDateValidateAndSubmit(_providerFoto.listDocument[i].dateTime),
                },
                "fileSpecification": {
                  "fileName": _docTitle,
                  "fileHeaderID": _providerFoto.listDocument[i].fileHeaderID,
                  "uploadDate": formatDateValidateAndSubmit(DateTime.now()),
                  "longitude": "${_providerFoto.listDocument[i].longitude}",
                  "latitude": "${_providerFoto.listDocument[i].latitude}"
                },
                "creationalSpecification": {
                  "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                  "createdBy": _preferences.getString("username"),
                  "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                  "modifiedBy": _preferences.getString("username"),
                },
                "status": "ACTIVE"
              });
            }
            else{
              try{
                var _response = await _dio.post(
                    "${BaseUrl.urlGeneral}$_urlUploadFile",
                    data: formData,
                    options: dio.Options(
                      headers: {"x-api-key": xApiKey, "Authorization":"bearer $token"}, // M1I02YSVDX3ZCV4NF1CBUSXWSKD204
                    )
                );
                print("cek ecm group object unit ${_response.statusCode}");
                insertLog(context,_timeStartValidate,DateTime.now(),_bodyDocumentGroupObjectUnit,"Start Upload Foto ${_data[0]['symbolName']}","Start Upload Foto ${_data[0]['symbolName']}");
                if(_response.statusCode != 200){
                  loadData = false;
                  // _showSnackBar("Error ECM document object unit response ${_response.statusCode}");
                  _showSnackBar("Problem koneksi ke ECM. Mohon ditunggu beberapa saat untuk submit kembali");
                  messageProcess = "";
                  return;
                }
                else{
                  print("result dokumen objek unit = ${_response.data}");
                  if(_response.data['status'] == 200){
                    _orderSupportingDocuments.add({
                      "orderSupportingDocumentID": "NEW",
                      "documentSpecification": {
                        "documentTypeID": _providerFoto.listDocument[i].jenisDocument.docTypeId,
                        "isMandatory": _providerFoto.listDocument[i].jenisDocument.mandatory,
                        "isDisplay": _providerFoto.listDocument[i].jenisDocument.display,
                        "isUnit": _providerFoto.listDocument[i].jenisDocument.flag_unit,
                        "receivedDate": formatDateValidateAndSubmit(_providerFoto.listDocument[i].dateTime),
                      },
                      "fileSpecification": {
                        "fileName": _docTitle,
                        "fileHeaderID": _response.data['objectId'].toString().replaceAll("{}", ""),
                        "uploadDate": formatDateValidateAndSubmit(DateTime.now()),
                        "longitude": "${_providerFoto.listDocument[i].longitude}",
                        "latitude": "${_providerFoto.listDocument[i].latitude}"
                      },
                      "creationalSpecification": {
                        "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                        "createdBy": _preferences.getString("username"),
                        "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                        "modifiedBy": _preferences.getString("username"),
                      },
                      "status": "ACTIVE"
                    });
                  }
                  else{
                    loadData = false;
                    // _showSnackBar("Error ECM tempat tinggal response ${_response.statusCode}");
                    _showSnackBar("Problem koneksi ke ECM. Mohon ditunggu beberapa saat untuk submit kembali");
                    messageProcess = "";
                    return;
                  }
                  // _listObjectIdDocumentUnitObject.add(_response.data['objectId'].toString().replaceAll("{}", ""));
                }
              }
              catch(e){
                loadData = false;
                _showSnackBar("Error submit ECM dokumen unit ${e.toString()}");
                return;
              }
            }
          }
          else{
            loadData = false;
            _showSnackBar("Error get metadata grup objek unit");
            return;
          }
        }
      }

      // info document
      if(_providerDocument.listInfoDocument.isNotEmpty){
        print("masuk if info document");
        for(int i=0; i < _providerDocument.listInfoDocument.length; i++){
          print("cek docTypeId infoDocument ${_providerDocument.listInfoDocument[i].documentType.docTypeId} === ${_providerDocument.listInfoDocument[i].orderSupportingDocumentID}");
          List _data = await getListMetadata("${_providerDocument.listInfoDocument[i].documentType.docTypeId}");
          if(_data.isNotEmpty){
            String _docTitle = _providerDocument.listInfoDocument[i].fileName.split("/").last;
            dio.FormData formData = dio.FormData.fromMap({
              "file" :  await dio.MultipartFile.fromFile(_providerDocument.listInfoDocument[i].documentDetail.file.path, filename: _docTitle),
              "documentTitle" : "${DateFormat("dd-MM-yyyy HH-mm-ss").format(DateTime.now())}-$_orderNo-$_docTitle",
              "documentType" : _data[0]['symbolName'],
              "application" : "REVAMPMS2",
              "objectStore" : "ADIRAOS",
              "requestId" : "2020369"
            });
            var _bodyInfoDocument = jsonEncode({
              "documentTitle" : "${DateFormat("dd-MM-yyyy HH-mm-ss").format(DateTime.now())}-$_orderNo-$_docTitle",
              "documentType" : _data[0]['symbolName'],
              "application" : "REVAMPMS2",
              "objectStore" : "ADIRAOS",
              "requestId" : "2020369"
            });
            print("dokumen belakang = ${_providerDocument.listInfoDocument[i].fileHeaderID} = ${_providerDocument.listInfoDocument[i].orderSupportingDocumentID}");
            if(_providerDocument.listInfoDocument[i].orderSupportingDocumentID != "NEW"){
              print("masuk iffffff");
              _orderSupportingDocuments.add({
                "orderSupportingDocumentID": _providerDocument.listInfoDocument[i].orderSupportingDocumentID,
                "documentSpecification": {
                  "documentTypeID": _providerDocument.listInfoDocument[i].documentType.docTypeId,
                  "isMandatory": _providerDocument.listInfoDocument[i].documentType.mandatory,
                  "isDisplay": _providerDocument.listInfoDocument[i].documentType.display,
                  "isUnit": _providerDocument.listInfoDocument[i].documentType.flag_unit,
                  "receivedDate": formatDateValidateAndSubmit(DateTime.parse(_providerDocument.listInfoDocument[i].date)),//_providerDocument.listInfoDocument[i].date,
                },
                "fileSpecification": {
                  "fileName": _docTitle,
                  "fileHeaderID": _providerDocument.listInfoDocument[i].fileHeaderID,
                  "uploadDate": formatDateValidateAndSubmit(DateTime.now()),
                  "longitude": "${_providerDocument.listInfoDocument[i].longitude}",
                  "latitude": "${_providerDocument.listInfoDocument[i].latitude}"
                },
                "creationalSpecification": {
                  "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                  "createdBy": _preferences.getString("username"),
                  "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                  "modifiedBy": _preferences.getString("username"),
                },
                "status": "ACTIVE"
              });
            }
            else{
              try{
                var _response = await _dio.post(
                    "${BaseUrl.urlECM}$_urlUploadFile",
                    // "http://10.50.3.123:8080/api/v1/files",
                    data: formData,
                    options: dio.Options(
                      headers: {"x-api-key": xApiKey, "Authorization":"bearer $token"}, // M1I02YSVDX3ZCV4NF1CBUSXWSKD204
                    )
                );
                insertLog(context,_timeStartValidate,DateTime.now(),_bodyInfoDocument,"Start Upload Foto ${_data[0]['symbolName']}","Start Upload Foto ${_data[0]['symbolName']}");
                if(_response.statusCode != 200){
                  loadData = false;
                  // _showSnackBar("Error ECM info document ${_response.statusCode}");
                  _showSnackBar("Problem koneksi ke ECM. Mohon ditunggu beberapa saat untuk submit kembali");
                  messageProcess = "";
                  return;
                }
                else{
                  // List _splitDate = _providerDocument.listInfoDocument[i].date.split("-");
                  // print("cek dateTime "+_splitDate[2]+"-"+_splitDate[1]+"-"+_splitDate[0]);
                  // print("info dokumen tgl ${formatDateValidateAndSubmit(DateTime.parse(_splitDate[2]+"-"+_splitDate[1]+"-"+_splitDate[0]))}");
                  print("result info dokumen = ${_response.data}");
                  if(_response.data['status'] == 200){
                    _orderSupportingDocuments.add({
                      "orderSupportingDocumentID": _providerDocument.listInfoDocument[i].orderSupportingDocumentID,
                      "documentSpecification": {
                        "documentTypeID": _providerDocument.listInfoDocument[i].documentType.docTypeId,
                        "isMandatory": _providerDocument.listInfoDocument[i].documentType.mandatory,
                        "isDisplay": _providerDocument.listInfoDocument[i].documentType.display,
                        "isUnit": _providerDocument.listInfoDocument[i].documentType.flag_unit,
                        "receivedDate": formatDateValidateAndSubmit(DateTime.parse(_providerDocument.listInfoDocument[i].date)),//_providerDocument.listInfoDocument[i].date,
                      },
                      "fileSpecification": {
                        "fileName": _docTitle,
                        "fileHeaderID": _response.data['objectId'].toString().replaceAll("{}", ""),
                        "uploadDate": formatDateValidateAndSubmit(DateTime.now()),
                        "longitude": "${_providerDocument.listInfoDocument[i].longitude}",
                        "latitude": "${_providerDocument.listInfoDocument[i].latitude}"
                      },
                      "creationalSpecification": {
                        "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                        "createdBy": _preferences.getString("username"),
                        "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                        "modifiedBy": _preferences.getString("username"),
                      },
                      "status": "ACTIVE"
                    });
                  }
                  else{
                    loadData = false;
                    // _showSnackBar("Error ECM tempat tinggal response ${_response.statusCode}");
                    _showSnackBar("Problem koneksi ke ECM. Mohon ditunggu beberapa saat untuk submit kembali");
                    messageProcess = "";
                    return;
                  }
                  // _listObjectIdInfoDocument.add(_response.data['objectId'].toString().replaceAll("{}", ""));
                }
              }
              catch(e){
                loadData = false;
                _showSnackBar("Error submit ECM info dokumen ${e.toString()}");
                return;
              }
            }
          }
          else{
            loadData = false;
            _showSnackBar("Error get metadata info document");
            return;
          }
        }
      }

      // photo survey
      // sementara
      if(_preferences.getString("last_known_state") == "IDE"){
        _providerResultSurvey.listSurveyPhoto.clear();
      }
      if(_providerResultSurvey.listSurveyPhoto.isNotEmpty){
        for(int i=0; i < _providerResultSurvey.listSurveyPhoto.length; i++){
          if(_providerResultSurvey.listSurveyPhoto[i].listImageModel.isNotEmpty){
            for(int j=0; j<_providerResultSurvey.listSurveyPhoto[i].listImageModel.length; j++){
              List _data = await getListMetadata("${_providerResultSurvey.listSurveyPhoto[i].photoTypeModel.id}");
              String _docTitle = _providerResultSurvey.listSurveyPhoto[i].listImageModel[j].path.split("/").last;
              dio.FormData formData = dio.FormData.fromMap({
                "file" :  await dio.MultipartFile.fromFile(_providerResultSurvey.listSurveyPhoto[i].listImageModel[j].path, filename: _docTitle),
                "documentTitle" : "${DateFormat("dd-MM-yyyy HH-mm-ss").format(DateTime.now())}-$_orderNo-$_docTitle",
                "documentType" : _data[0]['symbolName'],
                "application" : "REVAMPMS2",
                "objectStore" : "ADIRAOS",
                "requestId" : "2020369"
                // "nama" : _preferences.getString("fullname"),
                // "region" : _preferences.getString("branchid"),
                // "NIKPIC" : _preferences.getString("username")
              });
              var _bodyPhotoSurvey = jsonEncode({
                "documentTitle" : "${DateFormat("dd-MM-yyyy HH-mm-ss").format(DateTime.now())}-$_orderNo-$_docTitle",
                "documentType" : _data[0]['symbolName'],
                "application" : "REVAMPMS2",
                "objectStore" : "ADIRAOS",
                "requestId" : "2020369"
              });
              try{
                var _response = await _dio.post(
                    "${BaseUrl.urlECM}$_urlUploadFile",
                    // "http://10.50.3.123:8080/api/v1/files",
                    data: formData,
                    options: dio.Options(
                      headers: {"x-api-key": xApiKey, "Authorization":"bearer $token"}, // M1I02YSVDX3ZCV4NF1CBUSXWSKD204
                    )
                );
                insertLog(context,_timeStartValidate,DateTime.now(),_bodyPhotoSurvey,"Start Upload Foto ${_data[0]['symbolName']}","Start Upload Foto ${_data[0]['symbolName']}");
                if(_response.statusCode != 200){
                  loadData = false;
                  // _showSnackBar("Error ECM photo survey ${_response.statusCode}");
                  _showSnackBar("Problem koneksi ke ECM foto survey. Mohon ditunggu beberapa saat untuk submit kembali");
                  messageProcess = "";
                  return;
                }
                else{
                  print("result survey = ${_response.data}");
                  if(_response.data['status'] == 200){
                    _orderSupportingDocuments.add({
                      "orderSupportingDocumentID": "NEW",
                      "documentSpecification": {
                        "documentTypeID": "${_providerResultSurvey.listSurveyPhoto[i].photoTypeModel.id}",
                        "isMandatory": 1, // 0 = true | 1 = false
                        "isDisplay": 1, // 0 = true | 1 = false
                        "isUnit": 1, // 0 = true | 1 = false
                        "receivedDate": formatDateValidateAndSubmit(DateTime.now()),// tidak ada field tgl terima dokumen
                      },
                      "fileSpecification": {
                        "fileName": _docTitle,
                        "fileHeaderID": _response.data['objectId'].toString().replaceAll("{}", ""),
                        "uploadDate": formatDateValidateAndSubmit(DateTime.now()),
                        "longitude": "${_providerResultSurvey.listSurveyPhoto[i].listImageModel[j].longitude}",
                        "latitude": "${_providerResultSurvey.listSurveyPhoto[i].listImageModel[j].latitude}"
                      },
                      "creationalSpecification": {
                        "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                        "createdBy": _preferences.getString("username"),
                        "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                        "modifiedBy": _preferences.getString("username"),
                      },
                      "status": "ACTIVE"
                    });
                  }
                  else{
                    loadData = false;
                    // _showSnackBar("Error ECM tempat tinggal response ${_response.statusCode}");
                    _showSnackBar("Problem koneksi ke ECM. Mohon ditunggu beberapa saat untuk submit kembali");
                    messageProcess = "";
                    return;
                  }
                }
              }
              catch(e){
                loadData = false;
                _showSnackBar("Error submit ECM foto survey ${e.toString()}");
                return;
              }
            }
          }
        }
        await _submitDataPartial.submitPhoto(context, "2");
      }
    }

    loadData = false;
    validateData(context, orderId, orderProductId);
    // final ioc = new HttpClient();
    // ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    //
    // final _http = IOClient(ioc);
    //
    // var _body;
    // if(_providerDocument.listInfoDocument.isNotEmpty) {
    //   for(int i=0; i < _providerDocument.listInfoDocument.length; i++) {
    //     var _pathName = _providerDocument.listInfoDocument[i].path;
    //     var _splitPath = _pathName.split("/");
    //     var _filenameExten = _splitPath[_splitPath.length - 1];
    //     var _splitFilename = _filenameExten.split(".");
    //     var _filename = _splitFilename[_splitFilename.length -1];
    //     _body = jsonEncode({
    //       "file" : _providerDocument.listInfoDocument[i].documentDetail.file,
    //       "documentTitle" : _filename,
    //       "documentType" : _providerDocument.listInfoDocument[i].documentType,
    //       "application" : "MS2",
    //       "objectStore" : "ADIRAOS",
    //       "requestId" : "2020369",
    //       "nama" : _preferences.getString("fullname"),
    //       "region" : _preferences.getString("branchid"),
    //       "NIKPIC" : _preferences.getString("username"),
    //     });
    //
    //
    //     final _response = await _http.post(
    //       "http://10.50.3.123:8080/api/public/ecm/v1/files",
    //       body: _body,
    //       headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    //     );
    //
    //     if(_response.statusCode == 200){
    //       final _result = jsonDecode(_response.body);
    //       // _showSnackBar("Berhasil Submit");
    //       // Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => HomePage()), (Route <dynamic> route) => false);
    //
    //       if(_providerDocument.listInfoDocument.isNotEmpty) {
    //         for(int j=0; j < _providerDocument.listInfoDocument.length; j++) {
    //           _orderSupportingDocuments.add({
    //             "orderSupportingDocumentID": "NEW",
    //             "documentSpecification": {
    //               "documentTypeID": _providerDocument.listInfoDocument[j].documentType.docTypeId,
    //               "isMandatory": false,
    //               "isDisplay": false,
    //               "isUnit": false,
    //               "receivedDate": _providerDocument.listInfoDocument[j].date,
    //             },
    //             "fileSpecification": {
    //               "fileName": _providerDocument.listInfoDocument[j].documentDetail.fileName,
    //               "fileHeaderID": _result['object_id'].toString().replaceAll("{}", ""),
    //               "uploadDate": DateTime.now()
    //             },
    //             "creationalSpecification": {
    //               "createdAt": DateTime.now(),
    //               "createdBy": _preferences.getString("username"),
    //               "modifiedAt": DateTime.now(),
    //               "modifiedBy": _preferences.getString("username"),
    //             },
    //             "status": "ACTIVE"
    //           });
    //         }
    //       }
    //     } else{
    //       _showSnackBar("Error ${_response.statusCode}");
    //     }
    //   }
    //   validateData(context, orderId, orderProductId);
    // }
  }

  Future<void> setValueCustomerIncome(BuildContext context) async {
    this._customerIncomes.clear();
    var _providerIncome = Provider.of<FormMIncomeChangeNotifier>(context, listen: false);
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    for(int i=0; i < _providerIncome.listIncome.length; i++){
      _customerIncomes.add(
          {
            "customerIncomeID": _preferences.getString("last_known_state") != "IDE" ? _providerIncome.listIncome[i].customerIncomeID : "NEW",
            "incomeFrmlType": _providerIncome.listIncome[i].type_income_frml,
            "incomeType": _providerIncome.listIncome[i].income_type,
            "incomeValue": double.parse(_providerIncome.listIncome[i].income_value.replaceAll(",", "")),
            "dataStatus": "ACTIVE",
            "creationalSpecification": {
              "createdAt": formatDateValidateAndSubmit(DateTime.now()),
              "createdBy": _preferences.getString("username"),
              "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
              "modifiedBy": _preferences.getString("username"),
            },
          }
      );
    }
  }

  void validateData(BuildContext context, String orderId, String orderProductId) async {
    print("cek validateData");
    _showSnackBarSuccess("Foto berhasil diupload");
    // Dedup
    // var _listOID = Provider.of<ListOidChangeNotifier>(context,listen: false);
    List _dataCL = await _dbHelper.selectMS2LME();
    List _dataResultSurvey = await _dbHelper.selectResultSurveyMS2Assignment();

    // Form IDE Individu
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
    var _providerInfoNasabah = Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false);
    var _providerInfoAlamatNasabah = Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false);
    var _providerIbu = Provider.of<FormMInformasiKeluargaIBUChangeNotif>(context, listen: false);
    var _providerKeluarga = Provider.of<FormMInfoKeluargaChangeNotif>(context, listen: false);
    var _providerOccupation = Provider.of<FormMOccupationChangeNotif>(context,listen: false);
    // var _providerIncome = Provider.of<FormMIncomeChangeNotifier>(context, listen: false);
    var _providerGuarantor = Provider.of<FormMGuarantorChangeNotifier>(context, listen: false);

    // Form IDE Company
    var _providerRincian = Provider.of<FormMCompanyRincianChangeNotifier>(context, listen: false);
    var _providerInfoAlamatCompany = Provider.of<FormMCompanyAlamatChangeNotifier>(context, listen: false);
    // var _providerIncomeCompany = Provider.of<FormMCompanyPendapatanChangeNotifier>(context, listen: false);
    var _providerManajemenPIC = Provider.of<FormMCompanyManajemenPICChangeNotifier>(context, listen: false);
    // var _providerAddressManajemenPIC = Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context,listen: false);
    // var _providerPemegangSaham = Provider.of<AddPemegangSahamPribadiChangeNotifier>(context, listen: false); // di comment karena pemegang saham
    // var _providerAddressSharedHolder = Provider.of<FormMCompanyPemegangSahamPribadiAlamatChangeNotifier>(context, listen: false);

    // Form App
    var _providerApp = Provider.of<InfoAppChangeNotifier>(context, listen: false);
    var _providerUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
    var _providerKolateral = Provider.of<InformationCollateralChangeNotifier>(context, listen: false);
    var _providerSales = Provider.of<InformationSalesmanChangeNotifier>(context, listen: false);
    var _providerInsurance = Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false);
    var _providerAdditionalInsurance = Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false);
    var _providerKaroseri = Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false);
    var _providerCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false);
    var _providerCreditStructureType = Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context, listen: false);
    var _providerCreditIncome = Provider.of<InfoCreditIncomeChangeNotifier>(context, listen: false);
    var _providerWmp = Provider.of<InfoWMPChangeNotifier>(context,listen: false);
    var _providerSubsidy = Provider.of<InfoCreditSubsidyChangeNotifier>(context,listen: false);
    var _providerSurvey = Provider.of<ResultSurveyChangeNotifier>(context,listen:false);
    // var _providerSubsidyDetail = Provider.of<AddCreditSubsidyChangeNotifier>(context, listen: false);
    // var _providerCreditStructureTypeInstallment = Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false);
    // var _providerDocument = Provider.of<InfoDocumentChangeNotifier>(context,listen: false);

    // var occupation = Provider.of<FormMFotoChangeNotifier>(context, listen: false).occupationSelected.KODE;
    // var _providerInfoDocument = Provider.of<InfoDocumentChangeNotifier>(context, listen: false);

    SharedPreferences _preferences = await SharedPreferences.getInstance();

    await setValueCustomerIncome(context);
    // backup di rubah dr manual ke list
    // if(_preferences.getString("cust_type") == "PER") {
    //   if(_providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07") {
    //     if(_providerInfoNasabah.maritalStatusSelected.id != "01") {
    //       for(int i=0; i < 13; i++) {
    //         _customerIncomes.add(
    //             {
    //               "customerIncomeID": _preferences.getString("last_known_state") != "IDE" ? _preferences.getString("customerIncomeID") : "NEW",
    //               "incomeFrmlType": i == 0
    //                   ? "S" : i == 1
    //                   ? "+" : i == 2
    //                   ? "=" : i == 3
    //                   ? "-" : i == 4
    //                   ? "=" : i == 5
    //                   ? "-" : i == 6
    //                   ? "-" : i == 7
    //                   ? "=" : i == 8
    //                   ? "-" : i == 9
    //                   ? "=" : i == 10
    //                   ? "-" : i == 11
    //                   ? "="
    //                   : "N",
    //               "incomeType": i == 0
    //                   ? "001" : i == 1
    //                   ? "002" : i == 2
    //                   ? "003" : i == 3
    //                   ? "004" : i == 4
    //                   ? "005" : i == 5
    //                   ? "006" : i == 6
    //                   ? "007" : i == 7
    //                   ? "008" : i == 8
    //                   ? "009" : i == 9
    //                   ? "010" : i == 10
    //                   ? "011" : i == 11
    //                   ? "017"
    //                   : "013",
    //               "incomeValue": i == 0
    //                   ? double.parse(_providerIncome.controllerMonthlyIncome.text.replaceAll(",", "")) : i == 1
    //                   ? double.parse(_providerIncome.controllerOtherIncome.text.replaceAll(",", "")) : i == 2
    //                   ? double.parse(_providerIncome.controllerTotalIncome.text.replaceAll(",", "")) : i == 3
    //                   ? double.parse(_providerIncome.controllerCostOfGoodsSold.text.replaceAll(",", "")) : i == 4
    //                   ? double.parse(_providerIncome.controllerGrossProfit.text.replaceAll(",", "")) : i == 5
    //                   ? double.parse(_providerIncome.controllerOperatingCosts.text.replaceAll(",", "")) : i == 6
    //                   ? double.parse(_providerIncome.controllerOtherCosts.text.replaceAll(",", "")) : i == 7
    //                   ? double.parse(_providerIncome.controllerNetProfitBeforeTax.text.replaceAll(",", "")) : i == 8
    //                   ? double.parse(_providerIncome.controllerTax.text.replaceAll(",", "")) : i == 9
    //                   ? double.parse(_providerIncome.controllerNetProfitAfterTax.text.replaceAll(",", "")) : i == 10
    //                   ? double.parse(_providerIncome.controllerCostOfLiving.text.replaceAll(",", "")) : i == 11
    //                   ? double.parse(_providerIncome.controllerRestIncome.text.replaceAll(",", ""))
    //                   : double.parse(_providerIncome.controllerOtherInstallments.text.replaceAll(",", "")),
    //               "dataStatus": "ACTIVE",
    //               "creationalSpecification": {
    //                 "createdAt": formatDateValidateAndSubmit(DateTime.now()),
    //                 "createdBy": _preferences.getString("username"),
    //                 "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
    //                 "modifiedBy": _preferences.getString("username"),
    //               },
    //             }
    //         );
    //       }
    //     }
    //     else {
    //       for(int i=0; i < 14; i++) {
    //         _customerIncomes.add(
    //             {
    //               "customerIncomeID":_preferences.getString("last_known_state") != "IDE" ? _preferences.getString("customerIncomeID") : "NEW",
    //               "incomeFrmlType": i == 0
    //                   ? "S" : i == 1
    //                   ? "+" : i == 2
    //                   ? "=" : i == 3
    //                   ? "-" : i == 4
    //                   ? "=" : i == 5
    //                   ? "-" : i == 6
    //                   ? "-" : i == 7
    //                   ? "=" : i == 8
    //                   ? "-" : i == 9
    //                   ? "=" : i == 10
    //                   ? "-" : i == 11
    //                   ? "=" : i == 12
    //                   ? "N"
    //                   : "N",
    //               "incomeType": i == 0
    //                   ? "001" : i == 1
    //                   ? "002" : i == 2
    //                   ? "003" : i == 3
    //                   ? "004" : i == 4
    //                   ? "005" : i == 5
    //                   ? "006" : i == 6
    //                   ? "007" : i == 7
    //                   ? "008" : i == 8
    //                   ? "009" : i == 9
    //                   ? "010" : i == 10
    //                   ? "011" : i == 11
    //                   ? "017" : i == 12
    //                   ? "012"
    //                   : "013",
    //               "incomeValue": i == 0
    //                   ? double.parse(_providerIncome.controllerMonthlyIncome.text.replaceAll(",", "")) : i == 1
    //                   ? double.parse(_providerIncome.controllerOtherIncome.text.replaceAll(",", "")) : i == 2
    //                   ? double.parse(_providerIncome.controllerTotalIncome.text.replaceAll(",", "")) : i == 3
    //                   ? double.parse(_providerIncome.controllerCostOfGoodsSold.text.replaceAll(",", "")) : i == 4
    //                   ? double.parse(_providerIncome.controllerGrossProfit.text.replaceAll(",", "")) : i == 5
    //                   ? double.parse(_providerIncome.controllerOperatingCosts.text.replaceAll(",", "")) : i == 6
    //                   ? double.parse(_providerIncome.controllerOtherCosts.text.replaceAll(",", "")) : i == 7
    //                   ? double.parse(_providerIncome.controllerNetProfitBeforeTax.text.replaceAll(",", "")) : i == 8
    //                   ? double.parse(_providerIncome.controllerTax.text.replaceAll(",", "")) : i == 9
    //                   ? double.parse(_providerIncome.controllerNetProfitAfterTax.text.replaceAll(",", "")) : i == 10
    //                   ? double.parse(_providerIncome.controllerCostOfLiving.text.replaceAll(",", "")) : i == 11
    //                   ? double.parse(_providerIncome.controllerRestIncome.text.replaceAll(",", "")) : i == 12
    //                   ? double.parse(_providerIncome.controllerSpouseIncome.text.replaceAll(",", ""))
    //                   : double.parse(_providerIncome.controllerOtherInstallments.text.replaceAll(",", "")),
    //               "dataStatus": "ACTIVE",
    //               "creationalSpecification": {
    //                 "createdAt": formatDateValidateAndSubmit(DateTime.now()),
    //                 "createdBy": _preferences.getString("username"),
    //                 "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
    //                 "modifiedBy": _preferences.getString("username"),
    //               },
    //             }
    //         );
    //       }
    //     }
    //   }
    //   else {
    //     if(_providerInfoNasabah.maritalStatusSelected.id != "01") {
    //       for(int i=0; i < 6; i++) {
    //         _customerIncomes.add(
    //             {
    //               "customerIncomeID": _preferences.getString("last_known_state") != "IDE" ? _preferences.getString("customerIncomeID") : "NEW",
    //               "incomeFrmlType": i == 0
    //                   ? "S" : i == 1
    //                   ? "+" : i == 2
    //                   ? "=" : i == 3
    //                   ? "-" : i == 4
    //                   ? "="
    //                   : "N",
    //               "incomeType": i == 0
    //                   ? "016" : i == 1
    //                   ? "002" : i == 2
    //                   ? "003" : i == 3
    //                   ? "011" : i == 4
    //                   ? "017"
    //                   : "013",
    //               "incomeValue": i == 0
    //                   ? double.parse(_providerIncome.controllerMonthlyIncome.text.replaceAll(",", "")) : i == 1
    //                   ? double.parse(_providerIncome.controllerOtherIncome.text.replaceAll(",", "")) : i == 2
    //                   ? double.parse(_providerIncome.controllerTotalIncome.text.replaceAll(",", "")) : i == 3
    //                   ? double.parse(_providerIncome.controllerCostOfLiving.text.replaceAll(",", "")) : i == 4
    //                   ? double.parse(_providerIncome.controllerRestIncome.text.replaceAll(",", ""))
    //                   : double.parse(_providerIncome.controllerOtherInstallments.text.replaceAll(",", "")),
    //               "dataStatus": "ACTIVE",
    //               "creationalSpecification": {
    //                 "createdAt": formatDateValidateAndSubmit(DateTime.now()),
    //                 "createdBy": _preferences.getString("username"),
    //                 "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
    //                 "modifiedBy": _preferences.getString("username"),
    //               },
    //             }
    //         );
    //       }
    //     }
    //     else {
    //       for(int i=0; i < 7; i++) {
    //         _customerIncomes.add(
    //             {
    //               "customerIncomeID": _preferences.getString("last_known_state") != "IDE" ? _preferences.getString("customerIncomeID") : "NEW",
    //               "incomeFrmlType": i == 0
    //                   ? "S" : i == 1
    //                   ? "+" : i == 2
    //                   ? "+" : i == 3
    //                   ? "=" : i == 4
    //                   ? "-" : i == 5
    //                   ? "="
    //                   : "N",
    //               "incomeType": i == 0
    //                   ? "016" : i == 1
    //                   ? "012" : i == 2
    //                   ? "002" : i == 3
    //                   ? "003" : i == 4
    //                   ? "011" : i == 5
    //                   ? "017"
    //                   : "013",
    //               "incomeValue": i == 0
    //                   ? _providerIncome.controllerMonthlyIncome.text.replaceAll(",", "") : i == 1
    //                   ? _providerIncome.controllerSpouseIncome.text.replaceAll(",", "") : i == 2
    //                   ? _providerIncome.controllerOtherIncome.text.replaceAll(",", "") : i == 3
    //                   ? _providerIncome.controllerTotalIncome.text.replaceAll(",", "") : i == 4
    //                   ? _providerIncome.controllerCostOfLiving.text.replaceAll(",", "") : i == 5
    //                   ? _providerIncome.controllerRestIncome.text.replaceAll(",", "")
    //                   : _providerIncome.controllerOtherInstallments.text.replaceAll(",", ""),
    //               "dataStatus": "ACTIVE",
    //               "creationalSpecification": {
    //                 "createdAt": formatDateValidateAndSubmit(DateTime.now()),
    //                 "createdBy": _preferences.getString("username"),
    //                 "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
    //                 "modifiedBy": _preferences.getString("username"),
    //               },
    //             }
    //         );
    //       }
    //     }
    //   }
    // }
    // // adjut di company untukk limit looping
    // else {
    //   for(int i=0; i < 11; i++) {
    //     _customerIncomes.add(
    //         {
    //           "customerIncomeID": _preferences.getString("last_known_state") != "IDE" ? _preferences.getString("customerIncomeID") : "NEW",
    //           "incomeFrmlType": i == 0
    //               ? "S" : i == 1
    //               ? "+" : i == 2
    //               ? "=" : i == 3
    //               ? "-" : i == 4
    //               ? "=" : i == 5
    //               ? "-" : i == 6
    //               ? "-" : i == 7
    //               ? "=" : i == 8
    //               ? "-" : i == 9
    //               ? "="
    //               : "N",
    //           "incomeType": i == 0
    //               ? "016" : i == 1
    //               ? "002" : i == 2
    //               ? "003" : i == 3
    //               ? "004" : i == 4
    //               ? "005" : i == 5
    //               ? "006" : i == 6
    //               ? "007" : i == 7
    //               ? "008" : i == 8
    //               ? "009" : i == 9
    //               ? "010"
    //               : "013",
    //           "incomeValue": i == 0
    //               ? double.parse(_providerIncomeCompany.controllerMonthlyIncome.text.replaceAll(",", "")) : i == 1
    //               ? double.parse(_providerIncomeCompany.controllerOtherIncome.text.replaceAll(",", "")) : i == 2
    //               ? double.parse(_providerIncomeCompany.controllerTotalIncome.text.replaceAll(",", "")) : i == 3
    //               ? double.parse(_providerIncomeCompany.controllerCostOfRevenue.text.replaceAll(",", "")) : i == 4
    //               ? double.parse(_providerIncomeCompany.controllerGrossProfit.text.replaceAll(",", "")) : i == 5
    //               ? double.parse(_providerIncomeCompany.controllerOperatingCosts.text.replaceAll(",", "")) : i == 6
    //               ? double.parse(_providerIncomeCompany.controllerOtherCosts.text.replaceAll(",", "")) : i == 7
    //               ? double.parse(_providerIncomeCompany.controllerNetProfitBeforeTax.text.replaceAll(",", "")) : i == 8
    //               ? double.parse(_providerIncomeCompany.controllerTax.text.replaceAll(",", "")) : i == 9
    //               ? double.parse(_providerIncomeCompany.controllerNetProfitAfterTax.text.replaceAll(",", ""))
    //               : double.parse(_providerIncomeCompany.controllerOtherInstallments.text.replaceAll(",", "")),
    //           "dataStatus": "ACTIVE",
    //           "creationalSpecification": {
    //             "createdAt": formatDateValidateAndSubmit(DateTime.now()),
    //             "createdBy": _preferences.getString("username"),
    //             "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
    //             "modifiedBy": _preferences.getString("username"),
    //           },
    //         }
    //     );
    //   }
    // }

    String _npwpType = "";
    if(_preferences.getString("cust_type") == "PER"){
      if(_providerInfoNasabah.jenisNPWPSelected != null) {
        _npwpType = _providerInfoNasabah.jenisNPWPSelected != null ? _providerInfoNasabah.jenisNPWPSelected.id : "2";
      } else {
        _npwpType = "2";
      }
    }
    else{
      if(_providerRincian.typeNPWPSelected != null) {
        _npwpType = _providerRincian.typeNPWPSelected != null ? _providerRincian.typeNPWPSelected.id : "2";
      } else {
        _npwpType = "2";
      }
    }

    String _npwpAddress = "";
    if(_preferences.getString("cust_type") == "PER"){
      if(_providerInfoNasabah.radioValueIsHaveNPWP == 1){
        _npwpAddress = _providerInfoNasabah.controllerAlamatSesuaiNPWP.text.isNotEmpty ? _providerInfoNasabah.controllerAlamatSesuaiNPWP.text : "INDONESIA";
      } else {
        // _npwpAddress = "INDONESIA";
        for(int i=0; i < _providerInfoAlamatNasabah.listAlamatKorespondensi.length; i++) {
          if(_providerInfoAlamatNasabah.listAlamatKorespondensi[i].jenisAlamatModel.KODE == "03") {
            _npwpAddress = _providerInfoAlamatNasabah.listAlamatKorespondensi[i].address.toString();
          }
        }
      }
    }
    else{
      if(_providerRincian.radioValueIsHaveNPWP == 1){
        _npwpAddress = _providerRincian.controllerNPWPAddress.text.isNotEmpty ? "INDONESIA" : _providerRincian.controllerNPWPAddress.text;
      } else {
        _npwpAddress = "INDONESIA";
      }
    }

    String _npwpFullName = "";
    if(_preferences.getString("cust_type") == "PER"){
      if(_providerInfoNasabah.radioValueIsHaveNPWP == 1){
        _npwpFullName = _providerInfoNasabah.controllerNamaSesuaiNPWP.text;
      }
      else{
        // _npwpFullName = "NAMA IDENTITAS";
        _npwpFullName = _providerInfoNasabah.controllerNamaLengkapSesuaiIdentitas.text;
      }
    }
    else{
      if(_providerRincian.radioValueIsHaveNPWP == 1){
        _npwpFullName = _providerRincian.controllerFullNameNPWP.text;
      }
    }

    String _npwpNumber = "";
    if(_preferences.getString("cust_type") == "PER"){
      if(_providerInfoNasabah.radioValueIsHaveNPWP == 1){
        _npwpNumber = _providerInfoNasabah.controllerNoNPWP.text;
      }
      else{
        _npwpNumber = "000000000000000";
      }
    }
    else{
      if(_providerRincian.radioValueIsHaveNPWP == 1){
        _npwpNumber = _providerRincian.controllerNPWP.text;
      }
      else{
        _npwpNumber = "000000000000000";
      }
    }

    var _detailAddressesInfoNasabah = [];
    for (int i = 0; i <_providerInfoAlamatNasabah.listAlamatKorespondensi.length; i++) {
      _detailAddressesInfoNasabah.add({
        "addressID": _preferences.getString("last_known_state") != "IDE" ? _providerInfoAlamatNasabah.listAlamatKorespondensi[i].addressID != "NEW" ? _providerInfoAlamatNasabah.listAlamatKorespondensi[i].addressID : "NEW" : "NEW",
        "addressSpecification": {
          "koresponden": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].isCorrespondence ? "1" : "0",
          "matrixAddr": "5",
          "address": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].address,
          "rt": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].rt,
          "rw": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].rw,
          "provinsiID": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].kelurahanModel.PROV_ID,
          "kabkotID": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].kelurahanModel.KABKOT_ID,
          "kecamatanID": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].kelurahanModel.KEC_ID,
          "kelurahanID": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].kelurahanModel.KEL_ID,
          "zipcode": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].kelurahanModel.ZIPCODE
        },
        "contactSpecification": {
          "telephoneArea1": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].areaCode,
          "telephone1": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].phone,
          "telephoneArea2": null,
          "telephone2": null,
          "faxArea": null,
          "fax": null,
          "handphone": "",
          "email": ""
        },
        "addressType": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].jenisAlamatModel.KODE,
        "creationalSpecification": {
          "createdAt": formatDateValidateAndSubmit(DateTime.now()),
          "createdBy": _preferences.getString("username"),
          "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
          "modifiedBy": _preferences.getString("username"),
        },
        "status": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].active == 0 ? "ACTIVE" : "INACTIVE",
        "foreignBusinessID": _preferences.getString("last_known_state") != "IDE" ? _providerInfoAlamatNasabah.listAlamatKorespondensi[i].foreignBusinessID != "NEW" ? _providerInfoAlamatNasabah.listAlamatKorespondensi[i].foreignBusinessID : "CUST001" : "CUST001"
      });
    }
    for(int i=0; i < _providerOccupation.listOccupationAddress.length; i++){
      _detailAddressesInfoNasabah.add({
        "addressID": _preferences.getString("last_known_state") != "IDE" ? _providerOccupation.listOccupationAddress[i].addressID != "NEW" ? _providerOccupation.listOccupationAddress[i].addressID : "NEW" : "NEW",
        "addressSpecification": {
          "koresponden": _providerOccupation.listOccupationAddress[i].isCorrespondence ? "1" : "0",
          "matrixAddr": "4",
          "address": _providerOccupation.listOccupationAddress[i].address,
          "rt": _providerOccupation.listOccupationAddress[i].rt,
          "rw": _providerOccupation.listOccupationAddress[i].rw,
          "provinsiID": _providerOccupation.listOccupationAddress[i].kelurahanModel.PROV_ID,
          "kabkotID": _providerOccupation.listOccupationAddress[i].kelurahanModel.KABKOT_ID,
          "kecamatanID": _providerOccupation.listOccupationAddress[i].kelurahanModel.KEC_ID,
          "kelurahanID": _providerOccupation.listOccupationAddress[i].kelurahanModel.KEL_ID,
          "zipcode": _providerOccupation.listOccupationAddress[i].kelurahanModel.ZIPCODE
        },
        "contactSpecification": {
          "telephoneArea1": _providerOccupation.listOccupationAddress[i].areaCode,
          "telephone1": _providerOccupation.listOccupationAddress[i].phone,
          "telephoneArea2": null,
          "telephone2": null,
          "faxArea": null,
          "fax": null,
          "handphone": "",
          "email": ""
        },
        "addressType": _providerOccupation.listOccupationAddress[i].jenisAlamatModel.KODE,
        "creationalSpecification": {
          "createdAt": formatDateValidateAndSubmit(DateTime.now()),
          "createdBy": _preferences.getString("username"),
          "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
          "modifiedBy": _preferences.getString("username"),
        },
        "status": _providerOccupation.listOccupationAddress[i].active == 0 ? "ACTIVE" : "INACTIVE",
        "foreignBusinessID": _preferences.getString("last_known_state") != "IDE" ? _providerOccupation.listOccupationAddress[i].foreignBusinessID != "NEW" ? _providerOccupation.listOccupationAddress[i].foreignBusinessID : "IP001" : "IP001"
      });
    }

    //address colla
    if(_providerKolateral.collateralTypeModel.id == "002"){
      _detailAddressesInfoNasabah.add(
          {
            "addressID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.addressID != "NEW" ? _providerKolateral.addressID : "NEW" : "NEW",
            "foreignBusinessID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.foreignBusinessID != "NEW" ? _providerKolateral.foreignBusinessID : "COLP001" : "COLP001",
            "addressSpecification": {
              "koresponden": _providerKolateral.isKorespondensi.toString(),// tanya oji
              "matrixAddr": _preferences.getString("cust_type") == "PER" ? "6" : "13",
              "address": _providerKolateral.controllerAddress.text,
              "rt": _providerKolateral.controllerRT.text,
              "rw": _providerKolateral.controllerRW.text,
              "provinsiID": _providerKolateral.kelurahanSelected.PROV_ID,
              "kabkotID": _providerKolateral.kelurahanSelected.KABKOT_ID,
              "kecamatanID": _providerKolateral.kelurahanSelected.KEC_ID,
              "kelurahanID": _providerKolateral.kelurahanSelected.KEL_ID,
              "zipcode": _providerKolateral.kelurahanSelected.ZIPCODE
            },
            "contactSpecification": {
              "telephoneArea1": null,
              "telephone1": null,
              "telephoneArea2": null,
              "telephone2": null,
              "faxArea": null,
              "fax": null,
              "handphone": null,
              "email": null,
              "noWa": null,
              "noWa2": null,
              "noWa3": null
            },
            "addressType": _providerKolateral.addressTypeSelected.KODE,
            "creationalSpecification": {
              "createdAt": formatDateValidateAndSubmit(DateTime.now()),
              "createdBy": _preferences.getString("username"),
              "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
              "modifiedBy": _preferences.getString("username"),
            },
            "status": "ACTIVE"
          }
      );
    }

    // var _orderSupportingDocuments = [];
    // if(_providerDocument.listInfoDocument.isNotEmpty) {
    //   for(int i=0; i < _providerDocument.listInfoDocument.length; i++) {
    //     _orderSupportingDocuments.add({
    //       "orderSupportingDocumentID": "NEW",
    //       "documentSpecification": {
    //         "documentTypeID": _providerDocument.listInfoDocument[i].documentType.docTypeId,
    //         "isMandatory": false,
    //         "isDisplay": false,
    //         "isUnit": false,
    //         "receivedDate": _providerDocument.listInfoDocument[i].date,
    //       },
    //       "fileSpecification": {
    //         "fileName": _providerDocument.listInfoDocument[i].documentDetail.fileName,
    //         "fileHeaderID": "",
    //         "uploadDate": "17-11-2020 13:18:30"
    //       },
    //       "creationalSpecification": {
    //         "createdAt": formatDateValidateAndSubmit(DateTime.now()),
    //         "createdBy": _preferences.getString("username"),
    //         "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
    //         "modifiedBy": _preferences.getString("username"),
    //       },
    //       "status": "ACTIVE"
    //     });
    //   }
    // }

    //TODO belum dipasang ID
    var _installmentDetails = [];
    if(_providerCreditStructure.installmentTypeSelected.id == "06") {
      // Stepping
      if(_providerCreditStructureType.listInfoCreditStructureStepping.isNotEmpty) {
        for(int i=0; i < _providerCreditStructureType.listInfoCreditStructureStepping.length; i++) {
          _installmentDetails.add({
            "installmentID": _preferences.getString("last_known_state") != "IDE" ? null : "NEW",// nanti di bagian if di ambil dr sharedPreference
            "installmentNumber": 0,
            "percentage": _providerCreditStructureType.listInfoCreditStructureStepping[i].controllerPercentage.text != "" ? double.parse(_providerCreditStructureType.listInfoCreditStructureStepping[i].controllerPercentage.text.replaceAll(",", "")) : 0,
            "amount": _providerCreditStructureType.listInfoCreditStructureStepping[i].controllerValue.text != "" ? double.parse(_providerCreditStructureType.listInfoCreditStructureStepping[i].controllerValue.text.replaceAll(",", "")) : 0,
            "creationalSpecification": {
              "createdAt": formatDateValidateAndSubmit(DateTime.now()),
              "createdBy": _preferences.getString("username"),
              "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
              "modifiedBy": _preferences.getString("username"),
            },
            "status": "ACTIVE"
          });
        }
      }
    }
    else if(_providerCreditStructure.installmentTypeSelected.id == "07") {
      // Irreguler
      if(_providerCreditStructureType.listInfoCreditStructureIrregularModel.isNotEmpty) {
        for(int i=0; i < _providerCreditStructureType.listInfoCreditStructureIrregularModel.length; i++) {
          _installmentDetails.add({
            "installmentID": _preferences.getString("last_known_state") != "IDE" ? null : "NEW",// nanti di bagian if di ambil dr sharedPreference
            "installmentNumber": i+1,
            "percentage": 0,
            "amount": _providerCreditStructureType.listInfoCreditStructureIrregularModel[i].controller.text != "" ? double.parse(_providerCreditStructureType.listInfoCreditStructureIrregularModel[i].controller.text.replaceAll(",", "")) : 0,
            "creationalSpecification": {
              "createdAt": formatDateValidateAndSubmit(DateTime.now()),
              "createdBy": _preferences.getString("username"),
              "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
              "modifiedBy": _preferences.getString("username"),
            },
            "status": "ACTIVE"
          });
        }
      }
    }

    var _guarantorIndividuals = [];
    if(_providerGuarantor.listGuarantorIndividual.isNotEmpty) {
      for(int i=0; i < _providerGuarantor.listGuarantorIndividual.length; i++) {
        List _detailAddressGuarantorIndividu = [];
        String _foreignBKGuarantorIndividu = "IKPG";
        if(_providerGuarantor.listGuarantorIndividual.length < 10) {
          _foreignBKGuarantorIndividu = "IKPG00" + (i+1).toString();
        } else if(_providerGuarantor.listGuarantorIndividual.length < 100) {
          _foreignBKGuarantorIndividu = "IKPG0" + (i+1).toString();
        } else if(_providerGuarantor.listGuarantorIndividual.length < 1000) {
          _foreignBKGuarantorIndividu = "IKPG" + (i+1).toString();
        }
        if(_providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel.isNotEmpty){
          for(int j=0; j < _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel.length; j++){
            _detailAddressGuarantorIndividu.add({
              "addressID": _preferences.getString("last_known_state") != "IDE"
                  ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].addressID != "NEW"
                  ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].addressID : "NEW" : "NEW",
              "addressSpecification": {
                "koresponden": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isCorrespondence ? "1" : "0",
                "matrixAddr": _preferences.getString("cust_type") == "PER" ? "1" : "7",
                "address": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].address,
                "rt": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].rt,
                "rw": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].rw,
                "provinsiID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.PROV_ID,
                "kabkotID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_ID,
                "kecamatanID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KEC_ID,
                "kelurahanID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KEL_ID,
                "zipcode": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.ZIPCODE
              },
              "contactSpecification": {
                "telephoneArea1": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].areaCode,
                "telephone1": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].phone,
                "telephoneArea2": null,
                "telephone2": null,
                "faxArea": null,
                "fax": null,
                "handphone": "08${_providerGuarantor.listGuarantorIndividual[i].cellPhoneNumber}",
                "email": ""
              },
              "addressType": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].jenisAlamatModel.KODE,
              "creationalSpecification": {
                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                "createdBy": _preferences.getString("username"),
                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                "modifiedBy": _preferences.getString("username"),
              },
              "status": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].active == 0 ? "ACTIVE" : "INACTIVE",
              "foreignBusinessID": _preferences.getString("last_known_state") != "IDE"
                  ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].foreignBusinessID != "NEW"
                  ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].foreignBusinessID : _foreignBKGuarantorIndividu : _foreignBKGuarantorIndividu,
            });
            _detailAddressesInfoNasabah.add({
              "addressID": _preferences.getString("last_known_state") != "IDE"
                  ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].addressID != "NEW"
                  ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].addressID : "NEW" : "NEW",
              "addressSpecification": {
                "koresponden": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isCorrespondence ? "1" : "0",
                "matrixAddr": _preferences.getString("cust_type") == "PER" ? "1" : "7",
                "address": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].address,
                "rt": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].rt,
                "rw": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].rw,
                "provinsiID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.PROV_ID,
                "kabkotID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_ID,
                "kecamatanID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KEC_ID,
                "kelurahanID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KEL_ID,
                "zipcode": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.ZIPCODE
              },
              "contactSpecification": {
                "telephoneArea1": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].areaCode,
                "telephone1": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].phone,
                "telephoneArea2": null,
                "telephone2": null,
                "faxArea": null,
                "fax": null,
                "handphone": "",
                "email": ""
              },
              "addressType": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].jenisAlamatModel.KODE,
              "creationalSpecification": {
                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                "createdBy": _preferences.getString("username"),
                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                "modifiedBy": _preferences.getString("username"),
              },
              "status": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].active == 0 ? "ACTIVE" : "INACTIVE",
              "foreignBusinessID": _preferences.getString("last_known_state") != "IDE"
                  ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].foreignBusinessID != "NEW"
                  ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].foreignBusinessID : _foreignBKGuarantorIndividu : _foreignBKGuarantorIndividu,
            });
          }
        }
        _guarantorIndividuals.add(
            {
              "guarantorIndividualID": _preferences.getString("last_known_state") != "IDE"
                  ? _providerGuarantor.listGuarantorIndividual[i].guarantorIndividualID != null
                  ? _providerGuarantor.listGuarantorIndividual[i].guarantorIndividualID : _foreignBKGuarantorIndividu : _foreignBKGuarantorIndividu, // NEW
              "relationshipStatus":_providerGuarantor.listGuarantorIndividual[i].relationshipStatusModel != null ?
              _providerGuarantor.listGuarantorIndividual[i].relationshipStatusModel.PARA_FAMILY_TYPE_ID : "",
              "dedupScore": 0.0,
              "guarantorIdentity": {
                "identityType": _providerGuarantor.listGuarantorIndividual[i].identityModel != null ?
                _providerGuarantor.listGuarantorIndividual[i].identityModel.id : "",
                "identityNumber": _providerGuarantor.listGuarantorIndividual[i].identityNumber,
                "identityName": _providerGuarantor.listGuarantorIndividual[i].fullNameIdentity,
                "fullName": _providerGuarantor.listGuarantorIndividual[i].fullName,
                "alias": "",
                "title": "",
                "dateOfBirth": _providerGuarantor.listGuarantorIndividual[i].birthDate != "" ? formatDateValidateAndSubmit(DateTime.parse(_providerGuarantor.listGuarantorIndividual[i].birthDate)) : "01-01-1900 00:00:00",
                "placeOfBirth": _providerGuarantor.listGuarantorIndividual[i].birthPlaceIdentity1 != "" ? _providerGuarantor.listGuarantorIndividual[i].birthPlaceIdentity1 : "",
                "placeOfBirthKabKota": _providerGuarantor.listGuarantorIndividual[i].birthPlaceIdentity2 != null ? _providerGuarantor.listGuarantorIndividual[i].birthPlaceIdentity2.KABKOT_ID : "",
                "gender": _providerGuarantor.listGuarantorIndividual[i].gender,
                "identityActiveStart": _preferences.getString("cust_type") == "PER" ? _providerInfoNasabah.controllerTglIdentitas.text != "" ? formatDateValidateAndSubmit(_providerInfoNasabah.initialDateForTglIdentitas) : formatDateValidateAndSubmit(DateTime.now()) : formatDateValidateAndSubmit(DateTime.now()),
                "identityActiveEnd": _preferences.getString("cust_type") == "PER" ? _providerInfoNasabah.valueCheckBox ? "31-12-2999 00:00:00" : _providerInfoNasabah.controllerIdentitasBerlakuSampai.text != "" ? formatDateValidateAndSubmit(_providerInfoNasabah.initialDateForTglIdentitas) : "31-12-2999 00:00:00" : "31-12-2999 00:00:00", // berdasarkan issue jira 1382 = diganti jadi 31-12-2999 00:00:00
                "isLifetime": null,
                "religion": null,
                "occupationID": null,
                "positionID": null,
                "maritalStatusID": null
              },
              "guarantorContact": {
                "telephoneArea1": null,
                "telephone1": null,
                "telephoneArea2": null,
                "telephone2": null,
                "faxArea": null,
                "fax": null,
                "handphone": "08${_providerGuarantor.listGuarantorIndividual[i].cellPhoneNumber}",
                "email": null,
                "noWa": null,
                "noWa2": null,
                "noWa3": null
              },
              "guarantorIndividualAddresses": _detailAddressGuarantorIndividu,
              "creationalSpecification": {
                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                "createdBy": _preferences.getString("username"),
                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                "modifiedBy": _preferences.getString("username"),
              },
              "status": "ACTIVE"
            }
        );
      }
    }

    var _guarantorCorporates = [];
    if(_providerGuarantor.listGuarantorCompany.isNotEmpty) {
      for(int i=0; i < _providerGuarantor.listGuarantorCompany.length; i++) {
        print("establish date: ${formatDateValidateAndSubmit(DateTime.parse(_providerGuarantor.listGuarantorCompany[i].establishDate))}");
        debugPrint("CEK_DATA_EST_DATE ${_providerGuarantor.listGuarantorCompany[i].establishDate}");
        List _detailAddressGuarantorCompany = [];
        String _foreignBKGuarantorCorporate = "IKGK";
        if(_providerGuarantor.listGuarantorCompany.length < 10) {
          _foreignBKGuarantorCorporate = "IKGK00" + (i+1).toString();
        } else if(_providerGuarantor.listGuarantorCompany.length < 100) {
          _foreignBKGuarantorCorporate = "IKGK0" + (i+1).toString();
        } else if(_providerGuarantor.listGuarantorCompany.length < 1000) {
          _foreignBKGuarantorCorporate = "IKGK" + (i+1).toString();
        }
        if(_providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel.isNotEmpty){
          for(int j=0; j < _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel.length; j++){
            _detailAddressGuarantorCompany.add({
              "addressID": _preferences.getString("last_known_state") != "IDE"
                  ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].addressID != "NEW"
                  ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].addressID : "NEW" : "NEW",
              "addressSpecification": {
                "koresponden": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isCorrespondence ? "1" : "0",
                "matrixAddr": _preferences.getString("cust_type") == "PER" ? "2" : "8",
                "address": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].address,
                "rt": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].rt,
                "rw": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].rw,
                "provinsiID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.PROV_ID,
                "kabkotID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_ID,
                "kecamatanID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEC_ID,
                "kelurahanID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEL_ID,
                "zipcode": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.ZIPCODE
              },
              "contactSpecification": {
                "telephoneArea1": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phoneArea1,
                "telephone1": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phone1,
                "telephoneArea2": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phoneArea2,
                "telephone2": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phone2,
                "faxArea": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].faxArea,
                "fax": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].fax,
                "handphone": "",
                "email": ""
              },
              "addressType": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].jenisAlamatModel.KODE,
              "creationalSpecification": {
                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                "createdBy": _preferences.getString("username"),
                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                "modifiedBy": _preferences.getString("username"),
              },
              "status": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].active == 0 ? "ACTIVE" : "INACTIVE",
              "foreignBusinessID": _preferences.getString("last_known_state") != "IDE"
                  ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].foreignBusinessID != "NEW"
                  ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].foreignBusinessID : _foreignBKGuarantorCorporate : _foreignBKGuarantorCorporate,
            });
            _detailAddressesInfoNasabah.add({
              "addressID": _preferences.getString("last_known_state") != "IDE"
                  ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].addressID != "NEW"
                  ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].addressID : "NEW" : "NEW",
              "addressSpecification": {
                "koresponden": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isCorrespondence ? "1" : "0",
                "matrixAddr": _preferences.getString("cust_type") == "PER" ? "2" : "8",
                "address": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].address,
                "rt": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].rt,
                "rw": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].rw,
                "provinsiID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.PROV_ID,
                "kabkotID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_ID,
                "kecamatanID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEC_ID,
                "kelurahanID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEL_ID,
                "zipcode": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.ZIPCODE
              },
              "contactSpecification": {
                "telephoneArea1": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phoneArea1,
                "telephone1": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phone1,
                "telephoneArea2": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phoneArea2,
                "telephone2": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phone2,
                "faxArea": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].faxArea,
                "fax": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].fax,
                "handphone": "",
                "email": ""
              },
              "addressType": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].jenisAlamatModel.KODE,
              "creationalSpecification": {
                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                "createdBy": _preferences.getString("username"),
                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                "modifiedBy": _preferences.getString("username"),
              },
              "status": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].active == 0 ? "ACTIVE" : "INACTIVE",
              "foreignBusinessID": _preferences.getString("last_known_state") != "IDE"
                  ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].foreignBusinessID != "NEW"
                  ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].foreignBusinessID : _foreignBKGuarantorCorporate : _foreignBKGuarantorCorporate,
            });
          }
        }
        _guarantorCorporates.add(
            {
              "guarantorCorporateID": _preferences.getString("last_known_state") != "IDE"
                  ? _providerGuarantor.listGuarantorCompany[i].guarantorCorporateID != null
                  ? _providerGuarantor.listGuarantorCompany[i].guarantorCorporateID : _foreignBKGuarantorCorporate : _foreignBKGuarantorCorporate, // "NEW"
              "dedupScore": 0.0,
              "businessPermitSpecification": {
                "institutionType": _providerGuarantor.listGuarantorCompany[i].typeInstitutionModel.PARA_ID,
                "profile": _providerGuarantor.listGuarantorCompany[i].profilModel != null ? _providerGuarantor.listGuarantorCompany[i].profilModel.id : "",
                "fullName": _providerGuarantor.listGuarantorCompany[i].institutionName,
                "deedOfIncorporationNumber": null,
                "deedEndDate": null,
                "siupNumber": null,
                "siupStartDate": null,
                "tdpNumber": null,
                "tdpStartDate": null,
                "establishmentDate": formatDateValidateAndSubmit(DateTime.parse(_providerGuarantor.listGuarantorCompany[i].establishDate)),//"${_providerGuarantor.listGuarantorCompany[i].establishDate} 00:00:00",
                "isCompany": false,
                "authorizedCapital": 0,
                "paidCapital": 0
              },
              "businessSpecification": {
                "economySector": null,
                "businessField": null,
                "locationStatus": null,
                "businessLocation": null,
                "employeeTotal": 0,
                "bussinessLengthInMonth": 0,
                "totalBussinessLengthInMonth": 0
              },
              "customerNpwpSpecification": {
                "hasNpwp": true,
                "npwpAddress": _npwpAddress,
                "npwpFullname": _npwpFullName,
                "npwpNumber": _providerGuarantor.listGuarantorCompany[i].npwp,
                "npwpType": _npwpType,
                "pkpIdentifier": _providerRincian.signPKPSelected != null ? _providerRincian.signPKPSelected.id : "2"
              },
              "guarantorCorporateAddresses": _detailAddressGuarantorCompany,
              "creationalSpecification": {
                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                "createdBy": _preferences.getString("username"),
                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                "modifiedBy": _preferences.getString("username"),
              },
              "status": "ACTIVE"
            }
        );
      }
    }

    List _detailAddressPIC = [];
    // for(int i=0; i < _providerAddressManajemenPIC.listManajemenPICAddress.length; i++){
    //   _detailAddressPIC.add({
    //     "addressID": _preferences.getString("last_known_state") != "IDE" ? _providerAddressManajemenPIC.listManajemenPICAddress[i].addressID : "NEW",// dipastikan lagi
    //     "addressSpecification": {
    //       "koresponden": _providerAddressManajemenPIC.listManajemenPICAddress[i].isCorrespondence ? "1" : "0",
    //       "matrixAddr": "9",
    //       "address": _providerAddressManajemenPIC.listManajemenPICAddress[i].address,
    //       "rt": _providerAddressManajemenPIC.listManajemenPICAddress[i].rt,
    //       "rw": _providerAddressManajemenPIC.listManajemenPICAddress[i].rw,
    //       "provinsiID": _providerAddressManajemenPIC.listManajemenPICAddress[i].kelurahanModel.PROV_ID,
    //       "kabkotID": _providerAddressManajemenPIC.listManajemenPICAddress[i].kelurahanModel.KABKOT_ID,
    //       "kecamatanID": _providerAddressManajemenPIC.listManajemenPICAddress[i].kelurahanModel.KEC_ID,
    //       "kelurahanID": _providerAddressManajemenPIC.listManajemenPICAddress[i].kelurahanModel.KEL_ID,
    //       "zipcode": _providerAddressManajemenPIC.listManajemenPICAddress[i].kelurahanModel.ZIPCODE
    //     },
    //     "contactSpecification": {
    //       "telephoneArea1": _providerAddressManajemenPIC.listManajemenPICAddress[i].areaCode,
    //       "telephone1": _providerAddressManajemenPIC.listManajemenPICAddress[i].phone,
    //       "telephoneArea2": "",
    //       "telephone2": "",
    //       "faxArea": "",
    //       "fax": "",
    //       "handphone": "",
    //       "email": ""
    //     },
    //     "addressType": _providerAddressManajemenPIC.listManajemenPICAddress[i].jenisAlamatModel.KODE,
    //     "creationalSpecification": {
    //       "createdAt": formatDateValidateAndSubmit(DateTime.now()),
    //       "createdBy": _preferences.getString("username"),
    //       "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
    //       "modifiedBy": _preferences.getString("username"),
    //     },
    //     "status": "ACTIVE",
    //     "foreignBusinessID": "CUST001"
    //   });
    // }
    //
    // List _detailAddressShareHolderIndividu = [];
    // for(int i=0; i < _providerAddressSharedHolder.listPemegangSahamPribadiAddress.length; i++){
    //   _detailAddressShareHolderIndividu.add({
    //     "addressID": _preferences.getString("last_known_state") != "IDE" ? _providerAddressSharedHolder.listPemegangSahamPribadiAddress[i].addressID : "NEW",// dipastikan lagi
    //     "addressSpecification": {
    //       "koresponden": _providerAddressSharedHolder.listPemegangSahamPribadiAddress[i].isCorrespondence ? "1" : "0",
    //       "matrixAddr": "11",
    //       "address": _providerAddressSharedHolder.listPemegangSahamPribadiAddress[i].address,
    //       "rt": _providerAddressSharedHolder.listPemegangSahamPribadiAddress[i].rt,
    //       "rw": _providerAddressSharedHolder.listPemegangSahamPribadiAddress[i].rw,
    //       "provinsiID": _providerAddressSharedHolder.listPemegangSahamPribadiAddress[i].kelurahanModel.PROV_ID,
    //       "kabkotID": _providerAddressSharedHolder.listPemegangSahamPribadiAddress[i].kelurahanModel.KABKOT_ID,
    //       "kecamatanID": _providerAddressSharedHolder.listPemegangSahamPribadiAddress[i].kelurahanModel.KEC_ID,
    //       "kelurahanID": _providerAddressSharedHolder.listPemegangSahamPribadiAddress[i].kelurahanModel.KEL_ID,
    //       "zipcode": _providerAddressSharedHolder.listPemegangSahamPribadiAddress[i].kelurahanModel.ZIPCODE
    //     },
    //     "contactSpecification": {
    //       "telephoneArea1": _providerAddressSharedHolder.listPemegangSahamPribadiAddress[i].areaCode,
    //       "telephone1": _providerAddressSharedHolder.listPemegangSahamPribadiAddress[i].phone,
    //       "telephoneArea2": "",
    //       "telephone2": "",
    //       "faxArea": "",
    //       "fax": "",
    //       "handphone": "",
    //       "email": ""
    //     },
    //     "addressType": _providerAddressSharedHolder.listPemegangSahamPribadiAddress[i].jenisAlamatModel.KODE,
    //     "creationalSpecification": {
    //       "createdAt": formatDateValidateAndSubmit(DateTime.now()),
    //       "createdBy": _preferences.getString("username"),
    //       "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
    //       "modifiedBy": _preferences.getString("username"),
    //     },
    //     "status": "ACTIVE",
    //     "foreignBusinessID": "CUST001"
    //   });
    // }

    var _orderKaroseris = [];
    if(_providerKaroseri.listFormKaroseriObject.isNotEmpty) {
      for(int i=0; i < _providerKaroseri.listFormKaroseriObject.length; i++) {
        _orderKaroseris.add(
            {
              "orderKaroseriID": _preferences.getString("last_known_state") != "IDE" ? _providerKaroseri.listFormKaroseriObject[i].orderKaroseriID : "NEW",
              "isPksKaroseri": _providerKaroseri.listFormKaroseriObject[i].PKSKaroseri,
              "karoseriCompanyID": _providerKaroseri.listFormKaroseriObject[i].company != null ? _providerKaroseri.listFormKaroseriObject[i].company.id : null,
              "karoseriID": _providerKaroseri.listFormKaroseriObject[i].karoseri.kode,
              "karoseriPrice": double.parse(_providerKaroseri.listFormKaroseriObject[i].price.replaceAll(",", "")),
              "karoseriQty": _providerKaroseri.listFormKaroseriObject[i].jumlahKaroseri,
              "karoseriTotalPrice": double.parse(_providerKaroseri.listFormKaroseriObject[i].totalPrice.replaceAll(",", "")),
              "uangMukaKaroseri": 0,
              "flagKaroseri": 1,
              "creationalSpecification": {
                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                "createdBy": _preferences.getString("username"),
                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                "modifiedBy": _preferences.getString("username"),
              },
              "status": "ACTIVE"
            }
        );
      }
    }

    var _orderWmps = [];
    print("cek wmp ${_providerWmp.listWMP.isNotEmpty}");
    if(_providerWmp.listWMP.isNotEmpty) {
      print("masuk if");
      for(int i=0; i < _providerWmp.listWMP.length; i++) {
        _orderWmps.add(
          {
            "orderWmpID": _preferences.getString("last_known_state") != "IDE" ? _providerWmp.listWMP[i].orderWmpID : "NEW",
            "wmpNumber": _providerWmp.listWMP[i].noProposal,
            "wmpType": _providerWmp.listWMP[i].type,
            "wmpJob": "",
            "wmpSubsidyTypeID": _providerWmp.listWMP[i].typeSubsidi,
            "maxRefundAmount": _providerWmp.listWMP[i].amount,
            "status": "ACTIVE",
            "creationalSpecification": {
              "createdAt": formatDateValidateAndSubmit(DateTime.now()),
              "createdBy": _preferences.getString("username"),
              "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
              "modifiedBy": _preferences.getString("username"),
            },
          },
        );
      }
    }
    else{
      print("masuk else");
    }

    var _orderSubsidies = [];
    var _orderSubsidyDetails = [];
    if(_providerSubsidy.listInfoCreditSubsidy.isNotEmpty) {
      for(int i=0; i < _providerSubsidy.listInfoCreditSubsidy.length; i++) {
        for(int j=0; j < _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail.length; j++) {
          _orderSubsidyDetails.add(
              {
                "orderSubsidyDetailID": _preferences.getString("last_known_state") != "IDE" ? _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].orderSubsidyDetailID : "NEW",
                "installmentNumber": _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentIndex != null ? double.parse(_providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentIndex.replaceAll(",", "")) : 0,
                "installmentAmount": _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentSubsidy != null ? double.parse(_providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentSubsidy.replaceAll(",", "")) : 0,
                "creationalSpecification": {
                  "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                  "createdBy": _preferences.getString("username"),
                  "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                  "modifiedBy": _preferences.getString("username"),
                },
                "status": "ACTIVE"
              }
          );
        }
        _orderSubsidies.add(
            {
              "orderSubsidyID": _preferences.getString("last_known_state") != "IDE" ? _providerSubsidy.listInfoCreditSubsidy[i].orderSubsidyID : "NEW",
              "subsidyProviderID": _providerSubsidy.listInfoCreditSubsidy[i].giver,
              "subsidyTypeID": _providerSubsidy.listInfoCreditSubsidy[i].type != null ? _providerSubsidy.listInfoCreditSubsidy[i].type.id : "",
              "subsidyMethodTypeID": _providerSubsidy.listInfoCreditSubsidy[i].cuttingMethod != null ? _providerSubsidy.listInfoCreditSubsidy[i].cuttingMethod.id : "02",
              "refundAmount": _providerSubsidy.listInfoCreditSubsidy[i].value != ""
                  ? double.parse(_providerSubsidy.listInfoCreditSubsidy[i].value.replaceAll(",", "")) : 0,
              "refundAmountKlaim": _providerSubsidy.listInfoCreditSubsidy[i].claimValue != "" ?
              double.parse(_providerSubsidy.listInfoCreditSubsidy[i].claimValue.replaceAll(",", "")) : 0, //Nilai Klaim
              "effectiveRate": _providerSubsidy.listInfoCreditSubsidy[i].rateBeforeEff != ""
                  ? double.parse(_providerSubsidy.listInfoCreditSubsidy[i].rateBeforeEff.replaceAll(",", "")) : 0,
              "flatRate":_providerSubsidy.listInfoCreditSubsidy[i].rateBeforeFlat != "" ?
              double.parse(_providerSubsidy.listInfoCreditSubsidy[i].rateBeforeFlat.replaceAll(",", "")) : 0,
              "dpReal": 0,
              "totalInstallment": _providerSubsidy.listInfoCreditSubsidy[i].totalInstallment != "" && _providerSubsidy.listInfoCreditSubsidy[i].totalInstallment != null
                  ? _providerSubsidy.listInfoCreditSubsidy[i].totalInstallment.replaceAll(",", "") : "",
              "interestRate": 0,
              "creationalSpecification": {
                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                "createdBy": _preferences.getString("username"),
                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                "modifiedBy": _preferences.getString("username"),
              },
              "status": "ACTIVE",
              "orderSubsidyDetails": _orderSubsidyDetails
            }
        );
      }
    }

    debugPrint('CEK_SUBSIDY_SUBMIT $_orderSubsidies');

    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    // print('start');
    // for(int i=0; i<_providerKeluarga.listFormInfoKel.length; i++){
    //   print("relationshipStatus ${_preferences.getString("cust_type") == "PER" ? _providerKeluarga.listFormInfoKel[i].relationshipStatusModel.PARA_FAMILY_TYPE_ID : ""}");
    //   print("identityType ${_preferences.getString("cust_type") == "PER" ? _providerKeluarga.listFormInfoKel[i].identityModel.id : ""}");
    //   print("identityNumber ${_preferences.getString("cust_type") == "PER" ? _providerKeluarga.listFormInfoKel[i].noIdentitas : ""}");
    //   print("identityName ${_preferences.getString("cust_type") == "PER" ? _providerKeluarga.listFormInfoKel[i].namaLengkapSesuaiIdentitas : ""}");
    //   print("fullName ${_preferences.getString("cust_type") == "PER" ? _providerKeluarga.listFormInfoKel[i].namaLengkap : ""}");
    //   print("dateOfBirth ${_preferences.getString("cust_type") == "PER" ? _providerKeluarga.listFormInfoKel[i].birthDate : ""}");
    //   print("placeOfBirth ${_preferences.getString("cust_type") == "PER" ? _providerKeluarga.listFormInfoKel[i].tmptLahirSesuaiIdentitas : ""}");
    //   print("placeOfBirthKabKota ${_preferences.getString("cust_type") == "PER" ? _providerKeluarga.listFormInfoKel[i].birthPlaceModel.KABKOT_ID : ""}");
    //   print("gender ${_preferences.getString("cust_type") == "PER" ? _providerKeluarga.listFormInfoKel[i].gender == "01" ? "Laki Laki" : "Perempuan" : ""}");
    //   print("telephoneArea1 ${_preferences.getString("cust_type") == "PER" ? _providerKeluarga.listFormInfoKel[i].kodeArea : ""}");
    //   print("telephone1 ${_preferences.getString("cust_type") == "PER" ? _providerKeluarga.listFormInfoKel[i].noTlpn : ""}");
    //   print("handphone ${_preferences.getString("cust_type") == "PER" ? _providerKeluarga.listFormInfoKel[i].noHp : ""}");
    // }
    // print('end');

    var _familyInfoID = [];
    _familyInfoID.add(
      {
        "familyInfoID": _preferences.getString("last_known_state") != "IDE" ? _providerIbu.familyInfoID : "NEW",
        "relationshipStatus": "05",
        "familyIdentity": {
          "identityType": "",
          "identityNumber": "",
          "identityName": _providerIbu.controllerNamaIdentitas.text,
          "fullName": _providerIbu.controllerNamaLengkapdentitas.text,
          "alias": null,
          "title": null,
          "dateOfBirth": null,
          "placeOfBirth": "",
          "placeOfBirthKabKota": "",
          "gender": "02",
          "identityActiveStart": null,
          "identityActiveEnd": null,
          "religion": null,
          "occupationID": "0",
          "positionID": null
        },
        "creationalSpecification": {
          "createdAt": formatDateValidateAndSubmit(DateTime.now()),
          "createdBy": _preferences.getString("username"),
          "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
          "modifiedBy": _preferences.getString("username"),
        },
        "contactSpecification": {
          "telephoneArea1": _providerIbu.controllerKodeArea.text,
          "telephone1": _providerIbu.controllerTlpn.text,
          "telephoneArea2": null,
          "telephone2": null,
          "faxArea": null,
          "fax": null,
          "handphone": "",
          "email": null
        },
        "dedupScore": 0,
        "status": "ACTIVE"
      },
    );

    if (_providerKeluarga.listFormInfoKel.isNotEmpty) {
      for (int i = 0; i < _providerKeluarga.listFormInfoKel.length; i++) {
        if (_preferences.getString("cust_type") == "PER") {
          _familyInfoID.add({
            "familyInfoID": _preferences.getString("last_known_state") != "IDE" ? _providerKeluarga.listFormInfoKel[i].familyInfoID : "NEW",
            "relationshipStatus": _providerKeluarga.listFormInfoKel[i]
                .relationshipStatusModel == null ? "" : _providerKeluarga
                .listFormInfoKel[i].relationshipStatusModel.PARA_FAMILY_TYPE_ID,
            "familyIdentity": {
              "identityType": _providerKeluarga.listFormInfoKel[i]
                  .identityModel == null ? "" : _providerKeluarga
                  .listFormInfoKel[i].identityModel.id,
              "identityNumber": _providerKeluarga.listFormInfoKel[i].noIdentitas,
              "identityName": _providerKeluarga.listFormInfoKel[i]
                  .namaLengkapSesuaiIdentitas,
              "fullName": _providerKeluarga.listFormInfoKel[i].namaLengkap,
              "alias": null,
              "title": null,
              "dateOfBirth": _providerKeluarga.listFormInfoKel[i].birthDate == null
                  ? "01-01-1900 00:00:00"
                  : formatDateValidateAndSubmit(DateTime.parse(_providerKeluarga.listFormInfoKel[i].birthDate)),//"${_providerKeluarga.listFormInfoKel[i].birthDate} 00:00:00",
              "placeOfBirth": _providerKeluarga.listFormInfoKel[i]
                  .tmptLahirSesuaiIdentitas,
              "placeOfBirthKabKota": _providerKeluarga.listFormInfoKel[i]
                  .birthPlaceModel == null ? "" : _providerKeluarga
                  .listFormInfoKel[i].birthPlaceModel.KABKOT_ID,
              "gender": _providerKeluarga.listFormInfoKel[i].gender,
              "identityActiveStart": null,
              "identityActiveEnd": null,
              "religion": null,
              "occupationID": "0",
              "positionID": null
            },
            "creationalSpecification": {
              "createdAt": formatDateValidateAndSubmit(DateTime.now()),
              "createdBy": _preferences.getString("username"),
              "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
              "modifiedBy": _preferences.getString("username")
            },
            "contactSpecification": {
              "telephoneArea1": _providerKeluarga.listFormInfoKel[i].kodeArea,
              "telephone1": _providerKeluarga.listFormInfoKel[i].noTlpn,
              "telephoneArea2": null,
              "telephone2": null,
              "faxArea": null,
              "fax": null,
              "handphone": _providerKeluarga.listFormInfoKel[i].noHp != "" ? "08${_providerKeluarga.listFormInfoKel[i].noHp}" : "",
              "email": null
            },
            "dedupScore": 0,
            "status": "ACTIVE"
          });
        }
      }
    }

    var _orderProductSaleses = [];
    if(_providerSales.listInfoSales.isNotEmpty) {
      for(int i=0; i<_providerSales.listInfoSales.length; i++){
        _orderProductSaleses.add({
          "orderProductSalesID": _preferences.getString("last_known_state") != "IDE" ? _providerSales.listInfoSales[i].orderProductSalesID : "NEW",
          "salesType": _providerSales.listInfoSales[i].salesmanTypeModel.id,
          "employeeJob": _providerSales.listInfoSales[i].positionModel.id == "" ? null : _providerSales.listInfoSales[i].positionModel.id,
          "employeeID": _providerSales.listInfoSales[i].employeeModel == null ? '' : _providerSales.listInfoSales[i].employeeModel.NIK,
          "employeeHeadID": _providerSales.listInfoSales[i].employeeHeadModel == null ? '' : _providerSales.listInfoSales[i].employeeHeadModel.NIK,
          "referalContractNumber": null,
          "creationalSpecification": {
            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
            "createdBy": _preferences.getString("username"),
            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
            "modifiedBy": _preferences.getString("username"),
          },
          "status": "ACTIVE"
        });
      }
    }

    var _orderProductInsurances = [];
    if(_providerInsurance.listFormMajorInsurance.isNotEmpty) {
      String _collateralIDMajorInsurance = "";
      if(_providerKolateral.collateralTypeModel.id == "001") {
        _collateralIDMajorInsurance = "CAU001";
      } else if(_providerKolateral.collateralTypeModel.id == "002") {
        _collateralIDMajorInsurance = "COLP001";
      } else {
        _collateralIDMajorInsurance = "";
      }
      for(int i=0; i<_providerInsurance.listFormMajorInsurance.length; i++) {
        // debugPrint("CEK_ID ${_providerInsurance.listFormMajorInsurance[i].orderProductInsuranceID}");
        _orderProductInsurances.add({
          "orderProductInsuranceID": _providerInsurance.listFormMajorInsurance[i].orderProductInsuranceID,//_preferences.getString("last_known_state") != "IDE" ? _providerInsurance.listFormMajorInsurance[i].orderProductInsuranceID : "NEW",
          "insuranceCollateralReferenceSpecification": {
            "collateralID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.collateralID : _collateralIDMajorInsurance, // NEW
            "collateralTypeID": _providerKolateral.collateralTypeModel.id, // _providerInsurance.listFormMajorInsurance[i].colaType
          },
          "insuranceTypeID": _providerInsurance.listFormMajorInsurance[i].insuranceType.KODE,
          "insuranceCompanyID": _providerInsurance.listFormMajorInsurance[i].company  == null ? "02" : _providerInsurance.listFormMajorInsurance[i].company.KODE, // field perusahaan
          "insuranceProductID": _providerInsurance.listFormMajorInsurance[i].product != null ? _providerInsurance.listFormMajorInsurance[i].product.KODE : "", // field produk
          "period": _providerInsurance.listFormMajorInsurance[i].periodType == "" ? null : int.parse(_providerInsurance.listFormMajorInsurance[i].periodType),
          "type": _providerInsurance.listFormMajorInsurance[i].type,
          "type1": _providerInsurance.listFormMajorInsurance[i].coverage1 != null ? _providerInsurance.listFormMajorInsurance[i].coverage1.KODE :"", // field coverage 1
          "type2": _providerInsurance.listFormMajorInsurance[i].coverage2 != null ? _providerInsurance.listFormMajorInsurance[i].coverage2.KODE : "", // field coverage 2
          "subType": _providerInsurance.listFormMajorInsurance[i].subType != null ? _providerInsurance.listFormMajorInsurance[i].subType.KODE :"", // field coverage 1
          "subTypeCoverage": _providerInsurance.listFormMajorInsurance[i].coverageType == null ? null : _providerInsurance.listFormMajorInsurance[i].coverageType.KODE, // field sub type
          "insuranceSourceTypeID": _providerInsurance.listFormMajorInsurance[i].coverageType != null ? _providerInsurance.listFormMajorInsurance[i].coverageType.KODE : "", // field jenis pertanggungan
          "insuranceValue": _providerInsurance.listFormMajorInsurance[i].coverageValue.isEmpty ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].coverageValue.replaceAll(",", "")).round(),
          "upperLimitPercentage": _providerInsurance.listFormMajorInsurance[i].upperLimitRate == "" ? 0.0 : double.parse(_providerInsurance.listFormMajorInsurance[i].upperLimitRate),
          "upperLimitAmount": _providerInsurance.listFormMajorInsurance[i].upperLimit == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].upperLimit.replaceAll(",", "")),
          "lowerLimitPercentage": _providerInsurance.listFormMajorInsurance[i].lowerLimitRate == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].lowerLimitRate),
          "lowerLimitAmount": _providerInsurance.listFormMajorInsurance[i].lowerLimit == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].lowerLimit.replaceAll(",", "")),
          "insuranceCashFee": _providerInsurance.listFormMajorInsurance[i].priceCash == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].priceCash.replaceAll(",", "")).round(),
          "insuranceCreditFee": _providerInsurance.listFormMajorInsurance[i].priceCredit == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].priceCredit.replaceAll(",", "")).round(),
          "totalSplitFee": _providerInsurance.listFormMajorInsurance[i].totalPriceSplit == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].totalPriceSplit.replaceAll(",", "")),
          "totalInsuranceFeePercentage": _providerInsurance.listFormMajorInsurance[i].totalPriceRate == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].totalPriceRate),
          "totalInsuranceFeeAmount": _providerInsurance.listFormMajorInsurance[i].totalPrice == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].totalPrice.replaceAll(",", "")),
          "creationalSpecification": {
            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
            "createdBy": _preferences.getString("username"),
            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
            "modifiedBy": _preferences.getString("username"),
          },
          "status": "ACTIVE",
          "insurancePaymentType": null
        });
      }
    }

    if(_providerAdditionalInsurance.listFormAdditionalInsurance.isNotEmpty) {
      for(int i=0; i<_providerAdditionalInsurance.listFormAdditionalInsurance.length; i++) {
        _orderProductInsurances.add({
          "orderProductInsuranceID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].orderProductInsuranceID,//_preferences.getString("last_known_state") != "IDE" ? _providerAdditionalInsurance.listFormAdditionalInsurance[i].orderProductInsuranceID : "NEW",
          "insuranceCollateralReferenceSpecification": {
            "collateralID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.collateralID : "", // NEW
            "collateralTypeID": _providerKolateral.collateralTypeModel.id, // _providerAdditionalInsurance.listFormAdditionalInsurance[i].colaType
          },
          "insuranceTypeID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].insuranceType.KODE,
          "insuranceCompanyID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].company  == null ? "02" : _providerAdditionalInsurance.listFormAdditionalInsurance[i].company.KODE,
          "insuranceProductID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].product.KODE,
          "period": _providerAdditionalInsurance.listFormAdditionalInsurance[i].periodType == "" ? null : int.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].periodType),
          "type": "",
          "type1": "",
          "type2": "",
          "subType": "",
          "subTypeCoverage": "",
          "insuranceSourceTypeID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageType != null ? _providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageType.KODE : "", // field jenis pertanggungan
          "insuranceValue": _providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageValue.isEmpty ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageValue.replaceAll(",", "")).round(),
          "upperLimitPercentage": _providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimitRate == "" ? 0.0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimitRate),
          "upperLimitAmount": _providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimit == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimit.replaceAll(",", "")),
          "lowerLimitPercentage": _providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimitRate == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimitRate),
          "lowerLimitAmount": _providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimit == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimit.replaceAll(",", "")),
          "insuranceCashFee": _providerAdditionalInsurance.listFormAdditionalInsurance[i].priceCash == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].priceCash.replaceAll(",", "")).round(),
          "insuranceCreditFee": _providerAdditionalInsurance.listFormAdditionalInsurance[i].priceCredit == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].priceCredit.replaceAll(",", "")).round(),
          "totalSplitFee": "",
          "totalInsuranceFeePercentage": _providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPriceRate == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPriceRate),
          "totalInsuranceFeeAmount": _providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPrice == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPrice.replaceAll(",", "")),
          "creationalSpecification": {
            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
            "createdBy": _preferences.getString("username"),
            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
            "modifiedBy": _preferences.getString("username"),
          },
          "status": "ACTIVE",
          "insurancePaymentType": null
        });
      }
    }

    var _orderFees = [];
    if(_providerCreditStructure.listInfoFeeCreditStructure.isNotEmpty) {
      bool _isProvisiEmpty = true;
      bool _isAdminEmpty = true;
      for (int i = 0; i <  _providerCreditStructure.listInfoFeeCreditStructure.length; i++) {
        if(_providerCreditStructure.listInfoFeeCreditStructure[i].feeTypeModel.id == "01"){
          _isAdminEmpty = false;
        }
        if(_providerCreditStructure.listInfoFeeCreditStructure[i].feeTypeModel.id == "02"){
          _isProvisiEmpty = false;
        }
        _orderFees.add({
          "orderFeeID": _preferences.getString("last_known_state") != "IDE" ? _providerCreditStructure.listInfoFeeCreditStructure[i].orderFeeID : "NEW",
          "feeTypeID": _providerCreditStructure.listInfoFeeCreditStructure[i].feeTypeModel.id,
          "cashFee": _providerCreditStructure.listInfoFeeCreditStructure[i].cashCost == "" ? null : double.parse(_providerCreditStructure.listInfoFeeCreditStructure[i].cashCost.replaceAll(",", "")).round(),
          "creditFee": _providerCreditStructure.listInfoFeeCreditStructure[i].creditCost == "" ? null : double.parse(_providerCreditStructure.listInfoFeeCreditStructure[i].creditCost.replaceAll(",", "")).round(),
          "totalFee": _providerCreditStructure.listInfoFeeCreditStructure[i].totalCost == "" ? null : double.parse(_providerCreditStructure.listInfoFeeCreditStructure[i].totalCost.replaceAll(",", "")).round(),
          "creationalSpecification": {
            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
            "createdBy": _preferences.getString("username"),
            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
            "modifiedBy": _preferences.getString("username"),
          },
          "status": "ACTIVE"
        });
      }

      if(_isAdminEmpty){
        _orderFees.add({
          "orderFeeID": "NEW",
          "feeTypeID": "01",
          "cashFee": 0,
          "creditFee": 0,
          "totalFee": 0,
          "creationalSpecification": {
            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
            "createdBy": _preferences.getString("username"),
            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
            "modifiedBy": _preferences.getString("username"),
          },
          "status": "ACTIVE"
        });
      }

      if(_isProvisiEmpty){
        _orderFees.add({
          "orderFeeID": "NEW",
          "feeTypeID": "02",
          "cashFee": 0,
          "creditFee": 0,
          "totalFee": 0,
          "creationalSpecification": {
            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
            "createdBy": _preferences.getString("username"),
            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
            "modifiedBy": _preferences.getString("username"),
          },
          "status": "ACTIVE"
        });
      }
    }
    else{
      _orderFees.add({
        "orderFeeID": "NEW",
        "feeTypeID": "01",
        "cashFee": 0,
        "creditFee": 0,
        "totalFee": 0,
        "creationalSpecification": {
          "createdAt": formatDateValidateAndSubmit(DateTime.now()),
          "createdBy": _preferences.getString("username"),
          "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
          "modifiedBy": _preferences.getString("username"),
        },
        "status": "ACTIVE"
      });
      _orderFees.add({
        "orderFeeID": "NEW",
        "feeTypeID": "02",
        "cashFee": 0,
        "creditFee": 0,
        "totalFee": 0,
        "creationalSpecification": {
          "createdAt": formatDateValidateAndSubmit(DateTime.now()),
          "createdBy": _preferences.getString("username"),
          "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
          "modifiedBy": _preferences.getString("username"),
        },
        "status": "ACTIVE"
      });
    }

    var _surveyResultDetails = [];
    if(_providerSurvey.listResultSurveyDetailSurveyModel.isNotEmpty) {
      for(int i=0; i < _providerSurvey.listResultSurveyDetailSurveyModel.length; i++) {
        _surveyResultDetails.add({
          "surveyResultDetailID": "NEW",
          "infoEnvironment": _providerSurvey.listResultSurveyDetailSurveyModel[i].environmentalInformationModel.id,
          "sourceInfo": _providerSurvey.listResultSurveyDetailSurveyModel[i].recourcesInfoSurveyModel.PARA_INFORMATION_SOURCE_ID,
          "sourceNameInfo": _providerSurvey.listResultSurveyDetailSurveyModel[i].resourceInformationName,
          "status": "ACTIVE",
          "creationalSpecification": {
            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
            "createdBy": _preferences.getString("username"),
            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
            "modifiedBy": _preferences.getString("username"),
          }
        });
      }
    }

    var _surveyResultAssets = [];
    if(_providerSurvey.listResultSurveyAssetModel.isNotEmpty){
      for(int i=0; i< _providerSurvey.listResultSurveyAssetModel.length; i++){
        _surveyResultAssets.add({
              "surveyResultAssetID": "NEW",
              "assetType": _providerSurvey.listResultSurveyAssetModel[i].assetTypeModel != null ? _providerSurvey.listResultSurveyAssetModel[i].assetTypeModel.id : null,
              "assetAmount": _providerSurvey.listResultSurveyAssetModel[i].valueAsset != "" ? double.parse(_providerSurvey.listResultSurveyAssetModel[i].valueAsset.replaceAll(",", "")).toInt() : 0,
              "ownStatus": _providerSurvey.listResultSurveyAssetModel[i].ownershipModel != null ? _providerSurvey.listResultSurveyAssetModel[i].ownershipModel.id : null,
              "sizeofLand": _providerSurvey.listResultSurveyAssetModel[i].surfaceBuildingArea != "" ? _providerSurvey.listResultSurveyAssetModel[i].surfaceBuildingArea : null,
              "streetType": _providerSurvey.listResultSurveyAssetModel[i].roadTypeModel != null ? _providerSurvey.listResultSurveyAssetModel[i].roadTypeModel.kode : null,
              "electricityType": _providerSurvey.listResultSurveyAssetModel[i].electricityTypeModel != null ? _providerSurvey.listResultSurveyAssetModel[i].electricityTypeModel.PARA_ELECTRICITY_ID : null,
              "electricityBill": _providerSurvey.listResultSurveyAssetModel[i].electricityBills != "" ? double.parse(_providerSurvey.listResultSurveyAssetModel[i].electricityBills.replaceAll(",", "")).toInt() : 0,
              "expiredContractDate": _providerSurvey.listResultSurveyAssetModel[i].endDateLease != null ? formatDateValidateAndSubmit(_providerSurvey.listResultSurveyAssetModel[i].endDateLease) : null,
              "longOfStay": _providerSurvey.listResultSurveyAssetModel[i].lengthStay != "" ? _providerSurvey.listResultSurveyAssetModel[i].lengthStay : null,
              "status": "ACTIVE",
              "creationalSpecification": {
                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                "createdBy": _preferences.getString("username"),
                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                "modifiedBy": _preferences.getString("username")
              }
        });
      }
    }

    // var __body = jsonEncode({
    //   "orderProducts": [
    //     {
    //       "orderProductID": orderProductId,
    //       "orderProductSpecification": {
    //         "financingTypeID": _preferences.getString("cust_type") == "PER" ? _providerFoto.typeOfFinancingModelSelected.financingTypeId : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId,
    //         "ojkBusinessTypeID": _preferences.getString("cust_type") == "PER" ? _providerFoto.kegiatanUsahaSelected.id : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId,
    //         "ojkBussinessDetailID": _preferences.getString("cust_type") == "PER" ? _providerFoto.jenisKegiatanUsahaSelected.id : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId,
    //         "orderUnitSpecification": {
    //           "objectGroupID": _providerUnitObject.groupObjectSelected.KODE,
    //           "objectID": _providerUnitObject.objectSelected.id,
    //           "productTypeID": _providerUnitObject.productTypeSelected.id,
    //           "objectBrandID": _providerUnitObject.brandObjectSelected.id,
    //           "objectTypeID": _providerUnitObject.objectTypeSelected.id,
    //           "objectModelID": _providerUnitObject.controllerUsageObjectModel.text,
    //           "objectUsageID": _providerUnitObject.objectUsageModel.id,
    //           "objectPurposeID": _providerUnitObject.objectPurposeSelected.id,
    //           "modelDetail": _providerUnitObject.controllerDetailModel.text
    //         },
    //         "salesGroupID": _providerUnitObject.groupSalesSelected.kode,
    //         "orderSourceID": _providerUnitObject.sourceOrderSelected.kode,
    //         "orderSourceName": _providerUnitObject.sourceOrderNameSelected.kode,
    //         "orderProductDealerSpecification": {
    //           "isThirdParty": false,
    //           "thirdPartyTypeID": _providerUnitObject.thirdPartyTypeSelected.kode,
    //           "thirdPartyID": _providerUnitObject.thirdPartySelected.kode,
    //           "thirdPartyActivity": _providerUnitObject.activitiesModel.kode,
    //           "sentradID": _preferences.getString("SentraD"), //_providerUnitObject.thirdPartySelected.sentraId,
    //           "unitdID": _preferences.getString("UnitD"), //_providerUnitObject.thirdPartySelected.unitId,
    //         },
    //         "programID": _providerUnitObject.programSelected != null ? _providerUnitObject.programSelected.kode : "",
    //         "rehabType": _providerUnitObject.rehabTypeSelected != null ? _providerUnitObject.rehabTypeSelected.id : "",
    //         "referenceNumber": _providerUnitObject.referenceNumberModel != null ? _providerUnitObject.referenceNumberModel.noPK : "",
    //         "applicationUnitContractNumber": null,
    //         "orderProductSaleses": _orderProductSaleses
    //         ,
    //         "orderKaroseris": _orderKaroseris,
    //         "orderProductInsurances": _orderProductInsurances,
    //         "orderWmps": _orderWmps
    //       },
    //       "orderCreditStructure": {
    //         "installmentTypeID": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.id : "",
    //         "tenor": _providerCreditStructure.periodOfTimeSelected != null ? _providerCreditStructure.periodOfTimeSelected : "",
    //         "paymentMethodID": _providerCreditStructure.paymentMethodSelected != null ? _providerCreditStructure.paymentMethodSelected.id : "",
    //         "effectiveRate": _providerCreditStructure.controllerInterestRateEffective.text != "" ? _providerCreditStructure.controllerInterestRateEffective.text : "",
    //         "flatRate": _providerCreditStructure.controllerInterestRateFlat.text != "" ? _providerCreditStructure.controllerInterestRateFlat.text : "",
    //         "objectPrice": _providerCreditStructure.controllerObjectPrice.text != "" ? double.parse(_providerCreditStructure.controllerObjectPrice.text.replaceAll(",", "")) : 0,
    //         "karoseriTotalPrice": _providerCreditStructure.controllerKaroseriTotalPrice.text != "" ? double.parse(_providerCreditStructure.controllerKaroseriTotalPrice.text.replaceAll(",", "")) : 0,
    //         "totalPrice": _providerCreditStructure.controllerTotalPrice.text != "" ? double.parse(_providerCreditStructure.controllerTotalPrice.text.replaceAll(",", "")) : 0,
    //         "nettDownPayment": _providerCreditStructure.controllerNetDP.text != "" ? double.parse(_providerCreditStructure.controllerNetDP.text.replaceAll(",", "")) : 0,
    //         "declineNInstallment": 0,
    //         "paymentOfYear": 1,
    //         "branchDownPayment": _providerCreditStructure.controllerBranchDP.text != "" ? double.parse(_providerCreditStructure.controllerBranchDP.text.replaceAll(",", "")) : 0,
    //         "grossDownPayment": _providerCreditStructure.controllerGrossDP.text != "" ? double.parse(_providerCreditStructure.controllerGrossDP.text.replaceAll(",", "")) : 0,
    //         "totalLoan": _providerCreditStructure.controllerTotalLoan.text != "" ? double.parse(_providerCreditStructure.controllerTotalLoan.text.replaceAll(",", "")) : 0,
    //         "installmentAmount": _providerCreditStructure.controllerInstallment.text != "" ? double.parse(_providerCreditStructure.controllerInstallment.text.replaceAll(",", "")) : 0,
    //         "ltvRatio": _providerCreditStructure.controllerLTV.text != "" ? double.parse(_providerCreditStructure.controllerLTV.text.replaceAll(",", "")) : 0,
    //         "pencairan": 0,
    //         "interestAmount": _providerCreditStructure.interestAmount,
    //         "gpType": null,
    //         "newTenor": 0,
    //         "totalStepping": 0,
    //         "installmentBalloonPayment": {
    //           "balloonType": _providerCreditStructureType.radioValueBalloonPHOrBalloonInstallment,
    //           "lastInstallmentPercentage": _providerCreditStructureType.controllerInstallmentPercentage.text != "" ? double.parse(_providerCreditStructureType.controllerInstallmentPercentage.text.replaceAll(",", "")) : 0,
    //           "lastInstallmentValue": _providerCreditStructureType.controllerInstallmentValue.text != "" ? double.parse(_providerCreditStructureType.controllerInstallmentValue.text.replaceAll(",", "")) : 0
    //         },
    //         "installmentSchedule": {
    //           "installmentType": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.name : null, // OLD: ambil dari ID
    //           "paymentMethodType": _providerCreditStructure.paymentMethodSelected != null ? _providerCreditStructure.paymentMethodSelected.name : null,  // OLD: ambil dari ID
    //           "installment": _providerCreditStructure.controllerInstallment.text != "" ? double.parse(_providerCreditStructure.controllerInstallment.text.replaceAll(",", "")) : 0,
    //           "installmentRound": 0,
    //           "installmentMethod": 0,
    //           "lastKnownOutstanding": 0,
    //           "minInterest": 0,
    //           "maxInterest": 0,
    //           "futureValue": 0,
    //           "isInstallment": false,
    //           "roundingValue": null,
    //           "balloonInstallment": 0,
    //           "gracePeriode": 0,
    //           "gracePeriodes": [],
    //           "seasonal": 0,
    //           "steppings": [],
    //           "installmentScheduleDetails": []
    //         },
    //         "dsr": _providerCreditIncome.controllerDebtComparison.text != "" ? double.parse(_providerCreditIncome.controllerDebtComparison.text.replaceAll(",", "")) : 0,
    //         "dir": _providerCreditIncome.controllerIncomeComparison.text != "" ? double.parse(_providerCreditIncome.controllerIncomeComparison.text.replaceAll(",", "")) : 0,
    //         "dsc": _providerCreditIncome.controllerDebtComparison.text != "" ? double.parse(_providerCreditIncome.controllerDebtComparison.text.replaceAll(",", "")) : 0,
    //         "irr": _providerCreditIncome.controllerIRR.text != "" ? double.parse(_providerCreditIncome.controllerIRR.text.replaceAll(",", "")) : 0,
    //         "installmentDetails": _installmentDetails,
    //         "realDSR": 0
    //       },
    //       "orderFees": _orderFees,
    //       "orderSubsidies": [],
    //       "orderProductAdditionalSpecification": {
    //         "virtualAccountID": null,
    //         "virtualAccountValue": 0,
    //         "cashingPurposeID": null,
    //         "cashingTypeID": null
    //       },
    //       "creationalSpecification": {
    //         "createdAt": DateTime.now().toString(),
    //         "createdBy": _preferences.getString("username"),
    //         "modifiedAt": DateTime.now().toString(),
    //         "modifiedBy": _preferences.getString("username"),
    //       },
    //       "status": "ACTIVE",
    //       "collateralAutomotives": [
    //         {
    //           "collateralID": _providerKolateral.collateralTypeModel != null ? _providerKolateral.collateralTypeModel.id : "",
    //           "isCollaSameUnit": _providerKolateral.radioValueIsCollateralSameWithUnitOto, // Jika Collateral = Unit dipilih YA, maka kirim FALSE. Begitu juga sebaliknya (0 = YA/true | 1 = TIDAK/false)
  //             "isMultiUnitCollateral": _providerKolateral.radioValueForAllUnitOto, // ya = false = 0 | tidak = true = 1
  //             "isCollaNameSameWithCustomerName": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto, // Jika Nama Jaminan = Pemohon dipilih YA, maka kirim FALSE. Begitu juga sebaliknya (0 = YA/true | 1 = TIDAK/false)
    //           "identitySpecification": {
    //             "identityType": _providerKolateral.identityTypeSelectedAuto != null ? _providerKolateral.identityTypeSelectedAuto.id : "",
    //             "identityNumber": _providerKolateral.controllerIdentityNumberAuto.text != "" ? _providerKolateral.controllerIdentityNumberAuto.text : "",
    //             "identityName": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1 ? _providerInfoNasabah.controllerNamaLengkapSesuaiIdentitas.text : null,
    //             "fullName": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1 ? _providerInfoNasabah.controllerNamaLengkap.text : _providerKolateral.controllerNameOnCollateralAuto.text,
    //             "alias": null,
    //             "title": null,
    //             "dateOfBirth": _providerKolateral.controllerBirthDateAuto.text != "" ? _providerKolateral.controllerBirthDateAuto.text : "",
    //             "placeOfBirth": _providerKolateral.controllerBirthPlaceValidWithIdentityAuto.text != "" ? _providerKolateral.controllerBirthPlaceValidWithIdentityAuto.text : "",
    //             "placeOfBirthKabKota": _providerKolateral.controllerBirthPlaceValidWithIdentityLOVAuto.text != "" ? _providerKolateral.controllerBirthPlaceValidWithIdentityLOVAuto.text : "",
    //             "gender": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1 ? _providerInfoNasabah.radioValueGender == "01" ? "Laki Laki" : "Perempuan" : null,
    //             "identityActiveStart": null,
    //             "identityActiveEnd": null,
    //             "religion": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1 ? _providerInfoNasabah.religionSelected == null ? null : _providerInfoNasabah.religionSelected.id : null,
    //             "occupationID": _providerFoto.occupationSelected != null ? _providerFoto.occupationSelected.KODE : "",
    //             "positionID": null
    //           },
    //           "collateralAutomotiveSpecification": {
    //             "orderUnitSpecification": {
    //               "objectGroupID": _providerKolateral.groupObjectSelected != null ? _providerKolateral.groupObjectSelected.KODE : "",
    //               "objectID": _providerKolateral.objectSelected != null ? _providerKolateral.objectSelected.id : "",
    //               "productTypeID": _providerKolateral.productTypeSelected != null ? _providerKolateral.productTypeSelected.id : "",
    //               "objectBrandID": _providerKolateral.brandObjectSelected != null ? _providerKolateral.brandObjectSelected.id : "",
    //               "objectTypeID": _providerKolateral.objectTypeSelected != null ? _providerKolateral.objectTypeSelected.id : "",
    //               "objectModelID": _providerKolateral.controllerUsageObjectModel.text != "" ? _providerKolateral.controllerUsageObjectModel.text : "",
    //               "objectUsageID": _providerKolateral.objectUsageSelected != null ? _providerKolateral.objectUsageSelected.id : "",
    //               "objectPurposeID": ""
    //             },
    //             "productionYear": _providerKolateral.yearProductionSelected != null ? _providerKolateral.yearProductionSelected : "",
    //             "registrationYear": _providerKolateral.yearRegistrationSelected != null ? _providerKolateral.yearRegistrationSelected : "",
    //             "isYellowPlate": _providerKolateral.radioValueYellowPlat == 0 ? true : false,
    //             "isBuiltUp": _providerKolateral.radioValueBuiltUpNonATPM == 0 ? true : false,
    //             "utjSpecification": {
    //               "bpkbNumber": _providerKolateral.controllerBPKPNumber.text != "" ? _providerKolateral.controllerBPKPNumber.text : "",
    //               "chassisNumber": _providerKolateral.controllerFrameNumber.text != "" ? _providerKolateral.controllerFrameNumber.text : "",
    //               "engineNumber": _providerKolateral.controllerMachineNumber.text != "" ? _providerKolateral.controllerMachineNumber.text : "",
    //               "policeNumber": _providerKolateral.controllerPoliceNumber.text != "" ? _providerKolateral.controllerPoliceNumber.text : ""
    //             },
    //             "gradeUnit": _providerKolateral.controllerGradeUnit.text != "" ? _providerKolateral.controllerGradeUnit.text : "",
    //             "utjFacility": _providerKolateral.controllerFasilitasUTJ.text != "" ? _providerKolateral.controllerFasilitasUTJ.text : "",
    //             "bidderName": _providerKolateral.controllerNamaBidder.text != "" ? _providerKolateral.controllerNamaBidder.text : "",
    //             "showroomPrice": _providerKolateral.controllerHargaJualShowroom.text != "" ? _providerKolateral.controllerHargaJualShowroom.text : "",
    //             "additionalEquipment": _providerKolateral.controllerPerlengkapanTambahan.text != "" ? _providerKolateral.controllerPerlengkapanTambahan.text : "",
    //             "mpAdira": _providerKolateral.controllerMPAdira.text != "" ? _providerKolateral.controllerMPAdira.text : "",
    //             "physicalRecondition": _providerKolateral.controllerRekondisiFisik.text != "" ? double.parse(_providerKolateral.controllerRekondisiFisik.text.replaceAll(",", "")) : 0,
    //             "isProper": _providerKolateral.radioValueWorthyOrUnworthy,
    //             "ltvRatio": 0,
    //             "appraisalSpecification": {
    //               "collateralDp": double.parse(_providerKolateral.controllerDPJaminan.text == '' ? '0' : _providerKolateral.controllerDPJaminan.text.replaceAll(",", "")),
    //               "maximumPh": double.parse(_providerKolateral.controllerPHMaxAutomotive.text == '' ? '0' : _providerKolateral.controllerPHMaxAutomotive.text.replaceAll(",", "")),
    //               "appraisalPrice": double.parse(_providerKolateral.controllerTaksasiPriceAutomotive.text == '' ? '0' : _providerKolateral.controllerTaksasiPriceAutomotive.text.replaceAll(",", "")),
    //               "collateralUtilizationID": _providerKolateral.collateralUsageOtoSelected != null ? _providerKolateral.collateralUsageOtoSelected.id : "" //""
    //             },
    //             "collateralAutomotiveAdditionalSpecification": {
    //               "capacity": 0,
    //               "color": null,
    //               "manufacturer": null,
    //               "serialNumber": null,
    //               "invoiceNumber": null,
    //               "stnkActiveDate": null,
    //               "bpkbAddress": null,
    //               "bpkbIdentityTypeID": null
    //             },
    //           },
    //           "status": "ACTIVE",
    //           "creationalSpecification": {
    //             "createdAt": DateTime.now().toString(),
    //             "createdBy": _preferences.getString("username"),
    //             "modifiedAt": DateTime.now().toString(),
    //             "modifiedBy": _preferences.getString("username"),
    //           }
    //         }
    //       ],
    //       "collateralProperties": [],
    //       "orderProductProcessingCompletion": {
    //         "lastKnownOrderProductState": "APPLICATION_DOCUMENT",
    //         "nextKnownOrderProductState": "APPLICATION_DOCUMENT"
    //       },
    //       "isWithoutColla": _providerKolateral.collateralTypeModel.id == "003" ? 1 : 0,// _providerUnitObject.groupObjectSelected.KODE == "003" ? 1 : 0,
    //       "isSaveKaroseri": false
    //     }
    //   ]});
    //
    // final __response = await _http.post(
    //     "http://192.168.57.147/orca_api/public/login",
    //     body: __body,
    //     headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    // );
    //
    // print(__response);

    // bool _isHasNPWP(){
    //   if(_preferences.getString("cust_type") == "PER"){
    //     return _providerInfoNasabah.radioValueIsHaveNPWP == 1;
    //   }
    //   else{
    //     return _providerRincian.radioValueIsHaveNPWP == 1;
    //   }
    // }

    print("ini LME = $_dataCL");
    var _body = jsonEncode({
      "creditLimitDTO": _preferences.getString("status_aoro") == "1" || _preferences.getString("status_aoro") == "2" ? {
        "lmeId": _dataCL[0]['lme_id'] != "null" || _dataCL[0]['lme_id'] != "" ? _dataCL[0]['lme_id'] : "", // "",
        "flagEligible": _dataCL[0]['flag_eligible'] != "null" || _dataCL[0]['flag_eligible'] != "" ? _dataCL[0]['flag_eligible'] : "", // "",
        "disburseType": _dataCL[0]['disburse_type'] != "" ? int.parse(_dataCL[0]['disburse_type']) : 0, // 1,
        "productId": _dataCL[0]['product_id'] != "null" || _dataCL[0]['product_id'] != "" ? _dataCL[0]['product_id'] : "", // "",
        "installmentAmt": _dataCL[0]['installment_amount'] != "" ? int.parse(_dataCL[0]['installment_amount']) : 0, // 0,
        "phAmt": _dataCL[0]['ph_amount'] != "" ? int.parse(_dataCL[0]['ph_amount']) : 0, // 0,
        "voucherCode": _dataCL[0]['voucher_code'] != "null" || _dataCL[0]['voucher_code'] != "" ? _dataCL[0]['voucher_code'] : "", // "",
        "tenor": _dataCL[0]['tenor'] != "" ? int.parse(_dataCL[0]['tenor']) : 0, // 0,
        "customerGrading": _dataCL[0]['grading'] !="null" || _dataCL[0]['grading'] != "" ? _dataCL[0]['grading'] : "", // "",
        "jenisPenawaran": _dataCL[0]['jenis_penawaran'] != "null" || _dataCL[0]['jenis_penawaran'] != "" ? _dataCL[0]['jenis_penawaran'] : "", // "003",
        "urutanPencairan": _dataCL[0]['pencairan_ke'] != "" ? int.parse(_dataCL[0]['pencairan_ke']) : 0, // 0,
        "jenisOpsi": _dataCL[0]['opsi_multidisburse'] != "null" || _dataCL[0]['opsi_multidisburse'] != "" ? _dataCL[0]['opsi_multidisburse'] : "00", // "00",
        "maxPlafond": double.parse(_dataCL[0]['max_ph']), // 0,
        "maxTenor": _dataCL[0]['max_tenor'] != "null" ? int.parse(_dataCL[0]['max_tenor']) : 0, // 0,
        "noReferensi": _dataCL[0]['no_referensi'] != "null" || _dataCL[0]['no_referensi'] != "" ? _dataCL[0]['no_referensi'] : "", // "", _preferences.getString("jenis_penawaran") == "002" ? _providerUnitObject.controllerReferenceNumber.text != "" ? _providerUnitObject.controllerReferenceNumber.text : ""
        "maxInstallment": _dataCL[0]['max_installment'] != "" ? double.parse(_dataCL[0]['max_installment']) : 0, // 0,
        "portfolio": _dataCL[0]['portfolio'] != "null" || _dataCL[0]['portfolio'] != "" ? _dataCL[0]['portfolio'] : "", // "",
        "activationDate": "", // _dataCL[0]['activation_date'] != "null" || _dataCL[0]['activation_date'] != "" ? _dataCL[0]['activation_date'] : ""
      } : {},
      "applicationDTO": {
        "customerDTO": {
          "customerID": _preferences.getString("last_known_state") != "IDE" ? _providerInfoNasabah.customerID :"NEW",// perlu di rencanakan mau ditaruh di sqlite atau di sharedPreference
          "customerType": _preferences.getString("cust_type") == "PER" ? "PER" : "COM",
          "customerSpecification": {
            "customerNPWP": {
              "npwpAddress": _npwpAddress,//_providerInfoNasabah.radioValueIsHaveNPWP == 1 ? _providerInfoNasabah.controllerAlamatSesuaiNPWP.text : _providerInfoAlamat.listAlamatKorespondensi[0].address,
              "npwpFullname": _npwpFullName,//_providerInfoNasabah.radioValueIsHaveNPWP == 1 ? _providerInfoNasabah.controllerNamaSesuaiNPWP.text  : _providerInfoNasabah.controllerNamaLengkapSesuaiIdentitas.text,
              "npwpNumber": _npwpNumber,//_providerInfoNasabah.radioValueIsHaveNPWP == 1 ? _providerInfoNasabah.controllerNoNPWP.text : "00000000000000",
              "npwpType": _npwpType,
              "hasNpwp": _providerInfoNasabah.radioValueIsHaveNPWP == 1 ? true : false,//_providerInfoNasabah.radioValueIsHaveNPWP == 1 ? true : false,
              "pkpIdentifier": _preferences.getString("cust_type") == "PER" ? _providerInfoNasabah.tandaPKPSelected != null ? _providerInfoNasabah.tandaPKPSelected.id : "2" : _providerRincian.signPKPSelected != null ? _providerRincian.signPKPSelected.id : "2",
            },
            "detailAddresses": _detailAddressesInfoNasabah,
            "customerGuarantor": {
              "isGuarantor": _providerGuarantor.radioValueIsWithGuarantor == 0,
              "guarantorCorporates": _guarantorCorporates,
              "guarantorIndividuals": _guarantorIndividuals
            }
          },
          "customerIndividualDTO": _preferences.getString("cust_type") == "PER"
              ?
          [
            {
              "customerIndividualID": _preferences.getString("last_known_state") != "IDE" ? _providerInfoNasabah.customerIndividualID : "CUST001", // NEW
              "numberOfDependents": _providerInfoNasabah.controllerJumlahTanggungan.text != "" ? _providerInfoNasabah.controllerJumlahTanggungan.text : "0",
              "groupCustomer": _providerInfoNasabah.gcModel.id,
              "statusRelationship": _providerInfoNasabah.maritalStatusSelected.id,
              "educationID": _providerInfoNasabah.educationSelected.id,
              "customerIdentity": {
                "identityType": _providerInfoNasabah.identitasModel.id,
                "identityNumber": _providerInfoNasabah.controllerNoIdentitas.text,
                "identityName": _providerInfoNasabah.controllerNamaLengkapSesuaiIdentitas.text,
                "fullName": _providerInfoNasabah.controllerNamaLengkap.text,
                "alias": null,
                "title": null,
                "dateOfBirth": formatDateValidateAndSubmit(_providerInfoNasabah.initialDateForTglLahir),//_providerInfoNasabah.controllerTglLahir.text,
                "placeOfBirth": _providerInfoNasabah.controllerTempatLahirSesuaiIdentitas.text,
                "placeOfBirthKabKota": _providerInfoNasabah.birthPlaceSelected.KABKOT_ID,
                "gender": _providerInfoNasabah.radioValueGender,
                "identityActiveStart": _preferences.getString("cust_type") == "PER" ? _providerInfoNasabah.controllerTglIdentitas.text != "" ? formatDateValidateAndSubmit(_providerInfoNasabah.initialDateForTglIdentitas) : formatDateValidateAndSubmit(DateTime.now()) : formatDateValidateAndSubmit(DateTime.now()),
                "identityActiveEnd": _preferences.getString("cust_type") == "PER" ? _providerInfoNasabah.valueCheckBox ? "31-12-2999 00:00:00" : _providerInfoNasabah.controllerIdentitasBerlakuSampai.text != "" ? formatDateValidateAndSubmit(_providerInfoNasabah.initialDateForTglIdentitas) : "31-12-2999 00:00:00" : "31-12-2999 00:00:00", // berdasarkan issue jira 1382 = diganti jadi 31-12-2999 00:00:00
                "religion": _providerInfoNasabah.religionSelected != null ? _providerInfoNasabah.religionSelected.id : "99", // nilai defaultnya 01, per tanggal 06.05.2021 diganti kosong, per tanggal 07.05.2021 diganti 99
                "occupationID": _providerFoto.occupationSelected.KODE,
                "positionID": null,
                "isLifetime": _providerInfoNasabah.valueCheckBox
              },
              "customerContact": {
                "telephoneArea1": "",
                "telephone1": "",
                "telephoneArea2": null,
                "telephone2": null,
                "faxArea": null,
                "fax": null,
                "handphone": _providerInfoNasabah.controllerNoHp.text != "" ? "08${_providerInfoNasabah.controllerNoHp.text}" : "",
                "email": _providerInfoNasabah.controllerEmail.text != "" ? _providerInfoNasabah.controllerEmail.text : "",
                "noWa": _providerInfoNasabah.isNoHp1WA ? 1 : 0,
                "noWa2": null,
                "noWa3": null
              },
              "customerOccupation": [
                {
                  "customerOccupationID": _preferences.getString("last_known_state") != "IDE" ? _providerOccupation.customerOccupationID : "IP001",
                  "occupationID": _preferences.getString("cust_type") == "PER" ? _providerFoto.occupationSelected.KODE : "",
                  "businessName": _providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07" ? _providerOccupation.controllerBusinessName.text : _providerFoto.occupationSelected.KODE != "08" ? _providerOccupation.controllerCompanyName.text : null,// _preferences.getString("cust_type") == "PER" ? _providerOccupation.controllerBusinessName.text : null,
                  "businessType": _providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07"
                      ? _providerOccupation.typeOfBusinessModelSelected.id
                      : _providerFoto.occupationSelected.KODE != "08"
                      ? _providerOccupation.companyTypeModelSelected.id : null,//_preferences.getString("cust_type") == "PER" ? _providerRincian.typeInstitutionSelected == null ? null : _providerRincian.typeInstitutionSelected.PARA_ID : null,
                  "professionType": _providerFoto.occupationSelected != null
                        ? _providerFoto.occupationSelected.KODE == "08"
                          ? _providerOccupation.professionTypeModelSelected.id : null
                        : null,
                  "pepType": _providerFoto.occupationSelected.KODE != "05" || _providerFoto.occupationSelected.KODE != "07" || _providerFoto.occupationSelected.KODE != "08" ? _providerOccupation.pepModelSelected == null ? "" : _providerOccupation.pepModelSelected.KODE : "",
                  "employeeStatus": _preferences.getString("cust_type") == "PER" ?_providerOccupation.employeeStatusModelSelected != null ? _providerOccupation.employeeStatusModelSelected.id : "" : "",
                  "businessSpecification": {
                    "economySector": _providerOccupation.sectorEconomicModelSelected != null ? _providerOccupation.sectorEconomicModelSelected.KODE : null,
                    "businessField": _preferences.getString("cust_type") == "PER" ? _providerOccupation.businessFieldModelSelected == null ? null : _providerOccupation.businessFieldModelSelected.KODE : null,
                    "locationStatus": _preferences.getString("cust_type") == "PER" ? _providerOccupation.statusLocationModelSelected == null ? null : _providerOccupation.statusLocationModelSelected.id : null,
                    "businessLocation": _preferences.getString("cust_type") == "PER" ? _providerOccupation.businessLocationModelSelected != null ? _providerOccupation.businessLocationModelSelected.id : null : null,
                    "employeeTotal": _preferences.getString("cust_type") == "PER" ? double.parse(_providerOccupation.controllerEmployeeTotal.text == '' ? '0' : _providerOccupation.controllerEmployeeTotal.text.replaceAll(",", "")) : 0,
                    "bussinessLengthInMonth": _preferences.getString("cust_type") == "PER" ? double.parse(_providerOccupation.controllerEstablishedDate.text == '' ? '0' : _providerOccupation.controllerEstablishedDate.text.replaceAll(",", "").toString()) : 0,
                    "totalBussinessLengthInMonth": _preferences.getString("cust_type") == "PER" ? _providerOccupation.controllerTotalEstablishedDate.text == '' ? 0 : int.parse(_providerOccupation.controllerTotalEstablishedDate.text) : 0
                  },
                  "creationalSpecification": {
                    "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                    "createdBy": _preferences.getString("username"),
                    "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                    "modifiedBy": _preferences.getString("username"),
                  },
                  "status": "ACTIVE"
                }
              ],
              "customerIndividualAdditionalSpecification": null,
              "customerFamilies": _familyInfoID,
              "creationalSpecification": {
                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                "createdBy": _preferences.getString("username"),
                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                "modifiedBy": _preferences.getString("username"),
              },
              "status": "ACTIVE"
            }
          ]
              :
          [],
          "customerCorporateDTO": _preferences.getString("cust_type") == "PER" ? []:[
            {
              "customerCorporateID": _preferences.getString("last_known_state") != "IDE" ? _providerRincian.customerCorporateID : "NEW",
              "businessSpecification": {
                "economySector": _providerRincian.sectorEconomicModelSelected != null ? _providerRincian.sectorEconomicModelSelected.KODE : null,//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.sectorEconomicModelSelected.KODE,
                "businessField": _providerRincian.businessFieldModelSelected != null ? _providerRincian.businessFieldModelSelected.KODE : null ,//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.businessFieldModelSelected.KODE,
                "locationStatus": _providerRincian.locationStatusSelected != null ?  _providerRincian.locationStatusSelected.id : null,//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.locationStatusSelected.id,
                "businessLocation": _providerRincian.businessLocationSelected != null ? _providerRincian.businessLocationSelected.id : null,//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.businessLocationSelected.id,
                "employeeTotal": _providerRincian.controllerTotalEmployees.text != "" ? int.parse(_providerRincian.controllerTotalEmployees.text) : null,//_preferences.getString("cust_type") == "PER" ? 0 : double.parse(_providerRincian.controllerTotalEmployees.text.replaceAll(",", "")),
                "bussinessLengthInMonth": _providerRincian.controllerLamaUsahaBerjalan.text != "" ? int.parse(_providerRincian.controllerLamaUsahaBerjalan.text) : null,//_preferences.getString("cust_type") == "PER" ? 0 : double.parse(_providerRincian.controllerLamaUsahaBerjalan.text.replaceAll(",", "")),
                "totalBussinessLengthInMonth": _providerRincian.controllerTotalLamaUsahaBerjalan.text != "" ? int.parse(_providerRincian.controllerTotalLamaUsahaBerjalan.text) : null,//_preferences.getString("cust_type") == "PER" ? 0 : double.parse(_providerRincian.controllerTotalLamaUsahaBerjalan.text.replaceAll(",", "")),
              },
              "businessPermitSpecification": {
                "institutionType": _providerRincian.typeInstitutionSelected != null ?  _providerRincian.typeInstitutionSelected.PARA_ID : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.typeInstitutionSelected.PARA_ID,
                "profile": _providerRincian.profilSelected != null ? _providerRincian.profilSelected.id : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.profilSelected.id,
                "fullName": _providerRincian.controllerInstitutionName.text != "" ? _providerRincian.controllerInstitutionName.text : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.controllerInstitutionName.text,
                "deedOfIncorporationNumber": null,
                "deedEndDate": null,
                "siupNumber": null,
                "siupStartDate": null,
                "tdpNumber": null,
                "tdpStartDate": null,
                "establishmentDate": formatDateValidateAndSubmit(_providerRincian.initialDateForDateEstablishment),//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.controllerDateEstablishment.text,
                "isCompany": false,
                "authorizedCapital": 0,
                "paidCapital": 0
              },
              "managementPIC": {
                "picIdentity": {
                  "identityType": _providerManajemenPIC.typeIdentitySelected != null ? _providerManajemenPIC.typeIdentitySelected.id : "",
                  "identityNumber":  _providerManajemenPIC.controllerIdentityNumber.text != "" ? _providerManajemenPIC.controllerIdentityNumber.text : "",
                  "identityName": _providerManajemenPIC.controllerFullNameIdentity.text != "" ? _providerManajemenPIC.controllerFullNameIdentity.text : "",
                  "fullName": _providerManajemenPIC.controllerFullName.text != "" ? _providerManajemenPIC.controllerFullName.text : "",
                  "alias": null,
                  "title": null,
                  "dateOfBirth": _providerManajemenPIC.controllerBirthOfDate.text != "" ? formatDateValidateAndSubmit(_providerManajemenPIC.initialDateForBirthOfDate) : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.controllerBirthOfDate.text,
                  "placeOfBirth": _providerManajemenPIC.controllerPlaceOfBirthIdentity.text != "" ? _providerManajemenPIC.controllerPlaceOfBirthIdentity.text : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.controllerPlaceOfBirthIdentity.text,
                  "placeOfBirthKabKota": _providerManajemenPIC.birthPlaceSelected != null ? _providerManajemenPIC.birthPlaceSelected.KABKOT_ID : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.birthPlaceSelected.KABKOT_ID,
                  "gender": null,
                  "identityActiveStart": null,
                  "identityActiveEnd": null,
                  "isLifetime": null,
                  "religion": null,
                  "occupationID": null,
                  "positionID": "02",
                  "maritalStatusID": "02"
                },
                "picContact": {
                  "telephoneArea1": null,
                  "telephone1": null,
                  "telephoneArea2": null,
                  "telephone2": null,
                  "faxArea": null,
                  "fax": null,
                  "handphone": _providerManajemenPIC.controllerHandphoneNumber.text != "" ? "08${_providerManajemenPIC.controllerHandphoneNumber.text}" : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.controllerHandphoneNumber.text,
                  "email": _providerManajemenPIC.controllerEmail.text != "" ? _providerManajemenPIC.controllerEmail.text : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.controllerEmail.text,
                  "noWa": null,
                  "noWa2": null,
                  "noWa3": null
                },
                "picAddresses": _detailAddressPIC,
                "shareholding": 0
              },
              "shareholdersCorporates": [],
              "shareholdersIndividuals": [
                // {
                //   "shareholdersIndividualID": "NEW",
                //   "shareholding": _providerPemegangSaham.controllerPercentShare.text != "" ? double.parse(_providerPemegangSaham.controllerPercentShare.text.replaceAll(",", "")) : 0,
                //   "dedupScore": 0,
                //   "relationshipStatus": _providerPemegangSaham.relationshipStatusSelected != null ? _providerPemegangSaham.relationshipStatusSelected.PARA_FAMILY_TYPE_ID : "",
                //   "shareholdersIndividualIdentity": {
                //     "identityType": _providerPemegangSaham.typeIdentitySelected != null ? _providerPemegangSaham.typeIdentitySelected.id : "",
                //     "identityNumber": _providerPemegangSaham.controllerIdentityNumber.text != "" ?_providerPemegangSaham.controllerIdentityNumber.text : "",
                //     "identityName": _providerPemegangSaham.controllerFullNameIdentity.text != "" ? _providerPemegangSaham.controllerFullNameIdentity.text : "",
                //     "fullName": _providerPemegangSaham.controllerFullName.text != "" ? _providerPemegangSaham.controllerFullName.text : "",
                //     "alias": null,
                //     "title": null,
                //     "dateOfBirth": _providerPemegangSaham.controllerBirthOfDate.text != "" ? formatDateValidateAndSubmit(_providerPemegangSaham.initialDateForBirthOfDate) : "",
                //     "placeOfBirth": _providerPemegangSaham.controllerPlaceOfBirthIdentity.text != "" ? _providerPemegangSaham.controllerPlaceOfBirthIdentity.text : "",
                //     "placeOfBirthKabKota": _providerPemegangSaham.birthPlaceSelected != null ? _providerPemegangSaham.birthPlaceSelected.KABKOT_ID : "",
                //     "gender": "${_providerPemegangSaham.radioValueGender}",
                //     "identityActiveStart": null,
                //     "identityActiveEnd": null,
                //     "religion": null,
                //     "occupationID": "0",
                //     "positionID": null,
                //     "maritalStatusID": ""
                //   },
                //   "shareholderIndividualContact": {
                //     "telephoneArea1": null,
                //     "telephone1": null,
                //     "telephoneArea2": null,
                //     "telephone2": null,
                //     "faxArea": null,
                //     "fax": null,
                //     "handphone": null,
                //     "email": null
                //   },
                //   "shareholderIndividualAddresses": _detailAddressShareHolderIndividu,
                //   "creationalSpecification": {
                //     "createdAt":  formatDateValidateAndSubmit(DateTime.now()),
                //     "createdBy": _preferences.getString("username"),
                //     "modifiedAt":  formatDateValidateAndSubmit(DateTime.now()),
                //     "modifiedBy": _preferences.getString("username"),
                //   },
                //   "status": "ACTIVE"
                // }
              ],
              "dedupScore": 0.0,
              "status": "ACTIVE",
              "creationalSpecification": {
                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                "createdBy": _preferences.getString("username"),
                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                "modifiedBy": _preferences.getString("username"),
              },
            }
          ],
          "customerAdditionalInformation": null,
          "customerProcessingCompletion": {
            "lastKnownCustomerState": "CUSTOMER_GUARANTOR",
            "nextKnownCustomerState": "CUSTOMER_GUARANTOR"
          },
          "creationalSpecification": {
            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
            "createdBy": _preferences.getString("username"),
            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
            "modifiedBy": _preferences.getString("username"),
          },
          "status": "ACTIVE",
          "obligorID": _providerInfoNasabah.obligorID != "NEW" && _providerInfoNasabah.obligorID != "null" && _providerInfoNasabah.obligorID != null ? _providerInfoNasabah.obligorID : _preferences.getString("oid").toString(), // _preferences.getString("status_aoro") == "1" || _preferences.getString("status_aoro") == "2" ? _providerInfoNasabah.obligorID != "NEW" ? _providerInfoNasabah.obligorID : _preferences.getString("oid").toString() : "NEW",// _preferences.getString("last_known_state") != "IDE" ? _providerInfoNasabah.obligorID != "NEW" ? _providerInfoNasabah.obligorID : _preferences.getString("oid").toString() : "NEW",
          "exposureInformations": null,
          "customerIncomes": _customerIncomes
        },
        "orderDTO": {
          "orderID": _preferences.getString("last_known_state") != "IDE" ? _providerApp.applNo : orderId,
          "customerID": _preferences.getString("last_known_state") != "IDE" ? _providerInfoNasabah.customerID : "NEW",
          "orderSpecification": {
            "aosApprovalStatus": _preferences.getString("last_known_state") == "AOS" ? _providerSurvey.radioValueApproved : "",
            "isMayor": false, // _preferences.getString("last_known_state") == "AOS" && _providerSurvey.radioValueApproved == "0" ? true : | _preferences.getString("is_mayor") == "0",
            "hasFiducia": false,
            "applicationSource": "003",
            "orderDate": "${_providerApp.controllerOrderDate.text} 00:00:00",//di edit karena tgl orderDate lebih besar dr surveyAppointmentDate <formatDateValidateAndSubmit(_providerApp.initialDateOrder)>
            "applicationDate": "${_providerApp.controllerOrderDate.text} 00:00:00",// di edit karena tgl applicationDate lebih besar dr surveyAppointmentDate <formatDateValidateAndSubmit(_providerApp.initialDateOrder)>
            "surveyAppointmentDate": "${_providerApp.controllerSurveyAppointmentDate.text}" " ${_providerApp.controllerSurveyAppointmentTime.text}:00", //formatDateValidateAndSubmit(_providerApp.initialSurveyDate)
            "orderType": _preferences.getString("status_aoro") != null ? _preferences.getString("status_aoro") : "0",//_preferences.getString("status_aoro"),
            "isOpenAccount": true,
            "accountFormNumber": null,
            "sentraCID": _preferences.getString("SentraD"),
            "unitCID": _preferences.getString("UnitD"),
            "initialRecommendation": _providerApp.initialRecomendation,
            "finalRecommendation": _providerApp.finalRecomendation,
            "brmsScoring": _providerApp.brmsScoring,
            "isInPlaceApproval": false,
            "isPkSigned": _providerApp.isSignedPK,
            "maxApprovalLevel": null,
            "jenisKonsep": _providerFoto.jenisKonsepSelected != null ? _providerFoto.jenisKonsepSelected.id : "",
            "jumlahObjek": _providerApp.controllerTotalObject.text == "" ? null : int.parse(_providerApp.controllerTotalObject.text),
            "jenisProporsionalAsuransi": _providerApp.proportionalTypeOfInsuranceModelSelected != null ? _providerApp.proportionalTypeOfInsuranceModelSelected.kode : "",
            "unitKe": _providerApp.numberOfUnitSelected == "" ? null : int.parse(_providerApp.numberOfUnitSelected),
            "isWillingToAcceptInfo": false,
            "applicationContractNumber": null,
            "dealerNote": Provider.of<MarketingNotesChangeNotifier>(context,listen: false).controllerMarketingNotes.text,
            "userDealer": "",
            "orderDealer": "",
            "flagSourceMS2": "103", // diganti dari 006 ke 103 sesuai arahan mas Oji (per tanggal 06.07.2021)
            "orderSupportingDocuments": _orderSupportingDocuments,
            "collateralTypeID": _providerKolateral.collateralTypeModel.id,
            "applicationDocVerStatus": null
          },
          "orderProducts": [
            {
              "orderProductID": _preferences.getString("last_known_state") != "IDE" ? _providerUnitObject.applObjtID : orderProductId, // jika IDE ngambil dari nomer unit, selain IDE ngambil dari APPL_NO get data from DB API
              "orderProductSpecification": {
                "financingTypeID": _preferences.getString("cust_type") == "PER" ? _providerFoto.typeOfFinancingModelSelected.financingTypeId : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId,
                "ojkBusinessTypeID": _preferences.getString("cust_type") == "PER" ? _providerFoto.kegiatanUsahaSelected.id.toString() : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId.toString(),
                "ojkBussinessDetailID": _preferences.getString("cust_type") == "PER" ? _providerFoto.jenisKegiatanUsahaSelected.id.toString() : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId,
                "orderUnitSpecification": {
                  "objectGroupID": _providerUnitObject.groupObjectSelected != null ? _providerUnitObject.groupObjectSelected.KODE : "",
                  "objectID": _providerUnitObject.objectSelected != null ? _providerUnitObject.objectSelected.id : "",
                  "productTypeID": _providerUnitObject.productTypeSelected != null ? _providerUnitObject.productTypeSelected.id : "",
                  "objectBrandID": _providerUnitObject.brandObjectSelected != null ? _providerUnitObject.brandObjectSelected.id : "",
                  "objectTypeID": _providerUnitObject.objectTypeSelected != null ? _providerUnitObject.objectTypeSelected.id : "",
                  "objectModelID": _providerUnitObject.modelObjectSelected != null ? _providerUnitObject.modelObjectSelected.id : "",
                  "objectUsageID": _providerUnitObject.objectUsageModel != null ? _providerUnitObject.objectUsageModel.id : "", // field pemakaian objek (unit)
                  "objectPurposeID": _providerUnitObject.objectPurposeSelected != null ? _providerUnitObject.objectPurposeSelected.id : "", // field tujuan objek (unit)
                  "modelDetail": _providerUnitObject.controllerDetailModel.text == "" ? null : _providerUnitObject.controllerDetailModel.text
                },
                "salesGroupID": _providerUnitObject.groupSalesSelected != null ? _providerUnitObject.groupSalesSelected.kode : "",
                "orderSourceID": _providerUnitObject.sourceOrderSelected != null ? _providerUnitObject.sourceOrderSelected.kode : "",
                "orderSourceName": _providerUnitObject.sourceOrderNameSelected != null ? _providerUnitObject.sourceOrderNameSelected.kode : "",
                "orderProductDealerSpecification": {
                  "isThirdParty": false,
                  "thirdPartyTypeID": _providerUnitObject.thirdPartyTypeSelected != null ? _providerUnitObject.thirdPartyTypeSelected.kode : "",
                  "thirdPartyID": _providerUnitObject.thirdPartySelected != null ? _providerUnitObject.thirdPartySelected.kode : "",
                  "thirdPartyActivity": _providerUnitObject.activitiesModel != null ? _providerUnitObject.activitiesModel.kode : null,
                  "sentradID": _preferences.getString("SentraD"), //_providerUnitObject.thirdPartySelected != null ? _providerUnitObject.thirdPartySelected.sentraId : "",
                  "unitdID": _preferences.getString("UnitD"), //_providerUnitObject.thirdPartySelected != null ? _providerUnitObject.thirdPartySelected.unitId : "",
                  "dealerMatrix": _providerUnitObject.matriksDealerSelected != null ? _providerUnitObject.matriksDealerSelected.kode : ""
                },
                "programID": _providerUnitObject.programSelected != null ? _providerUnitObject.programSelected.kode : "",
                "rehabType": _providerUnitObject.rehabTypeSelected != null ? _providerUnitObject.rehabTypeSelected.id : "",
                "referenceNumber": _providerUnitObject.referenceNumberModel != null ? _providerUnitObject.referenceNumberModel.noPK : null,
                "applicationUnitContractNumber": null,
                "orderProductSaleses": _orderProductSaleses,
                "orderKaroseris": _orderKaroseris,
                "orderProductInsurances": _orderProductInsurances,
                "orderWmps": _orderWmps,
                "groupID": _providerUnitObject.grupIdSelected != null ? _providerUnitObject.grupIdSelected.kode : null
              },
              "orderCreditStructure": {
                "installmentTypeID": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.id : "", // sebelumnya ngambil by kode
                "tenor": _providerCreditStructure.periodOfTimeSelected != null ? int.parse(_providerCreditStructure.periodOfTimeSelected) : "",
                "paymentMethodID": _providerCreditStructure.paymentMethodSelected != null ? _providerCreditStructure.paymentMethodSelected.id : "", // sebelumnya ngambil by kode
                "effectiveRate": _providerCreditStructure.controllerInterestRateEffective.text != "" ? double.parse(_providerCreditStructure.controllerInterestRateEffective.text) : "",
                "flatRate": _providerCreditStructure.controllerInterestRateFlat.text != "" ? double.parse(_providerCreditStructure.controllerInterestRateFlat.text) : "",//0.48501
                "objectPrice": _providerCreditStructure.controllerObjectPrice.text != "" ? double.parse(_providerCreditStructure.controllerObjectPrice.text.replaceAll(",", "")) : 0,
                "karoseriTotalPrice": _providerCreditStructure.controllerKaroseriTotalPrice.text != "" ? double.parse(_providerCreditStructure.controllerKaroseriTotalPrice.text.replaceAll(",", "")) : 0,
                "totalPrice": _providerCreditStructure.controllerTotalPrice.text != "" ? double.parse(_providerCreditStructure.controllerTotalPrice.text.replaceAll(",", "")) : 0,
                "nettDownPayment": _providerCreditStructure.controllerNetDP.text != "" ? double.parse(_providerCreditStructure.controllerNetDP.text.replaceAll(",", "")) : 0,
                "declineNInstallment": _providerCreditStructure.installmentDeclineN,
                "paymentOfYear": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.id == "04" ? int.parse(_providerCreditStructure.controllerPaymentPerYear.text) : 1 : 1,
                "branchDownPayment": _providerCreditStructure.controllerBranchDP.text != "" ? double.parse(_providerCreditStructure.controllerBranchDP.text.replaceAll(",", "")) : 0,
                "grossDownPayment": _providerCreditStructure.controllerGrossDP.text != "" ? double.parse(_providerCreditStructure.controllerGrossDP.text.replaceAll(",", "")) : 0,
                "totalLoan": _providerCreditStructure.controllerTotalLoan.text != "" ? double.parse(_providerCreditStructure.controllerTotalLoan.text.replaceAll(",", "")) : 0,
                "installmentAmount": _providerCreditStructure.controllerInstallment.text != "" ? double.parse(_providerCreditStructure.controllerInstallment.text.replaceAll(",", "")) : 0,
                "ltvRatio": _providerCreditStructure.controllerLTV.text != "" ? double.parse(_providerCreditStructure.controllerLTV.text.replaceAll(",", "")) : 0.0,
                "pencairan": 0,
                "interestAmount": _providerCreditStructure.interestAmount,
                "gpType": null,
                "newTenor": 0,
                "totalStepping": 0,
                "uangMukaKaroseri": 0, // diset 0
                "uangMukaChasisNet": _providerCreditStructure.controllerNetDP.text != "" ? double.parse(_providerCreditStructure.controllerNetDP.text.replaceAll(",", "")) : 0, // set sama kaya nettDownPayment
                "uangMukaChasisGross": _providerCreditStructure.controllerGrossDP.text != "" ? double.parse(_providerCreditStructure.controllerGrossDP.text.replaceAll(",", "")) : 0, // set sama kaya grossDownPayment
                "installmentBalloonPayment": {
                  "balloonType": _providerCreditStructureType.radioValueBalloonPHOrBalloonInstallment,
                  "lastInstallmentPercentage": _providerCreditStructureType.controllerInstallmentPercentage.text != "" ? double.parse(_providerCreditStructureType.controllerInstallmentPercentage.text.replaceAll(",", "")) : 0,
                  "lastInstallmentValue": _providerCreditStructureType.controllerInstallmentValue.text != "" ? double.parse(_providerCreditStructureType.controllerInstallmentValue.text.replaceAll(",", "")) : 0
                },
                "installmentSchedule": {
                  "installmentType": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.name : null, // OLD: ambil dari ID
                  "paymentMethodType": _providerCreditStructure.paymentMethodSelected != null ? _providerCreditStructure.paymentMethodSelected.name : null,  // OLD: ambil dari ID
                  "installment": _providerCreditStructure.controllerInstallment.text != "" ? double.parse(_providerCreditStructure.controllerInstallment.text.replaceAll(",", "")) : 0,
                  "installmentRound": 0,
                  "installmentMethod": 0,
                  "lastKnownOutstanding": 0,
                  "minInterest": 0,
                  "maxInterest": 0,
                  "futureValue": 0,
                  "isInstallment": false,
                  "roundingValue": null,
                  "balloonInstallment": 0,
                  "gracePeriode": 0,
                  "gracePeriodes": [],
                  "seasonal": 0,
                  "steppings": [],
                  "installmentScheduleDetails": []
                },
                "dsr": _providerCreditIncome.controllerDebtComparison.text != "" ? double.parse(_providerCreditIncome.controllerDebtComparison.text.replaceAll(",", "")) : 0,
                "dir": _providerCreditIncome.controllerIncomeComparison.text != "" ? double.parse(_providerCreditIncome.controllerIncomeComparison.text.replaceAll(",", "")) : 0,
                "dsc": _providerCreditIncome.controllerDSC.text != "" ? double.parse(_providerCreditIncome.controllerDSC.text.replaceAll(",", "")) : 0.0,
                "irr": _providerCreditIncome.controllerIRR.text != "" ? double.parse(_providerCreditIncome.controllerIRR.text.replaceAll(",", "")) : 0.0,
                "installmentDetails": _installmentDetails,
                // [
                //   {
                //     "installmentID": null,
                //     "installmentNumber": 0,
                //     "percentage": 0,
                //     "amount": 0,
                //     "creationalSpecification": {
                //       "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                //       "createdBy": "10056030",
                //       "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                //       "modifiedBy": "10056030"
                //     },
                //     "status": "ACTIVE"
                //   }
                // ],
                "realDSR": 0 // di jadikan model dari get dsr
              },
              "orderFees": _orderFees,
              "orderSubsidies": _orderSubsidies,
              "orderProductAdditionalSpecification": {
                "virtualAccountID": null,
                "virtualAccountValue": 0,
                "cashingPurposeID": null,
                "cashingTypeID": null
              },
              "creationalSpecification": {
                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                "createdBy": _preferences.getString("username"),
                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                "modifiedBy": _preferences.getString("username"),
              },
              "status": "ACTIVE",
              "collateralAutomotives": _providerKolateral.collateralTypeModel.id == "001"
                  ?
              [
                {
                  "collateralID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.collateralID : "CAU001",// NEW | _providerKolateral.collateralTypeModel != null ? _providerKolateral.collateralTypeModel.id : "",
                  "isCollaSameUnit": _providerKolateral.radioValueIsCollateralSameWithUnitOto, // Jika Collateral = Unit dipilih YA, maka kirim TRUE. Begitu juga sebaliknya (0 = YA/true | 1 = TIDAK/false)
                  "isMultiUnitCollateral": _providerKolateral.radioValueForAllUnitOto, // ya = false = 0 | tidak = true = 1
                  "isCollaNameSameWithCustomerName": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto, // Jika Nama Jaminan = Pemohon dipilih YA, maka kirim TRUE. Begitu juga sebaliknya (0 = YA/true | 1 = TIDAK/false)
                  "identitySpecification": {
                    "identityType": _providerKolateral.identityTypeSelectedAuto != null ? _providerKolateral.identityTypeSelectedAuto.id : "",
                    "identityNumber": _providerKolateral.controllerIdentityNumberAuto.text != "" ? _providerKolateral.controllerIdentityNumberAuto.text : "",
                    "identityName": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0 ? _providerInfoNasabah.controllerNamaLengkapSesuaiIdentitas.text : _providerKolateral.controllerNameOnCollateralAuto.text,
                    "fullName": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0 ? _providerInfoNasabah.controllerNamaLengkap.text : _providerKolateral.controllerNameOnCollateralAuto.text,
                    "alias": null,
                    "title": null,
                    "dateOfBirth": _providerKolateral.controllerBirthDateAuto.text != "" ? _providerKolateral.controllerBirthDateAuto.text+" 00:00:00" : null,
                    "placeOfBirth": _providerKolateral.controllerBirthPlaceValidWithIdentityAuto.text != "" ? _providerKolateral.controllerBirthPlaceValidWithIdentityAuto.text : "",
                    "placeOfBirthKabKota": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0 ? _providerInfoNasabah.birthPlaceSelected.KABKOT_ID : _providerKolateral.birthPlaceAutoSelected != null ? _providerKolateral.birthPlaceAutoSelected.KABKOT_ID : null,
                    "gender": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0 ? _providerInfoNasabah.radioValueGender : null,
                    "identityActiveStart": null,
                    "identityActiveEnd": null,
                    "religion": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0 ? _providerInfoNasabah.religionSelected == null ? null : _providerInfoNasabah.religionSelected.id : null,
                    "occupationID": _providerFoto.occupationSelected != null ? _providerFoto.occupationSelected.KODE : "",
                    "positionID": null
                  },
                  "collateralAutomotiveSpecification": {
                    "orderUnitSpecification": {
                      "objectGroupID": _providerKolateral.groupObjectSelected != null ? _providerKolateral.groupObjectSelected.KODE : null,
                      "objectID": _providerKolateral.objectSelected != null ? _providerKolateral.objectSelected.id : null,
                      "productTypeID": _providerKolateral.productTypeSelected != null ? _providerKolateral.productTypeSelected.id : null,
                      "objectBrandID":  _providerKolateral.brandObjectSelected != null ? _providerKolateral.brandObjectSelected.id : null,
                      "objectTypeID": _providerKolateral.objectTypeSelected != null ? _providerKolateral.objectTypeSelected.id : null,
                      "objectModelID": _providerKolateral.modelObjectSelected != null ? _providerKolateral.modelObjectSelected.id : null,
                      "objectUsageID":  "", // tidak dipakai
                      "objectPurposeID": _providerKolateral.objectUsageSelected != null ? _providerKolateral.objectUsageSelected.id : null, // field tujuan penggunaan collateral
                    },
                    "mpAdiraUpld": _providerKolateral.controllerMPAdiraUpload.text != "" ? double.parse(_providerKolateral.controllerMPAdiraUpload.text.replaceAll(",", "")) : null,
                    "productionYear": _providerKolateral.yearProductionSelected != null ? int.parse(_providerKolateral.yearProductionSelected) : "",
                    "registrationYear": _providerKolateral.yearRegistrationSelected != null ? int.parse(_providerKolateral.yearRegistrationSelected) : "",
                    "isYellowPlate": _providerKolateral.radioValueYellowPlat, // per tanggal 10.05.2021 = dirubah ga dirubah jadi true false (OLD: == 0 ? false : true)
                    "isBuiltUp": _providerKolateral.radioValueBuiltUpNonATPM == 0 ? true : false,
                    "utjSpecification": {
                      "bpkbNumber": _providerKolateral.controllerBPKPNumber.text != "" ? _providerKolateral.controllerBPKPNumber.text : null,
                      "chassisNumber":  _providerKolateral.controllerFrameNumber.text != "" ? _providerKolateral.controllerFrameNumber.text : null,
                      "engineNumber": _providerKolateral.controllerMachineNumber.text != "" ? _providerKolateral.controllerMachineNumber.text : null,
                      "policeNumber": _providerKolateral.controllerPoliceNumber.text != "" ? _providerKolateral.controllerPoliceNumber.text : null,
                    },
                    "gradeUnit": _providerKolateral.controllerGradeUnit.text != "" ? _providerKolateral.controllerGradeUnit.text : null,
                    "utjFacility": _providerKolateral.controllerFasilitasUTJ.text != "" ? _providerKolateral.controllerFasilitasUTJ.text == "TUNAI" ? "1" : "0" : null,
                    "bidderName": _providerKolateral.controllerNamaBidder.text != "" ? _providerKolateral.controllerNamaBidder.text : null,
                    "showroomPrice": _providerKolateral.controllerHargaJualShowroom.text != "" ? double.parse(_providerKolateral.controllerHargaJualShowroom.text.replaceAll(",", "")).round() : "",
                    "additionalEquipment": _providerKolateral.controllerPerlengkapanTambahan.text != "" ? _providerKolateral.controllerPerlengkapanTambahan.text : "",
                    "mpAdira":  _providerKolateral.controllerMPAdira.text != "" ? double.parse(_providerKolateral.controllerMPAdira.text.replaceAll(",", "")) : null,
                    "physicalRecondition": _providerKolateral.controllerRekondisiFisik.text != "" ? double.parse(_providerKolateral.controllerRekondisiFisik.text.replaceAll(",", "")) : 0,
                    "isProper": _providerKolateral.radioValueWorthyOrUnworthy, // 0 = layak = false | 1 = tidak layak = true
                    "ltvRatio": 0.0,
                    "appraisalSpecification": {
                      "collateralDp": double.parse(_providerKolateral.controllerDPJaminan.text == '' ? '0' : _providerKolateral.controllerDPJaminan.text.replaceAll(",", "")),
                      "maximumPh": double.parse(_providerKolateral.controllerPHMaxAutomotive.text == '' ? '0' : _providerKolateral.controllerPHMaxAutomotive.text.replaceAll(",", "")),
                      "appraisalPrice": double.parse(_providerKolateral.controllerTaksasiPriceAutomotive.text == '' ? '0' : _providerKolateral.controllerTaksasiPriceAutomotive.text.replaceAll(",", "")),
                      "collateralUtilizationID": _providerKolateral.collateralUsageOtoSelected != null ? _providerKolateral.collateralUsageOtoSelected.id : "" // field tujuan penggunaan collateral
                    },
                    "collateralAutomotiveAdditionalSpecification": {
                      "capacity": 0,
                      "color": null,
                      "manufacturer": null,
                      "serialNumber": null,
                      "invoiceNumber": null,
                      "stnkActiveDate": null,
                      "bpkbAddress": null,
                      "bpkbIdentityTypeID": null
                    },
                  },
                  "status": "ACTIVE",
                  "creationalSpecification": {
                    "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                    "createdBy": _preferences.getString("username"),
                    "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                    "modifiedBy": _preferences.getString("username"),
                  }
                }
              ] : [],
              "collateralProperties": _providerKolateral.collateralTypeModel.id == "002"
                  ?
              [
                {
                  "isMultiUnitCollateral": true, // _providerKolateral.radioValueForAllUnitOto, // ya = false = 0 | tidak = true = 1
                  "isCollaNameSameWithCustomerName": _providerKolateral.radioValueIsCollateralSameWithApplicantProperty == 0,
                  "collateralID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.collateralID : "COLP001", // NEW
                  "identitySpecification": {
                    "identityType": _providerKolateral.identityModel != null ? _providerKolateral.identityModel.id : "",
                    "identityNumber": _providerKolateral.controllerIdentityNumber.text,
                    "identityName": null,
                    "fullName": _providerKolateral.controllerNameOnCollateral.text,
                    "alias": null,
                    "title": null,
                    "dateOfBirth": _providerKolateral.controllerBirthDateProp.text != "" ? _providerKolateral.controllerBirthDateProp.text : null,
                    "placeOfBirth": _providerKolateral.birthPlaceSelected.KABKOT_NAME,
                    "placeOfBirthKabKota": _providerKolateral.birthPlaceSelected.KABKOT_ID,
                    "gender": null,
                    "identityActiveStart": null,
                    "identityActiveEnd": null,
                    "isLifetime": null,
                    "religion": null,
                    "occupationID": null,
                    "positionID": null,
                    "maritalStatusID": null
                  },
                  "detailAddresses": [
                    {
                      "addressID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.addressID != "NEW" ? _providerKolateral.addressID : "NEW" : "NEW",
                      "foreignBusinessID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.foreignBusinessID != "NEW" ? _providerKolateral.foreignBusinessID : "CAU001" : "CAU001",
                      "addressSpecification": {
                        "koresponden": _providerKolateral.isKorespondensi.toString(),
                        "matrixAddr": _preferences.getString("cust_type") == "PER" ? "6" : "13",
                        "address": _providerKolateral.controllerAddress.text,
                        "rt": _providerKolateral.controllerRT.text,
                        "rw": _providerKolateral.controllerRW.text,
                        "provinsiID": _providerKolateral.kelurahanSelected.PROV_ID,
                        "kabkotID": _providerKolateral.kelurahanSelected.KABKOT_ID,
                        "kecamatanID": _providerKolateral.kelurahanSelected.KEC_ID,
                        "kelurahanID": _providerKolateral.kelurahanSelected.KEL_ID,
                        "zipcode": _providerKolateral.kelurahanSelected.ZIPCODE
                      },
                      "contactSpecification": {
                        "telephoneArea1": null,
                        "telephone1": null,
                        "telephoneArea2": null,
                        "telephone2": null,
                        "faxArea": null,
                        "fax": null,
                        "handphone": null,
                        "email": null,
                        "noWa": null,
                        "noWa2": null,
                        "noWa3": null
                      },
                      "addressType": _providerKolateral.addressTypeSelected.KODE,
                      "creationalSpecification": {
                        "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                        "createdBy": _preferences.getString("username"),
                        "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                        "modifiedBy": _preferences.getString("username"),
                      },
                      "status": "ACTIVE"
                    }
                  ],
                  "collateralPropertySpecification": {
                    "certificateNumber": _providerKolateral.controllerCertificateNumber.text,
                    "certificateTypeID": _providerKolateral.certificateTypeSelected.id,
                    "propertyTypeID": _providerKolateral.propertyTypeSelected.id,
                    "buildingArea": double.parse(_providerKolateral.controllerBuildingArea.text.replaceAll(",", "")),
                    "landArea": double.parse(_providerKolateral.controllerSurfaceArea.text.replaceAll(",", "")),
                    "appraisalSpecification": {
                      "collateralDp": double.parse(_providerKolateral.controllerDPJaminan.text.replaceAll(",", "")).toInt(),
                      "maximumPh": double.parse(_providerKolateral.controllerPHMax.text == "" ? '0' : _providerKolateral.controllerPHMax.text.replaceAll(",", "")),
                      "appraisalPrice": double.parse(_providerKolateral.controllerTaksasiPrice.text.replaceAll(",", "")).toInt(),
                      "collateralUtilizationID": _providerKolateral.collateralUsagePropertySelected != null ? _providerKolateral.collateralUsagePropertySelected.id : "" //"" field tujuan penggunaan collateral
                    },
                    "positiveFasumDistance": double.parse(_providerKolateral.controllerJarakFasumPositif.text.replaceAll(",","")).toInt(),
                    "negativeFasumDistance": double.parse(_providerKolateral.controllerJarakFasumNegatif.text.replaceAll(",","")).toInt(),
                    "landPrice": double.parse(_providerKolateral.controllerHargaTanah.text.replaceAll(",","")).toInt(),
                    "njopPrice": double.parse(_providerKolateral.controllerHargaNJOP.text.replaceAll(",","")).toInt(),
                    "buildingPrice": double.parse(_providerKolateral.controllerHargaBangunan.text.replaceAll(",","")).toInt(),
                    "roofTypeID": _providerKolateral.typeOfRoofSelected != null ? _providerKolateral.typeOfRoofSelected.id : null,
                    "wallTypeID": _providerKolateral.wallTypeSelected != null ? _providerKolateral.wallTypeSelected.id : null,
                    "floorTypeID": _providerKolateral.floorTypeSelected != null ? _providerKolateral.floorTypeSelected.id : null,
                    "foundationTypeID": _providerKolateral.foundationTypeSelected != null ? _providerKolateral.foundationTypeSelected.id : null,
                    "roadTypeID": _providerKolateral.streetTypeSelected != null ? _providerKolateral.streetTypeSelected.id : null,
                    "isCarPassable": _providerKolateral.radioValueAccessCar == 0,
                    "totalOfHouseInRadius": _providerKolateral.controllerJumlahRumahDalamRadius.text != "" ? int.parse(_providerKolateral.controllerJumlahRumahDalamRadius.text) : 0,
                    "sifatJaminan": _providerKolateral.controllerSifatJaminan.text,
                    "buktiKepemilikanTanah": _providerKolateral.controllerBuktiKepemilikan.text,
                    "tgglTerbitSertifikat": _providerKolateral.controllerCertificateReleaseDate.text != "" ? formatDateValidateAndSubmit(_providerKolateral.initialDateCertificateRelease) : "",
                    "tahunTerbitSertifikat": _providerKolateral.controllerCertificateReleaseYear.text,
                    "namaPemegangHak": _providerKolateral.controllerNamaPemegangHak.text,
                    "noSuratUkurGmbrSituasi": _providerKolateral.controllerNoSuratUkur.text,
                    "tgglSuratUkurGmbrSituasi": _providerKolateral.controllerDateOfMeasuringLetter.text != "" ? formatDateValidateAndSubmit(_providerKolateral.initialDateOfMeasuringLetter) : "",
                    "sertifikatSuratUkurDikeluarkanOleh": _providerKolateral.controllerCertificateOfMeasuringLetter.text,
                    "masaBerlakuHak": _providerKolateral.controllerMasaHakBerlaku.text,
                    "noImb": _providerKolateral.controllerNoIMB.text,
                    "tgglImb": _providerKolateral.controllerDateOfIMB.text != "" ? formatDateValidateAndSubmit(_providerKolateral.initialDateOfIMB) : "",
                    "luasBangunanImb": double.parse(_providerKolateral.controllerLuasBangunanIMB.text.replaceAll(",", "")),
                    "ltvRatio": 0,
                    "letakTanah": null,
                    "propertyGroupObjtID": null,
                    "propertyObjtID": null
                  },
                  "status": "INACTIVE",
                  "creationalSpecification": {
                    "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                    "createdBy": _preferences.getString("username"),
                    "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                    "modifiedBy": _preferences.getString("username"),
                  }
                }
              ]
                  :
              [],
              "orderProductProcessingCompletion": {
                "lastKnownOrderProductState": "APPLICATION_DOCUMENT",
                "nextKnownOrderProductState": "APPLICATION_DOCUMENT"
              },
              "isWithoutColla": _providerKolateral.collateralTypeModel.id == "003" ? 1 : 0,// _providerUnitObject.groupObjectSelected.KODE == "003" ? 1 : 0,
              "isSaveKaroseri": false
            }
          ],
          "orderProcessingCompletion": {
            "calculatedAt": 0,
            "lastKnownOrderState": null,
            "lastKnownOrderStatePosition": null,
            "lastKnownOrderStatus": null,
            "lastKnownOrderHandledBy": null
          },
          "creationalSpecification": {
            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
            "createdBy": _preferences.getString("username"),
            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
            "modifiedBy": _preferences.getString("username"),
          },
          "status": "ACTIVE"
        },
        "surveyDTO": _preferences.getString("last_known_state") != "IDE"
            ?
        {
          "surveyID": _dataResultSurvey.isNotEmpty ? _dataResultSurvey[0]['surveyID'] != null ? _dataResultSurvey[0]['surveyID'] : "NEW" : "NEW", // dibuat seperti ini karena ada kemungkinan IDE di buat di ACCTION, sehingga survey id sudah tergenerate saat melakukan SRE di MS2
          "orderID": _preferences.getString("last_known_state") != "IDE" ? _providerApp.applNo : orderId,
          "surveyType": "002",
          "employeeID": _preferences.getString("username"),
          "phoneNumber": null,
          "janjiSurveyDate": formatDateValidateAndSubmit(_providerApp.initialSurveyDate),
          "reason": "",
          "flagBRMS": "BRMS",
          "surveyStatus": "01",
          "status": "ACTIVE",
          "surveyProcess": "02",
          "surveyResultSpecification": {
            "surveyResultType": (_preferences.getString("last_known_state") == "AOS" || _preferences.getString("last_known_state") == "CONA") && _providerSurvey.radioValueApproved == "0"
                ? _providerSurvey.resultSurveySelected != null
                ? _providerSurvey.resultSurveySelected.KODE : "000"
                :  _providerSurvey.resultSurveySelected != null
                ? _providerSurvey.resultSurveySelected.KODE : "",
            "recomendationType": _providerSurvey.recommendationSurveySelected != null ? _providerSurvey.recommendationSurveySelected.KODE : null,
            "notes": _providerSurvey.controllerNote.text != "" ? _providerSurvey.controllerNote.text : "",
            "surveyResultDate": _providerSurvey.controllerResultSurveyDate.text.isNotEmpty ? "${_providerSurvey.controllerResultSurveyDate.text} ${_providerSurvey.controllerResultSurveyTime.text}:00" : formatDateValidateAndSubmit(DateTime.now()), //formatDateValidateAndSubmit(_providerSurvey.initialDateResultSurvey),
            "distanceWithSentra": _providerSurvey.controllerDistanceLocationWithSentraUnit.text != "" ? double.parse(_providerSurvey.controllerDistanceLocationWithSentraUnit.text.replaceAll(",", "")).toInt() : 0,
            "distanceWithDealer": _providerSurvey.controllerDistanceLocationWithDealerMerchantAXI.text != "" ? double.parse(_providerSurvey.controllerDistanceLocationWithDealerMerchantAXI.text.replaceAll(",", "")).toInt() : 0,
            "distanceObjWithSentra": _providerSurvey.controllerDistanceLocationWithUsageObjectWithSentraUnit.text != "" ? double.parse(_providerSurvey.controllerDistanceLocationWithUsageObjectWithSentraUnit.text.replaceAll(",", "")).toInt() : 0,
            "surveyResultAssets": _surveyResultAssets,
            "surveyResultDetails": _surveyResultDetails,
            "surveyResultTelesurveys": []
          },
          "creationalSpecification": {
            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
            "createdBy": _preferences.getString("username"),
            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
            "modifiedBy": _preferences.getString("username")
          },
          "jobSurveyor": ""
        }
        :
        null,
        "userCredentialMS2": {
          "nik": _preferences.getString("username"),
          "branchCode": _preferences.getString("branchid"),
          "fullName": _preferences.getString("fullname"),
          "role": _preferences.getString("job_name")
        },
      },
      "ms2TaskID": _preferences.getString("order_no")
    });

    // String urlAcction = await storage.read(key: "urlAcction");
    // print("${urlAcction}adira-acction-071043/acction/service/mss/validateData");

    // await http.post(
    //     "http://192.168.57.108:82/ad1ms2_backend/public/api/testSubmit",
    //     body: _body,
    //     headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    // );

    // await http.post(
    //     "http://192.168.57.122/orca_api/public/login",
    //     body: _body,
    //     headers: {"Content-Type":"application/json"}
    // );

    loadData = true;
    messageProcess = "Proses validate data...";
    String _validateIDE = await storage.read(key: "ValidateIDE");
    print("cek url validate ${"${BaseUrl.urlGeneral}$_validateIDE"}");
    DateTime _timeStartValidate = DateTime.now();
    insertLog(context,_timeStartValidate,DateTime.now(),_body,"Start Validate","validate ${_preferences.getString("last_known_state")}");
    try{
      final _response = await _http.post(
            // "http://192.168.57.122/orca_api/public/login",
          "${BaseUrl.urlGeneral}$_validateIDE",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 300));
      debugPrint("halooo = ${_response.body}");
      final _data = jsonDecode(_response.body);
      if(_response.statusCode == 201){
        if(_data['status'] == "1"){
          loadData = false;
          _showSnackBarSuccess("Validate ${_data['message']}");
          insertLog(context,_timeStartValidate,DateTime.now(),_body,jsonEncode(_data),"Validate ${_preferences.getString("last_known_state")}");
          if(_preferences.getString("last_known_state") != "DKR") {
            if(_preferences.getString("last_known_state") == "IDE" && _preferences.getString("jenis_penawaran") == "002") {
              getCheckLimit(_body, context, orderProductId);
            } else {
              submitData(_body, context);
            }
          } else {
            submitDakor(context, orderId, orderProductId);
          }
        }
        else{
          loadData = false;
          messageProcess = "";
          insertLog(context,_timeStartValidate,DateTime.now(),_body,jsonEncode(_data),"validate ${_preferences.getString("last_known_state")}");
          _showSnackBar("${_data['message']}");
        }
      }
      else{
        loadData = false;
        messageProcess = "";
        insertLog(context,_timeStartValidate,DateTime.now(),_body,jsonEncode(_data),"validate ${_preferences.getString("last_known_state")}");
        print(_response.statusCode);
        _showSnackBar("Error ${_response.statusCode}");
      }
    }
    on TimeoutException catch(_){
      loadData = false;
      messageProcess = "";
      _showSnackBar("Timeout connection APK");
    }
    catch(e){
      print("masuk catch");
      loadData = false;
      messageProcess = "";
      insertLog(context,_timeStartValidate,DateTime.now(),_body,e.toString(),"validate ${_preferences.getString("last_known_state")}");
      // print(_response.statusCode);
      _showSnackBar("Error validate ${e.toString()}");
    }
  }

  Future<void> getCheckLimit(String body, BuildContext context, String orderProductId) async {
    try {
      loadData = true;
      messageProcess = "Proses check limit...";
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);

      String _contractReference = await getMplActivationWithLmeId(context);
      SharedPreferences _preferences = await SharedPreferences.getInstance();
      DateTime _timeStartValidate = DateTime.now();
      var _date = dateFormat3.format(DateTime.now());
      List _dataCL = await _dbHelper.selectMS2LME();
      var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
      var _providerInfoNasabah = Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false);
      var _providerUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
      var _providerCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false);

      var _body = jsonEncode({
        "oid" : _providerInfoNasabah.obligorID != "NEW" ? _providerInfoNasabah.obligorID : _preferences.getString("oid").toString(),
        "lme_id" : _dataCL[0]['lme_id'],
        "source_reff_id" : "MS2${_preferences.getString("username")}",
        "source_channel_id" : "MS2",
        "disburse_type" : "2",
        "product_id" : _providerUnitObject.objectSelected.id,
        "contract_no" : _contractReference.toString(),
        "installment_amount" : double.parse(_providerCreditStructure.controllerInstallment.text.replaceAll(",", "")),
        "ph_amount" : double.parse(_providerCreditStructure.controllerTotalLoan.text.replaceAll(",", "")),
        "voucher_code" : _dataCL[0]['voucher_code'],
        "req_date" : _date.toString(),
        "tenor" : _providerCreditStructure.periodOfTimeSelected,
        "cust_id" : _providerInfoNasabah.controllerNoIdentitas.text,
        "branch_code" : _preferences.getString("branchid"),
        "appl_no" : orderProductId,
        "flag_book" : "Y",
        "jenis_kegiatan_usaha" : _providerFoto.jenisKegiatanUsahaSelected.id
      });

      insertLog(context, _timeStartValidate, DateTime.now(), _body, "Start Check Limit Submit", "Start Check Limit Submit");
      String _creditLimitCheckLimit = await storage.read(key: "CreditLimitCheckLimit");

      final _response = await _http.post(
          "${BaseUrl.urlLME}$_creditLimitCheckLimit",
          body: _body,
          headers: {
            "Content-Type":"application/json",
            "Authorization":"bearer $token",
          }
      ).timeout(Duration(seconds: 10));

      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        if(_result['response_code'] == "00") {
          loadData = false;
          _showSnackBarSuccess("Check Limit berhasil");
          submitData(body, context);
        }
        else {
          loadData = false;
          messageProcess = "";
          _showSnackBar("${_result['response_desc']}");
        }
      }
      else{
        loadData = false;
        messageProcess = "";
        _showSnackBar("Check limit gagal, status code ${_response.statusCode}");
      }
    }
    on TimeoutException catch(_){
      loadData = false;
      messageProcess = "";
      _showSnackBar("Request Timeout");
    }
    catch(e) {
      loadData = false;
      messageProcess = "";
      _showSnackBar("Error ${e.toString()}");
    }
  }

  Future<String> getMplActivationWithLmeId(BuildContext context) async {
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);

    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _date = dateFormat3.format(DateTime.now());
    List _dataCL = await _dbHelper.selectMS2LME();
    var _providerInfoNasabah = Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false);

    var _body = jsonEncode({
      "oid" : _providerInfoNasabah.obligorID != "NEW" ? _providerInfoNasabah.obligorID : _preferences.getString("oid").toString(),
      "lme_id" : _dataCL[0]['lme_id'],
      "branch_code": _preferences.getString("branchid").toString(),
      "source_reff_id": "MS2${_preferences.getString("username")}-${_providerInfoNasabah.obligorID != "NEW" ? _providerInfoNasabah.obligorID : _preferences.getString("oid").toString()}",
      "req_date": _date.toString(),
      "source_channel_id": "MS2"
    });

    String _getMplActivation = await storage.read(key: "CreditLimitCheckMplActivation");
    try {
      final _response = await _http.post(
          "${BaseUrl.urlLME}$_getMplActivation",
          body: _body,
          headers: {
            "Content-Type":"application/json",
            "Authorization":"bearer $token",
          }
      ).timeout(Duration(seconds: 10));

      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        return _result['contract_reference'].toString();
      }
      else{
        throw "Error response status code ${_response.statusCode}";
      }
    }
    on TimeoutException catch(_){
      throw "Request Timeout";
    }
    catch(e) {
      throw "Error get mpl activation: ${e.toString()}";
    }
  }

  void submitData(String body,BuildContext context) async {
    loadData = true;
    messageProcess = "Proses submit data...";
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    var _providerResultSurvey = Provider.of<ResultSurveyChangeNotifier>(context, listen: false);

    final _http = IOClient(ioc);
    String _submitIDE = await storage.read(key: "SubmitIDE");
    String _submitRegulerSurvey = await storage.read(key: "SubmitRegulerSurvey");
    String _submitPAC = await storage.read(key: "SubmitPAC");
    String _submitIA = await storage.read(key: "SubmitIA");
    String _submitAOSCMOApprove = await storage.read(key: "AOSCMOApprove");
    String _submitAOSCMOUnapprove = await storage.read(key: "AOSCMOUnapprove");
    String _submitAOSCFOApprove = await storage.read(key: "AOSCFOApprove");
    String _submitAOSCFOUnapprove = await storage.read(key: "AOSCFOUnapprove");
    // String _submitIAdanPACNDS = await storage.read(key: "SubmitIA(approve)danPACNDS");
    // String _submitIAUnapprovePACDS = await storage.read(key: "SubmitIA(unapprove)PACDS");
    // String _submitResurvey = await storage.read(key: "SubmitResurvey");
    // String _submitDakor = await storage.read(key: "SubmitDakor");

    if(_preferences.getString("last_known_state") == "IDE") {
      // print("IDE submit");
      // print("${BaseUrl.urlGeneral}$_submitIDE");
      // IDE
      DateTime _timeStartValidate = DateTime.now();
      insertLog(context, _timeStartValidate, DateTime.now(), body, "Start Submit ${_preferences.getString("last_known_state")}", "Start Submit ${_preferences.getString("last_known_state")}");
      try{
        final _response = await _http.post(
            // "http://10.134.177.143:82/ad1ms2_backend/public/api/testSubmit",
            "${BaseUrl.urlGeneral}$_submitIDE",
            // "${urlAcction}adira-acction-071043/acction/service/mss/ide",
            body: body,
            headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
        ).timeout(Duration(seconds: 300));

        // print("cek response submit ${_response.statusCode}");
        print("cek response code submit ${_response.statusCode}");

        debugPrint("cek response code submit ${_response.body}");
        insertLog(context, _timeStartValidate, DateTime.now(), body, _response.body, "Submit ${_preferences.getString("last_known_state")}");
        _insertApplNoDeleteTask(context);

        // if(_response.statusCode == 200){
        //   var _result = jsonDecode(_response.body);
        //   debugPrint("cek response code submit ${_response.body}");
        //   _insertApplNoDeleteTask(context);
        //   insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
        //
        //   // if(_result['status'] == "1"){
        //   //   print("cek response submit $_result");
        //   //   loadData = false;
        //   //   _showSnackBarSuccess("Submit $_result");
        //   //   insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
        //   //   _insertApplNoDeleteTask(context);
        //   // }
        //   // else{
        //   //   loadData = false;
        //   //   // print("cek status submit $_result");
        //   //   // print("cek status submit2 ${_result['message']}");
        //   //   // _showSnackBar("Error submit ${_result['message']}");
        //   //   insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
        //   //   _showSnackBar("Gagal Submit Read Timeout");
        //   // }
        //
        // }
        // else{
        //   debugPrint("masuk sini parent");
        //   // print("cek response submit ${_response.statusCode}");
        //   // print("cek response submit2 ${_response.body}");
        //   insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_response.body), "submit ${_preferences.getString("last_known_state")}");
        //   loadData = false;
        //   _showSnackBar("Error ${_response.statusCode} submit data");
        // }
      }
      on TimeoutException catch(_){
        loadData = false;
        _showSnackBar("Timeout connection APK submit IDE");
      }
      catch(e){
        // print("cek error submit ${e.toString()}");
        loadData = false;
        insertLog(context, _timeStartValidate, DateTime.now(), body, e.toString(), "submit ${_preferences.getString("last_known_state")}");
        _showSnackBar("Error submit data ${e.toString()}");
      }
    }
    else if(_preferences.getString("last_known_state") == "SRE" || _preferences.getString("last_known_state") == "RSVY") {
      // Reguler Survey dan Resurvey
      DateTime _timeStartValidate = DateTime.now();
      insertLog(context, _timeStartValidate, DateTime.now(), body, "Start Submit ${_preferences.getString("last_known_state")}", "Start Submit ${_preferences.getString("last_known_state")}");
      try{
        loadData = true;
        final _response = await _http.post(
            "${BaseUrl.urlAcction}$_submitRegulerSurvey",
            // "${urlAcction}adira-acction-071043/acction/service/mss/sre/reguler",
            body: body,
            headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
        ).timeout(Duration(seconds: 300));

        debugPrint("CEK_RESPONSE SUBMIT SRE ${_response.body}");
        debugPrint("CEK_RESPONSE SUBMIT SRE ${_response.statusCode}");
        insertLog(context, _timeStartValidate, DateTime.now(), body, _response.toString(), "Submit ${_preferences.getString("last_known_state")}");
        if(_providerResultSurvey.resultSurveySelected != null) {
          if(_providerResultSurvey.resultSurveySelected.KODE != "001") {
            _insertApplNoDeleteTask(context);
          } else {
            loadData = false;
            messageProcess = "";
            Future.delayed(Duration(seconds: 1), () {
              Navigator.pop(context);
            });
          }
        }

        // if(_response.statusCode == 200){
        //   var _result = jsonDecode(_response.body);
        //   print("cek response submit $_result");
        //   _showSnackBarSuccess("Submit $_result");
        //   _insertApplNoDeleteTask(context);
        //   insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
        //
        //   // if(_result['status'] == "1"){
        //   //   print("cek response submit $_result");
        //   //   loadData = false;
        //   //   _showSnackBarSuccess("Submit $_result");
        //   //   insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
        //   //   _insertApplNoDeleteTask(context);
        //   // }
        //   // else{
        //   //   loadData = false;
        //   //   // print("cek status submit $_result");
        //   //   // print("cek status submit2 ${_result['message']}");
        //   //   // _showSnackBar("Error submit ${_result['message']}");
        //   //   insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
        //   //   _showSnackBar("Gagal Submit Read Timeout");
        //   // }
        //   // loadData = false;
        //   // _showSnackBar("Berhasil Submit SRE");
        //   // insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_response.body), "submit ${_preferences.getString("last_known_state")}");
        //   // _insertApplNoDeleteTask(context);
        //   // Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => HomePage()), (Route <dynamic> route) => false);
        // }
        // else{
        //   loadData = false;
        //   insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_response.body), "submit ${_preferences.getString("last_known_state")}");
        //   _showSnackBar("Error ${_response.statusCode}");
        // }
      }
      on TimeoutException catch(_){
        loadData = false;
        _showSnackBar("Timeout connection APK submit SRE");
      }
      catch(e){
        // print("cek error submit ${e.toString()}");
        loadData = false;
        insertLog(context, _timeStartValidate, DateTime.now(), body, e.toString(), "Submit ${_preferences.getString("last_known_state")}");
        _showSnackBar("Error submit SRE ${e.toString()}");
      }
    }
    else if(_preferences.getString("last_known_state") == "PAC") {
      // PAC
      DateTime _timeStartValidate = DateTime.now();
      insertLog(context, _timeStartValidate, DateTime.now(), body, "Start Submit ${_preferences.getString("last_known_state")}", "Start Submit ${_preferences.getString("last_known_state")}");
      if(Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).groupSalesSelected.kode == "0000W") {
        // PAC NDS
        try{
          final _response = await _http.post(
              "${BaseUrl.urlAcction}$_submitPAC",
              body: body,
              headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
          ).timeout(Duration(seconds: 300));

          debugPrint("cek response submit ${_preferences.getString("last_known_state")} ${_response.toString()} ");
          _showSnackBar("Berhasil Submit ${_preferences.getString("last_known_state")}");
          insertLog(context, _timeStartValidate, DateTime.now(), body,  _response.toString(), "Submit ${_preferences.getString("last_known_state")}");
          if(_providerResultSurvey.resultSurveySelected != null) {
            if(_providerResultSurvey.resultSurveySelected.KODE != "001") {
              _insertApplNoDeleteTask(context);
            } else {
              loadData = false;
              messageProcess = "";
              Future.delayed(Duration(seconds: 1), () {
                Navigator.pop(context);
              });
            }
          }

          // if(_response.statusCode == 201){
          //   var _result = jsonDecode(_response.body);
          //   if(_result['status'] == "1"){
          //     debugPrint("cek response submit ${_preferences.getString("last_known_state")} $_result ");
          //     _showSnackBar("Berhasil Submit ${_preferences.getString("last_known_state")}");
          //     _insertApplNoDeleteTask(context);
          //     insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
          //   }
          //   else{
          //     loadData = false;
          //     insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
          //     _showSnackBar("Gagal Submit Read Timeout ${_preferences.getString("last_known_state")}");
          //   }
          //   // Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => HomePage()), (Route <dynamic> route) => false);
          // }
          // else{
          //   loadData = false;
          //   _showSnackBar("Error ${_response.statusCode}");
          // }
        }
        on TimeoutException catch(_){
          loadData = false;
          debugPrint("Timeout connection APK submit ${_preferences.getString("last_known_state")} ");
          _showSnackBar("Timeout connection APK submit ${_preferences.getString("last_known_state")}");
        }
        catch(e){
          loadData = false;
          debugPrint("Error submit data ${_preferences.getString("last_known_state")} ${e.toString()}");
          insertLog(context, _timeStartValidate, DateTime.now(), body, e.toString(), "Submit ${_preferences.getString("last_known_state")}");
          _showSnackBar("Error submit data ${_preferences.getString("last_known_state")}  ${e.toString()}");
        }
      }
      else {
        // PAC DS
        try{
          final _response = await _http.post(
              "${BaseUrl.urlAcction}$_submitIA",
              // "${urlAcction}adira-acction-071043/acction/service/mss/sre/ia",
              body: body,
              headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
          );

          debugPrint("cek response submit ${_preferences.getString("last_known_state")} ${_response.toString()} ");
          _showSnackBar("Berhasil Submit ${_preferences.getString("last_known_state")}");
          _insertApplNoDeleteTask(context);
          insertLog(context, _timeStartValidate, DateTime.now(), body,  _response.toString(), "submit ${_preferences.getString("last_known_state")}");

          // if(_response.statusCode == 201){
          //   var _result = jsonDecode(_response.body);
          //   if(_result['status'] == "1"){
          //     debugPrint("cek response submit ${_preferences.getString("last_known_state")} $_result ");
          //     _showSnackBar("Berhasil Submit ${_preferences.getString("last_known_state")}");
          //     _insertApplNoDeleteTask(context);
          //     insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
          //   }
          //   else{
          //     loadData = false;
          //     debugPrint("cek response submit ${_preferences.getString("last_known_state")} $_result ");
          //     insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
          //     _showSnackBar("Gagal Submit Read Timeout ${_preferences.getString("last_known_state")}");
          //   }
          //   // Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => HomePage()), (Route <dynamic> route) => false);
          // }
          // else{
          //   loadData = false;
          //   debugPrint("cek response submit ${_preferences.getString("last_known_state")} ${_response.body} ");
          //   _showSnackBar("Error ${_response.statusCode}");
          // }
        }
        on TimeoutException catch(_){
          loadData = false;
          debugPrint("Timeout connection APK submit ${_preferences.getString("last_known_state")} ");
          _showSnackBar("Timeout connection APK submit ${_preferences.getString("last_known_state")}");
        }
        catch(e){
          loadData = false;
          debugPrint("Timeout connection APK submit ${e.toString()} ${_preferences.getString("last_known_state")} ");
          insertLog(context, _timeStartValidate, DateTime.now(), body, e.toString(), "submit ${_preferences.getString("last_known_state")}");
          _showSnackBar("Error submit data ${e.toString()} ${_preferences.getString("last_known_state")} ");
        }
      }
    }
    else if(_preferences.getString("last_known_state") == "IA") {
      // IA
      DateTime _timeStartValidate = DateTime.now();
      insertLog(context, _timeStartValidate, DateTime.now(), body, "Start Submit ${_preferences.getString("last_known_state")}", "Start Submit ${_preferences.getString("last_known_state")}");
      if(Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).groupSalesSelected.kode == "000W") {
        // PAC NDS
        try{
          final _response = await _http.post(
              "${BaseUrl.urlAcction}$_submitIA",
              // "${urlAcction}adira-acction-071043/acction/service/mss/sre/ia/nds",
              body: body,
              headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
          ).timeout(Duration(seconds: 300));

          debugPrint("cek response submit ${_preferences.getString("last_known_state")} ${_response.toString()} ");
          _showSnackBar("Berhasil Submit ${_preferences.getString("last_known_state")}");
          insertLog(context, _timeStartValidate, DateTime.now(), body,  _response.toString(), "Submit ${_preferences.getString("last_known_state")}");
          if(_providerResultSurvey.resultSurveySelected != null) {
            if(_providerResultSurvey.resultSurveySelected.KODE != "001") {
              _insertApplNoDeleteTask(context);
            } else {
              loadData = false;
              messageProcess = "";
              Future.delayed(Duration(seconds: 1), () {
                Navigator.pop(context);
              });
            }
          }

          // if(_response.statusCode == 201){
          //   var _result = jsonDecode(_response.body);
          //   if(_result['status'] == "1"){
          //     debugPrint("cek response submit ${_preferences.getString("last_known_state")} $_result ");
          //     _showSnackBar("Berhasil Submit ${_preferences.getString("last_known_state")}");
          //     _insertApplNoDeleteTask(context);
          //     insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
          //   }
          //   else{
          //     loadData = false;
          //     insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
          //     _showSnackBar("Gagal Submit Read Timeout ${_preferences.getString("last_known_state")}");
          //   }
          //   // Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => HomePage()), (Route <dynamic> route) => false);
          // }
          // else{
          //   loadData = false;
          //   _showSnackBar("Error ${_response.statusCode}");
          // }
        }
        on TimeoutException catch(_){
          loadData = false;
          debugPrint("Timeout connection APK submit ${_preferences.getString("last_known_state")} ");
          _showSnackBar("Timeout connection APK submit ${_preferences.getString("last_known_state")}");
        }
        catch(e){
          loadData = false;
          debugPrint("Error submit data ${_preferences.getString("last_known_state")} ${e.toString()}");
          insertLog(context, _timeStartValidate, DateTime.now(), body, e.toString(), "Submit ${_preferences.getString("last_known_state")}");
          _showSnackBar("Error submit data ${_preferences.getString("last_known_state")}  ${e.toString()}");
        }
      }
      else {
        // PAC DS
        try{
          final _response = await _http.post(
              "${BaseUrl.urlAcction}$_submitIA",
              // "${urlAcction}adira-acction-071043/acction/service/mss/sre/ia",
              body: body,
              headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
          );

          debugPrint("cek response submit ${_preferences.getString("last_known_state")} ${_response.toString()} ");
          _showSnackBar("Berhasil Submit ${_preferences.getString("last_known_state")}");
          _insertApplNoDeleteTask(context);
          insertLog(context, _timeStartValidate, DateTime.now(), body,  _response.toString(), "submit ${_preferences.getString("last_known_state")}");

          // if(_response.statusCode == 201){
          //   var _result = jsonDecode(_response.body);
          //   if(_result['status'] == "1"){
          //     debugPrint("cek response submit ${_preferences.getString("last_known_state")} $_result ");
          //     _showSnackBar("Berhasil Submit ${_preferences.getString("last_known_state")}");
          //     _insertApplNoDeleteTask(context);
          //     insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
          //   }
          //   else{
          //     loadData = false;
          //     debugPrint("cek response submit ${_preferences.getString("last_known_state")} $_result ");
          //     insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
          //     _showSnackBar("Gagal Submit Read Timeout ${_preferences.getString("last_known_state")}");
          //   }
          //   // Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => HomePage()), (Route <dynamic> route) => false);
          // }
          // else{
          //   loadData = false;
          //   debugPrint("cek response submit ${_preferences.getString("last_known_state")} ${_response.body} ");
          //   _showSnackBar("Error ${_response.statusCode}");
          // }
        }
        on TimeoutException catch(_){
          loadData = false;
          debugPrint("Timeout connection APK submit ${_preferences.getString("last_known_state")} ");
          _showSnackBar("Timeout connection APK submit ${_preferences.getString("last_known_state")}");
        }
        catch(e){
          loadData = false;
          debugPrint("Timeout connection APK submit ${e.toString()} ${_preferences.getString("last_known_state")} ");
          insertLog(context, _timeStartValidate, DateTime.now(), body, e.toString(), "submit ${_preferences.getString("last_known_state")}");
          _showSnackBar("Error submit data ${e.toString()} ${_preferences.getString("last_known_state")} ");
        }
      }
    }
    else if(_preferences.getString("last_known_state") == "AOS") {
      // AOS
      DateTime _timeStartValidate = DateTime.now();
      insertLog(context, _timeStartValidate, DateTime.now(), body, "Start Submit ${_preferences.getString("last_known_state")}", "Start Submit ${_preferences.getString("last_known_state")}");
      try{
        final _response =  await _http.post(_providerResultSurvey.radioValueApproved == "1"
            ? "${BaseUrl.urlAcction}$_submitAOSCMOApprove" : "${BaseUrl.urlAcction}$_submitAOSCMOUnapprove",
            // "${urlAcction}adira-acction-071043/acction/service/mss/sre/ia/nds",
            body: body,
            headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
        ).timeout(Duration(seconds: 300));

        debugPrint("cek response submit ${_preferences.getString("last_known_state")} ${_response.toString()} ");
        _showSnackBar("Berhasil Submit ${_preferences.getString("last_known_state")}");
        insertLog(context, _timeStartValidate, DateTime.now(), body,  _response.toString(), "Submit ${_preferences.getString("last_known_state")}");
        if(_providerResultSurvey.resultSurveySelected != null) {
          if(_providerResultSurvey.resultSurveySelected.KODE != "001") {
            _insertApplNoDeleteTask(context);
          } else {
            loadData = false;
            messageProcess = "";
            Future.delayed(Duration(seconds: 1), () {
              Navigator.pop(context);
            });
          }
        }

        // if(_response.statusCode == 201){
        //   var _result = jsonDecode(_response.body);
        //   if(_result['status'] == "1"){
        //     debugPrint("cek response submit ${_preferences.getString("last_known_state")} $_result ");
        //     _showSnackBar("Berhasil Submit ${_preferences.getString("last_known_state")}");
        //     _insertApplNoDeleteTask(context);
        //     insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
        //   }
        //   else{
        //     loadData = false;
        //     insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
        //     _showSnackBar("Gagal Submit Read Timeout ${_preferences.getString("last_known_state")}");
        //   }
        //   // Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => HomePage()), (Route <dynamic> route) => false);
        // }
        // else{
        //   loadData = false;
        //   _showSnackBar("Error ${_response.statusCode}");
        // }
      }
      on TimeoutException catch(_){
        loadData = false;
        debugPrint("Timeout connection APK submit ${_preferences.getString("last_known_state")} ");
        _showSnackBar("Timeout connection APK submit ${_preferences.getString("last_known_state")}");
      }
      catch(e){
        loadData = false;
        debugPrint("Error submit data ${_preferences.getString("last_known_state")} ${e.toString()}");
        insertLog(context, _timeStartValidate, DateTime.now(), body, e.toString(), "Submit ${_preferences.getString("last_known_state")}");
        _showSnackBar("Error submit data ${_preferences.getString("last_known_state")}  ${e.toString()}");
      }
    }
    else if(_preferences.getString("last_known_state") == "CONA") {
      // AOS
      DateTime _timeStartValidate = DateTime.now();
      insertLog(context, _timeStartValidate, DateTime.now(), body, "Start Submit ${_preferences.getString("last_known_state")}", "Start Submit ${_preferences.getString("last_known_state")}");
      try{
        final _response =  await _http.post(_providerResultSurvey.radioValueApproved == "1"
            ? "${BaseUrl.urlAcction}$_submitAOSCFOApprove" : "${BaseUrl.urlAcction}$_submitAOSCFOUnapprove",
            // "${urlAcction}adira-acction-071043/acction/service/mss/sre/ia/nds",
            body: body,
            headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
        ).timeout(Duration(seconds: 300));

        debugPrint("cek response submit ${_preferences.getString("last_known_state")} ${_response.toString()} ");
        _showSnackBar("Berhasil Submit ${_preferences.getString("last_known_state")}");
        insertLog(context, _timeStartValidate, DateTime.now(), body,  _response.toString(), "Submit ${_preferences.getString("last_known_state")}");
        if(_providerResultSurvey.resultSurveySelected != null) {
          if(_providerResultSurvey.resultSurveySelected.KODE != "001") {
            _insertApplNoDeleteTask(context);
          } else {
            loadData = false;
            messageProcess = "";
            Future.delayed(Duration(seconds: 1), () {
              Navigator.pop(context);
            });
          }
        }

        // if(_response.statusCode == 201){
        //   var _result = jsonDecode(_response.body);
        //   if(_result['status'] == "1"){
        //     debugPrint("cek response submit ${_preferences.getString("last_known_state")} $_result ");
        //     _showSnackBar("Berhasil Submit ${_preferences.getString("last_known_state")}");
        //     _insertApplNoDeleteTask(context);
        //     insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
        //   }
        //   else{
        //     loadData = false;
        //     insertLog(context, _timeStartValidate, DateTime.now(), body, jsonEncode(_result), "submit ${_preferences.getString("last_known_state")}");
        //     _showSnackBar("Gagal Submit Read Timeout ${_preferences.getString("last_known_state")}");
        //   }
        //   // Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => HomePage()), (Route <dynamic> route) => false);
        // }
        // else{
        //   loadData = false;
        //   _showSnackBar("Error ${_response.statusCode}");
        // }
      }
      on TimeoutException catch(_){
        loadData = false;
        debugPrint("Timeout connection APK submit ${_preferences.getString("last_known_state")} ");
        _showSnackBar("Timeout connection APK submit ${_preferences.getString("last_known_state")}");
      }
      catch(e){
        loadData = false;
        debugPrint("Error submit data ${_preferences.getString("last_known_state")} ${e.toString()}");
        insertLog(context, _timeStartValidate, DateTime.now(), body, e.toString(), "Submit ${_preferences.getString("last_known_state")}");
        _showSnackBar("Error submit data ${_preferences.getString("last_known_state")}  ${e.toString()}");
      }
    }
    // else if(_preferences.getString("last_known_state") == "DKR") {
    //   // Dakor
    //   DateTime _timeStartValidate = DateTime.now();
    //   insertLog(context, _timeStartValidate, DateTime.now(), body, "Start Submit Dakor", "submit ${_preferences.getString("last_known_state")}");
    //   try{
    //     final _response = await _http.post(
    //         "${BaseUrl.urlAcction}$_submitDakor",
    //         // "${urlAcction}adira-acction-071043/acction/service/mss/dakor",
    //         body: body,
    //         headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    //     );
    //
    //     debugPrint("cek response submit ${_preferences.getString("last_known_state")} ${_response.toString()} ");
    //     _showSnackBar("Berhasil Submit ${_preferences.getString("last_known_state")}");
    //     _insertApplNoDeleteTask(context);
    //     insertLog(context, _timeStartValidate, DateTime.now(), body,  _response.toString(), "submit ${_preferences.getString("last_known_state")}");
    //
    //     if(_response.statusCode == 200){
    //       _showSnackBar("Berhasil Submit");
    //       Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => HomePage()), (Route <dynamic> route) => false);
    //     }
    //     else{
    //       _showSnackBar("Error ${_response.statusCode}");
    //     }
    //   }
    //   on TimeoutException catch(_){
    //     loadData = false;
    //     debugPrint("Timeout connection APK submit ${_preferences.getString("last_known_state")} ");
    //     _showSnackBar("Timeout connection APK submit ${_preferences.getString("last_known_state")}");
    //   }
    //   catch(e){
    //     loadData = false;
    //     debugPrint("Timeout connection APK submit ${e.toString()} ${_preferences.getString("last_known_state")} ");
    //     insertLog(context, _timeStartValidate, DateTime.now(), body, e.toString(), "submit ${_preferences.getString("last_known_state")}");
    //     _showSnackBar("Error submit data ${e.toString()} ${_preferences.getString("last_known_state")} ");
    //   }
    // }
  }

  Future<bool> isValidateTabDrawer(BuildContext context,int index) async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    final _form = formKeys[index].currentState;
    var _providerFormMFotoChangeNotif = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
    var _providerFormMCustomerInfoChangeNotifier = Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false);
    var _providerOccupationChangeNotif = Provider.of<FormMOccupationChangeNotif>(context, listen: false);
    var _providerInfoAlamat = Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false);
    var _providerInfoKeluargaIbu = Provider.of<FormMInformasiKeluargaIBUChangeNotif>(context, listen: false);
    var _providerInfoKeluarga = Provider.of<FormMInfoKeluargaChangeNotif>(context, listen: false);
    var _providerInfoApp = Provider.of<InfoAppChangeNotifier>(context, listen: false);
    var _providerIncomeChangeNotif = Provider.of<FormMIncomeChangeNotifier>(context, listen: false);
    var _providerInfoObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
    var _providerInfoSales = Provider.of<InformationSalesmanChangeNotifier>(context, listen: false);
    var _providerKolateral = Provider.of<InformationCollateralChangeNotifier>(context, listen: false);
    var _providerKaroseri = Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false);
    var _providerInfoCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false);
    var _providerCreditSubsidy = Provider.of<InfoCreditSubsidyChangeNotifier>(context, listen: false);
    var _providerMajorInsurance = Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false);
    var _providerAddMajorInsurance = Provider.of<FormMAddMajorInsuranceChangeNotifier>(context, listen: false);
    var _providerAdditionalInsurance = Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false);
    var _providerAddAdditionalInsurance = Provider.of<FormMAddAdditionalInsuranceChangeNotifier>(context, listen: false);
    var _providerWMP = Provider.of<InfoWMPChangeNotifier>(context, listen: false);
    var _providerInfoDocument = Provider.of<InfoDocumentChangeNotifier>(context, listen: false);
    var _providerTaksasiUnit = Provider.of<TaksasiUnitChangeNotifier>(context, listen: false);

    var _isValidate = false;

    if(index == 0){
      if(isPhotoDone){
        if(await _providerFormMFotoChangeNotif.deleteSQLite()){
          _providerFormMFotoChangeNotif.saveToSQLite(context);
        }
        _isValidate = false;
        selectedIndex += 1;
      } else {
        _isValidate = true;
      }
//      if (_providerFormMFotoChangeNotif.listFotoTempatTinggal.isEmpty) {
//        _providerFormMFotoChangeNotif.autoValidateFotoTempatTinggal = true;
//        _isValidate = true;
//      }
//      if (_providerFormMFotoChangeNotif.listGroupUnitObject.isEmpty) {
//        _providerFormMFotoChangeNotif.autoValidateGroupObjectUnit = true;
//        _isValidate = true;
//      }
//      if (_providerFormMFotoChangeNotif.jenisKegiatanUsahaSelected != null
//          || _providerFormMFotoChangeNotif.kegiatanUsahaSelected != null
//          || _providerFormMFotoChangeNotif.occupationSelected != null
//          || _providerFormMFotoChangeNotif.typeOfFinancingModelSelected != null) {
//        _providerFormMFotoChangeNotif.autoValidate = true;
//        _isValidate = true;
//      }
//      if (_providerFormMFotoChangeNotif.listDocument.isEmpty) {
//        _providerFormMFotoChangeNotif.autoValidateDocumentObjectUnit = true;
//        _isValidate = true;
//      }
//      if (_providerFormMFotoChangeNotif.occupationSelected != null) {
//        if (_providerFormMFotoChangeNotif.occupationSelected.KODE == "OC005" ||
//            _providerFormMFotoChangeNotif.occupationSelected.KODE == "OC006" ||
//            _providerFormMFotoChangeNotif.occupationSelected.KODE == "OC007") {
//          if (_providerFormMFotoChangeNotif.listFotoTempatUsaha.isEmpty) {
//            _providerFormMFotoChangeNotif.autoValidateTempatUsaha = true;
//            _isValidate = true;
//          }
//        }
//      }
//
//      if ((_providerFormMFotoChangeNotif.jenisKegiatanUsahaSelected != null
//          && _providerFormMFotoChangeNotif.kegiatanUsahaSelected != null
//          && _providerFormMFotoChangeNotif.occupationSelected != null
//          && _providerFormMFotoChangeNotif.typeOfFinancingModelSelected != null)
//          && _providerFormMFotoChangeNotif.listFotoTempatTinggal.isNotEmpty
//          && _providerFormMFotoChangeNotif.listGroupUnitObject.isNotEmpty
//          && _providerFormMFotoChangeNotif.listDocument.isNotEmpty) {
//        if (_providerFormMFotoChangeNotif.occupationSelected.KODE == "OC005" ||
//            _providerFormMFotoChangeNotif.occupationSelected.KODE == "OC006" ||
//            _providerFormMFotoChangeNotif.occupationSelected.KODE == "OC007") {
//          if (_providerFormMFotoChangeNotif.listFotoTempatUsaha.isNotEmpty) {
//            selectedIndex += 1;
//            isPhotoDone = true;
//            _isValidate = false;
//          }
//        }
//        else {
//          selectedIndex += 1;
//          isPhotoDone = true;
//          _providerFormMFotoChangeNotif.saveToSQLite(context);
//          _isValidate = false;
//        }
//      }
    }
    else if (index == 1) {
      if(!_providerFormMCustomerInfoChangeNotifier.flag){
        _providerFormMCustomerInfoChangeNotifier.autoValidate = true;
        _isValidate = true;
      }
      if(!_providerInfoAlamat.flag){
        _providerInfoAlamat.autoValidate = true;
        _isValidate = true;
      }
      if(!_providerInfoKeluargaIbu.flag){
        _providerInfoKeluargaIbu.autoValidate = true;
        _isValidate = true;
      }

      if(_providerFormMCustomerInfoChangeNotifier.maritalStatusSelected.id == "01") {
        if(_providerInfoKeluarga.listFormInfoKel.length == 0){
          if(!_providerInfoKeluarga.flag){
            _providerInfoKeluarga.autoValidate = true;
            _isValidate = true;
          }
        }
      }

      // SubmitDataPartial().submitCustomerIndividu(context);

      if(_providerFormMCustomerInfoChangeNotifier.maritalStatusSelected.id == "01") {
        print('masuk sini cuy');
        if(_providerInfoKeluarga.listFormInfoKel.length != 0){
          print('keluarga udah ada isi');
          print(_providerFormMCustomerInfoChangeNotifier.flag);
          print(_providerInfoAlamat.flag);
          print(_providerInfoKeluargaIbu.flag);
          print(_providerInfoKeluarga.flag);
          if(_providerFormMCustomerInfoChangeNotifier.flag && _providerInfoAlamat.flag && _providerInfoKeluargaIbu.flag && _providerInfoKeluarga.listFormInfoKel.length != 0){
            print('pake keluarga');
            if(await _providerFormMCustomerInfoChangeNotifier.deleteSQLite() && await _providerInfoKeluargaIbu.deleteSQLite() && await _providerInfoAlamat.deleteSQLite("5")){
              _providerFormMCustomerInfoChangeNotifier.saveToSQLite();
              _providerInfoKeluargaIbu.saveToSQLite();
              _providerInfoKeluarga.saveToSQLite();
              _providerInfoAlamat.saveToSQLite("5");
            }
            selectedIndex += 1;
            isMenuCustomerDetailDone = true;
          }
        }
      }
      else {
        print('dilewat woy');
        if(_providerFormMCustomerInfoChangeNotifier.flag && _providerInfoAlamat.flag && _providerInfoKeluargaIbu.flag){
          if(await _providerFormMCustomerInfoChangeNotifier.deleteSQLite() && await _providerInfoKeluargaIbu.deleteSQLite() && await _providerInfoAlamat.deleteSQLite("5")){
            _providerFormMCustomerInfoChangeNotifier.saveToSQLite();
            _providerInfoKeluargaIbu.saveToSQLite();
            _providerInfoKeluarga.saveToSQLite();
            _providerInfoAlamat.saveToSQLite("5");
          }
          selectedIndex += 1;
          isMenuCustomerDetailDone = true;
        }
      }

      // coding lama
//       if(!_providerFormMCustomerInfoChangeNotifier.flag){
//         _providerFormMCustomerInfoChangeNotifier.autoValidate = true;
//         _isValidate = true;
//       }
//       if(!_providerInfoAlamat.flag){
//         _providerInfoAlamat.autoValidate = true;
//         _isValidate = true;
//       }
//
//       if(!_providerInfoKeluargaIbu.flag){
//         _providerInfoKeluargaIbu.autoValidate = true;
//         _isValidate = true;
//       }
//       //Jangan dihapus
//       //      if(_providerInfoKeluaga.listFormInfoKel.length == 0){
//       //        _providerInfoKeluaga.flag = true;
//       //      }
//       if(_providerFormMCustomerInfoChangeNotifier.flag && _providerInfoAlamat.flag && _providerInfoKeluargaIbu.flag ){
//         selectedIndex += 1;
//         isMenuCustomerDetailDone = true;
// //        _providerInfoAlamat.saveToSQLite("5");
//         _isValidate = false;
//       }
      return _isValidate;
    }
    else if (index == 2) {
      if (isOccupationDone) {
        await _providerOccupationChangeNotif.checkDataDakor();
        selectedIndex += 1;
        isOccupationDone = true;
        if(await _providerOccupationChangeNotif.deleteSQLite()){
          if(await _providerInfoAlamat.deleteSQLite("4"))
            _providerOccupationChangeNotif.saveToSQLite(context, "4");
          _isValidate = false;
        }
      }
      else {
        _providerOccupationChangeNotif.autoValidate = true;
        _isValidate = true;
      }

//       if (isOccupationDone) {
//         selectedIndex += 1;
// //        _providerOccupationChangeNotif.saveToSQLite(context, "4");
//         _isValidate = false;
//       } else {
//         _providerOccupationChangeNotif.autoValidate = true;
//         _isValidate = true;
//       }
      return _isValidate;
    }
    else if (index == 3) {
      if (_form.validate()) {
        print(Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false).isGuarantorVisible);
        if(await _providerIncomeChangeNotif.deleteSQLite()){
          _providerIncomeChangeNotif.saveToSQLite(context);
          // if(Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false).isGuarantorVisible){
          //   selectedIndex += 2;
          //   isIncomeDone = true;
          //   _isValidate = false;
          // }
          // else{
            selectedIndex += 1;
            isIncomeDone = true;
            _isValidate = false;
          // }
        }
      } else {
        isIncomeDone = false;
        _providerIncomeChangeNotif.autoValidate = true;
        _isValidate = true;
      }

      // if(isIncomeDone){
      //   // if(Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false).isGuarantorVisible){
      //     selectedIndex += 1;
      //     isIncomeDone = true;
      //     _isValidate = false;
      //   // }
      //   // else{
      //   //   selectedIndex += 2;
      //   //   isIncomeDone = true;
      //   //   _isValidate = false;
      //   // }
      // } else {
      //   _isValidate = true;
      // }
//      if (_form.validate()) {
//        if(Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false).isGuarantorVisible){
//          selectedIndex += 1;
//          isIncomeDone = true;
//          _isValidate = false;
//        }
//        else{
//          selectedIndex += 2;
//          isIncomeDone = true;
//          _isValidate = false;
//        }
//        _providerIncomeChangeNotif.saveToSQLite(context);
//      } else {
//        _providerIncomeChangeNotif.autoValidate = true;
//        _isValidate = true;
//      }
     return _isValidate;
    }
    else if (index == 4) {
      var _provider = Provider.of<FormMGuarantorChangeNotifier>(context, listen: false);
      if(_provider.radioValueIsWithGuarantor == 0){
        if(_provider.listGuarantorIndividual.isNotEmpty || _provider.listGuarantorCompany.isNotEmpty){
          selectedIndex += 1;
          isGuarantorDone = true;
          if(await _provider.deleteSQLite()){
            _provider.saveToSQLite(context);
            // _provider.saveToSQLite();
          }
          _isValidate = false;
        }
        else{
          _provider.autoValidate = true;
          _isValidate = true;
        }
      }
      else{
        selectedIndex += 1;
        isGuarantorDone = true;
        _isValidate = false;
      }
      // var _provider = Provider.of<FormMGuarantorChangeNotifier>(context, listen: false);
      // if(_provider.radioValueIsWithGuarantor == 0){
      //   if(_provider.listGuarantorIndividual.isNotEmpty || _provider.listGuarantorCompany.isNotEmpty){
      //     selectedIndex += 1;
      //     isGuarantorDone = true;
      //     _isValidate = false;
      //   }
      //   else{
      //     _provider.autoValidate = true;
      //     _isValidate = true;
      //   }
      // }
      // else{
      //   selectedIndex += 1;
      //   isGuarantorDone = true;
      //   _isValidate = false;
      // }
      return _isValidate;
    }
    else if (index == 5){
      if(isInfoAppDone){
        await _providerInfoApp.checkDataDakor();
        if(await _providerInfoApp.deleteSQLite()){
          _providerInfoApp.saveToSQLite(context);
        }
        selectedIndex += 1;
        isInfoAppDone = true;
        _isValidate = false;
      } else {
        _isValidate = true;
      }
//      if (_form.validate()) {
//        _providerInfoApp.saveToSQLite(context);
//        selectedIndex += 1;
//        isInfoAppDone = true;
//        _isValidate = false;
//      } else {
//        _providerInfoApp.autoValidate = true;
//        _isValidate = true;
//      }
    }
    else if (index == 6){
      if(!_providerInfoObjectUnit.flag){
        _providerInfoObjectUnit.autoValidate = true;
        _isValidate = true;
      }
      if(_providerInfoSales.listInfoSales.length == 0){
        _providerInfoSales.autoValidate = true;
        _isValidate = true;
      }
      if(!_providerKolateral.flag){
        if(_providerKolateral.collateralTypeModel == null){
          _providerKolateral.autoValidateAuto = true;
          _providerKolateral.autoValidateProp = true;
          _isValidate = true;
        }
        else {
          if(_providerKolateral.collateralTypeModel.id == "001"){
            _providerKolateral.autoValidateAuto = true;
            _providerKolateral.autoValidateProp = false;
            _isValidate = true;
          }
          else if(_providerKolateral.collateralTypeModel.id == "002"){
            _providerKolateral.autoValidateAuto = false;
            _providerKolateral.autoValidateProp = true;
            _isValidate = true;
          }
        }
      }
      if(_providerInfoObjectUnit.flag && _providerInfoSales.listInfoSales.length != 0 && _providerKolateral.flag){
        if(await _providerInfoObjectUnit.deleteSQLite()){
          _providerInfoObjectUnit.saveToSQLite(context);
        }
        if(await _providerInfoSales.deleteSQLite()){
          _providerInfoSales.saveToSQLite(context);
        }
        if(_providerKolateral.collateralTypeModel.id == "001"){
          if(await _providerKolateral.deleteSQLiteCollaOto()){
            _providerKolateral.saveToSQLiteOto(context);
          }
        }
        else if(_providerKolateral.collateralTypeModel.id == "002") {
          if(await _providerKolateral.deleteSQLiteCollaProp()){
            _providerKolateral.saveToSQLiteProperti(context, "6");
          }
        }
        // selectedIndex +=1;
        Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: true).isVisible ? selectedIndex +=1 : selectedIndex +=2;
        isMenuObjectInformationDone = true;
        _isValidate = false;
      }
    }
    else if (index == 7){
      if(await _providerKaroseri.deleteSQLite()){
        _providerKaroseri.saveToSQLite();
      }
      selectedIndex += 1;
      isInfoObjKaroseriDone = true;
      _isValidate = false;
      // if(_providerKaroseri.listFormKaroseriObject.isNotEmpty){
      //   if(await _providerKaroseri.deleteSQLite()){
      //     _providerKaroseri.saveToSQLite();
      //   }
      //   selectedIndex += 1;
      //   isInfoObjKaroseriDone = true;
      //   _isValidate = false;
      // } else {
      //   _showSnackBar("Info objek karoseri Utama tidak boleh kosong");
      //   _providerKaroseri.autoValidate = true;
      //   _isValidate = true;
      // }
    }
    else if (index == 8){
      if(_providerInfoCreditStructure.controllerTotalPrice.text.isNotEmpty){
        if(int.parse(_providerInfoCreditStructure.controllerTotalPrice.text.replaceAll(",", "").split(".")[0]) >= 50000000 && _providerFormMCustomerInfoChangeNotifier.radioValueIsHaveNPWP == 0) {
          _providerInfoCreditStructure.flag = false;
          _providerInfoCreditStructure.messageError = "Wajib NPWP pada Rincian Nasabah karena Total Harga lebih dari 50,000,000.00.";
          showDialog(
              context: context,
              barrierDismissible: true,
              builder: (BuildContext context){
                return Theme(
                  data: ThemeData(
                      fontFamily: "NunitoSans",
                      primaryColor: Colors.black,
                      primarySwatch: primaryOrange,
                      accentColor: myPrimaryColor
                  ),
                  child: AlertDialog(
                    title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
                    content: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text("Wajib NPWP pada Rincian Nasabah karena Total Harga lebih dari 50,000,000.00.",),
                      ],
                    ),
                    actions: <Widget>[
                      FlatButton(
                        onPressed: () {
                          Navigator.of(context).pop(true);
                        },
                        child: Text('Close'),
                      ),
                    ],
                  ),
                );
              }
          );
        }
      }
      if(!_providerInfoCreditStructure.flag){
        _providerInfoCreditStructure.autoValidate = true;
        _isValidate = true;
      }
      await _providerMajorInsurance.onBackPress();
      await _providerAdditionalInsurance.onBackPress();
      if(_preferences.getString("last_known_state") != "IDE") {
        if(_providerMajorInsurance.listFormMajorInsurance.isNotEmpty) {
          for(int i=0; i<_providerMajorInsurance.listFormMajorInsurance.length; i++) {
            if(_providerMajorInsurance.listFormMajorInsurance[i].insuranceType.KODE == "2") {
              await _providerAddMajorInsurance.getBatasAtasBawahUtamaUpdated(context, _providerMajorInsurance.listFormMajorInsurance[i], i);
            } else if(_providerMajorInsurance.listFormMajorInsurance[i].insuranceType.KODE == "3") {
              await _providerAddMajorInsurance.getBatasAtasBawahPerluasanUpdated(context, _providerMajorInsurance.listFormMajorInsurance[i], i);
            }
          }
        }
        if(_providerAdditionalInsurance.listFormAdditionalInsurance.isNotEmpty) {
          for(int i=0; i<_providerAdditionalInsurance.listFormAdditionalInsurance.length; i++) {
            await _providerAddAdditionalInsurance.getLowerUpperLimitUpdated(context, _providerAdditionalInsurance.listFormAdditionalInsurance[i], i);
          }
        }
      }
      // if(_providerMajorInsurance.listFormMajorInsurance.length == 0){
      //   _providerMajorInsurance.flag = true;
      //   _isValidate = true;
      // }
      // if(_providerAdditionalInsurance.listFormAdditionalInsurance.length == 0 && _providerInfoObjectUnit.groupObjectSelected.KODE == "002"){
      //   _providerAdditionalInsurance.flag = true;
      //   _isValidate = true;
      // }
      if(_providerInfoCreditStructure.flag && !_providerMajorInsurance.flag && !_providerAdditionalInsurance.flag){
        if(await _providerInfoCreditStructure.deleteSQLite()){
          _providerInfoCreditStructure.saveToSQLiteInfStrukturKreditBiaya();
        }
        _providerInfoCreditStructure.updateMS2ApplObjectInfStrukturKredit();
        if(await _providerMajorInsurance.deleteSQLite()){
          _providerMajorInsurance.saveToSQLite();
        }
        if(await _providerAdditionalInsurance.deleteSQLite()){
          _providerAdditionalInsurance.saveToSQLite();
        }
        if(await _providerWMP.deleteSQLite()){
          _providerWMP.saveToSQLite();
        }
        if(!_providerInfoCreditStructure.checkIsAdminFeeExist(context)){
          dialogFailedSavePartial(context, "Harap isi biaya admin");
        }
        else{
          try{
            loadData = true;
            await Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).calculateCreditNew(context);
            await Provider.of<InfoCreditIncomeChangeNotifier>(context, listen: false).getDSR(context);
            await Provider.of<InfoCreditSubsidyChangeNotifier>(context, listen: false).getSukuBungaSebelumEffFlat(context);
            await _setLastStep(this._selectedIndex);
            loadData = false;
            selectedIndex += 1;
            isMenuDetailLoanDone = true;
          }
          catch(e){
            loadData = false;
            dialogFailedSavePartial(context, e.toString());
          }
        }
      }
    }
    else if (index == 9){
      // if(_providerCreditSubsidy.listInfoCreditSubsidy.isNotEmpty){
        if(await _providerCreditSubsidy.deleteSQLIte()){
          _providerCreditSubsidy.saveToSQLite();
        }
        selectedIndex += 1;
        _isCreditSubsidyDone = true;
        _isValidate = false;
      // } else {
      //   _showSnackBar("Info Kredit Subsidi tidak boleh kosong");
      //   _isValidate = true;
      // }
    }
    else if (index == 10){
      if(_providerInfoDocument.listInfoDocument.isNotEmpty){
        if(await _providerInfoDocument.deleteSQLite()){
          _providerInfoDocument.saveToSQLite(context);
        }
        if(_providerTaksasiUnit.isVisible){
          selectedIndex += 1;
          _isValidate = false;
        }
        else {
          selectedIndex += 2;
          _isValidate = false;
        }
        _isDocumentInfoDone = true;
      } else {
        _showSnackBar("Info Dokumen tidak boleh kosong");
        _isValidate = true;
      }
    }
    else if (index == 11){
      if(_providerKolateral.groupObjectSelected.KODE == "002") {
        if(_providerKolateral.objectSelected.id != "003") {
          _providerTaksasiUnit.calculatedCar(context);
        }
      } else {
        if(_providerKolateral.objectSelected.id != "001") {
          _providerTaksasiUnit.calculatedMotorCycle(context);
        }
      }
      if(_providerInfoObjectUnit.groupObjectSelected != null){
        if(_providerInfoObjectUnit.groupObjectSelected.KODE == "001"){
          if(_form.validate()){
            selectedIndex += 1;
            isTaksasiUnitDone = true;
            _isValidate = false;
          }
          _providerTaksasiUnit.autoValidateMotor;
          _isValidate = true;
        }
        else if(_providerInfoObjectUnit.groupObjectSelected.KODE == "002"){
          if(_form.validate()){
            selectedIndex += 1;
            isTaksasiUnitDone = true;
            _isValidate = false;
          }
          _providerTaksasiUnit.autoValidateCar;
          _isValidate = true;
        }
      }
    }
    return _isValidate;
  }

  void actionMoreButton(BuildContext context,String action) async {
    await _setLastStep(this._selectedIndex);
    List _data = await _dbHelper.selectLastStep();
    print("action more");
    var _providerFormMFotoChangeNotif = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
    var _providerFormMCustomerInfoChangeNotifier = Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false);
    var _providerOccupationChangeNotif = Provider.of<FormMOccupationChangeNotif>(context, listen: false);
    var _providerInfoAlamat = Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false);
    var _providerInfoKeluargaIbu = Provider.of<FormMInformasiKeluargaIBUChangeNotif>(context, listen: false);
    var _providerInfoKeluarga = Provider.of<FormMInfoKeluargaChangeNotif>(context, listen: false);
    var _providerInfoApp = Provider.of<InfoAppChangeNotifier>(context, listen: false);
    var _providerIncomeChangeNotif = Provider.of<FormMIncomeChangeNotifier>(context, listen: false);
    var _providerInfoObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
    var _providerInfoSales = Provider.of<InformationSalesmanChangeNotifier>(context, listen: false);
    var _providerKolateral = Provider.of<InformationCollateralChangeNotifier>(context, listen: false);
    var _providerKaroseri = Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false);
    var _providerWMP = Provider.of<InfoWMPChangeNotifier>(context, listen: false);
    var _providerCreditIncome = Provider.of<InfoCreditIncomeChangeNotifier>(context, listen: false);
    var _providerMajorInsurance = Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: true);
    var _providerAdditionalInsurance = Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: true);
    var _providerInfoCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false);
    var _providerCreditSubsidy = Provider.of<InfoCreditSubsidyChangeNotifier>(context, listen: false);
    var _providerInfoDocument = Provider.of<InfoDocumentChangeNotifier>(context, listen: false);
    var _providerTaksasiUnit = Provider.of<TaksasiUnitChangeNotifier>(context, listen: false);
    var _providerMarketingNotes = Provider.of<MarketingNotesChangeNotifier>(context, listen: false);
    var _providerGuarantor = Provider.of<FormMGuarantorChangeNotifier>(context, listen: false);
    var _providerSurvey = Provider.of<ResultSurveyChangeNotifier>(context, listen: false);
    var _providerListSurveyPhotoChangeNotifier = Provider.of<ListSurveyPhotoChangeNotifier>(context, listen: false);

    int _index = int.parse(_data[0]['idx']);
    print("INDEX SAVE TO DRAFT $_index");
    for(int i=0; i <= _index; i++){
      // if(action == LabelPopUpMenuButtonSaveToDraftBackToHome.SAVE_TO_DRAFT){
        if(i == 0){
          if(await _providerFormMFotoChangeNotif.deleteSQLite()){
            _providerFormMFotoChangeNotif.saveToSQLite(context);
          }
        }
        else if(i == 1){
          if(await _providerFormMCustomerInfoChangeNotifier.deleteSQLite()){
            _providerFormMCustomerInfoChangeNotifier.saveToSQLite();
          }
          if(await _providerInfoKeluargaIbu.deleteSQLite()){
            _providerInfoKeluargaIbu.saveToSQLite();
            _providerInfoKeluarga.saveToSQLite();
          }
          if(await _providerInfoAlamat.deleteSQLite("5")){
            _providerInfoAlamat.saveToSQLite("5");
          }
        }
        else if(i == 2){
          if(await _providerOccupationChangeNotif.deleteSQLite()){
            if(await _providerInfoAlamat.deleteSQLite("4"))
              _providerOccupationChangeNotif.saveToSQLite(context, "4");
          }
        }
        else if(i == 3){
          if(await _providerIncomeChangeNotif.deleteSQLite()){
            _providerIncomeChangeNotif.saveToSQLite(context);
          }
        }
        else if(i == 4){
          if(_providerGuarantor.radioValueIsWithGuarantor==0){
            if(await _providerGuarantor.deleteSQLite()){
              _providerGuarantor.saveToSQLite(context);
            }
          }
        }
        else if(i == 5){
          if(await _providerInfoApp.deleteSQLite()){
            _providerInfoApp.saveToSQLite(context);
          }
        }
        else if(i == 6){
          if(await _providerInfoObjectUnit.deleteSQLite()){
            _providerInfoObjectUnit.saveToSQLite(context);
          }
          if(await _providerInfoSales.deleteSQLite()){
            _providerInfoSales.saveToSQLite(context);
          }
          _providerKolateral.collateralTypeModel != null
              ?
          _providerKolateral.collateralTypeModel.id == "001"
              ?
          await _providerKolateral.deleteSQLiteCollaOto() ? _providerKolateral.saveToSQLiteOto(context) : print("")
              :
          await _providerKolateral.deleteSQLiteCollaProp() ? _providerKolateral.saveToSQLiteProperti(context, "6") : print("ada")
              :
          print("no colla");
          // _providerKolateral.collateralTypeModel.id == "001" ? _providerKolateral.saveToSQLiteOto() : _providerKolateral.saveToSQLiteProperti(context, "6");
        }
        else if(i == 7){
          if(await _providerKaroseri.deleteSQLite()){
            _providerKaroseri.saveToSQLite();
          }
        }
        else if(i == 8){
          _providerInfoCreditStructure.updateMS2ApplObjectInfStrukturKredit();
          if(await _providerInfoCreditStructure.deleteSQLite()){
            _providerInfoCreditStructure.saveToSQLiteInfStrukturKreditBiaya();
          }
          if(await _providerMajorInsurance.deleteSQLite()){
            _providerMajorInsurance.saveToSQLite();
          }
          if(await _providerAdditionalInsurance.deleteSQLite()){
            _providerAdditionalInsurance.saveToSQLite();
          }
          if(await _providerWMP.deleteSQLite()){
            _providerWMP.saveToSQLite();
          }
          _providerCreditIncome.updateMS2ApplObjectInfStrukturKreditIncome();
        }
        else if(i == 9){
          if(await _providerCreditSubsidy.deleteSQLIte()){
            _providerCreditSubsidy.saveToSQLite();
          }
        }
        else if(i == 10){
          if(await _providerInfoDocument.deleteSQLite()){
            _providerInfoDocument.saveToSQLite(context);
          }
        }
        else if(i == 11){
          if(await _providerTaksasiUnit.deleteSQLite()){
            _providerTaksasiUnit.saveToSQLite(context);
          }
        }
        else if(i==12){
          _providerMarketingNotes.saveToSQLite();
        }
        else if(i==13){
          _providerSurvey.insertDataSurveyAsset(context);
          _providerSurvey.insertDataSurveyDetail(context);
          _providerSurvey.insertDataMS2SvyAssgnmt(context);
          _providerListSurveyPhotoChangeNotifier.saveToSQLite(context);
        }
      }

    // Ditambahkan delay karena untuk mencegah context destroy sebelum semua fungsi dijalankan
    Future.delayed(const Duration(seconds: 5), () => Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
        Dashboard(flag: 0)), (Route<dynamic> route) => false));
      // else {
      //   isShowDialog(context);
      // }
    // }
  }

  void isShowDialog(BuildContext context, String action) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans"
            ),
            child: AlertDialog(
              title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Text("Simpan ke draft dan kembali ke home ?"),
              actions: <Widget>[
                FlatButton(
                    onPressed: (){
                      actionMoreButton(context, action);
                      // Navigator.push(context, MaterialPageRoute(builder: (context) => Dashboard()));
                      // Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
                      //     Dashboard(flag: 0)), (Route<dynamic> route) => false);
                    },
                    child: Text("Ya",
                        style: TextStyle(
                            color: primaryOrange,
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 1.25
                        )
                    )
                ),
                FlatButton(
                    onPressed: (){
                      Navigator.pop(context);
                    },
                    child: Text("Tidak",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 1.25
                        )
                    )
                )
              ],
            ),
          );
        }
    );
  }

  void dialogFailedSavePartial(BuildContext context, String message) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans"
            ),
            child: AlertDialog(
              title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Text(message),
              actions: <Widget>[
                FlatButton(
                    onPressed: (){
                      Navigator.pop(context);
                      notifyListeners();
                    },
                    child: Text("OK",
                        style: TextStyle(
                            color: primaryOrange,
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 1.25
                        )
                    )
                )
              ],
            ),
          );
        }
    );
  }

  // fungsi untuk set last step index di sqlite
  Future<void> _setLastStep(int index) async {
    print("check index parent last step $index");
    print(this._lastStepIndex);
    print(this._lastStepIndex <= index);
    if(this._lastStepIndex <= index){
      if(await _dbHelper.deleteSaveLastStep()){
        _dbHelper.saveLastStep(this._selectedIndex);
      }
    }
  }

  //setup data from sqlite
  void _setupDataFromSQLite(BuildContext context) async {
    debugPrint("_setupDataFromSQLite jalan");
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    loadData = true;
    List _data = await _dbHelper.selectLastStep();
    print("data masbro: $_data");
    this._lastStepIndex = int.parse(_data[0]['idx']);
    MenuCustomerDetailState _menuCustomerDetail = MenuCustomerDetailState();
    MenuObjectInformation _detailObject = MenuObjectInformation();
    MenuDetailLoanState _detailLoan = MenuDetailLoanState();
    ResultSurvey _survey = ResultSurvey();
    if(_data.isNotEmpty){
      var _providerFormMFotoChangeNotif = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
      var _providerOccupationChangeNotif = Provider.of<FormMOccupationChangeNotif>(context, listen: false);
      var _providerIncome = Provider.of<FormMIncomeChangeNotifier>(context,listen: false);

      await Provider.of<MarketingNotesChangeNotifier>(context,listen: false).setDataFromSQLite();
      //jangan dihapus
      // if(_preferences.getString("last_known_state") != "IDE" ){ // awalnya == "DKR"
      //   await _providerFormMFotoChangeNotif.getListOccupation(context,0);
      //   // await _providerFormMFotoChangeNotif.getBusinessActivities(context, 0);
      //   await _providerFormMCustomerInfoChangeNotifier.setDataSQLite();
      //   await _providerInfoAlamat.setDataFromSQLite("5");
      //   await _providerInfoKeluargaIbu.setDataSQLite();
      //   await _providerInfoKeluarga.setDataFromSQLite();
      //   await _providerOccupationChangeNotif.setDataFromSQLite(context, "4", null);
      //   await _providerIncome.setDataFromSQLite(context, null);
      //   await Provider.of<InfoAppChangeNotifier>(context, listen: true).addNumberOfUnitList(context,0,"DKR","PER");
      //   _detailObject.setNextState(context, 0, "DKR","PER");
      //   await Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false).setDataFromSQLite(context, 0, "DKR","PER");
      //   _detailLoan.setNextState(context, 0, "DKR","PER");
      //   await Provider.of<InfoCreditSubsidyChangeNotifier>(context, listen: true).setDataFromSQLite(context, 0, "DKR","PER");
      //   await Provider.of<InfoDocumentChangeNotifier>(context,listen: false).setDataFromSQLite(context, 0, "DKR","PER");
      //   await Provider.of<TaksasiUnitChangeNotifier>(context,listen: false).getListTaksasi(context, 0,Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).groupObjectSelected.KODE, "DKR");
      //   _survey.menuSetDataFromSQLite(context);
      //   loadData = false;
      // }
      if(_preferences.getString("last_known_state") != "IDE" || _preferences.getString("jenis_penawaran") != null){ //sementara
          // await _setLastStep(0);
          // await _providerFormMFotoChangeNotif.getListOccupation(context, 0, _preferences.getString("jenis_penawaran") != null);
          await _providerFormMFotoChangeNotif.setDataFromSQLite(context, 0);
          await _menuCustomerDetail.setNextState(context, null);
          await _providerOccupationChangeNotif.setDataFromSQLite(context, "4", null);
          await _providerIncome.setDataFromSQLite(context, null);
          await Provider.of<FormMGuarantorChangeNotifier>(context, listen: false).setDataFromSQLite(context, null, 1);
          await Provider.of<InfoAppChangeNotifier>(context, listen: false).addNumberOfUnitList(context,null);
          await _detailObject.setNextState(context, null);
          await Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false).setDataFromSQLite(context, null);
          await _detailLoan.setNextState(context, null);
          await Provider.of<InfoCreditSubsidyChangeNotifier>(context, listen: true).setDataFromSQLite(context, null);
          await Provider.of<InfoDocumentChangeNotifier>(context,listen: false).setDataFromSQLite(context, null);
          await Provider.of<TaksasiUnitChangeNotifier>(context,listen: false).getListTaksasi(context, null);
          await Provider.of<MarketingNotesChangeNotifier>(context,listen: false).setDataFromSQLite();
          await _survey.menuSetDataFromSQLite(context, _lastStepIndex);
          loadData = false;
      }
      else{
        print("selected index parent $_selectedIndex");
        this._lastStepIndex = int.parse(_data[0]['idx']);
        if(_preferences.getString("cust_type") == "PER"){
          // await _providerFormMFotoChangeNotif.getListOccupation(context,this._lastStepIndex, false);
          await _providerFormMFotoChangeNotif.setDataFromSQLite(context, this._lastStepIndex);
        }
        loadData = false;
      }
    }
    else{
      var _providerFormMFotoChangeNotif = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
      await _providerFormMFotoChangeNotif.getListOccupation(context);
      loadData = false;
    }
    loadData = false;
  }

  void submitDakor(BuildContext context, String orderId, String orderProductId) async {
    DateTime _timeStartValidate = DateTime.now();
    List _dataCL = await _dbHelper.selectMS2LME();
    List _dataResultSurvey = await _dbHelper.selectResultSurveyMS2Assignment();
    // Form IDE Individu
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
    var _providerInfoNasabah = Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false);
    var _providerInfoAlamatNasabah = Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false);
    var _providerIbu = Provider.of<FormMInformasiKeluargaIBUChangeNotif>(context, listen: false);
    var _providerKeluarga = Provider.of<FormMInfoKeluargaChangeNotif>(context, listen: false);
    var _providerOccupation = Provider.of<FormMOccupationChangeNotif>(context,listen: false);
    var _providerIncome = Provider.of<FormMIncomeChangeNotifier>(context, listen: false);
    var _providerGuarantor = Provider.of<FormMGuarantorChangeNotifier>(context, listen: false);

    // Form IDE Company
    var _providerRincian = Provider.of<FormMCompanyRincianChangeNotifier>(context, listen: false);
    // var _providerInfoAlamatCompany = Provider.of<FormMCompanyAlamatChangeNotifier>(context, listen: false);
    // var _providerIncomeCompany = Provider.of<FormMCompanyPendapatanChangeNotifier>(context, listen: false);
    var _providerManajemenPIC = Provider.of<FormMCompanyManajemenPICChangeNotifier>(context, listen: false);
    // var _providerAddressManajemenPIC = Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context,listen: false);
    // var _providerPemegangSaham = Provider.of<AddPemegangSahamPribadiChangeNotifier>(context, listen: false); // di comment karena pemegang saham
    // var _providerAddressSharedHolder = Provider.of<FormMCompanyPemegangSahamPribadiAlamatChangeNotifier>(context, listen: false);

    // Form App
    var _providerApp = Provider.of<InfoAppChangeNotifier>(context, listen: false);
    var _providerUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
    var _providerKolateral = Provider.of<InformationCollateralChangeNotifier>(context, listen: false);
    var _providerSales = Provider.of<InformationSalesmanChangeNotifier>(context, listen: false);
    // var _providerAddSales = Provider.of<AddSalesmanChangeNotifier>(context, listen: false);
    var _providerInsurance = Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false);
    var _providerAdditionalInsurance = Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false);
    var _providerKaroseri = Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false);
    var _providerCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false);
    var _providerCreditStructureType = Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context, listen: false);
    var _providerCreditIncome = Provider.of<InfoCreditIncomeChangeNotifier>(context, listen: false);
    var _providerWmp = Provider.of<InfoWMPChangeNotifier>(context,listen: false);
    var _providerSubsidy = Provider.of<InfoCreditSubsidyChangeNotifier>(context,listen: false);
    var _providerSurvey = Provider.of<ResultSurveyChangeNotifier>(context,listen:false);
    // var _providerSubsidyDetail = Provider.of<AddCreditSubsidyChangeNotifier>(context, listen: false);
    // var _providerCreditStructureTypeInstallment = Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false);
    // var _providerDocument = Provider.of<InfoDocumentChangeNotifier>(context,listen: false);

    // var occupation = Provider.of<FormMFotoChangeNotifier>(context, listen: false).occupationSelected.KODE;
    // var _providerInfoDocument = Provider.of<InfoDocumentChangeNotifier>(context, listen: false);

    SharedPreferences _preferences = await SharedPreferences.getInstance();
    // var _customerIncomes, _detailAddresses, _installmentDetails, _guarantorIndividuals, _guarantorCorporates, _orderKaroseris,
    //     _orderWmps, _orderSubsidies, _familyInfoID, _orderProductSaleses, _orderProductInsurances, _orderFees = [];

    await setValueCustomerIncome(context);
    // if(_preferences.getString("cust_type") == "PER") {
    //   if(_providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07") {
    //     if(_providerInfoNasabah.maritalStatusSelected.id != "01") {
    //       for(int i=0; i < 12; i++) {
    //         _customerIncomes.add(
    //             {
    //               "customerIncomeID": _preferences.getString("last_known_state") != "IDE" ? _preferences.getString("customerIncomeID") : "NEW",
    //               "incomeFrmlType": i == 0
    //                   ? "S" : i == 1
    //                   ? "+" : i == 2
    //                   ? "=" : i == 3
    //                   ? "-" : i == 4
    //                   ? "=" : i == 5
    //                   ? "-" : i == 6
    //                   ? "-" : i == 7
    //                   ? "=" : i == 8
    //                   ? "-" : i == 9
    //                   ? "=" : i == 10
    //                   ? "-" : i == 11
    //                   ? "="
    //                   : "N",
    //               "incomeType": i == 0
    //                   ? "001" : i == 1
    //                   ? "002" : i == 2
    //                   ? "003" : i == 3
    //                   ? "004" : i == 4
    //                   ? "005" : i == 5
    //                   ? "006" : i == 6
    //                   ? "007" : i == 7
    //                   ? "008" : i == 8
    //                   ? "009" : i == 9
    //                   ? "010" : i == 10
    //                   ? "011" : i == 11
    //                   ? "017"
    //                   : "013",
    //               "incomeValue": i == 0
    //                   ? double.parse(_providerIncome.controllerMonthlyIncome.text.replaceAll(",", "")) : i == 1
    //                   ? double.parse(_providerIncome.controllerOtherIncome.text.replaceAll(",", "")) : i == 2
    //                   ? double.parse(_providerIncome.controllerTotalIncome.text.replaceAll(",", "")) : i == 3
    //                   ? double.parse(_providerIncome.controllerCostOfGoodsSold.text.replaceAll(",", "")) : i == 4
    //                   ? double.parse(_providerIncome.controllerGrossProfit.text.replaceAll(",", "")) : i == 5
    //                   ? double.parse(_providerIncome.controllerOperatingCosts.text.replaceAll(",", "")) : i == 6
    //                   ? double.parse(_providerIncome.controllerOtherCosts.text.replaceAll(",", "")) : i == 7
    //                   ? double.parse(_providerIncome.controllerNetProfitBeforeTax.text.replaceAll(",", "")) : i == 8
    //                   ? double.parse(_providerIncome.controllerTax.text.replaceAll(",", "")) : i == 9
    //                   ? double.parse(_providerIncome.controllerNetProfitAfterTax.text.replaceAll(",", "")) : i == 10
    //                   ? double.parse(_providerIncome.controllerCostOfLiving.text.replaceAll(",", "")) : i == 11
    //                   ? double.parse(_providerIncome.controllerRestIncome.text.replaceAll(",", ""))
    //                   : double.parse(_providerIncome.controllerOtherInstallments.text.replaceAll(",", "")),
    //               "dataStatus": "ACTIVE",
    //               "creationalSpecification": {
    //                 "createdAt": formatDateValidateAndSubmit(DateTime.now()),
    //                 "createdBy": _preferences.getString("username"),
    //                 "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
    //                 "modifiedBy": _preferences.getString("username"),
    //               },
    //             }
    //         );
    //       }
    //     }
    //     else {
    //       for(int i=0; i < 13; i++) {
    //         _customerIncomes.add(
    //             {
    //               "customerIncomeID":_preferences.getString("last_known_state") != "IDE" ? _preferences.getString("customerIncomeID") : "NEW",
    //               "incomeFrmlType": i == 0
    //                   ? "S" : i == 1
    //                   ? "+" : i == 2
    //                   ? "=" : i == 3
    //                   ? "-" : i == 4
    //                   ? "=" : i == 5
    //                   ? "-" : i == 6
    //                   ? "-" : i == 7
    //                   ? "=" : i == 8
    //                   ? "-" : i == 9
    //                   ? "=" : i == 10
    //                   ? "-" : i == 11
    //                   ? "=" : i == 12
    //                   ? "N"
    //                   : "N",
    //               "incomeType": i == 0
    //                   ? "001" : i == 1
    //                   ? "002" : i == 2
    //                   ? "003" : i == 3
    //                   ? "004" : i == 4
    //                   ? "005" : i == 5
    //                   ? "006" : i == 6
    //                   ? "007" : i == 7
    //                   ? "008" : i == 8
    //                   ? "009" : i == 9
    //                   ? "010" : i == 10
    //                   ? "011" : i == 11
    //                   ? "017" : i == 12
    //                   ? "012"
    //                   : "013",
    //               "incomeValue": i == 0
    //                   ? double.parse(_providerIncome.controllerMonthlyIncome.text.replaceAll(",", "")) : i == 1
    //                   ? double.parse(_providerIncome.controllerOtherIncome.text.replaceAll(",", "")) : i == 2
    //                   ? double.parse(_providerIncome.controllerTotalIncome.text.replaceAll(",", "")) : i == 3
    //                   ? double.parse(_providerIncome.controllerCostOfGoodsSold.text.replaceAll(",", "")) : i == 4
    //                   ? double.parse(_providerIncome.controllerGrossProfit.text.replaceAll(",", "")) : i == 5
    //                   ? double.parse(_providerIncome.controllerOperatingCosts.text.replaceAll(",", "")) : i == 6
    //                   ? double.parse(_providerIncome.controllerOtherCosts.text.replaceAll(",", "")) : i == 7
    //                   ? double.parse(_providerIncome.controllerNetProfitBeforeTax.text.replaceAll(",", "")) : i == 8
    //                   ? double.parse(_providerIncome.controllerTax.text.replaceAll(",", "")) : i == 9
    //                   ? double.parse(_providerIncome.controllerNetProfitAfterTax.text.replaceAll(",", "")) : i == 10
    //                   ? double.parse(_providerIncome.controllerCostOfLiving.text.replaceAll(",", "")) : i == 11
    //                   ? double.parse(_providerIncome.controllerRestIncome.text.replaceAll(",", "")) : i == 12
    //                   ? double.parse(_providerIncome.controllerSpouseIncome.text.replaceAll(",", ""))
    //                   : double.parse(_providerIncome.controllerOtherInstallments.text.replaceAll(",", "")),
    //               // ? int.parse(_providerIncome.controllerMonthlyIncome.text) : i == 1
    //               // ? int.parse(_providerIncome.controllerOtherIncome.text) : i == 2
    //               // ? int.parse(_providerIncome.controllerTotalIncome.text) : i == 3
    //               // ? int.parse(_providerIncome.controllerCostOfGoodsSold.text) : i == 4
    //               // ? int.parse(_providerIncome.controllerGrossProfit.text) : i == 5
    //               // ? int.parse(_providerIncome.controllerOperatingCosts.text) : i == 6
    //               // ? int.parse(_providerIncome.controllerOtherCosts.text) : i == 7
    //               // ? int.parse(_providerIncome.controllerNetProfitBeforeTax.text) : i == 8
    //               // ? int.parse(_providerIncome.controllerTax.text) : i == 9
    //               // ? int.parse(_providerIncome.controllerNetProfitAfterTax.text) : i == 10
    //               // ? int.parse(_providerIncome.controllerCostOfLiving.text) : i == 11
    //               // ? int.parse(_providerIncome.controllerRestIncome.text) : i == 12
    //               // ? int.parse(_providerIncome.controllerSpouseIncome.text)
    //               // : int.parse(_providerIncome.controllerOtherInstallments.text),
    //               "dataStatus": "ACTIVE",
    //               "creationalSpecification": {
    //                 "createdAt": formatDateValidateAndSubmit(DateTime.now()),
    //                 "createdBy": _preferences.getString("username"),
    //                 "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
    //                 "modifiedBy": _preferences.getString("username"),
    //               },
    //             }
    //         );
    //       }
    //     }
    //   }
    //   else {
    //     if(_providerInfoNasabah.maritalStatusSelected.id != "01") {
    //       for(int i=0; i < 5; i++) {
    //         _customerIncomes.add(
    //             {
    //               "customerIncomeID": _preferences.getString("last_known_state") != "IDE" ? _preferences.getString("customerIncomeID") : "NEW",
    //               "incomeFrmlType": i == 0
    //                   ? "S" : i == 1
    //                   ? "+" : i == 2
    //                   ? "=" : i == 3
    //                   ? "-" : i == 4
    //                   ? "="
    //                   : "N",
    //               "incomeType": i == 0
    //                   ? "016" : i == 1
    //                   ? "002" : i == 2
    //                   ? "003" : i == 3
    //                   ? "011" : i == 4
    //                   ? "017"
    //                   : "013",
    //               "incomeValue": i == 0
    //                   ? double.parse(_providerIncome.controllerMonthlyIncome.text.replaceAll(",", "")) : i == 1
    //                   ? double.parse(_providerIncome.controllerOtherIncome.text.replaceAll(",", "")) : i == 2
    //                   ? double.parse(_providerIncome.controllerTotalIncome.text.replaceAll(",", "")) : i == 3
    //                   ? double.parse(_providerIncome.controllerCostOfLiving.text.replaceAll(",", "")) : i == 4
    //                   ? double.parse(_providerIncome.controllerRestIncome.text.replaceAll(",", ""))
    //                   : double.parse(_providerIncome.controllerOtherInstallments.text.replaceAll(",", "")),
    //               // ? int.parse(_providerIncome.controllerIncome.text) : i == 1
    //               // ? int.parse(_providerIncome.controllerOtherIncome.text) : i == 2
    //               // ? int.parse(_providerIncome.controllerTotalIncome.text) : i == 3
    //               // ? int.parse(_providerIncome.controllerCostOfLiving.text) : i == 4
    //               // ? int.parse(_providerIncome.controllerRestIncome.text)
    //               // : int.parse(_providerIncome.controllerOtherInstallments.text),
    //               "dataStatus": "ACTIVE",
    //               "creationalSpecification": {
    //                 "createdAt": formatDateValidateAndSubmit(DateTime.now()),
    //                 "createdBy": _preferences.getString("username"),
    //                 "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
    //                 "modifiedBy": _preferences.getString("username"),
    //               },
    //             }
    //         );
    //       }
    //     }
    //     else {
    //       for(int i=0; i < 6; i++) {
    //         _customerIncomes.add(
    //             {
    //               "customerIncomeID": _preferences.getString("last_known_state") != "IDE" ? _preferences.getString("customerIncomeID") : "NEW",
    //               "incomeFrmlType": i == 0
    //                   ? "S" : i == 1
    //                   ? "+" : i == 2
    //                   ? "+" : i == 3
    //                   ? "=" : i == 4
    //                   ? "-" : i == 5
    //                   ? "="
    //                   : "N",
    //               "incomeType": i == 0
    //                   ? "016" : i == 1
    //                   ? "012" : i == 2
    //                   ? "002" : i == 3
    //                   ? "003" : i == 4
    //                   ? "011" : i == 5
    //                   ? "017"
    //                   : "013",
    //               "incomeValue": i == 0
    //                   ? _providerIncome.controllerMonthlyIncome.text.replaceAll(",", "") : i == 1
    //                   ? _providerIncome.controllerSpouseIncome.text.replaceAll(",", "") : i == 2
    //                   ? _providerIncome.controllerOtherIncome.text.replaceAll(",", "") : i == 3
    //                   ? _providerIncome.controllerTotalIncome.text.replaceAll(",", "") : i == 4
    //                   ? _providerIncome.controllerCostOfLiving.text.replaceAll(",", "") : i == 5
    //                   ? _providerIncome.controllerRestIncome.text.replaceAll(",", "")
    //                   : _providerIncome.controllerOtherInstallments.text.replaceAll(",", ""),
    //               // ? int.parse(_providerIncome.controllerIncome.text) : i == 1
    //               // ? int.parse(_providerIncome.controllerSpouseIncome.text) : i == 2
    //               // ? int.parse(_providerIncome.controllerOtherIncome.text) : i == 3
    //               // ? int.parse(_providerIncome.controllerTotalIncome.text) : i == 4
    //               // ? int.parse(_providerIncome.controllerCostOfLiving.text) : i == 5
    //               // ? int.parse(_providerIncome.controllerRestIncome.text)
    //               // : int.parse(_providerIncome.controllerOtherInstallments.text),
    //               "dataStatus": "ACTIVE",
    //               "creationalSpecification": {
    //                 "createdAt": formatDateValidateAndSubmit(DateTime.now()),
    //                 "createdBy": _preferences.getString("username"),
    //                 "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
    //                 "modifiedBy": _preferences.getString("username"),
    //               },
    //             }
    //         );
    //       }
    //     }
    //   }
    // }
    // else {
    //   for(int i=0; i < 10; i++) {
    //     _customerIncomes.add(
    //         {
    //           "customerIncomeID": _preferences.getString("last_known_state") != "IDE" ? _preferences.getString("customerIncomeID") : "NEW",
    //           "incomeFrmlType": i == 0
    //               ? "S" : i == 1
    //               ? "+" : i == 2
    //               ? "=" : i == 3
    //               ? "-" : i == 4
    //               ? "=" : i == 5
    //               ? "-" : i == 6
    //               ? "-" : i == 7
    //               ? "=" : i == 8
    //               ? "-" : i == 9
    //               ? "="
    //               : "N",
    //           "incomeType": i == 0
    //               ? "016" : i == 1
    //               ? "002" : i == 2
    //               ? "003" : i == 3
    //               ? "004" : i == 4
    //               ? "005" : i == 5
    //               ? "006" : i == 6
    //               ? "007" : i == 7
    //               ? "008" : i == 8
    //               ? "009" : i == 9
    //               ? "010"
    //               : "013",
    //           "incomeValue": i == 0
    //               ? double.parse(_providerIncomeCompany.controllerMonthlyIncome.text.replaceAll(",", "")) : i == 1
    //               ? double.parse(_providerIncomeCompany.controllerOtherIncome.text.replaceAll(",", "")) : i == 2
    //               ? double.parse(_providerIncomeCompany.controllerTotalIncome.text.replaceAll(",", "")) : i == 3
    //               ? double.parse(_providerIncomeCompany.controllerCostOfRevenue.text.replaceAll(",", "")) : i == 4
    //               ? double.parse(_providerIncomeCompany.controllerGrossProfit.text.replaceAll(",", "")) : i == 5
    //               ? double.parse(_providerIncomeCompany.controllerOperatingCosts.text.replaceAll(",", "")) : i == 6
    //               ? double.parse(_providerIncomeCompany.controllerOtherCosts.text.replaceAll(",", "")) : i == 7
    //               ? double.parse(_providerIncomeCompany.controllerNetProfitBeforeTax.text.replaceAll(",", "")) : i == 8
    //               ? double.parse(_providerIncomeCompany.controllerTax.text.replaceAll(",", "")) : i == 9
    //               ? double.parse(_providerIncomeCompany.controllerNetProfitAfterTax.text.replaceAll(",", ""))
    //               : double.parse(_providerIncomeCompany.controllerOtherInstallments.text.replaceAll(",", "")),
    //           // ? int.parse(_providerIncomeCompany.controllerMonthlyIncome.text) : i == 1
    //           // ? int.parse(_providerIncomeCompany.controllerOtherIncome.text) : i == 2
    //           // ? int.parse(_providerIncomeCompany.controllerTotalIncome.text) : i == 3
    //           // ? int.parse(_providerIncomeCompany.controllerCostOfRevenue.text) : i == 4
    //           // ? int.parse(_providerIncomeCompany.controllerGrossProfit.text) : i == 5
    //           // ? int.parse(_providerIncomeCompany.controllerOperatingCosts.text) : i == 6
    //           // ? int.parse(_providerIncomeCompany.controllerOtherCosts.text) : i == 7
    //           // ? int.parse(_providerIncomeCompany.controllerNetProfitBeforeTax.text) : i == 8
    //           // ? int.parse(_providerIncomeCompany.controllerTax.text) : i == 9
    //           // ? int.parse(_providerIncomeCompany.controllerNetProfitAfterTax.text)
    //           // : int.parse(_providerIncomeCompany.controllerOtherInstallments.text),
    //           "dataStatus": "ACTIVE",
    //           "creationalSpecification": {
    //             "createdAt": formatDateValidateAndSubmit(DateTime.now()),
    //             "createdBy": _preferences.getString("username"),
    //             "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
    //             "modifiedBy": _preferences.getString("username"),
    //           },
    //         }
    //     );
    //   }
    // }

    String _npwpType = "";
    // if(_preferences.getString("cust_type") == "PER"){
      if(_providerInfoNasabah.jenisNPWPSelected != null) {
        _npwpType = _providerInfoNasabah.jenisNPWPSelected != null ? _providerInfoNasabah.jenisNPWPSelected.id : "2";
      } else {
        _npwpType = "2";
      }
    // }
    // else{
    //   if(_providerRincian.typeNPWPSelected != null) {
    //     _npwpType = _providerRincian.typeNPWPSelected != null ? _providerRincian.typeNPWPSelected.id : "2";
    //   } else {
    //     _npwpType = "2";
    //   }
    // }

    String _npwpAddress = "";
    // if(_preferences.getString("cust_type") == "PER"){
      if(_providerInfoNasabah.radioValueIsHaveNPWP == 1){
        _npwpAddress = _providerInfoNasabah.controllerAlamatSesuaiNPWP.text.isNotEmpty ? _providerInfoNasabah.controllerAlamatSesuaiNPWP.text : "INDONESIA";
      } else {
        // _npwpAddress = "INDONESIA";
        for(int i=0; i < _providerInfoAlamatNasabah.listAlamatKorespondensi.length; i++) {
          if(_providerInfoAlamatNasabah.listAlamatKorespondensi[i].jenisAlamatModel.KODE == "03") {
            _npwpAddress = _providerInfoAlamatNasabah.listAlamatKorespondensi[i].address.toString();
          }
        }
      }
    // }
    // else{
    //   if(_providerRincian.radioValueIsHaveNPWP == 1){
    //     _npwpAddress = _providerRincian.controllerNPWPAddress.text.isNotEmpty ? "INDONESIA" : _providerRincian.controllerNPWPAddress.text;
    //   } else {
    //     _npwpAddress = "INDONESIA";
    //   }
    // }

    String _npwpFullName = "";
    // if(_preferences.getString("cust_type") == "PER"){
      if(_providerInfoNasabah.radioValueIsHaveNPWP == 1){
        _npwpFullName = _providerInfoNasabah.controllerNamaSesuaiNPWP.text;
      }
      else{
        // _npwpFullName = "NAMA IDENTITAS";
        _npwpFullName = _providerInfoNasabah.controllerNamaLengkapSesuaiIdentitas.text;
      }
    // }
    // else{
    //   if(_providerRincian.radioValueIsHaveNPWP == 1){
    //     _npwpFullName = _providerRincian.controllerFullNameNPWP.text;
    //   }
    // }

    String _npwpNumber = "";
    // if(_preferences.getString("cust_type") == "PER"){
      if(_providerInfoNasabah.radioValueIsHaveNPWP == 1){
        _npwpNumber = _providerInfoNasabah.controllerNoNPWP.text;
      }
      else{
        _npwpNumber = "000000000000000";
      }
    // }
    // else{
    //   if(_providerRincian.radioValueIsHaveNPWP == 1){
    //     _npwpNumber = _providerRincian.controllerNPWP.text;
    //   }
    //   else{
    //     _npwpNumber = "000000000000000";
    //   }
    // }

    var _detailAddressesInfoNasabah = [];
    for (int i = 0; i <_providerInfoAlamatNasabah.listAlamatKorespondensi.length; i++) {
      _detailAddressesInfoNasabah.add({
        "addressID": _preferences.getString("last_known_state") != "IDE" ? _providerInfoAlamatNasabah.listAlamatKorespondensi[i].addressID != "NEW" ? _providerInfoAlamatNasabah.listAlamatKorespondensi[i].addressID : "NEW" : "NEW",
        "addressSpecification": {
          "koresponden": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].isCorrespondence ? "1" : "0",
          "matrixAddr": "5",
          "address": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].address,
          "rt": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].rt,
          "rw": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].rw,
          "provinsiID": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].kelurahanModel.PROV_ID,
          "kabkotID": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].kelurahanModel.KABKOT_ID,
          "kecamatanID": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].kelurahanModel.KEC_ID,
          "kelurahanID": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].kelurahanModel.KEL_ID,
          "zipcode": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].kelurahanModel.ZIPCODE
        },
        "contactSpecification": {
          "telephoneArea1": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].areaCode,
          "telephone1": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].phone,
          "telephoneArea2": null,
          "telephone2": null,
          "faxArea": null,
          "fax": null,
          "handphone": "",
          "email": ""
        },
        "addressType": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].jenisAlamatModel.KODE,
        "creationalSpecification": {
          "createdAt": formatDateValidateAndSubmit(DateTime.now()),
          "createdBy": _preferences.getString("username"),
          "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
          "modifiedBy": _preferences.getString("username"),
        },
        "status": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].active == 0 ? "ACTIVE" : "INACTIVE",
        "foreignBusinessID": _preferences.getString("last_known_state") != "IDE" ? _providerInfoAlamatNasabah.listAlamatKorespondensi[i].foreignBusinessID != "NEW" ? _providerInfoAlamatNasabah.listAlamatKorespondensi[i].foreignBusinessID : "CAU001" : "CAU001",
      });
    }
    for(int i=0; i < _providerOccupation.listOccupationAddress.length; i++){
      _detailAddressesInfoNasabah.add({
        "addressID": _preferences.getString("last_known_state") != "IDE" ? _providerOccupation.listOccupationAddress[i].addressID != "NEW" ? _providerOccupation.listOccupationAddress[i].addressID : "NEW" : "NEW",
        "addressSpecification": {
          "koresponden": _providerOccupation.listOccupationAddress[i].isCorrespondence ? "1" : "0",
          "matrixAddr": "4",
          "address": _providerOccupation.listOccupationAddress[i].address,
          "rt": _providerOccupation.listOccupationAddress[i].rt,
          "rw": _providerOccupation.listOccupationAddress[i].rw,
          "provinsiID": _providerOccupation.listOccupationAddress[i].kelurahanModel.PROV_ID,
          "kabkotID": _providerOccupation.listOccupationAddress[i].kelurahanModel.KABKOT_ID,
          "kecamatanID": _providerOccupation.listOccupationAddress[i].kelurahanModel.KEC_ID,
          "kelurahanID": _providerOccupation.listOccupationAddress[i].kelurahanModel.KEL_ID,
          "zipcode": _providerOccupation.listOccupationAddress[i].kelurahanModel.ZIPCODE
        },
        "contactSpecification": {
          "telephoneArea1": _providerOccupation.listOccupationAddress[i].areaCode,
          "telephone1": _providerOccupation.listOccupationAddress[i].phone,
          "telephoneArea2": null,
          "telephone2": null,
          "faxArea": null,
          "fax": null,
          "handphone": "",
          "email": ""
        },
        "addressType": _providerOccupation.listOccupationAddress[i].jenisAlamatModel.KODE,
        "creationalSpecification": {
          "createdAt": formatDateValidateAndSubmit(DateTime.now()),
          "createdBy": _preferences.getString("username"),
          "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
          "modifiedBy": _preferences.getString("username"),
        },
        "status": _providerOccupation.listOccupationAddress[i].active == 0 ? "ACTIVE" : "INACTIVE",
        "foreignBusinessID": _preferences.getString("last_known_state") != "IDE" ? _providerOccupation.listOccupationAddress[i].addressID != "NEW" ? _providerOccupation.listOccupationAddress[i].addressID : "IP001" : "IP001",
      });
    }

    var _familyInfoID = [];
    _familyInfoID.add(
      {
        "familyInfoID": _preferences.getString("last_known_state") != "IDE" ? _providerIbu.familyInfoID : "NEW",
        "relationshipStatus": "05",
        "familyIdentity": {
          "identityType": "",
          "identityNumber": "",
          "identityName": _providerIbu.controllerNamaIdentitas.text,
          "fullName": _providerIbu.controllerNamaLengkapdentitas.text,
          "alias": null,
          "title": null,
          "dateOfBirth": null,
          "placeOfBirth": "",
          "placeOfBirthKabKota": "",
          "gender": "02",
          "identityActiveStart": null,
          "identityActiveEnd": null,
          "religion": null,
          "occupationID": "0",
          "positionID": null
        },
        "creationalSpecification": {
          "createdAt": formatDateValidateAndSubmit(DateTime.now()),
          "createdBy": _preferences.getString("username"),
          "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
          "modifiedBy": _preferences.getString("username"),
        },
        "contactSpecification": {
          "telephoneArea1": _providerIbu.controllerKodeArea.text,
          "telephone1": _providerIbu.controllerTlpn.text,
          "telephoneArea2": null,
          "telephone2": null,
          "faxArea": null,
          "fax": null,
          "handphone": "",
          "email": null
        },
        "dedupScore": 0,
        "status": "ACTIVE"
      },
    );
    if (_providerKeluarga.listFormInfoKel.isNotEmpty) {
      for (int i = 0; i < _providerKeluarga.listFormInfoKel.length; i++) {
        if (_preferences.getString("cust_type") == "PER") {
          _familyInfoID.add({
            "familyInfoID": _preferences.getString("last_known_state") != "IDE" ? _providerKeluarga.listFormInfoKel[i].familyInfoID : "NEW",
            "relationshipStatus": _providerKeluarga.listFormInfoKel[i]
                .relationshipStatusModel == null ? "" : _providerKeluarga
                .listFormInfoKel[i].relationshipStatusModel.PARA_FAMILY_TYPE_ID,
            "familyIdentity": {
              "identityType": _providerKeluarga.listFormInfoKel[i]
                  .identityModel == null ? "" : _providerKeluarga
                  .listFormInfoKel[i].identityModel.id,
              "identityNumber": _providerKeluarga.listFormInfoKel[i].noIdentitas,
              "identityName": _providerKeluarga.listFormInfoKel[i]
                  .namaLengkapSesuaiIdentitas,
              "fullName": _providerKeluarga.listFormInfoKel[i].namaLengkap,
              "alias": null,
              "title": null,
              "dateOfBirth": _providerKeluarga.listFormInfoKel[i].birthDate == null
                  ? "01-01-1900 00:00:00"
                  : formatDateValidateAndSubmit(DateTime.parse(_providerKeluarga.listFormInfoKel[i].birthDate)),//"${_providerKeluarga.listFormInfoKel[i].birthDate} 00:00:00",
              "placeOfBirth": _providerKeluarga.listFormInfoKel[i]
                  .tmptLahirSesuaiIdentitas,
              "placeOfBirthKabKota": _providerKeluarga.listFormInfoKel[i]
                  .birthPlaceModel == null ? "" : _providerKeluarga
                  .listFormInfoKel[i].birthPlaceModel.KABKOT_ID,
              "gender": _providerKeluarga.listFormInfoKel[i].gender,
              "identityActiveStart": null,
              "identityActiveEnd": null,
              "religion": null,
              "occupationID": "0",
              "positionID": null
            },
            "creationalSpecification": {
              "createdAt": formatDateValidateAndSubmit(DateTime.now()),
              "createdBy": _preferences.getString("username"),
              "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
              "modifiedBy": _preferences.getString("username")
            },
            "contactSpecification": {
              "telephoneArea1": _providerKeluarga.listFormInfoKel[i].kodeArea,
              "telephone1": _providerKeluarga.listFormInfoKel[i].noTlpn,
              "telephoneArea2": null,
              "telephone2": null,
              "faxArea": null,
              "fax": null,
              "handphone": _providerKeluarga.listFormInfoKel[i].noHp != "" ? "08${_providerKeluarga.listFormInfoKel[i].noHp}" : "",
              "email": null
            },
            "dedupScore": 0,
            "status": "ACTIVE"
          });
        }
      }
    }

    var _guarantorIndividuals = [];
    if(_providerGuarantor.listGuarantorIndividual.isNotEmpty) {
      for(int i=0; i < _providerGuarantor.listGuarantorIndividual.length; i++) {
        List _detailAddressGuarantorIndividu = [];
        String _foreignBKGuarantorIndividu = "IKPG";
        if(_providerGuarantor.listGuarantorIndividual.length < 10) {
          _foreignBKGuarantorIndividu = "IKPG00" + (i+1).toString();
        } else if(_providerGuarantor.listGuarantorIndividual.length < 100) {
          _foreignBKGuarantorIndividu = "IKPG0" + (i+1).toString();
        } else if(_providerGuarantor.listGuarantorIndividual.length < 1000) {
          _foreignBKGuarantorIndividu = "IKPG" + (i+1).toString();
        }
        if(_providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel.isNotEmpty){
          for(int j=0; j < _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel.length; j++){
            _detailAddressGuarantorIndividu.add({
              "addressID": _preferences.getString("last_known_state") != "IDE"
                  ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].addressID != "NEW"
                  ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].addressID : "NEW" : "NEW",
              "addressSpecification": {
                "koresponden": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isCorrespondence ? "1" : "0",
                "matrixAddr": _preferences.getString("cust_type") == "PER" ? "1" : "7",
                "address": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].address,
                "rt": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].rt,
                "rw": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].rw,
                "provinsiID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.PROV_ID,
                "kabkotID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_ID,
                "kecamatanID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KEC_ID,
                "kelurahanID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KEL_ID,
                "zipcode": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.ZIPCODE
              },
              "contactSpecification": {
                "telephoneArea1": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].areaCode,
                "telephone1": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].phone,
                "telephoneArea2": null,
                "telephone2": null,
                "faxArea": null,
                "fax": null,
                "handphone": "08${_providerGuarantor.listGuarantorIndividual[i].cellPhoneNumber}",
                "email": ""
              },
              "addressType": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].jenisAlamatModel.KODE,
              "creationalSpecification": {
                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                "createdBy": _preferences.getString("username"),
                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                "modifiedBy": _preferences.getString("username"),
              },
              "status": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].active == 0 ? "ACTIVE" : "INACTIVE",
              "foreignBusinessID": _preferences.getString("last_known_state") != "IDE"
                  ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].foreignBusinessID != "NEW"
                  ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].foreignBusinessID : _foreignBKGuarantorIndividu : _foreignBKGuarantorIndividu,
            });
            _detailAddressesInfoNasabah.add({
              "addressID": _preferences.getString("last_known_state") != "IDE"
                  ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].addressID != "NEW"
                  ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].addressID : "NEW" : "NEW",
              "addressSpecification": {
                "koresponden": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isCorrespondence ? "1" : "0",
                "matrixAddr": _preferences.getString("cust_type") == "PER" ? "1" : "7",
                "address": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].address,
                "rt": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].rt,
                "rw": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].rw,
                "provinsiID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.PROV_ID,
                "kabkotID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_ID,
                "kecamatanID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KEC_ID,
                "kelurahanID": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KEL_ID,
                "zipcode": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.ZIPCODE
              },
              "contactSpecification": {
                "telephoneArea1": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].areaCode,
                "telephone1": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].phone,
                "telephoneArea2": null,
                "telephone2": null,
                "faxArea": null,
                "fax": null,
                "handphone": "",
                "email": ""
              },
              "addressType": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].jenisAlamatModel.KODE,
              "creationalSpecification": {
                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                "createdBy": _preferences.getString("username"),
                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                "modifiedBy": _preferences.getString("username"),
              },
              "status": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].active == 0 ? "ACTIVE" : "INACTIVE",
              "foreignBusinessID": _preferences.getString("last_known_state") != "IDE"
                  ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].foreignBusinessID != "NEW"
                  ? _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].foreignBusinessID : _foreignBKGuarantorIndividu : _foreignBKGuarantorIndividu,
            });
          }
        }
        _guarantorIndividuals.add(
            {
              "guarantorIndividualID": _preferences.getString("last_known_state") != "IDE"
                  ? _providerGuarantor.listGuarantorIndividual[i].guarantorIndividualID != null
                  ? _providerGuarantor.listGuarantorIndividual[i].guarantorIndividualID : _foreignBKGuarantorIndividu : _foreignBKGuarantorIndividu, // NEW,
              "relationshipStatus":_providerGuarantor.listGuarantorIndividual[i].relationshipStatusModel != null ?
              _providerGuarantor.listGuarantorIndividual[i].relationshipStatusModel.PARA_FAMILY_TYPE_ID : "",
              "dedupScore": 0.0,
              "guarantorIdentity": {
                "identityType": _providerGuarantor.listGuarantorIndividual[i].identityModel != null ?
                _providerGuarantor.listGuarantorIndividual[i].identityModel.id : "",
                "identityNumber": _providerGuarantor.listGuarantorIndividual[i].identityNumber,
                "identityName": _providerGuarantor.listGuarantorIndividual[i].fullNameIdentity,
                "fullName": _providerGuarantor.listGuarantorIndividual[i].fullName,
                "alias": "",
                "title": "",
                "dateOfBirth": _providerGuarantor.listGuarantorIndividual[i].birthDate != "" ? formatDateValidateAndSubmit(DateTime.parse(_providerGuarantor.listGuarantorIndividual[i].birthDate)) : "01-01-1900 00:00:00",
                "placeOfBirth": _providerGuarantor.listGuarantorIndividual[i].birthPlaceIdentity1 != "" ? _providerGuarantor.listGuarantorIndividual[i].birthPlaceIdentity1 : "",
                "placeOfBirthKabKota": _providerGuarantor.listGuarantorIndividual[i].birthPlaceIdentity2 != null ? _providerGuarantor.listGuarantorIndividual[i].birthPlaceIdentity2.KABKOT_ID : "",
                "gender": _providerGuarantor.listGuarantorIndividual[i].gender,
                "identityActiveStart": _preferences.getString("cust_type") == "PER" ? _providerInfoNasabah.controllerTglIdentitas.text != "" ? formatDateValidateAndSubmit(_providerInfoNasabah.initialDateForTglIdentitas) : formatDateValidateAndSubmit(DateTime.now()) : formatDateValidateAndSubmit(DateTime.now()),
                "identityActiveEnd": _preferences.getString("cust_type") == "PER" ? _providerInfoNasabah.valueCheckBox ? "31-12-2999 00:00:00" : _providerInfoNasabah.controllerIdentitasBerlakuSampai.text != "" ? formatDateValidateAndSubmit(_providerInfoNasabah.initialDateForTglIdentitas) : "31-12-2999 00:00:00" : "31-12-2999 00:00:00", // berdasarkan issue jira 1382 = diganti jadi 31-12-2999 00:00:00
                "isLifetime": null,
                "religion": null,
                "occupationID": null,
                "positionID": null,
                "maritalStatusID": null
              },
              "guarantorContact": {
                "telephoneArea1": null,
                "telephone1": null,
                "telephoneArea2": null,
                "telephone2": null,
                "faxArea": null,
                "fax": null,
                "handphone": "08${_providerGuarantor.listGuarantorIndividual[i].cellPhoneNumber}",
                "email": null,
                "noWa": null,
                "noWa2": null,
                "noWa3": null
              },
              "guarantorIndividualAddresses": _detailAddressGuarantorIndividu,
              "creationalSpecification": {
                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                "createdBy": _preferences.getString("username"),
                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                "modifiedBy": _preferences.getString("username"),
              },
              "status": "ACTIVE"
            }
        );
      }
    }

    var _guarantorCorporates = [];
    if(_providerGuarantor.listGuarantorCompany.isNotEmpty) {
      for(int i=0; i < _providerGuarantor.listGuarantorCompany.length; i++) {
        List _detailAddressGuarantorCompany = [];
        String _foreignBKGuarantorCorporate = "IKGK";
        if(_providerGuarantor.listGuarantorCompany.length < 10) {
          _foreignBKGuarantorCorporate = "IKGK00" + (i+1).toString();
        } else if(_providerGuarantor.listGuarantorCompany.length < 100) {
          _foreignBKGuarantorCorporate = "IKGK0" + (i+1).toString();
        } else if(_providerGuarantor.listGuarantorCompany.length < 1000) {
          _foreignBKGuarantorCorporate = "IKGK" + (i+1).toString();
        }
        if(_providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel.isNotEmpty){
          for(int j=0; j < _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel.length; j++){
            _detailAddressGuarantorCompany.add({
              "addressID": _preferences.getString("last_known_state") != "IDE"
                  ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].addressID != "NEW"
                  ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].addressID : "NEW" : "NEW",
              "addressSpecification": {
                "koresponden": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isCorrespondence ? "1" : "0",
                "matrixAddr": _preferences.getString("cust_type") == "PER" ? "2" : "8",
                "address": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].address,
                "rt": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].rt,
                "rw": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].rw,
                "provinsiID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.PROV_ID,
                "kabkotID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_ID,
                "kecamatanID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEC_ID,
                "kelurahanID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEL_ID,
                "zipcode": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.ZIPCODE
              },
              "contactSpecification": {
                "telephoneArea1": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phoneArea1,
                "telephone1": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phone1,
                "telephoneArea2": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phoneArea2,
                "telephone2": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phone2,
                "faxArea": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].faxArea,
                "fax": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].fax,
                "handphone": "",
                "email": ""
              },
              "addressType": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].jenisAlamatModel.KODE,
              "creationalSpecification": {
                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                "createdBy": _preferences.getString("username"),
                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                "modifiedBy": _preferences.getString("username"),
              },
              "status": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].active == 0 ? "ACTIVE" : "INACTIVE",
              "foreignBusinessID": _preferences.getString("last_known_state") != "IDE"
                  ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].foreignBusinessID != "NEW"
                  ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].foreignBusinessID : _foreignBKGuarantorCorporate : _foreignBKGuarantorCorporate,
            });
            _detailAddressesInfoNasabah.add({
              "addressID": _preferences.getString("last_known_state") != "IDE"
                  ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].addressID != "NEW"
                  ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].addressID : "NEW" : "NEW",
              "addressSpecification": {
                "koresponden": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isCorrespondence ? "1" : "0",
                "matrixAddr": _preferences.getString("cust_type") == "PER" ? "2" : "8",
                "address": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].address,
                "rt": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].rt,
                "rw": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].rw,
                "provinsiID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.PROV_ID,
                "kabkotID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_ID,
                "kecamatanID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEC_ID,
                "kelurahanID": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEL_ID,
                "zipcode": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.ZIPCODE
              },
              "contactSpecification": {
                "telephoneArea1": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phoneArea1,
                "telephone1": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phone1,
                "telephoneArea2": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phoneArea2,
                "telephone2": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].phone2,
                "faxArea": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].faxArea,
                "fax": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].fax,
                "handphone": "",
                "email": ""
              },
              "addressType": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].jenisAlamatModel.KODE,
              "creationalSpecification": {
                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                "createdBy": _preferences.getString("username"),
                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                "modifiedBy": _preferences.getString("username"),
              },
              "status": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].active == 0 ? "ACTIVE" : "INACTIVE",
              "foreignBusinessID": _preferences.getString("last_known_state") != "IDE"
                  ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].foreignBusinessID != "NEW"
                  ? _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].foreignBusinessID : _foreignBKGuarantorCorporate : _foreignBKGuarantorCorporate,
            });
          }
        }
        _guarantorCorporates.add(
            {
              "guarantorCorporateID": _preferences.getString("last_known_state") != "IDE"
                  ? _providerGuarantor.listGuarantorCompany[i].guarantorCorporateID != null
                  ? _providerGuarantor.listGuarantorCompany[i].guarantorCorporateID : _foreignBKGuarantorCorporate : _foreignBKGuarantorCorporate, // NEW
              "dedupScore": 0.0,
              "businessPermitSpecification": {
                "institutionType": _providerGuarantor.listGuarantorCompany[i].typeInstitutionModel.PARA_ID,
                "profile": _providerGuarantor.listGuarantorCompany[i].profilModel != null ? _providerGuarantor.listGuarantorCompany[i].profilModel.id : "",
                "fullName": _providerGuarantor.listGuarantorCompany[i].institutionName,
                "deedOfIncorporationNumber": null,
                "deedEndDate": null,
                "siupNumber": null,
                "siupStartDate": null,
                "tdpNumber": null,
                "tdpStartDate": null,
                "establishmentDate": formatDateValidateAndSubmit(DateTime.parse(_providerGuarantor.listGuarantorCompany[i].establishDate)),//"${_providerGuarantor.listGuarantorCompany[i].establishDate} 00:00:00",
                "isCompany": false,
                "authorizedCapital": 0,
                "paidCapital": 0
              },
              "businessSpecification": {
                "economySector": null,
                "businessField": null,
                "locationStatus": null,
                "businessLocation": null,
                "employeeTotal": 0,
                "bussinessLengthInMonth": 0,
                "totalBussinessLengthInMonth": 0
              },
              "customerNpwpSpecification": {
                "hasNpwp": true,
                "npwpAddress": _npwpAddress,
                "npwpFullname": _npwpFullName,
                "npwpNumber": _providerGuarantor.listGuarantorCompany[i].npwp,
                "npwpType": _npwpType,
                "pkpIdentifier": _providerRincian.signPKPSelected != null ? _providerRincian.signPKPSelected.id : "2"
              },
              "guarantorCorporateAddresses": _detailAddressGuarantorCompany,
              "creationalSpecification": {
                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                "createdBy": _preferences.getString("username"),
                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                "modifiedBy": _preferences.getString("username"),
              },
              "status": "ACTIVE"
            }
        );
      }
    }

    List _detailAddressPIC = [];

    var _orderProductSaleses = [];
    if(_providerSales.listInfoSales.isNotEmpty) {
      for(int i=0; i<_providerSales.listInfoSales.length; i++){
        _orderProductSaleses.add({
          "orderProductSalesID": _preferences.getString("last_known_state") != "IDE" ? _providerSales.listInfoSales[i].orderProductSalesID : "NEW",
          "salesType": _providerSales.listInfoSales[i].salesmanTypeModel.id,
          "employeeJob": _providerSales.listInfoSales[i].positionModel.id == "" ? null : _providerSales.listInfoSales[i].positionModel.id,
          "employeeID": _providerSales.listInfoSales[i].employeeModel == null ? '' : _providerSales.listInfoSales[i].employeeModel.NIK,
          "employeeHeadID": _providerSales.listInfoSales[i].employeeHeadModel == null ? '' : _providerSales.listInfoSales[i].employeeHeadModel.NIK,
          "referalContractNumber": null,
          "creationalSpecification": {
            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
            "createdBy": _preferences.getString("username"),
            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
            "modifiedBy": _preferences.getString("username"),
          },
          "status": "ACTIVE"
        });
      }
    }

    var _orderKaroseris = [];
    if(_providerKaroseri.listFormKaroseriObject.isNotEmpty) {
      for(int i=0; i < _providerKaroseri.listFormKaroseriObject.length; i++) {
        _orderKaroseris.add(
            {
              "orderKaroseriID": _preferences.getString("last_known_state") != "IDE" ? _providerKaroseri.listFormKaroseriObject[i].orderKaroseriID : "NEW",
              "isPksKaroseri": _providerKaroseri.listFormKaroseriObject[i].PKSKaroseri,
              "karoseriCompanyID": _providerKaroseri.listFormKaroseriObject[i].company != null ? _providerKaroseri.listFormKaroseriObject[i].company.id : null,
              "karoseriID": _providerKaroseri.listFormKaroseriObject[i].karoseri.kode,
              "karoseriPrice": double.parse(_providerKaroseri.listFormKaroseriObject[i].price.replaceAll(",", "")),
              "karoseriQty": _providerKaroseri.listFormKaroseriObject[i].jumlahKaroseri,
              "karoseriTotalPrice": double.parse(_providerKaroseri.listFormKaroseriObject[i].totalPrice.replaceAll(",", "")),
              "uangMukaKaroseri": 0,
              "flagKaroseri": 1,
              "creationalSpecification": {
                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                "createdBy": _preferences.getString("username"),
                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                "modifiedBy": _preferences.getString("username"),
              },
              "status": "ACTIVE"
            }
        );
      }
    }

    var _installmentDetails = [];
    if(_providerCreditStructure.installmentTypeSelected.id == "06") {
      // Stepping
      if(_providerCreditStructureType.listInfoCreditStructureStepping.isNotEmpty) {
        for(int i=0; i < _providerCreditStructureType.listInfoCreditStructureStepping.length; i++) {
          _installmentDetails.add({
            "installmentID": _preferences.getString("last_known_state") != "IDE" ? null : "NEW",// nanti di bagian if di ambil dr sharedPreference
            "installmentNumber": 0,
            "percentage": _providerCreditStructureType.listInfoCreditStructureStepping[i].controllerPercentage.text != "" ? double.parse(_providerCreditStructureType.listInfoCreditStructureStepping[i].controllerPercentage.text.replaceAll(",", "")) : 0,
            "amount": _providerCreditStructureType.listInfoCreditStructureStepping[i].controllerValue.text != "" ? double.parse(_providerCreditStructureType.listInfoCreditStructureStepping[i].controllerValue.text.replaceAll(",", "")) : 0,
            "creationalSpecification": {
              "createdAt": formatDateValidateAndSubmit(DateTime.now()),
              "createdBy": _preferences.getString("username"),
              "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
              "modifiedBy": _preferences.getString("username"),
            },
            "status": "ACTIVE"
          });
        }
      }
    }
    else if(_providerCreditStructure.installmentTypeSelected.id == "07") {
      // Irreguler
      if(_providerCreditStructureType.listInfoCreditStructureIrregularModel.isNotEmpty) {
        for(int i=0; i < _providerCreditStructureType.listInfoCreditStructureIrregularModel.length; i++) {
          _installmentDetails.add({
            "installmentID": _preferences.getString("last_known_state") != "IDE" ? null : "NEW",// nanti di bagian if di ambil dr sharedPreference
            "installmentNumber": i+1,
            "percentage": 0,
            "amount": _providerCreditStructureType.listInfoCreditStructureIrregularModel[i].controller.text != "" ? double.parse(_providerCreditStructureType.listInfoCreditStructureIrregularModel[i].controller.text.replaceAll(",", "")) : 0,
            "creationalSpecification": {
              "createdAt": formatDateValidateAndSubmit(DateTime.now()),
              "createdBy": _preferences.getString("username"),
              "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
              "modifiedBy": _preferences.getString("username"),
            },
            "status": "ACTIVE"
          });
        }
      }
    }

    var _orderFees = [];
    if(_providerCreditStructure.listInfoFeeCreditStructure.isNotEmpty) {
      bool _isProvisiEmpty = true;
      bool _isAdminEmpty = true;
      for (int i = 0; i <  _providerCreditStructure.listInfoFeeCreditStructure.length; i++) {
        if(_providerCreditStructure.listInfoFeeCreditStructure[i].feeTypeModel.id == "01"){
          _isAdminEmpty = false;
        }
        if(_providerCreditStructure.listInfoFeeCreditStructure[i].feeTypeModel.id == "02"){
          _isProvisiEmpty = false;
        }
        _orderFees.add({
          "orderFeeID": _preferences.getString("last_known_state") != "IDE" ? _providerCreditStructure.listInfoFeeCreditStructure[i].orderFeeID : "NEW",
          "feeTypeID": _providerCreditStructure.listInfoFeeCreditStructure[i].feeTypeModel.id,
          "cashFee": _providerCreditStructure.listInfoFeeCreditStructure[i].cashCost == "" ? null : double.parse(_providerCreditStructure.listInfoFeeCreditStructure[i].cashCost.replaceAll(",", "")).round(),
          "creditFee": _providerCreditStructure.listInfoFeeCreditStructure[i].creditCost == "" ? null : double.parse(_providerCreditStructure.listInfoFeeCreditStructure[i].creditCost.replaceAll(",", "")).round(),
          "totalFee": _providerCreditStructure.listInfoFeeCreditStructure[i].totalCost == "" ? null : double.parse(_providerCreditStructure.listInfoFeeCreditStructure[i].totalCost.replaceAll(",", "")).round(),
          "creationalSpecification": {
            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
            "createdBy": _preferences.getString("username"),
            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
            "modifiedBy": _preferences.getString("username"),
          },
          "status": "ACTIVE"
        });
      }

      if(_isAdminEmpty){
        _orderFees.add({
          "orderFeeID": "NEW",
          "feeTypeID": "01",
          "cashFee": 0,
          "creditFee": 0,
          "totalFee": 0,
          "creationalSpecification": {
            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
            "createdBy": _preferences.getString("username"),
            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
            "modifiedBy": _preferences.getString("username"),
          },
          "status": "ACTIVE"
        });
      }

      if(_isProvisiEmpty){
        _orderFees.add({
          "orderFeeID": "NEW",
          "feeTypeID": "02",
          "cashFee": 0,
          "creditFee": 0,
          "totalFee": 0,
          "creationalSpecification": {
            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
            "createdBy": _preferences.getString("username"),
            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
            "modifiedBy": _preferences.getString("username"),
          },
          "status": "ACTIVE"
        });
      }
    }
    else{
      _orderFees.add({
        "orderFeeID": "NEW",
        "feeTypeID": "01",
        "cashFee": 0,
        "creditFee": 0,
        "totalFee": 0,
        "creationalSpecification": {
          "createdAt": formatDateValidateAndSubmit(DateTime.now()),
          "createdBy": _preferences.getString("username"),
          "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
          "modifiedBy": _preferences.getString("username"),
        },
        "status": "ACTIVE"
      });
      _orderFees.add({
        "orderFeeID": "NEW",
        "feeTypeID": "02",
        "cashFee": 0,
        "creditFee": 0,
        "totalFee": 0,
        "creationalSpecification": {
          "createdAt": formatDateValidateAndSubmit(DateTime.now()),
          "createdBy": _preferences.getString("username"),
          "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
          "modifiedBy": _preferences.getString("username"),
        },
        "status": "ACTIVE"
      });
    }

    var _orderProductInsurances = [];
    if(_providerInsurance.listFormMajorInsurance.isNotEmpty) {
      String _collateralIDMajorInsurance = "";
      if(_providerKolateral.collateralTypeModel.id == "001") {
        _collateralIDMajorInsurance = "CAU001";
      } else if(_providerKolateral.collateralTypeModel.id == "002") {
        _collateralIDMajorInsurance = "COLP001";
      } else {
        _collateralIDMajorInsurance = "";
      }
      for(int i=0; i<_providerInsurance.listFormMajorInsurance.length; i++) {
        // debugPrint("CEK_ID ${_providerInsurance.listFormMajorInsurance[i].orderProductInsuranceID}");
        _orderProductInsurances.add({
          "orderProductInsuranceID": _providerInsurance.listFormMajorInsurance[i].orderProductInsuranceID,//_preferences.getString("last_known_state") != "IDE" ? _providerInsurance.listFormMajorInsurance[i].orderProductInsuranceID : "NEW",
          "insuranceCollateralReferenceSpecification": {
            "collateralID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.collateralID : _collateralIDMajorInsurance,
            "collateralTypeID": _providerKolateral.collateralTypeModel.id, // _providerInsurance.listFormMajorInsurance[i].colaType
          },
          "insuranceTypeID": _providerInsurance.listFormMajorInsurance[i].insuranceType.KODE,
          "insuranceCompanyID": _providerInsurance.listFormMajorInsurance[i].company  == null ? "02" : _providerInsurance.listFormMajorInsurance[i].company.KODE, // field perusahaan
          "insuranceProductID": _providerInsurance.listFormMajorInsurance[i].product != null ? _providerInsurance.listFormMajorInsurance[i].product.KODE : "", // field produk
          "period": _providerInsurance.listFormMajorInsurance[i].periodType == "" ? null : int.parse(_providerInsurance.listFormMajorInsurance[i].periodType),
          "type": _providerInsurance.listFormMajorInsurance[i].type,
          "type1": _providerInsurance.listFormMajorInsurance[i].coverage1 != null ? _providerInsurance.listFormMajorInsurance[i].coverage1.KODE :"", // field coverage 1
          "type2": _providerInsurance.listFormMajorInsurance[i].coverage2 != null ? _providerInsurance.listFormMajorInsurance[i].coverage2.KODE : "", // field coverage 2
          "subType": _providerInsurance.listFormMajorInsurance[i].subType != null ? _providerInsurance.listFormMajorInsurance[i].subType.KODE :"", // field coverage 1
          "subTypeCoverage": _providerInsurance.listFormMajorInsurance[i].coverageType == null ? null : _providerInsurance.listFormMajorInsurance[i].coverageType.KODE, // field sub type
          "insuranceSourceTypeID": _providerInsurance.listFormMajorInsurance[i].coverageType != null ? _providerInsurance.listFormMajorInsurance[i].coverageType.KODE : "", // field jenis pertanggungan
          "insuranceValue": _providerInsurance.listFormMajorInsurance[i].coverageValue.isEmpty ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].coverageValue.replaceAll(",", "")).round(),
          "upperLimitPercentage": _providerInsurance.listFormMajorInsurance[i].upperLimitRate == "" ? 0.0 : double.parse(_providerInsurance.listFormMajorInsurance[i].upperLimitRate),
          "upperLimitAmount": _providerInsurance.listFormMajorInsurance[i].upperLimit == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].upperLimit.replaceAll(",", "")),
          "lowerLimitPercentage": _providerInsurance.listFormMajorInsurance[i].lowerLimitRate == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].lowerLimitRate),
          "lowerLimitAmount": _providerInsurance.listFormMajorInsurance[i].lowerLimit == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].lowerLimit.replaceAll(",", "")),
          "insuranceCashFee": _providerInsurance.listFormMajorInsurance[i].priceCash == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].priceCash.replaceAll(",", "")).round(),
          "insuranceCreditFee": _providerInsurance.listFormMajorInsurance[i].priceCredit == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].priceCredit.replaceAll(",", "")).round(),
          "totalSplitFee": _providerInsurance.listFormMajorInsurance[i].totalPriceSplit == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].totalPriceSplit.replaceAll(",", "")),
          "totalInsuranceFeePercentage": _providerInsurance.listFormMajorInsurance[i].totalPriceRate == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].totalPriceRate),
          "totalInsuranceFeeAmount": _providerInsurance.listFormMajorInsurance[i].totalPrice == "" ? 0 : double.parse(_providerInsurance.listFormMajorInsurance[i].totalPrice.replaceAll(",", "")),
          "creationalSpecification": {
            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
            "createdBy": _preferences.getString("username"),
            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
            "modifiedBy": _preferences.getString("username"),
          },
          "status": "ACTIVE",
          "insurancePaymentType": null
        });
      }
    }

    if(_providerAdditionalInsurance.listFormAdditionalInsurance.isNotEmpty) {
      // debugPrint("CHECK INSURANCE ${_providerAdditionalInsurance.listFormAdditionalInsurance.length}");
      for(int i=0; i<_providerAdditionalInsurance.listFormAdditionalInsurance.length; i++) {
        _orderProductInsurances.add({
          "orderProductInsuranceID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].orderProductInsuranceID,//_preferences.getString("last_known_state") != "IDE" ? _providerAdditionalInsurance.listFormAdditionalInsurance[i].orderProductInsuranceID : "NEW",
          "insuranceCollateralReferenceSpecification": {
            "collateralID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.collateralID : "", // NEW
            "collateralTypeID": _providerKolateral.collateralTypeModel.id, // _providerAdditionalInsurance.listFormAdditionalInsurance[i].colaType
          },
          "insuranceTypeID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].insuranceType.KODE,
          "insuranceCompanyID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].company  == null ? "02" : _providerAdditionalInsurance.listFormAdditionalInsurance[i].company.KODE,
          "insuranceProductID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].product.KODE,
          "period": _providerAdditionalInsurance.listFormAdditionalInsurance[i].periodType == "" ? null : int.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].periodType),
          "type": "",
          "type1": "",
          "type2": "",
          "subType": "",
          "subTypeCoverage": "",
          "insuranceSourceTypeID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageType != null ? _providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageType.KODE : "", // field jenis pertanggungan
          "insuranceValue": _providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageValue.isEmpty ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageValue.replaceAll(",", "")).round(),
          "upperLimitPercentage": _providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimitRate == "" ? 0.0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimitRate),
          "upperLimitAmount": _providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimit == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimit.replaceAll(",", "")),
          "lowerLimitPercentage": _providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimitRate == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimitRate),
          "lowerLimitAmount": _providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimit == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimit.replaceAll(",", "")),
          "insuranceCashFee": _providerAdditionalInsurance.listFormAdditionalInsurance[i].priceCash == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].priceCash.replaceAll(",", "")).round(),
          "insuranceCreditFee": _providerAdditionalInsurance.listFormAdditionalInsurance[i].priceCredit == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].priceCredit.replaceAll(",", "")).round(),
          "totalSplitFee": "",
          "totalInsuranceFeePercentage": _providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPriceRate == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPriceRate),
          "totalInsuranceFeeAmount": _providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPrice == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPrice.replaceAll(",", "")),
          "creationalSpecification": {
            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
            "createdBy": _preferences.getString("username"),
            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
            "modifiedBy": _preferences.getString("username"),
          },
          "status": "ACTIVE",
          "insurancePaymentType": null
        });
      }
    }

    var _orderWmps = [];
    // print("cek wmp ${_providerWmp.listWMP.isNotEmpty}");
    if(_providerWmp.listWMP.isNotEmpty) {
      print("masuk if");
      for(int i=0; i < _providerWmp.listWMP.length; i++) {
        _orderWmps.add(
          {
            "orderWmpID": _preferences.getString("last_known_state") != "IDE" ? _providerWmp.listWMP[i].orderWmpID : "NEW",
            "wmpNumber": _providerWmp.listWMP[i].noProposal,
            "wmpType": _providerWmp.listWMP[i].type,
            "wmpJob": "",
            "wmpSubsidyTypeID": _providerWmp.listWMP[i].typeSubsidi,
            "maxRefundAmount": _providerWmp.listWMP[i].amount,
            "status": "ACTIVE",
            "creationalSpecification": {
              "createdAt": formatDateValidateAndSubmit(DateTime.now()),
              "createdBy": _preferences.getString("username"),
              "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
              "modifiedBy": _preferences.getString("username"),
            },
          },
        );
      }
    }
    else{
      print("masuk else");
    }

    var _orderSubsidies = [];
    var _orderSubsidyDetails = [];
    if(_providerSubsidy.listInfoCreditSubsidy.isNotEmpty) {
      for(int i=0; i < _providerSubsidy.listInfoCreditSubsidy.length; i++) {
        for(int j=0; j < _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail.length; j++) {
          _orderSubsidyDetails.add(
              {
                "orderSubsidyDetailID": _preferences.getString("last_known_state") != "IDE" ? _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].orderSubsidyDetailID : "NEW",
                "installmentNumber": _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentIndex != null ? double.parse(_providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentIndex.replaceAll(",", "")) : 0,
                "installmentAmount": _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentSubsidy != null ? double.parse(_providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentSubsidy.replaceAll(",", "")) : 0,
                "creationalSpecification": {
                  "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                  "createdBy": _preferences.getString("username"),
                  "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                  "modifiedBy": _preferences.getString("username"),
                },
                "status": "ACTIVE"
              }
          );
        }
        _orderSubsidies.add(
            {
              "orderSubsidyID": _preferences.getString("last_known_state") != "IDE" ? _providerSubsidy.listInfoCreditSubsidy[i].orderSubsidyID : "NEW",
              "subsidyProviderID": _providerSubsidy.listInfoCreditSubsidy[i].giver,
              "subsidyTypeID": _providerSubsidy.listInfoCreditSubsidy[i].type != null ? _providerSubsidy.listInfoCreditSubsidy[i].type.id : "",
              "subsidyMethodTypeID": _providerSubsidy.listInfoCreditSubsidy[i].cuttingMethod != null ? _providerSubsidy.listInfoCreditSubsidy[i].cuttingMethod.id : "02",
              "refundAmount": _providerSubsidy.listInfoCreditSubsidy[i].value != ""
                  ? double.parse(_providerSubsidy.listInfoCreditSubsidy[i].value.replaceAll(",", "")) : 0,
              "refundAmountKlaim": _providerSubsidy.listInfoCreditSubsidy[i].claimValue != "" ?
              double.parse(_providerSubsidy.listInfoCreditSubsidy[i].claimValue.replaceAll(",", "")) : 0, //Nilai Klaim
              "effectiveRate": _providerSubsidy.listInfoCreditSubsidy[i].rateBeforeEff != ""
                  ? double.parse(_providerSubsidy.listInfoCreditSubsidy[i].rateBeforeEff.replaceAll(",", "")) : 0,
              "flatRate":_providerSubsidy.listInfoCreditSubsidy[i].rateBeforeFlat != "" ?
              double.parse(_providerSubsidy.listInfoCreditSubsidy[i].rateBeforeFlat.replaceAll(",", "")) : 0,
              "dpReal": 0,
              "totalInstallment": _providerSubsidy.listInfoCreditSubsidy[i].totalInstallment != "" && _providerSubsidy.listInfoCreditSubsidy[i].totalInstallment != null
                  ? _providerSubsidy.listInfoCreditSubsidy[i].totalInstallment.replaceAll(",", "") : "",
              "interestRate": 0,
              "creationalSpecification": {
                "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                "createdBy": _preferences.getString("username"),
                "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                "modifiedBy": _preferences.getString("username"),
              },
              "status": "ACTIVE",
              "orderSubsidyDetails": _orderSubsidyDetails
            }
        );
      }
    }

    var _surveyResultDetails = [];
    if(_providerSurvey.listResultSurveyDetailSurveyModel.isNotEmpty) {
      for(int i=0; i < _providerSurvey.listResultSurveyDetailSurveyModel.length; i++) {
        _surveyResultDetails.add({
          "surveyResultDetailID": _providerSurvey.listResultSurveyDetailSurveyModel[i].surveyResultDetailID != null ? _providerSurvey.listResultSurveyDetailSurveyModel[i].surveyResultDetailID : "NEW",
          "infoEnvironment": _providerSurvey.listResultSurveyDetailSurveyModel[i].environmentalInformationModel.id,
          "sourceInfo": _providerSurvey.listResultSurveyDetailSurveyModel[i].recourcesInfoSurveyModel.PARA_INFORMATION_SOURCE_ID,
          "sourceNameInfo": _providerSurvey.listResultSurveyDetailSurveyModel[i].resourceInformationName,
          "status": "ACTIVE",
          "creationalSpecification": {
            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
            "createdBy": _preferences.getString("username"),
            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
            "modifiedBy": _preferences.getString("username"),
          }
        });
      }
    }

    var _surveyResultAssets = [];
    if(_providerSurvey.listResultSurveyAssetModel.isNotEmpty){
      for(int i=0; i< _providerSurvey.listResultSurveyAssetModel.length; i++){
        _surveyResultAssets.add({
          "surveyResultAssetID": _providerSurvey.listResultSurveyAssetModel[i].surveyResultAssetID != null ? _providerSurvey.listResultSurveyAssetModel[i].surveyResultAssetID : "NEW",
          "assetType": _providerSurvey.listResultSurveyAssetModel[i].assetTypeModel != null ? _providerSurvey.listResultSurveyAssetModel[i].assetTypeModel.id : null,
          "assetAmount": _providerSurvey.listResultSurveyAssetModel[i].valueAsset != "" ? double.parse(_providerSurvey.listResultSurveyAssetModel[i].valueAsset.replaceAll(",", "")).toInt() : 0,
          "ownStatus": _providerSurvey.listResultSurveyAssetModel[i].ownershipModel != null ? _providerSurvey.listResultSurveyAssetModel[i].ownershipModel.id : null,
          "sizeofLand": _providerSurvey.listResultSurveyAssetModel[i].surfaceBuildingArea != "" ? _providerSurvey.listResultSurveyAssetModel[i].surfaceBuildingArea : null,
          "streetType": _providerSurvey.listResultSurveyAssetModel[i].roadTypeModel != null ? _providerSurvey.listResultSurveyAssetModel[i].roadTypeModel.kode : null,
          "electricityType": _providerSurvey.listResultSurveyAssetModel[i].electricityTypeModel != null ? _providerSurvey.listResultSurveyAssetModel[i].electricityTypeModel.PARA_ELECTRICITY_ID : null,
          "electricityBill": _providerSurvey.listResultSurveyAssetModel[i].electricityBills != "" ? double.parse(_providerSurvey.listResultSurveyAssetModel[i].electricityBills.replaceAll(",", "")).toInt() : 0,
          "expiredContractDate": _providerSurvey.listResultSurveyAssetModel[i].endDateLease != null ? formatDateValidateAndSubmit(_providerSurvey.listResultSurveyAssetModel[i].endDateLease) : null,
          "longOfStay": _providerSurvey.listResultSurveyAssetModel[i].lengthStay != "" ? _providerSurvey.listResultSurveyAssetModel[i].lengthStay : null,
          "status": "ACTIVE",
          "creationalSpecification": {
            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
            "createdBy": _preferences.getString("username"),
            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
            "modifiedBy": _preferences.getString("username")
          }
        });
      }
    }

    var _orderProductSalesesDakor = [];
    if(_providerSales.listInfoSales.isNotEmpty) {
      for(int i=0; i<_providerSales.listInfoSales.length; i++){
        _orderProductSalesesDakor.add({
          "productSalesIDReason": null,
          "productSalesTypeReason": _providerSales.listInfoSales[i].isSalesmanTypeModelChanges ? "99" : null,
          "productSalesEmployeeJobReason": _providerSales.listInfoSales[i].isPositionModelChanges ? "99" : null,
          "productSalesEmployeeIDReason": _providerSales.listInfoSales[i].isEmployeeChanges ? "99" : null,
          "productSalesEmployeeHeadIDReason": _providerSales.listInfoSales[i].isEmployeeHeadModelChanges ? "99" : null,
          "productSalesReferalContractNumberReason": null,
          "productSalesCreatedAtReason": null,
          "productSalesCreatedByReason": null,
          "productSalesModifiedAtReason": null,
          "productSalesModifiedByReason": null,
          "productSalesStatusReason": null,
          "productSalesFlagCorrectionReason": null,
        });
      }
    }

    var _orderProductKaroserisReason = [];
    if(_providerKaroseri.listFormKaroseriObject.isNotEmpty) {
      for(int i=0; i < _providerKaroseri.listFormKaroseriObject.length; i++) {
        _orderProductKaroserisReason.add({
          "orderOrderKaroseriIDReason": null,
          "orderKaroseriIsPksKaroseriReason": _providerKaroseri.listFormKaroseriObject[i].isRadioValuePKSKaroseriChanges ? "99" : null,
          "orderKaroseriCompanyIDReason": _providerKaroseri.listFormKaroseriObject[i].isCompanyChanges ? "99" : null,
          "orderKaroseriIDReason": _providerKaroseri.listFormKaroseriObject[i].isKaroseriChanges ? "99" : null,
          "orderKaroseriPriceReason": _providerKaroseri.listFormKaroseriObject[i].isPriceChanges ? "99" : null,
          "orderKaroseriQtyReason": _providerKaroseri.listFormKaroseriObject[i].isJumlahKaroseriChanges ? "99" : null,
          "orderKaroseriTotalPriceReason": _providerKaroseri.listFormKaroseriObject[i].isTotalPriceChanges ? "99" : null,
          "orderKaroseriUangMukaKaroseriReason": null,
          "orderKaroseriCreatedAtReason": null,
          "orderKaroseriCreatedByReason": null,
          "orderKaroseriModifiedAtReason": null,
          "orderKaroseriModifiedByReason": null,
          "orderKaroseriStatusReason": null,
        });
      }
    }

    // Asuransi Utama
    var _orderProductInsurancesReason = [];
    if(_providerInsurance.listFormMajorInsurance.isNotEmpty) {
      for(int i=0; i < _providerInsurance.listFormMajorInsurance.length; i++) {
        _orderProductInsurancesReason.add({
          "orderProductInsuranceIDReason": null,
          "orderProductInsuranceCollateralIDReason": null,
          "orderProductInsuranceCollateralTypeReason": null,
          "orderProductInsuranceTypeIDReason": null,
          "orderProductInsuranceCompanyIDReason": _providerInsurance.listFormMajorInsurance[i].companyDakor ? "99" : null,
          "orderProductInsuranceProductIDReason": _providerInsurance.listFormMajorInsurance[i].productDakor ? "99" : null,
          "orderProductInsurancePeriodReason": _providerInsurance.listFormMajorInsurance[i].periodDakor ? "99" : null,
          "orderProductInsuranceTypeReason": _providerInsurance.listFormMajorInsurance[i].typeDakor ? "99" : null,
          "orderProductInsuranceType1Reason": _providerInsurance.listFormMajorInsurance[i].coverage1Dakor ? "99" : null,
          "orderProductInsuranceType2Reason": _providerInsurance.listFormMajorInsurance[i].coverage2Dakor ? "99" : null,
          "orderProductInsuranceSubTypeReason": _providerInsurance.listFormMajorInsurance[i].coverageTypeDakor ? "99" : null,
          "orderProductInsuranceSubTypeCoverageReason": _providerInsurance.listFormMajorInsurance[i].coverageTypeDakor ? "99" : null,
          "orderProductInsuranceSourceTypeIDReason": null,
          "orderProductInsuranceValueReason": _providerInsurance.listFormMajorInsurance[i].coverageValueDakor ? "99": null,
          "orderProductInsuranceUpperLimitPercentageReason": _providerInsurance.listFormMajorInsurance[i].upperLimitRateDakor ? "99" : null,
          "orderProductInsuranceUpperLimitAmountReason": _providerInsurance.listFormMajorInsurance[i].upperLimitValueDakor ? "99" : null,
          "orderProductInsuranceLowerLimitPercentageReason": _providerInsurance.listFormMajorInsurance[i].lowerLimitRateDakor ? "99" : null,
          "orderProductInsuranceLowerLimitAmountReason": _providerInsurance.listFormMajorInsurance[i].lowerLimitValueDakor ? "99" : null,
          "orderProductInsuranceCashFeeReason": _providerInsurance.listFormMajorInsurance[i].cashDakor ? "99" : null,
          "orderProductInsuranceCreditFeeReason": _providerInsurance.listFormMajorInsurance[i].creditDakor ? "99" : null,
          "orderProductInsuranceTotalSplitFeeReason": _providerInsurance.listFormMajorInsurance[i].totalPriceSplitDakor ? "99" : null,
          "orderProductTotalInsuranceFeePercentageReason": _providerInsurance.listFormMajorInsurance[i].totalPriceRateDakor ? "99" : null,
          "orderProductTotalInsuranceFeeAmountReason": _providerInsurance.listFormMajorInsurance[i].totalPriceDakor ? "99" : null,
          "orderProductInsuranceCreatedAtReason": null,
          "orderProductInsuranceCreatedByReason": null,
          "orderProductInsuranceModifiedAtReason": null,
          "orderProductInsuranceModifiedByReason": null,
          "orderProductInsuranceStatusReason": null,
          "orderProductInsurancePaymentTypeReason": null,
          "orderProductInsuranceFlagCorrectionReason": null
        });
      }
    }

    // Asuransi Tambahan
    if(_providerAdditionalInsurance.listFormAdditionalInsurance.isNotEmpty) {
      for(int i=0; i < _providerAdditionalInsurance.listFormAdditionalInsurance.length; i++) {
        _orderProductInsurancesReason.add({
          "orderProductInsuranceIDReason": null,
          "orderProductInsuranceCollateralIDReason": null,
          "orderProductInsuranceCollateralTypeReason": null,
          "orderProductInsuranceTypeIDReason": null,
          "orderProductInsuranceCompanyIDReason": _providerAdditionalInsurance.listFormAdditionalInsurance[i].companyDakor ? "99" : null,
          "orderProductInsuranceProductIDReason": _providerAdditionalInsurance.listFormAdditionalInsurance[i].productDakor ? "99" : null,
          "orderProductInsurancePeriodReason": _providerAdditionalInsurance.listFormAdditionalInsurance[i].periodDakor ? "99" : null,
          "orderProductInsuranceTypeReason": null,
          "orderProductInsuranceType1Reason": null,
          "orderProductInsuranceType2Reason": null,
          "orderProductInsuranceSubTypeReason": null,
          "orderProductInsuranceSubTypeCoverageReason": _providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageTypeDakor ? "99" : null,
          "orderProductInsuranceSourceTypeIDReason": null,
          "orderProductInsuranceValueReason": _providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageValueDakor ? "99": null,
          "orderProductInsuranceUpperLimitPercentageReason": _providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimitRateDakor ? "99" : null,
          "orderProductInsuranceUpperLimitAmountReason": _providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimitValueDakor ? "99" : null,
          "orderProductInsuranceLowerLimitPercentageReason": _providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimitRateDakor ? "99" : null,
          "orderProductInsuranceLowerLimitAmountReason": _providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimitValueDakor ? "99" : null,
          "orderProductInsuranceCashFeeReason": _providerAdditionalInsurance.listFormAdditionalInsurance[i].cashDakor ? "99" : null,
          "orderProductInsuranceCreditFeeReason": _providerAdditionalInsurance.listFormAdditionalInsurance[i].creditDakor ? "99" : null,
          "orderProductInsuranceTotalSplitFeeReason": null,
          "orderProductTotalInsuranceFeePercentageReason": _providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPriceRateDakor ? "99" : null,
          "orderProductTotalInsuranceFeeAmountReason": _providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPriceDakor ? "99" : null,
          "orderProductInsuranceCreatedAtReason": null,
          "orderProductInsuranceCreatedByReason": null,
          "orderProductInsuranceModifiedAtReason": null,
          "orderProductInsuranceModifiedByReason": null,
          "orderProductInsuranceStatusReason": null,
          "orderProductInsurancePaymentTypeReason": null,
          "orderProductInsuranceFlagCorrectionReason": null
        });
      }
    }

    var _orderProductWmpsReason = [];
    if(_providerWmp.listWMP.isNotEmpty) {
      for(int i=0; i < _providerWmp.listWMP.length; i++) {
        _orderProductWmpsReason.add({
          "orderWmpIDReason": null,
          "orderWmpNumberReason": _providerWmp.listWMP[i].isEditNoProposal ? "99" : null,
          "orderWmpTypeReason": _providerWmp.listWMP[i].isEditType ? "99" : null,
          "orderWmpJobReason": null,
          "orderWmpSubsidyTypeIDReason": _providerWmp.listWMP[i].isEditTypeSubsidi ? "99" : null,
          "orderWmpMaxRefundAmountReason": _providerWmp.listWMP[i].isEditAmount ? "99" : null,
          "orderWmpCreatedAtReason": null,
          "orderWmpCreatedByReason": null,
          "orderWmpModifiedAtReason": null,
          "orderWmpModifiedByReason": null,
          "orderWmpStatusReason": null,
          "orderWmpFlagCorrectionReason": null,
        });
      }
    }

    // Subsidi
    var _orderProductSubsidiesReason = [];
    var _orderSubsidyDetailsReason = [];
    if(_providerSubsidy.listInfoCreditSubsidy.isNotEmpty) {
      for(int i=0; i < _providerSubsidy.listInfoCreditSubsidy.length; i++) {
        for(int j=0; j < _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail.length; j++) {
          _orderSubsidyDetailsReason.add({
            "orderSubsidyDetailIDReason": null,
            "orderSubsidyDetailInstallmentNumberReason": null, // _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentIndex ? "99" : null
            "orderSubsidyDetailInstallmentAmountReason": null, // _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentSubsidy ? "99" : null
            "orderSubsidyDetailCreatedAtReason": null,
            "orderSubsidyDetailCreatedByReason": null,
            "orderSubsidyDetailModifiedAtReason": null,
            "orderSubsidyDetailModifiedByReason": null,
            "orderSubsidyDetailStatusReason": null,
            "orderSubsidyDetailFlagCorrectionReason": null,
          });
        }
        _orderProductSubsidiesReason.add({
          "orderSubsidyIDReason": null,
          "orderSubsidyProviderIDReason": null,
          "orderSubsidyTypeIDReason": _providerSubsidy.listInfoCreditSubsidy[i].typeDakor ? "99" : null,
          "orderSubsidyMethodTypeIDReason": _providerSubsidy.listInfoCreditSubsidy[i].cuttingMethodDakor ? "99" : null,
          "orderSubsidyRefundAmountReason": _providerSubsidy.listInfoCreditSubsidy[i].cuttingValueDakor ? "99" : null,
          "orderSubsidyRefundAmountKlaimReason": _providerSubsidy.listInfoCreditSubsidy[i].claimValueDakor ? "99" : null,
          "orderSubsidyEffectiveRateReason": _providerSubsidy.listInfoCreditSubsidy[i].rateBeforeEffDakor ? "99" : null,
          "orderSubsidyFlatRateReason": _providerSubsidy.listInfoCreditSubsidy[i].rateBeforeFlatDakor ? "99" : null,
          "orderSubsidyDpRealReason": null,
          "orderSubsidyTotalInstallmentReason": _providerSubsidy.listInfoCreditSubsidy[i].totalInstallmentDakor ? "99" : null,
          "orderSubsidyInterestRateReason": null,
          "orderSubsidyCreatedAtReason": null,
          "orderSubsidyCreatedByReason": null,
          "orderSubsidyModifiedAtReason": null,
          "orderSubsidyModifiedByReason": null,
          "orderSubsidyStatusReason": null,
          "orderSubsidyFlagCorrectionReason": null,
          "orderSubsidyDetailsReason": _orderSubsidyDetailsReason
        });
      }
    }

    //survey
    var _surveyResultAssetsDakor = [];
    if(_providerSurvey.listResultSurveyAssetModel.isNotEmpty){
      for(int i=0; i< _providerSurvey.listResultSurveyAssetModel.length; i++){
        _surveyResultAssetsDakor.add({
          "surveyResultAssetIDReason": null,
          "surveyResultAssetTypeReason": _providerSurvey.listResultSurveyAssetModel[i].isAssetTypeChanges ? "99" : null,
          "surveyResultAssetAmountReason": _providerSurvey.listResultSurveyAssetModel[i].isValueAssetChanges ? "99" : null,
          "surveyResultAssetOwnStatusReason": _providerSurvey.listResultSurveyAssetModel[i].isOwneshipInfoChanges ? "99" : null,
          "surveyResultAssetSizeofLandReason": _providerSurvey.listResultSurveyAssetModel[i].isSurfaceBuildingAreaInfoChanges ? "99" : null,
          "surveyResultAssetStreetTypeReason": _providerSurvey.listResultSurveyAssetModel[i].isRoadTypeInfoChanges ? "99" : null,
          "surveyResultAssetElectricityTypeReason": _providerSurvey.listResultSurveyAssetModel[i].isElectricityTypeInfoChanges ? "99" : null,
          "surveyResultAssetElectricityBillReason": _providerSurvey.listResultSurveyAssetModel[i].isElectricityBillInfoChanges ? "99" : null,
          "surveyResultAssetExpiredContractDateReason": _providerSurvey.listResultSurveyAssetModel[i].isEndDateInfoChanges ? "99" : null,
          "surveyResultAssetLongOfStayReason": _providerSurvey.listResultSurveyAssetModel[i].isLengthStayInfoChanges ? "99" : null,
          "surveyResultAssetStatusReason": null,
          "surveyResultAssetCreatedAtReason": null,
          "surveyResultAssetCreatedByReason": null,
          "surveyResultAssetModifiedAtReason": null,
          "surveyResultAssetModifiedByReason": null,
          "surveyResultAssetFlagCorrectionReason": null,
        });
      }
    }

    var _surveyResultDetailsDakor = [];
    if(_providerSurvey.listResultSurveyDetailSurveyModel.isNotEmpty){
      for(int i=0; i < _providerSurvey.listResultSurveyDetailSurveyModel.length; i++) {
        _surveyResultDetailsDakor.add({
          "surveyResultDetailIDReason": null,
          "surveyResultDetailInfoEnvironmentReason": _providerSurvey.listResultSurveyDetailSurveyModel[i].isEnvInformationChange ? "99" : null,
          "surveyResultDetailSourceInfoReason": _providerSurvey.listResultSurveyDetailSurveyModel[i].isSourceInformationChange ? "99" : null,
          "surveyResultDetailSourceNameInfoReason": _providerSurvey.listResultSurveyDetailSurveyModel[i].isSourceInfoNameChange ? "99" : null,
          "surveyResultDetailStatusReason": null,
          "surveyResultDetailCreatedAtReason": null,
          "surveyResultDetailCreatedByReason": null,
          "surveyResultDetailModifiedAtReason": null,
          "surveyResultDetailModifiedByReason": null,
          "surveyResultDetailFlagCorrectionReason": null,
        });
      }
    }

    var _customerIndividualOccupationBusinessTypeReason;
    if(_providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07"){
      _customerIndividualOccupationBusinessTypeReason = _providerOccupation.editJenisBadanUsahaWiraswasta ? "99" : null;
    }
    else if(_providerFoto.occupationSelected.KODE != "08"){
      _customerIndividualOccupationBusinessTypeReason = _providerOccupation.editJenisPerusahaanLainnya ? "99" : null;
    }

    var _customerIndividualOccupationBusinessEconomySectorReason;
    if(_providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07"){
      _customerIndividualOccupationBusinessEconomySectorReason = _providerOccupation.editSektorEkonomiWiraswasta ? "99" : null;
    }
    else if(_providerFoto.occupationSelected.KODE == "08"){
      _customerIndividualOccupationBusinessEconomySectorReason = _providerOccupation.editSektorEkonomiProfesional ? "99" : null;
    }
    else{
      _customerIndividualOccupationBusinessEconomySectorReason = _providerOccupation.editSektorEkonomiLainnya ? "99" : null;
    }

    var _customerIndividualOccupationBusinessBusinessFieldReason;
    if(_providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07"){
      _customerIndividualOccupationBusinessBusinessFieldReason = _providerOccupation.editLapanganUsahaWiraswasta ? "99" : null;
    }
    else if(_providerFoto.occupationSelected.KODE == "08"){
      _customerIndividualOccupationBusinessBusinessFieldReason = _providerOccupation.editLapanganUsahaProfesional ? "99" : null;
    }
    else{
      _customerIndividualOccupationBusinessBusinessFieldReason = _providerOccupation.editLapanganUsahaLainnya ? "99" : null;
    }

    var _customerIndividualOccupationBusinessLocationStatusReason;
    if(_providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07"){
      _customerIndividualOccupationBusinessLocationStatusReason = _providerOccupation.editStatusLokasiWiraswasta ? "99" : null;
    }
    else if(_providerFoto.occupationSelected.KODE == "08"){
      _customerIndividualOccupationBusinessLocationStatusReason = _providerOccupation.editStatusLokasiProfesional ? "99" : null;
    }

    var _customerIndividualOccupationBusinessBusinessLocationReason;
    if(_providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07"){
      _customerIndividualOccupationBusinessBusinessLocationReason = _providerOccupation.editLokasiUsahaWiraswasta ? "99" : null;
    }
    else if(_providerFoto.occupationSelected.KODE == "08"){
      _customerIndividualOccupationBusinessBusinessLocationReason = _providerOccupation.editLokasiUsahaProfesional ? "99" : null;
    }

    var _customerIndividualOccupationBusinessEmployeeTotalReason;
    if(_providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07"){
      _customerIndividualOccupationBusinessEmployeeTotalReason = _providerOccupation.editTotalPegawaiWiraswasta ? "99" : null;
    }
    else if(_providerFoto.occupationSelected.KODE != "08"){
      _customerIndividualOccupationBusinessEmployeeTotalReason = _providerOccupation.editTotalPegawaiLainnya ? "99" : null;
    }

    var _customerIndividualOccupationBusinessTotalBussinessLengthInMonthReason;
    if(_providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07"){
      _customerIndividualOccupationBusinessTotalBussinessLengthInMonthReason = _providerOccupation.editTotalLamaBekerjaWiraswasta ? "99" : null;
    }
    else if(_providerFoto.occupationSelected.KODE == "08"){
      _customerIndividualOccupationBusinessTotalBussinessLengthInMonthReason = _providerOccupation.editTotalLamaBekerjaProfesional ? "99" : null;
    }
    else{
      _customerIndividualOccupationBusinessTotalBussinessLengthInMonthReason = _providerOccupation.editTotalLamaBekerjaLainnya ? "99" : null;
    }

    var _custIndividualCustomerFamiliesReason = [];
    var _customerIncomesReason = [];

    _custIndividualCustomerFamiliesReason.add(
        {
        "custIndividualFamilyInfoIDReason": null,
        "custIndividualFamilyRelationshipStatusReason": _providerIbu.isRelationshipStatusChanges ? "99" : null,
        "custIndividualFamilyIdentityTypeReason": null,
        "custIndividualFamilyIdentityNumberReason": null,
        "custIndividualFamilyIdentityNameReason": _providerIbu.isNamaIdentitasChanges ? "99" : null,
        "custIndividualFamilyFullNameReason": _providerIbu.isNamaLengkapdentitasChanges ? "99" : null,
        "custIndividualFamilyAliasReason": null,
        "custIndividualFamilyTitleReason": null,
        "custIndividualFamilyDateOfBirthReason": null,
        "custIndividualFamilyPlaceOfBirthReason": null,
        "custIndividualFamilyPlaceOfBirthKabKotaReason": null,
        "custIndividualFamilyGenderReason": _providerIbu.isRadioValueGenderChanges ? "99" : null,
        "custIndividualFamilyIdentityActiveStartReason": null,
        "custIndividualFamilyIdentityActiveEndReason": null,
        "custIndividualFamilyReligionReason": null,
        "custIndividualFamilyOccupationIDReason": null,
        "custIndividualFamilyPositionIDReason": null,
        "custIndividualFamilyCreatedAtReason": null,
        "custIndividualFamilyCreatedByReason": null,
        "custIndividualFamilyModifiedAtReason": null,
        "custIndividualFamilyModifiedByReason": null,
        "custIndividualFamilyContactTelephoneArea1Reason": _providerIbu.isTlpnChanges ? "99" : null,
        "custIndividualFamilyContactTelephone1Reason": _providerIbu.isTlpnChanges ? "99" : null,
        "custIndividualFamilyContactTelephoneArea2Reason": null,
        "custIndividualFamilyContactTelephone2Reason": null,
        "custIndividualFamilyContactFaxAreaReason": null,
        "custIndividualFamilyContactFaxReason": null,
        "custIndividualFamilyContactHandphoneReason": _providerIbu.isNoHpChanges ? "99" : null,
        "custIndividualFamilyContactEmailReason": null,
        "custIndividualFamilyDedupScoreReason": null,
        "custIndividualFamilyStatusReason": null,
        "familyFlagCorrectionReason": null,
        }
    );

    for(int i=0; i < _providerKeluarga.listFormInfoKel.length; i++){
      _custIndividualCustomerFamiliesReason.add(
          {
            "custIndividualFamilyInfoIDReason": null,
            "custIndividualFamilyRelationshipStatusReason": _providerKeluarga.listFormInfoKel[i].isrelationshipStatusModelChanges ? "99" : null,
            "custIndividualFamilyIdentityTypeReason": _providerKeluarga.listFormInfoKel[i].isidentityModelChanges ? "99" : null,
            "custIndividualFamilyIdentityNumberReason": _providerKeluarga.listFormInfoKel[i].isnoIdentitasChanges ? "99" : null,
            "custIndividualFamilyIdentityNameReason": _providerKeluarga.listFormInfoKel[i].isnamaLengkapSesuaiIdentitasChanges ? "99" : null,
            "custIndividualFamilyFullNameReason": _providerKeluarga.listFormInfoKel[i].isnamaLengkapChanges ? "99" : null,
            "custIndividualFamilyAliasReason": null,
            "custIndividualFamilyTitleReason": null,
            "custIndividualFamilyDateOfBirthReason": _providerKeluarga.listFormInfoKel[i].isbirthDateChanges ? "99" : null,
            "custIndividualFamilyPlaceOfBirthReason": _providerKeluarga.listFormInfoKel[i].istmptLahirSesuaiIdentitasChanges ? "99" : null,
            "custIndividualFamilyPlaceOfBirthKabKotaReason": _providerKeluarga.listFormInfoKel[i].isbirthPlaceModelChanges ? "99" : null,
            "custIndividualFamilyGenderReason": _providerKeluarga.listFormInfoKel[i].isgenderChanges ? "99" : null,
            "custIndividualFamilyIdentityActiveStartReason": null,
            "custIndividualFamilyIdentityActiveEndReason": null,
            "custIndividualFamilyReligionReason": null,
            "custIndividualFamilyOccupationIDReason": null,
            "custIndividualFamilyPositionIDReason": null,
            "custIndividualFamilyCreatedAtReason": null,
            "custIndividualFamilyCreatedByReason": null,
            "custIndividualFamilyModifiedAtReason": null,
            "custIndividualFamilyModifiedByReason": null,
            "custIndividualFamilyContactTelephoneArea1Reason": _providerKeluarga.listFormInfoKel[i].isnoTlpnChanges ? "99" : null,
            "custIndividualFamilyContactTelephone1Reason": _providerKeluarga.listFormInfoKel[i].isnoTlpnChanges ? "99" : null,
            "custIndividualFamilyContactTelephoneArea2Reason": null,
            "custIndividualFamilyContactTelephone2Reason": null,
            "custIndividualFamilyContactFaxAreaReason": null,
            "custIndividualFamilyContactFaxReason": null,
            "custIndividualFamilyContactHandphoneReason": _providerKeluarga.listFormInfoKel[i].isnoHpChanges ? "99" : null,
            "custIndividualFamilyContactEmailReason": null,
            "custIndividualFamilyDedupScoreReason": null,
            "custIndividualFamilyStatusReason": null,
            "familyFlagCorrectionReason": null
          }
      );
    }

    for(int i=0; i < _providerIncome.listIncome.length; i++){
      _customerIncomesReason.add({
        "customerIncomeIDReason": null,
        "customerIncomeFrmlTypeReason": null,
        "customerIncomeTypeReason": null,
        "customerIncomeValueReason": _providerIncome.listIncome[i].edit_income_value == "1" ? "99" : null,
        "customerIncomeStatusReason": null,
        "customerIncomeCreatedAtReason": null,
        "customerIncomeCreatedByReason": null,
        "customerIncomeModifiedAtReason": null,
        "customerIncomeModifiedByReason": null,
        "customerIncomeFlagCorrectionReason": null
      });
    }

    List _orderProductFeesReason = [];
    for(int i=0; i < _providerCreditStructure.listInfoFeeCreditStructure.length; i++){
      _orderProductFeesReason.add(
          {
            "orderProductFeeIDReason": null,
            "orderProductFeeTypeIDReason": _providerCreditStructure.listInfoFeeCreditStructure[i].editFeeType  ? "99" : null,
            "orderProductFeeCashFeeReason": _providerCreditStructure.listInfoFeeCreditStructure[i].editFeeCash ? "99" : null,
            "orderProductFeeCreditFeeReason": _providerCreditStructure.listInfoFeeCreditStructure[i].editFeeCredit ? "99" : null,
            "orderProductTotalFeeReason": _providerCreditStructure.listInfoFeeCreditStructure[i].editTotalFee ? "99" : null,
            "orderProductFeeCreatedAtReason": null,
            "orderProductFeeCreatedByReason": null,
            "orderProductFeeModifiedAtReason": null,
            "orderProductFeeModifiedByReason": null,
            "orderProductFeeStatusReason": null,
            "orderProductFeeFlagCorrectionReason": null
          }
      );
    }

    var _detailAddressesReason = [];
    for (int i = 0; i <_providerInfoAlamatNasabah.listAlamatKorespondensi.length; i++) {
      _detailAddressesReason.add({
        "addressIDReason": null,
        "addressForeignBusinessIDReason": null,
        "addressSpecificationKorespondenReason": null,
        "addressSpecificationMatrixAddrReason": null,
        "addressSpecificationAddressReason": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].isAlamatChanges ? "99" : null,
        "addressSpecificationRtReason": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].isRTChanges ? "99" : null,
        "addressSpecificationRwReason": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].isRWChanges ? "99" : null,
        "addressSpecificationProvinsiIDReason": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].isProvinsiChanges ? "99" : null,
        "addressSpecificationKabkotIDReason": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].isKotaChanges ? "99" : null,
        "addressSpecificationKecamatanIDReason": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].isKecamatanChanges ? "99" : null,
        "addressSpecificationKelurahanIDReason": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].isKelurahanChanges ? "99" : null,
        "addressSpecificationZipcodeReason": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].isPostalCodeChanges ? "99" : null,
        "addressContactSpecificationTelephoneArea1Reason": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].isTeleponChanges ? "99" : null,
        "addressContactSpecificationTelephone1Reason": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].isTeleponAreaChanges ? "99" : null,
        "addressContactSpecificationTelephoneArea2Reason": null,
        "addressContactSpecificationTelephone2Reason": null,
        "addressContactSpecificationFaxAreaReason": null,
        "addressContactSpecificationFaxReason": null,
        "addressContactSpecificationHandphoneReason": null,
        "addressContactSpecificationEmailReason": null,
        "addressTypeReason": _providerInfoAlamatNasabah.listAlamatKorespondensi[i].isAddressTypeChanges ? "99" : null,
        "addressCreationalSpecificationCreatedAtReason": null,
        "addressCreationalSpecificationCreatedByReason": null,
        "addressCreationalSpecificationModifiedAtReason": null,
        "addressCreationalSpecificationModifiedByReason": null,
        "addressStatusReason": null,
        "addressFlagCorrectionReason": null,
      });
    }
    for (int i = 0; i <_providerOccupation.listOccupationAddress.length; i++) {
      _detailAddressesReason.add({
        "addressIDReason": null,
        "addressForeignBusinessIDReason": null,
        "addressSpecificationKorespondenReason": null,
        "addressSpecificationMatrixAddrReason": null,
        "addressSpecificationAddressReason": _providerOccupation.listOccupationAddress[i].isAlamatChanges ? "99" : null,
        "addressSpecificationRtReason": _providerOccupation.listOccupationAddress[i].isRTChanges ? "99" : null,
        "addressSpecificationRwReason": _providerOccupation.listOccupationAddress[i].isRWChanges ? "99" : null,
        "addressSpecificationProvinsiIDReason": _providerOccupation.listOccupationAddress[i].isProvinsiChanges ? "99" : null,
        "addressSpecificationKabkotIDReason": _providerOccupation.listOccupationAddress[i].isKotaChanges ? "99" : null,
        "addressSpecificationKecamatanIDReason": _providerOccupation.listOccupationAddress[i].isKecamatanChanges ? "99" : null,
        "addressSpecificationKelurahanIDReason": _providerOccupation.listOccupationAddress[i].isKelurahanChanges ? "99" : null,
        "addressSpecificationZipcodeReason": _providerOccupation.listOccupationAddress[i].isPostalCodeChanges ? "99" : null,
        "addressContactSpecificationTelephoneArea1Reason": _providerOccupation.listOccupationAddress[i].isTeleponChanges ? "99" : null,
        "addressContactSpecificationTelephone1Reason": _providerOccupation.listOccupationAddress[i].isTeleponAreaChanges ? "99" : null,
        "addressContactSpecificationTelephoneArea2Reason": null,
        "addressContactSpecificationTelephone2Reason": null,
        "addressContactSpecificationFaxAreaReason": null,
        "addressContactSpecificationFaxReason": null,
        "addressContactSpecificationHandphoneReason": null,
        "addressContactSpecificationEmailReason": null,
        "addressTypeReason": _providerOccupation.listOccupationAddress[i].isAddressTypeChanges ? "99" : null,
        "addressCreationalSpecificationCreatedAtReason": null,
        "addressCreationalSpecificationCreatedByReason": null,
        "addressCreationalSpecificationModifiedAtReason": null,
        "addressCreationalSpecificationModifiedByReason": null,
        "addressStatusReason": null,
        "addressFlagCorrectionReason": null,
      });
    }

    for(int i=0; i < _providerGuarantor.listGuarantorIndividual.length; i++){
      for(int j=0; j < _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel.length; j++){
        _detailAddressesReason.add({
          "addressIDReason": null,
          "addressForeignBusinessIDReason": null,
          "addressSpecificationKorespondenReason": null,
          "addressSpecificationMatrixAddrReason": null,
          "addressSpecificationAddressReason": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isAlamatChanges ? "99" : null,
          "addressSpecificationRtReason": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isRTChanges ? "99" : null,
          "addressSpecificationRwReason": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isRWChanges ? "99" : null,
          "addressSpecificationProvinsiIDReason": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isProvinsiChanges ? "99" : null,
          "addressSpecificationKabkotIDReason": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isKotaChanges ? "99" : null,
          "addressSpecificationKecamatanIDReason": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isKecamatanChanges ? "99" : null,
          "addressSpecificationKelurahanIDReason": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isKelurahanChanges ? "99" : null,
          "addressSpecificationZipcodeReason": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isPostalCodeChanges ? "99" : null,
          "addressContactSpecificationTelephoneArea1Reason": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isTeleponAreaChanges ? "99" : null,
          "addressContactSpecificationTelephone1Reason": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isTeleponChanges ? "99" : null,
          "addressContactSpecificationTelephoneArea2Reason": null,
          "addressContactSpecificationTelephone2Reason": null,
          "addressContactSpecificationFaxAreaReason": null,
          "addressContactSpecificationFaxReason": null,
          "addressContactSpecificationHandphoneReason": null,
          "addressContactSpecificationEmailReason": null,
          "addressTypeReason": _providerGuarantor.listGuarantorIndividual[i].listAddressGuarantorModel[j].isAddressTypeChanges ? "99" : null,
          "addressCreationalSpecificationCreatedAtReason": null,
          "addressCreationalSpecificationCreatedByReason": null,
          "addressCreationalSpecificationModifiedAtReason": null,
          "addressCreationalSpecificationModifiedByReason": null,
          "addressStatusReason": null,
          "addressFlagCorrectionReason": null,
        });
      }
    }

    for(int i=0; i < _providerGuarantor.listGuarantorCompany.length; i++){
      for(int j=0; j < _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel.length; j++){
        _detailAddressesReason.add({
          "addressIDReason": null,
          "addressForeignBusinessIDReason": null,
          "addressSpecificationKorespondenReason": null,
          "addressSpecificationMatrixAddrReason": null,
          "addressSpecificationAddressReason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isAddressDakor ? "99" : null,
          "addressSpecificationRtReason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isRTDakor ? "99" : null,
          "addressSpecificationRwReason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isRWDakor ? "99" : null,
          "addressSpecificationProvinsiIDReason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isProvinsiDakor ? "99" : null,
          "addressSpecificationKabkotIDReason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isKabKotDakor ? "99" : null,
          "addressSpecificationKecamatanIDReason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isKecamatanDakor ? "99" : null,
          "addressSpecificationKelurahanIDReason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isKelurahanDakor ? "99" : null,
          "addressSpecificationZipcodeReason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isKodePosDakor ? "99" : null,
          "addressContactSpecificationTelephoneArea1Reason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isTelp1AreaDakor ? "99" : null,
          "addressContactSpecificationTelephone1Reason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isTelp1Dakor ? "99" : null,
          "addressContactSpecificationTelephoneArea2Reason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isTelp2AreaDakor ? "99" : null,
          "addressContactSpecificationTelephone2Reason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isTelp2Dakor ? "99" : null,
          "addressContactSpecificationFaxAreaReason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isFaxAreaDakor ? "99" : null,
          "addressContactSpecificationFaxReason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isFaxDakor ? "99" : null,
          "addressContactSpecificationHandphoneReason": null,
          "addressContactSpecificationEmailReason": null,
          "addressTypeReason": _providerGuarantor.listGuarantorCompany[i].listAddressGuarantorModel[j].isAddressTypeDakor ? "99" : null,
          "addressCreationalSpecificationCreatedAtReason": null,
          "addressCreationalSpecificationCreatedByReason": null,
          "addressCreationalSpecificationModifiedAtReason": null,
          "addressCreationalSpecificationModifiedByReason": null,
          "addressStatusReason": null,
          "addressFlagCorrectionReason": null,
        });
      }
    }

    List _customerGuarantorCorporatesReason = [];
    List _customerGuarantorIndividualsReason = [];

    for(int i=0; i < _providerGuarantor.listGuarantorCompany.length; i++){
      _customerGuarantorCorporatesReason.add(
          {
            "guarantorCorporateIDReason": null,
            "guarantorCorporateDedupScoreReason": null,
            "guarantorCorporateBussinessPermitInstitutionTypeReason": _providerGuarantor.listGuarantorCompany[i].companyTypeDakor ? "99" : null,
            "guarantorCorporateBussinessPermitProfileReason": null,
            "guarantorCorporateBussinessPermitFullNameReason": _providerGuarantor.listGuarantorCompany[i].companyNameDakor ? "99" : null,
            "guarantorCorporateBussinessPermitDeedOfIncorporationNumberReason": null,
            "guarantorCorporateBussinessPermitDeedEndDateReason": null,
            "guarantorCorporateBussinessPermitSiupNumberReason": null,
            "guarantorCorporateBussinessPermitSiupStartDateReason": null,
            "guarantorCorporateBussinessPermitTdpNumberReason": null,
            "guarantorCorporateBussinessPermitTdpStartDateReason": null,
            "guarantorCorporateBussinessPermitEstablishmentDateReason": _providerGuarantor.listGuarantorCompany[i].establishedDateDakor ? "99" : null,
            "guarantorCorporateBussinessPermitIsCompanyReason": null,
            "guarantorCorporateBussinessPermitAuthorizedCapitalReason": null,
            "guarantorCorporateBussinessPermitPaidCapitalReason": null,
            "guarantorCorporateBusinessEconomySectorReason": null,
            "guarantorCorporateBusinessBusinessFieldReason": null,
            "guarantorCorporateBusinessLocationStatusReason": null,
            "guarantorCorporateBusinessBusinessLocationReason": null,
            "guarantorCorporateBusinessBusinessEmployeeTotalReason": null,
            "guarantorCorporateBusinessBussinessLengthInMonthReason": null,
            "guarantorCorporateBusinessTotalBussinessLengthInMonthReason": null,
            "guarantorCorporateHasNpwpReason": _providerGuarantor.listGuarantorCompany[i].npwpDakor ? "99" : null,
            "guarantorCorporateNpwpAddressReason": null,
            "guarantorCorporateNpwpFullnameReason": null,
            "guarantorCorporateNpwpNumberReason": null,
            "guarantorCorporateNpwpTypeReason": null,
            "guarantorCorporatePkpIdentifierReason": null,
            "guarantorCorporateCreationalCreatedAtReason": null,
            "guarantorCorporateCreationalCreatedByReason": null,
            "guarantorCorporateCreationalModifiedAtReason": null,
            "guarantorCorporateCreationalModifiedByReason": null,
            "guarantorCorporateStatusReason": null,
            "guarantorCorporateFlagCorrectionReason": null,
          }
      );
    }

    for(int i=0; i < _providerGuarantor.listGuarantorIndividual.length; i++){
      _customerGuarantorIndividualsReason.add({
        "guarantorIndividualIDReason": null,
        "guarantorIndividualRelationshipStatusReason": _providerGuarantor.listGuarantorIndividual[i].relationshipStatusDakor ? "99" : null,
        "guarantorIndividualDedupScoreReason": null,
        "guarantorIndividualIdentityIdentityTypeReason": _providerGuarantor.listGuarantorIndividual[i].identityTypeDakor ? "99" : null,
        "guarantorIndividualIdentityIdentityNumberReason": _providerGuarantor.listGuarantorIndividual[i].identityNoDakor ? "99" : null,
        "guarantorIndividualIdentityIdentityNameReason": _providerGuarantor.listGuarantorIndividual[i].fullnameIDDakor ? "99" : null,
        "guarantorIndividualIdentityFullNameReason": _providerGuarantor.listGuarantorIndividual[i].fullNameDakor ? "99" : null,
        "guarantorIndividualIdentityAliasReason": null,
        "guarantorIndividualIdentityTitleReason": null,
        "guarantorIndividualIdentityDateOfBirthReason": _providerGuarantor.listGuarantorIndividual[i].dateOfBirthDakor ? "99" : null,
        "guarantorIndividualIdentityPlaceOfBirthReason": _providerGuarantor.listGuarantorIndividual[i].placeOfBirthDakor ? "99" : null,
        "guarantorIndividualIdentityPlaceOfBirthKabKotaReason": _providerGuarantor.listGuarantorIndividual[i].placeOfBirthLOVDakor ? "99" : null,
        "guarantorIndividualIdentityGenderReason": _providerGuarantor.listGuarantorIndividual[i].genderDakor ? "99" : null,
        "guarantorIndividualIdentityIdentityActiveStartReason": null,
        "guarantorIndividualIdentityIdentityActiveEndReason": null,
        "guarantorIndividualIdentityReligionReason": null,
        "guarantorIndividualIdentityOccupationIDReason": null,
        "guarantorIndividualIdentityPositionIDReason": null,
        "guarantorIndividualContactTelephoneArea1Reason": null,
        "guarantorIndividualContactTelephone1Reason": null,
        "guarantorIndividualContactTelephoneArea2Reason": null,
        "guarantorIndividualContactTelephone2Reason": null,
        "guarantorIndividualContactFaxAreaReason": null,
        "guarantorIndividualContactFaxReason": null,
        "guarantorIndividualContactHandphoneReason": _providerGuarantor.listGuarantorIndividual[i].phoneDakor ? "99" : null,
        "guarantorIndividualContactEmailReason": null,
        "guarantorIndividualCreationalCreatedAtReason": null,
        "guarantorIndividualCreationalCreatedByReason": null,
        "guarantorIndividualCreationalModifiedAtReason": null,
        "guarantorIndividualCreationalModifiedByReason": null,
        "guarantorIndividualStatusReason": null,
        "guarantorIndividualFlagCorrectionReason": null,
      });
    }

    try{
      var _oldApplicationDTO = await OldApplicationDTOProvider().oldApplication(_providerApp.applNo);

      var _body = jsonEncode({
        "oldApplicationDTO": _oldApplicationDTO,
        "newApplicationDTO": {
          "customerDTO": {
            "customerID": _preferences.getString("last_known_state") != "IDE" ? _providerInfoNasabah.customerID :"NEW",// perlu di rencanakan mau ditaruh di sqlite atau di sharedPreference
            "customerType": _preferences.getString("cust_type") == "PER" ? "PER" : "COM",
            "customerSpecification": {
              "customerNPWP": {
                "npwpAddress": _npwpAddress,//_providerInfoNasabah.radioValueIsHaveNPWP == 1 ? _providerInfoNasabah.controllerAlamatSesuaiNPWP.text : _providerInfoAlamat.listAlamatKorespondensi[0].address,
                "npwpFullname": _npwpFullName,//_providerInfoNasabah.radioValueIsHaveNPWP == 1 ? _providerInfoNasabah.controllerNamaSesuaiNPWP.text  : _providerInfoNasabah.controllerNamaLengkapSesuaiIdentitas.text,
                "npwpNumber": _npwpNumber,//_providerInfoNasabah.radioValueIsHaveNPWP == 1 ? _providerInfoNasabah.controllerNoNPWP.text : "00000000000000",
                "npwpType": _npwpType,
                "hasNpwp": _providerInfoNasabah.radioValueIsHaveNPWP == 1 ? true : false,//_providerInfoNasabah.radioValueIsHaveNPWP == 1 ? true : false,
                "pkpIdentifier": _preferences.getString("cust_type") == "PER" ? _providerInfoNasabah.tandaPKPSelected != null ? _providerInfoNasabah.tandaPKPSelected.id : "2" : _providerRincian.signPKPSelected != null ? _providerRincian.signPKPSelected.id : "2",
              },
              "detailAddresses": _detailAddressesInfoNasabah,
              "customerGuarantor": {
                "isGuarantor": _providerGuarantor.radioValueIsWithGuarantor == 0,
                "guarantorCorporates": _guarantorCorporates,
                "guarantorIndividuals": _guarantorIndividuals
              }
            },
            "customerIndividualDTO": _preferences.getString("cust_type") == "PER"
                ?
            [
              {
                "customerIndividualID": _preferences.getString("last_known_state") != "IDE" ? _providerInfoNasabah.customerIndividualID : "CUST001", // NEW
                "numberOfDependents": _providerInfoNasabah.controllerJumlahTanggungan.text != "" ? _providerInfoNasabah.controllerJumlahTanggungan.text : "0",
                "groupCustomer": _providerInfoNasabah.gcModel.id,
                "statusRelationship": _providerInfoNasabah.maritalStatusSelected.id,
                "educationID": _providerInfoNasabah.educationSelected.id,
                "customerIdentity": {
                  "identityType": _providerInfoNasabah.identitasModel.id,
                  "identityNumber": _providerInfoNasabah.controllerNoIdentitas.text,
                  "identityName": _providerInfoNasabah.controllerNamaLengkapSesuaiIdentitas.text,
                  "fullName": _providerInfoNasabah.controllerNamaLengkap.text,
                  "alias": null,
                  "title": null,
                  "dateOfBirth": formatDateValidateAndSubmit(_providerInfoNasabah.initialDateForTglLahir), //_providerInfoNasabah.controllerTglLahir.text,
                  "placeOfBirth": _providerInfoNasabah.controllerTempatLahirSesuaiIdentitas.text,
                  "placeOfBirthKabKota": _providerInfoNasabah.birthPlaceSelected.KABKOT_ID,
                  "gender": _providerInfoNasabah.radioValueGender,
                  "identityActiveStart": _preferences.getString("cust_type") == "PER" ? _providerInfoNasabah.controllerTglIdentitas.text != "" ? formatDateValidateAndSubmit(_providerInfoNasabah.initialDateForTglIdentitas) : formatDateValidateAndSubmit(DateTime.now()) : formatDateValidateAndSubmit(DateTime.now()),
                  "identityActiveEnd": _preferences.getString("cust_type") == "PER" ? _providerInfoNasabah.valueCheckBox ? "31-12-2999 00:00:00" : _providerInfoNasabah.controllerIdentitasBerlakuSampai.text != "" ? formatDateValidateAndSubmit(_providerInfoNasabah.initialDateForTglIdentitas) : "31-12-2999 00:00:00" : "31-12-2999 00:00:00", // berdasarkan issue jira 1382 = diganti jadi 31-12-2999 00:00:00
                  "religion": _providerInfoNasabah.religionSelected != null ? _providerInfoNasabah.religionSelected.id : "99", // nilai defaultnya 01, per tanggal 06.05.2021 diganti kosong, per tanggal 07.05.2021 diganti 99
                  "occupationID": _providerFoto.occupationSelected.KODE,
                  "positionID": null,
                  "isLifetime": _providerInfoNasabah.valueCheckBox
                },
                "customerContact": {
                  "telephoneArea1": "",
                  "telephone1": "",
                  "telephoneArea2": null,
                  "telephone2": null,
                  "faxArea": null,
                  "fax": null,
                  "handphone": _providerInfoNasabah.controllerNoHp.text != "" ? "08${_providerInfoNasabah.controllerNoHp.text}" : "",
                  "email": _providerInfoNasabah.controllerEmail.text != "" ? _providerInfoNasabah.controllerEmail.text : "",
                  "noWa": _providerInfoNasabah.isNoHp1WA ? 1 : 0,
                  "noWa2": null,
                  "noWa3": null
                },
                "customerOccupation": [
                  {
                    "customerOccupationID": _preferences.getString("last_known_state") != "IDE" ? _providerOccupation.customerOccupationID : "IP001",
                    "occupationID": _preferences.getString("cust_type") == "PER" ? _providerFoto.occupationSelected.KODE : "",
                    "businessName": _providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07" ? _providerOccupation.controllerBusinessName.text : _providerFoto.occupationSelected.KODE != "08" ? _providerOccupation.controllerCompanyName.text : null,// _preferences.getString("cust_type") == "PER" ? _providerOccupation.controllerBusinessName.text : null,
                    "businessType": _providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07"
                        ? _providerOccupation.typeOfBusinessModelSelected.id
                        : _providerFoto.occupationSelected.KODE != "08"
                        ? _providerOccupation.companyTypeModelSelected.id : null,//_preferences.getString("cust_type") == "PER" ? _providerRincian.typeInstitutionSelected == null ? null : _providerRincian.typeInstitutionSelected.PARA_ID : null,
                    "professionType": _providerFoto.occupationSelected != null
                        ? _providerFoto.occupationSelected.KODE == "08"
                        ? _providerOccupation.professionTypeModelSelected.id : null
                        : null,
                    "pepType": _providerFoto.occupationSelected.KODE != "05" || _providerFoto.occupationSelected.KODE != "07" || _providerFoto.occupationSelected.KODE != "08" ? _providerOccupation.pepModelSelected == null ? "" : _providerOccupation.pepModelSelected.KODE : "",
                    "employeeStatus": _preferences.getString("cust_type") == "PER" ?_providerOccupation.employeeStatusModelSelected != null ? _providerOccupation.employeeStatusModelSelected.id : "" : "",
                    "businessSpecification": {
                      "economySector": _providerOccupation.sectorEconomicModelSelected != null ? _providerOccupation.sectorEconomicModelSelected.KODE : null,
                      "businessField": _preferences.getString("cust_type") == "PER" ? _providerOccupation.businessFieldModelSelected == null ? null : _providerOccupation.businessFieldModelSelected.KODE : null,
                      "locationStatus": _preferences.getString("cust_type") == "PER" ? _providerOccupation.statusLocationModelSelected == null ? null : _providerOccupation.statusLocationModelSelected.id : null,
                      "businessLocation": _preferences.getString("cust_type") == "PER" ? _providerOccupation.businessLocationModelSelected != null ? _providerOccupation.businessLocationModelSelected.id : null : null,
                      "employeeTotal": _preferences.getString("cust_type") == "PER" ? double.parse(_providerOccupation.controllerEmployeeTotal.text == '' ? '0' : _providerOccupation.controllerEmployeeTotal.text.replaceAll(",", "")) : 0,
                      "bussinessLengthInMonth": _preferences.getString("cust_type") == "PER" ? double.parse(_providerOccupation.controllerEstablishedDate.text == '' ? '0' : _providerOccupation.controllerEstablishedDate.text.replaceAll(",", "").toString()) : 0,
                      "totalBussinessLengthInMonth": _preferences.getString("cust_type") == "PER" ? _providerOccupation.controllerTotalEstablishedDate.text == '' ? 0 : int.parse(_providerOccupation.controllerTotalEstablishedDate.text) : 0
                    },
                    "creationalSpecification": {
                      "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                      "createdBy": _preferences.getString("username"),
                      "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                      "modifiedBy": _preferences.getString("username"),
                    },
                    "status": "ACTIVE"
                  }
                ],
                "customerIndividualAdditionalSpecification": null,
                "customerFamilies": _familyInfoID,
                "creationalSpecification": {
                  "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                  "createdBy": _preferences.getString("username"),
                  "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                  "modifiedBy": _preferences.getString("username"),
                },
                "status": "ACTIVE"
              }
            ]
                :
            [],
            "customerCorporateDTO": _preferences.getString("cust_type") == "PER" ? []:[
              {
                "customerCorporateID": _preferences.getString("last_known_state") != "IDE" ? _providerRincian.customerCorporateID : "NEW",
                "businessSpecification": {
                  "economySector": _providerRincian.sectorEconomicModelSelected != null ? _providerRincian.sectorEconomicModelSelected.KODE : null,//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.sectorEconomicModelSelected.KODE,
                  "businessField": _providerRincian.businessFieldModelSelected != null ? _providerRincian.businessFieldModelSelected.KODE : null ,//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.businessFieldModelSelected.KODE,
                  "locationStatus": _providerRincian.locationStatusSelected != null ?  _providerRincian.locationStatusSelected.id : null,//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.locationStatusSelected.id,
                  "businessLocation": _providerRincian.businessLocationSelected != null ? _providerRincian.businessLocationSelected.id : null,//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.businessLocationSelected.id,
                  "employeeTotal": _providerRincian.controllerTotalEmployees.text != "" ? int.parse(_providerRincian.controllerTotalEmployees.text) : null,//_preferences.getString("cust_type") == "PER" ? 0 : double.parse(_providerRincian.controllerTotalEmployees.text.replaceAll(",", "")),
                  "bussinessLengthInMonth": _providerRincian.controllerLamaUsahaBerjalan.text != "" ? int.parse(_providerRincian.controllerLamaUsahaBerjalan.text) : null,//_preferences.getString("cust_type") == "PER" ? 0 : double.parse(_providerRincian.controllerLamaUsahaBerjalan.text.replaceAll(",", "")),
                  "totalBussinessLengthInMonth": _providerRincian.controllerTotalLamaUsahaBerjalan.text != "" ? int.parse(_providerRincian.controllerTotalLamaUsahaBerjalan.text) : null,//_preferences.getString("cust_type") == "PER" ? 0 : double.parse(_providerRincian.controllerTotalLamaUsahaBerjalan.text.replaceAll(",", "")),
                },
                "businessPermitSpecification": {
                  "institutionType": _providerRincian.typeInstitutionSelected != null ?  _providerRincian.typeInstitutionSelected.PARA_ID : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.typeInstitutionSelected.PARA_ID,
                  "profile": _providerRincian.profilSelected != null ? _providerRincian.profilSelected.id : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.profilSelected.id,
                  "fullName": _providerRincian.controllerInstitutionName.text != "" ? _providerRincian.controllerInstitutionName.text : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.controllerInstitutionName.text,
                  "deedOfIncorporationNumber": null,
                  "deedEndDate": null,
                  "siupNumber": null,
                  "siupStartDate": null,
                  "tdpNumber": null,
                  "tdpStartDate": null,
                  "establishmentDate": formatDateValidateAndSubmit(_providerRincian.initialDateForDateEstablishment),//_preferences.getString("cust_type") == "PER" ? "" : _providerRincian.controllerDateEstablishment.text,
                  "isCompany": false,
                  "authorizedCapital": 0,
                  "paidCapital": 0
                },
                "managementPIC": {
                  "picIdentity": {
                    "identityType": _providerManajemenPIC.typeIdentitySelected != null ? _providerManajemenPIC.typeIdentitySelected.id : "",
                    "identityNumber":  _providerManajemenPIC.controllerIdentityNumber.text != "" ? _providerManajemenPIC.controllerIdentityNumber.text : "",
                    "identityName": _providerManajemenPIC.controllerFullNameIdentity.text != "" ? _providerManajemenPIC.controllerFullNameIdentity.text : "",
                    "fullName": _providerManajemenPIC.controllerFullName.text != "" ? _providerManajemenPIC.controllerFullName.text : "",
                    "alias": null,
                    "title": null,
                    "dateOfBirth": _providerManajemenPIC.controllerBirthOfDate.text != "" ? formatDateValidateAndSubmit(_providerManajemenPIC.initialDateForBirthOfDate) : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.controllerBirthOfDate.text,
                    "placeOfBirth": _providerManajemenPIC.controllerPlaceOfBirthIdentity.text != "" ? _providerManajemenPIC.controllerPlaceOfBirthIdentity.text : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.controllerPlaceOfBirthIdentity.text,
                    "placeOfBirthKabKota": _providerManajemenPIC.birthPlaceSelected != null ? _providerManajemenPIC.birthPlaceSelected.KABKOT_ID : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.birthPlaceSelected.KABKOT_ID,
                    "gender": null,
                    "identityActiveStart": null,
                    "identityActiveEnd": null,
                    "isLifetime": null,
                    "religion": null,
                    "occupationID": null,
                    "positionID": "02",
                    "maritalStatusID": "02"
                  },
                  "picContact": {
                    "telephoneArea1": null,
                    "telephone1": null,
                    "telephoneArea2": null,
                    "telephone2": null,
                    "faxArea": null,
                    "fax": null,
                    "handphone": _providerManajemenPIC.controllerHandphoneNumber.text != "" ? "08${_providerManajemenPIC.controllerHandphoneNumber.text}" : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.controllerHandphoneNumber.text,
                    "email": _providerManajemenPIC.controllerEmail.text != "" ? _providerManajemenPIC.controllerEmail.text : "",//_preferences.getString("cust_type") == "PER" ? "" : _providerManajemenPIC.controllerEmail.text,
                    "noWa": null,
                    "noWa2": null,
                    "noWa3": null
                  },
                  "picAddresses": _detailAddressPIC,
                  "shareholding": 0
                },
                "shareholdersCorporates": [],
                "shareholdersIndividuals": [
                  // {
                  //   "shareholdersIndividualID": "NEW",
                  //   "shareholding": _providerPemegangSaham.controllerPercentShare.text != "" ? double.parse(_providerPemegangSaham.controllerPercentShare.text.replaceAll(",", "")) : 0,
                  //   "dedupScore": 0,
                  //   "relationshipStatus": _providerPemegangSaham.relationshipStatusSelected != null ? _providerPemegangSaham.relationshipStatusSelected.PARA_FAMILY_TYPE_ID : "",
                  //   "shareholdersIndividualIdentity": {
                  //     "identityType": _providerPemegangSaham.typeIdentitySelected != null ? _providerPemegangSaham.typeIdentitySelected.id : "",
                  //     "identityNumber": _providerPemegangSaham.controllerIdentityNumber.text != "" ?_providerPemegangSaham.controllerIdentityNumber.text : "",
                  //     "identityName": _providerPemegangSaham.controllerFullNameIdentity.text != "" ? _providerPemegangSaham.controllerFullNameIdentity.text : "",
                  //     "fullName": _providerPemegangSaham.controllerFullName.text != "" ? _providerPemegangSaham.controllerFullName.text : "",
                  //     "alias": null,
                  //     "title": null,
                  //     "dateOfBirth": _providerPemegangSaham.controllerBirthOfDate.text != "" ? formatDateValidateAndSubmit(_providerPemegangSaham.initialDateForBirthOfDate) : "",
                  //     "placeOfBirth": _providerPemegangSaham.controllerPlaceOfBirthIdentity.text != "" ? _providerPemegangSaham.controllerPlaceOfBirthIdentity.text : "",
                  //     "placeOfBirthKabKota": _providerPemegangSaham.birthPlaceSelected != null ? _providerPemegangSaham.birthPlaceSelected.KABKOT_ID : "",
                  //     "gender": "${_providerPemegangSaham.radioValueGender}",
                  //     "identityActiveStart": null,
                  //     "identityActiveEnd": null,
                  //     "religion": null,
                  //     "occupationID": "0",
                  //     "positionID": null,
                  //     "maritalStatusID": ""
                  //   },
                  //   "shareholderIndividualContact": {
                  //     "telephoneArea1": null,
                  //     "telephone1": null,
                  //     "telephoneArea2": null,
                  //     "telephone2": null,
                  //     "faxArea": null,
                  //     "fax": null,
                  //     "handphone": null,
                  //     "email": null
                  //   },
                  //   "shareholderIndividualAddresses": _detailAddressShareHolderIndividu,
                  //   "creationalSpecification": {
                  //     "createdAt":  formatDateValidateAndSubmit(DateTime.now()),
                  //     "createdBy": _preferences.getString("username"),
                  //     "modifiedAt":  formatDateValidateAndSubmit(DateTime.now()),
                  //     "modifiedBy": _preferences.getString("username"),
                  //   },
                  //   "status": "ACTIVE"
                  // }
                ],
                "dedupScore": 0.0,
                "status": "ACTIVE",
                "creationalSpecification": {
                  "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                  "createdBy": _preferences.getString("username"),
                  "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                  "modifiedBy": _preferences.getString("username"),
                },
              }
            ],
            "customerAdditionalInformation": null,
            "customerProcessingCompletion": {
              "lastKnownCustomerState": "CUSTOMER_GUARANTOR",
              "nextKnownCustomerState": "CUSTOMER_GUARANTOR"
            },
            "creationalSpecification": {
              "createdAt": formatDateValidateAndSubmit(DateTime.now()),
              "createdBy": _preferences.getString("username"),
              "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
              "modifiedBy": _preferences.getString("username"),
            },
            "status": "ACTIVE",
            "obligorID": _providerInfoNasabah.obligorID, // _preferences.getString("last_known_state") != "IDE" ? _providerInfoNasabah.obligorID != "NEW" ? _providerInfoNasabah.obligorID : _preferences.getString("oid").toString() : "NEW",
            "exposureInformations": null,
            "customerIncomes": _customerIncomes
          },
          "orderDTO": {
            "orderID": _preferences.getString("last_known_state") != "IDE" ? _providerApp.applNo : orderId,
            "customerID": _preferences.getString("last_known_state") != "IDE" ? _providerInfoNasabah.customerID : "NEW",
            "orderSpecification": {
              "flagDakorCA": 1,
              "aosApprovalStatus": _preferences.getString("last_known_state") == "AOS" ? _providerSurvey.radioValueApproved : "",
              "isMayor": false, // _preferences.getString("last_known_state") == "AOS" && _providerSurvey.radioValueApproved == "0" ? true : | _preferences.getString("is_mayor") == "0",
              "hasFiducia": false,
              "applicationSource": "003",
              "orderDate": "${_providerApp.controllerOrderDate.text} 00:00:00",//di edit karena tgl orderDate lebih besar dr surveyAppointmentDate <formatDateValidateAndSubmit(_providerApp.initialDateOrder)>
              "applicationDate": "${_providerApp.controllerOrderDate.text} 00:00:00",// di edit karena tgl applicationDate lebih besar dr surveyAppointmentDate <formatDateValidateAndSubmit(_providerApp.initialDateOrder)>
              "surveyAppointmentDate": "${_providerApp.controllerSurveyAppointmentDate.text}" " ${_providerApp.controllerSurveyAppointmentTime.text}:00", //formatDateValidateAndSubmit(_providerApp.initialSurveyDate),
              "orderType": _preferences.getString("status_aoro") != null ? _preferences.getString("status_aoro") : "0",//_preferences.getString("status_aoro"),
              "isOpenAccount": true,
              "accountFormNumber": null,
              "sentraCID": _preferences.getString("SentraD"),
              "unitCID": _preferences.getString("UnitD"),
              "initialRecommendation": _providerApp.initialRecomendation,
              "finalRecommendation": _providerApp.finalRecomendation,
              "brmsScoring": _providerApp.brmsScoring,
              "isInPlaceApproval": false,
              "isPkSigned": _providerApp.isSignedPK,
              "maxApprovalLevel": null,
              "jenisKonsep": _providerFoto.jenisKonsepSelected != null ? _providerFoto.jenisKonsepSelected.id : "",
              "jumlahObjek": _providerApp.controllerTotalObject.text == "" ? null : int.parse(_providerApp.controllerTotalObject.text),
              "jenisProporsionalAsuransi": _providerApp.proportionalTypeOfInsuranceModelSelected != null ? _providerApp.proportionalTypeOfInsuranceModelSelected.kode : "",
              "unitKe": _providerApp.numberOfUnitSelected == "" ? null : int.parse(_providerApp.numberOfUnitSelected),
              "isWillingToAcceptInfo": false,
              "applicationContractNumber": null,
              "dealerNote": Provider.of<MarketingNotesChangeNotifier>(context,listen: false).controllerMarketingNotes.text,
              "userDealer": "",
              "orderDealer": "",
              "flagSourceMS2": "103", // diganti dari 006 ke 103 sesuai arahan mas Oji (per tanggal 06.07.2021)
              "orderSupportingDocuments": _orderSupportingDocuments,
              "collateralTypeID": _providerKolateral.collateralTypeModel.id,
              "applicationDocVerStatus": null
            },
            "orderProducts": [
              {
                "orderProductID": _preferences.getString("last_known_state") != "IDE" ? _providerUnitObject.applObjtID : orderProductId, // jika IDE ngambil dari nomer unit, selain IDE ngambil dari APPL_NO get data from DB API
                "orderProductSpecification": {
                  "financingTypeID": _preferences.getString("cust_type") == "PER" ? _providerFoto.typeOfFinancingModelSelected.financingTypeId : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId,
                  "ojkBusinessTypeID": _preferences.getString("cust_type") == "PER" ? _providerFoto.kegiatanUsahaSelected.id.toString() : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId.toString(),
                  "ojkBussinessDetailID": _preferences.getString("cust_type") == "PER" ? _providerFoto.jenisKegiatanUsahaSelected.id.toString() : _providerUnitObject.typeOfFinancingModelSelected.financingTypeId,
                  "orderUnitSpecification": {
                    "objectGroupID": _providerUnitObject.groupObjectSelected != null ? _providerUnitObject.groupObjectSelected.KODE : "",
                    "objectID": _providerUnitObject.objectSelected != null ? _providerUnitObject.objectSelected.id : "",
                    "productTypeID": _providerUnitObject.productTypeSelected != null ? _providerUnitObject.productTypeSelected.id : "",
                    "objectBrandID": _providerUnitObject.brandObjectSelected != null ? _providerUnitObject.brandObjectSelected.id : "",
                    "objectTypeID": _providerUnitObject.objectTypeSelected != null ? _providerUnitObject.objectTypeSelected.id : "",
                    "objectModelID": _providerUnitObject.modelObjectSelected != null ? _providerUnitObject.modelObjectSelected.id : "",
                    "objectUsageID": _providerUnitObject.objectUsageModel != null ? _providerUnitObject.objectUsageModel.id : "", // field pemakaian objek (unit)
                    "objectPurposeID": _providerUnitObject.objectPurposeSelected != null ? _providerUnitObject.objectPurposeSelected.id : "", // field tujuan objek (unit)
                    "modelDetail": _providerUnitObject.controllerDetailModel.text == "" ? null : _providerUnitObject.controllerDetailModel.text
                  },
                  "salesGroupID": _providerUnitObject.groupSalesSelected != null ? _providerUnitObject.groupSalesSelected.kode : "",
                  "orderSourceID": _providerUnitObject.sourceOrderSelected != null ? _providerUnitObject.sourceOrderSelected.kode : "",
                  "orderSourceName": _providerUnitObject.sourceOrderNameSelected != null ? _providerUnitObject.sourceOrderNameSelected.kode : "",
                  "orderProductDealerSpecification": {
                    "isThirdParty": false,
                    "thirdPartyTypeID": _providerUnitObject.thirdPartyTypeSelected != null ? _providerUnitObject.thirdPartyTypeSelected.kode : "",
                    "thirdPartyID": _providerUnitObject.thirdPartySelected != null ? _providerUnitObject.thirdPartySelected.kode : "",
                    "thirdPartyActivity": _providerUnitObject.activitiesModel != null ? _providerUnitObject.activitiesModel.kode : null,
                    "sentradID": _preferences.getString("SentraD"), //_providerUnitObject.thirdPartySelected != null ? _providerUnitObject.thirdPartySelected.sentraId : "",
                    "unitdID": _preferences.getString("UnitD"), //_providerUnitObject.thirdPartySelected != null ? _providerUnitObject.thirdPartySelected.unitId : "",
                    "dealerMatrix": _providerUnitObject.matriksDealerSelected != null ? _providerUnitObject.matriksDealerSelected.kode : ""
                  },
                  "programID": _providerUnitObject.programSelected != null ? _providerUnitObject.programSelected.kode : "",
                  "rehabType": _providerUnitObject.rehabTypeSelected != null ? _providerUnitObject.rehabTypeSelected.id : "",
                  "referenceNumber": _providerUnitObject.referenceNumberModel != null ? _providerUnitObject.referenceNumberModel.noPK : null,
                  "applicationUnitContractNumber": null,
                  "orderProductSaleses": _orderProductSaleses,
                  "orderKaroseris": _orderKaroseris,
                  "orderProductInsurances": _orderProductInsurances,
                  "orderWmps": _orderWmps,
                  "groupID": _providerUnitObject.grupIdSelected != null ? _providerUnitObject.grupIdSelected.kode : null
                },
                "orderCreditStructure": {
                  "installmentTypeID": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.id : "", // sebelumnya ngambil by kode
                  "tenor": _providerCreditStructure.periodOfTimeSelected != null ? int.parse(_providerCreditStructure.periodOfTimeSelected) : "",
                  "paymentMethodID": _providerCreditStructure.paymentMethodSelected != null ? _providerCreditStructure.paymentMethodSelected.id : "", // sebelumnya ngambil by kode
                  "effectiveRate": _providerCreditStructure.controllerInterestRateEffective.text != "" ? double.parse(_providerCreditStructure.controllerInterestRateEffective.text) : "",
                  "flatRate": _providerCreditStructure.controllerInterestRateFlat.text != "" ? double.parse(_providerCreditStructure.controllerInterestRateFlat.text) : "",//0.48501
                  "objectPrice": _providerCreditStructure.controllerObjectPrice.text != "" ? double.parse(_providerCreditStructure.controllerObjectPrice.text.replaceAll(",", "")) : 0,
                  "karoseriTotalPrice": _providerCreditStructure.controllerKaroseriTotalPrice.text != "" ? double.parse(_providerCreditStructure.controllerKaroseriTotalPrice.text.replaceAll(",", "")) : 0,
                  "totalPrice": _providerCreditStructure.controllerTotalPrice.text != "" ? double.parse(_providerCreditStructure.controllerTotalPrice.text.replaceAll(",", "")) : 0,
                  "nettDownPayment": _providerCreditStructure.controllerNetDP.text != "" ? double.parse(_providerCreditStructure.controllerNetDP.text.replaceAll(",", "")) : 0,
                  "declineNInstallment": _providerCreditStructure.installmentDeclineN,
                  "paymentOfYear": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.id == "04" ? int.parse(_providerCreditStructure.controllerPaymentPerYear.text) : 1 : 1,
                  "branchDownPayment": _providerCreditStructure.controllerBranchDP.text != "" ? double.parse(_providerCreditStructure.controllerBranchDP.text.replaceAll(",", "")) : 0,
                  "grossDownPayment": _providerCreditStructure.controllerGrossDP.text != "" ? double.parse(_providerCreditStructure.controllerGrossDP.text.replaceAll(",", "")) : 0,
                  "totalLoan": _providerCreditStructure.controllerTotalLoan.text != "" ? double.parse(_providerCreditStructure.controllerTotalLoan.text.replaceAll(",", "")) : 0,
                  "installmentAmount": _providerCreditStructure.controllerInstallment.text != "" ? double.parse(_providerCreditStructure.controllerInstallment.text.replaceAll(",", "")) : 0,
                  "ltvRatio": _providerCreditStructure.controllerLTV.text != "" ? double.parse(_providerCreditStructure.controllerLTV.text.replaceAll(",", "")) : 0.0,
                  "pencairan": 0,
                  "interestAmount": _providerCreditStructure.interestAmount,
                  "gpType": null,
                  "newTenor": 0,
                  "totalStepping": 0,
                  "uangMukaKaroseri": 0, // diset 0
                  "uangMukaChasisNet": _providerCreditStructure.controllerNetDP.text != "" ? double.parse(_providerCreditStructure.controllerNetDP.text.replaceAll(",", "")) : 0, // set sama kaya nettDownPayment
                  "uangMukaChasisGross": _providerCreditStructure.controllerGrossDP.text != "" ? double.parse(_providerCreditStructure.controllerGrossDP.text.replaceAll(",", "")) : 0, // set sama kaya grossDownPayment
                  "installmentBalloonPayment": {
                    "balloonType": _providerCreditStructureType.radioValueBalloonPHOrBalloonInstallment,
                    "lastInstallmentPercentage": _providerCreditStructureType.controllerInstallmentPercentage.text != "" ? double.parse(_providerCreditStructureType.controllerInstallmentPercentage.text.replaceAll(",", "")) : 0,
                    "lastInstallmentValue": _providerCreditStructureType.controllerInstallmentValue.text != "" ? double.parse(_providerCreditStructureType.controllerInstallmentValue.text.replaceAll(",", "")) : 0
                  },
                  "installmentSchedule": {
                    "installmentType": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.name : null, // OLD: ambil dari ID
                    "paymentMethodType": _providerCreditStructure.paymentMethodSelected != null ? _providerCreditStructure.paymentMethodSelected.name : null,  // OLD: ambil dari ID
                    "installment": _providerCreditStructure.controllerInstallment.text != "" ? double.parse(_providerCreditStructure.controllerInstallment.text.replaceAll(",", "")) : 0,
                    "installmentRound": 0,
                    "installmentMethod": 0,
                    "lastKnownOutstanding": 0,
                    "minInterest": 0,
                    "maxInterest": 0,
                    "futureValue": 0,
                    "isInstallment": false,
                    "roundingValue": null,
                    "balloonInstallment": 0,
                    "gracePeriode": 0,
                    "gracePeriodes": [],
                    "seasonal": 0,
                    "steppings": [],
                    "installmentScheduleDetails": []
                  },
                  "dsr": _providerCreditIncome.controllerDebtComparison.text != "" ? double.parse(_providerCreditIncome.controllerDebtComparison.text.replaceAll(",", "")) : 0,
                  "dir": _providerCreditIncome.controllerIncomeComparison.text != "" ? double.parse(_providerCreditIncome.controllerIncomeComparison.text.replaceAll(",", "")) : 0,
                  "dsc": _providerCreditIncome.controllerDSC.text != "" ? double.parse(_providerCreditIncome.controllerDSC.text.replaceAll(",", "")) : 0.0,
                  "irr": _providerCreditIncome.controllerIRR.text != "" ? double.parse(_providerCreditIncome.controllerIRR.text.replaceAll(",", "")) : 0.0,
                  "installmentDetails": _installmentDetails,
                  // [
                  //   {
                  //     "installmentID": null,
                  //     "installmentNumber": 0,
                  //     "percentage": 0,
                  //     "amount": 0,
                  //     "creationalSpecification": {
                  //       "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                  //       "createdBy": "10056030",
                  //       "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                  //       "modifiedBy": "10056030"
                  //     },
                  //     "status": "ACTIVE"
                  //   }
                  // ],
                  "realDSR": 0 // di jadikan model dari get dsr
                },
                "orderFees": _orderFees,
                "orderSubsidies": _orderSubsidies,
                "orderProductAdditionalSpecification": {
                  "virtualAccountID": null,
                  "virtualAccountValue": 0,
                  "cashingPurposeID": null,
                  "cashingTypeID": null
                },
                "creationalSpecification": {
                  "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                  "createdBy": _preferences.getString("username"),
                  "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                  "modifiedBy": _preferences.getString("username"),
                },
                "status": "ACTIVE",
                "collateralAutomotives": _providerKolateral.collateralTypeModel.id == "001"
                    ?
                [
                  {
                    "collateralID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.collateralID : "CAU001",// NEW | _providerKolateral.collateralTypeModel != null ? _providerKolateral.collateralTypeModel.id : "",
                    "isCollaSameUnit": _providerKolateral.radioValueIsCollateralSameWithUnitOto, // Jika Collateral = Unit dipilih YA, maka kirim TRUE. Begitu juga sebaliknya (0 = YA/true | 1 = TIDAK/false)
                    "isMultiUnitCollateral": _providerKolateral.radioValueForAllUnitOto, // ya = false = 0 | tidak = true = 1
                    "isCollaNameSameWithCustomerName": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto, // Jika Nama Jaminan = Pemohon dipilih YA, maka kirim TRUE. Begitu juga sebaliknya (0 = YA/true | 1 = TIDAK/false)
                    "identitySpecification": {
                      "identityType": _providerKolateral.identityTypeSelectedAuto != null ? _providerKolateral.identityTypeSelectedAuto.id : "",
                      "identityNumber": _providerKolateral.controllerIdentityNumberAuto.text != "" ? _providerKolateral.controllerIdentityNumberAuto.text : "",
                      "identityName": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0 ? _providerInfoNasabah.controllerNamaLengkapSesuaiIdentitas.text : _providerKolateral.controllerNameOnCollateralAuto.text,
                      "fullName": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0 ? _providerInfoNasabah.controllerNamaLengkap.text : _providerKolateral.controllerNameOnCollateralAuto.text,
                      "alias": null,
                      "title": null,
                      "dateOfBirth": _providerKolateral.controllerBirthDateAuto.text != "" ? _providerKolateral.controllerBirthDateAuto.text+" 00:00:00" : null,
                      "placeOfBirth": _providerKolateral.controllerBirthPlaceValidWithIdentityAuto.text != "" ? _providerKolateral.controllerBirthPlaceValidWithIdentityAuto.text : "",
                      "placeOfBirthKabKota": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0 ? _providerInfoNasabah.birthPlaceSelected.KABKOT_ID : _providerKolateral.birthPlaceAutoSelected != null ? _providerKolateral.birthPlaceAutoSelected.KABKOT_ID : null,
                      "gender": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0 ? _providerInfoNasabah.radioValueGender : null,
                      "identityActiveStart": null,
                      "identityActiveEnd": null,
                      "religion": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0 ? _providerInfoNasabah.religionSelected == null ? null : _providerInfoNasabah.religionSelected.id : null,
                      "occupationID": _providerFoto.occupationSelected != null ? _providerFoto.occupationSelected.KODE : "",
                      "positionID": null
                    },
                    "collateralAutomotiveSpecification": {
                      "orderUnitSpecification": {
                        "objectGroupID": _providerKolateral.groupObjectSelected != null ? _providerKolateral.groupObjectSelected.KODE : null,
                        "objectID": _providerKolateral.objectSelected != null ? _providerKolateral.objectSelected.id : null,
                        "productTypeID": _providerKolateral.productTypeSelected != null ? _providerKolateral.productTypeSelected.id : null,
                        "objectBrandID":  _providerKolateral.brandObjectSelected != null ? _providerKolateral.brandObjectSelected.id : null,
                        "objectTypeID": _providerKolateral.objectTypeSelected != null ? _providerKolateral.objectTypeSelected.id : null,
                        "objectModelID": _providerKolateral.modelObjectSelected != null ? _providerKolateral.modelObjectSelected.id : null,
                        "objectUsageID":  "", // tidak dipakai
                        "objectPurposeID": _providerKolateral.objectUsageSelected != null ? _providerKolateral.objectUsageSelected.id : null, // field tujuan penggunaan collateral
                      },
                      "mpAdiraUpld": _providerKolateral.controllerMPAdiraUpload.text != "" ? double.parse(_providerKolateral.controllerMPAdiraUpload.text.replaceAll(",", "")) : null,
                      "productionYear": _providerKolateral.yearProductionSelected != null ? int.parse(_providerKolateral.yearProductionSelected) : "",
                      "registrationYear": _providerKolateral.yearRegistrationSelected != null ? int.parse(_providerKolateral.yearRegistrationSelected) : "",
                      "isYellowPlate": _providerKolateral.radioValueYellowPlat, // per tanggal 10.05.2021 = dirubah ga dirubah jadi true false (OLD: == 0 ? false : true)
                      "isBuiltUp": _providerKolateral.radioValueBuiltUpNonATPM == 0 ? true : false,
                      "utjSpecification": {
                        "bpkbNumber": _providerKolateral.controllerBPKPNumber.text != "" ? _providerKolateral.controllerBPKPNumber.text : null,
                        "chassisNumber":  _providerKolateral.controllerFrameNumber.text != "" ? _providerKolateral.controllerFrameNumber.text : null,
                        "engineNumber": _providerKolateral.controllerMachineNumber.text != "" ? _providerKolateral.controllerMachineNumber.text : null,
                        "policeNumber": _providerKolateral.controllerPoliceNumber.text != "" ? _providerKolateral.controllerPoliceNumber.text : null,
                      },
                      "gradeUnit": _providerKolateral.controllerGradeUnit.text != "" ? _providerKolateral.controllerGradeUnit.text : null,
                      "utjFacility": _providerKolateral.controllerFasilitasUTJ.text != "" ? _providerKolateral.controllerFasilitasUTJ.text == "TUNAI" ? "1" : "0" : null,
                      "bidderName": _providerKolateral.controllerNamaBidder.text != "" ? _providerKolateral.controllerNamaBidder.text : null,
                      "showroomPrice": _providerKolateral.controllerHargaJualShowroom.text != "" ? double.parse(_providerKolateral.controllerHargaJualShowroom.text.replaceAll(",", "")).round() : "",
                      "additionalEquipment": _providerKolateral.controllerPerlengkapanTambahan.text != "" ? _providerKolateral.controllerPerlengkapanTambahan.text : "",
                      "mpAdira":  _providerKolateral.controllerMPAdira.text != "" ? double.parse(_providerKolateral.controllerMPAdira.text.replaceAll(",", "")) : null,
                      "physicalRecondition": _providerKolateral.controllerRekondisiFisik.text != "" ? double.parse(_providerKolateral.controllerRekondisiFisik.text.replaceAll(",", "")) : 0,
                      "isProper": _providerKolateral.radioValueWorthyOrUnworthy, // 0 = layak = false | 1 = tidak layak = true
                      "ltvRatio": 0.0,
                      "appraisalSpecification": {
                        "collateralDp": double.parse(_providerKolateral.controllerDPJaminan.text == '' ? '0' : _providerKolateral.controllerDPJaminan.text.replaceAll(",", "")),
                        "maximumPh": double.parse(_providerKolateral.controllerPHMaxAutomotive.text == '' ? '0' : _providerKolateral.controllerPHMaxAutomotive.text.replaceAll(",", "")),
                        "appraisalPrice": double.parse(_providerKolateral.controllerTaksasiPriceAutomotive.text == '' ? '0' : _providerKolateral.controllerTaksasiPriceAutomotive.text.replaceAll(",", "")),
                        "collateralUtilizationID": _providerKolateral.collateralUsageOtoSelected != null ? _providerKolateral.collateralUsageOtoSelected.id : "" // field tujuan penggunaan collateral
                      },
                      "collateralAutomotiveAdditionalSpecification": {
                        "capacity": 0,
                        "color": null,
                        "manufacturer": null,
                        "serialNumber": null,
                        "invoiceNumber": null,
                        "stnkActiveDate": null,
                        "bpkbAddress": null,
                        "bpkbIdentityTypeID": null
                      },
                    },
                    "status": "ACTIVE",
                    "creationalSpecification": {
                      "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                      "createdBy": _preferences.getString("username"),
                      "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                      "modifiedBy": _preferences.getString("username"),
                    }
                  }
                ] : [],
                "collateralProperties": _providerKolateral.collateralTypeModel.id == "002"
                    ?
                [
                  {
                    "isMultiUnitCollateral": true, // _providerKolateral.radioValueForAllUnitOto, // ya = false = 0 | tidak = true = 1
                    "isCollaNameSameWithCustomerName": _providerKolateral.radioValueIsCollateralSameWithApplicantProperty == 0,
                    "collateralID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.collateralID : "COLP001", // NEW
                    "identitySpecification": {
                      "identityType": _providerKolateral.identityModel != null ? _providerKolateral.identityModel.id : "",
                      "identityNumber": _providerKolateral.controllerIdentityNumber.text,
                      "identityName": null,
                      "fullName": _providerKolateral.controllerNameOnCollateral.text,
                      "alias": null,
                      "title": null,
                      "dateOfBirth": _providerKolateral.controllerBirthDateProp.text != "" ? _providerKolateral.controllerBirthDateProp.text : null,
                      "placeOfBirth": _providerKolateral.birthPlaceSelected.KABKOT_NAME,
                      "placeOfBirthKabKota": _providerKolateral.birthPlaceSelected.KABKOT_ID,
                      "gender": null,
                      "identityActiveStart": null,
                      "identityActiveEnd": null,
                      "isLifetime": null,
                      "religion": null,
                      "occupationID": null,
                      "positionID": null,
                      "maritalStatusID": null
                    },
                    "detailAddresses": [
                      {
                        "addressID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.addressID != "NEW" ? _providerKolateral.addressID : "NEW" : "NEW",
                        "foreignBusinessID": _preferences.getString("last_known_state") != "IDE" ? _providerKolateral.foreignBusinessID != "NEW" ? _providerKolateral.foreignBusinessID : "CAU001" : "CAU001",
                        "addressSpecification": {
                          "koresponden": _providerKolateral.isKorespondensi.toString(),
                          "matrixAddr": _preferences.getString("cust_type") == "PER" ? "6" : "13",
                          "address": _providerKolateral.controllerAddress.text,
                          "rt": _providerKolateral.controllerRT.text,
                          "rw": _providerKolateral.controllerRW.text,
                          "provinsiID": _providerKolateral.kelurahanSelected.PROV_ID,
                          "kabkotID": _providerKolateral.kelurahanSelected.KABKOT_ID,
                          "kecamatanID": _providerKolateral.kelurahanSelected.KEC_ID,
                          "kelurahanID": _providerKolateral.kelurahanSelected.KEL_ID,
                          "zipcode": _providerKolateral.kelurahanSelected.ZIPCODE
                        },
                        "contactSpecification": {
                          "telephoneArea1": null,
                          "telephone1": null,
                          "telephoneArea2": null,
                          "telephone2": null,
                          "faxArea": null,
                          "fax": null,
                          "handphone": null,
                          "email": null,
                          "noWa": null,
                          "noWa2": null,
                          "noWa3": null
                        },
                        "addressType": _providerKolateral.addressTypeSelected.KODE,
                        "creationalSpecification": {
                          "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                          "createdBy": _preferences.getString("username"),
                          "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                          "modifiedBy": _preferences.getString("username"),
                        },
                        "status": "ACTIVE"
                      }
                    ],
                    "collateralPropertySpecification": {
                      "certificateNumber": _providerKolateral.controllerCertificateNumber.text,
                      "certificateTypeID": _providerKolateral.certificateTypeSelected.id,
                      "propertyTypeID": _providerKolateral.propertyTypeSelected.id,
                      "buildingArea": double.parse(_providerKolateral.controllerBuildingArea.text.replaceAll(",", "")),
                      "landArea": double.parse(_providerKolateral.controllerSurfaceArea.text.replaceAll(",", "")),
                      "appraisalSpecification": {
                        "collateralDp": double.parse(_providerKolateral.controllerDPJaminan.text.replaceAll(",", "")).toInt(),
                        "maximumPh": double.parse(_providerKolateral.controllerPHMax.text == "" ? '0' : _providerKolateral.controllerPHMax.text.replaceAll(",", "")),
                        "appraisalPrice": double.parse(_providerKolateral.controllerTaksasiPrice.text.replaceAll(",", "")).toInt(),
                        "collateralUtilizationID": _providerKolateral.collateralUsagePropertySelected != null ? _providerKolateral.collateralUsagePropertySelected.id : "" //"" field tujuan penggunaan collateral
                      },
                      "positiveFasumDistance": double.parse(_providerKolateral.controllerJarakFasumPositif.text.replaceAll(",","")).toInt(),
                      "negativeFasumDistance": double.parse(_providerKolateral.controllerJarakFasumNegatif.text.replaceAll(",","")).toInt(),
                      "landPrice": double.parse(_providerKolateral.controllerHargaTanah.text.replaceAll(",","")).toInt(),
                      "njopPrice": double.parse(_providerKolateral.controllerHargaNJOP.text.replaceAll(",","")).toInt(),
                      "buildingPrice": double.parse(_providerKolateral.controllerHargaBangunan.text.replaceAll(",","")).toInt(),
                      "roofTypeID": _providerKolateral.typeOfRoofSelected != null ? _providerKolateral.typeOfRoofSelected.id : null,
                      "wallTypeID": _providerKolateral.wallTypeSelected != null ? _providerKolateral.wallTypeSelected.id : null,
                      "floorTypeID": _providerKolateral.floorTypeSelected != null ? _providerKolateral.floorTypeSelected.id : null,
                      "foundationTypeID": _providerKolateral.foundationTypeSelected != null ? _providerKolateral.foundationTypeSelected.id : null,
                      "roadTypeID": _providerKolateral.streetTypeSelected != null ? _providerKolateral.streetTypeSelected.id : null,
                      "isCarPassable": _providerKolateral.radioValueAccessCar == 0,
                      "totalOfHouseInRadius": _providerKolateral.controllerJumlahRumahDalamRadius.text != "" ? int.parse(_providerKolateral.controllerJumlahRumahDalamRadius.text) : 0,
                      "sifatJaminan": _providerKolateral.controllerSifatJaminan.text,
                      "buktiKepemilikanTanah": _providerKolateral.controllerBuktiKepemilikan.text,
                      "tgglTerbitSertifikat": _providerKolateral.controllerCertificateReleaseDate.text != "" ? formatDateValidateAndSubmit(_providerKolateral.initialDateCertificateRelease) : "",
                      "tahunTerbitSertifikat": _providerKolateral.controllerCertificateReleaseYear.text,
                      "namaPemegangHak": _providerKolateral.controllerNamaPemegangHak.text,
                      "noSuratUkurGmbrSituasi": _providerKolateral.controllerNoSuratUkur.text,
                      "tgglSuratUkurGmbrSituasi": _providerKolateral.controllerDateOfMeasuringLetter.text != "" ? formatDateValidateAndSubmit(_providerKolateral.initialDateOfMeasuringLetter) : "",
                      "sertifikatSuratUkurDikeluarkanOleh": _providerKolateral.controllerCertificateOfMeasuringLetter.text,
                      "masaBerlakuHak": _providerKolateral.controllerMasaHakBerlaku.text,
                      "noImb": _providerKolateral.controllerNoIMB.text,
                      "tgglImb": _providerKolateral.controllerDateOfIMB.text != "" ? formatDateValidateAndSubmit(_providerKolateral.initialDateOfIMB) : "",
                      "luasBangunanImb": double.parse(_providerKolateral.controllerLuasBangunanIMB.text.replaceAll(",", "")),
                      "ltvRatio": 0,
                      "letakTanah": null,
                      "propertyGroupObjtID": null,
                      "propertyObjtID": null
                    },
                    "status": "INACTIVE",
                    "creationalSpecification": {
                      "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                      "createdBy": _preferences.getString("username"),
                      "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                      "modifiedBy": _preferences.getString("username"),
                    }
                  }
                ]
                    :
                [],
                "orderProductProcessingCompletion": {
                  "lastKnownOrderProductState": "APPLICATION_DOCUMENT",
                  "nextKnownOrderProductState": "APPLICATION_DOCUMENT"
                },
                "isWithoutColla": _providerKolateral.collateralTypeModel.id == "003" ? 1 : 0,// _providerUnitObject.groupObjectSelected.KODE == "003" ? 1 : 0,
                "isSaveKaroseri": false
              }
            ],
            "orderProcessingCompletion": {
              "calculatedAt": 0,
              "lastKnownOrderState": null,
              "lastKnownOrderStatePosition": null,
              "lastKnownOrderStatus": null,
              "lastKnownOrderHandledBy": null
            },
            "creationalSpecification": {
              "createdAt": formatDateValidateAndSubmit(DateTime.now()),
              "createdBy": _preferences.getString("username"),
              "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
              "modifiedBy": _preferences.getString("username"),
            },
            "status": "ACTIVE"
          },
          "surveyDTO": {
            "surveyID": _dataResultSurvey[0]['surveyID'], // _providerSurvey.surveyID,
            "orderID": _providerApp.applNo,
            "surveyType": "002",
            "employeeID": _preferences.getString("username"),
            "phoneNumber": null,
            "janjiSurveyDate": formatDateValidateAndSubmit(_providerApp.initialSurveyDate),
            "reason": "",
            "flagBRMS": "BRMS",
            "surveyStatus": "01",
            "status": "ACTIVE",
            "surveyProcess": "02",
            "surveyResultSpecification": {
              "surveyResultType": _providerSurvey.resultSurveySelected != null ? _providerSurvey.resultSurveySelected.KODE : "",
              "recomendationType": _providerSurvey.recommendationSurveySelected != null ? _providerSurvey.recommendationSurveySelected.KODE : null,
              "notes": _providerSurvey.controllerNote.text != "" ? _providerSurvey.controllerNote.text : "",
              "surveyResultDate": _providerSurvey.controllerResultSurveyDate.text.isNotEmpty ? "${_providerSurvey.controllerResultSurveyDate.text} ${_providerSurvey.controllerResultSurveyTime.text}:00" : formatDateValidateAndSubmit(DateTime.now()), //formatDateValidateAndSubmit(_providerSurvey.initialDateResultSurvey),
              "distanceWithSentra": _providerSurvey.controllerDistanceLocationWithSentraUnit.text != "" ? double.parse(_providerSurvey.controllerDistanceLocationWithSentraUnit.text.replaceAll(",", "")).toInt() : 0,
              "distanceWithDealer": _providerSurvey.controllerDistanceLocationWithDealerMerchantAXI.text != "" ? double.parse(_providerSurvey.controllerDistanceLocationWithDealerMerchantAXI.text.replaceAll(",", "")).toInt() : 0,
              "distanceObjWithSentra": _providerSurvey.controllerDistanceLocationWithUsageObjectWithSentraUnit.text != "" ? double.parse(_providerSurvey.controllerDistanceLocationWithUsageObjectWithSentraUnit.text.replaceAll(",", "")).toInt() : 0,
              "surveyResultAssets": _surveyResultAssets,
              "surveyResultDetails": _surveyResultDetails,
              "surveyResultTelesurveys": []
            },
            "creationalSpecification": {
              "createdAt": formatDateValidateAndSubmit(DateTime.now()),
              "createdBy": _preferences.getString("username"),
              "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
              "modifiedBy": _preferences.getString("username")
            },
            "jobSurveyor": ""
          },
          "userCredentialMS2": {
            "nik": _preferences.getString("username"),
            "branchCode": _preferences.getString("branchid"),
            "fullName": _preferences.getString("fullname"),
            "role": _preferences.getString("job_name")
          }
        },
        "applicationDTOReason": {
          "customerDTOReason": {
            "customerIDReason": null,
            "customerObligorIDReason": null,
            "customerTypeReason": null,
            "customerSpecificationFormNumberReason": null,
            "customerNpwpSpecificationHasNpwpReason": _providerInfoNasabah.isHaveNPWPChanges ? "99" : null,
            "customerNpwpSpecificationNpwpAddressReason": _providerInfoNasabah.isAlamatSesuaiNPWPChanges ? "99" : null,
            "customerNpwpSpecificationNpwpFullnameReason": _providerInfoNasabah.isNamaSesuaiNPWPChanges ? "99" : null,
            "customerNpwpSpecificationNpwpNumberReason": _providerInfoNasabah.isNoNPWPChanges ? "99" : null,
            "customerNpwpSpecificationNpwpTypeReason": _providerInfoNasabah.isJenisNPWPChanges ? "99" : null,
            "customerNpwpSpecificationPkpIdentifierReason": _providerInfoNasabah.isTandaPKPChanges ? "99" : null,
            "customerDetailAddressesReason": _detailAddressesReason,
            "customerGuarantorSpecificationIsGuarantorReason": null,
            "customerGuarantorCorporatesReason": _customerGuarantorCorporatesReason,
            "customerGuarantorIndividualsReason": _customerGuarantorIndividualsReason,
            "customerIndividualReason": [
              {
                "customerIndividualIDReason": null,
                "customerIndividualNumberOfDependentsReason": _providerInfoNasabah.isJumlahTanggunganChanges ? "99" : null,
                "customerIndividualGroupCustomerReason": _providerInfoNasabah.isGCChanges ? "99" : null,
                "customerIndividualStatusRelationshipReason": _providerInfoNasabah.isMaritalStatusSelectedChanges ? "99" : null,
                "customerIndividualEducationIDReason": _providerInfoNasabah.isEducationSelectedChanges ? "99" : null,
                "customerIndividualIdentityIdentityTypeReason": _providerInfoNasabah.isIdentitasModelChanges ? "99" : null,
                "customerIndividualIdentityIdentityNumberReason": _providerInfoNasabah.isNoIdentitasChanges ? "99" : null,
                "customerIndividualIdentityIdentityNameReason": _providerInfoNasabah.isNamaLengkapSesuaiIdentitasChanges ? "99" : null,
                "customerIndividualIdentityFullNameReason": _providerInfoNasabah.isNamaLengkapChanges ? "99" : null,
                "customerIndividualIdentityAliasReason": null,
                "customerIndividualIdentityTitleReason": null,
                "customerIndividualIdentityDateOfBirthReason": _providerInfoNasabah.isTglLahirChanges ? "99" : null,
                "customerIndividualIdentityPlaceOfBirthReason": _providerInfoNasabah.isTempatLahirSesuaiIdentitasChanges ? "99" : null,
                "customerIndividualIdentityPlaceOfBirthKabKotaReason": _providerInfoNasabah.isTempatLahirSesuaiIdentitasLOVChanges ? "99" : null,
                "customerIndividualIdentityGenderReason": _providerInfoNasabah.isGenderChanges ? "99" : null,
                "customerIndividualIdentityIdentityActiveStartReason": null,
                "customerIndividualIdentityIdentityActiveEndReason": null,
                "customerIndividualIdentityReligionReason": null,
                "customerIndividualIdentityOccupationIDReason": null,
                "customerIndividualIdentityPositionIDReason": null,
                "customerIndividualIdentityisLifetimeReason": null,
                "customerIndividualContactTelephoneArea1Reason": null,
                "customerIndividualContactTelephone1Reason": null,
                "customerIndividualContactTelephoneArea2Reason": null,
                "customerIndividualContactTelephone2Reason": null,
                "customerIndividualContactFaxAreaReason": null,
                "customerIndividualContactFaxReason": null,
                "customerIndividualContactHandphoneReason": _providerInfoNasabah.isNoHp1WAChanges ? "99" : null,
                "customerIndividualContactEmailReason": _providerInfoNasabah.isEmailChanges ? "99" : null,
                "noWaReason": _providerInfoNasabah.isNoHp1WAChanges  ? "99" : null,
                "noWa2Reason": _providerInfoNasabah.isNoHp2WAChanges  ? "99" : null,
                "noWa3Reason": _providerInfoNasabah.isNoHp3WAChanges  ? "99" : null,
                "customerOccupationsReason": [
                  {
                    "customerIndividualCustomerOccupationIDReason": null,
                    "customerIndividualOccupationIDReason": null,
                    "customerIndividualOccupationBusinessNameReason": _providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07" ? _providerOccupation.editNamaUsahaWiraswasta :_providerOccupation.editNamaPerusahaanLainnya,
                    "customerIndividualOccupationBusinessTypeReason": _customerIndividualOccupationBusinessTypeReason,
                    "customerIndividualOccupationProfessionTypeReason": _providerOccupation.editJenisProfesiProfesional ? "99" : null,
                    "customerIndividualOccupationPepTypeReason": _providerOccupation.editJenisPepHighRiskLainnya ? "99" : null,
                    "customerIndividualOccupationEmployeeStatusReason": _providerOccupation.editTotalPegawaiLainnya ? "99" : null,
                    "customerIndividualOccupationBusinessEconomySectorReason": _customerIndividualOccupationBusinessEconomySectorReason,
                    "customerIndividualOccupationBusinessBusinessFieldReason": _customerIndividualOccupationBusinessBusinessFieldReason,
                    "customerIndividualOccupationBusinessLocationStatusReason": _customerIndividualOccupationBusinessLocationStatusReason,
                    "customerIndividualOccupationBusinessBusinessLocationReason": _customerIndividualOccupationBusinessBusinessLocationReason,
                    "customerIndividualOccupationBusinessEmployeeTotalReason": _customerIndividualOccupationBusinessEmployeeTotalReason,
                    "customerIndividualOccupationBusinessBussinessLengthInMonthReason": _providerOccupation.editLamaBekerjaBerjalan ? "99" : null,
                    "customerIndividualOccupationBusinessTotalBussinessLengthInMonthReason": _customerIndividualOccupationBusinessTotalBussinessLengthInMonthReason,
                    "customerIndividualOccupationCreationalCreatedAtReason": null,
                    "customerIndividualOccupationCreationalCreatedByReason": null,
                    "customerIndividualOccupationCreationalModifiedAtReason": null,
                    "customerIndividualOccupationCreationalModifiedByReason": null,
                    "customerIndividualOccupationStatusReason": null,
                    "customerOccupationFlagCorrectionReason": null
                  }
                ],
                "customerIndividualNearFutureNeedIDsReason": [],
                "custIndividualFacebookAccountReason": null,
                "custIndividualBlackBerryPinReason": null,
                "customerIndividualHobbyIDsReason": [],
                "customerIndividualParticipantIDsReason": [],
                "custIndividualFamilyCardNumberReason": null,
                "custIndividualFavoriteColorReason": null,
                "custIndividualFavoriteBrandReason": null,
                "custIndividualEmergencyRelationshipTypeIDReason": null,
                "custIndividualEmergencyIdentityFullNameReason": null,
                "custIndividualEmergencyFullNameReason": null,
                "custIndividualEmergencyTitleReason": null,
                "custIndividualEmergencyEmailReason": null,
                "custIndividualEmergencyHandphoneNumberReason": null,
                "customerIndividualCustomerCreditCardReason": [],
                "custIndividualCustomerFamiliesReason": _custIndividualCustomerFamiliesReason,
                "customerIndividualStatusReason": null,
                "customerIndividualCreatedAtReason": null,
                "customerIndividualCreatedByReason": null,
                "customerIndividualModifiedAtReason": null,
                "customerIndividualModifiedByReason": null
              }
            ],
            "customerCorporateReason": null,
            "exposureInformationsReason": [
              {
                "exposureInformationIDReason": null,
                "exposureGroupObjectIDReason": null,
                "exposureOsArReason": null,
                "exposureArOnProcessReason": null,
                "exposureTotalReason": null,
                "exposureObligorIDReason": null,
                "exposureArNewReason": null,
                "exposureExposureOthersReason": null,
                "exposureStatusReason": null,
                "exposureCreatedAtReason": null,
                "exposureCreatedByReason": null,
                "exposureModifiedAtReason": null,
                "exposureModifiedByReason": null,
                "exposureInformationFlagCorrectionReason": null
              }
            ],
            "customerIncomesReason": _customerIncomesReason,
            "customerAdditionalCorrespondenceTypeIDReason": null,
            "customerAdditionalDebtorL2CodeReason": null,
            "customerAdditionalDebtorL3CodeReason": null,
            "customerBankAccountsReason": [],
            "customerProcessingLastKnownCustomerStateReason": null,
            "customerProcessingNextKnownCustomerStateReason": null,
            "customerCreatedAtReason": null,
            "customerCreatedByReason": null,
            "customerModifiedAtReason": null,
            "customerModifiedByReason": null,
            "customerStatusReason": null
          },
          "orderDTOReason": {
            "orderIDReason": null,
            "orderCustomerIDReason": null,
            "orderSpecificationIsMayorReason": null,
            "orderSpecificationHasFiduciaReason": null,
            "orderSpecificationApplicationSourceReason": null,
            "orderSpecificationOrderDateReason": _providerApp.isOrderDateChanges ? "99" : null,
            "orderSpecificationApplicationDateReason": _providerApp.isOrderDateChanges ? "99" : null,
            "orderSpecificationSurveyAppointmentDateReason": _providerApp.isSurveyAppointmentDateChanges ? "99" : null,
            "orderSpecificationOrderTypeReason": null,
            "orderSpecificationIsOpenAccountReason": null,
            "orderSpecificationAccountFormNumberReason": null,
            "orderSpecificationSentraCIDReason": null,
            "orderSpecificationUnitCIDReason": null,
            "orderSpecificationInitialRecommendationReason": null,
            "orderSpecificationFinalRecommendationReason": null,
            "orderSpecificationBrmsScoringReason": null,
            "orderSpecificationIsInPlaceApprovalReason": null,
            "orderSpecificationIsPkSignedReason": _providerApp.isIsSignedPKChanges ? "99" : null,
            "orderSpecificationMaxApprovalLevelReason": null,
            "orderSpecificationJenisKonsepReason": _providerFoto.isjenisKonsepSelectedChanges ? "99" : null,
            "orderSpecificationJumlahObjekReason": _providerApp.isTotalObjectChanges ? "99" : null,
            "orderSpecificationJenisProporsionalAsuransiReason": _providerApp.isPropotionalObjectChanges ? "99" : null,
            "orderSpecificationUnitKeReason": _providerApp.isNumberOfUnitChanges ? "99" : null,
            "orderSpecificationflagConfirmDasReason": null,
            "orderSpecificationflagUploadDRReason": null,
            "orderSpecificationflagCADakorReason": null,
            "orderSpecificationflagInstantApprovalReason": null,
            "orderSpecificationCollateralTypeIDReason": null,
            "orderSpecificationIsWillingToAcceptInfoReason": null,
            "orderSpecificationApplicationContractNumberReason": null,
            "orderSpecificationApplicationDocVerStatusReason": null,
            "orderSpecificationCustomerStatusReason": null,
            "orderSpecificationAosApprovalStatusReason": null,
            "orderSupportingDocumentsReason": [
              {
                "orderSupportingDocumentIDReason": null,
                "orderSupportingDocumentProductIDReason": null,
                "orderSupportingDocumentTypeIDReason": null,
                "orderSupportingDocumentIsMandatoryReason": null,
                "orderSupportingDocumentIsMandatoryCAReason": null,
                "orderSupportingDocumentIsDisplayReason": null,
                "orderSupportingDocumentIsUnitReason": null,
                "orderSupportingDocumentDocumentReceivedDateReason": null,
                "orderSupportingDocumentFileNameReason": null,
                "orderSupportingDocumentHeaderIDReason": null,
                "orderSupportingDocumentUploadDateReason": null,
                "orderSupportingDocumentCreatedAtReason": null,
                "orderSupportingDocumentCreatedByReason": null,
                "orderSupportingDocumentModifiedAtReason": null,
                "orderSupportingDocumentModifiedByReason": null,
                "orderSupportingDocumentStatusReason": null,
                "orderSupportingDocumentFlagCorrectionReason": null
              }
            ],
            "orderProductsReason": [
              {
                "orderProductIDReason": null,
                "orderProductFinancingTypeIDReason": _providerFoto.istypeOfFinancingModelSelectedChanges ? "99" : null,
                "orderProductOjkBusinessTypeIDReason": _providerFoto.iskegiatanUsahaSelectedChanges ? "99" : null,
                "orderProductOjkBussinessDetailIDReason": _providerFoto.isjenisKegiatanUsahaSelectedChanges ? "99" : null,
                "orderProductObjectGroupIDReason": _providerUnitObject.isGroupObjectChanges ? "99" : null,
                "orderProductObjectIDReason": _providerUnitObject.isObjectChanges ? "99" : null,
                "orderProductProductTypeIDReason": _providerUnitObject.isTypeProductChanges ? "99" : null,
                "orderProductObjectBrandIDReason": _providerUnitObject.isBrandObjectChanges ? "99" : null,
                "orderProductObjectTypeIDReason": _providerUnitObject.isObjectTypeChanges ? "99" : null,
                "orderProductObjectModelIDReason": _providerUnitObject.isModelObjectChanges ? "99" : null,
                "orderProductModelDetailReason": _providerUnitObject.isDetailModelChanges ? "99" : null,
                "orderProductObjectUsageIDReason": _providerUnitObject.isUsageObjectModelChanges ? "99" : null,
                "orderProductObjectPurposeIDReason": _providerUnitObject.isObjectPurposeChanges ? "99" : null,
                "orderProductSalesGroupIDReason": _providerUnitObject.isGroupSalesChanges ? "99" : null,
                "orderProductOrderSourceIDReason": _providerUnitObject.isSourceOrderChanges ? "99" : null,
                "orderProductOrderSourceNameReason": _providerUnitObject.isSourceOrderNameChanges ? "99" : null,
                "orderProductGroupIDReason": _providerUnitObject.isGrupIdChanges ? "99" : null,
                "orderProductOutletIDReason": null,
                "orderProductIsThirdPartyReason": _providerUnitObject.isThirdPartyChanges ? "99" : null,
                "orderProductThirdPartyTypeIDReason": _providerUnitObject.isThirdPartyTypeChanges ? "99" : null,
                "orderProductThirdPartyIDReason": _providerUnitObject.isThirdPartyChanges ? "99" : null,
                "orderProductThirdPartyActivityReason": _providerUnitObject.isKegiatanChanges ? "99" : null,
                "orderProductSentradIDReason": null, //_providerUnitObject.isSentraDChanges ? "99" : null,
                "orderProductUnitdIDReason": null, //_providerUnitObject.isUnitDChanges ? "99" : null,
                "orderProductDealerMatrixReason": _providerUnitObject.isMatriksDealerChanges ? "99" : null,
                "orderProductProgramIDReason": _providerUnitObject.isProgramChanges ? "99" : null,
                "orderProductRehabTypeReason":  _providerUnitObject.isRehabTypeChanges ? "99" : null,
                "orderProductReferenceNumberReason": _providerUnitObject.isReferenceNumberChanges ? "99" : null,
                "orderProductApplicationUnitContractNumberReason": null,
                "orderProductSalesesReason": _orderProductSalesesDakor,
                "orderProductKaroserisReason": _orderProductKaroserisReason,
                "orderProductInsurancesReason": _orderProductInsurancesReason,
                "orderProductWmpsReason": _orderProductWmpsReason,
                "orderProductCreditStructureInstallmentTypeIDReason": _providerCreditStructure.installmentTypeDakor ? "99" : null,
                "orderProductCreditStructureTenorReason": _providerCreditStructure.periodTimeDakor ? "99" : null,
                "orderProductCreditStructurePaymentMethodIDReason": _providerCreditStructure.paymentMethodDakor ? "99" : null,
                "orderProductCreditStructureEffectiveRateReason": _providerCreditStructure.interestRateEffDakor ? "99" : null,
                "orderProductCreditStructureFlatRateReason": _providerCreditStructure.interestRateFlatDakor ? "99" : null,
                "orderProductCreditStructureObjectPriceReason": _providerCreditStructure.objectPriceDakor ? "99" : null,
                "orderProductCreditStructureKaroseriTotalPriceReason": _providerCreditStructure.totalPriceKaroseriDakor ? "99" : null,
                "orderProductCreditStructureTotalPriceReason": _providerCreditStructure.totalPriceDakor ? "99" : null,
                "orderProductCreditStructureNettDownPaymentReason": _providerCreditStructure.netDPDakor ? "99" : null,
                "orderProductCreditStructureDeclineNInstallmentReason": null,
                "orderProductCreditStructurePaymentOfYearReason": _providerCreditStructure.paymentPerYearDakor ? "99" : null,
                "orderProductCreditStructureBranchDownPaymentReason": _providerCreditStructure.branchDPDakor ? "99" : null,
                "orderProductCreditStructureGrossDownPaymentReason": _providerCreditStructure.grossDPDakor ? "99" : null,
                "orderProductCreditStructureTotalLoanReason": _providerCreditStructure.totalLoanDakor ? "99" : null,
                "orderProductCreditStructureInstallmentAmountReason": _providerCreditStructure.installmentDakor ? "99" : null,
                "orderProductCreditStructureLtvRatioReason": _providerCreditStructure.ltvDakor ? "99" : null,
                "orderProductCreditStructureInterestAmountReason": null,
                "orderProductCreditStructurePencairanReason": null,
                "orderProductCreditStructureDSRReason": _providerCreditIncome.isDebtComparisonDakor ? "99" : null,
                "orderProductCreditStructureDIRReason": _providerCreditIncome.isIncomeComparisonDakor ? "99" : null,
                "orderProductCreditStructureDSCReason": _providerCreditIncome.isDSCDakor ? "99" : null,
                "orderProductCreditStructureIRRReason": _providerCreditIncome.isIRRDakor ? "99" : null,
                "orderProductCreditStructureGpTypeReason": null,
                "orderProductCreditStructureNewTenorReason": null,
                "orderProductCreditStructureTotalSteppingReason": null,
                "orderProductCreditStructureBalloonTypeReason": _providerCreditStructureType.isRadioValueBalloonPHOrBalloonInstallmentChanges ? "99" : null,
                "orderProductCreditStructureLastInstallmentPercentageReason": _providerCreditStructureType.isInstallmentPercentage ? "99" : null,
                "orderProductCreditStructureLastInstallmentValueReason": _providerCreditStructureType.isInstallmentValue ? "99" : null,
                "orderProductCreditStructureFirstInstallmentValueReason": null,
                "orderProductFeesReason": _orderProductFeesReason,
                "orderProductSubsidiesReason": _orderProductSubsidiesReason,
                "orderProductCreditStructureInstallmentDetailsReason": [],
                "orderProductCollateralAutomotivesReason": _providerKolateral.collateralTypeModel.id == "001"
                    ?
                [
                  {
                    "collateralAutoCollateralIDReason": null,
                    "collateralAutoIsCollaSameUnitReason": _providerKolateral.isRadioValueIsCollateralSameWithUnitOtoChanges ? "99" : null,
                    "collateralAutoIsMultiUnitCollateralReason": null,
                    "collateralAutoIsCollaNameSameWithCustomerNameReason": _providerKolateral.isRadioValueIsCollaNameSameWithApplicantOtoChanges ? "99" : null,
                    "collateralAutoIdentityTypeReason": _providerKolateral.isIdentityTypeSelectedAutoChanges ? "99" : null,
                    "collateralAutoIdentityNumberReason": _providerKolateral.isIdentityNumberAutoChanges ? "99" : null,
                    "collateralAutoIdentityNameReason": _providerKolateral.isNameOnCollateralAutoChanges ? "99" : null,
                    "collateralAutoFullNameReason": _providerKolateral.isNameOnCollateralAutoChanges ? "99" : null,
                    "collateralAutoAliasReason": null,
                    "collateralAutoTitleReason": null,
                    "collateralAutoDateOfBirthReason": _providerKolateral.isBirthDateAutoChanges ? "99" : null,
                    "collateralAutoPlaceOfBirthReason": _providerKolateral.isBirthPlaceValidWithIdentityAutoChanges ? "99" : null,
                    "collateralAutoPlaceOfBirthKabKotaReason": _providerKolateral.isBirthPlaceValidWithIdentityLOVAutoChanges ? "99" : null,
                    "collateralAutoGenderReason": null,
                    "collateralAutoIdentityActiveStartReason": null,
                    "collateralAutoIdentityActiveEndReason": null,
                    "collateralAutoReligionReason": null,
                    "collateralAutoOccupationIDReason": null,
                    "collateralAutoPositionIDReason": null,
                    "collateralAutoObjectGroupIDReason": _providerKolateral.isGroupObjectChanges ? "99" : null,
                    "collateralAutoObjectIDReason": _providerKolateral.isObjectChanges ? "99" : null,
                    "collateralAutoProductTypeIDReason": null, //_providerKolateral.isTypeProductChanges ? "99" : null,
                    "collateralAutoObjectBrandIDReason": _providerKolateral.isBrandObjectChanges ? "99" : null,
                    "collateralAutoObjectTypeIDReason": _providerKolateral.isObjectTypeChanges ? "99" : null,
                    "collateralAutoObjectModelIDReason": _providerKolateral.isModelObjectChanges ? "99" : null,
                    "collateralAutoObjectUsageIDReason": _providerKolateral.isUsageObjectModelChanges ? "99" : null,
                    "collateralAutoObjectPurposeIDReason": _providerKolateral.isUsageCollateralOtoChanges ? "99" : null,
                    "collateralAutProductionYearReason": _providerKolateral.isYearProductionSelectedChanges ? "99" : null,
                    "collateralAutoRegistrationYearReason": _providerKolateral.isYearRegistrationSelectedChanges ? "99" : null,
                    "collateralAutoIsYellowPlateReason": _providerKolateral.isRadioValueYellowPlatChanges ? "99" : null,
                    "collateralAutoIsBuiltUpReason": _providerKolateral.isRadioValueBuiltUpNonATPMChanges ? "99" : null,
                    "collateralAutoBpkbNumberReason": _providerKolateral.isBPKPNumberChanges ? "99" : null,
                    "collateralAutoChassisNumberReason": _providerKolateral.isFrameNumberChanges ? "99" : null,
                    "collateralAutoEngineNumberReason": _providerKolateral.isMachineNumberChanges ? "99" : null,
                    "collateralAutoPoliceNumberReason": _providerKolateral.isPoliceNumberChanges ? "99" : null,
                    "collateralAutoGradeUnitReason": _providerKolateral.isGradeUnitChanges ? "99" : null,
                    "collateralAutoUtjFacilityReason": _providerKolateral.isFasilitasUTJChanges ? "99" : null,
                    "collateralAutoBidderNameReason": _providerKolateral.isNamaBidderChanges ? "99" : null,
                    "collateralAutoShowroomPriceReason": _providerKolateral.isHargaJualShowroomChanges ? "99" : null,
                    "collateralAutoAdditionalEquipmentReason": _providerKolateral.isPerlengkapanTambahanChanges ? "99" : null,
                    "collateralAutoMpAdiraReason": _providerKolateral.isMPAdiraChanges ? "99" : null,
                    "collateralAutoPhysicalReconditionReason": _providerKolateral.isRekondisiFisikChanges ? "99" : null,
                    "collateralAutoIsProperReason": null,
                    "collateralAutoLtvRatioReason": null,
                    "collateralAutoMpAdiraUploadReason": _providerKolateral.isMPAdiraUploadChanges ? "99" : null,
                    "collateralAutoCollateralDpReason": _providerKolateral.isDpGuaranteeChanges ? "99" : null,
                    "collateralAutoMaximumPhReason": _providerKolateral.isPHMaxAutomotiveChanges ? "99" : null,
                    "collateralAutoAppraisalPriceReason": _providerKolateral.isTaksasiPriceAutomotiveChanges ? "99" : null,
                    "collateralAutoCollateralUtilizationIDReason": _providerKolateral.isUsageCollateralOtoChanges ? "99" : null,
                    "collateralAutoAdditionalCapacityReason": null,
                    "collateralAutoAdditionalColorReason": null,
                    "collateralAutoAdditionalManufacturerReason": null,
                    "collateralAutoAdditionalSerialNumberReason": null,
                    "collateralAutoAdditionalInvoiceNumberReason": null,
                    "collateralAutoAdditionalStnkActiveDateReason": null,
                    "collateralAutoAdditionalBpkbAddressReason": null,
                    "collateralAutoAdditionalBpkbIdentityTypeIDReason": null,
                    "collateralAutoStatusReason": null,
                    "collateralAutoCreatedAtReason": null,
                    "collateralAutoCreatedByReason": null,
                    "collateralAutoModifiedAtReason": null,
                    "collateralAutoModifiedByReason": null,
                    "collateralAutoFlagCorrectionReason": null
                  }
                ]
                    : [],
                "orderProductCollateralPropertiesReason": _providerKolateral.collateralTypeModel.id == "002"
                    ?
                [
                  {
                    "collateralPropertyIsMultiUnitCollateralReason": null,
                    "collateralPropertyIsCollaNameSameWithCustomerNameReason": _providerKolateral.isRadioValueIsCollateralSameWithApplicantPropertyChanges ? "99" : null,
                    "collateralPropertyIDReason": null,
                    "collateralPropertyIdentityTypeReason": _providerKolateral.isIdentityModelChanges ? "99" : null,
                    "collateralPropertyIdentityNumberReason": _providerKolateral.isIdentityNumberPropChanges ? "99" : null,
                    "collateralPropertyIdentityNameReason": _providerKolateral.isNameOnCollateralChanges ? "99" : null,
                    "collateralPropertyIdentityFullNameReason": _providerKolateral.isNameOnCollateralChanges ? "99" : null,
                    "collateralPropertyIdentityAliasReason": null,
                    "collateralPropertyIdentityTitleReason": null,
                    "collateralPropertyIdentityDateOfBirthReason": _providerKolateral.isBirthDatePropChanges ? "99" : null,
                    "collateralPropertyIdentityPlaceOfBirthReason": _providerKolateral.isBirthPlaceValidWithIdentity1Changes ? "99" : null,
                    "collateralPropertyIdentityPlaceOfBirthKabKotaReason": _providerKolateral.isBirthPlaceValidWithIdentityLOVChanges ? "99" : null,
                    "collateralPropertyIdentityGenderReason": null,
                    "collateralPropertyIdentityActiveStartReason": null,
                    "collateralPropertyIdentityActiveEndReason": null,
                    "collateralPropertyIdentityReligionReason": null,
                    "collateralPropertyIdentityOccupationIDReason": null,
                    "collateralPropertyIdentityPositionIDReason": null,
                    "collateralPropertyCertificateNumberReason": _providerKolateral.isCertificateNumberChanges ? "99" : null,
                    "collateralPropertyCertificateTypeIDReason": _providerKolateral.isCertificateTypeSelectedChanges ? "99" : null,
                    "collateralPropertyPropertyTypeIDReason": _providerKolateral.isPropertyTypeSelectedChanges ? "99" : null,
                    "collateralPropertyBuildingAreaReason": _providerKolateral.isBuildingAreaChanges ? "99" : null,
                    "collateralPropertyLandAreaReason": _providerKolateral.isSurfaceAreaChanges ? "99" : null,
                    "collateralPropertyCollateralDpReason": _providerKolateral.isDPJaminanChanges ? "99" : null,
                    "collateralPropertyMaximumPhReason": _providerKolateral.isPHMaxChanges ? "99" : null,
                    "collateralPropertyAppraisalPriceReason": _providerKolateral.isTaksasiPriceChanges ? "99" : null,
                    "collateralPropertyCollateralUtilizationIDReason": _providerKolateral.isUsageCollateralPropertyChanges ? "99" : null,
                    "collateralPropertyPositiveFasumDistanceReason": _providerKolateral.isJarakFasumPositifChanges ? "99" : null,
                    "collateralPropertyNegativeFasumDistanceReason": _providerKolateral.isJarakFasumNegatifChanges ? "99" : null,
                    "collateralPropertyLandPriceReason": _providerKolateral.isHargaTanahChanges ? "99" : null,
                    "collateralPropertyNjopPriceReason": _providerKolateral.isHargaNJOPChanges ? "99" : null,
                    "collateralPropertyBuildingPriceReason": _providerKolateral.isHargaBangunanChanges ? "99" : null,
                    "collateralPropertyRoofTypeIDReason": _providerKolateral.isTypeOfRoofSelectedChanges ? "99" : null,
                    "collateralPropertyWallTypeIDReason": _providerKolateral.isWallTypeSelectedChanges ? "99" : null,
                    "collateralPropertyFloorTypeIDReason": _providerKolateral.isFloorTypeSelectedChanges ? "99" : null,
                    "collateralPropertyFoundationTypeIDReason": _providerKolateral.isFoundationTypeSelectedChanges ? "99" : null,
                    "collateralPropertyRoadTypeIDReason": _providerKolateral.isStreetTypeSelectedChanges ? "99" : null,
                    "collateralPropertyIsCarPassableReason": _providerKolateral.isRadioValueAccessCarChanges ? "99" : null,
                    "collateralPropertyTotalOfHouseInRadiusReason": _providerKolateral.isJumlahRumahDalamRadiusChanges ? "99" : null,
                    "collateralPropertySifatJaminanReason": _providerKolateral.isSifatJaminanChanges ? "99" : null,
                    "collateralPropertyBuktiKepemilikanTanahReason": _providerKolateral.isBuktiKepemilikanChanges ? "99" : null,
                    "collateralPropertyTgglTerbitSertifikatReason": _providerKolateral.isCertificateReleaseDateChanges ? "99" : null,
                    "collateralPropertyTahunTerbitSertifikatReason": _providerKolateral.isCertificateReleaseYearChanges ? "99" : null,
                    "collateralPropertyNamaPemegangHakReason": _providerKolateral.isNamaPemegangHakChanges ? "99" : null,
                    "collateralPropertyNoSuratUkurGmbrSituasiReason": _providerKolateral.isNoSuratUkurChanges ? "99" : null,
                    "collateralPropertyTgglSuratUkurGmbrSituasiReason": _providerKolateral.isDateOfMeasuringLetterChanges ? "99" : null,
                    "collateralPropertySertifikatSuratUkurDikeluarkanOlehReason": _providerKolateral.isCertificateOfMeasuringLetterChanges ? "99" : null,
                    "collateralPropertyMasaBerlakuHakReason": _providerKolateral.isMasaHakBerlakuChanges ? "99" : null,
                    "collateralPropertyNoImbReason": _providerKolateral.isNoIMBChanges ? "99" : null,
                    "collateralPropertyTgglImbReason": _providerKolateral.isDateOfIMBChanges ? "99" : null,
                    "collateralPropertyLuasBangunanImbReason": _providerKolateral.isLuasBangunanIMBChanges ? "99" : null,
                    "collateralPropertyLtvRatioReason": null,
                    "collateralPropertyLetakTanahReason": null,
                    "collateralPropertyDetailAddressesReason": [
                      {
                        "addressIDReason": null,
                        "addressForeignBusinessIDReason": null,
                        "addressSpecificationKorespondenReason": null,
                        "addressSpecificationMatrixAddrReason": null,
                        "addressSpecificationAddressReason": _providerKolateral.isAddressDakor ? "99" : null,
                        "addressSpecificationRtReason": _providerKolateral.isRTDakor ? "99" : null,
                        "addressSpecificationRwReason": _providerKolateral.isRWDakor ? "99" : null,
                        "addressSpecificationProvinsiIDReason": _providerKolateral.isProvinsiDakor ? "99" : null,
                        "addressSpecificationKabkotIDReason": _providerKolateral.isKabKotDakor ? "99" : null,
                        "addressSpecificationKecamatanIDReason": _providerKolateral.isKecamatanDakor ? "99" : null,
                        "addressSpecificationKelurahanIDReason": _providerKolateral.isKelurahanDakor ? "99" : null,
                        "addressSpecificationZipcodeReason": _providerKolateral.isKodePosDakor ? "99" : null,
                        "addressContactSpecificationTelephoneArea1Reason": null,
                        "addressContactSpecificationTelephone1Reason": null,
                        "addressContactSpecificationTelephoneArea2Reason": null,
                        "addressContactSpecificationTelephone2Reason": null,
                        "addressContactSpecificationFaxAreaReason": null,
                        "addressContactSpecificationFaxReason": null,
                        "addressContactSpecificationHandphoneReason": null,
                        "addressContactSpecificationEmailReason": null,
                        "addressTypeReason": _providerKolateral.isAddressTypeDakor ? "99" : null,
                        "addressCreationalSpecificationCreatedAtReason": null,
                        "addressCreationalSpecificationCreatedByReason": null,
                        "addressCreationalSpecificationModifiedAtReason": null,
                        "addressCreationalSpecificationModifiedByReason": null,
                        "addressStatusReason": null,
                        "addressFlagCorrectionReason": null,
                      }
                    ],
                    "collateralPropertyStatusReason": null,
                    "collateralPropertyCreatedAtReason": null,
                    "collateralPropertyCreatedByReason": null,
                    "collateralPropertyModifiedAtReason": null,
                    "collateralPropertyModifiedByReason": null,
                    "collateralPropertyFlagCorrectionReason": null,
                  }
                ]
                    : [],
                "orderProdAutoDebitIsAutoDebitReason": null,
                "orderProdAutoDebitBankIDReason": null,
                "orderProdAutoDebitAccountNumberReason": null,
                "orderProdAutoDebitAccountBehalfReason": null,
                "orderProdAutoDebitAccountPurposeTypeIDReason": null,
                "orderProdAutoDebitDownPaymentSourceIDReason": null,
                "orderProductAdditionalVirtualAccountIDReason": null,
                "orderProductAdditionalVirtualAccountValueReason": null,
                "orderProductAdditionalCashingPurposeIDReason": null,
                "orderProductAdditionalCashingTypeIDReason": null,
                "orderProductCompletionLastKnownOrderProductStateReason": null,
                "orderProductCompletionNextKnownOrderProductStateReason": null,
                "orderProductTacActualReason": null,
                "orderProductTacMaxReason": null,
                "orderProductCreatedAtReason": null,
                "orderProductCreatedByReason": null,
                "orderProductModifiedAtReason": null,
                "orderProductModifiedByReason": null,
                "orderProductStatusReason": null,
                "applSendFlagReason": null,
                "isWithoutCollaReason": null,
                "isSaveKaroseriReason": null,
                "orderProductFlagCorrectionReason": null,
                "orderProductNik": null,
                "orderProductNama": null,
                "orderProductJabatan": null
              }
            ],
            "orderProcessingCompletionCalculatedAtReason": null,
            "orderProcessingCompletionLastKnownOrderStateReason": null,
            "orderProcessingCompletionLastKnownOrderStatePositionReason": null,
            "orderProcessingCompletionLastKnownOrderStatusReason": null,
            "orderProcessingCompletionLastKnownOrderHandledByReason": null,
            "orderTabFlagsReason": null,
            "orderCreatedAtReason": null,
            "orderCreatedByReason": null,
            "orderModifiedAtReason": null,
            "orderModifiedByReason": null,
            "orderStatusReason": null
          },
          "surveyDTOReason": {
            "surveyIDReason": null,
            "surveyOrderIDReason": null,
            "surveyTypeReason": null,
            "surveyEmployeeIDReason": null,
            "surveyPhoneNumberReason": null,
            "surveyJanjiSurveyDateReason": null,
            "surveyReasonReason": null,
            "surveyFlagBRMSReason": null,
            "surveyStatusReason": null,
            "surveyDataStatusReason": null,
            "surveyProcessReason": null,
            "surveyCfoEligibilityStatusReason": null,
            "surveyResultTypeReason": _providerSurvey.isSurveyTypeChanges ? "99" : null,
            "surveyResultRecomendationTypeReason": _providerSurvey.isSurveyRecommendationChanges ? "99" : null,
            "surveyResultNotesReason": _providerSurvey.isSurveyNotesChanges ? "99" : null,
            "surveyResultDateReason": _providerSurvey.isSurveyDateChanges ? "99" : null,
            "surveyResultDistanceWithSentraReason": _providerSurvey.isDistanceLocationWithSentraUnitChanges ? "99" : null,
            "surveyResultDistanceWithDealerReason": _providerSurvey.isDistanceLocationWithDealerMerchantAXIChanges ? "99" : null,
            "surveyResultDistanceObjWithSentraReason": _providerSurvey.isDistanceLocationWithUsageObjectWithSentraUnitChanges ? "99" : null,
            "surveyResultAssetsReason": _surveyResultAssetsDakor,
            "surveyResultDetailsReason": _surveyResultDetailsDakor,
            "surveyResultTelesurveysReason": [],
            "surveyCreatedAtReason": null,
            "surveyCreatedByReason": null,
            "surveyModifiedAtReason": null,
            "surveyModifiedByReason": null
          }
        },
        "ms2TaskID": _preferences.getString("order_no"),
        "flag": "",
        "creditLimitDTO": _preferences.getString("status_aoro") == "1" || _preferences.getString("status_aoro") == "2" ? {
          "lmeId": _dataCL[0]['lme_id'] != "null" || _dataCL[0]['lme_id'] != "" ? _dataCL[0]['lme_id'] : "", // "",
          "flagEligible": _dataCL[0]['flag_eligible'] != "null" || _dataCL[0]['flag_eligible'] != "" ? _dataCL[0]['flag_eligible'] : "", // "",
          "disburseType": _dataCL[0]['disburse_type'] != "" ? int.parse(_dataCL[0]['disburse_type']) : 0, // 1,
          "productId": _dataCL[0]['product_id'] != "null" || _dataCL[0]['product_id'] != "" ? _dataCL[0]['product_id'] : "", // "",
          "installmentAmt": _dataCL[0]['installment_amount'] != "" ? int.parse(_dataCL[0]['installment_amount']) : 0, // 0,
          "phAmt": _dataCL[0]['ph_amount'] != "" ? int.parse(_dataCL[0]['ph_amount']) : 0, // 0,
          "voucherCode": _dataCL[0]['voucher_code'] != "null" || _dataCL[0]['voucher_code'] != "" ? _dataCL[0]['voucher_code'] : "", // "",
          "tenor": _dataCL[0]['tenor'] != "" ? int.parse(_dataCL[0]['tenor']) : 0, // 0,
          "customerGrading": _dataCL[0]['grading'] !="null" || _dataCL[0]['grading'] != "" ? _dataCL[0]['grading'] : "", // "",
          "jenisPenawaran": _dataCL[0]['jenis_penawaran'] != "null" || _dataCL[0]['jenis_penawaran'] != "" ? _dataCL[0]['jenis_penawaran'] : "", // "003",
          "urutanPencairan": _dataCL[0]['pencairan_ke'] != "" ? int.parse(_dataCL[0]['pencairan_ke']) : 0, // 0,
          "jenisOpsi": _dataCL[0]['opsi_multidisburse'] != "null" || _dataCL[0]['opsi_multidisburse'] != "" ? _dataCL[0]['opsi_multidisburse'] : "00", // "00",
          "maxPlafond": double.parse(_dataCL[0]['max_ph']), // 0,
          "maxTenor": _dataCL[0]['max_tenor'] != "null" ? int.parse(_dataCL[0]['max_tenor']) : 0, // 0,
          "noReferensi": _dataCL[0]['no_referensi'] != "null" || _dataCL[0]['no_referensi'] != "" ? _dataCL[0]['no_referensi'] : "", // "", _preferences.getString("jenis_penawaran") == "002" ? _providerUnitObject.controllerReferenceNumber.text != "" ? _providerUnitObject.controllerReferenceNumber.text : ""
          "maxInstallment": _dataCL[0]['max_installment'] != "" ? double.parse(_dataCL[0]['max_installment']) : 0, // 0,
          "portfolio": _dataCL[0]['portfolio'] != "null" || _dataCL[0]['portfolio'] != "" ? _dataCL[0]['portfolio'] : "", // "",
          "activationDate": "", // _dataCL[0]['activation_date'] != "null" || _dataCL[0]['activation_date'] != "" ? _dataCL[0]['activation_date'] : ""
        } : {}
      });

      insertLog(context, _timeStartValidate, DateTime.now(), _body, "Start Submit ${_preferences.getString("last_known_state")}", "Start Submit ${_preferences.getString("last_known_state")}");

      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);

      String _submitDakor = await storage.read(key: "SubmitDakor");
      final _response = await _http.post(
          "${BaseUrl.urlGeneral}$_submitDakor",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      );

      debugPrint("CEK_RESPONSE_DKR ${_response.statusCode}");
      debugPrint("CEK_RESPONSE_DKR2 ${_response.body}");
      insertLog(context, _timeStartValidate, DateTime.now(), _body, _response.body, "Submit ${_preferences.getString("last_known_state")}");
      if(_providerSurvey.resultSurveySelected != null) {
        if(_providerSurvey.resultSurveySelected.KODE != "001") {
          _insertApplNoDeleteTask(context);
        } else {
          loadData = false;
          messageProcess = "";
          Future.delayed(Duration(seconds: 1), () {
            Navigator.pop(context);
          });
        }
      }
    }
    catch(e){
      _showSnackBar(e.toString());
    }
    // final _data = jsonDecode(_response.body);
    // if(_response.statusCode == 201){
    //   _showSnackBar("${_data['message']}");
    // }
    // else{
    //   _showSnackBar("Error ${_response.statusCode}");
    // }
  }

  // fungsi untuk insert appl no dan delete task setelah submit data
  void _insertApplNoDeleteTask(BuildContext context) async {
    DateTime _timeStartValidate = DateTime.now();
    loadData = true;
    messageProcess = "Proses insert appl no & delete task...";
    String _insertApplNo = await storage.read(key: "InsertApplNo");
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    List _dataPayung = await _dbHelper.selectDataNoPayung();
    List _dataNoUnit = await _dbHelper.selectDataNoUnit();
    var _providerInfoAppl = Provider.of<InfoAppChangeNotifier>(context,listen: false);
    var _providerInfoApplObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    var _body = jsonEncode({
      "P_APPLNO_PAYUNG": _preferences.getString("last_known_state") == "IDE" ? _dataPayung[0]['no_aplikasi_payung'] : _providerInfoAppl.applNo,
      "P_APPLNO_UNIT": _preferences.getString("last_known_state") == "IDE" ? _dataNoUnit[0]['no_unit'] : _providerInfoApplObject.applObjtID,
      "P_ORDER_NO": _preferences.getString("order_no"),
      "P_STATUS": _preferences.getString("last_known_state")
    });
    debugPrint("body _insertApplNoDeleteTask ${_preferences.getString("last_known_state")} $_body");
    insertLog(context, _timeStartValidate, DateTime.now(), _body, "Start Delete Task ${_preferences.getString("last_known_state")}", "Start Delete Task ${_preferences.getString("last_known_state")}");
    try{
      var _response = await http.post(
          "${BaseUrl.urlGeneral}$_insertApplNo",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));

      debugPrint("body _insertApplNoDeleteTask ${_response.statusCode}");
      debugPrint("body _insertApplNoDeleteTask ${_response.body} ${_preferences.getString("last_known_state")}");

      if(_response.statusCode == 200){
        // print("insertApplNoDeleteTask1 ${_response.body}");
        var _result = jsonDecode(_response.body);
        if(_result['status'] != "1"){
          // print("insertApplNoDeleteTask2 ${_response.body}");
          loadData = false;
          messageProcess = "";
          _showSnackBar("${_result['message']}");
        }
        else{
          // print("insertApplNoDeleteTask3 ${_response.body}");
          loadData = false;
          _showSnackBarSuccess("Success ${_result['message']}");
          messageProcess = "";
          Future.delayed(Duration(seconds: 1), () {
            Navigator.pop(context);
          });
          // Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => HomePage()), (Route <dynamic> route) => false);
        }
      }
      else{
        // print("insertApplNoDeleteTask4 ${_response.statusCode}");
        loadData = false;
        messageProcess = "";
        _showSnackBar("Error response ${_response.statusCode} insert appl no");
      }
    }
    on TimeoutException catch(_){
      loadData = false;
      messageProcess = "";
      _showSnackBar("Timeout connection APK insert appl no & delete task");
    }
    catch(e){
      // print("insertApplNoDeleteTask5 ${e.toString()}");
      loadData = false;
      messageProcess = "";
      _showSnackBar("Error insert appl no & delete task ${e.toString()}");
    }
  }

  // fungsi untuk insert log json ke server
  void insertLog(BuildContext context,DateTime timeStart, DateTime timeEnd,String body,String response,String labelLogName) async {
    String _insertLog = await storage.read(key: "InsertLog");
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var diff = timeEnd.difference(timeStart).inMilliseconds;
    debugPrint(dateFormat3.format(DateTime.now()));

    // await http.post(
    //     "http://192.168.57.122/orca_api/public/login",
    //     body: {
    //       "duration": diff.toString(),
    //       "idx_step": "1",
    //       "json": body,
    //       "order_no": _preferences.getString("order_no"),
    //       "process_date": dateFormat3.format(DateTime.now()),
    //       "response": response,
    //       "user_id": _preferences.getString("username"),
    //       "log_name": labelLogName,
    //     },
    //     // headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    // );

    var _body = jsonEncode({
      "duration": diff.toString(),
      "process_date": dateFormatInsertLog.format(DateTime.now()),
      "idx_step": "1",
      "json": body,
      "order_no": _preferences.getString("order_no"),
      "response": response,
      "user_id": _preferences.getString("username"),
      "log_name": labelLogName,
    });

    // await http.post(
    //     "http://192.168.57.122/orca_api/public/login",
    //     body: _body,
    //     headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    // );

    debugPrint("$_body");
    debugPrint("${BaseUrl.urlGeneral}$_insertLog");

    try{
      var _response = await http.post(
          "${BaseUrl.urlGeneral}$_insertLog",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));

      if(_response.statusCode == 200){
        // print("insertApplNoDeleteTask1 ${_response.body}");
        var _result = jsonDecode(_response.body);
        debugPrint("$_result");
        if(_result['message'] != "SUCCESS"){
          debugPrint("${_result['message']}");
        }
        else{
          debugPrint("${_result['message']}");
        }
      }
      else{
        debugPrint("status code insert log ${_response.statusCode}");
      }
    }
    on TimeoutException catch(_){
      debugPrint("insert log timeout");
    }
    catch(e){
      debugPrint("insert log ${e.toString()}");
    }
  }

  void printWrapped(String text) {
    final pattern = new RegExp('.{1,800}'); // 800 is the size of each chunk
    pattern.allMatches(text).forEach((match) => print(match.group(0)));
  }
}
