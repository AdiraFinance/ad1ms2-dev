import 'dart:collection';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/form_m_company_rincian_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_income_model.dart';
import 'package:ad1ms2_dev/models/showhide_mandatory_visibility/set_income_company_model.dart';
import 'package:ad1ms2_dev/shared/dashboard/dashboard_change_notif.dart';
import 'package:ad1ms2_dev/shared/date_picker.dart';
import 'package:ad1ms2_dev/shared/form_m_guarantor_change_notifier.dart';
import 'package:ad1ms2_dev/shared/format_currency.dart';
import 'package:ad1ms2_dev/shared/regex_input_formatter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'constants.dart';
import 'form_m_company_parent_change_notifier.dart';

class FormMCompanyPendapatanChangeNotifier with ChangeNotifier {
  bool _autoValidate = false;
  FormatCurrency _formatCurrency = FormatCurrency();
  NumberFormat _oCcy = NumberFormat("#,##0.00", "en_US");
  RegExInputFormatter _amountValidator = RegExInputFormatter.withRegex('^[0-9]{0,13}(\\.[0-9]{0,2})?\$');
  TextEditingController _controllerMonthlyIncome = TextEditingController();
  TextEditingController _controllerOtherIncome = TextEditingController();
  TextEditingController _controllerTotalIncome = TextEditingController();
  TextEditingController _controllerCostOfRevenue = TextEditingController();
  TextEditingController _controllerGrossProfit = TextEditingController();
  TextEditingController _controllerOperatingCosts = TextEditingController();
  TextEditingController _controllerOtherCosts = TextEditingController();
  TextEditingController _controllerNetProfitBeforeTax = TextEditingController();
  TextEditingController _controllerTax = TextEditingController();
  TextEditingController _controllerNetProfitAfterTax = TextEditingController();
  TextEditingController _controllerOtherInstallments = TextEditingController();
  DbHelper _dbHelper = DbHelper();
  String _customerIncomeIDMonthlyIncome;
  String _customerIncomeIDOtherIncome;
  String _customerIncomeIDTotalIncome;
  String _customerIncomeIDCostOfRevenue;
  String _customerIncomeIDGrossProfit;
  String _customerIncomeIDOperatingCosts;
  String _customerIncomeIDOtherCosts;
  String _customerIncomeIDNetProfitBeforeTax;
  String _customerIncomeIDTax;
  String _customerIncomeIDNetProfitAfterTax;
  String _customerIncomeIDOtherInstallments;

  bool _monthIncomeDakor = false;
  bool _otherIncomeDakor = false;
  bool _totalIncomeDakor = false;
  bool _hppIncomeDakor = false;
  bool _labaKotorDakor = false;
  bool _operationalCostDakor = false;
  bool _otherCostDakor = false;
  bool _labaBeforeTaxDakor = false;
  bool _taxDakor = false;
  bool _labaAfterTaxDakor = false;
  bool _otherInstallmentDakor = false;

  SetIncomeCompanyModel _setIncomeCompanyModel;

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  // Pendapatan Perbulan
  TextEditingController get controllerMonthlyIncome {
    return this._controllerMonthlyIncome;
  }

  set controllerMonthlyIncome(value) {
    this._controllerMonthlyIncome = value;
    this.notifyListeners();
  }

  // Pendapatan Lainnya
  TextEditingController get controllerOtherIncome {
    return this._controllerOtherIncome;
  }

  set controllerOtherIncome(value) {
    this._controllerOtherIncome = value;
    this.notifyListeners();
  }

  // Total Pendapatan
  TextEditingController get controllerTotalIncome {
    return this._controllerTotalIncome;
  }

  set controllerTotalIncome(value) {
    this._controllerTotalIncome = value;
    this.notifyListeners();
  }

  // Harga Pokok Pendapatan
  TextEditingController get controllerCostOfRevenue {
    return this._controllerCostOfRevenue;
  }

  set controllerCostOfRevenue(value) {
    this._controllerCostOfRevenue = value;
    this.notifyListeners();
  }

  // Laba Kotor
  TextEditingController get controllerGrossProfit {
    return this._controllerGrossProfit;
  }

  set controllerGrossProfit(value) {
    this._controllerGrossProfit = value;
    this.notifyListeners();
  }

  // Biaya Operasional
  TextEditingController get controllerOperatingCosts {
    return this._controllerOperatingCosts;
  }

  set controllerOperatingCosts(value) {
    this._controllerOperatingCosts = value;
    this.notifyListeners();
  }

  // Biaya Lainnya
  TextEditingController get controllerOtherCosts {
    return this._controllerOtherCosts;
  }

  set controllerOtherCosts(value) {
    this._controllerOtherCosts = value;
    this.notifyListeners();
  }

  // Laba Bersih Sebelum Pajak
  TextEditingController get controllerNetProfitBeforeTax {
    return this._controllerNetProfitBeforeTax;
  }

  set controllerNetProfitBeforeTax(value) {
    this._controllerNetProfitBeforeTax = value;
    this.notifyListeners();
  }

  // Pajak
  TextEditingController get controllerTax {
    return this._controllerTax;
  }

  set controllerTax(value) {
    this._controllerTax = value;
    this.notifyListeners();
  }

  // Laba Bersih Setelah Pajak
  TextEditingController get controllerNetProfitAfterTax {
    return this._controllerNetProfitAfterTax;
  }

  set controllerNetProfitAfterTax(value) {
    this._controllerNetProfitAfterTax = value;
    this.notifyListeners();
  }

  // Angsuran Lainnya
  TextEditingController get controllerOtherInstallments {
    return this._controllerOtherInstallments;
  }

  set controllerOtherInstallments(value) {
    this._controllerOtherInstallments = value;
    this.notifyListeners();
  }

  RegExInputFormatter get amountValidator => _amountValidator;

  set amountValidator(RegExInputFormatter value) {
    this._amountValidator = value;
    notifyListeners();
  }

  NumberFormat get oCcy => _oCcy;

  set oCcy(NumberFormat value) {
    this._oCcy = value;
    notifyListeners();
  }

  FormatCurrency get formatCurrency => _formatCurrency;

  void calculateTotalIncome() {
    var _income = _controllerMonthlyIncome.text.isNotEmpty ? double.parse(_controllerMonthlyIncome.text.replaceAll(",", "")) : 0.0;
    var _otherIncome = _controllerOtherIncome.text.isNotEmpty ? double.parse(_controllerOtherIncome.text.replaceAll(",", "")) : 0.0;
    var _totalIncome = _income + _otherIncome;
    _controllerTotalIncome.text = _formatCurrency.formatCurrency2(_totalIncome.toString());
  }

  void calculateGrossProfit() {
    var _totalIncome = _controllerTotalIncome.text.isNotEmpty ? double.parse(_controllerTotalIncome.text.replaceAll(",", "")) : 0.0;
    var _costOfRevenue = _controllerCostOfRevenue.text.isNotEmpty ? double.parse(_controllerCostOfRevenue.text.replaceAll(",", "")) : 0.0;
    var _grossProfit = _totalIncome - _costOfRevenue;
    if(_grossProfit < 0) {
      _controllerCostOfRevenue.clear();
      _controllerGrossProfit.clear();
    } else {
      _controllerGrossProfit.text = _formatCurrency.formatCurrency2(_grossProfit.toString());
    }
  }

  void calculateNetProfitBeforeTax() {
    var _grossProfit = _controllerGrossProfit.text.isNotEmpty ? double.parse(_controllerGrossProfit.text.replaceAll(",", "")) : 0.0;
    var _operatingCosts = _controllerOperatingCosts.text.isNotEmpty ? double.parse(_controllerOperatingCosts.text.replaceAll(",", "")) : 0.0;
    var _otherCosts = _controllerOtherCosts.text.isNotEmpty ? double.parse(_controllerOtherCosts.text.replaceAll(",", "")) : 0.0;
    var _netProfitBeforeTax = _grossProfit - _operatingCosts - _otherCosts;
    if(_grossProfit < 0) {
      _controllerOperatingCosts.clear();
      _controllerOtherCosts.clear();
      _controllerNetProfitBeforeTax.clear();
    } else {
      _controllerNetProfitBeforeTax.text = _formatCurrency.formatCurrency2(_netProfitBeforeTax.toString());
    }
  }

  void calculateNetProfitAfterTax() {
    var _netProfitBeforeTax = _controllerNetProfitBeforeTax.text.isNotEmpty ? double.parse(_controllerNetProfitBeforeTax.text.replaceAll(",", "")) : 0.0;
    var _tax = _controllerTax.text.isNotEmpty ? double.parse(_controllerTax.text.replaceAll(",", "")) : 0.0;
    var _netProfitAfterTax = _netProfitBeforeTax - _tax;
    if(_netProfitAfterTax < 0) {
      _controllerTax.clear();
      _controllerNetProfitBeforeTax.clear();
    } else {
      _controllerNetProfitAfterTax.text = _formatCurrency.formatCurrency2(_netProfitAfterTax.toString());
    }
  }

  void formattingCompany() {
    _controllerMonthlyIncome.text = formatCurrency.formatCurrency2(_controllerMonthlyIncome.text);
    _controllerOtherIncome.text = formatCurrency.formatCurrency2(_controllerOtherIncome.text);
    _controllerTotalIncome.text = formatCurrency.formatCurrency2(_controllerTotalIncome.text);
    _controllerCostOfRevenue.text = formatCurrency.formatCurrency2(_controllerCostOfRevenue.text);
    _controllerGrossProfit.text = formatCurrency.formatCurrency2(_controllerGrossProfit.text);
    _controllerOperatingCosts.text = formatCurrency.formatCurrency2(_controllerOperatingCosts.text);
    _controllerOtherCosts.text = formatCurrency.formatCurrency2(_controllerOtherCosts.text);
    _controllerNetProfitBeforeTax.text = formatCurrency.formatCurrency2(_controllerNetProfitBeforeTax.text);
    _controllerTax.text = formatCurrency.formatCurrency2(_controllerTax.text);
    _controllerNetProfitAfterTax.text = formatCurrency.formatCurrency2(_controllerNetProfitAfterTax.text);
    _controllerOtherInstallments.text = formatCurrency.formatCurrency2(_controllerOtherInstallments.text);
  }

  void clearDataPendapatan() {
    this._controllerMonthlyIncome.clear();
    this._controllerOtherIncome.clear();
    this._controllerTotalIncome.clear();
    this._controllerCostOfRevenue.clear();
    this._controllerGrossProfit.clear();
    this._controllerOperatingCosts.clear();
    this._controllerOtherCosts.clear();
    this._controllerNetProfitBeforeTax.clear();
    this._controllerTax.clear();
    this._controllerNetProfitAfterTax.clear();
    this._controllerOtherInstallments.clear();
    this._monthIncomeDakor = false;
    this._otherIncomeDakor = false;
    this._totalIncomeDakor = false;
    this._hppIncomeDakor = false;
    this._labaKotorDakor = false;
    this._operationalCostDakor = false;
    this._otherCostDakor = false;
    this._labaBeforeTaxDakor = false;
    this._taxDakor = false;
    this._labaAfterTaxDakor = false;
    this._otherInstallmentDakor = false;
  }

  List<MS2CustIncomeModel> _listIncome = [];

  List<MS2CustIncomeModel> get listIncome => _listIncome;

  void saveToSQLite(BuildContext context){
    addIncome();
    // _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDMonthlyIncome, "S", "001", "PENDAPATAN PERBULAN", _controllerMonthlyIncome.text.replaceAll(",", ""),
    //     null, null, null, null, 1, 1, _monthIncomeDakor ? "1" : "0"));
    // _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDOtherIncome, "+", "002", "PENDAPATAN LAINNYA", _controllerOtherIncome.text.replaceAll(",", ""),
    //     null, null, null, null, 1, 2, _otherIncomeDakor ? "1" : "0"));
    // _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDTotalIncome, "=", "003", "TOTAL PENDAPATAN", _controllerTotalIncome.text.replaceAll(",", ""),
    //     null, null, null, null, 1, 3, _totalIncomeDakor ? "1" : "0"));
    // _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDCostOfRevenue, "-", "004", "HARGA POKOK PENDAPATAN", _controllerCostOfRevenue.text.replaceAll(",", ""),
    //     null, null, null, null, 1, 4, _hppIncomeDakor ? "1" : "0"));
    // _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDGrossProfit, "=", "005", "LABA KOTOR", _controllerGrossProfit.text.replaceAll(",", ""),
    //     null, null, null, null, 1, 5, _labaKotorDakor ? "1" : "0"));
    // _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDOperatingCosts, "-", "006", "BIAYA OPERASIONAL", _controllerOperatingCosts.text.replaceAll(",", ""),
    //     null, null, null, null, 1, 6, _operationalCostDakor ? "1" : "0"));
    // _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDOtherCosts, "-", "007", "BIAYA LAINNYA", _controllerOtherCosts.text.replaceAll(",", ""),
    //     null, null, null, null, 1, 7, _otherCostDakor ? "1" : "0"));
    // _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDNetProfitBeforeTax, "=", "008", "LABA BERSIH SEBELUM PAJAK", _controllerNetProfitBeforeTax.text.replaceAll(",", ""),
    //     null, null, null, null, 1, 8, _labaBeforeTaxDakor ? "1" : "0"));
    // _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDTax, "-", "009", "PAJAK", _controllerTax.text.replaceAll(",", ""),
    //     null, null, null, null, 1, 9, _taxDakor ? "1" : "0"));
    // _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDNetProfitAfterTax, "=", "010", "LABA BERSIH SETELAH PAJAK", _controllerNetProfitAfterTax.text.replaceAll(",", ""),
    //     null, null, null, null, 1, 10, _labaAfterTaxDakor ? "1" : "0"));
    // _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDOtherInstallments, "N", "013", "ANGSURAN LAINNYA", _controllerOtherInstallments.text.replaceAll(",", ""),
    //     null, null, null, null, 1, null, _otherInstallmentDakor ? "1" : "0"));
    _dbHelper.insertMS2CustIncome(_listIncome);
  }

  Future<bool> deleteSQLite() async{
    return await _dbHelper.deleteMS2CustIncome();
  }

  Future<void> setDataFromSQLite(BuildContext context, int index) async{
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    List _data = await _dbHelper.selectDataIncome();
    if(_data.isNotEmpty){
      for(int i=0; i<_data.length; i++){
        if(_data[i]['income_type'] == "001"){
          this._customerIncomeIDMonthlyIncome = _data[i]['customerIncomeID'].toString();
          this._controllerMonthlyIncome.text = _data[i]['income_value'].toString();
          this._monthIncomeDakor = _data[i]['edit_income_value'] == "1";
        }
        else if(_data[i]['income_type'] == "002"){
          this._customerIncomeIDOtherIncome = _data[i]['customerIncomeID'].toString();
          this._controllerOtherIncome.text = _data[i]['income_value'].toString();
          this._otherIncomeDakor = _data[i]['edit_income_value'] == "1";
        }
        else if(_data[i]['income_type'] == "003"){
          this._customerIncomeIDTotalIncome = _data[i]['customerIncomeID'].toString();
          this._controllerTotalIncome.text = _data[i]['income_value'].toString();
          this._totalIncomeDakor = _data[i]['edit_income_value'] == "1";
        }
        else if(_data[i]['income_type'] == "004"){
          this._customerIncomeIDCostOfRevenue = _data[i]['customerIncomeID'].toString();
          this._controllerCostOfRevenue.text = _data[i]['income_value'].toString();
          this._hppIncomeDakor = _data[i]['edit_income_value'] == "1";
        }
        else if(_data[i]['income_type'] == "005"){
          this._customerIncomeIDGrossProfit = _data[i]['customerIncomeID'].toString();
          this._controllerGrossProfit.text = _data[i]['income_value'].toString();
          this._labaKotorDakor = _data[i]['edit_income_value'] == "1";
        }
        else if(_data[i]['income_type'] == "006"){
          this._customerIncomeIDOperatingCosts = _data[i]['customerIncomeID'].toString();
          this._controllerOperatingCosts.text = _data[i]['income_value'].toString();
          this._operationalCostDakor = _data[i]['edit_income_value'] == "1";
        }
        else if(_data[i]['income_type'] == "007"){
          this._customerIncomeIDOtherCosts = _data[i]['customerIncomeID'].toString();
          this._controllerOtherCosts.text = _data[i]['income_value'].toString();
          this._otherCostDakor = _data[i]['edit_income_value'] == "1";
        }
        else if(_data[i]['income_type'] == "008"){
          this._customerIncomeIDNetProfitBeforeTax = _data[i]['customerIncomeID'].toString();
          this._controllerNetProfitBeforeTax.text = _data[i]['income_value'].toString();
          this._labaBeforeTaxDakor = _data[i]['edit_income_value'] == "1";
        }
        else if(_data[i]['income_type'] == "009"){
          this._customerIncomeIDTax = _data[i]['customerIncomeID'].toString();
          this._controllerTax.text = _data[i]['income_value'].toString();
          this._taxDakor = _data[i]['edit_income_value'] == "1";
        }
        else if(_data[i]['income_type'] == "010"){
          this._customerIncomeIDNetProfitAfterTax = _data[i]['customerIncomeID'].toString();
          this._controllerNetProfitAfterTax.text = _data[i]['income_value'].toString();
          this._labaAfterTaxDakor = _data[i]['edit_income_value'] == "1";
        }
        else if(_data[i]['income_type'] == "013"){
          this._customerIncomeIDOtherInstallments = _data[i]['customerIncomeID'].toString();
          this._controllerOtherInstallments.text = _data[i]['income_value'].toString();
          this._otherInstallmentDakor = _data[i]['edit_income_value'] == "1";
        }
      }
    }

    addIncome();

    await checkDataDakor();
    formattingCompany();
    var _providerGuarantor = Provider.of<FormMGuarantorChangeNotifier>(context,listen: false);
    if(_preferences.getString("last_known_state") == "IDE" && index != null){
      if(index != 1){
        Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).isPendapatan = true;
        Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).selectedIndex += 1;
        await _providerGuarantor.setDataFromSQLite(context, index, 2);
      }
      else{
        Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).selectedIndex = 1;
      }
    }
    notifyListeners();
  }

  Future<void> checkDataDakor() async{
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    List _data = await _dbHelper.selectDataIncome();
    if(_data.isNotEmpty && _preferences.getString("last_known_state") == "DKR"){
      for(int i=0; i<_data.length; i++){
        if(_data[i]['income_type'] == "001"){
          _monthIncomeDakor = double.parse(this._controllerMonthlyIncome.text.replaceAll(",", "")).toStringAsFixed(0) != _data[i]['income_value'].toString() || _data[i]['edit_income_value'] == "1";
        }
        else if(_data[i]['income_type'] == "002"){
          _otherIncomeDakor = double.parse(this._controllerOtherIncome.text.replaceAll(",", "")).toStringAsFixed(0) != _data[i]['income_value'].toString() || _data[i]['edit_income_value'] == "1";
        }
        else if(_data[i]['income_type'] == "003"){
          _totalIncomeDakor = double.parse(this._controllerTotalIncome.text.replaceAll(",", "")).toStringAsFixed(0) != _data[i]['income_value'].toString() || _data[i]['edit_income_value'] == "1";
        }
        else if(_data[i]['income_type'] == "004"){
          _hppIncomeDakor = double.parse(this._controllerCostOfRevenue.text.replaceAll(",", "")).toStringAsFixed(0) != _data[i]['income_value'].toString() || _data[i]['edit_income_value'] == "1";
        }
        else if(_data[i]['income_type'] == "005"){
          _labaKotorDakor = double.parse(this._controllerGrossProfit.text.replaceAll(",", "")).toStringAsFixed(0) != _data[i]['income_value'].toString() || _data[i]['edit_income_value'] == "1";
        }
        else if(_data[i]['income_type'] == "006"){
          _operationalCostDakor = double.parse(this._controllerOperatingCosts.text.replaceAll(",", "")).toStringAsFixed(0) != _data[i]['income_value'].toString() || _data[i]['edit_income_value'] == "1";
        }
        else if(_data[i]['income_type'] == "007"){
          _otherCostDakor = double.parse(this._controllerOtherCosts.text.replaceAll(",", "")).toStringAsFixed(0) != _data[i]['income_value'].toString() || _data[i]['edit_income_value'] == "1";
        }
        else if(_data[i]['income_type'] == "008"){
          _labaBeforeTaxDakor = double.parse(this._controllerNetProfitBeforeTax.text.replaceAll(",", "")).toStringAsFixed(0) != _data[i]['income_value'].toString() || _data[i]['edit_income_value'] == "1";
        }
        else if(_data[i]['income_type'] == "009"){
          _taxDakor = double.parse(this._controllerTax.text.replaceAll(",", "")).toStringAsFixed(0) != _data[i]['income_value'].toString() || _data[i]['edit_income_value'] == "1";
        }
        else if(_data[i]['income_type'] == "010"){
          _labaAfterTaxDakor = double.parse(this._controllerNetProfitAfterTax.text.replaceAll(",", "")).toStringAsFixed(0) != _data[i]['income_value'].toString() || _data[i]['edit_income_value'] == "1";
        }
        else if(_data[i]['income_type'] == "013"){
          _otherInstallmentDakor = double.parse(this.controllerOtherInstallments.text.replaceAll(",", "")).toStringAsFixed(0) != _data[i]['income_value'].toString() || _data[i]['edit_income_value'] == "1";
        }
      }
    }
  }

  void getDataFromDashboard(BuildContext context){
    _setIncomeCompanyModel = Provider.of<DashboardChangeNotif>(context, listen: false).setIncomeCompanyModel;
  }

  bool isMonthIncomeVisible() => _setIncomeCompanyModel.monthIncomeVisible;
  bool isOtherIncomeVisible() => _setIncomeCompanyModel.otherIncomeVisible;
  bool isTotalIncomeVisible() => _setIncomeCompanyModel.totalIncomeVisible;
  bool isHppIncomeVisible() => _setIncomeCompanyModel.hppIncomeVisible;
  bool isLabaKotorVisible() => _setIncomeCompanyModel.labaKotorVisible;
  bool isOperationalCostVisible() => _setIncomeCompanyModel.operationalCostVisible;
  bool isOtherCostVisible() => _setIncomeCompanyModel.otherCostVisible;
  bool isLabaBeforeTaxVisible() => _setIncomeCompanyModel.labaBeforeTaxVisible;
  bool isTaxVisible() => _setIncomeCompanyModel.taxVisible;
  bool isLabaAfterTaxVisible() => _setIncomeCompanyModel.labaAfterTaxVisible;
  bool isOtherInstallmentVisible() => _setIncomeCompanyModel.otherInstallmentVisible;

  bool isMonthIncomeMandatory() => _setIncomeCompanyModel.monthIncomeMandatory;
  bool isOtherIncomeMandatory() => _setIncomeCompanyModel.otherIncomeMandatory;
  bool isTotalIncomeMandatory() => _setIncomeCompanyModel.totalIncomeMandatory;
  bool isHppIncomeMandatory() => _setIncomeCompanyModel.hppIncomeMandatory;
  bool isLabaKotorMandatory() => _setIncomeCompanyModel.labaKotorMandatory;
  bool isOperationalCostMandatory() => _setIncomeCompanyModel.operationalCostMandatory;
  bool isOtherCostMandatory() => _setIncomeCompanyModel.otherCostMandatory;
  bool isLabaBeforeTaxMandatory() => _setIncomeCompanyModel.labaBeforeVATMandatory;
  bool isTaxMandatory() => _setIncomeCompanyModel.taxMandatory;
  bool isLabaAfterTaxMandatory() => _setIncomeCompanyModel.labaAfterVatMandatory;
  bool isOtherInstallmentMandatory() => _setIncomeCompanyModel.otherInstallmentMandatory;

  bool get otherInstallmentDakor => _otherInstallmentDakor;

  set otherInstallmentDakor(bool value) {
    _otherInstallmentDakor = value;
    notifyListeners();
  }

  bool get labaAfterTaxDakor => _labaAfterTaxDakor;

  set labaAfterTaxDakor(bool value) {
    _labaAfterTaxDakor = value;
    notifyListeners();
  }

  bool get taxDakor => _taxDakor;

  set taxDakor(bool value) {
    _taxDakor = value;
    notifyListeners();
  }

  bool get labaBeforeTaxDakor => _labaBeforeTaxDakor;

  set labaBeforeTaxDakor(bool value) {
    _labaBeforeTaxDakor = value;
    notifyListeners();
  }

  bool get otherCostDakor => _otherCostDakor;

  set otherCostDakor(bool value) {
    _otherCostDakor = value;
    notifyListeners();
  }

  bool get operationalCostDakor => _operationalCostDakor;

  set operationalCostDakor(bool value) {
    _operationalCostDakor = value;
    notifyListeners();
  }

  bool get labaKotorDakor => _labaKotorDakor;

  set labaKotorDakor(bool value) {
    _labaKotorDakor = value;
    notifyListeners();
  }

  bool get hppIncomeDakor => _hppIncomeDakor;

  set hppIncomeDakor(bool value) {
    _hppIncomeDakor = value;
    notifyListeners();
  }

  bool get totalIncomeDakor => _totalIncomeDakor;

  set totalIncomeDakor(bool value) {
    _totalIncomeDakor = value;
    notifyListeners();
  }

  bool get otherIncomeDakor => _otherIncomeDakor;

  set otherIncomeDakor(bool value) {
    _otherIncomeDakor = value;
    notifyListeners();
  }

  bool get monthIncomeDakor => _monthIncomeDakor;

  set monthIncomeDakor(bool value) {
    _monthIncomeDakor = value;
    notifyListeners();
  }

  String get customerIncomeIDOtherInstallments =>
      _customerIncomeIDOtherInstallments;

  set customerIncomeIDOtherInstallments(String value) {
    this._customerIncomeIDOtherInstallments = value;
    notifyListeners();
  }

  String get customerIncomeIDNetProfitAfterTax =>
      _customerIncomeIDNetProfitAfterTax;

  set customerIncomeIDNetProfitAfterTax(String value) {
    this._customerIncomeIDNetProfitAfterTax = value;
    notifyListeners();
  }

  String get customerIncomeIDTax => _customerIncomeIDTax;

  set customerIncomeIDTax(String value) {
    this._customerIncomeIDTax = value;
    notifyListeners();
  }

  String get customerIncomeIDNetProfitBeforeTax =>
      _customerIncomeIDNetProfitBeforeTax;

  set customerIncomeIDNetProfitBeforeTax(String value) {
    this._customerIncomeIDNetProfitBeforeTax = value;
    notifyListeners();
  }

  String get customerIncomeIDOtherCosts => _customerIncomeIDOtherCosts;

  set customerIncomeIDOtherCosts(String value) {
    this._customerIncomeIDOtherCosts = value;
    notifyListeners();
  }

  String get customerIncomeIDOperatingCosts => _customerIncomeIDOperatingCosts;

  set customerIncomeIDOperatingCosts(String value) {
    this._customerIncomeIDOperatingCosts = value;
    notifyListeners();
  }

  String get customerIncomeIDGrossProfit => _customerIncomeIDGrossProfit;

  set customerIncomeIDGrossProfit(String value) {
    this._customerIncomeIDGrossProfit = value;
    notifyListeners();
  }

  String get customerIncomeIDCostOfRevenue => _customerIncomeIDCostOfRevenue;

  set customerIncomeIDCostOfRevenue(String value) {
    this._customerIncomeIDCostOfRevenue = value;
    notifyListeners();
  }

  String get customerIncomeIDTotalIncome => _customerIncomeIDTotalIncome;

  set customerIncomeIDTotalIncome(String value) {
    this._customerIncomeIDTotalIncome = value;
    notifyListeners();
  }

  String get customerIncomeIDOtherIncome => _customerIncomeIDOtherIncome;

  set customerIncomeIDOtherIncome(String value) {
    this._customerIncomeIDOtherIncome = value;
    notifyListeners();
  }

  String get customerIncomeIDMonthlyIncome => _customerIncomeIDMonthlyIncome;

  set customerIncomeIDMonthlyIncome(String value) {
    this._customerIncomeIDMonthlyIncome = value;
    notifyListeners();
  }

  void addIncome(){
    this._listIncome.clear();
    _listIncome.add(MS2CustIncomeModel("123", customerIncomeIDMonthlyIncome, "S", "001", "PENDAPATAN PERBULAN", _controllerMonthlyIncome.text.replaceAll(",", ""),
        null, null, null, null, 1, 1, _monthIncomeDakor ? "1" : "0"));
    _listIncome.add(MS2CustIncomeModel("123", customerIncomeIDOtherIncome, "+", "002", "PENDAPATAN LAINNYA", _controllerOtherIncome.text.replaceAll(",", ""),
        null, null, null, null, 1, 2, _otherIncomeDakor ? "1" : "0"));
    _listIncome.add(MS2CustIncomeModel("123", customerIncomeIDTotalIncome, "=", "003", "TOTAL PENDAPATAN", _controllerTotalIncome.text.replaceAll(",", ""),
        null, null, null, null, 1, 3, _totalIncomeDakor ? "1" : "0"));
    _listIncome.add(MS2CustIncomeModel("123", customerIncomeIDCostOfRevenue, "-", "004", "HARGA POKOK PENDAPATAN", _controllerCostOfRevenue.text.replaceAll(",", ""),
        null, null, null, null, 1, 4, _hppIncomeDakor ? "1" : "0"));
    _listIncome.add(MS2CustIncomeModel("123", customerIncomeIDGrossProfit, "=", "005", "LABA KOTOR", _controllerGrossProfit.text.replaceAll(",", ""),
        null, null, null, null, 1, 5, _labaKotorDakor ? "1" : "0"));
    _listIncome.add(MS2CustIncomeModel("123", customerIncomeIDOperatingCosts, "-", "006", "BIAYA OPERASIONAL", _controllerOperatingCosts.text.replaceAll(",", ""),
        null, null, null, null, 1, 6, _operationalCostDakor ? "1" : "0"));
    _listIncome.add(MS2CustIncomeModel("123", customerIncomeIDOtherCosts, "-", "007", "BIAYA LAINNYA", _controllerOtherCosts.text.replaceAll(",", ""),
        null, null, null, null, 1, 7, _otherCostDakor ? "1" : "0"));
    _listIncome.add(MS2CustIncomeModel("123", customerIncomeIDNetProfitBeforeTax, "=", "008", "LABA BERSIH SEBELUM PAJAK", _controllerNetProfitBeforeTax.text.replaceAll(",", ""),
        null, null, null, null, 1, 8, _labaBeforeTaxDakor ? "1" : "0"));
    _listIncome.add(MS2CustIncomeModel("123", customerIncomeIDTax, "-", "009", "PAJAK", _controllerTax.text.replaceAll(",", ""),
        null, null, null, null, 1, 9, _taxDakor ? "1" : "0"));
    _listIncome.add(MS2CustIncomeModel("123", customerIncomeIDNetProfitAfterTax, "=", "010", "LABA BERSIH SETELAH PAJAK", _controllerNetProfitAfterTax.text.replaceAll(",", ""),
        null, null, null, null, 1, 10, _labaAfterTaxDakor ? "1" : "0"));
    _listIncome.add(MS2CustIncomeModel("123", customerIncomeIDOtherInstallments, "N", "013", "ANGSURAN LAINNYA", _controllerOtherInstallments.text.replaceAll(",", ""),
        null, null, null, null, 1, null, _otherInstallmentDakor ? "1" : "0"));
  }
}
