import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/source_order_name_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SearchSourceOrderNameKaryawanChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  bool _loadData = false;
  TextEditingController _controllerSearch = TextEditingController();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<SourceOrderNameModel> _listSourceOrderNameKaryawan = [];
  List<SourceOrderNameModel> _listSourceOrderNameKaryawanTemp = [];

  TextEditingController get controllerSearch => _controllerSearch;

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void  changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<SourceOrderNameModel> get listSourceOrderNameKaryawan {
    return UnmodifiableListView(this._listSourceOrderNameKaryawan);
  }

  UnmodifiableListView<SourceOrderNameModel> get listSourceOrderNameKaryawanTemp {
    return UnmodifiableListView(this._listSourceOrderNameKaryawanTemp);
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  void getSourceOrderNameKaryawan(String query) async{
    this._listSourceOrderNameKaryawan.clear();
    this._listSourceOrderNameKaryawanTemp.clear();
    // SharedPreferences _preferences = await SharedPreferences.getInstance();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
    var storage = FlutterSecureStorage();

    String _namaSumberOrderKaryawan = await storage.read(key: "NamaSumberOrderKaryawan");
    var _body = jsonEncode({
      "Nik": [query]
    });
    final _response = await _http.post(
        "${BaseUrl.urlGeneral}$_namaSumberOrderKaryawan",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      List _data = _result;
      if(_data.isEmpty){
        showSnackBar("Nama Sumber Order tidak ditemukan");
        loadData = false;
      }
      else{
        for(int i=0; i <_data.length; i++){
          this._listSourceOrderNameKaryawan.add(
            SourceOrderNameModel(
              _data[i]['login'],
              _data[i]['objectName']
            )
          );
        }
        loadData = false;
      }
    }
    else{
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState. showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  // void searchSourceOrderNameKaryawan(String query) async {
  //   if(query.length < 3) {
  //     showSnackBar("Input minimal 3 karakter");
  //   }
  //   else {
  //     _listSourceOrderNameKaryawanTemp.clear();
  //     if (query.isEmpty) {
  //       return;
  //     }
  //
  //     _listSourceOrderNameKaryawan.forEach((dataSourceOrderNameKaryawan) {
  //       if (dataSourceOrderNameKaryawan.kode.contains(query) || dataSourceOrderNameKaryawan.deskripsi.contains(query)) {
  //         this._listSourceOrderNameKaryawanTemp.add(dataSourceOrderNameKaryawan);
  //       }
  //     });
  //     notifyListeners();
  //   }
  // }

  void clearSearchTemp() {
    _listSourceOrderNameKaryawanTemp.clear();
  }
}
