import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/program_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';

class SearchProgramChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  TextEditingController _controllerSearch = TextEditingController();
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<ProgramModel> _listProgramModel = [
    // ProgramModel("00084", "CSOM"),
    // ProgramModel("00085", "MOLADIN"),
    // ProgramModel("00086", "MOTORAN"),
    // ProgramModel("00087", "OTOMOTO"),
    // ProgramModel("00088", "OLX"),
    // ProgramModel("00089", "ADEX"),
    // ProgramModel("0008C", "CARMUDI"),
  ];
  List<ProgramModel> _listProgramTemp = [];

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<ProgramModel> get listProgramModel {
    return UnmodifiableListView(this._listProgramModel);
  }

  UnmodifiableListView<ProgramModel> get listProgramTemp {
    return UnmodifiableListView(this._listProgramTemp);
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  void getProgram(BuildContext context) async{
    var _providerObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    this._listProgramModel.clear();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var _body = jsonEncode({
      "refOne": "${_providerObjectUnit.prodMatrixId}",
    });
    print("body program (pakai prodmatrixid) = $_body");

    var storage = FlutterSecureStorage();
    String _fieldProgram = await storage.read(key: "FieldProgram");
    final _response = await _http.post(
      "${BaseUrl.urlGeneral}$_fieldProgram",
      //   "${urlPublic}program-kegiatan/get_program",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      if(_result.isEmpty){
        showSnackBar("Program tidak ditemukan");
        loadData = false;
      }
      else{
        for(int i=0; i <_result.length; i++){
          this._listProgramModel.add(
              ProgramModel(_result[i]['kode'], _result[i]['deskripsi'])
          );
        }
        loadData = false;
      }
    }
    else{
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void searchProgram(String query) {
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    } else {
      _listProgramTemp.clear();
      if (query.isEmpty) {
        return;
      }

      _listProgramModel.forEach((dataProgram) {
        if (dataProgram.kode.contains(query) || dataProgram.deskripsi.contains(query)) {
          _listProgramTemp.add(dataProgram);
        }
      });
    }
    notifyListeners();
  }

  void clearSearchTemp() {
    _listProgramTemp.clear();
  }
}
