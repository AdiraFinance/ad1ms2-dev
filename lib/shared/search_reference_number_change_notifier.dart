import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/models/reference_number_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';
import 'change_notifier_app/information_collatelar_change_notifier.dart';

class SearchReferenceNumberChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  bool _loadData = false;
  TextEditingController _controllerSearch = TextEditingController();
  List<ReferenceNumberModel> _listReferenceNumber = [];
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController get controllerSearch => _controllerSearch;
  String _radioValue = "Kontrak";

  String get radioValue => _radioValue;

  set radioValue(String value) {
    this._radioValue = value;
  }

  bool get showClear => _showClear;

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<ReferenceNumberModel> get listReferenceNumber {
    return UnmodifiableListView(this._listReferenceNumber);
  }

  void getReferenceNumber(String query,BuildContext context) async{
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    } else {
      loadData = true;
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

      final _http = IOClient(ioc);
      SharedPreferences _preferences = await SharedPreferences.getInstance();
      var _providerColla = Provider.of<InformationCollateralChangeNotifier>(context, listen: false);

      var _body = jsonEncode({
        "jenisRehab": "${Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).rehabTypeSelected.id}",
        "lmeID": _preferences.getString("lme_id") != null ? _preferences.getString("lme_id") : "",
        "lmeOpsi": _preferences.getString("opsi_multidisburse") != null ? _preferences.getString("opsi_multidisburse") : "",
        "nomorMesin": _providerColla.controllerMachineNumber.text,
        "nomorRangka": _providerColla.controllerFrameNumber.text,
        "referenceSearch": query,
        "referenceType": this._radioValue,
      });
      print("body reference = $_body");

      var storage = FlutterSecureStorage();
      String _fieldNoReference = await storage.read(key: "FieldNoReference");
      final _response = await _http.post(
        "${BaseUrl.urlGeneral}$_fieldNoReference",
          // "${urlPublic}no-refference/get_no_reff",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      );

      if(_response.statusCode == 200){
        this._listReferenceNumber.clear();
        final _result = jsonDecode(_response.body);
        List _data = [];
        if(_result['data'] != null){
          _data = _result['data'];
        }
        print("result no referensi = $_data");
        if(_data.isNotEmpty){
          for(int i = 0; i < _data.length; i++){
            _listReferenceNumber.add(ReferenceNumberModel(_data[i]['APPL_CONTRACT_NO'], _data[i]['CUST_NAME']));
          }
          loadData = false;
        }
        else{
          showSnackBar("${_result['message']}");
          loadData = false;
        }
      }
      else{
        showSnackBar("Error get no reference response ${_response.statusCode}");
        loadData = false;
      }
    }
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  void filterDialog(BuildContext context) {
    showModalBottomSheet(context: context,
        builder: (_) {
          return Theme(
            data: ThemeData(
                accentColor: primaryOrange, primaryColor: myPrimaryColor, primarySwatch: primaryOrange),
            child: Container(child: StatefulBuilder(
                builder: (BuildContext context, StateSetter stateSetter) {
              return Wrap(
                children: [
                  Row(
                    children: [
                      Row(
                        children: [
                          Radio(
                              value: "Kontrak",
                              groupValue: this._radioValue,
                              onChanged: (value) {
                                stateSetter(() {
                                  return this._radioValue = value;
                                });
                              }),
                          SizedBox(width: 16),
                          Text("Kontrak")
                        ],
                      ),
                      Row(
                        children: [
                          Radio(
                              value: "Nama",
                              groupValue: this._radioValue,
                              onChanged: (value) {
                                stateSetter(() {
                                  return this._radioValue = value;
                                });
                              }),
                          SizedBox(width: 16),
                          Text("Nama")
                        ],
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8)),
                        color: myPrimaryColor,
                        onPressed: () {
                          Navigator.pop(context);
                          print("cek ${this._radioValue}");
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("APPLY"),
                          ],
                        )),
                  )
                ],
              );
            })),
          );
        });
  }
}
