import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/models/list_oid_company_model.dart';
import 'package:ad1ms2_dev/models/list_oid_model.dart';
import 'package:ad1ms2_dev/screens/dashboard.dart';
import 'package:ad1ms2_dev/screens/form_m/form_m_credit_limit.dart';
import 'package:ad1ms2_dev/shared/form_m_company_rincian_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_keluarga(ibu)_change_notif.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:dio/dio.dart' as Dio;

import '../main.dart';
import 'constants.dart';

class ListOidChangeNotifier with ChangeNotifier {
  String _customerType;
  // GlobalKey<FormState> _key = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String _statusAoro = "";
  String _statusNasabah = "";
  bool _autoValidate = false;
  bool _flag = false;
  bool _joinDedupOtomatis = false;
  int _selectedIndex = -1;
  bool _loadData = false;
  bool _loadAoro = false;
  List<ListOidModel> _listOid = [];
  List<ListOIDCompanyModel> _listOIDCompany = [];
  ListOidModel _listOIDPersonalSelected;
  ListOIDCompanyModel _listOIDCompanySelected;
  // DbHelper _dbHelper = DbHelper();
  var storage = FlutterSecureStorage();

  // GlobalKey<FormState> get key => _key;
  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  // Auto Validate
  bool get autoValidate {
    return _autoValidate;
  }

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  // Join Dedup Otomatis
  bool get joinDedupOtomatis => _joinDedupOtomatis;

  set joinDedupOtomatis(bool value) {
    this._joinDedupOtomatis = value;
    notifyListeners();
  }

  // Flag
  bool get flag {
    return _flag;
  }

  set flag(bool value) {
    this._flag = value;
    notifyListeners();
  }

  String get statusAoro => _statusAoro;

  set statusAoro(String value) {
    this._statusAoro = value;
    notifyListeners();
  }

  String get statusNasabah => _statusNasabah;

  set statusNasabah(String value) {
    this._statusNasabah = value;
    notifyListeners();
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  bool get loadAoro => _loadAoro;

  set loadAoro(bool value) {
    _loadAoro = value;
    notifyListeners();
  }

  String get customerType => _customerType;

  set customerType(String value) {
    this._customerType = value;
  }

  int get selectedIndex => _selectedIndex;

  set selectedIndex(int value) {
    this._selectedIndex = value;
    notifyListeners();
  }

  List<ListOidModel> get listOid => _listOid;

  List<ListOIDCompanyModel> get listOIDCompany => _listOIDCompany;

  void onBackPress(BuildContext context) {
    Navigator.pop(context);
  }

  ListOIDCompanyModel get listOIDCompanySelected => _listOIDCompanySelected;

  set listOIDCompanySelected(ListOIDCompanyModel value) {
    this._listOIDCompanySelected = value;
    notifyListeners();
  }

  ListOidModel get listOIDPersonalSelected => _listOIDPersonalSelected;

  set listOIDPersonalSelected(ListOidModel value) {
    this._listOIDPersonalSelected = value;
    notifyListeners();
  }

  bool _processSubmit = false;

  bool get processSubmit => _processSubmit;

  set processSubmit(bool value) {
    this._processSubmit = value;
    notifyListeners();
  }

  void getListOid(BuildContext context, Map dataDedup, String flag, String identityNumber, String fullname, DateTime birthDate, String birthPlace, String motherName, String identityAddress) async {
    print("get list oid");
    this._listOid.clear();
    this._selectedIndex = -1;
    _loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var _body = jsonEncode({
      "P_AC_ID_NO": identityNumber,
      "P_AC_CUST_NAME": fullname,
      "P_AC_MOTHER_NAME": motherName,
      "P_AC_DATE_BIRTH": formatDateDedup(birthDate),
      "P_AC_PLACE_BIRTH": birthPlace,
      "P_AC_ADDRESS": identityAddress,
    });
    print("body getlist oid: $_body");
    // List<ListOidModel> _listTemp = [];
    // _listTemp.clear();
    String _similarityIndividu = await storage.read(key: "SimilarityIndividu");
    print("${BaseUrl.urlGeneral}$_similarityIndividu");
    try{
      final _response = await _http.post(
        "${BaseUrl.urlGeneral}$_similarityIndividu",
        // "${BaseUrl.urlAcction}adira-acction-prod/service/dedup/getCustomerDedupInd",
        // "${urlPublicAcction}dedup/Customer/getCustomerDedupInd",
        // "http://10.81.3.137:99/DedupAPI/api/Customer/getCustomerDedupInd",
        // "https://103.110.89.34/public/ms2dev/api/dedup/getCustomerDedupInd",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 10));

      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        debugPrint("DATA_DEDUP $_result");
        final _data = _result['P_RC1'];

        if(_data != null){
          if(_data.isNotEmpty){
            // print("list oid: $_data");
            for(int i = 0; i < _data.length; i++){
              this._listOid.add(
                ListOidModel(
                  _data[i]['AC_CUST_ID'],
                  _data[i]['AC_ID_NO'],
                  _data[i]['AC_CUST_NAME'],
                  _data[i]['AC_DATE_BIRTH'],
                  _data[i]['AC_MOTHER_NAME'],
                  _data[i]['AC_PLACE_BIRTH'],
                  _data[i]['AC_ADDRESS'],
                  _data[i]['PARA_CUST_TYPE_ID'],
                  _data[i]['AC_FLAG_SOURCE'],
                  _data[i]['SCORE'].toString(),
                  _data[i]['MODIFIED_DATE'],
                  _data[i]['AC_FLAG_DETAIL'],
                  _data[i]['COLUMN_JOIN'].toString(),
                  _data[i]['PARA_CUST_NO'],
                  _data[i]['AC_BR_ID']
                )
              );
//          _listTemp.add(ListOidModel(
//              _data[i]['NUMBER_IDENTITY'],
//              _data[i]['FULLNAME'],
//              _data[i]['BIRTH_DATE'],
//              _data[i]['BIRTH_PLACE'],
//              _data[i]['MOTHER_NAME'],
//              _data[i]['IDENTITY_ADDRESS'],
//              _data[i]['SCORE'],
//              _data[i]['JOIN'],
//          ));
            }
//        var _uniqueIdentityNumber = _listTemp.map((e) => e.NUMBER_IDENTITY.trim()).toSet().toList();
//        var _uniqueFullname = _listTemp.map((e) => e.FULLNAME.trim()).toSet().toList();
//        var _uniqueBirthDate = _listTemp.map((e) => e.BIRTH_DATE.trim()).toSet().toList();
//        var _uniqueBirthPlace = _listTemp.map((e) => e.BIRTH_PLACE.trim()).toSet().toList();
//        var _uniqueMotherName = _listTemp.map((e) => e.MOTHER_NAME.trim()).toSet().toList();
//        var _uniqueIdentityAddress = _listTemp.map((e) => e.IDENTITY_ADDRESS.trim()).toSet().toList();
//        var _uniqueScore = _listTemp.map((e) => e.SCORE.trim()).toSet().toList();
//        var _uniqueJoin = _listTemp.map((e) => e.JOIN.trim()).toSet().toList();
//
//        for(var i =0; i<_uniqueIdentityNumber.length; i++){
//          ListOidModel _myData = ListOidModel(
//            _uniqueIdentityNumber[i],
//            _uniqueFullname[i],
//            _uniqueBirthDate[i],
//            _uniqueBirthPlace[i],
//            _uniqueMotherName[i],
//            _uniqueIdentityAddress[i],
//            _uniqueScore[i],
//            _uniqueJoin[i],
//          );
//          this._listOid.add(_myData);
//        }
            // Ngecek kondisi Join OID
            if(_data.length == 1 && _data[0]['COLUMN_JOIN'] == 1.0) {
              // Kondisi 1: Coloum_join = 1 dan responsenya hanya 1 data (join otomatis)
              _joinDedupOtomatis = true;
              _listOIDPersonalSelected = _listOid[0];
              print("kondisi 1");
              await getOrderTypeAORO(context, flag, _listOIDPersonalSelected, identityNumber, fullname,
                  birthDate, birthPlace, motherName, identityAddress);
              // getStatusNasabah(context, flag, _listOIDPersonalSelected, identityNumber, fullname,
              //     birthDate, birthPlace, motherName, identityAddress);
            } else if(_data.length > 1 && _data[0]['COLUMN_JOIN'] == 1.0) {
              _joinDedupOtomatis = false;
            } else if(_data.length > 1 && _data[0]['COLUMN_JOIN'] == 0.0) {
              _joinDedupOtomatis = false;
            }
            this._loadData = false;
          }
          else{
            showSnackBar("Hasil dedup tidak ditemukan");
            this._loadData = false;
            // Kondisi 4: Data tidak ada (join otomatis)
            setValueFromDedup(context, dataDedup);
            submitNewOid(context, identityNumber, fullname,
                birthDate, birthPlace, motherName, identityAddress, flag);
          }
        }
        else{
          showSnackBar("Hasil dedup tidak ditemukan");
          this._loadData = false;
          // Kondisi 4: Data tidak ada (join otomatis)
          setValueFromDedup(context, dataDedup);
          submitNewOid(context, identityNumber, fullname,
              birthDate, birthPlace, motherName, identityAddress, flag);
        }
      }
      else{
        showSnackBar("Error get OID individu response ${_response.statusCode}");
        this._loadData = false;
      }
    }
    on TimeoutException catch(_){
      showSnackBar("Request Timeout APK");
      this._loadData = false;
    }
    catch(e){
      showSnackBar("Error $e");
      this._loadData = false;
    }
    notifyListeners();
  }

  void getListOidCompany(BuildContext context, Map dataDedup, String flag, String npwpNumber, String custName, DateTime establishedDate, String address) async {
    this._listOIDCompany.clear();
    this._selectedIndex = -1;
    _loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var _body = jsonEncode({
      "P_AC_NPWP_NUMBER": npwpNumber,
      "P_AC_CUST_NAME": custName,
      "P_AC_ESTABILISHED_DATE": formatDateDedup(establishedDate),
      "P_AC_ADDRESS": address
    });

    // List<ListOidModel> _listTemp = [];
    // _listTemp.clear();
    String _similarityCompany = await storage.read(key: "SimilarityCompany");
    try{
      final _response = await _http.post(
        "${BaseUrl.urlGeneral}$_similarityCompany",
        // "${BaseUrl.urlAcction}adira-acction-prod/service/dedup/getCustomerDedupInd",
        // "${urlPublicAcction}dedup/Customer/getCustomerDedupCom",
        // "http://10.81.3.137:99/DedupAPI/api/Customer/getCustomerDedupCom",
        // "https://103.110.89.34/public/ms2dev/api/dedup/getCustomerDedupInd",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 10));

      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        final _data = _result['P_RC1'];
        if(_data != null){
          for(int i = 0; i < _data.length; i++){
            print("data: ${_data[i]}");
            this._listOIDCompany.add(
              ListOIDCompanyModel(
                _data[i]['AC_CUST_ID'],
                _data[i]['AC_NPWP_NUMBER'],
                _data[i]['AC_CUST_NAME'],
                _data[i]['AC_ESTABILISHED_DATE'],
                _data[i]['AC_MOTHER_NAME'],
                _data[i]['AC_PLACE_BIRTH'],
                _data[i]['AC_ADDRESS'],
                _data[i]['PARA_CUST_TYPE_ID'],
                _data[i]['AC_FLAG_SOURCE'],
                _data[i]['SCORE'].toString(),
                _data[i]['MODIFIED_DATE'],
                _data[i]['AC_FLAG_DETAIL'],
                _data[i]['COLUMN_JOIN'].toString(),
                _data[i]['PARA_CUST_NO'],
                _data[i]['AC_BR_ID']
              )
            );
          }

          if(_data.length == 1 && _data[0]['COLUMN_JOIN'] == 1.0) {
            _joinDedupOtomatis = true;
            _listOIDCompanySelected = _listOIDCompany[0];
            await getOrderTypeAOROCompany(context, flag, _listOIDCompanySelected, npwpNumber, custName, establishedDate, address);
            // submitNewOidCompany(context, flag, npwpNumber, custName, establishedDate, address);
          } else if(_data.length > 1 && _data[0]['COLUMN_JOIN'] == 1.0) {
            _joinDedupOtomatis = false;
          } else if(_data.length > 1 && _data[0]['COLUMN_JOIN'] == 0.0) {
            _joinDedupOtomatis = false;
          }
          this._loadData = false;
        }
        else{
          showSnackBar("Hasil dedup tidak ditemukan");
          this._loadData = false;
          // Kondisi 4: Data tidak ada (join otomatis)
          setValueCompanyFromDedup(context, dataDedup);
          submitNewOidCompany(context, flag, npwpNumber, custName, establishedDate, address);
        }
      }
      else{
        showSnackBar("Error get OID company response ${_response.statusCode}");
        this._loadData = false;
      }
    }
    on TimeoutException catch(_){
      showSnackBar("Request Timeout APK");
      this._loadData = false;
    }
    catch(e){
      showSnackBar("Error $e");
      this._loadData = false;
    }
    notifyListeners();
  }

  Future<void> getOrderTypeAORO(BuildContext context, String flag, ListOidModel listOIdPersonalSelected, String identityNumber,
      String fullname, DateTime initialDateBirthDate, String birthPlace, String motherName, String identityAddress) async {
    _loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    // final _http = IOClient(ioc);

    String _getOrderType = await storage.read(key: "GetOrderType(AORO)");
    // final _response = await http.get(
    //     "${BaseUrl.urlGeneral}$_getOrderType${listOIdPersonalSelected.AC_CUST_ID}",
    //     headers: {'Content-type': 'application/x-www-form-urlencoded'}
    // );

    var _response = await Dio.Dio().get(
        "${BaseUrl.urlGeneral}$_getOrderType${listOIdPersonalSelected.AC_CUST_ID}",
        options: Dio.Options(
            followRedirects: false,
            headers: {"Authorization": "bearer $token"},
            validateStatus: (status) { return status < 500; }
        )
    );
    if(_response.statusCode == 200 || _response.statusCode == 302){
      final _result = _response.data;
      if(_result.isNotEmpty){
        this._statusAoro = _result;
        loadData = false;
        getStatusNasabah(context, flag, _listOIDPersonalSelected, identityNumber, fullname,
            initialDateBirthDate, birthPlace, motherName, identityAddress);
      }
      else{
        showSnackBar("Status AORO tidak ditemukan");
        loadData = false;
      }
    }
    else{
      showSnackBar("Error get Status AORO response ${_response.statusCode}");
      loadData = false;
    }
  }

  Future<void> getOrderTypeAOROCompany(BuildContext context, String flag, ListOIDCompanyModel listOIDCompanySelected, String npwpNumber,
      String custName, DateTime establishedDate, String address) async {
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);

    String _getOrderType = await storage.read(key: "GetOrderType(AORO)");

    // final _response = await _http.get(
    //     "${BaseUrl.urlGeneral}$_getOrderType${listOIdCompanySelected.AC_CUST_ID}",
    //     headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    // );
    var _response = await Dio.Dio().get(
        "${BaseUrl.urlGeneral}$_getOrderType${_listOIDCompanySelected.AC_CUST_ID}",
        options: Dio.Options(
            followRedirects: false,
            headers: {"Authorization": "bearer $token"},
            validateStatus: (status) { return status < 500; }
        )
    );
    if(_response.statusCode == 200 || _response.statusCode == 302){
      print("response: ${_response.data}");
      final _result = _response.data;
      print("result: $_result");
      if(_result.isNotEmpty){
        this._statusAoro = _result;
        loadData = false;
        submitJoinOidCompany(context, flag, _listOIDCompanySelected, npwpNumber, custName, establishedDate, address);
      }
      else{
        showSnackBar("Status AORO tidak ditemukan");
        loadData = false;
      }
    }
    else{
      showSnackBar("Error get Status AORO response ${_response.statusCode}");
      loadData = false;
    }
  }

  void getStatusNasabah(BuildContext context, String flag, ListOidModel listOIDPersonalSelected, String identityNumber,
      String fullname, DateTime initialDateBirthDate, String birthPlace, String motherName, String identityAddress) async {
    loadAoro = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);

    var _body = jsonEncode({
      "AC_CUST_ID": _listOIDPersonalSelected.AC_CUST_ID,
      "AC_ID_NO": _listOIDPersonalSelected.AC_ID_NO,
      "AC_CUST_NAME": _listOIDPersonalSelected.AC_CUST_NAME,
      "AC_DATE_BIRTH": _listOIDPersonalSelected.AC_DATE_BIRTH,
      "AC_MOTHER_NAME": _listOIDPersonalSelected.AC_MOTHER_NAME,
      "AC_PLACE_BIRTH": _listOIDPersonalSelected.AC_PLACE_BIRTH,
      "AC_ADDRESS": _listOIDPersonalSelected.AC_ADDRESS,
      "PARA_CUST_TYPE_ID": _listOIDPersonalSelected.PARA_CUST_TYPE_ID,
      "AC_FLAG_SOURCE": _listOIDPersonalSelected.AC_FLAG_SOURCE,
      "SCORE": _listOIDPersonalSelected.SCORE,
      "MODIFIED_DATE": _listOIDPersonalSelected.MODIFIED_DATE,
      "AC_FLAG_DETAIL": _listOIDPersonalSelected.AC_FLAG_DETAIL,
      "COLUMN_JOIN": _listOIDPersonalSelected.COLUMN_JOIN,
      "PARA_CUST_NO": _listOIDPersonalSelected.PARA_CUST_NO,
      "AC_BR_ID": _listOIDPersonalSelected.AC_BR_ID
    });
    print("body status nasabah: $_body");

    String _statusAOROIndividu = await storage.read(key: "GetStatusNasabahIndividu");
    try{
      final _response = await _http.post(
        "${BaseUrl.urlAcction}$_statusAOROIndividu",
        // "${urlAcction}adira-acction-prod/acction/service/dedup/getStatusNasabah",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 10));

      if(_response.statusCode == 200 || _response.statusCode == 302){
        final _result = jsonDecode(_response.body);
        print("result status nasabah: $_result");
        // final _data = _result['P_RC1'];
        if(_result.isNotEmpty){
          this._statusNasabah = _result;
          // for(int i = 0; i < _data.length; i++){
          //   this._listOid.add(
          //     ListOidModel(
          //       _data[i]['AC_CUST_ID'],
          //       _data[i]['AC_ID_NO'],
          //       _data[i]['AC_CUST_NAME'],
          //       _data[i]['AC_DATE_BIRTH'],
          //       _data[i]['AC_MOTHER_NAME'],
          //       _data[i]['AC_PLACE_BIRTH'],
          //       _data[i]['AC_ADDRESS'],
          //       _data[i]['PARA_CUST_TYPE_ID'],
          //       _data[i]['AC_FLAG_SOURCE'],
          //       _data[i]['SCORE'].toString(),
          //       _data[i]['MODIFIED_DATE'],
          //       _data[i]['AC_FLAG_DETAIL'],
          //       _data[i]['COLUMN_JOIN'].toString(),
          //       _data[i]['PARA_CUST_NO'],
          //       _data[i]['AC_BR_ID']
          //     )
          //   );
          // }
          loadAoro = false;
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => FormMCreditLimit(
                flag: flag,
                model: listOIDPersonalSelected,
                identityNumber : identityNumber,
                fullName : fullname,
                initialDateBirthDate : initialDateBirthDate,
                birthPlace : birthPlace,
                motherName : motherName,
                identityAddress : identityAddress,
              ))
          ).then((value) => _joinDedupOtomatis ? Navigator.pop(context) : null);
        }
        else{
          showSnackBar("Failed to get Status AORO");
          loadAoro = false;
        }
      } else{
        showSnackBar("Error get Status AORO response ${_response.statusCode}");
        loadAoro = false;
      }
    } on TimeoutException catch(_){
      showSnackBar("Request Timeout get Status AORO APK");
      loadAoro = false;
    } catch(e){
      showSnackBar("Error $e");
      loadAoro = false;
    }
    notifyListeners();
  }

  // void getStatusCompany(BuildContext context, String flag, ListOIDCompanyModel listOIdCompanySelected, String npwpNumber,
  //     String custName, DateTime establishedDate, String address) async {
  //   loadAoro = true;
  //   final ioc = new HttpClient();
  //   ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
  //
  //   final _http = IOClient(ioc);
  //
  //   var _body = jsonEncode({
  //     "AC_CUST_ID": _listOIdCompanySelected.AC_CUST_ID,
  //     "AC_NPWP_NUMBER": _listOIdCompanySelected.AC_NPWP_NUMBER,
  //     "AC_CUST_NAME": _listOIdCompanySelected.AC_CUST_NAME,
  //     "AC_ESTABILISHED_DATE": _listOIdCompanySelected.AC_ESTABILISHED_DATE,
  //     "AC_MOTHER_NAME": _listOIdCompanySelected.AC_MOTHER_NAME,
  //     "AC_PLACE_BIRTH": _listOIdCompanySelected.AC_PLACE_BIRTH,
  //     "AC_ADDRESS": _listOIdCompanySelected.AC_ADDRESS,
  //     "PARA_CUST_TYPE_ID": _listOIdCompanySelected.PARA_CUST_TYPE_ID,
  //     "AC_FLAG_SOURCE": _listOIdCompanySelected.AC_FLAG_SOURCE,
  //     "SCORE": _listOIdCompanySelected.SCORE,
  //     "MODIFIED_DATE": _listOIdCompanySelected.MODIFIED_DATE,
  //     "AC_FLAG_DETAIL": _listOIdCompanySelected.AC_FLAG_DETAIL,
  //     "COLUMN_JOIN": _listOIdCompanySelected.COLUMN_JOIN,
  //     "PARA_CUST_NO": _listOIdCompanySelected.PARA_CUST_NO,
  //     "AC_BR_ID": _listOIdCompanySelected.AC_BR_ID
  //   });
  //
  //   String _statusAOROCompany = await storage.read(key: "StatusAOROCompany");
  //   try{
  //     final _response = await _http.post(
  //       "${BaseUrl.urlAcction}$_statusAOROCompany",
  //       // "${urlAcction}adira-acction-prod/acction/service/dedup/getStatusNasabahComp",
  //       body: _body,
  //       headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
  //     ).timeout(Duration(seconds: 10));
  //
  //     if(_response.statusCode == 200){
  //       final _result = jsonDecode(_response.body);
  //       // final _data = _result;
  //       if(_result.isNotEmpty){
  //         _statusAoro = _result;
  //         // for(int i = 0; i < _data.length; i++){
  //         //   this._listOid.add(
  //         //     ListOidModel(
  //         //       _data[i]['AC_CUST_ID'],
  //         //       _data[i]['AC_ID_NO'],
  //         //       _data[i]['AC_CUST_NAME'],
  //         //       _data[i]['AC_DATE_BIRTH'],
  //         //       _data[i]['AC_MOTHER_NAME'],
  //         //       _data[i]['AC_PLACE_BIRTH'],
  //         //       _data[i]['AC_ADDRESS'],
  //         //       _data[i]['PARA_CUST_TYPE_ID'],
  //         //       _data[i]['AC_FLAG_SOURCE'],
  //         //       _data[i]['SCORE'].toString(),
  //         //       _data[i]['MODIFIED_DATE'],
  //         //       _data[i]['AC_FLAG_DETAIL'],
  //         //       _data[i]['COLUMN_JOIN'].toString(),
  //         //       _data[i]['PARA_CUST_NO'],
  //         //       _data[i]['AC_BR_ID']
  //         //     )
  //         //   );
  //         // }
  //         submitNewOidCompany(context, flag, npwpNumber, custName, establishedDate, address);
  //         loadAoro = false;
  //       }
  //       else{
  //         showSnackBar("Failed to get Status AORO");
  //         loadAoro = false;
  //       }
  //     } else{
  //       showSnackBar("Error get Status AORO response ${_response.statusCode}");
  //       loadAoro = false;
  //     }
  //   } on TimeoutException catch(_){
  //     showSnackBar("Request Timeout get Status AORO APK");
  //     loadAoro = false;
  //   } catch(e){
  //     showSnackBar("Error $e");
  //     loadAoro = false;
  //   }
  //   notifyListeners();
  // }

  void submitNewOid(BuildContext context, String identityNumber, String fullname, DateTime birthDate, String birthPlace,
      String motherName, String identityAddress, String flag) async {
    debugPrint("SubmitNewOID EXEXCUTED");
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    processSubmit = true;
    // print("cek proses $processSubmit");
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var _body = jsonEncode({
      "P_ADDRESS": identityAddress != null ? identityAddress : "",
      "P_CP_UNIT_ID ": _preferences.getString("branchid"),
      "P_CREATED_BY ": _preferences.getString("username"),
      "P_CUST_NAME": fullname != null ? fullname : "",
      "P_DATE_BIRTH": formatDateDedup(birthDate) != null ? formatDateDedup(birthDate) : "",
      "P_FLAG_DETAIL": "0",
      "P_FLAG_SOURCE": "006",
      "P_ID_NO": identityNumber != null ? identityNumber : "",
      "P_MOTHER_NAME": motherName != null ? motherName : "",
      "P_PARA_CUST_TYPE_ID": flag,
      "P_PLACE_BIRTH": birthPlace != null ? birthPlace : "",
      "P_STATUS_AORO": "0",
      "P_OBLIGOR_ID": "NEW"
    });
    print("cek body submit newOid $_body");
    String _submitNewOID = await storage.read(key: "SubmitNewOID");
    try{
      final _response = await _http.post(
        "${BaseUrl.urlGeneral}$_submitNewOID",
        // "${urlPublic}api/save-draft/insert-dedup-customer",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 10));

      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        print("cek response newOid $_result");
        // print("cek result dedup $_result");
        String _status = _result['status'];
        if(_status != "1"){
          showSnackBar("${_result['message']}");
        }
        else{
          processSubmit = false;
          _statusAoro = "0";
          showSnackBarSuccess("Sukses create new OID");
          // _dbHelper.insertDataDedup(DataDedupModel(flag, identityNumber, fullname, birthDate.toString(), birthPlace, motherName, identityAddress, _statusAoro, DateTime.now().toString(), _preferences.getString('username'), DateTime.now().toString(), _preferences.getString('username')),_result['data']);
          _preferences.setString("order_no", _result['data'].toString());
          // _preferences.setString("status_aoro", _statusAoro);
          DateTime _timeStartValidate = DateTime.now();
          insertLog(context,_timeStartValidate,DateTime.now(),_body,"Insert NEW OID Dedup Individu","Insert NEW OID Dedup Individu");
          Timer(Duration(milliseconds: 1000), () {
            Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => Dashboard(flag: 2,)), (Route <dynamic> route) => false);
          });
        }
      } else{
        showSnackBar("Error submit new OID individu response ${_response.statusCode}");
      }
    } on TimeoutException catch(_){
      showSnackBar("Request Timeout APK");
    } catch(e){
      showSnackBar("Error $e");
    }
    processSubmit = false;
    notifyListeners();
  }

  void submitNewOidCompany(BuildContext context, String flag, String npwpNumber, String custName, DateTime establishedDate, String address) async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    processSubmit = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    // var _statusAoroCompany = "0";
    // if(_statusAoro == "0") {
    //   _statusAoroCompany = "0";
    // } else if(_statusAoro == "AO") {
    //   _statusAoroCompany = "1";
    // } else if(_statusAoro == "RO") {
    //   _statusAoroCompany = "2";
    // }

    var _body = jsonEncode({
      "P_ADDRESS": address != null ? address : "",
      "P_CP_UNIT_ID ": _preferences.getString("branchid"),
      "P_CREATED_BY ": _preferences.getString("username"),
      "P_CUST_NAME": custName != null ? custName : "",
      "P_DATE_BIRTH": formatDateDedup(establishedDate) != null ? formatDateDedup(establishedDate) : "",
      "P_FLAG_DETAIL": "0",
      "P_FLAG_SOURCE": "006",
      "P_ID_NO": npwpNumber != null ? npwpNumber : "",
      "P_MOTHER_NAME": "-",
      "P_PARA_CUST_TYPE_ID": flag,
      "P_PLACE_BIRTH": "-",
      "P_STATUS_AORO": "0",//_statusAoroCompany,
      "P_OBLIGOR_ID": this._listOIDCompanySelected != null ? this._listOIDCompanySelected.AC_CUST_ID : "NEW"
    });
    String _submitNewOID = await storage.read(key: "SubmitNewOID");
    try{
      final _response = await _http.post(
        "${BaseUrl.urlGeneral}$_submitNewOID",
        // "${urlPublic}api/save-draft/insert-dedup-customer",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 10));

      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        String _status = _result['status'];
        if(_status != "1"){
          showSnackBar("${_result['message']}");
        }
        else{
          _processSubmit = false;
          showSnackBarSuccess("Sukses create oid");
          // _dbHelper.insertDataDedup(DataDedupModel(flag,npwpNumber, custName, establishedDate.toString(), null, null, address, _statusAoro, DateTime.now().toString(), _preferences.getString('username'), DateTime.now().toString(), _preferences.getString('username')),_result['data']);
          _preferences.setString("order_no", _result['data']);
          // _preferences.setString("status_aoro", _statusAoroCompany);
          DateTime _timeStartValidate = DateTime.now();
          insertLog(context,_timeStartValidate,DateTime.now(),_body,"Insert NEW OID Dedup Company","Insert NEW OID Dedup Company");
          Timer(Duration(milliseconds: 1000), () {
            Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => Dashboard(flag: 2,)), (Route <dynamic> route) => false);
          });
        }
      }
      else{
        showSnackBar("Error submit new OID company response ${_response.statusCode}");
      }
    }
    on TimeoutException catch(_){
      showSnackBar("Request Timeout");
    }
    catch(e){
      showSnackBar("Error $e");
    }
    processSubmit = false;
    notifyListeners();
  }

  void submitJoinOidCompany(BuildContext context, String flag, ListOIDCompanyModel listOIDCompanySelected, String npwpNumber, String custName, DateTime establishedDate, String address) async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    // processSubmit = true;
    loadAoro = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var _statusAoroCompany = "0";
    if(_statusAoro == "NEW") {
      _statusAoroCompany = "0";
    } else if(_statusAoro == "AO") {
      _statusAoroCompany = "1";
    } else if(_statusAoro == "RO") {
      _statusAoroCompany = "2";
    }

    var _body = jsonEncode({
      "P_ADDRESS": address != null ? address : "",
      "P_CP_UNIT_ID ": _preferences.getString("branchid"),
      "P_CREATED_BY ": _preferences.getString("username"),
      "P_CUST_NAME": _listOIDCompanySelected.AC_CUST_NAME,// custName != null ? custName : "",
      "P_DATE_BIRTH": _listOIDCompanySelected.AC_ESTABILISHED_DATE.replaceAll("/", "-"),// formatDateDedup(establishedDate) != null ? formatDateDedup(establishedDate) : "",
      "P_FLAG_DETAIL": "0",
      "P_FLAG_SOURCE": "006",
      "P_ID_NO": _listOIDCompanySelected.AC_NPWP_NUMBER,
      "P_MOTHER_NAME": "-",
      "P_PARA_CUST_TYPE_ID": flag,
      "P_PLACE_BIRTH": "-",
      "P_STATUS_AORO": _statusAoroCompany,
      "P_OBLIGOR_ID": _listOIDCompanySelected.AC_CUST_ID
    });
    print("body: $_body");

    String _submitNewOID = await storage.read(key: "SubmitNewOID");
    try{
      final _response = await _http.post(
          "${BaseUrl.urlGeneral}$_submitNewOID",
          // "${urlPublic}api/save-draft/insert-dedup-customer",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 10));

      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        String _status = _result['status'];
        if(_status != "1"){
          showSnackBar("${_result['message']}");
        }
        else{
          // processSubmit = false;
          loadAoro = false;
          showSnackBarSuccess("Sukses create oid");
          // _dbHelper.insertDataDedup(DataDedupModel(flag,npwpNumber, custName, establishedDate.toString(), null, null, address, _statusAoro, DateTime.now().toString(), _preferences.getString('username'), DateTime.now().toString(), _preferences.getString('username')),_result['data']);
          _preferences.setString("order_no", _result['data']);
          // _preferences.setString("status_aoro", _statusAoroCompany);
          Timer(Duration(milliseconds: 1000), () {
            Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => Dashboard(flag: 2,)), (Route <dynamic> route) => false);
          });
        }
      }
      else{
        showSnackBar("Error get OID response ${_response.statusCode}");
      }
    }
    on TimeoutException catch(_){
      showSnackBar("Request Timeout");
    }
    catch(e){
      showSnackBar("Error $e");
    }
    // processSubmit = false;
    loadAoro = false;
    notifyListeners();
  }

  // void moreOption(context, index) {
  //   showModalBottomSheet(
  //       context: context,
  //       builder: (BuildContext bc) {
  //         return Theme(
  //           data: ThemeData(fontFamily: "NunitoSans"),
  //           child: Container(
  //             child: new Wrap(
  //               children: <Widget>[
  //                 FlatButton(
  //                     onPressed: () {
  //                       Navigator.pop(context);
  //                     },
  //                     child: Row(
  //                         mainAxisSize: MainAxisSize.max,
  //                         mainAxisAlignment: MainAxisAlignment.start,
  //                         children: <Widget>[
  //                           Icon(
  //                             Icons.close,
  //                             color: Colors.black,
  //                             size: 22.0,
  //                           ),
  //                         ])),
  //                 FlatButton(
  //                     onPressed: () {
  //                       // setState(() {
  //                       _selectedIndex = index;
  //                       // });
  //                       Navigator.pop(context);
  //                     },
  //                     child: Row(
  //                         mainAxisSize: MainAxisSize.max,
  //                         mainAxisAlignment: MainAxisAlignment.start,
  //                         children: <Widget>[
  //                           Icon(
  //                             Icons.location_on,
  //                             color: primaryOrange,
  //                             size: 22.0,
  //                           ),
  //                           SizedBox(
  //                             width: 12.0,
  //                           ),
  //                           Text(
  //                             "Pilih OID",
  //                             style: TextStyle(fontSize: 18.0),
  //                           )
  //                         ])),
  //                 // FlatButton(
  //                 //     onPressed: () {
  //                 //       setState(() {
  //                 //         _selectedIndex = index;
  //                 //       });
  //                 //     },
  //                 //     child: Row(
  //                 //         mainAxisSize: MainAxisSize.max,
  //                 //         mainAxisAlignment: MainAxisAlignment.start,
  //                 //         children: <Widget>[
  //                 //           Icon(
  //                 //             Icons.delete,
  //                 //             color: Colors.red,
  //                 //             size: 22.0,
  //                 //           ),
  //                 //           SizedBox(
  //                 //             width: 12.0,
  //                 //           ),
  //                 //           Text(
  //                 //             "Hapus",
  //                 //             style: TextStyle(fontSize: 18.0, color: Colors.red),
  //                 //           )
  //                 //         ])),
  //               ],
  //             ),
  //           ),
  //         );
  //       }
  //   );
  // }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating,
        backgroundColor: snackbarColor, duration: Duration(seconds: 2))
    );
  }

  void showSnackBarSuccess(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating,
        backgroundColor: Colors.green, duration: Duration(seconds: 2))
    );
  }

  void setValueFromDedup(BuildContext context, Map data){
    Provider.of<FormMInformasiKeluargaIBUChangeNotif>(context,listen: false).controllerNamaIdentitas.text = data['mother_name'];
    Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false).controllerNoIdentitas.text = data['identity_number'];
    Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false).controllerNamaLengkap.text = data['full_name'];
    Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false).controllerTempatLahirSesuaiIdentitas.text = data['birth_place'];
    Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false).controllerTglLahir.text = data['birth_date'];
    Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false).initialDateForTglLahir = data['initial_date_birth_date'];
  }

  void setValueCompanyFromDedup(BuildContext context, Map data){
    Provider.of<FormMCompanyRincianChangeNotifier>(context,listen: false).controllerInstitutionName.text = data['full_name'];
    Provider.of<FormMCompanyRincianChangeNotifier>(context,listen: false).controllerNPWP.text = data['identity_number'];
    Provider.of<FormMCompanyRincianChangeNotifier>(context,listen: false).initialDateForDateEstablishment = data['initial_date_birth_date'];
    Provider.of<FormMCompanyRincianChangeNotifier>(context,listen: false).controllerDateEstablishment.text = data['birth_date'];
  }

  void insertLog(BuildContext context,DateTime timeStart, DateTime timeEnd,String body,String response,String labelLogName) async{
    String _insertLog = await storage.read(key: "InsertLog");
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var diff = timeEnd.difference(timeStart).inMilliseconds;
    debugPrint(dateFormat3.format(DateTime.now()));


    // await http.post(
    //     "http://192.168.57.122/orca_api/public/login",
    //     body: {
    //       "duration": diff.toString(),
    //       "idx_step": "1",
    //       "json": body,
    //       "order_no": _preferences.getString("order_no"),
    //       "process_date": dateFormat3.format(DateTime.now()),
    //       "response": response,
    //       "user_id": _preferences.getString("username"),
    //       "log_name": labelLogName,
    //     },
    //     // headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    // );

    var _body = jsonEncode({
      "duration": diff.toString(),
      "process_date": dateFormatInsertLog.format(DateTime.now()),
      "idx_step": "1",
      "json": body,
      "order_no": _preferences.getString("order_no"),
      "response": response,
      "user_id": _preferences.getString("username"),
      "log_name": labelLogName,
    });

    // await http.post(
    //     "http://192.168.57.122/orca_api/public/login",
    //     body: _body,
    //     headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    // );

    debugPrint("$_body");
    debugPrint("${BaseUrl.urlGeneral}$_insertLog");

    try{
      var _response = await http.post(
          "${BaseUrl.urlGeneral}$_insertLog",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));

      if(_response.statusCode == 200){
        // print("insertApplNoDeleteTask1 ${_response.body}");
        var _result = jsonDecode(_response.body);
        debugPrint("$_result");
        if(_result['message'] != "SUCCESS"){
          debugPrint("${_result['message']}");
        }
        else{
          debugPrint("${_result['message']}");
        }
      }
      else{
        debugPrint("status code insert log ${_response.statusCode}");
      }
    }
    on TimeoutException catch(_){
      debugPrint("insert log timeout");
    }
    catch(e){
      debugPrint("insert log ${e.toString()}");
    }
  }
}