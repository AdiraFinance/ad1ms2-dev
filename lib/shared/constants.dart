import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/electricity_type_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/models/installment_type_model.dart';
import 'package:ad1ms2_dev/models/payment_method_model.dart';
import 'package:ad1ms2_dev/models/position_model.dart';
import 'package:ad1ms2_dev/models/product_lme_model.dart';
import 'package:ad1ms2_dev/models/recources_survey_info_model.dart';
import 'package:ad1ms2_dev/models/result_survey_model.dart';
import 'package:ad1ms2_dev/models/salesman_type_model.dart';
import 'package:ad1ms2_dev/models/type_of_financing_model.dart';
import 'package:ad1ms2_dev/models/type_offer_model.dart';
import 'package:ad1ms2_dev/screens/SA/sa_job_mayor.dart';
import 'package:ad1ms2_dev/screens/form_AOS/form_AOS.dart';
import 'package:ad1ms2_dev/screens/form_IA/form_IA.dart';
import 'package:ad1ms2_dev/screens/form_m/form_m_parent.dart';
import 'package:ad1ms2_dev/screens/survey/result_survey.dart';
import 'package:ad1ms2_dev/screens/form_m_company/form_m_company_parent.dart';
import 'package:ad1ms2_dev/shared/home/home_change_notifier.dart';
import 'package:ad1ms2_dev/shared/task_list_change_notifier/task_list_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_holo_date_picker/flutter_holo_date_picker.dart';
import 'package:intl/intl.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'form_m_individu_change_notifier/form_m_parent_individu_change_notifier.dart';

//parameter id = 1 for home 0 for general
String versionApp(int id){
  if(id == 1){
    return "v1008212";
  }
  else{
    return "Ad1MS2 DEV (v1008212)";
  }
}

const textInputDecoration = InputDecoration(
//  fillColor: Colors.white,
//  filled: true,
//  enabledBorder: OutlineInputBorder(
//    borderSide: BorderSide(color: Colors.white, width: 2.0),
//  ),
//  focusedBorder: OutlineInputBorder(
//    borderSide: BorderSide(color: Colors.blue, width: 2.0),
//  ),
    );

class TitlePopUpMenuButton {
  static const String Delete = "Delete";
  static const String Detail = "Detail";

  static const List<String> choices = <String>[Detail, Delete];
}

class TitlePopUpMenuButtonSRE {
  static const String Detail = "Detail";

  static const List<String> choices = <String>[Detail];
}

class LabelPopUpMenuButtonSaveToDraftBackToHome{
  static const String SAVE_TO_DRAFT = "Simpan ke draft";
  // static const String BACK_TO_HOME = "Back to home";

  static const List<String> choices = [SAVE_TO_DRAFT];
}

class PilihOIDMenuButton {
  static const String PilihOID = "Pilih OID";
  static const List<String> choices = <String>[PilihOID];
}

DateFormat dateFormat = DateFormat("dd-MM-yyyy");
DateFormat dateFormat2 = DateFormat("dd-MMM-yyyy");
DateFormat dateFormat3 = DateFormat("yyyy-MM-ddTHH:mm:ss");
DateTime dateNow = DateTime.now();
DateFormat dateFormatSurveyApp = DateFormat("yyyy-MM-dd");
DateFormat dateFormatSurveyAppWithTime = DateFormat("dd-MM-yyyy HH:mm");
DateFormat dateFormatInsertLog = DateFormat("yyyy-MM-dd HH:mm");
DateFormat time = DateFormat("HH:mm");

class RelationshipStatusList {
  List<RelationshipStatusModel> relationshipStatusItems = [
    RelationshipStatusModel("01", "SUAMI"),
    RelationshipStatusModel("02", "ISTRI"),
    RelationshipStatusModel("03", "ANAK KANDUNG"),
    RelationshipStatusModel("04", "AYAH KANDUNG"),
    RelationshipStatusModel("05", "IBU KANDUNG"),
    RelationshipStatusModel("89", "KAKAK KANDUNG"),
    RelationshipStatusModel("98", "LAINNYA"),
    RelationshipStatusModel("N8", "ADIK KANDUNG"),
  ];

  List<RelationshipStatusModel> relationshipStatusItemsPemegangSaham = [
    RelationshipStatusModel("01", "DIREKTUR UTAMA / PRES. DIR"),
    RelationshipStatusModel("02", "DIREKTUR"),
    RelationshipStatusModel("03", "KOMISARIS UTAMA / PRES. KOM"),
    RelationshipStatusModel("04", "KOMISARIS"),
    RelationshipStatusModel("05", "KUASA DIREKSI"),
    RelationshipStatusModel("06", "KETUA UMUM"),
    RelationshipStatusModel("07", "KETUA"),
    RelationshipStatusModel("08", "SEKRETARIS"),
    RelationshipStatusModel("09", "BENDAHARA"),
    RelationshipStatusModel("10", "LAINNYA"),
    RelationshipStatusModel("11", "PEMILIK"),
    RelationshipStatusModel("12", "WAKIL DIREKTUR"),
  ];
}

class RelationshipStatusModel {
  final String PARA_FAMILY_TYPE_ID;
  final String PARA_FAMILY_TYPE_NAME;

  RelationshipStatusModel(this.PARA_FAMILY_TYPE_ID, this.PARA_FAMILY_TYPE_NAME);
}

class TypeInstitutionList {
  List<TypeInstitutionModel> typeInstitutionItems = [
    TypeInstitutionModel("001", "PRIVATE"),
    TypeInstitutionModel("002", "PMA"),
    TypeInstitutionModel("003", "PMDN"),
    TypeInstitutionModel("004", "PMDA"),
    TypeInstitutionModel("005", "GOVERNMENT"),
    TypeInstitutionModel("006", "BUMN"),
    TypeInstitutionModel("007", "KOPERASI"),
    TypeInstitutionModel("008", "YAYASAN"),
    TypeInstitutionModel("009", "PT"),
    TypeInstitutionModel("010", "CV"),
    TypeInstitutionModel("011", "PT, TBK"),
    TypeInstitutionModel("013", "BUMD"),
    TypeInstitutionModel("014", "PDAM"),
    TypeInstitutionModel("015", "PD PASAR"),
    TypeInstitutionModel("016", "KOPERASI PRIMER"),
    TypeInstitutionModel("017", "PERUSAHAAN CAMPURAN"),
  ];
}

class TypeInstitutionModel {
  final String PARA_ID;
  final String PARA_NAME;

  TypeInstitutionModel(this.PARA_ID, this.PARA_NAME);
}

class ProductLMEList {
  List<ProductLMEModel> productLMEList = [
    ProductLMEModel("001", "MOTOR BARU"),
    ProductLMEModel("002", "MOTOR BEKAS"),
    ProductLMEModel("003", "MOBIL BARU"),
    ProductLMEModel("004", "MOBIL BEKAS"),
    ProductLMEModel("005", "DURABLE"),
    ProductLMEModel("006", "JASA PROPERTI"),
    ProductLMEModel("009", "MOTOR JASA"),
    ProductLMEModel("010", "MOBIL JASA"),
    ProductLMEModel("011", "UMROH MOTOR"),
    ProductLMEModel("012", "UMROH MOBIL"),
    ProductLMEModel("013", "UMROH PROPERTY"),
    ProductLMEModel("014", "JASA"),
    ProductLMEModel("015", "PAKET"),
    ProductLMEModel("016", "PROPERTY"),
  ];
}

class TypeOfFinancingList {
  List<FinancingTypeModel> financingTypeList = [
    FinancingTypeModel("1", "KONVENSIONAL"),
    FinancingTypeModel("2", "SYARIAH"),
  ];
}

class ResourceInfoSurveyList {
  List<ResourcesInfoSurveyModel> resourcesInfoSurveyList = [
    ResourcesInfoSurveyModel("001", "TETANGGA"),
    ResourcesInfoSurveyModel("002", "ATASAN KERJA"),
    ResourcesInfoSurveyModel("003", "HRD"),
    ResourcesInfoSurveyModel("004", "REKAN KERJA"),
    ResourcesInfoSurveyModel("005", "LAINNYA"),
  ];
}

class ElectricityType{
  List<ElectricityTypeModel> electricityTypeList = [
    ElectricityTypeModel("01", "450 WATT"),
    ElectricityTypeModel("02", "900 WATT"),
    ElectricityTypeModel("03", "1300 WATT"),
    ElectricityTypeModel("04", "2200 WATT"),
    ElectricityTypeModel("05", "3300 WATT"),
    ElectricityTypeModel("06", "4400 WATT"),
    ElectricityTypeModel("07", ">4400 WATT"),
  ];
}

class IdentityType{
  List<IdentityModel> lisIdentityModel = [
    IdentityModel("01", "KTP"),
    IdentityModel("03", "PASSPORT"),
    IdentityModel("04", "SIM"),
    IdentityModel("05", "KTP Sementara"),
    IdentityModel("06", "Resi KTP"),
    IdentityModel("07", "Ket. Domisili"),
  ];
}

class ListTypeOffer{
  List<TypeOfferModel> listTypeOffer = [
    TypeOfferModel("001", "VOUCHER"),
    TypeOfferModel("002", "CREDIT LIMIT"),
    TypeOfferModel("003", "NON VOUCHER")
  ];
}

class InstallmentType{
  List<InstallmentTypeModel> listInstallmentType =[
    InstallmentTypeModel("01", "ANNUITY"),
    InstallmentTypeModel("03", "DECLINE_N"),
    InstallmentTypeModel("04", "SEASONAL"),
    InstallmentTypeModel("05", "GRACE_PERIODE"),
    InstallmentTypeModel("06", "STEPPING"),
    InstallmentTypeModel("07", "IRREGULAR"),
    InstallmentTypeModel("08", "BALLOON_PAYMENT"),
    InstallmentTypeModel("09", "STRAIGHTLINE"),
    InstallmentTypeModel("10", "FLOATING"),
  ];

  List<InstallmentTypeModel> listInstallmentType2 =[
    InstallmentTypeModel("01", "ANNUITY"),
    InstallmentTypeModel("04", "SEASONAL"),
    InstallmentTypeModel("05", "GRACE_PERIODE"),
    InstallmentTypeModel("06", "STEPPING"),
    InstallmentTypeModel("07", "IRREGULAR"),
    InstallmentTypeModel("08", "BALLOON_PAYMENT"),
    InstallmentTypeModel("09", "STRAIGHTLINE"),
    InstallmentTypeModel("10", "FLOATING"),
  ];
}

class PaymentMethod{
  List<PaymentMethodModel> listPaymentMethod = [
    PaymentMethodModel("01", "ADVANCE"),
    PaymentMethodModel("02", "ARREAR"),
  ];
}

class BaseUrl{
  //tidak dipakai
  static String urlPublic = "https://ad1ms2newdev.adira.co.id/"; //"https://103.110.89.34/dev/public/ms2/";
  static String urlPublicAcction = "https://103.110.89.34/dev/public/acction/";
  // static String urlAcction = "http://10.81.3.21:9080/";
  static String urlLme = "http://10.161.16.207:31814/lmeServiceDEV/";
  static String urlDedup = "http://10.81.3.137:99/DedupAPI/api/";

  //dipakai (kondisional)
  static String urlGeneralPublic = "https://mobilegatewaytrn.adira.co.id/trn/public/"; //"https://mobilegatewaytrn.adira.co.id/trn/public/";
  static String urlAcctionPublic = "https://mobilegatewaytrn.adira.co.id/trn/public/"; //"https://ad1acctionuat.adira.co.id:9080/";
  static String urlLMEPublic= "https://mobilegatewaytrn.adira.co.id/trn/public/"; //"http://10.161.16.155:30540/lmeServiceUAT/";
  static String urlECMPublic = "https://mobilegatewaytrn.adira.co.id/trn/public/"; //"https://ad1ms2newdev.adira.co.id/proxy/ecm/";
  static String urlWMPPublic = "https://mobilegatewaytrn.adira.co.id/trn/public/"; //"https://mobilegatewaytrn.adira.co.id/trn/public/";

  static String urlGeneral = "https://mobilegatewaytrn.adira.co.id/trn/private/"; //"https://mobilegatewayuat.adira.co.id/uat/public/";
  static String urlAcction = "https://mobilegatewaytrn.adira.co.id/trn/private/"; //"https://ad1acctionuat.adira.co.id:9080/";
  static String urlLME= "https://mobilegatewaytrn.adira.co.id/trn/private/"; //"http://10.161.16.155:30540/lmeServiceUAT/";
  static String urlECM = "https://mobilegatewaytrn.adira.co.id/trn/private/"; //"https://ad1ms2newdev.adira.co.id/proxy/ecm/";
  static String urlWMP = "https://mobilegatewaytrn.adira.co.id/trn/private/"; //"https://mobilegatewayuat.adira.co.id/uat/public/";

  //dipakai (kondisional)
  //   static String urlGeneral = "https://mobilegatewayuat.adira.co.id/uat/public/";
  //   static String urlAcction = "https://ad1acctionuat.adira.co.id:9080/";
  //   static String urlLME= "http://10.161.16.155:30540/lmeServiceUAT/";
  //   static String urlECM = "https://ad1ms2newdev.adira.co.id/proxy/ecm/";
  //   static String urlWMP = "https://mobilegatewayuat.adira.co.id/uat/public/";

  //old version
  // static String urlGeneral = "https://103.110.89.34/dev/public/"; //"https://mobilegatewayuat.adira.co.id/uat/public/";
  // static String urlAcction = "http://10.81.3.21:9080/"; //"https://ad1acctionuat.adira.co.id:9080/";
  // static String urlLME= "http://10.161.16.207:31814/lmeServiceDEV/"; //"http://10.161.16.155:30540/lmeServiceUAT/";
  // static String urlECM = "http://10.50.3.123:8080/"; //"https://ad1ms2newdev.adira.co.id/proxy/ecm/";
  // static String urlWMP = "http://10.81.3.21:9080/"; //"https://mobilegatewayuat.adira.co.id/uat/public/";


  //tidak kepake
  // static String urlDedup ="http://10.81.3.137:99/";
  // static String urlAddress = "http://10.61.27.14:9016/";
  // static String urlOccupation = "http://10.61.27.14:9016/";
  // static String unit = "http://10.61.27.14:9093/";

}


String formatDateDedup(DateTime initialDate){
  DateFormat _formatDateDedup = DateFormat("dd-MMM-yyyy");
  return _formatDateDedup.format(initialDate);
}

String formatTime(DateTime initialDate){
  DateFormat _formatDateDedup = DateFormat("HH:mm:ss");
  return _formatDateDedup.format(initialDate);
}

String formatDateListOID(DateTime initialDate){
  DateFormat _formatDateDedup = DateFormat("dd-MM-yyyy");
  return _formatDateDedup.format(initialDate);
}

String formatDateValidateAndSubmit(DateTime initialDate){
  DateFormat _formatDateDedup = DateFormat("dd-MM-yyyy HH:mm:ss");
  return _formatDateDedup.format(initialDate);
}

String formatDateAndTime(DateTime initialDate){
  DateFormat _formatDateDedup = DateFormat("dd-MM-yyyy HH:mm");
  return _formatDateDedup.format(initialDate);
}

String formatDateInfoApp(DateTime initialDate){
  DateFormat _formatDateDedup = DateFormat("yyyy-MM-dd");
  return _formatDateDedup.format(initialDate);
}

Future<DateTime> selectDate(BuildContext context, DateTime initialDate) async{
  var datePicked = await DatePicker.showSimpleDatePicker(context,
    initialDate: initialDate,
    firstDate: DateTime(DateTime.now().year-100),
    lastDate: DateTime(DateTime.now().year+100),
    dateFormat: "dd-MMMM-yyyy",
    locale: DateTimePickerLocale.en_us,
    looping: true,
  );
  return datePicked;
}

Future<DateTime> selectDateApplication(BuildContext context, DateTime initialDate) async{
  var datePicked = await DatePicker.showSimpleDatePicker(context,
    initialDate: initialDate,
    firstDate: DateTime(initialDate.year, initialDate.month-1, initialDate.day),
    lastDate: initialDate,
    dateFormat: "dd-MMMM-yyyy",
    locale: DateTimePickerLocale.en_us,
    looping: false,
  );
  return datePicked;
}

Future<DateTime> selectDateFirstToday(BuildContext context, DateTime startDate, DateTime initialDate) async{
  var datePicked = await DatePicker.showSimpleDatePicker(context,
    initialDate: initialDate,
    firstDate: startDate,
    lastDate: DateTime(DateTime.now().year+100),
    dateFormat: "dd-MMMM-yyyy",
    locale: DateTimePickerLocale.en_us,
    looping: false,
  );
  return datePicked;
}

Future<DateTime> selectDateLastToday(BuildContext context, DateTime initialDate) async{
  var datePicked = await DatePicker.showSimpleDatePicker(context,
    initialDate: initialDate,
    firstDate: DateTime(DateTime.now().year-100),
    lastDate: DateTime(DateTime.now().year,DateTime.now().month,DateTime.now().day),
    dateFormat: "dd-MMMM-yyyy",
    locale: DateTimePickerLocale.en_us,
    looping: false,
  );
  return datePicked;
}

Future<DateTime> selectDateLast10Year(BuildContext context, DateTime initialDate) async{
  var datePicked = await DatePicker.showSimpleDatePicker(context,
    initialDate: initialDate,
    firstDate: DateTime(DateTime.now().year-100,12,1),
    lastDate: DateTime(DateTime.now().year-10,12,31),
    dateFormat: "dd-MMMM-yyyy",
    locale: DateTimePickerLocale.en_us,
    looping: false,
  );
  return datePicked;
}

Future<void> checkGetCustType(String typeFormId, BuildContext context, String orderNo, String orderDate, String custName,
    String custType, String IDX, String oid, String status_aoro, String jenisPenawaran, String noReferensi, String opsiMultidisburse, bool isFromHome) async{
  SharedPreferences _preferences = await SharedPreferences.getInstance();
  _preferences.setString("order_no", orderNo);
  _preferences.setString("order_date",orderDate);
  _preferences.setString("cust_name",custName);
  _preferences.setString("last_known_state",typeFormId);
  _preferences.setString("cust_type",custType);
  _preferences.setString("oid",oid);
  _preferences.setString("status_aoro",status_aoro);
  _preferences.setString("jenis_penawaran", jenisPenawaran);
  _preferences.setString("lme_id", noReferensi);
  _preferences.setString("opsi_multidisburse", opsiMultidisburse);
  // debugPrint("CONSTANT ${_preferences.getString("last_known_state")}");

  DbHelper _dbHelper = DbHelper();
  try{
    // kondi dibawah akan dihapus dan hanya memanggil getDataCustInfo, dan menambahkan fungsi set data dr server ke sqlite bagian company, setelah getDataCustInfo
    if(custType == "PER"){
      List _data = await _dbHelper.selectMS2PhotoHeader();
      // if(_data.isEmpty){
      //   print('Hei');
      await Provider.of<TaskListChangeNotifier>(context,listen: false).getDataCustInfo(typeFormId,context,orderNo,orderDate,custName,custType,IDX,isFromHome);
      // }
    }
    else{
      List _data = await _dbHelper.selectMS2CustomerCompany();
      await Provider.of<TaskListChangeNotifier>(context,listen: false).getDataCustInfo(typeFormId,context,orderNo,orderDate,custName,custType,IDX,isFromHome);
      // if(_data.isEmpty){
      //   await Provider.of<TaskListChangeNotifier>(context,listen: false).getDataCustInfo(typeFormId,context,orderNo,orderDate,custName,custType,IDX,isFromHome);
      // } else {
      //   navigateForm(typeFormId, context,orderNo,orderDate,custName, custType,isFromHome);
      // }
    }
  }
  catch(e){
    throw "${e.toString()}";
  }
}

void navigateForm(String typeFormId, BuildContext context,String orderNo,String orderDate,String custName, String custType,bool isFromHome){
  if(typeFormId == "SA"){
    Navigator.push(context, MaterialPageRoute(builder: (context) => SAJobMayor()));
    Provider.of<TaskListChangeNotifier>(context, listen: false).clearSearch();
  }
  else if(typeFormId == "AOS"){
    // Navigator.push(context, MaterialPageRoute(builder: (context) => FormAOS()));
    // Provider.of<TaskListChangeNotifier>(context, listen: false).clearSearch();
    // AOS
    if(custType == "PER"){
      Navigator.push(context, MaterialPageRoute(builder: (context) => FormMParent(typeFormId: typeFormId, flag: custType,))).then((value) => isFromHome
          ? Provider.of<HomeChangeNotifier>(context, listen: false).getTaskList() : Provider.of<TaskListChangeNotifier>(context, listen: false).getTaskList());
      Provider.of<TaskListChangeNotifier>(context, listen: false).clearSearch();
    }
    else {
      Navigator.push(context, MaterialPageRoute(builder: (context) => FormMCompanyParent(typeFormId: typeFormId, flag: custType,))).then((value) => isFromHome
          ? Provider.of<HomeChangeNotifier>(context, listen: false).getTaskList() : Provider.of<TaskListChangeNotifier>(context, listen: false).getTaskList());
      Provider.of<TaskListChangeNotifier>(context, listen: false).clearSearch();
    }
  }
  else if(typeFormId == "CONA"){
    // CONA atau CONAPPR atau CONAPP
    if(custType == "PER"){
      Navigator.push(context, MaterialPageRoute(builder: (context) => FormMParent(typeFormId: typeFormId, flag: custType,))).then((value) => isFromHome
          ? Provider.of<HomeChangeNotifier>(context, listen: false).getTaskList() : Provider.of<TaskListChangeNotifier>(context, listen: false).getTaskList());
      Provider.of<TaskListChangeNotifier>(context, listen: false).clearSearch();
    }
    else {
      Navigator.push(context, MaterialPageRoute(builder: (context) => FormMCompanyParent(typeFormId: typeFormId, flag: custType,))).then((value) => isFromHome
          ? Provider.of<HomeChangeNotifier>(context, listen: false).getTaskList() : Provider.of<TaskListChangeNotifier>(context, listen: false).getTaskList());
      Provider.of<TaskListChangeNotifier>(context, listen: false).clearSearch();
    }
  }
  else if(typeFormId == "IA"){
    Navigator.push(context, MaterialPageRoute(builder: (context) => FormIA()));
    Provider.of<TaskListChangeNotifier>(context, listen: false).clearSearch();
  }
  else if(typeFormId == "SRE"){
    // REGULER SURVEY
    if(custType == "PER"){
      // Navigator.push(context, MaterialPageRoute(builder: (context) => FormMParent(typeFormId: typeFormId, flag: custType,)));
      Navigator.push(context, MaterialPageRoute(builder: (context) => FormMParent(typeFormId: typeFormId, flag: custType,))).then((value) => isFromHome
          ? Provider.of<HomeChangeNotifier>(context, listen: false).getTaskList() : Provider.of<TaskListChangeNotifier>(context, listen: false).getTaskList());
      Provider.of<TaskListChangeNotifier>(context, listen: false).clearSearch();
    }
    else {
      // Navigator.push(context, MaterialPageRoute(builder: (context) => FormMCompanyParent(typeFormId: typeFormId, flag: custType,)));
      Navigator.push(context, MaterialPageRoute(builder: (context) => FormMCompanyParent(typeFormId: typeFormId, flag: custType,))).then((value) => isFromHome
          ? Provider.of<HomeChangeNotifier>(context, listen: false).getTaskList() : Provider.of<TaskListChangeNotifier>(context, listen: false).getTaskList());
      Provider.of<TaskListChangeNotifier>(context, listen: false).clearSearch();
    }
  }
  else if(typeFormId == "SARS"){
    // SA RESURVEY
    if(custType == "PER"){
      Navigator.push(context, MaterialPageRoute(builder: (context) => FormMParent(typeFormId: typeFormId, flag: custType,)));
      Provider.of<TaskListChangeNotifier>(context, listen: false).clearSearch();
    }
    else {
      Navigator.push(context, MaterialPageRoute(builder: (context) => FormMCompanyParent(typeFormId: typeFormId, flag: custType,)));
      Provider.of<TaskListChangeNotifier>(context, listen: false).clearSearch();
    }
  }
  else if(typeFormId == "RSVY"){
    // RESURVEY
    if(custType == "PER"){
      Navigator.push(context, MaterialPageRoute(builder: (context) => FormMParent(typeFormId: typeFormId, flag: custType,)));
      Provider.of<TaskListChangeNotifier>(context, listen: false).clearSearch();
    }
    else {
      Navigator.push(context, MaterialPageRoute(builder: (context) => FormMCompanyParent(typeFormId: typeFormId, flag: custType,)));
      Provider.of<TaskListChangeNotifier>(context, listen: false).clearSearch();
    }
  }
  else if(typeFormId == "PAC"){
    // PAC
    if(custType == "PER"){
      Navigator.push(context, MaterialPageRoute(builder: (context) => FormMParent(typeFormId: typeFormId, flag: custType,)));
      Provider.of<TaskListChangeNotifier>(context, listen: false).clearSearch();
    }
    else {
      Navigator.push(context, MaterialPageRoute(builder: (context) => FormMCompanyParent(typeFormId: typeFormId, flag: custType,)));
      Provider.of<TaskListChangeNotifier>(context, listen: false).clearSearch();
    }
  }
  else if(typeFormId == "DKR"){
    // DAKOR
    if(custType == "PER"){
      Navigator.push(context, MaterialPageRoute(builder: (context) => FormMParent(typeFormId: typeFormId, flag: custType,)));
      Provider.of<TaskListChangeNotifier>(context, listen: false).clearSearch();
    }
    else {
      Navigator.push(context, MaterialPageRoute(builder: (context) => FormMCompanyParent(typeFormId: typeFormId, flag: custType,)));
      Provider.of<TaskListChangeNotifier>(context, listen: false).clearSearch();
    }
  }
  else{
    // IDE
    if(custType == "PER"){
      Navigator.push(context, MaterialPageRoute(builder: (context) => FormMParent(typeFormId: typeFormId, flag: custType,))).then((value) => isFromHome
          ? Provider.of<HomeChangeNotifier>(context, listen: false).getTaskList() : Provider.of<TaskListChangeNotifier>(context, listen: false).getTaskList());
      Provider.of<TaskListChangeNotifier>(context, listen: false).clearSearch();
    }
    else {
      Navigator.push(context, MaterialPageRoute(builder: (context) => FormMCompanyParent(typeFormId: typeFormId, flag: custType,))).then((value) => isFromHome
          ? Provider.of<HomeChangeNotifier>(context, listen: false).getTaskList() : Provider.of<TaskListChangeNotifier>(context, listen: false).getTaskList());
      Provider.of<TaskListChangeNotifier>(context, listen: false).clearSearch();
    }
  }
}

class Sales{
  List<SalesmanTypeModel> salesmanTypeList = [
    SalesmanTypeModel("001", "SALES INTERNAL"),
    SalesmanTypeModel("002", "SALES EXTERNAL"),
    SalesmanTypeModel("003", "SALES REFFERAL"),
  ];

  List<PositionModel> positionList = [
    PositionModel("RAM", "REMEDIAL OFFICER"),
    PositionModel("CRH", "CUSTOMER RELATION HEAD"),
    PositionModel("MDR", "MEDIATOR"),
    PositionModel("CMO", "CREDIT MARKETING OFFICER"),
    PositionModel("COL", "COLLECTOR"),
    PositionModel("MNG", "MANAGER"),
    PositionModel("CFO", "CREDIT FIELD OFFICER"),
    PositionModel("KOS", "KOORDINATOR SUPERVISOR"),
    PositionModel("KBM", "KOORDINATOR BRANCH MANAGER"),
    PositionModel("KPT", "KEPALA TOKO"),
    PositionModel("OTH", "ADM DEALER"),
    PositionModel("MKO", "MARKETING OFFICER"),
    PositionModel("SPG", "SALES PROMOTION GIRL"),
    PositionModel("SAD", "SALES AGENT DURABLE"),
    PositionModel("ISC", "INTERNAL SALES COUNTER"),
    PositionModel("BMT", "KOPERASI SYARIAH"),
    PositionModel("KOR", "KOORDINATOR"),
    PositionModel("SPV", "SUPERVISOR"),
    PositionModel("TCO", "TRAFFIC COORDINATOR"),
    PositionModel("CMH", "CREDIT MARKETING HEAD"),
    PositionModel("SLS", "SALES DEALER"),
    PositionModel("BMG", "DEALER BRANCH MANAGER"),
    PositionModel("KOA", "KOORDINATOR AREA")
  ];

  List<PositionModel> positionExternalList = [
    PositionModel("PBO", "PERSONAL BANKING OFFICER"),
    PositionModel("FAO", "FLEET ACCOUNT OFFICER"),
    PositionModel("OWN", "OWNER"),
    PositionModel("OFF", "OFFICE"),
    PositionModel("DIR", "DIREKTUR"),
    PositionModel("SCS", "MARKETING DEALER"),
    PositionModel("EXA", "EXECUTOR AREA"),
    PositionModel("MDR", "MEDIATOR"),
    PositionModel("MNG", "MANAGER"),
    PositionModel("KOS", "KOORDINATOR SUPERVISOR"),
    PositionModel("KBM", "KOORDINATOR BRANCH MANAGER"),
    PositionModel("KPT", "KEPALA TOKO"),
    PositionModel("OTH", "ADM DEALER"),
    PositionModel("SPG", "SALES PROMOTION GIRL"),
    PositionModel("SAG", "SALES AGENT"),
    PositionModel("NDR", "MEDIATOR"),
    PositionModel("SMG", "SALES MANAGER"),
    PositionModel("EXC", "EXECUTOR"),
    PositionModel("AGM", "AGENCY MANAGER"),
    PositionModel("SAD", "SALES AGENT DURABLE"),
    PositionModel("LAW", "LAWYER"),
    PositionModel("MBE", "MITRA BACK END"),
    PositionModel("POL", "PELAPORAN"),
    PositionModel("AMB", "ADIRA MITRA BACK END"),
    PositionModel("HON", "HONORER MKA DURABLE"),
    PositionModel("BMT", "KOPERASI SYARIAH"),
    PositionModel("MKA", "MITRA KERJA ADIRA FRESH FLOW"),
    PositionModel("KOR", "KOORDINATOR"),
    PositionModel("MGR", "MANAGER"),
    PositionModel("SPV", "SUPERVISOR"),
    PositionModel("SLS", "SALES DEALER"),
    PositionModel("BDI", "JOIN SALES (BDI)"),
    PositionModel("BMG", "DEALER BRANCH MANAGER"),
    PositionModel("KOA", "KOORDINATOR AREA"),
    PositionModel("AMM", "AREA MARKETING MANAGER"),
    PositionModel("DCE", "DESK COLL EXTERNAL"),
    PositionModel("OPI", "P2D KOLEKTOR"),
    PositionModel("KML", "KOMISI LANGSUNG"),
    PositionModel("CSL", "COUNTER SALES DEALER"),
    PositionModel("DSL", "DURABLE SALES LEADER"),
    PositionModel("GMG", "GENERAL MANAGER"),
    PositionModel("CHN", "CHANNEL"),
  ];

  List<PositionModel> positionInternalList = [
    PositionModel("ASO", "AREA SALES OFFICER"),
    PositionModel("AAS", "AREA ADMIN SALES"),
    PositionModel("D49", "NON DEALER SALES DIV"),
    PositionModel("D19", "ACCOUNTING DIV"),
    PositionModel("D03", "INFORMATION TECHNOLOGY DIV"),
    PositionModel("D12", "MARKETING DEVELOPMENT DIV"),
    PositionModel("D48", "PROCESS IMPROVEMENT SUB DIV"),
    PositionModel("D08", "SUZUKI MARKETING DIV"),
    PositionModel("D09", "USED MOTORCYCLE MARKETING DIV"),
    PositionModel("D10", "YAMAHA MARKETING DIV"),
    PositionModel("CRS", "CUSTOMER RELATION OFFICER"),
    PositionModel("NDH", "NON DEALER SALES HEAD"),
    PositionModel("DLC", "DEALER CARE"),
    PositionModel("AWA", "AREA WAREHOUSE ADMIN"),
    PositionModel("CRO", "CREDIT HO"),
    PositionModel("AOM", "AREA OPERATION MANAGER"),
    PositionModel("RAM", "REMEDIAL OFFICER"),
    PositionModel("CRH", "CUSTOMER RELATION HEAD"),
    PositionModel("ACM", "AREA CREDIT MANAGER"),
    PositionModel("DSK", "DESK COLL"),
    PositionModel("CMO", "CREDIT MARKETING OFFICER"),
    PositionModel("COL", "COLLECTOR"),
    PositionModel("KCB", "KEPALA CABANG"),
    PositionModel("ADH", "ADMINISTRATION HEAD"),
    PositionModel("CLA", "BPKB STAFF"),
    PositionModel("CAD", "CREDIT ADMIN"),
    PositionModel("KRP", "KA KAREP"),
    PositionModel("RHD", "RECOVERY HEAD"),
    PositionModel("KDC", "HEAD OF COLLECTION"),
    PositionModel("KWL", "KEPALA WILAYAH"),
    PositionModel("CFO", "CREDIT FIELD OFFICER"),
    PositionModel("MKO", "MARKETING OFFICER"),
    PositionModel("CCO", "C TO C OFFICER"),
    PositionModel("AOF", "AO-FLEET"),
    PositionModel("FCC", "FLEET COMMITE CREDIT"),
    PositionModel("FMH", "FLEET MRKT SUPR SECTION HEAD"),
    PositionModel("FMM", "FLEET MARKETING MANAGER"),
    PositionModel("FOH", "FLEET OPERATION SEC HEAD"),
    PositionModel("F17", "FLEET UNDRWRITER ASS MNGR"),
    PositionModel("F15", "FLEET UNDERWRITER MANAGER"),
    PositionModel("AHA", "ADMIN MARKETING HO AREA"),
    PositionModel("FCM", "FLEET CRM SH-ASS MGR"),
    PositionModel("CLC", "COLLATERAL CAR"),
    PositionModel("SPO", "SALES PAYMENT OFFICER"),
    PositionModel("CAS", "CREDIT ADMIN STAFF"),
    PositionModel("CSF", "CUSTOMER SERVICE STAFF"),
    PositionModel("HCC", "HEAD OF CREDIT&COLLECTION-DD"),
    PositionModel("MSR", "MESSENGER"),
    PositionModel("HRO", "HEAD OF REGIONAL OPERATIONS"),
    PositionModel("RMR", "REMEDIAL MANAGER"),
    PositionModel("FSD", "FIELD SURVEYOR DURABLE"),
    PositionModel("CDV", "CREDIT DEVELOPMENT"),
    PositionModel("TSO", "TELESURVEY OFFICER"),
    PositionModel("D54", "SYARIAH DIVISION"),
    PositionModel("D60", "PARAMETER & RULE OPERATION"),
    PositionModel("PAO", "PROBLEM ACCOUNT OFFICER"),
    PositionModel("GAM", "GENERAL ADMIN STAFF"),
    PositionModel("HCD", "HEAD OF CREDIT DEVELOPMENT"),
    PositionModel("PPA", "PROCEDURE POLICY ASSOCIATE"),
    PositionModel("ISC", "INTERNAL SALES COUNTER"),
    PositionModel("MGC", "MARKETING MANAGER GROUP CUSTOM"),
    PositionModel("OTL", "OUTSOURCE TELLER"),
    PositionModel("CCH", "CLUSTER COLLECTION HEAD"),
    PositionModel("D58", "CUSTOMER RETENTION MANAGEMENT"),
    PositionModel("D59", "COMMUNITY DEVELOPMENT SUPPORT"),
    PositionModel("D61", "DIGITAL MARKETING"),
    PositionModel("RCS", "HEAD OF REGIONAL COMMERCIAL SA"),
    PositionModel("CRR", "CREDIT REVIEWER"),
    PositionModel("MDH", "CREDIT OPERATIONAL MCY DEPT HE"),
    PositionModel("OPH", "OPERATION HEAD"),
    PositionModel("D55", "OPERATIONAL RISK MANAGEMENT"),
    PositionModel("HCO", "HEAD OF COLLECTION OPERATION"),
    PositionModel("SHC", "SALES HEAD COMMERCIAL"),
    PositionModel("D56", "MARKETING PROCESS IMPROVEMENT"),
    PositionModel("ANM", "ADMIN MARKETING"),
    PositionModel("PGA", "GENERAL AFFAIR"),
    PositionModel("TCO", "TRAFFIC COORDINATOR"),
    PositionModel("SVH", "SURVEYOR HEAD"),
    PositionModel("ARM", "A/R MANAGER"),
    PositionModel("RCP", "RECEPTIONIST"),
    PositionModel("CMH", "CREDIT MARKETING HEAD"),
    PositionModel("TLR", "TELLER"),
    PositionModel("FIN", "FINANCE"),
    PositionModel("HGH", "HRD/GA HEAD"),
    PositionModel("MCA", "CREDIT ANALYST"),
    PositionModel("ARH", "AR HEAD"),
    PositionModel("CSA", "CREDIT ANALYST ADMIN"),
    PositionModel("ARA", "A/R ADMIN AREA"),
    PositionModel("CSB", "COUNTER SALES BRANCH"),
    PositionModel("ADO", "ADMIN OUTLET"),
    PositionModel("KDO", "KADIV OPERATION"),
    PositionModel("QAA", "QA AREA"),
    PositionModel("BOS", "BUSINESS OPERATION SUPPORT"),
    PositionModel("FCS", "FLEET CUSTODIAN STAFF"),
    PositionModel("FDH", "FLEET OPERATION DEPT HEAD"),
    PositionModel("FDP", "FLEET DOCUMENT PROCESSOR STAFF"),
    PositionModel("FFH", "FLEET FIN&ACCT SERV SEC HEAD"),
    PositionModel("FOD", "FLEET OPERT DEPUTY DEP HD"),
    PositionModel("FUA", "FLEET UNDERWRITER ASS MNGR"),
    PositionModel("D51", "PORTFOLIO MANAGEMENT DIVISION"),
    PositionModel("FRH", "FLEET AR SECT HEAD"),
    PositionModel("CFD", "CREDIT FLEET DEPT"),
    PositionModel("BPC", "PAYMENT PROCESSOR"),
    PositionModel("SME", "SMALL MEDIUM ENTERPRISE"),
    PositionModel("FAM", "FLEET ADMIN MARKETING"),
    PositionModel("FOS", "FLEET OPERATION SUPPORT ADMIN"),
    PositionModel("FUM", "FLEET UNDERWRITER MANAGER"),
    PositionModel("D50", "MARKETING SUPPORT DEPT"),
    PositionModel("FAS", "FLEET AR STAFF"),
    PositionModel("ALF", "ADIRA LEADERSHIP DEV"),
    PositionModel("CAC", "COLLECTION ADMIN"),
    PositionModel("FIS", "FLEET INSURANCE STAFF"),
    PositionModel("F25", "PRESIDENT DIRECTOR"),
    PositionModel("FDS", "FLEET DEVELOPMENT SECT HEAD"),
    PositionModel("F09", "FLEET MARKETING MANAGER"),
    PositionModel("F21", "CREDIT DIVISION HEAD"),
    PositionModel("F23", "RISK DIRECTOR"),
    PositionModel("AMA", "ADMIN MARKETING AREA"),
    PositionModel("DEC", "DATA ENTRY CAR"),
    PositionModel("AFO", "ANTI FRAUD & ORM"),
    PositionModel("FCA", "FLEET CREDIT ADMIN"),
    PositionModel("FFS", "FLEET FINANCE STAFF"),
    PositionModel("FRA", "FLEET REMOTE ADMIN"),
    PositionModel("F16", "FLEET CREDIT DEPT HEAD"),
    PositionModel("FUS", "FLEET UNDERWRITER SEC STAF"),
    PositionModel("PPR", "PAYMENT PROCESSOR RO"),
    PositionModel("AAR", "ADMIN ALL RO"),
    PositionModel("CDH", "CREDIT OPERATIONAL CAR DEPT HE"),
    PositionModel("D52", "MARKETING WHEELERS"),
    PositionModel("SMM", "SALES MARKETING MANAGER"),
    PositionModel("FAE", "FLEET ACCOUNT EXECUTIVE"),
    PositionModel("F22", "FLEET MARKETING DIVISION HEAD"),
    PositionModel("FMS", "FLEET MRKT SUPR DEPT HEAD"),
    PositionModel("F24", "MARKETING DIRECTOR"),
    PositionModel("D53", "CAR MARKETING DIVISION"),
    PositionModel("FAQ", "FLEET ACCOUNT QUALITY OFFICER"),
    PositionModel("RAA", "REMEDIAL ADMIN AREA"),
    PositionModel("ROA", "RECOVERY OFFICER AREA"),
    PositionModel("DBM", "DEPUTY BRANCH MANAGER"),
    PositionModel("MMA", "MARKETING MANAGER AREA"),
    PositionModel("RCM", "AREA RECOVERY MANAGER"),
    PositionModel("MK1", "ARO DURABLE"),
    PositionModel("MK2", "REMOFF DURABLE"),
    PositionModel("D62", "CROSS SELL PLAN & PRODUCT DEV"),
    PositionModel("D63", "SSD MANAGEMENT"),
    PositionModel("RCH", "REGIONAL COLLECTION HEAD"),
    PositionModel("RAC", "REGIONAL CREDIT ADMIN"),
    PositionModel("RCU", "REGIONAL CREDIT SUPPORT"),
    PositionModel("HOC", "HEAD OF CREDIT"),
    PositionModel("CRL", "CHIEF RISK & LEGAL OFFICER"),
    PositionModel("PDR", "PRESIDENT DIR-CEO"),
    PositionModel("PPS", "PAYMENT PROCESSOR STAFF"),
    PositionModel("D57", "BUSINESS DEVELOPMENT"),
    PositionModel("HFC", "HEAD OF FLEET COLLECTION"),
    PositionModel("AWS", "AREA WAREHOUSE STAFF"),
    PositionModel("AWO", "AREA WAREHOUSE OFFICER"),
    PositionModel("AWR", "AREA WAREHOUSE REP.STAFF"),
    PositionModel("RAH", "RECOVERY ASSET HEAD"),
    PositionModel("RSA", "AREA RECOVERY ASSET ADM"),
    PositionModel("D17", "CREDIT DIVISION"),
    PositionModel("D29", "AUDIT DIV"),
    PositionModel("D39", "BUDGET DIV"),
    PositionModel("D31", "COLLECTION DIV"),
    PositionModel("D20", "FINANCE DIV"),
    PositionModel("D06", "HONDA MARKETING DIV"),
    PositionModel("D45", "LEGAL & SPI DIV"),
    PositionModel("D47", "MULTI BRANDS MCY MKT DIV"),
    PositionModel("D07", "NEW & USED CAR MARKETING DIV"),
    PositionModel("D38", "OPERATION DEVELOPMENT DIV"),
    PositionModel("D44", "RECOVERY ASSET MANAGEMENT DIV"),
    PositionModel("D46", "RECOVERY DIV"),
    PositionModel("875", "FLEET MARKETING DIVISION"),
    PositionModel("888", "RISK MIS ANALYTICS & SCORING"),
    PositionModel("KSH", "KIOS HEAD"),
  ];
}

// class SetSSL{
//   var sccontext = SecurityContext.defaultContext;
//   void setSSLFromAsset() async{
//     // String data = await rootBundle.loadString("assets/ad1ms2newdev.pem");
//     try{
//       String data = await rootBundle.loadString("assets/ad1ms2newdev.pem");
//       print(data);
//
// //it can be "cert.crt" as well.
//       List bytes = utf8.encode(data);
//       sccontext.setTrustedCertificatesBytes(bytes);
//     }
//     catch(e){
//       print(e.toString());
//     }
//   }
// }
