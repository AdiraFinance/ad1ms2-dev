import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/ms2_appl_objt_wmp_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_wmp_model.dart';
import 'package:ad1ms2_dev/models/wmp_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_application_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/dashboard/dashboard_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InfoWMPChangeNotifier with ChangeNotifier {
  String _valueWMP;
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String get valueWMP => _valueWMP;
  List<WMPModel> _listWMP = [];
  DbHelper _dbHelper = DbHelper();
  var storage = FlutterSecureStorage();
  bool _flag = false;
  String _custType;
  String _lastKnownState;
  String _orderWmpID;
  bool _isDisablePACIAAOSCONA = false;
  bool _isCheckData = false;

  bool get isCheckData => _isCheckData;

  set isCheckData(bool value) {
    this._isCheckData = value;
    notifyListeners();
  }

  bool get isDisablePACIAAOSCONA => _isDisablePACIAAOSCONA;

  set isDisablePACIAAOSCONA(bool value) {
    this._isDisablePACIAAOSCONA = value;
    notifyListeners();
  }

  bool get flag => _flag;

  set flag(bool value) {
    this._flag = value;
  }

  set valueWMP(String value) {
    this._valueWMP = value;
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void getWMP(BuildContext context) async {
    List<WMPModel> _listWMPIDE = [];
    if(this._lastKnownState != "IDE"){
      for(int i=0; i<this._listWMP.length; i++){
        _listWMPIDE.add(this._listWMP[i]);
      }
    }
    this._listWMP.clear();
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    // var _infoUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    // var _infoCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false);
    // var _providerListOID = Provider.of<ListOidChangeNotifier>(context, listen: false);
    var _providerPhoto = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
    var _providerInfApplication = Provider.of<InfoAppChangeNotifier>(context, listen: false);
    var _providerInfObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
    var _providerInfCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false);

    var formatTanggal = DateFormat("ddMMyy");
    var date = formatTanggal.format(_providerInfApplication.initialDateOrder);

    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);
    print("prod matrix = ${_providerInfObjectUnit.prodMatrixId}");

    var _body = jsonEncode({
      "paraFinTypeId": _preferences.getString("cust_type") != "COM" ? "${_providerPhoto.typeOfFinancingModelSelected.financingTypeId}":"${_providerInfObjectUnit.typeOfFinancingModelSelected.financingTypeId}", // Fin Type, 1 - Konvensional, 2 - Syariah
      "paraObjectId": _providerInfObjectUnit.objectSelected != null ? _providerInfObjectUnit.objectSelected.id : "", // Kode Object
      "paraProdMatrixId": _providerInfObjectUnit.prodMatrixId != null ? _providerInfObjectUnit.prodMatrixId : "", // Kode Product Matrix
      "paraProdChannelId": _providerInfObjectUnit.thirdPartyTypeSelected != null ? _providerInfObjectUnit.thirdPartyTypeSelected.kode : "", // NEW: Field Jenis Pihak Ketiga | OLD: Kode Sumber Order
      "paraProdProgramId": _providerInfObjectUnit.productTypeSelected != null ? _providerInfObjectUnit.productTypeSelected.id : "", // Kode Jenis Produk
      "paraObjectBrandId": _providerInfObjectUnit.brandObjectSelected != null ? _providerInfObjectUnit.brandObjectSelected.id : "", // Kode Object Brand
      "date": date, // Tanggal Aplikasi format ddmmyy
      "aoroId": _preferences.getString("status_aoro") != null ? _preferences.getString("status_aoro") : "0", // Kode Status AORO
      "paraInstallmentTypeId": _providerInfCreditStructure.installmentTypeSelected != null ? _providerInfCreditStructure.installmentTypeSelected.id : "", // Kode Jenis Angsuran
      "paraObjectTypeId": _providerInfObjectUnit.objectTypeSelected != null ? _providerInfObjectUnit.objectTypeSelected.id : "", // Kode Tipe Object
      "paraObjectModelId": _providerInfObjectUnit.modelObjectSelected != null ? _providerInfObjectUnit.modelObjectSelected.id : "", // Kode Model Object
      "paraDealOutletId": _providerInfObjectUnit.thirdPartySelected != null ? _providerInfObjectUnit.thirdPartySelected.kode : "", // Kode Pihak Ketiga
      "tenor": _providerInfCreditStructure.periodOfTimeSelected != null ? _providerInfCreditStructure.periodOfTimeSelected : "", // Jangka Waktu
      "lendingRate": _providerInfCreditStructure.controllerInterestRateEffective.text != null ? _providerInfCreditStructure.controllerInterestRateEffective.text : "" // Value Eff Rate
    });
    print("body wmp = $_body");
    String _fieldWMP = await storage.read(key: "FieldWMP");
    print("${BaseUrl.urlWMP}$_fieldWMP");
    final _response = await _http.post(
        "${BaseUrl.urlWMP}$_fieldWMP",
        // "${urlAcction}adira-acction/acction/service/wmp/getProposalNo",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );

    if(_response.statusCode == 200 || _response.statusCode == 302){
      final _result = jsonDecode(_response.body);
      print("result wmp = $_result");
      List _data = _result['proposal'];
      if(_data.isEmpty){
        showSnackBar("WMP tidak ditemukan");
        loadData = false;
        check(context);
        // this._flag = false;
        // this._isCheckData = true;
      } else{
        for(int i=0; i <_data.length; i++){
          _listWMP.add(WMPModel(
              _data[i]['amount'].toString().replaceAll(".0", ""),
              _data[i]['deskripsi'],
              _data[i]['deskripsiProposal'],
              _data[i]['isHidden'],
              _data[i]['noProposal'],
              _data[i]['type'],
              _data[i]['typeSubsidi'],
              "NEW",
              false, false, false, false
          ));
        }
        if(_listWMPIDE.isNotEmpty){
          if(this._listWMP.length == _listWMPIDE.length) {
            print("kondisi 1");
            for(int i=0;i<this._listWMP.length; i++){
              if(this._listWMP[i].typeSubsidi == _listWMPIDE[i].typeSubsidi){
                this._listWMP[i] = WMPModel(
                    this._listWMP[i].amount,
                    this._listWMP[i].deskripsi,
                    this._listWMP[i].deskripsiProposal,
                    this._listWMP[i].isHidden,
                    this._listWMP[i].noProposal,
                    this._listWMP[i].type,
                    this._listWMP[i].typeSubsidi,
                    _listWMPIDE[i].orderWmpID,
                    false, false, false, false
                );
              }
            }
          }
          else if(this._listWMP.length > _listWMPIDE.length) {
            print("kondisi 2");
            var jumlahListBaru = 0;
            jumlahListBaru = this._listWMP.length - _listWMPIDE.length;
            // masukkan list dengan type subsidi yang ada
            for(int i=0; i<_listWMPIDE.length; i++) {
              if(this._listWMP[i].typeSubsidi == _listWMPIDE[i].typeSubsidi){
                this._listWMP[i] = WMPModel(
                    this._listWMP[i].amount,
                    this._listWMP[i].deskripsi,
                    this._listWMP[i].deskripsiProposal,
                    this._listWMP[i].isHidden,
                    this._listWMP[i].noProposal,
                    this._listWMP[i].type,
                    this._listWMP[i].typeSubsidi,
                    _listWMPIDE[i].orderWmpID,
                    false, false, false, false
                );
              }
            }
            // masukkan list dengan type subsidi yang baru
            for(int i=0; i<jumlahListBaru; i++) {
              this._listWMP[i] = WMPModel(
                  this._listWMP[i].amount,
                  this._listWMP[i].deskripsi,
                  this._listWMP[i].deskripsiProposal,
                  this._listWMP[i].isHidden,
                  this._listWMP[i].noProposal,
                  this._listWMP[i].type,
                  this._listWMP[i].typeSubsidi,
                  "NEW",
                  false, false, false, false
              );
            }
          }
          else if(this._listWMP.length < _listWMPIDE.length) {
            print("kondisi 3");
            for(int i=0; i<this._listWMP.length; i++) {
              for(int j=0; j<_listWMPIDE.length; j++) {
                if(this._listWMP[i].typeSubsidi == _listWMPIDE[j].typeSubsidi){
                  this._listWMP[j] = WMPModel(
                      this._listWMP[j].amount,
                      this._listWMP[j].deskripsi,
                      this._listWMP[j].deskripsiProposal,
                      this._listWMP[j].isHidden,
                      this._listWMP[j].noProposal,
                      this._listWMP[j].type,
                      this._listWMP[j].typeSubsidi,
                      _listWMPIDE[j].orderWmpID,
                      false, false, false, false
                  );
                }
              }
            }
          }

          if(this._listWMP.length > _listWMPIDE.length) {
            for(int h=0; h < this._listWMP.length; h++){
              for(int i=0; i<_listWMPIDE.length; i++) {
                if(this._listWMP[h].typeSubsidi == _listWMPIDE[i].typeSubsidi){
                  this._listWMP[h] = WMPModel(
                      this._listWMP[h].amount,
                      this._listWMP[h].deskripsi,
                      this._listWMP[h].deskripsiProposal,
                      this._listWMP[h].isHidden,
                      this._listWMP[h].noProposal,
                      this._listWMP[h].type,
                      this._listWMP[h].typeSubsidi,
                      _listWMPIDE[i].orderWmpID,
                      false, false, false, false
                  );
                }
                else{
                  this._listWMP[h] = WMPModel(
                      this._listWMP[h].amount,
                      this._listWMP[h].deskripsi,
                      this._listWMP[h].deskripsiProposal,
                      this._listWMP[h].isHidden,
                      this._listWMP[h].noProposal,
                      this._listWMP[h].type,
                      this._listWMP[h].typeSubsidi,
                      "NEW",
                      false, false, false, false
                  );
                }
              }
            }
          }
        }
        for(int x=0; x < _listWMP.length; x++) {
          print("WMP ID YANG DIPAKE: ${_listWMP[x].orderWmpID}");
          print("NO PROPOSAL YANG DIPAKE: ${_listWMP[x].noProposal}");
        }
        loadData = false;
        check(context);
        // this._flag = false;
        // this._isCheckData = true;
      }
    }
    else{
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    await checkDataDakor(context);
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  List<WMPModel> get listWMP => _listWMP;

  void saveToSQLite(){
    List<MS2ApplObjtWmpModel> _listData = [];
    for(int i=0; i<_listWMP.length; i++){
      _listData.add(MS2ApplObjtWmpModel(
        null,
        _listWMP[i].orderWmpID,
        _listWMP[i].noProposal,
        _listWMP[i].type,
        _listWMP[i].deskripsiProposal,
        _listWMP[i].typeSubsidi,
        _listWMP[i].deskripsi,
        double.parse(_listWMP[i].amount),
        null,
        null,
        null,
        null,
        null,
        _listWMP[i].isEditNoProposal ? "1" : "0",
        _listWMP[i].isEditType ? "1" : "0",
        _listWMP[i].isEditTypeSubsidi ? "1" : "0",
        _listWMP[i].isEditAmount ? "1" : "0"
        // _listWMP[i].orderWmpID,
        // _listWMP[i].noProposal,
        // _listWMP[i].type,
        // _listWMP[i].deskripsi,
        // _listWMP[i].typeSubsidi,
        // _listWMP[i].deskripsiProposal,
        // double.parse(_listWMP[i].amount),
        // null,
        // null,
        // null,
        // null,
        // null,
      ));
    }
    _dbHelper.insertMS2ApplObjtWmp(_listData);
  }

  Future<bool> deleteSQLite() async{
    return await _dbHelper.deleteMS2ApplOBJWmp();
  }

  void clearDataWMP(){
    this._flag = false;
    this._listWMP.clear();
    this._isCheckData = false;
  }

  void check(BuildContext context){
    var _providerProduct = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).productTypeSelected != null ? Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).productTypeSelected.id : "";
    if((_providerProduct == "0002F" || _providerProduct == "0002G") && this._listWMP.isEmpty){
      this._flag = true;
      this._isCheckData = false;
    }
    else{
      this._flag = false;
      this._isCheckData = true;
    }
    notifyListeners();
  }

  String get orderWmpID => _orderWmpID;

  set orderWmpID(String value) {
    this._orderWmpID = value;
  }

  Future<void> setDataFromSQLite(BuildContext context) async{
   var _check = await _dbHelper.selectMS2ApplObjtWmp();
   print("sqlite wmp $_check");
   if(_check.isNotEmpty){
     print("total wmp = ${_check.length}");
      for(int i=0; i<_check.length; i++){
        _listWMP.add(WMPModel(
            _check[i]['max_refund_amt'].toString(),
            _check[i]['wmp_subsidy_type_id_desc'].toString(), // _check[i]['wmp_type'].toString(),
            _check[i]['wmp_type_desc'].toString(),
            '',
            _check[i]['wmp_no'].toString(),
            _check[i]['wmp_type'].toString(), // _check[i]['wmp_subsidy_type_id'].toString(),
            _check[i]['wmp_subsidy_type_id'].toString(), // _check[i]['wmp_subsidy_type_id_desc'].toString(),
            _check[i]['orderWmpID'],
            _check[i]['edit_wmp_no'] == "1",
            _check[i]['edit_wmp_type'] == "1",
            _check[i]['edit_wmp_subsidy_type_id'] == "1",
            _check[i]['edit_max_refund_amt'] == "1",
        ));
      }
   }
   notifyListeners();
  }

  Future<void> checkDataDakor(BuildContext context) async{
    var _check = await _dbHelper.selectMS2ApplObjtWmp();
    print("dakor sqlite wmp $_check");
    if(_check.isNotEmpty && this._lastKnownState == "DKR"){
      if(_check.length == _listWMP.length){
        bool _isWmpNo = false;
        bool _isWmpType = false;
        bool _isWmpSubsidyType = false;
        bool _isRefundAmt = false;
        for(int i=0; i<_check.length; i++){
          _isWmpNo = _listWMP[i].noProposal != _check[i]['wmp_no'].toString();
          _isWmpType = _listWMP[i].type != _check[i]['wmp_type'].toString();
          _isWmpSubsidyType = _listWMP[i].typeSubsidi != _check[i]['wmp_subsidy_type_id'].toString();
          _isRefundAmt = _listWMP[i].amount != _check[i]['max_refund_amt'].toString();
          _listWMP[i].isEditNoProposal = _isWmpNo;
          _listWMP[i].isEditType = _isWmpType;
          _listWMP[i].isEditTypeSubsidi = _isWmpSubsidyType;
          _listWMP[i].isEditAmount = _isRefundAmt;
        }
      }
    }
    notifyListeners();
  }

  String get custType => _custType;

  set custType(String value) {
    this._custType = value;
    notifyListeners();
  }

  String get lastKnownState => _lastKnownState;

  set lastKnownState(String value) {
    this._lastKnownState = value;
    notifyListeners();
  }

  Future<void> setPreference(BuildContext context) async {
    getDataFromDashboard(context);
    SharedPreferences _preference = await SharedPreferences.getInstance();
    this._custType = _preference.getString("cust_type");
    this._lastKnownState = _preference.getString("last_known_state");
    if(this._lastKnownState == "AOS" || this._lastKnownState == "CONA" || this._lastKnownState == "IA" || this._lastKnownState == "PAC") {
      this._isDisablePACIAAOSCONA = true;
    }
    this._isCheckData = true;
  }

  ShowMandatoryInfoWMPModel _showMandatoryInfoWMPIdeModel;
  ShowMandatoryInfoWMPModel _showMandatoryInfoWMPIdeCompanyModel;
  void showMandatoryIdeModel(BuildContext context){
    _showMandatoryInfoWMPIdeModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoWMPIdeModel;
    _showMandatoryInfoWMPIdeCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoWMPIdeCompanyModel;
  }

  ShowMandatoryInfoWMPModel _showMandatoryInfoWMPRegulerSurveyModel;
  ShowMandatoryInfoWMPModel _showMandatoryInfoWMPRegulerSurveyCompanyModel;
  void showMandatoryRegulerSurveyModel(BuildContext context){
    _showMandatoryInfoWMPRegulerSurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoWMPRegulerSurveyModel;
    _showMandatoryInfoWMPRegulerSurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoWMPRegulerSurveyCompanyModel;
  }

  ShowMandatoryInfoWMPModel _showMandatoryInfoWMPPacModel;
  ShowMandatoryInfoWMPModel _showMandatoryInfoWMPPacCompanyModel;
  void showMandatoryPacModel(BuildContext context){
    _showMandatoryInfoWMPPacModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoWMPPacModel;
    _showMandatoryInfoWMPPacCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoWMPPacCompanyModel;
  }

  ShowMandatoryInfoWMPModel _showMandatoryInfoWMPResurveyModel;
  ShowMandatoryInfoWMPModel _showMandatoryInfoWMPResurveyCompanyModel;
  void showMandatoryResurveyModel(BuildContext context){
    _showMandatoryInfoWMPResurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoWMPResurveyModel;
    _showMandatoryInfoWMPResurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoWMPResurveyCompanyModel;
  }

  ShowMandatoryInfoWMPModel _showMandatoryInfoWMPDakorModel;
  ShowMandatoryInfoWMPModel _showMandatoryInfoWMPDakorCompanyModel;
  void showMandatoryDakorModel(BuildContext context){
    _showMandatoryInfoWMPDakorModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoWMPDakorModel;
    _showMandatoryInfoWMPDakorCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoWMPDakorCompanyModel;
  }

  void getDataFromDashboard(BuildContext context){
    showMandatoryIdeModel(context);
    showMandatoryRegulerSurveyModel(context);
    showMandatoryPacModel(context);
    showMandatoryResurveyModel(context);
    showMandatoryDakorModel(context);
  }

  bool isGetWMPVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoWMPIdeModel.isGetWMPVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoWMPRegulerSurveyModel.isGetWMPVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoWMPPacModel.isGetWMPVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoWMPResurveyModel.isGetWMPVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoWMPDakorModel.isGetWMPVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoWMPIdeCompanyModel.isGetWMPVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoWMPRegulerSurveyCompanyModel.isGetWMPVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoWMPPacCompanyModel.isGetWMPVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoWMPResurveyCompanyModel.isGetWMPVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoWMPDakorCompanyModel.isGetWMPVisible;
      }
    }
    return value;
  }
}