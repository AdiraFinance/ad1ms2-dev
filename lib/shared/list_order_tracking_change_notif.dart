import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/models/list_order_tracking_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';
import 'constants.dart';

class ListOrderTrackingChangeNotifier with ChangeNotifier {
  String _jobFlag;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  var storage = FlutterSecureStorage();

  bool _autoValidate = false;
  bool _flag = false;
  //int _selectedIndex = -1;
  bool _loadData = false;
  List<ListOrderTrackingModel> _listOrderTracking = [];
  //ListOrderTrackingModel _listOIdPersonalSelected;

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  // Auto Validate
  bool get autoValidate {
    return _autoValidate;
  }

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  // Flag
  bool get flag {
    return _flag;
  }

  set flag(bool value) {
    this._flag = value;
    notifyListeners();
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  String get jobFlag => _jobFlag;

  set jobFlag(String value) {
    this._jobFlag = value;
  }

  List<ListOrderTrackingModel> get listOrderTracking => _listOrderTracking;

  void onBackPress(BuildContext context) {
    Navigator.pop(context);
  }

  // ListOrderTrackingModel get listOIdPersonalSelected =>
  //     _listOIdPersonalSelected; //remark

  void getListOrderTracking(String startDate, String endDate) async {
    this._listOrderTracking.clear();
    //this._selectedIndex = -1;
    loadData = true;
    final ioc = new HttpClient();
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    String _orderTrackingSvy = await storage.read(key: "OrderTrackingSvy");
    String _orderTrackingSls = await storage.read(key: "OrderTrackingSls");
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var _jobName = _preferences.getString("job_name");
    var _body = jsonEncode({
      "CP_UNIT_ID": _preferences.getString("branchid"),
      "END_DATE": endDate,
      "NIK": _preferences.getString("username"),
      "START_DATE": startDate
    });
    print("job name = $_jobName");
    print("body order tracking = $_body");

    if (_jobName == "SVY" || _jobName == "SAD" || _jobName == "SA" || _jobName == "CMO") {
      print("masuk-svy");
      final _response = await _http.post(
          "${BaseUrl.urlGeneral}$_orderTrackingSvy",
          // "https://103.110.89.34/public/ms2dev/api/acction/order-tracking-svy",
          body: _body,
          headers: {
            "Content-Type": "application/json", "Authorization":"bearer $token"
          }).timeout(Duration(seconds: 60));

      print(_response.statusCode);

      try {
        if (_response.statusCode == 200) {
          final _data = jsonDecode(_response.body);
          if (_data.isNotEmpty) {
            print(_data.length);
            for (int i = 0; i < _data.length; i++) {
              this._listOrderTracking.add(ListOrderTrackingModel(
                  _data[i]['TANGGAL_APLIKASI'],
                  _data[i]['NO_APLIKASI'],
                  _data[i]['KODE_CABANG'],
                  _data[i]['NAMA_NASABAH'],
                  _data[i]['FINAL_RECOMENDATION'],
                  _data[i]['INITIAL_RECOMENDATION'],
                  _data[i]['LAST_KNOWN_STATE'],
                  _data[i]['TANGGAL_ORDER'],
                  _data[i]['DEALER']));
            }

            loadData = false;
          } else {
            showSnackBar("Hasil Order Tracking tidak ditemukan");
            loadData = false;
          }
        } else {
          showSnackBar(
              "Error get Order Tracking response ${_response.statusCode}");
          loadData = false;
        }
      } on TimeoutException catch (_) {
        showSnackBar("Request Timeout");
        loadData = false;
      } catch (e) {
        showSnackBar("Error $e");
        loadData = false;
      }
      notifyListeners();
    }
    else {
      print("masuk-sls");
      final _response = await _http.post(
          "${BaseUrl.urlGeneral}$_orderTrackingSls",
          // "https://103.110.89.34/public/ms2dev/api/acction/order-tracking-sls",
          body: _body,
          headers: {
            "Content-Type": "application/json", "Authorization":"bearer $token"
          }).timeout(Duration(seconds: 10));

      try {
        if (_response.statusCode == 200) {
          final _data = jsonDecode(_response.body);
          if (_data.isNotEmpty) {
            print(_data.length);
            for (int i = 0; i < _data.length; i++) {
              this._listOrderTracking.add(ListOrderTrackingModel(
                  _data[i]['TANGGAL_APLIKASI'].toString(),
                  _data[i]['NO_APLIKASI'],
                  _data[i]['KODE_CABANG'],
                  _data[i]['NAMA_NASABAH'],
                  _data[i]['FINAL_RECOMENDATION'],
                  _data[i]['INITIAL_RECOMENDATION'],
                  _data[i]['LAST_KNOWN_STATE'],
                  _data[i]['TANGGAL_ORDER'].toString(),
                  _data[i]['DEALER']));
            }

            loadData = false;
          } else {
            showSnackBar("Hasil Order Tracking tidak ditemukan");
            loadData = false;
          }
        } else {
          showSnackBar(
              "Error get Order Tracking response ${_response.statusCode}");
          loadData = false;
        }
      } on TimeoutException catch (_) {
        showSnackBar("Request Timeout");
        loadData = false;
      } catch (e) {
        showSnackBar("Error $e");
        loadData = false;
      }
      notifyListeners();
    }
  }

  void showSnackBar(String text) {
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"),
        behavior: SnackBarBehavior.floating,
        backgroundColor: snackbarColor,
        duration: Duration(seconds: 2)));
  }
}
