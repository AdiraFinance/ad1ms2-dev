import 'dart:collection';

import 'package:ad1ms2_dev/models/address_guarantor_model.dart';
import 'package:ad1ms2_dev/models/form_m_company_alamat_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/screens/form_m/search_kelurahan.dart';
import 'package:ad1ms2_dev/shared/form_m_company_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/search_kelurahan_change_notif.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


//tidak dipakai
class FormMCompanyAddAlamatKorespondensiChangeNotifier with ChangeNotifier {
  bool _autoValidate = false;
  TextEditingController _controllerAlamat = TextEditingController();
  TextEditingController _controllerRT = TextEditingController();
  TextEditingController _controllerRW = TextEditingController();
  TextEditingController _controllerKelurahan = TextEditingController();
  TextEditingController _controllerKecamatan = TextEditingController();
  TextEditingController _controllerKota = TextEditingController();
  TextEditingController _controllerProvinsi = TextEditingController();
  TextEditingController _controllerPostalCode = TextEditingController();
  TextEditingController _controllerTelepon1Area = TextEditingController();
  TextEditingController _controllerTelepon1 = TextEditingController();
  TextEditingController _controllerTelepon2Area = TextEditingController();
  TextEditingController _controllerTelepon2 = TextEditingController();
  TextEditingController _controllerFaxArea = TextEditingController();
  TextEditingController _controllerFax = TextEditingController();
  bool _isSameWithIdentity = false;
  JenisAlamatModel _jenisAlamatSelected;
  JenisAlamatModel _jenisAlamatSelectedTemp;
  KelurahanModel _kelurahanSelected;
  KelurahanModel _kelurahanSelectedTemp;
  AddressModelCompany _addressModelTemp;
  Map _dummy;
  String _alamatTemp,
      _rtTemp,
      _rwTemp,
      _kelurahanTemp,
      _kecamatanTemp,
      _kotaTemp,
      _provinsiTemp,
      _postalCodeTemp,
      _telepon1AreaTemp,
      _telepon1Temp,
      _telepon2AreaTemp,
      _telepon2Temp,
      _faxAreaTemp,
      _faxTemp;
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  bool _enableTfAddress = true;
  bool _enableTfRT = true;
  bool _enableTfRW = true;
  bool _enableTfKelurahan = true;
  bool _enableTfKecamatan = true;
  bool _enableTfKota = true;
  bool _enableTfProv = true;
  bool _enableTfPostalCode = true;
  bool _enableTfAreaCode1 = true;
  bool _enableTfPhone1 = true;
  bool _enableTfAreaCode2 = true;
  bool _enableTfPhone2 = true;
  bool _enableTfAreaCodeFax = true;
  bool _enableTfFax = true;

  GlobalKey<FormState> get key => _key;

  List<JenisAlamatModel> _listJenisAlamat = [];

  JenisAlamatModel get jenisAlamatSelectedTemp => _jenisAlamatSelectedTemp;

  KelurahanModel get kelurahanSelectedTemp => _kelurahanSelectedTemp;

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  TextEditingController get controllerAlamat => _controllerAlamat;
  TextEditingController get controllerRT => _controllerRT;
  TextEditingController get controllerRW => _controllerRW;
  TextEditingController get controllerKota => _controllerKota;
  TextEditingController get controllerKecamatan => _controllerKecamatan;
  TextEditingController get controllerKelurahan => _controllerKelurahan;
  TextEditingController get controllerProvinsi => _controllerProvinsi;
  TextEditingController get controllerPostalCode => _controllerPostalCode;
  TextEditingController get controllerTelepon1Area => _controllerTelepon1Area;
  TextEditingController get controllerTelepon1 => _controllerTelepon1;
  TextEditingController get controllerTelepon2Area => _controllerTelepon2Area;
  TextEditingController get controllerTelepon2 => _controllerTelepon2;
  TextEditingController get controllerFaxArea => _controllerFaxArea;
  TextEditingController get controllerFax => _controllerFax;

  // Jenis Alamat
  UnmodifiableListView<JenisAlamatModel> get listJenisAlamat {
    return UnmodifiableListView(this._listJenisAlamat);
  }
  JenisAlamatModel get jenisAlamatSelected => _jenisAlamatSelected;

  set jenisAlamatSelected(JenisAlamatModel value) {
    this._jenisAlamatSelected = value;
    notifyListeners();
  }

  // Sama Dengan Identitas
  bool get isSameWithIdentity => _isSameWithIdentity;

  set isSameWithIdentity(bool value) {
    this._isSameWithIdentity = value;
    notifyListeners();
  }

  void searchKelurahan(BuildContext context) async {
    final data = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ChangeNotifierProvider(
          create: (context) => SearchKelurahanChangeNotif(),
          child: SearchKelurahan()
        )
      )
    );
    if (data != null) {
      kelurahanSelected = data;
      this._controllerKelurahan.text = data.KEL_NAME;
      this._controllerKecamatan.text = data.KEC_NAME;
      this._controllerKota.text = data.KABKOT_NAME;
      this._controllerProvinsi.text = data.PROV_NAME;
      this._controllerPostalCode.text = data.ZIPCODE;
      notifyListeners();
    } else {
      return;
    }
  }

  void addDataListJenisAlamat(BuildContext context, int index) {
    var _provider =
    Provider.of<FormMCompanyAlamatChangeNotifier>(context, listen: false);
    if (index == null) {
      // add
      if (_provider.listAlamatKorespondensi.isEmpty) {
        _listJenisAlamat.add(JenisAlamatModel("01", "Identitas"));
      } else {
        _listJenisAlamat = [
          JenisAlamatModel("02", "Domisili 1"),
          JenisAlamatModel("03", "Domisili 2"),
          JenisAlamatModel("04", "Domisili 3"),
          JenisAlamatModel("05", "Kantor 1"),
          JenisAlamatModel("05", "Kantor 2"),
          JenisAlamatModel("07", "Kantor 3")
        ];
      }
    } else {
      // edit
      if (_provider.listAlamatKorespondensi[index].jenisAlamatModel.KODE == '01') {
        _listJenisAlamat.add(JenisAlamatModel("01", "Identitas"));
      } else {
        _listJenisAlamat = [
          JenisAlamatModel("02", "Domisili 1"),
          JenisAlamatModel("03", "Domisili 2"),
          JenisAlamatModel("04", "Domisili 3"),
          JenisAlamatModel("05", "Kantor 1"),
          JenisAlamatModel("05", "Kantor 2"),
          JenisAlamatModel("07", "Kantor 3")
        ];
      }
    }
  }

  // void check(BuildContext context, int flag, int index) {
  //   final _form = _key.currentState;
  //   if (flag == 0) {
  //     if (_form.validate()) {
  //       Provider.of<FormMCompanyAlamatChangeNotifier>(context, listen: false)
  //         .addAlamatKorespondensi(AddressGuarantorModelCompany(
  //           this._jenisAlamatSelected,
  //           this._kelurahanSelected,
  //           this._controllerAlamat.text,
  //           this._controllerRT.text,
  //           this._controllerRW.text,
  //           this._controllerTelepon1Area.text,
  //           this._controllerTelepon1.text,
  //           this._controllerTelepon2Area.text,
  //           this._controllerTelepon2.text,
  //           this._controllerFaxArea.text,
  //           this._controllerFax.text,
  //           this._isSameWithIdentity, _dummy,false
  //         ));
  //       Navigator.pop(context);
  //     } else {
  //       autoValidate = true;
  //     }
  //   } else {
  //     if (_form.validate()) {
  //       Provider.of<FormMCompanyAlamatChangeNotifier>(context, listen: false)
  //         .updateAlamatKorespondensi(AddressGuarantorModelCompany(
  //           this._jenisAlamatSelected,
  //           this._kelurahanSelected,
  //           this._controllerAlamat.text,
  //           this._controllerRT.text,
  //           this._controllerRW.text,
  //           this._controllerTelepon1Area.text,
  //           this._controllerTelepon1.text,
  //           this._controllerTelepon2Area.text,
  //           this._controllerTelepon2.text,
  //           this._controllerFaxArea.text,
  //           this._controllerFax.text,
  //           this._isSameWithIdentity,_dummy, false),
  //           index);
  //       Navigator.pop(context);
  //     } else {
  //       autoValidate = true;
  //     }
  //   }
  // }

  void checkValidCodeArea1(String value) {
    if (value == "08") {
      Future.delayed(Duration(milliseconds: 100), () {
        this._controllerTelepon1Area.clear();
      });
    } else {
      return;
    }
  }

  void checkValidCodeArea2(String value) {
    if (value == "08") {
      Future.delayed(Duration(milliseconds: 100), () {
        this._controllerTelepon2Area.clear();
      });
    } else {
      return;
    }
  }

  void checkValidCodeAreaFax(String value) {
    if (value == "08") {
      Future.delayed(Duration(milliseconds: 100), () {
        this._controllerFaxArea.clear();
      });
    } else {
      return;
    }
  }

  Future<void> setValueForEdit(AddressModelCompany data, BuildContext context, int index, bool isSameWithIdentity) async {
    addDataListJenisAlamat(context, index);
//    if (isSameWithIdentity) {
//      setValueIsSameWithIdentity(isSameWithIdentity, context, data);
//    } else {
      _addressModelTemp = data;
      for (int i = 0; i < this._listJenisAlamat.length; i++) {
        if (data.jenisAlamatModel.KODE == this._listJenisAlamat[i].KODE) {
          this._jenisAlamatSelected = this._listJenisAlamat[i];
          this._jenisAlamatSelectedTemp = this._listJenisAlamat[i];
        }
      }
      this._kelurahanSelected = data.kelurahanModel;
      this._controllerAlamat.text = data.address;
      this._alamatTemp = this._controllerAlamat.text;
      this._controllerRT.text = data.rt;
      this._rtTemp = this._controllerRT.text;
      this._controllerRW.text = data.rw;
      this._rwTemp = this._controllerRW.text;
      this._controllerKelurahan.text = data.kelurahanModel.KEL_NAME;
      this._kelurahanTemp = this._controllerKelurahan.text;
      this._controllerKecamatan.text = data.kelurahanModel.KEC_NAME;
      this._kecamatanTemp = this._controllerKecamatan.text;
      this._controllerKota.text = data.kelurahanModel.KABKOT_NAME;
      this._kotaTemp = this._controllerKota.text;
      this._controllerProvinsi.text = data.kelurahanModel.PROV_NAME;
      this._provinsiTemp = this._controllerProvinsi.text;
      this._controllerPostalCode.text = data.kelurahanModel.ZIPCODE;
      this._postalCodeTemp = this._controllerPostalCode.text;

      this._controllerTelepon1Area.text = data.phoneArea1;
      this._telepon1AreaTemp = this._controllerTelepon1Area.text;
      this._controllerTelepon1.text = data.phone1;
      this._telepon1Temp = this._controllerTelepon1.text;

      this._controllerTelepon2Area.text = data.phoneArea2;
      this._telepon2AreaTemp = this._controllerTelepon2Area.text;
      this._controllerTelepon2.text = data.phone2;
      this._telepon2Temp = this._controllerTelepon2.text;

      this._controllerFaxArea.text = data.faxArea;
      this._faxAreaTemp = this._controllerFaxArea.text;
      this._controllerFax.text = data.fax;
      this._faxTemp = this._controllerFax.text;
//    }
  }

  String get alamatTemp => _alamatTemp;
  get rtTemp => _rtTemp;
  get rwTemp => _rwTemp;
  get kotaTemp => _kotaTemp;
  get kecamatanTemp => _kecamatanTemp;
  get kelurahanTemp => _kelurahanTemp;
  get provinsiTemp => _provinsiTemp;
  get postalCodeTemp => _postalCodeTemp;
  get telepon1AreaTemp => _telepon1AreaTemp;
  get telepon1Temp => _telepon1Temp;
  get telepon2AreaTemp => _telepon2AreaTemp;
  get telepon2Temp => _telepon2Temp;
  get faxAreaTemp => _faxAreaTemp;
  get faxTemp => _faxTemp;

  KelurahanModel get kelurahanSelected => _kelurahanSelected;

  set kelurahanSelected(KelurahanModel value) {
    this._kelurahanSelected = value;
    notifyListeners();
  }

//  void setValueIsSameWithIdentity(bool value, BuildContext context, CompanyAlamatKorespondensiModel model) {
//    var _provider =
//    Provider.of<FormMCompanyAlamatChangeNotifier>(context, listen: false);
//    CompanyAlamatKorespondensiModel data;
//    if (_addressModelTemp == null) {
//      if (value) {
//        for (int i = 0; i < _provider.listAlamatKorespondensi.length; i++) {
//          if (_provider.listAlamatKorespondensi[i].jenisAlamatModel.KODE == "01") {
//            data = _provider.listAlamatKorespondensi[i];
//          }
//        }
//        if (model != null) {
//          for (int i = 0; i < _listJenisAlamat.length; i++) {
//            if (model.jenisAlamatModel.KODE == _listJenisAlamat[i].KODE) {
//              this._jenisAlamatSelected = _listJenisAlamat[i];
//              this._jenisAlamatSelectedTemp = this._jenisAlamatSelected;
//              this._isSameWithIdentity = value;
//            }
//          }
//        }
//        this._controllerAlamat.text = data.address;
//        this._controllerRT.text = data.rt;
//        this._controllerRW.text = data.rw;
//        this._kelurahanSelected = data.kelurahanModel;
//        this._controllerAlamat.text = data.address;
//        this._alamatTemp = this._controllerAlamat.text;
//        this._controllerRT.text = data.rt;
//        this._rtTemp = this._controllerRT.text;
//        this._controllerRW.text = data.rw;
//        this._rwTemp = this._controllerRW.text;
//        this._controllerKelurahan.text = data.kelurahanModel.KEL_NAME;
//        this._kelurahanTemp = this._controllerKelurahan.text;
//        this._controllerKecamatan.text = data.kelurahanModel.KEC_NAME;
//        this._kecamatanTemp = this._controllerKecamatan.text;
//        this._controllerKota.text = data.kelurahanModel.KABKOT_NAME;
//        this._kotaTemp = this._controllerKota.text;
//        this._controllerProvinsi.text = data.kelurahanModel.PROV_NAME;
//        this._provinsiTemp = this._controllerProvinsi.text;
//        this._controllerPostalCode.text = data.kelurahanModel.ZIPCODE;
//        this._postalCodeTemp = this._controllerPostalCode.text;
//
//        this._controllerTelepon1Area.text = data.phone1AreaCode;
//        this._telepon1AreaTemp = this._controllerTelepon1Area.text;
//        this._controllerTelepon1.text = data.phone1;
//        this._telepon1Temp = this._controllerTelepon1.text;
//
//        this._controllerTelepon2Area.text = data.phone2AreaCode;
//        this._telepon2AreaTemp = this._controllerTelepon2Area.text;
//        this._controllerTelepon2.text = data.phone2;
//        this._telepon2Temp = this._controllerTelepon2.text;
//
//        this._controllerFaxArea.text = data.faxAreaCode;
//        this._faxAreaTemp = this._controllerFaxArea.text;
//        this._controllerFax.text = data.fax;
//        this._faxTemp = this._controllerFax.text;
//        enableTfAddress = !value;
//        enableTfRT = !value;
//        enableTfRW = !value;
//        enableTfKelurahan = !value;
//        enableTfKecamatan = !value;
//        enableTfKota = !value;
//        enableTfProv = !value;
//        enableTfPostalCode = !value;
//        enableTfAreaCode1 = !value;
//        enableTfPhone1 = !value;
//        enableTfAreaCode2 = !value;
//        enableTfPhone2 = !value;
//        enableTfAreaCodeFax = !value;
//        enableTfFax = !value;
//      } else {
//        this._controllerAlamat.clear();
//        this._controllerRT.clear();
//        this._controllerRW.clear();
//        this._kelurahanSelected = null;
//        this._controllerAlamat.clear();
//        this._controllerRT.clear();
//        this._controllerRW.clear();
//        this._controllerKelurahan.clear();
//        this._controllerKecamatan.clear();
//        this._controllerKota.clear();
//        this._controllerProvinsi.clear();
//        this._controllerPostalCode.clear();
//        this._controllerTelepon1Area.clear();
//        this._controllerTelepon1.clear();
//        this._controllerTelepon2Area.clear();
//        this._controllerTelepon2.clear();
//        this._controllerFaxArea.clear();
//        this._controllerFax.clear();
//        enableTfAddress = !value;
//        enableTfRT = !value;
//        enableTfRW = !value;
//        enableTfKelurahan = !value;
//        enableTfKecamatan = !value;
//        enableTfKota = !value;
//        enableTfProv = !value;
//        enableTfPostalCode = !value;
//        enableTfAreaCode1 = !value;
//        enableTfPhone1 = !value;
//        enableTfAreaCode2 = !value;
//        enableTfPhone2 = !value;
//        enableTfAreaCodeFax = !value;
//        enableTfFax = !value;
//      }
//    } else {
//      if (value) {
//        for (int i = 0; i < _provider.listAlamatKorespondensi.length; i++) {
//          if (_provider.listAlamatKorespondensi[i].jenisAlamatModel.KODE == "01") {
//            data = _provider.listAlamatKorespondensi[i];
//          }
//        }
//        this._controllerAlamat.text = data.address;
//        this._controllerRT.text = data.rt;
//        this._controllerRW.text = data.rw;
//        this._kelurahanSelected = data.kelurahanModel;
//        this._controllerAlamat.text = data.address;
//        this._controllerRT.text = data.rt;
//        this._controllerRW.text = data.rw;
//        this._controllerKelurahan.text = data.kelurahanModel.KEL_NAME;
//        this._controllerKecamatan.text = data.kelurahanModel.KEC_NAME;
//        this._controllerKota.text = data.kelurahanModel.KABKOT_NAME;
//        this._controllerProvinsi.text = data.kelurahanModel.PROV_NAME;
//        this._controllerPostalCode.text = data.kelurahanModel.ZIPCODE;
//        this._controllerTelepon1Area.text = data.phone1AreaCode;
//        this._controllerTelepon1.text = data.phone1;
//        this._controllerTelepon2Area.text = data.phone2AreaCode;
//        this._controllerTelepon2.text = data.phone2;
//        this._controllerFaxArea.text = data.faxAreaCode;
//        this._controllerFax.text = data.fax;
//        enableTfAddress = !value;
//        enableTfRT = !value;
//        enableTfRW = !value;
//        enableTfKelurahan = !value;
//        enableTfKecamatan = !value;
//        enableTfKota = !value;
//        enableTfProv = !value;
//        enableTfPostalCode = !value;
//        enableTfAreaCode1 = !value;
//        enableTfPhone1 = !value;
//        enableTfAreaCode2 = !value;
//        enableTfPhone2 = !value;
//        enableTfAreaCodeFax = !value;
//        enableTfFax = !value;
//      } else {
//        this._controllerAlamat.clear();
//        this._controllerRT.clear();
//        this._controllerRW.clear();
//        this._kelurahanSelected = null;
//        this._controllerAlamat.clear();
//        this._controllerRT.clear();
//        this._controllerRW.clear();
//        this._controllerKelurahan.clear();
//        this._controllerKecamatan.clear();
//        this._controllerKota.clear();
//        this._controllerProvinsi.clear();
//        this._controllerPostalCode.clear();
//        this._controllerTelepon1Area.clear();
//        this._controllerTelepon1.clear();
//        this._controllerTelepon2Area.clear();
//        this._controllerTelepon2.clear();
//        this._controllerFaxArea.clear();
//        this._controllerFax.clear();
//        enableTfAddress = !value;
//        enableTfRT = !value;
//        enableTfRW = !value;
//        enableTfKelurahan = !value;
//        enableTfKecamatan = !value;
//        enableTfKota = !value;
//        enableTfProv = !value;
//        enableTfPostalCode = !value;
//        enableTfAreaCode1 = !value;
//        enableTfPhone1 = !value;
//        enableTfAreaCode2 = !value;
//        enableTfPhone2 = !value;
//        enableTfAreaCodeFax = !value;
//        enableTfFax = !value;
//      }
//    }
//  }

  bool get enableTfAddress => _enableTfAddress;

  set enableTfAddress(bool value) {
    this._enableTfAddress = value;
  }

  bool get enableTfProv => _enableTfProv;

  set enableTfProv(bool value) {
    this._enableTfProv = value;
  }

  bool get enableTfKota => _enableTfKota;

  set enableTfKota(bool value) {
    this._enableTfKota = value;
  }

  bool get enableTfKecamatan => _enableTfKecamatan;

  set enableTfKecamatan(bool value) {
    this._enableTfKecamatan = value;
  }

  bool get enableTfKelurahan => _enableTfKelurahan;

  set enableTfKelurahan(bool value) {
    this._enableTfKelurahan = value;
  }

  bool get enableTfRW => _enableTfRW;

  set enableTfRW(bool value) {
    this._enableTfRW = value;
  }

  bool get enableTfRT => _enableTfRT;

  set enableTfRT(bool value) {
    this._enableTfRT = value;
  }

  bool get enableTfPostalCode => _enableTfPostalCode;

  set enableTfPostalCode(bool value) {
    this._enableTfPostalCode = value;
  }

  bool get enableTfAreaCode1 => _enableTfAreaCode1;

  set enableTfAreaCode1(bool value) {
    this._enableTfAreaCode1 = value;
  }

  bool get enableTfPhone1 => _enableTfPhone1;

  set enableTfPhone1(bool value) {
    this._enableTfPhone1 = value;
  }

  bool get enableTfAreaCode2 => _enableTfAreaCode2;

  set enableTfAreaCode2(bool value) {
    this._enableTfAreaCode2 = value;
  }

  bool get enableTfPhone2 => _enableTfPhone2;

  set enableTfPhone2(bool value) {
    this._enableTfPhone2 = value;
  }

  bool get enableTfAreaCodeFax => _enableTfAreaCodeFax;

  set enableTfAreaCodeFax(bool value) {
    this._enableTfAreaCodeFax = value;
  }

  bool get enableTfFax => _enableTfFax;

  set enableTfFax(bool value) {
    this._enableTfFax = value;
  }

  // get areaCodeTemp => _telepon1AreaTemp;
  //
  // set areaCodeTemp(value) {
  //   this._telepon1AreaTemp = value;
  // }
}
