import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/source_order_name_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SearchSourceOrderNameAXIChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  bool _loadData = false;
  TextEditingController _controllerSearch = TextEditingController();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<SourceOrderNameModel> _listSourceOrderNameAXI = [];
  List<SourceOrderNameModel> _listSourceOrderNameAXITemp = [];

  TextEditingController get controllerSearch => _controllerSearch;

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void  changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<SourceOrderNameModel> get listSourceOrderNameAXI {
    return UnmodifiableListView(this._listSourceOrderNameAXI);
  }

  UnmodifiableListView<SourceOrderNameModel> get listSourceOrderNameAXITemp {
    return UnmodifiableListView(this._listSourceOrderNameAXITemp);
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  void getSourceOrderNameAXI(String query) async{
    this._listSourceOrderNameAXI.clear();
    this._listSourceOrderNameAXITemp.clear();
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
    var storage = FlutterSecureStorage();

    String _namaSumberOrderAXI = await storage.read(key: "NamaSumberOrderAXI");
    var _body = jsonEncode({
      "SZBRANCH": _preferences.getString("branchid"),
      "SZTYPE": "",
      "SZKEYWORD": query
    });
    final _response = await _http.post(
        "${BaseUrl.urlGeneral}$_namaSumberOrderAXI",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      var _data = _result['RC1'];
      if(_data == null){
        showSnackBar("Nama Sumber Order tidak ditemukan");
        loadData = false;
      }
      else{
        for(int i=0; i <_data.length; i++){
          this._listSourceOrderNameAXI.add(
            SourceOrderNameModel(
              _data[i]['AXI_NO'],
              _data[i]['AXI_FULL_NAME']
            )
          );
        }
        loadData = false;
      }
    }
    else{
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState. showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  void searchSourceOrderNameAXI(String query) async {
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    }
    else {
      _listSourceOrderNameAXITemp.clear();
      if (query.isEmpty) {
        return;
      }

      _listSourceOrderNameAXI.forEach((dataSourceOrderNameAXI) {
        if (dataSourceOrderNameAXI.kode.contains(query) || dataSourceOrderNameAXI.deskripsi.contains(query)) {
          this._listSourceOrderNameAXITemp.add(dataSourceOrderNameAXI);
        }
      });
      notifyListeners();
    }
  }

  void clearSearchTemp() {
    _listSourceOrderNameAXITemp.clear();
  }
}
