import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/source_order_name_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SearchSourceOrderNameCustomerChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  bool _loadData = false;
  TextEditingController _controllerSearch = TextEditingController();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<SourceOrderNameModel> _listSourceOrderNameCustomer = [];
  List<SourceOrderNameModel> _listSourceOrderNameCustomerTemp = [];

  TextEditingController get controllerSearch => _controllerSearch;

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void  changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<SourceOrderNameModel> get listSourceOrderNameCustomer {
    return UnmodifiableListView(this._listSourceOrderNameCustomer);
  }

  UnmodifiableListView<SourceOrderNameModel> get listSourceOrderNameCustomerTemp {
    return UnmodifiableListView(this._listSourceOrderNameCustomerTemp);
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  void getSourceOrderNameCustomer(String query) async{
    this._listSourceOrderNameCustomer.clear();
    this._listSourceOrderNameCustomerTemp.clear();
    // SharedPreferences _preferences = await SharedPreferences.getInstance();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
    var storage = FlutterSecureStorage();

    String _namaSumberOrderCustomer = await storage.read(key: "NamaSumberOrderCustomer");
    var _body = jsonEncode({
      "P_AC_CUST_ID": query
    });
    final _response = await _http.post(
        "${BaseUrl.urlGeneral}$_namaSumberOrderCustomer",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      var _data = _result['P_RC1'];
      if(_data == null){
        showSnackBar("${_result['strmessage']}");
        loadData = false;
      }
      else{
        for(int i=0; i <_data.length; i++){
          this._listSourceOrderNameCustomer.add(
            SourceOrderNameModel(
              _data[i]['KODE'],
              _data[i]['DESKRIPSI'],
            )
          );
        }
        loadData = false;
      }
    }
    else{
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState. showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  // void searchSourceOrderNameCustomer(String query) async {
  //   if(query.length < 3) {
  //     showSnackBar("Input minimal 3 karakter");
  //   }
  //   else {
  //     _listSourceOrderNameCustomerTemp.clear();
  //     if (query.isEmpty) {
  //       return;
  //     }
  //
  //     _listSourceOrderNameCustomer.forEach((dataSourceOrderNameCustomer) {
  //       if (dataSourceOrderNameCustomer.kode.contains(query) || dataSourceOrderNameCustomer.deskripsi.contains(query)) {
  //         this._listSourceOrderNameCustomerTemp.add(dataSourceOrderNameCustomer);
  //       }
  //     });
  //     notifyListeners();
  //   }
  // }

  void clearSearchTemp() {
    _listSourceOrderNameCustomerTemp.clear();
  }
}
