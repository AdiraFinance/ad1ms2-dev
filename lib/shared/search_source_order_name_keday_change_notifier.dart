import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/source_order_name_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SearchSourceOrderNameKedayChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  bool _loadData = false;
  TextEditingController _controllerSearch = TextEditingController();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<SourceOrderNameModel> _listSourceOrderNameKeday = [];
  List<SourceOrderNameModel> _listSourceOrderNameKedayTemp = [];

  TextEditingController get controllerSearch => _controllerSearch;

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void  changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<SourceOrderNameModel> get listSourceOrderNameKeday {
    return UnmodifiableListView(this._listSourceOrderNameKeday);
  }

  UnmodifiableListView<SourceOrderNameModel> get listSourceOrderNameKedayTemp {
    return UnmodifiableListView(this._listSourceOrderNameKedayTemp);
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  void getSourceOrderNameKeday() async{
    this._listSourceOrderNameKeday.clear();
    this._listSourceOrderNameKedayTemp.clear();
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
    var storage = FlutterSecureStorage();

    String _namaSumberOrderKeday = await storage.read(key: "NamaSumberOrderKeday");
    var _body = jsonEncode({
      "P_BRANCH_ID": _preferences.getString("branchid"),
    });
    final _response = await _http.post(
        "${BaseUrl.urlGeneral}$_namaSumberOrderKeday",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      var _data = _result['RC1'];
      if(_data == null){
        showSnackBar("${_result['strmessage']}");
        loadData = false;
      }
      else{
        for(int i=0; i <_data.length; i++){
          this._listSourceOrderNameKeday.add(
            SourceOrderNameModel(
              _data[i]['KODE'],
              _data[i]['DESKRIPSI']
            )
          );
        }
        loadData = false;
      }
    }
    else{
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState. showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  void searchSourceOrderNameKeday(String query) async {
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    }
    else {
      _listSourceOrderNameKedayTemp.clear();
      if (query.isEmpty) {
        return;
      }

      _listSourceOrderNameKeday.forEach((dataSourceOrderNameKeday) {
        if (dataSourceOrderNameKeday.kode.contains(query) || dataSourceOrderNameKeday.deskripsi.contains(query)) {
          this._listSourceOrderNameKedayTemp.add(dataSourceOrderNameKeday);
        }
      });
      notifyListeners();
    }
  }

  void clearSearchTemp() {
    _listSourceOrderNameKedayTemp.clear();
  }
}
