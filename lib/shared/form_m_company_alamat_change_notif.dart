import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/address_guarantor_model.dart';
import 'package:ad1ms2_dev/models/form_m_company_alamat_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_addr_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FormMCompanyAlamatChangeNotifier with ChangeNotifier {
  int _oldListSize = 0;
  int _selectedIndex = -1;
  bool _autoValidate = false;
  List<AddressModelCompany> _listAlamatKorespondensi = [];
  TextEditingController _controllerAddress = TextEditingController();
  TextEditingController _controllerAddressType = TextEditingController();
  TextEditingController _controllerRT = TextEditingController();
  TextEditingController _controllerRW = TextEditingController();
  TextEditingController _controllerKelurahan = TextEditingController();
  TextEditingController _controllerKecamatan = TextEditingController();
  TextEditingController _controllerKota = TextEditingController();
  TextEditingController _controllerProvinsi = TextEditingController();
  TextEditingController _controllerPostalCode = TextEditingController();
  TextEditingController _controllerTelepon1Area = TextEditingController();
  TextEditingController _controllerTelepon1 = TextEditingController();
  TextEditingController _controllerTelepon2Area = TextEditingController();
  TextEditingController _controllerTelepon2 = TextEditingController();
  TextEditingController _controllerFaxArea = TextEditingController();
  TextEditingController _controllerFax = TextEditingController();
  DbHelper _dbHelper = DbHelper();
  KelurahanModel _kelurahanSelected;
  String _lastKnownState;

  bool get autoValidate => _autoValidate;
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  bool _flag = false;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  int get oldListSize => _oldListSize;

  set oldListSize(int value) {
    this._oldListSize = value;
    notifyListeners();
  }

  int get selectedIndex => _selectedIndex;

  set selectedIndex(int value) {
    this._selectedIndex = value;
    notifyListeners();
  }

  KelurahanModel get kelurahanSelected => _kelurahanSelected;

  set kelurahanSelected(KelurahanModel value) {
      this._kelurahanSelected = value;
  }

  List<AddressModelCompany> get listAlamatKorespondensi => _listAlamatKorespondensi;

  // Alamat
  TextEditingController get controllerAddress => _controllerAddress;

  // Jenis Alamat
  TextEditingController get controllerAddressType => _controllerAddressType;

  // RT
  TextEditingController get controllerRT => _controllerRT;

  // RW
  TextEditingController get controllerRW => _controllerRW;

  // Kelurahan
  TextEditingController get controllerKelurahan => _controllerKelurahan;

  // Kecamatan
  TextEditingController get controllerKecamatan => _controllerKecamatan;

  // Kabupaten/Kota
  TextEditingController get controllerKota => _controllerKota;

  // Provinsi
  TextEditingController get controllerProvinsi => _controllerProvinsi;

  // Kode Pos
  TextEditingController get controllerPostalCode => _controllerPostalCode;

  // Telepon 1 Area
  TextEditingController get controllerTelepon1Area => _controllerTelepon1Area;

  // Telepon 1
  TextEditingController get controllerTelepon1 => _controllerTelepon1;

  // Telepon 2 Area
  TextEditingController get controllerTelepon2Area => _controllerTelepon2Area;

  // Telepon 2
  TextEditingController get controllerTelepon2 => _controllerTelepon2;

  // Kode Area Fax
  TextEditingController get controllerFaxArea => _controllerFaxArea;

  // Fax
  TextEditingController get controllerFax => _controllerFax;

  void setPreference() async {
    SharedPreferences _preference = await SharedPreferences.getInstance();
    this._lastKnownState = _preference.getString("last_known_state");
  }

  void addAlamatKorespondensi(AddressModelCompany value) {
    this._listAlamatKorespondensi.add(value);
    notifyListeners();
  }

  void updateAlamatKorespondensi(AddressModelCompany value, int index) {
    this._listAlamatKorespondensi[index] = value;
    notifyListeners();
  }

  void deleteAlamatKorespondensi(BuildContext context, int index) {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context){
        return Theme(
          data: ThemeData(
              fontFamily: "NunitoSans",
              primaryColor: Colors.black,
              primarySwatch: primaryOrange,
              accentColor: myPrimaryColor
          ),
          child: AlertDialog(
            title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
            content: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text("Apakah kamu yakin menghapus alamat ini?",),
              ],
            ),
            actions: <Widget>[
              new FlatButton(
                onPressed: () {
                  if(this._lastKnownState == "IDE"){
                    this._listAlamatKorespondensi.removeAt(index);
                    if(selectedIndex == index){
                      selectedIndex = -1;
                      this._controllerAddress.clear();
                      this._controllerAddressType.clear();
                      this._controllerRT.clear();
                      this._controllerRW.clear();
                      this._controllerKelurahan.clear();
                      this._controllerKecamatan.clear();
                      this._controllerKota.clear();
                      this._controllerProvinsi.clear();
                      this._controllerPostalCode.clear();
                      this._controllerTelepon1Area.clear();
                      this._controllerTelepon1.clear();
                      this._controllerTelepon2Area.clear();
                      this._controllerTelepon2.clear();
                      this._controllerFaxArea.clear();
                      this._controllerFax.clear();
                    }
                  }
                  else{
                    this._listAlamatKorespondensi[index].active = 1;
                  }
                  notifyListeners();
                  Navigator.pop(context);
                },
                child: new Text('Ya', style: TextStyle(color: Colors.grey),),
              ),
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: new Text('Tidak'),
              ),
            ],
          ),
        );
      }
    );
  }

  void setAddress(AddressModelCompany value) {
    kelurahanSelected = value.kelurahanModel;
    this._controllerAddress.text = value.address;
    this._controllerAddressType.text = value.jenisAlamatModel.KODE + " - " + value.jenisAlamatModel.DESKRIPSI;
    this._controllerRT.text = value.rt;
    this._controllerRW.text = value.rw;
    this._controllerKelurahan.text = value.kelurahanModel.KEL_ID + " - " + value.kelurahanModel.KEL_NAME;
    this._controllerKecamatan.text = value.kelurahanModel.KEC_ID + " - " + value.kelurahanModel.KEC_NAME;
    this._controllerKota.text = value.kelurahanModel.KABKOT_ID + " - " + value.kelurahanModel.KABKOT_NAME;
    this._controllerProvinsi.text = value.kelurahanModel.PROV_ID + " - " + value.kelurahanModel.PROV_NAME;
    this._controllerPostalCode.text = value.kelurahanModel.ZIPCODE;
    this._controllerTelepon1Area.text = value.phoneArea1;
    this._controllerTelepon1.text = value.phone1;
    this._controllerTelepon2Area.text = value.phoneArea2;
    this._controllerTelepon2.text = value.phone2;
    this._controllerFaxArea.text = value.faxArea;
    this._controllerFax.text = value.fax;
    notifyListeners();
  }

  void iconShowDialog(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans"
            ),
            child: AlertDialog(
              title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(
                      "∙ Pilih salah satu alamat untuk mengubah detail alamat",
                      style: TextStyle(fontWeight: FontWeight.w400)
                  ),
                  Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                            text: "∙ Klik tanda",
                            style: TextStyle(fontWeight: FontWeight.w400)
                        ),
                        WidgetSpan(
                          child: Icon(Icons.more_vert, color: Colors.grey),
                        ),
                        TextSpan(
                            text: "pada salah satu alamat untuk memilih sebagai Alamat Korespondensi atau menghapus alamat",
                            style: TextStyle(fontWeight: FontWeight.w400)
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height/37,),
                  Text.rich(
                    TextSpan(
                      style: TextStyle(
                        fontSize: 12,
                      ),
                      children: [
                        TextSpan(
                            text: "* Tekan",
                            style: TextStyle(fontSize: 12, color: Colors.grey)
                        ),
                        WidgetSpan(
                          child: Icon(Icons.info_outline, color: Colors.grey),
                        ),
                        TextSpan(
                            text: "untuk melihat kembali informasi",
                            style: TextStyle(fontSize: 12, color: Colors.grey)
                        )
                      ],
                    ),
                  ),
                  // Text("* Tekan icon pada pojok kanan atas untuk melihat kembali",style: TextStyle(fontSize: 12, color: Colors.grey))
                ],
              ),
              actions: <Widget>[
                FlatButton(
                    onPressed: (){
                      Navigator.pop(context);
                      // _updateStatusShowDialogSimilarity();
                    },
                    child: Text(
                        "CLOSE",
                        style: TextStyle(
                          color: primaryOrange,
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          letterSpacing: 1.25,
                        )
                    )
                )
              ],
            ),
          );
        }
    );
  }

  void isShowDialog(BuildContext context) {
    if(this.listAlamatKorespondensi.length == 1 || this.listAlamatKorespondensi.length == 2) {
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context){
            return Theme(
              data: ThemeData(
                  fontFamily: "NunitoSans"
              ),
              child: AlertDialog(
                title: Text("Information", style: TextStyle(fontWeight: FontWeight.bold)),
                content: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      "∙ Tekan 1x untuk edit",
                    ),
                    Text(
                      "∙ Tekan lama untuk memilih alamat korespondensi",
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height/37,),
                    Text("* Tekan icon pada pojok kanan atas untuk melihat kembali",style: TextStyle(fontSize: 12, color: Colors.grey))
                  ],
                ),
                actions: <Widget>[
                  FlatButton(
                      onPressed: (){
                        Navigator.pop(context);
                        // _updateStatusShowDialogSimilarity();
                      },
                      child: Text(
                          "CLOSE",
                          style: TextStyle(
                              color: primaryOrange,
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 1.25
                          )
                      )
                  )
                ],
              ),
            );
          }
        );
    }
  }

  void setCorrespondence(int index){
    for(int i=0; i < this._listAlamatKorespondensi.length; i++){
      if(this._listAlamatKorespondensi[i].isCorrespondence){
        this._listAlamatKorespondensi[i].isCorrespondence = false;
      }
    }
    this._listAlamatKorespondensi[index].isCorrespondence = true;
    notifyListeners();
  }

  void moreDialog(BuildContext context, index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans"
            ),
            child: AlertDialog(
              title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.check,
                          color: Colors.green,
                          size: 22.0,
                        ),
                        SizedBox(width: 12.0),
                        Expanded(
                          child: GestureDetector(
                            onTap: (){
                              selectedIndex = index;
                              _controllerAddress.clear();
                              setAddress(listAlamatKorespondensi[index]);
                              setCorrespondence(index);
                              Navigator.pop(context);
                              Navigator.pop(context);
                            },
                            child: Text(
                              "Pilih sebagai Alamat Korespondensi",
                              style: TextStyle(fontSize: 14.0),
                            ),
                          ),
                        )
                      ]
                  ),
                  SizedBox(height: 12.0),
                  listAlamatKorespondensi[index].jenisAlamatModel.KODE != "03" ?
                      // ? listAlamatKorespondensi[index].isSameWithIdentity
                      // ? SizedBox()
                      // :
                  Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.delete,
                          color: Colors.red,
                          size: 22.0,
                        ),
                        SizedBox(
                          width: 12.0,
                        ),
                        GestureDetector(
                          onTap: (){
                            Navigator.pop(context);
                            deleteAlamatKorespondensi(context, index);
                          },
                          child: Text(
                            "Hapus",
                            style: TextStyle(fontSize: 14.0, color: Colors.red),
                          ),
                        )
                      ]
                  )
                      : SizedBox(),
                ],
              ),
              actions: <Widget>[
                FlatButton(
                    onPressed: (){
                      Navigator.pop(context);
                      // _updateStatusShowDialogSimilarity();
                    },
                    child: Text(
                        "CLOSE",
                        style: TextStyle(
                            color: primaryOrange,
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 1.25
                        )
                    )
                )
              ],
            ),
          );
        }
    );
  }

  GlobalKey<FormState> get keyForm => _key;

  bool get flag => _flag;

  set flag(bool value) {
    _flag = value;
    notifyListeners();
  }

  void check(BuildContext context) {
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
      Navigator.pop(context);
    } else {
      flag = false;
      autoValidate = true;
    }
  }

  Future<bool> onBackPress() async{
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
    } else {
      flag = false;
      autoValidate = true;
    }
    return true;
  }

  // Future<void> clearDataAlamat() async {
  void clearDataAlamat() {
    this._autoValidate = false;
    this._flag = false;
    this._selectedIndex = -1;
    this._controllerAddress.clear();
    this._controllerAddressType.clear();
    this._controllerRT.clear();
    this._controllerRW.clear();
    this._controllerKelurahan.clear();
    this._controllerKecamatan.clear();
    this._controllerKota.clear();
    this._controllerProvinsi.clear();
    this._controllerPostalCode.clear();
    this._controllerTelepon1Area.clear();
    this._controllerTelepon1.clear();
    this._controllerTelepon2Area.clear();
    this._controllerTelepon2.clear();
    this._controllerFaxArea.clear();
    this._controllerFax.clear();
    this._listAlamatKorespondensi = [];
  }

  void saveToSQLite(String type) async{
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    List<MS2CustAddrModel> _listAddress = [];
    for(int i=0; i<_listAlamatKorespondensi.length; i++){
      _listAddress.add(MS2CustAddrModel(
          "123",
          _listAlamatKorespondensi[i].addressID,
          _listAlamatKorespondensi[i].foreignBusinessID,
          null,
          _listAlamatKorespondensi[i].isCorrespondence ? "1" : "0",
          type,
          _listAlamatKorespondensi[i].address,
          _listAlamatKorespondensi[i].rt,
          null,
          _listAlamatKorespondensi[i].rw,
          null,
          _listAlamatKorespondensi[i].kelurahanModel.PROV_ID,
          _listAlamatKorespondensi[i].kelurahanModel.PROV_NAME,
          _listAlamatKorespondensi[i].kelurahanModel.KABKOT_ID,
          _listAlamatKorespondensi[i].kelurahanModel.KABKOT_NAME,
          _listAlamatKorespondensi[i].kelurahanModel.KEC_ID,
          _listAlamatKorespondensi[i].kelurahanModel.KEC_NAME,
          _listAlamatKorespondensi[i].kelurahanModel.KEL_ID,
          _listAlamatKorespondensi[i].kelurahanModel.KEL_NAME,
          _listAlamatKorespondensi[i].kelurahanModel.ZIPCODE,
          null,
          _listAlamatKorespondensi[i].phone1,
          _listAlamatKorespondensi[i].phoneArea1,
          _listAlamatKorespondensi[i].phone2,
          _listAlamatKorespondensi[i].phoneArea2,
          _listAlamatKorespondensi[i].fax,
          _listAlamatKorespondensi[i].faxArea,
          _listAlamatKorespondensi[i].jenisAlamatModel.KODE,
          _listAlamatKorespondensi[i].jenisAlamatModel.DESKRIPSI,
          _listAlamatKorespondensi[i].addressLatLong != null ? _listAlamatKorespondensi[i].addressLatLong['latitude'].toString(): null,
          _listAlamatKorespondensi[i].addressLatLong != null ? _listAlamatKorespondensi[i].addressLatLong['longitude'].toString(): null,
          _listAlamatKorespondensi[i].addressLatLong != null ? _listAlamatKorespondensi[i].addressLatLong['address'].toString(): null,
          DateTime.now().toString(),
          _preferences.getString("username"),
          null,
          null,
          _listAlamatKorespondensi[i].active,
          _listAlamatKorespondensi[i].isAddressTypeDakor ? "1" : "0",
          _listAlamatKorespondensi[i].isAddressDakor ? "1" : "0",
          _listAlamatKorespondensi[i].isRTDakor ? "1" : "0",
          _listAlamatKorespondensi[i].isRWDakor ? "1" : "0",
          _listAlamatKorespondensi[i].isKelurahanDakor ? "1" : "0",
          _listAlamatKorespondensi[i].isKecamatanDakor ? "1" : "0",
          _listAlamatKorespondensi[i].isKabKotDakor ? "1" : "0",
          _listAlamatKorespondensi[i].isProvinsiDakor ? "1" : "0",
          _listAlamatKorespondensi[i].isKodePosDakor ? "1" : "0",
          _listAlamatKorespondensi[i].isTelp1AreaDakor ? "1" : "0",
          _listAlamatKorespondensi[i].isTelp1Dakor ? "1" : "0",
          _listAlamatKorespondensi[i].isTelp2AreaDakor ? "1" : "0",
          _listAlamatKorespondensi[i].isTelp2Dakor ? "1" : "0",
          _listAlamatKorespondensi[i].isFaxAreaDakor ? "1" : "0",
          _listAlamatKorespondensi[i].isFaxDakor ? "1" : "0",
          _listAlamatKorespondensi[i].isDataLatLongDakor ? "1" : "0",
      ));
    }
    _dbHelper.insertMS2CustAddr(_listAddress);
  }

  Future<bool> deleteSQLite(String type) async{
    return await _dbHelper.deleteMS2CustAddr(type);
  }

  void setDataSQLite(BuildContext context) async{
    debugPrint("SET_ADDRESS_COM");
    var _check = await _dbHelper.selectMS2CustAddr("10");
    debugPrint("SET_ADDRESS_COM_CHECK $_check");
    if(this._listAlamatKorespondensi.isNotEmpty){
      debugPrint("SET_ADDRESS_COM_IF");
    }
    else{
      if(_check.isNotEmpty){
        debugPrint("SET_ADDRESS_COM_ELSE");
        for(int i=0; i<_check.length; i++){
          var jenisAlamatModel = JenisAlamatModel(_check[i]['addr_type'], _check[i]['addr_desc']);

          var kelurahanModel = KelurahanModel(_check[i]['kelurahan'], _check[i]['kelurahan_desc'],
              _check[i]['kecamatan_desc'], _check[i]['kabkot_desc'], _check[i]['provinsi_desc'], _check[i]['zip_code'],
              _check[i]['kecamatan'], _check[i]['kabkot'], _check[i]['provinsi']);

          var _addressFromMap = {
            "address": _check[i]['address_from_map'],
            "latitude": _check[i]['latitude'],
            "longitude": _check[i]['longitude']
          };
          var _koresponden = _check[i]['koresponden'].toString().toLowerCase();
          _listAlamatKorespondensi.add(
              AddressModelCompany(
                jenisAlamatModel,
                _check[i]['addressID'],
                _check[i]['foreignBusinessID'],
                null,
                kelurahanModel,
                _check[i]['address'].toString(),
                _check[i]['rt'].toString(),
                _check[i]['rw'].toString(),
                _check[i]['phone1_area'].toString(),
                _check[i]['phone1'].toString(),
                _check[i]['phone_2_area'].toString(),
                _check[i]['phone_2'].toString(),
                _check[i]['fax_area'].toString(),
                _check[i]['fax'].toString(),
                false,
                _addressFromMap,
                _koresponden == '1' ? true : false,
                _check[i]['active'],
                _check[i]['edit_addr_type'] == "1" ||
                _check[i]['edit_address'] == "1" ||
                _check[i]['edit_rt'] == "1" ||
                _check[i]['edit_rw'] == "1" ||
                _check[i]['edit_kelurahan'] == "1" ||
                _check[i]['edit_kecamatan'] == "1" ||
                _check[i]['edit_kabkot'] == "1" ||
                _check[i]['edit_provinsi'] == "1" ||
                _check[i]['edit_zip_code'] == "1" ||
                _check[i]['edit_phone1_area'] == "1" ||
                _check[i]['edit_phone1'] == "1" ||
                _check[i]['edit_phone_2_area'] == "1" ||
                _check[i]['edit_phone_2'] == "1" ||
                _check[i]['edit_fax_area'] == "1" ||
                _check[i]['edit_fax'] == "1" ||
                _check[i]['edit_address_from_map'] == "1" ? true : false,
                _check[i]['edit_addr_type'] == "1",
                _check[i]['edit_address'] == "1",
                _check[i]['edit_rt'] == "1",
                _check[i]['edit_rw'] == "1",
                _check[i]['edit_kelurahan'] == "1",
                _check[i]['edit_kecamatan'] == "1",
                _check[i]['edit_kabkot'] == "1",
                _check[i]['edit_provinsi'] == "1",
                _check[i]['edit_zip_code'] == "1",
                _check[i]['edit_phone1_area'] == "1",
                _check[i]['edit_phone1'] == "1",
                _check[i]['edit_phone_2_area'] == "1",
                _check[i]['edit_phone_2'] == "1",
                _check[i]['edit_fax_area'] == "1",
                _check[i]['edit_fax'] == "1",
                _check[i]['edit_address_from_map'] == "1",
              )
          );
          if(_check[i]['koresponden'] == "1"){
            selectedIndex = i;
            _controllerAddress.clear();
            setAddress(listAlamatKorespondensi[i]);
            setCorrespondence(i);
          }
        }
      }
    }
    notifyListeners();
  }

}
