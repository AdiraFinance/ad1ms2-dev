import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';

class SearchProfesiChangeNotifier with ChangeNotifier{
  bool _showClear = false;
  TextEditingController _controllerSearch = TextEditingController();
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  TextEditingController get controllerSearch => _controllerSearch;
  List<ProfessionTypeModel> _listProfessionTypes = [];
  List<ProfessionTypeModel> _listProfessionTypesTemp = [];

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  void _showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  UnmodifiableListView<ProfessionTypeModel> get listProfessionTypes {
    return UnmodifiableListView(this._listProfessionTypes);
  }

  UnmodifiableListView<ProfessionTypeModel> get listProfessionTypesTemp {
    return UnmodifiableListView(this._listProfessionTypesTemp);
  }

  void getDataProfessionType() async{
    this._loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);
    var storage = FlutterSecureStorage();
    String _fieldProfesi = await storage.read(key: "FieldJenisProfesi");
    try{
      final _response = await _http.get(
        "${BaseUrl.urlGeneral}$_fieldProfesi",
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
        //   "${urlPublic}program-kegiatan/get_program",
      ).timeout(Duration(seconds: 30));
      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        print("body profesi = $_result");
        if(_result['data'].isEmpty){
          _showSnackBar("Program tidak ditemukan");
        }
        else{
          for(int i=0; i <_result['data'].length; i++){
            this._listProfessionTypes.add(
                ProfessionTypeModel(_result['data'][i]['kode'], _result['data'][i]['deskripsi'])
            );
          }
        }
      }
      else{
        _showSnackBar("Error response status ${_response.statusCode}");
      }
    }
    catch(e){
      _showSnackBar(e.toString());
      this._loadData = false;
    }
    this._loadData = false;
    notifyListeners();
  }

  void searchProfesi(String query) {
    if(query.length < 3) {
      _showSnackBar("Input minimal 3 karakter");
    } else {
      _listProfessionTypesTemp.clear();
      if (query.isEmpty) {
        return;
      }

      _listProfessionTypes.forEach((dataProgram) {
        if (dataProgram.id.contains(query) || dataProgram.desc.contains(query)) {
          _listProfessionTypesTemp.add(dataProgram);
        }
      });
    }
    notifyListeners();
  }

  void clearSearchTemp() {
    _listProfessionTypesTemp.clear();
  }
}