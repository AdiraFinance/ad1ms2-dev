import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/group_object_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_company_rincian_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SearchGroupObjectChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  TextEditingController _controllerSearch = TextEditingController();
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<GroupObjectModel> _listGroupObject = [
    // GroupObjectModel("001", "MOTOR"),
    // GroupObjectModel("002", "MOBIL"),
    // GroupObjectModel("003", "DURABLE"),
//    GroupObjectModel("004", "PROPERTY"),
//    GroupObjectModel("005", "PAKET"),
//     GroupObjectModel("007", "JASA"),
//    GroupObjectModel("99", "OTHER"),
  ];
  DbHelper _dbHelper = DbHelper();

  List<GroupObjectModel> _listGroupObjectTemp = [];

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<GroupObjectModel> get listGroupObject {
    return UnmodifiableListView(this._listGroupObject);
  }

  UnmodifiableListView<GroupObjectModel> get listGroupObjectTemp {
    return UnmodifiableListView(this._listGroupObjectTemp);
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void getGroupObject(BuildContext context, String flag, String unitColla) async{
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
    var _providerObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    List _dataCL = await _dbHelper.selectMS2LME();
    this._listGroupObject.clear();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    if(unitColla == "1") {
      // Inf Unit Object
      var _body = jsonEncode({
        "P_CUST_TYPE" : "$flag", //"$flag",
        "P_FLAG_NPWP" : flag == "COM" ? "${Provider.of<FormMCompanyRincianChangeNotifier>(context,listen: false).radioValueIsHaveNPWP}" : "${Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false).radioValueIsHaveNPWP}",
        "P_OCCUPATION_DETAIL_ID" : flag == "COM" ? "" : "${_providerFoto.occupationSelected.KODE}" ,
        "P_OJK_BUSS_DETAIL_ID" : flag == "COM" ? "${_providerObjectUnit.businessActivitiesTypeModelSelected.id}" : "${_providerFoto.jenisKegiatanUsahaSelected.id}",
        "P_OJK_BUSS_ID" : flag == "COM" ? "${_providerObjectUnit.businessActivitiesModelSelected.id}" : "${_providerFoto.kegiatanUsahaSelected.id}"
      });
      debugPrint("body groupObject: $_body");

      var storage = FlutterSecureStorage();
      String _fieldGrupObjek = await storage.read(key: "FieldGrupObjek");
      final _response = await _http.post(
          "${BaseUrl.urlGeneral}$_fieldGrupObjek",
          // "${urlPublic}api/parameter/get-group-object",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      );

      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        final _data = _result['data'];
        if(_data.isEmpty){
          showSnackBar("Group Objek tidak ditemukan");
          loadData = false;
        }
        else{
          for(int i=0; i <_data.length; i++){
            if(_preferences.getString("jenis_penawaran") == "001") {
              if(_dataCL[0]['portfolio'] == "Jasa/Durable") {
                if(_data[i]['KODE'] == "003" || _data[i]['KODE'] == "007") {
                  this._listGroupObject.add(
                      GroupObjectModel(_data[i]['KODE'], _data[i]['DESKRIPSI'])
                  );
                }
              } else if(_dataCL[0]['portfolio'] == "MOTOR") {
                if(_data[i]['KODE'] == "001") {
                  this._listGroupObject.add(
                      GroupObjectModel(_data[i]['KODE'], _data[i]['DESKRIPSI'])
                  );
                }
              } else if(_dataCL[0]['portfolio'] == "MOBIL") {
                if(_data[i]['KODE'] == "002") {
                  this._listGroupObject.add(
                      GroupObjectModel(_data[i]['KODE'], _data[i]['DESKRIPSI'])
                  );
                }
              } else if(_dataCL[0]['portfolio'] == "MPL") {
                if(_data[i]['KODE'] != "001" && _data[i]['KODE'] != "002" && _data[i]['KODE'] != "003") {
                  this._listGroupObject.add(
                      GroupObjectModel(_data[i]['KODE'], _data[i]['DESKRIPSI'])
                  );
                }
              }
            }
            else if(_preferences.getString("jenis_penawaran") == "002") {
              if(_dataCL[0]['portfolio'] == "Jasa/Durable") {
                if(_dataCL[0]['pencairan_ke'] == "1" && _dataCL[0]['opsi_multidisburse'] == "2") {
                  if(_data[i]['KODE'] == "007") {
                    this._listGroupObject.add(
                        GroupObjectModel(_data[i]['KODE'], _data[i]['DESKRIPSI'])
                    );
                  }
                } else {
                  if(_data[i]['KODE'] == "003" || _data[i]['KODE'] == "007") {
                    this._listGroupObject.add(
                        GroupObjectModel(_data[i]['KODE'], _data[i]['DESKRIPSI'])
                    );
                  }
                }
              } else if(_dataCL[0]['portfolio'] == "MOTOR") {
                if(_data[i]['KODE'] == "001") {
                  this._listGroupObject.add(
                      GroupObjectModel(_data[i]['KODE'], _data[i]['DESKRIPSI'])
                  );
                }
              } else if(_dataCL[0]['portfolio'] == "MOBIL") {
                if(_data[i]['KODE'] == "002") {
                  this._listGroupObject.add(
                      GroupObjectModel(_data[i]['KODE'], _data[i]['DESKRIPSI'])
                  );
                }
              } else if(_dataCL[0]['portfolio'] == "MPL") {
                if(_data[i]['KODE'] == "007") {
                  this._listGroupObject.add(
                      GroupObjectModel(_data[i]['KODE'], _data[i]['DESKRIPSI'])
                  );
                }
              }
            }
            else {
              this._listGroupObject.add(
                  GroupObjectModel(_data[i]['KODE'], _data[i]['DESKRIPSI'])
              );
            }
          }
          loadData = false;
        }
      }
      else{
        showSnackBar("Error response status ${_response.statusCode}");
        this._loadData = false;
      }
    }
    else {
      // Inf Collateral
      var storage = FlutterSecureStorage();
      String _fieldGrupObjek = await storage.read(key: "FieldGroupObjectJaminan");
      final _response = await _http.get(
          "${BaseUrl.urlGeneral}$_fieldGrupObjek",
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      );

      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        final _data = _result['data'];
        if(_data.isEmpty){
          showSnackBar("Group Objek tidak ditemukan");
          loadData = false;
        }
        else{
          for(int i=0; i <_data.length; i++){
            this._listGroupObject.add(
                GroupObjectModel(_data[i]['KODE'], _data[i]['DESKRIPSI'])
            );
          }
          loadData = false;
        }
      }
      else{
        showSnackBar("Error response status ${_response.statusCode}");
        this._loadData = false;
      }
    }
    notifyListeners();
  }

  void searchGroupObject(String query) {
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    } else {
      _listGroupObjectTemp.clear();
      if (query.isEmpty) {
        return;
      }

      _listGroupObject.forEach((dataGroupObject) {
        if (dataGroupObject.KODE.contains(query) || dataGroupObject.DESKRIPSI.contains(query)) {
          _listGroupObjectTemp.add(dataGroupObject);
        }
      });
    }
    notifyListeners();
  }

  void clearSearchTemp() {
    _listGroupObjectTemp.clear();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }
}
