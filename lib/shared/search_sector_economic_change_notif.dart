import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';

import '../main.dart';

class SearchSectorEconomicChangeNotif with ChangeNotifier {
  bool _showClear = false;
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController _controllerSearch = TextEditingController();
  // List<SectorEconomicModel> _listSectorEconomic = [
  //   SectorEconomicModel("01", "PERTANIAN"),
  //   SectorEconomicModel("02", "PERKEBUNAN"),
  //   SectorEconomicModel("03", "PETERNAKAN"),
  //   SectorEconomicModel("04", "KEHUTANAN"),
  //   SectorEconomicModel("05", "PERIKANAN"),
  //   SectorEconomicModel("06", "PERTAMBANGAN")
  // ];

  List<SectorEconomicModel> _listSectorEconomic = [];
  List<SectorEconomicModel> _listSectorEconomicTemp = [];

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<SectorEconomicModel> get listSectorEconomic {
    return UnmodifiableListView(this._listSectorEconomic);
  }

  UnmodifiableListView<SectorEconomicModel> get listSectorEconomicTemp {
    return UnmodifiableListView(this._listSectorEconomicTemp);
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
    notifyListeners();
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void getSectorEconomic() async{
    this._listSectorEconomic.clear();
    this._loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var storage = FlutterSecureStorage();
    String _fieldSektorEkonomi = await storage.read(key: "FieldSektorEkonomi");
    final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_fieldSektorEkonomi",
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
        // "${urlPublic}api/occupation/get-sektor-ekonomi"
    );

    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      if(_result.isNotEmpty){
        for(int i = 0 ; i < _result.length ; i++) {
          _listSectorEconomic.add(SectorEconomicModel(_result[i]['KODE'], _result[i]['DESKRIPSI']));
        }
      }
      this._loadData = false;
    } else {
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  void searchSectorEconomic(String query) {
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    } else {
      _listSectorEconomicTemp.clear();
      if (query.isEmpty) {
        return;
      }

      _listSectorEconomic.forEach((dataSectorEconomic) {
        if (dataSectorEconomic.KODE.contains(query) || dataSectorEconomic.DESKRIPSI.contains(query)) {
          _listSectorEconomicTemp.add(dataSectorEconomic);
        }
      });
    }
    notifyListeners();
  }

  void clearSearchTemp() {
    _listSectorEconomicTemp.clear();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }
}
