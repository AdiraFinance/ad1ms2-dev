import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/models/task_list_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';

class HomeChangeNotifier with ChangeNotifier{
  List<TaskListModel> _taskList = [];
  String _username;
  String _fullName;
  var storage = FlutterSecureStorage();

  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<TaskListModel> get taskList => _taskList;

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
    notifyListeners();
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  String get username => _username;

  set username(String value) {
    this._username = value;
    notifyListeners();
  }

  String get fullName => _fullName;

  set fullName(String value) {
    this._fullName = value;
    notifyListeners();
  }

  // void navigateForm(String typeFormId, BuildContext context,String orderNo,String orderDate,String custName) async{
  //   SharedPreferences _preferences = await SharedPreferences.getInstance();
  //   _preferences.setString("order_no", orderNo);
  //   _preferences.setString("order_date",orderDate);
  //   _preferences.setString("cust_name",custName);
  //   _preferences.setString("last_known_state",typeFormId);
  //   if(typeFormId == "SA"){
  //     Navigator.push(context, MaterialPageRoute(builder: (context) => SAJobMayor()));
  //   }
  //   else if(typeFormId == "AOS"){
  //     Navigator.push(context, MaterialPageRoute(builder: (context) => FormAOS()));
  //   }
  //   else if(typeFormId == "IA"){
  //     Navigator.push(context, MaterialPageRoute(builder: (context) => FormIA()));
  //   }
  //   else{
  //     Navigator.push(context, MaterialPageRoute(builder: (context) => FormMParent(typeFormId: typeFormId)));
  //   }
  // }

  Future<String> getTaskList() async {
    print("dari tombol back form");
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    _username = _preferences.getString("username");
    _fullName = _preferences.getString("fullname");
    print("isMayor ${_preferences.getString("is_mayor") == "0"} ${_preferences.getString("is_mayor")}");
    this._taskList.clear();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    try {
      var _body = jsonEncode({
        "P_NIK": _preferences.getString("username"),
        // "10065901"
      });
      String _urlTaskList = await storage.read(key: "TaskList");
      print("${BaseUrl.urlGeneral}$_urlTaskList");
      final _response = await _http.post(
        "${BaseUrl.urlGeneral}$_urlTaskList",
        // "https://103.110.89.34/public/ms2dev/api/tasklist/get-tasklist-ms2",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));
      print(_response.statusCode);

      if(_response.statusCode == 200){
        List _result = jsonDecode(_response.body);
        if(_result.isEmpty) {
          loadData = false;
          return "isEmpty";
          // showSnackBar("Task List tidak ditemukan");
          // this._loadData = false;
        } else {
          for(int i=0; i < _result.length; i++){
            var _date = _result[i]['ORDER_DATE'] != null ? DateTime.parse(_result[i]['ORDER_DATE'].toString().replaceAll("T", " ")) : DateTime.now();
            var _dateMS2 = _result[i]['MS2PROCESS_DATE'] != null ? DateTime.parse(_result[i]['MS2PROCESS_DATE'].toString().replaceAll("T", " ")) : DateTime.now();
            this._taskList.add(
              TaskListModel(
                _result[i]['ORDER_NO'],
                _date,
                _result[i]['CUST_NAME'],
                _result[i]['CUST_TYPE'],
                _result[i]['LAST_KNOWN_STATE'],
                _result[i]['LAST_KNOWN_HANDLED_BY'],
                _result[i]['IDX'],
                _result[i]['SOURCE_APPLICATION'],
                _result[i]['PRIORITY'],
                _result[i]['OBLIGOR_ID'],
                _result[i]['STATUS_AORO'],
                _dateMS2,
                _result[i]['JENIS_PENAWARAN'],
                _result[i]['NO_REFERENSI'],
                _result[i]['OPSI_MULTIDISBURSE'],
              )
            );
          }
          loadData = false;
          return "isNotEmpty";
          // this._loadData = false;
        }
      } else {
        loadData = false;
        throw("response status code ${_response.statusCode}");
        // showSnackBar("Error response status ${_response.statusCode}");
        // this._loadData = false;
      }
    }
    on TimeoutException catch(_){
      loadData = false;
      throw("Request timeout");
    }
    catch(e) {
      loadData = false;
      throw("${e.toString()}");
      // showSnackBar("Error ${e.toString()}");
      // this._loadData = false;
    }

  }

  // void showSnackBar(String text){
  //   this._scaffoldKey.currentState.showSnackBar(new SnackBar(
  //       content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  // }
}