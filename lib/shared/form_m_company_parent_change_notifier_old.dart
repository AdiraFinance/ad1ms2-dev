import 'package:ad1ms2_dev/shared/form_m_company_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_manajemen_pic_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_manajemen_pic_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_pemegang_saham_kelembagaan_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/add_pemegang_saham_lembaga_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_pemegang_saham_pribadi_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_pendapatan_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_penjamin_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_rincian_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/pemegang_saham_pribadi_change_notifier.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FormMCompanyParentChangeNotifierOld with ChangeNotifier {
  List<GlobalKey<FormState>> formKeys = [
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
  ];

  VoidCallback _onStepContinue;
  VoidCallback _onStepCancel;

  int _currentStep = 0;
  bool _flagStatusForm1 = false;
  bool _flagStatusForm2 = false;
  bool _flagStatusForm3 = false;
  bool _flagStatusForm4 = false;
  bool _flagStatusForm5 = false;
  bool _flagStatusForm6 = false;
  bool _flagStatusForm7 = false;
  bool _flagStatusForm8 = false;
  bool _flagStatusForm9 = false;
  bool _flagStatusForm10 = false;
  bool _flagStatusForm11 = false;
  bool _enableTapForm2 = false;
  bool _enableTapForm3 = false;
  bool _enableTapForm4 = false;
  bool _enableTapForm5 = false;
  bool _enableTapForm6 = false;
  bool _enableTapForm7 = false;
  bool _enableTapForm8 = false;
  bool _enableTapForm9 = false;
  bool _enableTapForm10 = false;
  bool _enableTapForm11 = false;

  int get currentStep => _currentStep;

  set currentStep(int value) {
    this._currentStep = value;
    notifyListeners();
  }

  VoidCallback get onStepCancel => _onStepCancel;

  set onStepCancel(VoidCallback value) {
    this._onStepCancel = value;
//    notifyListeners();
  }

  VoidCallback get onStepContinue => _onStepContinue;

  set onStepContinue(VoidCallback value) {
    this._onStepContinue = value;
//    notifyListeners();
  }

  bool get flagStatusForm1 => _flagStatusForm1;

  set flagStatusForm1(bool value) {
    this._flagStatusForm1 = value;
    notifyListeners();
  }

  bool get flagStatusForm2 => _flagStatusForm2;

  set flagStatusForm2(bool value) {
    this._flagStatusForm2 = value;
    notifyListeners();
  }

  bool get enableTapForm2 => _enableTapForm2;

  set enableTapForm2(bool value) {
    this._enableTapForm2 = value;
    notifyListeners();
  }

  bool get flagStatusForm3 => _flagStatusForm3;

  set flagStatusForm3(bool value) {
    this._flagStatusForm3 = value;
    notifyListeners();
  }

  bool get enableTapForm3 => _enableTapForm3;

  set enableTapForm3(bool value) {
    this._enableTapForm3 = value;
    notifyListeners();
  }

  bool get flagStatusForm4 => _flagStatusForm4;

  set flagStatusForm4(bool value) {
    this._flagStatusForm4 = value;
    notifyListeners();
  }

  bool get enableTapForm4 => _enableTapForm4;

  set enableTapForm4(bool value) {
    this._enableTapForm4 = value;
    notifyListeners();
  }

  bool get flagStatusForm5 => _flagStatusForm5;

  set flagStatusForm5(bool value) {
    this._flagStatusForm5 = value;
    notifyListeners();
  }

  bool get enableTapForm5 => _enableTapForm5;

  set enableTapForm5(bool value) {
    this._enableTapForm5 = value;
    notifyListeners();
  }

  bool get flagStatusForm6 => _flagStatusForm6;

  set flagStatusForm6(bool value) {
    this._flagStatusForm6 = value;
    notifyListeners();
  }

  bool get enableTapForm6 => _enableTapForm6;

  set enableTapForm6(bool value) {
    this._enableTapForm6 = value;
    notifyListeners();
  }

  bool get flagStatusForm7 => _flagStatusForm7;

  set flagStatusForm7(bool value) {
    this._flagStatusForm7 = value;
    notifyListeners();
  }

  bool get enableTapForm7 => _enableTapForm7;

  set enableTapForm7(bool value) {
    this._enableTapForm7 = value;
    notifyListeners();
  }

  bool get flagStatusForm8 => _flagStatusForm8;

  set flagStatusForm8(bool value) {
    this._flagStatusForm8 = value;
    notifyListeners();
  }

  bool get enableTapForm8 => _enableTapForm8;

  set enableTapForm8(bool value) {
    this._enableTapForm8 = value;
    notifyListeners();
  }

  bool get flagStatusForm9 => _flagStatusForm9;

  set flagStatusForm9(bool value) {
    this._flagStatusForm9 = value;
    notifyListeners();
  }

  bool get enableTapForm9 => _enableTapForm9;

  set enableTapForm9(bool value) {
    this._enableTapForm9 = value;
    notifyListeners();
  }

  bool get flagStatusForm10 => _flagStatusForm10;

  set flagStatusForm10(bool value) {
    this._flagStatusForm10 = value;
    notifyListeners();
  }

  bool get enableTapForm10 => _enableTapForm10;

  set enableTapForm10(bool value) {
    this._enableTapForm10 = value;
    notifyListeners();
  }

  bool get flagStatusForm11 => _flagStatusForm11;

  set flagStatusForm11(bool value) {
    this._flagStatusForm10 = value;
    notifyListeners();
  }

  bool get enableTapForm11 => _enableTapForm11;

  set enableTapForm11(bool value) {
    this._enableTapForm11 = value;
    notifyListeners();
  }

  void check(BuildContext context) {
    final form = formKeys[this._currentStep].currentState;
    var _providerFormMCompanyRincianChangeNotif = Provider.of<FormMCompanyRincianChangeNotifier>(context, listen: false);
    var _providerFormMCompanyAlamatChangeNotif = Provider.of<FormMCompanyAlamatChangeNotifier>(context, listen: false);
    var _providerFormMCompanyPendapatanChangeNotif = Provider.of<FormMCompanyPendapatanChangeNotifier>(context, listen: false);
    var _providerFormMCompanyPenjaminChangeNotif = Provider.of<FormMCompanyPenjaminChangeNotifier>(context, listen: false);
    var _providerFormMCompanyManajemenPICChangeNotif = Provider.of<FormMCompanyManajemenPICChangeNotifier>(context, listen: false);
    var _providerFormMCompanyManajemenPICAlamatChangeNotif = Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: false);
    var _providerFormMCompanyPemegangSahamPribadiChangeNotif = Provider.of<PemegangSahamPribadiChangeNotifier>(context, listen: false);
    var _providerFormMCompanyPemegangSahamPribadiAlamatChangeNotif = Provider.of<FormMCompanyPemegangSahamPribadiAlamatChangeNotifier>(context, listen: false);
    var _providerFormMCompanyPemegangSahamKelembagaanChangeNotif = Provider.of<AddPemegangSahamLembagaChangeNotif>(context, listen: false);
    var _providerFormMCompanyPemegangSahamKelembagaanAlamatChangeNotif = Provider.of<FormMCompanyPemegangSahamKelembagaanAlamatChangeNotifier>(context, listen: false);

    if (this._currentStep == 0) {
      if (form.validate()) {
        enableTapForm2 = true;
        flagStatusForm1 = true;
        this._onStepContinue();
      } else {
        _providerFormMCompanyRincianChangeNotif.autoValidate = true;
      }
    } else if (this._currentStep == 1) {
      if (form.validate()) {
        enableTapForm3 = true;
        flagStatusForm2 = true;
        this._onStepContinue();
      } else {
        _providerFormMCompanyAlamatChangeNotif.autoValidate = true;
      }
    } else if (this._currentStep == 2) {
      if (form.validate()) {
        enableTapForm4 = true;
        flagStatusForm3 = true;
        this._onStepContinue();
      } else {
        _providerFormMCompanyPendapatanChangeNotif.autoValidate = true;
      }
    } else if (this._currentStep == 3) {
      if (form.validate()) {
        enableTapForm5 = true;
        flagStatusForm4 = true;
        this._onStepContinue();
      } else {
        _providerFormMCompanyPenjaminChangeNotif.autoValidate = true;
      }
    } else if (this._currentStep == 4) {
      if (form.validate()) {
        enableTapForm6 = true;
        flagStatusForm5 = true;
        this._onStepContinue();
      } else {
        _providerFormMCompanyManajemenPICChangeNotif.autoValidate = true;
      }
    } else if (this._currentStep == 5) {
      if (form.validate()) {
        enableTapForm7 = true;
        flagStatusForm6 = true;
        this._onStepContinue();
      } else {
        _providerFormMCompanyManajemenPICAlamatChangeNotif.autoValidate = true;
      }
    } else if (this._currentStep == 6) {
      if (form.validate()) {
        enableTapForm8 = true;
        flagStatusForm7 = true;
        this._onStepContinue();
      } else {
        _providerFormMCompanyPemegangSahamPribadiChangeNotif.autoValidate = true;
      }
    } else if (this._currentStep == 7) {
      if (form.validate()) {
        enableTapForm9 = true;
        flagStatusForm8 = true;
        this._onStepContinue();
      } else {
        _providerFormMCompanyPemegangSahamPribadiAlamatChangeNotif.autoValidate = true;
      }
    } else if (this._currentStep == 8) {
      if (form.validate()) {
        enableTapForm10 = true;
        flagStatusForm9 = true;
        this._onStepContinue();
      } else {
        _providerFormMCompanyPemegangSahamKelembagaanChangeNotif.autoValidate = true;
      }
    } else if (this._currentStep == 9) {
      if (form.validate()) {
        enableTapForm11 = true;
        flagStatusForm10 = true;
        this._onStepContinue();
      } else {
        _providerFormMCompanyPemegangSahamKelembagaanAlamatChangeNotif.autoValidate = true;
      }
    }
  }

  void onTapCheck(int step, BuildContext context) {
    final form = formKeys[this._currentStep].currentState;
    var _provider =
        Provider.of<FormMFotoChangeNotifier>(context, listen: false);
    if (this._currentStep == 0) {
      if (step < this._currentStep) {
        currentStep = step;
      } else {
        currentStep = step;
      }
    } else if (this._currentStep == 1) {
      if (step < this._currentStep) {
        currentStep = step;
      } else {
        currentStep = step;
      }
    } else if (this._currentStep == 2) {
      if (step < this._currentStep) {
        currentStep = step;
      } else {
        currentStep = step;
      }
    } else if (this._currentStep == 3) {
      if (step < this._currentStep) {
        currentStep = step;
      } else {
        currentStep = step;
      }
    } else if (this._currentStep == 4) {
      if (step < this._currentStep) {
        currentStep = step;
      } else {
        currentStep = step;
      }
    } else if (this._currentStep == 5) {
      if (step < this._currentStep) {
        currentStep = step;
      } else {
        currentStep = step;
      }
    } else if (this._currentStep == 6) {
      if (step < this._currentStep) {
        currentStep = step;
      } else {
        currentStep = step;
      }
    } else if (this._currentStep ==  7) {
      if (step < this._currentStep) {
        currentStep = step;
      } else {
        currentStep = step;
      }
    } else if (this._currentStep ==  8) {
      if (step < this._currentStep) {
        currentStep = step;
      } else {
        currentStep = step;
      }
    } else if (this._currentStep ==  9) {
      if (step < this._currentStep) {
        currentStep = step;
      } else {
        currentStep = step;
      }
    }
  }
}
