import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/source_order_name_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SearchSourceOrderNameSPGChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  bool _loadData = false;
  TextEditingController _controllerSearch = TextEditingController();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<SourceOrderNameModel> _listSourceOrderNameSPG = [];
  List<SourceOrderNameModel> _listSourceOrderNameSPGTemp = [];

  TextEditingController get controllerSearch => _controllerSearch;

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void  changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<SourceOrderNameModel> get listSourceOrderNameSPG {
    return UnmodifiableListView(this._listSourceOrderNameSPG);
  }

  UnmodifiableListView<SourceOrderNameModel> get listSourceOrderNameSPGTemp {
    return UnmodifiableListView(this._listSourceOrderNameSPGTemp);
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  void getSourceOrderNameSPG() async{
    this._listSourceOrderNameSPG.clear();
    this._listSourceOrderNameSPGTemp.clear();
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
    var storage = FlutterSecureStorage();

    String _namaSumberOrderSPG = await storage.read(key: "NamaSumberOrderSPG");
    var _body = jsonEncode({
      "SZBRANCH": _preferences.getString("branchid"),
      "SZSO": "SPG"
    });
    final _response = await _http.post(
        "${BaseUrl.urlGeneral}$_namaSumberOrderSPG",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      List _data = _result['RC1'];
      if(_data.isEmpty){
        showSnackBar("${_result['strmessage']}");
        loadData = false;
      }
      else{
        for(int i=0; i <_data.length; i++){
          this._listSourceOrderNameSPG.add(
            SourceOrderNameModel(
              _data[i]['ERP_BP_PARTNEREXTERNAL'],
              _data[i]['ERP_BP_NAME']
            )
          );
        }
        loadData = false;
      }
    }
    else{
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  void searchSourceOrderNameSPG(String query) async {
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    }
    else {
      _listSourceOrderNameSPGTemp.clear();
      if (query.isEmpty) {
        return;
      }

      _listSourceOrderNameSPG.forEach((dataSourceOrderNameSPG) {
        if (dataSourceOrderNameSPG.kode.contains(query) || dataSourceOrderNameSPG.deskripsi.contains(query)) {
          this._listSourceOrderNameSPGTemp.add(dataSourceOrderNameSPG);
        }
      });
      if(this._listSourceOrderNameSPGTemp.isEmpty) {
        this._listSourceOrderNameSPG.clear();
      }
      notifyListeners();
    }
  }

  void clearSearchTemp() {
    _listSourceOrderNameSPGTemp.clear();
    getSourceOrderNameSPG();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState. showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }
}
