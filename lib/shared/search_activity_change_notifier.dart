import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/activities_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';

class SearchActivityChangeNotifier with ChangeNotifier{
  var storage = FlutterSecureStorage();
  bool _showClear = false;
  TextEditingController _controllerSearch = TextEditingController();
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<ActivitiesModel> _listActivities = [
//    SourceOrderNameModel("07040000", "MAKASSAR DRB"),
//    SourceOrderNameModel("07050000", "KENDARI-AHMAD YANI"),
//    SourceOrderNameModel("07060000", "PALU-EMMY SAELAN"),
//    SourceOrderNameModel("07070000", "MANADO-AHMAD YANI"),
//    SourceOrderNameModel("07080000", "GORONTALO-NANI WARTABONE"),
//    SourceOrderNameModel("07090000", "KOTAMUBAGU"),
//    SourceOrderNameModel("07120000", "MAKASSAR 2 CAR-PANAKUKANG"),
//    SourceOrderNameModel("07160000", "SENGKANG-WAJO-PANGGARU"),
//    SourceOrderNameModel("07200000", "MAMUJU-URIP SUMOHARJO"),
  ];

  List<ActivitiesModel> _listActivitiesTemp = [];

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<ActivitiesModel> get listActivities {
    return UnmodifiableListView(this._listActivities);
  }

  UnmodifiableListView<ActivitiesModel> get listActivitiesTemp {
    return UnmodifiableListView(this._listActivitiesTemp);
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  void getActivities(BuildContext context) async{
    var _provider = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    this._listActivities.clear();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);
    var _body = jsonEncode(
        {
          "refOne": _provider.sourceOrderNameSelected.kode
        }
    );
    print("body kegiatan = $_body");
    String _fieldKegiatan = await storage.read(key: "FieldKegiatan");
    final _response = await _http.post(
      "${BaseUrl.urlGeneral}$_fieldKegiatan",
      // "${urlPublic}program-kegiatan/get_kegiatan",
      body: _body,
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      print(_result);
      if(_result == null){
        showSnackBar("Kegiatan tidak ditemukan");
        loadData = false;
      }
      else{
        for(int i=0; i < _result.length; i++){
          this._listActivities.add(
              ActivitiesModel(_result[i]['kode'], _result[i]['deskripsi'])
          );
        }
        loadData = false;
      }
    }
    else{
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void searchActivity(String query) {
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    } else {
      _listActivitiesTemp.clear();
      if (query.isEmpty) {
        return;
      }

      _listActivitiesTemp.forEach((dataActivity) {
        if (dataActivity.kode.contains(query) || dataActivity.deskripsi.contains(query)) {
          _listActivitiesTemp.add(dataActivity);
        }
      });
    }
    notifyListeners();
  }

  void clearSearchTemp() {
    _listActivitiesTemp.clear();
  }
}