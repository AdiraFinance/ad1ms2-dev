import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/models/form_m_company_rincian_model.dart';
import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:ad1ms2_dev/shared/search_sector_economic_change_notif.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';

import '../main.dart';
import 'constants.dart';
import 'form_m_company_rincian_change_notif.dart';

class SearchStatusLocationChangeNotif with ChangeNotifier {
    bool _showClear = false;
    bool _loadData = false;
    GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
    TextEditingController _controllerSearch = TextEditingController();

    List<StatusLocationModel> _listStatusLocationField = [];
    List<StatusLocationModel> _listStatusLocationFieldTemp = [];

    TextEditingController get controllerSearch => _controllerSearch;

    bool get showClear => _showClear;

    set showClear(bool value) {
        this._showClear = value;
        notifyListeners();
    }

    void changeAction(String value) {
        if (value != "") {
            showClear = true;
        } else {
            showClear = false;
        }
    }

    UnmodifiableListView<StatusLocationModel> get listBusinessField {
        return UnmodifiableListView(this._listStatusLocationField);
    }

    UnmodifiableListView<StatusLocationModel> get listBusinessFieldTemp {
        return UnmodifiableListView(this._listStatusLocationFieldTemp);
    }

    bool get loadData => _loadData;

    set loadData(bool value) {
        this._loadData = value;
        notifyListeners();
    }

    GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

    Future<void> getStatusLocationField() async{
        this._listStatusLocationField.clear();
        this._loadData = true;
        final ioc = new HttpClient();
        ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

        final _http = IOClient(ioc);

        var storage = FlutterSecureStorage();
        String _fieldStatusLocation = await storage.read(key: "FieldStatusLokasi");
        final _response = await _http.get(
            "${BaseUrl.urlGeneral}$_fieldStatusLocation",
            headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
            // "${urlPublic}api/occupation/get-lapangan-usaha",
        );

        if(_response.statusCode == 200){
            final _result = jsonDecode(_response.body);
            if(_result['data'].isNotEmpty){
                for(int i = 0 ; i < _result['data'].length ; i++) {
                    _listStatusLocationField.add(StatusLocationModel(_result['data'][i]['kode'], _result['data'][i]['deskripsi']));
                }
            }
            this._loadData = false;
        } else {
            showSnackBar("Error response status ${_response.statusCode}");
            this._loadData = false;
        }
        notifyListeners();
    }

    void searchStatusLocationField(String query) {
        if(query.length < 3) {
            showSnackBar("Input minimal 3 karakter");
        } else {
            _listStatusLocationFieldTemp.clear();
            if (query.isEmpty) {
                return;
            }
            _listStatusLocationField.forEach((dataBusinessField) {
                if (dataBusinessField.id.contains(query) || dataBusinessField.desc.contains(query)) {
                    _listStatusLocationFieldTemp.add(dataBusinessField);
                }
            });
        }
        notifyListeners();
    }

    void clearSearchTemp() {
        _listStatusLocationFieldTemp.clear();
    }

    void showSnackBar(String text){
        this._scaffoldKey.currentState.showSnackBar(new SnackBar(
            content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
    }
}
