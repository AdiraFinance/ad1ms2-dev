import 'dart:collection';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/birth_place_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/models/pemegang_saham_model.dart';
import 'package:ad1ms2_dev/models/showhide_mandatory_visibility/set_pemegang_saham_individu_company_model.dart';
import 'package:ad1ms2_dev/screens/search_birth_place.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/dashboard/dashboard_change_notif.dart';
import 'package:ad1ms2_dev/shared/date_picker.dart';
import 'package:ad1ms2_dev/shared/pemegang_saham_pribadi_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_birth_place_change_notifier.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../main.dart';

class AddPemegangSahamPribadiChangeNotifier with ChangeNotifier {
  bool _autoValidate = false;
  RelationshipStatusModel _relationshipStatusSelected;
  IdentityModel _typeIdentitySelected;
  TextEditingController _controllerIdentityNumber = TextEditingController();
  TextEditingController _controllerFullNameIdentity = TextEditingController();
  TextEditingController _controllerFullName = TextEditingController();
  TextEditingController _controllerAlias = TextEditingController();
  TextEditingController _controllerDegree = TextEditingController();
  TextEditingController _controllerBirthOfDate = TextEditingController();
  DateTime _initialDateForBirthOfDate = DateTime(dateNow.year, dateNow.month, dateNow.day);
  TextEditingController _controllerPlaceOfBirthIdentity = TextEditingController();
  TextEditingController _controllerBirthPlaceIdentityLOV = TextEditingController();
  String _radioValueGender = "01";
  TextEditingController _controllerPercentShare = TextEditingController();
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  BirthPlaceModel _birthPlaceSelected;
  DbHelper _dbHelper = DbHelper();
  SetPemegangSahamIndividuCompanyModel _setPemegangSahamIndividuCompanyModel;
  String _custType;
  String _lastKnownState;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String _shareholdersIndividualID;

  bool _isEdit = false;
  bool _relationshipStatusDakor = false;
  bool _identityTypeDakor = false;
  bool _identityNoDakor = false;
  bool _fullnameIDDakor = false;
  bool _fullnameDakor = false;
  bool _dateOfBirthDakor = false;
  bool _placeOfBirthDakor = false;
  bool _placeOfBirthLOVDakor = false;
  bool _genderDakor = false;
  bool _shareDakor = false;

  int _selectedIndex = -1;
  List<AddressModel> _listPemegangSahamPribadiAddress = [];
  TextEditingController _controllerAddress = TextEditingController();
  TextEditingController _controllerAddressType = TextEditingController();
  TextEditingController _controllerRT = TextEditingController();
  TextEditingController _controllerRW = TextEditingController();
  TextEditingController _controllerKelurahan = TextEditingController();
  TextEditingController _controllerKecamatan = TextEditingController();
  TextEditingController _controllerKota = TextEditingController();
  TextEditingController _controllerProvinsi = TextEditingController();
  TextEditingController _controllerPostalCode = TextEditingController();
  TextEditingController _controllerTeleponArea = TextEditingController();
  TextEditingController _controllerTelepon = TextEditingController();

  int get selectedIndex => _selectedIndex;

  set selectedIndex(int value) {
    this._selectedIndex = value;
    notifyListeners();
  }

  List<AddressModel> get listPemegangSahamPribadiAddress => _listPemegangSahamPribadiAddress;

  // Alamat
  TextEditingController get controllerAddress => _controllerAddress;

  // Jenis Alamat
  TextEditingController get controllerAddressType => _controllerAddressType;

  // RT
  TextEditingController get controllerRT => _controllerRT;

  // RW
  TextEditingController get controllerRW => _controllerRW;

  // Kelurahan
  TextEditingController get controllerKelurahan => _controllerKelurahan;

  // Kecamatan
  TextEditingController get controllerKecamatan => _controllerKecamatan;

  // Kabupaten/Kota
  TextEditingController get controllerKota => _controllerKota;

  // Provinsi
  TextEditingController get controllerProvinsi => _controllerProvinsi;

  // Kode Pos
  TextEditingController get controllerPostalCode => _controllerPostalCode;

  // Telepon 1 Area
  TextEditingController get controllerTeleponArea => _controllerTeleponArea;

  // Telepon 1
  TextEditingController get controllerTelepon => _controllerTelepon;

  void addPemegangSahamPribadiAddress(AddressModel value) {
    this._listPemegangSahamPribadiAddress.add(value);
    notifyListeners();
  }

  void updatePemegangSahamPribadiAddress(AddressModel value, int index) {
    this._listPemegangSahamPribadiAddress[index] = value;
    notifyListeners();
  }

  void deletePemegangSahamPribadiAddress(BuildContext context, int index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                primaryColor: Colors.black,
                primarySwatch: primaryOrange,
                accentColor: myPrimaryColor
            ),
            child: AlertDialog(
              title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Apakah kamu yakin menghapus alamat ini?",),
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    if(this._lastKnownState == "IDE"){
                      this._listPemegangSahamPribadiAddress.removeAt(index);
                      if(selectedIndex == index){
                        selectedIndex = -1;
                        this._controllerAddress.clear();
                        this._controllerAddressType.clear();
                        this._controllerRT.clear();
                        this._controllerRW.clear();
                        this._controllerKelurahan.clear();
                        this._controllerKecamatan.clear();
                        this._controllerKota.clear();
                        this._controllerProvinsi.clear();
                        this._controllerPostalCode.clear();
                        this._controllerTeleponArea.clear();
                        this._controllerTelepon.clear();
                      }
                    }
                    else{
                      this._listPemegangSahamPribadiAddress[index].active = 1;
                    }
                    notifyListeners();
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                ),
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Tidak'),
                ),
              ],
            ),
          );
        }
    );
  }

  void setCorrespondence(int index){
    for(int i=0; i < this._listPemegangSahamPribadiAddress.length; i++){
      if(this._listPemegangSahamPribadiAddress[i].isCorrespondence){
        this._listPemegangSahamPribadiAddress[i].isCorrespondence = false;
      }
    }
    this._listPemegangSahamPribadiAddress[index].isCorrespondence = true;
    notifyListeners();
  }

  void setAddress(AddressModel value) {
    this._controllerAddress.text = value.address;
    this._controllerAddressType.text = value.jenisAlamatModel.KODE + " - " + value.jenisAlamatModel.DESKRIPSI;
    this._controllerRT.text = value.rt;
    this._controllerRW.text = value.rw;
    this._controllerKelurahan.text = value.kelurahanModel.KEL_ID + " - " + value.kelurahanModel.KEL_NAME;
    this._controllerKecamatan.text = value.kelurahanModel.KEC_ID + " - " + value.kelurahanModel.KEC_NAME;
    this._controllerKota.text = value.kelurahanModel.KABKOT_ID + " - " + value.kelurahanModel.KABKOT_NAME;
    this._controllerProvinsi.text = value.kelurahanModel.PROV_ID + " - " + value.kelurahanModel.PROV_NAME;
    this._controllerPostalCode.text = value.kelurahanModel.ZIPCODE;
    this._controllerTeleponArea.text = value.areaCode;
    this._controllerTelepon.text = value.phone;
    notifyListeners();
  }

  void moreDialog(BuildContext context, index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans"
            ),
            child: AlertDialog(
              title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.check,
                          color: Colors.green,
                          size: 22.0,
                        ),
                        SizedBox(width: 12.0),
                        GestureDetector(
                          onTap: (){
                            selectedIndex = index;
                            _controllerAddress.clear();
                            setAddress(listPemegangSahamPribadiAddress[index]);
                            setCorrespondence(index);
                            Navigator.pop(context);
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Pilih sebagai Alamat Korespondensi",
                            style: TextStyle(fontSize: 14.0),
                          ),
                        )
                      ]
                  ),
                  SizedBox(height: 12.0),
                  listPemegangSahamPribadiAddress[index].jenisAlamatModel.KODE != "03" ?
                      // ? listPemegangSahamPribadiAddress[index].isSameWithIdentity
                      // ? SizedBox()
                      // :
                  Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.delete,
                          color: Colors.red,
                          size: 22.0,
                        ),
                        SizedBox(
                          width: 12.0,
                        ),
                        GestureDetector(
                          onTap: (){
                            Navigator.pop(context);
                            deletePemegangSahamPribadiAddress(context, index);
                          },
                          child: Text(
                            "Hapus",
                            style: TextStyle(fontSize: 14.0, color: Colors.red),
                          ),
                        )
                      ]
                  )
                      : SizedBox(),
                ],
              ),
              actions: <Widget>[
                FlatButton(
                    onPressed: (){
                      Navigator.pop(context);
                      // _updateStatusShowDialogSimilarity();
                    },
                    child: Text(
                        "CLOSE",
                        style: TextStyle(
                            color: primaryOrange,
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 1.25
                        )
                    )
                )
              ],
            ),
          );
        }
    );
  }

  void clearDataAlamat() {
    this._selectedIndex = -1;
    this._controllerAddress.clear();
    this._controllerAddressType.clear();
    this._controllerRT.clear();
    this._controllerRW.clear();
    this._controllerKelurahan.clear();
    this._controllerKecamatan.clear();
    this._controllerKota.clear();
    this._controllerProvinsi.clear();
    this._controllerPostalCode.clear();
    this._controllerTelepon.clear();
    this._controllerTeleponArea.clear();
    this._listPemegangSahamPribadiAddress = [];
  }

  bool get autoValidate => _autoValidate;

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  List<RelationshipStatusModel> _listRelationshipStatus = RelationshipStatusList().relationshipStatusItemsPemegangSaham;

  List<IdentityModel> _listTypeIdentity = IdentityType().lisIdentityModel;

  // Status Hubungan
  UnmodifiableListView<RelationshipStatusModel> get listRelationshipStatus {
    return UnmodifiableListView(this._listRelationshipStatus);
  }

  RelationshipStatusModel get relationshipStatusSelected => _relationshipStatusSelected;

  set relationshipStatusSelected(RelationshipStatusModel value) {
    this._relationshipStatusSelected = value;
    if(this._relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "01" || this._relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "04") {
      radioValueGender = "01";
    } else if(this._relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "02" || this._relationshipStatusSelected.PARA_FAMILY_TYPE_ID == "05") {
      radioValueGender = "02";
    }
    notifyListeners();
  }

  // Jenis Identitas
  UnmodifiableListView<IdentityModel> get listTypeIdentity {
    return UnmodifiableListView(this._listTypeIdentity);
  }

  IdentityModel get typeIdentitySelected => _typeIdentitySelected;

  set typeIdentitySelected(IdentityModel value) {
    this._typeIdentitySelected = value;
    this._controllerIdentityNumber.clear();
    notifyListeners();
  }

  // No Identitas
  TextEditingController get controllerIdentityNumber {
    return this._controllerIdentityNumber;
  }

  set controllerIdentityNumber(value) {
    this._controllerIdentityNumber = value;
    this.notifyListeners();
  }

  // Nama Lengkap Sesuai Identitas
  TextEditingController get controllerFullNameIdentity {
    return this._controllerFullNameIdentity;
  }

  set controllerFullNameIdentity(value) {
    this._controllerFullNameIdentity = value;
    this.notifyListeners();
  }

  // Nama Lengkap
  TextEditingController get controllerFullName {
    return this._controllerFullName;
  }

  set controllerFullName(value) {
    this._controllerFullName = value;
    this.notifyListeners();
  }

  // Alias
  TextEditingController get controllerAlias {
    return this._controllerAlias;
  }

  set controllerAlias(value) {
    this._controllerAlias = value;
    this.notifyListeners();
  }

  // Gelar
  TextEditingController get controllerDegree {
    return this._controllerDegree;
  }

  set controllerDegree(value) {
    this._controllerDegree = value;
    this.notifyListeners();
  }

  // Tanggal Lahir
  TextEditingController get controllerBirthOfDate => _controllerBirthOfDate;

  DateTime get initialDateForBirthOfDate => _initialDateForBirthOfDate;

  void selectBirthDate(BuildContext context) async {
    // DatePickerShared _datePickerShared = DatePickerShared();
    // var _datePickerSelected = await _datePickerShared.selectStartDate(
    //     context, this._initialDateForBirthOfDate,
    //     canAccessNextDay: false);
    var _datePickerSelected = await selectDate(context, this._initialDateForBirthOfDate);
    if (_datePickerSelected != null) {
      this.controllerBirthOfDate.text = dateFormat.format(_datePickerSelected);
      this._initialDateForBirthOfDate = _datePickerSelected;
      notifyListeners();
    } else {
      return;
    }
  }

  // Tempat Lahir Sesuai Identitas
  TextEditingController get controllerPlaceOfBirthIdentity {
    return this._controllerPlaceOfBirthIdentity;
  }

  set controllerPlaceOfBirthIdentity(value) {
    this._controllerPlaceOfBirthIdentity = value;
    this.notifyListeners();
  }

  // Tempat Lahir Sesuai Identitas LOV
  TextEditingController get controllerBirthPlaceIdentityLOV => _controllerBirthPlaceIdentityLOV;

  set controllerBirthPlaceIdentityLOV(TextEditingController value) {
    this._controllerBirthPlaceIdentityLOV = value;
    notifyListeners();
  }

  // Jenis Kalmin
  String get radioValueGender {
    return _radioValueGender;
  }

  set radioValueGender(String value) {
    this._radioValueGender = value;
    notifyListeners();
  }

  // % Share
  TextEditingController get controllerPercentShare {
    return this._controllerPercentShare;
  }

  set controllerPercentShare(value) {
    this._controllerPercentShare = value;
    this.notifyListeners();
  }

  GlobalKey<FormState> get keyForm => _key;

  String get custType => _custType;

  set custType(String value) {
    this._custType = value;
    notifyListeners();
  }

  String get lastKnownState => _lastKnownState;

  set lastKnownState(String value) {
    this._lastKnownState = value;
    notifyListeners();
  }

  BirthPlaceModel get birthPlaceSelected => _birthPlaceSelected;

  set birthPlaceSelected(BirthPlaceModel value) {
    this._birthPlaceSelected = value;
    notifyListeners();
  }

  String get shareholdersIndividualID => _shareholdersIndividualID;

  set shareholdersIndividualID(String value) {
    this._shareholdersIndividualID = value;
  }

  void check(BuildContext context, int flag, int index) {
    var _provider = Provider.of<PemegangSahamPribadiChangeNotifier>(context, listen: false);
    final _form = _key.currentState;
    bool _status = false;
    for(int i=0; i<_provider.listPemegangSahamPribadi.length; i++){
      if(this._controllerIdentityNumber.text == _provider.listPemegangSahamPribadi[i].identityNumber && flag == 0){
        _status = true;
        _showSnackBar("No Identitas telah digunakan");
      }
    }
    if(_form.validate()){
      if(flag == 0){
        if(!_status){
          _provider.addPemegangSahamPribadi(PemegangSahamPribadiModel(
              this._relationshipStatusSelected,
              this._typeIdentitySelected,
              this._controllerIdentityNumber.text,
              this._controllerFullNameIdentity.text,
              this._controllerFullName.text,
              this._initialDateForBirthOfDate.toString(),//this._controllerBirthOfDate.text,
              this._controllerPlaceOfBirthIdentity.text,
              this._birthPlaceSelected,
              this._radioValueGender,
              this._controllerPercentShare.text,
              this._listPemegangSahamPribadiAddress,
              isEdit,
              this._relationshipStatusDakor,
              this._identityTypeDakor,
              this._identityNoDakor,
              this._fullnameIDDakor,
              this._fullnameDakor,
              this._dateOfBirthDakor,
              this._placeOfBirthDakor,
              this._placeOfBirthLOVDakor,
              this._genderDakor,
              this._shareDakor,
              null
          ));
        }
      }
      else{
        _provider.updateListPemegangSahamPribadi(index, PemegangSahamPribadiModel(
          this._relationshipStatusSelected,
          this._typeIdentitySelected,
          this._controllerIdentityNumber.text,
          this._controllerFullNameIdentity.text,
          this._controllerFullName.text,
          this._initialDateForBirthOfDate.toString(),
          this._controllerPlaceOfBirthIdentity.text,
          this._birthPlaceSelected,
          this._radioValueGender,
          this._controllerPercentShare.text,
          this._listPemegangSahamPribadiAddress,
          isEdit,
          this._relationshipStatusDakor,
          this._identityTypeDakor,
          this._identityNoDakor,
          this._fullnameIDDakor,
          this._fullnameDakor,
          this._dateOfBirthDakor,
          this._placeOfBirthDakor,
          this._placeOfBirthLOVDakor,
          this._genderDakor,
          this._shareDakor,
          shareholdersIndividualID
        ));
      }
      autoValidate = false;
      checkDataDakor();
      if(!_status){
        Navigator.pop(context);
      }
    }
    else {
      autoValidate = true;
    }
  }

  void searchBirthPlace(BuildContext context) async{
    BirthPlaceModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchBirthPlaceChangeNotifier(),
                child: SearchBirthPlace())));
    if (data != null) {
      this._birthPlaceSelected = data;
      this._controllerBirthPlaceIdentityLOV.text = "${data.KABKOT_ID} - ${data.KABKOT_NAME}";
      notifyListeners();
    } else {
      return;
    }
  }

  void clearDataPemegangSahamPribadi() {
    this._autoValidate = false;
    this._relationshipStatusSelected = null;
    this._typeIdentitySelected = null;
    this._controllerIdentityNumber.clear();
    this._controllerFullNameIdentity.clear();
    this._controllerFullName.clear();
    // _controllerAlias.clear();
    // _controllerDegree.clear();
    this._controllerBirthOfDate.clear();
    this._controllerPlaceOfBirthIdentity.clear();
    this._controllerBirthPlaceIdentityLOV.clear();
    this._radioValueGender = "01";
    this._controllerPercentShare.clear();
    this._relationshipStatusDakor = false;
    this._identityTypeDakor = false;
    this._identityNoDakor = false;
    this._fullnameIDDakor = false;
    this._fullnameDakor = false;
    this._dateOfBirthDakor = false;
    this._placeOfBirthDakor = false;
    this._placeOfBirthLOVDakor = false;
    this._genderDakor = false;
    this._shareDakor = false;
    clearDataAlamat();
  }

  Future<void> setValue(int flag, int index, PemegangSahamPribadiModel value, BuildContext context) async{
    await getDataFromDashboard(context);
    for(int i=0; i < this._listRelationshipStatus.length; i++){
      if(value.relationshipStatusSelected.PARA_FAMILY_TYPE_ID == _listRelationshipStatus[i].PARA_FAMILY_TYPE_ID){
        _relationshipStatusSelected = _listRelationshipStatus[i];
      }
    }
    for(int i=0; i < this._listTypeIdentity.length; i++){
      if(value.typeIdentitySelected.id == _listTypeIdentity[i].id){
        _typeIdentitySelected = _listTypeIdentity[i];
      }
    }
    this._controllerIdentityNumber.text = value.identityNumber;
    this._controllerFullNameIdentity.text = value.fullNameIdentity;
    this._controllerFullName.text = value.fullName;
    if(value.birthOfDate != ""){
      this._initialDateForBirthOfDate = DateTime.parse(value.birthOfDate);
      this._controllerBirthOfDate.text = dateFormat.format(DateTime.parse(value.birthOfDate));
    }
    this._controllerPlaceOfBirthIdentity.text = value.placeOfBirthIdentity;
    this._controllerBirthPlaceIdentityLOV.text = value.birthPlaceIdentityLOV.KABKOT_NAME;
    this._birthPlaceSelected = value.birthPlaceIdentityLOV;
    this._radioValueGender = value.gender;
    this._controllerPercentShare.text = value.percentShare;
    for (int i = 0; i < value.listAddress.length; i++) {
      if (value.listAddress[i].isCorrespondence) {
        this._selectedIndex = i;
        this._controllerAddress.text = value.listAddress[i].address;
        this._controllerAddressType.text = value.listAddress[i].jenisAlamatModel.KODE + " - " + value.listAddress[i].jenisAlamatModel.DESKRIPSI;
        this._controllerRT.text = value.listAddress[i].rt;
        this._controllerRW.text = value.listAddress[i].rw;
        this._controllerKelurahan.text = value.listAddress[i].kelurahanModel.KEL_ID + " - " + value.listAddress[i].kelurahanModel.KEL_NAME;
        this._controllerKecamatan.text = value.listAddress[i].kelurahanModel.KEC_ID + " - " + value.listAddress[i].kelurahanModel.KEC_NAME;
        this._controllerKota.text = value.listAddress[i].kelurahanModel.KABKOT_ID + " - " + value.listAddress[i].kelurahanModel.KABKOT_NAME;
        this._controllerProvinsi.text = value.listAddress[i].kelurahanModel.PROV_ID + " - " + value.listAddress[i].kelurahanModel.PROV_NAME;
        this._controllerTeleponArea.text = value.listAddress[i].areaCode;
        this._controllerTelepon.text = value.listAddress[i].phone;
        this._controllerPostalCode.text = value.listAddress[i].kelurahanModel.ZIPCODE;
      }
    }
    for (int i = 0; i < value.listAddress.length; i++) {
      this._listPemegangSahamPribadiAddress.add(value.listAddress[i]);
    }
    shareholdersIndividualID = value.shareholdersIndividualID;
    checkDataDakor();
    notifyListeners();
  }

  Future<void> getDataFromDashboard(BuildContext context) async{
    _setPemegangSahamIndividuCompanyModel = Provider.of<DashboardChangeNotif>(context, listen: false).setPemegangSahamIndividuCompanyModel;
    SharedPreferences _preference = await SharedPreferences.getInstance();
    this._custType = _preference.getString("cust_type");
    this._lastKnownState = _preference.getString("last_known_state");
  }

  void _showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  bool isRelationshipStatusVisible() => _setPemegangSahamIndividuCompanyModel.relationshipStatusVisibility;
  bool isIdentityTypeVisible() => _setPemegangSahamIndividuCompanyModel.identityTypeVisibility;
  bool isIdentityNoVisible() => _setPemegangSahamIndividuCompanyModel.identityNoVisibility;
  bool isFullnameIDVisible() => _setPemegangSahamIndividuCompanyModel.fullnameIDVisibility;
  bool isFullnameVisible() => _setPemegangSahamIndividuCompanyModel.fullnameVisibility;
  bool isDateOfBirthVisible() => _setPemegangSahamIndividuCompanyModel.dateOfBirthVisibility;
  bool isPlaceOfBirthVisible() => _setPemegangSahamIndividuCompanyModel.placeOfBirthVisibility;
  bool isPlaceOfBirthLOVVisible() => _setPemegangSahamIndividuCompanyModel.placeOfBirthLOVVisibility;
  bool isGenderVisible() => _setPemegangSahamIndividuCompanyModel.genderVisibility;
  bool isShareVisible() => _setPemegangSahamIndividuCompanyModel.shareVisibility;

  bool isRelationshipStatusMandatory() => _setPemegangSahamIndividuCompanyModel.relationshipStatusMandatory;
  bool isIdentityTypeMandatory() => _setPemegangSahamIndividuCompanyModel.identityTypeMandatory;
  bool isIdentityNoMandatory() => _setPemegangSahamIndividuCompanyModel.identityNoMandatory;
  bool isFullnameIDMandatory() => _setPemegangSahamIndividuCompanyModel.fullnameIDMandatory;
  bool isFullnameMandatory() => _setPemegangSahamIndividuCompanyModel.fullnameMandatory;
  bool isDateOfBirthMandatory() => _setPemegangSahamIndividuCompanyModel.dateOfBirthMandatory;
  bool isPlaceOfBirthMandatory() => _setPemegangSahamIndividuCompanyModel.placeOfBirthMandatory;
  bool isPlaceOfBirthLOVMandatory() => _setPemegangSahamIndividuCompanyModel.placeOfBirthLOVMandatory;
  bool isGenderMandatory() => _setPemegangSahamIndividuCompanyModel.genderMandatory;
  bool isShareMandatory() => _setPemegangSahamIndividuCompanyModel.shareMandatory;

  // Fungsi validasi mandatory berdasarkan field untuk pemegang saham pribadi
  bool checkIsMandatoryPemegangSahamPribadi(){
    return !isRelationshipStatusMandatory() && !isIdentityTypeMandatory() && !isIdentityNoMandatory() && !isFullnameIDMandatory()
        && !isFullnameMandatory() && !isDateOfBirthMandatory() && !isPlaceOfBirthMandatory() && !isPlaceOfBirthLOVMandatory() && !isGenderMandatory()
        && !isShareMandatory();
  }

  void checkDataDakor() async{
    List _data = await _dbHelper.selectMS2CustShrhldrInd();
    if(_data.isNotEmpty && _lastKnownState == "DKR"){
      relationshipStatusDakor = this._relationshipStatusSelected.PARA_FAMILY_TYPE_ID != _data[0]['relation_status'] || _data[0]['edit_relation_status'] == "1";
      identityTypeDakor = this._typeIdentitySelected.id != _data[0]['id_type'] || _data[0]['edit_id_type'] == "1";
      identityNoDakor = this._controllerIdentityNumber.text != _data[0]['id_no'].toString() || _data[0]['edit_id_no'] == "1";
      fullnameIDDakor = this._controllerFullNameIdentity.text != _data[0]['full_name_id'].toString() || _data[0]['edit_full_name_id'] == "1";
      fullnameDakor = this._controllerFullName.text != _data[0]['full_name'].toString() || _data[0]['edit_full_name'] == "1";
      dateOfBirthDakor = this._initialDateForBirthOfDate != DateTime.parse(_data[0]['date_of_birth']) || _data[0]['edit_date_of_birth'] == "1";
      placeOfBirthDakor = this._controllerPlaceOfBirthIdentity.text != _data[0]['place_of_birth'].toString() || _data[0]['edit_place_of_birth'] == "1";
      placeOfBirthLOVDakor = this._controllerBirthPlaceIdentityLOV.text != _data[0]['place_of_birth_kabkota_desc'].toString() || _data[0]['edit_place_of_birth_kabkota'] == "1";
      genderDakor = this._radioValueGender != _data[0]['gender'].toString() || _data[0]['edit_gender'] == "1";
      shareDakor = this._controllerPercentShare.text != _data[0]['shrhldng_percent'].toString() || _data[0]['edit_shrhldng_percent'] == "1";
      if(relationshipStatusDakor || identityTypeDakor || identityNoDakor || fullnameIDDakor || fullnameDakor || dateOfBirthDakor || placeOfBirthDakor || placeOfBirthLOVDakor || genderDakor || shareDakor){
        isEdit = true;
      } else {
        isEdit = false;
      }
    }
    notifyListeners();
  }

  bool get isEdit => _isEdit;

  set isEdit(bool value) {
    _isEdit = value;
    notifyListeners();
  }

  bool get shareDakor => _shareDakor;

  set shareDakor(bool value) {
    _shareDakor = value;
    notifyListeners();
  }

  bool get genderDakor => _genderDakor;

  set genderDakor(bool value) {
    _genderDakor = value;
    notifyListeners();
  }

  bool get placeOfBirthLOVDakor => _placeOfBirthLOVDakor;

  set placeOfBirthLOVDakor(bool value) {
    _placeOfBirthLOVDakor = value;
    notifyListeners();
  }

  bool get placeOfBirthDakor => _placeOfBirthDakor;

  set placeOfBirthDakor(bool value) {
    _placeOfBirthDakor = value;
    notifyListeners();
  }

  bool get dateOfBirthDakor => _dateOfBirthDakor;

  set dateOfBirthDakor(bool value) {
    _dateOfBirthDakor = value;
    notifyListeners();
  }

  bool get fullnameDakor => _fullnameDakor;

  set fullnameDakor(bool value) {
    _fullnameDakor = value;
    notifyListeners();
  }

  bool get fullnameIDDakor => _fullnameIDDakor;

  set fullnameIDDakor(bool value) {
    _fullnameIDDakor = value;
    notifyListeners();
  }

  bool get identityNoDakor => _identityNoDakor;

  set identityNoDakor(bool value) {
    _identityNoDakor = value;
    notifyListeners();
  }

  bool get identityTypeDakor => _identityTypeDakor;

  set identityTypeDakor(bool value) {
    _identityTypeDakor = value;
    notifyListeners();
  }

  bool get relationshipStatusDakor => _relationshipStatusDakor;

  set relationshipStatusDakor(bool value) {
    _relationshipStatusDakor = value;
    notifyListeners();
  }

  void limitInput(String value){
    if (int.parse(value) > 100) {
      Future.delayed(Duration(milliseconds: 100), () {
        this._controllerPercentShare.clear();
      });
    }
  }
}
