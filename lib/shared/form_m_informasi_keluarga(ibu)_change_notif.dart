import 'dart:collection';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/birth_place_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_family_model.dart';
import 'package:ad1ms2_dev/screens/search_birth_place.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/search_birth_place_change_notifier.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'date_picker.dart';

class FormMInformasiKeluargaIBUChangeNotif with ChangeNotifier {
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  bool _flag = false;
  List<RelationshipStatusModel> _listRelationShipStatus = RelationshipStatusList().relationshipStatusItems;
  bool _autoValidate = false;
  RelationshipStatusModel _relationshipStatusSelected;
  IdentityModel _identitySelected;
  TextEditingController _controllerNoIdentitas = TextEditingController();
  TextEditingController _controllerNamaLengkapdentitas = TextEditingController();
  TextEditingController _controllerNamaIdentitas = TextEditingController();
  TextEditingController _controllerTglLahir = TextEditingController();
  TextEditingController _controllerTempatLahirSesuaiIdentitas = TextEditingController();
  TextEditingController _controllerTempatLahirSesuaiIdentitasLOV = TextEditingController();
  TextEditingController _controllerKodeArea = TextEditingController();
  TextEditingController _controllerTlpn = TextEditingController();
  TextEditingController _controllerNoHp = TextEditingController();
  String _radioValueGender = "02";
  List<IdentityModel> _listIdentityType = IdentityType().lisIdentityModel;
  BirthPlaceModel _birthPlaceSelected;
  bool _isEnableFieldFullName = true;
  DbHelper _dbHelper = DbHelper();
  String _familyInfoID;
  bool _enableDataDedup = false;
  bool _isDisablePACIAAOSCONA = false;

  bool _isRelationshipStatusVisible = false;
  bool _isIdentityVisible = false;
  bool _isNoIdentitasVisible = false;
  bool _isNamaLengkapdentitasVisible = false;
  bool _isNamaIdentitasVisible = false;
  bool _isTglLahirVisible = false;
  bool _isTempatLahirSesuaiIdentitasVisible = false;
  bool _isTempatLahirSesuaiIdentitasLOVVisible = false;
  bool _isRadioValueGenderVisible = false;
  bool _isKodeAreaVisible = false;
  bool _isTlpnVisible = false;
  bool _isNoHpVisible = false;

  bool _isRelationshipStatusMandatory = false;
  bool _isIdentityMandatory = false;
  bool _isNoIdentitasMandatory = false;
  bool _isNamaLengkapdentitasMandatory = false;
  bool _isNamaIdentitasMandatory = false;
  bool _isTglLahirMandatory = false;
  bool _isTempatLahirSesuaiIdentitasMandatory = false;
  bool _isTempatLahirSesuaiIdentitasLOVMandatory = false;
  bool _isRadioValueGenderMandatory = false;
  bool _isKodeAreaMandatory = false;
  bool _isTlpnMandatory = false;
  bool _isNoHpMandatory = false;

  bool _isRelationshipStatusChanges = false;
  bool _isIdentityChanges = false;
  bool _isNoIdentitasChanges = false;
  bool _isNamaLengkapdentitasChanges = false;
  bool _isNamaIdentitasChanges = false;
  bool _isTglLahirChanges = false;
  bool _isTempatLahirSesuaiIdentitasChanges = false;
  bool _isTempatLahirSesuaiIdentitasLOVChanges = false;
  bool _isRadioValueGenderChanges = false;
  bool _isKodeAreaChanges = false;
  bool _isTlpnChanges = false;
  bool _isNoHpChanges = false;

  // bool _isRelationshipStatusEnable = false;
  // bool _isNamaLengkapdentitasEnable = false;
  // bool _isNamaIdentitasEnable = false;

  bool get isRelationshipStatusVisible => _isRelationshipStatusVisible;
  bool get isIdentityVisible => _isIdentityVisible;
  bool get isNoIdentitasVisible => _isNoIdentitasVisible;
  bool get isNamaLengkapdentitasVisible => _isNamaLengkapdentitasVisible;
  bool get isNamaIdentitasVisible => _isNamaIdentitasVisible;
  bool get isTglLahirVisible => _isTglLahirVisible;
  bool get isTempatLahirSesuaiIdentitasVisible => _isTempatLahirSesuaiIdentitasVisible;
  bool get isTempatLahirSesuaiIdentitasLOVVisible => _isTempatLahirSesuaiIdentitasLOVVisible;
  bool get isRadioValueGenderVisible => _isRadioValueGenderVisible;
  bool get isKodeAreaVisible => _isKodeAreaVisible;
  bool get isTlpnVisible => _isTlpnVisible;
  bool get isNoHpVisible => _isNoHpVisible;

  bool get isRelationshipStatusMandatory => _isRelationshipStatusMandatory;
  bool get isIdentityMandatory => _isIdentityMandatory;
  bool get isNoIdentitasMandatory => _isNoIdentitasMandatory;
  bool get isNamaLengkapdentitasMandatory => _isNamaLengkapdentitasMandatory;
  bool get isNamaIdentitasMandatory => _isNamaIdentitasMandatory;
  bool get isTglLahirMandatory => _isTglLahirMandatory;
  bool get isTempatLahirSesuaiIdentitasMandatory => _isTempatLahirSesuaiIdentitasMandatory;
  bool get isTempatLahirSesuaiIdentitasLOVMandatory => _isTempatLahirSesuaiIdentitasLOVMandatory;
  bool get isRadioValueGenderMandatory => _isRadioValueGenderMandatory;
  bool get isKodeAreaMandatory => _isKodeAreaMandatory;
  bool get isTlpnMandatory => _isTlpnMandatory;
  bool get isNoHpMandatory => _isNoHpMandatory;

  bool get isRelationshipStatusChanges => _isRelationshipStatusChanges;
  bool get isIdentityChanges => _isIdentityChanges;
  bool get isNoIdentitasChanges => _isNoIdentitasChanges;
  bool get isNamaLengkapdentitasChanges => _isNamaLengkapdentitasChanges;
  bool get isNamaIdentitasChanges => _isNamaIdentitasChanges;
  bool get isTglLahirChanges => _isTglLahirChanges;
  bool get isTempatLahirSesuaiIdentitasChanges => _isTempatLahirSesuaiIdentitasChanges;
  bool get isTempatLahirSesuaiIdentitasLOVChanges => _isTempatLahirSesuaiIdentitasLOVChanges;
  bool get isRadioValueGenderChanges => _isRadioValueGenderChanges;
  bool get isKodeAreaChanges => _isKodeAreaChanges;
  bool get isTlpnChanges => _isTlpnChanges;
  bool get isNoHpChanges => _isNoHpChanges;

  // bool get isRelationshipStatusEnable => _isRelationshipStatusEnable;
  // bool get isNamaLengkapdentitasEnable => _isNamaLengkapdentitasEnable;
  // bool get isNamaIdentitasEnable => _isNamaIdentitasEnable;

  bool get isDisablePACIAAOSCONA => _isDisablePACIAAOSCONA;

  set isDisablePACIAAOSCONA(bool value) {
    this._isDisablePACIAAOSCONA = value;
    notifyListeners();
  }

  bool get enableDataDedup => _enableDataDedup;

  set enableDataDedup(bool value) {
    this._enableDataDedup = value;
    notifyListeners();
  }

  String get familyInfoID => _familyInfoID;

  set familyInfoID(String value) {
    this._familyInfoID = value;
    notifyListeners();
  }

  set isRelationshipStatusVisible(bool value) {
    this._isRelationshipStatusVisible = value;
    notifyListeners();
  }
  set isIdentityVisible(bool value) {
    this._isIdentityVisible = value;
    notifyListeners();
  }
  set isNoIdentitasVisible(bool value) {
    this._isNoIdentitasVisible = value;
    notifyListeners();
  }
  set isNamaLengkapdentitasVisible(bool value) {
    this._isNamaLengkapdentitasVisible = value;
    notifyListeners();
  }
  set isNamaIdentitasVisible(bool value) {
    this._isNamaIdentitasVisible = value;
    notifyListeners();
  }
  set isTglLahirVisible(bool value) {
    this._isTglLahirVisible = value;
    notifyListeners();
  }
  set isTempatLahirSesuaiIdentitasVisible(bool value) {
    this._isTempatLahirSesuaiIdentitasVisible = value;
    notifyListeners();
  }
  set isTempatLahirSesuaiIdentitasLOVVisible(bool value) {
    this._isTempatLahirSesuaiIdentitasLOVVisible = value;
    notifyListeners();
  }
  set isRadioValueGenderVisible(bool value) {
    this._isRadioValueGenderVisible = value;
    notifyListeners();
  }
  set isKodeAreaVisible(bool value) {
    this._isKodeAreaVisible = value;
    notifyListeners();
  }
  set isTlpnVisible(bool value) {
    this._isTlpnVisible = value;
    notifyListeners();
  }
  set isNoHpVisible(bool value) {
    this._isNoHpVisible = value;
    notifyListeners();
  }
//
  set isRelationshipStatusMandatory(bool value) {
    this._isRelationshipStatusMandatory = value;
    notifyListeners();
  }
  set isIdentityMandatory(bool value) {
    this._isIdentityMandatory = value;
    notifyListeners();
  }
  set isNoIdentitasMandatory(bool value) {
    this._isNoIdentitasMandatory = value;
    notifyListeners();
  }
  set isNamaLengkapdentitasMandatory(bool value) {
    this._isNamaLengkapdentitasMandatory = value;
    notifyListeners();
  }
  set isNamaIdentitasMandatory(bool value) {
    this._isNamaIdentitasMandatory = value;
    notifyListeners();
  }
  set isTglLahirMandatory(bool value) {
    this._isTglLahirMandatory = value;
    notifyListeners();
  }
  set isTempatLahirSesuaiIdentitasMandatory(bool value) {
    this._isTempatLahirSesuaiIdentitasMandatory = value;
    notifyListeners();
  }
  set isTempatLahirSesuaiIdentitasLOVMandatory(bool value) {
    this._isTempatLahirSesuaiIdentitasLOVMandatory = value;
    notifyListeners();
  }
  set isRadioValueGenderMandatory(bool value) {
    this._isRadioValueGenderMandatory = value;
    notifyListeners();
  }
  set isKodeAreaMandatory(bool value) {
    this._isKodeAreaMandatory = value;
    notifyListeners();
  }
  set isTlpnMandatory(bool value) {
    this._isTlpnMandatory = value;
    notifyListeners();
  }
  set isNoHpMandatory(bool value) {
    this._isNoHpMandatory = value;
    notifyListeners();
  }
//
  set isRelationshipStatusChanges(bool value) {
    this._isRelationshipStatusChanges = value;
    notifyListeners();
  }
  set isIdentityChanges(bool value) {
    this._isIdentityChanges = value;
    notifyListeners();
  }
  set isNoIdentitasChanges(bool value) {
    this._isNoIdentitasChanges = value;
    notifyListeners();
  }
  set isNamaLengkapdentitasChanges(bool value) {
    this._isNamaLengkapdentitasChanges = value;
    notifyListeners();
  }
  set isNamaIdentitasChanges(bool value) {
    this._isNamaIdentitasChanges = value;
    notifyListeners();
  }
  set isTglLahirChanges(bool value) {
    this._isTglLahirChanges = value;
    notifyListeners();
  }
  set isTempatLahirSesuaiIdentitasChanges(bool value) {
    this._isTempatLahirSesuaiIdentitasChanges = value;
    notifyListeners();
  }
  set isTempatLahirSesuaiIdentitasLOVChanges(bool value) {
    this._isTempatLahirSesuaiIdentitasLOVChanges = value;
    notifyListeners();
  }
  set isRadioValueGenderChanges(bool value) {
    this._isRadioValueGenderChanges = value;
    notifyListeners();
  }
  set isKodeAreaChanges(bool value) {
    this._isKodeAreaChanges = value;
    notifyListeners();
  }
  set isTlpnChanges(bool value) {
    this._isTlpnChanges = value;
    notifyListeners();
  }
  set isNoHpChanges(bool value) {
    this._isNoHpChanges = value;
    notifyListeners();
  }

//   set isRelationshipStatusEnable(bool value) {
//     this._isRelationshipStatusEnable = value;
//     notifyListeners();
//   }
//   set isNamaLengkapdentitasEnable(bool value) {
//     this._isNamaLengkapdentitasEnable = value;
//     notifyListeners();
//   }
//   set isNamaIdentitasEnable(bool value) {
//     this._isNamaIdentitasEnable = value;
//     notifyListeners();
//   }


  DateTime _initialDateForTglLahir =
      DateTime(dateNow.year, dateNow.month, dateNow.day);

  bool get autoValidate => _autoValidate;

  RelationshipStatusModel get relationshipStatusSelected => _relationshipStatusSelected;

  set relationshipStatusSelected(RelationshipStatusModel value) {
    this._relationshipStatusSelected = value;
    notifyListeners();
  }

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  IdentityModel get identitySelected => _identitySelected;

  set identitySelected(IdentityModel value) {
    this._identitySelected = value;
    this._controllerNoIdentitas.clear();
    notifyListeners();
  }

  String get radioValueGender => _radioValueGender;
  set radioValueGender(String value) {
    this._radioValueGender = value;
    notifyListeners();
  }

  TextEditingController get controllerNoIdentitas => _controllerNoIdentitas;

  TextEditingController get controllerNamaLengkapdentitas => _controllerNamaLengkapdentitas;

  TextEditingController get controllerNamaIdentitas => _controllerNamaIdentitas;

  TextEditingController get controllerTglLahir => _controllerTglLahir;

  TextEditingController get controllerTempatLahirSesuaiIdentitas => _controllerTempatLahirSesuaiIdentitas;

  TextEditingController get controllerKodeArea => _controllerKodeArea;

  TextEditingController get controllerTlpn => _controllerTlpn;

  TextEditingController get controllerNoHp => _controllerNoHp;

  TextEditingController get controllerTempatLahirSesuaiIdentitasLOV => _controllerTempatLahirSesuaiIdentitasLOV;

  UnmodifiableListView<RelationshipStatusModel> get listRelationShipStatus {
    return UnmodifiableListView(this._listRelationShipStatus);
  }

  UnmodifiableListView<IdentityModel> get listIdentityType {
    return UnmodifiableListView(this._listIdentityType);
  }

  void setDefaultValue() {
    this._relationshipStatusSelected = _listRelationShipStatus[4];
    this._identitySelected = _listIdentityType[0];
//    if (this._relationshipStatusSelected == null &&
//        this._identitySelected == null) {
//
//    }
  }

  void selectBirthDate(BuildContext context) async {
    DatePickerShared _datePickerShared = DatePickerShared();
    var _datePickerSelected = await _datePickerShared.selectStartDate(
        context, this._initialDateForTglLahir,
        canAccessNextDay: false);
    if (_datePickerSelected != null) {
      this._controllerTglLahir.text = dateFormat.format(_datePickerSelected);
      this._initialDateForTglLahir = _datePickerSelected;
      notifyListeners();
    } else {
      return;
    }
  }

  void checkValidCodeArea(String value) {
    if (value == "08") {
      Future.delayed(Duration(milliseconds: 100), () {
        this._controllerKodeArea.clear();
      });
    } else {
      return;
    }
  }

  void checkValidNotZero(String value) {
    if (value == "0") {
      Future.delayed(Duration(milliseconds: 100), () {
        this._controllerNoHp.clear();
      });
    } else {
      return;
    }
  }

  GlobalKey<FormState> get keyForm => _key;

  bool get flag => _flag;

  set flag(bool value) {
    _flag = value;
    notifyListeners();
  }

  void check(BuildContext context) {
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
      Navigator.pop(context);
    } else {
      flag = false;
      autoValidate = true;
    }
  }

  Future<bool> onBackPress() async{
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
    } else {
      flag = false;
      autoValidate = true;
    }
    return true;
  }

  BirthPlaceModel get birthPlaceSelected => _birthPlaceSelected;

  set birthPlaceSelected(BirthPlaceModel value) {
    this._birthPlaceSelected = value;
  }

  void searchBirthPlace(BuildContext context) async{
    BirthPlaceModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchBirthPlaceChangeNotifier(),
                child: SearchBirthPlace())));
    if (data != null) {
      birthPlaceSelected = data;
      this._controllerTempatLahirSesuaiIdentitasLOV.text = "${data.KABKOT_ID} - ${data.KABKOT_NAME}";
      notifyListeners();
    } else {
      return;
    }
  }

  bool get isEnableFieldFullName => _isEnableFieldFullName;

  set isEnableFieldFullName(bool value) {
    this._isEnableFieldFullName = value;
    notifyListeners();
  }

  // Future<void> clearDataKeluargaIbu() async {
  void clearDataKeluargaIbu() {
    this._flag = false;
    this._autoValidate = false;
    this._relationshipStatusSelected = null;
    isDisablePACIAAOSCONA = false;
    enableDataDedup = false;
    // this._identitySelected = null;
    // this._controllerNoIdentitas.clear();
    this._controllerNamaIdentitas.clear();
    this._controllerNamaLengkapdentitas.clear();
//    this._controllerNamaIdentitas.clear();
    // this._controllerTglLahir.clear();
    // this._controllerTempatLahirSesuaiIdentitas.clear();
    // this._controllerTempatLahirSesuaiIdentitasLOV.clear();
    // this._radioValueGender = "02";
    this._controllerKodeArea.clear();
    this._controllerTlpn.clear();
    // this._controllerNoHp.clear();


  }

  void saveToSQLite(){
    String _relationStatusId = _relationshipStatusSelected != null ? _relationshipStatusSelected.PARA_FAMILY_TYPE_ID : "";
    String _relationStatusDesc = _relationshipStatusSelected != null ? _relationshipStatusSelected.PARA_FAMILY_TYPE_NAME : "";
    String _identitySelectedId = _identitySelected != null ? _identitySelected.id : null;
    String _identitySelectedName = _identitySelected != null ? _identitySelected.name : null;
    _dbHelper.insertMS2CustFamily(MS2CustFamilyModel(
        "123",
        _familyInfoID,
        _relationStatusId,
        _relationStatusDesc,
        _controllerNamaLengkapdentitas.text,
        _controllerNamaIdentitas.text,
        _controllerNoIdentitas.text,
        this._controllerTglLahir.text != "" ? _initialDateForTglLahir.toString() : null,
        _controllerTempatLahirSesuaiIdentitas.text,
        null,
        null,
        _radioValueGender,
        _radioValueGender,
        null,
        _identitySelectedId,
        _identitySelectedName,
        null, null, null, null, 1, null, _controllerTlpn.text, _controllerKodeArea.text, _controllerNoHp.text,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null));
  }

  Future<bool> deleteSQLite() async{
    return await _dbHelper.deleteMS2CustFamily();
  }

  String _custType;
  String _lastKnownState;

  String get custType => _custType;

  set custType(String value) {
    this._custType = value;
    notifyListeners();
  }

  String get lastKnownState => _lastKnownState;

  set lastKnownState(String value) {
    this._lastKnownState = value;
    notifyListeners();
  }

  Future<void> setPreference() async {
    SharedPreferences _preference = await SharedPreferences.getInstance();
    this._custType = _preference.getString("cust_type");
    this._lastKnownState = _preference.getString("last_known_state");
    if(this._lastKnownState == "SRE" || this._lastKnownState == "RSVY" || this._lastKnownState == "DKR") {
      this._enableDataDedup = true;
    }
    if(this._lastKnownState == "AOS" || this._lastKnownState == "CONA" || this._lastKnownState == "IA" || this._lastKnownState == "PAC") {
      this._isDisablePACIAAOSCONA = true;
    }
  }

  void checkDataChanges() async{
    var _dedup = await _dbHelper.selectDataInfoNasabahFromDataDedup();
    List _data = await _dbHelper.selectDataInfoKeluarga("05");

    if(_dedup.isNotEmpty && _data.length > 0 && _lastKnownState == "DKR"){
      // _isRelationshipStatusChanges = _relationshipStatusSelected.PARA_FAMILY_TYPE_ID != _data[0]['relation_stauts'];
      _isIdentityChanges = _identitySelected.id != _data[0]['id_type'];
      _isNoIdentitasChanges = _controllerNoIdentitas.text != _dedup[0]['no_identitas'];
      _isNamaLengkapdentitasChanges = _controllerNamaLengkapdentitas.text != _data[0]['full_name_id'];
      _isNamaIdentitasChanges = _controllerNamaIdentitas.text != _dedup[0]['nama_gadis_ibu_kandung'];
      // _isTglLahirChanges = _controllerTglLahir.text != dateFormat.format(DateTime.parse(_data[0]['date_of_birth']));
      _isTempatLahirSesuaiIdentitasChanges = _controllerTempatLahirSesuaiIdentitas.text != _data[0]['place_of_birth'];
      _isTempatLahirSesuaiIdentitasLOVChanges = _controllerTempatLahirSesuaiIdentitasLOV.text != "${_data[0]['place_of_birth_kabkota']} - ${_data[0]['place_of_birth_kabkota_desc']}";
      _isRadioValueGenderChanges = _radioValueGender != _data[0]['gender'];
      _isKodeAreaChanges = _controllerKodeArea.text != _data[0]['phone1_area'];
      _isTlpnChanges = _controllerTlpn.text != _data[0]['phone1'];
      _isNoHpChanges = _controllerNoHp.text != _data[0]['handphone_no'];
    }
  }

  Future<void> setDataSQLite() async{
    var _dedup = await _dbHelper.selectDataInfoNasabahFromDataDedup();
    List _data = await _dbHelper.selectDataInfoKeluarga("05");
    if(this._controllerNamaIdentitas.text.isEmpty) {
      if(_data.isEmpty){
        this._controllerNamaIdentitas.text = _dedup[0]['nama_gadis_ibu_kandung'];
      }
      else{
        this._familyInfoID = _data[0]['familyInfoID'];
        // this._controllerNamaIdentitas.text = _data[0]['full_name_id'];
        // this._controllerNamaLengkapdentitas.text = _data[0]['full_name_id'];
        this._relationshipStatusSelected = RelationshipStatusModel(_data[0]['relation_status'], _data[0]['relation_status_desc']);
        this._identitySelected = IdentityModel(_data[0]['id_type'], _data[0]['id_desc']);
        this._controllerNamaIdentitas.text = _data[0]['full_name'];
        this._controllerNoIdentitas.text = _data[0]['id_no'];
        this._controllerNamaLengkapdentitas.text = _data[0]['full_name_id'] != "null" ? _data[0]['full_name_id'] : "";
        this._controllerTglLahir.text = _data[0]['date_of_birth'] != "null" ? dateFormat.format(DateTime.parse(_data[0]['date_of_birth'])) : "";
        this._controllerTempatLahirSesuaiIdentitas.text = _data[0]['place_of_birth'];
        this._controllerTempatLahirSesuaiIdentitasLOV.text = "${_data[0]['place_of_birth_kabkota']} - ${_data[0]['place_of_birth_kabkota_desc']}";
        this._controllerKodeArea.text = _data[0]['phone1_area'] != "null" ? _data[0]['phone1_area'] : "";
        this._controllerTlpn.text = _data[0]['phone1'] != "null" ? _data[0]['phone1'] : "";
        this._controllerNoHp.text = _data[0]['handphone_no'] != "null" ? _data[0]['handphone_no'] : "";
      }
    }
    checkDataChanges();
    notifyListeners();
  }
}
