
class LogPrint {
  static void printWrapped(String text) {
    final pattern = RegExp('.{1,800}'); // 800 is the size of each chunk
    pattern.allMatches(text).forEach((match) => print('validate submit ${match.group(0)}'));
  }
}