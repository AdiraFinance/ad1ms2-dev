import 'dart:collection';
import 'dart:convert';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/birth_place_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/models/guarantor_idividual_model.dart';
import 'package:ad1ms2_dev/models/showhide_mandatory_visibility/set_address_company_model.dart';
import 'package:ad1ms2_dev/models/showhide_mandatory_visibility/set_guarantor_individu_model.dart';
import 'package:ad1ms2_dev/screens/search_birth_place.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/dashboard/dashboard_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_guarantor_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_birth_place_change_notifier.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'date_picker.dart';

class FormMAddGuarantorIndividualChangeNotifier with ChangeNotifier {
  int _oldListSize = 0;
  List<RelationshipStatusModel> _listRelationShipStatus = [];
  RelationshipStatusModel _relationshipStatusModelSelected;
  RelationshipStatusModel _relationshipStatusModelTemp;
  IdentityModel _identityModelSelected;
  IdentityModel _identityModelTemp;
  String _identiyNumberTemp,
      _fullNameIdentityTemp,
      _fullNameTemp,
      _birthDateTemp,
      _birthPlaceTemp,
      _addressTemp,
      _addressTypeTemp,
      _rtTemp,
      _rwTemp,
      _kelurahanTemp,
      _kecamatanTemp,
      _kotaTemp,
      _provTemp,
      _areaCodeTemp,
      _phoneTemp,
      _postalCodeTemp;
  bool _autoValidate = false;
  TextEditingController _controllerIdentityNumber = TextEditingController();
  TextEditingController _controllerFullNameIdentity = TextEditingController();
  TextEditingController _controllerFullName = TextEditingController();
  TextEditingController _controllerBirthDate = TextEditingController();
  TextEditingController _controllerBirthPlaceIdentity = TextEditingController();
  TextEditingController _controllerCellPhoneNumber = TextEditingController();
  TextEditingController _controllerAddress = TextEditingController();
  TextEditingController _controllerAddressType = TextEditingController();
  TextEditingController _controllerRT = TextEditingController();
  TextEditingController _controllerRW = TextEditingController();
  TextEditingController _controllerKelurahan = TextEditingController();
  TextEditingController _controllerKecamatan = TextEditingController();
  TextEditingController _controllerKota = TextEditingController();
  TextEditingController _controllerProvinsi = TextEditingController();
  TextEditingController _controllerPostalCode = TextEditingController();
  TextEditingController _controllerKodeArea = TextEditingController();
  TextEditingController _controllerTlpn = TextEditingController();
  TextEditingController _controllerBirthPlaceIdentityLOV = TextEditingController();
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  int _indexCorrespondenceAddress = -1;
  String _radioValueGender = "01";
  String _custType;
  String _lastKnownState;
  SetGuarantorIndividu _setGuarantorIndividu, _setGuarantorIndividuCompany;
  DbHelper _dbHelper = DbHelper();
  String _guarantorIndividualID;
  bool _disableJenisPenawaran = false;

  bool _isEditList = false;
  bool _relationshipStatusDakor = false;
  bool _identityTypeDakor = false;
  bool _identityNoDakor = false;
  bool _fullnameIDDakor = false;
  bool _fullNameDakor = false;
  bool _dateOfBirthDakor = false;
  bool _placeOfBirthDakor = false;
  bool _placeOfBirthLOVDakor = false;
  bool _genderDakor = false;
  bool _phoneDakor = false;

  int _selectedIndex = -1;
  int _sizeList = 0;
  int _oldSelectedIndex = -1;

  DateTime _initialDateForBirthDate = DateTime(dateNow.year, dateNow.month, dateNow.day);

  List<AddressModel> _listGuarantorAddress = [];

  List<IdentityModel> _lisIdentityModel = IdentityType().lisIdentityModel;
//  [
//    IdentityModel("01", "KTP"),
//    IdentityModel("03", "PASSPORT"),
//    IdentityModel("04", "SIM"),
//    IdentityModel("05", "KTP Sementara"),
//    IdentityModel("06", "Resi KTP"),
//    IdentityModel("07", "Ket. Domisili"),
//  ];
  BirthPlaceModel _birthPlaceSelected;


  String get custType => _custType;

  set custType(String value) {
    this._custType = value;
  }

  int get oldListSize => _oldListSize;

  set oldListSize(int value) {
    this._oldListSize = value;
    notifyListeners();
  }

  RelationshipStatusModel get relationshipStatusModelSelected => _relationshipStatusModelSelected;

  set relationshipStatusModelSelected(RelationshipStatusModel value) {
    this._relationshipStatusModelSelected = value;
    if(this._relationshipStatusModelSelected.PARA_FAMILY_TYPE_ID == "01") {
      radioValueGender = "01";
    } else if(this._relationshipStatusModelSelected.PARA_FAMILY_TYPE_ID == "02") {
      radioValueGender = "02";
    } else if(this._relationshipStatusModelSelected.PARA_FAMILY_TYPE_ID == "04") {
      radioValueGender = "01";
    }
    notifyListeners();
  }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  UnmodifiableListView<RelationshipStatusModel> get listRelationShipStatus {
    return UnmodifiableListView(this._listRelationShipStatus);
  }

  UnmodifiableListView<IdentityModel> get lisIdentityModel {
    return UnmodifiableListView(this._lisIdentityModel);
  }

  IdentityModel get identityModelSelected => _identityModelSelected;

  set identityModelSelected(IdentityModel value) {
    this._identityModelSelected = value;
    this._controllerIdentityNumber.clear();
    notifyListeners();
  }

  TextEditingController get controllerIdentityNumber => _controllerIdentityNumber;

  TextEditingController get controllerFullName => _controllerFullName;

  TextEditingController get controllerFullNameIdentity => _controllerFullNameIdentity;

  TextEditingController get controllerBirthDate => _controllerBirthDate;

  TextEditingController get controllerBirthPlaceIdentityLOV => _controllerBirthPlaceIdentityLOV;

  void selectBirthDate(BuildContext context) async {
    // DatePickerShared _datePickerShared = DatePickerShared();
    // var _datePickerSelected = await _datePickerShared.selectStartDate(
    //     context, this._initialDateForBirthDate,
    //     canAccessNextDay: false);
    // if (_datePickerSelected != null) {
    //   this.controllerBirthDate.text = dateFormat.format(_datePickerSelected);
    //   this._initialDateForBirthDate = _datePickerSelected;
    //   notifyListeners();
    // } else {
    //   return;
    // }
    var _datePickerSelected = await selectDateLast10Year(context, this._initialDateForBirthDate);
    if (_datePickerSelected != null) {
      this.controllerBirthDate.text = dateFormat.format(_datePickerSelected);
      this._initialDateForBirthDate = _datePickerSelected;
      notifyListeners();
    } else {
      return;
    }
  }

  TextEditingController get controllerBirthPlaceIdentity => _controllerBirthPlaceIdentity;

  TextEditingController get controllerAddress => _controllerAddress;

  TextEditingController get controllerCellPhoneNumber => _controllerCellPhoneNumber;

  List<AddressModel> get listGuarantorAddress => _listGuarantorAddress;

  void addGuarantorAddress(AddressModel value) {
    this._listGuarantorAddress.add(value);
    notifyListeners();
  }

  void updateGuarantorAddress(AddressModel value, int index) {
    this._listGuarantorAddress[index] = value;
    if (index == this._selectedIndex) setCorrespondenceAddress(value, index);
    notifyListeners();
  }

  void deleteListOccupationAddress(BuildContext context, int index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                primaryColor: Colors.black,
                primarySwatch: primaryOrange,
                accentColor: myPrimaryColor
            ),
            child: AlertDialog(
              title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Apakah kamu yakin menghapus alamat ini?",),
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    if(_lastKnownState == "IDE"){
                      this._listGuarantorAddress.removeAt(index);
                      if(selectedIndex == index){
                        selectedIndex = -1;
                        this._controllerAddress.clear();
                        this._controllerAddressType.clear();
                        this._controllerRT.clear();
                        this._controllerRW.clear();
                        this._controllerKelurahan.clear();
                        this._controllerKecamatan.clear();
                        this._controllerKota.clear();
                        this._controllerProvinsi.clear();
                        this._controllerPostalCode.clear();
                        this._controllerKodeArea.clear();
                        this._controllerTlpn.clear();
                      }
                    }
                    else{
                      this._listGuarantorAddress[index].active = 1;
                    }
                    notifyListeners();
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                ),
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Tidak'),
                ),
              ],
            ),
          );
        }
    );
  }

  void setCorrespondenceAddress(AddressModel value, int index) {
    this._controllerAddress.text = value.address;
    this._controllerAddressType.text = value.jenisAlamatModel.KODE + " - " + value.jenisAlamatModel.DESKRIPSI;
    this._controllerRT.text = value.rt;
    this._controllerRW.text = value.rw;
    this._controllerKelurahan.text = value.kelurahanModel.KEL_ID + " - " + value.kelurahanModel.KEL_NAME;
    this._controllerKecamatan.text = value.kelurahanModel.KEC_ID + " - " + value.kelurahanModel.KEC_NAME;
    this._controllerKota.text = value.kelurahanModel.KABKOT_ID + " - " + value.kelurahanModel.KABKOT_NAME;
    this._controllerProvinsi.text = value.kelurahanModel.PROV_ID + " - " + value.kelurahanModel.PROV_NAME;
    this._controllerPostalCode.text = value.kelurahanModel.ZIPCODE;
    this._controllerKodeArea.text = value.areaCode;
    this._controllerTlpn.text = value.phone;

    for(int i=0; i < this._listGuarantorAddress.length; i++){
      if(this._listGuarantorAddress[i].isCorrespondence){
        this._listGuarantorAddress[i].isCorrespondence = false;
      }
    }
    this._listGuarantorAddress[index].isCorrespondence = true;
    notifyListeners();
  }

  int get selectedIndex => _selectedIndex;

  set selectedIndex(int value) {
    this._selectedIndex = value;
    notifyListeners();
  }

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void _showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating,backgroundColor: snackbarColor, duration: Duration(seconds: 4)));
  }

  void check(BuildContext context, int flag, int index) {
    final _form = _key.currentState;
    List<AddressModel> _model = [];
    bool _status = false;
    var _provider = Provider.of<FormMGuarantorChangeNotifier>(context, listen: false);
    for(int i=0; i<_provider.listGuarantorIndividual.length; i++){
      if(this._controllerIdentityNumber.text == _provider.listGuarantorIndividual[i].identityNumber && flag == 0){
        _status = true;
        _showSnackBar("No Identitas telah digunakan");
      }
    }
    if (flag == 0) {
      if (_form.validate() && !_status) {
        for (int i = 0; i < this._listGuarantorAddress.length; i++) {
          if (_selectedIndex == i) {
            _model.add(AddressModel(
                this._listGuarantorAddress[i].jenisAlamatModel,
                this._listGuarantorAddress[i].addressID,
                this._listGuarantorAddress[i].foreignBusinessID,
                null,
                this._listGuarantorAddress[i].kelurahanModel,
                this._listGuarantorAddress[i].address,
                this._listGuarantorAddress[i].rt,
                this._listGuarantorAddress[i].rw,
                this._listGuarantorAddress[i].areaCode,
                this._listGuarantorAddress[i].phone,
                this._listGuarantorAddress[i].isSameWithIdentity,
                this._listGuarantorAddress[i].addressLatLong,
                true,
                this._listGuarantorAddress[i].active,
                this._listGuarantorAddress[i].isEditAddress,
                this._listGuarantorAddress[i].isAlamatChanges,
                this._listGuarantorAddress[i].isAddressTypeChanges,
                this._listGuarantorAddress[i].isRTChanges,
                this._listGuarantorAddress[i].isRWChanges,
                this._listGuarantorAddress[i].isKelurahanChanges,
                this._listGuarantorAddress[i].isKecamatanChanges,
                this._listGuarantorAddress[i].isKotaChanges,
                this._listGuarantorAddress[i].isProvinsiChanges,
                this._listGuarantorAddress[i].isPostalCodeChanges,
                this._listGuarantorAddress[i].isAddressFromMapChanges,
                this._listGuarantorAddress[i].isTeleponAreaChanges,
                this._listGuarantorAddress[i].isTeleponChanges));
          } else {
            _model.add(AddressModel(
                this._listGuarantorAddress[i].jenisAlamatModel,
                this._listGuarantorAddress[i].addressID,
                this._listGuarantorAddress[i].foreignBusinessID,
                null,
                this._listGuarantorAddress[i].kelurahanModel,
                this._listGuarantorAddress[i].address,
                this._listGuarantorAddress[i].rt,
                this._listGuarantorAddress[i].rw,
                this._listGuarantorAddress[i].areaCode,
                this._listGuarantorAddress[i].phone,
                this._listGuarantorAddress[i].isSameWithIdentity,
                this._listGuarantorAddress[i].addressLatLong,
                false,
                this._listGuarantorAddress[i].active,
                this._listGuarantorAddress[i].isEditAddress,
                this._listGuarantorAddress[i].isAlamatChanges,
                this._listGuarantorAddress[i].isAddressTypeChanges,
                this._listGuarantorAddress[i].isRTChanges,
                this._listGuarantorAddress[i].isRWChanges,
                this._listGuarantorAddress[i].isKelurahanChanges,
                this._listGuarantorAddress[i].isKecamatanChanges,
                this._listGuarantorAddress[i].isKotaChanges,
                this._listGuarantorAddress[i].isProvinsiChanges,
                this._listGuarantorAddress[i].isPostalCodeChanges,
                this._listGuarantorAddress[i].isAddressFromMapChanges,
                this._listGuarantorAddress[i].isTeleponAreaChanges,
                this._listGuarantorAddress[i].isTeleponChanges));
          }
        }
        Provider.of<FormMGuarantorChangeNotifier>(context, listen: false)
            .addGuarantorIndividual(GuarantorIndividualModel(
                this._relationshipStatusModelSelected,
                this.identityModelSelected,
                this._controllerIdentityNumber.text,
                this._controllerFullNameIdentity.text,
                this._controllerFullName.text,
                this.controllerBirthDate.text != "" ? this._initialDateForBirthDate.toString() : "",
                this._controllerBirthPlaceIdentity.text,
                this._birthPlaceSelected,
                _model,
                this._controllerCellPhoneNumber.text,
                this._radioValueGender,
                this._isEditList,
                this._relationshipStatusDakor,
                this._identityTypeDakor,
                this._identityNoDakor,
                this._fullnameIDDakor,
                this._fullNameDakor,
                this._dateOfBirthDakor,
                this._placeOfBirthDakor,
                this._placeOfBirthLOVDakor,
                this._genderDakor,
                this._phoneDakor,null
            )
        );
        if (this._autoValidate) autoValidate = false;
        selectedIndex = -1;
        notifyListeners();
        Navigator.pop(context);
      }
      else {
        autoValidate = true;
      }
    }
    else {
      if (_form.validate()) {
        checkDataDakor(index);
        if (this._oldSelectedIndex != this._selectedIndex) {
          for (int i = 0; i < this._listGuarantorAddress.length; i++) {
            if (_oldSelectedIndex == i) {
              _listGuarantorAddress[i].isCorrespondence = false;
            }
          }
          for (int i = 0; i < this._listGuarantorAddress.length; i++) {
            if (_selectedIndex == i) {
              _model.add(AddressModel(
                  this._listGuarantorAddress[i].jenisAlamatModel,
                  this._listGuarantorAddress[i].addressID,
                  this._listGuarantorAddress[i].foreignBusinessID,
                  null,
                  this._listGuarantorAddress[i].kelurahanModel,
                  this._listGuarantorAddress[i].address,
                  this._listGuarantorAddress[i].rt,
                  this._listGuarantorAddress[i].rw,
                  this._listGuarantorAddress[i].areaCode,
                  this._listGuarantorAddress[i].phone,
                  this._listGuarantorAddress[i].isSameWithIdentity,
                  this._listGuarantorAddress[i].addressLatLong,
                  true,
                  this._listGuarantorAddress[i].active,
                  this._listGuarantorAddress[i].isEditAddress,
                  this._listGuarantorAddress[i].isAlamatChanges,
                  this._listGuarantorAddress[i].isAddressTypeChanges,
                  this._listGuarantorAddress[i].isRTChanges,
                  this._listGuarantorAddress[i].isRWChanges,
                  this._listGuarantorAddress[i].isKelurahanChanges,
                  this._listGuarantorAddress[i].isKecamatanChanges,
                  this._listGuarantorAddress[i].isKotaChanges,
                  this._listGuarantorAddress[i].isProvinsiChanges,
                  this._listGuarantorAddress[i].isPostalCodeChanges,
                  this._listGuarantorAddress[i].isAddressFromMapChanges,
                  this._listGuarantorAddress[i].isTeleponAreaChanges,
                  this._listGuarantorAddress[i].isTeleponChanges));
            }
            else {
              _model.add(AddressModel(
                  this._listGuarantorAddress[i].jenisAlamatModel,
                  this._listGuarantorAddress[i].addressID,
                  this._listGuarantorAddress[i].foreignBusinessID,
                  null,
                  this._listGuarantorAddress[i].kelurahanModel,
                  this._listGuarantorAddress[i].address,
                  this._listGuarantorAddress[i].rt,
                  this._listGuarantorAddress[i].rw,
                  this._listGuarantorAddress[i].areaCode,
                  this._listGuarantorAddress[i].phone,
                  this._listGuarantorAddress[i].isSameWithIdentity,
                  this._listGuarantorAddress[i].addressLatLong,
                  false,
                  this._listGuarantorAddress[i].active,
                  this._listGuarantorAddress[i].isEditAddress,
                  this._listGuarantorAddress[i].isAlamatChanges,
                  this._listGuarantorAddress[i].isAddressTypeChanges,
                  this._listGuarantorAddress[i].isRTChanges,
                  this._listGuarantorAddress[i].isRWChanges,
                  this._listGuarantorAddress[i].isKelurahanChanges,
                  this._listGuarantorAddress[i].isKecamatanChanges,
                  this._listGuarantorAddress[i].isKotaChanges,
                  this._listGuarantorAddress[i].isProvinsiChanges,
                  this._listGuarantorAddress[i].isPostalCodeChanges,
                  this._listGuarantorAddress[i].isAddressFromMapChanges,
                  this._listGuarantorAddress[i].isTeleponAreaChanges,
                  this._listGuarantorAddress[i].isTeleponChanges));
            }
          }
          Provider.of<FormMGuarantorChangeNotifier>(context, listen: false)
              .updateListGuarantorIndividual(
                  GuarantorIndividualModel(
                      this._relationshipStatusModelSelected,
                      this.identityModelSelected,
                      this._controllerIdentityNumber.text,
                      this._controllerFullNameIdentity.text,
                      this._controllerFullName.text,
                      this.controllerBirthDate.text != "" ? this._initialDateForBirthDate.toString() : "",
                      this._controllerBirthPlaceIdentity.text,
                      this._birthPlaceSelected,
                      _model,this._controllerCellPhoneNumber.text,
                      this._radioValueGender,
                      this._isEditList,
                      this._relationshipStatusDakor,
                      this._identityTypeDakor,
                      this._identityNoDakor,
                      this._fullnameIDDakor,
                      this._fullNameDakor,
                      this._dateOfBirthDakor,
                      this._placeOfBirthDakor,
                      this._placeOfBirthLOVDakor,
                      this._genderDakor,
                      this._phoneDakor,
                      this.guarantorIndividualID
                  ),
                  index);
          if (this._autoValidate) autoValidate = false;
          selectedIndex = -1;
          this._oldSelectedIndex = -1;
          notifyListeners();
          Navigator.pop(context);
        }
        else {
          for (int i = 0; i < this._listGuarantorAddress.length; i++) {
            if (_selectedIndex == i) {
              _model.add(AddressModel(
                  this._listGuarantorAddress[i].jenisAlamatModel,
                  this._listGuarantorAddress[i].addressID,
                  this._listGuarantorAddress[i].foreignBusinessID,
                  null,
                  this._listGuarantorAddress[i].kelurahanModel,
                  this._listGuarantorAddress[i].address,
                  this._listGuarantorAddress[i].rt,
                  this._listGuarantorAddress[i].rw,
                  this._listGuarantorAddress[i].areaCode,
                  this._listGuarantorAddress[i].phone,
                  this._listGuarantorAddress[i].isSameWithIdentity,
                  this._listGuarantorAddress[i].addressLatLong,
                  true,
                  this._listGuarantorAddress[i].active,
                  this._listGuarantorAddress[i].isEditAddress,
                  this._listGuarantorAddress[i].isAlamatChanges,
                  this._listGuarantorAddress[i].isAddressTypeChanges,
                  this._listGuarantorAddress[i].isRTChanges,
                  this._listGuarantorAddress[i].isRWChanges,
                  this._listGuarantorAddress[i].isKelurahanChanges,
                  this._listGuarantorAddress[i].isKecamatanChanges,
                  this._listGuarantorAddress[i].isKotaChanges,
                  this._listGuarantorAddress[i].isProvinsiChanges,
                  this._listGuarantorAddress[i].isPostalCodeChanges,
                  this._listGuarantorAddress[i].isAddressFromMapChanges,
                  this._listGuarantorAddress[i].isTeleponAreaChanges,
                  this._listGuarantorAddress[i].isTeleponChanges));
            } else {
              _model.add(AddressModel(
                  this._listGuarantorAddress[i].jenisAlamatModel,
                  this._listGuarantorAddress[i].addressID,
                  this._listGuarantorAddress[i].foreignBusinessID,
                  null,
                  this._listGuarantorAddress[i].kelurahanModel,
                  this._listGuarantorAddress[i].address,
                  this._listGuarantorAddress[i].rt,
                  this._listGuarantorAddress[i].rw,
                  this._listGuarantorAddress[i].areaCode,
                  this._listGuarantorAddress[i].phone,
                  this._listGuarantorAddress[i].isSameWithIdentity,
                  this._listGuarantorAddress[i].addressLatLong,
                  false,
                  this._listGuarantorAddress[i].active,
                  this._listGuarantorAddress[i].isEditAddress,
                  this._listGuarantorAddress[i].isAlamatChanges,
                  this._listGuarantorAddress[i].isAddressTypeChanges,
                  this._listGuarantorAddress[i].isRTChanges,
                  this._listGuarantorAddress[i].isRWChanges,
                  this._listGuarantorAddress[i].isKelurahanChanges,
                  this._listGuarantorAddress[i].isKecamatanChanges,
                  this._listGuarantorAddress[i].isKotaChanges,
                  this._listGuarantorAddress[i].isProvinsiChanges,
                  this._listGuarantorAddress[i].isPostalCodeChanges,
                  this._listGuarantorAddress[i].isAddressFromMapChanges,
                  this._listGuarantorAddress[i].isTeleponAreaChanges,
                  this._listGuarantorAddress[i].isTeleponChanges));
            }
          }
          Provider.of<FormMGuarantorChangeNotifier>(context, listen: false)
              .updateListGuarantorIndividual(
                  GuarantorIndividualModel(
                      this._relationshipStatusModelSelected,
                      this.identityModelSelected,
                      this._controllerIdentityNumber.text,
                      this._controllerFullNameIdentity.text,
                      this._controllerFullName.text,
                      this.controllerBirthDate.text != "" ? this._initialDateForBirthDate.toString() : "",
                      this._controllerBirthPlaceIdentity.text,
                      this._birthPlaceSelected,
                      _model,this._controllerCellPhoneNumber.text,
                      this._radioValueGender,
                      this._isEditList,
                      this._relationshipStatusDakor,
                      this._identityTypeDakor,
                      this._identityNoDakor,
                      this._fullnameIDDakor,
                      this._fullNameDakor,
                      this._dateOfBirthDakor,
                      this._placeOfBirthDakor,
                      this._placeOfBirthLOVDakor,
                      this._genderDakor,
                      this._phoneDakor,this.guarantorIndividualID),
                  index);
          if (this._autoValidate) autoValidate = false;
          selectedIndex = -1;
          this._oldSelectedIndex = -1;
          notifyListeners();
          Navigator.pop(context);
        }
      } else {
        autoValidate = true;
      }
    }
  }

  TextEditingController get controllerAddressType => _controllerAddressType;

  TextEditingController get controllerRW => _controllerRW;

  TextEditingController get controllerRT => _controllerRT;

  TextEditingController get controllerKelurahan => _controllerKelurahan;

  TextEditingController get controllerKecamatan => _controllerKecamatan;

  TextEditingController get controllerKota => _controllerKota;

  TextEditingController get controllerProvinsi => _controllerProvinsi;

  TextEditingController get controllerKodeArea => _controllerKodeArea;

  TextEditingController get controllerPostalCode => _controllerPostalCode;

  TextEditingController get controllerTlpn => _controllerTlpn;

  GlobalKey<FormState> get key => _key;

  int get indexCorrespondenceAddress => _indexCorrespondenceAddress;

  set indexCorrespondenceAddress(int value) {
    this._indexCorrespondenceAddress = value;
    notifyListeners();
  }

  String get guarantorIndividualID => _guarantorIndividualID;

  set guarantorIndividualID(String value) {
    this._guarantorIndividualID = value;
  }

  bool get disableJenisPenawaran => _disableJenisPenawaran;

  set disableJenisPenawaran(bool value) {
    this._disableJenisPenawaran = value;
  }

  Future<void> setValueForEdit(BuildContext context, GuarantorIndividualModel data, int flag, int index) async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    _custType = _preferences.getString("cust_type");
    _lastKnownState = _preferences.getString("last_known_state");
    this._listRelationShipStatus = _custType == "PER" ? RelationshipStatusList().relationshipStatusItems : RelationshipStatusList().relationshipStatusItemsPemegangSaham;
    this._disableJenisPenawaran = false;
    if(_preferences.getString("jenis_penawaran") == "002"){
      this._disableJenisPenawaran = true;
    }
    getDataFromDashboard(context);
    clearData();
    if(flag == 1){
      for(int ii=0; ii < this._listRelationShipStatus.length; ii++){
        if(_listRelationShipStatus[ii].PARA_FAMILY_TYPE_ID == data.relationshipStatusModel.PARA_FAMILY_TYPE_ID){
          _relationshipStatusModelSelected = _listRelationShipStatus[ii];
          _relationshipStatusModelTemp = _listRelationShipStatus[ii];
        }
      }

      for(int ii=0; ii < this._lisIdentityModel.length; ii++){
        if(_lisIdentityModel[ii].id == data.identityModel.id){
          _identityModelSelected = _lisIdentityModel[ii];
          _identityModelTemp = _lisIdentityModel[ii];
        }
      }

      this._controllerIdentityNumber.text = data.identityNumber;
      this._identiyNumberTemp = this._controllerIdentityNumber.text;
      this._controllerFullNameIdentity.text = data.fullNameIdentity;
      this._fullNameIdentityTemp = this._controllerFullNameIdentity.text;
      this._controllerFullName.text = data.fullName;
      this._fullNameTemp = this._controllerFullName.text;
      if(data.birthDate != ""){
        this._initialDateForBirthDate = DateTime.parse(data.birthDate);
        this._controllerBirthDate.text = dateFormat.format(DateTime.parse(data.birthDate));
        this._birthDateTemp = this._controllerBirthDate.text;
      }
      this._controllerBirthPlaceIdentity.text = data.birthPlaceIdentity1;
      this._birthPlaceTemp = this._controllerBirthPlaceIdentity.text;
      this._birthPlaceSelected = data.birthPlaceIdentity2;
      this._controllerBirthPlaceIdentityLOV.text = data.birthPlaceIdentity2.KABKOT_ID + " - " + data.birthPlaceIdentity2.KABKOT_NAME;
      this._radioValueGender = data.gender;
      this._controllerCellPhoneNumber.text = data.cellPhoneNumber;
      guarantorIndividualID = data.guarantorIndividualID;

      for (int i = 0; i < data.listAddressGuarantorModel.length; i++) {
        if (data.listAddressGuarantorModel[i].isCorrespondence) {
          this._selectedIndex = i;
          this._oldSelectedIndex = i;
          this._controllerAddress.text = data.listAddressGuarantorModel[i].address;
          this._addressTemp = this._controllerAddress.text;
          this._controllerAddressType.text = data.listAddressGuarantorModel[i].jenisAlamatModel.KODE + " - " + data.listAddressGuarantorModel[i].jenisAlamatModel.DESKRIPSI;
          this._addressTypeTemp = this._controllerAddressType.text;
          this._controllerRT.text = data.listAddressGuarantorModel[i].rt;
          this._rtTemp = this._controllerRT.text;
          this._controllerRW.text = data.listAddressGuarantorModel[i].rw;
          this._rwTemp = this._controllerRW.text;
          this._controllerKelurahan.text = data.listAddressGuarantorModel[i].kelurahanModel.KEL_ID + " - " + data.listAddressGuarantorModel[i].kelurahanModel.KEL_NAME;
          this._kelurahanTemp = this._controllerKelurahan.text;
          this._controllerKecamatan.text = data.listAddressGuarantorModel[i].kelurahanModel.KEC_ID + " - " + data.listAddressGuarantorModel[i].kelurahanModel.KEC_NAME;
          this._kecamatanTemp = this._controllerKecamatan.text;
          this._controllerKota.text = data.listAddressGuarantorModel[i].kelurahanModel.KABKOT_ID + " - " + data.listAddressGuarantorModel[i].kelurahanModel.KABKOT_NAME;
          this._kotaTemp = this._controllerKota.text;
          this._controllerProvinsi.text = data.listAddressGuarantorModel[i].kelurahanModel.PROV_ID + " - " + data.listAddressGuarantorModel[i].kelurahanModel.PROV_NAME;
          this._provTemp = this._controllerProvinsi.text;
          this._controllerKodeArea.text = data.listAddressGuarantorModel[i].areaCode;
          this._areaCodeTemp = this._controllerKodeArea.text;
          this._controllerTlpn.text = data.listAddressGuarantorModel[i].phone;
          this._phoneTemp = this._controllerTlpn.text;
          this._controllerPostalCode.text = data.listAddressGuarantorModel[i].kelurahanModel.ZIPCODE;
          this._postalCodeTemp = this._controllerPostalCode.text;
        }
      }
      for (int i = 0; i < data.listAddressGuarantorModel.length; i++) {
        this._listGuarantorAddress.add(data.listAddressGuarantorModel[i]);
      }
      this._sizeList = this._listGuarantorAddress.length;
      checkDataDakor(index);
    }
  }

  void clearData() {
    this._selectedIndex = -1;
    this._listGuarantorAddress.clear();
    this._relationshipStatusModelSelected = null;
    this._identityModelSelected = null;
    this._controllerIdentityNumber.clear();
    this._controllerFullNameIdentity.clear();
    this._controllerFullName.clear();
    this._controllerBirthDate.clear();
    this._controllerBirthPlaceIdentity.clear();
    this._controllerBirthPlaceIdentityLOV.clear();
    this._controllerAddress.clear();
    this._controllerAddressType.clear();
    this._controllerRT.clear();
    this._controllerRW.clear();
    this._controllerKelurahan.clear();
    this._controllerKecamatan.clear();
    this._controllerKota.clear();
    this._controllerProvinsi.clear();
    this._controllerPostalCode.clear();
    this._controllerKodeArea.clear();
    this._controllerTlpn.clear();
    this._controllerCellPhoneNumber.clear();
    this._isEditList = false;
    this._relationshipStatusDakor = false;
    this._identityTypeDakor = false;
    this._identityNoDakor = false;
    this._fullnameIDDakor = false;
    this._fullNameDakor = false;
    this._dateOfBirthDakor = false;
    this._placeOfBirthDakor = false;
    this._placeOfBirthLOVDakor = false;
    this._genderDakor = false;
    this._phoneDakor = false;
  }

  get phoneTemp => _phoneTemp;

  get areaCodeTemp => _areaCodeTemp;

  get provTemp => _provTemp;

  get kotaTemp => _kotaTemp;

  get kecamatanTemp => _kecamatanTemp;

  get kelurahanTemp => _kelurahanTemp;

  get rwTemp => _rwTemp;

  get rtTemp => _rtTemp;

  get addressTypeTemp => _addressTypeTemp;

  get addressTemp => _addressTemp;

  get birthPlaceTemp => _birthPlaceTemp;

  get birthDateTemp => _birthDateTemp;

  get fullNameTemp => _fullNameTemp;

  get postalCodeTemp => _postalCodeTemp;

  get fullNameIdentityTemp => _fullNameIdentityTemp;

  String get identiyNumberTemp => _identiyNumberTemp;

  IdentityModel get identityModelTemp => _identityModelTemp;

  RelationshipStatusModel get relationshipStatusModelTemp => _relationshipStatusModelTemp;

  int get sizeList => _sizeList;

  int get oldSelectedIndex => _oldSelectedIndex;

  String get radioValueGender => _radioValueGender;

  set radioValueGender(String value) {
    this._radioValueGender = value;
    notifyListeners();
  }

  void iconShowDialog(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans"
            ),
            child: AlertDialog(
              title: Text("Information", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(
                    "∙ Tekan 1x untuk edit",
                  ),
                  Text(
                    "∙ Tekan lama untuk memilih alamat korespondensi",
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height/37,),
                  Text("* Tekan icon pada pojok kanan atas untuk melihat kembali",style: TextStyle(fontSize: 12, color: Colors.grey))
                ],
              ),
              actions: <Widget>[
                FlatButton(
                    onPressed: (){
                      Navigator.pop(context);
                      // _updateStatusShowDialogSimilarity();
                    },
                    child: Text(
                        "CLOSE",
                        style: TextStyle(
                            color: primaryOrange,
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 1.25
                        )
                    )
                )
              ],
            ),
          );
        }
    );
  }

  void isShowDialog(BuildContext context) {
    if(this.listGuarantorAddress.length == 1 || this.listGuarantorAddress.length == 2) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context){
            return Theme(
              data: ThemeData(
                  fontFamily: "NunitoSans"
              ),
              child: AlertDialog(
                title: Text("Information", style: TextStyle(fontWeight: FontWeight.bold)),
                content: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      "∙ Tekan 1x untuk edit",
                    ),
                    Text(
                      "∙ Tekan lama untuk memilih alamat korespondensi",
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height/37,),
                    Text("* Tekan icon pada pojok kanan atas untuk melihat kembali",style: TextStyle(fontSize: 12, color: Colors.grey))
                  ],
                ),
                actions: <Widget>[
                  FlatButton(
                      onPressed: (){
                        Navigator.pop(context);
                        // _updateStatusShowDialogSimilarity();
                      },
                      child: Text(
                          "CLOSE",
                          style: TextStyle(
                              color: primaryOrange,
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 1.25
                          )
                      )
                  )
                ],
              ),
            );
          }
      );
    }
  }

  void searchBirthPlace(BuildContext context) async{
    BirthPlaceModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchBirthPlaceChangeNotifier(),
                child: SearchBirthPlace())));
    if (data != null) {
      this._birthPlaceSelected = data;
      this._controllerBirthPlaceIdentityLOV.text = "${data.KABKOT_ID} - ${data.KABKOT_NAME}";
      notifyListeners();
    } else {
      return;
    }
  }

  void moreDialog(BuildContext context, index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans"
            ),
            child: AlertDialog(
              title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.check,
                          color: Colors.green,
                          size: 22.0,
                        ),
                        SizedBox(width: 12.0),
                        Expanded(
                          child: GestureDetector(
                            onTap: (){
                              selectedIndex = index;
                              controllerAddress.clear();
                              setCorrespondenceAddress(listGuarantorAddress[index], index);
                              Navigator.pop(context);
                              Navigator.pop(context);
                            },
                            child: Text(
                              "Pilih sebagai Alamat Korespondensi",
                              style: TextStyle(fontSize: 14.0),
                            ),
                          ),
                        )
                      ]
                  ),
                  SizedBox(height: 12.0),
                  listGuarantorAddress[index].jenisAlamatModel.KODE != "03" ?
                      // ? listGuarantorAddress[index].isSameWithIdentity
                      // ? SizedBox()
                      // :
                  Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.delete,
                          color: Colors.red,
                          size: 22.0,
                        ),
                        SizedBox(
                          width: 12.0,
                        ),
                        GestureDetector(
                          onTap: (){
                            Navigator.pop(context);
                            deleteListOccupationAddress(context, index);
                          },
                          child: Text(
                            "Hapus",
                            style: TextStyle(fontSize: 14.0, color: Colors.red),
                          ),
                        )
                      ]
                  )
                      : SizedBox(),
                ],
              ),
              actions: <Widget>[
                FlatButton(
                    onPressed: (){
                      Navigator.pop(context);
                      // _updateStatusShowDialogSimilarity();
                    },
                    child: Text(
                        "CLOSE",
                        style: TextStyle(
                            color: primaryOrange,
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 1.25
                        )
                    )
                )
              ],
            ),
          );
        }
    );
  }

  void getDataFromDashboard(BuildContext context){
    _setGuarantorIndividu = Provider.of<DashboardChangeNotif>(context, listen: false).setGuarantorIndividu;
    _setGuarantorIndividuCompany = Provider.of<DashboardChangeNotif>(context, listen: false).setGuarantorIndividuCompany;
  }

  bool isRelationshipStatusVisible() => _custType == "PER" ? _setGuarantorIndividu.relationshipStatusVisibility : _setGuarantorIndividuCompany.relationshipStatusVisibility;
  bool isIdentityTypeVisible() => _custType == "PER" ? _setGuarantorIndividu.identityTypeVisibility : _setGuarantorIndividuCompany.identityTypeVisibility;
  bool isIdentityNoVisible() => _custType == "PER" ? _setGuarantorIndividu.identityNoVisibility : _setGuarantorIndividuCompany.identityNoVisibility;
  bool isFullnameIDVisible() => _custType == "PER" ? _setGuarantorIndividu.fullnameIDVisibility : _setGuarantorIndividuCompany.fullnameIDVisibility;
  bool isFullNameVisible() => _custType == "PER" ? _setGuarantorIndividu.fullNameVisibility : _setGuarantorIndividuCompany.fullNameVisibility;
  bool isDateOfBirthVisible() => _custType == "PER" ? _setGuarantorIndividu.dateOfBirthVisibility : _setGuarantorIndividuCompany.dateOfBirthVisibility;
  bool isPlaceOfBirthVisible() => _custType == "PER" ? _setGuarantorIndividu.placeOfBirthVisibility : _setGuarantorIndividuCompany.placeOfBirthVisibility;
  bool isPlaceOfBirthLOVVisible() => _custType == "PER" ? _setGuarantorIndividu.placeOfBirthLOVVisibility : _setGuarantorIndividuCompany.placeOfBirthLOVVisibility;
  bool isGenderVisible() => _custType == "PER" ? _setGuarantorIndividu.genderVisibility : _setGuarantorIndividuCompany.genderVisibility;
  bool isPhoneVisible() => _custType == "PER" ? _setGuarantorIndividu.phoneVisibility : _setGuarantorIndividuCompany.phoneVisibility;

  bool isRelationshipStatusMandatory() => _custType == "PER" ? _setGuarantorIndividu.relationshipStatusMandatory : _setGuarantorIndividuCompany.relationshipStatusMandatory;
  bool isIdentityTypeMandatory() => _custType == "PER" ? _setGuarantorIndividu.identityTypeMandatory : _setGuarantorIndividuCompany.identityTypeMandatory;
  bool isIdentityNoMandatory() => _custType == "PER" ? _setGuarantorIndividu.identityNoMandatory : _setGuarantorIndividuCompany.identityNoMandatory;
  bool isFullnameIDMandatory() => _custType == "PER" ? _setGuarantorIndividu.fullnameIDMandatory : _setGuarantorIndividuCompany.fullnameIDMandatory;
  bool isFullNameMandatory() => _custType == "PER" ? _setGuarantorIndividu.fullNameMandatory : _setGuarantorIndividuCompany.fullNameMandatory;
  bool isDateOfBirthMandatory() => _custType == "PER" ? _setGuarantorIndividu.dateOfBirthMandatory : _setGuarantorIndividuCompany.dateOfBirthMandatory;
  bool isPlaceOfBirthMandatory() => _custType == "PER" ? _setGuarantorIndividu.placeOfBirthMandatory : _setGuarantorIndividuCompany.placeOfBirthMandatory;
  bool isPlaceOfBirthLOVMandatory() => _custType == "PER" ? _setGuarantorIndividu.placeOfBirthLOVMandatory : _setGuarantorIndividuCompany.placeOfBirthLOVMandatory;
  bool isGenderMandatory() => _custType == "PER" ? _setGuarantorIndividu.genderMandatory : _setGuarantorIndividuCompany.genderMandatory;
  bool isPhoneMandatory() => _custType == "PER" ? _setGuarantorIndividu.phoneMandatory : _setGuarantorIndividuCompany.phoneMandatory;

  void checkDataDakor(int index) async {
    SharedPreferences _preference = await SharedPreferences.getInstance();
    if(_preference.getString("last_known_state") == "DKR"){
      List _data = await _dbHelper.selectGuarantorInd();
      relationshipStatusDakor = _relationshipStatusModelSelected.PARA_FAMILY_TYPE_NAME != _data[index]['relation_status_desc'] || _data[index]['edit_relation_status_desc'] == "1";
      identityTypeDakor = _identityModelSelected.id != _data[index]['id_type'] || _data[index]['edit_id_type'] == "1";
      identityNoDakor =  this._controllerIdentityNumber.text != _data[index]['id_no'] || _data[index]['edit_id_no'] == "1";
      fullnameIDDakor =  this._controllerFullNameIdentity.text != _data[index]['full_name_id'] || _data[index]['edit_full_name_id'] == "1";
      fullNameDakor = this._controllerFullName.text != _data[index]['full_name'] || _data[index]['edit_full_name'] == "1";
      dateOfBirthDakor =  this._initialDateForBirthDate != DateTime.parse(_data[index]['date_of_birth']) || _data[index]['edit_date_of_birth'] == "1";
      placeOfBirthDakor = this._controllerBirthPlaceIdentity.text != _data[index]['place_of_birth'] || _data[index]['edit_place_of_birth'] == "1";
      placeOfBirthLOVDakor = this._birthPlaceSelected.KABKOT_ID != _data[index]['place_of_birth_kabkota'] || _data[index]['edit_place_of_birth_kabkota'] == "1";
      genderDakor = this._radioValueGender != _data[index]['gender'] || _data[index]['edit_gender'] == "1";
      phoneDakor = this._controllerCellPhoneNumber.text != _data[index]['handphone_no'] || _data[index]['edit_handphone_no'] == "1";
      if(relationshipStatusDakor || identityTypeDakor || identityNoDakor || fullnameIDDakor ||
          fullNameDakor || dateOfBirthDakor ||placeOfBirthDakor || placeOfBirthLOVDakor ||
          genderDakor || phoneDakor){
        isEditList = true;
      } else {
        isEditList = false;
      }
      notifyListeners();
    }
  }

  bool get isEditList => _isEditList;

  set isEditList(bool value) {
    _isEditList = value;
    notifyListeners();
  }

  bool get phoneDakor => _phoneDakor;

  set phoneDakor(bool value) {
    _phoneDakor = value;
    notifyListeners();
  }

  bool get genderDakor => _genderDakor;

  set genderDakor(bool value) {
    _genderDakor = value;
    notifyListeners();
  }

  bool get placeOfBirthLOVDakor => _placeOfBirthLOVDakor;

  set placeOfBirthLOVDakor(bool value) {
    _placeOfBirthLOVDakor = value;
    notifyListeners();
  }

  bool get placeOfBirthDakor => _placeOfBirthDakor;

  set placeOfBirthDakor(bool value) {
    _placeOfBirthDakor = value;
    notifyListeners();
  }

  bool get dateOfBirthDakor => _dateOfBirthDakor;

  set dateOfBirthDakor(bool value) {
    _dateOfBirthDakor = value;
    notifyListeners();
  }

  bool get fullNameDakor => _fullNameDakor;

  set fullNameDakor(bool value) {
    _fullNameDakor = value;
    notifyListeners();
  }

  bool get fullnameIDDakor => _fullnameIDDakor;

  set fullnameIDDakor(bool value) {
    _fullnameIDDakor = value;
    notifyListeners();
  }

  bool get identityNoDakor => _identityNoDakor;

  set identityNoDakor(bool value) {
    _identityNoDakor = value;
    notifyListeners();
  }

  bool get identityTypeDakor => _identityTypeDakor;

  set identityTypeDakor(bool value) {
    _identityTypeDakor = value;
    notifyListeners();
  }

  bool get relationshipStatusDakor => _relationshipStatusDakor;

  set relationshipStatusDakor(bool value) {
    _relationshipStatusDakor = value;
    notifyListeners();
  }

  void checkValidNoHP(String value) {
    if (value == "08") {
      Future.delayed(Duration(milliseconds: 100), () {
        this.controllerCellPhoneNumber.clear();
      });
    } else {
      return;
    }
  }
}
