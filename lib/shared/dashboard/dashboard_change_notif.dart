import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/address_individu/set_address_individu.dart';
import 'package:ad1ms2_dev/models/show_mandatory_income_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_asuransi_tambahan_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_colla_oto_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_colla_properti_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_dokumen_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_karoseri_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_object_unit_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_salesman_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_struktur_kredit_biaya_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_struktur_kredit_bp_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_struktur_kredit_floating_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_struktur_kredit_gp_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_struktur_kredit_irreguler_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_struktur_kredit_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_struktur_kredit_stepping_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_subsidi_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_wmp_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_marketing_notes_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_visible_resultgroupnote.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_app_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_m_foto_model.dart';
import 'package:ad1ms2_dev/models/showhide_mandatory_visibility/set_add_info_keluarga.dart';
import 'package:ad1ms2_dev/models/show_mandatory_occupation_model.dart';
import 'package:ad1ms2_dev/models/showhide_mandatory_visibility/set_address_company_model.dart';
import 'package:ad1ms2_dev/models/showhide_mandatory_visibility/set_guarantor_company_model.dart';
import 'package:ad1ms2_dev/models/showhide_mandatory_visibility/set_guarantor_individu_model.dart';
import 'package:ad1ms2_dev/models/showhide_mandatory_visibility/set_income_company_model.dart';
import 'package:ad1ms2_dev/models/showhide_mandatory_visibility/set_major_insurance_model.dart';
import 'package:ad1ms2_dev/models/showhide_mandatory_visibility/set_management_pic_company_model.dart';
import 'package:ad1ms2_dev/models/showhide_mandatory_visibility/set_pemegang_saham_individu_company_model.dart';
import 'package:ad1ms2_dev/models/showhide_mandatory_visibility/set_pemegang_saham_lembaga_company_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_keluarga(ibu)_change_notif.dart';
import 'package:ad1ms2_dev/shared/resource/get_set_vme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../form_m_company_rincian_change_notif.dart';
import '../form_m_info_nasabah_change_notif.dart';

class DashboardChangeNotif with ChangeNotifier{
  //individu
  SetAddInfoKeluargaModel _setAddInfoKeluarga;

  //occupation
  ShowMandatoryOccupationModel _showMandatoryOccupationModel;

  //address individu
  SetAddressIndividu _setAddressIndividuOccupation,_setAddressIndividuInfoNasabah, _setAddressIndividuPenjamin, _setAddressIndividuPenjaminCompany, _setAddressIndividuManajemenPIC, _setAddressIndividuPemegangSaham;

  //income
  ShowMandatoryIncomeModel _showMandatoryIncomeModel;

  ShowMandatoryFotoModel _showMandatoryFotoModel;

  // Inf Aplikasi
  ShowMandatoryInfoAppModel _showMandatoryInfoAppIdeModel;
  ShowMandatoryInfoAppModel _showMandatoryInfoAppIdeCompanyModel;
  ShowMandatoryInfoAppModel _showMandatoryInfoAppRegulerSurveyModel;
  ShowMandatoryInfoAppModel _showMandatoryInfoAppRegulerSurveyCompanyModel;
  ShowMandatoryInfoAppModel _showMandatoryInfoAppPacModel;
  ShowMandatoryInfoAppModel _showMandatoryInfoAppPacCompanyModel;
  ShowMandatoryInfoAppModel _showMandatoryInfoAppResurveyModel;
  ShowMandatoryInfoAppModel _showMandatoryInfoAppResurveyCompanyModel;
  ShowMandatoryInfoAppModel _showMandatoryInfoAppDakorModel;
  ShowMandatoryInfoAppModel _showMandatoryInfoAppDakorCompanyModel;

  // Inf Object Unit
  ShowMandatoryInfoObjectUnitModel _showMandatoryInfoObjectUnitIdeModel;
  ShowMandatoryInfoObjectUnitModel _showMandatoryInfoObjectUnitIdeCompanyModel;
  ShowMandatoryInfoObjectUnitModel _showMandatoryInfoObjectUnitRegulerSurveyModel;
  ShowMandatoryInfoObjectUnitModel _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel;
  ShowMandatoryInfoObjectUnitModel _showMandatoryInfoObjectUnitPacModel;
  ShowMandatoryInfoObjectUnitModel _showMandatoryInfoObjectUnitPacCompanyModel;
  ShowMandatoryInfoObjectUnitModel _showMandatoryInfoObjectUnitResurveyModel;
  ShowMandatoryInfoObjectUnitModel _showMandatoryInfoObjectUnitResurveyCompanyModel;
  ShowMandatoryInfoObjectUnitModel _showMandatoryInfoObjectUnitDakorModel;
  ShowMandatoryInfoObjectUnitModel _showMandatoryInfoObjectUnitDakorCompanyModel;

  // Inf Salesman
  ShowMandatoryInfoSalesmanModel _showMandatoryInfoSalesmanIdeModel;
  ShowMandatoryInfoSalesmanModel _showMandatoryInfoSalesmanIdeCompanyModel;
  ShowMandatoryInfoSalesmanModel _showMandatoryInfoSalesmanRegulerSurveyModel;
  ShowMandatoryInfoSalesmanModel _showMandatoryInfoSalesmanRegulerSurveyCompanyModel;
  ShowMandatoryInfoSalesmanModel _showMandatoryInfoSalesmanPacModel;
  ShowMandatoryInfoSalesmanModel _showMandatoryInfoSalesmanPacCompanyModel;
  ShowMandatoryInfoSalesmanModel _showMandatoryInfoSalesmanResurveyModel;
  ShowMandatoryInfoSalesmanModel _showMandatoryInfoSalesmanResurveyCompanyModel;
  ShowMandatoryInfoSalesmanModel _showMandatoryInfoSalesmanDakorModel;
  ShowMandatoryInfoSalesmanModel _showMandatoryInfoSalesmanDakorCompanyModel;

  // Inf Colla Oto
  ShowMandatoryInfoCollaOtoModel _showMandatoryInfoCollaOtoIdeModel;
  ShowMandatoryInfoCollaOtoModel _showMandatoryInfoCollaOtoIdeCompanyModel;
  ShowMandatoryInfoCollaOtoModel _showMandatoryInfoCollaOtoRegulerSurveyModel;
  ShowMandatoryInfoCollaOtoModel _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel;
  ShowMandatoryInfoCollaOtoModel _showMandatoryInfoCollaOtoPacModel;
  ShowMandatoryInfoCollaOtoModel _showMandatoryInfoCollaOtoPacCompanyModel;
  ShowMandatoryInfoCollaOtoModel _showMandatoryInfoCollaOtoResurveyModel;
  ShowMandatoryInfoCollaOtoModel _showMandatoryInfoCollaOtoResurveyCompanyModel;
  ShowMandatoryInfoCollaOtoModel _showMandatoryInfoCollaOtoDakorModel;
  ShowMandatoryInfoCollaOtoModel _showMandatoryInfoCollaOtoDakorCompanyModel;

  // Inf Colla Prop
  ShowMandatoryInfoCollaPropModel _showMandatoryInfoCollaPropIdeModel;
  ShowMandatoryInfoCollaPropModel _showMandatoryInfoCollaPropIdeCompanyModel;
  ShowMandatoryInfoCollaPropModel _showMandatoryInfoCollaPropRegulerSurveyModel;
  ShowMandatoryInfoCollaPropModel _showMandatoryInfoCollaPropRegulerSurveyCompanyModel;
  ShowMandatoryInfoCollaPropModel _showMandatoryInfoCollaPropPacModel;
  ShowMandatoryInfoCollaPropModel _showMandatoryInfoCollaPropPacCompanyModel;
  ShowMandatoryInfoCollaPropModel _showMandatoryInfoCollaPropResurveyModel;
  ShowMandatoryInfoCollaPropModel _showMandatoryInfoCollaPropResurveyCompanyModel;
  ShowMandatoryInfoCollaPropModel _showMandatoryInfoCollaPropDakorModel;
  ShowMandatoryInfoCollaPropModel _showMandatoryInfoCollaPropDakorCompanyModel;

  // Inf Karoseri
  ShowMandatoryInfoKaroseriModel _showMandatoryInfoKaroseriIdeModel;
  ShowMandatoryInfoKaroseriModel _showMandatoryInfoKaroseriIdeCompanyModel;
  ShowMandatoryInfoKaroseriModel _showMandatoryInfoKaroseriRegulerSurveyModel;
  ShowMandatoryInfoKaroseriModel _showMandatoryInfoKaroseriRegulerSurveyCompanyModel;
  ShowMandatoryInfoKaroseriModel _showMandatoryInfoKaroseriPacModel;
  ShowMandatoryInfoKaroseriModel _showMandatoryInfoKaroseriPacCompanyModel;
  ShowMandatoryInfoKaroseriModel _showMandatoryInfoKaroseriResurveyModel;
  ShowMandatoryInfoKaroseriModel _showMandatoryInfoKaroseriResurveyCompanyModel;
  ShowMandatoryInfoKaroseriModel _showMandatoryInfoKaroseriDakorModel;
  ShowMandatoryInfoKaroseriModel _showMandatoryInfoKaroseriDakorCompanyModel;

  // Inf Struktur Kredit
  ShowMandatoryInfoStrukturKreditModel _showMandatoryInfoStrukturKreditIdeModel;
  ShowMandatoryInfoStrukturKreditModel _showMandatoryInfoStrukturKreditIdeCompanyModel;
  ShowMandatoryInfoStrukturKreditModel _showMandatoryInfoStrukturKreditRegulerSurveyModel;
  ShowMandatoryInfoStrukturKreditModel _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel;
  ShowMandatoryInfoStrukturKreditModel _showMandatoryInfoStrukturKreditPacModel;
  ShowMandatoryInfoStrukturKreditModel _showMandatoryInfoStrukturKreditPacCompanyModel;
  ShowMandatoryInfoStrukturKreditModel _showMandatoryInfoStrukturKreditResurveyModel;
  ShowMandatoryInfoStrukturKreditModel _showMandatoryInfoStrukturKreditResurveyCompanyModel;
  ShowMandatoryInfoStrukturKreditModel _showMandatoryInfoStrukturKreditDakorModel;
  ShowMandatoryInfoStrukturKreditModel _showMandatoryInfoStrukturKreditDakorCompanyModel;

  // Inf Struktur Kredit Biaya
  ShowMandatoryInfoStrukturKreditBiayaModel _showMandatoryInfoStrukturKreditBiayaIdeModel;
  ShowMandatoryInfoStrukturKreditBiayaModel _showMandatoryInfoStrukturKreditBiayaIdeCompanyModel;
  ShowMandatoryInfoStrukturKreditBiayaModel _showMandatoryInfoStrukturKreditBiayaRegulerSurveyModel;
  ShowMandatoryInfoStrukturKreditBiayaModel _showMandatoryInfoStrukturKreditBiayaRegulerSurveyCompanyModel;
  ShowMandatoryInfoStrukturKreditBiayaModel _showMandatoryInfoStrukturKreditBiayaPacModel;
  ShowMandatoryInfoStrukturKreditBiayaModel _showMandatoryInfoStrukturKreditBiayaPacCompanyModel;
  ShowMandatoryInfoStrukturKreditBiayaModel _showMandatoryInfoStrukturKreditBiayaResurveyModel;
  ShowMandatoryInfoStrukturKreditBiayaModel _showMandatoryInfoStrukturKreditBiayaResurveyCompanyModel;
  ShowMandatoryInfoStrukturKreditBiayaModel _showMandatoryInfoStrukturKreditBiayaDakorModel;
  ShowMandatoryInfoStrukturKreditBiayaModel _showMandatoryInfoStrukturKreditBiayaDakorCompanyModel;

  // Inf Struktur Kredit Floating
  ShowMandatoryInfoStrukturKreditFloatingModel _showMandatoryInfoStrukturKreditFloatingIdeModel;
  ShowMandatoryInfoStrukturKreditFloatingModel _showMandatoryInfoStrukturKreditFloatingIdeCompanyModel;
  ShowMandatoryInfoStrukturKreditFloatingModel _showMandatoryInfoStrukturKreditFloatingRegulerSurveyModel;
  ShowMandatoryInfoStrukturKreditFloatingModel _showMandatoryInfoStrukturKreditFloatingRegulerSurveyCompanyModel;
  ShowMandatoryInfoStrukturKreditFloatingModel _showMandatoryInfoStrukturKreditFloatingPacModel;
  ShowMandatoryInfoStrukturKreditFloatingModel _showMandatoryInfoStrukturKreditFloatingPacCompanyModel;
  ShowMandatoryInfoStrukturKreditFloatingModel _showMandatoryInfoStrukturKreditFloatingResurveyModel;
  ShowMandatoryInfoStrukturKreditFloatingModel _showMandatoryInfoStrukturKreditFloatingResurveyCompanyModel;
  ShowMandatoryInfoStrukturKreditFloatingModel _showMandatoryInfoStrukturKreditFloatingDakorModel;
  ShowMandatoryInfoStrukturKreditFloatingModel _showMandatoryInfoStrukturKreditFloatingDakorCompanyModel;

  // Inf Struktur Kredit GP
  ShowMandatoryInfoStrukturKreditGPModel _showMandatoryInfoStrukturKreditGPIdeModel;
  ShowMandatoryInfoStrukturKreditGPModel _showMandatoryInfoStrukturKreditGPIdeCompanyModel;
  ShowMandatoryInfoStrukturKreditGPModel _showMandatoryInfoStrukturKreditGPRegulerSurveyModel;
  ShowMandatoryInfoStrukturKreditGPModel _showMandatoryInfoStrukturKreditGPRegulerSurveyCompanyModel;
  ShowMandatoryInfoStrukturKreditGPModel _showMandatoryInfoStrukturKreditGPPacModel;
  ShowMandatoryInfoStrukturKreditGPModel _showMandatoryInfoStrukturKreditGPPacCompanyModel;
  ShowMandatoryInfoStrukturKreditGPModel _showMandatoryInfoStrukturKreditGPResurveyModel;
  ShowMandatoryInfoStrukturKreditGPModel _showMandatoryInfoStrukturKreditGPResurveyCompanyModel;
  ShowMandatoryInfoStrukturKreditGPModel _showMandatoryInfoStrukturKreditGPDakorModel;
  ShowMandatoryInfoStrukturKreditGPModel _showMandatoryInfoStrukturKreditGPDakorCompanyModel;

  // Inf Struktur Kredit Irreguler
  ShowMandatoryInfoStrukturKreditIrregulerModel _showMandatoryInfoStrukturKreditIrregulerIdeModel;
  ShowMandatoryInfoStrukturKreditIrregulerModel _showMandatoryInfoStrukturKreditIrregulerIdeCompanyModel;
  ShowMandatoryInfoStrukturKreditIrregulerModel _showMandatoryInfoStrukturKreditIrregulerRegulerSurveyModel;
  ShowMandatoryInfoStrukturKreditIrregulerModel _showMandatoryInfoStrukturKreditIrregulerRegulerSurveyCompanyModel;
  ShowMandatoryInfoStrukturKreditIrregulerModel _showMandatoryInfoStrukturKreditIrregulerPacModel;
  ShowMandatoryInfoStrukturKreditIrregulerModel _showMandatoryInfoStrukturKreditIrregulerPacCompanyModel;
  ShowMandatoryInfoStrukturKreditIrregulerModel _showMandatoryInfoStrukturKreditIrregulerResurveyModel;
  ShowMandatoryInfoStrukturKreditIrregulerModel _showMandatoryInfoStrukturKreditIrregulerResurveyCompanyModel;
  ShowMandatoryInfoStrukturKreditIrregulerModel _showMandatoryInfoStrukturKreditIrregulerDakorModel;
  ShowMandatoryInfoStrukturKreditIrregulerModel _showMandatoryInfoStrukturKreditIrregulerDakorCompanyModel;

  // Inf Struktur Kredit Stepping
  ShowMandatoryInfoStrukturKreditSteppingModel _showMandatoryInfoStrukturKreditSteppingIdeModel;
  ShowMandatoryInfoStrukturKreditSteppingModel _showMandatoryInfoStrukturKreditSteppingIdeCompanyModel;
  ShowMandatoryInfoStrukturKreditSteppingModel _showMandatoryInfoStrukturKreditSteppingRegulerSurveyModel;
  ShowMandatoryInfoStrukturKreditSteppingModel _showMandatoryInfoStrukturKreditSteppingRegulerSurveyCompanyModel;
  ShowMandatoryInfoStrukturKreditSteppingModel _showMandatoryInfoStrukturKreditSteppingPacModel;
  ShowMandatoryInfoStrukturKreditSteppingModel _showMandatoryInfoStrukturKreditSteppingPacCompanyModel;
  ShowMandatoryInfoStrukturKreditSteppingModel _showMandatoryInfoStrukturKreditSteppingResurveyModel;
  ShowMandatoryInfoStrukturKreditSteppingModel _showMandatoryInfoStrukturKreditSteppingResurveyCompanyModel;
  ShowMandatoryInfoStrukturKreditSteppingModel _showMandatoryInfoStrukturKreditSteppingDakorModel;
  ShowMandatoryInfoStrukturKreditSteppingModel _showMandatoryInfoStrukturKreditSteppingDakorCompanyModel;

  // Inf Struktur Kredit BP
  ShowMandatoryInfoStrukturKreditBPModel _showMandatoryInfoStrukturKreditBPIdeModel;
  ShowMandatoryInfoStrukturKreditBPModel _showMandatoryInfoStrukturKreditBPIdeCompanyModel;
  ShowMandatoryInfoStrukturKreditBPModel _showMandatoryInfoStrukturKreditBPRegulerSurveyModel;
  ShowMandatoryInfoStrukturKreditBPModel _showMandatoryInfoStrukturKreditBPRegulerSurveyCompanyModel;
  ShowMandatoryInfoStrukturKreditBPModel _showMandatoryInfoStrukturKreditBPPacModel;
  ShowMandatoryInfoStrukturKreditBPModel _showMandatoryInfoStrukturKreditBPPacCompanyModel;
  ShowMandatoryInfoStrukturKreditBPModel _showMandatoryInfoStrukturKreditBPResurveyModel;
  ShowMandatoryInfoStrukturKreditBPModel _showMandatoryInfoStrukturKreditBPResurveyCompanyModel;
  ShowMandatoryInfoStrukturKreditBPModel _showMandatoryInfoStrukturKreditBPDakorModel;
  ShowMandatoryInfoStrukturKreditBPModel _showMandatoryInfoStrukturKreditBPDakorCompanyModel;

  // //App Asuransi Utama
  // SetMajorInsuranceModel _setMajorInsuranceModel;

  // Inf Asuransi Utama
  SetMajorInsuranceModel _showMandatoryInfoAsuransiUtamaIdeModel;
  SetMajorInsuranceModel _showMandatoryInfoAsuransiUtamaIdeCompanyModel;
  SetMajorInsuranceModel _showMandatoryInfoAsuransiUtamaRegulerSurveyModel;
  SetMajorInsuranceModel _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel;
  SetMajorInsuranceModel _showMandatoryInfoAsuransiUtamaPacModel;
  SetMajorInsuranceModel _showMandatoryInfoAsuransiUtamaPacCompanyModel;
  SetMajorInsuranceModel _showMandatoryInfoAsuransiUtamaResurveyModel;
  SetMajorInsuranceModel _showMandatoryInfoAsuransiUtamaResurveyCompanyModel;
  SetMajorInsuranceModel _showMandatoryInfoAsuransiUtamaDakorModel;
  SetMajorInsuranceModel _showMandatoryInfoAsuransiUtamaDakorCompanyModel;

  // Inf Asuransi Tambahan
  ShowMandatoryInfoAsuransiTambahanModel _showMandatoryInfoAsuransiTambahanIdeModel;
  ShowMandatoryInfoAsuransiTambahanModel _showMandatoryInfoAsuransiTambahanIdeCompanyModel;
  ShowMandatoryInfoAsuransiTambahanModel _showMandatoryInfoAsuransiTambahanRegulerSurveyModel;
  ShowMandatoryInfoAsuransiTambahanModel _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel;
  ShowMandatoryInfoAsuransiTambahanModel _showMandatoryInfoAsuransiTambahanPacModel;
  ShowMandatoryInfoAsuransiTambahanModel _showMandatoryInfoAsuransiTambahanPacCompanyModel;
  ShowMandatoryInfoAsuransiTambahanModel _showMandatoryInfoAsuransiTambahanResurveyModel;
  ShowMandatoryInfoAsuransiTambahanModel _showMandatoryInfoAsuransiTambahanResurveyCompanyModel;
  ShowMandatoryInfoAsuransiTambahanModel _showMandatoryInfoAsuransiTambahanDakorModel;
  ShowMandatoryInfoAsuransiTambahanModel _showMandatoryInfoAsuransiTambahanDakorCompanyModel;

  // Info WMP
  ShowMandatoryInfoWMPModel _showMandatoryInfoWMPIdeModel;
  ShowMandatoryInfoWMPModel _showMandatoryInfoWMPIdeCompanyModel;
  ShowMandatoryInfoWMPModel _showMandatoryInfoWMPRegulerSurveyModel;
  ShowMandatoryInfoWMPModel _showMandatoryInfoWMPRegulerSurveyCompanyModel;
  ShowMandatoryInfoWMPModel _showMandatoryInfoWMPPacModel;
  ShowMandatoryInfoWMPModel _showMandatoryInfoWMPPacCompanyModel;
  ShowMandatoryInfoWMPModel _showMandatoryInfoWMPResurveyModel;
  ShowMandatoryInfoWMPModel _showMandatoryInfoWMPResurveyCompanyModel;
  ShowMandatoryInfoWMPModel _showMandatoryInfoWMPDakorModel;
  ShowMandatoryInfoWMPModel _showMandatoryInfoWMPDakorCompanyModel;

  // Inf Subsidi
  ShowMandatoryInfoSubsidiModel _showMandatoryInfoSubsidiIdeModel;
  ShowMandatoryInfoSubsidiModel _showMandatoryInfoSubsidiIdeCompanyModel;
  ShowMandatoryInfoSubsidiModel _showMandatoryInfoSubsidiRegulerSurveyModel;
  ShowMandatoryInfoSubsidiModel _showMandatoryInfoSubsidiRegulerSurveyCompanyModel;
  ShowMandatoryInfoSubsidiModel _showMandatoryInfoSubsidiPacModel;
  ShowMandatoryInfoSubsidiModel _showMandatoryInfoSubsidiPacCompanyModel;
  ShowMandatoryInfoSubsidiModel _showMandatoryInfoSubsidiResurveyModel;
  ShowMandatoryInfoSubsidiModel _showMandatoryInfoSubsidiResurveyCompanyModel;
  ShowMandatoryInfoSubsidiModel _showMandatoryInfoSubsidiDakorModel;
  ShowMandatoryInfoSubsidiModel _showMandatoryInfoSubsidiDakorCompanyModel;

  // Inf Dokumen
  ShowMandatoryInfoDokumenModel _showMandatoryInfoDokumenIdeModel;
  ShowMandatoryInfoDokumenModel _showMandatoryInfoDokumenIdeCompanyModel;
  ShowMandatoryInfoDokumenModel _showMandatoryInfoDokumenRegulerSurveyModel;
  ShowMandatoryInfoDokumenModel _showMandatoryInfoDokumenRegulerSurveyCompanyModel;
  ShowMandatoryInfoDokumenModel _showMandatoryInfoDokumenPacModel;
  ShowMandatoryInfoDokumenModel _showMandatoryInfoDokumenPacCompanyModel;
  ShowMandatoryInfoDokumenModel _showMandatoryInfoDokumenResurveyModel;
  ShowMandatoryInfoDokumenModel _showMandatoryInfoDokumenResurveyCompanyModel;
  ShowMandatoryInfoDokumenModel _showMandatoryInfoDokumenDakorModel;
  ShowMandatoryInfoDokumenModel _showMandatoryInfoDokumenDakorCompanyModel;

  // Marketing Notes
  ShowMandatoryMarketingNotesModel _showMandatoryMarketingNotesIdeModel;
  ShowMandatoryMarketingNotesModel _showMandatoryMarketingNotesIdeCompanyModel;
  ShowMandatoryMarketingNotesModel _showMandatoryMarketingNotesRegulerSurveyModel;
  ShowMandatoryMarketingNotesModel _showMandatoryMarketingNotesRegulerSurveyCompanyModel;
  ShowMandatoryMarketingNotesModel _showMandatoryMarketingNotesPacModel;
  ShowMandatoryMarketingNotesModel _showMandatoryMarketingNotesPacCompanyModel;
  ShowMandatoryMarketingNotesModel _showMandatoryMarketingNotesResurveyModel;
  ShowMandatoryMarketingNotesModel _showMandatoryMarketingNotesResurveyCompanyModel;
  ShowMandatoryMarketingNotesModel _showMandatoryMarketingNotesDakorModel;
  ShowMandatoryMarketingNotesModel _showMandatoryMarketingNotesDakorCompanyModel;

  //company
  SetIncomeCompanyModel _setIncomeCompanyModel;
  SetManagementPICCompanyModel _setManagementPICModel;
  //saham
  SetPemegangSahamIndividuCompanyModel _setPemegangSahamIndividuCompanyModel;
  SetPemegangSahamLembagaCompanyModel _setPemegangSahamLembagaCompanyModel;

  //address Company
  SetAddressCompanyModel _setAddressCompanyGuarantorIndividuModel, _setAddressCompanyGuarantorModel, _setAddressCompanyModel, _setAddressSahamLembagaCompanyModel;

  //guarantor
  SetGuarantorIndividu _setGuarantorIndividu, _setGuarantorIndividuCompany;
  SetGuarantorCompanyModel _setGuarantorLembaga, _setGuarantorLembagaCompany;

  // SetMajorInsuranceModel get setMajorInsuranceModel => _setMajorInsuranceModel;
  //
  // set setMajorInsuranceModel(SetMajorInsuranceModel value) {
  //     this._setMajorInsuranceModel = value;
  // }

  SetAddressCompanyModel get setAddressCompanyGuarantorIndividuModel => _setAddressCompanyGuarantorIndividuModel;

  set setAddressCompanyGuarantorIndividuModel(SetAddressCompanyModel value) {
    this._setAddressCompanyGuarantorIndividuModel = value;
  }

  get setGuarantorLembagaCompany => _setGuarantorLembagaCompany;

  set setGuarantorLembagaCompany(value) {
    this._setGuarantorLembagaCompany = value;
  }

  SetGuarantorCompanyModel get setGuarantorLembaga => _setGuarantorLembaga;

  set setGuarantorLembaga(SetGuarantorCompanyModel value) {
    this._setGuarantorLembaga = value;
  }

  get setAddressIndividuPenjaminCompany => _setAddressIndividuPenjaminCompany;

  set setAddressIndividuPenjaminCompany(value) {
    this._setAddressIndividuPenjaminCompany = value;
  }

  get setGuarantorIndividuCompany => _setGuarantorIndividuCompany;

  set setGuarantorIndividuCompany(value) {
    this._setGuarantorIndividuCompany = value;
  }

  SetGuarantorIndividu get setGuarantorIndividu => _setGuarantorIndividu;

  set setGuarantorIndividu(SetGuarantorIndividu value) {
    this._setGuarantorIndividu = value;
  }

  get setAddressIndividuPenjamin => _setAddressIndividuPenjamin;

  set setAddressIndividuPenjamin(value) {
    this._setAddressIndividuPenjamin = value;
  }

  get setAddressIndividuManajemenPIC => _setAddressIndividuManajemenPIC;

  set setAddressIndividuManajemenPIC(value) {
    this._setAddressIndividuManajemenPIC = value;
  }

  get setAddressIndividuPemegangSaham => _setAddressIndividuPemegangSaham;

  set setAddressIndividuPemegangSaham(value) {
    this._setAddressIndividuPemegangSaham = value;
  }

  //Result Survey Personal
  ShowMandatoryEnabledResultSurvey _showMandatoryResultSurveyRegularSurvey;
  ShowMandatoryEnabledResultSurvey _showMandatoryResultSurveyPAC;
  ShowMandatoryEnabledResultSurvey _showMandatoryResultSurveyResurvey;
  ShowMandatoryEnabledResultSurvey _showMandatoryResultSurveyDakor;


  ShowMandatoryEnabledResultSurvey get showMandatoryResultSurveyDakor => _showMandatoryResultSurveyDakor;

  set showMandatoryResultSurveyDakor(ShowMandatoryEnabledResultSurvey value) {
    this._showMandatoryResultSurveyDakor = value;
  }

  ShowMandatoryEnabledResultSurvey get showMandatoryResultSurveyResurvey => _showMandatoryResultSurveyResurvey;

  set showMandatoryResultSurveyResurvey(ShowMandatoryEnabledResultSurvey value) {
    this._showMandatoryResultSurveyResurvey = value;
  }

  ShowMandatoryEnabledResultSurvey get showMandatoryResultSurveyPAC => _showMandatoryResultSurveyPAC;

  set showMandatoryResultSurveyPAC(ShowMandatoryEnabledResultSurvey value) {
    this._showMandatoryResultSurveyPAC = value;
  }

  ShowMandatoryEnabledResultSurvey get showMandatoryResultGroupNoteRegularSurvey => _showMandatoryResultSurveyRegularSurvey;

  set showMandatoryResultGroupNoteRegularSurvey(ShowMandatoryEnabledResultSurvey value) {
    this._showMandatoryResultSurveyRegularSurvey = value;
  }

  //Result Survey Company

  ShowMandatoryEnabledResultSurvey _showMandatoryResultSurveyComRegularSurvey;
  ShowMandatoryEnabledResultSurvey _showMandatoryResultSurveyComPAC;
  ShowMandatoryEnabledResultSurvey _showMandatoryResultSurveyComResurvey;
  ShowMandatoryEnabledResultSurvey _showMandatoryResultSurveyComDakor;

  ShowMandatoryEnabledResultSurvey get showMandatoryResultSurveyComRegularSurvey => _showMandatoryResultSurveyComRegularSurvey;

  set showMandatoryResultSurveyComRegularSurvey(ShowMandatoryEnabledResultSurvey value) {
    this._showMandatoryResultSurveyComRegularSurvey = value;
  }

  ShowMandatoryEnabledResultSurvey get showMandatoryResultSurveyComPAC => _showMandatoryResultSurveyComPAC;

  set showMandatoryResultSurveyComPAC(ShowMandatoryEnabledResultSurvey value) {
    this._showMandatoryResultSurveyComPAC = value;
  }

  ShowMandatoryEnabledResultSurvey get showMandatoryResultSurveyComResurvey => _showMandatoryResultSurveyComResurvey;

  set showMandatoryResultSurveyComResurvey(ShowMandatoryEnabledResultSurvey value) {
    this._showMandatoryResultSurveyComResurvey = value;
  }

  ShowMandatoryEnabledResultSurvey get showMandatoryResultSurveyComDakor => _showMandatoryResultSurveyComDakor;

  set showMandatoryResultSurveyComDakor(ShowMandatoryEnabledResultSurvey value) {
    this._showMandatoryResultSurveyComDakor = value;
  }

  SetPemegangSahamIndividuCompanyModel get setPemegangSahamIndividuCompanyModel => _setPemegangSahamIndividuCompanyModel;

  set setPemegangSahamIndividuCompanyModel(SetPemegangSahamIndividuCompanyModel value) {
    this._setPemegangSahamIndividuCompanyModel = value;
  }

  SetPemegangSahamLembagaCompanyModel get setPemegangSahamLembagaCompanyModel => _setPemegangSahamLembagaCompanyModel;

  set setPemegangSahamLembagaCompanyModel(SetPemegangSahamLembagaCompanyModel value) {
    this._setPemegangSahamLembagaCompanyModel = value;
  }

  SetManagementPICCompanyModel get setManagementPICModel => _setManagementPICModel;

  set setManagementPICModel(SetManagementPICCompanyModel value) {
    this._setManagementPICModel = value;
  }

  SetIncomeCompanyModel get setIncomeCompanyModel => _setIncomeCompanyModel;

  set setIncomeCompanyModel(SetIncomeCompanyModel value) {
    this._setIncomeCompanyModel = value;
  }

  SetAddressCompanyModel get setAddressCompanyGuarantorModel => _setAddressCompanyGuarantorModel;

  set setAddressCompanyGuarantorModel(SetAddressCompanyModel value) {
    this._setAddressCompanyGuarantorModel = value;
  }

  SetAddressCompanyModel get setAddressCompanyModel => _setAddressCompanyModel;

  set setAddressCompanyModel(SetAddressCompanyModel value) {
    this._setAddressCompanyModel = value;
  }

  SetAddressCompanyModel get setAddressSahamLembagaCompanyModel =>_setAddressSahamLembagaCompanyModel;

  set setAddressSahamLembagaCompanyModel(SetAddressCompanyModel value) {
    this._setAddressSahamLembagaCompanyModel = value;
  }

  SetAddInfoKeluargaModel get setAddInfoKeluarga => _setAddInfoKeluarga;

  set setAddInfoKeluarga(SetAddInfoKeluargaModel value) {
    this._setAddInfoKeluarga = value;
  }

  SetAddressIndividu get setAddressIndividuOccupation => _setAddressIndividuOccupation;

  set setAddressIndividuOccupation(SetAddressIndividu value) {
    this._setAddressIndividuOccupation = value;
  }

  get setAddressIndividuInfoNasabah => _setAddressIndividuInfoNasabah;

  set setAddressIndividuInfoNasabah(value) {
    this._setAddressIndividuInfoNasabah = value;
  }

  ShowMandatoryOccupationModel get showMandatoryOccupationModel => _showMandatoryOccupationModel;

  set showMandatoryOccupationModel(ShowMandatoryOccupationModel value) {
    this._showMandatoryOccupationModel = value;
  }

  ShowMandatoryIncomeModel get showMandatoryIncomeModel => _showMandatoryIncomeModel;

  set showMandatoryIncomeModel(ShowMandatoryIncomeModel value) {
    this._showMandatoryIncomeModel = value;
  }

  ShowMandatoryFotoModel get showMandatoryFotoModel => _showMandatoryFotoModel;

  set showMandatoryFotoModel(ShowMandatoryFotoModel value) {
    this._showMandatoryFotoModel = value;
  }

  // Inf Aplikasi
  ShowMandatoryInfoAppModel get showMandatoryInfoAppIdeModel => _showMandatoryInfoAppIdeModel;

  set showMandatoryInfoAppIdeModel(ShowMandatoryInfoAppModel value) {
    this._showMandatoryInfoAppIdeModel = value;
  }

  ShowMandatoryInfoAppModel get showMandatoryInfoAppIdeCompanyModel => _showMandatoryInfoAppIdeCompanyModel;

  set showMandatoryInfoAppIdeModelCompany(ShowMandatoryInfoAppModel value) {
    this._showMandatoryInfoAppIdeCompanyModel = value;
  }

  ShowMandatoryInfoAppModel get showMandatoryInfoAppPacModel => _showMandatoryInfoAppPacModel;

  set showMandatoryInfoAppPacModel(ShowMandatoryInfoAppModel value) {
    this._showMandatoryInfoAppPacModel = value;
  }

  ShowMandatoryInfoAppModel get showMandatoryInfoAppRegulerSurveyModel => _showMandatoryInfoAppRegulerSurveyModel;

  set showMandatoryInfoAppRegulerSurveyModel(ShowMandatoryInfoAppModel value) {
    this._showMandatoryInfoAppRegulerSurveyModel = value;
  }

  ShowMandatoryInfoAppModel
  get showMandatoryInfoAppRegulerSurveyCompanyModel => _showMandatoryInfoAppRegulerSurveyCompanyModel;

  set showMandatoryInfoAppRegulerSurveyCompanyModel(ShowMandatoryInfoAppModel value) {
    this._showMandatoryInfoAppRegulerSurveyCompanyModel = value;
  }

  ShowMandatoryInfoAppModel get showMandatoryInfoAppPacCompanyModel => _showMandatoryInfoAppPacCompanyModel;

  set showMandatoryInfoAppPacCompanyModel(ShowMandatoryInfoAppModel value) {
    this._showMandatoryInfoAppPacCompanyModel = value;
  }

  ShowMandatoryInfoAppModel get showMandatoryInfoAppResurveyModel => _showMandatoryInfoAppResurveyModel;

  set showMandatoryInfoAppResurveyModel(ShowMandatoryInfoAppModel value) {
    this._showMandatoryInfoAppResurveyModel = value;
  }

  ShowMandatoryInfoAppModel get showMandatoryInfoAppResurveyCompanyModel => _showMandatoryInfoAppResurveyCompanyModel;

  set showMandatoryInfoAppResurveyCompanyModel(ShowMandatoryInfoAppModel value) {
    this._showMandatoryInfoAppResurveyCompanyModel = value;
  }

  ShowMandatoryInfoAppModel get showMandatoryInfoAppDakorModel => _showMandatoryInfoAppDakorModel;

  set showMandatoryInfoAppDakorModel(ShowMandatoryInfoAppModel value) {
    this._showMandatoryInfoAppDakorModel = value;
  }

  ShowMandatoryInfoAppModel get showMandatoryInfoAppDakorCompanyModel => _showMandatoryInfoAppDakorCompanyModel;

  set showMandatoryInfoAppDakorCompanyModel(ShowMandatoryInfoAppModel value) {
    this._showMandatoryInfoAppDakorCompanyModel = value;
  }
  // End Inf Aplikasi

  // Inf Object Unit
    ShowMandatoryInfoObjectUnitModel get showMandatoryInfoObjectUnitIdeModel => _showMandatoryInfoObjectUnitIdeModel;

    set showMandatoryInfoObjectUnitIdeModel(ShowMandatoryInfoObjectUnitModel value) {
      this._showMandatoryInfoObjectUnitIdeModel = value;
    }

    ShowMandatoryInfoObjectUnitModel get showMandatoryInfoObjectUnitIdeCompanyModel => _showMandatoryInfoObjectUnitIdeCompanyModel;

    set showMandatoryInfoObjectUnitIdeCompanyModel(ShowMandatoryInfoObjectUnitModel value) {
      this._showMandatoryInfoObjectUnitIdeCompanyModel = value;
    }

    ShowMandatoryInfoObjectUnitModel get showMandatoryInfoObjectUnitDakorCompanyModel => _showMandatoryInfoObjectUnitDakorCompanyModel;

    set showMandatoryInfoObjectUnitDakorCompanyModel(ShowMandatoryInfoObjectUnitModel value) {
      this._showMandatoryInfoObjectUnitDakorCompanyModel = value;
    }

    ShowMandatoryInfoObjectUnitModel get showMandatoryInfoObjectUnitDakorModel => _showMandatoryInfoObjectUnitDakorModel;

    set showMandatoryInfoObjectUnitDakorModel(ShowMandatoryInfoObjectUnitModel value) {
      this._showMandatoryInfoObjectUnitDakorModel = value;
    }

    ShowMandatoryInfoObjectUnitModel get showMandatoryInfoObjectUnitResurveyCompanyModel => _showMandatoryInfoObjectUnitResurveyCompanyModel;

    set showMandatoryInfoObjectUnitResurveyCompanyModel(ShowMandatoryInfoObjectUnitModel value) {
      this._showMandatoryInfoObjectUnitResurveyCompanyModel = value;
    }

    ShowMandatoryInfoObjectUnitModel
    get showMandatoryInfoObjectUnitResurveyModel => _showMandatoryInfoObjectUnitResurveyModel;

    set showMandatoryInfoObjectUnitResurveyModel(ShowMandatoryInfoObjectUnitModel value) {
      this._showMandatoryInfoObjectUnitResurveyModel = value;
    }

    ShowMandatoryInfoObjectUnitModel get showMandatoryInfoObjectUnitPacCompanyModel => _showMandatoryInfoObjectUnitPacCompanyModel;

    set showMandatoryInfoObjectUnitPacCompanyModel(ShowMandatoryInfoObjectUnitModel value) {
      this._showMandatoryInfoObjectUnitPacCompanyModel = value;
    }

    ShowMandatoryInfoObjectUnitModel get showMandatoryInfoObjectUnitPacModel => _showMandatoryInfoObjectUnitPacModel;

    set showMandatoryInfoObjectUnitPacModel(ShowMandatoryInfoObjectUnitModel value) {
      this._showMandatoryInfoObjectUnitPacModel = value;
    }

    ShowMandatoryInfoObjectUnitModel get showMandatoryInfoObjectUnitRegulerSurveyCompanyModel => _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel;

    set showMandatoryInfoObjectUnitRegulerSurveyCompanyModel(ShowMandatoryInfoObjectUnitModel value) {
      this._showMandatoryInfoObjectUnitRegulerSurveyCompanyModel = value;
    }

    ShowMandatoryInfoObjectUnitModel get showMandatoryInfoObjectUnitRegulerSurveyModel => _showMandatoryInfoObjectUnitRegulerSurveyModel;

    set showMandatoryInfoObjectUnitRegulerSurveyModel(ShowMandatoryInfoObjectUnitModel value) {
      this._showMandatoryInfoObjectUnitRegulerSurveyModel = value;
    }
  // End Inf Object Unit

  // Inf Salesman
  ShowMandatoryInfoSalesmanModel get showMandatoryInfoSalesmanIdeModel => _showMandatoryInfoSalesmanIdeModel;

  set showMandatoryInfoSalesmanIdeModel(ShowMandatoryInfoSalesmanModel value) {
    this._showMandatoryInfoSalesmanIdeModel = value;
  }

  ShowMandatoryInfoSalesmanModel get showMandatoryInfoSalesmanIdeCompanyModel => _showMandatoryInfoSalesmanIdeCompanyModel;

  set showMandatoryInfoSalesmanIdeCompanyModel(ShowMandatoryInfoSalesmanModel value) {
    this._showMandatoryInfoSalesmanIdeCompanyModel = value;
  }

  ShowMandatoryInfoSalesmanModel get showMandatoryInfoSalesmanRegulerSurveyModel => _showMandatoryInfoSalesmanRegulerSurveyModel;

  set showMandatoryInfoSalesmanRegulerSurveyModel(ShowMandatoryInfoSalesmanModel value) {
    this._showMandatoryInfoSalesmanRegulerSurveyModel = value;
  }

  ShowMandatoryInfoSalesmanModel get showMandatoryInfoSalesmanRegulerSurveyCompanyModel => _showMandatoryInfoSalesmanRegulerSurveyCompanyModel;

  set showMandatoryInfoSalesmanRegulerSurveyCompanyModel(ShowMandatoryInfoSalesmanModel value) {
    this._showMandatoryInfoSalesmanRegulerSurveyCompanyModel = value;
  }

  ShowMandatoryInfoSalesmanModel get showMandatoryInfoSalesmanPacModel => _showMandatoryInfoSalesmanPacModel;

  set showMandatoryInfoSalesmanPacModel(ShowMandatoryInfoSalesmanModel value) {
    this._showMandatoryInfoSalesmanPacModel = value;
  }

  ShowMandatoryInfoSalesmanModel get showMandatoryInfoSalesmanPacCompanyModel => _showMandatoryInfoSalesmanPacCompanyModel;

  set showMandatoryInfoSalesmanPacCompanyModel(ShowMandatoryInfoSalesmanModel value) {
    this._showMandatoryInfoSalesmanPacCompanyModel = value;
  }

  ShowMandatoryInfoSalesmanModel get showMandatoryInfoSalesmanResurveyModel => _showMandatoryInfoSalesmanResurveyModel;

  set showMandatoryInfoSalesmanResurveyModel(ShowMandatoryInfoSalesmanModel value) {
    this._showMandatoryInfoSalesmanResurveyModel = value;
  }

  ShowMandatoryInfoSalesmanModel get showMandatoryInfoSalesmanResurveyCompanyModel => _showMandatoryInfoSalesmanResurveyCompanyModel;

  set showMandatoryInfoSalesmanResurveyCompanyModel(ShowMandatoryInfoSalesmanModel value) {
    this._showMandatoryInfoSalesmanResurveyCompanyModel = value;
  }

  ShowMandatoryInfoSalesmanModel get showMandatoryInfoSalesmanDakorModel => _showMandatoryInfoSalesmanDakorModel;

  set showMandatoryInfoSalesmanDakorModel(ShowMandatoryInfoSalesmanModel value) {
    this._showMandatoryInfoSalesmanDakorModel = value;
  }

  ShowMandatoryInfoSalesmanModel get showMandatoryInfoSalesmanDakorCompanyModel => _showMandatoryInfoSalesmanDakorCompanyModel;

  set showMandatoryInfoSalesmanDakorCompanyModel(ShowMandatoryInfoSalesmanModel value) {
    this._showMandatoryInfoSalesmanDakorCompanyModel = value;
  }
  // End Inf Salesman

  // Inf Colla Oto
  ShowMandatoryInfoCollaOtoModel get showMandatoryInfoCollaOtoDakorCompanyModel => _showMandatoryInfoCollaOtoDakorCompanyModel;

  set showMandatoryInfoCollaOtoDakorCompanyModel(ShowMandatoryInfoCollaOtoModel value) {
    this._showMandatoryInfoCollaOtoDakorCompanyModel = value;
  }

  ShowMandatoryInfoCollaOtoModel get showMandatoryInfoCollaOtoDakorModel => _showMandatoryInfoCollaOtoDakorModel;

  set showMandatoryInfoCollaOtoDakorModel(ShowMandatoryInfoCollaOtoModel value) {
    this._showMandatoryInfoCollaOtoDakorModel = value;
  }

  ShowMandatoryInfoCollaOtoModel get showMandatoryInfoCollaOtoResurveyCompanyModel => _showMandatoryInfoCollaOtoResurveyCompanyModel;

  set showMandatoryInfoCollaOtoResurveyCompanyModel(ShowMandatoryInfoCollaOtoModel value) {
    this._showMandatoryInfoCollaOtoResurveyCompanyModel = value;
  }

  ShowMandatoryInfoCollaOtoModel get showMandatoryInfoCollaOtoResurveyModel => _showMandatoryInfoCollaOtoResurveyModel;

  set showMandatoryInfoCollaOtoResurveyModel(ShowMandatoryInfoCollaOtoModel value) {
    this._showMandatoryInfoCollaOtoResurveyModel = value;
  }

  ShowMandatoryInfoCollaOtoModel get showMandatoryInfoCollaOtoPacCompanyModel => _showMandatoryInfoCollaOtoPacCompanyModel;

  set showMandatoryInfoCollaOtoPacCompanyModel(ShowMandatoryInfoCollaOtoModel value) {
    this._showMandatoryInfoCollaOtoPacCompanyModel = value;
  }

  ShowMandatoryInfoCollaOtoModel get showMandatoryInfoCollaOtoPacModel => _showMandatoryInfoCollaOtoPacModel;

  set showMandatoryInfoCollaOtoPacModel(ShowMandatoryInfoCollaOtoModel value) {
    this._showMandatoryInfoCollaOtoPacModel = value;
  }

  ShowMandatoryInfoCollaOtoModel get showMandatoryInfoCollaOtoRegulerSurveyCompanyModel => _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel;

  set showMandatoryInfoCollaOtoRegulerSurveyCompanyModel(ShowMandatoryInfoCollaOtoModel value) {
    this._showMandatoryInfoCollaOtoRegulerSurveyCompanyModel = value;
  }

  ShowMandatoryInfoCollaOtoModel get showMandatoryInfoCollaOtoRegulerSurveyModel => _showMandatoryInfoCollaOtoRegulerSurveyModel;

  set showMandatoryInfoCollaOtoRegulerSurveyModel(ShowMandatoryInfoCollaOtoModel value) {
    this._showMandatoryInfoCollaOtoRegulerSurveyModel = value;
  }

  ShowMandatoryInfoCollaOtoModel get showMandatoryInfoCollaOtoIdeCompanyModel => _showMandatoryInfoCollaOtoIdeCompanyModel;

  set showMandatoryInfoCollaOtoIdeCompanyModel(ShowMandatoryInfoCollaOtoModel value) {
    this._showMandatoryInfoCollaOtoIdeCompanyModel = value;
  }

  ShowMandatoryInfoCollaOtoModel get showMandatoryInfoCollaOtoIdeModel => _showMandatoryInfoCollaOtoIdeModel;

  set showMandatoryInfoCollaOtoIdeModel(ShowMandatoryInfoCollaOtoModel value) {
    this._showMandatoryInfoCollaOtoIdeModel = value;
  }
  // End Inf Colla Oto

  // Inf Colla Prop
  ShowMandatoryInfoCollaPropModel get showMandatoryInfoCollaPropDakorCompanyModel => _showMandatoryInfoCollaPropDakorCompanyModel;

  set showMandatoryInfoCollaPropDakorCompanyModel(ShowMandatoryInfoCollaPropModel value) {
    this._showMandatoryInfoCollaPropDakorCompanyModel = value;
  }

  ShowMandatoryInfoCollaPropModel get showMandatoryInfoCollaPropDakorModel => _showMandatoryInfoCollaPropDakorModel;

  set showMandatoryInfoCollaPropDakorModel(ShowMandatoryInfoCollaPropModel value) {
    this._showMandatoryInfoCollaPropDakorModel = value;
  }

  ShowMandatoryInfoCollaPropModel get showMandatoryInfoCollaPropResurveyCompanyModel => _showMandatoryInfoCollaPropResurveyCompanyModel;

  set showMandatoryInfoCollaPropResurveyCompanyModel(ShowMandatoryInfoCollaPropModel value) {
    this._showMandatoryInfoCollaPropResurveyCompanyModel = value;
  }

  ShowMandatoryInfoCollaPropModel get showMandatoryInfoCollaPropResurveyModel => _showMandatoryInfoCollaPropResurveyModel;

  set showMandatoryInfoCollaPropResurveyModel(ShowMandatoryInfoCollaPropModel value) {
    this._showMandatoryInfoCollaPropResurveyModel = value;
  }

  ShowMandatoryInfoCollaPropModel get showMandatoryInfoCollaPropPacCompanyModel => _showMandatoryInfoCollaPropPacCompanyModel;

  set showMandatoryInfoCollaPropPacCompanyModel(ShowMandatoryInfoCollaPropModel value) {
    this._showMandatoryInfoCollaPropPacCompanyModel = value;
  }

  ShowMandatoryInfoCollaPropModel get showMandatoryInfoCollaPropPacModel => _showMandatoryInfoCollaPropPacModel;

  set showMandatoryInfoCollaPropPacModel(ShowMandatoryInfoCollaPropModel value) {
    this._showMandatoryInfoCollaPropPacModel = value;
  }

  ShowMandatoryInfoCollaPropModel get showMandatoryInfoCollaPropRegulerSurveyCompanyModel => _showMandatoryInfoCollaPropRegulerSurveyCompanyModel;

  set showMandatoryInfoCollaPropRegulerSurveyCompanyModel(ShowMandatoryInfoCollaPropModel value) {
    this._showMandatoryInfoCollaPropRegulerSurveyCompanyModel = value;
  }

  ShowMandatoryInfoCollaPropModel get showMandatoryInfoCollaPropRegulerSurveyModel => _showMandatoryInfoCollaPropRegulerSurveyModel;

  set showMandatoryInfoCollaPropRegulerSurveyModel(ShowMandatoryInfoCollaPropModel value) {
    this._showMandatoryInfoCollaPropRegulerSurveyModel = value;
  }

  ShowMandatoryInfoCollaPropModel get showMandatoryInfoCollaPropIdeCompanyModel => _showMandatoryInfoCollaPropIdeCompanyModel;

  set showMandatoryInfoCollaPropIdeCompanyModel(ShowMandatoryInfoCollaPropModel value) {
    this._showMandatoryInfoCollaPropIdeCompanyModel = value;
  }

  ShowMandatoryInfoCollaPropModel get showMandatoryInfoCollaPropIdeModel => _showMandatoryInfoCollaPropIdeModel;

  set showMandatoryInfoCollaPropIdeModel(ShowMandatoryInfoCollaPropModel value) {
    this._showMandatoryInfoCollaPropIdeModel = value;
  }
  // End Inf Colla Prop

  // Inf Karoseri
  ShowMandatoryInfoKaroseriModel get showMandatoryInfoKaroseriDakorCompanyModel => _showMandatoryInfoKaroseriDakorCompanyModel;

  set showMandatoryInfoKaroseriDakorCompanyModel(ShowMandatoryInfoKaroseriModel value) {
    this._showMandatoryInfoKaroseriDakorCompanyModel = value;
  }

  ShowMandatoryInfoKaroseriModel get showMandatoryInfoKaroseriDakorModel => _showMandatoryInfoKaroseriDakorModel;

  set showMandatoryInfoKaroseriDakorModel(ShowMandatoryInfoKaroseriModel value) {
    this._showMandatoryInfoKaroseriDakorModel = value;
  }

  ShowMandatoryInfoKaroseriModel get showMandatoryInfoKaroseriResurveyCompanyModel => _showMandatoryInfoKaroseriResurveyCompanyModel;

  set showMandatoryInfoKaroseriResurveyCompanyModel(ShowMandatoryInfoKaroseriModel value) {
    this._showMandatoryInfoKaroseriResurveyCompanyModel = value;
  }

  ShowMandatoryInfoKaroseriModel get showMandatoryInfoKaroseriResurveyModel => _showMandatoryInfoKaroseriResurveyModel;

  set showMandatoryInfoKaroseriResurveyModel(ShowMandatoryInfoKaroseriModel value) {
    this._showMandatoryInfoKaroseriResurveyModel = value;
  }

  ShowMandatoryInfoKaroseriModel get showMandatoryInfoKaroseriPacCompanyModel => _showMandatoryInfoKaroseriPacCompanyModel;

  set showMandatoryInfoKaroseriPacCompanyModel(ShowMandatoryInfoKaroseriModel value) {
    this._showMandatoryInfoKaroseriPacCompanyModel = value;
  }

  ShowMandatoryInfoKaroseriModel get showMandatoryInfoKaroseriPacModel => _showMandatoryInfoKaroseriPacModel;

  set showMandatoryInfoKaroseriPacModel(ShowMandatoryInfoKaroseriModel value) {
    this._showMandatoryInfoKaroseriPacModel = value;
  }

  ShowMandatoryInfoKaroseriModel get showMandatoryInfoKaroseriRegulerSurveyCompanyModel => _showMandatoryInfoKaroseriRegulerSurveyCompanyModel;

  set showMandatoryInfoKaroseriRegulerSurveyCompanyModel(ShowMandatoryInfoKaroseriModel value) {
    this._showMandatoryInfoKaroseriRegulerSurveyCompanyModel = value;
  }

  ShowMandatoryInfoKaroseriModel get showMandatoryInfoKaroseriRegulerSurveyModel => _showMandatoryInfoKaroseriRegulerSurveyModel;

  set showMandatoryInfoKaroseriRegulerSurveyModel(ShowMandatoryInfoKaroseriModel value) {
    this._showMandatoryInfoKaroseriRegulerSurveyModel = value;
  }

  ShowMandatoryInfoKaroseriModel get showMandatoryInfoKaroseriIdeCompanyModel => _showMandatoryInfoKaroseriIdeCompanyModel;

  set showMandatoryInfoKaroseriIdeCompanyModel(ShowMandatoryInfoKaroseriModel value) {
    this._showMandatoryInfoKaroseriIdeCompanyModel = value;
  }

  ShowMandatoryInfoKaroseriModel get showMandatoryInfoKaroseriIdeModel => _showMandatoryInfoKaroseriIdeModel;

  set showMandatoryInfoKaroseriIdeModel(ShowMandatoryInfoKaroseriModel value) {
    this._showMandatoryInfoKaroseriIdeModel = value;
  }
  // End Inf Karoseri

  // Inf Struktur Kredit
  ShowMandatoryInfoStrukturKreditModel get showMandatoryInfoStrukturKreditDakorCompanyModel => _showMandatoryInfoStrukturKreditDakorCompanyModel;

  set showMandatoryInfoStrukturKreditDakorCompanyModel(ShowMandatoryInfoStrukturKreditModel value) {
    this._showMandatoryInfoStrukturKreditDakorCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditModel get showMandatoryInfoStrukturKreditDakorModel => _showMandatoryInfoStrukturKreditDakorModel;

  set showMandatoryInfoStrukturKreditDakorModel(ShowMandatoryInfoStrukturKreditModel value) {
    this._showMandatoryInfoStrukturKreditDakorModel = value;
  }

  ShowMandatoryInfoStrukturKreditModel get showMandatoryInfoStrukturKreditResurveyCompanyModel => _showMandatoryInfoStrukturKreditResurveyCompanyModel;

  set showMandatoryInfoStrukturKreditResurveyCompanyModel(ShowMandatoryInfoStrukturKreditModel value) {
    this._showMandatoryInfoStrukturKreditResurveyCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditModel get showMandatoryInfoStrukturKreditResurveyModel => _showMandatoryInfoStrukturKreditResurveyModel;

  set showMandatoryInfoStrukturKreditResurveyModel(ShowMandatoryInfoStrukturKreditModel value) {
    this._showMandatoryInfoStrukturKreditResurveyModel = value;
  }

  ShowMandatoryInfoStrukturKreditModel get showMandatoryInfoStrukturKreditPacCompanyModel => _showMandatoryInfoStrukturKreditPacCompanyModel;

  set showMandatoryInfoStrukturKreditPacCompanyModel(ShowMandatoryInfoStrukturKreditModel value) {
    this._showMandatoryInfoStrukturKreditPacCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditModel get showMandatoryInfoStrukturKreditPacModel => _showMandatoryInfoStrukturKreditPacModel;

  set showMandatoryInfoStrukturKreditPacModel(ShowMandatoryInfoStrukturKreditModel value) {
    this._showMandatoryInfoStrukturKreditPacModel = value;
  }

  ShowMandatoryInfoStrukturKreditModel get showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel => _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel;

  set showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel(ShowMandatoryInfoStrukturKreditModel value) {
    this._showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditModel get showMandatoryInfoStrukturKreditRegulerSurveyModel => _showMandatoryInfoStrukturKreditRegulerSurveyModel;

  set showMandatoryInfoStrukturKreditRegulerSurveyModel(ShowMandatoryInfoStrukturKreditModel value) {
    this._showMandatoryInfoStrukturKreditRegulerSurveyModel = value;
  }

  ShowMandatoryInfoStrukturKreditModel get showMandatoryInfoStrukturKreditIdeCompanyModel => _showMandatoryInfoStrukturKreditIdeCompanyModel;

  set showMandatoryInfoStrukturKreditIdeCompanyModel(ShowMandatoryInfoStrukturKreditModel value) {
    this._showMandatoryInfoStrukturKreditIdeCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditModel get showMandatoryInfoStrukturKreditIdeModel => _showMandatoryInfoStrukturKreditIdeModel;

  set showMandatoryInfoStrukturKreditIdeModel(ShowMandatoryInfoStrukturKreditModel value) {
    this._showMandatoryInfoStrukturKreditIdeModel = value;
  }
  // End Inf Struktur Kredit

  // Inf Struktur Kredit Biaya
  ShowMandatoryInfoStrukturKreditBiayaModel get showMandatoryInfoStrukturKreditBiayaDakorCompanyModel => _showMandatoryInfoStrukturKreditBiayaDakorCompanyModel;

  set showMandatoryInfoStrukturKreditBiayaDakorCompanyModel(ShowMandatoryInfoStrukturKreditBiayaModel value) {
    this._showMandatoryInfoStrukturKreditBiayaDakorCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditBiayaModel get showMandatoryInfoStrukturKreditBiayaDakorModel => _showMandatoryInfoStrukturKreditBiayaDakorModel;

  set showMandatoryInfoStrukturKreditBiayaDakorModel(ShowMandatoryInfoStrukturKreditBiayaModel value) {
    this._showMandatoryInfoStrukturKreditBiayaDakorModel = value;
  }

  ShowMandatoryInfoStrukturKreditBiayaModel get showMandatoryInfoStrukturKreditBiayaResurveyCompanyModel => _showMandatoryInfoStrukturKreditBiayaResurveyCompanyModel;

  set showMandatoryInfoStrukturKreditBiayaResurveyCompanyModel(ShowMandatoryInfoStrukturKreditBiayaModel value) {
    this._showMandatoryInfoStrukturKreditBiayaResurveyCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditBiayaModel get showMandatoryInfoStrukturKreditBiayaResurveyModel => _showMandatoryInfoStrukturKreditBiayaResurveyModel;

  set showMandatoryInfoStrukturKreditBiayaResurveyModel(ShowMandatoryInfoStrukturKreditBiayaModel value) {
    this._showMandatoryInfoStrukturKreditBiayaResurveyModel = value;
  }

  ShowMandatoryInfoStrukturKreditBiayaModel get showMandatoryInfoStrukturKreditBiayaPacCompanyModel => _showMandatoryInfoStrukturKreditBiayaPacCompanyModel;

  set showMandatoryInfoStrukturKreditBiayaPacCompanyModel(ShowMandatoryInfoStrukturKreditBiayaModel value) {
    this._showMandatoryInfoStrukturKreditBiayaPacCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditBiayaModel get showMandatoryInfoStrukturKreditBiayaPacModel => _showMandatoryInfoStrukturKreditBiayaPacModel;

  set showMandatoryInfoStrukturKreditBiayaPacModel(ShowMandatoryInfoStrukturKreditBiayaModel value) {
    this._showMandatoryInfoStrukturKreditBiayaPacModel = value;
  }

  ShowMandatoryInfoStrukturKreditBiayaModel get showMandatoryInfoStrukturKreditBiayaRegulerSurveyCompanyModel => _showMandatoryInfoStrukturKreditBiayaRegulerSurveyCompanyModel;

  set showMandatoryInfoStrukturKreditBiayaRegulerSurveyCompanyModel(ShowMandatoryInfoStrukturKreditBiayaModel value) {
    this._showMandatoryInfoStrukturKreditBiayaRegulerSurveyCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditBiayaModel get showMandatoryInfoStrukturKreditBiayaRegulerSurveyModel => _showMandatoryInfoStrukturKreditBiayaRegulerSurveyModel;

  set showMandatoryInfoStrukturKreditBiayaRegulerSurveyModel(ShowMandatoryInfoStrukturKreditBiayaModel value) {
    this._showMandatoryInfoStrukturKreditBiayaRegulerSurveyModel = value;
  }

  ShowMandatoryInfoStrukturKreditBiayaModel get showMandatoryInfoStrukturKreditBiayaIdeCompanyModel => _showMandatoryInfoStrukturKreditBiayaIdeCompanyModel;

  set showMandatoryInfoStrukturKreditBiayaIdeCompanyModel(ShowMandatoryInfoStrukturKreditBiayaModel value) {
    this._showMandatoryInfoStrukturKreditBiayaIdeCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditBiayaModel get showMandatoryInfoStrukturKreditBiayaIdeModel => _showMandatoryInfoStrukturKreditBiayaIdeModel;

  set showMandatoryInfoStrukturKreditBiayaIdeModel(ShowMandatoryInfoStrukturKreditBiayaModel value) {
    this._showMandatoryInfoStrukturKreditBiayaIdeModel = value;
  }
  // End Inf Struktur Kredit Biaya

  // Inf Struktur Kredit Floating
  ShowMandatoryInfoStrukturKreditFloatingModel get showMandatoryInfoStrukturKreditFloatingDakorCompanyModel => _showMandatoryInfoStrukturKreditFloatingDakorCompanyModel;

  set showMandatoryInfoStrukturKreditFloatingDakorCompanyModel(ShowMandatoryInfoStrukturKreditFloatingModel value) {
    this._showMandatoryInfoStrukturKreditFloatingDakorCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditFloatingModel get showMandatoryInfoStrukturKreditFloatingDakorModel => _showMandatoryInfoStrukturKreditFloatingDakorModel;

  set showMandatoryInfoStrukturKreditFloatingDakorModel(ShowMandatoryInfoStrukturKreditFloatingModel value) {
    this._showMandatoryInfoStrukturKreditFloatingDakorModel = value;
  }

  ShowMandatoryInfoStrukturKreditFloatingModel get showMandatoryInfoStrukturKreditFloatingResurveyCompanyModel => _showMandatoryInfoStrukturKreditFloatingResurveyCompanyModel;

  set showMandatoryInfoStrukturKreditFloatingResurveyCompanyModel(ShowMandatoryInfoStrukturKreditFloatingModel value) {
    this._showMandatoryInfoStrukturKreditFloatingResurveyCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditFloatingModel get showMandatoryInfoStrukturKreditFloatingResurveyModel => _showMandatoryInfoStrukturKreditFloatingResurveyModel;

  set showMandatoryInfoStrukturKreditFloatingResurveyModel(ShowMandatoryInfoStrukturKreditFloatingModel value) {
    this._showMandatoryInfoStrukturKreditFloatingResurveyModel = value;
  }

  ShowMandatoryInfoStrukturKreditFloatingModel get showMandatoryInfoStrukturKreditFloatingPacCompanyModel => _showMandatoryInfoStrukturKreditFloatingPacCompanyModel;

  set showMandatoryInfoStrukturKreditFloatingPacCompanyModel(ShowMandatoryInfoStrukturKreditFloatingModel value) {
    this._showMandatoryInfoStrukturKreditFloatingPacCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditFloatingModel get showMandatoryInfoStrukturKreditFloatingPacModel => _showMandatoryInfoStrukturKreditFloatingPacModel;

  set showMandatoryInfoStrukturKreditFloatingPacModel(ShowMandatoryInfoStrukturKreditFloatingModel value) {
    this._showMandatoryInfoStrukturKreditFloatingPacModel = value;
  }

  ShowMandatoryInfoStrukturKreditFloatingModel get showMandatoryInfoStrukturKreditFloatingRegulerSurveyCompanyModel => _showMandatoryInfoStrukturKreditFloatingRegulerSurveyCompanyModel;

  set showMandatoryInfoStrukturKreditFloatingRegulerSurveyCompanyModel(ShowMandatoryInfoStrukturKreditFloatingModel value) {
    this._showMandatoryInfoStrukturKreditFloatingRegulerSurveyCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditFloatingModel get showMandatoryInfoStrukturKreditFloatingRegulerSurveyModel => _showMandatoryInfoStrukturKreditFloatingRegulerSurveyModel;

  set showMandatoryInfoStrukturKreditFloatingRegulerSurveyModel(ShowMandatoryInfoStrukturKreditFloatingModel value) {
    this._showMandatoryInfoStrukturKreditFloatingRegulerSurveyModel = value;
  }

  ShowMandatoryInfoStrukturKreditFloatingModel get showMandatoryInfoStrukturKreditFloatingIdeCompanyModel => _showMandatoryInfoStrukturKreditFloatingIdeCompanyModel;

  set showMandatoryInfoStrukturKreditFloatingIdeCompanyModel(ShowMandatoryInfoStrukturKreditFloatingModel value) {
    this._showMandatoryInfoStrukturKreditFloatingIdeCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditFloatingModel get showMandatoryInfoStrukturKreditFloatingIdeModel => _showMandatoryInfoStrukturKreditFloatingIdeModel;

  set showMandatoryInfoStrukturKreditFloatingIdeModel(ShowMandatoryInfoStrukturKreditFloatingModel value) {
    this._showMandatoryInfoStrukturKreditFloatingIdeModel = value;
  }
  // End Inf Struktur Kredit Floating

  // Inf Struktur Kredit GP
  ShowMandatoryInfoStrukturKreditGPModel get showMandatoryInfoStrukturKreditGPDakorCompanyModel => _showMandatoryInfoStrukturKreditGPDakorCompanyModel;

  set showMandatoryInfoStrukturKreditGPDakorCompanyModel(ShowMandatoryInfoStrukturKreditGPModel value) {
    this._showMandatoryInfoStrukturKreditGPDakorCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditGPModel get showMandatoryInfoStrukturKreditGPDakorModel => _showMandatoryInfoStrukturKreditGPDakorModel;

  set showMandatoryInfoStrukturKreditGPDakorModel(ShowMandatoryInfoStrukturKreditGPModel value) {
    this._showMandatoryInfoStrukturKreditGPDakorModel = value;
  }

  ShowMandatoryInfoStrukturKreditGPModel get showMandatoryInfoStrukturKreditGPResurveyCompanyModel => _showMandatoryInfoStrukturKreditGPResurveyCompanyModel;

  set showMandatoryInfoStrukturKreditGPResurveyCompanyModel(ShowMandatoryInfoStrukturKreditGPModel value) {
    this._showMandatoryInfoStrukturKreditGPResurveyCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditGPModel get showMandatoryInfoStrukturKreditGPResurveyModel => _showMandatoryInfoStrukturKreditGPResurveyModel;

  set showMandatoryInfoStrukturKreditGPResurveyModel(ShowMandatoryInfoStrukturKreditGPModel value) {
    this._showMandatoryInfoStrukturKreditGPResurveyModel = value;
  }

  ShowMandatoryInfoStrukturKreditGPModel get showMandatoryInfoStrukturKreditGPPacCompanyModel => _showMandatoryInfoStrukturKreditGPPacCompanyModel;

  set showMandatoryInfoStrukturKreditGPPacCompanyModel(ShowMandatoryInfoStrukturKreditGPModel value) {
    this._showMandatoryInfoStrukturKreditGPPacCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditGPModel get showMandatoryInfoStrukturKreditGPPacModel => _showMandatoryInfoStrukturKreditGPPacModel;

  set showMandatoryInfoStrukturKreditGPPacModel(ShowMandatoryInfoStrukturKreditGPModel value) {
    this._showMandatoryInfoStrukturKreditGPPacModel = value;
  }

  ShowMandatoryInfoStrukturKreditGPModel get showMandatoryInfoStrukturKreditGPRegulerSurveyCompanyModel => _showMandatoryInfoStrukturKreditGPRegulerSurveyCompanyModel;

  set showMandatoryInfoStrukturKreditGPRegulerSurveyCompanyModel(ShowMandatoryInfoStrukturKreditGPModel value) {
    this._showMandatoryInfoStrukturKreditGPRegulerSurveyCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditGPModel get showMandatoryInfoStrukturKreditGPRegulerSurveyModel => _showMandatoryInfoStrukturKreditGPRegulerSurveyModel;

  set showMandatoryInfoStrukturKreditGPRegulerSurveyModel(ShowMandatoryInfoStrukturKreditGPModel value) {
    this._showMandatoryInfoStrukturKreditGPRegulerSurveyModel = value;
  }

  ShowMandatoryInfoStrukturKreditGPModel get showMandatoryInfoStrukturKreditGPIdeCompanyModel => _showMandatoryInfoStrukturKreditGPIdeCompanyModel;

  set showMandatoryInfoStrukturKreditGPIdeCompanyModel(ShowMandatoryInfoStrukturKreditGPModel value) {
    this._showMandatoryInfoStrukturKreditGPIdeCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditGPModel get showMandatoryInfoStrukturKreditGPIdeModel => _showMandatoryInfoStrukturKreditGPIdeModel;

  set showMandatoryInfoStrukturKreditGPIdeModel(ShowMandatoryInfoStrukturKreditGPModel value) {
    this._showMandatoryInfoStrukturKreditGPIdeModel = value;
  }
  // End Inf Struktur Kredit GP

  // Inf Struktur Kredit Stepping
  ShowMandatoryInfoStrukturKreditSteppingModel get showMandatoryInfoStrukturKreditSteppingDakorCompanyModel => _showMandatoryInfoStrukturKreditSteppingDakorCompanyModel;

  set showMandatoryInfoStrukturKreditSteppingDakorCompanyModel(ShowMandatoryInfoStrukturKreditSteppingModel value) {
    this._showMandatoryInfoStrukturKreditSteppingDakorCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditSteppingModel get showMandatoryInfoStrukturKreditSteppingDakorModel => _showMandatoryInfoStrukturKreditSteppingDakorModel;

  set showMandatoryInfoStrukturKreditSteppingDakorModel(ShowMandatoryInfoStrukturKreditSteppingModel value) {
    this._showMandatoryInfoStrukturKreditSteppingDakorModel = value;
  }

  ShowMandatoryInfoStrukturKreditSteppingModel get showMandatoryInfoStrukturKreditSteppingResurveyCompanyModel => _showMandatoryInfoStrukturKreditSteppingResurveyCompanyModel;

  set showMandatoryInfoStrukturKreditSteppingResurveyCompanyModel(ShowMandatoryInfoStrukturKreditSteppingModel value) {
    this._showMandatoryInfoStrukturKreditSteppingResurveyCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditSteppingModel get showMandatoryInfoStrukturKreditSteppingResurveyModel => _showMandatoryInfoStrukturKreditSteppingResurveyModel;

  set showMandatoryInfoStrukturKreditSteppingResurveyModel(ShowMandatoryInfoStrukturKreditSteppingModel value) {
    this._showMandatoryInfoStrukturKreditSteppingResurveyModel = value;
  }

  ShowMandatoryInfoStrukturKreditSteppingModel get showMandatoryInfoStrukturKreditSteppingPacCompanyModel => _showMandatoryInfoStrukturKreditSteppingPacCompanyModel;

  set showMandatoryInfoStrukturKreditSteppingPacCompanyModel(ShowMandatoryInfoStrukturKreditSteppingModel value) {
    this._showMandatoryInfoStrukturKreditSteppingPacCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditSteppingModel get showMandatoryInfoStrukturKreditSteppingPacModel => _showMandatoryInfoStrukturKreditSteppingPacModel;

  set showMandatoryInfoStrukturKreditSteppingPacModel(ShowMandatoryInfoStrukturKreditSteppingModel value) {
    this._showMandatoryInfoStrukturKreditSteppingPacModel = value;
  }

  ShowMandatoryInfoStrukturKreditSteppingModel get showMandatoryInfoStrukturKreditSteppingRegulerSurveyCompanyModel => _showMandatoryInfoStrukturKreditSteppingRegulerSurveyCompanyModel;

  set showMandatoryInfoStrukturKreditSteppingRegulerSurveyCompanyModel(ShowMandatoryInfoStrukturKreditSteppingModel value) {
    this._showMandatoryInfoStrukturKreditSteppingRegulerSurveyCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditSteppingModel get showMandatoryInfoStrukturKreditSteppingRegulerSurveyModel => _showMandatoryInfoStrukturKreditSteppingRegulerSurveyModel;

  set showMandatoryInfoStrukturKreditSteppingRegulerSurveyModel(ShowMandatoryInfoStrukturKreditSteppingModel value) {
    this._showMandatoryInfoStrukturKreditSteppingRegulerSurveyModel = value;
  }

  ShowMandatoryInfoStrukturKreditSteppingModel get showMandatoryInfoStrukturKreditSteppingIdeCompanyModel => _showMandatoryInfoStrukturKreditSteppingIdeCompanyModel;

  set showMandatoryInfoStrukturKreditSteppingIdeCompanyModel(ShowMandatoryInfoStrukturKreditSteppingModel value) {
    this._showMandatoryInfoStrukturKreditSteppingIdeCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditSteppingModel get showMandatoryInfoStrukturKreditSteppingIdeModel => _showMandatoryInfoStrukturKreditSteppingIdeModel;

  set showMandatoryInfoStrukturKreditSteppingIdeModel(ShowMandatoryInfoStrukturKreditSteppingModel value) {
    this._showMandatoryInfoStrukturKreditSteppingIdeModel = value;
  }
  // End Inf Struktur Kredit Stepping

  // Inf Struktur Kredit Irreguler
  ShowMandatoryInfoStrukturKreditIrregulerModel get showMandatoryInfoStrukturKreditIrregulerDakorCompanyModel => _showMandatoryInfoStrukturKreditIrregulerDakorCompanyModel;

  set showMandatoryInfoStrukturKreditIrregulerDakorCompanyModel(ShowMandatoryInfoStrukturKreditIrregulerModel value) {
    this._showMandatoryInfoStrukturKreditIrregulerDakorCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditIrregulerModel get showMandatoryInfoStrukturKreditIrregulerDakorModel => _showMandatoryInfoStrukturKreditIrregulerDakorModel;

  set showMandatoryInfoStrukturKreditIrregulerDakorModel(ShowMandatoryInfoStrukturKreditIrregulerModel value) {
    this._showMandatoryInfoStrukturKreditIrregulerDakorModel = value;
  }

  ShowMandatoryInfoStrukturKreditIrregulerModel get showMandatoryInfoStrukturKreditIrregulerResurveyCompanyModel => _showMandatoryInfoStrukturKreditIrregulerResurveyCompanyModel;

  set showMandatoryInfoStrukturKreditIrregulerResurveyCompanyModel(ShowMandatoryInfoStrukturKreditIrregulerModel value) {
    this._showMandatoryInfoStrukturKreditIrregulerResurveyCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditIrregulerModel get showMandatoryInfoStrukturKreditIrregulerResurveyModel => _showMandatoryInfoStrukturKreditIrregulerResurveyModel;

  set showMandatoryInfoStrukturKreditIrregulerResurveyModel(ShowMandatoryInfoStrukturKreditIrregulerModel value) {
    this._showMandatoryInfoStrukturKreditIrregulerResurveyModel = value;
  }

  ShowMandatoryInfoStrukturKreditIrregulerModel get showMandatoryInfoStrukturKreditIrregulerPacCompanyModel => _showMandatoryInfoStrukturKreditIrregulerPacCompanyModel;

  set showMandatoryInfoStrukturKreditIrregulerPacCompanyModel(ShowMandatoryInfoStrukturKreditIrregulerModel value) {
    this._showMandatoryInfoStrukturKreditIrregulerPacCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditIrregulerModel get showMandatoryInfoStrukturKreditIrregulerPacModel => _showMandatoryInfoStrukturKreditIrregulerPacModel;

  set showMandatoryInfoStrukturKreditIrregulerPacModel(ShowMandatoryInfoStrukturKreditIrregulerModel value) {
    this._showMandatoryInfoStrukturKreditIrregulerPacModel = value;
  }

  ShowMandatoryInfoStrukturKreditIrregulerModel get showMandatoryInfoStrukturKreditIrregulerRegulerSurveyCompanyModel => _showMandatoryInfoStrukturKreditIrregulerRegulerSurveyCompanyModel;

  set showMandatoryInfoStrukturKreditIrregulerRegulerSurveyCompanyModel(ShowMandatoryInfoStrukturKreditIrregulerModel value) {
    this._showMandatoryInfoStrukturKreditIrregulerRegulerSurveyCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditIrregulerModel get showMandatoryInfoStrukturKreditIrregulerRegulerSurveyModel => _showMandatoryInfoStrukturKreditIrregulerRegulerSurveyModel;

  set showMandatoryInfoStrukturKreditIrregulerRegulerSurveyModel(ShowMandatoryInfoStrukturKreditIrregulerModel value) {
    this._showMandatoryInfoStrukturKreditIrregulerRegulerSurveyModel = value;
  }

  ShowMandatoryInfoStrukturKreditIrregulerModel get showMandatoryInfoStrukturKreditIrregulerIdeCompanyModel => _showMandatoryInfoStrukturKreditIrregulerIdeCompanyModel;

  set showMandatoryInfoStrukturKreditIrregulerIdeCompanyModel(ShowMandatoryInfoStrukturKreditIrregulerModel value) {
    this._showMandatoryInfoStrukturKreditIrregulerIdeCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditIrregulerModel get showMandatoryInfoStrukturKreditIrregulerIdeModel => _showMandatoryInfoStrukturKreditIrregulerIdeModel;

  set showMandatoryInfoStrukturKreditIrregulerIdeModel(ShowMandatoryInfoStrukturKreditIrregulerModel value) {
    this._showMandatoryInfoStrukturKreditIrregulerIdeModel = value;
  }
  // End Inf Struktur Kredit Irreguler

  // Inf Struktur Kredit BP
  ShowMandatoryInfoStrukturKreditBPModel get showMandatoryInfoStrukturKreditBPDakorCompanyModel => _showMandatoryInfoStrukturKreditBPDakorCompanyModel;

  set showMandatoryInfoStrukturKreditBPDakorCompanyModel(ShowMandatoryInfoStrukturKreditBPModel value) {
    this._showMandatoryInfoStrukturKreditBPDakorCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditBPModel get showMandatoryInfoStrukturKreditBPDakorModel => _showMandatoryInfoStrukturKreditBPDakorModel;

  set showMandatoryInfoStrukturKreditBPDakorModel(ShowMandatoryInfoStrukturKreditBPModel value) {
    this._showMandatoryInfoStrukturKreditBPDakorModel = value;
  }

  ShowMandatoryInfoStrukturKreditBPModel get showMandatoryInfoStrukturKreditBPResurveyCompanyModel => _showMandatoryInfoStrukturKreditBPResurveyCompanyModel;

  set showMandatoryInfoStrukturKreditBPResurveyCompanyModel(ShowMandatoryInfoStrukturKreditBPModel value) {
    this._showMandatoryInfoStrukturKreditBPResurveyCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditBPModel get showMandatoryInfoStrukturKreditBPResurveyModel => _showMandatoryInfoStrukturKreditBPResurveyModel;

  set showMandatoryInfoStrukturKreditBPResurveyModel(ShowMandatoryInfoStrukturKreditBPModel value) {
    this._showMandatoryInfoStrukturKreditBPResurveyModel = value;
  }

  ShowMandatoryInfoStrukturKreditBPModel get showMandatoryInfoStrukturKreditBPPacCompanyModel => _showMandatoryInfoStrukturKreditBPPacCompanyModel;

  set showMandatoryInfoStrukturKreditBPPacCompanyModel(ShowMandatoryInfoStrukturKreditBPModel value) {
    this._showMandatoryInfoStrukturKreditBPPacCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditBPModel get showMandatoryInfoStrukturKreditBPPacModel => _showMandatoryInfoStrukturKreditBPPacModel;

  set showMandatoryInfoStrukturKreditBPPacModel(ShowMandatoryInfoStrukturKreditBPModel value) {
    this._showMandatoryInfoStrukturKreditBPPacModel = value;
  }

  ShowMandatoryInfoStrukturKreditBPModel get showMandatoryInfoStrukturKreditBPRegulerSurveyCompanyModel => _showMandatoryInfoStrukturKreditBPRegulerSurveyCompanyModel;

  set showMandatoryInfoStrukturKreditBPRegulerSurveyCompanyModel(ShowMandatoryInfoStrukturKreditBPModel value) {
    this._showMandatoryInfoStrukturKreditBPRegulerSurveyCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditBPModel get showMandatoryInfoStrukturKreditBPRegulerSurveyModel => _showMandatoryInfoStrukturKreditBPRegulerSurveyModel;

  set showMandatoryInfoStrukturKreditBPRegulerSurveyModel(ShowMandatoryInfoStrukturKreditBPModel value) {
    this._showMandatoryInfoStrukturKreditBPRegulerSurveyModel = value;
  }

  ShowMandatoryInfoStrukturKreditBPModel get showMandatoryInfoStrukturKreditBPIdeCompanyModel => _showMandatoryInfoStrukturKreditBPIdeCompanyModel;

  set showMandatoryInfoStrukturKreditBPIdeCompanyModel(ShowMandatoryInfoStrukturKreditBPModel value) {
    this._showMandatoryInfoStrukturKreditBPIdeCompanyModel = value;
  }

  ShowMandatoryInfoStrukturKreditBPModel get showMandatoryInfoStrukturKreditBPIdeModel => _showMandatoryInfoStrukturKreditBPIdeModel;

  set showMandatoryInfoStrukturKreditBPIdeModel(ShowMandatoryInfoStrukturKreditBPModel value) {
    this._showMandatoryInfoStrukturKreditBPIdeModel = value;
  }
  // End Inf Struktur Kredit BP

  // Inf Asuransi Utama
  SetMajorInsuranceModel get showMandatoryInfoAsuransiUtamaDakorCompanyModel => _showMandatoryInfoAsuransiUtamaDakorCompanyModel;

  set showMandatoryInfoAsuransiUtamaDakorCompanyModel(SetMajorInsuranceModel value) {
    this._showMandatoryInfoAsuransiUtamaDakorCompanyModel = value;
  }

  SetMajorInsuranceModel get showMandatoryInfoAsuransiUtamaDakorModel => _showMandatoryInfoAsuransiUtamaDakorModel;

  set showMandatoryInfoAsuransiUtamaDakorModel(SetMajorInsuranceModel value) {
    this._showMandatoryInfoAsuransiUtamaDakorModel = value;
  }

  SetMajorInsuranceModel get showMandatoryInfoAsuransiUtamaResurveyCompanyModel => _showMandatoryInfoAsuransiUtamaResurveyCompanyModel;

  set showMandatoryInfoAsuransiUtamaResurveyCompanyModel(SetMajorInsuranceModel value) {
    this._showMandatoryInfoAsuransiUtamaResurveyCompanyModel = value;
  }

  SetMajorInsuranceModel get showMandatoryInfoAsuransiUtamaResurveyModel => _showMandatoryInfoAsuransiUtamaResurveyModel;

  set showMandatoryInfoAsuransiUtamaResurveyModel(SetMajorInsuranceModel value) {
    this._showMandatoryInfoAsuransiUtamaResurveyModel = value;
  }

  SetMajorInsuranceModel get showMandatoryInfoAsuransiUtamaPacCompanyModel => _showMandatoryInfoAsuransiUtamaPacCompanyModel;

  set showMandatoryInfoAsuransiUtamaPacCompanyModel(SetMajorInsuranceModel value) {
    this._showMandatoryInfoAsuransiUtamaPacCompanyModel = value;
  }

  SetMajorInsuranceModel get showMandatoryInfoAsuransiUtamaPacModel => _showMandatoryInfoAsuransiUtamaPacModel;

  set showMandatoryInfoAsuransiUtamaPacModel(SetMajorInsuranceModel value) {
    this._showMandatoryInfoAsuransiUtamaPacModel = value;
  }

  SetMajorInsuranceModel get showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel => _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel;

  set showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel(SetMajorInsuranceModel value) {
    this._showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel = value;
  }

  SetMajorInsuranceModel get showMandatoryInfoAsuransiUtamaRegulerSurveyModel => _showMandatoryInfoAsuransiUtamaRegulerSurveyModel;

  set showMandatoryInfoAsuransiUtamaRegulerSurveyModel(SetMajorInsuranceModel value) {
    this._showMandatoryInfoAsuransiUtamaRegulerSurveyModel = value;
  }

  SetMajorInsuranceModel get showMandatoryInfoAsuransiUtamaIdeCompanyModel => _showMandatoryInfoAsuransiUtamaIdeCompanyModel;

  set showMandatoryInfoAsuransiUtamaIdeCompanyModel(SetMajorInsuranceModel value) {
    this._showMandatoryInfoAsuransiUtamaIdeCompanyModel = value;
  }

  SetMajorInsuranceModel get showMandatoryInfoAsuransiUtamaIdeModel => _showMandatoryInfoAsuransiUtamaIdeModel;

  set showMandatoryInfoAsuransiUtamaIdeModel(SetMajorInsuranceModel value) {
    this._showMandatoryInfoAsuransiUtamaIdeModel = value;
  }
  // End Inf Asuransi Utama

  // Inf Asuransi Tambahan
  ShowMandatoryInfoAsuransiTambahanModel get showMandatoryInfoAsuransiTambahanDakorCompanyModel => _showMandatoryInfoAsuransiTambahanDakorCompanyModel;

  set showMandatoryInfoAsuransiTambahanDakorCompanyModel(ShowMandatoryInfoAsuransiTambahanModel value) {
    this._showMandatoryInfoAsuransiTambahanDakorCompanyModel = value;
  }

  ShowMandatoryInfoAsuransiTambahanModel get showMandatoryInfoAsuransiTambahanDakorModel => _showMandatoryInfoAsuransiTambahanDakorModel;

  set showMandatoryInfoAsuransiTambahanDakorModel(ShowMandatoryInfoAsuransiTambahanModel value) {
    this._showMandatoryInfoAsuransiTambahanDakorModel = value;
  }

  ShowMandatoryInfoAsuransiTambahanModel get showMandatoryInfoAsuransiTambahanResurveyCompanyModel => _showMandatoryInfoAsuransiTambahanResurveyCompanyModel;

  set showMandatoryInfoAsuransiTambahanResurveyCompanyModel(ShowMandatoryInfoAsuransiTambahanModel value) {
    this._showMandatoryInfoAsuransiTambahanResurveyCompanyModel = value;
  }

  ShowMandatoryInfoAsuransiTambahanModel get showMandatoryInfoAsuransiTambahanResurveyModel => _showMandatoryInfoAsuransiTambahanResurveyModel;

  set showMandatoryInfoAsuransiTambahanResurveyModel(ShowMandatoryInfoAsuransiTambahanModel value) {
    this._showMandatoryInfoAsuransiTambahanResurveyModel = value;
  }

  ShowMandatoryInfoAsuransiTambahanModel get showMandatoryInfoAsuransiTambahanPacCompanyModel => _showMandatoryInfoAsuransiTambahanPacCompanyModel;

  set showMandatoryInfoAsuransiTambahanPacCompanyModel(ShowMandatoryInfoAsuransiTambahanModel value) {
    this._showMandatoryInfoAsuransiTambahanPacCompanyModel = value;
  }

  ShowMandatoryInfoAsuransiTambahanModel get showMandatoryInfoAsuransiTambahanPacModel => _showMandatoryInfoAsuransiTambahanPacModel;

  set showMandatoryInfoAsuransiTambahanPacModel(ShowMandatoryInfoAsuransiTambahanModel value) {
    this._showMandatoryInfoAsuransiTambahanPacModel = value;
  }

  ShowMandatoryInfoAsuransiTambahanModel get showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel => _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel;

  set showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel(ShowMandatoryInfoAsuransiTambahanModel value) {
    this._showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel = value;
  }

  ShowMandatoryInfoAsuransiTambahanModel get showMandatoryInfoAsuransiTambahanRegulerSurveyModel => _showMandatoryInfoAsuransiTambahanRegulerSurveyModel;

  set showMandatoryInfoAsuransiTambahanRegulerSurveyModel(ShowMandatoryInfoAsuransiTambahanModel value) {
    this._showMandatoryInfoAsuransiTambahanRegulerSurveyModel = value;
  }

  ShowMandatoryInfoAsuransiTambahanModel get showMandatoryInfoAsuransiTambahanIdeCompanyModel => _showMandatoryInfoAsuransiTambahanIdeCompanyModel;

  set showMandatoryInfoAsuransiTambahanIdeCompanyModel(ShowMandatoryInfoAsuransiTambahanModel value) {
    this._showMandatoryInfoAsuransiTambahanIdeCompanyModel = value;
  }

  ShowMandatoryInfoAsuransiTambahanModel get showMandatoryInfoAsuransiTambahanIdeModel => _showMandatoryInfoAsuransiTambahanIdeModel;

  set showMandatoryInfoAsuransiTambahanIdeModel(ShowMandatoryInfoAsuransiTambahanModel value) {
    this._showMandatoryInfoAsuransiTambahanIdeModel = value;
  }
  // End Inf Asuransi Tambahan

  // Inf WMP
  ShowMandatoryInfoWMPModel get showMandatoryInfoWMPDakorCompanyModel => _showMandatoryInfoWMPDakorCompanyModel;

  set showMandatoryInfoWMPDakorCompanyModel(ShowMandatoryInfoWMPModel value) {
    this._showMandatoryInfoWMPDakorCompanyModel = value;
  }

  ShowMandatoryInfoWMPModel get showMandatoryInfoWMPDakorModel => _showMandatoryInfoWMPDakorModel;

  set showMandatoryInfoWMPDakorModel(ShowMandatoryInfoWMPModel value) {
    this._showMandatoryInfoWMPDakorModel = value;
  }

  ShowMandatoryInfoWMPModel get showMandatoryInfoWMPResurveyCompanyModel => _showMandatoryInfoWMPResurveyCompanyModel;

  set showMandatoryInfoWMPResurveyCompanyModel(ShowMandatoryInfoWMPModel value) {
    this._showMandatoryInfoWMPResurveyCompanyModel = value;
  }

  ShowMandatoryInfoWMPModel get showMandatoryInfoWMPResurveyModel => _showMandatoryInfoWMPResurveyModel;

  set showMandatoryInfoWMPResurveyModel(ShowMandatoryInfoWMPModel value) {
    this._showMandatoryInfoWMPResurveyModel = value;
  }

  ShowMandatoryInfoWMPModel get showMandatoryInfoWMPPacCompanyModel => _showMandatoryInfoWMPPacCompanyModel;

  set showMandatoryInfoWMPPacCompanyModel(ShowMandatoryInfoWMPModel value) {
    this._showMandatoryInfoWMPPacCompanyModel = value;
  }

  ShowMandatoryInfoWMPModel get showMandatoryInfoWMPPacModel => _showMandatoryInfoWMPPacModel;

  set showMandatoryInfoWMPPacModel(ShowMandatoryInfoWMPModel value) {
    this._showMandatoryInfoWMPPacModel = value;
  }

  ShowMandatoryInfoWMPModel get showMandatoryInfoWMPRegulerSurveyCompanyModel => _showMandatoryInfoWMPRegulerSurveyCompanyModel;

  set showMandatoryInfoWMPRegulerSurveyCompanyModel(ShowMandatoryInfoWMPModel value) {
    this._showMandatoryInfoWMPRegulerSurveyCompanyModel = value;
  }

  ShowMandatoryInfoWMPModel get showMandatoryInfoWMPRegulerSurveyModel => _showMandatoryInfoWMPRegulerSurveyModel;

  set showMandatoryInfoWMPRegulerSurveyModel(ShowMandatoryInfoWMPModel value) {
    this._showMandatoryInfoWMPRegulerSurveyModel = value;
  }

  ShowMandatoryInfoWMPModel get showMandatoryInfoWMPIdeCompanyModel => _showMandatoryInfoWMPIdeCompanyModel;

  set showMandatoryInfoWMPIdeCompanyModel(ShowMandatoryInfoWMPModel value) {
    this._showMandatoryInfoWMPIdeCompanyModel = value;
  }

  ShowMandatoryInfoWMPModel get showMandatoryInfoWMPIdeModel => _showMandatoryInfoWMPIdeModel;

  set showMandatoryInfoWMPIdeModel(ShowMandatoryInfoWMPModel value) {
    this._showMandatoryInfoWMPIdeModel = value;
  }
  // End Inf WMP

  // Inf Subsidi
  ShowMandatoryInfoSubsidiModel get showMandatoryInfoSubsidiDakorCompanyModel => _showMandatoryInfoSubsidiDakorCompanyModel;

  set showMandatoryInfoSubsidiDakorCompanyModel(ShowMandatoryInfoSubsidiModel value) {
    this._showMandatoryInfoSubsidiDakorCompanyModel = value;
  }

  ShowMandatoryInfoSubsidiModel get showMandatoryInfoSubsidiDakorModel => _showMandatoryInfoSubsidiDakorModel;

  set showMandatoryInfoSubsidiDakorModel(ShowMandatoryInfoSubsidiModel value) {
    this._showMandatoryInfoSubsidiDakorModel = value;
  }

  ShowMandatoryInfoSubsidiModel get showMandatoryInfoSubsidiResurveyCompanyModel => _showMandatoryInfoSubsidiResurveyCompanyModel;

  set showMandatoryInfoSubsidiResurveyCompanyModel(ShowMandatoryInfoSubsidiModel value) {
    this._showMandatoryInfoSubsidiResurveyCompanyModel = value;
  }

  ShowMandatoryInfoSubsidiModel get showMandatoryInfoSubsidiResurveyModel => _showMandatoryInfoSubsidiResurveyModel;

  set showMandatoryInfoSubsidiResurveyModel(ShowMandatoryInfoSubsidiModel value) {
    this._showMandatoryInfoSubsidiResurveyModel = value;
  }

  ShowMandatoryInfoSubsidiModel get showMandatoryInfoSubsidiPacCompanyModel => _showMandatoryInfoSubsidiPacCompanyModel;

  set showMandatoryInfoSubsidiPacCompanyModel(ShowMandatoryInfoSubsidiModel value) {
    this._showMandatoryInfoSubsidiPacCompanyModel = value;
  }

  ShowMandatoryInfoSubsidiModel get showMandatoryInfoSubsidiPacModel => _showMandatoryInfoSubsidiPacModel;

  set showMandatoryInfoSubsidiPacModel(ShowMandatoryInfoSubsidiModel value) {
    this._showMandatoryInfoSubsidiPacModel = value;
  }

  ShowMandatoryInfoSubsidiModel get showMandatoryInfoSubsidiRegulerSurveyCompanyModel => _showMandatoryInfoSubsidiRegulerSurveyCompanyModel;

  set showMandatoryInfoSubsidiRegulerSurveyCompanyModel(ShowMandatoryInfoSubsidiModel value) {
    this._showMandatoryInfoSubsidiRegulerSurveyCompanyModel = value;
  }

  ShowMandatoryInfoSubsidiModel get showMandatoryInfoSubsidiRegulerSurveyModel => _showMandatoryInfoSubsidiRegulerSurveyModel;

  set showMandatoryInfoSubsidiRegulerSurveyModel(ShowMandatoryInfoSubsidiModel value) {
    this._showMandatoryInfoSubsidiRegulerSurveyModel = value;
  }

  ShowMandatoryInfoSubsidiModel get showMandatoryInfoSubsidiIdeCompanyModel => _showMandatoryInfoSubsidiIdeCompanyModel;

  set showMandatoryInfoSubsidiIdeCompanyModel(ShowMandatoryInfoSubsidiModel value) {
    this._showMandatoryInfoSubsidiIdeCompanyModel = value;
  }

  ShowMandatoryInfoSubsidiModel get showMandatoryInfoSubsidiIdeModel => _showMandatoryInfoSubsidiIdeModel;

  set showMandatoryInfoSubsidiIdeModel(ShowMandatoryInfoSubsidiModel value) {
    this._showMandatoryInfoSubsidiIdeModel = value;
  }
  //End Inf Subsidi

  // Inf Dokumen
  ShowMandatoryInfoDokumenModel get showMandatoryInfoDokumenDakorCompanyModel => _showMandatoryInfoDokumenDakorCompanyModel;

  set showMandatoryInfoDokumenDakorCompanyModel(ShowMandatoryInfoDokumenModel value) {
    this._showMandatoryInfoDokumenDakorCompanyModel = value;
  }

  ShowMandatoryInfoDokumenModel get showMandatoryInfoDokumenDakorModel => _showMandatoryInfoDokumenDakorModel;

  set showMandatoryInfoDokumenDakorModel(ShowMandatoryInfoDokumenModel value) {
    this._showMandatoryInfoDokumenDakorModel = value;
  }

  ShowMandatoryInfoDokumenModel get showMandatoryInfoDokumenResurveyCompanyModel => _showMandatoryInfoDokumenResurveyCompanyModel;

  set showMandatoryInfoDokumenResurveyCompanyModel(ShowMandatoryInfoDokumenModel value) {
    this._showMandatoryInfoDokumenResurveyCompanyModel = value;
  }

  ShowMandatoryInfoDokumenModel get showMandatoryInfoDokumenResurveyModel => _showMandatoryInfoDokumenResurveyModel;

  set showMandatoryInfoDokumenResurveyModel(ShowMandatoryInfoDokumenModel value) {
    this._showMandatoryInfoDokumenResurveyModel = value;
  }

  ShowMandatoryInfoDokumenModel get showMandatoryInfoDokumenPacCompanyModel => _showMandatoryInfoDokumenPacCompanyModel;

  set showMandatoryInfoDokumenPacCompanyModel(ShowMandatoryInfoDokumenModel value) {
    this._showMandatoryInfoDokumenPacCompanyModel = value;
  }

  ShowMandatoryInfoDokumenModel get showMandatoryInfoDokumenPacModel => _showMandatoryInfoDokumenPacModel;

  set showMandatoryInfoDokumenPacModel(ShowMandatoryInfoDokumenModel value) {
    this._showMandatoryInfoDokumenPacModel = value;
  }

  ShowMandatoryInfoDokumenModel get showMandatoryInfoDokumenRegulerSurveyCompanyModel => _showMandatoryInfoDokumenRegulerSurveyCompanyModel;

  set showMandatoryInfoDokumenRegulerSurveyCompanyModel(ShowMandatoryInfoDokumenModel value) {
    this._showMandatoryInfoDokumenRegulerSurveyCompanyModel = value;
  }

  ShowMandatoryInfoDokumenModel get showMandatoryInfoDokumenRegulerSurveyModel => _showMandatoryInfoDokumenRegulerSurveyModel;

  set showMandatoryInfoDokumenRegulerSurveyModel(ShowMandatoryInfoDokumenModel value) {
    this._showMandatoryInfoDokumenRegulerSurveyModel = value;
  }

  ShowMandatoryInfoDokumenModel get showMandatoryInfoDokumenIdeCompanyModel => _showMandatoryInfoDokumenIdeCompanyModel;

  set showMandatoryInfoDokumenIdeCompanyModel(ShowMandatoryInfoDokumenModel value) {
    this._showMandatoryInfoDokumenIdeCompanyModel = value;
  }

  ShowMandatoryInfoDokumenModel get showMandatoryInfoDokumenIdeModel => _showMandatoryInfoDokumenIdeModel;

  set showMandatoryInfoDokumenIdeModel(ShowMandatoryInfoDokumenModel value) {
    this._showMandatoryInfoDokumenIdeModel = value;
  }
  // End Inf Dokumen

  // Marketing Notes
  ShowMandatoryMarketingNotesModel get showMandatoryMarketingNotesDakorCompanyModel => _showMandatoryMarketingNotesDakorCompanyModel;

  set showMandatoryMarketingNotesDakorCompanyModel(ShowMandatoryMarketingNotesModel value) {
    this._showMandatoryMarketingNotesDakorCompanyModel = value;
  }

  ShowMandatoryMarketingNotesModel get showMandatoryMarketingNotesDakorModel => _showMandatoryMarketingNotesDakorModel;

  set showMandatoryMarketingNotesDakorModel(ShowMandatoryMarketingNotesModel value) {
    this._showMandatoryMarketingNotesDakorModel = value;
  }

  ShowMandatoryMarketingNotesModel get showMandatoryMarketingNotesResurveyCompanyModel => _showMandatoryMarketingNotesResurveyCompanyModel;

  set showMandatoryMarketingNotesResurveyCompanyModel(
      ShowMandatoryMarketingNotesModel value) {
    this._showMandatoryMarketingNotesResurveyCompanyModel = value;
  }

  ShowMandatoryMarketingNotesModel get showMandatoryMarketingNotesResurveyModel => _showMandatoryMarketingNotesResurveyModel;

  set showMandatoryMarketingNotesResurveyModel(ShowMandatoryMarketingNotesModel value) {
    this._showMandatoryMarketingNotesResurveyModel = value;
  }

  ShowMandatoryMarketingNotesModel get showMandatoryMarketingNotesPacCompanyModel => _showMandatoryMarketingNotesPacCompanyModel;

  set showMandatoryMarketingNotesPacCompanyModel(
      ShowMandatoryMarketingNotesModel value) {
    this._showMandatoryMarketingNotesPacCompanyModel = value;
  }

  ShowMandatoryMarketingNotesModel get showMandatoryMarketingNotesPacModel => _showMandatoryMarketingNotesPacModel;

  set showMandatoryMarketingNotesPacModel(
      ShowMandatoryMarketingNotesModel value) {
    this._showMandatoryMarketingNotesPacModel = value;
  }

  ShowMandatoryMarketingNotesModel get showMandatoryMarketingNotesRegulerSurveyCompanyModel => _showMandatoryMarketingNotesRegulerSurveyCompanyModel;

  set showMandatoryMarketingNotesRegulerSurveyCompanyModel(
      ShowMandatoryMarketingNotesModel value) {
    this._showMandatoryMarketingNotesRegulerSurveyCompanyModel = value;
  }

  ShowMandatoryMarketingNotesModel get showMandatoryMarketingNotesRegulerSurveyModel => _showMandatoryMarketingNotesRegulerSurveyModel;

  set showMandatoryMarketingNotesRegulerSurveyModel(
      ShowMandatoryMarketingNotesModel value) {
    this._showMandatoryMarketingNotesRegulerSurveyModel = value;
  }

  ShowMandatoryMarketingNotesModel get showMandatoryMarketingNotesIdeCompanyModel => _showMandatoryMarketingNotesIdeCompanyModel;

  set showMandatoryMarketingNotesIdeCompanyModel(
      ShowMandatoryMarketingNotesModel value) {
    this._showMandatoryMarketingNotesIdeCompanyModel = value;
  }

  ShowMandatoryMarketingNotesModel get showMandatoryMarketingNotesIdeModel => _showMandatoryMarketingNotesIdeModel;

  set showMandatoryMarketingNotesIdeModel(
      ShowMandatoryMarketingNotesModel value) {
    this._showMandatoryMarketingNotesIdeModel = value;
  }
  // End Marketing Notes

  ShowMandatoryEnabledResultSurvey get showMandatoryResultSurveyRegularSurvey => _showMandatoryResultSurveyRegularSurvey;

  set showMandatoryResultSurveyRegularSurvey(ShowMandatoryEnabledResultSurvey value) {
    this._showMandatoryResultSurveyRegularSurvey = value;
  }

  bool _loadData = false;

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
    notifyListeners();
  }

  void getListParam() async{
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);
    String _listParameter = await storage.read(key: "ListParameter");
    final _response = await _http.get(
        "${BaseUrl.urlGeneral}$_listParameter", //api/list-param/get_list_param,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      print(_result);
    }
    else{
      print("error ${_response.statusCode}");
    }
  }

  //individu hideshow
  List _dataHideShowIde;
  List _dataHideShowFormMRegulerSurvey;
  List _dataHideShowPACInd;
  List _dataHideShowFormMResurvey;
  List _dataHideShowFormMDakor;

  //individu mandatory
  List _dataMandatoryIde;
  List _dataMandatoryFormMRegulerSurvey;
  List _dataMandatoryPACInd;
  List _dataMandatoryFormMResurvey;
  List _dataMandatoryFormMDakor;

  //company hideshow
  List _dataHSIdeCom;
  List _dataHideShowFormMRegulerSurveyCompany;
  List _dataHideShowFormMPACCompany;
  List _dataHideShowFormMResurveyCompany;
  List _dataHideShowFormMDakorCom;

  //company mandatory
  List _dataMandatoryIdeCom;
  List _dataMandatoryFormMRegulerSurveyCompany;
  List _dataMandatoryFormMPACCompany;
  List _dataMandatoryFormMResurveyCompany;
  List _dataMandatoryFormMDakorCom;

  Future<void> getData(BuildContext context) async{
    this._loadData = true;
    try{
      //individu hideshow
      _dataHideShowIde = await getHideShowDataIde();
      _dataHideShowFormMRegulerSurvey = await getHideShowFormMRegularSurvey();
      _dataHideShowPACInd = await getHideShowFormMPac();
      _dataHideShowFormMResurvey = await getHideShowFormMResurvey();
      _dataHideShowFormMDakor = await getHideShowFormMDakor();

      //individu mandatory
      _dataMandatoryIde = await getMandatoryDataIde();
      _dataMandatoryFormMRegulerSurvey = await getMandatoryFormMRegularSurvey();
      _dataMandatoryPACInd = await getMandatoryFormMPac();
      _dataMandatoryFormMResurvey = await getMandatoryFormMResurvey();
      _dataMandatoryFormMDakor = await getMandatoryFormMDakor();

      //company hideshow
      _dataHSIdeCom = await getHideShowDataIdeCom();
      _dataHideShowFormMRegulerSurveyCompany = await getHideShowFormMRegularSurveyCom();
      _dataHideShowFormMPACCompany = await getHideShowFormMPacCom();
      _dataHideShowFormMResurveyCompany = await getHideShowFormMResurveyCom();
      _dataHideShowFormMDakorCom = await getHideShowFormMDakorCom();

      //company mandatory
      _dataMandatoryIdeCom = await getMandatoryDataIdeCom();
      _dataMandatoryFormMRegulerSurveyCompany = await getMandatoryFormMRegularSurveyCom();
      _dataMandatoryFormMPACCompany = await getMandatoryFormMPacCom();
      _dataMandatoryFormMResurveyCompany = await getMandatoryFormMResurveyCom();
      _dataMandatoryFormMDakorCom = await getMandatoryFormMDakorCom();

      // List API Form App
      List _dataHideShowAppIde = await getHideShowDataAppIde();
      List _dataMandatoryAppIde = await getMandatoryDataAppIde();
      List _dataHideShowAppIdeCompany = await getHideShowDataAppIdeCom();
      List _dataMandatoryAppIdeCompany = await getMandatoryDataAppIdeCom();

      List _dataHideShowAppRegulerSurvey = await getHideShowAppRegularSurvey();
      List _dataMandatoryAppRegulerSurvey = await getMandatoryAppRegularSurvey();//suspect 1
      List _dataHideShowAppRegulerSurveyCompany = await getHideShowAppRegularSurveyCom();
      List _dataMandatoryAppRegulerSurveyCompany = await getMandatoryAppRegularSurveyCom();

      List _dataHideShowAppPac = await getHideShowAppPAC();
      List _dataMandatoryAppPac = await getMandatoryAppPAC();
      List _dataHideShowAppPacCompany = await getHideShowAppPACCom();
      List _dataMandatoryAppPacCompany = await getMandatoryAppPACCom();

      List _dataHideShowAppResurvey = await getHideShowAppResurvey();
      List _dataMandatoryAppResurvey = await getMandatoryAppResurvey();
      List _dataHideShowAppResurveyCompany = await getHideShowAppResurveyCom();
      List _dataMandatoryAppResurveyCompany = await getMandatoryAppResurveyCom();

      List _dataHideShowAppDakor = await getHideShowAppDakor();
      List _dataMandatoryAppDakor = await getMandatoryAppDakor();
      List _dataHideShowAppDakorCom = await getHideShowAppDakorCom();
      List _dataMandatoryAppDakorCom = await getMandatoryAppDakorCom();

      // Inf Aplikasi
      // IDE
      _showMandatoryInfoAppIdeModel = ShowMandatoryInfoAppModel(
        _dataHideShowAppIde[0]['tgl_PEMESANAN']== "1",
        _dataHideShowAppIde[0]['tanggal_JANJI_SURVEY']== "1",
        _dataHideShowAppIde[0]['pk_TELAH_DITANDATANGANI']== "1",
        _dataHideShowAppIde[0]['jenis_KONSEP']== "1",
        _dataHideShowAppIde[0]['jumlah_OBJEK']== "1",
        _dataHideShowAppIde[0]['jenis_PROPOSIONAL_ASURANSI']== "1",
        _dataHideShowAppIde[0]['unit_KE']== "1",
        _dataMandatoryAppIde[0]['tgl_PEMESANAN']== "1",
        _dataMandatoryAppIde[0]['tanggal_JANJI_SURVEY']== "1",
        _dataMandatoryAppIde[0]['pk_TELAH_DITANDATANGANI']== "1",
        _dataMandatoryAppIde[0]['jenis_KONSEP']== "1",
        _dataMandatoryAppIde[0]['jumlah_OBJEK']== "1",
        _dataMandatoryAppIde[0]['jenis_PROPOSIONAL_ASURANSI']== "1",
        _dataMandatoryAppIde[0]['unit_KE']== "1",
      );
      _showMandatoryInfoAppIdeCompanyModel = ShowMandatoryInfoAppModel(
        _dataHideShowAppIdeCompany[0]['tgl_PEMESANAN']== "1",
        _dataHideShowAppIdeCompany[0]['tanggal_JANJI_SURVEY']== "1",
        _dataHideShowAppIdeCompany[0]['pk_TELAH_DITANDATANGANI']== "1",
        _dataHideShowAppIdeCompany[0]['jenis_KONSEP']== "1",
        _dataHideShowAppIdeCompany[0]['jumlah_OBJEK']== "1",
        _dataHideShowAppIdeCompany[0]['jenis_PROPOSIONAL_ASURANSI']== "1",
        _dataHideShowAppIdeCompany[0]['unit_KE']== "1",
        _dataMandatoryAppIdeCompany[0]['tgl_PEMESANAN']== "1",
        _dataMandatoryAppIdeCompany[0]['tanggal_JANJI_SURVEY']== "1",
        _dataMandatoryAppIdeCompany[0]['pk_TELAH_DITANDATANGANI']== "1",
        _dataMandatoryAppIdeCompany[0]['jenis_KONSEP']== "1",
        _dataMandatoryAppIdeCompany[0]['jumlah_OBJEK']== "1",
        _dataMandatoryAppIdeCompany[0]['jenis_PROPOSIONAL_ASURANSI']== "1",
        _dataMandatoryAppIdeCompany[0]['unit_KE']== "1",
      );
      // Reguler Survey
      _showMandatoryInfoAppRegulerSurveyModel = ShowMandatoryInfoAppModel(
        _dataHideShowAppRegulerSurvey[0]['tgl_PEMESANAN']== "1",
        _dataHideShowAppRegulerSurvey[0]['tanggal_JANJI_SURVEY']== "1",
        _dataHideShowAppRegulerSurvey[0]['pk_TELAH_DITANDATANGANI']== "1",
        _dataHideShowAppRegulerSurvey[0]['jenis_KONSEP']== "1",
        _dataHideShowAppRegulerSurvey[0]['jumlah_OBJEK']== "1",
        _dataHideShowAppRegulerSurvey[0]['jenis_PROPOSIONAL_ASURANSI']== "1",
        _dataHideShowAppRegulerSurvey[0]['unit_KE']== "1",
        _dataMandatoryAppRegulerSurvey[0]['tgl_PEMESANAN']== "1",
        _dataMandatoryAppRegulerSurvey[0]['tanggal_JANJI_SURVEY']== "1",
        _dataMandatoryAppRegulerSurvey[0]['pk_TELAH_DITANDATANGANI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jenis_KONSEP']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jumlah_OBJEK']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jenis_PROPOSIONAL_ASURANSI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['unit_KE']== "1",
      );
      _showMandatoryInfoAppRegulerSurveyCompanyModel = ShowMandatoryInfoAppModel(
        _dataHideShowAppRegulerSurveyCompany[0]['tgl_PEMESANAN']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['tanggal_JANJI_SURVEY']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['pk_TELAH_DITANDATANGANI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jenis_KONSEP']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jumlah_OBJEK']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jenis_PROPOSIONAL_ASURANSI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['unit_KE']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['tgl_PEMESANAN']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['tanggal_JANJI_SURVEY']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['pk_TELAH_DITANDATANGANI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jenis_KONSEP']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jumlah_OBJEK']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jenis_PROPOSIONAL_ASURANSI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['unit_KE']== "1",
      );
      // PAC
      _showMandatoryInfoAppPacModel = ShowMandatoryInfoAppModel(
        _dataHideShowAppPac[0]['tgl_PEMESANAN']== "1",
        _dataHideShowAppPac[0]['tanggal_JANJI_SURVEY']== "1",
        _dataHideShowAppPac[0]['pk_TELAH_DITANDATANGANI']== "1",
        _dataHideShowAppPac[0]['jenis_KONSEP']== "1",
        _dataHideShowAppPac[0]['jumlah_OBJEK']== "1",
        _dataHideShowAppPac[0]['jenis_PROPOSIONAL_ASURANSI']== "1",
        _dataHideShowAppPac[0]['unit_KE']== "1",
        _dataMandatoryAppPac[0]['tgl_PEMESANAN']== "1",
        _dataMandatoryAppPac[0]['tanggal_JANJI_SURVEY']== "1",
        _dataMandatoryAppPac[0]['pk_TELAH_DITANDATANGANI']== "1",
        _dataMandatoryAppPac[0]['jenis_KONSEP']== "1",
        _dataMandatoryAppPac[0]['jumlah_OBJEK']== "1",
        _dataMandatoryAppPac[0]['jenis_PROPOSIONAL_ASURANSI']== "1",
        _dataMandatoryAppPac[0]['unit_KE']== "1",
      );
      _showMandatoryInfoAppPacCompanyModel = ShowMandatoryInfoAppModel(
        _dataHideShowAppPacCompany[0]['tgl_PEMESANAN']== "1",
        _dataHideShowAppPacCompany[0]['tanggal_JANJI_SURVEY']== "1",
        _dataHideShowAppPacCompany[0]['pk_TELAH_DITANDATANGANI']== "1",
        _dataHideShowAppPacCompany[0]['jenis_KONSEP']== "1",
        _dataHideShowAppPacCompany[0]['jumlah_OBJEK']== "1",
        _dataHideShowAppPacCompany[0]['jenis_PROPOSIONAL_ASURANSI']== "1",
        _dataHideShowAppPacCompany[0]['unit_KE']== "1",
        _dataMandatoryAppPacCompany[0]['tgl_PEMESANAN']== "1",
        _dataMandatoryAppPacCompany[0]['tanggal_JANJI_SURVEY']== "1",
        _dataMandatoryAppPacCompany[0]['pk_TELAH_DITANDATANGANI']== "1",
        _dataMandatoryAppPacCompany[0]['jenis_KONSEP']== "1",
        _dataMandatoryAppPacCompany[0]['jumlah_OBJEK']== "1",
        _dataMandatoryAppPacCompany[0]['jenis_PROPOSIONAL_ASURANSI']== "1",
        _dataMandatoryAppPacCompany[0]['unit_KE']== "1",
      );
      // Resurvey
      _showMandatoryInfoAppResurveyModel = ShowMandatoryInfoAppModel(
        _dataHideShowAppResurvey[0]['tgl_PEMESANAN']== "1",
        _dataHideShowAppResurvey[0]['tanggal_JANJI_SURVEY']== "1",
        _dataHideShowAppResurvey[0]['pk_TELAH_DITANDATANGANI']== "1",
        _dataHideShowAppResurvey[0]['jenis_KONSEP']== "1",
        _dataHideShowAppResurvey[0]['jumlah_OBJEK']== "1",
        _dataHideShowAppResurvey[0]['jenis_PROPOSIONAL_ASURANSI']== "1",
        _dataHideShowAppResurvey[0]['unit_KE']== "1",
        _dataMandatoryAppResurvey[0]['tgl_PEMESANAN']== "1",
        _dataMandatoryAppResurvey[0]['tanggal_JANJI_SURVEY']== "1",
        _dataMandatoryAppResurvey[0]['pk_TELAH_DITANDATANGANI']== "1",
        _dataMandatoryAppResurvey[0]['jenis_KONSEP']== "1",
        _dataMandatoryAppResurvey[0]['jumlah_OBJEK']== "1",
        _dataMandatoryAppResurvey[0]['jenis_PROPOSIONAL_ASURANSI']== "1",
        _dataMandatoryAppResurvey[0]['unit_KE']== "1",
      );
      _showMandatoryInfoAppResurveyCompanyModel = ShowMandatoryInfoAppModel(
        _dataHideShowAppResurveyCompany[0]['tgl_PEMESANAN']== "1",
        _dataHideShowAppResurveyCompany[0]['tanggal_JANJI_SURVEY']== "1",
        _dataHideShowAppResurveyCompany[0]['pk_TELAH_DITANDATANGANI']== "1",
        _dataHideShowAppResurveyCompany[0]['jenis_KONSEP']== "1",
        _dataHideShowAppResurveyCompany[0]['jumlah_OBJEK']== "1",
        _dataHideShowAppResurveyCompany[0]['jenis_PROPOSIONAL_ASURANSI']== "1",
        _dataHideShowAppResurveyCompany[0]['unit_KE']== "1",
        _dataMandatoryAppResurveyCompany[0]['tgl_PEMESANAN']== "1",
        _dataMandatoryAppResurveyCompany[0]['tanggal_JANJI_SURVEY']== "1",
        _dataMandatoryAppResurveyCompany[0]['pk_TELAH_DITANDATANGANI']== "1",
        _dataMandatoryAppResurveyCompany[0]['jenis_KONSEP']== "1",
        _dataMandatoryAppResurveyCompany[0]['jumlah_OBJEK']== "1",
        _dataMandatoryAppResurveyCompany[0]['jenis_PROPOSIONAL_ASURANSI']== "1",
        _dataMandatoryAppResurveyCompany[0]['unit_KE']== "1",
      );
      // Dakor
      _showMandatoryInfoAppDakorModel = ShowMandatoryInfoAppModel(
        _dataHideShowAppDakor[0]['tgl_PEMESANAN']== "1",
        _dataHideShowAppDakor[0]['tanggal_JANJI_SURVEY']== "1",
        _dataHideShowAppDakor[0]['pk_TELAH_DITANDATANGANI']== "1",
        _dataHideShowAppDakor[0]['jenis_KONSEP']== "1",
        _dataHideShowAppDakor[0]['jumlah_OBJEK']== "1",
        _dataHideShowAppDakor[0]['jenis_PROPOSIONAL_ASURANSI']== "1",
        _dataHideShowAppDakor[0]['unit_KE']== "1",
        _dataMandatoryAppDakor[0]['tgl_PEMESANAN']== "1",
        _dataMandatoryAppDakor[0]['tanggal_JANJI_SURVEY']== "1",
        _dataMandatoryAppDakor[0]['pk_TELAH_DITANDATANGANI']== "1",
        _dataMandatoryAppDakor[0]['jenis_KONSEP']== "1",
        _dataMandatoryAppDakor[0]['jumlah_OBJEK']== "1",
        _dataMandatoryAppDakor[0]['jenis_PROPOSIONAL_ASURANSI']== "1",
        _dataMandatoryAppDakor[0]['unit_KE']== "1",
      );
      _showMandatoryInfoAppDakorCompanyModel = ShowMandatoryInfoAppModel(
        _dataHideShowAppDakorCom[0]['tgl_PEMESANAN']== "1",
        _dataHideShowAppDakorCom[0]['tanggal_JANJI_SURVEY']== "1",
        _dataHideShowAppDakorCom[0]['pk_TELAH_DITANDATANGANI']== "1",
        _dataHideShowAppDakorCom[0]['jenis_KONSEP']== "1",
        _dataHideShowAppDakorCom[0]['jumlah_OBJEK']== "1",
        _dataHideShowAppDakorCom[0]['jenis_PROPOSIONAL_ASURANSI']== "1",
        _dataHideShowAppDakorCom[0]['unit_KE']== "1",
        _dataMandatoryAppDakorCom[0]['tgl_PEMESANAN']== "1",
        _dataMandatoryAppDakorCom[0]['tanggal_JANJI_SURVEY']== "1",
        _dataMandatoryAppDakorCom[0]['pk_TELAH_DITANDATANGANI']== "1",
        _dataMandatoryAppDakorCom[0]['jenis_KONSEP']== "1",
        _dataMandatoryAppDakorCom[0]['jumlah_OBJEK']== "1",
        _dataMandatoryAppDakorCom[0]['jenis_PROPOSIONAL_ASURANSI']== "1",
        _dataMandatoryAppDakorCom[0]['unit_KE']== "1",
      );
      // End Inf Aplikasi

      // Inf Object Unit
      // IDE
      _showMandatoryInfoObjectUnitIdeModel = ShowMandatoryInfoObjectUnitModel(
        _dataHideShowAppIde[0]['jenis_PEMBIAYAAN']== "1",
        _dataHideShowAppIde[0]['kegiatan_USAHA']== "1",
        _dataHideShowAppIde[0]['jenis_KEGIATAN_USAHA']== "1",
        _dataHideShowAppIde[0]['grup_OBJEK']== "1",
        _dataHideShowAppIde[0]['objek']== "1",
        _dataHideShowAppIde[0]['jenis_PRODUK']== "1",
        _dataHideShowAppIde[0]['merek_OBJEK']== "1",
        _dataHideShowAppIde[0]['jenis_OBJEK']== "1",
        _dataHideShowAppIde[0]['model_OBJEK']== "1",
        _dataHideShowAppIde[0]['model_DETAIL']== "1",
        _dataHideShowAppIde[0]['pemakaian_OBJECT']== "1",
        _dataHideShowAppIde[0]['tujuan_PENGGUNAAN_OBJEK']== "1",
        _dataHideShowAppIde[0]['grup_SALES']== "1",
        _dataHideShowAppIde[0]['group_ID']== "1",
        _dataHideShowAppIde[0]['sumber_ORDER']== "1",
        _dataHideShowAppIde[0]['nama_SUMBER_ORDER']== "1",
        _dataHideShowAppIde[0]['jenis_PIHAK_KETIGA']== "1",
        _dataHideShowAppIde[0]['pihak_KETIGA']== "1",
        _dataHideShowAppIde[0]['matriks_DEALER']== "1",
        _dataHideShowAppIde[0]['kegiatan']== "1",
        _dataHideShowAppIde[0]['sentra_D']== "1",
        _dataHideShowAppIde[0]['unit_D']== "1",
        _dataHideShowAppIde[0]['program']== "1",
        _dataHideShowAppIde[0]['jenis_REHAB']== "1",
        _dataHideShowAppIde[0]['no_REFERENCE']== "1",
        _dataMandatoryAppIde[0]['jenis_PEMBIAYAAN']== "1",
        _dataMandatoryAppIde[0]['kegiatan_USAHA']== "1",
        _dataMandatoryAppIde[0]['jenis_KEGIATAN_USAHA']== "1",
        _dataMandatoryAppIde[0]['grup_OBJEK']== "1",
        _dataMandatoryAppIde[0]['objek']== "1",
        _dataMandatoryAppIde[0]['jenis_PRODUK']== "1",
        _dataMandatoryAppIde[0]['merek_OBJEK']== "1",
        _dataMandatoryAppIde[0]['jenis_OBJEK']== "1",
        _dataMandatoryAppIde[0]['model_OBJEK']== "1",
        _dataMandatoryAppIde[0]['model_DETAIL']== "1",
        _dataMandatoryAppIde[0]['pemakaian_OBJECT']== "1",
        _dataMandatoryAppIde[0]['tujuan_PENGGUNAAN_OBJEK']== "1",
        _dataMandatoryAppIde[0]['grup_SALES']== "1",
        _dataMandatoryAppIde[0]['group_ID']== "1",
        _dataMandatoryAppIde[0]['sumber_ORDER']== "1",
        _dataMandatoryAppIde[0]['nama_SUMBER_ORDER']== "1",
        _dataMandatoryAppIde[0]['jenis_PIHAK_KETIGA']== "1",
        _dataMandatoryAppIde[0]['pihak_KETIGA']== "1",
        _dataMandatoryAppIde[0]['matriks_DEALER'] == "1",
        _dataMandatoryAppIde[0]['kegiatan']== "1",
        _dataMandatoryAppIde[0]['sentra_D']== "1",
        _dataMandatoryAppIde[0]['unit_D']== "1",
        _dataMandatoryAppIde[0]['program']== "1",
        _dataMandatoryAppIde[0]['jenis_REHAB']== "1",
        _dataMandatoryAppIde[0]['no_REFERENCE']== "1",
      );
      _showMandatoryInfoObjectUnitIdeCompanyModel = ShowMandatoryInfoObjectUnitModel(
        _dataHideShowAppIdeCompany[0]['jenis_PEMBIAYAAN']== "1",
        _dataHideShowAppIdeCompany[0]['kegiatan_USAHA']== "1",
        _dataHideShowAppIdeCompany[0]['jenis_KEGIATAN_USAHA']== "1",
        _dataHideShowAppIdeCompany[0]['grup_OBJEK']== "1",
        _dataHideShowAppIdeCompany[0]['objek']== "1",
        _dataHideShowAppIdeCompany[0]['jenis_PRODUK']== "1",
        _dataHideShowAppIdeCompany[0]['merek_OBJEK']== "1",
        _dataHideShowAppIdeCompany[0]['jenis_OBJEK']== "1",
        _dataHideShowAppIdeCompany[0]['model_OBJEK']== "1",
        _dataHideShowAppIdeCompany[0]['model_DETAIL']== "1",
        _dataHideShowAppIdeCompany[0]['pemakaian_OBJECT']== "1",
        _dataHideShowAppIdeCompany[0]['tujuan_PENGGUNAAN_OBJEK']== "1",
        _dataHideShowAppIdeCompany[0]['grup_SALES']== "1",
        _dataHideShowAppIdeCompany[0]['group_ID']== "1",
        _dataHideShowAppIdeCompany[0]['sumber_ORDER']== "1",
        _dataHideShowAppIdeCompany[0]['nama_SUMBER_ORDER']== "1",
        _dataHideShowAppIdeCompany[0]['jenis_PIHAK_KETIGA']== "1",
        _dataHideShowAppIdeCompany[0]['pihak_KETIGA']== "1",
        _dataHideShowAppIdeCompany[0]['matriks_DEALER']== "1",
        _dataHideShowAppIdeCompany[0]['kegiatan']== "1",
        _dataHideShowAppIdeCompany[0]['sentra_D']== "1",
        _dataHideShowAppIdeCompany[0]['unit_D']== "1",
        _dataHideShowAppIdeCompany[0]['program']== "1",
        _dataHideShowAppIdeCompany[0]['jenis_REHAB']== "1",
        _dataHideShowAppIdeCompany[0]['no_REFERENCE']== "1",
        _dataMandatoryAppIdeCompany[0]['jenis_PEMBIAYAAN']== "1",
        _dataMandatoryAppIdeCompany[0]['kegiatan_USAHA']== "1",
        _dataMandatoryAppIdeCompany[0]['jenis_KEGIATAN_USAHA']== "1",
        _dataMandatoryAppIdeCompany[0]['grup_OBJEK']== "1",
        _dataMandatoryAppIdeCompany[0]['objek']== "1",
        _dataMandatoryAppIdeCompany[0]['jenis_PRODUK']== "1",
        _dataMandatoryAppIdeCompany[0]['merek_OBJEK']== "1",
        _dataMandatoryAppIdeCompany[0]['jenis_OBJEK']== "1",
        _dataMandatoryAppIdeCompany[0]['model_OBJEK']== "1",
        _dataMandatoryAppIdeCompany[0]['model_DETAIL']== "1",
        _dataMandatoryAppIdeCompany[0]['pemakaian_OBJECT']== "1",
        _dataMandatoryAppIdeCompany[0]['tujuan_PENGGUNAAN_OBJEK']== "1",
        _dataMandatoryAppIdeCompany[0]['grup_SALES']== "1",
        _dataMandatoryAppIdeCompany[0]['group_ID']== "1",
        _dataMandatoryAppIdeCompany[0]['sumber_ORDER']== "1",
        _dataMandatoryAppIdeCompany[0]['nama_SUMBER_ORDER']== "1",
        _dataMandatoryAppIdeCompany[0]['jenis_PIHAK_KETIGA']== "1",
        _dataMandatoryAppIdeCompany[0]['pihak_KETIGA']== "1",
        _dataMandatoryAppIdeCompany[0]['matriks_DEALER']== "1",
        _dataMandatoryAppIdeCompany[0]['kegiatan']== "1",
        _dataMandatoryAppIdeCompany[0]['sentra_D']== "1",
        _dataMandatoryAppIdeCompany[0]['unit_D']== "1",
        _dataMandatoryAppIdeCompany[0]['program']== "1",
        _dataMandatoryAppIdeCompany[0]['jenis_REHAB']== "1",
        _dataMandatoryAppIdeCompany[0]['no_REFERENCE']== "1",
      );
      // Reguler Survey
      _showMandatoryInfoObjectUnitRegulerSurveyModel = ShowMandatoryInfoObjectUnitModel(
        _dataHideShowAppRegulerSurvey[0]['jenis_PEMBIAYAAN']== "1",
        _dataHideShowAppRegulerSurvey[0]['kegiatan_USAHA']== "1",
        _dataHideShowAppRegulerSurvey[0]['jenis_KEGIATAN_USAHA']== "1",
        _dataHideShowAppRegulerSurvey[0]['grup_OBJEK']== "1",
        _dataHideShowAppRegulerSurvey[0]['objek']== "1",
        _dataHideShowAppRegulerSurvey[0]['jenis_PRODUK']== "1",
        _dataHideShowAppRegulerSurvey[0]['merek_OBJEK']== "1",
        _dataHideShowAppRegulerSurvey[0]['jenis_OBJEK']== "1",
        _dataHideShowAppRegulerSurvey[0]['model_OBJEK']== "1",
        _dataHideShowAppRegulerSurvey[0]['model_DETAIL']== "1",
        _dataHideShowAppRegulerSurvey[0]['pemakaian_OBJECT']== "1",
        _dataHideShowAppRegulerSurvey[0]['tujuan_PENGGUNAAN_OBJEK']== "1",
        _dataHideShowAppRegulerSurvey[0]['grup_SALES']== "1",
        _dataHideShowAppRegulerSurvey[0]['group_ID']== "1",
        _dataHideShowAppRegulerSurvey[0]['sumber_ORDER']== "1",
        _dataHideShowAppRegulerSurvey[0]['nama_SUMBER_ORDER']== "1",
        _dataHideShowAppRegulerSurvey[0]['jenis_PIHAK_KETIGA']== "1",
        _dataHideShowAppRegulerSurvey[0]['pihak_KETIGA']== "1",
        _dataHideShowAppRegulerSurvey[0]['matriks_DEALER']== "1",
        _dataHideShowAppRegulerSurvey[0]['kegiatan']== "1",
        _dataHideShowAppRegulerSurvey[0]['sentra_D']== "1",
        _dataHideShowAppRegulerSurvey[0]['unit_D']== "1",
        _dataHideShowAppRegulerSurvey[0]['program']== "1",
        _dataHideShowAppRegulerSurvey[0]['jenis_REHAB']== "1",
        _dataHideShowAppRegulerSurvey[0]['no_REFERENCE']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jenis_PEMBIAYAAN']== "1",
        _dataMandatoryAppRegulerSurvey[0]['kegiatan_USAHA']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jenis_KEGIATAN_USAHA']== "1",
        _dataMandatoryAppRegulerSurvey[0]['grup_OBJEK']== "1",
        _dataMandatoryAppRegulerSurvey[0]['objek']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jenis_PRODUK']== "1",
        _dataMandatoryAppRegulerSurvey[0]['merek_OBJEK']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jenis_OBJEK']== "1",
        _dataMandatoryAppRegulerSurvey[0]['model_OBJEK']== "1",
        _dataMandatoryAppRegulerSurvey[0]['model_DETAIL']== "1",
        _dataMandatoryAppRegulerSurvey[0]['pemakaian_OBJECT']== "1",
        _dataMandatoryAppRegulerSurvey[0]['tujuan_PENGGUNAAN_OBJEK']== "1",
        _dataMandatoryAppRegulerSurvey[0]['grup_SALES']== "1",
        _dataMandatoryAppRegulerSurvey[0]['group_ID']== "1",
        _dataMandatoryAppRegulerSurvey[0]['sumber_ORDER']== "1",
        _dataMandatoryAppRegulerSurvey[0]['nama_SUMBER_ORDER']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jenis_PIHAK_KETIGA']== "1",
        _dataMandatoryAppRegulerSurvey[0]['pihak_KETIGA']== "1",
        _dataMandatoryAppRegulerSurvey[0]['matriks_DEALER']== "1",
        _dataMandatoryAppRegulerSurvey[0]['kegiatan']== "1",
        _dataMandatoryAppRegulerSurvey[0]['sentra_D']== "1",
        _dataMandatoryAppRegulerSurvey[0]['unit_D']== "1",
        _dataMandatoryAppRegulerSurvey[0]['program']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jenis_REHAB']== "1",
        _dataMandatoryAppRegulerSurvey[0]['no_REFERENCE']== "1",
      );
      _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel = ShowMandatoryInfoObjectUnitModel(
        _dataHideShowAppRegulerSurveyCompany[0]['jenis_PEMBIAYAAN']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['kegiatan_USAHA']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jenis_KEGIATAN_USAHA']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['grup_OBJEK']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['objek']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jenis_PRODUK']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['merek_OBJEK']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jenis_OBJEK']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['model_OBJEK']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['model_DETAIL']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['pemakaian_OBJECT']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['tujuan_PENGGUNAAN_OBJEK']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['grup_SALES']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['group_ID']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['sumber_ORDER']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['nama_SUMBER_ORDER']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jenis_PIHAK_KETIGA']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['pihak_KETIGA']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['matriks_DEALER']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['kegiatan']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['sentra_D']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['unit_D']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['program']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jenis_REHAB']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['no_REFERENCE']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jenis_PEMBIAYAAN']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['kegiatan_USAHA']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jenis_KEGIATAN_USAHA']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['grup_OBJEK']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['objek']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jenis_PRODUK']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['merek_OBJEK']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jenis_OBJEK']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['model_OBJEK']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['model_DETAIL']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['pemakaian_OBJECT']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['tujuan_PENGGUNAAN_OBJEK']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['grup_SALES']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['group_ID']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['sumber_ORDER']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['nama_SUMBER_ORDER']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jenis_PIHAK_KETIGA']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['pihak_KETIGA']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['matriks_DEALER']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['kegiatan']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['sentra_D']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['unit_D']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['program']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jenis_REHAB']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['no_REFERENCE']== "1",
      );
      // PAC
      _showMandatoryInfoObjectUnitPacModel = ShowMandatoryInfoObjectUnitModel(
        _dataHideShowAppPac[0]['jenis_PEMBIAYAAN']== "1",
        _dataHideShowAppPac[0]['kegiatan_USAHA']== "1",
        _dataHideShowAppPac[0]['jenis_KEGIATAN_USAHA']== "1",
        _dataHideShowAppPac[0]['grup_OBJEK']== "1",
        _dataHideShowAppPac[0]['objek']== "1",
        _dataHideShowAppPac[0]['jenis_PRODUK']== "1",
        _dataHideShowAppPac[0]['merek_OBJEK']== "1",
        _dataHideShowAppPac[0]['jenis_OBJEK']== "1",
        _dataHideShowAppPac[0]['model_OBJEK']== "1",
        _dataHideShowAppPac[0]['model_DETAIL']== "1",
        _dataHideShowAppPac[0]['pemakaian_OBJECT']== "1",
        _dataHideShowAppPac[0]['tujuan_PENGGUNAAN_OBJEK']== "1",
        _dataHideShowAppPac[0]['grup_SALES']== "1",
        _dataHideShowAppPac[0]['group_ID']== "1",
        _dataHideShowAppPac[0]['sumber_ORDER']== "1",
        _dataHideShowAppPac[0]['nama_SUMBER_ORDER']== "1",
        _dataHideShowAppPac[0]['jenis_PIHAK_KETIGA']== "1",
        _dataHideShowAppPac[0]['pihak_KETIGA']== "1",
        _dataHideShowAppPac[0]['matriks_DEALER']== "1",
        _dataHideShowAppPac[0]['kegiatan']== "1",
        _dataHideShowAppPac[0]['sentra_D']== "1",
        _dataHideShowAppPac[0]['unit_D']== "1",
        _dataHideShowAppPac[0]['program']== "1",
        _dataHideShowAppPac[0]['jenis_REHAB']== "1",
        _dataHideShowAppPac[0]['no_REFERENCE']== "1",
        _dataMandatoryAppPac[0]['jenis_PEMBIAYAAN']== "1",
        _dataMandatoryAppPac[0]['kegiatan_USAHA']== "1",
        _dataMandatoryAppPac[0]['jenis_KEGIATAN_USAHA']== "1",
        _dataMandatoryAppPac[0]['grup_OBJEK']== "1",
        _dataMandatoryAppPac[0]['objek']== "1",
        _dataMandatoryAppPac[0]['jenis_PRODUK']== "1",
        _dataMandatoryAppPac[0]['merek_OBJEK']== "1",
        _dataMandatoryAppPac[0]['jenis_OBJEK']== "1",
        _dataMandatoryAppPac[0]['model_OBJEK']== "1",
        _dataMandatoryAppPac[0]['model_DETAIL']== "1",
        _dataMandatoryAppPac[0]['pemakaian_OBJECT']== "1",
        _dataMandatoryAppPac[0]['tujuan_PENGGUNAAN_OBJEK']== "1",
        _dataMandatoryAppPac[0]['grup_SALES']== "1",
        _dataMandatoryAppPac[0]['group_ID']== "1",
        _dataMandatoryAppPac[0]['sumber_ORDER']== "1",
        _dataMandatoryAppPac[0]['nama_SUMBER_ORDER']== "1",
        _dataMandatoryAppPac[0]['jenis_PIHAK_KETIGA']== "1",
        _dataMandatoryAppPac[0]['pihak_KETIGA']== "1",
        _dataMandatoryAppPac[0]['matriks_DEALER']== "1",
        _dataMandatoryAppPac[0]['kegiatan']== "1",
        _dataMandatoryAppPac[0]['sentra_D']== "1",
        _dataMandatoryAppPac[0]['unit_D']== "1",
        _dataMandatoryAppPac[0]['program']== "1",
        _dataMandatoryAppPac[0]['jenis_REHAB']== "1",
        _dataMandatoryAppPac[0]['no_REFERENCE']== "1",
      );
      _showMandatoryInfoObjectUnitPacCompanyModel = ShowMandatoryInfoObjectUnitModel(
        _dataHideShowAppPacCompany[0]['jenis_PEMBIAYAAN']== "1",
        _dataHideShowAppPacCompany[0]['kegiatan_USAHA']== "1",
        _dataHideShowAppPacCompany[0]['jenis_KEGIATAN_USAHA']== "1",
        _dataHideShowAppPacCompany[0]['grup_OBJEK']== "1",
        _dataHideShowAppPacCompany[0]['objek']== "1",
        _dataHideShowAppPacCompany[0]['jenis_PRODUK']== "1",
        _dataHideShowAppPacCompany[0]['merek_OBJEK']== "1",
        _dataHideShowAppPacCompany[0]['jenis_OBJEK']== "1",
        _dataHideShowAppPacCompany[0]['model_OBJEK']== "1",
        _dataHideShowAppPacCompany[0]['model_DETAIL']== "1",
        _dataHideShowAppPacCompany[0]['pemakaian_OBJECT']== "1",
        _dataHideShowAppPacCompany[0]['tujuan_PENGGUNAAN_OBJEK']== "1",
        _dataHideShowAppPacCompany[0]['grup_SALES']== "1",
        _dataHideShowAppPacCompany[0]['group_ID']== "1",
        _dataHideShowAppPacCompany[0]['sumber_ORDER']== "1",
        _dataHideShowAppPacCompany[0]['nama_SUMBER_ORDER']== "1",
        _dataHideShowAppPacCompany[0]['jenis_PIHAK_KETIGA']== "1",
        _dataHideShowAppPacCompany[0]['pihak_KETIGA']== "1",
        _dataHideShowAppPacCompany[0]['matriks_DEALER']== "1",
        _dataHideShowAppPacCompany[0]['kegiatan']== "1",
        _dataHideShowAppPacCompany[0]['sentra_D']== "1",
        _dataHideShowAppPacCompany[0]['unit_D']== "1",
        _dataHideShowAppPacCompany[0]['program']== "1",
        _dataHideShowAppPacCompany[0]['jenis_REHAB']== "1",
        _dataHideShowAppPacCompany[0]['no_REFERENCE']== "1",
        _dataMandatoryAppPacCompany[0]['jenis_PEMBIAYAAN']== "1",
        _dataMandatoryAppPacCompany[0]['kegiatan_USAHA']== "1",
        _dataMandatoryAppPacCompany[0]['jenis_KEGIATAN_USAHA']== "1",
        _dataMandatoryAppPacCompany[0]['grup_OBJEK']== "1",
        _dataMandatoryAppPacCompany[0]['objek']== "1",
        _dataMandatoryAppPacCompany[0]['jenis_PRODUK']== "1",
        _dataMandatoryAppPacCompany[0]['merek_OBJEK']== "1",
        _dataMandatoryAppPacCompany[0]['jenis_OBJEK']== "1",
        _dataMandatoryAppPacCompany[0]['model_OBJEK']== "1",
        _dataMandatoryAppPacCompany[0]['model_DETAIL']== "1",
        _dataMandatoryAppPacCompany[0]['pemakaian_OBJECT']== "1",
        _dataMandatoryAppPacCompany[0]['tujuan_PENGGUNAAN_OBJEK']== "1",
        _dataMandatoryAppPacCompany[0]['grup_SALES']== "1",
        _dataMandatoryAppPacCompany[0]['group_ID']== "1",
        _dataMandatoryAppPacCompany[0]['sumber_ORDER']== "1",
        _dataMandatoryAppPacCompany[0]['nama_SUMBER_ORDER']== "1",
        _dataMandatoryAppPacCompany[0]['jenis_PIHAK_KETIGA']== "1",
        _dataMandatoryAppPacCompany[0]['pihak_KETIGA']== "1",
        _dataMandatoryAppPacCompany[0]['matriks_DEALER']== "1",
        _dataMandatoryAppPacCompany[0]['kegiatan']== "1",
        _dataMandatoryAppPacCompany[0]['sentra_D']== "1",
        _dataMandatoryAppPacCompany[0]['unit_D']== "1",
        _dataMandatoryAppPacCompany[0]['program']== "1",
        _dataMandatoryAppPacCompany[0]['jenis_REHAB']== "1",
        _dataMandatoryAppPacCompany[0]['no_REFERENCE']== "1",
      );
      // Resurvey
      _showMandatoryInfoObjectUnitResurveyModel = ShowMandatoryInfoObjectUnitModel(
        _dataHideShowAppResurvey[0]['jenis_PEMBIAYAAN']== "1",
        _dataHideShowAppResurvey[0]['kegiatan_USAHA']== "1",
        _dataHideShowAppResurvey[0]['jenis_KEGIATAN_USAHA']== "1",
        _dataHideShowAppResurvey[0]['grup_OBJEK']== "1",
        _dataHideShowAppResurvey[0]['objek']== "1",
        _dataHideShowAppResurvey[0]['jenis_PRODUK']== "1",
        _dataHideShowAppResurvey[0]['merek_OBJEK']== "1",
        _dataHideShowAppResurvey[0]['jenis_OBJEK']== "1",
        _dataHideShowAppResurvey[0]['model_OBJEK']== "1",
        _dataHideShowAppResurvey[0]['model_DETAIL']== "1",
        _dataHideShowAppResurvey[0]['pemakaian_OBJECT']== "1",
        _dataHideShowAppResurvey[0]['tujuan_PENGGUNAAN_OBJEK']== "1",
        _dataHideShowAppResurvey[0]['grup_SALES']== "1",
        _dataHideShowAppResurvey[0]['group_ID']== "1",
        _dataHideShowAppResurvey[0]['sumber_ORDER']== "1",
        _dataHideShowAppResurvey[0]['nama_SUMBER_ORDER']== "1",
        _dataHideShowAppResurvey[0]['jenis_PIHAK_KETIGA']== "1",
        _dataHideShowAppResurvey[0]['pihak_KETIGA']== "1",
        _dataHideShowAppResurvey[0]['matriks_DEALER']== "1",
        _dataHideShowAppResurvey[0]['kegiatan']== "1",
        _dataHideShowAppResurvey[0]['sentra_D']== "1",
        _dataHideShowAppResurvey[0]['unit_D']== "1",
        _dataHideShowAppResurvey[0]['program']== "1",
        _dataHideShowAppResurvey[0]['jenis_REHAB']== "1",
        _dataHideShowAppResurvey[0]['no_REFERENCE']== "1",
        _dataMandatoryAppResurvey[0]['jenis_PEMBIAYAAN']== "1",
        _dataMandatoryAppResurvey[0]['kegiatan_USAHA']== "1",
        _dataMandatoryAppResurvey[0]['jenis_KEGIATAN_USAHA']== "1",
        _dataMandatoryAppResurvey[0]['grup_OBJEK']== "1",
        _dataMandatoryAppResurvey[0]['objek']== "1",
        _dataMandatoryAppResurvey[0]['jenis_PRODUK']== "1",
        _dataMandatoryAppResurvey[0]['merek_OBJEK']== "1",
        _dataMandatoryAppResurvey[0]['jenis_OBJEK']== "1",
        _dataMandatoryAppResurvey[0]['model_OBJEK']== "1",
        _dataMandatoryAppResurvey[0]['model_DETAIL']== "1",
        _dataMandatoryAppResurvey[0]['pemakaian_OBJECT']== "1",
        _dataMandatoryAppResurvey[0]['tujuan_PENGGUNAAN_OBJEK']== "1",
        _dataMandatoryAppResurvey[0]['grup_SALES']== "1",
        _dataMandatoryAppResurvey[0]['group_ID']== "1",
        _dataMandatoryAppResurvey[0]['sumber_ORDER']== "1",
        _dataMandatoryAppResurvey[0]['nama_SUMBER_ORDER']== "1",
        _dataMandatoryAppResurvey[0]['jenis_PIHAK_KETIGA']== "1",
        _dataMandatoryAppResurvey[0]['pihak_KETIGA']== "1",
        _dataMandatoryAppResurvey[0]['matriks_DEALER']== "1",
        _dataMandatoryAppResurvey[0]['kegiatan']== "1",
        _dataMandatoryAppResurvey[0]['sentra_D']== "1",
        _dataMandatoryAppResurvey[0]['unit_D']== "1",
        _dataMandatoryAppResurvey[0]['program']== "1",
        _dataMandatoryAppResurvey[0]['jenis_REHAB']== "1",
        _dataMandatoryAppResurvey[0]['no_REFERENCE']== "1",
      );
      _showMandatoryInfoObjectUnitResurveyCompanyModel = ShowMandatoryInfoObjectUnitModel(
        _dataHideShowAppResurveyCompany[0]['jenis_PEMBIAYAAN']== "1",
        _dataHideShowAppResurveyCompany[0]['kegiatan_USAHA']== "1",
        _dataHideShowAppResurveyCompany[0]['jenis_KEGIATAN_USAHA']== "1",
        _dataHideShowAppResurveyCompany[0]['grup_OBJEK']== "1",
        _dataHideShowAppResurveyCompany[0]['objek']== "1",
        _dataHideShowAppResurveyCompany[0]['jenis_PRODUK']== "1",
        _dataHideShowAppResurveyCompany[0]['merek_OBJEK']== "1",
        _dataHideShowAppResurveyCompany[0]['jenis_OBJEK']== "1",
        _dataHideShowAppResurveyCompany[0]['model_OBJEK']== "1",
        _dataHideShowAppResurveyCompany[0]['model_DETAIL']== "1",
        _dataHideShowAppResurveyCompany[0]['pemakaian_OBJECT']== "1",
        _dataHideShowAppResurveyCompany[0]['tujuan_PENGGUNAAN_OBJEK']== "1",
        _dataHideShowAppResurveyCompany[0]['grup_SALES']== "1",
        _dataHideShowAppResurveyCompany[0]['group_ID']== "1",
        _dataHideShowAppResurveyCompany[0]['sumber_ORDER']== "1",
        _dataHideShowAppResurveyCompany[0]['nama_SUMBER_ORDER']== "1",
        _dataHideShowAppResurveyCompany[0]['jenis_PIHAK_KETIGA']== "1",
        _dataHideShowAppResurveyCompany[0]['pihak_KETIGA']== "1",
        _dataHideShowAppResurveyCompany[0]['matriks_DEALER']== "1",
        _dataHideShowAppResurveyCompany[0]['kegiatan']== "1",
        _dataHideShowAppResurveyCompany[0]['sentra_D']== "1",
        _dataHideShowAppResurveyCompany[0]['unit_D']== "1",
        _dataHideShowAppResurveyCompany[0]['program']== "1",
        _dataHideShowAppResurveyCompany[0]['jenis_REHAB']== "1",
        _dataHideShowAppResurveyCompany[0]['no_REFERENCE']== "1",
        _dataMandatoryAppResurveyCompany[0]['jenis_PEMBIAYAAN']== "1",
        _dataMandatoryAppResurveyCompany[0]['kegiatan_USAHA']== "1",
        _dataMandatoryAppResurveyCompany[0]['jenis_KEGIATAN_USAHA']== "1",
        _dataMandatoryAppResurveyCompany[0]['grup_OBJEK']== "1",
        _dataMandatoryAppResurveyCompany[0]['objek']== "1",
        _dataMandatoryAppResurveyCompany[0]['jenis_PRODUK']== "1",
        _dataMandatoryAppResurveyCompany[0]['merek_OBJEK']== "1",
        _dataMandatoryAppResurveyCompany[0]['jenis_OBJEK']== "1",
        _dataMandatoryAppResurveyCompany[0]['model_OBJEK']== "1",
        _dataMandatoryAppResurveyCompany[0]['model_DETAIL']== "1",
        _dataMandatoryAppResurveyCompany[0]['pemakaian_OBJECT']== "1",
        _dataMandatoryAppResurveyCompany[0]['tujuan_PENGGUNAAN_OBJEK']== "1",
        _dataMandatoryAppResurveyCompany[0]['grup_SALES']== "1",
        _dataMandatoryAppResurveyCompany[0]['group_ID']== "1",
        _dataMandatoryAppResurveyCompany[0]['sumber_ORDER']== "1",
        _dataMandatoryAppResurveyCompany[0]['nama_SUMBER_ORDER']== "1",
        _dataMandatoryAppResurveyCompany[0]['jenis_PIHAK_KETIGA']== "1",
        _dataMandatoryAppResurveyCompany[0]['pihak_KETIGA']== "1",
        _dataMandatoryAppResurveyCompany[0]['matriks_DEALER']== "1",
        _dataMandatoryAppResurveyCompany[0]['kegiatan']== "1",
        _dataMandatoryAppResurveyCompany[0]['sentra_D']== "1",
        _dataMandatoryAppResurveyCompany[0]['unit_D']== "1",
        _dataMandatoryAppResurveyCompany[0]['program']== "1",
        _dataMandatoryAppResurveyCompany[0]['jenis_REHAB']== "1",
        _dataMandatoryAppResurveyCompany[0]['no_REFERENCE']== "1",
      );
      // Dakor
      _showMandatoryInfoObjectUnitDakorModel = ShowMandatoryInfoObjectUnitModel(
        _dataHideShowAppDakor[0]['jenis_PEMBIAYAAN']== "1",
        _dataHideShowAppDakor[0]['kegiatan_USAHA']== "1",
        _dataHideShowAppDakor[0]['jenis_KEGIATAN_USAHA']== "1",
        _dataHideShowAppDakor[0]['grup_OBJEK']== "1",
        _dataHideShowAppDakor[0]['objek']== "1",
        _dataHideShowAppDakor[0]['jenis_PRODUK']== "1",
        _dataHideShowAppDakor[0]['merek_OBJEK']== "1",
        _dataHideShowAppDakor[0]['jenis_OBJEK']== "1",
        _dataHideShowAppDakor[0]['model_OBJEK']== "1",
        _dataHideShowAppDakor[0]['model_DETAIL']== "1",
        _dataHideShowAppDakor[0]['pemakaian_OBJECT']== "1",
        _dataHideShowAppDakor[0]['tujuan_PENGGUNAAN_OBJEK']== "1",
        _dataHideShowAppDakor[0]['grup_SALES']== "1",
        _dataHideShowAppDakor[0]['group_ID']== "1",
        _dataHideShowAppDakor[0]['sumber_ORDER']== "1",
        _dataHideShowAppDakor[0]['nama_SUMBER_ORDER']== "1",
        _dataHideShowAppDakor[0]['jenis_PIHAK_KETIGA']== "1",
        _dataHideShowAppDakor[0]['pihak_KETIGA']== "1",
        _dataHideShowAppDakor[0]['matriks_DEALER']== "1",
        _dataHideShowAppDakor[0]['kegiatan']== "1",
        _dataHideShowAppDakor[0]['sentra_D']== "1",
        _dataHideShowAppDakor[0]['unit_D']== "1",
        _dataHideShowAppDakor[0]['program']== "1",
        _dataHideShowAppDakor[0]['jenis_REHAB']== "1",
        _dataHideShowAppDakor[0]['no_REFERENCE']== "1",
        _dataMandatoryAppDakor[0]['jenis_PEMBIAYAAN']== "1",
        _dataMandatoryAppDakor[0]['kegiatan_USAHA']== "1",
        _dataMandatoryAppDakor[0]['jenis_KEGIATAN_USAHA']== "1",
        _dataMandatoryAppDakor[0]['grup_OBJEK']== "1",
        _dataMandatoryAppDakor[0]['objek']== "1",
        _dataMandatoryAppDakor[0]['jenis_PRODUK']== "1",
        _dataMandatoryAppDakor[0]['merek_OBJEK']== "1",
        _dataMandatoryAppDakor[0]['jenis_OBJEK']== "1",
        _dataMandatoryAppDakor[0]['model_OBJEK']== "1",
        _dataMandatoryAppDakor[0]['model_DETAIL']== "1",
        _dataMandatoryAppDakor[0]['pemakaian_OBJECT']== "1",
        _dataMandatoryAppDakor[0]['tujuan_PENGGUNAAN_OBJEK']== "1",
        _dataMandatoryAppDakor[0]['grup_SALES']== "1",
        _dataMandatoryAppDakor[0]['group_ID']== "1",
        _dataMandatoryAppDakor[0]['sumber_ORDER']== "1",
        _dataMandatoryAppDakor[0]['nama_SUMBER_ORDER']== "1",
        _dataMandatoryAppDakor[0]['jenis_PIHAK_KETIGA']== "1",
        _dataMandatoryAppDakor[0]['pihak_KETIGA']== "1",
        _dataMandatoryAppDakor[0]['matriks_DEALER']== "1",
        _dataMandatoryAppDakor[0]['kegiatan']== "1",
        _dataMandatoryAppDakor[0]['sentra_D']== "1",
        _dataMandatoryAppDakor[0]['unit_D']== "1",
        _dataMandatoryAppDakor[0]['program']== "1",
        _dataMandatoryAppDakor[0]['jenis_REHAB']== "1",
        _dataMandatoryAppDakor[0]['no_REFERENCE']== "1",
      );
      _showMandatoryInfoObjectUnitDakorCompanyModel = ShowMandatoryInfoObjectUnitModel(
        _dataHideShowAppDakorCom[0]['jenis_PEMBIAYAAN']== "1",
        _dataHideShowAppDakorCom[0]['kegiatan_USAHA']== "1",
        _dataHideShowAppDakorCom[0]['jenis_KEGIATAN_USAHA']== "1",
        _dataHideShowAppDakorCom[0]['grup_OBJEK']== "1",
        _dataHideShowAppDakorCom[0]['objek']== "1",
        _dataHideShowAppDakorCom[0]['jenis_PRODUK']== "1",
        _dataHideShowAppDakorCom[0]['merek_OBJEK']== "1",
        _dataHideShowAppDakorCom[0]['jenis_OBJEK']== "1",
        _dataHideShowAppDakorCom[0]['model_OBJEK']== "1",
        _dataHideShowAppDakorCom[0]['model_DETAIL']== "1",
        _dataHideShowAppDakorCom[0]['pemakaian_OBJECT']== "1",
        _dataHideShowAppDakorCom[0]['tujuan_PENGGUNAAN_OBJEK']== "1",
        _dataHideShowAppDakorCom[0]['grup_SALES']== "1",
        _dataHideShowAppDakorCom[0]['group_ID']== "1",
        _dataHideShowAppDakorCom[0]['sumber_ORDER']== "1",
        _dataHideShowAppDakorCom[0]['nama_SUMBER_ORDER']== "1",
        _dataHideShowAppDakorCom[0]['jenis_PIHAK_KETIGA']== "1",
        _dataHideShowAppDakorCom[0]['pihak_KETIGA']== "1",
        _dataHideShowAppDakorCom[0]['matriks_DEALER']== "1",
        _dataHideShowAppDakorCom[0]['kegiatan']== "1",
        _dataHideShowAppDakorCom[0]['sentra_D']== "1",
        _dataHideShowAppDakorCom[0]['unit_D']== "1",
        _dataHideShowAppDakorCom[0]['program']== "1",
        _dataHideShowAppDakorCom[0]['jenis_REHAB']== "1",
        _dataHideShowAppDakorCom[0]['no_REFERENCE']== "1",
        _dataMandatoryAppDakorCom[0]['jenis_PEMBIAYAAN']== "1",
        _dataMandatoryAppDakorCom[0]['kegiatan_USAHA']== "1",
        _dataMandatoryAppDakorCom[0]['jenis_KEGIATAN_USAHA']== "1",
        _dataMandatoryAppDakorCom[0]['grup_OBJEK']== "1",
        _dataMandatoryAppDakorCom[0]['objek']== "1",
        _dataMandatoryAppDakorCom[0]['jenis_PRODUK']== "1",
        _dataMandatoryAppDakorCom[0]['merek_OBJEK']== "1",
        _dataMandatoryAppDakorCom[0]['jenis_OBJEK']== "1",
        _dataMandatoryAppDakorCom[0]['model_OBJEK']== "1",
        _dataMandatoryAppDakorCom[0]['model_DETAIL']== "1",
        _dataMandatoryAppDakorCom[0]['pemakaian_OBJECT']== "1",
        _dataMandatoryAppDakorCom[0]['tujuan_PENGGUNAAN_OBJEK']== "1",
        _dataMandatoryAppDakorCom[0]['grup_SALES']== "1",
        _dataMandatoryAppDakorCom[0]['group_ID']== "1",
        _dataMandatoryAppDakorCom[0]['sumber_ORDER']== "1",
        _dataMandatoryAppDakorCom[0]['nama_SUMBER_ORDER']== "1",
        _dataMandatoryAppDakorCom[0]['jenis_PIHAK_KETIGA']== "1",
        _dataMandatoryAppDakorCom[0]['pihak_KETIGA']== "1",
        _dataMandatoryAppDakorCom[0]['matriks_DEALER']== "1",
        _dataMandatoryAppDakorCom[0]['kegiatan']== "1",
        _dataMandatoryAppDakorCom[0]['sentra_D']== "1",
        _dataMandatoryAppDakorCom[0]['unit_D']== "1",
        _dataMandatoryAppDakorCom[0]['program']== "1",
        _dataMandatoryAppDakorCom[0]['jenis_REHAB']== "1",
        _dataMandatoryAppDakorCom[0]['no_REFERENCE']== "1",
      );
      // End Inf Object Unit

      // Inf Salesman
      // IDE
      showMandatoryInfoSalesmanIdeModel = ShowMandatoryInfoSalesmanModel(
        _dataHideShowAppIde[0]['jenis_SALES']== "1",
        _dataHideShowAppIde[0]['jabatan']== "1",
        _dataHideShowAppIde[0]['pegawai']== "1",
        _dataHideShowAppIde[0]['atasan']== "1",
        _dataMandatoryAppIde[0]['jenis_SALES']== "1",
        _dataMandatoryAppIde[0]['jabatan']== "1",
        _dataMandatoryAppIde[0]['pegawai']== "1",
        _dataMandatoryAppIde[0]['atasan']== "1",
      );
      _showMandatoryInfoSalesmanIdeCompanyModel = ShowMandatoryInfoSalesmanModel(
        _dataHideShowAppIdeCompany[0]['jenis_SALES']== "1",
        _dataHideShowAppIdeCompany[0]['jabatan']== "1",
        _dataHideShowAppIdeCompany[0]['pegawai']== "1",
        _dataHideShowAppIdeCompany[0]['atasan']== "1",
        _dataMandatoryAppIdeCompany[0]['jenis_SALES']== "1",
        _dataMandatoryAppIdeCompany[0]['jabatan']== "1",
        _dataMandatoryAppIdeCompany[0]['pegawai']== "1",
        _dataMandatoryAppIdeCompany[0]['atasan']== "1",
      );
      // Reguler Survey
      _showMandatoryInfoSalesmanRegulerSurveyModel = ShowMandatoryInfoSalesmanModel(
        _dataHideShowAppRegulerSurvey[0]['jenis_SALES']== "1",
        _dataHideShowAppRegulerSurvey[0]['jabatan']== "1",
        _dataHideShowAppRegulerSurvey[0]['pegawai']== "1",
        _dataHideShowAppRegulerSurvey[0]['atasan']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jenis_SALES']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jabatan']== "1",
        _dataMandatoryAppRegulerSurvey[0]['pegawai']== "1",
        _dataMandatoryAppRegulerSurvey[0]['atasan']== "1",
      );
      _showMandatoryInfoSalesmanRegulerSurveyCompanyModel = ShowMandatoryInfoSalesmanModel(
        _dataHideShowAppRegulerSurveyCompany[0]['jenis_SALES']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jabatan']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['pegawai']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['atasan']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jenis_SALES']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jabatan']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['pegawai']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['atasan']== "1",
      );
      // PAC
      _showMandatoryInfoSalesmanPacModel = ShowMandatoryInfoSalesmanModel(
        _dataHideShowAppPac[0]['jenis_SALES']== "1",
        _dataHideShowAppPac[0]['jabatan']== "1",
        _dataHideShowAppPac[0]['pegawai']== "1",
        _dataHideShowAppPac[0]['atasan']== "1",
        _dataMandatoryAppPac[0]['jenis_SALES']== "1",
        _dataMandatoryAppPac[0]['jabatan']== "1",
        _dataMandatoryAppPac[0]['pegawai']== "1",
        _dataMandatoryAppPac[0]['atasan']== "1",
      );
      _showMandatoryInfoSalesmanPacCompanyModel = ShowMandatoryInfoSalesmanModel(
        _dataHideShowAppPacCompany[0]['jenis_SALES']== "1",
        _dataHideShowAppPacCompany[0]['jabatan']== "1",
        _dataHideShowAppPacCompany[0]['pegawai']== "1",
        _dataHideShowAppPacCompany[0]['atasan']== "1",
        _dataMandatoryAppPacCompany[0]['jenis_SALES']== "1",
        _dataMandatoryAppPacCompany[0]['jabatan']== "1",
        _dataMandatoryAppPacCompany[0]['pegawai']== "1",
        _dataMandatoryAppPacCompany[0]['atasan']== "1",
      );
      // Resurvey
      _showMandatoryInfoSalesmanResurveyModel = ShowMandatoryInfoSalesmanModel(
        _dataHideShowAppResurvey[0]['jenis_SALES']== "1",
        _dataHideShowAppResurvey[0]['jabatan']== "1",
        _dataHideShowAppResurvey[0]['pegawai']== "1",
        _dataHideShowAppResurvey[0]['atasan']== "1",
        _dataMandatoryAppResurvey[0]['jenis_SALES']== "1",
        _dataMandatoryAppResurvey[0]['jabatan']== "1",
        _dataMandatoryAppResurvey[0]['pegawai']== "1",
        _dataMandatoryAppResurvey[0]['atasan']== "1",
      );
      _showMandatoryInfoSalesmanResurveyCompanyModel = ShowMandatoryInfoSalesmanModel(
        _dataHideShowAppResurveyCompany[0]['jenis_SALES']== "1",
        _dataHideShowAppResurveyCompany[0]['jabatan']== "1",
        _dataHideShowAppResurveyCompany[0]['pegawai']== "1",
        _dataHideShowAppResurveyCompany[0]['atasan']== "1",
        _dataMandatoryAppResurveyCompany[0]['jenis_SALES']== "1",
        _dataMandatoryAppResurveyCompany[0]['jabatan']== "1",
        _dataMandatoryAppResurveyCompany[0]['pegawai']== "1",
        _dataMandatoryAppResurveyCompany[0]['atasan']== "1",
      );
      // Dakor
      _showMandatoryInfoSalesmanDakorModel = ShowMandatoryInfoSalesmanModel(
        _dataHideShowAppDakor[0]['jenis_SALES']== "1",
        _dataHideShowAppDakor[0]['jabatan']== "1",
        _dataHideShowAppDakor[0]['pegawai']== "1",
        _dataHideShowAppDakor[0]['atasan']== "1",
        _dataMandatoryAppDakor[0]['jenis_SALES']== "1",
        _dataMandatoryAppDakor[0]['jabatan']== "1",
        _dataMandatoryAppDakor[0]['pegawai']== "1",
        _dataMandatoryAppDakor[0]['atasan']== "1",
      );
      _showMandatoryInfoSalesmanDakorCompanyModel = ShowMandatoryInfoSalesmanModel(
        _dataHideShowAppDakorCom[0]['jenis_SALES']== "1",
        _dataHideShowAppDakorCom[0]['jabatan']== "1",
        _dataHideShowAppDakorCom[0]['pegawai']== "1",
        _dataHideShowAppDakorCom[0]['atasan']== "1",
        _dataMandatoryAppDakorCom[0]['jenis_SALES']== "1",
        _dataMandatoryAppDakorCom[0]['jabatan']== "1",
        _dataMandatoryAppDakorCom[0]['pegawai']== "1",
        _dataMandatoryAppDakorCom[0]['atasan']== "1",
      );
      // End Inf Salesman

      // Inf Colla Oto
      // IDE
      _showMandatoryInfoCollaOtoIdeModel = ShowMandatoryInfoCollaOtoModel(
        _dataHideShowAppIde[0]['jenis_JAMINAN_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['nama_JAMINAN_PEMOHON_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['jenis_IDENTITAS_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['no_IDENTITAS_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['nama_PADA_JAMINAN_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['tanggal_LAHIR']== "1",
        _dataHideShowAppIde[0]['tempat_LAHIR_SESUAI_IDENTITAS']== "1",
        _dataHideShowAppIde[0]['tempat_LAHIR_SESUAI_IDENTITAS_LOV']== "1",
        _dataHideShowAppIde[0]['collateral_UNIT_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['grup_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['objek_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['jenis_PRODUK_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['merek_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['jenis_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['model_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['pemakaian_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['tahun_PEMBUATAN_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['tahun_REGISTRASI_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['plat_KUNING_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['built_UP_NON_ATPM_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['no_POLISI_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['no_RANGKA_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['no_MESIN_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['no_BPKP_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['grade_UNIT_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['fasilitas_UTJ_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['nama_BIDDER_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['harga_JUAL_SHOWROOM_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['perlengkapan_TAMBAHAN']== "1",
        _dataHideShowAppIde[0]['mp_ADIRA_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['harga_PASAR_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['rekondisi_FISIK_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['ph_MAKSIMAL_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['harga_TAKSASI_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['kesimpulan_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['tujuan_PENGGUNAAN_COLLA_OTO']== "1",
        _dataHideShowAppIde[0]['untuk_SEMUA_UNIT']== "1",
        _dataMandatoryAppIde[0]['jenis_JAMINAN_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['nama_JAMINAN_PEMOHON_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['jenis_IDENTITAS_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['no_IDENTITAS_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['nama_PADA_JAMINAN_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['tanggal_LAHIR']== "1",
        _dataMandatoryAppIde[0]['tempat_LAHIR_SESUAI_IDENTITAS']== "1",
        _dataMandatoryAppIde[0]['tempat_LAHIR_SESUAI_IDENTITAS_LOV']== "1",
        _dataMandatoryAppIde[0]['collateral_UNIT_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['grup_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['objek_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['jenis_PRODUK_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['merek_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['jenis_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['model_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['pemakaian_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['tahun_PEMBUATAN_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['tahun_REGISTRASI_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['plat_KUNING_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['built_UP_NON_ATPM_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['no_POLISI_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['no_RANGKA_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['no_MESIN_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['no_BPKP_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['grade_UNIT_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['fasilitas_UTJ_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['nama_BIDDER_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['harga_JUAL_SHOWROOM_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['perlengkapan_TAMBAHAN']== "1",
        _dataMandatoryAppIde[0]['mp_ADIRA_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['harga_PASAR_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['rekondisi_FISIK_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['ph_MAKSIMAL_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['harga_TAKSASI_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['kesimpulan_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['tujuan_PENGGUNAAN_COLLA_OTO']== "1",
        _dataMandatoryAppIde[0]['untuk_SEMUA_UNIT']== "1",
      );
      _showMandatoryInfoCollaOtoIdeCompanyModel = ShowMandatoryInfoCollaOtoModel(
        _dataHideShowAppIdeCompany[0]['jenis_JAMINAN_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['nama_JAMINAN_PEMOHON_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['jenis_IDENTITAS_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['no_IDENTITAS_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['nama_PADA_JAMINAN_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['tanggal_LAHIR']== "1",
        _dataHideShowAppIdeCompany[0]['tempat_LAHIR_SESUAI_IDENTITAS']== "1",
        _dataHideShowAppIdeCompany[0]['tempat_LAHIR_SESUAI_IDENTITAS_LOV']== "1",
        _dataHideShowAppIdeCompany[0]['collateral_UNIT_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['grup_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['objek_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['jenis_PRODUK_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['merek_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['jenis_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['model_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['pemakaian_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['tahun_PEMBUATAN_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['tahun_REGISTRASI_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['plat_KUNING_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['built_UP_NON_ATPM_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['no_POLISI_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['no_RANGKA_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['no_MESIN_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['no_BPKP_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['grade_UNIT_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['fasilitas_UTJ_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['nama_BIDDER_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['harga_JUAL_SHOWROOM_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['perlengkapan_TAMBAHAN']== "1",
        _dataHideShowAppIdeCompany[0]['mp_ADIRA_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['harga_PASAR_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['rekondisi_FISIK_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['ph_MAKSIMAL_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['harga_TAKSASI_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['kesimpulan_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['tujuan_PENGGUNAAN_COLLA_OTO']== "1",
        _dataHideShowAppIdeCompany[0]['untuk_SEMUA_UNIT']== "1",
        _dataMandatoryAppIdeCompany[0]['jenis_JAMINAN_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['nama_JAMINAN_PEMOHON_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['jenis_IDENTITAS_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['no_IDENTITAS_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['nama_PADA_JAMINAN_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['tanggal_LAHIR']== "1",
        _dataMandatoryAppIdeCompany[0]['tempat_LAHIR_SESUAI_IDENTITAS']== "1",
        _dataMandatoryAppIdeCompany[0]['tempat_LAHIR_SESUAI_IDENTITAS_LOV']== "1",
        _dataMandatoryAppIdeCompany[0]['collateral_UNIT_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['grup_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['objek_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['jenis_PRODUK_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['merek_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['jenis_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['model_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['pemakaian_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['tahun_PEMBUATAN_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['tahun_REGISTRASI_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['plat_KUNING_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['built_UP_NON_ATPM_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['no_POLISI_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['no_RANGKA_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['no_MESIN_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['no_BPKP_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['grade_UNIT_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['fasilitas_UTJ_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['nama_BIDDER_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['harga_JUAL_SHOWROOM_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['perlengkapan_TAMBAHAN']== "1",
        _dataMandatoryAppIdeCompany[0]['mp_ADIRA_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['harga_PASAR_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['rekondisi_FISIK_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['ph_MAKSIMAL_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['harga_TAKSASI_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['kesimpulan_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['tujuan_PENGGUNAAN_COLLA_OTO']== "1",
        _dataMandatoryAppIdeCompany[0]['untuk_SEMUA_UNIT']== "1",
      );
      // Reguler Survey
      _showMandatoryInfoCollaOtoRegulerSurveyModel = ShowMandatoryInfoCollaOtoModel(
        _dataHideShowAppRegulerSurvey[0]['jenis_JAMINAN_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['nama_JAMINAN_PEMOHON_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['jenis_IDENTITAS_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['no_IDENTITAS_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['nama_PADA_JAMINAN_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['tanggal_LAHIR']== "1",
        _dataHideShowAppRegulerSurvey[0]['tempat_LAHIR_SESUAI_IDENTITAS']== "1",
        _dataHideShowAppRegulerSurvey[0]['tempat_LAHIR_SESUAI_IDENTITAS_LOV']== "1",
        _dataHideShowAppRegulerSurvey[0]['collateral_UNIT_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['grup_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['objek_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['jenis_PRODUK_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['merek_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['jenis_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['model_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['pemakaian_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['tahun_PEMBUATAN_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['tahun_REGISTRASI_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['plat_KUNING_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['built_UP_NON_ATPM_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['no_POLISI_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['no_RANGKA_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['no_MESIN_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['no_BPKP_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['grade_UNIT_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['fasilitas_UTJ_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['nama_BIDDER_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['harga_JUAL_SHOWROOM_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['perlengkapan_TAMBAHAN']== "1",
        _dataHideShowAppRegulerSurvey[0]['mp_ADIRA_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['harga_PASAR_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['rekondisi_FISIK_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['ph_MAKSIMAL_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['harga_TAKSASI_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['kesimpulan_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['tujuan_PENGGUNAAN_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurvey[0]['untuk_SEMUA_UNIT']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jenis_JAMINAN_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['nama_JAMINAN_PEMOHON_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jenis_IDENTITAS_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['no_IDENTITAS_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['nama_PADA_JAMINAN_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['tanggal_LAHIR']== "1",
        _dataMandatoryAppRegulerSurvey[0]['tempat_LAHIR_SESUAI_IDENTITAS']== "1",
        _dataMandatoryAppRegulerSurvey[0]['tempat_LAHIR_SESUAI_IDENTITAS_LOV']== "1",
        _dataMandatoryAppRegulerSurvey[0]['collateral_UNIT_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['grup_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['objek_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jenis_PRODUK_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['merek_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jenis_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['model_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['pemakaian_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['tahun_PEMBUATAN_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['tahun_REGISTRASI_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['plat_KUNING_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['built_UP_NON_ATPM_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['no_POLISI_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['no_RANGKA_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['no_MESIN_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['no_BPKP_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['grade_UNIT_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['fasilitas_UTJ_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['nama_BIDDER_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['harga_JUAL_SHOWROOM_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['perlengkapan_TAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurvey[0]['mp_ADIRA_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['harga_PASAR_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['rekondisi_FISIK_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['ph_MAKSIMAL_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['harga_TAKSASI_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['kesimpulan_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['tujuan_PENGGUNAAN_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurvey[0]['untuk_SEMUA_UNIT']== "1",
      );
      _showMandatoryInfoCollaOtoRegulerSurveyCompanyModel = ShowMandatoryInfoCollaOtoModel(
        _dataHideShowAppRegulerSurveyCompany[0]['jenis_JAMINAN_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['nama_JAMINAN_PEMOHON_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jenis_IDENTITAS_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['no_IDENTITAS_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['nama_PADA_JAMINAN_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['tanggal_LAHIR']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['tempat_LAHIR_SESUAI_IDENTITAS']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['tempat_LAHIR_SESUAI_IDENTITAS_LOV']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['collateral_UNIT_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['grup_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['objek_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jenis_PRODUK_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['merek_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jenis_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['model_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['pemakaian_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['tahun_PEMBUATAN_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['tahun_REGISTRASI_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['plat_KUNING_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['built_UP_NON_ATPM_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['no_POLISI_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['no_RANGKA_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['no_MESIN_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['no_BPKP_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['grade_UNIT_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['fasilitas_UTJ_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['nama_BIDDER_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['harga_JUAL_SHOWROOM_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['perlengkapan_TAMBAHAN']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['mp_ADIRA_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['harga_PASAR_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['rekondisi_FISIK_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['ph_MAKSIMAL_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['harga_TAKSASI_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['kesimpulan_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['tujuan_PENGGUNAAN_COLLA_OTO']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['untuk_SEMUA_UNIT']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jenis_JAMINAN_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['nama_JAMINAN_PEMOHON_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jenis_IDENTITAS_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['no_IDENTITAS_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['nama_PADA_JAMINAN_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['tanggal_LAHIR']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['tempat_LAHIR_SESUAI_IDENTITAS']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['tempat_LAHIR_SESUAI_IDENTITAS_LOV']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['collateral_UNIT_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['grup_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['objek_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jenis_PRODUK_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['merek_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jenis_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['model_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['pemakaian_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['tahun_PEMBUATAN_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['tahun_REGISTRASI_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['plat_KUNING_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['built_UP_NON_ATPM_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['no_POLISI_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['no_RANGKA_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['no_MESIN_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['no_BPKP_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['grade_UNIT_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['fasilitas_UTJ_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['nama_BIDDER_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['harga_JUAL_SHOWROOM_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['perlengkapan_TAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['mp_ADIRA_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['harga_PASAR_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['rekondisi_FISIK_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['ph_MAKSIMAL_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['harga_TAKSASI_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['kesimpulan_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['tujuan_PENGGUNAAN_COLLA_OTO']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['untuk_SEMUA_UNIT']== "1",
      );
      // PAC
      _showMandatoryInfoCollaOtoPacModel = ShowMandatoryInfoCollaOtoModel(
        _dataHideShowAppPac[0]['jenis_JAMINAN_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['nama_JAMINAN_PEMOHON_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['jenis_IDENTITAS_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['no_IDENTITAS_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['nama_PADA_JAMINAN_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['tanggal_LAHIR']== "1",
        _dataHideShowAppPac[0]['tempat_LAHIR_SESUAI_IDENTITAS']== "1",
        _dataHideShowAppPac[0]['tempat_LAHIR_SESUAI_IDENTITAS_LOV']== "1",
        _dataHideShowAppPac[0]['collateral_UNIT_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['grup_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['objek_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['jenis_PRODUK_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['merek_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['jenis_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['model_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['pemakaian_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['tahun_PEMBUATAN_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['tahun_REGISTRASI_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['plat_KUNING_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['built_UP_NON_ATPM_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['no_POLISI_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['no_RANGKA_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['no_MESIN_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['no_BPKP_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['grade_UNIT_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['fasilitas_UTJ_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['nama_BIDDER_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['harga_JUAL_SHOWROOM_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['perlengkapan_TAMBAHAN']== "1",
        _dataHideShowAppPac[0]['mp_ADIRA_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['harga_PASAR_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['rekondisi_FISIK_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['ph_MAKSIMAL_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['harga_TAKSASI_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['kesimpulan_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['tujuan_PENGGUNAAN_COLLA_OTO']== "1",
        _dataHideShowAppPac[0]['untuk_SEMUA_UNIT']== "1",
        _dataMandatoryAppPac[0]['jenis_JAMINAN_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['nama_JAMINAN_PEMOHON_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['jenis_IDENTITAS_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['no_IDENTITAS_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['nama_PADA_JAMINAN_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['tanggal_LAHIR']== "1",
        _dataMandatoryAppPac[0]['tempat_LAHIR_SESUAI_IDENTITAS']== "1",
        _dataMandatoryAppPac[0]['tempat_LAHIR_SESUAI_IDENTITAS_LOV']== "1",
        _dataMandatoryAppPac[0]['collateral_UNIT_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['grup_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['objek_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['jenis_PRODUK_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['merek_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['jenis_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['model_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['pemakaian_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['tahun_PEMBUATAN_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['tahun_REGISTRASI_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['plat_KUNING_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['built_UP_NON_ATPM_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['no_POLISI_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['no_RANGKA_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['no_MESIN_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['no_BPKP_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['grade_UNIT_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['fasilitas_UTJ_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['nama_BIDDER_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['harga_JUAL_SHOWROOM_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['perlengkapan_TAMBAHAN']== "1",
        _dataMandatoryAppPac[0]['mp_ADIRA_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['harga_PASAR_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['rekondisi_FISIK_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['ph_MAKSIMAL_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['harga_TAKSASI_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['kesimpulan_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['tujuan_PENGGUNAAN_COLLA_OTO']== "1",
        _dataMandatoryAppPac[0]['untuk_SEMUA_UNIT']== "1",
      );
      _showMandatoryInfoCollaOtoPacCompanyModel = ShowMandatoryInfoCollaOtoModel(
        _dataHideShowAppPacCompany[0]['jenis_JAMINAN_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['nama_JAMINAN_PEMOHON_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['jenis_IDENTITAS_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['no_IDENTITAS_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['nama_PADA_JAMINAN_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['tanggal_LAHIR']== "1",
        _dataHideShowAppPacCompany[0]['tempat_LAHIR_SESUAI_IDENTITAS']== "1",
        _dataHideShowAppPacCompany[0]['tempat_LAHIR_SESUAI_IDENTITAS_LOV']== "1",
        _dataHideShowAppPacCompany[0]['collateral_UNIT_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['grup_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['objek_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['jenis_PRODUK_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['merek_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['jenis_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['model_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['pemakaian_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['tahun_PEMBUATAN_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['tahun_REGISTRASI_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['plat_KUNING_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['built_UP_NON_ATPM_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['no_POLISI_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['no_RANGKA_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['no_MESIN_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['no_BPKP_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['grade_UNIT_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['fasilitas_UTJ_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['nama_BIDDER_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['harga_JUAL_SHOWROOM_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['perlengkapan_TAMBAHAN']== "1",
        _dataHideShowAppPacCompany[0]['mp_ADIRA_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['harga_PASAR_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['rekondisi_FISIK_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['ph_MAKSIMAL_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['harga_TAKSASI_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['kesimpulan_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['tujuan_PENGGUNAAN_COLLA_OTO']== "1",
        _dataHideShowAppPacCompany[0]['untuk_SEMUA_UNIT']== "1",
        _dataMandatoryAppPacCompany[0]['jenis_JAMINAN_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['nama_JAMINAN_PEMOHON_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['jenis_IDENTITAS_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['no_IDENTITAS_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['nama_PADA_JAMINAN_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['tanggal_LAHIR']== "1",
        _dataMandatoryAppPacCompany[0]['tempat_LAHIR_SESUAI_IDENTITAS']== "1",
        _dataMandatoryAppPacCompany[0]['tempat_LAHIR_SESUAI_IDENTITAS_LOV']== "1",
        _dataMandatoryAppPacCompany[0]['collateral_UNIT_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['grup_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['objek_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['jenis_PRODUK_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['merek_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['jenis_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['model_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['pemakaian_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['tahun_PEMBUATAN_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['tahun_REGISTRASI_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['plat_KUNING_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['built_UP_NON_ATPM_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['no_POLISI_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['no_RANGKA_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['no_MESIN_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['no_BPKP_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['grade_UNIT_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['fasilitas_UTJ_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['nama_BIDDER_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['harga_JUAL_SHOWROOM_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['perlengkapan_TAMBAHAN']== "1",
        _dataMandatoryAppPacCompany[0]['mp_ADIRA_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['harga_PASAR_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['rekondisi_FISIK_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['ph_MAKSIMAL_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['harga_TAKSASI_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['kesimpulan_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['tujuan_PENGGUNAAN_COLLA_OTO']== "1",
        _dataMandatoryAppPacCompany[0]['untuk_SEMUA_UNIT']== "1",
      );
      // Resurvey
      _showMandatoryInfoCollaOtoResurveyModel = ShowMandatoryInfoCollaOtoModel(
        _dataHideShowAppResurvey[0]['jenis_JAMINAN_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['nama_JAMINAN_PEMOHON_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['jenis_IDENTITAS_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['no_IDENTITAS_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['nama_PADA_JAMINAN_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['tanggal_LAHIR']== "1",
        _dataHideShowAppResurvey[0]['tempat_LAHIR_SESUAI_IDENTITAS']== "1",
        _dataHideShowAppResurvey[0]['tempat_LAHIR_SESUAI_IDENTITAS_LOV']== "1",
        _dataHideShowAppResurvey[0]['collateral_UNIT_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['grup_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['objek_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['jenis_PRODUK_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['merek_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['jenis_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['model_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['pemakaian_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['tahun_PEMBUATAN_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['tahun_REGISTRASI_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['plat_KUNING_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['built_UP_NON_ATPM_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['no_POLISI_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['no_RANGKA_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['no_MESIN_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['no_BPKP_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['grade_UNIT_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['fasilitas_UTJ_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['nama_BIDDER_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['harga_JUAL_SHOWROOM_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['perlengkapan_TAMBAHAN']== "1",
        _dataHideShowAppResurvey[0]['mp_ADIRA_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['harga_PASAR_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['rekondisi_FISIK_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['ph_MAKSIMAL_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['harga_TAKSASI_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['kesimpulan_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['tujuan_PENGGUNAAN_COLLA_OTO']== "1",
        _dataHideShowAppResurvey[0]['untuk_SEMUA_UNIT']== "1",
        _dataMandatoryAppResurvey[0]['jenis_JAMINAN_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['nama_JAMINAN_PEMOHON_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['jenis_IDENTITAS_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['no_IDENTITAS_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['nama_PADA_JAMINAN_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['tanggal_LAHIR']== "1",
        _dataMandatoryAppResurvey[0]['tempat_LAHIR_SESUAI_IDENTITAS']== "1",
        _dataMandatoryAppResurvey[0]['tempat_LAHIR_SESUAI_IDENTITAS_LOV']== "1",
        _dataMandatoryAppResurvey[0]['collateral_UNIT_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['grup_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['objek_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['jenis_PRODUK_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['merek_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['jenis_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['model_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['pemakaian_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['tahun_PEMBUATAN_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['tahun_REGISTRASI_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['plat_KUNING_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['built_UP_NON_ATPM_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['no_POLISI_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['no_RANGKA_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['no_MESIN_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['no_BPKP_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['grade_UNIT_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['fasilitas_UTJ_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['nama_BIDDER_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['harga_JUAL_SHOWROOM_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['perlengkapan_TAMBAHAN']== "1",
        _dataMandatoryAppResurvey[0]['mp_ADIRA_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['harga_PASAR_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['rekondisi_FISIK_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['ph_MAKSIMAL_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['harga_TAKSASI_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['kesimpulan_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['tujuan_PENGGUNAAN_COLLA_OTO']== "1",
        _dataMandatoryAppResurvey[0]['untuk_SEMUA_UNIT']== "1",
      );
      _showMandatoryInfoCollaOtoResurveyCompanyModel = ShowMandatoryInfoCollaOtoModel(
        _dataHideShowAppResurveyCompany[0]['jenis_JAMINAN_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['nama_JAMINAN_PEMOHON_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['jenis_IDENTITAS_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['no_IDENTITAS_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['nama_PADA_JAMINAN_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['tanggal_LAHIR']== "1",
        _dataHideShowAppResurveyCompany[0]['tempat_LAHIR_SESUAI_IDENTITAS']== "1",
        _dataHideShowAppResurveyCompany[0]['tempat_LAHIR_SESUAI_IDENTITAS_LOV']== "1",
        _dataHideShowAppResurveyCompany[0]['collateral_UNIT_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['grup_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['objek_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['jenis_PRODUK_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['merek_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['jenis_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['model_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['pemakaian_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['tahun_PEMBUATAN_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['tahun_REGISTRASI_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['plat_KUNING_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['built_UP_NON_ATPM_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['no_POLISI_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['no_RANGKA_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['no_MESIN_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['no_BPKP_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['grade_UNIT_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['fasilitas_UTJ_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['nama_BIDDER_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['harga_JUAL_SHOWROOM_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['perlengkapan_TAMBAHAN']== "1",
        _dataHideShowAppResurveyCompany[0]['mp_ADIRA_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['harga_PASAR_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['rekondisi_FISIK_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['ph_MAKSIMAL_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['harga_TAKSASI_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['kesimpulan_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['tujuan_PENGGUNAAN_COLLA_OTO']== "1",
        _dataHideShowAppResurveyCompany[0]['untuk_SEMUA_UNIT']== "1",
        _dataMandatoryAppResurveyCompany[0]['jenis_JAMINAN_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['nama_JAMINAN_PEMOHON_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['jenis_IDENTITAS_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['no_IDENTITAS_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['nama_PADA_JAMINAN_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['tanggal_LAHIR']== "1",
        _dataMandatoryAppResurveyCompany[0]['tempat_LAHIR_SESUAI_IDENTITAS']== "1",
        _dataMandatoryAppResurveyCompany[0]['tempat_LAHIR_SESUAI_IDENTITAS_LOV']== "1",
        _dataMandatoryAppResurveyCompany[0]['collateral_UNIT_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['grup_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['objek_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['jenis_PRODUK_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['merek_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['jenis_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['model_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['pemakaian_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['tahun_PEMBUATAN_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['tahun_REGISTRASI_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['plat_KUNING_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['built_UP_NON_ATPM_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['no_POLISI_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['no_RANGKA_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['no_MESIN_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['no_BPKP_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['grade_UNIT_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['fasilitas_UTJ_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['nama_BIDDER_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['harga_JUAL_SHOWROOM_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['perlengkapan_TAMBAHAN']== "1",
        _dataMandatoryAppResurveyCompany[0]['mp_ADIRA_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['harga_PASAR_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['rekondisi_FISIK_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['ph_MAKSIMAL_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['harga_TAKSASI_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['kesimpulan_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['tujuan_PENGGUNAAN_COLLA_OTO']== "1",
        _dataMandatoryAppResurveyCompany[0]['untuk_SEMUA_UNIT']== "1",
      );
      // Dakor
      _showMandatoryInfoCollaOtoDakorModel = ShowMandatoryInfoCollaOtoModel(
        _dataHideShowAppDakor[0]['jenis_JAMINAN_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['nama_JAMINAN_PEMOHON_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['jenis_IDENTITAS_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['no_IDENTITAS_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['nama_PADA_JAMINAN_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['tanggal_LAHIR']== "1",
        _dataHideShowAppDakor[0]['tempat_LAHIR_SESUAI_IDENTITAS']== "1",
        _dataHideShowAppDakor[0]['tempat_LAHIR_SESUAI_IDENTITAS_LOV']== "1",
        _dataHideShowAppDakor[0]['collateral_UNIT_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['grup_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['objek_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['jenis_PRODUK_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['merek_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['jenis_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['model_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['pemakaian_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['tahun_PEMBUATAN_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['tahun_REGISTRASI_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['plat_KUNING_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['built_UP_NON_ATPM_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['no_POLISI_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['no_RANGKA_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['no_MESIN_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['no_BPKP_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['grade_UNIT_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['fasilitas_UTJ_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['nama_BIDDER_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['harga_JUAL_SHOWROOM_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['perlengkapan_TAMBAHAN']== "1",
        _dataHideShowAppDakor[0]['mp_ADIRA_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['harga_PASAR_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['rekondisi_FISIK_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['ph_MAKSIMAL_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['harga_TAKSASI_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['kesimpulan_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['tujuan_PENGGUNAAN_COLLA_OTO']== "1",
        _dataHideShowAppDakor[0]['untuk_SEMUA_UNIT']== "1",
        _dataMandatoryAppDakor[0]['jenis_JAMINAN_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['nama_JAMINAN_PEMOHON_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['jenis_IDENTITAS_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['no_IDENTITAS_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['nama_PADA_JAMINAN_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['tanggal_LAHIR']== "1",
        _dataMandatoryAppDakor[0]['tempat_LAHIR_SESUAI_IDENTITAS']== "1",
        _dataMandatoryAppDakor[0]['tempat_LAHIR_SESUAI_IDENTITAS_LOV']== "1",
        _dataMandatoryAppDakor[0]['collateral_UNIT_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['grup_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['objek_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['jenis_PRODUK_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['merek_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['jenis_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['model_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['pemakaian_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['tahun_PEMBUATAN_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['tahun_REGISTRASI_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['plat_KUNING_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['built_UP_NON_ATPM_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['no_POLISI_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['no_RANGKA_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['no_MESIN_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['no_BPKP_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['grade_UNIT_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['fasilitas_UTJ_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['nama_BIDDER_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['harga_JUAL_SHOWROOM_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['perlengkapan_TAMBAHAN']== "1",
        _dataMandatoryAppDakor[0]['mp_ADIRA_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['harga_PASAR_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['rekondisi_FISIK_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['ph_MAKSIMAL_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['harga_TAKSASI_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['kesimpulan_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['tujuan_PENGGUNAAN_COLLA_OTO']== "1",
        _dataMandatoryAppDakor[0]['untuk_SEMUA_UNIT']== "1",
      );
      _showMandatoryInfoCollaOtoDakorCompanyModel = ShowMandatoryInfoCollaOtoModel(
        _dataHideShowAppDakorCom[0]['jenis_JAMINAN_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['nama_JAMINAN_PEMOHON_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['jenis_IDENTITAS_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['no_IDENTITAS_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['nama_PADA_JAMINAN_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['tanggal_LAHIR']== "1",
        _dataHideShowAppDakorCom[0]['tempat_LAHIR_SESUAI_IDENTITAS']== "1",
        _dataHideShowAppDakorCom[0]['tempat_LAHIR_SESUAI_IDENTITAS_LOV']== "1",
        _dataHideShowAppDakorCom[0]['collateral_UNIT_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['grup_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['objek_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['jenis_PRODUK_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['merek_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['jenis_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['model_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['pemakaian_OBJEK_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['tahun_PEMBUATAN_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['tahun_REGISTRASI_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['plat_KUNING_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['built_UP_NON_ATPM_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['no_POLISI_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['no_RANGKA_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['no_MESIN_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['no_BPKP_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['grade_UNIT_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['fasilitas_UTJ_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['nama_BIDDER_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['harga_JUAL_SHOWROOM_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['perlengkapan_TAMBAHAN']== "1",
        _dataHideShowAppDakorCom[0]['mp_ADIRA_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['harga_PASAR_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['rekondisi_FISIK_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['ph_MAKSIMAL_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['harga_TAKSASI_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['kesimpulan_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['tujuan_PENGGUNAAN_COLLA_OTO']== "1",
        _dataHideShowAppDakorCom[0]['untuk_SEMUA_UNIT']== "1",
        _dataMandatoryAppDakorCom[0]['jenis_JAMINAN_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['nama_JAMINAN_PEMOHON_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['jenis_IDENTITAS_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['no_IDENTITAS_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['nama_PADA_JAMINAN_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['tanggal_LAHIR']== "1",
        _dataMandatoryAppDakorCom[0]['tempat_LAHIR_SESUAI_IDENTITAS']== "1",
        _dataMandatoryAppDakorCom[0]['tempat_LAHIR_SESUAI_IDENTITAS_LOV']== "1",
        _dataMandatoryAppDakorCom[0]['collateral_UNIT_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['grup_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['objek_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['jenis_PRODUK_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['merek_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['jenis_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['model_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['pemakaian_OBJEK_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['tahun_PEMBUATAN_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['tahun_REGISTRASI_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['plat_KUNING_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['built_UP_NON_ATPM_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['no_POLISI_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['no_RANGKA_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['no_MESIN_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['no_BPKP_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['grade_UNIT_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['fasilitas_UTJ_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['nama_BIDDER_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['harga_JUAL_SHOWROOM_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['perlengkapan_TAMBAHAN']== "1",
        _dataMandatoryAppDakorCom[0]['mp_ADIRA_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['harga_PASAR_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['rekondisi_FISIK_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['ph_MAKSIMAL_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['harga_TAKSASI_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['kesimpulan_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['tujuan_PENGGUNAAN_COLLA_OTO']== "1",
        _dataMandatoryAppDakorCom[0]['untuk_SEMUA_UNIT']== "1",
      );
      // End Inf Colla Oto

      // Inf Colla Prop
      // IDE
      _showMandatoryInfoCollaPropIdeModel = ShowMandatoryInfoCollaPropModel(
        _dataHideShowAppIde[0]['nm_JAMIN_PMOHON_COLLA_PROPERTI']== "1",
        _dataHideShowAppIde[0]['jenis_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataHideShowAppIde[0]['no_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataHideShowAppIde[0]['nmjaminan_COLLA_PROPERTI']== "1",
        _dataHideShowAppIde[0]['tanggal_LAHIR_COLLA_PROPERTI']== "1",
        _dataHideShowAppIde[0]['tmpt_LAHIR_IDEN_COLLA_PROPERTI']== "1",
        _dataHideShowAppIde[0]['tptlahir_IDENLOV_COLLAPROPERTI']== "1",
        _dataHideShowAppIde[0]['no_SERTIFIKAT_COLLA_PROPERTI']== "1",
        _dataHideShowAppIde[0]['jenis_SERTIFIKAT_COLLAPROPERTI']== "1",
        _dataHideShowAppIde[0]['jenis_PROPERTI_COLLA_PROPERTI']== "1",
        _dataHideShowAppIde[0]['luas_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppIde[0]['luas_TANAH_COLLA_PROPERTI']== "1",
        _dataHideShowAppIde[0]['harga_TAKSASI_COLLA_PROPERTI']== "1",
        _dataHideShowAppIde[0]['sifat_JAMINAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppIde[0]['bktpemilikan_JAMIN_COLPROPERTI']== "1",
        _dataHideShowAppIde[0]['tglpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataHideShowAppIde[0]['thnpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataHideShowAppIde[0]['namapemegang_HAK_COLLAPROPERTI']== "1",
        _dataHideShowAppIde[0]['no_SURAT_UKUR_COLLA_PROPERTI']== "1",
        _dataHideShowAppIde[0]['tgl_SURAT_UKUR_COLLAPROPERTI']== "1",
        _dataHideShowAppIde[0]['sertifikat_SURATUKUR_COLLAPROP']== "1",
        _dataHideShowAppIde[0]['dp_JAMINAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppIde[0]['ph_MAKSIMAL_COLLA_PROPERTI']== "1",
        _dataHideShowAppIde[0]['jarakfasum_POSITIF_COLLAPROP']== "1",
        _dataHideShowAppIde[0]['jarakfasum_NEGATIF_COLLAPROP']== "1",
        _dataHideShowAppIde[0]['harga_TANAH_COLLA_PROPERTI']== "1",
        _dataHideShowAppIde[0]['harga_NJOP_COLLA_PROPERTI']== "1",
        _dataHideShowAppIde[0]['harga_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppIde[0]['jenis_ATAP_COLLA_PROPERTI']== "1",
        _dataHideShowAppIde[0]['jenis_DINDING_COLLA_PROPERTI']== "1",
        _dataHideShowAppIde[0]['jenis_LANTAI_COLLA_PROPERTI']== "1",
        _dataHideShowAppIde[0]['jenis_FONDASI_COLLA_PROPERTI']== "1",
        _dataHideShowAppIde[0]['tipe_JALAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppIde[0]['dilewati_MOBIL_COLLA_PROPERTI']== "1",
        _dataHideShowAppIde[0]['jml_RUMAH_DLM_RADIUS_COLLAPROP']== "1",
        _dataHideShowAppIde[0]['masa_BERLAKU_HAK_COLLAPROPERTI']== "1",
        _dataHideShowAppIde[0]['no_IMB_COLLA_PROPERTI']== "1",
        _dataHideShowAppIde[0]['tanggal_IMB_COLLA_PROPERTI']== "1",
        _dataHideShowAppIde[0]['luasbangunan_IMB_COLLAPROPERTI']== "1",
        _dataHideShowAppIde[0]['tuj_PENGGUNAAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppIde[0]['ltv_COLLA_PROPERTI']== "1",
        _dataHideShowAppIde[0]['semua_UNIT_COLLA_PROPERTI']== "1",
        _dataHideShowAppIde[0]['jns_ALAMAT_COLLAPROPERTIALAMAT']== "1",
        _dataHideShowAppIde[0]['alamat_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppIde[0]['rt_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppIde[0]['rw_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppIde[0]['provinsi_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppIde[0]['kab_KOTA_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppIde[0]['kec_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppIde[0]['kel_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppIde[0]['kode_POS_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppIde[0]['geolocation_alamat_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppIde[0]['nm_JAMIN_PMOHON_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIde[0]['jenis_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIde[0]['no_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIde[0]['nmjaminan_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIde[0]['tanggal_LAHIR_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIde[0]['tmpt_LAHIR_IDEN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIde[0]['tptlahir_IDENLOV_COLLAPROPERTI']== "1",
        _dataMandatoryAppIde[0]['no_SERTIFIKAT_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIde[0]['jenis_SERTIFIKAT_COLLAPROPERTI']== "1",
        _dataMandatoryAppIde[0]['jenis_PROPERTI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIde[0]['luas_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIde[0]['luas_TANAH_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIde[0]['harga_TAKSASI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIde[0]['sifat_JAMINAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIde[0]['bktpemilikan_JAMIN_COLPROPERTI']== "1",
        _dataMandatoryAppIde[0]['tglpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataMandatoryAppIde[0]['thnpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataMandatoryAppIde[0]['namapemegang_HAK_COLLAPROPERTI']== "1",
        _dataMandatoryAppIde[0]['no_SURAT_UKUR_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIde[0]['tgl_SURAT_UKUR_COLLAPROPERTI']== "1",
        _dataMandatoryAppIde[0]['sertifikat_SURATUKUR_COLLAPROP']== "1",
        _dataMandatoryAppIde[0]['dp_JAMINAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIde[0]['ph_MAKSIMAL_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIde[0]['jarakfasum_POSITIF_COLLAPROP']== "1",
        _dataMandatoryAppIde[0]['jarakfasum_NEGATIF_COLLAPROP']== "1",
        _dataMandatoryAppIde[0]['harga_TANAH_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIde[0]['harga_NJOP_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIde[0]['harga_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIde[0]['jenis_ATAP_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIde[0]['jenis_DINDING_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIde[0]['jenis_LANTAI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIde[0]['jenis_FONDASI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIde[0]['tipe_JALAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIde[0]['dilewati_MOBIL_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIde[0]['jml_RUMAH_DLM_RADIUS_COLLAPROP']== "1",
        _dataMandatoryAppIde[0]['masa_BERLAKU_HAK_COLLAPROPERTI']== "1",
        _dataMandatoryAppIde[0]['no_IMB_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIde[0]['tanggal_IMB_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIde[0]['luasbangunan_IMB_COLLAPROPERTI']== "1",
        _dataMandatoryAppIde[0]['tuj_PENGGUNAAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIde[0]['ltv_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIde[0]['semua_UNIT_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIde[0]['jns_ALAMAT_COLLAPROPERTIALAMAT']== "1",
        _dataMandatoryAppIde[0]['alamat_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppIde[0]['rt_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppIde[0]['rw_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppIde[0]['provinsi_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppIde[0]['kab_KOTA_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppIde[0]['kec_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppIde[0]['kel_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppIde[0]['kode_POS_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppIde[0]['geolocation_alamat_COLLA_PROPERTI_ALAMAT']== "1",
      );
      _showMandatoryInfoCollaPropIdeCompanyModel = ShowMandatoryInfoCollaPropModel(
        _dataHideShowAppIdeCompany[0]['nm_JAMIN_PMOHON_COLLA_PROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['jenis_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['no_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['nmjaminan_COLLA_PROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['tanggal_LAHIR_COLLA_PROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['tmpt_LAHIR_IDEN_COLLA_PROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['tptlahir_IDENLOV_COLLAPROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['no_SERTIFIKAT_COLLA_PROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['jenis_SERTIFIKAT_COLLAPROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['jenis_PROPERTI_COLLA_PROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['luas_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['luas_TANAH_COLLA_PROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['harga_TAKSASI_COLLA_PROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['sifat_JAMINAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['bktpemilikan_JAMIN_COLPROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['tglpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataHideShowAppIdeCompany[0]['thnpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataHideShowAppIdeCompany[0]['namapemegang_HAK_COLLAPROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['no_SURAT_UKUR_COLLA_PROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['tgl_SURAT_UKUR_COLLAPROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['sertifikat_SURATUKUR_COLLAPROP']== "1",
        _dataHideShowAppIdeCompany[0]['dp_JAMINAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['ph_MAKSIMAL_COLLA_PROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['jarakfasum_POSITIF_COLLAPROP']== "1",
        _dataHideShowAppIdeCompany[0]['jarakfasum_NEGATIF_COLLAPROP']== "1",
        _dataHideShowAppIdeCompany[0]['harga_TANAH_COLLA_PROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['harga_NJOP_COLLA_PROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['harga_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['jenis_ATAP_COLLA_PROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['jenis_DINDING_COLLA_PROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['jenis_LANTAI_COLLA_PROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['jenis_FONDASI_COLLA_PROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['tipe_JALAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['dilewati_MOBIL_COLLA_PROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['jml_RUMAH_DLM_RADIUS_COLLAPROP']== "1",
        _dataHideShowAppIdeCompany[0]['masa_BERLAKU_HAK_COLLAPROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['no_IMB_COLLA_PROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['tanggal_IMB_COLLA_PROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['luasbangunan_IMB_COLLAPROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['tuj_PENGGUNAAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['ltv_COLLA_PROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['semua_UNIT_COLLA_PROPERTI']== "1",
        _dataHideShowAppIdeCompany[0]['jns_ALAMAT_COLLAPROPERTIALAMAT']== "1",
        _dataHideShowAppIdeCompany[0]['alamat_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppIdeCompany[0]['rt_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppIdeCompany[0]['rw_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppIdeCompany[0]['provinsi_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppIdeCompany[0]['kab_KOTA_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppIdeCompany[0]['kec_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppIdeCompany[0]['kel_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppIdeCompany[0]['kode_POS_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppIdeCompany[0]['geo_LOCATION_ALAMAT']== "1",
        _dataMandatoryAppIdeCompany[0]['nm_JAMIN_PMOHON_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['jenis_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['no_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['nmjaminan_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['tanggal_LAHIR_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['tmpt_LAHIR_IDEN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['tptlahir_IDENLOV_COLLAPROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['no_SERTIFIKAT_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['jenis_SERTIFIKAT_COLLAPROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['jenis_PROPERTI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['luas_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['luas_TANAH_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['harga_TAKSASI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['sifat_JAMINAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['bktpemilikan_JAMIN_COLPROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['tglpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataMandatoryAppIdeCompany[0]['thnpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataMandatoryAppIdeCompany[0]['namapemegang_HAK_COLLAPROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['no_SURAT_UKUR_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['tgl_SURAT_UKUR_COLLAPROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['sertifikat_SURATUKUR_COLLAPROP']== "1",
        _dataMandatoryAppIdeCompany[0]['dp_JAMINAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['ph_MAKSIMAL_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['jarakfasum_POSITIF_COLLAPROP']== "1",
        _dataMandatoryAppIdeCompany[0]['jarakfasum_NEGATIF_COLLAPROP']== "1",
        _dataMandatoryAppIdeCompany[0]['harga_TANAH_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['harga_NJOP_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['harga_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['jenis_ATAP_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['jenis_DINDING_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['jenis_LANTAI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['jenis_FONDASI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['tipe_JALAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['dilewati_MOBIL_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['jml_RUMAH_DLM_RADIUS_COLLAPROP']== "1",
        _dataMandatoryAppIdeCompany[0]['masa_BERLAKU_HAK_COLLAPROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['no_IMB_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['tanggal_IMB_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['luasbangunan_IMB_COLLAPROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['tuj_PENGGUNAAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['ltv_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['semua_UNIT_COLLA_PROPERTI']== "1",
        _dataMandatoryAppIdeCompany[0]['jns_ALAMAT_COLLAPROPERTIALAMAT']== "1",
        _dataMandatoryAppIdeCompany[0]['alamat_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppIdeCompany[0]['rt_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppIdeCompany[0]['rw_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppIdeCompany[0]['provinsi_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppIdeCompany[0]['kab_KOTA_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppIdeCompany[0]['kec_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppIdeCompany[0]['kel_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppIdeCompany[0]['kode_POS_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppIdeCompany[0]['geo_LOCATION_ALAMAT']== "1",
      );
      // Reguler Survey
      _showMandatoryInfoCollaPropRegulerSurveyModel = ShowMandatoryInfoCollaPropModel(
        _dataHideShowAppRegulerSurvey[0]['nm_JAMIN_PMOHON_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['jenis_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['no_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['nmjaminan_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['tanggal_LAHIR_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['tmpt_LAHIR_IDEN_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['tptlahir_IDENLOV_COLLAPROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['no_SERTIFIKAT_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['jenis_SERTIFIKAT_COLLAPROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['jenis_PROPERTI_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['luas_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['luas_TANAH_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['harga_TAKSASI_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['sifat_JAMINAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['bktpemilikan_JAMIN_COLPROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['tglpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataHideShowAppRegulerSurvey[0]['thnpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataHideShowAppRegulerSurvey[0]['namapemegang_HAK_COLLAPROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['no_SURAT_UKUR_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['tgl_SURAT_UKUR_COLLAPROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['sertifikat_SURATUKUR_COLLAPROP']== "1",
        _dataHideShowAppRegulerSurvey[0]['dp_JAMINAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['ph_MAKSIMAL_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['jarakfasum_POSITIF_COLLAPROP']== "1",
        _dataHideShowAppRegulerSurvey[0]['jarakfasum_NEGATIF_COLLAPROP']== "1",
        _dataHideShowAppRegulerSurvey[0]['harga_TANAH_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['harga_NJOP_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['harga_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['jenis_ATAP_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['jenis_DINDING_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['jenis_LANTAI_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['jenis_FONDASI_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['tipe_JALAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['dilewati_MOBIL_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['jml_RUMAH_DLM_RADIUS_COLLAPROP']== "1",
        _dataHideShowAppRegulerSurvey[0]['masa_BERLAKU_HAK_COLLAPROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['no_IMB_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['tanggal_IMB_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['luasbangunan_IMB_COLLAPROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['tuj_PENGGUNAAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['ltv_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['semua_UNIT_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurvey[0]['jns_ALAMAT_COLLAPROPERTIALAMAT']== "1",
        _dataHideShowAppRegulerSurvey[0]['alamat_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppRegulerSurvey[0]['rt_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppRegulerSurvey[0]['rw_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppRegulerSurvey[0]['provinsi_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppRegulerSurvey[0]['kab_KOTA_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppRegulerSurvey[0]['kec_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppRegulerSurvey[0]['kel_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppRegulerSurvey[0]['kode_POS_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppRegulerSurvey[0]['geo_LOCATION_ALAMAT']== "1",
        _dataMandatoryAppRegulerSurvey[0]['nm_JAMIN_PMOHON_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jenis_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['no_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['nmjaminan_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['tanggal_LAHIR_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['tmpt_LAHIR_IDEN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['tptlahir_IDENLOV_COLLAPROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['no_SERTIFIKAT_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jenis_SERTIFIKAT_COLLAPROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jenis_PROPERTI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['luas_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['luas_TANAH_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['harga_TAKSASI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['sifat_JAMINAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['bktpemilikan_JAMIN_COLPROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['tglpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataMandatoryAppRegulerSurvey[0]['thnpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataMandatoryAppRegulerSurvey[0]['namapemegang_HAK_COLLAPROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['no_SURAT_UKUR_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['tgl_SURAT_UKUR_COLLAPROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['sertifikat_SURATUKUR_COLLAPROP']== "1",
        _dataMandatoryAppRegulerSurvey[0]['dp_JAMINAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['ph_MAKSIMAL_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jarakfasum_POSITIF_COLLAPROP']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jarakfasum_NEGATIF_COLLAPROP']== "1",
        _dataMandatoryAppRegulerSurvey[0]['harga_TANAH_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['harga_NJOP_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['harga_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jenis_ATAP_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jenis_DINDING_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jenis_LANTAI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jenis_FONDASI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['tipe_JALAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['dilewati_MOBIL_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jml_RUMAH_DLM_RADIUS_COLLAPROP']== "1",
        _dataMandatoryAppRegulerSurvey[0]['masa_BERLAKU_HAK_COLLAPROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['no_IMB_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['tanggal_IMB_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['luasbangunan_IMB_COLLAPROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['tuj_PENGGUNAAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['ltv_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['semua_UNIT_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jns_ALAMAT_COLLAPROPERTIALAMAT']== "1",
        _dataMandatoryAppRegulerSurvey[0]['alamat_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppRegulerSurvey[0]['rt_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppRegulerSurvey[0]['rw_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppRegulerSurvey[0]['provinsi_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppRegulerSurvey[0]['kab_KOTA_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppRegulerSurvey[0]['kec_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppRegulerSurvey[0]['kel_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppRegulerSurvey[0]['kode_POS_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppRegulerSurvey[0]['geo_LOCATION_ALAMAT']== "1",
      );
      _showMandatoryInfoCollaPropRegulerSurveyCompanyModel = ShowMandatoryInfoCollaPropModel(
        _dataHideShowAppRegulerSurveyCompany[0]['nm_JAMIN_PMOHON_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jenis_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['no_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['nmjaminan_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['tanggal_LAHIR_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['tmpt_LAHIR_IDEN_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['tptlahir_IDENLOV_COLLAPROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['no_SERTIFIKAT_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jenis_SERTIFIKAT_COLLAPROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jenis_PROPERTI_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['luas_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['luas_TANAH_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['harga_TAKSASI_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['sifat_JAMINAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['bktpemilikan_JAMIN_COLPROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['tglpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['thnpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['namapemegang_HAK_COLLAPROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['no_SURAT_UKUR_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['tgl_SURAT_UKUR_COLLAPROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['sertifikat_SURATUKUR_COLLAPROP']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['dp_JAMINAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['ph_MAKSIMAL_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jarakfasum_POSITIF_COLLAPROP']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jarakfasum_NEGATIF_COLLAPROP']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['harga_TANAH_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['harga_NJOP_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['harga_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jenis_ATAP_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jenis_DINDING_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jenis_LANTAI_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jenis_FONDASI_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['tipe_JALAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['dilewati_MOBIL_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jml_RUMAH_DLM_RADIUS_COLLAPROP']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['masa_BERLAKU_HAK_COLLAPROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['no_IMB_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['tanggal_IMB_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['luasbangunan_IMB_COLLAPROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['tuj_PENGGUNAAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['ltv_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['semua_UNIT_COLLA_PROPERTI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jns_ALAMAT_COLLAPROPERTIALAMAT']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['alamat_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['rt_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['rw_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['provinsi_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['kab_KOTA_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['kec_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['kel_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['kode_POS_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['geo_LOCATION_ALAMAT']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['nm_JAMIN_PMOHON_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jenis_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['no_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['nmjaminan_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['tanggal_LAHIR_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['tmpt_LAHIR_IDEN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['tptlahir_IDENLOV_COLLAPROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['no_SERTIFIKAT_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jenis_SERTIFIKAT_COLLAPROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jenis_PROPERTI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['luas_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['luas_TANAH_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['harga_TAKSASI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['sifat_JAMINAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['bktpemilikan_JAMIN_COLPROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['tglpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['thnpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['namapemegang_HAK_COLLAPROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['no_SURAT_UKUR_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['tgl_SURAT_UKUR_COLLAPROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['sertifikat_SURATUKUR_COLLAPROP']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['dp_JAMINAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['ph_MAKSIMAL_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jarakfasum_POSITIF_COLLAPROP']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jarakfasum_NEGATIF_COLLAPROP']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['harga_TANAH_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['harga_NJOP_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['harga_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jenis_ATAP_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jenis_DINDING_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jenis_LANTAI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jenis_FONDASI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['tipe_JALAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['dilewati_MOBIL_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jml_RUMAH_DLM_RADIUS_COLLAPROP']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['masa_BERLAKU_HAK_COLLAPROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['no_IMB_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['tanggal_IMB_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['luasbangunan_IMB_COLLAPROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['tuj_PENGGUNAAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['ltv_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['semua_UNIT_COLLA_PROPERTI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jns_ALAMAT_COLLAPROPERTIALAMAT']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['alamat_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['rt_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['rw_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['provinsi_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['kab_KOTA_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['kec_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['kel_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['kode_POS_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['geo_LOCATION_ALAMAT']== "1",
      );
      // PAC
      _showMandatoryInfoCollaPropPacModel = ShowMandatoryInfoCollaPropModel(
        _dataHideShowAppPac[0]['nm_JAMIN_PMOHON_COLLA_PROPERTI']== "1",
        _dataHideShowAppPac[0]['jenis_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataHideShowAppPac[0]['no_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataHideShowAppPac[0]['nmjaminan_COLLA_PROPERTI']== "1",
        _dataHideShowAppPac[0]['tanggal_LAHIR_COLLA_PROPERTI']== "1",
        _dataHideShowAppPac[0]['tmpt_LAHIR_IDEN_COLLA_PROPERTI']== "1",
        _dataHideShowAppPac[0]['tptlahir_IDENLOV_COLLAPROPERTI']== "1",
        _dataHideShowAppPac[0]['no_SERTIFIKAT_COLLA_PROPERTI']== "1",
        _dataHideShowAppPac[0]['jenis_SERTIFIKAT_COLLAPROPERTI']== "1",
        _dataHideShowAppPac[0]['jenis_PROPERTI_COLLA_PROPERTI']== "1",
        _dataHideShowAppPac[0]['luas_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppPac[0]['luas_TANAH_COLLA_PROPERTI']== "1",
        _dataHideShowAppPac[0]['harga_TAKSASI_COLLA_PROPERTI']== "1",
        _dataHideShowAppPac[0]['sifat_JAMINAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppPac[0]['bktpemilikan_JAMIN_COLPROPERTI']== "1",
        _dataHideShowAppPac[0]['tglpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataHideShowAppPac[0]['thnpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataHideShowAppPac[0]['namapemegang_HAK_COLLAPROPERTI']== "1",
        _dataHideShowAppPac[0]['no_SURAT_UKUR_COLLA_PROPERTI']== "1",
        _dataHideShowAppPac[0]['tgl_SURAT_UKUR_COLLAPROPERTI']== "1",
        _dataHideShowAppPac[0]['sertifikat_SURATUKUR_COLLAPROP']== "1",
        _dataHideShowAppPac[0]['dp_JAMINAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppPac[0]['ph_MAKSIMAL_COLLA_PROPERTI']== "1",
        _dataHideShowAppPac[0]['jarakfasum_POSITIF_COLLAPROP']== "1",
        _dataHideShowAppPac[0]['jarakfasum_NEGATIF_COLLAPROP']== "1",
        _dataHideShowAppPac[0]['harga_TANAH_COLLA_PROPERTI']== "1",
        _dataHideShowAppPac[0]['harga_NJOP_COLLA_PROPERTI']== "1",
        _dataHideShowAppPac[0]['harga_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppPac[0]['jenis_ATAP_COLLA_PROPERTI']== "1",
        _dataHideShowAppPac[0]['jenis_DINDING_COLLA_PROPERTI']== "1",
        _dataHideShowAppPac[0]['jenis_LANTAI_COLLA_PROPERTI']== "1",
        _dataHideShowAppPac[0]['jenis_FONDASI_COLLA_PROPERTI']== "1",
        _dataHideShowAppPac[0]['tipe_JALAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppPac[0]['dilewati_MOBIL_COLLA_PROPERTI']== "1",
        _dataHideShowAppPac[0]['jml_RUMAH_DLM_RADIUS_COLLAPROP']== "1",
        _dataHideShowAppPac[0]['masa_BERLAKU_HAK_COLLAPROPERTI']== "1",
        _dataHideShowAppPac[0]['no_IMB_COLLA_PROPERTI']== "1",
        _dataHideShowAppPac[0]['tanggal_IMB_COLLA_PROPERTI']== "1",
        _dataHideShowAppPac[0]['luasbangunan_IMB_COLLAPROPERTI']== "1",
        _dataHideShowAppPac[0]['tuj_PENGGUNAAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppPac[0]['ltv_COLLA_PROPERTI']== "1",
        _dataHideShowAppPac[0]['semua_UNIT_COLLA_PROPERTI']== "1",
        _dataHideShowAppPac[0]['jns_ALAMAT_COLLAPROPERTIALAMAT']== "1",
        _dataHideShowAppPac[0]['alamat_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppPac[0]['rt_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppPac[0]['rw_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppPac[0]['provinsi_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppPac[0]['kab_KOTA_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppPac[0]['kec_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppPac[0]['kel_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppPac[0]['kode_POS_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppPac[0]['geo_LOCATION_ALAMAT']== "1",
        _dataMandatoryAppPac[0]['nm_JAMIN_PMOHON_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPac[0]['jenis_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPac[0]['no_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPac[0]['nmjaminan_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPac[0]['tanggal_LAHIR_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPac[0]['tmpt_LAHIR_IDEN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPac[0]['tptlahir_IDENLOV_COLLAPROPERTI']== "1",
        _dataMandatoryAppPac[0]['no_SERTIFIKAT_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPac[0]['jenis_SERTIFIKAT_COLLAPROPERTI']== "1",
        _dataMandatoryAppPac[0]['jenis_PROPERTI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPac[0]['luas_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPac[0]['luas_TANAH_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPac[0]['harga_TAKSASI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPac[0]['sifat_JAMINAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPac[0]['bktpemilikan_JAMIN_COLPROPERTI']== "1",
        _dataMandatoryAppPac[0]['tglpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataMandatoryAppPac[0]['thnpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataMandatoryAppPac[0]['namapemegang_HAK_COLLAPROPERTI']== "1",
        _dataMandatoryAppPac[0]['no_SURAT_UKUR_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPac[0]['tgl_SURAT_UKUR_COLLAPROPERTI']== "1",
        _dataMandatoryAppPac[0]['sertifikat_SURATUKUR_COLLAPROP']== "1",
        _dataMandatoryAppPac[0]['dp_JAMINAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPac[0]['ph_MAKSIMAL_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPac[0]['jarakfasum_POSITIF_COLLAPROP']== "1",
        _dataMandatoryAppPac[0]['jarakfasum_NEGATIF_COLLAPROP']== "1",
        _dataMandatoryAppPac[0]['harga_TANAH_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPac[0]['harga_NJOP_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPac[0]['harga_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPac[0]['jenis_ATAP_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPac[0]['jenis_DINDING_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPac[0]['jenis_LANTAI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPac[0]['jenis_FONDASI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPac[0]['tipe_JALAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPac[0]['dilewati_MOBIL_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPac[0]['jml_RUMAH_DLM_RADIUS_COLLAPROP']== "1",
        _dataMandatoryAppPac[0]['masa_BERLAKU_HAK_COLLAPROPERTI']== "1",
        _dataMandatoryAppPac[0]['no_IMB_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPac[0]['tanggal_IMB_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPac[0]['luasbangunan_IMB_COLLAPROPERTI']== "1",
        _dataMandatoryAppPac[0]['tuj_PENGGUNAAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPac[0]['ltv_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPac[0]['semua_UNIT_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPac[0]['jns_ALAMAT_COLLAPROPERTIALAMAT']== "1",
        _dataMandatoryAppPac[0]['alamat_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppPac[0]['rt_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppPac[0]['rw_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppPac[0]['provinsi_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppPac[0]['kab_KOTA_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppPac[0]['kec_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppPac[0]['kel_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppPac[0]['kode_POS_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppPac[0]['geo_LOCATION_ALAMAT']== "1",
      );
      _showMandatoryInfoCollaPropPacCompanyModel = ShowMandatoryInfoCollaPropModel(
        _dataHideShowAppPacCompany[0]['nm_JAMIN_PMOHON_COLLA_PROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['jenis_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['no_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['nmjaminan_COLLA_PROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['tanggal_LAHIR_COLLA_PROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['tmpt_LAHIR_IDEN_COLLA_PROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['tptlahir_IDENLOV_COLLAPROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['no_SERTIFIKAT_COLLA_PROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['jenis_SERTIFIKAT_COLLAPROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['jenis_PROPERTI_COLLA_PROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['luas_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['luas_TANAH_COLLA_PROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['harga_TAKSASI_COLLA_PROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['sifat_JAMINAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['bktpemilikan_JAMIN_COLPROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['tglpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataHideShowAppPacCompany[0]['thnpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataHideShowAppPacCompany[0]['namapemegang_HAK_COLLAPROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['no_SURAT_UKUR_COLLA_PROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['tgl_SURAT_UKUR_COLLAPROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['sertifikat_SURATUKUR_COLLAPROP']== "1",
        _dataHideShowAppPacCompany[0]['dp_JAMINAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['ph_MAKSIMAL_COLLA_PROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['jarakfasum_POSITIF_COLLAPROP']== "1",
        _dataHideShowAppPacCompany[0]['jarakfasum_NEGATIF_COLLAPROP']== "1",
        _dataHideShowAppPacCompany[0]['harga_TANAH_COLLA_PROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['harga_NJOP_COLLA_PROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['harga_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['jenis_ATAP_COLLA_PROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['jenis_DINDING_COLLA_PROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['jenis_LANTAI_COLLA_PROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['jenis_FONDASI_COLLA_PROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['tipe_JALAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['dilewati_MOBIL_COLLA_PROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['jml_RUMAH_DLM_RADIUS_COLLAPROP']== "1",
        _dataHideShowAppPacCompany[0]['masa_BERLAKU_HAK_COLLAPROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['no_IMB_COLLA_PROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['tanggal_IMB_COLLA_PROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['luasbangunan_IMB_COLLAPROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['tuj_PENGGUNAAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['ltv_COLLA_PROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['semua_UNIT_COLLA_PROPERTI']== "1",
        _dataHideShowAppPacCompany[0]['jns_ALAMAT_COLLAPROPERTIALAMAT']== "1",
        _dataHideShowAppPacCompany[0]['alamat_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppPacCompany[0]['rt_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppPacCompany[0]['rw_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppPacCompany[0]['provinsi_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppPacCompany[0]['kab_KOTA_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppPacCompany[0]['kec_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppPacCompany[0]['kel_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppPacCompany[0]['kode_POS_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppPacCompany[0]['geo_LOCATION_ALAMAT']== "1",
        _dataMandatoryAppPacCompany[0]['nm_JAMIN_PMOHON_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['jenis_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['no_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['nmjaminan_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['tanggal_LAHIR_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['tmpt_LAHIR_IDEN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['tptlahir_IDENLOV_COLLAPROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['no_SERTIFIKAT_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['jenis_SERTIFIKAT_COLLAPROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['jenis_PROPERTI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['luas_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['luas_TANAH_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['harga_TAKSASI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['sifat_JAMINAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['bktpemilikan_JAMIN_COLPROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['tglpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataMandatoryAppPacCompany[0]['thnpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataMandatoryAppPacCompany[0]['namapemegang_HAK_COLLAPROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['no_SURAT_UKUR_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['tgl_SURAT_UKUR_COLLAPROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['sertifikat_SURATUKUR_COLLAPROP']== "1",
        _dataMandatoryAppPacCompany[0]['dp_JAMINAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['ph_MAKSIMAL_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['jarakfasum_POSITIF_COLLAPROP']== "1",
        _dataMandatoryAppPacCompany[0]['jarakfasum_NEGATIF_COLLAPROP']== "1",
        _dataMandatoryAppPacCompany[0]['harga_TANAH_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['harga_NJOP_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['harga_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['jenis_ATAP_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['jenis_DINDING_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['jenis_LANTAI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['jenis_FONDASI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['tipe_JALAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['dilewati_MOBIL_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['jml_RUMAH_DLM_RADIUS_COLLAPROP']== "1",
        _dataMandatoryAppPacCompany[0]['masa_BERLAKU_HAK_COLLAPROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['no_IMB_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['tanggal_IMB_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['luasbangunan_IMB_COLLAPROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['tuj_PENGGUNAAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['ltv_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['semua_UNIT_COLLA_PROPERTI']== "1",
        _dataMandatoryAppPacCompany[0]['jns_ALAMAT_COLLAPROPERTIALAMAT']== "1",
        _dataMandatoryAppPacCompany[0]['alamat_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppPacCompany[0]['rt_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppPacCompany[0]['rw_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppPacCompany[0]['provinsi_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppPacCompany[0]['kab_KOTA_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppPacCompany[0]['kec_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppPacCompany[0]['kel_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppPacCompany[0]['kode_POS_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppPacCompany[0]['geo_LOCATION_ALAMAT']== "1",
      );
      // Resurvey
      _showMandatoryInfoCollaPropResurveyModel = ShowMandatoryInfoCollaPropModel(
        _dataHideShowAppResurvey[0]['nm_JAMIN_PMOHON_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurvey[0]['jenis_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurvey[0]['no_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurvey[0]['nmjaminan_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurvey[0]['tanggal_LAHIR_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurvey[0]['tmpt_LAHIR_IDEN_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurvey[0]['tptlahir_IDENLOV_COLLAPROPERTI']== "1",
        _dataHideShowAppResurvey[0]['no_SERTIFIKAT_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurvey[0]['jenis_SERTIFIKAT_COLLAPROPERTI']== "1",
        _dataHideShowAppResurvey[0]['jenis_PROPERTI_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurvey[0]['luas_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurvey[0]['luas_TANAH_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurvey[0]['harga_TAKSASI_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurvey[0]['sifat_JAMINAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurvey[0]['bktpemilikan_JAMIN_COLPROPERTI']== "1",
        _dataHideShowAppResurvey[0]['tglpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataHideShowAppResurvey[0]['thnpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataHideShowAppResurvey[0]['namapemegang_HAK_COLLAPROPERTI']== "1",
        _dataHideShowAppResurvey[0]['no_SURAT_UKUR_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurvey[0]['tgl_SURAT_UKUR_COLLAPROPERTI']== "1",
        _dataHideShowAppResurvey[0]['sertifikat_SURATUKUR_COLLAPROP']== "1",
        _dataHideShowAppResurvey[0]['dp_JAMINAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurvey[0]['ph_MAKSIMAL_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurvey[0]['jarakfasum_POSITIF_COLLAPROP']== "1",
        _dataHideShowAppResurvey[0]['jarakfasum_NEGATIF_COLLAPROP']== "1",
        _dataHideShowAppResurvey[0]['harga_TANAH_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurvey[0]['harga_NJOP_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurvey[0]['harga_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurvey[0]['jenis_ATAP_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurvey[0]['jenis_DINDING_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurvey[0]['jenis_LANTAI_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurvey[0]['jenis_FONDASI_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurvey[0]['tipe_JALAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurvey[0]['dilewati_MOBIL_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurvey[0]['jml_RUMAH_DLM_RADIUS_COLLAPROP']== "1",
        _dataHideShowAppResurvey[0]['masa_BERLAKU_HAK_COLLAPROPERTI']== "1",
        _dataHideShowAppResurvey[0]['no_IMB_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurvey[0]['tanggal_IMB_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurvey[0]['luasbangunan_IMB_COLLAPROPERTI']== "1",
        _dataHideShowAppResurvey[0]['tuj_PENGGUNAAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurvey[0]['ltv_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurvey[0]['semua_UNIT_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurvey[0]['jns_ALAMAT_COLLAPROPERTIALAMAT']== "1",
        _dataHideShowAppResurvey[0]['alamat_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppResurvey[0]['rt_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppResurvey[0]['rw_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppResurvey[0]['provinsi_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppResurvey[0]['kab_KOTA_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppResurvey[0]['kec_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppResurvey[0]['kel_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppResurvey[0]['kode_POS_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppResurvey[0]['geo_LOCATION_ALAMAT']== "1",
        _dataMandatoryAppResurvey[0]['nm_JAMIN_PMOHON_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['jenis_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['no_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['nmjaminan_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['tanggal_LAHIR_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['tmpt_LAHIR_IDEN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['tptlahir_IDENLOV_COLLAPROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['no_SERTIFIKAT_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['jenis_SERTIFIKAT_COLLAPROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['jenis_PROPERTI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['luas_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['luas_TANAH_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['harga_TAKSASI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['sifat_JAMINAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['bktpemilikan_JAMIN_COLPROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['tglpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataMandatoryAppResurvey[0]['thnpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataMandatoryAppResurvey[0]['namapemegang_HAK_COLLAPROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['no_SURAT_UKUR_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['tgl_SURAT_UKUR_COLLAPROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['sertifikat_SURATUKUR_COLLAPROP']== "1",
        _dataMandatoryAppResurvey[0]['dp_JAMINAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['ph_MAKSIMAL_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['jarakfasum_POSITIF_COLLAPROP']== "1",
        _dataMandatoryAppResurvey[0]['jarakfasum_NEGATIF_COLLAPROP']== "1",
        _dataMandatoryAppResurvey[0]['harga_TANAH_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['harga_NJOP_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['harga_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['jenis_ATAP_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['jenis_DINDING_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['jenis_LANTAI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['jenis_FONDASI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['tipe_JALAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['dilewati_MOBIL_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['jml_RUMAH_DLM_RADIUS_COLLAPROP']== "1",
        _dataMandatoryAppResurvey[0]['masa_BERLAKU_HAK_COLLAPROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['no_IMB_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['tanggal_IMB_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['luasbangunan_IMB_COLLAPROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['tuj_PENGGUNAAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['ltv_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['semua_UNIT_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurvey[0]['jns_ALAMAT_COLLAPROPERTIALAMAT']== "1",
        _dataMandatoryAppResurvey[0]['alamat_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppResurvey[0]['rt_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppResurvey[0]['rw_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppResurvey[0]['provinsi_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppResurvey[0]['kab_KOTA_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppResurvey[0]['kec_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppResurvey[0]['kel_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppResurvey[0]['kode_POS_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppResurvey[0]['geo_LOCATION_ALAMAT']== "1",
      );
      _showMandatoryInfoCollaPropResurveyCompanyModel = ShowMandatoryInfoCollaPropModel(
        _dataHideShowAppResurveyCompany[0]['nm_JAMIN_PMOHON_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['jenis_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['no_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['nmjaminan_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['tanggal_LAHIR_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['tmpt_LAHIR_IDEN_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['tptlahir_IDENLOV_COLLAPROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['no_SERTIFIKAT_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['jenis_SERTIFIKAT_COLLAPROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['jenis_PROPERTI_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['luas_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['luas_TANAH_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['harga_TAKSASI_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['sifat_JAMINAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['bktpemilikan_JAMIN_COLPROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['tglpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataHideShowAppResurveyCompany[0]['thnpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataHideShowAppResurveyCompany[0]['namapemegang_HAK_COLLAPROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['no_SURAT_UKUR_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['tgl_SURAT_UKUR_COLLAPROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['sertifikat_SURATUKUR_COLLAPROP']== "1",
        _dataHideShowAppResurveyCompany[0]['dp_JAMINAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['ph_MAKSIMAL_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['jarakfasum_POSITIF_COLLAPROP']== "1",
        _dataHideShowAppResurveyCompany[0]['jarakfasum_NEGATIF_COLLAPROP']== "1",
        _dataHideShowAppResurveyCompany[0]['harga_TANAH_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['harga_NJOP_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['harga_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['jenis_ATAP_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['jenis_DINDING_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['jenis_LANTAI_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['jenis_FONDASI_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['tipe_JALAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['dilewati_MOBIL_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['jml_RUMAH_DLM_RADIUS_COLLAPROP']== "1",
        _dataHideShowAppResurveyCompany[0]['masa_BERLAKU_HAK_COLLAPROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['no_IMB_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['tanggal_IMB_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['luasbangunan_IMB_COLLAPROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['tuj_PENGGUNAAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['ltv_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['semua_UNIT_COLLA_PROPERTI']== "1",
        _dataHideShowAppResurveyCompany[0]['jns_ALAMAT_COLLAPROPERTIALAMAT']== "1",
        _dataHideShowAppResurveyCompany[0]['alamat_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppResurveyCompany[0]['rt_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppResurveyCompany[0]['rw_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppResurveyCompany[0]['provinsi_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppResurveyCompany[0]['kab_KOTA_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppResurveyCompany[0]['kec_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppResurveyCompany[0]['kel_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppResurveyCompany[0]['kode_POS_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppResurveyCompany[0]['geo_LOCATION_ALAMAT']== "1",
        _dataMandatoryAppResurveyCompany[0]['nm_JAMIN_PMOHON_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['jenis_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['no_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['nmjaminan_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['tanggal_LAHIR_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['tmpt_LAHIR_IDEN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['tptlahir_IDENLOV_COLLAPROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['no_SERTIFIKAT_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['jenis_SERTIFIKAT_COLLAPROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['jenis_PROPERTI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['luas_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['luas_TANAH_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['harga_TAKSASI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['sifat_JAMINAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['bktpemilikan_JAMIN_COLPROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['tglpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataMandatoryAppResurveyCompany[0]['thnpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataMandatoryAppResurveyCompany[0]['namapemegang_HAK_COLLAPROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['no_SURAT_UKUR_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['tgl_SURAT_UKUR_COLLAPROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['sertifikat_SURATUKUR_COLLAPROP']== "1",
        _dataMandatoryAppResurveyCompany[0]['dp_JAMINAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['ph_MAKSIMAL_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['jarakfasum_POSITIF_COLLAPROP']== "1",
        _dataMandatoryAppResurveyCompany[0]['jarakfasum_NEGATIF_COLLAPROP']== "1",
        _dataMandatoryAppResurveyCompany[0]['harga_TANAH_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['harga_NJOP_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['harga_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['jenis_ATAP_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['jenis_DINDING_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['jenis_LANTAI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['jenis_FONDASI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['tipe_JALAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['dilewati_MOBIL_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['jml_RUMAH_DLM_RADIUS_COLLAPROP']== "1",
        _dataMandatoryAppResurveyCompany[0]['masa_BERLAKU_HAK_COLLAPROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['no_IMB_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['tanggal_IMB_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['luasbangunan_IMB_COLLAPROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['tuj_PENGGUNAAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['ltv_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['semua_UNIT_COLLA_PROPERTI']== "1",
        _dataMandatoryAppResurveyCompany[0]['jns_ALAMAT_COLLAPROPERTIALAMAT']== "1",
        _dataMandatoryAppResurveyCompany[0]['alamat_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppResurveyCompany[0]['rt_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppResurveyCompany[0]['rw_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppResurveyCompany[0]['provinsi_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppResurveyCompany[0]['kab_KOTA_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppResurveyCompany[0]['kec_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppResurveyCompany[0]['kel_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppResurveyCompany[0]['kode_POS_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppResurveyCompany[0]['geo_LOCATION_ALAMAT']== "1",
      );
      // Dakor
      _showMandatoryInfoCollaPropDakorModel = ShowMandatoryInfoCollaPropModel(
        _dataHideShowAppDakor[0]['nm_JAMIN_PMOHON_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakor[0]['jenis_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakor[0]['no_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakor[0]['nmjaminan_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakor[0]['tanggal_LAHIR_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakor[0]['tmpt_LAHIR_IDEN_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakor[0]['tptlahir_IDENLOV_COLLAPROPERTI']== "1",
        _dataHideShowAppDakor[0]['no_SERTIFIKAT_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakor[0]['jenis_SERTIFIKAT_COLLAPROPERTI']== "1",
        _dataHideShowAppDakor[0]['jenis_PROPERTI_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakor[0]['luas_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakor[0]['luas_TANAH_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakor[0]['harga_TAKSASI_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakor[0]['sifat_JAMINAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakor[0]['bktpemilikan_JAMIN_COLPROPERTI']== "1",
        _dataHideShowAppDakor[0]['tglpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataHideShowAppDakor[0]['thnpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataHideShowAppDakor[0]['namapemegang_HAK_COLLAPROPERTI']== "1",
        _dataHideShowAppDakor[0]['no_SURAT_UKUR_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakor[0]['tgl_SURAT_UKUR_COLLAPROPERTI']== "1",
        _dataHideShowAppDakor[0]['sertifikat_SURATUKUR_COLLAPROP']== "1",
        _dataHideShowAppDakor[0]['dp_JAMINAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakor[0]['ph_MAKSIMAL_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakor[0]['jarakfasum_POSITIF_COLLAPROP']== "1",
        _dataHideShowAppDakor[0]['jarakfasum_NEGATIF_COLLAPROP']== "1",
        _dataHideShowAppDakor[0]['harga_TANAH_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakor[0]['harga_NJOP_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakor[0]['harga_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakor[0]['jenis_ATAP_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakor[0]['jenis_DINDING_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakor[0]['jenis_LANTAI_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakor[0]['jenis_FONDASI_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakor[0]['tipe_JALAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakor[0]['dilewati_MOBIL_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakor[0]['jml_RUMAH_DLM_RADIUS_COLLAPROP']== "1",
        _dataHideShowAppDakor[0]['masa_BERLAKU_HAK_COLLAPROPERTI']== "1",
        _dataHideShowAppDakor[0]['no_IMB_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakor[0]['tanggal_IMB_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakor[0]['luasbangunan_IMB_COLLAPROPERTI']== "1",
        _dataHideShowAppDakor[0]['tuj_PENGGUNAAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakor[0]['ltv_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakor[0]['semua_UNIT_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakor[0]['jns_ALAMAT_COLLAPROPERTIALAMAT']== "1",
        _dataHideShowAppDakor[0]['alamat_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppDakor[0]['rt_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppDakor[0]['rw_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppDakor[0]['provinsi_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppDakor[0]['kab_KOTA_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppDakor[0]['kec_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppDakor[0]['kel_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppDakor[0]['kode_POS_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppDakor[0]['geo_LOCATION_ALAMAT']== "1",
        _dataMandatoryAppDakor[0]['nm_JAMIN_PMOHON_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakor[0]['jenis_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakor[0]['no_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakor[0]['nmjaminan_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakor[0]['tanggal_LAHIR_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakor[0]['tmpt_LAHIR_IDEN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakor[0]['tptlahir_IDENLOV_COLLAPROPERTI']== "1",
        _dataMandatoryAppDakor[0]['no_SERTIFIKAT_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakor[0]['jenis_SERTIFIKAT_COLLAPROPERTI']== "1",
        _dataMandatoryAppDakor[0]['jenis_PROPERTI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakor[0]['luas_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakor[0]['luas_TANAH_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakor[0]['harga_TAKSASI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakor[0]['sifat_JAMINAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakor[0]['bktpemilikan_JAMIN_COLPROPERTI']== "1",
        _dataMandatoryAppDakor[0]['tglpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataMandatoryAppDakor[0]['thnpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataMandatoryAppDakor[0]['namapemegang_HAK_COLLAPROPERTI']== "1",
        _dataMandatoryAppDakor[0]['no_SURAT_UKUR_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakor[0]['tgl_SURAT_UKUR_COLLAPROPERTI']== "1",
        _dataMandatoryAppDakor[0]['sertifikat_SURATUKUR_COLLAPROP']== "1",
        _dataMandatoryAppDakor[0]['dp_JAMINAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakor[0]['ph_MAKSIMAL_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakor[0]['jarakfasum_POSITIF_COLLAPROP']== "1",
        _dataMandatoryAppDakor[0]['jarakfasum_NEGATIF_COLLAPROP']== "1",
        _dataMandatoryAppDakor[0]['harga_TANAH_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakor[0]['harga_NJOP_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakor[0]['harga_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakor[0]['jenis_ATAP_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakor[0]['jenis_DINDING_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakor[0]['jenis_LANTAI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakor[0]['jenis_FONDASI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakor[0]['tipe_JALAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakor[0]['dilewati_MOBIL_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakor[0]['jml_RUMAH_DLM_RADIUS_COLLAPROP']== "1",
        _dataMandatoryAppDakor[0]['masa_BERLAKU_HAK_COLLAPROPERTI']== "1",
        _dataMandatoryAppDakor[0]['no_IMB_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakor[0]['tanggal_IMB_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakor[0]['luasbangunan_IMB_COLLAPROPERTI']== "1",
        _dataMandatoryAppDakor[0]['tuj_PENGGUNAAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakor[0]['ltv_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakor[0]['semua_UNIT_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakor[0]['jns_ALAMAT_COLLAPROPERTIALAMAT']== "1",
        _dataMandatoryAppDakor[0]['alamat_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppDakor[0]['rt_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppDakor[0]['rw_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppDakor[0]['provinsi_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppDakor[0]['kab_KOTA_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppDakor[0]['kec_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppDakor[0]['kel_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppDakor[0]['kode_POS_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppDakor[0]['geo_LOCATION_ALAMAT']== "1",
      );
      _showMandatoryInfoCollaPropDakorCompanyModel = ShowMandatoryInfoCollaPropModel(
        _dataHideShowAppDakorCom[0]['nm_JAMIN_PMOHON_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['jenis_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['no_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['nmjaminan_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['tanggal_LAHIR_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['tmpt_LAHIR_IDEN_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['tptlahir_IDENLOV_COLLAPROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['no_SERTIFIKAT_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['jenis_SERTIFIKAT_COLLAPROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['jenis_PROPERTI_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['luas_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['luas_TANAH_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['harga_TAKSASI_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['sifat_JAMINAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['bktpemilikan_JAMIN_COLPROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['tglpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataHideShowAppDakorCom[0]['thnpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataHideShowAppDakorCom[0]['namapemegang_HAK_COLLAPROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['no_SURAT_UKUR_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['tgl_SURAT_UKUR_COLLAPROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['sertifikat_SURATUKUR_COLLAPROP']== "1",
        _dataHideShowAppDakorCom[0]['dp_JAMINAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['ph_MAKSIMAL_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['jarakfasum_POSITIF_COLLAPROP']== "1",
        _dataHideShowAppDakorCom[0]['jarakfasum_NEGATIF_COLLAPROP']== "1",
        _dataHideShowAppDakorCom[0]['harga_TANAH_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['harga_NJOP_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['harga_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['jenis_ATAP_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['jenis_DINDING_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['jenis_LANTAI_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['jenis_FONDASI_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['tipe_JALAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['dilewati_MOBIL_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['jml_RUMAH_DLM_RADIUS_COLLAPROP']== "1",
        _dataHideShowAppDakorCom[0]['masa_BERLAKU_HAK_COLLAPROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['no_IMB_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['tanggal_IMB_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['luasbangunan_IMB_COLLAPROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['tuj_PENGGUNAAN_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['ltv_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['semua_UNIT_COLLA_PROPERTI']== "1",
        _dataHideShowAppDakorCom[0]['jns_ALAMAT_COLLAPROPERTIALAMAT']== "1",
        _dataHideShowAppDakorCom[0]['alamat_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppDakorCom[0]['rt_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppDakorCom[0]['rw_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppDakorCom[0]['provinsi_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppDakorCom[0]['kab_KOTA_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppDakorCom[0]['kec_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppDakorCom[0]['kel_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppDakorCom[0]['kode_POS_COLLA_PROPERTI_ALAMAT']== "1",
        _dataHideShowAppDakorCom[0]['geo_LOCATION_ALAMAT']== "1",
        _dataMandatoryAppDakorCom[0]['nm_JAMIN_PMOHON_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['jenis_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['no_IDENTITAS_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['nmjaminan_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['tanggal_LAHIR_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['tmpt_LAHIR_IDEN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['tptlahir_IDENLOV_COLLAPROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['no_SERTIFIKAT_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['jenis_SERTIFIKAT_COLLAPROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['jenis_PROPERTI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['luas_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['luas_TANAH_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['harga_TAKSASI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['sifat_JAMINAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['bktpemilikan_JAMIN_COLPROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['tglpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataMandatoryAppDakorCom[0]['thnpenerbit_SERTIFIKAT_COLPROP']== "1",
        _dataMandatoryAppDakorCom[0]['namapemegang_HAK_COLLAPROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['no_SURAT_UKUR_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['tgl_SURAT_UKUR_COLLAPROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['sertifikat_SURATUKUR_COLLAPROP']== "1",
        _dataMandatoryAppDakorCom[0]['dp_JAMINAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['ph_MAKSIMAL_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['jarakfasum_POSITIF_COLLAPROP']== "1",
        _dataMandatoryAppDakorCom[0]['jarakfasum_NEGATIF_COLLAPROP']== "1",
        _dataMandatoryAppDakorCom[0]['harga_TANAH_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['harga_NJOP_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['harga_BANGUNAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['jenis_ATAP_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['jenis_DINDING_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['jenis_LANTAI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['jenis_FONDASI_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['tipe_JALAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['dilewati_MOBIL_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['jml_RUMAH_DLM_RADIUS_COLLAPROP']== "1",
        _dataMandatoryAppDakorCom[0]['masa_BERLAKU_HAK_COLLAPROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['no_IMB_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['tanggal_IMB_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['luasbangunan_IMB_COLLAPROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['tuj_PENGGUNAAN_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['ltv_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['semua_UNIT_COLLA_PROPERTI']== "1",
        _dataMandatoryAppDakorCom[0]['jns_ALAMAT_COLLAPROPERTIALAMAT']== "1",
        _dataMandatoryAppDakorCom[0]['alamat_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppDakorCom[0]['rt_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppDakorCom[0]['rw_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppDakorCom[0]['provinsi_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppDakorCom[0]['kab_KOTA_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppDakorCom[0]['kec_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppDakorCom[0]['kel_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppDakorCom[0]['kode_POS_COLLA_PROPERTI_ALAMAT']== "1",
        _dataMandatoryAppDakorCom[0]['geo_LOCATION_ALAMAT']== "1",
      );
      // End Inf Colla Prop

      // Inf Karoseri
      // IDE
      _showMandatoryInfoKaroseriIdeModel = ShowMandatoryInfoKaroseriModel(
        _dataHideShowAppIde[0]['pks_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataHideShowAppIde[0]['perusahaan_OBJEK_KAROSERI']== "1",
        _dataHideShowAppIde[0]['karoseri_OBJEK_KAROSERI']== "1",
        _dataHideShowAppIde[0]['jumlah_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataHideShowAppIde[0]['harga_OBJEK_KAROSERI']== "1",
        _dataHideShowAppIde[0]['total_HARGA_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppIde[0]['pks_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppIde[0]['perusahaan_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppIde[0]['karoseri_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppIde[0]['jumlah_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppIde[0]['harga_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppIde[0]['total_HARGA_OBJEK_KAROSERI']== "1",
      );
      _showMandatoryInfoKaroseriIdeCompanyModel = ShowMandatoryInfoKaroseriModel(
        _dataHideShowAppIdeCompany[0]['pks_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataHideShowAppIdeCompany[0]['perusahaan_OBJEK_KAROSERI']== "1",
        _dataHideShowAppIdeCompany[0]['karoseri_OBJEK_KAROSERI']== "1",
        _dataHideShowAppIdeCompany[0]['jumlah_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataHideShowAppIdeCompany[0]['harga_OBJEK_KAROSERI']== "1",
        _dataHideShowAppIdeCompany[0]['total_HARGA_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppIdeCompany[0]['pks_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppIdeCompany[0]['perusahaan_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppIdeCompany[0]['karoseri_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppIdeCompany[0]['jumlah_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppIdeCompany[0]['harga_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppIdeCompany[0]['total_HARGA_OBJEK_KAROSERI']== "1",
      );
      // Reguler Survey
      _showMandatoryInfoKaroseriRegulerSurveyModel = ShowMandatoryInfoKaroseriModel(
        _dataHideShowAppRegulerSurvey[0]['pks_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataHideShowAppRegulerSurvey[0]['perusahaan_OBJEK_KAROSERI']== "1",
        _dataHideShowAppRegulerSurvey[0]['karoseri_OBJEK_KAROSERI']== "1",
        _dataHideShowAppRegulerSurvey[0]['jumlah_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataHideShowAppRegulerSurvey[0]['harga_OBJEK_KAROSERI']== "1",
        _dataHideShowAppRegulerSurvey[0]['total_HARGA_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['pks_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['perusahaan_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['karoseri_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jumlah_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['harga_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['total_HARGA_OBJEK_KAROSERI']== "1",
      );
      _showMandatoryInfoKaroseriRegulerSurveyCompanyModel = ShowMandatoryInfoKaroseriModel(
        _dataHideShowAppRegulerSurveyCompany[0]['pks_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['perusahaan_OBJEK_KAROSERI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['karoseri_OBJEK_KAROSERI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jumlah_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['harga_OBJEK_KAROSERI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['total_HARGA_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['pks_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['perusahaan_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['karoseri_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jumlah_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['harga_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['total_HARGA_OBJEK_KAROSERI']== "1",
      );
      // PAC
      _showMandatoryInfoKaroseriPacModel = ShowMandatoryInfoKaroseriModel(
        _dataHideShowAppPac[0]['pks_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataHideShowAppPac[0]['perusahaan_OBJEK_KAROSERI']== "1",
        _dataHideShowAppPac[0]['karoseri_OBJEK_KAROSERI']== "1",
        _dataHideShowAppPac[0]['jumlah_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataHideShowAppPac[0]['harga_OBJEK_KAROSERI']== "1",
        _dataHideShowAppPac[0]['total_HARGA_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppPac[0]['pks_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppPac[0]['perusahaan_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppPac[0]['karoseri_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppPac[0]['jumlah_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppPac[0]['harga_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppPac[0]['total_HARGA_OBJEK_KAROSERI']== "1",
      );
      _showMandatoryInfoKaroseriPacCompanyModel = ShowMandatoryInfoKaroseriModel(
        _dataHideShowAppPacCompany[0]['pks_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataHideShowAppPacCompany[0]['perusahaan_OBJEK_KAROSERI']== "1",
        _dataHideShowAppPacCompany[0]['karoseri_OBJEK_KAROSERI']== "1",
        _dataHideShowAppPacCompany[0]['jumlah_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataHideShowAppPacCompany[0]['harga_OBJEK_KAROSERI']== "1",
        _dataHideShowAppPacCompany[0]['total_HARGA_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppPacCompany[0]['pks_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppPacCompany[0]['perusahaan_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppPacCompany[0]['karoseri_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppPacCompany[0]['jumlah_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppPacCompany[0]['harga_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppPacCompany[0]['total_HARGA_OBJEK_KAROSERI']== "1",
      );
      // Resurvey
      _showMandatoryInfoKaroseriResurveyModel = ShowMandatoryInfoKaroseriModel(
        _dataHideShowAppResurvey[0]['pks_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataHideShowAppResurvey[0]['perusahaan_OBJEK_KAROSERI']== "1",
        _dataHideShowAppResurvey[0]['karoseri_OBJEK_KAROSERI']== "1",
        _dataHideShowAppResurvey[0]['jumlah_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataHideShowAppResurvey[0]['harga_OBJEK_KAROSERI']== "1",
        _dataHideShowAppResurvey[0]['total_HARGA_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppResurvey[0]['pks_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppResurvey[0]['perusahaan_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppResurvey[0]['karoseri_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppResurvey[0]['jumlah_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppResurvey[0]['harga_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppResurvey[0]['total_HARGA_OBJEK_KAROSERI']== "1",
      );
      _showMandatoryInfoKaroseriResurveyCompanyModel = ShowMandatoryInfoKaroseriModel(
        _dataHideShowAppResurveyCompany[0]['pks_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataHideShowAppResurveyCompany[0]['perusahaan_OBJEK_KAROSERI']== "1",
        _dataHideShowAppResurveyCompany[0]['karoseri_OBJEK_KAROSERI']== "1",
        _dataHideShowAppResurveyCompany[0]['jumlah_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataHideShowAppResurveyCompany[0]['harga_OBJEK_KAROSERI']== "1",
        _dataHideShowAppResurveyCompany[0]['total_HARGA_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppResurveyCompany[0]['pks_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppResurveyCompany[0]['perusahaan_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppResurveyCompany[0]['karoseri_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppResurveyCompany[0]['jumlah_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppResurveyCompany[0]['harga_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppResurveyCompany[0]['total_HARGA_OBJEK_KAROSERI']== "1",
      );
      // Dakor
      _showMandatoryInfoKaroseriDakorModel = ShowMandatoryInfoKaroseriModel(
        _dataHideShowAppDakor[0]['pks_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataHideShowAppDakor[0]['perusahaan_OBJEK_KAROSERI']== "1",
        _dataHideShowAppDakor[0]['karoseri_OBJEK_KAROSERI']== "1",
        _dataHideShowAppDakor[0]['jumlah_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataHideShowAppDakor[0]['harga_OBJEK_KAROSERI']== "1",
        _dataHideShowAppDakor[0]['total_HARGA_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppDakor[0]['pks_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppDakor[0]['perusahaan_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppDakor[0]['karoseri_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppDakor[0]['jumlah_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppDakor[0]['harga_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppDakor[0]['total_HARGA_OBJEK_KAROSERI']== "1",
      );
      _showMandatoryInfoKaroseriDakorCompanyModel = ShowMandatoryInfoKaroseriModel(
        _dataHideShowAppDakorCom[0]['pks_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataHideShowAppDakorCom[0]['perusahaan_OBJEK_KAROSERI']== "1",
        _dataHideShowAppDakorCom[0]['karoseri_OBJEK_KAROSERI']== "1",
        _dataHideShowAppDakorCom[0]['jumlah_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataHideShowAppDakorCom[0]['harga_OBJEK_KAROSERI']== "1",
        _dataHideShowAppDakorCom[0]['total_HARGA_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppDakorCom[0]['pks_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppDakorCom[0]['perusahaan_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppDakorCom[0]['karoseri_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppDakorCom[0]['jumlah_KAROSERI_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppDakorCom[0]['harga_OBJEK_KAROSERI']== "1",
        _dataMandatoryAppDakorCom[0]['total_HARGA_OBJEK_KAROSERI']== "1",
      );
      // End Inf Karoseri

      // Inf Struktur Kredit
      // IDE
      _showMandatoryInfoStrukturKreditIdeModel = ShowMandatoryInfoStrukturKreditModel(
        _dataHideShowAppIde[0]['jenis_ANGSURAN_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppIde[0]['jangka_WAKTU_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppIde[0]['payment_PER_YEAR_STRUK_KREDIT']== "1",
        _dataHideShowAppIde[0]['metodepembayaran_STRKTURKREDIT']== "1",
        _dataHideShowAppIde[0]['suku_BUNGA_EFF_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppIde[0]['suku_BUNGA_FLAT_STRKTUR_KREDIT']== "1",
        _dataHideShowAppIde[0]['harga_OBJEK_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppIde[0]['totharga_KARO_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppIde[0]['totharga_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppIde[0]['uang_MUKA_NET_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppIde[0]['dp_CABANG_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppIde[0]['dp_GROSS_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppIde[0]['jum_PINJAMAN_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppIde[0]['angsuran_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppIde[0]['ltv_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppIde[0]['jenis_ANGSURAN_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppIde[0]['jangka_WAKTU_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppIde[0]['payment_PER_YEAR_STRUK_KREDIT']== "1",
        _dataMandatoryAppIde[0]['metodepembayaran_STRKTURKREDIT']== "1",
        _dataMandatoryAppIde[0]['suku_BUNGA_EFF_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppIde[0]['suku_BUNGA_FLAT_STRKTUR_KREDIT']== "1",
        _dataMandatoryAppIde[0]['harga_OBJEK_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppIde[0]['totharga_KARO_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppIde[0]['totharga_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppIde[0]['uang_MUKA_NET_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppIde[0]['dp_CABANG_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppIde[0]['dp_GROSS_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppIde[0]['jum_PINJAMAN_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppIde[0]['angsuran_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppIde[0]['ltv_STRUKTUR_KREDIT']== "1",
      );
      _showMandatoryInfoStrukturKreditIdeCompanyModel = ShowMandatoryInfoStrukturKreditModel(
        _dataHideShowAppIdeCompany[0]['jenis_ANGSURAN_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppIdeCompany[0]['jangka_WAKTU_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppIdeCompany[0]['payment_PER_YEAR_STRUK_KREDIT']== "1",
        _dataHideShowAppIdeCompany[0]['metodepembayaran_STRKTURKREDIT']== "1",
        _dataHideShowAppIdeCompany[0]['suku_BUNGA_EFF_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppIdeCompany[0]['suku_BUNGA_FLAT_STRKTUR_KREDIT']== "1",
        _dataHideShowAppIdeCompany[0]['harga_OBJEK_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppIdeCompany[0]['totharga_KARO_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppIdeCompany[0]['totharga_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppIdeCompany[0]['uang_MUKA_NET_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppIdeCompany[0]['dp_CABANG_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppIdeCompany[0]['dp_GROSS_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppIdeCompany[0]['jum_PINJAMAN_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppIdeCompany[0]['angsuran_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppIdeCompany[0]['ltv_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppIdeCompany[0]['jenis_ANGSURAN_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppIdeCompany[0]['jangka_WAKTU_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppIdeCompany[0]['payment_PER_YEAR_STRUK_KREDIT']== "1",
        _dataMandatoryAppIdeCompany[0]['metodepembayaran_STRKTURKREDIT']== "1",
        _dataMandatoryAppIdeCompany[0]['suku_BUNGA_EFF_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppIdeCompany[0]['suku_BUNGA_FLAT_STRKTUR_KREDIT']== "1",
        _dataMandatoryAppIdeCompany[0]['harga_OBJEK_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppIdeCompany[0]['totharga_KARO_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppIdeCompany[0]['totharga_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppIdeCompany[0]['uang_MUKA_NET_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppIdeCompany[0]['dp_CABANG_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppIdeCompany[0]['dp_GROSS_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppIdeCompany[0]['jum_PINJAMAN_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppIdeCompany[0]['angsuran_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppIdeCompany[0]['ltv_STRUKTUR_KREDIT']== "1",
      );
      // Reguler Survey
      _showMandatoryInfoStrukturKreditRegulerSurveyModel = ShowMandatoryInfoStrukturKreditModel(
        _dataHideShowAppRegulerSurvey[0]['jenis_ANGSURAN_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppRegulerSurvey[0]['jangka_WAKTU_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppRegulerSurvey[0]['payment_PER_YEAR_STRUK_KREDIT']== "1",
        _dataHideShowAppRegulerSurvey[0]['metodepembayaran_STRKTURKREDIT']== "1",
        _dataHideShowAppRegulerSurvey[0]['suku_BUNGA_EFF_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppRegulerSurvey[0]['suku_BUNGA_FLAT_STRKTUR_KREDIT']== "1",
        _dataHideShowAppRegulerSurvey[0]['harga_OBJEK_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppRegulerSurvey[0]['totharga_KARO_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppRegulerSurvey[0]['totharga_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppRegulerSurvey[0]['uang_MUKA_NET_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppRegulerSurvey[0]['dp_CABANG_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppRegulerSurvey[0]['dp_GROSS_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppRegulerSurvey[0]['jum_PINJAMAN_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppRegulerSurvey[0]['angsuran_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppRegulerSurvey[0]['ltv_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jenis_ANGSURAN_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jangka_WAKTU_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppRegulerSurvey[0]['payment_PER_YEAR_STRUK_KREDIT']== "1",
        _dataMandatoryAppRegulerSurvey[0]['metodepembayaran_STRKTURKREDIT']== "1",
        _dataMandatoryAppRegulerSurvey[0]['suku_BUNGA_EFF_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppRegulerSurvey[0]['suku_BUNGA_FLAT_STRKTUR_KREDIT']== "1",
        _dataMandatoryAppRegulerSurvey[0]['harga_OBJEK_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppRegulerSurvey[0]['totharga_KARO_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppRegulerSurvey[0]['totharga_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppRegulerSurvey[0]['uang_MUKA_NET_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppRegulerSurvey[0]['dp_CABANG_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppRegulerSurvey[0]['dp_GROSS_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jum_PINJAMAN_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppRegulerSurvey[0]['angsuran_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppRegulerSurvey[0]['ltv_STRUKTUR_KREDIT']== "1",
      );
      _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel = ShowMandatoryInfoStrukturKreditModel(
        _dataHideShowAppRegulerSurveyCompany[0]['jenis_ANGSURAN_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jangka_WAKTU_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['payment_PER_YEAR_STRUK_KREDIT']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['metodepembayaran_STRKTURKREDIT']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['suku_BUNGA_EFF_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['suku_BUNGA_FLAT_STRKTUR_KREDIT']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['harga_OBJEK_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['totharga_KARO_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['totharga_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['uang_MUKA_NET_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['dp_CABANG_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['dp_GROSS_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jum_PINJAMAN_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['angsuran_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['ltv_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jenis_ANGSURAN_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jangka_WAKTU_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['payment_PER_YEAR_STRUK_KREDIT']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['metodepembayaran_STRKTURKREDIT']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['suku_BUNGA_EFF_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['suku_BUNGA_FLAT_STRKTUR_KREDIT']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['harga_OBJEK_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['totharga_KARO_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['totharga_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['uang_MUKA_NET_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['dp_CABANG_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['dp_GROSS_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jum_PINJAMAN_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['angsuran_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['ltv_STRUKTUR_KREDIT']== "1",
      );
      // PAC
      _showMandatoryInfoStrukturKreditPacModel = ShowMandatoryInfoStrukturKreditModel(
        _dataHideShowAppPac[0]['jenis_ANGSURAN_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppPac[0]['jangka_WAKTU_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppPac[0]['payment_PER_YEAR_STRUK_KREDIT']== "1",
        _dataHideShowAppPac[0]['metodepembayaran_STRKTURKREDIT']== "1",
        _dataHideShowAppPac[0]['suku_BUNGA_EFF_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppPac[0]['suku_BUNGA_FLAT_STRKTUR_KREDIT']== "1",
        _dataHideShowAppPac[0]['harga_OBJEK_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppPac[0]['totharga_KARO_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppPac[0]['totharga_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppPac[0]['uang_MUKA_NET_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppPac[0]['dp_CABANG_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppPac[0]['dp_GROSS_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppPac[0]['jum_PINJAMAN_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppPac[0]['angsuran_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppPac[0]['ltv_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppPac[0]['jenis_ANGSURAN_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppPac[0]['jangka_WAKTU_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppPac[0]['payment_PER_YEAR_STRUK_KREDIT']== "1",
        _dataMandatoryAppPac[0]['metodepembayaran_STRKTURKREDIT']== "1",
        _dataMandatoryAppPac[0]['suku_BUNGA_EFF_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppPac[0]['suku_BUNGA_FLAT_STRKTUR_KREDIT']== "1",
        _dataMandatoryAppPac[0]['harga_OBJEK_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppPac[0]['totharga_KARO_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppPac[0]['totharga_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppPac[0]['uang_MUKA_NET_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppPac[0]['dp_CABANG_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppPac[0]['dp_GROSS_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppPac[0]['jum_PINJAMAN_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppPac[0]['angsuran_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppPac[0]['ltv_STRUKTUR_KREDIT']== "1",
      );
      _showMandatoryInfoStrukturKreditPacCompanyModel = ShowMandatoryInfoStrukturKreditModel(
        _dataHideShowAppPacCompany[0]['jenis_ANGSURAN_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppPacCompany[0]['jangka_WAKTU_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppPacCompany[0]['payment_PER_YEAR_STRUK_KREDIT']== "1",
        _dataHideShowAppPacCompany[0]['metodepembayaran_STRKTURKREDIT']== "1",
        _dataHideShowAppPacCompany[0]['suku_BUNGA_EFF_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppPacCompany[0]['suku_BUNGA_FLAT_STRKTUR_KREDIT']== "1",
        _dataHideShowAppPacCompany[0]['harga_OBJEK_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppPacCompany[0]['totharga_KARO_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppPacCompany[0]['totharga_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppPacCompany[0]['uang_MUKA_NET_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppPacCompany[0]['dp_CABANG_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppPacCompany[0]['dp_GROSS_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppPacCompany[0]['jum_PINJAMAN_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppPacCompany[0]['angsuran_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppPacCompany[0]['ltv_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppPacCompany[0]['jenis_ANGSURAN_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppPacCompany[0]['jangka_WAKTU_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppPacCompany[0]['payment_PER_YEAR_STRUK_KREDIT']== "1",
        _dataMandatoryAppPacCompany[0]['metodepembayaran_STRKTURKREDIT']== "1",
        _dataMandatoryAppPacCompany[0]['suku_BUNGA_EFF_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppPacCompany[0]['suku_BUNGA_FLAT_STRKTUR_KREDIT']== "1",
        _dataMandatoryAppPacCompany[0]['harga_OBJEK_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppPacCompany[0]['totharga_KARO_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppPacCompany[0]['totharga_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppPacCompany[0]['uang_MUKA_NET_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppPacCompany[0]['dp_CABANG_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppPacCompany[0]['dp_GROSS_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppPacCompany[0]['jum_PINJAMAN_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppPacCompany[0]['angsuran_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppPacCompany[0]['ltv_STRUKTUR_KREDIT']== "1",
      );
      // Resurvey
      _showMandatoryInfoStrukturKreditResurveyModel = ShowMandatoryInfoStrukturKreditModel(
        _dataHideShowAppResurvey[0]['jenis_ANGSURAN_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppResurvey[0]['jangka_WAKTU_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppResurvey[0]['payment_PER_YEAR_STRUK_KREDIT']== "1",
        _dataHideShowAppResurvey[0]['metodepembayaran_STRKTURKREDIT']== "1",
        _dataHideShowAppResurvey[0]['suku_BUNGA_EFF_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppResurvey[0]['suku_BUNGA_FLAT_STRKTUR_KREDIT']== "1",
        _dataHideShowAppResurvey[0]['harga_OBJEK_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppResurvey[0]['totharga_KARO_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppResurvey[0]['totharga_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppResurvey[0]['uang_MUKA_NET_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppResurvey[0]['dp_CABANG_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppResurvey[0]['dp_GROSS_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppResurvey[0]['jum_PINJAMAN_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppResurvey[0]['angsuran_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppResurvey[0]['ltv_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppResurvey[0]['jenis_ANGSURAN_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppResurvey[0]['jangka_WAKTU_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppResurvey[0]['payment_PER_YEAR_STRUK_KREDIT']== "1",
        _dataMandatoryAppResurvey[0]['metodepembayaran_STRKTURKREDIT']== "1",
        _dataMandatoryAppResurvey[0]['suku_BUNGA_EFF_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppResurvey[0]['suku_BUNGA_FLAT_STRKTUR_KREDIT']== "1",
        _dataMandatoryAppResurvey[0]['harga_OBJEK_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppResurvey[0]['totharga_KARO_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppResurvey[0]['totharga_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppResurvey[0]['uang_MUKA_NET_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppResurvey[0]['dp_CABANG_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppResurvey[0]['dp_GROSS_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppResurvey[0]['jum_PINJAMAN_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppResurvey[0]['angsuran_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppResurvey[0]['ltv_STRUKTUR_KREDIT']== "1",
      );
      _showMandatoryInfoStrukturKreditResurveyCompanyModel = ShowMandatoryInfoStrukturKreditModel(
        _dataHideShowAppResurveyCompany[0]['jenis_ANGSURAN_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppResurveyCompany[0]['jangka_WAKTU_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppResurveyCompany[0]['payment_PER_YEAR_STRUK_KREDIT']== "1",
        _dataHideShowAppResurveyCompany[0]['metodepembayaran_STRKTURKREDIT']== "1",
        _dataHideShowAppResurveyCompany[0]['suku_BUNGA_EFF_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppResurveyCompany[0]['suku_BUNGA_FLAT_STRKTUR_KREDIT']== "1",
        _dataHideShowAppResurveyCompany[0]['harga_OBJEK_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppResurveyCompany[0]['totharga_KARO_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppResurveyCompany[0]['totharga_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppResurveyCompany[0]['uang_MUKA_NET_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppResurveyCompany[0]['dp_CABANG_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppResurveyCompany[0]['dp_GROSS_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppResurveyCompany[0]['jum_PINJAMAN_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppResurveyCompany[0]['angsuran_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppResurveyCompany[0]['ltv_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppResurveyCompany[0]['jenis_ANGSURAN_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppResurveyCompany[0]['jangka_WAKTU_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppResurveyCompany[0]['payment_PER_YEAR_STRUK_KREDIT']== "1",
        _dataMandatoryAppResurveyCompany[0]['metodepembayaran_STRKTURKREDIT']== "1",
        _dataMandatoryAppResurveyCompany[0]['suku_BUNGA_EFF_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppResurveyCompany[0]['suku_BUNGA_FLAT_STRKTUR_KREDIT']== "1",
        _dataMandatoryAppResurveyCompany[0]['harga_OBJEK_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppResurveyCompany[0]['totharga_KARO_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppResurveyCompany[0]['totharga_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppResurveyCompany[0]['uang_MUKA_NET_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppResurveyCompany[0]['dp_CABANG_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppResurveyCompany[0]['dp_GROSS_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppResurveyCompany[0]['jum_PINJAMAN_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppResurveyCompany[0]['angsuran_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppResurveyCompany[0]['ltv_STRUKTUR_KREDIT']== "1",
      );
      // Dakor
      _showMandatoryInfoStrukturKreditDakorModel = ShowMandatoryInfoStrukturKreditModel(
        _dataHideShowAppDakor[0]['jenis_ANGSURAN_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppDakor[0]['jangka_WAKTU_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppDakor[0]['payment_PER_YEAR_STRUK_KREDIT']== "1",
        _dataHideShowAppDakor[0]['metodepembayaran_STRKTURKREDIT']== "1",
        _dataHideShowAppDakor[0]['suku_BUNGA_EFF_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppDakor[0]['suku_BUNGA_FLAT_STRKTUR_KREDIT']== "1",
        _dataHideShowAppDakor[0]['harga_OBJEK_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppDakor[0]['totharga_KARO_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppDakor[0]['totharga_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppDakor[0]['uang_MUKA_NET_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppDakor[0]['dp_CABANG_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppDakor[0]['dp_GROSS_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppDakor[0]['jum_PINJAMAN_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppDakor[0]['angsuran_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppDakor[0]['ltv_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppDakor[0]['jenis_ANGSURAN_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppDakor[0]['jangka_WAKTU_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppDakor[0]['payment_PER_YEAR_STRUK_KREDIT']== "1",
        _dataMandatoryAppDakor[0]['metodepembayaran_STRKTURKREDIT']== "1",
        _dataMandatoryAppDakor[0]['suku_BUNGA_EFF_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppDakor[0]['suku_BUNGA_FLAT_STRKTUR_KREDIT']== "1",
        _dataMandatoryAppDakor[0]['harga_OBJEK_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppDakor[0]['totharga_KARO_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppDakor[0]['totharga_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppDakor[0]['uang_MUKA_NET_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppDakor[0]['dp_CABANG_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppDakor[0]['dp_GROSS_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppDakor[0]['jum_PINJAMAN_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppDakor[0]['angsuran_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppDakor[0]['ltv_STRUKTUR_KREDIT']== "1",
      );
      _showMandatoryInfoStrukturKreditDakorCompanyModel = ShowMandatoryInfoStrukturKreditModel(
        _dataHideShowAppDakorCom[0]['jenis_ANGSURAN_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppDakorCom[0]['jangka_WAKTU_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppDakorCom[0]['payment_PER_YEAR_STRUK_KREDIT']== "1",
        _dataHideShowAppDakorCom[0]['metodepembayaran_STRKTURKREDIT']== "1",
        _dataHideShowAppDakorCom[0]['suku_BUNGA_EFF_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppDakorCom[0]['suku_BUNGA_FLAT_STRKTUR_KREDIT']== "1",
        _dataHideShowAppDakorCom[0]['harga_OBJEK_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppDakorCom[0]['totharga_KARO_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppDakorCom[0]['totharga_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppDakorCom[0]['uang_MUKA_NET_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppDakorCom[0]['dp_CABANG_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppDakorCom[0]['dp_GROSS_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppDakorCom[0]['jum_PINJAMAN_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppDakorCom[0]['angsuran_STRUKTUR_KREDIT']== "1",
        _dataHideShowAppDakorCom[0]['ltv_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppDakorCom[0]['jenis_ANGSURAN_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppDakorCom[0]['jangka_WAKTU_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppDakorCom[0]['payment_PER_YEAR_STRUK_KREDIT']== "1",
        _dataMandatoryAppDakorCom[0]['metodepembayaran_STRKTURKREDIT']== "1",
        _dataMandatoryAppDakorCom[0]['suku_BUNGA_EFF_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppDakorCom[0]['suku_BUNGA_FLAT_STRKTUR_KREDIT']== "1",
        _dataMandatoryAppDakorCom[0]['harga_OBJEK_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppDakorCom[0]['totharga_KARO_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppDakorCom[0]['totharga_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppDakorCom[0]['uang_MUKA_NET_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppDakorCom[0]['dp_CABANG_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppDakorCom[0]['dp_GROSS_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppDakorCom[0]['jum_PINJAMAN_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppDakorCom[0]['angsuran_STRUKTUR_KREDIT']== "1",
        _dataMandatoryAppDakorCom[0]['ltv_STRUKTUR_KREDIT']== "1",
      );
      // End Inf Struktur Kredit

      // Inf Struktur Kredit Biaya
      // IDE
      _showMandatoryInfoStrukturKreditBiayaIdeModel = ShowMandatoryInfoStrukturKreditBiayaModel(
        _dataHideShowAppIde[0]['jenis_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppIde[0]['biaya_TUNAI_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppIde[0]['biaya_KREDIT_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppIde[0]['tot_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppIde[0]['jenis_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppIde[0]['biaya_TUNAI_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppIde[0]['biaya_KREDIT_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppIde[0]['tot_BIAYA_STRUKKREDIT_BIAYA']== "1",
      );
      _showMandatoryInfoStrukturKreditBiayaIdeCompanyModel = ShowMandatoryInfoStrukturKreditBiayaModel(
        _dataHideShowAppIdeCompany[0]['jenis_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppIdeCompany[0]['biaya_TUNAI_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppIdeCompany[0]['biaya_KREDIT_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppIdeCompany[0]['tot_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppIdeCompany[0]['jenis_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppIdeCompany[0]['biaya_TUNAI_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppIdeCompany[0]['biaya_KREDIT_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppIdeCompany[0]['tot_BIAYA_STRUKKREDIT_BIAYA']== "1",

      );
      // Reguler Survey
      _showMandatoryInfoStrukturKreditBiayaRegulerSurveyModel = ShowMandatoryInfoStrukturKreditBiayaModel(
        _dataHideShowAppRegulerSurvey[0]['jenis_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppRegulerSurvey[0]['biaya_TUNAI_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppRegulerSurvey[0]['biaya_KREDIT_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppRegulerSurvey[0]['tot_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jenis_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppRegulerSurvey[0]['biaya_TUNAI_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppRegulerSurvey[0]['biaya_KREDIT_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppRegulerSurvey[0]['tot_BIAYA_STRUKKREDIT_BIAYA']== "1",
      );
      _showMandatoryInfoStrukturKreditBiayaRegulerSurveyCompanyModel = ShowMandatoryInfoStrukturKreditBiayaModel(
        _dataHideShowAppRegulerSurveyCompany[0]['jenis_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['biaya_TUNAI_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['biaya_KREDIT_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['tot_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jenis_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['biaya_TUNAI_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['biaya_KREDIT_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['tot_BIAYA_STRUKKREDIT_BIAYA']== "1",
      );
      // PAC
      _showMandatoryInfoStrukturKreditBiayaPacModel = ShowMandatoryInfoStrukturKreditBiayaModel(
        _dataHideShowAppPac[0]['jenis_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppPac[0]['biaya_TUNAI_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppPac[0]['biaya_KREDIT_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppPac[0]['tot_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppPac[0]['jenis_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppPac[0]['biaya_TUNAI_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppPac[0]['biaya_KREDIT_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppPac[0]['tot_BIAYA_STRUKKREDIT_BIAYA']== "1",
      );
      _showMandatoryInfoStrukturKreditBiayaPacCompanyModel = ShowMandatoryInfoStrukturKreditBiayaModel(
        _dataHideShowAppPacCompany[0]['jenis_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppPacCompany[0]['biaya_TUNAI_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppPacCompany[0]['biaya_KREDIT_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppPacCompany[0]['tot_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppPacCompany[0]['jenis_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppPacCompany[0]['biaya_TUNAI_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppPacCompany[0]['biaya_KREDIT_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppPacCompany[0]['tot_BIAYA_STRUKKREDIT_BIAYA']== "1",
      );
      // Resurvey
      _showMandatoryInfoStrukturKreditBiayaResurveyModel = ShowMandatoryInfoStrukturKreditBiayaModel(
        _dataHideShowAppResurvey[0]['jenis_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppResurvey[0]['biaya_TUNAI_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppResurvey[0]['biaya_KREDIT_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppResurvey[0]['tot_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppResurvey[0]['jenis_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppResurvey[0]['biaya_TUNAI_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppResurvey[0]['biaya_KREDIT_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppResurvey[0]['tot_BIAYA_STRUKKREDIT_BIAYA']== "1",
      );
      _showMandatoryInfoStrukturKreditBiayaResurveyCompanyModel = ShowMandatoryInfoStrukturKreditBiayaModel(
        _dataHideShowAppResurveyCompany[0]['jenis_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppResurveyCompany[0]['biaya_TUNAI_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppResurveyCompany[0]['biaya_KREDIT_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppResurveyCompany[0]['tot_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppResurveyCompany[0]['jenis_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppResurveyCompany[0]['biaya_TUNAI_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppResurveyCompany[0]['biaya_KREDIT_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppResurveyCompany[0]['tot_BIAYA_STRUKKREDIT_BIAYA']== "1",
      );
      // Dakor
      _showMandatoryInfoStrukturKreditBiayaDakorModel = ShowMandatoryInfoStrukturKreditBiayaModel(
        _dataHideShowAppDakor[0]['jenis_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppDakor[0]['biaya_TUNAI_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppDakor[0]['biaya_KREDIT_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppDakor[0]['tot_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppDakor[0]['jenis_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppDakor[0]['biaya_TUNAI_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppDakor[0]['biaya_KREDIT_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppDakor[0]['tot_BIAYA_STRUKKREDIT_BIAYA']== "1",
      );
      _showMandatoryInfoStrukturKreditBiayaDakorCompanyModel = ShowMandatoryInfoStrukturKreditBiayaModel(
        _dataHideShowAppDakorCom[0]['jenis_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppDakorCom[0]['biaya_TUNAI_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppDakorCom[0]['biaya_KREDIT_STRUKKREDIT_BIAYA']== "1",
        _dataHideShowAppDakorCom[0]['tot_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppDakorCom[0]['jenis_BIAYA_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppDakorCom[0]['biaya_TUNAI_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppDakorCom[0]['biaya_KREDIT_STRUKKREDIT_BIAYA']== "1",
        _dataMandatoryAppDakorCom[0]['tot_BIAYA_STRUKKREDIT_BIAYA']== "1",
      );
      // End Inf Struktur Kredit Biaya
      // End Inf Struktur Kredit Biaya

      // Inf Struktur Kredit GP
      // IDE
      _showMandatoryInfoStrukturKreditGPIdeModel = ShowMandatoryInfoStrukturKreditGPModel(
        _dataHideShowAppIde[0]['jenis_GP_STRUKTUR_KREDIT_GP']== "1",
        _dataHideShowAppIde[0]['angsuran_KE_STRUKTUR_KREDIT_GP']== "1",
        _dataHideShowAppIde[0]['tenor_BARU_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppIde[0]['jenis_GP_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppIde[0]['angsuran_KE_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppIde[0]['tenor_BARU_STRUKTUR_KREDIT_GP']== "1",
      );
      _showMandatoryInfoStrukturKreditGPIdeCompanyModel = ShowMandatoryInfoStrukturKreditGPModel(
        _dataHideShowAppIdeCompany[0]['jenis_GP_STRUKTUR_KREDIT_GP']== "1",
        _dataHideShowAppIdeCompany[0]['angsuran_KE_STRUKTUR_KREDIT_GP']== "1",
        _dataHideShowAppIdeCompany[0]['tenor_BARU_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppIdeCompany[0]['jenis_GP_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppIdeCompany[0]['angsuran_KE_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppIdeCompany[0]['tenor_BARU_STRUKTUR_KREDIT_GP']== "1",

      );
      // Reguler Survey
      _showMandatoryInfoStrukturKreditGPRegulerSurveyModel = ShowMandatoryInfoStrukturKreditGPModel(
        _dataHideShowAppRegulerSurvey[0]['jenis_GP_STRUKTUR_KREDIT_GP']== "1",
        _dataHideShowAppRegulerSurvey[0]['angsuran_KE_STRUKTUR_KREDIT_GP']== "1",
        _dataHideShowAppRegulerSurvey[0]['tenor_BARU_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jenis_GP_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppRegulerSurvey[0]['angsuran_KE_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppRegulerSurvey[0]['tenor_BARU_STRUKTUR_KREDIT_GP']== "1",
      );
      _showMandatoryInfoStrukturKreditGPRegulerSurveyCompanyModel = ShowMandatoryInfoStrukturKreditGPModel(
        _dataHideShowAppRegulerSurveyCompany[0]['jenis_GP_STRUKTUR_KREDIT_GP']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['angsuran_KE_STRUKTUR_KREDIT_GP']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['tenor_BARU_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jenis_GP_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['angsuran_KE_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['tenor_BARU_STRUKTUR_KREDIT_GP']== "1",
      );
      // PAC
      _showMandatoryInfoStrukturKreditGPPacModel = ShowMandatoryInfoStrukturKreditGPModel(
        _dataHideShowAppPac[0]['jenis_GP_STRUKTUR_KREDIT_GP']== "1",
        _dataHideShowAppPac[0]['angsuran_KE_STRUKTUR_KREDIT_GP']== "1",
        _dataHideShowAppPac[0]['tenor_BARU_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppPac[0]['jenis_GP_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppPac[0]['angsuran_KE_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppPac[0]['tenor_BARU_STRUKTUR_KREDIT_GP']== "1",
      );
      _showMandatoryInfoStrukturKreditGPPacCompanyModel = ShowMandatoryInfoStrukturKreditGPModel(
        _dataHideShowAppPacCompany[0]['jenis_GP_STRUKTUR_KREDIT_GP']== "1",
        _dataHideShowAppPacCompany[0]['angsuran_KE_STRUKTUR_KREDIT_GP']== "1",
        _dataHideShowAppPacCompany[0]['tenor_BARU_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppPacCompany[0]['jenis_GP_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppPacCompany[0]['angsuran_KE_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppPacCompany[0]['tenor_BARU_STRUKTUR_KREDIT_GP']== "1",
      );
      // Resurvey
      _showMandatoryInfoStrukturKreditGPResurveyModel = ShowMandatoryInfoStrukturKreditGPModel(
        _dataHideShowAppResurvey[0]['jenis_GP_STRUKTUR_KREDIT_GP']== "1",
        _dataHideShowAppResurvey[0]['angsuran_KE_STRUKTUR_KREDIT_GP']== "1",
        _dataHideShowAppResurvey[0]['tenor_BARU_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppResurvey[0]['jenis_GP_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppResurvey[0]['angsuran_KE_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppResurvey[0]['tenor_BARU_STRUKTUR_KREDIT_GP']== "1",
      );
      _showMandatoryInfoStrukturKreditGPResurveyCompanyModel = ShowMandatoryInfoStrukturKreditGPModel(
        _dataHideShowAppResurveyCompany[0]['jenis_GP_STRUKTUR_KREDIT_GP']== "1",
        _dataHideShowAppResurveyCompany[0]['angsuran_KE_STRUKTUR_KREDIT_GP']== "1",
        _dataHideShowAppResurveyCompany[0]['tenor_BARU_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppResurveyCompany[0]['jenis_GP_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppResurveyCompany[0]['angsuran_KE_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppResurveyCompany[0]['tenor_BARU_STRUKTUR_KREDIT_GP']== "1",
      );
      // Dakor
      _showMandatoryInfoStrukturKreditGPDakorModel = ShowMandatoryInfoStrukturKreditGPModel(
        _dataHideShowAppDakor[0]['jenis_GP_STRUKTUR_KREDIT_GP']== "1",
        _dataHideShowAppDakor[0]['angsuran_KE_STRUKTUR_KREDIT_GP']== "1",
        _dataHideShowAppDakor[0]['tenor_BARU_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppDakor[0]['jenis_GP_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppDakor[0]['angsuran_KE_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppDakor[0]['tenor_BARU_STRUKTUR_KREDIT_GP']== "1",
      );
      _showMandatoryInfoStrukturKreditGPDakorCompanyModel = ShowMandatoryInfoStrukturKreditGPModel(
        _dataHideShowAppDakorCom[0]['jenis_GP_STRUKTUR_KREDIT_GP']== "1",
        _dataHideShowAppDakorCom[0]['angsuran_KE_STRUKTUR_KREDIT_GP']== "1",
        _dataHideShowAppDakorCom[0]['tenor_BARU_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppDakorCom[0]['jenis_GP_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppDakorCom[0]['angsuran_KE_STRUKTUR_KREDIT_GP']== "1",
        _dataMandatoryAppDakorCom[0]['tenor_BARU_STRUKTUR_KREDIT_GP']== "1",
      );
      // End Inf Struktur Kredit GP

      // Inf Struktur Kredit Stepping
      // IDE
      _showMandatoryInfoStrukturKreditSteppingIdeModel = ShowMandatoryInfoStrukturKreditSteppingModel(
        _dataHideShowAppIde[0]['jum_TEP_STRUKKREDIT_STEPPING']== "1",
        _dataHideShowAppIde[0]['presen_STRUKKREDIT_STEPPING']== "1",
        _dataHideShowAppIde[0]['nilai_STRUKTUR_KREDIT_STEPPING']== "1",
        _dataMandatoryAppIde[0]['jum_TEP_STRUKKREDIT_STEPPING']== "1",
        _dataMandatoryAppIde[0]['presen_STRUKKREDIT_STEPPING']== "1",
        _dataMandatoryAppIde[0]['nilai_STRUKTUR_KREDIT_STEPPING']== "1",
      );
      _showMandatoryInfoStrukturKreditSteppingIdeCompanyModel = ShowMandatoryInfoStrukturKreditSteppingModel(
        _dataHideShowAppIdeCompany[0]['jum_TEP_STRUKKREDIT_STEPPING']== "1",
        _dataHideShowAppIdeCompany[0]['presen_STRUKKREDIT_STEPPING']== "1",
        _dataHideShowAppIdeCompany[0]['nilai_STRUKTUR_KREDIT_STEPPING']== "1",
        _dataMandatoryAppIdeCompany[0]['jum_TEP_STRUKKREDIT_STEPPING']== "1",
        _dataMandatoryAppIdeCompany[0]['presen_STRUKKREDIT_STEPPING']== "1",
        _dataMandatoryAppIdeCompany[0]['nilai_STRUKTUR_KREDIT_STEPPING']== "1",

      );
      // Reguler Survey
      _showMandatoryInfoStrukturKreditSteppingRegulerSurveyModel = ShowMandatoryInfoStrukturKreditSteppingModel(
        _dataHideShowAppRegulerSurvey[0]['jum_TEP_STRUKKREDIT_STEPPING']== "1",
        _dataHideShowAppRegulerSurvey[0]['presen_STRUKKREDIT_STEPPING']== "1",
        _dataHideShowAppRegulerSurvey[0]['nilai_STRUKTUR_KREDIT_STEPPING']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jum_TEP_STRUKKREDIT_STEPPING']== "1",
        _dataMandatoryAppRegulerSurvey[0]['presen_STRUKKREDIT_STEPPING']== "1",
        _dataMandatoryAppRegulerSurvey[0]['nilai_STRUKTUR_KREDIT_STEPPING']== "1",
      );
      _showMandatoryInfoStrukturKreditSteppingRegulerSurveyCompanyModel = ShowMandatoryInfoStrukturKreditSteppingModel(
        _dataHideShowAppRegulerSurveyCompany[0]['jum_TEP_STRUKKREDIT_STEPPING']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['presen_STRUKKREDIT_STEPPING']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['nilai_STRUKTUR_KREDIT_STEPPING']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jum_TEP_STRUKKREDIT_STEPPING']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['presen_STRUKKREDIT_STEPPING']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['nilai_STRUKTUR_KREDIT_STEPPING']== "1",
      );
      // PAC
      _showMandatoryInfoStrukturKreditSteppingPacModel = ShowMandatoryInfoStrukturKreditSteppingModel(
        _dataHideShowAppPac[0]['jum_TEP_STRUKKREDIT_STEPPING']== "1",
        _dataHideShowAppPac[0]['presen_STRUKKREDIT_STEPPING']== "1",
        _dataHideShowAppPac[0]['nilai_STRUKTUR_KREDIT_STEPPING']== "1",
        _dataMandatoryAppPac[0]['jum_TEP_STRUKKREDIT_STEPPING']== "1",
        _dataMandatoryAppPac[0]['presen_STRUKKREDIT_STEPPING']== "1",
        _dataMandatoryAppPac[0]['nilai_STRUKTUR_KREDIT_STEPPING']== "1",
      );
      _showMandatoryInfoStrukturKreditSteppingPacCompanyModel = ShowMandatoryInfoStrukturKreditSteppingModel(
        _dataHideShowAppPacCompany[0]['jum_TEP_STRUKKREDIT_STEPPING']== "1",
        _dataHideShowAppPacCompany[0]['presen_STRUKKREDIT_STEPPING']== "1",
        _dataHideShowAppPacCompany[0]['nilai_STRUKTUR_KREDIT_STEPPING']== "1",
        _dataMandatoryAppPacCompany[0]['jum_TEP_STRUKKREDIT_STEPPING']== "1",
        _dataMandatoryAppPacCompany[0]['presen_STRUKKREDIT_STEPPING']== "1",
        _dataMandatoryAppPacCompany[0]['nilai_STRUKTUR_KREDIT_STEPPING']== "1",
      );
      // Resurvey
      _showMandatoryInfoStrukturKreditSteppingResurveyModel = ShowMandatoryInfoStrukturKreditSteppingModel(
        _dataHideShowAppResurvey[0]['jum_TEP_STRUKKREDIT_STEPPING']== "1",
        _dataHideShowAppResurvey[0]['presen_STRUKKREDIT_STEPPING']== "1",
        _dataHideShowAppResurvey[0]['nilai_STRUKTUR_KREDIT_STEPPING']== "1",
        _dataMandatoryAppResurvey[0]['jum_TEP_STRUKKREDIT_STEPPING']== "1",
        _dataMandatoryAppResurvey[0]['presen_STRUKKREDIT_STEPPING']== "1",
        _dataMandatoryAppResurvey[0]['nilai_STRUKTUR_KREDIT_STEPPING']== "1",
      );
      _showMandatoryInfoStrukturKreditSteppingResurveyCompanyModel = ShowMandatoryInfoStrukturKreditSteppingModel(
        _dataHideShowAppResurveyCompany[0]['jum_TEP_STRUKKREDIT_STEPPING']== "1",
        _dataHideShowAppResurveyCompany[0]['presen_STRUKKREDIT_STEPPING']== "1",
        _dataHideShowAppResurveyCompany[0]['nilai_STRUKTUR_KREDIT_STEPPING']== "1",
        _dataMandatoryAppResurveyCompany[0]['jum_TEP_STRUKKREDIT_STEPPING']== "1",
        _dataMandatoryAppResurveyCompany[0]['presen_STRUKKREDIT_STEPPING']== "1",
        _dataMandatoryAppResurveyCompany[0]['nilai_STRUKTUR_KREDIT_STEPPING']== "1",
      );
      // Dakor
      _showMandatoryInfoStrukturKreditSteppingDakorModel = ShowMandatoryInfoStrukturKreditSteppingModel(
        _dataHideShowAppDakor[0]['jum_TEP_STRUKKREDIT_STEPPING']== "1",
        _dataHideShowAppDakor[0]['presen_STRUKKREDIT_STEPPING']== "1",
        _dataHideShowAppDakor[0]['nilai_STRUKTUR_KREDIT_STEPPING']== "1",
        _dataMandatoryAppDakor[0]['jum_TEP_STRUKKREDIT_STEPPING']== "1",
        _dataMandatoryAppDakor[0]['presen_STRUKKREDIT_STEPPING']== "1",
        _dataMandatoryAppDakor[0]['nilai_STRUKTUR_KREDIT_STEPPING']== "1",
      );
      _showMandatoryInfoStrukturKreditSteppingDakorCompanyModel = ShowMandatoryInfoStrukturKreditSteppingModel(
        _dataHideShowAppDakorCom[0]['jum_TEP_STRUKKREDIT_STEPPING']== "1",
        _dataHideShowAppDakorCom[0]['presen_STRUKKREDIT_STEPPING']== "1",
        _dataHideShowAppDakorCom[0]['nilai_STRUKTUR_KREDIT_STEPPING']== "1",
        _dataMandatoryAppDakorCom[0]['jum_TEP_STRUKKREDIT_STEPPING']== "1",
        _dataMandatoryAppDakorCom[0]['presen_STRUKKREDIT_STEPPING']== "1",
        _dataMandatoryAppDakorCom[0]['nilai_STRUKTUR_KREDIT_STEPPING']== "1",
      );
      // End Inf Struktur Kredit Stepping

      // Inf Struktur Kredit Irreguler
      // IDE
      _showMandatoryInfoStrukturKreditIrregulerIdeModel = ShowMandatoryInfoStrukturKreditIrregulerModel(
        _dataHideShowAppIde[0]['nilaiangs_STRUKKREDIT_IRREG']== "1",
        _dataMandatoryAppIde[0]['nilaiangs_STRUKKREDIT_IRREG']== "1",
      );
      _showMandatoryInfoStrukturKreditIrregulerIdeCompanyModel = ShowMandatoryInfoStrukturKreditIrregulerModel(
        _dataHideShowAppIdeCompany[0]['nilaiangs_STRUKKREDIT_IRREG']== "1",
        _dataMandatoryAppIdeCompany[0]['nilaiangs_STRUKKREDIT_IRREG']== "1",

      );
      // Reguler Survey
      _showMandatoryInfoStrukturKreditIrregulerRegulerSurveyModel = ShowMandatoryInfoStrukturKreditIrregulerModel(
        _dataHideShowAppRegulerSurvey[0]['nilaiangs_STRUKKREDIT_IRREG']== "1",
        _dataMandatoryAppRegulerSurvey[0]['nilaiangs_STRUKKREDIT_IRREG']== "1",
      );
      _showMandatoryInfoStrukturKreditIrregulerRegulerSurveyCompanyModel = ShowMandatoryInfoStrukturKreditIrregulerModel(
        _dataHideShowAppRegulerSurveyCompany[0]['nilaiangs_STRUKKREDIT_IRREG']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['nilaiangs_STRUKKREDIT_IRREG']== "1",
      );
      // PAC
      _showMandatoryInfoStrukturKreditIrregulerPacModel = ShowMandatoryInfoStrukturKreditIrregulerModel(
        _dataHideShowAppPac[0]['nilaiangs_STRUKKREDIT_IRREG']== "1",
        _dataMandatoryAppPac[0]['nilaiangs_STRUKKREDIT_IRREG']== "1",
      );
      _showMandatoryInfoStrukturKreditIrregulerPacCompanyModel = ShowMandatoryInfoStrukturKreditIrregulerModel(
        _dataHideShowAppPacCompany[0]['nilaiangs_STRUKKREDIT_IRREG']== "1",
        _dataMandatoryAppPacCompany[0]['nilaiangs_STRUKKREDIT_IRREG']== "1",
      );
      // Resurvey
      _showMandatoryInfoStrukturKreditIrregulerResurveyModel = ShowMandatoryInfoStrukturKreditIrregulerModel(
        _dataHideShowAppResurvey[0]['nilaiangs_STRUKKREDIT_IRREG']== "1",
        _dataMandatoryAppResurvey[0]['nilaiangs_STRUKKREDIT_IRREG']== "1",
      );
      _showMandatoryInfoStrukturKreditIrregulerResurveyCompanyModel = ShowMandatoryInfoStrukturKreditIrregulerModel(
        _dataHideShowAppResurveyCompany[0]['nilaiangs_STRUKKREDIT_IRREG']== "1",
        _dataMandatoryAppResurveyCompany[0]['nilaiangs_STRUKKREDIT_IRREG']== "1",
      );
      // Dakor
      _showMandatoryInfoStrukturKreditIrregulerDakorModel = ShowMandatoryInfoStrukturKreditIrregulerModel(
        _dataHideShowAppDakor[0]['nilaiangs_STRUKKREDIT_IRREG']== "1",
        _dataMandatoryAppDakor[0]['nilaiangs_STRUKKREDIT_IRREG']== "1",
      );
      _showMandatoryInfoStrukturKreditIrregulerDakorCompanyModel = ShowMandatoryInfoStrukturKreditIrregulerModel(
        _dataHideShowAppDakorCom[0]['nilaiangs_STRUKKREDIT_IRREG']== "1",
        _dataMandatoryAppDakorCom[0]['nilaiangs_STRUKKREDIT_IRREG']== "1",
      );
      // End Inf Struktur Kredit Irreguler

      // Inf Struktur Kredit BP
      // IDE
      _showMandatoryInfoStrukturKreditBPIdeModel = ShowMandatoryInfoStrukturKreditBPModel(
        _dataHideShowAppIde[0]['balloon_BYPH_STRUKKREDIT_BP']== "1",
        _dataHideShowAppIde[0]['presball_LASTANG_STRUKKREDITBP']== "1",
        _dataHideShowAppIde[0]['nilai_BALL_BYPH_STRUKKREDIT_BP']== "1",
        _dataMandatoryAppIde[0]['balloon_BYPH_STRUKKREDIT_BP']== "1",
        _dataMandatoryAppIde[0]['presball_LASTANG_STRUKKREDITBP']== "1",
        _dataMandatoryAppIde[0]['nilai_BALL_BYPH_STRUKKREDIT_BP']== "1",
      );
      _showMandatoryInfoStrukturKreditBPIdeCompanyModel = ShowMandatoryInfoStrukturKreditBPModel(
        _dataHideShowAppIdeCompany[0]['balloon_BYPH_STRUKKREDIT_BP']== "1",
        _dataHideShowAppIdeCompany[0]['presball_LASTANG_STRUKKREDITBP']== "1",
        _dataHideShowAppIdeCompany[0]['nilai_BALL_BYPH_STRUKKREDIT_BP']== "1",
        _dataMandatoryAppIdeCompany[0]['balloon_BYPH_STRUKKREDIT_BP']== "1",
        _dataMandatoryAppIdeCompany[0]['presball_LASTANG_STRUKKREDITBP']== "1",
        _dataMandatoryAppIdeCompany[0]['nilai_BALL_BYPH_STRUKKREDIT_BP']== "1",

      );
      // Reguler Survey
      _showMandatoryInfoStrukturKreditBPRegulerSurveyModel = ShowMandatoryInfoStrukturKreditBPModel(
        _dataHideShowAppRegulerSurvey[0]['balloon_BYPH_STRUKKREDIT_BP']== "1",
        _dataHideShowAppRegulerSurvey[0]['presball_LASTANG_STRUKKREDITBP']== "1",
        _dataHideShowAppRegulerSurvey[0]['nilai_BALL_BYPH_STRUKKREDIT_BP']== "1",
        _dataMandatoryAppRegulerSurvey[0]['balloon_BYPH_STRUKKREDIT_BP']== "1",
        _dataMandatoryAppRegulerSurvey[0]['presball_LASTANG_STRUKKREDITBP']== "1",
        _dataMandatoryAppRegulerSurvey[0]['nilai_BALL_BYPH_STRUKKREDIT_BP']== "1",
      );
      _showMandatoryInfoStrukturKreditBPRegulerSurveyCompanyModel = ShowMandatoryInfoStrukturKreditBPModel(
        _dataHideShowAppRegulerSurveyCompany[0]['balloon_BYPH_STRUKKREDIT_BP']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['presball_LASTANG_STRUKKREDITBP']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['nilai_BALL_BYPH_STRUKKREDIT_BP']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['balloon_BYPH_STRUKKREDIT_BP']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['presball_LASTANG_STRUKKREDITBP']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['nilai_BALL_BYPH_STRUKKREDIT_BP']== "1",
      );
      // PAC
      _showMandatoryInfoStrukturKreditBPPacModel = ShowMandatoryInfoStrukturKreditBPModel(
        _dataHideShowAppPac[0]['balloon_BYPH_STRUKKREDIT_BP']== "1",
        _dataHideShowAppPac[0]['presball_LASTANG_STRUKKREDITBP']== "1",
        _dataHideShowAppPac[0]['nilai_BALL_BYPH_STRUKKREDIT_BP']== "1",
        _dataMandatoryAppPac[0]['balloon_BYPH_STRUKKREDIT_BP']== "1",
        _dataMandatoryAppPac[0]['presball_LASTANG_STRUKKREDITBP']== "1",
        _dataMandatoryAppPac[0]['nilai_BALL_BYPH_STRUKKREDIT_BP']== "1",
      );
      _showMandatoryInfoStrukturKreditBPPacCompanyModel = ShowMandatoryInfoStrukturKreditBPModel(
        _dataHideShowAppPacCompany[0]['balloon_BYPH_STRUKKREDIT_BP']== "1",
        _dataHideShowAppPacCompany[0]['presball_LASTANG_STRUKKREDITBP']== "1",
        _dataHideShowAppPacCompany[0]['nilai_BALL_BYPH_STRUKKREDIT_BP']== "1",
        _dataMandatoryAppPacCompany[0]['balloon_BYPH_STRUKKREDIT_BP']== "1",
        _dataMandatoryAppPacCompany[0]['presball_LASTANG_STRUKKREDITBP']== "1",
        _dataMandatoryAppPacCompany[0]['nilai_BALL_BYPH_STRUKKREDIT_BP']== "1",
      );
      // Resurvey
      _showMandatoryInfoStrukturKreditBPResurveyModel = ShowMandatoryInfoStrukturKreditBPModel(
        _dataHideShowAppResurvey[0]['balloon_BYPH_STRUKKREDIT_BP']== "1",
        _dataHideShowAppResurvey[0]['presball_LASTANG_STRUKKREDITBP']== "1",
        _dataHideShowAppResurvey[0]['nilai_BALL_BYPH_STRUKKREDIT_BP']== "1",
        _dataMandatoryAppResurvey[0]['balloon_BYPH_STRUKKREDIT_BP']== "1",
        _dataMandatoryAppResurvey[0]['presball_LASTANG_STRUKKREDITBP']== "1",
        _dataMandatoryAppResurvey[0]['nilai_BALL_BYPH_STRUKKREDIT_BP']== "1",
      );
      _showMandatoryInfoStrukturKreditBPResurveyCompanyModel = ShowMandatoryInfoStrukturKreditBPModel(
        _dataHideShowAppResurveyCompany[0]['balloon_BYPH_STRUKKREDIT_BP']== "1",
        _dataHideShowAppResurveyCompany[0]['presball_LASTANG_STRUKKREDITBP']== "1",
        _dataHideShowAppResurveyCompany[0]['nilai_BALL_BYPH_STRUKKREDIT_BP']== "1",
        _dataMandatoryAppResurveyCompany[0]['balloon_BYPH_STRUKKREDIT_BP']== "1",
        _dataMandatoryAppResurveyCompany[0]['presball_LASTANG_STRUKKREDITBP']== "1",
        _dataMandatoryAppResurveyCompany[0]['nilai_BALL_BYPH_STRUKKREDIT_BP']== "1",
      );
      // Dakor
      _showMandatoryInfoStrukturKreditBPDakorModel = ShowMandatoryInfoStrukturKreditBPModel(
        _dataHideShowAppDakor[0]['balloon_BYPH_STRUKKREDIT_BP']== "1",
        _dataHideShowAppDakor[0]['presball_LASTANG_STRUKKREDITBP']== "1",
        _dataHideShowAppDakor[0]['nilai_BALL_BYPH_STRUKKREDIT_BP']== "1",
        _dataMandatoryAppDakor[0]['balloon_BYPH_STRUKKREDIT_BP']== "1",
        _dataMandatoryAppDakor[0]['presball_LASTANG_STRUKKREDITBP']== "1",
        _dataMandatoryAppDakor[0]['nilai_BALL_BYPH_STRUKKREDIT_BP']== "1",
      );
      _showMandatoryInfoStrukturKreditBPDakorCompanyModel = ShowMandatoryInfoStrukturKreditBPModel(
        _dataHideShowAppDakorCom[0]['balloon_BYPH_STRUKKREDIT_BP']== "1",
        _dataHideShowAppDakorCom[0]['presball_LASTANG_STRUKKREDITBP']== "1",
        _dataHideShowAppDakorCom[0]['nilai_BALL_BYPH_STRUKKREDIT_BP']== "1",
        _dataMandatoryAppDakorCom[0]['balloon_BYPH_STRUKKREDIT_BP']== "1",
        _dataMandatoryAppDakorCom[0]['presball_LASTANG_STRUKKREDITBP']== "1",
        _dataMandatoryAppDakorCom[0]['nilai_BALL_BYPH_STRUKKREDIT_BP']== "1",
      );
      // End Inf Struktur Kredit BP

      // // Inf Asuransi Utama
      // setMajorInsuranceModel = SetMajorInsuranceModel(
      //   _dataHideShowAppIde[0]['tipe_ASURANSI_ASURANSI_UTAMA'] == "1",
      //   _dataHideShowAppIde[0]['jaminan_KE_ASURANSI_UTAMA'] == "1",
      //   _dataHideShowAppIde[0]['perusahaan_ASURANSI_UTAMA'] == "1",
      //   _dataHideShowAppIde[0]['produk_ASURANSI_UTAMA'] == "1",
      //   _dataHideShowAppIde[0]['periode_JENIS_ASURANSI_UTAMA'] == "1",
      //   _dataHideShowAppIde[0][''] == "1",
      //   _dataHideShowAppIde[0]['jenis_1_ASURANSI_UTAMA'] == "1",
      //   _dataHideShowAppIde[0][''] == "1",
      //   _dataHideShowAppIde[0]['jenis_PERTANGGUNGAN_ASR_UTAMA'] == "1",
      //   _dataHideShowAppIde[0]['niliai_PERTANGGUNGAN_ASR_UTAMA'] == "1",
      //   _dataHideShowAppIde[0]['batas_ATAS_ASURANSI_UTAMA'] == "1",
      //   _dataHideShowAppIde[0]['batas_BAWAH_ASURANSI_UTAMA']== "1",
      //   _dataHideShowAppIde[0]['batas_ATAS_RATE_ASURANSI_UTAMA'] == "1",
      //   _dataHideShowAppIde[0]['batas_BAWAH_RATE_ASR_UTAMA'] == "1",
      //   _dataHideShowAppIde[0]['biaya_TUNAI_ASURANSI_UTAMA'] == "1",
      //   _dataHideShowAppIde[0]['biaya_KREDIT_ASURANSI_UTAMA'] == "1",
      //   _dataHideShowAppIde[0][''] == "1",
      //   _dataHideShowAppIde[0][''] == "1",
      //   _dataHideShowAppIde[0]['total_BIAYA_ASURANSI_UTAMA'] == "1",
      //   _dataMandatoryAppIde[0]['tipe_ASURANSI_ASURANSI_UTAMA'] == "1",
      //   _dataMandatoryAppIde[0]['jaminan_KE_ASURANSI_UTAMA'] == "1",
      //   _dataMandatoryAppIde[0]['perusahaan_ASURANSI_UTAMA'] == "1",
      //   _dataMandatoryAppIde[0]['produk_ASURANSI_UTAMA'] == "1",
      //   _dataMandatoryAppIde[0]['periode_JENIS_ASURANSI_UTAMA'] == "1",
      //   _dataMandatoryAppIde[0][''] == "1",
      //   _dataMandatoryAppIde[0]['jenis_1_ASURANSI_UTAMA'] == "1",
      //   _dataMandatoryAppIde[0][''] == "1",
      //   _dataMandatoryAppIde[0]['jenis_PERTANGGUNGAN_ASR_UTAMA'] == "1",
      //   _dataMandatoryAppIde[0]['niliai_PERTANGGUNGAN_ASR_UTAMA'] == "1",
      //   _dataMandatoryAppIde[0]['batas_ATAS_ASURANSI_UTAMA'] == "1",
      //   _dataMandatoryAppIde[0]['batas_BAWAH_ASURANSI_UTAMA'] == "1",
      //   _dataMandatoryAppIde[0]['batas_ATAS_RATE_ASURANSI_UTAMA'] == "1",
      //   _dataMandatoryAppIde[0]['batas_BAWAH_RATE_ASR_UTAMA'] == "1",
      //   _dataMandatoryAppIde[0]['biaya_TUNAI_ASURANSI_UTAMA'] == "1",
      //   _dataMandatoryAppIde[0]['biaya_KREDIT_ASURANSI_UTAMA'] == "1",
      //   _dataMandatoryAppIde[0][''] == "1",
      //   _dataMandatoryAppIde[0][''] == "1",
      //   _dataMandatoryAppIde[0]['total_BIAYA_ASURANSI_UTAMA'] == "1",
      // );
      // // End Inf Asuransi Utama

      // Inf Asuransi Utama
      // IDE
      _showMandatoryInfoAsuransiUtamaIdeModel = SetMajorInsuranceModel(
        _dataHideShowAppIde[0]['tipe_ASURANSI_ASURANSI_UTAMA']== "1",
        _dataHideShowAppIde[0]['jaminan_KE_ASURANSI_UTAMA']== "1",
        _dataHideShowAppIde[0]['perusahaan_ASURANSI_UTAMA']== "1",
        _dataHideShowAppIde[0]['produk_ASURANSI_UTAMA']== "1",
        _dataHideShowAppIde[0]['periode_JENIS_ASURANSI_UTAMA']== "1",
        _dataHideShowAppIde[0]['jenis_1_ASURANSI_UTAMA']== "1",
        _dataHideShowAppIde[0]['coverage_1_ASURANSI_UTAMA']== "1",
        _dataHideShowAppIde[0]['coverage_2_ASURANSI_UTAMA']== "1",
        _dataHideShowAppIde[0]['jenis_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataHideShowAppIde[0]['niliai_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataHideShowAppIde[0]['batas_ATAS_ASURANSI_UTAMA']== "1",
        _dataHideShowAppIde[0]['batas_BAWAH_ASURANSI_UTAMA']== "1",
        _dataHideShowAppIde[0]['batas_ATAS_RATE_ASURANSI_UTAMA']== "1",
        _dataHideShowAppIde[0]['batas_BAWAH_RATE_ASR_UTAMA']== "1",
        _dataHideShowAppIde[0]['biaya_TUNAI_ASURANSI_UTAMA']== "1",
        _dataHideShowAppIde[0]['biaya_KREDIT_ASURANSI_UTAMA']== "1",
        _dataHideShowAppIde[0]['total_BIAYA_SPLIT_ASR_UTAMA']== "1",
        _dataHideShowAppIde[0]['total_BIAYA_ASURANSI_UTAMA']== "1",
        _dataHideShowAppIde[0]['total_BIAYA_RATE_ASR_UTAMA']== "1",
        _dataMandatoryAppIde[0]['tipe_ASURANSI_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppIde[0]['jaminan_KE_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppIde[0]['perusahaan_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppIde[0]['produk_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppIde[0]['periode_JENIS_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppIde[0]['jenis_1_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppIde[0]['coverage_1_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppIde[0]['coverage_2_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppIde[0]['jenis_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataMandatoryAppIde[0]['niliai_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataMandatoryAppIde[0]['batas_ATAS_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppIde[0]['batas_BAWAH_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppIde[0]['batas_ATAS_RATE_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppIde[0]['batas_BAWAH_RATE_ASR_UTAMA']== "1",
        _dataMandatoryAppIde[0]['biaya_TUNAI_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppIde[0]['biaya_KREDIT_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppIde[0]['total_BIAYA_SPLIT_ASR_UTAMA']== "1",
        _dataMandatoryAppIde[0]['total_BIAYA_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppIde[0]['total_BIAYA_RATE_ASR_UTAMA']== "1",
      );
      _showMandatoryInfoAsuransiUtamaIdeCompanyModel = SetMajorInsuranceModel(
        _dataHideShowAppIdeCompany[0]['tipe_ASURANSI_ASURANSI_UTAMA']== "1",
        _dataHideShowAppIdeCompany[0]['jaminan_KE_ASURANSI_UTAMA']== "1",
        _dataHideShowAppIdeCompany[0]['perusahaan_ASURANSI_UTAMA']== "1",
        _dataHideShowAppIdeCompany[0]['produk_ASURANSI_UTAMA']== "1",
        _dataHideShowAppIdeCompany[0]['periode_JENIS_ASURANSI_UTAMA']== "1",
        _dataHideShowAppIdeCompany[0]['jenis_1_ASURANSI_UTAMA']== "1",
        _dataHideShowAppIdeCompany[0]['coverage_1_ASURANSI_UTAMA']== "1",
        _dataHideShowAppIdeCompany[0]['coverage_2_ASURANSI_UTAMA']== "1",
        _dataHideShowAppIdeCompany[0]['jenis_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataHideShowAppIdeCompany[0]['niliai_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataHideShowAppIdeCompany[0]['batas_ATAS_ASURANSI_UTAMA']== "1",
        _dataHideShowAppIdeCompany[0]['batas_BAWAH_ASURANSI_UTAMA']== "1",
        _dataHideShowAppIdeCompany[0]['batas_ATAS_RATE_ASURANSI_UTAMA']== "1",
        _dataHideShowAppIdeCompany[0]['batas_BAWAH_RATE_ASR_UTAMA']== "1",
        _dataHideShowAppIdeCompany[0]['biaya_TUNAI_ASURANSI_UTAMA']== "1",
        _dataHideShowAppIdeCompany[0]['biaya_KREDIT_ASURANSI_UTAMA']== "1",
        _dataHideShowAppIdeCompany[0]['total_BIAYA_SPLIT_ASR_UTAMA']== "1",
        _dataHideShowAppIdeCompany[0]['total_BIAYA_ASURANSI_UTAMA']== "1",
        _dataHideShowAppIdeCompany[0]['total_BIAYA_RATE_ASR_UTAMA']== "1",
        _dataMandatoryAppIdeCompany[0]['tipe_ASURANSI_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppIdeCompany[0]['jaminan_KE_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppIdeCompany[0]['perusahaan_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppIdeCompany[0]['produk_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppIdeCompany[0]['periode_JENIS_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppIdeCompany[0]['jenis_1_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppIdeCompany[0]['coverage_1_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppIdeCompany[0]['coverage_2_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppIdeCompany[0]['jenis_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataMandatoryAppIdeCompany[0]['niliai_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataMandatoryAppIdeCompany[0]['batas_ATAS_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppIdeCompany[0]['batas_BAWAH_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppIdeCompany[0]['batas_ATAS_RATE_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppIdeCompany[0]['batas_BAWAH_RATE_ASR_UTAMA']== "1",
        _dataMandatoryAppIdeCompany[0]['biaya_TUNAI_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppIdeCompany[0]['biaya_KREDIT_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppIdeCompany[0]['total_BIAYA_SPLIT_ASR_UTAMA']== "1",
        _dataMandatoryAppIdeCompany[0]['total_BIAYA_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppIdeCompany[0]['total_BIAYA_RATE_ASR_UTAMA']== "1",
      );
      // Reguler Survey
      _showMandatoryInfoAsuransiUtamaRegulerSurveyModel = SetMajorInsuranceModel(
        _dataHideShowAppRegulerSurvey[0]['tipe_ASURANSI_ASURANSI_UTAMA']== "1",
        _dataHideShowAppRegulerSurvey[0]['jaminan_KE_ASURANSI_UTAMA']== "1",
        _dataHideShowAppRegulerSurvey[0]['perusahaan_ASURANSI_UTAMA']== "1",
        _dataHideShowAppRegulerSurvey[0]['produk_ASURANSI_UTAMA']== "1",
        _dataHideShowAppRegulerSurvey[0]['periode_JENIS_ASURANSI_UTAMA']== "1",
        _dataHideShowAppRegulerSurvey[0]['jenis_1_ASURANSI_UTAMA']== "1",
        _dataHideShowAppRegulerSurvey[0]['coverage_1_ASURANSI_UTAMA']== "1",
        _dataHideShowAppRegulerSurvey[0]['coverage_2_ASURANSI_UTAMA']== "1",
        _dataHideShowAppRegulerSurvey[0]['jenis_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataHideShowAppRegulerSurvey[0]['niliai_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataHideShowAppRegulerSurvey[0]['batas_ATAS_ASURANSI_UTAMA']== "1",
        _dataHideShowAppRegulerSurvey[0]['batas_BAWAH_ASURANSI_UTAMA']== "1",
        _dataHideShowAppRegulerSurvey[0]['batas_ATAS_RATE_ASURANSI_UTAMA']== "1",
        _dataHideShowAppRegulerSurvey[0]['batas_BAWAH_RATE_ASR_UTAMA']== "1",
        _dataHideShowAppRegulerSurvey[0]['biaya_TUNAI_ASURANSI_UTAMA']== "1",
        _dataHideShowAppRegulerSurvey[0]['biaya_KREDIT_ASURANSI_UTAMA']== "1",
        _dataHideShowAppRegulerSurvey[0]['total_BIAYA_SPLIT_ASR_UTAMA']== "1",
        _dataHideShowAppRegulerSurvey[0]['total_BIAYA_ASURANSI_UTAMA']== "1",
        _dataHideShowAppRegulerSurvey[0]['total_BIAYA_RATE_ASR_UTAMA']== "1",
        _dataMandatoryAppRegulerSurvey[0]['tipe_ASURANSI_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jaminan_KE_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppRegulerSurvey[0]['perusahaan_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppRegulerSurvey[0]['produk_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppRegulerSurvey[0]['periode_JENIS_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jenis_1_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppRegulerSurvey[0]['coverage_1_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppRegulerSurvey[0]['coverage_2_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jenis_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataMandatoryAppRegulerSurvey[0]['niliai_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataMandatoryAppRegulerSurvey[0]['batas_ATAS_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppRegulerSurvey[0]['batas_BAWAH_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppRegulerSurvey[0]['batas_ATAS_RATE_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppRegulerSurvey[0]['batas_BAWAH_RATE_ASR_UTAMA']== "1",
        _dataMandatoryAppRegulerSurvey[0]['biaya_TUNAI_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppRegulerSurvey[0]['biaya_KREDIT_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppRegulerSurvey[0]['total_BIAYA_SPLIT_ASR_UTAMA']== "1",
        _dataMandatoryAppRegulerSurvey[0]['total_BIAYA_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppRegulerSurvey[0]['total_BIAYA_RATE_ASR_UTAMA']== "1",
      );
      _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel = SetMajorInsuranceModel(
        _dataHideShowAppRegulerSurveyCompany[0]['tipe_ASURANSI_ASURANSI_UTAMA']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jaminan_KE_ASURANSI_UTAMA']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['perusahaan_ASURANSI_UTAMA']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['produk_ASURANSI_UTAMA']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['periode_JENIS_ASURANSI_UTAMA']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jenis_1_ASURANSI_UTAMA']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['coverage_1_ASURANSI_UTAMA']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['coverage_2_ASURANSI_UTAMA']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jenis_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['niliai_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['batas_ATAS_ASURANSI_UTAMA']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['batas_BAWAH_ASURANSI_UTAMA']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['batas_ATAS_RATE_ASURANSI_UTAMA']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['batas_BAWAH_RATE_ASR_UTAMA']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['biaya_TUNAI_ASURANSI_UTAMA']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['biaya_KREDIT_ASURANSI_UTAMA']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['total_BIAYA_SPLIT_ASR_UTAMA']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['total_BIAYA_ASURANSI_UTAMA']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['total_BIAYA_RATE_ASR_UTAMA']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['tipe_ASURANSI_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jaminan_KE_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['perusahaan_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['produk_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['periode_JENIS_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jenis_1_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['coverage_1_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['coverage_2_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jenis_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['niliai_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['batas_ATAS_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['batas_BAWAH_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['batas_ATAS_RATE_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['batas_BAWAH_RATE_ASR_UTAMA']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['biaya_TUNAI_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['biaya_KREDIT_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['total_BIAYA_SPLIT_ASR_UTAMA']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['total_BIAYA_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['total_BIAYA_RATE_ASR_UTAMA']== "1",
      );
      // PAC
      _showMandatoryInfoAsuransiUtamaPacModel = SetMajorInsuranceModel(
        _dataHideShowAppPac[0]['tipe_ASURANSI_ASURANSI_UTAMA']== "1",
        _dataHideShowAppPac[0]['jaminan_KE_ASURANSI_UTAMA']== "1",
        _dataHideShowAppPac[0]['perusahaan_ASURANSI_UTAMA']== "1",
        _dataHideShowAppPac[0]['produk_ASURANSI_UTAMA']== "1",
        _dataHideShowAppPac[0]['periode_JENIS_ASURANSI_UTAMA']== "1",
        _dataHideShowAppPac[0]['jenis_1_ASURANSI_UTAMA']== "1",
        _dataHideShowAppPac[0]['coverage_1_ASURANSI_UTAMA']== "1",
        _dataHideShowAppPac[0]['coverage_2_ASURANSI_UTAMA']== "1",
        _dataHideShowAppPac[0]['jenis_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataHideShowAppPac[0]['niliai_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataHideShowAppPac[0]['batas_ATAS_ASURANSI_UTAMA']== "1",
        _dataHideShowAppPac[0]['batas_BAWAH_ASURANSI_UTAMA']== "1",
        _dataHideShowAppPac[0]['batas_ATAS_RATE_ASURANSI_UTAMA']== "1",
        _dataHideShowAppPac[0]['batas_BAWAH_RATE_ASR_UTAMA']== "1",
        _dataHideShowAppPac[0]['biaya_TUNAI_ASURANSI_UTAMA']== "1",
        _dataHideShowAppPac[0]['biaya_KREDIT_ASURANSI_UTAMA']== "1",
        _dataHideShowAppPac[0]['total_BIAYA_SPLIT_ASR_UTAMA']== "1",
        _dataHideShowAppPac[0]['total_BIAYA_ASURANSI_UTAMA']== "1",
        _dataHideShowAppPac[0]['total_BIAYA_RATE_ASR_UTAMA']== "1",
        _dataMandatoryAppPac[0]['tipe_ASURANSI_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppPac[0]['jaminan_KE_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppPac[0]['perusahaan_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppPac[0]['produk_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppPac[0]['periode_JENIS_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppPac[0]['jenis_1_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppPac[0]['coverage_1_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppPac[0]['coverage_2_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppPac[0]['jenis_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataMandatoryAppPac[0]['niliai_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataMandatoryAppPac[0]['batas_ATAS_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppPac[0]['batas_BAWAH_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppPac[0]['batas_ATAS_RATE_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppPac[0]['batas_BAWAH_RATE_ASR_UTAMA']== "1",
        _dataMandatoryAppPac[0]['biaya_TUNAI_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppPac[0]['biaya_KREDIT_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppPac[0]['total_BIAYA_SPLIT_ASR_UTAMA']== "1",
        _dataMandatoryAppPac[0]['total_BIAYA_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppPac[0]['total_BIAYA_RATE_ASR_UTAMA']== "1",
      );
      _showMandatoryInfoAsuransiUtamaPacCompanyModel = SetMajorInsuranceModel(
        _dataHideShowAppPacCompany[0]['tipe_ASURANSI_ASURANSI_UTAMA']== "1",
        _dataHideShowAppPacCompany[0]['jaminan_KE_ASURANSI_UTAMA']== "1",
        _dataHideShowAppPacCompany[0]['perusahaan_ASURANSI_UTAMA']== "1",
        _dataHideShowAppPacCompany[0]['produk_ASURANSI_UTAMA']== "1",
        _dataHideShowAppPacCompany[0]['periode_JENIS_ASURANSI_UTAMA']== "1",
        _dataHideShowAppPacCompany[0]['jenis_1_ASURANSI_UTAMA']== "1",
        _dataHideShowAppPacCompany[0]['coverage_1_ASURANSI_UTAMA']== "1",
        _dataHideShowAppPacCompany[0]['coverage_2_ASURANSI_UTAMA']== "1",
        _dataHideShowAppPacCompany[0]['jenis_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataHideShowAppPacCompany[0]['niliai_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataHideShowAppPacCompany[0]['batas_ATAS_ASURANSI_UTAMA']== "1",
        _dataHideShowAppPacCompany[0]['batas_BAWAH_ASURANSI_UTAMA']== "1",
        _dataHideShowAppPacCompany[0]['batas_ATAS_RATE_ASURANSI_UTAMA']== "1",
        _dataHideShowAppPacCompany[0]['batas_BAWAH_RATE_ASR_UTAMA']== "1",
        _dataHideShowAppPacCompany[0]['biaya_TUNAI_ASURANSI_UTAMA']== "1",
        _dataHideShowAppPacCompany[0]['biaya_KREDIT_ASURANSI_UTAMA']== "1",
        _dataHideShowAppPacCompany[0]['total_BIAYA_SPLIT_ASR_UTAMA']== "1",
        _dataHideShowAppPacCompany[0]['total_BIAYA_ASURANSI_UTAMA']== "1",
        _dataHideShowAppPacCompany[0]['total_BIAYA_RATE_ASR_UTAMA']== "1",
        _dataMandatoryAppPacCompany[0]['tipe_ASURANSI_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppPacCompany[0]['jaminan_KE_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppPacCompany[0]['perusahaan_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppPacCompany[0]['produk_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppPacCompany[0]['periode_JENIS_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppPacCompany[0]['jenis_1_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppPacCompany[0]['coverage_1_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppPacCompany[0]['coverage_2_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppPacCompany[0]['jenis_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataMandatoryAppPacCompany[0]['niliai_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataMandatoryAppPacCompany[0]['batas_ATAS_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppPacCompany[0]['batas_BAWAH_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppPacCompany[0]['batas_ATAS_RATE_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppPacCompany[0]['batas_BAWAH_RATE_ASR_UTAMA']== "1",
        _dataMandatoryAppPacCompany[0]['biaya_TUNAI_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppPacCompany[0]['biaya_KREDIT_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppPacCompany[0]['total_BIAYA_SPLIT_ASR_UTAMA']== "1",
        _dataMandatoryAppPacCompany[0]['total_BIAYA_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppPacCompany[0]['total_BIAYA_RATE_ASR_UTAMA']== "1",
      );
      // Resurvey
      _showMandatoryInfoAsuransiUtamaResurveyModel = SetMajorInsuranceModel(
        _dataHideShowAppResurvey[0]['tipe_ASURANSI_ASURANSI_UTAMA']== "1",
        _dataHideShowAppResurvey[0]['jaminan_KE_ASURANSI_UTAMA']== "1",
        _dataHideShowAppResurvey[0]['perusahaan_ASURANSI_UTAMA']== "1",
        _dataHideShowAppResurvey[0]['produk_ASURANSI_UTAMA']== "1",
        _dataHideShowAppResurvey[0]['periode_JENIS_ASURANSI_UTAMA']== "1",
        _dataHideShowAppResurvey[0]['jenis_1_ASURANSI_UTAMA']== "1",
        _dataHideShowAppResurvey[0]['coverage_1_ASURANSI_UTAMA']== "1",
        _dataHideShowAppResurvey[0]['coverage_2_ASURANSI_UTAMA']== "1",
        _dataHideShowAppResurvey[0]['jenis_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataHideShowAppResurvey[0]['niliai_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataHideShowAppResurvey[0]['batas_ATAS_ASURANSI_UTAMA']== "1",
        _dataHideShowAppResurvey[0]['batas_BAWAH_ASURANSI_UTAMA']== "1",
        _dataHideShowAppResurvey[0]['batas_ATAS_RATE_ASURANSI_UTAMA']== "1",
        _dataHideShowAppResurvey[0]['batas_BAWAH_RATE_ASR_UTAMA']== "1",
        _dataHideShowAppResurvey[0]['biaya_TUNAI_ASURANSI_UTAMA']== "1",
        _dataHideShowAppResurvey[0]['biaya_KREDIT_ASURANSI_UTAMA']== "1",
        _dataHideShowAppResurvey[0]['total_BIAYA_SPLIT_ASR_UTAMA']== "1",
        _dataHideShowAppResurvey[0]['total_BIAYA_ASURANSI_UTAMA']== "1",
        _dataHideShowAppResurvey[0]['total_BIAYA_RATE_ASR_UTAMA']== "1",
        _dataMandatoryAppResurvey[0]['tipe_ASURANSI_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppResurvey[0]['jaminan_KE_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppResurvey[0]['perusahaan_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppResurvey[0]['produk_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppResurvey[0]['periode_JENIS_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppResurvey[0]['jenis_1_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppResurvey[0]['coverage_1_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppResurvey[0]['coverage_2_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppResurvey[0]['jenis_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataMandatoryAppResurvey[0]['niliai_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataMandatoryAppResurvey[0]['batas_ATAS_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppResurvey[0]['batas_BAWAH_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppResurvey[0]['batas_ATAS_RATE_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppResurvey[0]['batas_BAWAH_RATE_ASR_UTAMA']== "1",
        _dataMandatoryAppResurvey[0]['biaya_TUNAI_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppResurvey[0]['biaya_KREDIT_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppResurvey[0]['total_BIAYA_SPLIT_ASR_UTAMA']== "1",
        _dataMandatoryAppResurvey[0]['total_BIAYA_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppResurvey[0]['total_BIAYA_RATE_ASR_UTAMA']== "1",
      );
      _showMandatoryInfoAsuransiUtamaResurveyCompanyModel = SetMajorInsuranceModel(
        _dataHideShowAppResurveyCompany[0]['tipe_ASURANSI_ASURANSI_UTAMA']== "1",
        _dataHideShowAppResurveyCompany[0]['jaminan_KE_ASURANSI_UTAMA']== "1",
        _dataHideShowAppResurveyCompany[0]['perusahaan_ASURANSI_UTAMA']== "1",
        _dataHideShowAppResurveyCompany[0]['produk_ASURANSI_UTAMA']== "1",
        _dataHideShowAppResurveyCompany[0]['periode_JENIS_ASURANSI_UTAMA']== "1",
        _dataHideShowAppResurveyCompany[0]['jenis_1_ASURANSI_UTAMA']== "1",
        _dataHideShowAppResurveyCompany[0]['coverage_1_ASURANSI_UTAMA']== "1",
        _dataHideShowAppResurveyCompany[0]['coverage_2_ASURANSI_UTAMA']== "1",
        _dataHideShowAppResurveyCompany[0]['jenis_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataHideShowAppResurveyCompany[0]['niliai_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataHideShowAppResurveyCompany[0]['batas_ATAS_ASURANSI_UTAMA']== "1",
        _dataHideShowAppResurveyCompany[0]['batas_BAWAH_ASURANSI_UTAMA']== "1",
        _dataHideShowAppResurveyCompany[0]['batas_ATAS_RATE_ASURANSI_UTAMA']== "1",
        _dataHideShowAppResurveyCompany[0]['batas_BAWAH_RATE_ASR_UTAMA']== "1",
        _dataHideShowAppResurveyCompany[0]['biaya_TUNAI_ASURANSI_UTAMA']== "1",
        _dataHideShowAppResurveyCompany[0]['biaya_KREDIT_ASURANSI_UTAMA']== "1",
        _dataHideShowAppResurveyCompany[0]['total_BIAYA_SPLIT_ASR_UTAMA']== "1",
        _dataHideShowAppResurveyCompany[0]['total_BIAYA_ASURANSI_UTAMA']== "1",
        _dataHideShowAppResurveyCompany[0]['total_BIAYA_RATE_ASR_UTAMA']== "1",
        _dataMandatoryAppResurveyCompany[0]['tipe_ASURANSI_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppResurveyCompany[0]['jaminan_KE_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppResurveyCompany[0]['perusahaan_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppResurveyCompany[0]['produk_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppResurveyCompany[0]['periode_JENIS_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppResurveyCompany[0]['jenis_1_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppResurveyCompany[0]['coverage_1_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppResurveyCompany[0]['coverage_2_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppResurveyCompany[0]['jenis_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataMandatoryAppResurveyCompany[0]['niliai_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataMandatoryAppResurveyCompany[0]['batas_ATAS_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppResurveyCompany[0]['batas_BAWAH_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppResurveyCompany[0]['batas_ATAS_RATE_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppResurveyCompany[0]['batas_BAWAH_RATE_ASR_UTAMA']== "1",
        _dataMandatoryAppResurveyCompany[0]['biaya_TUNAI_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppResurveyCompany[0]['biaya_KREDIT_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppResurveyCompany[0]['total_BIAYA_SPLIT_ASR_UTAMA']== "1",
        _dataMandatoryAppResurveyCompany[0]['total_BIAYA_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppResurveyCompany[0]['total_BIAYA_RATE_ASR_UTAMA']== "1",
      );
      // Dakor
      _showMandatoryInfoAsuransiUtamaDakorModel = SetMajorInsuranceModel(
        _dataHideShowAppDakor[0]['tipe_ASURANSI_ASURANSI_UTAMA']== "1",
        _dataHideShowAppDakor[0]['jaminan_KE_ASURANSI_UTAMA']== "1",
        _dataHideShowAppDakor[0]['perusahaan_ASURANSI_UTAMA']== "1",
        _dataHideShowAppDakor[0]['produk_ASURANSI_UTAMA']== "1",
        _dataHideShowAppDakor[0]['periode_JENIS_ASURANSI_UTAMA']== "1",
        _dataHideShowAppDakor[0]['jenis_1_ASURANSI_UTAMA']== "1",
        _dataHideShowAppDakor[0]['coverage_1_ASURANSI_UTAMA']== "1",
        _dataHideShowAppDakor[0]['coverage_2_ASURANSI_UTAMA']== "1",
        _dataHideShowAppDakor[0]['jenis_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataHideShowAppDakor[0]['niliai_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataHideShowAppDakor[0]['batas_ATAS_ASURANSI_UTAMA']== "1",
        _dataHideShowAppDakor[0]['batas_BAWAH_ASURANSI_UTAMA']== "1",
        _dataHideShowAppDakor[0]['batas_ATAS_RATE_ASURANSI_UTAMA']== "1",
        _dataHideShowAppDakor[0]['batas_BAWAH_RATE_ASR_UTAMA']== "1",
        _dataHideShowAppDakor[0]['biaya_TUNAI_ASURANSI_UTAMA']== "1",
        _dataHideShowAppDakor[0]['biaya_KREDIT_ASURANSI_UTAMA']== "1",
        _dataHideShowAppDakor[0]['total_BIAYA_SPLIT_ASR_UTAMA']== "1",
        _dataHideShowAppDakor[0]['total_BIAYA_ASURANSI_UTAMA']== "1",
        _dataHideShowAppDakor[0]['total_BIAYA_RATE_ASR_UTAMA']== "1",
        _dataMandatoryAppDakor[0]['tipe_ASURANSI_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppDakor[0]['jaminan_KE_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppDakor[0]['perusahaan_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppDakor[0]['produk_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppDakor[0]['periode_JENIS_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppDakor[0]['jenis_1_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppDakor[0]['coverage_1_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppDakor[0]['coverage_2_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppDakor[0]['jenis_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataMandatoryAppDakor[0]['niliai_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataMandatoryAppDakor[0]['batas_ATAS_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppDakor[0]['batas_BAWAH_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppDakor[0]['batas_ATAS_RATE_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppDakor[0]['batas_BAWAH_RATE_ASR_UTAMA']== "1",
        _dataMandatoryAppDakor[0]['biaya_TUNAI_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppDakor[0]['biaya_KREDIT_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppDakor[0]['total_BIAYA_SPLIT_ASR_UTAMA']== "1",
        _dataMandatoryAppDakor[0]['total_BIAYA_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppDakor[0]['total_BIAYA_RATE_ASR_UTAMA']== "1",
      );
      _showMandatoryInfoAsuransiUtamaDakorCompanyModel = SetMajorInsuranceModel(
        _dataHideShowAppDakorCom[0]['tipe_ASURANSI_ASURANSI_UTAMA']== "1",
        _dataHideShowAppDakorCom[0]['jaminan_KE_ASURANSI_UTAMA']== "1",
        _dataHideShowAppDakorCom[0]['perusahaan_ASURANSI_UTAMA']== "1",
        _dataHideShowAppDakorCom[0]['produk_ASURANSI_UTAMA']== "1",
        _dataHideShowAppDakorCom[0]['periode_JENIS_ASURANSI_UTAMA']== "1",
        _dataHideShowAppDakorCom[0]['jenis_1_ASURANSI_UTAMA']== "1",
        _dataHideShowAppDakorCom[0]['coverage_1_ASURANSI_UTAMA']== "1",
        _dataHideShowAppDakorCom[0]['coverage_2_ASURANSI_UTAMA']== "1",
        _dataHideShowAppDakorCom[0]['jenis_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataHideShowAppDakorCom[0]['niliai_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataHideShowAppDakorCom[0]['batas_ATAS_ASURANSI_UTAMA']== "1",
        _dataHideShowAppDakorCom[0]['batas_BAWAH_ASURANSI_UTAMA']== "1",
        _dataHideShowAppDakorCom[0]['batas_ATAS_RATE_ASURANSI_UTAMA']== "1",
        _dataHideShowAppDakorCom[0]['batas_BAWAH_RATE_ASR_UTAMA']== "1",
        _dataHideShowAppDakorCom[0]['biaya_TUNAI_ASURANSI_UTAMA']== "1",
        _dataHideShowAppDakorCom[0]['biaya_KREDIT_ASURANSI_UTAMA']== "1",
        _dataHideShowAppDakorCom[0]['total_BIAYA_SPLIT_ASR_UTAMA']== "1",
        _dataHideShowAppDakorCom[0]['total_BIAYA_ASURANSI_UTAMA']== "1",
        _dataHideShowAppDakorCom[0]['total_BIAYA_RATE_ASR_UTAMA']== "1",
        _dataMandatoryAppDakorCom[0]['tipe_ASURANSI_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppDakorCom[0]['jaminan_KE_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppDakorCom[0]['perusahaan_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppDakorCom[0]['produk_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppDakorCom[0]['periode_JENIS_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppDakorCom[0]['jenis_1_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppDakorCom[0]['coverage_1_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppDakorCom[0]['coverage_2_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppDakorCom[0]['jenis_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataMandatoryAppDakorCom[0]['niliai_PERTANGGUNGAN_ASR_UTAMA']== "1",
        _dataMandatoryAppDakorCom[0]['batas_ATAS_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppDakorCom[0]['batas_BAWAH_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppDakorCom[0]['batas_ATAS_RATE_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppDakorCom[0]['batas_BAWAH_RATE_ASR_UTAMA']== "1",
        _dataMandatoryAppDakorCom[0]['biaya_TUNAI_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppDakorCom[0]['biaya_KREDIT_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppDakorCom[0]['total_BIAYA_SPLIT_ASR_UTAMA']== "1",
        _dataMandatoryAppDakorCom[0]['total_BIAYA_ASURANSI_UTAMA']== "1",
        _dataMandatoryAppDakorCom[0]['total_BIAYA_RATE_ASR_UTAMA']== "1",
      );
      // End Inf Asuransi Utama

      // Inf Asuransi Tambahan
      // IDE
      _showMandatoryInfoAsuransiTambahanIdeModel = ShowMandatoryInfoAsuransiTambahanModel(
        _dataHideShowAppIde[0]['tipe_ASR_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppIde[0]['perusahaan_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppIde[0]['produk_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppIde[0]['periode_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppIde[0]['jns_PERTANGGUNGAN_ASR_TAMBAHAN']== "1",
        _dataHideShowAppIde[0]['nilaipertanggungan_ASRTAMBAHAN']== "1",
        _dataHideShowAppIde[0]['batas_ATAS_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppIde[0]['batas_BAWAH_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppIde[0]['batas_ATAS_RATE_ASR_TAMBAHAN']== "1",
        _dataHideShowAppIde[0]['batas_BAWAH_RATE_ASR_TAMBAHAN']== "1",
        _dataHideShowAppIde[0]['biaya_TUNAI_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppIde[0]['biaya_KREDIT_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppIde[0]['total_BIAYA_RATE_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppIde[0]['total_BIAYA_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppIde[0]['tipe_ASR_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppIde[0]['perusahaan_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppIde[0]['produk_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppIde[0]['periode_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppIde[0]['jns_PERTANGGUNGAN_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppIde[0]['nilaipertanggungan_ASRTAMBAHAN']== "1",
        _dataMandatoryAppIde[0]['batas_ATAS_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppIde[0]['batas_BAWAH_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppIde[0]['batas_ATAS_RATE_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppIde[0]['batas_BAWAH_RATE_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppIde[0]['biaya_TUNAI_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppIde[0]['biaya_KREDIT_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppIde[0]['total_BIAYA_RATE_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppIde[0]['total_BIAYA_ASURANSI_TAMBAHAN']== "1",
      );
      _showMandatoryInfoAsuransiTambahanIdeCompanyModel = ShowMandatoryInfoAsuransiTambahanModel(
        _dataHideShowAppIdeCompany[0]['tipe_ASR_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppIdeCompany[0]['perusahaan_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppIdeCompany[0]['produk_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppIdeCompany[0]['periode_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppIdeCompany[0]['jns_PERTANGGUNGAN_ASR_TAMBAHAN']== "1",
        _dataHideShowAppIdeCompany[0]['nilaipertanggungan_ASRTAMBAHAN']== "1",
        _dataHideShowAppIdeCompany[0]['batas_ATAS_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppIdeCompany[0]['batas_BAWAH_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppIdeCompany[0]['batas_ATAS_RATE_ASR_TAMBAHAN']== "1",
        _dataHideShowAppIdeCompany[0]['batas_BAWAH_RATE_ASR_TAMBAHAN']== "1",
        _dataHideShowAppIdeCompany[0]['biaya_TUNAI_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppIdeCompany[0]['biaya_KREDIT_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppIdeCompany[0]['total_BIAYA_RATE_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppIdeCompany[0]['total_BIAYA_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppIdeCompany[0]['tipe_ASR_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppIdeCompany[0]['perusahaan_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppIdeCompany[0]['produk_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppIdeCompany[0]['periode_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppIdeCompany[0]['jns_PERTANGGUNGAN_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppIdeCompany[0]['nilaipertanggungan_ASRTAMBAHAN']== "1",
        _dataMandatoryAppIdeCompany[0]['batas_ATAS_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppIdeCompany[0]['batas_BAWAH_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppIdeCompany[0]['batas_ATAS_RATE_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppIdeCompany[0]['batas_BAWAH_RATE_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppIdeCompany[0]['biaya_TUNAI_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppIdeCompany[0]['biaya_KREDIT_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppIdeCompany[0]['total_BIAYA_RATE_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppIdeCompany[0]['total_BIAYA_ASURANSI_TAMBAHAN']== "1",
      );
      // Reguler Survey
      _showMandatoryInfoAsuransiTambahanRegulerSurveyModel = ShowMandatoryInfoAsuransiTambahanModel(
        _dataHideShowAppRegulerSurvey[0]['tipe_ASR_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppRegulerSurvey[0]['perusahaan_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppRegulerSurvey[0]['produk_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppRegulerSurvey[0]['periode_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppRegulerSurvey[0]['jns_PERTANGGUNGAN_ASR_TAMBAHAN']== "1",
        _dataHideShowAppRegulerSurvey[0]['nilaipertanggungan_ASRTAMBAHAN']== "1",
        _dataHideShowAppRegulerSurvey[0]['batas_ATAS_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppRegulerSurvey[0]['batas_BAWAH_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppRegulerSurvey[0]['batas_ATAS_RATE_ASR_TAMBAHAN']== "1",
        _dataHideShowAppRegulerSurvey[0]['batas_BAWAH_RATE_ASR_TAMBAHAN']== "1",
        _dataHideShowAppRegulerSurvey[0]['biaya_TUNAI_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppRegulerSurvey[0]['biaya_KREDIT_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppRegulerSurvey[0]['total_BIAYA_RATE_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppRegulerSurvey[0]['total_BIAYA_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurvey[0]['tipe_ASR_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurvey[0]['perusahaan_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurvey[0]['produk_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurvey[0]['periode_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jns_PERTANGGUNGAN_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurvey[0]['nilaipertanggungan_ASRTAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurvey[0]['batas_ATAS_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurvey[0]['batas_BAWAH_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurvey[0]['batas_ATAS_RATE_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurvey[0]['batas_BAWAH_RATE_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurvey[0]['biaya_TUNAI_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurvey[0]['biaya_KREDIT_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurvey[0]['total_BIAYA_RATE_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurvey[0]['total_BIAYA_ASURANSI_TAMBAHAN']== "1",
      );
      _showMandatoryInfoAsuransiTambahanRegulerSurveyCompanyModel = ShowMandatoryInfoAsuransiTambahanModel(
        _dataHideShowAppRegulerSurveyCompany[0]['tipe_ASR_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['perusahaan_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['produk_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['periode_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jns_PERTANGGUNGAN_ASR_TAMBAHAN']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['nilaipertanggungan_ASRTAMBAHAN']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['batas_ATAS_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['batas_BAWAH_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['batas_ATAS_RATE_ASR_TAMBAHAN']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['batas_BAWAH_RATE_ASR_TAMBAHAN']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['biaya_TUNAI_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['biaya_KREDIT_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['total_BIAYA_RATE_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['total_BIAYA_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['tipe_ASR_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['perusahaan_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['produk_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['periode_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jns_PERTANGGUNGAN_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['nilaipertanggungan_ASRTAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['batas_ATAS_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['batas_BAWAH_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['batas_ATAS_RATE_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['batas_BAWAH_RATE_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['biaya_TUNAI_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['biaya_KREDIT_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['total_BIAYA_RATE_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['total_BIAYA_ASURANSI_TAMBAHAN']== "1",
      );
      // PAC
      _showMandatoryInfoAsuransiTambahanPacModel = ShowMandatoryInfoAsuransiTambahanModel(
        _dataHideShowAppPac[0]['tipe_ASR_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppPac[0]['perusahaan_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppPac[0]['produk_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppPac[0]['periode_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppPac[0]['jns_PERTANGGUNGAN_ASR_TAMBAHAN']== "1",
        _dataHideShowAppPac[0]['nilaipertanggungan_ASRTAMBAHAN']== "1",
        _dataHideShowAppPac[0]['batas_ATAS_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppPac[0]['batas_BAWAH_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppPac[0]['batas_ATAS_RATE_ASR_TAMBAHAN']== "1",
        _dataHideShowAppPac[0]['batas_BAWAH_RATE_ASR_TAMBAHAN']== "1",
        _dataHideShowAppPac[0]['biaya_TUNAI_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppPac[0]['biaya_KREDIT_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppPac[0]['total_BIAYA_RATE_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppPac[0]['total_BIAYA_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppPac[0]['tipe_ASR_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppPac[0]['perusahaan_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppPac[0]['produk_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppPac[0]['periode_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppPac[0]['jns_PERTANGGUNGAN_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppPac[0]['nilaipertanggungan_ASRTAMBAHAN']== "1",
        _dataMandatoryAppPac[0]['batas_ATAS_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppPac[0]['batas_BAWAH_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppPac[0]['batas_ATAS_RATE_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppPac[0]['batas_BAWAH_RATE_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppPac[0]['biaya_TUNAI_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppPac[0]['biaya_KREDIT_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppPac[0]['total_BIAYA_RATE_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppPac[0]['total_BIAYA_ASURANSI_TAMBAHAN']== "1",
      );
      _showMandatoryInfoAsuransiTambahanPacCompanyModel = ShowMandatoryInfoAsuransiTambahanModel(
        _dataHideShowAppPacCompany[0]['tipe_ASR_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppPacCompany[0]['perusahaan_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppPacCompany[0]['produk_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppPacCompany[0]['periode_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppPacCompany[0]['jns_PERTANGGUNGAN_ASR_TAMBAHAN']== "1",
        _dataHideShowAppPacCompany[0]['nilaipertanggungan_ASRTAMBAHAN']== "1",
        _dataHideShowAppPacCompany[0]['batas_ATAS_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppPacCompany[0]['batas_BAWAH_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppPacCompany[0]['batas_ATAS_RATE_ASR_TAMBAHAN']== "1",
        _dataHideShowAppPacCompany[0]['batas_BAWAH_RATE_ASR_TAMBAHAN']== "1",
        _dataHideShowAppPacCompany[0]['biaya_TUNAI_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppPacCompany[0]['biaya_KREDIT_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppPacCompany[0]['total_BIAYA_RATE_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppPacCompany[0]['total_BIAYA_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppPacCompany[0]['tipe_ASR_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppPacCompany[0]['perusahaan_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppPacCompany[0]['produk_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppPacCompany[0]['periode_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppPacCompany[0]['jns_PERTANGGUNGAN_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppPacCompany[0]['nilaipertanggungan_ASRTAMBAHAN']== "1",
        _dataMandatoryAppPacCompany[0]['batas_ATAS_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppPacCompany[0]['batas_BAWAH_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppPacCompany[0]['batas_ATAS_RATE_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppPacCompany[0]['batas_BAWAH_RATE_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppPacCompany[0]['biaya_TUNAI_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppPacCompany[0]['biaya_KREDIT_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppPacCompany[0]['total_BIAYA_RATE_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppPacCompany[0]['total_BIAYA_ASURANSI_TAMBAHAN']== "1",
      );
      // Resurvey
      _showMandatoryInfoAsuransiTambahanResurveyModel = ShowMandatoryInfoAsuransiTambahanModel(
        _dataHideShowAppResurvey[0]['tipe_ASR_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppResurvey[0]['perusahaan_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppResurvey[0]['produk_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppResurvey[0]['periode_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppResurvey[0]['jns_PERTANGGUNGAN_ASR_TAMBAHAN']== "1",
        _dataHideShowAppResurvey[0]['nilaipertanggungan_ASRTAMBAHAN']== "1",
        _dataHideShowAppResurvey[0]['batas_ATAS_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppResurvey[0]['batas_BAWAH_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppResurvey[0]['batas_ATAS_RATE_ASR_TAMBAHAN']== "1",
        _dataHideShowAppResurvey[0]['batas_BAWAH_RATE_ASR_TAMBAHAN']== "1",
        _dataHideShowAppResurvey[0]['biaya_TUNAI_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppResurvey[0]['biaya_KREDIT_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppResurvey[0]['total_BIAYA_RATE_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppResurvey[0]['total_BIAYA_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppResurvey[0]['tipe_ASR_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppResurvey[0]['perusahaan_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppResurvey[0]['produk_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppResurvey[0]['periode_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppResurvey[0]['jns_PERTANGGUNGAN_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppResurvey[0]['nilaipertanggungan_ASRTAMBAHAN']== "1",
        _dataMandatoryAppResurvey[0]['batas_ATAS_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppResurvey[0]['batas_BAWAH_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppResurvey[0]['batas_ATAS_RATE_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppResurvey[0]['batas_BAWAH_RATE_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppResurvey[0]['biaya_TUNAI_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppResurvey[0]['biaya_KREDIT_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppResurvey[0]['total_BIAYA_RATE_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppResurvey[0]['total_BIAYA_ASURANSI_TAMBAHAN']== "1",
      );
      _showMandatoryInfoAsuransiTambahanResurveyCompanyModel = ShowMandatoryInfoAsuransiTambahanModel(
        _dataHideShowAppResurveyCompany[0]['tipe_ASR_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppResurveyCompany[0]['perusahaan_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppResurveyCompany[0]['produk_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppResurveyCompany[0]['periode_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppResurveyCompany[0]['jns_PERTANGGUNGAN_ASR_TAMBAHAN']== "1",
        _dataHideShowAppResurveyCompany[0]['nilaipertanggungan_ASRTAMBAHAN']== "1",
        _dataHideShowAppResurveyCompany[0]['batas_ATAS_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppResurveyCompany[0]['batas_BAWAH_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppResurveyCompany[0]['batas_ATAS_RATE_ASR_TAMBAHAN']== "1",
        _dataHideShowAppResurveyCompany[0]['batas_BAWAH_RATE_ASR_TAMBAHAN']== "1",
        _dataHideShowAppResurveyCompany[0]['biaya_TUNAI_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppResurveyCompany[0]['biaya_KREDIT_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppResurveyCompany[0]['total_BIAYA_RATE_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppResurveyCompany[0]['total_BIAYA_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppResurveyCompany[0]['tipe_ASR_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppResurveyCompany[0]['perusahaan_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppResurveyCompany[0]['produk_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppResurveyCompany[0]['periode_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppResurveyCompany[0]['jns_PERTANGGUNGAN_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppResurveyCompany[0]['nilaipertanggungan_ASRTAMBAHAN']== "1",
        _dataMandatoryAppResurveyCompany[0]['batas_ATAS_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppResurveyCompany[0]['batas_BAWAH_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppResurveyCompany[0]['batas_ATAS_RATE_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppResurveyCompany[0]['batas_BAWAH_RATE_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppResurveyCompany[0]['biaya_TUNAI_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppResurveyCompany[0]['biaya_KREDIT_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppResurveyCompany[0]['total_BIAYA_RATE_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppResurveyCompany[0]['total_BIAYA_ASURANSI_TAMBAHAN']== "1",
      );
      // Dakor
      _showMandatoryInfoAsuransiTambahanDakorModel = ShowMandatoryInfoAsuransiTambahanModel(
        _dataHideShowAppDakor[0]['tipe_ASR_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppDakor[0]['perusahaan_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppDakor[0]['produk_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppDakor[0]['periode_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppDakor[0]['jns_PERTANGGUNGAN_ASR_TAMBAHAN']== "1",
        _dataHideShowAppDakor[0]['nilaipertanggungan_ASRTAMBAHAN']== "1",
        _dataHideShowAppDakor[0]['batas_ATAS_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppDakor[0]['batas_BAWAH_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppDakor[0]['batas_ATAS_RATE_ASR_TAMBAHAN']== "1",
        _dataHideShowAppDakor[0]['batas_BAWAH_RATE_ASR_TAMBAHAN']== "1",
        _dataHideShowAppDakor[0]['biaya_TUNAI_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppDakor[0]['biaya_KREDIT_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppDakor[0]['total_BIAYA_RATE_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppDakor[0]['total_BIAYA_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppDakor[0]['tipe_ASR_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppDakor[0]['perusahaan_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppDakor[0]['produk_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppDakor[0]['periode_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppDakor[0]['jns_PERTANGGUNGAN_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppDakor[0]['nilaipertanggungan_ASRTAMBAHAN']== "1",
        _dataMandatoryAppDakor[0]['batas_ATAS_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppDakor[0]['batas_BAWAH_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppDakor[0]['batas_ATAS_RATE_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppDakor[0]['batas_BAWAH_RATE_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppDakor[0]['biaya_TUNAI_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppDakor[0]['biaya_KREDIT_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppDakor[0]['total_BIAYA_RATE_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppDakor[0]['total_BIAYA_ASURANSI_TAMBAHAN']== "1",
      );
      _showMandatoryInfoAsuransiTambahanDakorCompanyModel = ShowMandatoryInfoAsuransiTambahanModel(
        _dataHideShowAppDakorCom[0]['tipe_ASR_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppDakorCom[0]['perusahaan_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppDakorCom[0]['produk_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppDakorCom[0]['periode_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppDakorCom[0]['jns_PERTANGGUNGAN_ASR_TAMBAHAN']== "1",
        _dataHideShowAppDakorCom[0]['nilaipertanggungan_ASRTAMBAHAN']== "1",
        _dataHideShowAppDakorCom[0]['batas_ATAS_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppDakorCom[0]['batas_BAWAH_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppDakorCom[0]['batas_ATAS_RATE_ASR_TAMBAHAN']== "1",
        _dataHideShowAppDakorCom[0]['batas_BAWAH_RATE_ASR_TAMBAHAN']== "1",
        _dataHideShowAppDakorCom[0]['biaya_TUNAI_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppDakorCom[0]['biaya_KREDIT_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppDakorCom[0]['total_BIAYA_RATE_ASURANSI_TAMBAHAN']== "1",
        _dataHideShowAppDakorCom[0]['total_BIAYA_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppDakorCom[0]['tipe_ASR_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppDakorCom[0]['perusahaan_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppDakorCom[0]['produk_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppDakorCom[0]['periode_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppDakorCom[0]['jns_PERTANGGUNGAN_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppDakorCom[0]['nilaipertanggungan_ASRTAMBAHAN']== "1",
        _dataMandatoryAppDakorCom[0]['batas_ATAS_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppDakorCom[0]['batas_BAWAH_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppDakorCom[0]['batas_ATAS_RATE_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppDakorCom[0]['batas_BAWAH_RATE_ASR_TAMBAHAN']== "1",
        _dataMandatoryAppDakorCom[0]['biaya_TUNAI_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppDakorCom[0]['biaya_KREDIT_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppDakorCom[0]['total_BIAYA_RATE_ASURANSI_TAMBAHAN']== "1",
        _dataMandatoryAppDakorCom[0]['total_BIAYA_ASURANSI_TAMBAHAN']== "1",
      );
      // End Inf Asuransi Tambahan

      // Inf WMP
      // IDE
      _showMandatoryInfoWMPIdeModel = ShowMandatoryInfoWMPModel(
        _dataHideShowAppIde[0]['get_WMP']== "1",
      );
      _showMandatoryInfoWMPIdeCompanyModel = ShowMandatoryInfoWMPModel(
        _dataHideShowAppIdeCompany[0]['get_WMP']== "1",

      );
      // Reguler Survey
      _showMandatoryInfoWMPRegulerSurveyModel = ShowMandatoryInfoWMPModel(
        _dataHideShowAppRegulerSurvey[0]['get_WMP']== "1",
      );
      _showMandatoryInfoWMPRegulerSurveyCompanyModel = ShowMandatoryInfoWMPModel(
        _dataHideShowAppRegulerSurveyCompany[0]['get_WMP']== "1",
      );
      // PAC
      _showMandatoryInfoWMPPacModel = ShowMandatoryInfoWMPModel(
        _dataHideShowAppPac[0]['get_WMP']== "1",
      );
      _showMandatoryInfoWMPPacCompanyModel = ShowMandatoryInfoWMPModel(
        _dataHideShowAppPacCompany[0]['get_WMP']== "1",
      );
      // Resurvey
      _showMandatoryInfoWMPResurveyModel = ShowMandatoryInfoWMPModel(
        _dataHideShowAppResurvey[0]['get_WMP']== "1",
      );
      _showMandatoryInfoWMPResurveyCompanyModel = ShowMandatoryInfoWMPModel(
        _dataHideShowAppResurveyCompany[0]['get_WMP']== "1",
      );
      // Dakor
      _showMandatoryInfoWMPDakorModel = ShowMandatoryInfoWMPModel(
        _dataHideShowAppDakor[0]['get_WMP']== "1",
      );
      _showMandatoryInfoWMPDakorCompanyModel = ShowMandatoryInfoWMPModel(
        _dataHideShowAppDakorCom[0]['get_WMP']== "1",
      );
      // End Inf WMP

      // Inf Subsidi
      // IDE
      _showMandatoryInfoSubsidiIdeModel = ShowMandatoryInfoSubsidiModel(
        _dataHideShowAppIde[0]['pemberi_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppIde[0]['jenis_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppIde[0]['metode_POTONG_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppIde[0]['nilai_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppIde[0]['bunga_SBLM_EFF_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppIde[0]['bunga_SBLM_FLAT_KREDITSUBSIDI']== "1",
        _dataHideShowAppIde[0]['subsidi_ANGS_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppIde[0]['jumlah_ANGSURAN_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppIde[0]['angsuran_KE_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppIde[0]['pemberi_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppIde[0]['jenis_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppIde[0]['metode_POTONG_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppIde[0]['nilai_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppIde[0]['bunga_SBLM_EFF_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppIde[0]['bunga_SBLM_FLAT_KREDITSUBSIDI']== "1",
        _dataMandatoryAppIde[0]['subsidi_ANGS_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppIde[0]['jumlah_ANGSURAN_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppIde[0]['angsuran_KE_KREDIT_SUBSIDI']== "1",
      );
      debugPrint("CEK_pemberi_KREDIT_SUBSIDI ${_showMandatoryInfoSubsidiIdeModel.isRadioValueGiverVisible}");
      _showMandatoryInfoSubsidiIdeCompanyModel = ShowMandatoryInfoSubsidiModel(
        _dataHideShowAppIdeCompany[0]['pemberi_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppIdeCompany[0]['jenis_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppIdeCompany[0]['metode_POTONG_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppIdeCompany[0]['nilai_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppIdeCompany[0]['bunga_SBLM_EFF_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppIdeCompany[0]['bunga_SBLM_FLAT_KREDITSUBSIDI']== "1",
        _dataHideShowAppIdeCompany[0]['subsidi_ANGS_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppIdeCompany[0]['jumlah_ANGSURAN_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppIdeCompany[0]['angsuran_KE_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppIdeCompany[0]['pemberi_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppIdeCompany[0]['jenis_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppIdeCompany[0]['metode_POTONG_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppIdeCompany[0]['nilai_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppIdeCompany[0]['bunga_SBLM_EFF_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppIdeCompany[0]['bunga_SBLM_FLAT_KREDITSUBSIDI']== "1",
        _dataMandatoryAppIdeCompany[0]['subsidi_ANGS_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppIdeCompany[0]['jumlah_ANGSURAN_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppIdeCompany[0]['angsuran_KE_KREDIT_SUBSIDI']== "1",
      );
      // Reguler Survey
      _showMandatoryInfoSubsidiRegulerSurveyModel = ShowMandatoryInfoSubsidiModel(
        _dataHideShowAppRegulerSurvey[0]['pemberi_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppRegulerSurvey[0]['jenis_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppRegulerSurvey[0]['metode_POTONG_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppRegulerSurvey[0]['nilai_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppRegulerSurvey[0]['bunga_SBLM_EFF_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppRegulerSurvey[0]['bunga_SBLM_FLAT_KREDITSUBSIDI']== "1",
        _dataHideShowAppRegulerSurvey[0]['subsidi_ANGS_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppRegulerSurvey[0]['jumlah_ANGSURAN_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppRegulerSurvey[0]['angsuran_KE_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['pemberi_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jenis_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['metode_POTONG_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['nilai_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['bunga_SBLM_EFF_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['bunga_SBLM_FLAT_KREDITSUBSIDI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['subsidi_ANGS_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['jumlah_ANGSURAN_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppRegulerSurvey[0]['angsuran_KE_KREDIT_SUBSIDI']== "1",
      );
      _showMandatoryInfoSubsidiRegulerSurveyCompanyModel = ShowMandatoryInfoSubsidiModel(
        _dataHideShowAppRegulerSurveyCompany[0]['pemberi_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jenis_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['metode_POTONG_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['nilai_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['bunga_SBLM_EFF_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['bunga_SBLM_FLAT_KREDITSUBSIDI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['subsidi_ANGS_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['jumlah_ANGSURAN_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['angsuran_KE_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['pemberi_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jenis_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['metode_POTONG_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['nilai_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['bunga_SBLM_EFF_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['bunga_SBLM_FLAT_KREDITSUBSIDI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['subsidi_ANGS_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['jumlah_ANGSURAN_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['angsuran_KE_KREDIT_SUBSIDI']== "1",
      );
      // PAC
      _showMandatoryInfoSubsidiPacModel = ShowMandatoryInfoSubsidiModel(
        _dataHideShowAppPac[0]['pemberi_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppPac[0]['jenis_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppPac[0]['metode_POTONG_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppPac[0]['nilai_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppPac[0]['bunga_SBLM_EFF_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppPac[0]['bunga_SBLM_FLAT_KREDITSUBSIDI']== "1",
        _dataHideShowAppPac[0]['subsidi_ANGS_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppPac[0]['jumlah_ANGSURAN_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppPac[0]['angsuran_KE_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppPac[0]['pemberi_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppPac[0]['jenis_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppPac[0]['metode_POTONG_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppPac[0]['nilai_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppPac[0]['bunga_SBLM_EFF_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppPac[0]['bunga_SBLM_FLAT_KREDITSUBSIDI']== "1",
        _dataMandatoryAppPac[0]['subsidi_ANGS_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppPac[0]['jumlah_ANGSURAN_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppPac[0]['angsuran_KE_KREDIT_SUBSIDI']== "1",
      );
      _showMandatoryInfoSubsidiPacCompanyModel = ShowMandatoryInfoSubsidiModel(
        _dataHideShowAppPacCompany[0]['pemberi_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppPacCompany[0]['jenis_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppPacCompany[0]['metode_POTONG_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppPacCompany[0]['nilai_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppPacCompany[0]['bunga_SBLM_EFF_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppPacCompany[0]['bunga_SBLM_FLAT_KREDITSUBSIDI']== "1",
        _dataHideShowAppPacCompany[0]['subsidi_ANGS_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppPacCompany[0]['jumlah_ANGSURAN_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppPacCompany[0]['angsuran_KE_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppPacCompany[0]['pemberi_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppPacCompany[0]['jenis_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppPacCompany[0]['metode_POTONG_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppPacCompany[0]['nilai_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppPacCompany[0]['bunga_SBLM_EFF_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppPacCompany[0]['bunga_SBLM_FLAT_KREDITSUBSIDI']== "1",
        _dataMandatoryAppPacCompany[0]['subsidi_ANGS_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppPacCompany[0]['jumlah_ANGSURAN_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppPacCompany[0]['angsuran_KE_KREDIT_SUBSIDI']== "1",
      );
      // Resurvey
      _showMandatoryInfoSubsidiResurveyModel = ShowMandatoryInfoSubsidiModel(
        _dataHideShowAppResurvey[0]['pemberi_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppResurvey[0]['jenis_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppResurvey[0]['metode_POTONG_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppResurvey[0]['nilai_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppResurvey[0]['bunga_SBLM_EFF_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppResurvey[0]['bunga_SBLM_FLAT_KREDITSUBSIDI']== "1",
        _dataHideShowAppResurvey[0]['subsidi_ANGS_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppResurvey[0]['jumlah_ANGSURAN_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppResurvey[0]['angsuran_KE_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppResurvey[0]['pemberi_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppResurvey[0]['jenis_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppResurvey[0]['metode_POTONG_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppResurvey[0]['nilai_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppResurvey[0]['bunga_SBLM_EFF_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppResurvey[0]['bunga_SBLM_FLAT_KREDITSUBSIDI']== "1",
        _dataMandatoryAppResurvey[0]['subsidi_ANGS_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppResurvey[0]['jumlah_ANGSURAN_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppResurvey[0]['angsuran_KE_KREDIT_SUBSIDI']== "1",
      );
      _showMandatoryInfoSubsidiResurveyCompanyModel = ShowMandatoryInfoSubsidiModel(
        _dataHideShowAppResurveyCompany[0]['pemberi_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppResurveyCompany[0]['jenis_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppResurveyCompany[0]['metode_POTONG_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppResurveyCompany[0]['nilai_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppResurveyCompany[0]['bunga_SBLM_EFF_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppResurveyCompany[0]['bunga_SBLM_FLAT_KREDITSUBSIDI']== "1",
        _dataHideShowAppResurveyCompany[0]['subsidi_ANGS_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppResurveyCompany[0]['jumlah_ANGSURAN_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppResurveyCompany[0]['angsuran_KE_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppResurveyCompany[0]['pemberi_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppResurveyCompany[0]['jenis_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppResurveyCompany[0]['metode_POTONG_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppResurveyCompany[0]['nilai_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppResurveyCompany[0]['bunga_SBLM_EFF_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppResurveyCompany[0]['bunga_SBLM_FLAT_KREDITSUBSIDI']== "1",
        _dataMandatoryAppResurveyCompany[0]['subsidi_ANGS_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppResurveyCompany[0]['jumlah_ANGSURAN_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppResurveyCompany[0]['angsuran_KE_KREDIT_SUBSIDI']== "1",
      );
      // Dakor
      _showMandatoryInfoSubsidiDakorModel = ShowMandatoryInfoSubsidiModel(
        _dataHideShowAppDakor[0]['pemberi_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppDakor[0]['jenis_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppDakor[0]['metode_POTONG_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppDakor[0]['nilai_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppDakor[0]['bunga_SBLM_EFF_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppDakor[0]['bunga_SBLM_FLAT_KREDITSUBSIDI']== "1",
        _dataHideShowAppDakor[0]['subsidi_ANGS_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppDakor[0]['jumlah_ANGSURAN_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppDakor[0]['angsuran_KE_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppDakor[0]['pemberi_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppDakor[0]['jenis_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppDakor[0]['metode_POTONG_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppDakor[0]['nilai_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppDakor[0]['bunga_SBLM_EFF_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppDakor[0]['bunga_SBLM_FLAT_KREDITSUBSIDI']== "1",
        _dataMandatoryAppDakor[0]['subsidi_ANGS_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppDakor[0]['jumlah_ANGSURAN_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppDakor[0]['angsuran_KE_KREDIT_SUBSIDI']== "1",
      );
      _showMandatoryInfoSubsidiDakorCompanyModel = ShowMandatoryInfoSubsidiModel(
        _dataHideShowAppDakorCom[0]['pemberi_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppDakorCom[0]['jenis_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppDakorCom[0]['metode_POTONG_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppDakorCom[0]['nilai_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppDakorCom[0]['bunga_SBLM_EFF_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppDakorCom[0]['bunga_SBLM_FLAT_KREDITSUBSIDI']== "1",
        _dataHideShowAppDakorCom[0]['subsidi_ANGS_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppDakorCom[0]['jumlah_ANGSURAN_KREDIT_SUBSIDI']== "1",
        _dataHideShowAppDakorCom[0]['angsuran_KE_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppDakorCom[0]['pemberi_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppDakorCom[0]['jenis_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppDakorCom[0]['metode_POTONG_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppDakorCom[0]['nilai_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppDakorCom[0]['bunga_SBLM_EFF_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppDakorCom[0]['bunga_SBLM_FLAT_KREDITSUBSIDI']== "1",
        _dataMandatoryAppDakorCom[0]['subsidi_ANGS_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppDakorCom[0]['jumlah_ANGSURAN_KREDIT_SUBSIDI']== "1",
        _dataMandatoryAppDakorCom[0]['angsuran_KE_KREDIT_SUBSIDI']== "1",
      );
      // End Inf Subsidi

      // Inf Dokumen
      // IDE
      _showMandatoryInfoDokumenIdeModel = ShowMandatoryInfoDokumenModel(
        _dataHideShowAppIde[0]['dokumen_DOKUMEN']== "1",
        _dataHideShowAppIde[0]['tanggal_TERIMA_DOKUMEN']== "1",
        _dataHideShowAppIde[0]['dokumen_UPLOAD_DOKUMEN']== "1",
        _dataMandatoryAppIde[0]['dokumen_DOKUMEN']== "1",
        _dataMandatoryAppIde[0]['tanggal_TERIMA_DOKUMEN']== "1",
        _dataMandatoryAppIde[0]['dokumen_UPLOAD_DOKUMEN']== "1",
      );
      _showMandatoryInfoDokumenIdeCompanyModel = ShowMandatoryInfoDokumenModel(
        _dataHideShowAppIdeCompany[0]['dokumen_DOKUMEN']== "1",
        _dataHideShowAppIdeCompany[0]['tanggal_TERIMA_DOKUMEN']== "1",
        _dataHideShowAppIdeCompany[0]['dokumen_UPLOAD_DOKUMEN']== "1",
        _dataMandatoryAppIdeCompany[0]['dokumen_DOKUMEN']== "1",
        _dataMandatoryAppIdeCompany[0]['tanggal_TERIMA_DOKUMEN']== "1",
        _dataMandatoryAppIdeCompany[0]['dokumen_UPLOAD_DOKUMEN']== "1",
      );
      // Reguler Survey
      _showMandatoryInfoDokumenRegulerSurveyModel = ShowMandatoryInfoDokumenModel(
        _dataHideShowAppRegulerSurvey[0]['dokumen_DOKUMEN']== "1",
        _dataHideShowAppRegulerSurvey[0]['tanggal_TERIMA_DOKUMEN']== "1",
        _dataHideShowAppRegulerSurvey[0]['dokumen_UPLOAD_DOKUMEN']== "1",
        _dataMandatoryAppRegulerSurvey[0]['dokumen_DOKUMEN']== "1",
        _dataMandatoryAppRegulerSurvey[0]['tanggal_TERIMA_DOKUMEN']== "1",
        _dataMandatoryAppRegulerSurvey[0]['dokumen_UPLOAD_DOKUMEN']== "1",
      );
      _showMandatoryInfoDokumenRegulerSurveyCompanyModel = ShowMandatoryInfoDokumenModel(
        _dataHideShowAppRegulerSurveyCompany[0]['dokumen_DOKUMEN']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['tanggal_TERIMA_DOKUMEN']== "1",
        _dataHideShowAppRegulerSurveyCompany[0]['dokumen_UPLOAD_DOKUMEN']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['dokumen_DOKUMEN']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['tanggal_TERIMA_DOKUMEN']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['dokumen_UPLOAD_DOKUMEN']== "1",
      );
      // PAC
      _showMandatoryInfoDokumenPacModel = ShowMandatoryInfoDokumenModel(
        _dataHideShowAppPac[0]['dokumen_DOKUMEN']== "1",
        _dataHideShowAppPac[0]['tanggal_TERIMA_DOKUMEN']== "1",
        _dataHideShowAppPac[0]['dokumen_UPLOAD_DOKUMEN']== "1",
        _dataMandatoryAppPac[0]['dokumen_DOKUMEN']== "1",
        _dataMandatoryAppPac[0]['tanggal_TERIMA_DOKUMEN']== "1",
        _dataMandatoryAppPac[0]['dokumen_UPLOAD_DOKUMEN']== "1",
      );
      _showMandatoryInfoDokumenPacCompanyModel = ShowMandatoryInfoDokumenModel(
        _dataHideShowAppPacCompany[0]['dokumen_DOKUMEN']== "1",
        _dataHideShowAppPacCompany[0]['tanggal_TERIMA_DOKUMEN']== "1",
        _dataHideShowAppPacCompany[0]['dokumen_UPLOAD_DOKUMEN']== "1",
        _dataMandatoryAppPacCompany[0]['dokumen_DOKUMEN']== "1",
        _dataMandatoryAppPacCompany[0]['tanggal_TERIMA_DOKUMEN']== "1",
        _dataMandatoryAppPacCompany[0]['dokumen_UPLOAD_DOKUMEN']== "1",
      );
      // Resurvey
      _showMandatoryInfoDokumenResurveyModel = ShowMandatoryInfoDokumenModel(
        _dataHideShowAppResurvey[0]['dokumen_DOKUMEN']== "1",
        _dataHideShowAppResurvey[0]['tanggal_TERIMA_DOKUMEN']== "1",
        _dataHideShowAppResurvey[0]['dokumen_UPLOAD_DOKUMEN']== "1",
        _dataMandatoryAppResurvey[0]['dokumen_DOKUMEN']== "1",
        _dataMandatoryAppResurvey[0]['tanggal_TERIMA_DOKUMEN']== "1",
        _dataMandatoryAppResurvey[0]['dokumen_UPLOAD_DOKUMEN']== "1",
      );
      _showMandatoryInfoDokumenResurveyCompanyModel = ShowMandatoryInfoDokumenModel(
        _dataHideShowAppResurveyCompany[0]['dokumen_DOKUMEN']== "1",
        _dataHideShowAppResurveyCompany[0]['tanggal_TERIMA_DOKUMEN']== "1",
        _dataHideShowAppResurveyCompany[0]['dokumen_UPLOAD_DOKUMEN']== "1",
        _dataMandatoryAppResurveyCompany[0]['dokumen_DOKUMEN']== "1",
        _dataMandatoryAppResurveyCompany[0]['tanggal_TERIMA_DOKUMEN']== "1",
        _dataMandatoryAppResurveyCompany[0]['dokumen_UPLOAD_DOKUMEN']== "1",
      );
      // Dakor
      _showMandatoryInfoDokumenDakorModel = ShowMandatoryInfoDokumenModel(
        _dataHideShowAppDakor[0]['dokumen_DOKUMEN']== "1",
        _dataHideShowAppDakor[0]['tanggal_TERIMA_DOKUMEN']== "1",
        _dataHideShowAppDakor[0]['dokumen_UPLOAD_DOKUMEN']== "1",
        _dataMandatoryAppDakor[0]['dokumen_DOKUMEN']== "1",
        _dataMandatoryAppDakor[0]['tanggal_TERIMA_DOKUMEN']== "1",
        _dataMandatoryAppDakor[0]['dokumen_UPLOAD_DOKUMEN']== "1",
      );
      _showMandatoryInfoDokumenDakorCompanyModel = ShowMandatoryInfoDokumenModel(
        _dataHideShowAppDakorCom[0]['dokumen_DOKUMEN']== "1",
        _dataHideShowAppDakorCom[0]['tanggal_TERIMA_DOKUMEN']== "1",
        _dataHideShowAppDakorCom[0]['dokumen_UPLOAD_DOKUMEN']== "1",
        _dataMandatoryAppDakorCom[0]['dokumen_DOKUMEN']== "1",
        _dataMandatoryAppDakorCom[0]['tanggal_TERIMA_DOKUMEN']== "1",
        _dataMandatoryAppDakorCom[0]['dokumen_UPLOAD_DOKUMEN']== "1",
      );
      // End Inf Dokumen

      // Marketing Notes
      // IDE
      _showMandatoryMarketingNotesIdeModel = ShowMandatoryMarketingNotesModel(
        _dataHideShowAppIde[0]['marketing_NOTES']== "1",
        _dataMandatoryAppIde[0]['marketing_NOTES']== "1",
      );
      _showMandatoryMarketingNotesIdeCompanyModel = ShowMandatoryMarketingNotesModel(
        _dataHideShowAppIdeCompany[0]['marketing_NOTES']== "1",
        _dataMandatoryAppIdeCompany[0]['marketing_NOTES']== "1",
      );
      // Reguler Survey
      _showMandatoryMarketingNotesRegulerSurveyModel = ShowMandatoryMarketingNotesModel(
        _dataHideShowAppRegulerSurvey[0]['marketing_NOTES']== "1",
        _dataMandatoryAppRegulerSurvey[0]['marketing_NOTES']== "1",
      );
      _showMandatoryMarketingNotesRegulerSurveyCompanyModel = ShowMandatoryMarketingNotesModel(
        _dataHideShowAppRegulerSurveyCompany[0]['marketing_NOTES']== "1",
        _dataMandatoryAppRegulerSurveyCompany[0]['marketing_NOTES']== "1",
      );
      // PAC
      _showMandatoryMarketingNotesPacModel = ShowMandatoryMarketingNotesModel(
        _dataHideShowAppPac[0]['marketing_NOTES']== "1",
        _dataMandatoryAppPac[0]['marketing_NOTES']== "1",
      );
      _showMandatoryMarketingNotesPacCompanyModel = ShowMandatoryMarketingNotesModel(
        _dataHideShowAppPacCompany[0]['marketing_NOTES']== "1",
        _dataMandatoryAppPacCompany[0]['marketing_NOTES']== "1",
      );
      // Resurvey
      _showMandatoryMarketingNotesResurveyModel = ShowMandatoryMarketingNotesModel(
        _dataHideShowAppResurvey[0]['marketing_NOTES']== "1",
        _dataMandatoryAppResurvey[0]['marketing_NOTES']== "1",
      );
      _showMandatoryMarketingNotesResurveyCompanyModel = ShowMandatoryMarketingNotesModel(
        _dataHideShowAppResurveyCompany[0]['marketing_NOTES']== "1",
        _dataMandatoryAppResurveyCompany[0]['marketing_NOTES']== "1",
      );
      // Dakor
      _showMandatoryMarketingNotesDakorModel = ShowMandatoryMarketingNotesModel(
        _dataHideShowAppDakor[0]['marketing_NOTES']== "1",
        _dataMandatoryAppDakor[0]['marketing_NOTES']== "1",
      );
      _showMandatoryMarketingNotesDakorCompanyModel = ShowMandatoryMarketingNotesModel(
        _dataHideShowAppDakorCom[0]['marketing_NOTES']== "1",
        _dataMandatoryAppDakorCom[0]['marketing_NOTES']== "1",
      );
      // End Marketing Notes

      _showMandatoryIncomeModel = ShowMandatoryIncomeModel(
         _dataHideShowIde[0]['incomeperbulanwiraswasta']== "1",
         _dataHideShowIde[0]['incomelainnyawiraswasta']== "1",
         _dataHideShowIde[0]['totalincomewiraswasta']== "1",
         _dataHideShowIde[0]['pokokincomewiraswasta']== "1",
         _dataHideShowIde[0]['labakotorwiraswasta']== "1",
         _dataHideShowIde[0]['biayaoperasionalwiraswasta']== "1",
         _dataHideShowIde[0]['biayalainnyawiraswasta']== "1",
         _dataHideShowIde[0]['netsebelumpajakwiraswasta']== "1",
         _dataHideShowIde[0]['pajakwiraswasta']== "1",
         _dataHideShowIde[0]['netsetelahpajakwiraswasta']== "1",
         _dataHideShowIde[0]['biayahidupwiraswasta']== "1",
         _dataHideShowIde[0]['sisaincomewiraswasta']== "1",
         _dataHideShowIde[0]['incomepasanganwiraswasta']== "1",
         _dataHideShowIde[0]['angsuranlainnyawiraswasta']== "1",
         _dataHideShowIde[0]['incomeprofesional']== "1",
         _dataHideShowIde[0]['incomepasanganprofesional']== "1",
         _dataHideShowIde[0]['incomelainnyaprofesional']== "1",
         _dataHideShowIde[0]['totalincomeprofesional']== "1",
         _dataHideShowIde[0]['biayahidupprofesional']== "1",
         _dataHideShowIde[0]['sisaincomeprofesional']== "1",
         _dataHideShowIde[0]['angsuranlainnyaprofesional']== "1",
         _dataMandatoryIde[0]['incomeperbulanwiraswasta']== "1",
         _dataMandatoryIde[0]['incomelainnyawiraswasta']== "1",
         _dataMandatoryIde[0]['totalincomewiraswasta']== "1",
         _dataMandatoryIde[0]['pokokincomewiraswasta']== "1",
         _dataMandatoryIde[0]['labakotorwiraswasta']== "1",
         _dataMandatoryIde[0]['biayaoperasionalwiraswasta']== "1",
         _dataMandatoryIde[0]['biayalainnyawiraswasta']== "1",
         _dataMandatoryIde[0]['netsebelumpajakwiraswasta']== "1",
         _dataMandatoryIde[0]['pajakwiraswasta']== "1",
         _dataMandatoryIde[0]['netsetelahpajakwiraswasta']== "1",
         _dataMandatoryIde[0]['biayahidupwiraswasta']== "1",
         _dataMandatoryIde[0]['sisaincomewiraswasta']== "1",
         _dataMandatoryIde[0]['incomepasanganwiraswasta']== "1",
         _dataMandatoryIde[0]['angsuranlainnyawiraswasta']== "1",
         _dataMandatoryIde[0]['incomeprofesional']== "1",
         _dataMandatoryIde[0]['incomepasanganprofesional']== "1",
         _dataMandatoryIde[0]['incomelainnyaprofesional']== "1",
         _dataMandatoryIde[0]['totalincomeprofesional']== "1",
         _dataMandatoryIde[0]['biayahidupprofesional']== "1",
         _dataMandatoryIde[0]['sisaincomeprofesional']== "1",
         _dataMandatoryIde[0]['angsuranlainnyaprofesional']== "1"
      );

      List _dataHideShowRegularSurvey = await getHideShowRegularSurvey();
      List _dataMandatoryRegularSurvey = await getMandatoryRegularSurvey();
      List _dataEnableDisableRegularSurvey = await getEnabledRegularSurvey();
      _showMandatoryResultSurveyRegularSurvey = ShowMandatoryEnabledResultSurvey(

        //Widget Result Survey Group Note

        _dataHideShowRegularSurvey[0]['hasilSurvey'] == "1",
        _dataMandatoryRegularSurvey[0]['hasilSurvey'] == "1",
        _dataEnableDisableRegularSurvey[0]['hasilSurvey'] == "1",


        _dataHideShowRegularSurvey[0]['recomSurvey'] == "1",
        _dataMandatoryRegularSurvey[0]['recomSurvey'] == "1",
        _dataEnableDisableRegularSurvey[0]['recomSurvey'] == "1",


        _dataHideShowRegularSurvey[0]['catatan'] == "1",
        _dataMandatoryRegularSurvey[0]['catatan'] == "1",
        _dataEnableDisableRegularSurvey[0]['catatan'] == "1",


        _dataHideShowRegularSurvey[0]['tglHasilSurvey'] == "1",
        _dataMandatoryRegularSurvey[0]['tglHasilSurvey'] == "1",
        _dataEnableDisableRegularSurvey[0]['tglHasilSurvey'] == "1",


        //Widget Result Survey Photo

        _dataHideShowRegularSurvey[0]['jenisFoto'] == "1",
        _dataMandatoryRegularSurvey[0]['jenisFoto'] == "1",
        _dataEnableDisableRegularSurvey[0]['jenisFoto'] == "1",


        //Widget Result Survey Group Location

        _dataHideShowRegularSurvey[0]['jenisSurvey'] == "1",
        _dataMandatoryRegularSurvey[0]['jenisSurvey'] == "1",
        _dataEnableDisableRegularSurvey[0]['jenisSurvey'] == "1",


        _dataHideShowRegularSurvey[0]['jarakSentra'] == "1",
        _dataMandatoryRegularSurvey[0]['jarakSentra'] == "1",
        _dataEnableDisableRegularSurvey[0]['jarakSentra'] == "1",


        _dataHideShowRegularSurvey[0]['jarakDealer'] == "1",
        _dataMandatoryRegularSurvey[0]['jarakDealer'] == "1",
        _dataEnableDisableRegularSurvey[0]['jarakDealer'] == "1",


        _dataHideShowRegularSurvey[0]['jarakObjSentra'] == "1",
        _dataMandatoryRegularSurvey[0]['jarakObjSentra'] == "1",
        _dataEnableDisableRegularSurvey[0]['jarakObjSentra'] == "1",


        //Widget Add Edit Asset

        _dataHideShowRegularSurvey[0]['jenisAsset'] == "1",
        _dataMandatoryRegularSurvey[0]['jenisAsset'] == "1",
        _dataEnableDisableRegularSurvey[0]['jenisAsset'] == "1",


        _dataHideShowRegularSurvey[0]['nilai'] == "1",
        _dataMandatoryRegularSurvey[0]['nilai'] == "1",
        _dataEnableDisableRegularSurvey[0]['nilai'] == "1",


        _dataHideShowRegularSurvey[0]['kepemilikan'] == "1",
        _dataMandatoryRegularSurvey[0]['kepemilikan'] == "1",
        _dataEnableDisableRegularSurvey[0]['kepemilikan'] == "1",


        _dataHideShowRegularSurvey[0]['ukuranLuasTanah'] == "1",
        _dataMandatoryRegularSurvey[0]['ukuranLuasTanah'] == "1",
        _dataEnableDisableRegularSurvey[0]['ukuranLuasTanah'] == "1",


        _dataHideShowRegularSurvey[0]['jenisJalan'] == "1",
        _dataMandatoryRegularSurvey[0]['jenisJalan'] == "1",
        _dataEnableDisableRegularSurvey[0]['jenisJalan'] == "1",


        _dataHideShowRegularSurvey[0]['listrik'] == "1",
        _dataMandatoryRegularSurvey[0]['listrik'] == "1",
        _dataEnableDisableRegularSurvey[0]['listrik'] == "1",


        _dataHideShowRegularSurvey[0]['tagihanListrik'] == "1",
        _dataMandatoryRegularSurvey[0]['tagihanListrik'] == "1",
        _dataEnableDisableRegularSurvey[0]['tagihanListrik'] == "1",


        _dataHideShowRegularSurvey[0]['tglAkhirSewa'] == "1",
        _dataMandatoryRegularSurvey[0]['tglAkhirSewa'] == "1",
        _dataEnableDisableRegularSurvey[0]['tglAkhirSewa'] == "1",


        _dataHideShowRegularSurvey[0]['lamaTinggal'] == "1",
        _dataMandatoryRegularSurvey[0]['lamaTinggal'] == "1",
        _dataEnableDisableRegularSurvey[0]['lamaTinggal'] == "1",


        //Widget Add Edit Survey Detail
        _dataHideShowRegularSurvey[0]['infoLingkungan'] == "1",
        _dataMandatoryRegularSurvey[0]['infoLingkungan'] == "1",
        _dataEnableDisableRegularSurvey[0]['infoLingkungan'] == "1",


        _dataHideShowRegularSurvey[0]['sumberInformasi'] == "1",
        _dataMandatoryRegularSurvey[0]['sumberInformasi'] == "1",
        _dataEnableDisableRegularSurvey[0]['sumberInformasi'] == "1",


        _dataHideShowRegularSurvey[0]['namaSumberInformasi'] == "1",
        _dataMandatoryRegularSurvey[0]['namaSumberInformasi'] == "1",
        _dataEnableDisableRegularSurvey[0]['namaSumberInformasi'] == "1",

      );

      List _dataHideShowPAC = await getHideShowPAC();
      List _dataMandatoryPAC = await getMandatoryPAC();
      List _dataEnableDisablePAC = await getEnabledPAC();
      _showMandatoryResultSurveyPAC = ShowMandatoryEnabledResultSurvey(

        //Widget Result Survey Group Note

        _dataHideShowPAC[0]['hasilSurvey'] == "1",
        _dataMandatoryPAC[0]['hasilSurvey'] == "1",
        _dataEnableDisablePAC[0]['hasilSurvey'] == "1",


        _dataHideShowPAC[0]['recomSurvey'] == "1",
        _dataMandatoryPAC[0]['recomSurvey'] == "1",
        _dataEnableDisablePAC[0]['recomSurvey'] == "1",


        _dataHideShowPAC[0]['catatan'] == "1",
        _dataMandatoryPAC[0]['catatan'] == "1",
        _dataEnableDisablePAC[0]['catatan'] == "1",


        _dataHideShowPAC[0]['tglHasilSurvey'] == "1",
        _dataMandatoryPAC[0]['tglHasilSurvey'] == "1",
        _dataEnableDisablePAC[0]['tglHasilSurvey'] == "1",


        //Widget Result Survey Photo

        _dataHideShowPAC[0]['jenisFoto'] == "1",
        _dataMandatoryPAC[0]['jenisFoto'] == "1",
        _dataEnableDisablePAC[0]['jenisFoto'] == "1",


        //Widget Result Survey Group Location

        _dataHideShowPAC[0]['jenisSurvey'] == "1",
        _dataMandatoryPAC[0]['jenisSurvey'] == "1",
        _dataEnableDisablePAC[0]['jenisSurvey'] == "1",


        _dataHideShowPAC[0]['jarakSentra'] == "1",
        _dataMandatoryPAC[0]['jarakSentra'] == "1",
        _dataEnableDisablePAC[0]['jarakSentra'] == "1",


        _dataHideShowPAC[0]['jarakDealer'] == "1",
        _dataMandatoryPAC[0]['jarakDealer'] == "1",
        _dataEnableDisablePAC[0]['jarakDealer'] == "1",


        _dataHideShowPAC[0]['jarakObjSentra'] == "1",
        _dataMandatoryPAC[0]['jarakObjSentra'] == "1",
        _dataEnableDisablePAC[0]['jarakObjSentra'] == "1",


        //Widget Add Edit Asset

        _dataHideShowPAC[0]['jenisAsset'] == "1",
        _dataMandatoryPAC[0]['jenisAsset'] == "1",
        _dataEnableDisablePAC[0]['jenisAsset'] == "1",


        _dataHideShowPAC[0]['nilai'] == "1",
        _dataMandatoryPAC[0]['nilai'] == "1",
        _dataEnableDisablePAC[0]['nilai'] == "1",


        _dataHideShowPAC[0]['kepemilikan'] == "1",
        _dataMandatoryPAC[0]['kepemilikan'] == "1",
        _dataEnableDisablePAC[0]['kepemilikan'] == "1",


        _dataHideShowPAC[0]['ukuranLuasTanah'] == "1",
        _dataMandatoryPAC[0]['ukuranLuasTanah'] == "1",
        _dataEnableDisablePAC[0]['ukuranLuasTanah'] == "1",


        _dataHideShowPAC[0]['jenisJalan'] == "1",
        _dataMandatoryPAC[0]['jenisJalan'] == "1",
        _dataEnableDisablePAC[0]['jenisJalan'] == "1",


        _dataHideShowPAC[0]['listrik'] == "1",
        _dataMandatoryPAC[0]['listrik'] == "1",
        _dataEnableDisablePAC[0]['listrik'] == "1",


        _dataHideShowPAC[0]['tagihanListrik'] == "1",
        _dataMandatoryPAC[0]['tagihanListrik'] == "1",
        _dataEnableDisablePAC[0]['tagihanListrik'] == "1",


        _dataHideShowPAC[0]['tglAkhirSewa'] == "1",
        _dataMandatoryPAC[0]['tglAkhirSewa'] == "1",
        _dataEnableDisablePAC[0]['tglAkhirSewa'] == "1",


        _dataHideShowPAC[0]['lamaTinggal'] == "1",
        _dataMandatoryPAC[0]['lamaTinggal'] == "1",
        _dataEnableDisablePAC[0]['lamaTinggal'] == "1",


        //Widget Add Edit Survey Detail
        _dataHideShowPAC[0]['infoLingkungan'] == "1",
        _dataMandatoryPAC[0]['infoLingkungan'] == "1",
        _dataEnableDisablePAC[0]['infoLingkungan'] == "1",


        _dataHideShowPAC[0]['sumberInformasi'] == "1",
        _dataMandatoryPAC[0]['sumberInformasi'] == "1",
        _dataEnableDisablePAC[0]['sumberInformasi'] == "1",


        _dataHideShowPAC[0]['namaSumberInformasi'] == "1",
        _dataMandatoryPAC[0]['namaSumberInformasi'] == "1",
        _dataEnableDisablePAC[0]['namaSumberInformasi'] == "1",

      );

      List _dataHideShowResurvey = await getHideShowResurvey();
      List _dataMandatoryResurvey = await getMandatoryResurvey();
      List _dataEnableDisableResurvey = await getEnabledResurvey();
      _showMandatoryResultSurveyResurvey = ShowMandatoryEnabledResultSurvey(

        //Widget Result Survey Group Note

        _dataHideShowResurvey[0]['hasilSurvey'] == "1",
        _dataMandatoryResurvey[0]['hasilSurvey'] == "1",
        _dataEnableDisableResurvey[0]['hasilSurvey'] == "1",


        _dataHideShowResurvey[0]['recomSurvey'] == "1",
        _dataMandatoryResurvey[0]['recomSurvey'] == "1",
        _dataEnableDisableResurvey[0]['recomSurvey'] == "1",


        _dataHideShowResurvey[0]['catatan'] == "1",
        _dataMandatoryResurvey[0]['catatan'] == "1",
        _dataEnableDisableResurvey[0]['catatan'] == "1",


        _dataHideShowResurvey[0]['tglHasilSurvey'] == "1",
        _dataMandatoryResurvey[0]['tglHasilSurvey'] == "1",
        _dataEnableDisableResurvey[0]['tglHasilSurvey'] == "1",


        //Widget Result Survey Photo

        _dataHideShowResurvey[0]['jenisFoto'] == "1",
        _dataMandatoryResurvey[0]['jenisFoto'] == "1",
        _dataEnableDisableResurvey[0]['jenisFoto'] == "1",


        //Widget Result Survey Group Location

        _dataHideShowResurvey[0]['jenisSurvey'] == "1",
        _dataMandatoryResurvey[0]['jenisSurvey'] == "1",
        _dataEnableDisableResurvey[0]['jenisSurvey'] == "1",


        _dataHideShowResurvey[0]['jarakSentra'] == "1",
        _dataMandatoryResurvey[0]['jarakSentra'] == "1",
        _dataEnableDisableResurvey[0]['jarakSentra'] == "1",


        _dataHideShowResurvey[0]['jarakDealer'] == "1",
        _dataMandatoryResurvey[0]['jarakDealer'] == "1",
        _dataEnableDisableResurvey[0]['jarakDealer'] == "1",


        _dataHideShowResurvey[0]['jarakObjSentra'] == "1",
        _dataMandatoryResurvey[0]['jarakObjSentra'] == "1",
        _dataEnableDisableResurvey[0]['jarakObjSentra'] == "1",


        //Widget Add Edit Asset

        _dataHideShowResurvey[0]['jenisAsset'] == "1",
        _dataMandatoryResurvey[0]['jenisAsset'] == "1",
        _dataEnableDisableResurvey[0]['jenisAsset'] == "1",


        _dataHideShowResurvey[0]['nilai'] == "1",
        _dataMandatoryResurvey[0]['nilai'] == "1",
        _dataEnableDisableResurvey[0]['nilai'] == "1",


        _dataHideShowResurvey[0]['kepemilikan'] == "1",
        _dataMandatoryResurvey[0]['kepemilikan'] == "1",
        _dataEnableDisableResurvey[0]['kepemilikan'] == "1",


        _dataHideShowResurvey[0]['ukuranLuasTanah'] == "1",
        _dataMandatoryResurvey[0]['ukuranLuasTanah'] == "1",
        _dataEnableDisableResurvey[0]['ukuranLuasTanah'] == "1",


        _dataHideShowResurvey[0]['jenisJalan'] == "1",
        _dataMandatoryResurvey[0]['jenisJalan'] == "1",
        _dataEnableDisableResurvey[0]['jenisJalan'] == "1",


        _dataHideShowResurvey[0]['listrik'] == "1",
        _dataMandatoryResurvey[0]['listrik'] == "1",
        _dataEnableDisableResurvey[0]['listrik'] == "1",


        _dataHideShowResurvey[0]['tagihanListrik'] == "1",
        _dataMandatoryResurvey[0]['tagihanListrik'] == "1",
        _dataEnableDisableResurvey[0]['tagihanListrik'] == "1",


        _dataHideShowResurvey[0]['tglAkhirSewa'] == "1",
        _dataMandatoryResurvey[0]['tglAkhirSewa'] == "1",
        _dataEnableDisableResurvey[0]['tglAkhirSewa'] == "1",


        _dataHideShowResurvey[0]['lamaTinggal'] == "1",
        _dataMandatoryResurvey[0]['lamaTinggal'] == "1",
        _dataEnableDisableResurvey[0]['lamaTinggal'] == "1",

        //Widget Add Edit Survey Detail
        _dataHideShowResurvey[0]['infoLingkungan'] == "1",
        _dataMandatoryResurvey[0]['infoLingkungan'] == "1",
        _dataEnableDisableResurvey[0]['infoLingkungan'] == "1",


        _dataHideShowResurvey[0]['sumberInformasi'] == "1",
        _dataMandatoryResurvey[0]['sumberInformasi'] == "1",
        _dataEnableDisableResurvey[0]['sumberInformasi'] == "1",


        _dataHideShowResurvey[0]['namaSumberInformasi'] == "1",
        _dataMandatoryResurvey[0]['namaSumberInformasi'] == "1",
        _dataEnableDisableResurvey[0]['namaSumberInformasi'] == "1",

      );

      List _dataHideShowDakor = await getHideShowDakor();
      List _dataMandatoryDakor = await getMandatoryDakor();
      List _dataEnableDisableDakor = await getEnabledDakor();
      _showMandatoryResultSurveyDakor = ShowMandatoryEnabledResultSurvey(

        //Widget Result Survey Group Note

        _dataHideShowDakor[0]['HASIL_SURVEY'] == "1",
        _dataMandatoryDakor[0]['HASIL_SURVEY'] == "1",
        _dataEnableDisableDakor[0]['HASIL_SURVEY'] == "1",


        _dataHideShowDakor[0]['REKOMENDASI_SURVEY'] == "1",
        _dataMandatoryDakor[0]['REKOMENDASI_SURVEY'] == "1",
        _dataEnableDisableDakor[0]['REKOMENDASI_SURVEY'] == "1",


        _dataHideShowDakor[0]['CATATAN'] == "1",
        _dataMandatoryDakor[0]['CATATAN'] == "1",
        _dataEnableDisableDakor[0]['CATATAN'] == "1",


        _dataHideShowDakor[0]['TANGGAL_HASIL_SURVEY'] == "1",
        _dataMandatoryDakor[0]['TANGGAL_HASIL_SURVEY'] == "1",
        _dataEnableDisableDakor[0]['TANGGAL_HASIL_SURVEY'] == "1",


        //Widget Result Survey Photo

        _dataHideShowDakor[0]['JENIS_FOTO'] == "1",
        _dataMandatoryDakor[0]['JENIS_FOTO'] == "1",
        _dataEnableDisableDakor[0]['JENIS_FOTO'] == "1",


        //Widget Result Survey Group Location

        _dataHideShowDakor[0]['JENIS_SURVEY'] == "1",
        _dataMandatoryDakor[0]['JENIS_SURVEY'] == "1",
        _dataEnableDisableDakor[0]['JENIS_SURVEY'] == "1",


        _dataHideShowDakor[0]['JARAK_LOKASI_DENGAN_SENTRA'] == "1",
        _dataMandatoryDakor[0]['JARAK_LOKASI_DENGAN_SENTRA'] == "1",
        _dataEnableDisableDakor[0]['JARAK_LOKASI_DENGAN_SENTRA'] == "1",


        _dataHideShowDakor[0]['JARAK_LOKASI_DENGAN_DEALER'] == "1",
        _dataMandatoryDakor[0]['JARAK_LOKASI_DENGAN_DEALER'] == "1",
        _dataEnableDisableDakor[0]['JARAK_LOKASI_DENGAN_DEALER'] == "1",


        _dataHideShowDakor[0]['JRK_LOK_PENGGUNA_OBJSENTRA'] == "1",
        _dataMandatoryDakor[0]['JRK_LOK_PENGGUNA_OBJSENTRA'] == "1",
        _dataEnableDisableDakor[0]['JRK_LOK_PENGGUNA_OBJSENTRA'] == "1",


        //Widget Add Edit Asset

        _dataHideShowDakor[0]['JENIS_ASET'] == "1",
        _dataMandatoryDakor[0]['JENIS_ASET'] == "1",
        _dataEnableDisableDakor[0]['JENIS_ASET'] == "1",


        _dataHideShowDakor[0]['NILAI'] == "1",
        _dataMandatoryDakor[0]['NILAI'] == "1",
        _dataEnableDisableDakor[0]['NILAI'] == "1",

        _dataHideShowDakor[0]['KEPEMILIKAN'] == "1",
        _dataMandatoryDakor[0]['KEPEMILIKAN'] == "1",
        _dataEnableDisableDakor[0]['KEPEMILIKAN'] == "1",


        _dataHideShowDakor[0]['UKURAN_LUAS_TANAH'] == "1",
        _dataMandatoryDakor[0]['UKURAN_LUAS_TANAH'] == "1",
        _dataEnableDisableDakor[0]['UKURAN_LUAS_TANAH'] == "1",


        _dataHideShowDakor[0]['JENIS_JALAN'] == "1",
        _dataMandatoryDakor[0]['JENIS_JALAN'] == "1",
        _dataEnableDisableDakor[0]['JENIS_JALAN'] == "1",


        _dataHideShowDakor[0]['LISTRIK'] == "1",
        _dataMandatoryDakor[0]['LISTRIK'] == "1",
        _dataEnableDisableDakor[0]['LISTRIK'] == "1",


        _dataHideShowDakor[0]['TAGIHAN_LISTRIK'] == "1",
        _dataMandatoryDakor[0]['TAGIHAN_LISTRIK'] == "1",
        _dataEnableDisableDakor[0]['TAGIHAN_LISTRIK'] == "1",


        _dataHideShowDakor[0]['TANGGAL_AKHIR_KONTRAK_SEWA'] == "1",
        _dataMandatoryDakor[0]['TANGGAL_AKHIR_KONTRAK_SEWA'] == "1",
        _dataEnableDisableDakor[0]['TANGGAL_AKHIR_KONTRAK_SEWA'] == "1",


        _dataHideShowDakor[0]['LAMA_TINGGAL'] == "1",
        _dataMandatoryDakor[0]['LAMA_TINGGAL'] == "1",
        _dataEnableDisableDakor[0]['LAMA_TINGGAL'] == "1",


        //Widget Add Edit Survey Detail
        _dataHideShowDakor[0]['INFORMASI_LINGKUNGAN'] == "1",
        _dataMandatoryDakor[0]['INFORMASI_LINGKUNGAN'] == "1",
        _dataEnableDisableDakor[0]['INFORMASI_LINGKUNGAN'] == "1",


        _dataHideShowDakor[0]['SUMBER_INFORMASI'] == "1",
        _dataMandatoryDakor[0]['SUMBER_INFORMASI'] == "1",
        _dataEnableDisableDakor[0]['SUMBER_INFORMASI'] == "1",


        _dataHideShowDakor[0]['NAMA_SUMBER_INFORMASI'] == "1",
        _dataMandatoryDakor[0]['NAMA_SUMBER_INFORMASI'] == "1",
        _dataEnableDisableDakor[0]['NAMA_SUMBER_INFORMASI'] == "1",
      );

      List _dataHideShowComRegularSurvey = await getHideShowComRegularSurvey();
      List _dataMandatoryComRegularSurvey = await getMandatoryComRegularSurvey();
      List _dataEnableDisableComRegularSurvey = await getEnabledComRegularSurvey();
      _showMandatoryResultSurveyComRegularSurvey = ShowMandatoryEnabledResultSurvey(

        //Widget Result Survey Group Note

        _dataHideShowComRegularSurvey[0]['hasilSurvey'] == "1",
        _dataMandatoryComRegularSurvey[0]['hasilSurvey'] == "1",
        _dataEnableDisableComRegularSurvey[0]['hasilSurvey'] == "1",


        _dataHideShowComRegularSurvey[0]['recomSurvey'] == "1",
        _dataMandatoryComRegularSurvey[0]['recomSurvey'] == "1",
        _dataEnableDisableComRegularSurvey[0]['recomSurvey'] == "1",


        _dataHideShowComRegularSurvey[0]['catatan'] == "1",
        _dataMandatoryComRegularSurvey[0]['catatan'] == "1",
        _dataEnableDisableComRegularSurvey[0]['catatan'] == "1",


        _dataHideShowComRegularSurvey[0]['tglHasilSurvey'] == "1",
        _dataMandatoryComRegularSurvey[0]['tglHasilSurvey'] == "1",
        _dataEnableDisableComRegularSurvey[0]['tglHasilSurvey'] == "1",


        //Widget Result Survey Photo

        _dataHideShowComRegularSurvey[0]['jenisFoto'] == "1",
        _dataMandatoryComRegularSurvey[0]['jenisFoto'] == "1",
        _dataEnableDisableComRegularSurvey[0]['jenisFoto'] == "1",


        //Widget Result Survey Group Location

        _dataHideShowComRegularSurvey[0]['jenisSurvey'] == "1",
        _dataMandatoryComRegularSurvey[0]['jenisSurvey'] == "1",
        _dataEnableDisableComRegularSurvey[0]['jenisSurvey'] == "1",


        _dataHideShowComRegularSurvey[0]['jarakSentra'] == "1",
        _dataMandatoryComRegularSurvey[0]['jarakSentra'] == "1",
        _dataEnableDisableComRegularSurvey[0]['jarakSentra'] == "1",


        _dataHideShowComRegularSurvey[0]['jarakDealer'] == "1",
        _dataMandatoryComRegularSurvey[0]['jarakDealer'] == "1",
        _dataEnableDisableComRegularSurvey[0]['jarakDealer'] == "1",


        _dataHideShowComRegularSurvey[0]['jarakObjSentra'] == "1",
        _dataMandatoryComRegularSurvey[0]['jarakObjSentra'] == "1",
        _dataEnableDisableComRegularSurvey[0]['jarakObjSentra'] == "1",


        //Widget Add Edit Asset

        _dataHideShowComRegularSurvey[0]['jenisAsset'] == "1",
        _dataMandatoryComRegularSurvey[0]['jenisAsset'] == "1",
        _dataEnableDisableComRegularSurvey[0]['jenisAsset'] == "1",


        _dataHideShowComRegularSurvey[0]['nilai'] == "1",
        _dataMandatoryComRegularSurvey[0]['nilai'] == "1",
        _dataEnableDisableComRegularSurvey[0]['nilai'] == "1",


        _dataHideShowComRegularSurvey[0]['kepemilikan'] == "1",
        _dataMandatoryComRegularSurvey[0]['kepemilikan'] == "1",
        _dataEnableDisableComRegularSurvey[0]['kepemilikan'] == "1",


        _dataHideShowComRegularSurvey[0]['ukuranLuasTanah'] == "1",
        _dataMandatoryComRegularSurvey[0]['ukuranLuasTanah'] == "1",
        _dataEnableDisableComRegularSurvey[0]['ukuranLuasTanah'] == "1",


        _dataHideShowComRegularSurvey[0]['jenisJalan'] == "1",
        _dataMandatoryComRegularSurvey[0]['jenisJalan'] == "1",
        _dataEnableDisableComRegularSurvey[0]['jenisJalan'] == "1",


        _dataHideShowComRegularSurvey[0]['listrik'] == "1",
        _dataMandatoryComRegularSurvey[0]['listrik'] == "1",
        _dataEnableDisableComRegularSurvey[0]['listrik'] == "1",


        _dataHideShowComRegularSurvey[0]['tagihanListrik'] == "1",
        _dataMandatoryComRegularSurvey[0]['tagihanListrik'] == "1",
        _dataEnableDisableComRegularSurvey[0]['tagihanListrik'] == "1",


        _dataHideShowComRegularSurvey[0]['tglAkhirSewa'] == "1",
        _dataMandatoryComRegularSurvey[0]['tglAkhirSewa'] == "1",
        _dataEnableDisableComRegularSurvey[0]['tglAkhirSewa'] == "1",


        _dataHideShowComRegularSurvey[0]['lamaTinggal'] == "1",
        _dataMandatoryComRegularSurvey[0]['lamaTinggal'] == "1",
        _dataEnableDisableComRegularSurvey[0]['lamaTinggal'] == "1",


        //Widget Add Edit Survey Detail
        _dataHideShowComRegularSurvey[0]['infoLingkungan'] == "1",
        _dataMandatoryComRegularSurvey[0]['infoLingkungan'] == "1",
        _dataEnableDisableComRegularSurvey[0]['infoLingkungan'] == "1",


        _dataHideShowComRegularSurvey[0]['sumberInformasi'] == "1",
        _dataMandatoryComRegularSurvey[0]['sumberInformasi'] == "1",
        _dataEnableDisableComRegularSurvey[0]['sumberInformasi'] == "1",


        _dataHideShowComRegularSurvey[0]['namaSumberInformasi'] == "1",
        _dataMandatoryComRegularSurvey[0]['namaSumberInformasi'] == "1",
        _dataEnableDisableComRegularSurvey[0]['namaSumberInformasi'] == "1",

      );

      List _dataHideShowComPAC = await getHideShowComPAC();
      List _dataMandatoryComPAC = await getMandatoryComPAC();
      List _dataEnableDisableComPAC = await getEnabledComPAC();
      _showMandatoryResultSurveyComPAC = ShowMandatoryEnabledResultSurvey(

        //Widget Result Survey Group Note

        _dataHideShowComPAC[0]['hasilSurvey'] == "1",
        _dataMandatoryComPAC[0]['hasilSurvey'] == "1",
        _dataEnableDisableComPAC[0]['hasilSurvey'] == "1",


        _dataHideShowComPAC[0]['recomSurvey'] == "1",
        _dataMandatoryComPAC[0]['recomSurvey'] == "1",
        _dataEnableDisableComPAC[0]['recomSurvey'] == "1",


        _dataHideShowComPAC[0]['catatan'] == "1",
        _dataMandatoryComPAC[0]['catatan'] == "1",
        _dataEnableDisableComPAC[0]['catatan'] == "1",


        _dataHideShowComPAC[0]['tglHasilSurvey'] == "1",
        _dataMandatoryComPAC[0]['tglHasilSurvey'] == "1",
        _dataEnableDisableComPAC[0]['tglHasilSurvey'] == "1",


        //Widget Result Survey Photo

        _dataHideShowComPAC[0]['jenisFoto'] == "1",
        _dataMandatoryComPAC[0]['jenisFoto'] == "1",
        _dataEnableDisableComPAC[0]['jenisFoto'] == "1",


        //Widget Result Survey Group Location

        _dataHideShowComPAC[0]['jenisSurvey'] == "1",
        _dataMandatoryComPAC[0]['jenisSurvey'] == "1",
        _dataEnableDisableComPAC[0]['jenisSurvey'] == "1",


        _dataHideShowComPAC[0]['jarakSentra'] == "1",
        _dataMandatoryComPAC[0]['jarakSentra'] == "1",
        _dataEnableDisableComPAC[0]['jarakSentra'] == "1",


        _dataHideShowComPAC[0]['jarakDealer'] == "1",
        _dataMandatoryComPAC[0]['jarakDealer'] == "1",
        _dataEnableDisableComPAC[0]['jarakDealer'] == "1",


        _dataHideShowComPAC[0]['jarakObjSentra'] == "1",
        _dataMandatoryComPAC[0]['jarakObjSentra'] == "1",
        _dataEnableDisableComPAC[0]['jarakObjSentra'] == "1",


        //Widget Add Edit Asset

        _dataHideShowComPAC[0]['jenisAsset'] == "1",
        _dataMandatoryComPAC[0]['jenisAsset'] == "1",
        _dataEnableDisableComPAC[0]['jenisAsset'] == "1",


        _dataHideShowComPAC[0]['nilai'] == "1",
        _dataMandatoryComPAC[0]['nilai'] == "1",
        _dataEnableDisableComPAC[0]['nilai'] == "1",


        _dataHideShowComPAC[0]['kepemilikan'] == "1",
        _dataMandatoryComPAC[0]['kepemilikan'] == "1",
        _dataEnableDisableComPAC[0]['kepemilikan'] == "1",


        _dataHideShowComPAC[0]['ukuranLuasTanah'] == "1",
        _dataMandatoryComPAC[0]['ukuranLuasTanah'] == "1",
        _dataEnableDisableComPAC[0]['ukuranLuasTanah'] == "1",


        _dataHideShowComPAC[0]['jenisJalan'] == "1",
        _dataMandatoryComPAC[0]['jenisJalan'] == "1",
        _dataEnableDisableComPAC[0]['jenisJalan'] == "1",


        _dataHideShowComPAC[0]['listrik'] == "1",
        _dataMandatoryComPAC[0]['listrik'] == "1",
        _dataEnableDisableComPAC[0]['listrik'] == "1",


        _dataHideShowComPAC[0]['tagihanListrik'] == "1",
        _dataMandatoryComPAC[0]['tagihanListrik'] == "1",
        _dataEnableDisableComPAC[0]['tagihanListrik'] == "1",


        _dataHideShowComPAC[0]['tglAkhirSewa'] == "1",
        _dataMandatoryComPAC[0]['tglAkhirSewa'] == "1",
        _dataEnableDisableComPAC[0]['tglAkhirSewa'] == "1",


        _dataHideShowComPAC[0]['lamaTinggal'] == "1",
        _dataMandatoryComPAC[0]['lamaTinggal'] == "1",
        _dataEnableDisableComPAC[0]['lamaTinggal'] == "1",


        //Widget Add Edit Survey Detail
        _dataHideShowComPAC[0]['infoLingkungan'] == "1",
        _dataMandatoryComPAC[0]['infoLingkungan'] == "1",
        _dataEnableDisableComPAC[0]['infoLingkungan'] == "1",


        _dataHideShowComPAC[0]['sumberInformasi'] == "1",
        _dataMandatoryComPAC[0]['sumberInformasi'] == "1",
        _dataEnableDisableComPAC[0]['sumberInformasi'] == "1",


        _dataHideShowComPAC[0]['namaSumberInformasi'] == "1",
        _dataMandatoryComPAC[0]['namaSumberInformasi'] == "1",
        _dataEnableDisableComPAC[0]['namaSumberInformasi'] == "1",

      );

      List _dataHideShowComResurvey = await getHideShowComResurvey();
      List _dataMandatoryComResurvey = await getMandatoryComResurvey();
      List _dataEnableDisableComResurvey = await getEnabledComResurvey();
      _showMandatoryResultSurveyComResurvey = ShowMandatoryEnabledResultSurvey(

        //Widget Result Survey Group Note

        _dataHideShowComResurvey[0]['hasilSurvey'] == "1",
        _dataMandatoryComResurvey[0]['hasilSurvey'] == "1",
        _dataEnableDisableComResurvey[0]['hasilSurvey'] == "1",


        _dataHideShowComResurvey[0]['recomSurvey'] == "1",
        _dataMandatoryComResurvey[0]['recomSurvey'] == "1",
        _dataEnableDisableComResurvey[0]['recomSurvey'] == "1",


        _dataHideShowComResurvey[0]['catatan'] == "1",
        _dataMandatoryComResurvey[0]['catatan'] == "1",
        _dataEnableDisableComResurvey[0]['catatan'] == "1",


        _dataHideShowComResurvey[0]['tglHasilSurvey'] == "1",
        _dataMandatoryComResurvey[0]['tglHasilSurvey'] == "1",
        _dataEnableDisableComResurvey[0]['tglHasilSurvey'] == "1",


        //Widget Result Survey Photo

        _dataHideShowComResurvey[0]['jenisFoto'] == "1",
        _dataMandatoryComResurvey[0]['jenisFoto'] == "1",
        _dataEnableDisableComResurvey[0]['jenisFoto'] == "1",


        //Widget Result Survey Group Location

        _dataHideShowComResurvey[0]['jenisSurvey'] == "1",
        _dataMandatoryComResurvey[0]['jenisSurvey'] == "1",
        _dataEnableDisableComResurvey[0]['jenisSurvey'] == "1",


        _dataHideShowComResurvey[0]['jarakSentra'] == "1",
        _dataMandatoryComResurvey[0]['jarakSentra'] == "1",
        _dataEnableDisableComResurvey[0]['jarakSentra'] == "1",


        _dataHideShowComResurvey[0]['jarakDealer'] == "1",
        _dataMandatoryComResurvey[0]['jarakDealer'] == "1",
        _dataEnableDisableComResurvey[0]['jarakDealer'] == "1",


        _dataHideShowComResurvey[0]['jarakObjSentra'] == "1",
        _dataMandatoryComResurvey[0]['jarakObjSentra'] == "1",
        _dataEnableDisableComResurvey[0]['jarakObjSentra'] == "1",


        //Widget Add Edit Asset

        _dataHideShowComResurvey[0]['jenisAsset'] == "1",
        _dataMandatoryComResurvey[0]['jenisAsset'] == "1",
        _dataEnableDisableComResurvey[0]['jenisAsset'] == "1",


        _dataHideShowComResurvey[0]['nilai'] == "1",
        _dataMandatoryComResurvey[0]['nilai'] == "1",
        _dataEnableDisableComResurvey[0]['nilai'] == "1",


        _dataHideShowComResurvey[0]['kepemilikan'] == "1",
        _dataMandatoryComResurvey[0]['kepemilikan'] == "1",
        _dataEnableDisableComResurvey[0]['kepemilikan'] == "1",


        _dataHideShowComResurvey[0]['ukuranLuasTanah'] == "1",
        _dataMandatoryComResurvey[0]['ukuranLuasTanah'] == "1",
        _dataEnableDisableComResurvey[0]['ukuranLuasTanah'] == "1",


        _dataHideShowComResurvey[0]['jenisJalan'] == "1",
        _dataMandatoryComResurvey[0]['jenisJalan'] == "1",
        _dataEnableDisableComResurvey[0]['jenisJalan'] == "1",


        _dataHideShowComResurvey[0]['listrik'] == "1",
        _dataMandatoryComResurvey[0]['listrik'] == "1",
        _dataEnableDisableComResurvey[0]['listrik'] == "1",


        _dataHideShowComResurvey[0]['tagihanListrik'] == "1",
        _dataMandatoryComResurvey[0]['tagihanListrik'] == "1",
        _dataEnableDisableComResurvey[0]['tagihanListrik'] == "1",


        _dataHideShowComResurvey[0]['tglAkhirSewa'] == "1",
        _dataMandatoryComResurvey[0]['tglAkhirSewa'] == "1",
        _dataEnableDisableComResurvey[0]['tglAkhirSewa'] == "1",


        _dataHideShowComResurvey[0]['lamaTinggal'] == "1",
        _dataMandatoryComResurvey[0]['lamaTinggal'] == "1",
        _dataEnableDisableComResurvey[0]['lamaTinggal'] == "1",


        //Widget Add Edit Survey Detail
        _dataHideShowComResurvey[0]['infoLingkungan'] == "1",
        _dataMandatoryComResurvey[0]['infoLingkungan'] == "1",
        _dataEnableDisableComResurvey[0]['infoLingkungan'] == "1",


        _dataHideShowComResurvey[0]['sumberInformasi'] == "1",
        _dataMandatoryComResurvey[0]['sumberInformasi'] == "1",
        _dataEnableDisableComResurvey[0]['sumberInformasi'] == "1",


        _dataHideShowComResurvey[0]['namaSumberInformasi'] == "1",
        _dataMandatoryComResurvey[0]['namaSumberInformasi'] == "1",
        _dataEnableDisableComResurvey[0]['namaSumberInformasi'] == "1",

      );

      List _dataHideShowComDakor = await getHideShowComDakor();
      List _dataMandatoryComDakor = await getMandatoryComDakor();
      List _dataEnableDisableComDakor = await getEnabledComDakor();
      _showMandatoryResultSurveyComDakor = ShowMandatoryEnabledResultSurvey(

        //Widget Result Survey Group Note

        _dataHideShowComDakor[0]['hasilSurvey'] == "1",
        _dataMandatoryComDakor[0]['hasilSurvey'] == "1",
        _dataEnableDisableComDakor[0]['hasilSurvey'] == "1",


        _dataHideShowComDakor[0]['recomSurvey'] == "1",
        _dataMandatoryComDakor[0]['recomSurvey'] == "1",
        _dataEnableDisableComDakor[0]['recomSurvey'] == "1",


        _dataHideShowComDakor[0]['catatan'] == "1",
        _dataMandatoryComDakor[0]['catatan'] == "1",
        _dataEnableDisableComDakor[0]['catatan'] == "1",


        _dataHideShowComDakor[0]['tglHasilSurvey'] == "1",
        _dataMandatoryComDakor[0]['tglHasilSurvey'] == "1",
        _dataEnableDisableComDakor[0]['tglHasilSurvey'] == "1",


        //Widget Result Survey Photo

        _dataHideShowComDakor[0]['jenisFoto'] == "1",
        _dataMandatoryComDakor[0]['jenisFoto'] == "1",
        _dataEnableDisableComDakor[0]['jenisFoto'] == "1",


        //Widget Result Survey Group Location

        _dataHideShowComDakor[0]['jenisSurvey'] == "1",
        _dataMandatoryComDakor[0]['jenisSurvey'] == "1",
        _dataEnableDisableComDakor[0]['jenisSurvey'] == "1",


        _dataHideShowComDakor[0]['jarakSentra'] == "1",
        _dataMandatoryComDakor[0]['jarakSentra'] == "1",
        _dataEnableDisableComDakor[0]['jarakSentra'] == "1",


        _dataHideShowComDakor[0]['jarakDealer'] == "1",
        _dataMandatoryComDakor[0]['jarakDealer'] == "1",
        _dataEnableDisableComDakor[0]['jarakDealer'] == "1",


        _dataHideShowComDakor[0]['jarakObjSentra'] == "1",
        _dataMandatoryComDakor[0]['jarakObjSentra'] == "1",
        _dataEnableDisableComDakor[0]['jarakObjSentra'] == "1",


        //Widget Add Edit Asset

        _dataHideShowComDakor[0]['jenisAsset'] == "1",
        _dataMandatoryComDakor[0]['jenisAsset'] == "1",
        _dataEnableDisableComDakor[0]['jenisAsset'] == "1",


        _dataHideShowComDakor[0]['nilai'] == "1",
        _dataMandatoryComDakor[0]['nilai'] == "1",
        _dataEnableDisableComDakor[0]['nilai'] == "1",


        _dataHideShowComDakor[0]['kepemilikan'] == "1",
        _dataMandatoryComDakor[0]['kepemilikan'] == "1",
        _dataEnableDisableComDakor[0]['kepemilikan'] == "1",


        _dataHideShowComDakor[0]['ukuranLuasTanah'] == "1",
        _dataMandatoryComDakor[0]['ukuranLuasTanah'] == "1",
        _dataEnableDisableComDakor[0]['ukuranLuasTanah'] == "1",


        _dataHideShowComDakor[0]['jenisJalan'] == "1",
        _dataMandatoryComDakor[0]['jenisJalan'] == "1",
        _dataEnableDisableComDakor[0]['jenisJalan'] == "1",


        _dataHideShowComDakor[0]['listrik'] == "1",
        _dataMandatoryComDakor[0]['listrik'] == "1",
        _dataEnableDisableComDakor[0]['listrik'] == "1",


        _dataHideShowComDakor[0]['tagihanListrik'] == "1",
        _dataMandatoryComDakor[0]['tagihanListrik'] == "1",
        _dataEnableDisableComDakor[0]['tagihanListrik'] == "1",


        _dataHideShowComDakor[0]['tglAkhirSewa'] == "1",
        _dataMandatoryComDakor[0]['tglAkhirSewa'] == "1",
        _dataEnableDisableComDakor[0]['tglAkhirSewa'] == "1",


        _dataHideShowComDakor[0]['lamaTinggal'] == "1",
        _dataMandatoryComDakor[0]['lamaTinggal'] == "1",
        _dataEnableDisableComDakor[0]['lamaTinggal'] == "1",


        //Widget Add Edit Survey Detail
        _dataHideShowComDakor[0]['infoLingkungan'] == "1",
        _dataMandatoryComDakor[0]['infoLingkungan'] == "1",
        _dataEnableDisableComDakor[0]['infoLingkungan'] == "1",


        _dataHideShowComDakor[0]['sumberInformasi'] == "1",
        _dataMandatoryComDakor[0]['sumberInformasi'] == "1",
        _dataEnableDisableComDakor[0]['sumberInformasi'] == "1",


        _dataHideShowComDakor[0]['namaSumberInformasi'] == "1",
        _dataMandatoryComDakor[0]['namaSumberInformasi'] == "1",
        _dataEnableDisableComDakor[0]['namaSumberInformasi'] == "1",

      );
      loadData = false;
    }
    catch(e){
      loadData = false;
      print("catchhhh get show hide mandatory ${e.toString()}");
      throw "${e.toString()}";
      // _showSnackBar(e.toString());
    }
  }

  Future<void> setShowHideMandatoryFormM(BuildContext context) async{
    var _providerInfoNasabah = Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false);
    var _providerInformasiKeluargaIBU = Provider.of<FormMInformasiKeluargaIBUChangeNotif>(context, listen: false);
    var _providerRincianCompany = Provider.of<FormMCompanyRincianChangeNotifier>(context, listen: false);
    SharedPreferences _preference = await SharedPreferences.getInstance();
    String _custType = _preference.getString("cust_type");
    String _lastKnownState = _preference.getString("last_known_state");
    if(_custType == "PER") {
      print("masuk per");
      if(_lastKnownState == "IDE") {
        print("masuk ide");
        //foto hideshow dan mandatory
        _showMandatoryFotoModel = ShowMandatoryFotoModel(
          _dataHideShowIde[0]['fototempattinggal']== "1",
          _dataHideShowIde[0]['pekerjaanfoto']== "1",
          _dataHideShowIde[0]['fotousaha']== "1",
          _dataHideShowIde[0]['jenis_PEMBIAYAAN']== "1",
          _dataHideShowIde[0]['kegiatanusaha']== "1",
          _dataHideShowIde[0]['jeniskegiatanusaha']== "1",
          _dataHideShowIde[0]['jeniskonsep']== "1",
          _dataHideShowIde[0]['grupobjekunit']== "1",
          _dataHideShowIde[0]['fotoobjekpembiayaanunit']== "1",
          _dataHideShowIde[0]['dokumen']== "1",
          _dataMandatoryIde[0]['fototempattinggal']== "1",
          _dataMandatoryIde[0]['pekerjaanfoto']== "1",
          _dataMandatoryIde[0]['fotousaha']== "1",
          _dataMandatoryIde[0]['jenis_PEMBIAYAAN']== "1",
          _dataMandatoryIde[0]['kegiatanusaha']== "1",
          _dataMandatoryIde[0]['jeniskegiatanusaha']== "1",
          _dataMandatoryIde[0]['jeniskonsep']== "1",
          _dataMandatoryIde[0]['grupobjekunit']== "1",
          _dataMandatoryIde[0]['fotoobjekpembiayaanunit']== "1",
          _dataMandatoryIde[0]['dokumen']== "1",
        );

        //semua alamat hideshow & mandatory
        _setAddressIndividuInfoNasabah = SetAddressIndividu(
            _dataHideShowIde[0]['jenisalamat'] == "1",
            _dataHideShowIde[0]['alamat'] == "1",
            _dataHideShowIde[0]['rt'] == "1",
            _dataHideShowIde[0]['rw'] == "1",
            _dataHideShowIde[0]['kelurahan'] == "1",
            _dataHideShowIde[0]['kecamatan'] == "1",
            _dataHideShowIde[0]['kabupatenkota'] == "1",
            _dataHideShowIde[0]['provinsi'] == "1",
            _dataHideShowIde[0]['kodepos'] == "1",
            _dataHideShowIde[0]['teleponarea'] == "1",
            _dataHideShowIde[0]['telepon'] == "1",
            _dataMandatoryIde[0]['jenisalamat'] == "1",
            _dataMandatoryIde[0]['alamat'] == "1",
            _dataMandatoryIde[0]['rt'] == "1",
            _dataMandatoryIde[0]['rw'] == "1",
            _dataMandatoryIde[0]['kelurahan'] == "1",
            _dataMandatoryIde[0]['kecamatan'] == "1",
            _dataMandatoryIde[0]['kabupatenkota'] == "1",
            _dataMandatoryIde[0]['provinsi'] == "1",
            _dataMandatoryIde[0]['kodepos'] == "1",
            _dataMandatoryIde[0]['teleponarea'] == "1",
            _dataMandatoryIde[0]['telepon'] == "1"
        );
        _setAddressIndividuOccupation = SetAddressIndividu(
            _dataHideShowIde[0]['jenisalamatpekerjaan']== "1",
            _dataHideShowIde[0]['alamatpekerjaan']== "1",
            _dataHideShowIde[0]['rtpekerjaan']== "1",
            _dataHideShowIde[0]['rwpekerjaan']== "1",
            _dataHideShowIde[0]['kelurahanpekerjaan']== "1",
            _dataHideShowIde[0]['kecamatanpekerjaan']== "1",
            _dataHideShowIde[0]['kabupatenkotapekerjaan']== "1",
            _dataHideShowIde[0]['provinsipekerjaan']== "1",
            _dataHideShowIde[0]['kodepospekerjaan']== "1",
            _dataHideShowIde[0]['teleponareapekerjaan']== "1",
            _dataHideShowIde[0]['teleponpekerjaan']== "1",
            _dataMandatoryIde[0]['jenisalamatpekerjaan']== "1",
            _dataMandatoryIde[0]['alamatpekerjaan']== "1",
            _dataMandatoryIde[0]['rtpekerjaan']== "1",
            _dataMandatoryIde[0]['rwpekerjaan']== "1",
            _dataMandatoryIde[0]['kelurahanpekerjaan']== "1",
            _dataMandatoryIde[0]['kecamatanpekerjaan']== "1",
            _dataMandatoryIde[0]['kabupatenkotapekerjaan']== "1",
            _dataMandatoryIde[0]['provinsipekerjaan']== "1",
            _dataMandatoryIde[0]['kodepospekerjaan']== "1",
            _dataMandatoryIde[0]['teleponareapekerjaan']== "1",
            _dataMandatoryIde[0]['teleponpekerjaan'] == "1"
        );
        _setAddressIndividuPenjamin = SetAddressIndividu(
            _dataHideShowIde[0]['jenisalamatalamat'] == "1",
            _dataHideShowIde[0]['alamatpribadialamat'] == "1",
            _dataHideShowIde[0]['rtpribadialamat'] == "1",
            _dataHideShowIde[0]['rwpribadialamat'] == "1",
            _dataHideShowIde[0]['kelurahanpribadialamat'] == "1",
            _dataHideShowIde[0]['kecamatanpribadialamat'] == "1",
            _dataHideShowIde[0]['kabupatenkotapribadialamat'] == "1",
            _dataHideShowIde[0]['provinsipribadialamat'] == "1",
            _dataHideShowIde[0]['kodepospribadialamat'] == "1",
            _dataHideShowIde[0]['teleponareapribadialamat'] == "1",
            _dataHideShowIde[0]['teleponpribadialamat'] == "1",
            _dataMandatoryIde[0]['jenisalamatalamat'] == "1",
            _dataMandatoryIde[0]['alamatpribadialamat'] == "1",
            _dataMandatoryIde[0]['rtpribadialamat'] == "1",
            _dataMandatoryIde[0]['rwpribadialamat'] == "1",
            _dataMandatoryIde[0]['kelurahanpribadialamat'] == "1",
            _dataMandatoryIde[0]['kecamatanpribadialamat'] == "1",
            _dataMandatoryIde[0]['kabupatenkotapribadialamat'] == "1",
            _dataMandatoryIde[0]['provinsipribadialamat'] == "1",
            _dataMandatoryIde[0]['kodepospribadialamat'] == "1",
            _dataMandatoryIde[0]['teleponareapribadialamat'] == "1",
            _dataMandatoryIde[0]['teleponpribadialamat'] == "1"
        );
        _setAddressCompanyGuarantorIndividuModel = SetAddressCompanyModel(
          _dataHideShowIde[0]['jenisalamatkelembagaan'] == "1",
          _dataHideShowIde[0]['alamatkelembagaan'] == "1",
          _dataHideShowIde[0]['rtkelembagaan'] == "1",
          _dataHideShowIde[0]['rwkelembagaan'] == "1",
          _dataHideShowIde[0]['kelurahankelembagaan'] == "1",
          _dataHideShowIde[0]['kecamatankelembagaan'] == "1",
          _dataHideShowIde[0]['kabupatenkotakelembagaan'] == "1",
          _dataHideShowIde[0]['provinsikelembagaan'] == "1",
          _dataHideShowIde[0]['kodeposkelembagaan'] == "1",
          _dataHideShowIde[0]['teleponarea1KELEMBAGAAN'] == "1",
          _dataHideShowIde[0]['telepon1KELEMBAGAAN'] == "1",
          _dataHideShowIde[0]['teleponarea2KELEMBAGAAN'] == "1",
          _dataHideShowIde[0]['telepon2KELEMBAGAAN'] == "1",
          _dataHideShowIde[0]['faxareakelembagaan'] == "1",
          _dataHideShowIde[0]['faxkelembagaan'] == "1",
          _dataMandatoryIde[0]['jenis_ALAMAT'] == "1",
          _dataMandatoryIde[0]['alamatkelembagaan'] == "1",
          _dataMandatoryIde[0]['rtkelembagaan'] == "1",
          _dataMandatoryIde[0]['rwkelembagaan'] == "1",
          _dataMandatoryIde[0]['kelurahankelembagaan'] == "1",
          _dataMandatoryIde[0]['kecamatankelembagaan'] == "1",
          _dataMandatoryIde[0]['kabupatenkotakelembagaan'] == "1",
          _dataMandatoryIde[0]['provinsikelembagaan'] == "1",
          _dataMandatoryIde[0]['kodeposkelembagaan'] == "1",
          _dataMandatoryIde[0]['teleponarea1KELEMBAGAAN'] == "1",
          _dataMandatoryIde[0]['telepon1KELEMBAGAAN'] == "1",
          _dataMandatoryIde[0]['teleponarea2KELEMBAGAAN'] == "1",
          _dataMandatoryIde[0]['telepon2KELEMBAGAAN'] == "1",
          _dataMandatoryIde[0]['faxareakelembagaan'] == "1",
          _dataMandatoryIde[0]['faxkelembagaan'] == "1",
        );

        //info nasabah hideshow
        _providerInfoNasabah.isGCVisible  = _dataHideShowIde[0]['gc'] == "1";
        _providerInfoNasabah.isIdentitasModelVisible = _dataHideShowIde[0]['jenisidentitas'] == "1";
        _providerInfoNasabah.isNoIdentitasVisible = _dataHideShowIde[0]['noidentitas'] == "1";
        _providerInfoNasabah.isTglIdentitasVisible = _dataHideShowIde[0]['tanggalidentitas'] == "1";
        _providerInfoNasabah.isIdentitasBerlakuSampaiVisible = _dataHideShowIde[0]['identitasberlaku'] == "1";
        _providerInfoNasabah.isNamaLengkapSesuaiIdentitasVisible = _dataHideShowIde[0]['namalengkapid'] == "1";
        _providerInfoNasabah.isNamaLengkapVisible = _dataHideShowIde[0]['namalengkap'] == "1";
        _providerInfoNasabah.isTglLahirVisible = _dataHideShowIde[0]['tanggallahir'] == "1";
        _providerInfoNasabah.isTempatLahirSesuaiIdentitasVisible = _dataHideShowIde[0]['tempatlahirid'] == "1";
        _providerInfoNasabah.isTempatLahirSesuaiIdentitasLOVVisible = _dataHideShowIde[0]['tempatlahiridlov'] == "1";
        _providerInfoNasabah.isGenderVisible = _dataHideShowIde[0]['jeniskelamin'] == "1";
        _providerInfoNasabah.isReligionVisible = _dataHideShowIde[0]['agama'] == "1";
        _providerInfoNasabah.isEducationSelectedVisible = _dataHideShowIde[0]['pendidikan'] == "1";
        _providerInfoNasabah.isJumlahTanggunganVisible = _dataHideShowIde[0]['jumlahtanggungan'] == "1";
        _providerInfoNasabah.isMaritalStatusSelectedVisible = _dataHideShowIde[0]['statuspernikahan'] == "1";
        _providerInfoNasabah.isNoHpVisible = _dataHideShowIde[0]['handphonepribadi'] == "1";
        _providerInfoNasabah.isNoHp1WAVisible = _dataHideShowIde[0]['nowa1'] == "1";
        _providerInfoNasabah.isHaveNPWPVisible = _dataHideShowIde[0]['punyanpwp'] == "1";
        _providerInfoNasabah.isNamaSesuaiNPWPVisible = _dataHideShowIde[0]['namalengkapsesuainpwp'] == "1";
        _providerInfoNasabah.isJenisNPWPVisible = _dataHideShowIde[0]['jenisnpwp'] == "1";
        _providerInfoNasabah.isTandaPKPVisible = _dataHideShowIde[0]['tandapkp'] == "1";
        _providerInfoNasabah.isAlamatSesuaiNPWPVisible = _dataHideShowIde[0]['alamatnpwp'] == "1";
        _providerInfoNasabah.isNoNPWPVisible = _dataHideShowIde[0]['npwp'] == "1";

        //info nasabah mandatory
        _providerInfoNasabah.isGCMandatory  = _dataMandatoryIde[0]['gc'] == "1";
        _providerInfoNasabah.isIdentitasModelMandatory = _dataMandatoryIde[0]['jenisidentitas'] == "1";
        _providerInfoNasabah.isNoIdentitasMandatory = _dataMandatoryIde[0]['noidentitas'] == "1";
        _providerInfoNasabah.isTglIdentitasMandatory = _dataMandatoryIde[0]['tanggalidentitas'] == "1";
        _providerInfoNasabah.isIdentitasBerlakuSampaiMandatory = _dataMandatoryIde[0]['identitasberlaku'] == "1";
        _providerInfoNasabah.isNamaLengkapSesuaiIdentitasMandatory = _dataMandatoryIde[0]['namalengkapid'] == "1";
        _providerInfoNasabah.isNamaLengkapMandatory = _dataMandatoryIde[0]['namalengkap'] == "1";
        _providerInfoNasabah.isTglLahirMandatory = _dataMandatoryIde[0]['tanggallahir'] == "1";
        _providerInfoNasabah.isTempatLahirSesuaiIdentitasMandatory = _dataMandatoryIde[0]['tempatlahirid'] == "1";
        _providerInfoNasabah.isTempatLahirSesuaiIdentitasLOVMandatory = _dataMandatoryIde[0]['tempatlahiridlov'] == "1";
        _providerInfoNasabah.isGenderMandatory = _dataMandatoryIde[0]['jeniskelamin'] == "1";
        _providerInfoNasabah.isReligionMandatory = _dataMandatoryIde[0]['agama'] == "1";
        _providerInfoNasabah.isEducationSelectedMandatory = _dataMandatoryIde[0]['pendidikan'] == "1";
        _providerInfoNasabah.isJumlahTanggunganMandatory = _dataMandatoryIde[0]['jumlahtanggungan'] == "1";
        _providerInfoNasabah.isMaritalStatusSelectedMandatory = _dataMandatoryIde[0]['statuspernikahan'] == "1";
        _providerInfoNasabah.isNoHpMandatory = _dataMandatoryIde[0]['handphonepribadi'] == "1";
        _providerInfoNasabah.isNoHp1WAMandatory = _dataMandatoryIde[0]['nowa1'] == "1";
        _providerInfoNasabah.isHaveNPWPMandatory = _dataMandatoryIde[0]['punyanpwp'] == "1";
        _providerInfoNasabah.isNamaSesuaiNPWPMandatory = _dataMandatoryIde[0]['namalengkapsesuainpwp'] == "1";
        _providerInfoNasabah.isJenisNPWPMandatory = _dataMandatoryIde[0]['jenisnpwp'] == "1";
        _providerInfoNasabah.isTandaPKPMandatory = _dataMandatoryIde[0]['tandapkp'] == "1";
        _providerInfoNasabah.isAlamatSesuaiNPWPMandatory = _dataMandatoryIde[0]['alamatnpwp'] == "1";
        _providerInfoNasabah.isNoNPWPMandatory = _dataMandatoryIde[0]['npwp'] == "1";

        //info keluarga ibu hideshow
        _providerInformasiKeluargaIBU.isRelationshipStatusVisible = _dataHideShowIde[0]['statushubunganibu'] == "1";
        _providerInformasiKeluargaIBU.isIdentityVisible = _dataHideShowIde[0]['jenisidentitasibu'] == "1";
        _providerInformasiKeluargaIBU.isNoIdentitasVisible = _dataHideShowIde[0]['noidentitasibu'] == "1";
        _providerInformasiKeluargaIBU.isNamaLengkapdentitasVisible = _dataHideShowIde[0]['namalengkapidibu'] == "1";
        _providerInformasiKeluargaIBU.isNamaIdentitasVisible = _dataHideShowIde[0]['namalengkapibu'] == "1";
        _providerInformasiKeluargaIBU.isTglLahirVisible = _dataHideShowIde[0]['tanggallahiribu'] == "1";
        _providerInformasiKeluargaIBU.isTempatLahirSesuaiIdentitasVisible = _dataHideShowIde[0]['tempatlahiridibu'] == "1";
        _providerInformasiKeluargaIBU.isTempatLahirSesuaiIdentitasLOVVisible = _dataHideShowIde[0]['tempatlahiridlovibu'] == "1";
        _providerInformasiKeluargaIBU.isRadioValueGenderVisible = _dataHideShowIde[0]['jeniskelaminibu'] == "1";
        _providerInformasiKeluargaIBU.isKodeAreaVisible = _dataHideShowIde[0]['teleponareaibu'] == "1";
        _providerInformasiKeluargaIBU.isTlpnVisible = _dataHideShowIde[0]['teleponibu'] == "1";
        _providerInformasiKeluargaIBU.isNoHpVisible = _dataHideShowIde[0]['handphoneibu'] == "1";

        //info keluarga ibumandatory
        _providerInformasiKeluargaIBU.isRelationshipStatusMandatory = _dataMandatoryIde[0]['statushubunganibu'] == "1";
        _providerInformasiKeluargaIBU.isIdentityMandatory = _dataMandatoryIde[0]['jenisidentitasibu'] == "1";
        _providerInformasiKeluargaIBU.isNoIdentitasMandatory = _dataMandatoryIde[0]['noidentitasibu'] == "1";
        _providerInformasiKeluargaIBU.isNamaLengkapdentitasMandatory = _dataMandatoryIde[0]['namalengkapidibu'] == "1";
        _providerInformasiKeluargaIBU.isNamaIdentitasMandatory = _dataMandatoryIde[0]['namalengkapibu'] == "1";
        _providerInformasiKeluargaIBU.isTglLahirMandatory = _dataMandatoryIde[0]['tanggallahiribu'] == "1";
        _providerInformasiKeluargaIBU.isTempatLahirSesuaiIdentitasMandatory = _dataMandatoryIde[0]['tempatlahiridibu'] == "1";
        _providerInformasiKeluargaIBU.isTempatLahirSesuaiIdentitasLOVMandatory = _dataMandatoryIde[0]['tempatlahiridlovibu'] == "1";
        _providerInformasiKeluargaIBU.isRadioValueGenderMandatory = _dataMandatoryIde[0]['jeniskelaminibu'] == "1";
        _providerInformasiKeluargaIBU.isKodeAreaMandatory = _dataMandatoryIde[0]['teleponareaibu'] == "1";
        _providerInformasiKeluargaIBU.isTlpnMandatory = _dataMandatoryIde[0]['teleponibu'] == "1";
        _providerInformasiKeluargaIBU.isNoHpMandatory = _dataMandatoryIde[0]['handphoneibu'] == "1";

        print("kode ibu = ${_providerInformasiKeluargaIBU.isKodeAreaMandatory} ===== ${_dataMandatoryIde[0]['teleponareaibu']}");

        //info list keluarga showhide & mandatory
        _setAddInfoKeluarga = SetAddInfoKeluargaModel(
          _dataHideShowIde[0]['statushubunganspouse'] == "1",
          _dataHideShowIde[0]['jenisidentitasspouse'] == "1",
          _dataHideShowIde[0]['noidentitasspouse'] == "1",
          _dataHideShowIde[0]['namalengkapidspouse'] == "1",
          _dataHideShowIde[0]['namalengkapspouse'] == "1",
          _dataHideShowIde[0]['tanggallahirspouse'] == "1",
          _dataHideShowIde[0]['tempatlahiridspouse'] == "1",
          _dataHideShowIde[0]['tempatlahiridlovspouse'] == "1",
          _dataHideShowIde[0]['jeniskelaminspouse'] == "1",
          _dataHideShowIde[0]['teleponareaspouse'] == "1",
          _dataHideShowIde[0]['teleponspouse'] == "1",
          _dataHideShowIde[0]['handphonespouse'] == "1",
          _dataMandatoryIde[0]['statushubunganspouse'] == "1",
          _dataMandatoryIde[0]['jenisidentitasspouse'] == "1",
          _dataMandatoryIde[0]['noidentitasspouse'] == "1",
          _dataMandatoryIde[0]['namalengkapidspouse'] == "1",
          _dataMandatoryIde[0]['namalengkapspouse'] == "1",
          _dataMandatoryIde[0]['tanggallahirspouse'] == "1",
          _dataMandatoryIde[0]['tempatlahiridspouse'] == "1",
          _dataMandatoryIde[0]['tempatlahiridlovspouse'] == "1",
          _dataMandatoryIde[0]['jeniskelaminspouse'] == "1",
          _dataMandatoryIde[0]['teleponareaspouse'] == "1",
          _dataMandatoryIde[0]['teleponspouse'] == "1",
          _dataMandatoryIde[0]['handphonespouse'] == "1",
        );

        //occupation hideshow & mandatory
        _showMandatoryOccupationModel = ShowMandatoryOccupationModel(
          _dataHideShowIde[0]['lamabekerjaberjalan']== "1",
          _dataHideShowIde[0]['namausahawiraswasta']== "1",
          _dataHideShowIde[0]['jenisbadanusahawiraswasta']== "1",
          _dataHideShowIde[0]['sektorekonomiwiraswasta']== "1",
          _dataHideShowIde[0]['lapanganusahawiraswasta']== "1",
          _dataHideShowIde[0]['statuslokasiwiraswasta']== "1",
          _dataHideShowIde[0]['lokasiusahawiraswasta']== "1",
          _dataHideShowIde[0]['totalpegawaiwiraswasta']== "1",
          _dataHideShowIde[0]['totallamabekerjawiraswasta']== "1",
          _dataHideShowIde[0]['jenisprofesiprofesional']== "1",
          _dataHideShowIde[0]['sektorekonomiprofesional']== "1",
          _dataHideShowIde[0]['lapanganusahaprofesional']== "1",
          _dataHideShowIde[0]['statuslokasiprofesional']== "1",
          _dataHideShowIde[0]['lokasiusahaprofesional']== "1",
          _dataHideShowIde[0]['totallamabekerjaprofesional']== "1",
          _dataHideShowIde[0]['namaperusahaanlainnya']== "1",
          _dataHideShowIde[0]['jenisperusahaanlainnya']== "1",
          _dataHideShowIde[0]['sektorekonomilainnya']== "1",
          _dataHideShowIde[0]['lapanganusahalainnya']== "1",
          _dataHideShowIde[0]['totalpegawailainnya']== "1",
          _dataHideShowIde[0]['totallamabekerjalainnya']== "1",
          _dataHideShowIde[0]['jenispephighrisklainnya'] == "1",
          _dataHideShowIde[0]['statuspegawailainnya'] == "1",
          _dataMandatoryIde[0]['lamabekerjaberjalan']== "1",
          _dataMandatoryIde[0]['namausahawiraswasta']== "1",
          _dataMandatoryIde[0]['jenisbadanusahawiraswasta']== "1",
          _dataMandatoryIde[0]['sektorekonomiwiraswasta']== "1",
          _dataMandatoryIde[0]['lapanganusahawiraswasta']== "1",
          _dataMandatoryIde[0]['statuslokasiwiraswasta']== "1",
          _dataMandatoryIde[0]['lokasiusahawiraswasta']== "1",
          _dataMandatoryIde[0]['totalpegawaiwiraswasta']== "1",
          _dataMandatoryIde[0]['totallamabekerjawiraswasta']== "1",
          _dataMandatoryIde[0]['jenisprofesiprofesional']== "1",
          _dataMandatoryIde[0]['sektorekonomiprofesional']== "1",
          _dataMandatoryIde[0]['lapanganusahaprofesional']== "1",
          _dataMandatoryIde[0]['statuslokasiprofesional']== "1",
          _dataMandatoryIde[0]['lokasiusahaprofesional']== "1",
          _dataMandatoryIde[0]['totallamabekerjaprofesional']== "1",
          _dataMandatoryIde[0]['namaperusahaanlainnya']== "1",
          _dataMandatoryIde[0]['jenisperusahaanlainnya']== "1",
          _dataMandatoryIde[0]['sektorekonomilainnya']== "1",
          _dataMandatoryIde[0]['lapanganusahalainnya']== "1",
          _dataMandatoryIde[0]['totalpegawailainnya']== "1",
          _dataMandatoryIde[0]['totallamabekerjalainnya']== "1",
          _dataMandatoryIde[0]['jenispephighrisklainnya'] == "1",
          _dataMandatoryIde[0]['statuspegawailainnya'] == "1",
        );

        //income hideshow & mandatory
        _showMandatoryIncomeModel = ShowMandatoryIncomeModel(
            _dataHideShowIde[0]['incomeperbulanwiraswasta']== "1",
            _dataHideShowIde[0]['incomelainnyawiraswasta']== "1",
            _dataHideShowIde[0]['totalincomewiraswasta']== "1",
            _dataHideShowIde[0]['pokokincomewiraswasta']== "1",
            _dataHideShowIde[0]['labakotorwiraswasta']== "1",
            _dataHideShowIde[0]['biayaoperasionalwiraswasta']== "1",
            _dataHideShowIde[0]['biayalainnyawiraswasta']== "1",
            _dataHideShowIde[0]['netsebelumpajakwiraswasta']== "1",
            _dataHideShowIde[0]['pajakwiraswasta']== "1",
            _dataHideShowIde[0]['netsetelahpajakwiraswasta']== "1",
            _dataHideShowIde[0]['biayahidupwiraswasta']== "1",
            _dataHideShowIde[0]['sisaincomewiraswasta']== "1",
            _dataHideShowIde[0]['incomepasanganwiraswasta']== "1",
            _dataHideShowIde[0]['angsuranlainnyawiraswasta']== "1",
            _dataHideShowIde[0]['incomeprofesional']== "1",
            _dataHideShowIde[0]['incomepasanganprofesional']== "1",
            _dataHideShowIde[0]['incomelainnyaprofesional']== "1",
            _dataHideShowIde[0]['totalincomeprofesional']== "1",
            _dataHideShowIde[0]['biayahidupprofesional']== "1",
            _dataHideShowIde[0]['sisaincomeprofesional']== "1",
            _dataHideShowIde[0]['angsuranlainnyaprofesional']== "1",
            _dataMandatoryIde[0]['incomeperbulanwiraswasta']== "1",
            _dataMandatoryIde[0]['incomelainnyawiraswasta']== "1",
            _dataMandatoryIde[0]['totalincomewiraswasta']== "1",
            _dataMandatoryIde[0]['pokokincomewiraswasta']== "1",
            _dataMandatoryIde[0]['labakotorwiraswasta']== "1",
            _dataMandatoryIde[0]['biayaoperasionalwiraswasta']== "1",
            _dataMandatoryIde[0]['biayalainnyawiraswasta']== "1",
            _dataMandatoryIde[0]['netsebelumpajakwiraswasta']== "1",
            _dataMandatoryIde[0]['pajakwiraswasta']== "1",
            _dataMandatoryIde[0]['netsetelahpajakwiraswasta']== "1",
            _dataMandatoryIde[0]['biayahidupwiraswasta']== "1",
            _dataMandatoryIde[0]['sisaincomewiraswasta']== "1",
            _dataMandatoryIde[0]['incomepasanganwiraswasta']== "1",
            _dataMandatoryIde[0]['angsuranlainnyawiraswasta']== "1",
            _dataMandatoryIde[0]['incomeprofesional']== "1",
            _dataMandatoryIde[0]['incomepasanganprofesional']== "1",
            _dataMandatoryIde[0]['incomelainnyaprofesional']== "1",
            _dataMandatoryIde[0]['totalincomeprofesional']== "1",
            _dataMandatoryIde[0]['biayahidupprofesional']== "1",
            _dataMandatoryIde[0]['sisaincomeprofesional']== "1",
            _dataMandatoryIde[0]['angsuranlainnyaprofesional']== "1"
        );

        //penjamin individu - lembaga hideshow & mandatory
        _setGuarantorIndividu = SetGuarantorIndividu(
          _dataHideShowIde[0]['statushubunganpribadi'] == "1",
          _dataHideShowIde[0]['jenisidentitaspribadi'] == "1",
          _dataHideShowIde[0]['noidentitaspribadi'] == "1",
          _dataHideShowIde[0]['namalengkapidpribadi'] == "1",
          _dataHideShowIde[0]['namalengkappribadi'] == "1",
          _dataHideShowIde[0]['tanggallahirpribadi'] == "1",
          _dataHideShowIde[0]['tempatlahiridpribadi'] == "1",
          _dataHideShowIde[0]['tempatlahiridlovpribadi'] == "1",
          _dataHideShowIde[0]['jeniskelaminpribadi'] == "1",
          _dataHideShowIde[0]['handphonepribadi'] == "1",
          _dataMandatoryIde[0]['statushubunganpribadi'] == "1",
          _dataMandatoryIde[0]['jenisidentitaspribadi'] == "1",
          _dataMandatoryIde[0]['noidentitaspribadi'] == "1",
          _dataMandatoryIde[0]['namalengkapidpribadi'] == "1",
          _dataMandatoryIde[0]['namalengkappribadi'] == "1",
          _dataMandatoryIde[0]['tanggallahirpribadi'] == "1",
          _dataMandatoryIde[0]['tempatlahiridpribadi'] == "1",
          _dataMandatoryIde[0]['tempatlahiridlovpribadi'] == "1",
          _dataMandatoryIde[0]['jeniskelaminpribadi'] == "1",
          _dataMandatoryIde[0]['handphonepribadi'] == "1",
        );
        _setGuarantorLembaga =  SetGuarantorCompanyModel(
            _dataHideShowIde[0]['jenislembagakelembagaan'] == "1",
            _dataHideShowIde[0]['profilkelembagaan'] == "1",
            _dataHideShowIde[0]['namalembagakelembagaan'] == "1",
            _dataHideShowIde[0]['npwpkelembagaan'] == "1",
            _dataMandatoryIde[0]['jenislembagakelembagaan'] == "1",
            _dataMandatoryIde[0]['profilkelembagaan'] == "1",
            _dataMandatoryIde[0]['namalembagakelembagaan'] == "1",
            _dataMandatoryIde[0]['npwpkelembagaan'] == "1"
        );
      }
      else if (_lastKnownState == "SRE") {
        print("masuk sre");
        //foto hideshow dan mandatory
        _showMandatoryFotoModel = ShowMandatoryFotoModel(
          _dataHideShowFormMRegulerSurvey[0]['fototempattinggal']== "1",
          _dataHideShowFormMRegulerSurvey[0]['pekerjaanfoto']== "1",
          _dataHideShowFormMRegulerSurvey[0]['fotousaha']== "1",
          _dataHideShowFormMRegulerSurvey[0]['jenis_PEMBIAYAAN']== "1",
          _dataHideShowFormMRegulerSurvey[0]['kegiatanusaha']== "1",
          _dataHideShowFormMRegulerSurvey[0]['jeniskegiatanusaha']== "1",
          _dataHideShowFormMRegulerSurvey[0]['jeniskonsep']== "1",
          _dataHideShowFormMRegulerSurvey[0]['grupobjekunit']== "1",
          _dataHideShowFormMRegulerSurvey[0]['fotoobjekpembiayaanunit']== "1",
          _dataHideShowFormMRegulerSurvey[0]['dokumen']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['fototempattinggal']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['pekerjaanfoto']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['fotousaha']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['jenis_PEMBIAYAAN']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['kegiatanusaha']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['jeniskegiatanusaha']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['jeniskonsep']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['grupobjekunit']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['fotoobjekpembiayaanunit']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['dokumen']== "1",
        );

        //semua alamat hideshow & mandatory
        _setAddressIndividuInfoNasabah = SetAddressIndividu(
            _dataHideShowFormMRegulerSurvey[0]['jenisalamat'] == "1",
            _dataHideShowFormMRegulerSurvey[0]['alamat'] == "1",
            _dataHideShowFormMRegulerSurvey[0]['rt'] == "1",
            _dataHideShowFormMRegulerSurvey[0]['rw'] == "1",
            _dataHideShowFormMRegulerSurvey[0]['kelurahan'] == "1",
            _dataHideShowFormMRegulerSurvey[0]['kecamatan'] == "1",
            _dataHideShowFormMRegulerSurvey[0]['kabupatenkota'] == "1",
            _dataHideShowFormMRegulerSurvey[0]['provinsi'] == "1",
            _dataHideShowFormMRegulerSurvey[0]['kodepos'] == "1",
            _dataHideShowFormMRegulerSurvey[0]['teleponarea'] == "1",
            _dataHideShowFormMRegulerSurvey[0]['telepon'] == "1",
            _dataMandatoryFormMRegulerSurvey[0]['jenisalamat'] == "1",
            _dataMandatoryFormMRegulerSurvey[0]['alamat'] == "1",
            _dataMandatoryFormMRegulerSurvey[0]['rt'] == "1",
            _dataMandatoryFormMRegulerSurvey[0]['rw'] == "1",
            _dataMandatoryFormMRegulerSurvey[0]['kelurahan'] == "1",
            _dataMandatoryFormMRegulerSurvey[0]['kecamatan'] == "1",
            _dataMandatoryFormMRegulerSurvey[0]['kabupatenkota'] == "1",
            _dataMandatoryFormMRegulerSurvey[0]['provinsi'] == "1",
            _dataMandatoryFormMRegulerSurvey[0]['kodepos'] == "1",
            _dataMandatoryFormMRegulerSurvey[0]['teleponarea'] == "1",
            _dataMandatoryFormMRegulerSurvey[0]['telepon'] == "1"
        );
        _setAddressIndividuOccupation = SetAddressIndividu(
            _dataHideShowFormMRegulerSurvey[0]['jenisalamatpekerjaan']== "1",
            _dataHideShowFormMRegulerSurvey[0]['alamatpekerjaan']== "1",
            _dataHideShowFormMRegulerSurvey[0]['rtpekerjaan']== "1",
            _dataHideShowFormMRegulerSurvey[0]['rwpekerjaan']== "1",
            _dataHideShowFormMRegulerSurvey[0]['kelurahanpekerjaan']== "1",
            _dataHideShowFormMRegulerSurvey[0]['kecamatanpekerjaan']== "1",
            _dataHideShowFormMRegulerSurvey[0]['kabupatenkotapekerjaan']== "1",
            _dataHideShowFormMRegulerSurvey[0]['provinsipekerjaan']== "1",
            _dataHideShowFormMRegulerSurvey[0]['kodepospekerjaan']== "1",
            _dataHideShowFormMRegulerSurvey[0]['teleponareapekerjaan']== "1",
            _dataHideShowFormMRegulerSurvey[0]['teleponpekerjaan']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['jenisalamatpekerjaan']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['alamatpekerjaan']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['rtpekerjaan']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['rwpekerjaan']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['kelurahanpekerjaan']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['kecamatanpekerjaan']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['kabupatenkotapekerjaan']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['provinsipekerjaan']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['kodepospekerjaan']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['teleponareapekerjaan']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['teleponpekerjaan'] == "1"
        );
        _setAddressIndividuPenjamin = SetAddressIndividu(
            _dataHideShowFormMRegulerSurvey[0]['jenisalamatalamat'] == "1",
            _dataHideShowFormMRegulerSurvey[0]['alamatpribadialamat'] == "1",
            _dataHideShowFormMRegulerSurvey[0]['rtpribadialamat'] == "1",
            _dataHideShowFormMRegulerSurvey[0]['rwpribadialamat'] == "1",
            _dataHideShowFormMRegulerSurvey[0]['kelurahanpribadialamat'] == "1",
            _dataHideShowFormMRegulerSurvey[0]['kecamatanpribadialamat'] == "1",
            _dataHideShowFormMRegulerSurvey[0]['kabupatenkotapribadialamat'] == "1",
            _dataHideShowFormMRegulerSurvey[0]['provinsipribadialamat'] == "1",
            _dataHideShowFormMRegulerSurvey[0]['kodepospribadialamat'] == "1",
            _dataHideShowFormMRegulerSurvey[0]['teleponareapribadialamat'] == "1",
            _dataHideShowFormMRegulerSurvey[0]['teleponpribadialamat'] == "1",
            _dataMandatoryFormMRegulerSurvey[0]['jenisalamatalamat'] == "1",
            _dataMandatoryFormMRegulerSurvey[0]['alamatpribadialamat'] == "1",
            _dataMandatoryFormMRegulerSurvey[0]['rtpribadialamat'] == "1",
            _dataMandatoryFormMRegulerSurvey[0]['rwpribadialamat'] == "1",
            _dataMandatoryFormMRegulerSurvey[0]['kelurahanpribadialamat'] == "1",
            _dataMandatoryFormMRegulerSurvey[0]['kecamatanpribadialamat'] == "1",
            _dataMandatoryFormMRegulerSurvey[0]['kabupatenkotapribadialamat'] == "1",
            _dataMandatoryFormMRegulerSurvey[0]['provinsipribadialamat'] == "1",
            _dataMandatoryFormMRegulerSurvey[0]['kodepospribadialamat'] == "1",
            _dataMandatoryFormMRegulerSurvey[0]['teleponareapribadialamat'] == "1",
            _dataMandatoryFormMRegulerSurvey[0]['teleponpribadialamat'] == "1"
        );
        _setAddressCompanyGuarantorIndividuModel = SetAddressCompanyModel(
          _dataHideShowFormMRegulerSurvey[0]['jenisalamatkelembagaan'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['alamatkelembagaan'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['rtkelembagaan'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['rwkelembagaan'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['kelurahankelembagaan'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['kecamatankelembagaan'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['kabupatenkotakelembagaan'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['provinsikelembagaan'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['kodeposkelembagaan'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['teleponarea1KELEMBAGAAN'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['telepon1KELEMBAGAAN'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['teleponarea2KELEMBAGAAN'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['telepon2KELEMBAGAAN'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['faxareakelembagaan'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['faxkelembagaan'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['jenis_ALAMAT'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['alamatkelembagaan'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['rtkelembagaan'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['rwkelembagaan'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['kelurahankelembagaan'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['kecamatankelembagaan'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['kabupatenkotakelembagaan'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['provinsikelembagaan'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['kodeposkelembagaan'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['teleponarea1KELEMBAGAAN'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['telepon1KELEMBAGAAN'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['teleponarea2KELEMBAGAAN'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['telepon2KELEMBAGAAN'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['faxareakelembagaan'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['faxkelembagaan'] == "1",
        );

        //info nasabah hideshow
        _providerInfoNasabah.isGCVisible  = _dataHideShowFormMRegulerSurvey[0]['gc'] == "1";
        _providerInfoNasabah.isIdentitasModelVisible = _dataHideShowFormMRegulerSurvey[0]['jenisidentitas'] == "1";
        _providerInfoNasabah.isNoIdentitasVisible = _dataHideShowFormMRegulerSurvey[0]['noidentitas'] == "1";
        _providerInfoNasabah.isTglIdentitasVisible = _dataHideShowFormMRegulerSurvey[0]['tanggalidentitas'] == "1";
        _providerInfoNasabah.isIdentitasBerlakuSampaiVisible = _dataHideShowFormMRegulerSurvey[0]['identitasberlaku'] == "1";
        _providerInfoNasabah.isNamaLengkapSesuaiIdentitasVisible = _dataHideShowFormMRegulerSurvey[0]['namalengkapid'] == "1";
        _providerInfoNasabah.isNamaLengkapVisible = _dataHideShowFormMRegulerSurvey[0]['namalengkap'] == "1";
        _providerInfoNasabah.isTglLahirVisible = _dataHideShowFormMRegulerSurvey[0]['tanggallahir'] == "1";
        _providerInfoNasabah.isTempatLahirSesuaiIdentitasVisible = _dataHideShowFormMRegulerSurvey[0]['tempatlahirid'] == "1";
        _providerInfoNasabah.isTempatLahirSesuaiIdentitasLOVVisible = _dataHideShowFormMRegulerSurvey[0]['tempatlahiridlov'] == "1";
        _providerInfoNasabah.isGenderVisible = _dataHideShowFormMRegulerSurvey[0]['jeniskelamin'] == "1";
        _providerInfoNasabah.isReligionVisible = _dataHideShowFormMRegulerSurvey[0]['agama'] == "1";
        _providerInfoNasabah.isEducationSelectedVisible = _dataHideShowFormMRegulerSurvey[0]['pendidikan'] == "1";
        _providerInfoNasabah.isJumlahTanggunganVisible = _dataHideShowFormMRegulerSurvey[0]['jumlahtanggungan'] == "1";
        _providerInfoNasabah.isMaritalStatusSelectedVisible = _dataHideShowFormMRegulerSurvey[0]['statuspernikahan'] == "1";
        _providerInfoNasabah.isNoHpVisible = _dataHideShowFormMRegulerSurvey[0]['handphonepribadi'] == "1";
        _providerInfoNasabah.isNoHp1WAVisible = _dataHideShowFormMRegulerSurvey[0]['nowa1'] == "1";
        _providerInfoNasabah.isHaveNPWPVisible = _dataHideShowFormMRegulerSurvey[0]['punyanpwp'] == "1";
        _providerInfoNasabah.isNamaSesuaiNPWPVisible = _dataHideShowFormMRegulerSurvey[0]['namalengkapsesuainpwp'] == "1";
        _providerInfoNasabah.isJenisNPWPVisible = _dataHideShowFormMRegulerSurvey[0]['jenisnpwp'] == "1";
        _providerInfoNasabah.isTandaPKPVisible = _dataHideShowFormMRegulerSurvey[0]['tandapkp'] == "1";
        _providerInfoNasabah.isAlamatSesuaiNPWPVisible = _dataHideShowFormMRegulerSurvey[0]['alamatnpwp'] == "1";
        _providerInfoNasabah.isNoNPWPVisible = _dataHideShowFormMRegulerSurvey[0]['npwp'] == "1";

        //info nasabah mandatory
        _providerInfoNasabah.isGCMandatory  = _dataMandatoryFormMRegulerSurvey[0]['gc'] == "1";
        _providerInfoNasabah.isIdentitasModelMandatory = _dataMandatoryFormMRegulerSurvey[0]['jenisidentitas'] == "1";
        _providerInfoNasabah.isNoIdentitasMandatory = _dataMandatoryFormMRegulerSurvey[0]['noidentitas'] == "1";
        _providerInfoNasabah.isTglIdentitasMandatory = _dataMandatoryFormMRegulerSurvey[0]['tanggalidentitas'] == "1";
        _providerInfoNasabah.isIdentitasBerlakuSampaiMandatory = _dataMandatoryFormMRegulerSurvey[0]['identitasberlaku'] == "1";
        _providerInfoNasabah.isNamaLengkapSesuaiIdentitasMandatory = _dataMandatoryFormMRegulerSurvey[0]['namalengkapid'] == "1";
        _providerInfoNasabah.isNamaLengkapMandatory = _dataMandatoryFormMRegulerSurvey[0]['namalengkap'] == "1";
        _providerInfoNasabah.isTglLahirMandatory = _dataMandatoryFormMRegulerSurvey[0]['tanggallahir'] == "1";
        _providerInfoNasabah.isTempatLahirSesuaiIdentitasMandatory = _dataMandatoryFormMRegulerSurvey[0]['tempatlahirid'] == "1";
        _providerInfoNasabah.isTempatLahirSesuaiIdentitasLOVMandatory = _dataMandatoryFormMRegulerSurvey[0]['tempatlahiridlov'] == "1";
        _providerInfoNasabah.isGenderMandatory = _dataMandatoryFormMRegulerSurvey[0]['jeniskelamin'] == "1";
        _providerInfoNasabah.isReligionMandatory = _dataMandatoryFormMRegulerSurvey[0]['agama'] == "1";
        _providerInfoNasabah.isEducationSelectedMandatory = _dataMandatoryFormMRegulerSurvey[0]['pendidikan'] == "1";
        _providerInfoNasabah.isJumlahTanggunganMandatory = _dataMandatoryFormMRegulerSurvey[0]['jumlahtanggungan'] == "1";
        _providerInfoNasabah.isMaritalStatusSelectedMandatory = _dataMandatoryFormMRegulerSurvey[0]['statuspernikahan'] == "1";
        _providerInfoNasabah.isNoHpMandatory = _dataMandatoryFormMRegulerSurvey[0]['handphonepribadi'] == "1";
        _providerInfoNasabah.isNoHp1WAMandatory = _dataMandatoryFormMRegulerSurvey[0]['nowa1'] == "1";
        _providerInfoNasabah.isHaveNPWPMandatory = _dataMandatoryFormMRegulerSurvey[0]['punyanpwp'] == "1";
        _providerInfoNasabah.isNamaSesuaiNPWPMandatory = _dataMandatoryFormMRegulerSurvey[0]['namalengkapsesuainpwp'] == "1";
        _providerInfoNasabah.isJenisNPWPMandatory = _dataMandatoryFormMRegulerSurvey[0]['jenisnpwp'] == "1";
        _providerInfoNasabah.isTandaPKPMandatory = _dataMandatoryFormMRegulerSurvey[0]['tandapkp'] == "1";
        _providerInfoNasabah.isAlamatSesuaiNPWPMandatory = _dataMandatoryFormMRegulerSurvey[0]['alamatnpwp'] == "1";
        _providerInfoNasabah.isNoNPWPMandatory = _dataMandatoryFormMRegulerSurvey[0]['npwp'] == "1";

        //info keluarga ibu hideshow
        _providerInformasiKeluargaIBU.isRelationshipStatusVisible = _dataHideShowFormMRegulerSurvey[0]['statushubunganibu'] == "1";
        _providerInformasiKeluargaIBU.isIdentityVisible = _dataHideShowFormMRegulerSurvey[0]['jenisidentitasibu'] == "1";
        _providerInformasiKeluargaIBU.isNoIdentitasVisible = _dataHideShowFormMRegulerSurvey[0]['noidentitasibu'] == "1";
        _providerInformasiKeluargaIBU.isNamaLengkapdentitasVisible = _dataHideShowFormMRegulerSurvey[0]['namalengkapidibu'] == "1";
        _providerInformasiKeluargaIBU.isNamaIdentitasVisible = _dataHideShowFormMRegulerSurvey[0]['namalengkapibu'] == "1";
        _providerInformasiKeluargaIBU.isTglLahirVisible = _dataHideShowFormMRegulerSurvey[0]['tanggallahiribu'] == "1";
        _providerInformasiKeluargaIBU.isTempatLahirSesuaiIdentitasVisible = _dataHideShowFormMRegulerSurvey[0]['tempatlahiridibu'] == "1";
        _providerInformasiKeluargaIBU.isTempatLahirSesuaiIdentitasLOVVisible = _dataHideShowFormMRegulerSurvey[0]['tempatlahiridlovibu'] == "1";
        _providerInformasiKeluargaIBU.isRadioValueGenderVisible = _dataHideShowFormMRegulerSurvey[0]['jeniskelaminibu'] == "1";
        _providerInformasiKeluargaIBU.isKodeAreaVisible = _dataHideShowFormMRegulerSurvey[0]['teleponareaibu'] == "1";
        _providerInformasiKeluargaIBU.isTlpnVisible = _dataHideShowFormMRegulerSurvey[0]['teleponibu'] == "1";
        _providerInformasiKeluargaIBU.isNoHpVisible = _dataHideShowFormMRegulerSurvey[0]['handphoneibu'] == "1";

        //info keluarga ibumandatory
        _providerInformasiKeluargaIBU.isRelationshipStatusMandatory = _dataMandatoryFormMRegulerSurvey[0]['statushubunganibu'] == "1";
        _providerInformasiKeluargaIBU.isIdentityMandatory = _dataMandatoryFormMRegulerSurvey[0]['jenisidentitasibu'] == "1";
        _providerInformasiKeluargaIBU.isNoIdentitasMandatory = _dataMandatoryFormMRegulerSurvey[0]['noidentitasibu'] == "1";
        _providerInformasiKeluargaIBU.isNamaLengkapdentitasMandatory = _dataMandatoryFormMRegulerSurvey[0]['namalengkapidibu'] == "1";
        _providerInformasiKeluargaIBU.isNamaIdentitasMandatory = _dataMandatoryFormMRegulerSurvey[0]['namalengkapibu'] == "1";
        _providerInformasiKeluargaIBU.isTglLahirMandatory = _dataMandatoryFormMRegulerSurvey[0]['tanggallahiribu'] == "1";
        _providerInformasiKeluargaIBU.isTempatLahirSesuaiIdentitasMandatory = _dataMandatoryFormMRegulerSurvey[0]['tempatlahiridibu'] == "1";
        _providerInformasiKeluargaIBU.isTempatLahirSesuaiIdentitasLOVMandatory = _dataMandatoryFormMRegulerSurvey[0]['tempatlahiridlovibu'] == "1";
        _providerInformasiKeluargaIBU.isRadioValueGenderMandatory = _dataMandatoryFormMRegulerSurvey[0]['jeniskelaminibu'] == "1";
        _providerInformasiKeluargaIBU.isKodeAreaMandatory = _dataMandatoryFormMRegulerSurvey[0]['teleponareaibu'] == "1";
        _providerInformasiKeluargaIBU.isTlpnMandatory = _dataMandatoryFormMRegulerSurvey[0]['teleponibu'] == "1";
        _providerInformasiKeluargaIBU.isNoHpMandatory = _dataMandatoryFormMRegulerSurvey[0]['handphoneibu'] == "1";

        //info list keluarga showhide & mandatory
        _setAddInfoKeluarga = SetAddInfoKeluargaModel(
          _dataHideShowFormMRegulerSurvey[0]['statushubunganspouse'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['jenisidentitasspouse'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['noidentitasspouse'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['namalengkapidspouse'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['namalengkapspouse'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['tanggallahirspouse'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['tempatlahiridspouse'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['tempatlahiridlovspouse'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['jeniskelaminspouse'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['teleponareaspouse'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['teleponspouse'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['handphonespouse'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['statushubunganspouse'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['jenisidentitasspouse'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['noidentitasspouse'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['namalengkapidspouse'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['namalengkapspouse'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['tanggallahirspouse'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['tempatlahiridspouse'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['tempatlahiridlovspouse'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['jeniskelaminspouse'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['teleponareaspouse'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['teleponspouse'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['handphonespouse'] == "1",
        );

        //occupation hideshow & mandatory
        _showMandatoryOccupationModel = ShowMandatoryOccupationModel(
          _dataHideShowFormMRegulerSurvey[0]['lamabekerjaberjalan']== "1",
          _dataHideShowFormMRegulerSurvey[0]['namausahawiraswasta']== "1",
          _dataHideShowFormMRegulerSurvey[0]['jenisbadanusahawiraswasta']== "1",
          _dataHideShowFormMRegulerSurvey[0]['sektorekonomiwiraswasta']== "1",
          _dataHideShowFormMRegulerSurvey[0]['lapanganusahawiraswasta']== "1",
          _dataHideShowFormMRegulerSurvey[0]['statuslokasiwiraswasta']== "1",
          _dataHideShowFormMRegulerSurvey[0]['lokasiusahawiraswasta']== "1",
          _dataHideShowFormMRegulerSurvey[0]['totalpegawaiwiraswasta']== "1",
          _dataHideShowFormMRegulerSurvey[0]['totallamabekerjawiraswasta']== "1",
          _dataHideShowFormMRegulerSurvey[0]['jenisprofesiprofesional']== "1",
          _dataHideShowFormMRegulerSurvey[0]['sektorekonomiprofesional']== "1",
          _dataHideShowFormMRegulerSurvey[0]['lapanganusahaprofesional']== "1",
          _dataHideShowFormMRegulerSurvey[0]['statuslokasiprofesional']== "1",
          _dataHideShowFormMRegulerSurvey[0]['lokasiusahaprofesional']== "1",
          _dataHideShowFormMRegulerSurvey[0]['totallamabekerjaprofesional']== "1",
          _dataHideShowFormMRegulerSurvey[0]['namaperusahaanlainnya']== "1",
          _dataHideShowFormMRegulerSurvey[0]['jenisperusahaanlainnya']== "1",
          _dataHideShowFormMRegulerSurvey[0]['sektorekonomilainnya']== "1",
          _dataHideShowFormMRegulerSurvey[0]['lapanganusahalainnya']== "1",
          _dataHideShowFormMRegulerSurvey[0]['totalpegawailainnya']== "1",
          _dataHideShowFormMRegulerSurvey[0]['totallamabekerjalainnya']== "1",
          _dataHideShowFormMRegulerSurvey[0]['jenispephighrisklainnya'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['statuspegawailainnya'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['lamabekerjaberjalan']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['namausahawiraswasta']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['jenisbadanusahawiraswasta']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['sektorekonomiwiraswasta']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['lapanganusahawiraswasta']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['statuslokasiwiraswasta']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['lokasiusahawiraswasta']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['totalpegawaiwiraswasta']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['totallamabekerjawiraswasta']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['jenisprofesiprofesional']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['sektorekonomiprofesional']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['lapanganusahaprofesional']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['statuslokasiprofesional']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['lokasiusahaprofesional']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['totallamabekerjaprofesional']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['namaperusahaanlainnya']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['jenisperusahaanlainnya']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['sektorekonomilainnya']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['lapanganusahalainnya']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['totalpegawailainnya']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['totallamabekerjalainnya']== "1",
          _dataMandatoryFormMRegulerSurvey[0]['jenispephighrisklainnya'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['statuspegawailainnya'] == "1",
        );

        //income hideshow & mandatory
        _showMandatoryIncomeModel = ShowMandatoryIncomeModel(
            _dataHideShowFormMRegulerSurvey[0]['incomeperbulanwiraswasta']== "1",
            _dataHideShowFormMRegulerSurvey[0]['incomelainnyawiraswasta']== "1",
            _dataHideShowFormMRegulerSurvey[0]['totalincomewiraswasta']== "1",
            _dataHideShowFormMRegulerSurvey[0]['pokokincomewiraswasta']== "1",
            _dataHideShowFormMRegulerSurvey[0]['labakotorwiraswasta']== "1",
            _dataHideShowFormMRegulerSurvey[0]['biayaoperasionalwiraswasta']== "1",
            _dataHideShowFormMRegulerSurvey[0]['biayalainnyawiraswasta']== "1",
            _dataHideShowFormMRegulerSurvey[0]['netsebelumpajakwiraswasta']== "1",
            _dataHideShowFormMRegulerSurvey[0]['pajakwiraswasta']== "1",
            _dataHideShowFormMRegulerSurvey[0]['netsetelahpajakwiraswasta']== "1",
            _dataHideShowFormMRegulerSurvey[0]['biayahidupwiraswasta']== "1",
            _dataHideShowFormMRegulerSurvey[0]['sisaincomewiraswasta']== "1",
            _dataHideShowFormMRegulerSurvey[0]['incomepasanganwiraswasta']== "1",
            _dataHideShowFormMRegulerSurvey[0]['angsuranlainnyawiraswasta']== "1",
            _dataHideShowFormMRegulerSurvey[0]['incomeprofesional']== "1",
            _dataHideShowFormMRegulerSurvey[0]['incomepasanganprofesional']== "1",
            _dataHideShowFormMRegulerSurvey[0]['incomelainnyaprofesional']== "1",
            _dataHideShowFormMRegulerSurvey[0]['totalincomeprofesional']== "1",
            _dataHideShowFormMRegulerSurvey[0]['biayahidupprofesional']== "1",
            _dataHideShowFormMRegulerSurvey[0]['sisaincomeprofesional']== "1",
            _dataHideShowFormMRegulerSurvey[0]['angsuranlainnyaprofesional']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['incomeperbulanwiraswasta']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['incomelainnyawiraswasta']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['totalincomewiraswasta']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['pokokincomewiraswasta']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['labakotorwiraswasta']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['biayaoperasionalwiraswasta']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['biayalainnyawiraswasta']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['netsebelumpajakwiraswasta']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['pajakwiraswasta']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['netsetelahpajakwiraswasta']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['biayahidupwiraswasta']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['sisaincomewiraswasta']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['incomepasanganwiraswasta']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['angsuranlainnyawiraswasta']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['incomeprofesional']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['incomepasanganprofesional']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['incomelainnyaprofesional']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['totalincomeprofesional']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['biayahidupprofesional']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['sisaincomeprofesional']== "1",
            _dataMandatoryFormMRegulerSurvey[0]['angsuranlainnyaprofesional']== "1"
        );

        //penjamin individu - lembaga hideshow & mandatory
        _setGuarantorIndividu = SetGuarantorIndividu(
          _dataHideShowFormMRegulerSurvey[0]['statushubunganpribadi'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['jenisidentitaspribadi'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['noidentitaspribadi'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['namalengkapidpribadi'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['namalengkappribadi'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['tanggallahirpribadi'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['tempatlahiridpribadi'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['tempatlahiridlovpribadi'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['jeniskelaminpribadi'] == "1",
          _dataHideShowFormMRegulerSurvey[0]['handphonepribadi'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['statushubunganpribadi'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['jenisidentitaspribadi'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['noidentitaspribadi'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['namalengkapidpribadi'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['namalengkappribadi'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['tanggallahirpribadi'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['tempatlahiridpribadi'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['tempatlahiridlovpribadi'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['jeniskelaminpribadi'] == "1",
          _dataMandatoryFormMRegulerSurvey[0]['handphonepribadi'] == "1",
        );
        _setGuarantorLembaga =  SetGuarantorCompanyModel(
            _dataHideShowFormMRegulerSurvey[0]['jenislembagakelembagaan'] == "1",
            _dataHideShowFormMRegulerSurvey[0]['profilkelembagaan'] == "1",
            _dataHideShowFormMRegulerSurvey[0]['namalembagakelembagaan'] == "1",
            _dataHideShowFormMRegulerSurvey[0]['npwpkelembagaan'] == "1",
            _dataMandatoryFormMRegulerSurvey[0]['jenislembagakelembagaan'] == "1",
            _dataMandatoryFormMRegulerSurvey[0]['profilkelembagaan'] == "1",
            _dataMandatoryFormMRegulerSurvey[0]['namalembagakelembagaan'] == "1",
            _dataMandatoryFormMRegulerSurvey[0]['npwpkelembagaan'] == "1"
        );
      }
      else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        //foto hideshow dan mandatory
        _showMandatoryFotoModel = ShowMandatoryFotoModel(
          _dataHideShowPACInd[0]['fototempattinggal']== "1",
          _dataHideShowPACInd[0]['pekerjaanfoto']== "1",
          _dataHideShowPACInd[0]['fotousaha']== "1",
          _dataHideShowPACInd[0]['jenis_PEMBIAYAAN']== "1",
          _dataHideShowPACInd[0]['kegiatanusaha']== "1",
          _dataHideShowPACInd[0]['jeniskegiatanusaha']== "1",
          _dataHideShowPACInd[0]['jeniskonsep']== "1",
          _dataHideShowPACInd[0]['grupobjekunit']== "1",
          _dataHideShowPACInd[0]['fotoobjekpembiayaanunit']== "1",
          _dataHideShowPACInd[0]['dokumen']== "1",
          _dataMandatoryPACInd[0]['fototempattinggal']== "1",
          _dataMandatoryPACInd[0]['pekerjaanfoto']== "1",
          _dataMandatoryPACInd[0]['fotousaha']== "1",
          _dataMandatoryPACInd[0]['jenis_PEMBIAYAAN']== "1",
          _dataMandatoryPACInd[0]['kegiatanusaha']== "1",
          _dataMandatoryPACInd[0]['jeniskegiatanusaha']== "1",
          _dataMandatoryPACInd[0]['jeniskonsep']== "1",
          _dataMandatoryPACInd[0]['grupobjekunit']== "1",
          _dataMandatoryPACInd[0]['fotoobjekpembiayaanunit']== "1",
          _dataMandatoryPACInd[0]['dokumen']== "1",
        );

        //semua alamat hideshow & mandatory
        _setAddressIndividuInfoNasabah = SetAddressIndividu(
            _dataHideShowPACInd[0]['jenisalamat'] == "1",
            _dataHideShowPACInd[0]['alamat'] == "1",
            _dataHideShowPACInd[0]['rt'] == "1",
            _dataHideShowPACInd[0]['rw'] == "1",
            _dataHideShowPACInd[0]['kelurahan'] == "1",
            _dataHideShowPACInd[0]['kecamatan'] == "1",
            _dataHideShowPACInd[0]['kabupatenkota'] == "1",
            _dataHideShowPACInd[0]['provinsi'] == "1",
            _dataHideShowPACInd[0]['kodepos'] == "1",
            _dataHideShowPACInd[0]['teleponarea'] == "1",
            _dataHideShowPACInd[0]['telepon'] == "1",
            _dataMandatoryPACInd[0]['jenisalamat'] == "1",
            _dataMandatoryPACInd[0]['alamat'] == "1",
            _dataMandatoryPACInd[0]['rt'] == "1",
            _dataMandatoryPACInd[0]['rw'] == "1",
            _dataMandatoryPACInd[0]['kelurahan'] == "1",
            _dataMandatoryPACInd[0]['kecamatan'] == "1",
            _dataMandatoryPACInd[0]['kabupatenkota'] == "1",
            _dataMandatoryPACInd[0]['provinsi'] == "1",
            _dataMandatoryPACInd[0]['kodepos'] == "1",
            _dataMandatoryPACInd[0]['teleponarea'] == "1",
            _dataMandatoryPACInd[0]['telepon'] == "1"
        );
        _setAddressIndividuOccupation = SetAddressIndividu(
            _dataHideShowPACInd[0]['jenisalamatpekerjaan']== "1",
            _dataHideShowPACInd[0]['alamatpekerjaan']== "1",
            _dataHideShowPACInd[0]['rtpekerjaan']== "1",
            _dataHideShowPACInd[0]['rwpekerjaan']== "1",
            _dataHideShowPACInd[0]['kelurahanpekerjaan']== "1",
            _dataHideShowPACInd[0]['kecamatanpekerjaan']== "1",
            _dataHideShowPACInd[0]['kabupatenkotapekerjaan']== "1",
            _dataHideShowPACInd[0]['provinsipekerjaan']== "1",
            _dataHideShowPACInd[0]['kodepospekerjaan']== "1",
            _dataHideShowPACInd[0]['teleponareapekerjaan']== "1",
            _dataHideShowPACInd[0]['teleponpekerjaan']== "1",
            _dataMandatoryPACInd[0]['jenisalamatpekerjaan']== "1",
            _dataMandatoryPACInd[0]['alamatpekerjaan']== "1",
            _dataMandatoryPACInd[0]['rtpekerjaan']== "1",
            _dataMandatoryPACInd[0]['rwpekerjaan']== "1",
            _dataMandatoryPACInd[0]['kelurahanpekerjaan']== "1",
            _dataMandatoryPACInd[0]['kecamatanpekerjaan']== "1",
            _dataMandatoryPACInd[0]['kabupatenkotapekerjaan']== "1",
            _dataMandatoryPACInd[0]['provinsipekerjaan']== "1",
            _dataMandatoryPACInd[0]['kodepospekerjaan']== "1",
            _dataMandatoryPACInd[0]['teleponareapekerjaan']== "1",
            _dataMandatoryPACInd[0]['teleponpekerjaan'] == "1"
        );
        _setAddressIndividuPenjamin = SetAddressIndividu(
            _dataHideShowPACInd[0]['jenisalamatalamat'] == "1",
            _dataHideShowPACInd[0]['alamatpribadialamat'] == "1",
            _dataHideShowPACInd[0]['rtpribadialamat'] == "1",
            _dataHideShowPACInd[0]['rwpribadialamat'] == "1",
            _dataHideShowPACInd[0]['kelurahanpribadialamat'] == "1",
            _dataHideShowPACInd[0]['kecamatanpribadialamat'] == "1",
            _dataHideShowPACInd[0]['kabupatenkotapribadialamat'] == "1",
            _dataHideShowPACInd[0]['provinsipribadialamat'] == "1",
            _dataHideShowPACInd[0]['kodepospribadialamat'] == "1",
            _dataHideShowPACInd[0]['teleponareapribadialamat'] == "1",
            _dataHideShowPACInd[0]['teleponpribadialamat'] == "1",
            _dataMandatoryPACInd[0]['jenisalamatalamat'] == "1",
            _dataMandatoryPACInd[0]['alamatpribadialamat'] == "1",
            _dataMandatoryPACInd[0]['rtpribadialamat'] == "1",
            _dataMandatoryPACInd[0]['rwpribadialamat'] == "1",
            _dataMandatoryPACInd[0]['kelurahanpribadialamat'] == "1",
            _dataMandatoryPACInd[0]['kecamatanpribadialamat'] == "1",
            _dataMandatoryPACInd[0]['kabupatenkotapribadialamat'] == "1",
            _dataMandatoryPACInd[0]['provinsipribadialamat'] == "1",
            _dataMandatoryPACInd[0]['kodepospribadialamat'] == "1",
            _dataMandatoryPACInd[0]['teleponareapribadialamat'] == "1",
            _dataMandatoryPACInd[0]['teleponpribadialamat'] == "1"
        );
        _setAddressCompanyGuarantorIndividuModel = SetAddressCompanyModel(
          _dataHideShowPACInd[0]['jenisalamatkelembagaan'] == "1",
          _dataHideShowPACInd[0]['alamatkelembagaan'] == "1",
          _dataHideShowPACInd[0]['rtkelembagaan'] == "1",
          _dataHideShowPACInd[0]['rwkelembagaan'] == "1",
          _dataHideShowPACInd[0]['kelurahankelembagaan'] == "1",
          _dataHideShowPACInd[0]['kecamatankelembagaan'] == "1",
          _dataHideShowPACInd[0]['kabupatenkotakelembagaan'] == "1",
          _dataHideShowPACInd[0]['provinsikelembagaan'] == "1",
          _dataHideShowPACInd[0]['kodeposkelembagaan'] == "1",
          _dataHideShowPACInd[0]['teleponarea1KELEMBAGAAN'] == "1",
          _dataHideShowPACInd[0]['telepon1KELEMBAGAAN'] == "1",
          _dataHideShowPACInd[0]['teleponarea2KELEMBAGAAN'] == "1",
          _dataHideShowPACInd[0]['telepon2KELEMBAGAAN'] == "1",
          _dataHideShowPACInd[0]['faxareakelembagaan'] == "1",
          _dataHideShowPACInd[0]['faxkelembagaan'] == "1",
          _dataMandatoryPACInd[0]['jenis_ALAMAT'] == "1",
          _dataMandatoryPACInd[0]['alamatkelembagaan'] == "1",
          _dataMandatoryPACInd[0]['rtkelembagaan'] == "1",
          _dataMandatoryPACInd[0]['rwkelembagaan'] == "1",
          _dataMandatoryPACInd[0]['kelurahankelembagaan'] == "1",
          _dataMandatoryPACInd[0]['kecamatankelembagaan'] == "1",
          _dataMandatoryPACInd[0]['kabupatenkotakelembagaan'] == "1",
          _dataMandatoryPACInd[0]['provinsikelembagaan'] == "1",
          _dataMandatoryPACInd[0]['kodeposkelembagaan'] == "1",
          _dataMandatoryPACInd[0]['teleponarea1KELEMBAGAAN'] == "1",
          _dataMandatoryPACInd[0]['telepon1KELEMBAGAAN'] == "1",
          _dataMandatoryPACInd[0]['teleponarea2KELEMBAGAAN'] == "1",
          _dataMandatoryPACInd[0]['telepon2KELEMBAGAAN'] == "1",
          _dataMandatoryPACInd[0]['faxareakelembagaan'] == "1",
          _dataMandatoryPACInd[0]['faxkelembagaan'] == "1",
        );

        //info nasabah hideshow
        _providerInfoNasabah.isGCVisible  = _dataHideShowPACInd[0]['gc'] == "1";
        _providerInfoNasabah.isIdentitasModelVisible = _dataHideShowPACInd[0]['jenisidentitas'] == "1";
        _providerInfoNasabah.isNoIdentitasVisible = _dataHideShowPACInd[0]['noidentitas'] == "1";
        _providerInfoNasabah.isTglIdentitasVisible = _dataHideShowPACInd[0]['tanggalidentitas'] == "1";
        _providerInfoNasabah.isIdentitasBerlakuSampaiVisible = _dataHideShowPACInd[0]['identitasberlaku'] == "1";
        _providerInfoNasabah.isNamaLengkapSesuaiIdentitasVisible = _dataHideShowPACInd[0]['namalengkapid'] == "1";
        _providerInfoNasabah.isNamaLengkapVisible = _dataHideShowPACInd[0]['namalengkap'] == "1";
        _providerInfoNasabah.isTglLahirVisible = _dataHideShowPACInd[0]['tanggallahir'] == "1";
        _providerInfoNasabah.isTempatLahirSesuaiIdentitasVisible = _dataHideShowPACInd[0]['tempatlahirid'] == "1";
        _providerInfoNasabah.isTempatLahirSesuaiIdentitasLOVVisible = _dataHideShowPACInd[0]['tempatlahiridlov'] == "1";
        _providerInfoNasabah.isGenderVisible = _dataHideShowPACInd[0]['jeniskelamin'] == "1";
        _providerInfoNasabah.isReligionVisible = _dataHideShowPACInd[0]['agama'] == "1";
        _providerInfoNasabah.isEducationSelectedVisible = _dataHideShowPACInd[0]['pendidikan'] == "1";
        _providerInfoNasabah.isJumlahTanggunganVisible = _dataHideShowPACInd[0]['jumlahtanggungan'] == "1";
        _providerInfoNasabah.isMaritalStatusSelectedVisible = _dataHideShowPACInd[0]['statuspernikahan'] == "1";
        _providerInfoNasabah.isNoHpVisible = _dataHideShowPACInd[0]['handphonepribadi'] == "1";
        _providerInfoNasabah.isNoHp1WAVisible = _dataHideShowPACInd[0]['nowa1'] == "1";
        _providerInfoNasabah.isHaveNPWPVisible = _dataHideShowPACInd[0]['punyanpwp'] == "1";
        _providerInfoNasabah.isNamaSesuaiNPWPVisible = _dataHideShowPACInd[0]['namalengkapsesuainpwp'] == "1";
        _providerInfoNasabah.isJenisNPWPVisible = _dataHideShowPACInd[0]['jenisnpwp'] == "1";
        _providerInfoNasabah.isTandaPKPVisible = _dataHideShowPACInd[0]['tandapkp'] == "1";
        _providerInfoNasabah.isAlamatSesuaiNPWPVisible = _dataHideShowPACInd[0]['alamatnpwp'] == "1";
        _providerInfoNasabah.isNoNPWPVisible = _dataHideShowPACInd[0]['npwp'] == "1";

        //info nasabah mandatory
        _providerInfoNasabah.isGCMandatory  = _dataMandatoryPACInd[0]['gc'] == "1";
        _providerInfoNasabah.isIdentitasModelMandatory = _dataMandatoryPACInd[0]['jenisidentitas'] == "1";
        _providerInfoNasabah.isNoIdentitasMandatory = _dataMandatoryPACInd[0]['noidentitas'] == "1";
        _providerInfoNasabah.isTglIdentitasMandatory = _dataMandatoryPACInd[0]['tanggalidentitas'] == "1";
        _providerInfoNasabah.isIdentitasBerlakuSampaiMandatory = _dataMandatoryPACInd[0]['identitasberlaku'] == "1";
        _providerInfoNasabah.isNamaLengkapSesuaiIdentitasMandatory = _dataMandatoryPACInd[0]['namalengkapid'] == "1";
        _providerInfoNasabah.isNamaLengkapMandatory = _dataMandatoryPACInd[0]['namalengkap'] == "1";
        _providerInfoNasabah.isTglLahirMandatory = _dataMandatoryPACInd[0]['tanggallahir'] == "1";
        _providerInfoNasabah.isTempatLahirSesuaiIdentitasMandatory = _dataMandatoryPACInd[0]['tempatlahirid'] == "1";
        _providerInfoNasabah.isTempatLahirSesuaiIdentitasLOVMandatory = _dataMandatoryPACInd[0]['tempatlahiridlov'] == "1";
        _providerInfoNasabah.isGenderMandatory = _dataMandatoryPACInd[0]['jeniskelamin'] == "1";
        _providerInfoNasabah.isReligionMandatory = _dataMandatoryPACInd[0]['agama'] == "1";
        _providerInfoNasabah.isEducationSelectedMandatory = _dataMandatoryPACInd[0]['pendidikan'] == "1";
        _providerInfoNasabah.isJumlahTanggunganMandatory = _dataMandatoryPACInd[0]['jumlahtanggungan'] == "1";
        _providerInfoNasabah.isMaritalStatusSelectedMandatory = _dataMandatoryPACInd[0]['statuspernikahan'] == "1";
        _providerInfoNasabah.isNoHpMandatory = _dataMandatoryPACInd[0]['handphonepribadi'] == "1";
        _providerInfoNasabah.isNoHp1WAMandatory = _dataMandatoryPACInd[0]['nowa1'] == "1";
        _providerInfoNasabah.isHaveNPWPMandatory = _dataMandatoryPACInd[0]['punyanpwp'] == "1";
        _providerInfoNasabah.isNamaSesuaiNPWPMandatory = _dataMandatoryPACInd[0]['namalengkapsesuainpwp'] == "1";
        _providerInfoNasabah.isJenisNPWPMandatory = _dataMandatoryPACInd[0]['jenisnpwp'] == "1";
        _providerInfoNasabah.isTandaPKPMandatory = _dataMandatoryPACInd[0]['tandapkp'] == "1";
        _providerInfoNasabah.isAlamatSesuaiNPWPMandatory = _dataMandatoryPACInd[0]['alamatnpwp'] == "1";
        _providerInfoNasabah.isNoNPWPMandatory = _dataMandatoryPACInd[0]['npwp'] == "1";

        //info keluarga ibu hideshow
        _providerInformasiKeluargaIBU.isRelationshipStatusVisible = _dataHideShowPACInd[0]['statushubunganibu'] == "1";
        _providerInformasiKeluargaIBU.isIdentityVisible = _dataHideShowPACInd[0]['jenisidentitasibu'] == "1";
        _providerInformasiKeluargaIBU.isNoIdentitasVisible = _dataHideShowPACInd[0]['noidentitasibu'] == "1";
        _providerInformasiKeluargaIBU.isNamaLengkapdentitasVisible = _dataHideShowPACInd[0]['namalengkapidibu'] == "1";
        _providerInformasiKeluargaIBU.isNamaIdentitasVisible = _dataHideShowPACInd[0]['namalengkapibu'] == "1";
        _providerInformasiKeluargaIBU.isTglLahirVisible = _dataHideShowPACInd[0]['tanggallahiribu'] == "1";
        _providerInformasiKeluargaIBU.isTempatLahirSesuaiIdentitasVisible = _dataHideShowPACInd[0]['tempatlahiridibu'] == "1";
        _providerInformasiKeluargaIBU.isTempatLahirSesuaiIdentitasLOVVisible = _dataHideShowPACInd[0]['tempatlahiridlovibu'] == "1";
        _providerInformasiKeluargaIBU.isRadioValueGenderVisible = _dataHideShowPACInd[0]['jeniskelaminibu'] == "1";
        _providerInformasiKeluargaIBU.isKodeAreaVisible = _dataHideShowPACInd[0]['teleponareaibu'] == "1";
        _providerInformasiKeluargaIBU.isTlpnVisible = _dataHideShowPACInd[0]['teleponibu'] == "1";
        _providerInformasiKeluargaIBU.isNoHpVisible = _dataHideShowPACInd[0]['handphoneibu'] == "1";

        //info keluarga ibu mandatory
        _providerInformasiKeluargaIBU.isRelationshipStatusMandatory = _dataMandatoryPACInd[0]['statushubunganibu'] == "1";
        _providerInformasiKeluargaIBU.isIdentityMandatory = _dataMandatoryPACInd[0]['jenisidentitasibu'] == "1";
        _providerInformasiKeluargaIBU.isNoIdentitasMandatory = _dataMandatoryPACInd[0]['noidentitasibu'] == "1";
        _providerInformasiKeluargaIBU.isNamaLengkapdentitasMandatory = _dataMandatoryPACInd[0]['namalengkapidibu'] == "1";
        _providerInformasiKeluargaIBU.isNamaIdentitasMandatory = _dataMandatoryPACInd[0]['namalengkapibu'] == "1";
        _providerInformasiKeluargaIBU.isTglLahirMandatory = _dataMandatoryPACInd[0]['tanggallahiribu'] == "1";
        _providerInformasiKeluargaIBU.isTempatLahirSesuaiIdentitasMandatory = _dataMandatoryPACInd[0]['tempatlahiridibu'] == "1";
        _providerInformasiKeluargaIBU.isTempatLahirSesuaiIdentitasLOVMandatory = _dataMandatoryPACInd[0]['tempatlahiridlovibu'] == "1";
        _providerInformasiKeluargaIBU.isRadioValueGenderMandatory = _dataMandatoryPACInd[0]['jeniskelaminibu'] == "1";
        _providerInformasiKeluargaIBU.isKodeAreaMandatory = _dataMandatoryPACInd[0]['teleponareaibu'] == "1";
        _providerInformasiKeluargaIBU.isTlpnMandatory = _dataMandatoryPACInd[0]['teleponibu'] == "1";
        _providerInformasiKeluargaIBU.isNoHpMandatory = _dataMandatoryPACInd[0]['handphoneibu'] == "1";

        //info list keluarga showhide & mandatory
        _setAddInfoKeluarga = SetAddInfoKeluargaModel(
          _dataHideShowPACInd[0]['statushubunganspouse'] == "1",
          _dataHideShowPACInd[0]['jenisidentitasspouse'] == "1",
          _dataHideShowPACInd[0]['noidentitasspouse'] == "1",
          _dataHideShowPACInd[0]['namalengkapidspouse'] == "1",
          _dataHideShowPACInd[0]['namalengkapspouse'] == "1",
          _dataHideShowPACInd[0]['tanggallahirspouse'] == "1",
          _dataHideShowPACInd[0]['tempatlahiridspouse'] == "1",
          _dataHideShowPACInd[0]['tempatlahiridlovspouse'] == "1",
          _dataHideShowPACInd[0]['jeniskelaminspouse'] == "1",
          _dataHideShowPACInd[0]['teleponareaspouse'] == "1",
          _dataHideShowPACInd[0]['teleponspouse'] == "1",
          _dataHideShowPACInd[0]['handphonespouse'] == "1",
          _dataMandatoryPACInd[0]['statushubunganspouse'] == "1",
          _dataMandatoryPACInd[0]['jenisidentitasspouse'] == "1",
          _dataMandatoryPACInd[0]['noidentitasspouse'] == "1",
          _dataMandatoryPACInd[0]['namalengkapidspouse'] == "1",
          _dataMandatoryPACInd[0]['namalengkapspouse'] == "1",
          _dataMandatoryPACInd[0]['tanggallahirspouse'] == "1",
          _dataMandatoryPACInd[0]['tempatlahiridspouse'] == "1",
          _dataMandatoryPACInd[0]['tempatlahiridlovspouse'] == "1",
          _dataMandatoryPACInd[0]['jeniskelaminspouse'] == "1",
          _dataMandatoryPACInd[0]['teleponareaspouse'] == "1",
          _dataMandatoryPACInd[0]['teleponspouse'] == "1",
          _dataMandatoryPACInd[0]['handphonespouse'] == "1",
        );

        //occupation hideshow & mandatory
        _showMandatoryOccupationModel = ShowMandatoryOccupationModel(
          _dataHideShowPACInd[0]['lamabekerjaberjalan']== "1",
          _dataHideShowPACInd[0]['namausahawiraswasta']== "1",
          _dataHideShowPACInd[0]['jenisbadanusahawiraswasta']== "1",
          _dataHideShowPACInd[0]['sektorekonomiwiraswasta']== "1",
          _dataHideShowPACInd[0]['lapanganusahawiraswasta']== "1",
          _dataHideShowPACInd[0]['statuslokasiwiraswasta']== "1",
          _dataHideShowPACInd[0]['lokasiusahawiraswasta']== "1",
          _dataHideShowPACInd[0]['totalpegawaiwiraswasta']== "1",
          _dataHideShowPACInd[0]['totallamabekerjawiraswasta']== "1",
          _dataHideShowPACInd[0]['jenisprofesiprofesional']== "1",
          _dataHideShowPACInd[0]['sektorekonomiprofesional']== "1",
          _dataHideShowPACInd[0]['lapanganusahaprofesional']== "1",
          _dataHideShowPACInd[0]['statuslokasiprofesional']== "1",
          _dataHideShowPACInd[0]['lokasiusahaprofesional']== "1",
          _dataHideShowPACInd[0]['totallamabekerjaprofesional']== "1",
          _dataHideShowPACInd[0]['namaperusahaanlainnya']== "1",
          _dataHideShowPACInd[0]['jenisperusahaanlainnya']== "1",
          _dataHideShowPACInd[0]['sektorekonomilainnya']== "1",
          _dataHideShowPACInd[0]['lapanganusahalainnya']== "1",
          _dataHideShowPACInd[0]['totalpegawailainnya']== "1",
          _dataHideShowPACInd[0]['totallamabekerjalainnya']== "1",
          _dataHideShowPACInd[0]['jenispephighrisklainnya'] == "1",
          _dataHideShowPACInd[0]['statuspegawailainnya'] == "1",
          _dataMandatoryPACInd[0]['lamabekerjaberjalan']== "1",
          _dataMandatoryPACInd[0]['namausahawiraswasta']== "1",
          _dataMandatoryPACInd[0]['jenisbadanusahawiraswasta']== "1",
          _dataMandatoryPACInd[0]['sektorekonomiwiraswasta']== "1",
          _dataMandatoryPACInd[0]['lapanganusahawiraswasta']== "1",
          _dataMandatoryPACInd[0]['statuslokasiwiraswasta']== "1",
          _dataMandatoryPACInd[0]['lokasiusahawiraswasta']== "1",
          _dataMandatoryPACInd[0]['totalpegawaiwiraswasta']== "1",
          _dataMandatoryPACInd[0]['totallamabekerjawiraswasta']== "1",
          _dataMandatoryPACInd[0]['jenisprofesiprofesional']== "1",
          _dataMandatoryPACInd[0]['sektorekonomiprofesional']== "1",
          _dataMandatoryPACInd[0]['lapanganusahaprofesional']== "1",
          _dataMandatoryPACInd[0]['statuslokasiprofesional']== "1",
          _dataMandatoryPACInd[0]['lokasiusahaprofesional']== "1",
          _dataMandatoryPACInd[0]['totallamabekerjaprofesional']== "1",
          _dataMandatoryPACInd[0]['namaperusahaanlainnya']== "1",
          _dataMandatoryPACInd[0]['jenisperusahaanlainnya']== "1",
          _dataMandatoryPACInd[0]['sektorekonomilainnya']== "1",
          _dataMandatoryPACInd[0]['lapanganusahalainnya']== "1",
          _dataMandatoryPACInd[0]['totalpegawailainnya']== "1",
          _dataMandatoryPACInd[0]['totallamabekerjalainnya']== "1",
          _dataMandatoryPACInd[0]['jenispephighrisklainnya'] == "1",
          _dataMandatoryPACInd[0]['statuspegawailainnya'] == "1",
        );

        //income hideshow & mandatory
        _showMandatoryIncomeModel = ShowMandatoryIncomeModel(
            _dataHideShowPACInd[0]['incomeperbulanwiraswasta']== "1",
            _dataHideShowPACInd[0]['incomelainnyawiraswasta']== "1",
            _dataHideShowPACInd[0]['totalincomewiraswasta']== "1",
            _dataHideShowPACInd[0]['pokokincomewiraswasta']== "1",
            _dataHideShowPACInd[0]['labakotorwiraswasta']== "1",
            _dataHideShowPACInd[0]['biayaoperasionalwiraswasta']== "1",
            _dataHideShowPACInd[0]['biayalainnyawiraswasta']== "1",
            _dataHideShowPACInd[0]['netsebelumpajakwiraswasta']== "1",
            _dataHideShowPACInd[0]['pajakwiraswasta']== "1",
            _dataHideShowPACInd[0]['netsetelahpajakwiraswasta']== "1",
            _dataHideShowPACInd[0]['biayahidupwiraswasta']== "1",
            _dataHideShowPACInd[0]['sisaincomewiraswasta']== "1",
            _dataHideShowPACInd[0]['incomepasanganwiraswasta']== "1",
            _dataHideShowPACInd[0]['angsuranlainnyawiraswasta']== "1",
            _dataHideShowPACInd[0]['incomeprofesional']== "1",
            _dataHideShowPACInd[0]['incomepasanganprofesional']== "1",
            _dataHideShowPACInd[0]['incomelainnyaprofesional']== "1",
            _dataHideShowPACInd[0]['totalincomeprofesional']== "1",
            _dataHideShowPACInd[0]['biayahidupprofesional']== "1",
            _dataHideShowPACInd[0]['sisaincomeprofesional']== "1",
            _dataHideShowPACInd[0]['angsuranlainnyaprofesional']== "1",
            _dataMandatoryPACInd[0]['incomeperbulanwiraswasta']== "1",
            _dataMandatoryPACInd[0]['incomelainnyawiraswasta']== "1",
            _dataMandatoryPACInd[0]['totalincomewiraswasta']== "1",
            _dataMandatoryPACInd[0]['pokokincomewiraswasta']== "1",
            _dataMandatoryPACInd[0]['labakotorwiraswasta']== "1",
            _dataMandatoryPACInd[0]['biayaoperasionalwiraswasta']== "1",
            _dataMandatoryPACInd[0]['biayalainnyawiraswasta']== "1",
            _dataMandatoryPACInd[0]['netsebelumpajakwiraswasta']== "1",
            _dataMandatoryPACInd[0]['pajakwiraswasta']== "1",
            _dataMandatoryPACInd[0]['netsetelahpajakwiraswasta']== "1",
            _dataMandatoryPACInd[0]['biayahidupwiraswasta']== "1",
            _dataMandatoryPACInd[0]['sisaincomewiraswasta']== "1",
            _dataMandatoryPACInd[0]['incomepasanganwiraswasta']== "1",
            _dataMandatoryPACInd[0]['angsuranlainnyawiraswasta']== "1",
            _dataMandatoryPACInd[0]['incomeprofesional']== "1",
            _dataMandatoryPACInd[0]['incomepasanganprofesional']== "1",
            _dataMandatoryPACInd[0]['incomelainnyaprofesional']== "1",
            _dataMandatoryPACInd[0]['totalincomeprofesional']== "1",
            _dataMandatoryPACInd[0]['biayahidupprofesional']== "1",
            _dataMandatoryPACInd[0]['sisaincomeprofesional']== "1",
            _dataMandatoryPACInd[0]['angsuranlainnyaprofesional']== "1"
        );

        //penjamin individu - lembaga hideshow & mandatory
        _setGuarantorIndividu = SetGuarantorIndividu(
          _dataHideShowPACInd[0]['statushubunganpribadi'] == "1",
          _dataHideShowPACInd[0]['jenisidentitaspribadi'] == "1",
          _dataHideShowPACInd[0]['noidentitaspribadi'] == "1",
          _dataHideShowPACInd[0]['namalengkapidpribadi'] == "1",
          _dataHideShowPACInd[0]['namalengkappribadi'] == "1",
          _dataHideShowPACInd[0]['tanggallahirpribadi'] == "1",
          _dataHideShowPACInd[0]['tempatlahiridpribadi'] == "1",
          _dataHideShowPACInd[0]['tempatlahiridlovpribadi'] == "1",
          _dataHideShowPACInd[0]['jeniskelaminpribadi'] == "1",
          _dataHideShowPACInd[0]['handphonepribadi'] == "1",
          _dataMandatoryPACInd[0]['statushubunganpribadi'] == "1",
          _dataMandatoryPACInd[0]['jenisidentitaspribadi'] == "1",
          _dataMandatoryPACInd[0]['noidentitaspribadi'] == "1",
          _dataMandatoryPACInd[0]['namalengkapidpribadi'] == "1",
          _dataMandatoryPACInd[0]['namalengkappribadi'] == "1",
          _dataMandatoryPACInd[0]['tanggallahirpribadi'] == "1",
          _dataMandatoryPACInd[0]['tempatlahiridpribadi'] == "1",
          _dataMandatoryPACInd[0]['tempatlahiridlovpribadi'] == "1",
          _dataMandatoryPACInd[0]['jeniskelaminpribadi'] == "1",
          _dataMandatoryPACInd[0]['handphonepribadi'] == "1",
        );
        _setGuarantorLembaga =  SetGuarantorCompanyModel(
            _dataHideShowPACInd[0]['jenislembagakelembagaan'] == "1",
            _dataHideShowPACInd[0]['profilkelembagaan'] == "1",
            _dataHideShowPACInd[0]['namalembagakelembagaan'] == "1",
            _dataHideShowPACInd[0]['npwpkelembagaan'] == "1",
            _dataMandatoryPACInd[0]['jenislembagakelembagaan'] == "1",
            _dataMandatoryPACInd[0]['profilkelembagaan'] == "1",
            _dataMandatoryPACInd[0]['namalembagakelembagaan'] == "1",
            _dataMandatoryPACInd[0]['npwpkelembagaan'] == "1"
        );
      }
      else if (_lastKnownState == "RSVY") {
        print("masuk resurvey");
        //foto hideshow dan mandatory
        _showMandatoryFotoModel = ShowMandatoryFotoModel(
          _dataHideShowFormMResurvey[0]['fototempattinggal']== "1",
          _dataHideShowFormMResurvey[0]['pekerjaanfoto']== "1",
          _dataHideShowFormMResurvey[0]['fotousaha']== "1",
          _dataHideShowFormMResurvey[0]['jenis_PEMBIAYAAN']== "1",
          _dataHideShowFormMResurvey[0]['kegiatanusaha']== "1",
          _dataHideShowFormMResurvey[0]['jeniskegiatanusaha']== "1",
          _dataHideShowFormMResurvey[0]['jeniskonsep']== "1",
          _dataHideShowFormMResurvey[0]['grupobjekunit']== "1",
          _dataHideShowFormMResurvey[0]['fotoobjekpembiayaanunit']== "1",
          _dataHideShowFormMResurvey[0]['dokumen']== "1",
          _dataMandatoryFormMResurvey[0]['fototempattinggal']== "1",
          _dataMandatoryFormMResurvey[0]['pekerjaanfoto']== "1",
          _dataMandatoryFormMResurvey[0]['fotousaha']== "1",
          _dataMandatoryFormMResurvey[0]['jenis_PEMBIAYAAN']== "1",
          _dataMandatoryFormMResurvey[0]['kegiatanusaha']== "1",
          _dataMandatoryFormMResurvey[0]['jeniskegiatanusaha']== "1",
          _dataMandatoryFormMResurvey[0]['jeniskonsep']== "1",
          _dataMandatoryFormMResurvey[0]['grupobjekunit']== "1",
          _dataMandatoryFormMResurvey[0]['fotoobjekpembiayaanunit']== "1",
          _dataMandatoryFormMResurvey[0]['dokumen']== "1",
        );

        //semua alamat hideshow & mandatory
        _setAddressIndividuInfoNasabah = SetAddressIndividu(
            _dataHideShowFormMResurvey[0]['jenisalamat'] == "1",
            _dataHideShowFormMResurvey[0]['alamat'] == "1",
            _dataHideShowFormMResurvey[0]['rt'] == "1",
            _dataHideShowFormMResurvey[0]['rw'] == "1",
            _dataHideShowFormMResurvey[0]['kelurahan'] == "1",
            _dataHideShowFormMResurvey[0]['kecamatan'] == "1",
            _dataHideShowFormMResurvey[0]['kabupatenkota'] == "1",
            _dataHideShowFormMResurvey[0]['provinsi'] == "1",
            _dataHideShowFormMResurvey[0]['kodepos'] == "1",
            _dataHideShowFormMResurvey[0]['teleponarea'] == "1",
            _dataHideShowFormMResurvey[0]['telepon'] == "1",
            _dataMandatoryFormMResurvey[0]['jenisalamat'] == "1",
            _dataMandatoryFormMResurvey[0]['alamat'] == "1",
            _dataMandatoryFormMResurvey[0]['rt'] == "1",
            _dataMandatoryFormMResurvey[0]['rw'] == "1",
            _dataMandatoryFormMResurvey[0]['kelurahan'] == "1",
            _dataMandatoryFormMResurvey[0]['kecamatan'] == "1",
            _dataMandatoryFormMResurvey[0]['kabupatenkota'] == "1",
            _dataMandatoryFormMResurvey[0]['provinsi'] == "1",
            _dataMandatoryFormMResurvey[0]['kodepos'] == "1",
            _dataMandatoryFormMResurvey[0]['teleponarea'] == "1",
            _dataMandatoryFormMResurvey[0]['telepon'] == "1"
        );
        _setAddressIndividuOccupation = SetAddressIndividu(
            _dataHideShowFormMResurvey[0]['jenisalamatpekerjaan']== "1",
            _dataHideShowFormMResurvey[0]['alamatpekerjaan']== "1",
            _dataHideShowFormMResurvey[0]['rtpekerjaan']== "1",
            _dataHideShowFormMResurvey[0]['rwpekerjaan']== "1",
            _dataHideShowFormMResurvey[0]['kelurahanpekerjaan']== "1",
            _dataHideShowFormMResurvey[0]['kecamatanpekerjaan']== "1",
            _dataHideShowFormMResurvey[0]['kabupatenkotapekerjaan']== "1",
            _dataHideShowFormMResurvey[0]['provinsipekerjaan']== "1",
            _dataHideShowFormMResurvey[0]['kodepospekerjaan']== "1",
            _dataHideShowFormMResurvey[0]['teleponareapekerjaan']== "1",
            _dataHideShowFormMResurvey[0]['teleponpekerjaan']== "1",
            _dataMandatoryFormMResurvey[0]['jenisalamatpekerjaan']== "1",
            _dataMandatoryFormMResurvey[0]['alamatpekerjaan']== "1",
            _dataMandatoryFormMResurvey[0]['rtpekerjaan']== "1",
            _dataMandatoryFormMResurvey[0]['rwpekerjaan']== "1",
            _dataMandatoryFormMResurvey[0]['kelurahanpekerjaan']== "1",
            _dataMandatoryFormMResurvey[0]['kecamatanpekerjaan']== "1",
            _dataMandatoryFormMResurvey[0]['kabupatenkotapekerjaan']== "1",
            _dataMandatoryFormMResurvey[0]['provinsipekerjaan']== "1",
            _dataMandatoryFormMResurvey[0]['kodepospekerjaan']== "1",
            _dataMandatoryFormMResurvey[0]['teleponareapekerjaan']== "1",
            _dataMandatoryFormMResurvey[0]['teleponpekerjaan'] == "1"
        );
        _setAddressIndividuPenjamin = SetAddressIndividu(
            _dataHideShowFormMResurvey[0]['jenisalamatalamat'] == "1",
            _dataHideShowFormMResurvey[0]['alamatpribadialamat'] == "1",
            _dataHideShowFormMResurvey[0]['rtpribadialamat'] == "1",
            _dataHideShowFormMResurvey[0]['rwpribadialamat'] == "1",
            _dataHideShowFormMResurvey[0]['kelurahanpribadialamat'] == "1",
            _dataHideShowFormMResurvey[0]['kecamatanpribadialamat'] == "1",
            _dataHideShowFormMResurvey[0]['kabupatenkotapribadialamat'] == "1",
            _dataHideShowFormMResurvey[0]['provinsipribadialamat'] == "1",
            _dataHideShowFormMResurvey[0]['kodepospribadialamat'] == "1",
            _dataHideShowFormMResurvey[0]['teleponareapribadialamat'] == "1",
            _dataHideShowFormMResurvey[0]['teleponpribadialamat'] == "1",
            _dataMandatoryFormMResurvey[0]['jenisalamatalamat'] == "1",
            _dataMandatoryFormMResurvey[0]['alamatpribadialamat'] == "1",
            _dataMandatoryFormMResurvey[0]['rtpribadialamat'] == "1",
            _dataMandatoryFormMResurvey[0]['rwpribadialamat'] == "1",
            _dataMandatoryFormMResurvey[0]['kelurahanpribadialamat'] == "1",
            _dataMandatoryFormMResurvey[0]['kecamatanpribadialamat'] == "1",
            _dataMandatoryFormMResurvey[0]['kabupatenkotapribadialamat'] == "1",
            _dataMandatoryFormMResurvey[0]['provinsipribadialamat'] == "1",
            _dataMandatoryFormMResurvey[0]['kodepospribadialamat'] == "1",
            _dataMandatoryFormMResurvey[0]['teleponareapribadialamat'] == "1",
            _dataMandatoryFormMResurvey[0]['teleponpribadialamat'] == "1"
        );
        _setAddressCompanyGuarantorIndividuModel = SetAddressCompanyModel(
          _dataHideShowFormMResurvey[0]['jenisalamatkelembagaan'] == "1",
          _dataHideShowFormMResurvey[0]['alamatkelembagaan'] == "1",
          _dataHideShowFormMResurvey[0]['rtkelembagaan'] == "1",
          _dataHideShowFormMResurvey[0]['rwkelembagaan'] == "1",
          _dataHideShowFormMResurvey[0]['kelurahankelembagaan'] == "1",
          _dataHideShowFormMResurvey[0]['kecamatankelembagaan'] == "1",
          _dataHideShowFormMResurvey[0]['kabupatenkotakelembagaan'] == "1",
          _dataHideShowFormMResurvey[0]['provinsikelembagaan'] == "1",
          _dataHideShowFormMResurvey[0]['kodeposkelembagaan'] == "1",
          _dataHideShowFormMResurvey[0]['teleponarea1KELEMBAGAAN'] == "1",
          _dataHideShowFormMResurvey[0]['telepon1KELEMBAGAAN'] == "1",
          _dataHideShowFormMResurvey[0]['teleponarea2KELEMBAGAAN'] == "1",
          _dataHideShowFormMResurvey[0]['telepon2KELEMBAGAAN'] == "1",
          _dataHideShowFormMResurvey[0]['faxareakelembagaan'] == "1",
          _dataHideShowFormMResurvey[0]['faxkelembagaan'] == "1",
          _dataMandatoryFormMResurvey[0]['jenis_ALAMAT'] == "1",
          _dataMandatoryFormMResurvey[0]['alamatkelembagaan'] == "1",
          _dataMandatoryFormMResurvey[0]['rtkelembagaan'] == "1",
          _dataMandatoryFormMResurvey[0]['rwkelembagaan'] == "1",
          _dataMandatoryFormMResurvey[0]['kelurahankelembagaan'] == "1",
          _dataMandatoryFormMResurvey[0]['kecamatankelembagaan'] == "1",
          _dataMandatoryFormMResurvey[0]['kabupatenkotakelembagaan'] == "1",
          _dataMandatoryFormMResurvey[0]['provinsikelembagaan'] == "1",
          _dataMandatoryFormMResurvey[0]['kodeposkelembagaan'] == "1",
          _dataMandatoryFormMResurvey[0]['teleponarea1KELEMBAGAAN'] == "1",
          _dataMandatoryFormMResurvey[0]['telepon1KELEMBAGAAN'] == "1",
          _dataMandatoryFormMResurvey[0]['teleponarea2KELEMBAGAAN'] == "1",
          _dataMandatoryFormMResurvey[0]['telepon2KELEMBAGAAN'] == "1",
          _dataMandatoryFormMResurvey[0]['faxareakelembagaan'] == "1",
          _dataMandatoryFormMResurvey[0]['faxkelembagaan'] == "1",
        );

        //info nasabah hideshow
        _providerInfoNasabah.isGCVisible  = _dataHideShowFormMResurvey[0]['gc'] == "1";
        _providerInfoNasabah.isIdentitasModelVisible = _dataHideShowFormMResurvey[0]['jenisidentitas'] == "1";
        _providerInfoNasabah.isNoIdentitasVisible = _dataHideShowFormMResurvey[0]['noidentitas'] == "1";
        _providerInfoNasabah.isTglIdentitasVisible = _dataHideShowFormMResurvey[0]['tanggalidentitas'] == "1";
        _providerInfoNasabah.isIdentitasBerlakuSampaiVisible = _dataHideShowFormMResurvey[0]['identitasberlaku'] == "1";
        _providerInfoNasabah.isNamaLengkapSesuaiIdentitasVisible = _dataHideShowFormMResurvey[0]['namalengkapid'] == "1";
        _providerInfoNasabah.isNamaLengkapVisible = _dataHideShowFormMResurvey[0]['namalengkap'] == "1";
        _providerInfoNasabah.isTglLahirVisible = _dataHideShowFormMResurvey[0]['tanggallahir'] == "1";
        _providerInfoNasabah.isTempatLahirSesuaiIdentitasVisible = _dataHideShowFormMResurvey[0]['tempatlahirid'] == "1";
        _providerInfoNasabah.isTempatLahirSesuaiIdentitasLOVVisible = _dataHideShowFormMResurvey[0]['tempatlahiridlov'] == "1";
        _providerInfoNasabah.isGenderVisible = _dataHideShowFormMResurvey[0]['jeniskelamin'] == "1";
        _providerInfoNasabah.isReligionVisible = _dataHideShowFormMResurvey[0]['agama'] == "1";
        _providerInfoNasabah.isEducationSelectedVisible = _dataHideShowFormMResurvey[0]['pendidikan'] == "1";
        _providerInfoNasabah.isJumlahTanggunganVisible = _dataHideShowFormMResurvey[0]['jumlahtanggungan'] == "1";
        _providerInfoNasabah.isMaritalStatusSelectedVisible = _dataHideShowFormMResurvey[0]['statuspernikahan'] == "1";
        _providerInfoNasabah.isNoHpVisible = _dataHideShowFormMResurvey[0]['handphonepribadi'] == "1";
        _providerInfoNasabah.isNoHp1WAVisible = _dataHideShowFormMResurvey[0]['nowa1'] == "1";
        _providerInfoNasabah.isHaveNPWPVisible = _dataHideShowFormMResurvey[0]['punyanpwp'] == "1";
        _providerInfoNasabah.isNamaSesuaiNPWPVisible = _dataHideShowFormMResurvey[0]['namalengkapsesuainpwp'] == "1";
        _providerInfoNasabah.isJenisNPWPVisible = _dataHideShowFormMResurvey[0]['jenisnpwp'] == "1";
        _providerInfoNasabah.isTandaPKPVisible = _dataHideShowFormMResurvey[0]['tandapkp'] == "1";
        _providerInfoNasabah.isAlamatSesuaiNPWPVisible = _dataHideShowFormMResurvey[0]['alamatnpwp'] == "1";
        _providerInfoNasabah.isNoNPWPVisible = _dataHideShowFormMResurvey[0]['npwp'] == "1";

        //info nasabah mandatory
        _providerInfoNasabah.isGCMandatory  = _dataMandatoryFormMResurvey[0]['gc'] == "1";
        _providerInfoNasabah.isIdentitasModelMandatory = _dataMandatoryFormMResurvey[0]['jenisidentitas'] == "1";
        _providerInfoNasabah.isNoIdentitasMandatory = _dataMandatoryFormMResurvey[0]['noidentitas'] == "1";
        _providerInfoNasabah.isTglIdentitasMandatory = _dataMandatoryFormMResurvey[0]['tanggalidentitas'] == "1";
        _providerInfoNasabah.isIdentitasBerlakuSampaiMandatory = _dataMandatoryFormMResurvey[0]['identitasberlaku'] == "1";
        _providerInfoNasabah.isNamaLengkapSesuaiIdentitasMandatory = _dataMandatoryFormMResurvey[0]['namalengkapid'] == "1";
        _providerInfoNasabah.isNamaLengkapMandatory = _dataMandatoryFormMResurvey[0]['namalengkap'] == "1";
        _providerInfoNasabah.isTglLahirMandatory = _dataMandatoryFormMResurvey[0]['tanggallahir'] == "1";
        _providerInfoNasabah.isTempatLahirSesuaiIdentitasMandatory = _dataMandatoryFormMResurvey[0]['tempatlahirid'] == "1";
        _providerInfoNasabah.isTempatLahirSesuaiIdentitasLOVMandatory = _dataMandatoryFormMResurvey[0]['tempatlahiridlov'] == "1";
        _providerInfoNasabah.isGenderMandatory = _dataMandatoryFormMResurvey[0]['jeniskelamin'] == "1";
        _providerInfoNasabah.isReligionMandatory = _dataMandatoryFormMResurvey[0]['agama'] == "1";
        _providerInfoNasabah.isEducationSelectedMandatory = _dataMandatoryFormMResurvey[0]['pendidikan'] == "1";
        _providerInfoNasabah.isJumlahTanggunganMandatory = _dataMandatoryFormMResurvey[0]['jumlahtanggungan'] == "1";
        _providerInfoNasabah.isMaritalStatusSelectedMandatory = _dataMandatoryFormMResurvey[0]['statuspernikahan'] == "1";
        _providerInfoNasabah.isNoHpMandatory = _dataMandatoryFormMResurvey[0]['handphonepribadi'] == "1";
        _providerInfoNasabah.isNoHp1WAMandatory = _dataMandatoryFormMResurvey[0]['nowa1'] == "1";
        _providerInfoNasabah.isHaveNPWPMandatory = _dataMandatoryFormMResurvey[0]['punyanpwp'] == "1";
        _providerInfoNasabah.isNamaSesuaiNPWPMandatory = _dataMandatoryFormMResurvey[0]['namalengkapsesuainpwp'] == "1";
        _providerInfoNasabah.isJenisNPWPMandatory = _dataMandatoryFormMResurvey[0]['jenisnpwp'] == "1";
        _providerInfoNasabah.isTandaPKPMandatory = _dataMandatoryFormMResurvey[0]['tandapkp'] == "1";
        _providerInfoNasabah.isAlamatSesuaiNPWPMandatory = _dataMandatoryFormMResurvey[0]['alamatnpwp'] == "1";
        _providerInfoNasabah.isNoNPWPMandatory = _dataMandatoryFormMResurvey[0]['npwp'] == "1";

        //info keluarga ibu hideshow
        _providerInformasiKeluargaIBU.isRelationshipStatusVisible = _dataHideShowFormMResurvey[0]['statushubunganibu'] == "1";
        _providerInformasiKeluargaIBU.isIdentityVisible = _dataHideShowFormMResurvey[0]['jenisidentitasibu'] == "1";
        _providerInformasiKeluargaIBU.isNoIdentitasVisible = _dataHideShowFormMResurvey[0]['noidentitasibu'] == "1";
        _providerInformasiKeluargaIBU.isNamaLengkapdentitasVisible = _dataHideShowFormMResurvey[0]['namalengkapidibu'] == "1";
        _providerInformasiKeluargaIBU.isNamaIdentitasVisible = _dataHideShowFormMResurvey[0]['namalengkapibu'] == "1";
        _providerInformasiKeluargaIBU.isTglLahirVisible = _dataHideShowFormMResurvey[0]['tanggallahiribu'] == "1";
        _providerInformasiKeluargaIBU.isTempatLahirSesuaiIdentitasVisible = _dataHideShowFormMResurvey[0]['tempatlahiridibu'] == "1";
        _providerInformasiKeluargaIBU.isTempatLahirSesuaiIdentitasLOVVisible = _dataHideShowFormMResurvey[0]['tempatlahiridlovibu'] == "1";
        _providerInformasiKeluargaIBU.isRadioValueGenderVisible = _dataHideShowFormMResurvey[0]['jeniskelaminibu'] == "1";
        _providerInformasiKeluargaIBU.isKodeAreaVisible = _dataHideShowFormMResurvey[0]['teleponareaibu'] == "1";
        _providerInformasiKeluargaIBU.isTlpnVisible = _dataHideShowFormMResurvey[0]['teleponibu'] == "1";
        _providerInformasiKeluargaIBU.isNoHpVisible = _dataHideShowFormMResurvey[0]['handphoneibu'] == "1";

        //info keluarga ibu mandatory
        _providerInformasiKeluargaIBU.isRelationshipStatusMandatory = _dataMandatoryFormMResurvey[0]['statushubunganibu'] == "1";
        _providerInformasiKeluargaIBU.isIdentityMandatory = _dataMandatoryFormMResurvey[0]['jenisidentitasibu'] == "1";
        _providerInformasiKeluargaIBU.isNoIdentitasMandatory = _dataMandatoryFormMResurvey[0]['noidentitasibu'] == "1";
        _providerInformasiKeluargaIBU.isNamaLengkapdentitasMandatory = _dataMandatoryFormMResurvey[0]['namalengkapidibu'] == "1";
        _providerInformasiKeluargaIBU.isNamaIdentitasMandatory = _dataMandatoryFormMResurvey[0]['namalengkapibu'] == "1";
        _providerInformasiKeluargaIBU.isTglLahirMandatory = _dataMandatoryFormMResurvey[0]['tanggallahiribu'] == "1";
        _providerInformasiKeluargaIBU.isTempatLahirSesuaiIdentitasMandatory = _dataMandatoryFormMResurvey[0]['tempatlahiridibu'] == "1";
        _providerInformasiKeluargaIBU.isTempatLahirSesuaiIdentitasLOVMandatory = _dataMandatoryFormMResurvey[0]['tempatlahiridlovibu'] == "1";
        _providerInformasiKeluargaIBU.isRadioValueGenderMandatory = _dataMandatoryFormMResurvey[0]['jeniskelaminibu'] == "1";
        _providerInformasiKeluargaIBU.isKodeAreaMandatory = _dataMandatoryFormMResurvey[0]['teleponareaibu'] == "1";
        _providerInformasiKeluargaIBU.isTlpnMandatory = _dataMandatoryFormMResurvey[0]['teleponibu'] == "1";
        _providerInformasiKeluargaIBU.isNoHpMandatory = _dataMandatoryFormMResurvey[0]['handphoneibu'] == "1";

        //info list keluarga showhide & mandatory
        _setAddInfoKeluarga = SetAddInfoKeluargaModel(
          _dataHideShowFormMResurvey[0]['statushubunganspouse'] == "1",
          _dataHideShowFormMResurvey[0]['jenisidentitasspouse'] == "1",
          _dataHideShowFormMResurvey[0]['noidentitasspouse'] == "1",
          _dataHideShowFormMResurvey[0]['namalengkapidspouse'] == "1",
          _dataHideShowFormMResurvey[0]['namalengkapspouse'] == "1",
          _dataHideShowFormMResurvey[0]['tanggallahirspouse'] == "1",
          _dataHideShowFormMResurvey[0]['tempatlahiridspouse'] == "1",
          _dataHideShowFormMResurvey[0]['tempatlahiridlovspouse'] == "1",
          _dataHideShowFormMResurvey[0]['jeniskelaminspouse'] == "1",
          _dataHideShowFormMResurvey[0]['teleponareaspouse'] == "1",
          _dataHideShowFormMResurvey[0]['teleponspouse'] == "1",
          _dataHideShowFormMResurvey[0]['handphonespouse'] == "1",
          _dataMandatoryFormMResurvey[0]['statushubunganspouse'] == "1",
          _dataMandatoryFormMResurvey[0]['jenisidentitasspouse'] == "1",
          _dataMandatoryFormMResurvey[0]['noidentitasspouse'] == "1",
          _dataMandatoryFormMResurvey[0]['namalengkapidspouse'] == "1",
          _dataMandatoryFormMResurvey[0]['namalengkapspouse'] == "1",
          _dataMandatoryFormMResurvey[0]['tanggallahirspouse'] == "1",
          _dataMandatoryFormMResurvey[0]['tempatlahiridspouse'] == "1",
          _dataMandatoryFormMResurvey[0]['tempatlahiridlovspouse'] == "1",
          _dataMandatoryFormMResurvey[0]['jeniskelaminspouse'] == "1",
          _dataMandatoryFormMResurvey[0]['teleponareaspouse'] == "1",
          _dataMandatoryFormMResurvey[0]['teleponspouse'] == "1",
          _dataMandatoryFormMResurvey[0]['handphonespouse'] == "1",
        );

        //occupation hideshow & mandatory
        _showMandatoryOccupationModel = ShowMandatoryOccupationModel(
          _dataHideShowFormMResurvey[0]['lamabekerjaberjalan']== "1",
          _dataHideShowFormMResurvey[0]['namausahawiraswasta']== "1",
          _dataHideShowFormMResurvey[0]['jenisbadanusahawiraswasta']== "1",
          _dataHideShowFormMResurvey[0]['sektorekonomiwiraswasta']== "1",
          _dataHideShowFormMResurvey[0]['lapanganusahawiraswasta']== "1",
          _dataHideShowFormMResurvey[0]['statuslokasiwiraswasta']== "1",
          _dataHideShowFormMResurvey[0]['lokasiusahawiraswasta']== "1",
          _dataHideShowFormMResurvey[0]['totalpegawaiwiraswasta']== "1",
          _dataHideShowFormMResurvey[0]['totallamabekerjawiraswasta']== "1",
          _dataHideShowFormMResurvey[0]['jenisprofesiprofesional']== "1",
          _dataHideShowFormMResurvey[0]['sektorekonomiprofesional']== "1",
          _dataHideShowFormMResurvey[0]['lapanganusahaprofesional']== "1",
          _dataHideShowFormMResurvey[0]['statuslokasiprofesional']== "1",
          _dataHideShowFormMResurvey[0]['lokasiusahaprofesional']== "1",
          _dataHideShowFormMResurvey[0]['totallamabekerjaprofesional']== "1",
          _dataHideShowFormMResurvey[0]['namaperusahaanlainnya']== "1",
          _dataHideShowFormMResurvey[0]['jenisperusahaanlainnya']== "1",
          _dataHideShowFormMResurvey[0]['sektorekonomilainnya']== "1",
          _dataHideShowFormMResurvey[0]['lapanganusahalainnya']== "1",
          _dataHideShowFormMResurvey[0]['totalpegawailainnya']== "1",
          _dataHideShowFormMResurvey[0]['totallamabekerjalainnya']== "1",
          _dataHideShowFormMResurvey[0]['jenispephighrisklainnya'] == "1",
          _dataHideShowFormMResurvey[0]['statuspegawailainnya'] == "1",
          _dataMandatoryFormMResurvey[0]['lamabekerjaberjalan']== "1",
          _dataMandatoryFormMResurvey[0]['namausahawiraswasta']== "1",
          _dataMandatoryFormMResurvey[0]['jenisbadanusahawiraswasta']== "1",
          _dataMandatoryFormMResurvey[0]['sektorekonomiwiraswasta']== "1",
          _dataMandatoryFormMResurvey[0]['lapanganusahawiraswasta']== "1",
          _dataMandatoryFormMResurvey[0]['statuslokasiwiraswasta']== "1",
          _dataMandatoryFormMResurvey[0]['lokasiusahawiraswasta']== "1",
          _dataMandatoryFormMResurvey[0]['totalpegawaiwiraswasta']== "1",
          _dataMandatoryFormMResurvey[0]['totallamabekerjawiraswasta']== "1",
          _dataMandatoryFormMResurvey[0]['jenisprofesiprofesional']== "1",
          _dataMandatoryFormMResurvey[0]['sektorekonomiprofesional']== "1",
          _dataMandatoryFormMResurvey[0]['lapanganusahaprofesional']== "1",
          _dataMandatoryFormMResurvey[0]['statuslokasiprofesional']== "1",
          _dataMandatoryFormMResurvey[0]['lokasiusahaprofesional']== "1",
          _dataMandatoryFormMResurvey[0]['totallamabekerjaprofesional']== "1",
          _dataMandatoryFormMResurvey[0]['namaperusahaanlainnya']== "1",
          _dataMandatoryFormMResurvey[0]['jenisperusahaanlainnya']== "1",
          _dataMandatoryFormMResurvey[0]['sektorekonomilainnya']== "1",
          _dataMandatoryFormMResurvey[0]['lapanganusahalainnya']== "1",
          _dataMandatoryFormMResurvey[0]['totalpegawailainnya']== "1",
          _dataMandatoryFormMResurvey[0]['totallamabekerjalainnya']== "1",
          _dataMandatoryFormMResurvey[0]['jenispephighrisklainnya'] == "1",
          _dataMandatoryFormMResurvey[0]['statuspegawailainnya'] == "1",
        );

        //income hideshow & mandatory
        _showMandatoryIncomeModel = ShowMandatoryIncomeModel(
            _dataHideShowFormMResurvey[0]['incomeperbulanwiraswasta']== "1",
            _dataHideShowFormMResurvey[0]['incomelainnyawiraswasta']== "1",
            _dataHideShowFormMResurvey[0]['totalincomewiraswasta']== "1",
            _dataHideShowFormMResurvey[0]['pokokincomewiraswasta']== "1",
            _dataHideShowFormMResurvey[0]['labakotorwiraswasta']== "1",
            _dataHideShowFormMResurvey[0]['biayaoperasionalwiraswasta']== "1",
            _dataHideShowFormMResurvey[0]['biayalainnyawiraswasta']== "1",
            _dataHideShowFormMResurvey[0]['netsebelumpajakwiraswasta']== "1",
            _dataHideShowFormMResurvey[0]['pajakwiraswasta']== "1",
            _dataHideShowFormMResurvey[0]['netsetelahpajakwiraswasta']== "1",
            _dataHideShowFormMResurvey[0]['biayahidupwiraswasta']== "1",
            _dataHideShowFormMResurvey[0]['sisaincomewiraswasta']== "1",
            _dataHideShowFormMResurvey[0]['incomepasanganwiraswasta']== "1",
            _dataHideShowFormMResurvey[0]['angsuranlainnyawiraswasta']== "1",
            _dataHideShowFormMResurvey[0]['incomeprofesional']== "1",
            _dataHideShowFormMResurvey[0]['incomepasanganprofesional']== "1",
            _dataHideShowFormMResurvey[0]['incomelainnyaprofesional']== "1",
            _dataHideShowFormMResurvey[0]['totalincomeprofesional']== "1",
            _dataHideShowFormMResurvey[0]['biayahidupprofesional']== "1",
            _dataHideShowFormMResurvey[0]['sisaincomeprofesional']== "1",
            _dataHideShowFormMResurvey[0]['angsuranlainnyaprofesional']== "1",
            _dataMandatoryFormMResurvey[0]['incomeperbulanwiraswasta']== "1",
            _dataMandatoryFormMResurvey[0]['incomelainnyawiraswasta']== "1",
            _dataMandatoryFormMResurvey[0]['totalincomewiraswasta']== "1",
            _dataMandatoryFormMResurvey[0]['pokokincomewiraswasta']== "1",
            _dataMandatoryFormMResurvey[0]['labakotorwiraswasta']== "1",
            _dataMandatoryFormMResurvey[0]['biayaoperasionalwiraswasta']== "1",
            _dataMandatoryFormMResurvey[0]['biayalainnyawiraswasta']== "1",
            _dataMandatoryFormMResurvey[0]['netsebelumpajakwiraswasta']== "1",
            _dataMandatoryFormMResurvey[0]['pajakwiraswasta']== "1",
            _dataMandatoryFormMResurvey[0]['netsetelahpajakwiraswasta']== "1",
            _dataMandatoryFormMResurvey[0]['biayahidupwiraswasta']== "1",
            _dataMandatoryFormMResurvey[0]['sisaincomewiraswasta']== "1",
            _dataMandatoryFormMResurvey[0]['incomepasanganwiraswasta']== "1",
            _dataMandatoryFormMResurvey[0]['angsuranlainnyawiraswasta']== "1",
            _dataMandatoryFormMResurvey[0]['incomeprofesional']== "1",
            _dataMandatoryFormMResurvey[0]['incomepasanganprofesional']== "1",
            _dataMandatoryFormMResurvey[0]['incomelainnyaprofesional']== "1",
            _dataMandatoryFormMResurvey[0]['totalincomeprofesional']== "1",
            _dataMandatoryFormMResurvey[0]['biayahidupprofesional']== "1",
            _dataMandatoryFormMResurvey[0]['sisaincomeprofesional']== "1",
            _dataMandatoryFormMResurvey[0]['angsuranlainnyaprofesional']== "1"
        );

        //penjamin individu - lembaga hideshow & mandatory
        _setGuarantorIndividu = SetGuarantorIndividu(
          _dataHideShowFormMResurvey[0]['statushubunganpribadi'] == "1",
          _dataHideShowFormMResurvey[0]['jenisidentitaspribadi'] == "1",
          _dataHideShowFormMResurvey[0]['noidentitaspribadi'] == "1",
          _dataHideShowFormMResurvey[0]['namalengkapidpribadi'] == "1",
          _dataHideShowFormMResurvey[0]['namalengkappribadi'] == "1",
          _dataHideShowFormMResurvey[0]['tanggallahirpribadi'] == "1",
          _dataHideShowFormMResurvey[0]['tempatlahiridpribadi'] == "1",
          _dataHideShowFormMResurvey[0]['tempatlahiridlovpribadi'] == "1",
          _dataHideShowFormMResurvey[0]['jeniskelaminpribadi'] == "1",
          _dataHideShowFormMResurvey[0]['handphonepribadi'] == "1",
          _dataMandatoryFormMResurvey[0]['statushubunganpribadi'] == "1",
          _dataMandatoryFormMResurvey[0]['jenisidentitaspribadi'] == "1",
          _dataMandatoryFormMResurvey[0]['noidentitaspribadi'] == "1",
          _dataMandatoryFormMResurvey[0]['namalengkapidpribadi'] == "1",
          _dataMandatoryFormMResurvey[0]['namalengkappribadi'] == "1",
          _dataMandatoryFormMResurvey[0]['tanggallahirpribadi'] == "1",
          _dataMandatoryFormMResurvey[0]['tempatlahiridpribadi'] == "1",
          _dataMandatoryFormMResurvey[0]['tempatlahiridlovpribadi'] == "1",
          _dataMandatoryFormMResurvey[0]['jeniskelaminpribadi'] == "1",
          _dataMandatoryFormMResurvey[0]['handphonepribadi'] == "1",
        );
        _setGuarantorLembaga =  SetGuarantorCompanyModel(
            _dataHideShowFormMResurvey[0]['jenislembagakelembagaan'] == "1",
            _dataHideShowFormMResurvey[0]['profilkelembagaan'] == "1",
            _dataHideShowFormMResurvey[0]['namalembagakelembagaan'] == "1",
            _dataHideShowFormMResurvey[0]['npwpkelembagaan'] == "1",
            _dataMandatoryFormMResurvey[0]['jenislembagakelembagaan'] == "1",
            _dataMandatoryFormMResurvey[0]['profilkelembagaan'] == "1",
            _dataMandatoryFormMResurvey[0]['namalembagakelembagaan'] == "1",
            _dataMandatoryFormMResurvey[0]['npwpkelembagaan'] == "1"
        );
      }
      else if (_lastKnownState == "DKR") {
        print("masuk dakor");
        //foto hideshow dan mandatory
        _showMandatoryFotoModel = ShowMandatoryFotoModel(
          _dataHideShowFormMDakor[0]['fototempattinggal']== "1",
          _dataHideShowFormMDakor[0]['pekerjaanfoto']== "1",
          _dataHideShowFormMDakor[0]['fotousaha']== "1",
          _dataHideShowFormMDakor[0]['jenis_PEMBIAYAAN']== "1",
          _dataHideShowFormMDakor[0]['kegiatanusaha']== "1",
          _dataHideShowFormMDakor[0]['jeniskegiatanusaha']== "1",
          _dataHideShowFormMDakor[0]['jeniskonsep']== "1",
          _dataHideShowFormMDakor[0]['grupobjekunit']== "1",
          _dataHideShowFormMDakor[0]['fotoobjekpembiayaanunit']== "1",
          _dataHideShowFormMDakor[0]['dokumen']== "1",
          _dataMandatoryFormMDakor[0]['fototempattinggal']== "1",
          _dataMandatoryFormMDakor[0]['pekerjaanfoto']== "1",
          _dataMandatoryFormMDakor[0]['fotousaha']== "1",
          _dataMandatoryFormMDakor[0]['jenis_PEMBIAYAAN']== "1",
          _dataMandatoryFormMDakor[0]['kegiatanusaha']== "1",
          _dataMandatoryFormMDakor[0]['jeniskegiatanusaha']== "1",
          _dataMandatoryFormMDakor[0]['jeniskonsep']== "1",
          _dataMandatoryFormMDakor[0]['grupobjekunit']== "1",
          _dataMandatoryFormMDakor[0]['fotoobjekpembiayaanunit']== "1",
          _dataMandatoryFormMDakor[0]['dokumen']== "1",
        );

        //semua alamat hideshow & mandatory
        _setAddressIndividuInfoNasabah = SetAddressIndividu(
            _dataHideShowFormMDakor[0]['jenisalamat'] == "1",
            _dataHideShowFormMDakor[0]['alamat'] == "1",
            _dataHideShowFormMDakor[0]['rt'] == "1",
            _dataHideShowFormMDakor[0]['rw'] == "1",
            _dataHideShowFormMDakor[0]['kelurahan'] == "1",
            _dataHideShowFormMDakor[0]['kecamatan'] == "1",
            _dataHideShowFormMDakor[0]['kabupatenkota'] == "1",
            _dataHideShowFormMDakor[0]['provinsi'] == "1",
            _dataHideShowFormMDakor[0]['kodepos'] == "1",
            _dataHideShowFormMDakor[0]['teleponarea'] == "1",
            _dataHideShowFormMDakor[0]['telepon'] == "1",
            _dataMandatoryFormMDakor[0]['jenisalamat'] == "1",
            _dataMandatoryFormMDakor[0]['alamat'] == "1",
            _dataMandatoryFormMDakor[0]['rt'] == "1",
            _dataMandatoryFormMDakor[0]['rw'] == "1",
            _dataMandatoryFormMDakor[0]['kelurahan'] == "1",
            _dataMandatoryFormMDakor[0]['kecamatan'] == "1",
            _dataMandatoryFormMDakor[0]['kabupatenkota'] == "1",
            _dataMandatoryFormMDakor[0]['provinsi'] == "1",
            _dataMandatoryFormMDakor[0]['kodepos'] == "1",
            _dataMandatoryFormMDakor[0]['teleponarea'] == "1",
            _dataMandatoryFormMDakor[0]['telepon'] == "1"
        );
        _setAddressIndividuOccupation = SetAddressIndividu(
            _dataHideShowFormMDakor[0]['jenisalamatpekerjaan']== "1",
            _dataHideShowFormMDakor[0]['alamatpekerjaan']== "1",
            _dataHideShowFormMDakor[0]['rtpekerjaan']== "1",
            _dataHideShowFormMDakor[0]['rwpekerjaan']== "1",
            _dataHideShowFormMDakor[0]['kelurahanpekerjaan']== "1",
            _dataHideShowFormMDakor[0]['kecamatanpekerjaan']== "1",
            _dataHideShowFormMDakor[0]['kabupatenkotapekerjaan']== "1",
            _dataHideShowFormMDakor[0]['provinsipekerjaan']== "1",
            _dataHideShowFormMDakor[0]['kodepospekerjaan']== "1",
            _dataHideShowFormMDakor[0]['teleponareapekerjaan']== "1",
            _dataHideShowFormMDakor[0]['teleponpekerjaan']== "1",
            _dataMandatoryFormMDakor[0]['jenisalamatpekerjaan']== "1",
            _dataMandatoryFormMDakor[0]['alamatpekerjaan']== "1",
            _dataMandatoryFormMDakor[0]['rtpekerjaan']== "1",
            _dataMandatoryFormMDakor[0]['rwpekerjaan']== "1",
            _dataMandatoryFormMDakor[0]['kelurahanpekerjaan']== "1",
            _dataMandatoryFormMDakor[0]['kecamatanpekerjaan']== "1",
            _dataMandatoryFormMDakor[0]['kabupatenkotapekerjaan']== "1",
            _dataMandatoryFormMDakor[0]['provinsipekerjaan']== "1",
            _dataMandatoryFormMDakor[0]['kodepospekerjaan']== "1",
            _dataMandatoryFormMDakor[0]['teleponareapekerjaan']== "1",
            _dataMandatoryFormMDakor[0]['teleponpekerjaan'] == "1"
        );
        _setAddressIndividuPenjamin = SetAddressIndividu(
            _dataHideShowFormMDakor[0]['jenisalamatalamat'] == "1",
            _dataHideShowFormMDakor[0]['alamatpribadialamat'] == "1",
            _dataHideShowFormMDakor[0]['rtpribadialamat'] == "1",
            _dataHideShowFormMDakor[0]['rwpribadialamat'] == "1",
            _dataHideShowFormMDakor[0]['kelurahanpribadialamat'] == "1",
            _dataHideShowFormMDakor[0]['kecamatanpribadialamat'] == "1",
            _dataHideShowFormMDakor[0]['kabupatenkotapribadialamat'] == "1",
            _dataHideShowFormMDakor[0]['provinsipribadialamat'] == "1",
            _dataHideShowFormMDakor[0]['kodepospribadialamat'] == "1",
            _dataHideShowFormMDakor[0]['teleponareapribadialamat'] == "1",
            _dataHideShowFormMDakor[0]['teleponpribadialamat'] == "1",
            _dataMandatoryFormMDakor[0]['jenisalamatalamat'] == "1",
            _dataMandatoryFormMDakor[0]['alamatpribadialamat'] == "1",
            _dataMandatoryFormMDakor[0]['rtpribadialamat'] == "1",
            _dataMandatoryFormMDakor[0]['rwpribadialamat'] == "1",
            _dataMandatoryFormMDakor[0]['kelurahanpribadialamat'] == "1",
            _dataMandatoryFormMDakor[0]['kecamatanpribadialamat'] == "1",
            _dataMandatoryFormMDakor[0]['kabupatenkotapribadialamat'] == "1",
            _dataMandatoryFormMDakor[0]['provinsipribadialamat'] == "1",
            _dataMandatoryFormMDakor[0]['kodepospribadialamat'] == "1",
            _dataMandatoryFormMDakor[0]['teleponareapribadialamat'] == "1",
            _dataMandatoryFormMDakor[0]['teleponpribadialamat'] == "1"
        );
        _setAddressCompanyGuarantorIndividuModel = SetAddressCompanyModel(
          _dataHideShowFormMDakor[0]['jenisalamatkelembagaan'] == "1",
          _dataHideShowFormMDakor[0]['alamatkelembagaan'] == "1",
          _dataHideShowFormMDakor[0]['rtkelembagaan'] == "1",
          _dataHideShowFormMDakor[0]['rwkelembagaan'] == "1",
          _dataHideShowFormMDakor[0]['kelurahankelembagaan'] == "1",
          _dataHideShowFormMDakor[0]['kecamatankelembagaan'] == "1",
          _dataHideShowFormMDakor[0]['kabupatenkotakelembagaan'] == "1",
          _dataHideShowFormMDakor[0]['provinsikelembagaan'] == "1",
          _dataHideShowFormMDakor[0]['kodeposkelembagaan'] == "1",
          _dataHideShowFormMDakor[0]['teleponarea1KELEMBAGAAN'] == "1",
          _dataHideShowFormMDakor[0]['telepon1KELEMBAGAAN'] == "1",
          _dataHideShowFormMDakor[0]['teleponarea2KELEMBAGAAN'] == "1",
          _dataHideShowFormMDakor[0]['telepon2KELEMBAGAAN'] == "1",
          _dataHideShowFormMDakor[0]['faxareakelembagaan'] == "1",
          _dataHideShowFormMDakor[0]['faxkelembagaan'] == "1",
          _dataMandatoryFormMDakor[0]['jenis_ALAMAT'] == "1",
          _dataMandatoryFormMDakor[0]['alamatkelembagaan'] == "1",
          _dataMandatoryFormMDakor[0]['rtkelembagaan'] == "1",
          _dataMandatoryFormMDakor[0]['rwkelembagaan'] == "1",
          _dataMandatoryFormMDakor[0]['kelurahankelembagaan'] == "1",
          _dataMandatoryFormMDakor[0]['kecamatankelembagaan'] == "1",
          _dataMandatoryFormMDakor[0]['kabupatenkotakelembagaan'] == "1",
          _dataMandatoryFormMDakor[0]['provinsikelembagaan'] == "1",
          _dataMandatoryFormMDakor[0]['kodeposkelembagaan'] == "1",
          _dataMandatoryFormMDakor[0]['teleponarea1KELEMBAGAAN'] == "1",
          _dataMandatoryFormMDakor[0]['telepon1KELEMBAGAAN'] == "1",
          _dataMandatoryFormMDakor[0]['teleponarea2KELEMBAGAAN'] == "1",
          _dataMandatoryFormMDakor[0]['telepon2KELEMBAGAAN'] == "1",
          _dataMandatoryFormMDakor[0]['faxareakelembagaan'] == "1",
          _dataMandatoryFormMDakor[0]['faxkelembagaan'] == "1",
        );

        //info nasabah hideshow
        _providerInfoNasabah.isGCVisible  = _dataHideShowFormMDakor[0]['gc'] == "1";
        _providerInfoNasabah.isIdentitasModelVisible = _dataHideShowFormMDakor[0]['jenisidentitas'] == "1";
        _providerInfoNasabah.isNoIdentitasVisible = _dataHideShowFormMDakor[0]['noidentitas'] == "1";
        _providerInfoNasabah.isTglIdentitasVisible = _dataHideShowFormMDakor[0]['tanggalidentitas'] == "1";
        _providerInfoNasabah.isIdentitasBerlakuSampaiVisible = _dataHideShowFormMDakor[0]['identitasberlaku'] == "1";
        _providerInfoNasabah.isNamaLengkapSesuaiIdentitasVisible = _dataHideShowFormMDakor[0]['namalengkapid'] == "1";
        _providerInfoNasabah.isNamaLengkapVisible = _dataHideShowFormMDakor[0]['namalengkap'] == "1";
        _providerInfoNasabah.isTglLahirVisible = _dataHideShowFormMDakor[0]['tanggallahir'] == "1";
        _providerInfoNasabah.isTempatLahirSesuaiIdentitasVisible = _dataHideShowFormMDakor[0]['tempatlahirid'] == "1";
        _providerInfoNasabah.isTempatLahirSesuaiIdentitasLOVVisible = _dataHideShowFormMDakor[0]['tempatlahiridlov'] == "1";
        _providerInfoNasabah.isGenderVisible = _dataHideShowFormMDakor[0]['jeniskelamin'] == "1";
        _providerInfoNasabah.isReligionVisible = _dataHideShowFormMDakor[0]['agama'] == "1";
        _providerInfoNasabah.isEducationSelectedVisible = _dataHideShowFormMDakor[0]['pendidikan'] == "1";
        _providerInfoNasabah.isJumlahTanggunganVisible = _dataHideShowFormMDakor[0]['jumlahtanggungan'] == "1";
        _providerInfoNasabah.isMaritalStatusSelectedVisible = _dataHideShowFormMDakor[0]['statuspernikahan'] == "1";
        _providerInfoNasabah.isNoHpVisible = _dataHideShowFormMDakor[0]['handphonepribadi'] == "1";
        _providerInfoNasabah.isNoHp1WAVisible = _dataHideShowFormMDakor[0]['nowa1'] == "1";
        _providerInfoNasabah.isHaveNPWPVisible = _dataHideShowFormMDakor[0]['punyanpwp'] == "1";
        _providerInfoNasabah.isNamaSesuaiNPWPVisible = _dataHideShowFormMDakor[0]['namalengkapsesuainpwp'] == "1";
        _providerInfoNasabah.isJenisNPWPVisible = _dataHideShowFormMDakor[0]['jenisnpwp'] == "1";
        _providerInfoNasabah.isTandaPKPVisible = _dataHideShowFormMDakor[0]['tandapkp'] == "1";
        _providerInfoNasabah.isAlamatSesuaiNPWPVisible = _dataHideShowFormMDakor[0]['alamatnpwp'] == "1";
        _providerInfoNasabah.isNoNPWPVisible = _dataHideShowFormMDakor[0]['npwp'] == "1";

        //info nasabah mandatory
        _providerInfoNasabah.isGCMandatory  = _dataMandatoryFormMDakor[0]['gc'] == "1";
        _providerInfoNasabah.isIdentitasModelMandatory = _dataMandatoryFormMDakor[0]['jenisidentitas'] == "1";
        _providerInfoNasabah.isNoIdentitasMandatory = _dataMandatoryFormMDakor[0]['noidentitas'] == "1";
        _providerInfoNasabah.isTglIdentitasMandatory = _dataMandatoryFormMDakor[0]['tanggalidentitas'] == "1";
        _providerInfoNasabah.isIdentitasBerlakuSampaiMandatory = _dataMandatoryFormMDakor[0]['identitasberlaku'] == "1";
        _providerInfoNasabah.isNamaLengkapSesuaiIdentitasMandatory = _dataMandatoryFormMDakor[0]['namalengkapid'] == "1";
        _providerInfoNasabah.isNamaLengkapMandatory = _dataMandatoryFormMDakor[0]['namalengkap'] == "1";
        _providerInfoNasabah.isTglLahirMandatory = _dataMandatoryFormMDakor[0]['tanggallahir'] == "1";
        _providerInfoNasabah.isTempatLahirSesuaiIdentitasMandatory = _dataMandatoryFormMDakor[0]['tempatlahirid'] == "1";
        _providerInfoNasabah.isTempatLahirSesuaiIdentitasLOVMandatory = _dataMandatoryFormMDakor[0]['tempatlahiridlov'] == "1";
        _providerInfoNasabah.isGenderMandatory = _dataMandatoryFormMDakor[0]['jeniskelamin'] == "1";
        _providerInfoNasabah.isReligionMandatory = _dataMandatoryFormMDakor[0]['agama'] == "1";
        _providerInfoNasabah.isEducationSelectedMandatory = _dataMandatoryFormMDakor[0]['pendidikan'] == "1";
        _providerInfoNasabah.isJumlahTanggunganMandatory = _dataMandatoryFormMDakor[0]['jumlahtanggungan'] == "1";
        _providerInfoNasabah.isMaritalStatusSelectedMandatory = _dataMandatoryFormMDakor[0]['statuspernikahan'] == "1";
        _providerInfoNasabah.isNoHpMandatory = _dataMandatoryFormMDakor[0]['handphonepribadi'] == "1";
        _providerInfoNasabah.isNoHp1WAMandatory = _dataMandatoryFormMDakor[0]['nowa1'] == "1";
        _providerInfoNasabah.isHaveNPWPMandatory = _dataMandatoryFormMDakor[0]['punyanpwp'] == "1";
        _providerInfoNasabah.isNamaSesuaiNPWPMandatory = _dataMandatoryFormMDakor[0]['namalengkapsesuainpwp'] == "1";
        _providerInfoNasabah.isJenisNPWPMandatory = _dataMandatoryFormMDakor[0]['jenisnpwp'] == "1";
        _providerInfoNasabah.isTandaPKPMandatory = _dataMandatoryFormMDakor[0]['tandapkp'] == "1";
        _providerInfoNasabah.isAlamatSesuaiNPWPMandatory = _dataMandatoryFormMDakor[0]['alamatnpwp'] == "1";
        _providerInfoNasabah.isNoNPWPMandatory = _dataMandatoryFormMDakor[0]['npwp'] == "1";

        //info keluarga ibu hideshow
        _providerInformasiKeluargaIBU.isRelationshipStatusVisible = _dataHideShowFormMDakor[0]['statushubunganibu'] == "1";
        _providerInformasiKeluargaIBU.isIdentityVisible = _dataHideShowFormMDakor[0]['jenisidentitasibu'] == "1";
        _providerInformasiKeluargaIBU.isNoIdentitasVisible = _dataHideShowFormMDakor[0]['noidentitasibu'] == "1";
        _providerInformasiKeluargaIBU.isNamaLengkapdentitasVisible = _dataHideShowFormMDakor[0]['namalengkapidibu'] == "1";
        _providerInformasiKeluargaIBU.isNamaIdentitasVisible = _dataHideShowFormMDakor[0]['namalengkapibu'] == "1";
        _providerInformasiKeluargaIBU.isTglLahirVisible = _dataHideShowFormMDakor[0]['tanggallahiribu'] == "1";
        _providerInformasiKeluargaIBU.isTempatLahirSesuaiIdentitasVisible = _dataHideShowFormMDakor[0]['tempatlahiridibu'] == "1";
        _providerInformasiKeluargaIBU.isTempatLahirSesuaiIdentitasLOVVisible = _dataHideShowFormMDakor[0]['tempatlahiridlovibu'] == "1";
        _providerInformasiKeluargaIBU.isRadioValueGenderVisible = _dataHideShowFormMDakor[0]['jeniskelaminibu'] == "1";
        _providerInformasiKeluargaIBU.isKodeAreaVisible = _dataHideShowFormMDakor[0]['teleponareaibu'] == "1";
        _providerInformasiKeluargaIBU.isTlpnVisible = _dataHideShowFormMDakor[0]['teleponibu'] == "1";
        _providerInformasiKeluargaIBU.isNoHpVisible = _dataHideShowFormMDakor[0]['handphoneibu'] == "1";

        //info keluarga ibu mandatory
        _providerInformasiKeluargaIBU.isRelationshipStatusMandatory = _dataMandatoryFormMDakor[0]['statushubunganibu'] == "1";
        _providerInformasiKeluargaIBU.isIdentityMandatory = _dataMandatoryFormMDakor[0]['jenisidentitasibu'] == "1";
        _providerInformasiKeluargaIBU.isNoIdentitasMandatory = _dataMandatoryFormMDakor[0]['noidentitasibu'] == "1";
        _providerInformasiKeluargaIBU.isNamaLengkapdentitasMandatory = _dataMandatoryFormMDakor[0]['namalengkapidibu'] == "1";
        _providerInformasiKeluargaIBU.isNamaIdentitasMandatory = _dataMandatoryFormMDakor[0]['namalengkapibu'] == "1";
        _providerInformasiKeluargaIBU.isTglLahirMandatory = _dataMandatoryFormMDakor[0]['tanggallahiribu'] == "1";
        _providerInformasiKeluargaIBU.isTempatLahirSesuaiIdentitasMandatory = _dataMandatoryFormMDakor[0]['tempatlahiridibu'] == "1";
        _providerInformasiKeluargaIBU.isTempatLahirSesuaiIdentitasLOVMandatory = _dataMandatoryFormMDakor[0]['tempatlahiridlovibu'] == "1";
        _providerInformasiKeluargaIBU.isRadioValueGenderMandatory = _dataMandatoryFormMDakor[0]['jeniskelaminibu'] == "1";
        _providerInformasiKeluargaIBU.isKodeAreaMandatory = _dataMandatoryFormMDakor[0]['teleponareaibu'] == "1";
        _providerInformasiKeluargaIBU.isTlpnMandatory = _dataMandatoryFormMDakor[0]['teleponibu'] == "1";
        _providerInformasiKeluargaIBU.isNoHpMandatory = _dataMandatoryFormMDakor[0]['handphoneibu'] == "1";

        //info list keluarga showhide & mandatory
        _setAddInfoKeluarga = SetAddInfoKeluargaModel(
          _dataHideShowFormMDakor[0]['statushubunganspouse'] == "1",
          _dataHideShowFormMDakor[0]['jenisidentitasspouse'] == "1",
          _dataHideShowFormMDakor[0]['noidentitasspouse'] == "1",
          _dataHideShowFormMDakor[0]['namalengkapidspouse'] == "1",
          _dataHideShowFormMDakor[0]['namalengkapspouse'] == "1",
          _dataHideShowFormMDakor[0]['tanggallahirspouse'] == "1",
          _dataHideShowFormMDakor[0]['tempatlahiridspouse'] == "1",
          _dataHideShowFormMDakor[0]['tempatlahiridlovspouse'] == "1",
          _dataHideShowFormMDakor[0]['jeniskelaminspouse'] == "1",
          _dataHideShowFormMDakor[0]['teleponareaspouse'] == "1",
          _dataHideShowFormMDakor[0]['teleponspouse'] == "1",
          _dataHideShowFormMDakor[0]['handphonespouse'] == "1",
          _dataMandatoryFormMDakor[0]['statushubunganspouse'] == "1",
          _dataMandatoryFormMDakor[0]['jenisidentitasspouse'] == "1",
          _dataMandatoryFormMDakor[0]['noidentitasspouse'] == "1",
          _dataMandatoryFormMDakor[0]['namalengkapidspouse'] == "1",
          _dataMandatoryFormMDakor[0]['namalengkapspouse'] == "1",
          _dataMandatoryFormMDakor[0]['tanggallahirspouse'] == "1",
          _dataMandatoryFormMDakor[0]['tempatlahiridspouse'] == "1",
          _dataMandatoryFormMDakor[0]['tempatlahiridlovspouse'] == "1",
          _dataMandatoryFormMDakor[0]['jeniskelaminspouse'] == "1",
          _dataMandatoryFormMDakor[0]['teleponareaspouse'] == "1",
          _dataMandatoryFormMDakor[0]['teleponspouse'] == "1",
          _dataMandatoryFormMDakor[0]['handphonespouse'] == "1",
        );

        //occupation hideshow & mandatory
        _showMandatoryOccupationModel = ShowMandatoryOccupationModel(
          _dataHideShowFormMDakor[0]['lamabekerjaberjalan']== "1",
          _dataHideShowFormMDakor[0]['namausahawiraswasta']== "1",
          _dataHideShowFormMDakor[0]['jenisbadanusahawiraswasta']== "1",
          _dataHideShowFormMDakor[0]['sektorekonomiwiraswasta']== "1",
          _dataHideShowFormMDakor[0]['lapanganusahawiraswasta']== "1",
          _dataHideShowFormMDakor[0]['statuslokasiwiraswasta']== "1",
          _dataHideShowFormMDakor[0]['lokasiusahawiraswasta']== "1",
          _dataHideShowFormMDakor[0]['totalpegawaiwiraswasta']== "1",
          _dataHideShowFormMDakor[0]['totallamabekerjawiraswasta']== "1",
          _dataHideShowFormMDakor[0]['jenisprofesiprofesional']== "1",
          _dataHideShowFormMDakor[0]['sektorekonomiprofesional']== "1",
          _dataHideShowFormMDakor[0]['lapanganusahaprofesional']== "1",
          _dataHideShowFormMDakor[0]['statuslokasiprofesional']== "1",
          _dataHideShowFormMDakor[0]['lokasiusahaprofesional']== "1",
          _dataHideShowFormMDakor[0]['totallamabekerjaprofesional']== "1",
          _dataHideShowFormMDakor[0]['namaperusahaanlainnya']== "1",
          _dataHideShowFormMDakor[0]['jenisperusahaanlainnya']== "1",
          _dataHideShowFormMDakor[0]['sektorekonomilainnya']== "1",
          _dataHideShowFormMDakor[0]['lapanganusahalainnya']== "1",
          _dataHideShowFormMDakor[0]['totalpegawailainnya']== "1",
          _dataHideShowFormMDakor[0]['totallamabekerjalainnya']== "1",
          _dataHideShowFormMDakor[0]['jenispephighrisklainnya'] == "1",
          _dataHideShowFormMDakor[0]['statuspegawailainnya'] == "1",
          _dataMandatoryFormMDakor[0]['lamabekerjaberjalan']== "1",
          _dataMandatoryFormMDakor[0]['namausahawiraswasta']== "1",
          _dataMandatoryFormMDakor[0]['jenisbadanusahawiraswasta']== "1",
          _dataMandatoryFormMDakor[0]['sektorekonomiwiraswasta']== "1",
          _dataMandatoryFormMDakor[0]['lapanganusahawiraswasta']== "1",
          _dataMandatoryFormMDakor[0]['statuslokasiwiraswasta']== "1",
          _dataMandatoryFormMDakor[0]['lokasiusahawiraswasta']== "1",
          _dataMandatoryFormMDakor[0]['totalpegawaiwiraswasta']== "1",
          _dataMandatoryFormMDakor[0]['totallamabekerjawiraswasta']== "1",
          _dataMandatoryFormMDakor[0]['jenisprofesiprofesional']== "1",
          _dataMandatoryFormMDakor[0]['sektorekonomiprofesional']== "1",
          _dataMandatoryFormMDakor[0]['lapanganusahaprofesional']== "1",
          _dataMandatoryFormMDakor[0]['statuslokasiprofesional']== "1",
          _dataMandatoryFormMDakor[0]['lokasiusahaprofesional']== "1",
          _dataMandatoryFormMDakor[0]['totallamabekerjaprofesional']== "1",
          _dataMandatoryFormMDakor[0]['namaperusahaanlainnya']== "1",
          _dataMandatoryFormMDakor[0]['jenisperusahaanlainnya']== "1",
          _dataMandatoryFormMDakor[0]['sektorekonomilainnya']== "1",
          _dataMandatoryFormMDakor[0]['lapanganusahalainnya']== "1",
          _dataMandatoryFormMDakor[0]['totalpegawailainnya']== "1",
          _dataMandatoryFormMDakor[0]['totallamabekerjalainnya']== "1",
          _dataMandatoryFormMDakor[0]['jenispephighrisklainnya'] == "1",
          _dataMandatoryFormMDakor[0]['statuspegawailainnya'] == "1",
        );

        //income hideshow & mandatory
        _showMandatoryIncomeModel = ShowMandatoryIncomeModel(
            _dataHideShowFormMDakor[0]['incomeperbulanwiraswasta']== "1",
            _dataHideShowFormMDakor[0]['incomelainnyawiraswasta']== "1",
            _dataHideShowFormMDakor[0]['totalincomewiraswasta']== "1",
            _dataHideShowFormMDakor[0]['pokokincomewiraswasta']== "1",
            _dataHideShowFormMDakor[0]['labakotorwiraswasta']== "1",
            _dataHideShowFormMDakor[0]['biayaoperasionalwiraswasta']== "1",
            _dataHideShowFormMDakor[0]['biayalainnyawiraswasta']== "1",
            _dataHideShowFormMDakor[0]['netsebelumpajakwiraswasta']== "1",
            _dataHideShowFormMDakor[0]['pajakwiraswasta']== "1",
            _dataHideShowFormMDakor[0]['netsetelahpajakwiraswasta']== "1",
            _dataHideShowFormMDakor[0]['biayahidupwiraswasta']== "1",
            _dataHideShowFormMDakor[0]['sisaincomewiraswasta']== "1",
            _dataHideShowFormMDakor[0]['incomepasanganwiraswasta']== "1",
            _dataHideShowFormMDakor[0]['angsuranlainnyawiraswasta']== "1",
            _dataHideShowFormMDakor[0]['incomeprofesional']== "1",
            _dataHideShowFormMDakor[0]['incomepasanganprofesional']== "1",
            _dataHideShowFormMDakor[0]['incomelainnyaprofesional']== "1",
            _dataHideShowFormMDakor[0]['totalincomeprofesional']== "1",
            _dataHideShowFormMDakor[0]['biayahidupprofesional']== "1",
            _dataHideShowFormMDakor[0]['sisaincomeprofesional']== "1",
            _dataHideShowFormMDakor[0]['angsuranlainnyaprofesional']== "1",
            _dataMandatoryFormMDakor[0]['incomeperbulanwiraswasta']== "1",
            _dataMandatoryFormMDakor[0]['incomelainnyawiraswasta']== "1",
            _dataMandatoryFormMDakor[0]['totalincomewiraswasta']== "1",
            _dataMandatoryFormMDakor[0]['pokokincomewiraswasta']== "1",
            _dataMandatoryFormMDakor[0]['labakotorwiraswasta']== "1",
            _dataMandatoryFormMDakor[0]['biayaoperasionalwiraswasta']== "1",
            _dataMandatoryFormMDakor[0]['biayalainnyawiraswasta']== "1",
            _dataMandatoryFormMDakor[0]['netsebelumpajakwiraswasta']== "1",
            _dataMandatoryFormMDakor[0]['pajakwiraswasta']== "1",
            _dataMandatoryFormMDakor[0]['netsetelahpajakwiraswasta']== "1",
            _dataMandatoryFormMDakor[0]['biayahidupwiraswasta']== "1",
            _dataMandatoryFormMDakor[0]['sisaincomewiraswasta']== "1",
            _dataMandatoryFormMDakor[0]['incomepasanganwiraswasta']== "1",
            _dataMandatoryFormMDakor[0]['angsuranlainnyawiraswasta']== "1",
            _dataMandatoryFormMDakor[0]['incomeprofesional']== "1",
            _dataMandatoryFormMDakor[0]['incomepasanganprofesional']== "1",
            _dataMandatoryFormMDakor[0]['incomelainnyaprofesional']== "1",
            _dataMandatoryFormMDakor[0]['totalincomeprofesional']== "1",
            _dataMandatoryFormMDakor[0]['biayahidupprofesional']== "1",
            _dataMandatoryFormMDakor[0]['sisaincomeprofesional']== "1",
            _dataMandatoryFormMDakor[0]['angsuranlainnyaprofesional']== "1"
        );

        //penjamin individu - lembaga hideshow & mandatory
        _setGuarantorIndividu = SetGuarantorIndividu(
          _dataHideShowFormMDakor[0]['statushubunganpribadi'] == "1",
          _dataHideShowFormMDakor[0]['jenisidentitaspribadi'] == "1",
          _dataHideShowFormMDakor[0]['noidentitaspribadi'] == "1",
          _dataHideShowFormMDakor[0]['namalengkapidpribadi'] == "1",
          _dataHideShowFormMDakor[0]['namalengkappribadi'] == "1",
          _dataHideShowFormMDakor[0]['tanggallahirpribadi'] == "1",
          _dataHideShowFormMDakor[0]['tempatlahiridpribadi'] == "1",
          _dataHideShowFormMDakor[0]['tempatlahiridlovpribadi'] == "1",
          _dataHideShowFormMDakor[0]['jeniskelaminpribadi'] == "1",
          _dataHideShowFormMDakor[0]['handphonepribadi'] == "1",
          _dataMandatoryFormMDakor[0]['statushubunganpribadi'] == "1",
          _dataMandatoryFormMDakor[0]['jenisidentitaspribadi'] == "1",
          _dataMandatoryFormMDakor[0]['noidentitaspribadi'] == "1",
          _dataMandatoryFormMDakor[0]['namalengkapidpribadi'] == "1",
          _dataMandatoryFormMDakor[0]['namalengkappribadi'] == "1",
          _dataMandatoryFormMDakor[0]['tanggallahirpribadi'] == "1",
          _dataMandatoryFormMDakor[0]['tempatlahiridpribadi'] == "1",
          _dataMandatoryFormMDakor[0]['tempatlahiridlovpribadi'] == "1",
          _dataMandatoryFormMDakor[0]['jeniskelaminpribadi'] == "1",
          _dataMandatoryFormMDakor[0]['handphonepribadi'] == "1",
        );
        _setGuarantorLembaga =  SetGuarantorCompanyModel(
            _dataHideShowFormMDakor[0]['jenislembagakelembagaan'] == "1",
            _dataHideShowFormMDakor[0]['profilkelembagaan'] == "1",
            _dataHideShowFormMDakor[0]['namalembagakelembagaan'] == "1",
            _dataHideShowFormMDakor[0]['npwpkelembagaan'] == "1",
            _dataMandatoryFormMDakor[0]['jenislembagakelembagaan'] == "1",
            _dataMandatoryFormMDakor[0]['profilkelembagaan'] == "1",
            _dataMandatoryFormMDakor[0]['namalembagakelembagaan'] == "1",
            _dataMandatoryFormMDakor[0]['npwpkelembagaan'] == "1"
        );
      }
    }
    else{
      print("masuk com");
      if(_lastKnownState == "IDE"){
        //rincian company hideshow
        _providerRincianCompany.isCompanyTypeShow = _dataHSIdeCom[0]['jenis_LEMBAGA_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isProfileShow = _dataHSIdeCom[0]['profil_RINCIAN'] == "1";
        _providerRincianCompany.isCompanyNameShow = _dataHSIdeCom[0]['nama_LEMBAGA_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isHaveNPWPShow = _dataHSIdeCom[0]['punya_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isNoNPWPShow = _dataHSIdeCom[0]['npwp_RINCIAN'] == "1";
        _providerRincianCompany.isNameNPWPShow = _dataHSIdeCom[0]['nama_LENGKAP_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isTypeNPWPShow = _dataHSIdeCom[0]['jenis_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isPKPSignShow = _dataHSIdeCom[0]['tanda_PKP_RINCIAN'] == "1";
        _providerRincianCompany.isAddressNPWPShow = _dataHSIdeCom[0]['alamat_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isEstablishDateShow = _dataHSIdeCom[0]['tanggal_PENDIRIAN_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isSectorEconomicShow = _dataHSIdeCom[0]['sektor_EKONOMI_RINCIAN'] == "1";
        _providerRincianCompany.isBusinessFieldShow = _dataHSIdeCom[0]['lapangan_USAHA_RINCIAN'] == "1";
        _providerRincianCompany.isLocationStatusShow = _dataHSIdeCom[0]['status_LOKASI_RINCIAN'] == "1";
        _providerRincianCompany.isBusinessLocationShow = _dataHSIdeCom[0]['lokasi_USAHA_RINCIAN'] == "1";
        _providerRincianCompany.isTotalEmpShow = _dataHSIdeCom[0]['total_PEGAWAI_RINCIAN'] == "1";
        _providerRincianCompany.isLamaUsahaShow = _dataHSIdeCom[0]['lama_USAHA_BERJALAN_RINCIAN'] == "1";
        _providerRincianCompany.isTotalLamaUsahaShow = _dataHSIdeCom[0]['total_LAMA_USAHA_BERJALAN'] == "1";

        //rincian company mandatory
        _providerRincianCompany.isCompanyTypeMandatory = _dataMandatoryIdeCom[0]['jenis_LEMBAGA_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isProfileMandatory = _dataMandatoryIdeCom[0]['profil_RINCIAN'] == "1";
        _providerRincianCompany.isCompanyNameMandatory = _dataMandatoryIdeCom[0]['nama_LEMBAGA_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isHaveNPWPMandatory = _dataMandatoryIdeCom[0]['punya_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isNoNPWPMandatory = _dataMandatoryIdeCom[0]['npwp_RINCIAN'] == "1";
        _providerRincianCompany.isNameNPWPMandatory = _dataMandatoryIdeCom[0]['nama_LENGKAP_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isTypeNPWPMandatory = _dataMandatoryIdeCom[0]['jenis_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isPKPSignMandatory = _dataMandatoryIdeCom[0]['tanda_PKP_RINCIAN'] == "1";
        _providerRincianCompany.isAddressNPWPMandatory = _dataMandatoryIdeCom[0]['alamat_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isEstablishDateMandatory = _dataMandatoryIdeCom[0]['tanggal_PENDIRIAN_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isSectorEconomicMandatory = _dataMandatoryIdeCom[0]['sektor_EKONOMI_RINCIAN'] == "1";
        _providerRincianCompany.isBusinessFieldMandatory = _dataMandatoryIdeCom[0]['lapangan_USAHA_RINCIAN'] == "1";
        _providerRincianCompany.isLocationStatusMandatory = _dataMandatoryIdeCom[0]['status_LOKASI_RINCIAN'] == "1";
        _providerRincianCompany.isBusinessLocationMandatory = _dataMandatoryIdeCom[0]['lokasi_USAHA_RINCIAN'] == "1";
        _providerRincianCompany.isTotalEmpMandatory = _dataMandatoryIdeCom[0]['total_PEGAWAI_RINCIAN'] == "1";
        _providerRincianCompany.isLamaUsahaMandatory = _dataMandatoryIdeCom[0]['lama_USAHA_BERJALAN_RINCIAN'] == "1";
        _providerRincianCompany.isTotalLamaUsahaMandatory = _dataMandatoryIdeCom[0]['total_LAMA_USAHA_BERJALAN'] == "1";

        //pendapatan hideshow & mandatory
        _setIncomeCompanyModel = SetIncomeCompanyModel(
          _dataHSIdeCom[0]['pendapatan_PERBULAN'] == "1",
          _dataHSIdeCom[0]['pendapatan_LAINNYA'] == "1",
          _dataHSIdeCom[0]['total_PENDAPATAN'] == "1",
          _dataHSIdeCom[0]['harga_POKOK_PENDAPATAN'] == "1",
          _dataHSIdeCom[0]['laba_KOTOR'] == "1",
          _dataHSIdeCom[0]['biaya_OPERASIONAL'] == "1",
          _dataHSIdeCom[0]['biaya_LAINNYA'] == "1",
          _dataHSIdeCom[0]['laba_BERSIH_SEBELUM_PAJAK'] == "1",
          _dataHSIdeCom[0]['pajak'] == "1",
          _dataHSIdeCom[0]['laba_BERSIH_SETELAH_PAJAK'] == "1",
          _dataHSIdeCom[0]['angsuran_LAINNYA'] == "1",
          _dataMandatoryIdeCom[0]['pendapatan_PERBULAN'] == "1",
          _dataMandatoryIdeCom[0]['pendapatan_LAINNYA'] == "1",
          _dataMandatoryIdeCom[0]['total_PENDAPATAN'] == "1",
          _dataMandatoryIdeCom[0]['harga_POKOK_PENDAPATAN'] == "1",
          _dataMandatoryIdeCom[0]['laba_KOTOR'] == "1",
          _dataMandatoryIdeCom[0]['biaya_OPERASIONAL'] == "1",
          _dataMandatoryIdeCom[0]['biaya_LAINNYA'] == "1",
          _dataMandatoryIdeCom[0]['laba_BERSIH_SEBELUM_PAJAK'] == "1",
          _dataMandatoryIdeCom[0]['pajak'] == "1",
          _dataMandatoryIdeCom[0]['laba_BERSIH_SETELAH_PAJAK'] == "1",
          _dataMandatoryIdeCom[0]['angsuran_LAINNYA'] == "1",
        );

        //penjamin hideshow & mandatory
        _setGuarantorIndividuCompany = SetGuarantorIndividu(
          _dataHSIdeCom[0]['status_HUBUNGAN_IND'] == "1",
          _dataHSIdeCom[0]['jenis_IDENTITAS_IND'] == "1",
          _dataHSIdeCom[0]['no_IDENTITAS_IND'] == "1",
          _dataHSIdeCom[0]['nama_LENGKAP_ID_IND'] == "1",
          _dataHSIdeCom[0]['nama_LENGKAP_IND'] == "1",
          _dataHSIdeCom[0]['tanggal_LAHIR_IND'] == "1",
          _dataHSIdeCom[0]['pob_ID_IND'] == "1",
          _dataHSIdeCom[0]['pob_ID_LOV_IND'] == "1",
          _dataHSIdeCom[0]['jenis_KELAMIN_IND'] == "1",
          _dataHSIdeCom[0]['handphone_IND'] == "1",
          _dataMandatoryIdeCom[0]['status_HUBUNGAN_IND'] == "1",
          _dataMandatoryIdeCom[0]['jenis_IDENTITAS_IND'] == "1",
          _dataMandatoryIdeCom[0]['no_IDENTITAS_IND'] == "1",
          _dataMandatoryIdeCom[0]['nama_LENGKAP_ID_IND'] == "1",
          _dataMandatoryIdeCom[0]['nama_LENGKAP_IND'] == "1",
          _dataMandatoryIdeCom[0]['tanggal_LAHIR_IND'] == "1",
          _dataMandatoryIdeCom[0]['pob_ID_IND'] == "1",
          _dataMandatoryIdeCom[0]['pob_ID_LOV_IND'] == "1",
          _dataMandatoryIdeCom[0]['jenis_KELAMIN_IND'] == "1",
          _dataMandatoryIdeCom[0]['handphone_IND'] == "1",
        );
        _setGuarantorLembagaCompany = SetGuarantorCompanyModel(
            _dataHSIdeCom[0]['jenis_LEMBAGA_KELEMBAGAAN'] == "1",
            _dataHSIdeCom[0]['nama_LEMBAGA_KELEMBAGAAN'] == "1",
            _dataHSIdeCom[0]['tanggal_PENDIRIAN_KELEMBAGAAN'] == "1",
            _dataHSIdeCom[0]['npwp_KELEMBAGAAN'] == "1",
            _dataMandatoryIdeCom[0]['jenis_LEMBAGA_KELEMBAGAAN'] == "1",
            _dataMandatoryIdeCom[0]['nama_LEMBAGA_KELEMBAGAAN'] == "1",
            _dataMandatoryIdeCom[0]['tanggal_PENDIRIAN_KELEMBAGAAN'] == "1",
            _dataMandatoryIdeCom[0]['npwp_KELEMBAGAAN'] == "1"
        );

        //manajemenPIC hideshow & mandatory
        _setManagementPICModel = SetManagementPICCompanyModel(
          _dataHSIdeCom[0]['jenis_IDENTITAS_MANAJEMEN_PIC'] == "1",
          _dataHSIdeCom[0]['no_IDENTITAS_MANAJEMEN_PIC'] == "1",
          _dataHSIdeCom[0]['nama_LENGKAP_ID_MANAJEMEN_PIC'] == "1",
          _dataHSIdeCom[0]['nama_LENGKAP_MANAJEMEN_PIC'] == "1",
          _dataHSIdeCom[0]['tanggal_LAHIR_MANAJEMEN_PIC'] == "1",
          _dataHSIdeCom[0]['pob_ID_MANAJEMEN_PIC'] == "1",
          _dataHSIdeCom[0]['pob_ID_LOV_PIC'] == "1",
          _dataHSIdeCom[0]['jabatan_MANAJEMEN_PIC'] == "1",
          _dataHSIdeCom[0]['email_MANAJEMEN_PIC'] == "1",
          _dataHSIdeCom[0]['handphone_MANAJEMEN_PIC'] == "1",
          _dataMandatoryIdeCom[0]['jenis_IDENTITAS_MANAJEMEN_PIC'] == "1",
          _dataMandatoryIdeCom[0]['no_IDENTITAS_MANAJEMEN_PIC'] == "1",
          _dataMandatoryIdeCom[0]['nama_LENGKAP_ID_MANAJEMEN_PIC'] == "1",
          _dataMandatoryIdeCom[0]['nama_LENGKAP_MANAJEMEN_PIC'] == "1",
          _dataMandatoryIdeCom[0]['tanggal_LAHIR_MANAJEMEN_PIC'] == "1",
          _dataMandatoryIdeCom[0]['pob_ID_MANAJEMEN_PIC'] == "1",
          _dataMandatoryIdeCom[0]['pob_ID_LOV_PIC'] == "1",
          _dataMandatoryIdeCom[0]['jabatan_MANAJEMEN_PIC'] == "1",
          _dataMandatoryIdeCom[0]['email_MANAJEMEN_PIC'] == "1",
          _dataMandatoryIdeCom[0]['handphone_MANAJEMEN_PIC'] == "1",
        );

        //pemegang saham individu-lembaga hideshow & mandatory
        _setPemegangSahamIndividuCompanyModel = SetPemegangSahamIndividuCompanyModel(
          _dataHSIdeCom[0]['hubungan_PEMEGANG_SAHAM_IND'] == "1",
          _dataHSIdeCom[0]['jenis_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataHSIdeCom[0]['no_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataHSIdeCom[0]['nama_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataHSIdeCom[0]['nama_PEMEGANG_SAHAM_IND'] == "1",
          _dataHSIdeCom[0]['tgl_LAHIR_PEMEGANG_SAHAM_IND'] == "1",
          _dataHSIdeCom[0]['pob_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataHSIdeCom[0]['pob_ID_LOV_PEMEGANG_SAHAM_IND'] == "1",
          _dataHSIdeCom[0]['jns_KELAMIN_PEMEGANG_SAHAM_IND'] == "1",
          _dataHSIdeCom[0]['share_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryIdeCom[0]['hubungan_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryIdeCom[0]['jenis_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryIdeCom[0]['no_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryIdeCom[0]['nama_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryIdeCom[0]['nama_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryIdeCom[0]['tgl_LAHIR_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryIdeCom[0]['pob_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryIdeCom[0]['pob_ID_LOV_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryIdeCom[0]['jns_KELAMIN_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryIdeCom[0]['share_PEMEGANG_SAHAM_IND'] == "1",
        );
        _setPemegangSahamLembagaCompanyModel = SetPemegangSahamLembagaCompanyModel(
          _dataHSIdeCom[0]['jenis_LEMBAGA_PEMEGANG_SAHAM'] == "1",
          _dataHSIdeCom[0]['nama_LEMBAGA_PEMEGANG_SAHAM'] == "1",
          _dataHSIdeCom[0]['tgl_PENDIRIAN_PEMEGANG_SAHAM'] == "1",
          _dataHSIdeCom[0]['npwp_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHSIdeCom[0]['share_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryIdeCom[0]['jenis_LEMBAGA_PEMEGANG_SAHAM'] == "1",
          _dataMandatoryIdeCom[0]['nama_LEMBAGA_PEMEGANG_SAHAM'] == "1",
          _dataMandatoryIdeCom[0]['tgl_PENDIRIAN_PEMEGANG_SAHAM'] == "1",
          _dataMandatoryIdeCom[0]['npwp_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryIdeCom[0]['share_PEMEGANG_SAHAM_LEMBAGA'] == "1",
        );

        //alamat
        //
        print("ALAMAT_DASH ${_dataHSIdeCom[0]['alamat'] == "1"}");
        _setAddressCompanyModel = SetAddressCompanyModel(
          _dataHSIdeCom[0]['jenis_ALAMAT'] == "1",
          _dataHSIdeCom[0]['alamat'] == "1",
          _dataHSIdeCom[0]['rt'] == "1",
          _dataHSIdeCom[0]['rw'] == "1",
          _dataHSIdeCom[0]['kelurahan'] == "1",
          _dataHSIdeCom[0]['kecamatan'] == "1",
          _dataHSIdeCom[0]['kabupaten_KOTA'] == "1",
          _dataHSIdeCom[0]['provinsi'] == "1",
          _dataHSIdeCom[0]['kode_POS'] == "1",
          _dataHSIdeCom[0]['telepon_AREA_1'] == "1",
          _dataHSIdeCom[0]['telepon_1'] == "1",
          _dataHSIdeCom[0]['telepon_AREA_2'] == "1",
          _dataHSIdeCom[0]['telepon_2'] == "1",
          _dataHSIdeCom[0]['fax_AREA'] == "1",
          _dataHSIdeCom[0]['fax'] == "1",
          _dataMandatoryIdeCom[0]['jenis_ALAMAT'] == "1",
          _dataMandatoryIdeCom[0]['alamat'] == "1",
          _dataMandatoryIdeCom[0]['rt'] == "1",
          _dataMandatoryIdeCom[0]['rw'] == "1",
          _dataMandatoryIdeCom[0]['kelurahan'] == "1",
          _dataMandatoryIdeCom[0]['kecamatan'] == "1",
          _dataMandatoryIdeCom[0]['kabupaten_KOTA'] == "1",
          _dataMandatoryIdeCom[0]['provinsi'] == "1",
          _dataMandatoryIdeCom[0]['kode_POS'] == "1",
          _dataMandatoryIdeCom[0]['telepon_AREA_1'] == "1",
          _dataMandatoryIdeCom[0]['telepon_1'] == "1",
          _dataMandatoryIdeCom[0]['telepon_AREA_2'] == "1",
          _dataMandatoryIdeCom[0]['telepon_2'] == "1",
          _dataMandatoryIdeCom[0]['fax_AREA'] == "1",
          _dataMandatoryIdeCom[0]['fax'] == "1",
        );
        _setAddressIndividuPenjaminCompany = SetAddressIndividu(
            _dataHSIdeCom[0]['jenis_ALAMAT_IND_ALAMAT'] == "1",
            _dataHSIdeCom[0]['alamat_IND_ALAMAT'] == "1",
            _dataHSIdeCom[0]['rt_IND_ALAMAT'] == "1",
            _dataHSIdeCom[0]['rw_IND_ALAMAT'] == "1",
            _dataHSIdeCom[0]['kelurahan_IND_ALAMAT'] == "1",
            _dataHSIdeCom[0]['kecamatan_IND_ALAMAT'] == "1",
            _dataHSIdeCom[0]['kabupaten_KOTA_IND_ALAMAT'] == "1",
            _dataHSIdeCom[0]['provinsi_IND_ALAMAT'] == "1",
            _dataHSIdeCom[0]['kode_POS_IND_ALAMAT'] == "1",
            _dataHSIdeCom[0]['telepon_AREA_IND_ALAMAT'] == "1",
            _dataHSIdeCom[0]['telepon_IND_ALAMAT'] == "1",
            _dataMandatoryIdeCom[0]['jenis_ALAMAT_IND_ALAMAT'] == "1",
            _dataMandatoryIdeCom[0]['alamat_IND_ALAMAT'] == "1",
            _dataMandatoryIdeCom[0]['rt_IND_ALAMAT'] == "1",
            _dataMandatoryIdeCom[0]['rw_IND_ALAMAT'] == "1",
            _dataMandatoryIdeCom[0]['kelurahan_IND_ALAMAT'] == "1",
            _dataMandatoryIdeCom[0]['kecamatan_IND_ALAMAT'] == "1",
            _dataMandatoryIdeCom[0]['kabupaten_KOTA_IND_ALAMAT'] == "1",
            _dataMandatoryIdeCom[0]['provinsi_IND_ALAMAT'] == "1",
            _dataMandatoryIdeCom[0]['kode_POS_IND_ALAMAT'] == "1",
            _dataMandatoryIdeCom[0]['telepon_AREA_IND_ALAMAT'] == "1",
            _dataMandatoryIdeCom[0]['telepon_IND_ALAMAT'] == "1"
        );
        _setAddressCompanyGuarantorModel = SetAddressCompanyModel(
          _dataHSIdeCom[0]['jenis_ALAMAT_KELEMBAGAAN'] == "1",
          _dataHSIdeCom[0]['alamat_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHSIdeCom[0]['rt_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHSIdeCom[0]['rw_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHSIdeCom[0]['kelurahan_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHSIdeCom[0]['kecamatan_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHSIdeCom[0]['kabupaten_KOTA_KELEMBAGAAN'] == "1",
          _dataHSIdeCom[0]['provinsi_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHSIdeCom[0]['kode_POS_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHSIdeCom[0]['telepon_AREA_1_KELEMBAGAAN'] == "1",
          _dataHSIdeCom[0]['telepon_1_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHSIdeCom[0]['telepon_AREA_2_KELEMBAGAAN'] == "1",
          _dataHSIdeCom[0]['telepon_2_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHSIdeCom[0]['fax_AREA_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHSIdeCom[0]['fax_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryIdeCom[0]['jenis_ALAMAT_KELEMBAGAAN'] == "1",
          _dataMandatoryIdeCom[0]['alamat_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryIdeCom[0]['rt_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryIdeCom[0]['rw_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryIdeCom[0]['kelurahan_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryIdeCom[0]['kecamatan_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryIdeCom[0]['kabupaten_KOTA_KELEMBAGAAN'] == "1",
          _dataMandatoryIdeCom[0]['provinsi_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryIdeCom[0]['kode_POS_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryIdeCom[0]['telepon_AREA_1_KELEMBAGAAN'] == "1",
          _dataMandatoryIdeCom[0]['telepon_1_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryIdeCom[0]['telepon_AREA_2_KELEMBAGAAN'] == "1",
          _dataMandatoryIdeCom[0]['telepon_2_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryIdeCom[0]['fax_AREA_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryIdeCom[0]['fax_KELEMBAGAAN_ALAMAT'] == "1",
        );
        _setAddressIndividuManajemenPIC = SetAddressIndividu(
            _dataHSIdeCom[0]['jenis_ALAMAT_MANAJEMEN_PIC'] == "1",
            _dataHSIdeCom[0]['alamat_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHSIdeCom[0]['rt_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHSIdeCom[0]['rw_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHSIdeCom[0]['kelurahan_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHSIdeCom[0]['kecamatan_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHSIdeCom[0]['kabupaten_KOTA_MANAJEMEN_PIC'] == "1",
            _dataHSIdeCom[0]['provinsi_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHSIdeCom[0]['kode_POS_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHSIdeCom[0]['telepon_AREA_MANAJEMEN_PIC'] == "1",
            _dataHSIdeCom[0]['telepon_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryIdeCom[0]['jenis_ALAMAT_MANAJEMEN_PIC'] == "1",
            _dataMandatoryIdeCom[0]['alamat_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryIdeCom[0]['rt_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryIdeCom[0]['rw_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryIdeCom[0]['kelurahan_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryIdeCom[0]['kecamatan_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryIdeCom[0]['kabupaten_KOTA_MANAJEMEN_PIC'] == "1",
            _dataMandatoryIdeCom[0]['provinsi_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryIdeCom[0]['kode_POS_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryIdeCom[0]['telepon_AREA_MANAJEMEN_PIC'] == "1",
            _dataMandatoryIdeCom[0]['telepon_MANAJEMEN_PIC_ALAMAT'] == "1"
        );
        _setAddressIndividuPemegangSaham = SetAddressIndividu(
            _dataHSIdeCom[0]['jns_ALAMAT_PEMEGANG_SAHAM_IND']== "1",
            _dataHSIdeCom[0]['alamat_PEMEGANG_SAHAM_IND']== "1",
            _dataHSIdeCom[0]['rt_PEMEGANG_SAHAM_IND_ALAMAT']== "1",
            _dataHSIdeCom[0]['rw_PEMEGANG_SAHAM_IND_ALAMAT']== "1",
            _dataHSIdeCom[0]['kelurahan_PEMEGANG_SAHAM_IND']== "1",
            _dataHSIdeCom[0]['kecamatan_PEMEGANG_SAHAM_IND']== "1",
            _dataHSIdeCom[0]['kabkot_PEMEGANG_SAHAM_IND']== "1",
            _dataHSIdeCom[0]['provinsi_PEMEGANG_SAHAM_IND']== "1",
            _dataHSIdeCom[0]['kode_POS_PEMEGANG_SAHAM_IND']== "1",
            _dataHSIdeCom[0]['phone_AREA_PEMEGANG_SAHAM_IND']== "1",
            _dataHSIdeCom[0]['phone_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryIdeCom[0]['jns_ALAMAT_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryIdeCom[0]['alamat_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryIdeCom[0]['rt_PEMEGANG_SAHAM_IND_ALAMAT']== "1",
            _dataMandatoryIdeCom[0]['rw_PEMEGANG_SAHAM_IND_ALAMAT']== "1",
            _dataMandatoryIdeCom[0]['kelurahan_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryIdeCom[0]['kecamatan_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryIdeCom[0]['kabkot_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryIdeCom[0]['provinsi_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryIdeCom[0]['kode_POS_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryIdeCom[0]['phone_AREA_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryIdeCom[0]['phone_PEMEGANG_SAHAM_IND'] == "1"
        );
        _setAddressSahamLembagaCompanyModel = SetAddressCompanyModel(
          _dataHSIdeCom[0]['jns_ALAMAT_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHSIdeCom[0]['addr_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHSIdeCom[0]['rt_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
          _dataHSIdeCom[0]['rw_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
          _dataHSIdeCom[0]['kelurahan_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHSIdeCom[0]['kecamatan_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHSIdeCom[0]['kabkota_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHSIdeCom[0]['prov_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHSIdeCom[0]['kode_POS_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHSIdeCom[0]['phonearea1_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHSIdeCom[0]['phone1_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHSIdeCom[0]['phonearea2_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHSIdeCom[0]['phone2_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHSIdeCom[0]['faxarea_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHSIdeCom[0]['fax_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
          _dataMandatoryIdeCom[0]['jns_ALAMAT_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryIdeCom[0]['addr_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryIdeCom[0]['rt_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
          _dataMandatoryIdeCom[0]['rw_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
          _dataMandatoryIdeCom[0]['kelurahan_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryIdeCom[0]['kecamatan_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryIdeCom[0]['kabkota_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryIdeCom[0]['prov_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryIdeCom[0]['kode_POS_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryIdeCom[0]['phonearea1_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryIdeCom[0]['phone1_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryIdeCom[0]['phonearea2_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryIdeCom[0]['phone2_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryIdeCom[0]['faxarea_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryIdeCom[0]['fax_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
        );
      }
      else if (_lastKnownState == "SRE") {
        //rincian company hideshow
        _providerRincianCompany.isCompanyTypeShow = _dataHideShowFormMRegulerSurveyCompany[0]['jenis_LEMBAGA_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isProfileShow = _dataHideShowFormMRegulerSurveyCompany[0]['profil_RINCIAN'] == "1";
        _providerRincianCompany.isCompanyNameShow = _dataHideShowFormMRegulerSurveyCompany[0]['nama_LEMBAGA_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isHaveNPWPShow = _dataHideShowFormMRegulerSurveyCompany[0]['punya_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isNoNPWPShow = _dataHideShowFormMRegulerSurveyCompany[0]['npwp_RINCIAN'] == "1";
        _providerRincianCompany.isNameNPWPShow = _dataHideShowFormMRegulerSurveyCompany[0]['nama_LENGKAP_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isTypeNPWPShow = _dataHideShowFormMRegulerSurveyCompany[0]['jenis_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isPKPSignShow = _dataHideShowFormMRegulerSurveyCompany[0]['tanda_PKP_RINCIAN'] == "1";
        _providerRincianCompany.isAddressNPWPShow = _dataHideShowFormMRegulerSurveyCompany[0]['alamat_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isEstablishDateShow = _dataHideShowFormMRegulerSurveyCompany[0]['tanggal_PENDIRIAN_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isSectorEconomicShow = _dataHideShowFormMRegulerSurveyCompany[0]['sektor_EKONOMI_RINCIAN'] == "1";
        _providerRincianCompany.isBusinessFieldShow = _dataHideShowFormMRegulerSurveyCompany[0]['lapangan_USAHA_RINCIAN'] == "1";
        _providerRincianCompany.isLocationStatusShow = _dataHideShowFormMRegulerSurveyCompany[0]['status_LOKASI_RINCIAN'] == "1";
        _providerRincianCompany.isBusinessLocationShow = _dataHideShowFormMRegulerSurveyCompany[0]['lokasi_USAHA_RINCIAN'] == "1";
        _providerRincianCompany.isTotalEmpShow = _dataHideShowFormMRegulerSurveyCompany[0]['total_PEGAWAI_RINCIAN'] == "1";
        _providerRincianCompany.isLamaUsahaShow = _dataHideShowFormMRegulerSurveyCompany[0]['lama_USAHA_BERJALAN_RINCIAN'] == "1";
        _providerRincianCompany.isTotalLamaUsahaShow = _dataHideShowFormMRegulerSurveyCompany[0]['total_LAMA_USAHA_BERJALAN'] == "1";

        //rincian company mandatory
        _providerRincianCompany.isCompanyTypeMandatory = _dataMandatoryFormMRegulerSurveyCompany[0]['jenis_LEMBAGA_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isProfileMandatory = _dataMandatoryFormMRegulerSurveyCompany[0]['profil_RINCIAN'] == "1";
        _providerRincianCompany.isCompanyNameMandatory = _dataMandatoryFormMRegulerSurveyCompany[0]['nama_LEMBAGA_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isHaveNPWPMandatory = _dataMandatoryFormMRegulerSurveyCompany[0]['punya_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isNoNPWPMandatory = _dataMandatoryFormMRegulerSurveyCompany[0]['npwp_RINCIAN'] == "1";
        _providerRincianCompany.isNameNPWPMandatory = _dataMandatoryFormMRegulerSurveyCompany[0]['nama_LENGKAP_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isTypeNPWPMandatory = _dataMandatoryFormMRegulerSurveyCompany[0]['jenis_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isPKPSignMandatory = _dataMandatoryFormMRegulerSurveyCompany[0]['tanda_PKP_RINCIAN'] == "1";
        _providerRincianCompany.isAddressNPWPMandatory = _dataMandatoryFormMRegulerSurveyCompany[0]['alamat_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isEstablishDateMandatory = _dataMandatoryFormMRegulerSurveyCompany[0]['tanggal_PENDIRIAN_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isSectorEconomicMandatory = _dataMandatoryFormMRegulerSurveyCompany[0]['sektor_EKONOMI_RINCIAN'] == "1";
        _providerRincianCompany.isBusinessFieldMandatory = _dataMandatoryFormMRegulerSurveyCompany[0]['lapangan_USAHA_RINCIAN'] == "1";
        _providerRincianCompany.isLocationStatusMandatory = _dataMandatoryFormMRegulerSurveyCompany[0]['status_LOKASI_RINCIAN'] == "1";
        _providerRincianCompany.isBusinessLocationMandatory = _dataMandatoryFormMRegulerSurveyCompany[0]['lokasi_USAHA_RINCIAN'] == "1";
        _providerRincianCompany.isTotalEmpMandatory = _dataMandatoryFormMRegulerSurveyCompany[0]['total_PEGAWAI_RINCIAN'] == "1";
        _providerRincianCompany.isLamaUsahaMandatory = _dataMandatoryFormMRegulerSurveyCompany[0]['lama_USAHA_BERJALAN_RINCIAN'] == "1";
        _providerRincianCompany.isTotalLamaUsahaMandatory = _dataMandatoryFormMRegulerSurveyCompany[0]['total_LAMA_USAHA_BERJALAN'] == "1";

        //pendapatan hideshow & mandatory
        _setIncomeCompanyModel = SetIncomeCompanyModel(
          _dataHideShowFormMRegulerSurveyCompany[0]['pendapatan_PERBULAN'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['pendapatan_LAINNYA'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['total_PENDAPATAN'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['harga_POKOK_PENDAPATAN'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['laba_KOTOR'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['biaya_OPERASIONAL'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['biaya_LAINNYA'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['laba_BERSIH_SEBELUM_PAJAK'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['pajak'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['laba_BERSIH_SETELAH_PAJAK'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['angsuran_LAINNYA'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['pendapatan_PERBULAN'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['pendapatan_LAINNYA'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['total_PENDAPATAN'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['harga_POKOK_PENDAPATAN'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['laba_KOTOR'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['biaya_OPERASIONAL'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['biaya_LAINNYA'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['laba_BERSIH_SEBELUM_PAJAK'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['pajak'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['laba_BERSIH_SETELAH_PAJAK'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['angsuran_LAINNYA'] == "1",
        );

        //penjamin hideshow & mandatory
        _setGuarantorIndividuCompany = SetGuarantorIndividu(
          _dataHideShowFormMRegulerSurveyCompany[0]['status_HUBUNGAN_IND'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['jenis_IDENTITAS_IND'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['no_IDENTITAS_IND'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['nama_LENGKAP_ID_IND'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['nama_LENGKAP_IND'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['tanggal_LAHIR_IND'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['pob_ID_IND'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['pob_ID_LOV_IND'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['jenis_KELAMIN_IND'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['handphone_IND'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['status_HUBUNGAN_IND'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['jenis_IDENTITAS_IND'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['no_IDENTITAS_IND'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['nama_LENGKAP_ID_IND'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['nama_LENGKAP_IND'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['tanggal_LAHIR_IND'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['pob_ID_IND'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['pob_ID_LOV_IND'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['jenis_KELAMIN_IND'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['handphone_IND'] == "1",
        );
        _setGuarantorLembagaCompany = SetGuarantorCompanyModel(
            _dataHideShowFormMRegulerSurveyCompany[0]['jenis_LEMBAGA_KELEMBAGAAN'] == "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['nama_LEMBAGA_KELEMBAGAAN'] == "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['tanggal_PENDIRIAN_KELEMBAGAAN'] == "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['npwp_KELEMBAGAAN'] == "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['jenis_LEMBAGA_KELEMBAGAAN'] == "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['nama_LEMBAGA_KELEMBAGAAN'] == "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['tanggal_PENDIRIAN_KELEMBAGAAN'] == "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['npwp_KELEMBAGAAN'] == "1"
        );

        //manajemenPIC hideshow & mandatory
        _setManagementPICModel = SetManagementPICCompanyModel(
          _dataHideShowFormMRegulerSurveyCompany[0]['jenis_IDENTITAS_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['no_IDENTITAS_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['nama_LENGKAP_ID_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['nama_LENGKAP_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['tanggal_LAHIR_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['pob_ID_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['pob_ID_LOV_PIC'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['jabatan_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['email_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['handphone_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['jenis_IDENTITAS_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['no_IDENTITAS_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['nama_LENGKAP_ID_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['nama_LENGKAP_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['tanggal_LAHIR_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['pob_ID_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['pob_ID_LOV_PIC'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['jabatan_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['email_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['handphone_MANAJEMEN_PIC'] == "1",
        );

        //pemegang saham individu-lembaga hideshow & mandatory
        _setPemegangSahamIndividuCompanyModel = SetPemegangSahamIndividuCompanyModel(
          _dataHideShowFormMRegulerSurveyCompany[0]['hubungan_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['jenis_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['no_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['nama_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['nama_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['tgl_LAHIR_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['pob_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['pob_ID_LOV_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['jns_KELAMIN_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['share_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['hubungan_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['jenis_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['no_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['nama_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['nama_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['tgl_LAHIR_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['pob_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['pob_ID_LOV_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['jns_KELAMIN_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['share_PEMEGANG_SAHAM_IND'] == "1",
        );
        _setPemegangSahamLembagaCompanyModel = SetPemegangSahamLembagaCompanyModel(
          _dataHideShowFormMRegulerSurveyCompany[0]['jenis_LEMBAGA_PEMEGANG_SAHAM'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['nama_LEMBAGA_PEMEGANG_SAHAM'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['tgl_PENDIRIAN_PEMEGANG_SAHAM'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['npwp_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['share_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['jenis_LEMBAGA_PEMEGANG_SAHAM'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['nama_LEMBAGA_PEMEGANG_SAHAM'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['tgl_PENDIRIAN_PEMEGANG_SAHAM'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['npwp_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['share_PEMEGANG_SAHAM_LEMBAGA'] == "1",
        );

        //alamat
        _setAddressCompanyModel = SetAddressCompanyModel(
          _dataHideShowFormMRegulerSurveyCompany[0]['jenis_ALAMAT'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['alamat'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['rt'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['rw'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['kelurahan'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['kecamatan'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['kabupaten_KOTA'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['provinsi'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['kode_POS'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['telepon_AREA_1'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['telepon_1'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['telepon_AREA_2'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['telepon_2'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['fax_AREA'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['fax'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['jenis_ALAMAT'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['alamat'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['rt'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['rw'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['kelurahan'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['kecamatan'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['kabupaten_KOTA'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['provinsi'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['kode_POS'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['telepon_AREA_1'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['telepon_1'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['telepon_AREA_2'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['telepon_2'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['fax_AREA'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['fax'] == "1",
        );
        _setAddressIndividuPenjaminCompany = SetAddressIndividu(
            _dataHideShowFormMRegulerSurveyCompany[0]['jenis_ALAMAT_IND_ALAMAT'] == "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['alamat_IND_ALAMAT'] == "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['rt_IND_ALAMAT'] == "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['rw_IND_ALAMAT'] == "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['kelurahan_IND_ALAMAT'] == "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['kecamatan_IND_ALAMAT'] == "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['kabupaten_KOTA_IND_ALAMAT'] == "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['provinsi_IND_ALAMAT'] == "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['kode_POS_IND_ALAMAT'] == "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['telepon_AREA_IND_ALAMAT'] == "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['telepon_IND_ALAMAT'] == "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['jenis_ALAMAT_IND_ALAMAT'] == "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['alamat_IND_ALAMAT'] == "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['rt_IND_ALAMAT'] == "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['rw_IND_ALAMAT'] == "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['kelurahan_IND_ALAMAT'] == "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['kecamatan_IND_ALAMAT'] == "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['kabupaten_KOTA_IND_ALAMAT'] == "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['provinsi_IND_ALAMAT'] == "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['kode_POS_IND_ALAMAT'] == "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['telepon_AREA_IND_ALAMAT'] == "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['telepon_IND_ALAMAT'] == "1"
        );
        _setAddressCompanyGuarantorModel = SetAddressCompanyModel(
          _dataHideShowFormMRegulerSurveyCompany[0]['jenis_ALAMAT_KELEMBAGAAN'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['alamat_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['rt_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['rw_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['kelurahan_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['kecamatan_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['kabupaten_KOTA_KELEMBAGAAN'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['provinsi_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['kode_POS_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['telepon_AREA_1_KELEMBAGAAN'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['telepon_1_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['telepon_AREA_2_KELEMBAGAAN'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['telepon_2_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['fax_AREA_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['fax_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['jenis_ALAMAT_KELEMBAGAAN'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['alamat_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['rt_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['rw_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['kelurahan_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['kecamatan_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['kabupaten_KOTA_KELEMBAGAAN'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['provinsi_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['kode_POS_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['telepon_AREA_1_KELEMBAGAAN'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['telepon_1_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['telepon_AREA_2_KELEMBAGAAN'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['telepon_2_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['fax_AREA_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['fax_KELEMBAGAAN_ALAMAT'] == "1",
        );
        _setAddressIndividuManajemenPIC = SetAddressIndividu(
            _dataHideShowFormMRegulerSurveyCompany[0]['jenis_ALAMAT_MANAJEMEN_PIC'] == "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['alamat_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['rt_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['rw_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['kelurahan_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['kecamatan_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['kabupaten_KOTA_MANAJEMEN_PIC'] == "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['provinsi_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['kode_POS_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['telepon_AREA_MANAJEMEN_PIC'] == "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['telepon_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['jenis_ALAMAT_MANAJEMEN_PIC'] == "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['alamat_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['rt_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['rw_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['kelurahan_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['kecamatan_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['kabupaten_KOTA_MANAJEMEN_PIC'] == "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['provinsi_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['kode_POS_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['telepon_AREA_MANAJEMEN_PIC'] == "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['telepon_MANAJEMEN_PIC_ALAMAT'] == "1"
        );
        _setAddressIndividuPemegangSaham = SetAddressIndividu(
            _dataHideShowFormMRegulerSurveyCompany[0]['jns_ALAMAT_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['alamat_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['rt_PEMEGANG_SAHAM_IND_ALAMAT']== "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['rw_PEMEGANG_SAHAM_IND_ALAMAT']== "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['kelurahan_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['kecamatan_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['kabkot_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['provinsi_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['kode_POS_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['phone_AREA_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMRegulerSurveyCompany[0]['phone_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['jns_ALAMAT_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['alamat_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['rt_PEMEGANG_SAHAM_IND_ALAMAT']== "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['rw_PEMEGANG_SAHAM_IND_ALAMAT']== "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['kelurahan_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['kecamatan_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['kabkot_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['provinsi_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['kode_POS_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['phone_AREA_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMRegulerSurveyCompany[0]['phone_PEMEGANG_SAHAM_IND'] == "1"
        );
        _setAddressSahamLembagaCompanyModel = SetAddressCompanyModel(
          _dataHideShowFormMRegulerSurveyCompany[0]['jns_ALAMAT_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['addr_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['rt_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['rw_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['kelurahan_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['kecamatan_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['kabkota_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['prov_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['kode_POS_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['phonearea1_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['phone1_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['phonearea2_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['phone2_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['faxarea_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHideShowFormMRegulerSurveyCompany[0]['fax_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['jns_ALAMAT_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['addr_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['rt_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['rw_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['kelurahan_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['kecamatan_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['kabkota_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['prov_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['kode_POS_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['phonearea1_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['phone1_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['phonearea2_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['phone2_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['faxarea_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryFormMRegulerSurveyCompany[0]['fax_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
        );
      }
      else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        //rincian company hideshow
        _providerRincianCompany.isCompanyTypeShow = _dataHideShowFormMPACCompany[0]['jenis_LEMBAGA_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isProfileShow = _dataHideShowFormMPACCompany[0]['profil_RINCIAN'] == "1";
        _providerRincianCompany.isCompanyNameShow = _dataHideShowFormMPACCompany[0]['nama_LEMBAGA_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isHaveNPWPShow = _dataHideShowFormMPACCompany[0]['punya_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isNoNPWPShow = _dataHideShowFormMPACCompany[0]['npwp_RINCIAN'] == "1";
        _providerRincianCompany.isNameNPWPShow = _dataHideShowFormMPACCompany[0]['nama_LENGKAP_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isTypeNPWPShow = _dataHideShowFormMPACCompany[0]['jenis_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isPKPSignShow = _dataHideShowFormMPACCompany[0]['tanda_PKP_RINCIAN'] == "1";
        _providerRincianCompany.isAddressNPWPShow = _dataHideShowFormMPACCompany[0]['alamat_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isEstablishDateShow = _dataHideShowFormMPACCompany[0]['tanggal_PENDIRIAN_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isSectorEconomicShow = _dataHideShowFormMPACCompany[0]['sektor_EKONOMI_RINCIAN'] == "1";
        _providerRincianCompany.isBusinessFieldShow = _dataHideShowFormMPACCompany[0]['lapangan_USAHA_RINCIAN'] == "1";
        _providerRincianCompany.isLocationStatusShow = _dataHideShowFormMPACCompany[0]['status_LOKASI_RINCIAN'] == "1";
        _providerRincianCompany.isBusinessLocationShow = _dataHideShowFormMPACCompany[0]['lokasi_USAHA_RINCIAN'] == "1";
        _providerRincianCompany.isTotalEmpShow = _dataHideShowFormMPACCompany[0]['total_PEGAWAI_RINCIAN'] == "1";
        _providerRincianCompany.isLamaUsahaShow = _dataHideShowFormMPACCompany[0]['lama_USAHA_BERJALAN_RINCIAN'] == "1";
        _providerRincianCompany.isTotalLamaUsahaShow = _dataHideShowFormMPACCompany[0]['total_LAMA_USAHA_BERJALAN'] == "1";

        //rincian company mandatory
        _providerRincianCompany.isCompanyTypeMandatory = _dataMandatoryFormMPACCompany[0]['jenis_LEMBAGA_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isProfileMandatory = _dataMandatoryFormMPACCompany[0]['profil_RINCIAN'] == "1";
        _providerRincianCompany.isCompanyNameMandatory = _dataMandatoryFormMPACCompany[0]['nama_LEMBAGA_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isHaveNPWPMandatory = _dataMandatoryFormMPACCompany[0]['punya_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isNoNPWPMandatory = _dataMandatoryFormMPACCompany[0]['npwp_RINCIAN'] == "1";
        _providerRincianCompany.isNameNPWPMandatory = _dataMandatoryFormMPACCompany[0]['nama_LENGKAP_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isTypeNPWPMandatory = _dataMandatoryFormMPACCompany[0]['jenis_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isPKPSignMandatory = _dataMandatoryFormMPACCompany[0]['tanda_PKP_RINCIAN'] == "1";
        _providerRincianCompany.isAddressNPWPMandatory = _dataMandatoryFormMPACCompany[0]['alamat_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isEstablishDateMandatory = _dataMandatoryFormMPACCompany[0]['tanggal_PENDIRIAN_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isSectorEconomicMandatory = _dataMandatoryFormMPACCompany[0]['sektor_EKONOMI_RINCIAN'] == "1";
        _providerRincianCompany.isBusinessFieldMandatory = _dataMandatoryFormMPACCompany[0]['lapangan_USAHA_RINCIAN'] == "1";
        _providerRincianCompany.isLocationStatusMandatory = _dataMandatoryFormMPACCompany[0]['status_LOKASI_RINCIAN'] == "1";
        _providerRincianCompany.isBusinessLocationMandatory = _dataMandatoryFormMPACCompany[0]['lokasi_USAHA_RINCIAN'] == "1";
        _providerRincianCompany.isTotalEmpMandatory = _dataMandatoryFormMPACCompany[0]['total_PEGAWAI_RINCIAN'] == "1";
        _providerRincianCompany.isLamaUsahaMandatory = _dataMandatoryFormMPACCompany[0]['lama_USAHA_BERJALAN_RINCIAN'] == "1";
        _providerRincianCompany.isTotalLamaUsahaMandatory = _dataMandatoryFormMPACCompany[0]['total_LAMA_USAHA_BERJALAN'] == "1";

        //pendapatan hideshow & mandatory
        _setIncomeCompanyModel = SetIncomeCompanyModel(
          _dataHideShowFormMPACCompany[0]['pendapatan_PERBULAN'] == "1",
          _dataHideShowFormMPACCompany[0]['pendapatan_LAINNYA'] == "1",
          _dataHideShowFormMPACCompany[0]['total_PENDAPATAN'] == "1",
          _dataHideShowFormMPACCompany[0]['harga_POKOK_PENDAPATAN'] == "1",
          _dataHideShowFormMPACCompany[0]['laba_KOTOR'] == "1",
          _dataHideShowFormMPACCompany[0]['biaya_OPERASIONAL'] == "1",
          _dataHideShowFormMPACCompany[0]['biaya_LAINNYA'] == "1",
          _dataHideShowFormMPACCompany[0]['laba_BERSIH_SEBELUM_PAJAK'] == "1",
          _dataHideShowFormMPACCompany[0]['pajak'] == "1",
          _dataHideShowFormMPACCompany[0]['laba_BERSIH_SETELAH_PAJAK'] == "1",
          _dataHideShowFormMPACCompany[0]['angsuran_LAINNYA'] == "1",
          _dataMandatoryFormMPACCompany[0]['pendapatan_PERBULAN'] == "1",
          _dataMandatoryFormMPACCompany[0]['pendapatan_LAINNYA'] == "1",
          _dataMandatoryFormMPACCompany[0]['total_PENDAPATAN'] == "1",
          _dataMandatoryFormMPACCompany[0]['harga_POKOK_PENDAPATAN'] == "1",
          _dataMandatoryFormMPACCompany[0]['laba_KOTOR'] == "1",
          _dataMandatoryFormMPACCompany[0]['biaya_OPERASIONAL'] == "1",
          _dataMandatoryFormMPACCompany[0]['biaya_LAINNYA'] == "1",
          _dataMandatoryFormMPACCompany[0]['laba_BERSIH_SEBELUM_PAJAK'] == "1",
          _dataMandatoryFormMPACCompany[0]['pajak'] == "1",
          _dataMandatoryFormMPACCompany[0]['laba_BERSIH_SETELAH_PAJAK'] == "1",
          _dataMandatoryFormMPACCompany[0]['angsuran_LAINNYA'] == "1",
        );

        //penjamin hideshow & mandatory
        _setGuarantorIndividuCompany = SetGuarantorIndividu(
          _dataHideShowFormMPACCompany[0]['status_HUBUNGAN_IND'] == "1",
          _dataHideShowFormMPACCompany[0]['jenis_IDENTITAS_IND'] == "1",
          _dataHideShowFormMPACCompany[0]['no_IDENTITAS_IND'] == "1",
          _dataHideShowFormMPACCompany[0]['nama_LENGKAP_ID_IND'] == "1",
          _dataHideShowFormMPACCompany[0]['nama_LENGKAP_IND'] == "1",
          _dataHideShowFormMPACCompany[0]['tanggal_LAHIR_IND'] == "1",
          _dataHideShowFormMPACCompany[0]['pob_ID_IND'] == "1",
          _dataHideShowFormMPACCompany[0]['pob_ID_LOV_IND'] == "1",
          _dataHideShowFormMPACCompany[0]['jenis_KELAMIN_IND'] == "1",
          _dataHideShowFormMPACCompany[0]['handphone_IND'] == "1",
          _dataMandatoryFormMPACCompany[0]['status_HUBUNGAN_IND'] == "1",
          _dataMandatoryFormMPACCompany[0]['jenis_IDENTITAS_IND'] == "1",
          _dataMandatoryFormMPACCompany[0]['no_IDENTITAS_IND'] == "1",
          _dataMandatoryFormMPACCompany[0]['nama_LENGKAP_ID_IND'] == "1",
          _dataMandatoryFormMPACCompany[0]['nama_LENGKAP_IND'] == "1",
          _dataMandatoryFormMPACCompany[0]['tanggal_LAHIR_IND'] == "1",
          _dataMandatoryFormMPACCompany[0]['pob_ID_IND'] == "1",
          _dataMandatoryFormMPACCompany[0]['pob_ID_LOV_IND'] == "1",
          _dataMandatoryFormMPACCompany[0]['jenis_KELAMIN_IND'] == "1",
          _dataMandatoryFormMPACCompany[0]['handphone_IND'] == "1",
        );
        _setGuarantorLembagaCompany = SetGuarantorCompanyModel(
            _dataHideShowFormMPACCompany[0]['jenis_LEMBAGA_KELEMBAGAAN'] == "1",
            _dataHideShowFormMPACCompany[0]['nama_LEMBAGA_KELEMBAGAAN'] == "1",
            _dataHideShowFormMPACCompany[0]['tanggal_PENDIRIAN_KELEMBAGAAN'] == "1",
            _dataHideShowFormMPACCompany[0]['npwp_KELEMBAGAAN'] == "1",
            _dataMandatoryFormMPACCompany[0]['jenis_LEMBAGA_KELEMBAGAAN'] == "1",
            _dataMandatoryFormMPACCompany[0]['nama_LEMBAGA_KELEMBAGAAN'] == "1",
            _dataMandatoryFormMPACCompany[0]['tanggal_PENDIRIAN_KELEMBAGAAN'] == "1",
            _dataMandatoryFormMPACCompany[0]['npwp_KELEMBAGAAN'] == "1"
        );

        //manajemenPIC hideshow & mandatory
        _setManagementPICModel = SetManagementPICCompanyModel(
          _dataHideShowFormMPACCompany[0]['jenis_IDENTITAS_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMPACCompany[0]['no_IDENTITAS_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMPACCompany[0]['nama_LENGKAP_ID_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMPACCompany[0]['nama_LENGKAP_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMPACCompany[0]['tanggal_LAHIR_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMPACCompany[0]['pob_ID_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMPACCompany[0]['pob_ID_LOV_PIC'] == "1",
          _dataHideShowFormMPACCompany[0]['jabatan_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMPACCompany[0]['email_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMPACCompany[0]['handphone_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMPACCompany[0]['jenis_IDENTITAS_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMPACCompany[0]['no_IDENTITAS_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMPACCompany[0]['nama_LENGKAP_ID_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMPACCompany[0]['nama_LENGKAP_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMPACCompany[0]['tanggal_LAHIR_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMPACCompany[0]['pob_ID_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMPACCompany[0]['pob_ID_LOV_PIC'] == "1",
          _dataMandatoryFormMPACCompany[0]['jabatan_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMPACCompany[0]['email_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMPACCompany[0]['handphone_MANAJEMEN_PIC'] == "1",
        );

        //pemegang saham individu-lembaga hideshow & mandatory
        _setPemegangSahamIndividuCompanyModel = SetPemegangSahamIndividuCompanyModel(
          _dataHideShowFormMPACCompany[0]['hubungan_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMPACCompany[0]['jenis_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMPACCompany[0]['no_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMPACCompany[0]['nama_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMPACCompany[0]['nama_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMPACCompany[0]['tgl_LAHIR_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMPACCompany[0]['pob_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMPACCompany[0]['pob_ID_LOV_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMPACCompany[0]['jns_KELAMIN_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMPACCompany[0]['share_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMPACCompany[0]['hubungan_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMPACCompany[0]['jenis_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMPACCompany[0]['no_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMPACCompany[0]['nama_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMPACCompany[0]['nama_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMPACCompany[0]['tgl_LAHIR_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMPACCompany[0]['pob_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMPACCompany[0]['pob_ID_LOV_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMPACCompany[0]['jns_KELAMIN_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMPACCompany[0]['share_PEMEGANG_SAHAM_IND'] == "1",
        );
        _setPemegangSahamLembagaCompanyModel = SetPemegangSahamLembagaCompanyModel(
          _dataHideShowFormMPACCompany[0]['jenis_LEMBAGA_PEMEGANG_SAHAM'] == "1",
          _dataHideShowFormMPACCompany[0]['nama_LEMBAGA_PEMEGANG_SAHAM'] == "1",
          _dataHideShowFormMPACCompany[0]['tgl_PENDIRIAN_PEMEGANG_SAHAM'] == "1",
          _dataHideShowFormMPACCompany[0]['npwp_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHideShowFormMPACCompany[0]['share_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryFormMPACCompany[0]['jenis_LEMBAGA_PEMEGANG_SAHAM'] == "1",
          _dataMandatoryFormMPACCompany[0]['nama_LEMBAGA_PEMEGANG_SAHAM'] == "1",
          _dataMandatoryFormMPACCompany[0]['tgl_PENDIRIAN_PEMEGANG_SAHAM'] == "1",
          _dataMandatoryFormMPACCompany[0]['npwp_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryFormMPACCompany[0]['share_PEMEGANG_SAHAM_LEMBAGA'] == "1",
        );

        //alamat
        _setAddressCompanyModel = SetAddressCompanyModel(
          _dataHideShowFormMPACCompany[0]['jenis_ALAMAT'] == "1",
          _dataHideShowFormMPACCompany[0]['alamat'] == "1",
          _dataHideShowFormMPACCompany[0]['rt'] == "1",
          _dataHideShowFormMPACCompany[0]['rw'] == "1",
          _dataHideShowFormMPACCompany[0]['kelurahan'] == "1",
          _dataHideShowFormMPACCompany[0]['kecamatan'] == "1",
          _dataHideShowFormMPACCompany[0]['kabupaten_KOTA'] == "1",
          _dataHideShowFormMPACCompany[0]['provinsi'] == "1",
          _dataHideShowFormMPACCompany[0]['kode_POS'] == "1",
          _dataHideShowFormMPACCompany[0]['telepon_AREA_1'] == "1",
          _dataHideShowFormMPACCompany[0]['telepon_1'] == "1",
          _dataHideShowFormMPACCompany[0]['telepon_AREA_2'] == "1",
          _dataHideShowFormMPACCompany[0]['telepon_2'] == "1",
          _dataHideShowFormMPACCompany[0]['fax_AREA'] == "1",
          _dataHideShowFormMPACCompany[0]['fax'] == "1",
          _dataMandatoryFormMPACCompany[0]['jenis_ALAMAT'] == "1",
          _dataMandatoryFormMPACCompany[0]['alamat'] == "1",
          _dataMandatoryFormMPACCompany[0]['rt'] == "1",
          _dataMandatoryFormMPACCompany[0]['rw'] == "1",
          _dataMandatoryFormMPACCompany[0]['kelurahan'] == "1",
          _dataMandatoryFormMPACCompany[0]['kecamatan'] == "1",
          _dataMandatoryFormMPACCompany[0]['kabupaten_KOTA'] == "1",
          _dataMandatoryFormMPACCompany[0]['provinsi'] == "1",
          _dataMandatoryFormMPACCompany[0]['kode_POS'] == "1",
          _dataMandatoryFormMPACCompany[0]['telepon_AREA_1'] == "1",
          _dataMandatoryFormMPACCompany[0]['telepon_1'] == "1",
          _dataMandatoryFormMPACCompany[0]['telepon_AREA_2'] == "1",
          _dataMandatoryFormMPACCompany[0]['telepon_2'] == "1",
          _dataMandatoryFormMPACCompany[0]['fax_AREA'] == "1",
          _dataMandatoryFormMPACCompany[0]['fax'] == "1",
        );
        _setAddressIndividuPenjaminCompany = SetAddressIndividu(
            _dataHideShowFormMPACCompany[0]['jenis_ALAMAT_IND_ALAMAT'] == "1",
            _dataHideShowFormMPACCompany[0]['alamat_IND_ALAMAT'] == "1",
            _dataHideShowFormMPACCompany[0]['rt_IND_ALAMAT'] == "1",
            _dataHideShowFormMPACCompany[0]['rw_IND_ALAMAT'] == "1",
            _dataHideShowFormMPACCompany[0]['kelurahan_IND_ALAMAT'] == "1",
            _dataHideShowFormMPACCompany[0]['kecamatan_IND_ALAMAT'] == "1",
            _dataHideShowFormMPACCompany[0]['kabupaten_KOTA_IND_ALAMAT'] == "1",
            _dataHideShowFormMPACCompany[0]['provinsi_IND_ALAMAT'] == "1",
            _dataHideShowFormMPACCompany[0]['kode_POS_IND_ALAMAT'] == "1",
            _dataHideShowFormMPACCompany[0]['telepon_AREA_IND_ALAMAT'] == "1",
            _dataHideShowFormMPACCompany[0]['telepon_IND_ALAMAT'] == "1",
            _dataMandatoryFormMPACCompany[0]['jenis_ALAMAT_IND_ALAMAT'] == "1",
            _dataMandatoryFormMPACCompany[0]['alamat_IND_ALAMAT'] == "1",
            _dataMandatoryFormMPACCompany[0]['rt_IND_ALAMAT'] == "1",
            _dataMandatoryFormMPACCompany[0]['rw_IND_ALAMAT'] == "1",
            _dataMandatoryFormMPACCompany[0]['kelurahan_IND_ALAMAT'] == "1",
            _dataMandatoryFormMPACCompany[0]['kecamatan_IND_ALAMAT'] == "1",
            _dataMandatoryFormMPACCompany[0]['kabupaten_KOTA_IND_ALAMAT'] == "1",
            _dataMandatoryFormMPACCompany[0]['provinsi_IND_ALAMAT'] == "1",
            _dataMandatoryFormMPACCompany[0]['kode_POS_IND_ALAMAT'] == "1",
            _dataMandatoryFormMPACCompany[0]['telepon_AREA_IND_ALAMAT'] == "1",
            _dataMandatoryFormMPACCompany[0]['telepon_IND_ALAMAT'] == "1"
        );
        _setAddressCompanyGuarantorModel = SetAddressCompanyModel(
          _dataHideShowFormMPACCompany[0]['jenis_ALAMAT_KELEMBAGAAN'] == "1",
          _dataHideShowFormMPACCompany[0]['alamat_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMPACCompany[0]['rt_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMPACCompany[0]['rw_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMPACCompany[0]['kelurahan_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMPACCompany[0]['kecamatan_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMPACCompany[0]['kabupaten_KOTA_KELEMBAGAAN'] == "1",
          _dataHideShowFormMPACCompany[0]['provinsi_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMPACCompany[0]['kode_POS_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMPACCompany[0]['telepon_AREA_1_KELEMBAGAAN'] == "1",
          _dataHideShowFormMPACCompany[0]['telepon_1_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMPACCompany[0]['telepon_AREA_2_KELEMBAGAAN'] == "1",
          _dataHideShowFormMPACCompany[0]['telepon_2_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMPACCompany[0]['fax_AREA_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMPACCompany[0]['fax_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMPACCompany[0]['jenis_ALAMAT_KELEMBAGAAN'] == "1",
          _dataMandatoryFormMPACCompany[0]['alamat_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMPACCompany[0]['rt_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMPACCompany[0]['rw_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMPACCompany[0]['kelurahan_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMPACCompany[0]['kecamatan_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMPACCompany[0]['kabupaten_KOTA_KELEMBAGAAN'] == "1",
          _dataMandatoryFormMPACCompany[0]['provinsi_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMPACCompany[0]['kode_POS_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMPACCompany[0]['telepon_AREA_1_KELEMBAGAAN'] == "1",
          _dataMandatoryFormMPACCompany[0]['telepon_1_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMPACCompany[0]['telepon_AREA_2_KELEMBAGAAN'] == "1",
          _dataMandatoryFormMPACCompany[0]['telepon_2_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMPACCompany[0]['fax_AREA_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMPACCompany[0]['fax_KELEMBAGAAN_ALAMAT'] == "1",
        );
        _setAddressIndividuManajemenPIC = SetAddressIndividu(
            _dataHideShowFormMPACCompany[0]['jenis_ALAMAT_MANAJEMEN_PIC'] == "1",
            _dataHideShowFormMPACCompany[0]['alamat_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHideShowFormMPACCompany[0]['rt_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHideShowFormMPACCompany[0]['rw_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHideShowFormMPACCompany[0]['kelurahan_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHideShowFormMPACCompany[0]['kecamatan_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHideShowFormMPACCompany[0]['kabupaten_KOTA_MANAJEMEN_PIC'] == "1",
            _dataHideShowFormMPACCompany[0]['provinsi_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHideShowFormMPACCompany[0]['kode_POS_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHideShowFormMPACCompany[0]['telepon_AREA_MANAJEMEN_PIC'] == "1",
            _dataHideShowFormMPACCompany[0]['telepon_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMPACCompany[0]['jenis_ALAMAT_MANAJEMEN_PIC'] == "1",
            _dataMandatoryFormMPACCompany[0]['alamat_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMPACCompany[0]['rt_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMPACCompany[0]['rw_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMPACCompany[0]['kelurahan_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMPACCompany[0]['kecamatan_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMPACCompany[0]['kabupaten_KOTA_MANAJEMEN_PIC'] == "1",
            _dataMandatoryFormMPACCompany[0]['provinsi_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMPACCompany[0]['kode_POS_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMPACCompany[0]['telepon_AREA_MANAJEMEN_PIC'] == "1",
            _dataMandatoryFormMPACCompany[0]['telepon_MANAJEMEN_PIC_ALAMAT'] == "1"
        );
        _setAddressIndividuPemegangSaham = SetAddressIndividu(
            _dataHideShowFormMPACCompany[0]['jns_ALAMAT_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMPACCompany[0]['alamat_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMPACCompany[0]['rt_PEMEGANG_SAHAM_IND_ALAMAT']== "1",
            _dataHideShowFormMPACCompany[0]['rw_PEMEGANG_SAHAM_IND_ALAMAT']== "1",
            _dataHideShowFormMPACCompany[0]['kelurahan_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMPACCompany[0]['kecamatan_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMPACCompany[0]['kabkot_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMPACCompany[0]['provinsi_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMPACCompany[0]['kode_POS_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMPACCompany[0]['phone_AREA_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMPACCompany[0]['phone_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMPACCompany[0]['jns_ALAMAT_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMPACCompany[0]['alamat_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMPACCompany[0]['rt_PEMEGANG_SAHAM_IND_ALAMAT']== "1",
            _dataMandatoryFormMPACCompany[0]['rw_PEMEGANG_SAHAM_IND_ALAMAT']== "1",
            _dataMandatoryFormMPACCompany[0]['kelurahan_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMPACCompany[0]['kecamatan_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMPACCompany[0]['kabkot_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMPACCompany[0]['provinsi_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMPACCompany[0]['kode_POS_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMPACCompany[0]['phone_AREA_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMPACCompany[0]['phone_PEMEGANG_SAHAM_IND'] == "1"
        );
        _setAddressSahamLembagaCompanyModel = SetAddressCompanyModel(
          _dataHideShowFormMPACCompany[0]['jns_ALAMAT_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHideShowFormMPACCompany[0]['addr_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHideShowFormMPACCompany[0]['rt_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
          _dataHideShowFormMPACCompany[0]['rw_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
          _dataHideShowFormMPACCompany[0]['kelurahan_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHideShowFormMPACCompany[0]['kecamatan_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHideShowFormMPACCompany[0]['kabkota_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHideShowFormMPACCompany[0]['prov_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHideShowFormMPACCompany[0]['kode_POS_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHideShowFormMPACCompany[0]['phonearea1_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHideShowFormMPACCompany[0]['phone1_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHideShowFormMPACCompany[0]['phonearea2_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHideShowFormMPACCompany[0]['phone2_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHideShowFormMPACCompany[0]['faxarea_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHideShowFormMPACCompany[0]['fax_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
          _dataMandatoryFormMPACCompany[0]['jns_ALAMAT_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryFormMPACCompany[0]['addr_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryFormMPACCompany[0]['rt_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
          _dataMandatoryFormMPACCompany[0]['rw_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
          _dataMandatoryFormMPACCompany[0]['kelurahan_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryFormMPACCompany[0]['kecamatan_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryFormMPACCompany[0]['kabkota_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryFormMPACCompany[0]['prov_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryFormMPACCompany[0]['kode_POS_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryFormMPACCompany[0]['phonearea1_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryFormMPACCompany[0]['phone1_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryFormMPACCompany[0]['phonearea2_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryFormMPACCompany[0]['phone2_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryFormMPACCompany[0]['faxarea_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryFormMPACCompany[0]['fax_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
        );
      }
      else if (_lastKnownState == "RSVY") {
        //rincian company hideshow
        _providerRincianCompany.isCompanyTypeShow = _dataHideShowFormMResurveyCompany[0]['jenis_LEMBAGA_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isProfileShow = _dataHideShowFormMResurveyCompany[0]['profil_RINCIAN'] == "1";
        _providerRincianCompany.isCompanyNameShow = _dataHideShowFormMResurveyCompany[0]['nama_LEMBAGA_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isHaveNPWPShow = _dataHideShowFormMResurveyCompany[0]['punya_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isNoNPWPShow = _dataHideShowFormMResurveyCompany[0]['npwp_RINCIAN'] == "1";
        _providerRincianCompany.isNameNPWPShow = _dataHideShowFormMResurveyCompany[0]['nama_LENGKAP_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isTypeNPWPShow = _dataHideShowFormMResurveyCompany[0]['jenis_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isPKPSignShow = _dataHideShowFormMResurveyCompany[0]['tanda_PKP_RINCIAN'] == "1";
        _providerRincianCompany.isAddressNPWPShow = _dataHideShowFormMResurveyCompany[0]['alamat_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isEstablishDateShow = _dataHideShowFormMResurveyCompany[0]['tanggal_PENDIRIAN_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isSectorEconomicShow = _dataHideShowFormMResurveyCompany[0]['sektor_EKONOMI_RINCIAN'] == "1";
        _providerRincianCompany.isBusinessFieldShow = _dataHideShowFormMResurveyCompany[0]['lapangan_USAHA_RINCIAN'] == "1";
        _providerRincianCompany.isLocationStatusShow = _dataHideShowFormMResurveyCompany[0]['status_LOKASI_RINCIAN'] == "1";
        _providerRincianCompany.isBusinessLocationShow = _dataHideShowFormMResurveyCompany[0]['lokasi_USAHA_RINCIAN'] == "1";
        _providerRincianCompany.isTotalEmpShow = _dataHideShowFormMResurveyCompany[0]['total_PEGAWAI_RINCIAN'] == "1";
        _providerRincianCompany.isLamaUsahaShow = _dataHideShowFormMResurveyCompany[0]['lama_USAHA_BERJALAN_RINCIAN'] == "1";
        _providerRincianCompany.isTotalLamaUsahaShow = _dataHideShowFormMResurveyCompany[0]['total_LAMA_USAHA_BERJALAN'] == "1";

        //rincian company mandatory
        _providerRincianCompany.isCompanyTypeMandatory = _dataMandatoryFormMResurveyCompany[0]['jenis_LEMBAGA_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isProfileMandatory = _dataMandatoryFormMResurveyCompany[0]['profil_RINCIAN'] == "1";
        _providerRincianCompany.isCompanyNameMandatory = _dataMandatoryFormMResurveyCompany[0]['nama_LEMBAGA_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isHaveNPWPMandatory = _dataMandatoryFormMResurveyCompany[0]['punya_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isNoNPWPMandatory = _dataMandatoryFormMResurveyCompany[0]['npwp_RINCIAN'] == "1";
        _providerRincianCompany.isNameNPWPMandatory = _dataMandatoryFormMResurveyCompany[0]['nama_LENGKAP_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isTypeNPWPMandatory = _dataMandatoryFormMResurveyCompany[0]['jenis_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isPKPSignMandatory = _dataMandatoryFormMResurveyCompany[0]['tanda_PKP_RINCIAN'] == "1";
        _providerRincianCompany.isAddressNPWPMandatory = _dataMandatoryFormMResurveyCompany[0]['alamat_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isEstablishDateMandatory = _dataMandatoryFormMResurveyCompany[0]['tanggal_PENDIRIAN_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isSectorEconomicMandatory = _dataMandatoryFormMResurveyCompany[0]['sektor_EKONOMI_RINCIAN'] == "1";
        _providerRincianCompany.isBusinessFieldMandatory = _dataMandatoryFormMResurveyCompany[0]['lapangan_USAHA_RINCIAN'] == "1";
        _providerRincianCompany.isLocationStatusMandatory = _dataMandatoryFormMResurveyCompany[0]['status_LOKASI_RINCIAN'] == "1";
        _providerRincianCompany.isBusinessLocationMandatory = _dataMandatoryFormMResurveyCompany[0]['lokasi_USAHA_RINCIAN'] == "1";
        _providerRincianCompany.isTotalEmpMandatory = _dataMandatoryFormMResurveyCompany[0]['total_PEGAWAI_RINCIAN'] == "1";
        _providerRincianCompany.isLamaUsahaMandatory = _dataMandatoryFormMResurveyCompany[0]['lama_USAHA_BERJALAN_RINCIAN'] == "1";
        _providerRincianCompany.isTotalLamaUsahaMandatory = _dataMandatoryFormMResurveyCompany[0]['total_LAMA_USAHA_BERJALAN'] == "1";

        //pendapatan hideshow & mandatory
        _setIncomeCompanyModel = SetIncomeCompanyModel(
          _dataHideShowFormMResurveyCompany[0]['pendapatan_PERBULAN'] == "1",
          _dataHideShowFormMResurveyCompany[0]['pendapatan_LAINNYA'] == "1",
          _dataHideShowFormMResurveyCompany[0]['total_PENDAPATAN'] == "1",
          _dataHideShowFormMResurveyCompany[0]['harga_POKOK_PENDAPATAN'] == "1",
          _dataHideShowFormMResurveyCompany[0]['laba_KOTOR'] == "1",
          _dataHideShowFormMResurveyCompany[0]['biaya_OPERASIONAL'] == "1",
          _dataHideShowFormMResurveyCompany[0]['biaya_LAINNYA'] == "1",
          _dataHideShowFormMResurveyCompany[0]['laba_BERSIH_SEBELUM_PAJAK'] == "1",
          _dataHideShowFormMResurveyCompany[0]['pajak'] == "1",
          _dataHideShowFormMResurveyCompany[0]['laba_BERSIH_SETELAH_PAJAK'] == "1",
          _dataHideShowFormMResurveyCompany[0]['angsuran_LAINNYA'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['pendapatan_PERBULAN'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['pendapatan_LAINNYA'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['total_PENDAPATAN'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['harga_POKOK_PENDAPATAN'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['laba_KOTOR'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['biaya_OPERASIONAL'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['biaya_LAINNYA'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['laba_BERSIH_SEBELUM_PAJAK'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['pajak'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['laba_BERSIH_SETELAH_PAJAK'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['angsuran_LAINNYA'] == "1",
        );

        //penjamin hideshow & mandatory
        _setGuarantorIndividuCompany = SetGuarantorIndividu(
          _dataHideShowFormMResurveyCompany[0]['status_HUBUNGAN_IND'] == "1",
          _dataHideShowFormMResurveyCompany[0]['jenis_IDENTITAS_IND'] == "1",
          _dataHideShowFormMResurveyCompany[0]['no_IDENTITAS_IND'] == "1",
          _dataHideShowFormMResurveyCompany[0]['nama_LENGKAP_ID_IND'] == "1",
          _dataHideShowFormMResurveyCompany[0]['nama_LENGKAP_IND'] == "1",
          _dataHideShowFormMResurveyCompany[0]['tanggal_LAHIR_IND'] == "1",
          _dataHideShowFormMResurveyCompany[0]['pob_ID_IND'] == "1",
          _dataHideShowFormMResurveyCompany[0]['pob_ID_LOV_IND'] == "1",
          _dataHideShowFormMResurveyCompany[0]['jenis_KELAMIN_IND'] == "1",
          _dataHideShowFormMResurveyCompany[0]['handphone_IND'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['status_HUBUNGAN_IND'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['jenis_IDENTITAS_IND'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['no_IDENTITAS_IND'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['nama_LENGKAP_ID_IND'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['nama_LENGKAP_IND'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['tanggal_LAHIR_IND'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['pob_ID_IND'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['pob_ID_LOV_IND'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['jenis_KELAMIN_IND'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['handphone_IND'] == "1",
        );
        _setGuarantorLembagaCompany = SetGuarantorCompanyModel(
            _dataHideShowFormMResurveyCompany[0]['jenis_LEMBAGA_KELEMBAGAAN'] == "1",
            _dataHideShowFormMResurveyCompany[0]['nama_LEMBAGA_KELEMBAGAAN'] == "1",
            _dataHideShowFormMResurveyCompany[0]['tanggal_PENDIRIAN_KELEMBAGAAN'] == "1",
            _dataHideShowFormMResurveyCompany[0]['npwp_KELEMBAGAAN'] == "1",
            _dataMandatoryFormMResurveyCompany[0]['jenis_LEMBAGA_KELEMBAGAAN'] == "1",
            _dataMandatoryFormMResurveyCompany[0]['nama_LEMBAGA_KELEMBAGAAN'] == "1",
            _dataMandatoryFormMResurveyCompany[0]['tanggal_PENDIRIAN_KELEMBAGAAN'] == "1",
            _dataMandatoryFormMResurveyCompany[0]['npwp_KELEMBAGAAN'] == "1"
        );

        //manajemenPIC hideshow & mandatory
        _setManagementPICModel = SetManagementPICCompanyModel(
          _dataHideShowFormMResurveyCompany[0]['jenis_IDENTITAS_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMResurveyCompany[0]['no_IDENTITAS_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMResurveyCompany[0]['nama_LENGKAP_ID_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMResurveyCompany[0]['nama_LENGKAP_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMResurveyCompany[0]['tanggal_LAHIR_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMResurveyCompany[0]['pob_ID_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMResurveyCompany[0]['pob_ID_LOV_PIC'] == "1",
          _dataHideShowFormMResurveyCompany[0]['jabatan_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMResurveyCompany[0]['email_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMResurveyCompany[0]['handphone_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['jenis_IDENTITAS_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['no_IDENTITAS_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['nama_LENGKAP_ID_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['nama_LENGKAP_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['tanggal_LAHIR_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['pob_ID_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['pob_ID_LOV_PIC'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['jabatan_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['email_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['handphone_MANAJEMEN_PIC'] == "1",
        );

        //pemegang saham individu-lembaga hideshow & mandatory
        _setPemegangSahamIndividuCompanyModel = SetPemegangSahamIndividuCompanyModel(
          _dataHideShowFormMResurveyCompany[0]['hubungan_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMResurveyCompany[0]['jenis_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMResurveyCompany[0]['no_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMResurveyCompany[0]['nama_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMResurveyCompany[0]['nama_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMResurveyCompany[0]['tgl_LAHIR_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMResurveyCompany[0]['pob_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMResurveyCompany[0]['pob_ID_LOV_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMResurveyCompany[0]['jns_KELAMIN_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMResurveyCompany[0]['share_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['hubungan_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['jenis_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['no_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['nama_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['nama_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['tgl_LAHIR_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['pob_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['pob_ID_LOV_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['jns_KELAMIN_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['share_PEMEGANG_SAHAM_IND'] == "1",
        );
        _setPemegangSahamLembagaCompanyModel = SetPemegangSahamLembagaCompanyModel(
          _dataHideShowFormMResurveyCompany[0]['jenis_LEMBAGA_PEMEGANG_SAHAM'] == "1",
          _dataHideShowFormMResurveyCompany[0]['nama_LEMBAGA_PEMEGANG_SAHAM'] == "1",
          _dataHideShowFormMResurveyCompany[0]['tgl_PENDIRIAN_PEMEGANG_SAHAM'] == "1",
          _dataHideShowFormMResurveyCompany[0]['npwp_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHideShowFormMResurveyCompany[0]['share_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['jenis_LEMBAGA_PEMEGANG_SAHAM'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['nama_LEMBAGA_PEMEGANG_SAHAM'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['tgl_PENDIRIAN_PEMEGANG_SAHAM'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['npwp_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['share_PEMEGANG_SAHAM_LEMBAGA'] == "1",
        );

        //alamat
        _setAddressCompanyModel = SetAddressCompanyModel(
          _dataHideShowFormMResurveyCompany[0]['jenis_ALAMAT'] == "1",
          _dataHideShowFormMResurveyCompany[0]['alamat'] == "1",
          _dataHideShowFormMResurveyCompany[0]['rt'] == "1",
          _dataHideShowFormMResurveyCompany[0]['rw'] == "1",
          _dataHideShowFormMResurveyCompany[0]['kelurahan'] == "1",
          _dataHideShowFormMResurveyCompany[0]['kecamatan'] == "1",
          _dataHideShowFormMResurveyCompany[0]['kabupaten_KOTA'] == "1",
          _dataHideShowFormMResurveyCompany[0]['provinsi'] == "1",
          _dataHideShowFormMResurveyCompany[0]['kode_POS'] == "1",
          _dataHideShowFormMResurveyCompany[0]['telepon_AREA_1'] == "1",
          _dataHideShowFormMResurveyCompany[0]['telepon_1'] == "1",
          _dataHideShowFormMResurveyCompany[0]['telepon_AREA_2'] == "1",
          _dataHideShowFormMResurveyCompany[0]['telepon_2'] == "1",
          _dataHideShowFormMResurveyCompany[0]['fax_AREA'] == "1",
          _dataHideShowFormMResurveyCompany[0]['fax'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['jenis_ALAMAT'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['alamat'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['rt'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['rw'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['kelurahan'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['kecamatan'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['kabupaten_KOTA'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['provinsi'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['kode_POS'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['telepon_AREA_1'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['telepon_1'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['telepon_AREA_2'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['telepon_2'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['fax_AREA'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['fax'] == "1",
        );
        _setAddressIndividuPenjaminCompany = SetAddressIndividu(
            _dataHideShowFormMResurveyCompany[0]['jenis_ALAMAT_IND_ALAMAT'] == "1",
            _dataHideShowFormMResurveyCompany[0]['alamat_IND_ALAMAT'] == "1",
            _dataHideShowFormMResurveyCompany[0]['rt_IND_ALAMAT'] == "1",
            _dataHideShowFormMResurveyCompany[0]['rw_IND_ALAMAT'] == "1",
            _dataHideShowFormMResurveyCompany[0]['kelurahan_IND_ALAMAT'] == "1",
            _dataHideShowFormMResurveyCompany[0]['kecamatan_IND_ALAMAT'] == "1",
            _dataHideShowFormMResurveyCompany[0]['kabupaten_KOTA_IND_ALAMAT'] == "1",
            _dataHideShowFormMResurveyCompany[0]['provinsi_IND_ALAMAT'] == "1",
            _dataHideShowFormMResurveyCompany[0]['kode_POS_IND_ALAMAT'] == "1",
            _dataHideShowFormMResurveyCompany[0]['telepon_AREA_IND_ALAMAT'] == "1",
            _dataHideShowFormMResurveyCompany[0]['telepon_IND_ALAMAT'] == "1",
            _dataMandatoryFormMResurveyCompany[0]['jenis_ALAMAT_IND_ALAMAT'] == "1",
            _dataMandatoryFormMResurveyCompany[0]['alamat_IND_ALAMAT'] == "1",
            _dataMandatoryFormMResurveyCompany[0]['rt_IND_ALAMAT'] == "1",
            _dataMandatoryFormMResurveyCompany[0]['rw_IND_ALAMAT'] == "1",
            _dataMandatoryFormMResurveyCompany[0]['kelurahan_IND_ALAMAT'] == "1",
            _dataMandatoryFormMResurveyCompany[0]['kecamatan_IND_ALAMAT'] == "1",
            _dataMandatoryFormMResurveyCompany[0]['kabupaten_KOTA_IND_ALAMAT'] == "1",
            _dataMandatoryFormMResurveyCompany[0]['provinsi_IND_ALAMAT'] == "1",
            _dataMandatoryFormMResurveyCompany[0]['kode_POS_IND_ALAMAT'] == "1",
            _dataMandatoryFormMResurveyCompany[0]['telepon_AREA_IND_ALAMAT'] == "1",
            _dataMandatoryFormMResurveyCompany[0]['telepon_IND_ALAMAT'] == "1"
        );
        _setAddressCompanyGuarantorModel = SetAddressCompanyModel(
          _dataHideShowFormMResurveyCompany[0]['jenis_ALAMAT_KELEMBAGAAN'] == "1",
          _dataHideShowFormMResurveyCompany[0]['alamat_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMResurveyCompany[0]['rt_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMResurveyCompany[0]['rw_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMResurveyCompany[0]['kelurahan_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMResurveyCompany[0]['kecamatan_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMResurveyCompany[0]['kabupaten_KOTA_KELEMBAGAAN'] == "1",
          _dataHideShowFormMResurveyCompany[0]['provinsi_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMResurveyCompany[0]['kode_POS_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMResurveyCompany[0]['telepon_AREA_1_KELEMBAGAAN'] == "1",
          _dataHideShowFormMResurveyCompany[0]['telepon_1_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMResurveyCompany[0]['telepon_AREA_2_KELEMBAGAAN'] == "1",
          _dataHideShowFormMResurveyCompany[0]['telepon_2_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMResurveyCompany[0]['fax_AREA_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMResurveyCompany[0]['fax_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['jenis_ALAMAT_KELEMBAGAAN'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['alamat_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['rt_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['rw_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['kelurahan_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['kecamatan_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['kabupaten_KOTA_KELEMBAGAAN'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['provinsi_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['kode_POS_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['telepon_AREA_1_KELEMBAGAAN'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['telepon_1_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['telepon_AREA_2_KELEMBAGAAN'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['telepon_2_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['fax_AREA_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['fax_KELEMBAGAAN_ALAMAT'] == "1",
        );
        _setAddressIndividuManajemenPIC = SetAddressIndividu(
            _dataHideShowFormMResurveyCompany[0]['jenis_ALAMAT_MANAJEMEN_PIC'] == "1",
            _dataHideShowFormMResurveyCompany[0]['alamat_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHideShowFormMResurveyCompany[0]['rt_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHideShowFormMResurveyCompany[0]['rw_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHideShowFormMResurveyCompany[0]['kelurahan_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHideShowFormMResurveyCompany[0]['kecamatan_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHideShowFormMResurveyCompany[0]['kabupaten_KOTA_MANAJEMEN_PIC'] == "1",
            _dataHideShowFormMResurveyCompany[0]['provinsi_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHideShowFormMResurveyCompany[0]['kode_POS_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHideShowFormMResurveyCompany[0]['telepon_AREA_MANAJEMEN_PIC'] == "1",
            _dataHideShowFormMResurveyCompany[0]['telepon_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMResurveyCompany[0]['jenis_ALAMAT_MANAJEMEN_PIC'] == "1",
            _dataMandatoryFormMResurveyCompany[0]['alamat_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMResurveyCompany[0]['rt_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMResurveyCompany[0]['rw_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMResurveyCompany[0]['kelurahan_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMResurveyCompany[0]['kecamatan_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMResurveyCompany[0]['kabupaten_KOTA_MANAJEMEN_PIC'] == "1",
            _dataMandatoryFormMResurveyCompany[0]['provinsi_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMResurveyCompany[0]['kode_POS_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMResurveyCompany[0]['telepon_AREA_MANAJEMEN_PIC'] == "1",
            _dataMandatoryFormMResurveyCompany[0]['telepon_MANAJEMEN_PIC_ALAMAT'] == "1"
        );
        _setAddressIndividuPemegangSaham = SetAddressIndividu(
            _dataHideShowFormMResurveyCompany[0]['jns_ALAMAT_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMResurveyCompany[0]['alamat_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMResurveyCompany[0]['rt_PEMEGANG_SAHAM_IND_ALAMAT']== "1",
            _dataHideShowFormMResurveyCompany[0]['rw_PEMEGANG_SAHAM_IND_ALAMAT']== "1",
            _dataHideShowFormMResurveyCompany[0]['kelurahan_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMResurveyCompany[0]['kecamatan_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMResurveyCompany[0]['kabkot_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMResurveyCompany[0]['provinsi_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMResurveyCompany[0]['kode_POS_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMResurveyCompany[0]['phone_AREA_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMResurveyCompany[0]['phone_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMResurveyCompany[0]['jns_ALAMAT_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMResurveyCompany[0]['alamat_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMResurveyCompany[0]['rt_PEMEGANG_SAHAM_IND_ALAMAT']== "1",
            _dataMandatoryFormMResurveyCompany[0]['rw_PEMEGANG_SAHAM_IND_ALAMAT']== "1",
            _dataMandatoryFormMResurveyCompany[0]['kelurahan_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMResurveyCompany[0]['kecamatan_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMResurveyCompany[0]['kabkot_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMResurveyCompany[0]['provinsi_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMResurveyCompany[0]['kode_POS_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMResurveyCompany[0]['phone_AREA_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMResurveyCompany[0]['phone_PEMEGANG_SAHAM_IND'] == "1"
        );
        _setAddressSahamLembagaCompanyModel = SetAddressCompanyModel(
          _dataHideShowFormMResurveyCompany[0]['jns_ALAMAT_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHideShowFormMResurveyCompany[0]['addr_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHideShowFormMResurveyCompany[0]['rt_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
          _dataHideShowFormMResurveyCompany[0]['rw_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
          _dataHideShowFormMResurveyCompany[0]['kelurahan_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHideShowFormMResurveyCompany[0]['kecamatan_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHideShowFormMResurveyCompany[0]['kabkota_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHideShowFormMResurveyCompany[0]['prov_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHideShowFormMResurveyCompany[0]['kode_POS_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHideShowFormMResurveyCompany[0]['phonearea1_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHideShowFormMResurveyCompany[0]['phone1_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHideShowFormMResurveyCompany[0]['phonearea2_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHideShowFormMResurveyCompany[0]['phone2_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHideShowFormMResurveyCompany[0]['faxarea_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHideShowFormMResurveyCompany[0]['fax_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['jns_ALAMAT_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['addr_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['rt_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['rw_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['kelurahan_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['kecamatan_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['kabkota_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['prov_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['kode_POS_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['phonearea1_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['phone1_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['phonearea2_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['phone2_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['faxarea_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryFormMResurveyCompany[0]['fax_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
        );
      }
      else if (_lastKnownState == "DKR") {
        //rincian company hideshow
        _providerRincianCompany.isCompanyTypeShow = _dataHideShowFormMDakorCom[0]['jenis_LEMBAGA_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isProfileShow = _dataHideShowFormMDakorCom[0]['profil_RINCIAN'] == "1";
        _providerRincianCompany.isCompanyNameShow = _dataHideShowFormMDakorCom[0]['nama_LEMBAGA_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isHaveNPWPShow = _dataHideShowFormMDakorCom[0]['punya_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isNoNPWPShow = _dataHideShowFormMDakorCom[0]['npwp_RINCIAN'] == "1";
        _providerRincianCompany.isNameNPWPShow = _dataHideShowFormMDakorCom[0]['nama_LENGKAP_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isTypeNPWPShow = _dataHideShowFormMDakorCom[0]['jenis_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isPKPSignShow = _dataHideShowFormMDakorCom[0]['tanda_PKP_RINCIAN'] == "1";
        _providerRincianCompany.isAddressNPWPShow = _dataHideShowFormMDakorCom[0]['alamat_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isEstablishDateShow = _dataHideShowFormMDakorCom[0]['tanggal_PENDIRIAN_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isSectorEconomicShow = _dataHideShowFormMDakorCom[0]['sektor_EKONOMI_RINCIAN'] == "1";
        _providerRincianCompany.isBusinessFieldShow = _dataHideShowFormMDakorCom[0]['lapangan_USAHA_RINCIAN'] == "1";
        _providerRincianCompany.isLocationStatusShow = _dataHideShowFormMDakorCom[0]['status_LOKASI_RINCIAN'] == "1";
        _providerRincianCompany.isBusinessLocationShow = _dataHideShowFormMDakorCom[0]['lokasi_USAHA_RINCIAN'] == "1";
        _providerRincianCompany.isTotalEmpShow = _dataHideShowFormMDakorCom[0]['total_PEGAWAI_RINCIAN'] == "1";
        _providerRincianCompany.isLamaUsahaShow = _dataHideShowFormMDakorCom[0]['lama_USAHA_BERJALAN_RINCIAN'] == "1";
        _providerRincianCompany.isTotalLamaUsahaShow = _dataHideShowFormMDakorCom[0]['total_LAMA_USAHA_BERJALAN'] == "1";

        //rincian company mandatory
        _providerRincianCompany.isCompanyTypeMandatory = _dataMandatoryFormMDakorCom[0]['jenis_LEMBAGA_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isProfileMandatory = _dataMandatoryFormMDakorCom[0]['profil_RINCIAN'] == "1";
        _providerRincianCompany.isCompanyNameMandatory = _dataMandatoryFormMDakorCom[0]['nama_LEMBAGA_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isHaveNPWPMandatory = _dataMandatoryFormMDakorCom[0]['punya_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isNoNPWPMandatory = _dataMandatoryFormMDakorCom[0]['npwp_RINCIAN'] == "1";
        _providerRincianCompany.isNameNPWPMandatory = _dataMandatoryFormMDakorCom[0]['nama_LENGKAP_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isTypeNPWPMandatory = _dataMandatoryFormMDakorCom[0]['jenis_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isPKPSignMandatory = _dataMandatoryFormMDakorCom[0]['tanda_PKP_RINCIAN'] == "1";
        _providerRincianCompany.isAddressNPWPMandatory = _dataMandatoryFormMDakorCom[0]['alamat_NPWP_RINCIAN'] == "1";
        _providerRincianCompany.isEstablishDateMandatory = _dataMandatoryFormMDakorCom[0]['tanggal_PENDIRIAN_KELEMBAGAAN'] == "1";
        _providerRincianCompany.isSectorEconomicMandatory = _dataMandatoryFormMDakorCom[0]['sektor_EKONOMI_RINCIAN'] == "1";
        _providerRincianCompany.isBusinessFieldMandatory = _dataMandatoryFormMDakorCom[0]['lapangan_USAHA_RINCIAN'] == "1";
        _providerRincianCompany.isLocationStatusMandatory = _dataMandatoryFormMDakorCom[0]['status_LOKASI_RINCIAN'] == "1";
        _providerRincianCompany.isBusinessLocationMandatory = _dataMandatoryFormMDakorCom[0]['lokasi_USAHA_RINCIAN'] == "1";
        _providerRincianCompany.isTotalEmpMandatory = _dataMandatoryFormMDakorCom[0]['total_PEGAWAI_RINCIAN'] == "1";
        _providerRincianCompany.isLamaUsahaMandatory = _dataMandatoryFormMDakorCom[0]['lama_USAHA_BERJALAN_RINCIAN'] == "1";
        _providerRincianCompany.isTotalLamaUsahaMandatory = _dataMandatoryFormMDakorCom[0]['total_LAMA_USAHA_BERJALAN'] == "1";

        //pendapatan hideshow & mandatory
        _setIncomeCompanyModel = SetIncomeCompanyModel(
          _dataHideShowFormMDakorCom[0]['pendapatan_PERBULAN'] == "1",
          _dataHideShowFormMDakorCom[0]['pendapatan_LAINNYA'] == "1",
          _dataHideShowFormMDakorCom[0]['total_PENDAPATAN'] == "1",
          _dataHideShowFormMDakorCom[0]['harga_POKOK_PENDAPATAN'] == "1",
          _dataHideShowFormMDakorCom[0]['laba_KOTOR'] == "1",
          _dataHideShowFormMDakorCom[0]['biaya_OPERASIONAL'] == "1",
          _dataHideShowFormMDakorCom[0]['biaya_LAINNYA'] == "1",
          _dataHideShowFormMDakorCom[0]['laba_BERSIH_SEBELUM_PAJAK'] == "1",
          _dataHideShowFormMDakorCom[0]['pajak'] == "1",
          _dataHideShowFormMDakorCom[0]['laba_BERSIH_SETELAH_PAJAK'] == "1",
          _dataHideShowFormMDakorCom[0]['angsuran_LAINNYA'] == "1",
          _dataMandatoryFormMDakorCom[0]['pendapatan_PERBULAN'] == "1",
          _dataMandatoryFormMDakorCom[0]['pendapatan_LAINNYA'] == "1",
          _dataMandatoryFormMDakorCom[0]['total_PENDAPATAN'] == "1",
          _dataMandatoryFormMDakorCom[0]['harga_POKOK_PENDAPATAN'] == "1",
          _dataMandatoryFormMDakorCom[0]['laba_KOTOR'] == "1",
          _dataMandatoryFormMDakorCom[0]['biaya_OPERASIONAL'] == "1",
          _dataMandatoryFormMDakorCom[0]['biaya_LAINNYA'] == "1",
          _dataMandatoryFormMDakorCom[0]['laba_BERSIH_SEBELUM_PAJAK'] == "1",
          _dataMandatoryFormMDakorCom[0]['pajak'] == "1",
          _dataMandatoryFormMDakorCom[0]['laba_BERSIH_SETELAH_PAJAK'] == "1",
          _dataMandatoryFormMDakorCom[0]['angsuran_LAINNYA'] == "1",
        );

        //penjamin hideshow & mandatory
        _setGuarantorIndividuCompany = SetGuarantorIndividu(
          _dataHideShowFormMDakorCom[0]['status_HUBUNGAN_IND'] == "1",
          _dataHideShowFormMDakorCom[0]['jenis_IDENTITAS_IND'] == "1",
          _dataHideShowFormMDakorCom[0]['no_IDENTITAS_IND'] == "1",
          _dataHideShowFormMDakorCom[0]['nama_LENGKAP_ID_IND'] == "1",
          _dataHideShowFormMDakorCom[0]['nama_LENGKAP_IND'] == "1",
          _dataHideShowFormMDakorCom[0]['tanggal_LAHIR_IND'] == "1",
          _dataHideShowFormMDakorCom[0]['pob_ID_IND'] == "1",
          _dataHideShowFormMDakorCom[0]['pob_ID_LOV_IND'] == "1",
          _dataHideShowFormMDakorCom[0]['jenis_KELAMIN_IND'] == "1",
          _dataHideShowFormMDakorCom[0]['handphone_IND'] == "1",
          _dataMandatoryFormMDakorCom[0]['status_HUBUNGAN_IND'] == "1",
          _dataMandatoryFormMDakorCom[0]['jenis_IDENTITAS_IND'] == "1",
          _dataMandatoryFormMDakorCom[0]['no_IDENTITAS_IND'] == "1",
          _dataMandatoryFormMDakorCom[0]['nama_LENGKAP_ID_IND'] == "1",
          _dataMandatoryFormMDakorCom[0]['nama_LENGKAP_IND'] == "1",
          _dataMandatoryFormMDakorCom[0]['tanggal_LAHIR_IND'] == "1",
          _dataMandatoryFormMDakorCom[0]['pob_ID_IND'] == "1",
          _dataMandatoryFormMDakorCom[0]['pob_ID_LOV_IND'] == "1",
          _dataMandatoryFormMDakorCom[0]['jenis_KELAMIN_IND'] == "1",
          _dataMandatoryFormMDakorCom[0]['handphone_IND'] == "1",
        );
        _setGuarantorLembagaCompany = SetGuarantorCompanyModel(
            _dataHideShowFormMDakorCom[0]['jenis_LEMBAGA_KELEMBAGAAN'] == "1",
            _dataHideShowFormMDakorCom[0]['nama_LEMBAGA_KELEMBAGAAN'] == "1",
            _dataHideShowFormMDakorCom[0]['tanggal_PENDIRIAN_KELEMBAGAAN'] == "1",
            _dataHideShowFormMDakorCom[0]['npwp_KELEMBAGAAN'] == "1",
            _dataMandatoryFormMDakorCom[0]['jenis_LEMBAGA_KELEMBAGAAN'] == "1",
            _dataMandatoryFormMDakorCom[0]['nama_LEMBAGA_KELEMBAGAAN'] == "1",
            _dataMandatoryFormMDakorCom[0]['tanggal_PENDIRIAN_KELEMBAGAAN'] == "1",
            _dataMandatoryFormMDakorCom[0]['npwp_KELEMBAGAAN'] == "1"
        );

        //manajemenPIC hideshow & mandatory
        _setManagementPICModel = SetManagementPICCompanyModel(
          _dataHideShowFormMDakorCom[0]['jenis_IDENTITAS_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMDakorCom[0]['no_IDENTITAS_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMDakorCom[0]['nama_LENGKAP_ID_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMDakorCom[0]['nama_LENGKAP_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMDakorCom[0]['tanggal_LAHIR_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMDakorCom[0]['pob_ID_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMDakorCom[0]['pob_ID_LOV_PIC'] == "1",
          _dataHideShowFormMDakorCom[0]['jabatan_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMDakorCom[0]['email_MANAJEMEN_PIC'] == "1",
          _dataHideShowFormMDakorCom[0]['handphone_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMDakorCom[0]['jenis_IDENTITAS_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMDakorCom[0]['no_IDENTITAS_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMDakorCom[0]['nama_LENGKAP_ID_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMDakorCom[0]['nama_LENGKAP_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMDakorCom[0]['tanggal_LAHIR_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMDakorCom[0]['pob_ID_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMDakorCom[0]['pob_ID_LOV_PIC'] == "1",
          _dataMandatoryFormMDakorCom[0]['jabatan_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMDakorCom[0]['email_MANAJEMEN_PIC'] == "1",
          _dataMandatoryFormMDakorCom[0]['handphone_MANAJEMEN_PIC'] == "1",
        );

        //pemegang saham individu-lembaga hideshow & mandatory
        _setPemegangSahamIndividuCompanyModel = SetPemegangSahamIndividuCompanyModel(
          _dataHideShowFormMDakorCom[0]['hubungan_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMDakorCom[0]['jenis_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMDakorCom[0]['no_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMDakorCom[0]['nama_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMDakorCom[0]['nama_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMDakorCom[0]['tgl_LAHIR_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMDakorCom[0]['pob_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMDakorCom[0]['pob_ID_LOV_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMDakorCom[0]['jns_KELAMIN_PEMEGANG_SAHAM_IND'] == "1",
          _dataHideShowFormMDakorCom[0]['share_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMDakorCom[0]['hubungan_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMDakorCom[0]['jenis_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMDakorCom[0]['no_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMDakorCom[0]['nama_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMDakorCom[0]['nama_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMDakorCom[0]['tgl_LAHIR_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMDakorCom[0]['pob_ID_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMDakorCom[0]['pob_ID_LOV_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMDakorCom[0]['jns_KELAMIN_PEMEGANG_SAHAM_IND'] == "1",
          _dataMandatoryFormMDakorCom[0]['share_PEMEGANG_SAHAM_IND'] == "1",
        );
        _setPemegangSahamLembagaCompanyModel = SetPemegangSahamLembagaCompanyModel(
          _dataHideShowFormMDakorCom[0]['jenis_LEMBAGA_PEMEGANG_SAHAM'] == "1",
          _dataHideShowFormMDakorCom[0]['nama_LEMBAGA_PEMEGANG_SAHAM'] == "1",
          _dataHideShowFormMDakorCom[0]['tgl_PENDIRIAN_PEMEGANG_SAHAM'] == "1",
          _dataHideShowFormMDakorCom[0]['npwp_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHideShowFormMDakorCom[0]['share_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryFormMDakorCom[0]['jenis_LEMBAGA_PEMEGANG_SAHAM'] == "1",
          _dataMandatoryFormMDakorCom[0]['nama_LEMBAGA_PEMEGANG_SAHAM'] == "1",
          _dataMandatoryFormMDakorCom[0]['tgl_PENDIRIAN_PEMEGANG_SAHAM'] == "1",
          _dataMandatoryFormMDakorCom[0]['npwp_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryFormMDakorCom[0]['share_PEMEGANG_SAHAM_LEMBAGA'] == "1",
        );

        //alamat
        _setAddressCompanyModel = SetAddressCompanyModel(
          _dataHideShowFormMDakorCom[0]['jenis_ALAMAT'] == "1",
          _dataHideShowFormMDakorCom[0]['alamat'] == "1",
          _dataHideShowFormMDakorCom[0]['rt'] == "1",
          _dataHideShowFormMDakorCom[0]['rw'] == "1",
          _dataHideShowFormMDakorCom[0]['kelurahan'] == "1",
          _dataHideShowFormMDakorCom[0]['kecamatan'] == "1",
          _dataHideShowFormMDakorCom[0]['kabupaten_KOTA'] == "1",
          _dataHideShowFormMDakorCom[0]['provinsi'] == "1",
          _dataHideShowFormMDakorCom[0]['kode_POS'] == "1",
          _dataHideShowFormMDakorCom[0]['telepon_AREA_1'] == "1",
          _dataHideShowFormMDakorCom[0]['telepon_1'] == "1",
          _dataHideShowFormMDakorCom[0]['telepon_AREA_2'] == "1",
          _dataHideShowFormMDakorCom[0]['telepon_2'] == "1",
          _dataHideShowFormMDakorCom[0]['fax_AREA'] == "1",
          _dataHideShowFormMDakorCom[0]['fax'] == "1",
          _dataMandatoryFormMDakorCom[0]['jenis_ALAMAT'] == "1",
          _dataMandatoryFormMDakorCom[0]['alamat'] == "1",
          _dataMandatoryFormMDakorCom[0]['rt'] == "1",
          _dataMandatoryFormMDakorCom[0]['rw'] == "1",
          _dataMandatoryFormMDakorCom[0]['kelurahan'] == "1",
          _dataMandatoryFormMDakorCom[0]['kecamatan'] == "1",
          _dataMandatoryFormMDakorCom[0]['kabupaten_KOTA'] == "1",
          _dataMandatoryFormMDakorCom[0]['provinsi'] == "1",
          _dataMandatoryFormMDakorCom[0]['kode_POS'] == "1",
          _dataMandatoryFormMDakorCom[0]['telepon_AREA_1'] == "1",
          _dataMandatoryFormMDakorCom[0]['telepon_1'] == "1",
          _dataMandatoryFormMDakorCom[0]['telepon_AREA_2'] == "1",
          _dataMandatoryFormMDakorCom[0]['telepon_2'] == "1",
          _dataMandatoryFormMDakorCom[0]['fax_AREA'] == "1",
          _dataMandatoryFormMDakorCom[0]['fax'] == "1",
        );
        _setAddressIndividuPenjaminCompany = SetAddressIndividu(
            _dataHideShowFormMDakorCom[0]['jenis_ALAMAT_IND_ALAMAT'] == "1",
            _dataHideShowFormMDakorCom[0]['alamat_IND_ALAMAT'] == "1",
            _dataHideShowFormMDakorCom[0]['rt_IND_ALAMAT'] == "1",
            _dataHideShowFormMDakorCom[0]['rw_IND_ALAMAT'] == "1",
            _dataHideShowFormMDakorCom[0]['kelurahan_IND_ALAMAT'] == "1",
            _dataHideShowFormMDakorCom[0]['kecamatan_IND_ALAMAT'] == "1",
            _dataHideShowFormMDakorCom[0]['kabupaten_KOTA_IND_ALAMAT'] == "1",
            _dataHideShowFormMDakorCom[0]['provinsi_IND_ALAMAT'] == "1",
            _dataHideShowFormMDakorCom[0]['kode_POS_IND_ALAMAT'] == "1",
            _dataHideShowFormMDakorCom[0]['telepon_AREA_IND_ALAMAT'] == "1",
            _dataHideShowFormMDakorCom[0]['telepon_IND_ALAMAT'] == "1",
            _dataMandatoryFormMDakorCom[0]['jenis_ALAMAT_IND_ALAMAT'] == "1",
            _dataMandatoryFormMDakorCom[0]['alamat_IND_ALAMAT'] == "1",
            _dataMandatoryFormMDakorCom[0]['rt_IND_ALAMAT'] == "1",
            _dataMandatoryFormMDakorCom[0]['rw_IND_ALAMAT'] == "1",
            _dataMandatoryFormMDakorCom[0]['kelurahan_IND_ALAMAT'] == "1",
            _dataMandatoryFormMDakorCom[0]['kecamatan_IND_ALAMAT'] == "1",
            _dataMandatoryFormMDakorCom[0]['kabupaten_KOTA_IND_ALAMAT'] == "1",
            _dataMandatoryFormMDakorCom[0]['provinsi_IND_ALAMAT'] == "1",
            _dataMandatoryFormMDakorCom[0]['kode_POS_IND_ALAMAT'] == "1",
            _dataMandatoryFormMDakorCom[0]['telepon_AREA_IND_ALAMAT'] == "1",
            _dataMandatoryFormMDakorCom[0]['telepon_IND_ALAMAT'] == "1"
        );
        _setAddressCompanyGuarantorModel = SetAddressCompanyModel(
          _dataHideShowFormMDakorCom[0]['jenis_ALAMAT_KELEMBAGAAN'] == "1",
          _dataHideShowFormMDakorCom[0]['alamat_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMDakorCom[0]['rt_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMDakorCom[0]['rw_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMDakorCom[0]['kelurahan_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMDakorCom[0]['kecamatan_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMDakorCom[0]['kabupaten_KOTA_KELEMBAGAAN'] == "1",
          _dataHideShowFormMDakorCom[0]['provinsi_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMDakorCom[0]['kode_POS_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMDakorCom[0]['telepon_AREA_1_KELEMBAGAAN'] == "1",
          _dataHideShowFormMDakorCom[0]['telepon_1_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMDakorCom[0]['telepon_AREA_2_KELEMBAGAAN'] == "1",
          _dataHideShowFormMDakorCom[0]['telepon_2_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMDakorCom[0]['fax_AREA_KELEMBAGAAN_ALAMAT'] == "1",
          _dataHideShowFormMDakorCom[0]['fax_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMDakorCom[0]['jenis_ALAMAT_KELEMBAGAAN'] == "1",
          _dataMandatoryFormMDakorCom[0]['alamat_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMDakorCom[0]['rt_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMDakorCom[0]['rw_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMDakorCom[0]['kelurahan_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMDakorCom[0]['kecamatan_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMDakorCom[0]['kabupaten_KOTA_KELEMBAGAAN'] == "1",
          _dataMandatoryFormMDakorCom[0]['provinsi_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMDakorCom[0]['kode_POS_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMDakorCom[0]['telepon_AREA_1_KELEMBAGAAN'] == "1",
          _dataMandatoryFormMDakorCom[0]['telepon_1_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMDakorCom[0]['telepon_AREA_2_KELEMBAGAAN'] == "1",
          _dataMandatoryFormMDakorCom[0]['telepon_2_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMDakorCom[0]['fax_AREA_KELEMBAGAAN_ALAMAT'] == "1",
          _dataMandatoryFormMDakorCom[0]['fax_KELEMBAGAAN_ALAMAT'] == "1",
        );
        _setAddressIndividuManajemenPIC = SetAddressIndividu(
            _dataHideShowFormMDakorCom[0]['jenis_ALAMAT_MANAJEMEN_PIC'] == "1",
            _dataHideShowFormMDakorCom[0]['alamat_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHideShowFormMDakorCom[0]['rt_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHideShowFormMDakorCom[0]['rw_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHideShowFormMDakorCom[0]['kelurahan_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHideShowFormMDakorCom[0]['kecamatan_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHideShowFormMDakorCom[0]['kabupaten_KOTA_MANAJEMEN_PIC'] == "1",
            _dataHideShowFormMDakorCom[0]['provinsi_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHideShowFormMDakorCom[0]['kode_POS_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataHideShowFormMDakorCom[0]['telepon_AREA_MANAJEMEN_PIC'] == "1",
            _dataHideShowFormMDakorCom[0]['telepon_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMDakorCom[0]['jenis_ALAMAT_MANAJEMEN_PIC'] == "1",
            _dataMandatoryFormMDakorCom[0]['alamat_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMDakorCom[0]['rt_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMDakorCom[0]['rw_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMDakorCom[0]['kelurahan_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMDakorCom[0]['kecamatan_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMDakorCom[0]['kabupaten_KOTA_MANAJEMEN_PIC'] == "1",
            _dataMandatoryFormMDakorCom[0]['provinsi_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMDakorCom[0]['kode_POS_MANAJEMEN_PIC_ALAMAT'] == "1",
            _dataMandatoryFormMDakorCom[0]['telepon_AREA_MANAJEMEN_PIC'] == "1",
            _dataMandatoryFormMDakorCom[0]['telepon_MANAJEMEN_PIC_ALAMAT'] == "1"
        );
        _setAddressIndividuPemegangSaham = SetAddressIndividu(
            _dataHideShowFormMDakorCom[0]['jns_ALAMAT_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMDakorCom[0]['alamat_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMDakorCom[0]['rt_PEMEGANG_SAHAM_IND_ALAMAT']== "1",
            _dataHideShowFormMDakorCom[0]['rw_PEMEGANG_SAHAM_IND_ALAMAT']== "1",
            _dataHideShowFormMDakorCom[0]['kelurahan_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMDakorCom[0]['kecamatan_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMDakorCom[0]['kabkot_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMDakorCom[0]['provinsi_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMDakorCom[0]['kode_POS_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMDakorCom[0]['phone_AREA_PEMEGANG_SAHAM_IND']== "1",
            _dataHideShowFormMDakorCom[0]['phone_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMDakorCom[0]['jns_ALAMAT_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMDakorCom[0]['alamat_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMDakorCom[0]['rt_PEMEGANG_SAHAM_IND_ALAMAT']== "1",
            _dataMandatoryFormMDakorCom[0]['rw_PEMEGANG_SAHAM_IND_ALAMAT']== "1",
            _dataMandatoryFormMDakorCom[0]['kelurahan_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMDakorCom[0]['kecamatan_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMDakorCom[0]['kabkot_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMDakorCom[0]['provinsi_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMDakorCom[0]['kode_POS_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMDakorCom[0]['phone_AREA_PEMEGANG_SAHAM_IND']== "1",
            _dataMandatoryFormMDakorCom[0]['phone_PEMEGANG_SAHAM_IND'] == "1"
        );
        _setAddressSahamLembagaCompanyModel = SetAddressCompanyModel(
          _dataHideShowFormMDakorCom[0]['jns_ALAMAT_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHideShowFormMDakorCom[0]['addr_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHideShowFormMDakorCom[0]['rt_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
          _dataHideShowFormMDakorCom[0]['rw_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
          _dataHideShowFormMDakorCom[0]['kelurahan_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHideShowFormMDakorCom[0]['kecamatan_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHideShowFormMDakorCom[0]['kabkota_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHideShowFormMDakorCom[0]['prov_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHideShowFormMDakorCom[0]['kode_POS_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHideShowFormMDakorCom[0]['phonearea1_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHideShowFormMDakorCom[0]['phone1_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHideShowFormMDakorCom[0]['phonearea2_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataHideShowFormMDakorCom[0]['phone2_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHideShowFormMDakorCom[0]['faxarea_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataHideShowFormMDakorCom[0]['fax_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
          _dataMandatoryFormMDakorCom[0]['jns_ALAMAT_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryFormMDakorCom[0]['addr_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryFormMDakorCom[0]['rt_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
          _dataMandatoryFormMDakorCom[0]['rw_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
          _dataMandatoryFormMDakorCom[0]['kelurahan_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryFormMDakorCom[0]['kecamatan_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryFormMDakorCom[0]['kabkota_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryFormMDakorCom[0]['prov_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryFormMDakorCom[0]['kode_POS_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryFormMDakorCom[0]['phonearea1_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryFormMDakorCom[0]['phone1_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryFormMDakorCom[0]['phonearea2_PEMEGANGSHM_LEMBAGA'] == "1",
          _dataMandatoryFormMDakorCom[0]['phone2_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryFormMDakorCom[0]['faxarea_PEMEGANG_SAHAM_LEMBAGA'] == "1",
          _dataMandatoryFormMDakorCom[0]['fax_PEMEGANG_SAHAM_KELEMBAGAAN'] == "1",
        );
      }
    }
  }
}