import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/object_model.dart';
import 'package:ad1ms2_dev/models/product_lme_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/resource/get_set_vme.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SearchObjectChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  bool _loadData = false;
  TextEditingController _controllerSearch = TextEditingController();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<ObjectModel> _listObjectModel = [];
  List<ObjectModel> _listObjectTemp = [];
  List<ObjectModel> get listObjectModel => _listObjectModel;
  DbHelper _dbHelper = DbHelper();
  List<ProductLMEModel> _listProductLME = ProductLMEList().productLMEList;
  String _objectFromVoucher = "";

  UnmodifiableListView<ProductLMEModel> get listProductLME {
    return UnmodifiableListView(this._listProductLME);
  }

  UnmodifiableListView<ObjectModel> get listObjectTemp {
    return UnmodifiableListView(this._listObjectTemp);
  }

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  void getObject(BuildContext context, String object, String unitColla) async{
    this._listObjectModel.clear();
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    List _dataCL = await _dbHelper.selectMS2LME();
    var _providerObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);

    try{
      if(_preferences.getString("jenis_penawaran") == "001") await getReferenceNumberCL(context);

      var _body = jsonEncode({
        "P_KODE": object
      });

      var storage = FlutterSecureStorage();
      String _fieldObjek = await storage.read(key: "FieldObjek");
      final _response = await _http.post(
        "${BaseUrl.urlGeneral}$_fieldObjek",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      );
      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);

        List _data = _result['data'];
        if(_data.isEmpty){
          showSnackBar("Object not found");
          loadData = false;
        }
        else{
          if(unitColla == "1") {
            // Inf Unit Object
            for(int i=0; i <_data.length; i++){
              if(_preferences.getString("jenis_penawaran") == "001") {
                if(_objectFromVoucher == _data[i]['KODE']) {
                  this._listObjectModel.add(
                      ObjectModel(_data[i]['KODE'], _data[i]['DESKRIPSI'])
                  );
                }
              } else {
                this._listObjectModel.add(
                    ObjectModel(_data[i]['KODE'], _data[i]['DESKRIPSI'])
                );
              }
            }
          } else {
            // Inf Collateral Automotives
            for(int i=0; i <_data.length; i++){
              this._listObjectModel.add(
                  ObjectModel(_data[i]['KODE'], _data[i]['DESKRIPSI'])
              );
            }
          }
          loadData = false;
        }
      }
      else{
        showSnackBar("Error response status ${_response.statusCode}");
        this._loadData = false;
      }
    }
    catch(e){
      showSnackBar("Error ${e.toString()}");
      this._loadData = false;
    }

    notifyListeners();
  }

  void searchObject(String query) {
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    } else {
      _listObjectTemp.clear();
      if (query.isEmpty) {
        return;
      }

      _listObjectModel.forEach((dataGroupObject) {
        if (dataGroupObject.id.contains(query) || dataGroupObject.name.contains(query)) {
          _listObjectTemp.add(dataGroupObject);
        }
      });
    }
    notifyListeners();
  }

  Future<void> getReferenceNumberCL(BuildContext context) async{
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    List _dataCL = await _dbHelper.selectMS2LME();
    if(_preferences.getString("jenis_penawaran") == "001") {
      var _date = dateFormat3.format(DateTime.now());

      loadData = true;
      final ioc = new HttpClient();
      ioc.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;

      final _http = IOClient(ioc);

      var _body = jsonEncode({
        "oid": _dataCL[0]['oid'],
        "source_reff_id": "MS2${_preferences.getString("username")}-${_dataCL[0]['oid']}",
        "source_channel_id": "REVAMP",
        "req_date": _date.toString()
      });
      print("body inq voucher: $_body");
      String _creditLimitinqVoucher = await storage.read(key: "CreditLimitInqVoucher");
      try {
        final _response = await _http.post(
            "${BaseUrl.urlLME}$_creditLimitinqVoucher",
            body: _body,
            headers: {
              "Content-Type":"application/json", "Authorization":"bearer $token"
            }
        ).timeout(Duration(seconds: 10));

        if(_response.statusCode == 200){
          final _result = jsonDecode(_response.body);
          print("result inq voucher: $_result");
          if(_result['response_code'] == "00") {
            if(_result.isNotEmpty){
              for(int i=0; i < _result['list_voucher'].length; i++) {
                if(_dataCL[0]['voucher_code'] == _result['list_voucher'][i]['voucher_code']) {
                  _objectFromVoucher = _result['list_voucher'][i]['product_type'];
                }
              }
              loadData = false;
            }
            else{
              showSnackBar("List Voucher tidak ditemukan");
              loadData = false;
            }
          } else {
            showSnackBar("${_result['response_desc']}");
            loadData = false;
          }
        }
        else{
          showSnackBar("Error get inq voucher response ${_response.statusCode}");
          loadData = false;
        }
      }
      on TimeoutException catch(_){
        showSnackBar("Request Timeout");
        this._loadData = false;
      }
      catch(e){
        showSnackBar("Error $e");
        this._loadData = false;
      }
    }
    notifyListeners();
  }

  void clearSearchTemp() {
    _listObjectTemp.clear();
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }
}
