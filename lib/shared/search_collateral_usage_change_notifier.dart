import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/models/collateral_usage_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';
import 'change_notifier_app/information_object_unit_change_notifier.dart';
import 'constants.dart';
import 'form_m_foto_change_notif.dart';

class SearchCollateralUsageChangeNotifier with ChangeNotifier{
  bool _showClear = false;
  TextEditingController _controllerSearch = TextEditingController();
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  List<CollateralUsageModel> _listCollateralUsage = [
    // CollateralUsageModel("001", "PERSONAL"),
    // CollateralUsageModel("002", "Passenger"),
    // CollateralUsageModel("003", "PERSONAL/B2B/B2C"),
    // CollateralUsageModel("004", "B2C"),
    // CollateralUsageModel("005", "Commercial"),
  ];

  List<CollateralUsageModel> _listCollateralUsageTemp = [];

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<CollateralUsageModel> get listCollateralUsage {
    return UnmodifiableListView(this._listCollateralUsage);
  }

  UnmodifiableListView<CollateralUsageModel> get listCollateralUsageTemp {
    return UnmodifiableListView(this._listCollateralUsageTemp);
  }

  void getObjectPurpose(BuildContext context) async{
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
    var _providerObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    this._listCollateralUsage.clear();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var _body = jsonEncode({
      "P_OBJECT_GROUP_ID": "${_providerObjectUnit.groupObjectSelected.KODE}",
      "P_OBJECT_ID": "${_providerObjectUnit.objectSelected.id}",
      "P_OJK_BUSS_DETAIL_ID" : _preferences.getString("cust_type") == "COM" ? "${_providerObjectUnit.businessActivitiesTypeModelSelected.id}" : "${_providerFoto.jenisKegiatanUsahaSelected.id}",
      "P_OJK_BUSS_ID" : _preferences.getString("cust_type") == "COM" ? "${_providerObjectUnit.businessActivitiesModelSelected.id}" : "${_providerFoto.kegiatanUsahaSelected.id}"
    });
    print("body tujuan colla: $_body");

    var storage = FlutterSecureStorage();
    String _fieldTujuanPenggunaanObjek = await storage.read(key: "FieldTujuanPenggunaanObjek");
    final _response = await _http.post(
        "${BaseUrl.urlGeneral}$_fieldTujuanPenggunaanObjek",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      final _data = _result['data'];
      if(_data.isEmpty){
        showSnackBar("Group Object not found");
        loadData = false;
      }
      else{
        for(int i=0; i <_data.length; i++){
          this._listCollateralUsage.add(CollateralUsageModel(_data[i]['KODE'], _data[i]['DESKRIPSI'])
          );
        }
        loadData = false;
      }
    }
    else{
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  void searchGroupObject(String query) {
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    } else {
      _listCollateralUsageTemp.clear();
      if (query.isEmpty) {
        return;
      }

      _listCollateralUsage.forEach((dataGroupObject) {
        if (dataGroupObject.id.contains(query) || dataGroupObject.name.contains(query)) {
          _listCollateralUsageTemp.add(dataGroupObject);
        }
      });
    }
    notifyListeners();
  }

  void clearSearchTemp() {
    _listCollateralUsage.clear();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }
}