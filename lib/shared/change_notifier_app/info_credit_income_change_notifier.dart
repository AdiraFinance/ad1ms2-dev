import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/ms2_appl_object_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_application_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_income_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_individu_change_notifier/form_m_parent_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/regex_input_formatter.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../form_m_company_pendapatan_change_notif.dart';
import '../format_currency.dart';

class InfoCreditIncomeChangeNotifier with ChangeNotifier{
  bool _autoValidate = false;
  bool _loadData = false;
  bool _loadDataDSR = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController _controllerDebtComparison = TextEditingController();
  TextEditingController _controllerDSC = TextEditingController();
  TextEditingController _controllerIncomeComparison = TextEditingController();
  TextEditingController _controllerIRR = TextEditingController();
  DbHelper _dbHelper = DbHelper();
  RegExInputFormatter _amountValidator = RegExInputFormatter.withRegex('^[0-9]{0,9}(\\.[0-9]{0,2})?\$');
  final storage = FlutterSecureStorage();
  bool _flag = false;
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  String _custType;
  String _lastKnownState;
  FormatCurrency _formatCurrency = FormatCurrency();
  bool _isDisablePACIAAOSCONA = false;

  bool _isDebtComparisonDakor = false;
  bool _isIncomeComparisonDakor = false;
  bool _isDSCDakor = false;
  bool _isIRRDakor = false;

  RegExInputFormatter get amountValidator => _amountValidator;

  GlobalKey<FormState> get keyForm => _key;

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  bool get loadDataDSR => _loadDataDSR;

  set loadDataDSR(bool value) {
    this._loadDataDSR = value;
  }

  bool get flag => _flag;

  set flag(bool value) {
      this._flag = value;
      notifyListeners();
  }


  bool get isDisablePACIAAOSCONA => _isDisablePACIAAOSCONA;

  set isDisablePACIAAOSCONA(bool value) {
    this._isDisablePACIAAOSCONA = value;
    notifyListeners();
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  set amountValidator(RegExInputFormatter value) {
    this._amountValidator = value;
    notifyListeners();
  }

  TextEditingController get controllerDebtComparison => _controllerDebtComparison;

  TextEditingController get controllerDSC => _controllerDSC;

  TextEditingController get controllerIncomeComparison => _controllerIncomeComparison;

  TextEditingController get controllerIRR => _controllerIRR;

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  String get custType => _custType;

  set custType(String value) {
      this._custType = value;
      notifyListeners();
  }

  String get lastKnownState => _lastKnownState;

  set lastKnownState(String value) {
      this._lastKnownState = value;
      notifyListeners();
  }

  FormatCurrency get formatCurrency => _formatCurrency;

  void check(BuildContext context) {
    if(this._controllerDebtComparison.text != "" && this._controllerIncomeComparison.text != "" && this._controllerDSC.text != "" && this._controllerIRR.text != "") {
      flag = true;
    }
      // final _form = _key.currentState;
      // if (_form.validate()) {
      //     flag = true;
      //     autoValidate = false;
    Navigator.pop(context);
    checkDataDakor();
      // } else {
      //     flag = false;
      //     autoValidate = true;
      // }
  }

  Future<bool> onBackPress() async{
    if(this._controllerDebtComparison.text != "" && this._controllerIncomeComparison.text != "" && this._controllerDSC.text != "" && this._controllerIRR.text != "") {
      flag = true;
    }
    checkDataDakor();
      // final _form = _key.currentState;
      // if (_form.validate()) {
      //     flag = true;
      //     autoValidate = false;
      // } else {
      //     flag = false;
      //     autoValidate = true;
      // }
      return true;
  }

  // List mappingCustomerIncomeWiraswastaBelumMenikah(BuildContext context) {
  //   var _providerIncome = Provider.of<FormMIncomeChangeNotifier>(context, listen: false);
  //
  //   // WIraswasta Belum Menikah
  //   var _customerIncome = [];
  //   for(int i=0; i < 12; i++) {
  //     _customerIncome.add(
  //       {
  //         "customerIncomeID": "NEW",
  //         "incomeFrmlType": i == 0
  //             ? "S" : i == 1
  //             ? "+" : i == 2
  //             ? "=" : i == 3
  //             ? "-" : i == 4
  //             ? "=" : i == 5
  //             ? "-" : i == 6
  //             ? "-" : i == 7
  //             ? "=" : i == 8
  //             ? "-" : i == 9
  //             ? "=" : i == 10
  //             ? "-" : i == 11
  //             ? "="
  //             : "",
  //         "incomeType": i == 0
  //             ? "001" : i == 1
  //             ? "002" : i == 2
  //             ? "003" : i == 3
  //             ? "004" : i == 4
  //             ? "005" : i == 5
  //             ? "006" : i == 6
  //             ? "007" : i == 7
  //             ? "008" : i == 8
  //             ? "009" : i == 9
  //             ? "010" : i == 10
  //             ? "011" : i == 11
  //             ? "017"
  //             : "",
  //         "incomeValue": i == 0
  //             ? _providerIncome.controllerMonthlyIncome.text : i == 1
  //             ? _providerIncome.controllerOtherIncome.text : i == 2
  //             ? _providerIncome.controllerTotalIncome.text : i == 3
  //             ? _providerIncome.controllerCostOfGoodsSold.text : i == 4
  //             ? _providerIncome.controllerGrossProfit.text : i == 5
  //             ? _providerIncome.controllerOperatingCosts.text : i == 6
  //             ? _providerIncome.controllerOtherCosts.text : i == 7
  //             ? _providerIncome.controllerNetProfitBeforeTax.text : i == 8
  //             ? _providerIncome.controllerTax.text : i == 9
  //             ? _providerIncome.controllerNetProfitAfterTax.text : i == 10
  //             ? _providerIncome.controllerCostOfLiving.text : i == 11
  //             ? _providerIncome.controllerRestIncome.text
  //             : _providerIncome.controllerOtherInstallments.text,
  //         "dataStatus": "ACTIVE",
  //         "creationalSpecification": {
  //           "createdAt": 1605192713593,
  //           "createdBy": "SYSTEM",
  //           "modifiedAt": null,
  //           "modifiedBy": null
  //         }
  //       });
  //   }
  //
  //   return _customerIncome;
  // }
  //
  // List mappingCustomerIncomeWiraswastaMenikah(BuildContext context) {
  //   var _providerIncome = Provider.of<FormMIncomeChangeNotifier>(context, listen: false);
  //
  //   // WIraswasta Menikah
  //   var _customerIncome = [];
  //   for(int i=0; i < 13; i++) {
  //     _customerIncome.add(
  //       {
  //         "customerIncomeID": "NEW",
  //         "incomeFrmlType": i == 0
  //           ? "S" : i == 1
  //           ? "+" : i == 2
  //           ? "=" : i == 3
  //           ? "-" : i == 4
  //           ? "=" : i == 5
  //           ? "-" : i == 6
  //           ? "-" : i == 7
  //           ? "=" : i == 8
  //           ? "-" : i == 9
  //           ? "=" : i == 10
  //           ? "-" : i == 11
  //           ? "=" : i == 12
  //           ? ""
  //           : "",
  //         "incomeType": i == 0
  //           ? "001" : i == 1
  //           ? "002" : i == 2
  //           ? "003" : i == 3
  //           ? "004" : i == 4
  //           ? "005" : i == 5
  //           ? "006" : i == 6
  //           ? "007" : i == 7
  //           ? "008" : i == 8
  //           ? "009" : i == 9
  //           ? "010" : i == 10
  //           ? "011" : i == 11
  //           ? "017" : i == 12
  //           ? ""
  //           : "",
  //         "incomeValue": i == 0
  //           ? _providerIncome.controllerMonthlyIncome.text : i == 1
  //           ? _providerIncome.controllerOtherIncome.text : i == 2
  //           ? _providerIncome.controllerTotalIncome.text : i == 3
  //           ? _providerIncome.controllerCostOfGoodsSold.text : i == 4
  //           ? _providerIncome.controllerGrossProfit.text : i == 5
  //           ? _providerIncome.controllerOperatingCosts.text : i == 6
  //           ? _providerIncome.controllerOtherCosts.text : i == 7
  //           ? _providerIncome.controllerNetProfitBeforeTax.text : i == 8
  //           ? _providerIncome.controllerTax.text : i == 9
  //           ? _providerIncome.controllerNetProfitAfterTax.text : i == 10
  //           ? _providerIncome.controllerCostOfLiving.text : i == 11
  //           ? _providerIncome.controllerRestIncome.text : i == 12
  //           ? _providerIncome.controllerSpouseIncome.text
  //           : _providerIncome.controllerOtherInstallments.text,
  //         "dataStatus": "ACTIVE",
  //         "creationalSpecification": {
  //           "createdAt": 1605192713593,
  //           "createdBy": "SYSTEM",
  //           "modifiedAt": null,
  //           "modifiedBy": null
  //         }
  //       }
  //     );
  //   }
  //
  //   return _customerIncome;
  // }
  //
  // List mappingCustomerIncomeProfesionalBelumMenikah(BuildContext context) {
  //   var _providerIncome = Provider.of<FormMIncomeChangeNotifier>(context, listen: false);
  //
  //   var _customerIncome = [];
  //   // Profesional Belum Menikah
  //   for(int i=0; i < 5; i++) {
  //     _customerIncome.add(
  //       {
  //         "customerIncomeID": "NEW",
  //         "incomeFrmlType": i == 0
  //           ? "S" : i == 1
  //           ? "+" : i == 2
  //           ? "=" : i == 3
  //           ? "-" : i == 4
  //           ? "="
  //           : "",
  //         "incomeType": i == 0
  //           ? "001" : i == 1
  //           ? "002" : i == 2
  //           ? "003" : i == 3
  //           ? "011" : i == 4
  //           ? "017"
  //           : "",
  //         "incomeValue": i == 0
  //           ? _providerIncome.controllerIncome.text : i == 1
  //           ? _providerIncome.controllerOtherIncome.text : i == 2
  //           ? _providerIncome.controllerTotalIncome.text : i == 3
  //           ? _providerIncome.controllerCostOfLiving.text : i == 4
  //           ? _providerIncome.controllerRestIncome.text
  //           : _providerIncome.controllerOtherInstallments.text,
  //         "dataStatus": "ACTIVE",
  //         "creationalSpecification": {
  //           "createdAt": 1605192713593,
  //           "createdBy": "SYSTEM",
  //           "modifiedAt": null,
  //           "modifiedBy": null
  //         }
  //       }
  //     );
  //   }
  //
  //   return _customerIncome;
  // }
  //
  // List mappingCustomerIncomeProfesionalMenikah(BuildContext context) {
  //   var _providerIncome = Provider.of<FormMIncomeChangeNotifier>(context, listen: false);
  //
  //   var _customerIncome = [];
  //   // Profesional Menikah
  //   for(int i=0; i < 6; i++) {
  //     _customerIncome.add(
  //       {
  //         "customerIncomeID": "NEW",
  //         "incomeFrmlType": i == 0
  //           ? "S" : i == 1
  //           ? "+" : i == 2
  //           ? "+" : i == 3
  //           ? "=" : i == 4
  //           ? "-" : i == 5
  //           ? "="
  //           : "",
  //         "incomeType": i == 0
  //           ? "001" : i == 1
  //           ? "002" : i == 2
  //           ? "?" : i == 3
  //           ? "003" : i == 4
  //           ? "011" : i == 5
  //           ? "017"
  //           : "",
  //         "incomeValue": i == 0
  //           ? _providerIncome.controllerIncome.text : i == 1
  //           ? _providerIncome.controllerSpouseIncome.text : i == 2
  //           ? _providerIncome.controllerOtherIncome.text : i == 3
  //           ? _providerIncome.controllerTotalIncome.text : i == 4
  //           ? _providerIncome.controllerCostOfLiving.text : i == 5
  //           ? _providerIncome.controllerRestIncome.text
  //           : _providerIncome.controllerOtherInstallments.text,
  //         "dataStatus": "ACTIVE",
  //         "creationalSpecification": {
  //           "createdAt": 1605192713593,
  //           "createdBy": "SYSTEM",
  //           "modifiedAt": null,
  //           "modifiedBy": null
  //         }
  //       }
  //     );
  //   }
  //
  //   return _customerIncome;
  // }
  //
  // List mappingCustomerIncomeCompany (BuildContext context) {
  //   var _providerIncome = Provider.of<FormMCompanyPendapatanChangeNotifier>(context, listen: false);
  //
  //   var _customerIncome = [];
  //   for(int i=0; i < 10; i++) {
  //     _customerIncome.add(
  //         {
  //           "customerIncomeID": "NEW",
  //           "incomeFrmlType": i == 0
  //               ? "S" : i == 1
  //               ? "+" : i == 2
  //               ? "=" : i == 3
  //               ? "-" : i == 4
  //               ? "=" : i == 5
  //               ? "-" : i == 6
  //               ? "-" : i == 7
  //               ? "=" : i == 8
  //               ? "-" : i == 9
  //               ? "="
  //               : "",
  //           "incomeType": i == 0
  //               ? "001" : i == 1
  //               ? "002" : i == 2
  //               ? "003" : i == 3
  //               ? "004" : i == 4
  //               ? "005" : i == 5
  //               ? "006" : i == 6
  //               ? "007" : i == 7
  //               ? "008" : i == 8
  //               ? "" : i == 9
  //               ? ""
  //               : "",
  //           "incomeValue": i == 0
  //               ? _providerIncome.controllerMonthlyIncome.text : i == 1
  //               ? _providerIncome.controllerOtherIncome.text : i == 2
  //               ? _providerIncome.controllerTotalIncome.text : i == 3
  //               ? _providerIncome.controllerCostOfRevenue.text : i == 4
  //               ? _providerIncome.controllerGrossProfit.text : i == 5
  //               ? _providerIncome.controllerOperatingCosts.text : i == 6
  //               ? _providerIncome.controllerOtherCosts.text : i == 7
  //               ? _providerIncome.controllerNetProfitBeforeTax.text : i == 8
  //               ? _providerIncome.controllerTax.text : i == 9
  //               ? _providerIncome.controllerNetProfitAfterTax.text
  //               : _providerIncome.controllerOtherInstallments.text,
  //           "dataStatus": "ACTIVE",
  //           "creationalSpecification": {
  //             "createdAt": 1605192713593,
  //             "createdBy": "SYSTEM",
  //             "modifiedAt": null,
  //             "modifiedBy": null
  //           }
  //         }
  //     );
  //   }
  //
  //   return _customerIncome;
  // }

  void setPreference() async {
      SharedPreferences _preference = await SharedPreferences.getInstance();
      this._custType = _preference.getString("cust_type");
      this._lastKnownState = _preference.getString("last_known_state");
      if(this._lastKnownState == "AOS" || this._lastKnownState == "IA") {
        this._isDisablePACIAAOSCONA = true;
      }
  }

  Future<void> getDSR(BuildContext context) async {
    DateTime _timeStartValidate = DateTime.now();
    var _providerParentIndividu = Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false);

    // Form IDE Individu
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
    var _providerIncome = Provider.of<FormMIncomeChangeNotifier>(context, listen: false);
    var _providerIncomeCom = Provider.of<FormMCompanyPendapatanChangeNotifier>(context, listen: false);

    // Form App
    var _providerApp = Provider.of<InfoAppChangeNotifier>(context, listen: false);
    var _providerInfCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false);

    SharedPreferences _preferences = await SharedPreferences.getInstance();
    loadData = true;
    this._loadDataDSR = true;
    try{
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);

      var _customerIncomes = [];
      // if(_preferences.getString("cust_type") == "PER") {
      //   if(_providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07") {
      //     if(_providerInfoNasabah.maritalStatusSelected.id != "01") {
      //       for(int i=0; i < 13; i++) {
      //         _customerIncomes.add(
      //             {
      //               "customerIncomeID": _preferences.getString("last_known_state") != "IDE" ? _preferences.getString("customerIncomeID") : "NEW",
      //               "incomeFrmlType": i == 0
      //                   ? "S" : i == 1
      //                   ? "+" : i == 2
      //                   ? "=" : i == 3
      //                   ? "-" : i == 4
      //                   ? "=" : i == 5
      //                   ? "-" : i == 6
      //                   ? "-" : i == 7
      //                   ? "=" : i == 8
      //                   ? "-" : i == 9
      //                   ? "=" : i == 10
      //                   ? "-" : i == 11
      //                   ? "="
      //                   : "N",
      //               "incomeType": i == 0
      //                   ? "001" : i == 1
      //                   ? "002" : i == 2
      //                   ? "003" : i == 3
      //                   ? "004" : i == 4
      //                   ? "005" : i == 5
      //                   ? "006" : i == 6
      //                   ? "007" : i == 7
      //                   ? "008" : i == 8
      //                   ? "009" : i == 9
      //                   ? "010" : i == 10
      //                   ? "011" : i == 11
      //                   ? "017"
      //                   : "013",
      //               "incomeValue": i == 0
      //                   ? double.parse(_providerIncome.controllerMonthlyIncome.text.replaceAll(",", "")) : i == 1
      //                   ? double.parse(_providerIncome.controllerOtherIncome.text.replaceAll(",", "")) : i == 2
      //                   ? double.parse(_providerIncome.controllerTotalIncome.text.replaceAll(",", "")) : i == 3
      //                   ? double.parse(_providerIncome.controllerCostOfGoodsSold.text.replaceAll(",", "")) : i == 4
      //                   ? double.parse(_providerIncome.controllerGrossProfit.text.replaceAll(",", "")) : i == 5
      //                   ? double.parse(_providerIncome.controllerOperatingCosts.text.replaceAll(",", "")) : i == 6
      //                   ? double.parse(_providerIncome.controllerOtherCosts.text.replaceAll(",", "")) : i == 7
      //                   ? double.parse(_providerIncome.controllerNetProfitBeforeTax.text.replaceAll(",", "")) : i == 8
      //                   ? double.parse(_providerIncome.controllerTax.text.replaceAll(",", "")) : i == 9
      //                   ? double.parse(_providerIncome.controllerNetProfitAfterTax.text.replaceAll(",", "")) : i == 10
      //                   ? double.parse(_providerIncome.controllerCostOfLiving.text.replaceAll(",", "")) : i == 11
      //                   ? double.parse(_providerIncome.controllerRestIncome.text.replaceAll(",", ""))
      //                   : double.parse(_providerIncome.controllerOtherInstallments.text.replaceAll(",", "")),
      //               "dataStatus": "ACTIVE",
      //               "creationalSpecification": {
      //                 "createdAt": formatDateValidateAndSubmit(DateTime.now()),
      //                 "createdBy": _preferences.getString("username"),
      //                 "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
      //                 "modifiedBy": _preferences.getString("username"),
      //               },
      //             }
      //         );
      //       }
      //     }
      //     else {
      //       for(int i=0; i < 14; i++) {
      //         _customerIncomes.add(
      //             {
      //               "customerIncomeID":_preferences.getString("last_known_state") != "IDE" ? _preferences.getString("customerIncomeID") : "NEW",
      //               "incomeFrmlType": i == 0
      //                   ? "S" : i == 1
      //                   ? "+" : i == 2
      //                   ? "=" : i == 3
      //                   ? "-" : i == 4
      //                   ? "=" : i == 5
      //                   ? "-" : i == 6
      //                   ? "-" : i == 7
      //                   ? "=" : i == 8
      //                   ? "-" : i == 9
      //                   ? "=" : i == 10
      //                   ? "-" : i == 11
      //                   ? "=" : i == 12
      //                   ? "N"
      //                   : "N",
      //               "incomeType": i == 0
      //                   ? "001" : i == 1
      //                   ? "002" : i == 2
      //                   ? "003" : i == 3
      //                   ? "004" : i == 4
      //                   ? "005" : i == 5
      //                   ? "006" : i == 6
      //                   ? "007" : i == 7
      //                   ? "008" : i == 8
      //                   ? "009" : i == 9
      //                   ? "010" : i == 10
      //                   ? "011" : i == 11
      //                   ? "017" : i == 12
      //                   ? "012"
      //                   : "013",
      //               "incomeValue": i == 0
      //                   ? double.parse(_providerIncome.controllerMonthlyIncome.text.replaceAll(",", "")) : i == 1
      //                   ? double.parse(_providerIncome.controllerOtherIncome.text.replaceAll(",", "")) : i == 2
      //                   ? double.parse(_providerIncome.controllerTotalIncome.text.replaceAll(",", "")) : i == 3
      //                   ? double.parse(_providerIncome.controllerCostOfGoodsSold.text.replaceAll(",", "")) : i == 4
      //                   ? double.parse(_providerIncome.controllerGrossProfit.text.replaceAll(",", "")) : i == 5
      //                   ? double.parse(_providerIncome.controllerOperatingCosts.text.replaceAll(",", "")) : i == 6
      //                   ? double.parse(_providerIncome.controllerOtherCosts.text.replaceAll(",", "")) : i == 7
      //                   ? double.parse(_providerIncome.controllerNetProfitBeforeTax.text.replaceAll(",", "")) : i == 8
      //                   ? double.parse(_providerIncome.controllerTax.text.replaceAll(",", "")) : i == 9
      //                   ? double.parse(_providerIncome.controllerNetProfitAfterTax.text.replaceAll(",", "")) : i == 10
      //                   ? double.parse(_providerIncome.controllerCostOfLiving.text.replaceAll(",", "")) : i == 11
      //                   ? double.parse(_providerIncome.controllerRestIncome.text.replaceAll(",", "")) : i == 12
      //                   ? double.parse(_providerIncome.controllerSpouseIncome.text.replaceAll(",", ""))
      //                   : double.parse(_providerIncome.controllerOtherInstallments.text.replaceAll(",", "")),
      //               "dataStatus": "ACTIVE",
      //               "creationalSpecification": {
      //                 "createdAt": formatDateValidateAndSubmit(DateTime.now()),
      //                 "createdBy": _preferences.getString("username"),
      //                 "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
      //                 "modifiedBy": _preferences.getString("username"),
      //               },
      //             }
      //         );
      //       }
      //     }
      //   }
      //   else {
      //     if(_providerInfoNasabah.maritalStatusSelected.id != "01") {
      //       for(int i=0; i < 6; i++) {
      //         _customerIncomes.add(
      //             {
      //               "customerIncomeID": _preferences.getString("last_known_state") != "IDE" ? _preferences.getString("customerIncomeID") : "NEW",
      //               "incomeFrmlType": i == 0
      //                   ? "S" : i == 1
      //                   ? "+" : i == 2
      //                   ? "=" : i == 3
      //                   ? "-" : i == 4
      //                   ? "="
      //                   : "N",
      //               "incomeType": i == 0
      //                   ? "016" : i == 1
      //                   ? "002" : i == 2
      //                   ? "003" : i == 3
      //                   ? "011" : i == 4
      //                   ? "017"
      //                   : "013",
      //               "incomeValue": i == 0
      //                   ? double.parse(_providerIncome.controllerMonthlyIncome.text.replaceAll(",", "")) : i == 1
      //                   ? double.parse(_providerIncome.controllerOtherIncome.text.replaceAll(",", "")) : i == 2
      //                   ? double.parse(_providerIncome.controllerTotalIncome.text.replaceAll(",", "")) : i == 3
      //                   ? double.parse(_providerIncome.controllerCostOfLiving.text.replaceAll(",", "")) : i == 4
      //                   ? double.parse(_providerIncome.controllerRestIncome.text.replaceAll(",", ""))
      //                   : double.parse(_providerIncome.controllerOtherInstallments.text.replaceAll(",", "")),
      //               "dataStatus": "ACTIVE",
      //               "creationalSpecification": {
      //                 "createdAt": formatDateValidateAndSubmit(DateTime.now()),
      //                 "createdBy": _preferences.getString("username"),
      //                 "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
      //                 "modifiedBy": _preferences.getString("username"),
      //               },
      //             }
      //         );
      //       }
      //     }
      //     else {
      //       for(int i=0; i < 7; i++) {
      //         _customerIncomes.add(
      //             {
      //               "customerIncomeID": _preferences.getString("last_known_state") != "IDE" ? _preferences.getString("customerIncomeID") : "NEW",
      //               "incomeFrmlType": i == 0
      //                   ? "S" : i == 1
      //                   ? "+" : i == 2
      //                   ? "+" : i == 3
      //                   ? "=" : i == 4
      //                   ? "-" : i == 5
      //                   ? "="
      //                   : "N",
      //               "incomeType": i == 0
      //                   ? "016" : i == 1
      //                   ? "012" : i == 2
      //                   ? "002" : i == 3
      //                   ? "003" : i == 4
      //                   ? "011" : i == 5
      //                   ? "017"
      //                   : "013",
      //               "incomeValue": i == 0
      //                   ? _providerIncome.controllerMonthlyIncome.text.replaceAll(",", "") : i == 1
      //                   ? _providerIncome.controllerSpouseIncome.text.replaceAll(",", "") : i == 2
      //                   ? _providerIncome.controllerOtherIncome.text.replaceAll(",", "") : i == 3
      //                   ? _providerIncome.controllerTotalIncome.text.replaceAll(",", "") : i == 4
      //                   ? _providerIncome.controllerCostOfLiving.text.replaceAll(",", "") : i == 5
      //                   ? _providerIncome.controllerRestIncome.text.replaceAll(",", "")
      //                   : _providerIncome.controllerOtherInstallments.text.replaceAll(",", ""),
      //               "dataStatus": "ACTIVE",
      //               "creationalSpecification": {
      //                 "createdAt": formatDateValidateAndSubmit(DateTime.now()),
      //                 "createdBy": _preferences.getString("username"),
      //                 "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
      //                 "modifiedBy": _preferences.getString("username"),
      //               },
      //             }
      //         );
      //       }
      //     }
      //   }
      // }
      // // adjut di company untukk limit looping
      // else {
      //   for(int i=0; i < 11; i++) {
      //     _customerIncomes.add(
      //         {
      //           "customerIncomeID": _preferences.getString("last_known_state") != "IDE" ? _preferences.getString("customerIncomeID") : "NEW",
      //           "incomeFrmlType": i == 0
      //               ? "S" : i == 1
      //               ? "+" : i == 2
      //               ? "=" : i == 3
      //               ? "-" : i == 4
      //               ? "=" : i == 5
      //               ? "-" : i == 6
      //               ? "-" : i == 7
      //               ? "=" : i == 8
      //               ? "-" : i == 9
      //               ? "="
      //               : "N",
      //           "incomeType": i == 0
      //               ? "016" : i == 1
      //               ? "002" : i == 2
      //               ? "003" : i == 3
      //               ? "004" : i == 4
      //               ? "005" : i == 5
      //               ? "006" : i == 6
      //               ? "007" : i == 7
      //               ? "008" : i == 8
      //               ? "009" : i == 9
      //               ? "010"
      //               : "013",
      //           "incomeValue": i == 0
      //               ? double.parse(_providerIncomeCompany.controllerMonthlyIncome.text.replaceAll(",", "")) : i == 1
      //               ? double.parse(_providerIncomeCompany.controllerOtherIncome.text.replaceAll(",", "")) : i == 2
      //               ? double.parse(_providerIncomeCompany.controllerTotalIncome.text.replaceAll(",", "")) : i == 3
      //               ? double.parse(_providerIncomeCompany.controllerCostOfRevenue.text.replaceAll(",", "")) : i == 4
      //               ? double.parse(_providerIncomeCompany.controllerGrossProfit.text.replaceAll(",", "")) : i == 5
      //               ? double.parse(_providerIncomeCompany.controllerOperatingCosts.text.replaceAll(",", "")) : i == 6
      //               ? double.parse(_providerIncomeCompany.controllerOtherCosts.text.replaceAll(",", "")) : i == 7
      //               ? double.parse(_providerIncomeCompany.controllerNetProfitBeforeTax.text.replaceAll(",", "")) : i == 8
      //               ? double.parse(_providerIncomeCompany.controllerTax.text.replaceAll(",", "")) : i == 9
      //               ? double.parse(_providerIncomeCompany.controllerNetProfitAfterTax.text.replaceAll(",", ""))
      //               : double.parse(_providerIncomeCompany.controllerOtherInstallments.text.replaceAll(",", "")),
      //           "dataStatus": "ACTIVE",
      //           "creationalSpecification": {
      //             "createdAt": formatDateValidateAndSubmit(DateTime.now()),
      //             "createdBy": _preferences.getString("username"),
      //             "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
      //             "modifiedBy": _preferences.getString("username"),
      //           },
      //         }
      //     );
      //   }
      // }
      var cust_type = _preferences.getString("cust_type");
      if(cust_type == "PER"){
        for(int i=0; i < _providerIncome.listIncome.length; i++){
          _customerIncomes.add(
              {
                "customerIncomeID": _preferences.getString("last_known_state") != "IDE" ? _providerIncome.listIncome[i].customerIncomeID : "NEW",
                "incomeFrmlType": _providerIncome.listIncome[i].type_income_frml,
                "incomeType": _providerIncome.listIncome[i].income_type,
                "incomeValue": double.parse(_providerIncome.listIncome[i].income_value.replaceAll(",", "")),
                "dataStatus": "ACTIVE",
                "creationalSpecification": {
                  "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                  "createdBy": _preferences.getString("username"),
                  "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                  "modifiedBy": _preferences.getString("username"),
                },
              }
          );
        }
      }else{
        for(int i=0; i < _providerIncomeCom.listIncome.length; i++){
          _customerIncomes.add(
              {
                "customerIncomeID": _preferences.getString("last_known_state") != "IDE" ? _providerIncomeCom.listIncome[i].customerIncomeID : "NEW",
                "incomeFrmlType": _providerIncomeCom.listIncome[i].type_income_frml,
                "incomeType": _providerIncomeCom.listIncome[i].income_type,
                "incomeValue": double.parse(_providerIncomeCom.listIncome[i].income_value.replaceAll(",", "")),
                "dataStatus": "ACTIVE",
                "creationalSpecification": {
                  "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                  "createdBy": _preferences.getString("username"),
                  "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                  "modifiedBy": _preferences.getString("username"),
                },
              }
          );
        }
      }



      var _body = jsonEncode({
        "customerType": _preferences.getString("cust_type"),
        "occupationType": _preferences.getString("cust_type") != "COM" ? _providerFoto.occupationSelected != null ? _providerFoto.occupationSelected.KODE : "" : "LEMBAGA",
        "sumInstallment": _providerInfCreditStructure.controllerInstallment.text != "" ? double.parse(_providerInfCreditStructure.controllerInstallment.text.replaceAll(",", "")) : 0,
        "customerIncomes": _customerIncomes,
        "aoInstallment": 0,
        "obligorID": _preferences.getString("oid") != null ? _preferences.getString("oid") : "",
        "statusAORO": _preferences.getString("status_aoro"), // _preferences.getString("status_aoro") != null ? _preferences.getString("status_aoro") : "0"
        "orderID": _preferences.getString("last_known_state") != "IDE" ? _providerApp.applNo : ""
      });
      print("body get dsr: $_body");
      _providerParentIndividu.insertLog(context, _timeStartValidate, DateTime.now(), _body, "Start Get DSR ${_preferences.getString("last_known_state")}", "Start Get DSR ${_preferences.getString("last_known_state")}");
      String _getDSR = await storage.read(key: "GetDSR");
      final _response = await _http.post(
          "${BaseUrl.urlGeneral}$_getDSR",
          // "http://192.168.57.84/orca_api/public/login",
          // "${getUrlAcction}adira-acction/acction/service/dsr/calculate",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));
      if(_response.statusCode == 200 || _response.statusCode == 302){
        final _result = jsonDecode(_response.body);
        print("result dsr = $_result");
        if(_result == null){
          showSnackBar("DSR tidak ditemukan");
          this._flag = false;
          loadData = false;
          this._loadDataDSR = false;
        } else{
          this._controllerDebtComparison.clear();
          this._controllerIncomeComparison.clear();
          this._controllerIRR.clear();
          this._controllerDSC.clear();
          this._controllerDebtComparison.text =  _result['dsr'].toString();
          this._controllerIncomeComparison.text =  _result['dir'].toString();
          this._controllerIRR.text = _result['irr'].toString();
          this._controllerDSC.text = _result['dsc'].toString();
          this._flag = true;
          loadData = false;
          this._loadDataDSR = false;
          _formatting();
        }
      }
      else{
        showSnackBar("Error get dsr response status ${_response.statusCode}");
        loadData = false;
      }
    }
    on TimeoutException catch(_){
      loadData = false;
      this._loadDataDSR = false;
      showSnackBar("Timeout connection");
    }
    catch(e){
      showSnackBar("Error ${e.toString()}");
      loadData = false;
      this._loadDataDSR = false;
    }
    loadData = false;
    this._loadDataDSR = false;
    notifyListeners();
  }

  void _formatting(){
    this._controllerDebtComparison.text = formatCurrency.formatCurrencyPoint5(this._controllerDebtComparison.text);
    this._controllerIncomeComparison.text = formatCurrency.formatCurrencyPoint5(this._controllerIncomeComparison.text);
    this._controllerIRR.text =formatCurrency.formatCurrencyPoint2(this._controllerIRR.text); // per tanggal 27.07.2021 diganti formatnya jadi 5 angka dibelakang koma (formatCurrency2)
    this._controllerDSC.text = formatCurrency.formatCurrencyPoint2(this._controllerDSC.text); // per tanggal 27.07.2021 diganti formatnya jadi 5 angka dibelakang koma (formatCurrency2)
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  clearDataInfoCreditIncome(){
    this._flag = false;
    this._autoValidate = false;
    this._controllerDebtComparison.clear();
    this._controllerDSC.clear();
    this._controllerIncomeComparison.clear();
    this._controllerIRR.clear();
    isDisablePACIAAOSCONA = false;
  }

  void updateMS2ApplObjectInfStrukturKreditIncome() async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    _dbHelper.updateMS2ApplObjectInfStrukturKreditIncome(MS2ApplObjectModel(
      "1",
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      this._controllerDebtComparison.text.isNotEmpty ? double.parse(this._controllerDebtComparison.text.replaceAll(",", "")) : null,
      this._controllerIncomeComparison.text.isNotEmpty ? double.parse(this._controllerIncomeComparison.text.replaceAll(",", "")) : null,
      this._controllerDSC.text.isNotEmpty ? double.parse(this._controllerDSC.text.replaceAll(",", "")) : null,
      this._controllerIRR.text.isNotEmpty ? double.parse(this._controllerIRR.text.replaceAll(",", "")) : null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      DateTime.now().toString(),
      _preferences.getString("username"),
      1,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null
    ));
  }

  Future<void> setDataFromSQLite(BuildContext context) async{
    var _check = await _dbHelper.selectMS2ApplObjectInfStrukturKreditIncome();
    if(_check.isNotEmpty){
      if(_check[0]['dsr'] != "null" && _check[0]['dsr'] != ""){
        this._controllerDebtComparison.text = formatCurrency.formatCurrencyPoint5(_check[0]['dsr'].toString());
        this._controllerIncomeComparison.text = formatCurrency.formatCurrencyPoint5(_check[0]['dir'].toString());
        this._controllerDSC.text = formatCurrency.formatCurrencyPoint2(_check[0]['dsc'].toString());
        this._controllerIRR.text = formatCurrency.formatCurrencyPoint2(_check[0]['irr'].toString());
        // this._flag = true; // biar ga kecentang saja
      }
    }
    checkDataDakor();
    notifyListeners();
  }

  void checkDataDakor() async{
      if(_lastKnownState == "DKR"){
          var _check = await _dbHelper.selectMS2ApplObjectInfStrukturKreditIncome();
          if(_check.isNotEmpty){
            if(_check[0]['dsr'] != "null" && _check[0]['dsr'] != ""){
              isDebtComparisonDakor = this._controllerDebtComparison.text != _check[0]['dsr'].toString() || _check[0]['edit_dsr'] == "1";
              isIncomeComparisonDakor = this._controllerIncomeComparison.text != _check[0]['dir'].toString() || _check[0]['edit_dir'] == "1";
              isDSCDakor = this._controllerDSC.text != formatCurrency.formatCurrency2(_check[0]['dsc'].toString()) || _check[0]['edit_dsc'] == "1";
              isIRRDakor = double.parse(this._controllerIRR.text.replaceAll(",", "")) != _check[0]['irr'] || _check[0]['edit_irr'] == "1";
            }
          }
          notifyListeners();
      }
  }

  bool get isIRRDakor => _isIRRDakor;

  set isIRRDakor(bool value) {
    _isIRRDakor = value;
    notifyListeners();
  }

  bool get isDSCDakor => _isDSCDakor;

  set isDSCDakor(bool value) {
    _isDSCDakor = value;
    notifyListeners();
  }

  bool get isIncomeComparisonDakor => _isIncomeComparisonDakor;

  set isIncomeComparisonDakor(bool value) {
    _isIncomeComparisonDakor = value;
    notifyListeners();
  }

  bool get isDebtComparisonDakor => _isDebtComparisonDakor;

  set isDebtComparisonDakor(bool value) {
    _isDebtComparisonDakor = value;
    notifyListeners();
  }

  Future<String> getDSRNew(BuildContext context) async {
    // Form IDE Individu
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
    var _providerIncome = Provider.of<FormMIncomeChangeNotifier>(context, listen: false);
    var _providerIncomeCom = Provider.of<FormMCompanyPendapatanChangeNotifier>(context, listen: false);

    // Form App
    var _providerApp = Provider.of<InfoAppChangeNotifier>(context, listen: false);
    var _providerInfCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false);

    SharedPreferences _preferences = await SharedPreferences.getInstance();
    loadData = true;
    this._loadDataDSR = true;
    try{
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);

      var _customerIncomes = [];
      // if(_preferences.getString("cust_type") == "PER") {
      //   if(_providerFoto.occupationSelected.KODE == "05" || _providerFoto.occupationSelected.KODE == "07") {
      //     if(_providerInfoNasabah.maritalStatusSelected.id != "01") {
      //       for(int i=0; i < 13; i++) {
      //         _customerIncomes.add(
      //             {
      //               "customerIncomeID": _preferences.getString("last_known_state") != "IDE" ? _preferences.getString("customerIncomeID") : "NEW",
      //               "incomeFrmlType": i == 0
      //                   ? "S" : i == 1
      //                   ? "+" : i == 2
      //                   ? "=" : i == 3
      //                   ? "-" : i == 4
      //                   ? "=" : i == 5
      //                   ? "-" : i == 6
      //                   ? "-" : i == 7
      //                   ? "=" : i == 8
      //                   ? "-" : i == 9
      //                   ? "=" : i == 10
      //                   ? "-" : i == 11
      //                   ? "="
      //                   : "N",
      //               "incomeType": i == 0
      //                   ? "001" : i == 1
      //                   ? "002" : i == 2
      //                   ? "003" : i == 3
      //                   ? "004" : i == 4
      //                   ? "005" : i == 5
      //                   ? "006" : i == 6
      //                   ? "007" : i == 7
      //                   ? "008" : i == 8
      //                   ? "009" : i == 9
      //                   ? "010" : i == 10
      //                   ? "011" : i == 11
      //                   ? "017"
      //                   : "013",
      //               "incomeValue": i == 0
      //                   ? double.parse(_providerIncome.controllerMonthlyIncome.text.replaceAll(",", "")) : i == 1
      //                   ? double.parse(_providerIncome.controllerOtherIncome.text.replaceAll(",", "")) : i == 2
      //                   ? double.parse(_providerIncome.controllerTotalIncome.text.replaceAll(",", "")) : i == 3
      //                   ? double.parse(_providerIncome.controllerCostOfGoodsSold.text.replaceAll(",", "")) : i == 4
      //                   ? double.parse(_providerIncome.controllerGrossProfit.text.replaceAll(",", "")) : i == 5
      //                   ? double.parse(_providerIncome.controllerOperatingCosts.text.replaceAll(",", "")) : i == 6
      //                   ? double.parse(_providerIncome.controllerOtherCosts.text.replaceAll(",", "")) : i == 7
      //                   ? double.parse(_providerIncome.controllerNetProfitBeforeTax.text.replaceAll(",", "")) : i == 8
      //                   ? double.parse(_providerIncome.controllerTax.text.replaceAll(",", "")) : i == 9
      //                   ? double.parse(_providerIncome.controllerNetProfitAfterTax.text.replaceAll(",", "")) : i == 10
      //                   ? double.parse(_providerIncome.controllerCostOfLiving.text.replaceAll(",", "")) : i == 11
      //                   ? double.parse(_providerIncome.controllerRestIncome.text.replaceAll(",", ""))
      //                   : double.parse(_providerIncome.controllerOtherInstallments.text.replaceAll(",", "")),
      //               "dataStatus": "ACTIVE",
      //               "creationalSpecification": {
      //                 "createdAt": formatDateValidateAndSubmit(DateTime.now()),
      //                 "createdBy": _preferences.getString("username"),
      //                 "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
      //                 "modifiedBy": _preferences.getString("username"),
      //               },
      //             }
      //         );
      //       }
      //     }
      //     else {
      //       for(int i=0; i < 14; i++) {
      //         _customerIncomes.add(
      //             {
      //               "customerIncomeID":_preferences.getString("last_known_state") != "IDE" ? _preferences.getString("customerIncomeID") : "NEW",
      //               "incomeFrmlType": i == 0
      //                   ? "S" : i == 1
      //                   ? "+" : i == 2
      //                   ? "=" : i == 3
      //                   ? "-" : i == 4
      //                   ? "=" : i == 5
      //                   ? "-" : i == 6
      //                   ? "-" : i == 7
      //                   ? "=" : i == 8
      //                   ? "-" : i == 9
      //                   ? "=" : i == 10
      //                   ? "-" : i == 11
      //                   ? "=" : i == 12
      //                   ? "N"
      //                   : "N",
      //               "incomeType": i == 0
      //                   ? "001" : i == 1
      //                   ? "002" : i == 2
      //                   ? "003" : i == 3
      //                   ? "004" : i == 4
      //                   ? "005" : i == 5
      //                   ? "006" : i == 6
      //                   ? "007" : i == 7
      //                   ? "008" : i == 8
      //                   ? "009" : i == 9
      //                   ? "010" : i == 10
      //                   ? "011" : i == 11
      //                   ? "017" : i == 12
      //                   ? "012"
      //                   : "013",
      //               "incomeValue": i == 0
      //                   ? double.parse(_providerIncome.controllerMonthlyIncome.text.replaceAll(",", "")) : i == 1
      //                   ? double.parse(_providerIncome.controllerOtherIncome.text.replaceAll(",", "")) : i == 2
      //                   ? double.parse(_providerIncome.controllerTotalIncome.text.replaceAll(",", "")) : i == 3
      //                   ? double.parse(_providerIncome.controllerCostOfGoodsSold.text.replaceAll(",", "")) : i == 4
      //                   ? double.parse(_providerIncome.controllerGrossProfit.text.replaceAll(",", "")) : i == 5
      //                   ? double.parse(_providerIncome.controllerOperatingCosts.text.replaceAll(",", "")) : i == 6
      //                   ? double.parse(_providerIncome.controllerOtherCosts.text.replaceAll(",", "")) : i == 7
      //                   ? double.parse(_providerIncome.controllerNetProfitBeforeTax.text.replaceAll(",", "")) : i == 8
      //                   ? double.parse(_providerIncome.controllerTax.text.replaceAll(",", "")) : i == 9
      //                   ? double.parse(_providerIncome.controllerNetProfitAfterTax.text.replaceAll(",", "")) : i == 10
      //                   ? double.parse(_providerIncome.controllerCostOfLiving.text.replaceAll(",", "")) : i == 11
      //                   ? double.parse(_providerIncome.controllerRestIncome.text.replaceAll(",", "")) : i == 12
      //                   ? double.parse(_providerIncome.controllerSpouseIncome.text.replaceAll(",", ""))
      //                   : double.parse(_providerIncome.controllerOtherInstallments.text.replaceAll(",", "")),
      //               "dataStatus": "ACTIVE",
      //               "creationalSpecification": {
      //                 "createdAt": formatDateValidateAndSubmit(DateTime.now()),
      //                 "createdBy": _preferences.getString("username"),
      //                 "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
      //                 "modifiedBy": _preferences.getString("username"),
      //               },
      //             }
      //         );
      //       }
      //     }
      //   }
      //   else {
      //     if(_providerInfoNasabah.maritalStatusSelected.id != "01") {
      //       for(int i=0; i < 6; i++) {
      //         _customerIncomes.add(
      //             {
      //               "customerIncomeID": _preferences.getString("last_known_state") != "IDE" ? _preferences.getString("customerIncomeID") : "NEW",
      //               "incomeFrmlType": i == 0
      //                   ? "S" : i == 1
      //                   ? "+" : i == 2
      //                   ? "=" : i == 3
      //                   ? "-" : i == 4
      //                   ? "="
      //                   : "N",
      //               "incomeType": i == 0
      //                   ? "016" : i == 1
      //                   ? "002" : i == 2
      //                   ? "003" : i == 3
      //                   ? "011" : i == 4
      //                   ? "017"
      //                   : "013",
      //               "incomeValue": i == 0
      //                   ? double.parse(_providerIncome.controllerMonthlyIncome.text.replaceAll(",", "")) : i == 1
      //                   ? double.parse(_providerIncome.controllerOtherIncome.text.replaceAll(",", "")) : i == 2
      //                   ? double.parse(_providerIncome.controllerTotalIncome.text.replaceAll(",", "")) : i == 3
      //                   ? double.parse(_providerIncome.controllerCostOfLiving.text.replaceAll(",", "")) : i == 4
      //                   ? double.parse(_providerIncome.controllerRestIncome.text.replaceAll(",", ""))
      //                   : double.parse(_providerIncome.controllerOtherInstallments.text.replaceAll(",", "")),
      //               "dataStatus": "ACTIVE",
      //               "creationalSpecification": {
      //                 "createdAt": formatDateValidateAndSubmit(DateTime.now()),
      //                 "createdBy": _preferences.getString("username"),
      //                 "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
      //                 "modifiedBy": _preferences.getString("username"),
      //               },
      //             }
      //         );
      //       }
      //     }
      //     else {
      //       for(int i=0; i < 7; i++) {
      //         _customerIncomes.add(
      //             {
      //               "customerIncomeID": _preferences.getString("last_known_state") != "IDE" ? _preferences.getString("customerIncomeID") : "NEW",
      //               "incomeFrmlType": i == 0
      //                   ? "S" : i == 1
      //                   ? "+" : i == 2
      //                   ? "+" : i == 3
      //                   ? "=" : i == 4
      //                   ? "-" : i == 5
      //                   ? "="
      //                   : "N",
      //               "incomeType": i == 0
      //                   ? "016" : i == 1
      //                   ? "012" : i == 2
      //                   ? "002" : i == 3
      //                   ? "003" : i == 4
      //                   ? "011" : i == 5
      //                   ? "017"
      //                   : "013",
      //               "incomeValue": i == 0
      //                   ? _providerIncome.controllerMonthlyIncome.text.replaceAll(",", "") : i == 1
      //                   ? _providerIncome.controllerSpouseIncome.text.replaceAll(",", "") : i == 2
      //                   ? _providerIncome.controllerOtherIncome.text.replaceAll(",", "") : i == 3
      //                   ? _providerIncome.controllerTotalIncome.text.replaceAll(",", "") : i == 4
      //                   ? _providerIncome.controllerCostOfLiving.text.replaceAll(",", "") : i == 5
      //                   ? _providerIncome.controllerRestIncome.text.replaceAll(",", "")
      //                   : _providerIncome.controllerOtherInstallments.text.replaceAll(",", ""),
      //               "dataStatus": "ACTIVE",
      //               "creationalSpecification": {
      //                 "createdAt": formatDateValidateAndSubmit(DateTime.now()),
      //                 "createdBy": _preferences.getString("username"),
      //                 "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
      //                 "modifiedBy": _preferences.getString("username"),
      //               },
      //             }
      //         );
      //       }
      //     }
      //   }
      // }
      // // adjut di company untukk limit looping
      // else {
      //   for(int i=0; i < 11; i++) {
      //     _customerIncomes.add(
      //         {
      //           "customerIncomeID": _preferences.getString("last_known_state") != "IDE" ? _preferences.getString("customerIncomeID") : "NEW",
      //           "incomeFrmlType": i == 0
      //               ? "S" : i == 1
      //               ? "+" : i == 2
      //               ? "=" : i == 3
      //               ? "-" : i == 4
      //               ? "=" : i == 5
      //               ? "-" : i == 6
      //               ? "-" : i == 7
      //               ? "=" : i == 8
      //               ? "-" : i == 9
      //               ? "="
      //               : "N",
      //           "incomeType": i == 0
      //               ? "016" : i == 1
      //               ? "002" : i == 2
      //               ? "003" : i == 3
      //               ? "004" : i == 4
      //               ? "005" : i == 5
      //               ? "006" : i == 6
      //               ? "007" : i == 7
      //               ? "008" : i == 8
      //               ? "009" : i == 9
      //               ? "010"
      //               : "013",
      //           "incomeValue": i == 0
      //               ? double.parse(_providerIncomeCompany.controllerMonthlyIncome.text.replaceAll(",", "")) : i == 1
      //               ? double.parse(_providerIncomeCompany.controllerOtherIncome.text.replaceAll(",", "")) : i == 2
      //               ? double.parse(_providerIncomeCompany.controllerTotalIncome.text.replaceAll(",", "")) : i == 3
      //               ? double.parse(_providerIncomeCompany.controllerCostOfRevenue.text.replaceAll(",", "")) : i == 4
      //               ? double.parse(_providerIncomeCompany.controllerGrossProfit.text.replaceAll(",", "")) : i == 5
      //               ? double.parse(_providerIncomeCompany.controllerOperatingCosts.text.replaceAll(",", "")) : i == 6
      //               ? double.parse(_providerIncomeCompany.controllerOtherCosts.text.replaceAll(",", "")) : i == 7
      //               ? double.parse(_providerIncomeCompany.controllerNetProfitBeforeTax.text.replaceAll(",", "")) : i == 8
      //               ? double.parse(_providerIncomeCompany.controllerTax.text.replaceAll(",", "")) : i == 9
      //               ? double.parse(_providerIncomeCompany.controllerNetProfitAfterTax.text.replaceAll(",", ""))
      //               : double.parse(_providerIncomeCompany.controllerOtherInstallments.text.replaceAll(",", "")),
      //           "dataStatus": "ACTIVE",
      //           "creationalSpecification": {
      //             "createdAt": formatDateValidateAndSubmit(DateTime.now()),
      //             "createdBy": _preferences.getString("username"),
      //             "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
      //             "modifiedBy": _preferences.getString("username"),
      //           },
      //         }
      //     );
      //   }
      // }

      var cust_type = _preferences.getString("cust_type");
      if(cust_type == "PER"){
        for(int i=0; i < _providerIncome.listIncome.length; i++){
          _customerIncomes.add(
              {
                "customerIncomeID": _preferences.getString("last_known_state") != "IDE" ? _providerIncome.listIncome[i].customerIncomeID : "NEW",
                "incomeFrmlType": _providerIncome.listIncome[i].type_income_frml,
                "incomeType": _providerIncome.listIncome[i].income_type,
                "incomeValue": double.parse(_providerIncome.listIncome[i].income_value.replaceAll(",", "")),
                "dataStatus": "ACTIVE",
                "creationalSpecification": {
                  "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                  "createdBy": _preferences.getString("username"),
                  "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                  "modifiedBy": _preferences.getString("username"),
                },
              }
          );
        }
      }else{
        for(int i=0; i < _providerIncomeCom.listIncome.length; i++){
          _customerIncomes.add(
              {
                "customerIncomeID": _preferences.getString("last_known_state") != "IDE" ? _providerIncomeCom.listIncome[i].customerIncomeID : "NEW",
                "incomeFrmlType": _providerIncomeCom.listIncome[i].type_income_frml,
                "incomeType": _providerIncomeCom.listIncome[i].income_type,
                "incomeValue": double.parse(_providerIncomeCom.listIncome[i].income_value.replaceAll(",", "")),
                "dataStatus": "ACTIVE",
                "creationalSpecification": {
                  "createdAt": formatDateValidateAndSubmit(DateTime.now()),
                  "createdBy": _preferences.getString("username"),
                  "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
                  "modifiedBy": _preferences.getString("username"),
                },
              }
          );
        }
      }


      var _body = jsonEncode({
        "customerType": _preferences.getString("cust_type"),
        "occupationType": _preferences.getString("cust_type") != "COM" ? _providerFoto.occupationSelected != null ? _providerFoto.occupationSelected.KODE : "" : "LEMBAGA",
        "sumInstallment": _providerInfCreditStructure.controllerInstallment.text != "" ? double.parse(_providerInfCreditStructure.controllerInstallment.text.replaceAll(",", "")) : 0,
        "customerIncomes": _customerIncomes,
        "aoInstallment": 0,
        "obligorID": _preferences.getString("oid") != null ? _preferences.getString("oid") : "",
        "statusAORO": _preferences.getString("status_aoro"), // _preferences.getString("status_aoro") != null ? _preferences.getString("status_aoro") : "0"
        "orderID": _preferences.getString("last_known_state") != "IDE" ? _providerApp.applNo : ""
      });
      print("body get dsr: $_body");

      String _getDSR = await storage.read(key: "GetDSR");
      final _response = await _http.post(
          "${BaseUrl.urlGeneral}$_getDSR",
          // "http://192.168.57.84/orca_api/public/login",
          // "${getUrlAcction}adira-acction/acction/service/dsr/calculate",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));
      if(_response.statusCode == 200 || _response.statusCode == 302){
        final _result = jsonDecode(_response.body);
        print("result dsr = $_result");
        if(_result == null){
          showSnackBar("DSR tidak ditemukan");
          this._flag = false;
          loadData = false;
          this._loadDataDSR = false;
          throw "DSR tidak ditemukan";
        } else{
          this._controllerDebtComparison.clear();
          this._controllerIncomeComparison.clear();
          this._controllerIRR.clear();
          this._controllerDSC.clear();
          this._controllerDebtComparison.text =  _result['dsr'].toString();
          this._controllerIncomeComparison.text =  _result['dir'].toString();
          this._controllerIRR.text = _result['irr'].toString();
          this._controllerDSC.text = _result['dsc'].toString();
          this._flag = true;
          loadData = false;
          this._loadDataDSR = false;
          _formatting();
        }
      }
      else{
        loadData = false;
        throw "DSR tidak ditemukan";
      }
    }
    on TimeoutException catch(_){
      loadData = false;
      this._loadDataDSR = false;
      throw "Timeout connection";
    }
    catch(e){
      loadData = false;
      this._loadDataDSR = false;
      throw "Error ${e.toString()}";
    }
    loadData = false;
    this._loadDataDSR = false;
    notifyListeners();
  }
}