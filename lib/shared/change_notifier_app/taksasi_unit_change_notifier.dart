import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/ms2_appl_taksasi_model.dart';
import 'package:ad1ms2_dev/models/taksasi_unit_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_collatelar_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/marketing_notes_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_individu_change_notifier/form_m_parent_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/format_currency.dart';
import 'package:ad1ms2_dev/shared/resource/submit_data_partial.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TaksasiUnitChangeNotifier with ChangeNotifier{
  DbHelper _dbHelper = DbHelper();
  bool _isVisible = false;
  bool _autoValidateCar = false;
  bool _autoValidateMotor = false;
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<TaksasiUnitModel> _listTaksasi = [];
  String _kode;
  double _totalNilaiBobot = 0;
  var storage = FlutterSecureStorage();
  SubmitDataPartial _submitDataPartial = SubmitDataPartial();
  TextEditingController _controllerTotalNilaiBobot = TextEditingController();
  FormatCurrency _formatCurrency = FormatCurrency();

  FormatCurrency get formatCurrency => _formatCurrency;

  TextEditingController get controllerTotalNilaiBobot =>
      _controllerTotalNilaiBobot;

  String get kode => _kode;

  set kode(String value) {
    this._kode = value;
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  bool get autoValidateCar => _autoValidateCar;

  set autoValidateCar(bool value) {
    this._autoValidateCar = value;
    notifyListeners();
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
    notifyListeners();
  }

  double get totalNilaiBobot => _totalNilaiBobot;

  set totalNilaiBobot(double value) {
    this._totalNilaiBobot = value;
    notifyListeners();
  }

  bool get autoValidateMotor => _autoValidateMotor;

  set autoValidateMotor(bool value) {
    this._autoValidateMotor = value;
    notifyListeners();
  }

  bool get isVisible => _isVisible;

  set isVisible(bool value) {
    this._isVisible = value;
    notifyListeners();
  }

  void setIsVisible(BuildContext context){
    var _providerColla = Provider.of<InformationCollateralChangeNotifier>(context,listen: false);
    isVisible = false;
    if(_providerColla.collateralTypeModel != null){
      if(_providerColla.collateralTypeModel.id == "001"){
        if(_providerColla.objectSelected != null){
          if(_providerColla.objectSelected.id != "001" && _providerColla.objectSelected.id != "003"){
            isVisible = true;
          }
        }
      }
    }
  }

  //mobil
  TextEditingController _controllerEngineSound = TextEditingController();
  TextEditingController _controllerMachineClean = TextEditingController();
  TextEditingController _controllerAC = TextEditingController();
  TextEditingController _controllerRadioTapeCD = TextEditingController();
  TextEditingController _controllerPowerWindow = TextEditingController();
  TextEditingController _controllerCentralLock = TextEditingController();
  TextEditingController _controllerDashboard = TextEditingController();
  TextEditingController _controllerJok = TextEditingController();
  TextEditingController _controllerPlafond = TextEditingController();
  TextEditingController _controllerAlarm = TextEditingController();
  TextEditingController _controllerBumper = TextEditingController();
  TextEditingController _controllerLampu = TextEditingController();
  TextEditingController _controllerSpion = TextEditingController();
  TextEditingController _controllerFender = TextEditingController();
  TextEditingController _controllerTiresAndWheels = TextEditingController();
  TextEditingController _controllerWindshield = TextEditingController();
  TextEditingController _controllerFrameAndPole = TextEditingController();
  TextEditingController _controllerHood = TextEditingController();//kap mesin
  TextEditingController _controllerDoor = TextEditingController();
  TextEditingController _controllerCarTrunk = TextEditingController();
  TextEditingController _controllerBoxIronTubTank = TextEditingController();

  TextEditingController get controllerEngineSound => _controllerEngineSound;
  TextEditingController get controllerMachineClean => _controllerMachineClean;
  TextEditingController get controllerAC => _controllerAC;
  TextEditingController get controllerRadioTapeCD => _controllerRadioTapeCD;
  TextEditingController get controllerPowerWindow => _controllerPowerWindow;
  TextEditingController get controllerCentralLock => _controllerCentralLock;
  TextEditingController get controllerDashboard => _controllerDashboard;
  TextEditingController get controllerJok => _controllerJok;
  TextEditingController get controllerPlafond => _controllerPlafond;
  TextEditingController get controllerAlarm => _controllerAlarm;
  TextEditingController get controllerBumper => _controllerBumper;
  TextEditingController get controllerLampu => _controllerLampu;
  TextEditingController get controllerSpion => _controllerSpion;
  TextEditingController get controllerFender => _controllerFender;
  TextEditingController get controllerTiresAndWheels => _controllerTiresAndWheels;
  TextEditingController get controllerWindshield => _controllerWindshield;
  TextEditingController get controllerFrameAndPole => _controllerFrameAndPole;
  TextEditingController get controllerHood => _controllerHood;
  TextEditingController get controllerDoor => _controllerDoor;
  TextEditingController get controllerCarTrunk => _controllerCarTrunk;
  TextEditingController get controllerBoxIronTubTank => _controllerBoxIronTubTank;

  List<bool> _listValueEngineSound = [];
  List<bool> _listValueEngineClean = [];
  List<bool> _listValueAC = [];
  List<bool> _listValueRadioTapeCD = [];
  List<bool> _listValuePowerWindow = [];
  List<bool> _listValueCentralLock = [];
  List<bool> _listValueDashboard = [];
  List<bool> _listValueJok = [];
  List<bool> _listValuePlafond = [];
  List<bool> _listValueAlarm = [];
  List<bool> _listValueBumper = [];
  List<bool> _listValueLampu = [];
  List<bool> _listValueSpion = [];
  List<bool> _listValueFender = [];
  List<bool> _listValueTireAndWheels = [];
  List<bool> _listValueWindShield = [];
  List<bool> _listValueFrameAndPole = [];
  List<bool> _listValueHood = [];
  List<bool> _listValueDoor = [];
  List<bool> _listValueCarTrunk = [];
  List<bool> _listValueBoxIronTubTank = [];

  List<bool> get listValueEngineSound => _listValueEngineSound;
  List<bool> get listValueEngineClean => _listValueEngineClean;
  List<bool> get listValueAC => _listValueAC;
  List<bool> get listValueRadioTapeCD => _listValueRadioTapeCD;
  List<bool> get listValuePowerWindow => _listValuePowerWindow;
  List<bool> get listValueCentralLock => _listValueCentralLock;
  List<bool> get listValueDashboard => _listValueDashboard;
  List<bool> get listValueJok => _listValueJok;
  List<bool> get listValuePlafond => _listValuePlafond;
  List<bool> get listValueAlarm => _listValueAlarm;
  List<bool> get listValueBumper => _listValueBumper;
  List<bool> get listValueLampu => _listValueLampu;
  List<bool> get listValueSpion => _listValueSpion;
  List<bool> get listValueFender => _listValueFender;
  List<bool> get listValueTireAndWheels => _listValueTireAndWheels;
  List<bool> get listValueWindShield => _listValueWindShield;
  List<bool> get listValueFrameAndPole => _listValueFrameAndPole;
  List<bool> get listValueHood => _listValueHood;
  List<bool> get listValueDoor => _listValueDoor;
  List<bool> get listValueCarTrunk => _listValueCarTrunk;
  List<bool> get listValueBoxIronTubTank => _listValueBoxIronTubTank;

  void calculateEngineSound(String data){
    int value = double.parse(data).round();
    _listValueEngineSound.clear();
    if(value <= 10){
      _listValueEngineSound.add(true);
      _listValueEngineSound.add(false);
      _listValueEngineSound.add(false);
    }else if(value > 10 && value <= 30){
      _listValueEngineSound.add(false);
      _listValueEngineSound.add(true);
      _listValueEngineSound.add(false);
    }
    else{
      _listValueEngineSound.add(false);
      _listValueEngineSound.add(false);
      _listValueEngineSound.add(true);
    }
    notifyListeners();
  }

  void calculateEngineClean(String data){
    int value = double.parse(data).round();
    _listValueEngineClean.clear();
    if(value <= 10){
      _listValueEngineClean.add(true);
      _listValueEngineClean.add(false);
      _listValueEngineClean.add(false);
    }else if(value > 10 && value <= 30){
      _listValueEngineClean.add(false);
      _listValueEngineClean.add(true);
      _listValueEngineClean.add(false);
    }
    else{
      _listValueEngineClean.add(false);
      _listValueEngineClean.add(false);
      _listValueEngineClean.add(true);
    }
    notifyListeners();
  }

  void calculateAc(String data){
    int value = double.parse(data).round();
    _listValueAC.clear();
    if(value <= 10){
      _listValueAC.add(true);
      _listValueAC.add(false);
      _listValueAC.add(false);
    }else if(value > 10 && value <= 30){
      _listValueAC.add(false);
      _listValueAC.add(true);
      _listValueAC.add(false);
    }
    else{
      _listValueAC.add(false);
      _listValueAC.add(false);
      _listValueAC.add(true);
    }
    notifyListeners();
  }

  void calculateRadioTapeCD(String data){
    int value = double.parse(data).round();
    _listValueRadioTapeCD.clear();
    if(value <= 10){
      _listValueRadioTapeCD.add(true);
      _listValueRadioTapeCD.add(false);
      _listValueRadioTapeCD.add(false);
    }else if(value > 10 && value <= 30){
      _listValueRadioTapeCD.add(false);
      _listValueRadioTapeCD.add(true);
      _listValueRadioTapeCD.add(false);
    }
    else{
      _listValueRadioTapeCD.add(false);
      _listValueRadioTapeCD.add(false);
      _listValueRadioTapeCD.add(true);
    }
    notifyListeners();
  }

  void calculatePowerWindow(String data){
    int value = double.parse(data).round();
    _listValuePowerWindow.clear();
    if(value <= 10){
      _listValuePowerWindow.add(true);
      _listValuePowerWindow.add(false);
      _listValuePowerWindow.add(false);
    }else if(value > 10 && value <= 30){
      _listValuePowerWindow.add(false);
      _listValuePowerWindow.add(true);
      _listValuePowerWindow.add(false);
    }
    else{
      _listValuePowerWindow.add(false);
      _listValuePowerWindow.add(false);
      _listValuePowerWindow.add(true);
    }
    notifyListeners();
  }

  void calculateCentralLock(String data){
    int value = double.parse(data).round();
    _listValueCentralLock.clear();
    if(value <= 10){
      _listValueCentralLock.add(true);
      _listValueCentralLock.add(false);
      _listValueCentralLock.add(false);
    }else if(value > 10 && value <= 30){
      _listValueCentralLock.add(false);
      _listValueCentralLock.add(true);
      _listValueCentralLock.add(false);
    }
    else{
      _listValueCentralLock.add(false);
      _listValueCentralLock.add(false);
      _listValueCentralLock.add(true);
    }
    notifyListeners();
  }

  void calculateDashboard(String data){
    int value = double.parse(data).round();
    _listValueDashboard.clear();
    if(value <= 10){
      _listValueDashboard.add(true);
      _listValueDashboard.add(false);
      _listValueDashboard.add(false);
    }else if(value > 10 && value <= 30){
      _listValueDashboard.add(false);
      _listValueDashboard.add(true);
      _listValueDashboard.add(false);
    }
    else{
      _listValueDashboard.add(false);
      _listValueDashboard.add(false);
      _listValueDashboard.add(true);
    }
    notifyListeners();
  }

  void calculateJok(String data){
    int value = double.parse(data).round();
    _listValueJok.clear();
    if(value <= 10){
      _listValueJok.add(true);
      _listValueJok.add(false);
      _listValueJok.add(false);
    }else if(value > 10 && value <= 30){
      _listValueJok.add(false);
      _listValueJok.add(true);
      _listValueJok.add(false);
    }
    else{
      _listValueJok.add(false);
      _listValueJok.add(false);
      _listValueJok.add(true);
    }
    notifyListeners();
  }

  void calculatePlafond(String data){
    int value = double.parse(data).round();
    _listValuePlafond.clear();
    if(value <= 10){
      _listValuePlafond.add(true);
      _listValuePlafond.add(false);
      _listValuePlafond.add(false);
    }else if(value > 10 && value <= 30){
      _listValuePlafond.add(false);
      _listValuePlafond.add(true);
      _listValuePlafond.add(false);
    }
    else{
      _listValuePlafond.add(false);
      _listValuePlafond.add(false);
      _listValuePlafond.add(true);
    }
    notifyListeners();
  }

  void calculateAlarm(String data){
    int value = double.parse(data).round();
    _listValueAlarm.clear();
    if(value <= 10){
      _listValueAlarm.add(true);
      _listValueAlarm.add(false);
      _listValueAlarm.add(false);
    }else if(value > 10 && value <= 30){
      _listValueAlarm.add(false);
      _listValueAlarm.add(true);
      _listValueAlarm.add(false);
    }
    else{
      _listValuePlafond.add(false);
      _listValuePlafond.add(false);
      _listValuePlafond.add(true);
    }
    notifyListeners();
  }

  void calculateBumper(String data){
    int value = double.parse(data).round();
    _listValueBumper.clear();
    if(value <= 10){
      _listValueBumper.add(true);
      _listValueBumper.add(false);
      _listValueBumper.add(false);
    }else if(value > 10 && value <= 30){
      _listValueBumper.add(false);
      _listValueBumper.add(true);
      _listValueBumper.add(false);
    }
    else{
      _listValueBumper.add(false);
      _listValueBumper.add(false);
      _listValueBumper.add(true);
    }
    notifyListeners();
  }

  void calculateLampu(String data){
    int value = double.parse(data).round();
    _listValueLampu.clear();
    if(value <= 10){
      _listValueLampu.add(true);
      _listValueLampu.add(false);
      _listValueLampu.add(false);
    }else if(value > 10 && value <= 30){
      _listValueLampu.add(false);
      _listValueLampu.add(true);
      _listValueLampu.add(false);
    }
    else{
      _listValueLampu.add(false);
      _listValueLampu.add(false);
      _listValueLampu.add(true);
    }
    notifyListeners();
  }

  void calculateSpion(String data){
    int value = double.parse(data).round();
    _listValueSpion.clear();
    if(value <= 10){
      _listValueSpion.add(true);
      _listValueSpion.add(false);
      _listValueSpion.add(false);
    }else if(value > 10 && value <= 30){
      _listValueSpion.add(false);
      _listValueSpion.add(true);
      _listValueSpion.add(false);
    }
    else{
      _listValueSpion.add(false);
      _listValueSpion.add(false);
      _listValueSpion.add(true);
    }
    notifyListeners();
  }

  void calculateFender(String data){
    int value = double.parse(data).round();
    _listValueFender.clear();
    if(value <= 10){
      _listValueFender.add(true);
      _listValueFender.add(false);
      _listValueFender.add(false);
    }else if(value > 10 && value <= 30){
      _listValueFender.add(false);
      _listValueFender.add(true);
      _listValueFender.add(false);
    }
    else{
      _listValueFender.add(false);
      _listValueFender.add(false);
      _listValueFender.add(true);
    }
    notifyListeners();
  }

  void calculateTireAndWheels(String data){
    int value = double.parse(data).round();
    _listValueTireAndWheels.clear();
    if(value <= 10){
      _listValueTireAndWheels.add(true);
      _listValueTireAndWheels.add(false);
      _listValueTireAndWheels.add(false);
    }else if(value > 10 && value <= 30){
      _listValueTireAndWheels.add(false);
      _listValueTireAndWheels.add(true);
      _listValueTireAndWheels.add(false);
    }
    else{
      _listValueTireAndWheels.add(false);
      _listValueTireAndWheels.add(false);
      _listValueTireAndWheels.add(true);
    }
    notifyListeners();
  }

  void calculateWindShield(String data){
    int value = double.parse(data).round();
    _listValueWindShield.clear();
    if(value <= 10){
      _listValueWindShield.add(true);
      _listValueWindShield.add(false);
      _listValueWindShield.add(false);
    }else if(value > 10 && value <= 30){
      _listValueWindShield.add(false);
      _listValueWindShield.add(true);
      _listValueWindShield.add(false);
    }
    else{
      _listValueWindShield.add(false);
      _listValueWindShield.add(false);
      _listValueWindShield.add(true);
    }
    notifyListeners();
  }

  void calculateFrameAndPole(String data){
    int value = double.parse(data).round();
    _listValueFrameAndPole.clear();
    if(value <= 10){
      _listValueFrameAndPole.add(true);
      _listValueFrameAndPole.add(false);
      _listValueFrameAndPole.add(false);
    }else if(value > 10 && value <= 30){
      _listValueFrameAndPole.add(false);
      _listValueFrameAndPole.add(true);
      _listValueFrameAndPole.add(false);
    }
    else{
      _listValueFrameAndPole.add(false);
      _listValueFrameAndPole.add(false);
      _listValueFrameAndPole.add(true);
    }
    notifyListeners();
  }

  void calculateHood(String data){
    int value = double.parse(data).round();
    _listValueHood.clear();
    if(value <= 10){
      _listValueHood.add(true);
      _listValueHood.add(false);
      _listValueHood.add(false);
    }else if(value > 10 && value <= 30){
      _listValueHood.add(false);
      _listValueHood.add(true);
      _listValueHood.add(false);
    }
    else{
      _listValueHood.add(false);
      _listValueHood.add(false);
      _listValueHood.add(true);
    }
    notifyListeners();
  }

  void calculateDoor(String data){
    int value = double.parse(data).round();
    _listValueDoor.clear();
    if(value <= 10){
      _listValueDoor.add(true);
      _listValueDoor.add(false);
      _listValueDoor.add(false);
    }else if(value > 10 && value <= 30){
      _listValueDoor.add(false);
      _listValueDoor.add(true);
      _listValueDoor.add(false);
    }
    else{
      _listValueDoor.add(false);
      _listValueDoor.add(false);
      _listValueDoor.add(true);
    }
    notifyListeners();
  }

  void calculateCarTrunk(String data){
    int value = double.parse(data).round();
    _listValueCarTrunk.clear();
    if(value <= 10){
      _listValueCarTrunk.add(true);
      _listValueCarTrunk.add(false);
      _listValueCarTrunk.add(false);
    }else if(value > 10 && value <= 30){
      _listValueCarTrunk.add(false);
      _listValueCarTrunk.add(true);
      _listValueCarTrunk.add(false);
    }
    else{
      _listValueCarTrunk.add(false);
      _listValueCarTrunk.add(false);
      _listValueCarTrunk.add(true);
    }
    notifyListeners();
  }

  void calculateBoxIronTubTank(String data){
    int value = double.parse(data).round();
    _listValueBoxIronTubTank.clear();
    if(value <= 10){
      _listValueBoxIronTubTank.add(true);
      _listValueBoxIronTubTank.add(false);
      _listValueBoxIronTubTank.add(false);
    }else if(value > 10 && value <= 30){
      _listValueBoxIronTubTank.add(false);
      _listValueBoxIronTubTank.add(true);
      _listValueBoxIronTubTank.add(false);
    }
    else{
      _listValueBoxIronTubTank.add(false);
      _listValueBoxIronTubTank.add(false);
      _listValueBoxIronTubTank.add(true);
    }
    notifyListeners();
  }

  //Motor
  TextEditingController _controllerEngineSoundMotorCycle = TextEditingController();
  TextEditingController _controllerMachineCleanMotorCycle = TextEditingController();
  TextEditingController _controllerTires = TextEditingController();
  TextEditingController _controllerChassis = TextEditingController();
  TextEditingController _controllerCombinationMeter = TextEditingController();
  TextEditingController _controllerCoverBodyUtama = TextEditingController();
  TextEditingController _controllerCoverStangKemudi = TextEditingController();
  TextEditingController _controllerEmblem = TextEditingController();
  TextEditingController _controllerFootStepBracket = TextEditingController();
  TextEditingController _controllerJokMotor = TextEditingController();
  TextEditingController _controllerKnalpot = TextEditingController();
  TextEditingController _controllerLampuBelakang = TextEditingController();
  TextEditingController _controllerLampuBesarDepan = TextEditingController();
  TextEditingController _controllerLampuSein = TextEditingController();
  TextEditingController _controllerWings = TextEditingController();
  TextEditingController _controllerSpionMotor = TextEditingController();
  TextEditingController _controllerStarter = TextEditingController();
  TextEditingController _controllerIronTubAndCover = TextEditingController();//kap mesin
  TextEditingController _controllerBakBelakang = TextEditingController();
  TextEditingController _controllerMachineAndBlokMachine = TextEditingController();
  TextEditingController _controllerRadiatorWaterPump = TextEditingController();
  TextEditingController _controllerKarburator = TextEditingController();
  TextEditingController _controllerGearSetChainCVT = TextEditingController();
  TextEditingController _controllerMechanicBreak = TextEditingController();
  TextEditingController _controllerSwingArmBosh = TextEditingController();
  TextEditingController _controllerBackShockAbsorber = TextEditingController();
  TextEditingController _controllerStangForkShock = TextEditingController();
  TextEditingController _controllerVelg = TextEditingController();

  TextEditingController get controllerEngineSoundMotorCycle => _controllerEngineSoundMotorCycle;
  TextEditingController get controllerMachineCleanMotorCycle => _controllerMachineCleanMotorCycle;
  TextEditingController get controllerTires => _controllerTires;
  TextEditingController get controllerChassis => _controllerChassis;
  TextEditingController get controllerCombinationMeter => _controllerCombinationMeter;
  TextEditingController get controllerCoverBodyUtama => _controllerCoverBodyUtama;
  TextEditingController get controllerCoverStangKemudi => _controllerCoverStangKemudi;
  TextEditingController get controllerEmblem => _controllerEmblem;
  TextEditingController get controllerFootStepBracket => _controllerFootStepBracket;
  TextEditingController get controllerJokMotor => _controllerJokMotor;
  TextEditingController get controllerKnalpot => _controllerKnalpot;
  TextEditingController get controllerLampuBelakang => _controllerLampuBelakang;
  TextEditingController get controllerLampuBesarDepan => _controllerLampuBesarDepan;
  TextEditingController get controllerLampuSein => _controllerLampuSein;
  TextEditingController get controllerWings => _controllerWings;
  TextEditingController get controllerSpionMotor => _controllerSpionMotor;
  TextEditingController get controllerStarter => _controllerStarter;
  TextEditingController get controllerIronTubAndCover => _controllerIronTubAndCover;
  TextEditingController get controllerBakBelakang => _controllerBakBelakang;
  TextEditingController get controllerMachineAndBlokMachine => _controllerMachineAndBlokMachine;
  TextEditingController get controllerRadiatorWaterPump => _controllerRadiatorWaterPump;
  TextEditingController get controllerKarburator => _controllerKarburator;
  TextEditingController get controllerGearSetChainCVT => _controllerGearSetChainCVT;
  TextEditingController get controllerMechanicBreak => _controllerMechanicBreak;
  TextEditingController get controllerSwingArmBosh => _controllerSwingArmBosh;
  TextEditingController get controllerBackShockAbsorber => _controllerBackShockAbsorber;
  TextEditingController get controllerStangForkShock => _controllerStangForkShock;
  TextEditingController get controllerVelg => _controllerVelg;

  List<bool> _listValueEngineSoundMotorCycle = [];
  List<bool> _listValueEngineCleanMotorCycle = [];
  List<bool> _listValueTires = [];
  List<bool> _listValueChassis = [];
  List<bool> _listValueCombinationMeter = [];
  List<bool> _listValueCoverBodyPrimary = [];
  List<bool> _listValueCoverStangKemudi = [];
  List<bool> _listValueEmblem = [];
  List<bool> _listValueFootStepBracket = [];
  List<bool> _listValueJokMotor = [];
  List<bool> _listValueKnalpot = [];
  List<bool> _listValueLampuBelakang = [];
  List<bool> _listValueLampuBesarDepan = [];
  List<bool> _listValueLampuSein = [];
  List<bool> _listValueWings = [];
  List<bool> _listValueSpionMotorCycle = [];
  List<bool> _listValueStarter = [];
  List<bool> _listValueIronTankCover = [];
  List<bool> _listValueBakBelakang = [];
  List<bool> _listValueMachineBlokMachine = [];
  List<bool> _listValueRadiatorWaterPump = [];
  List<bool> _listValueKarburator = [];
  List<bool> _listValueGearSetCVT = [];
  List<bool> _listValueMechanicBreak = [];
  List<bool> _listValueSwingArmBosh = [];
  List<bool> _listValueShockAbsorber= [];
  List<bool> _listValueStangForkShockDepan = [];
  List<bool> _listValueVelg = [];

  List<bool> get listValueEngineSoundMotorCycle => _listValueEngineSoundMotorCycle;
  List<bool> get listValueEngineCleanMotorCycle => _listValueEngineCleanMotorCycle;
  List<bool> get listValueTires => _listValueTires;
  List<bool> get listValueChassis => _listValueChassis;
  List<bool> get listValueCombinationMeter => _listValueCombinationMeter;
  List<bool> get listValueCoverBodyPrimary => _listValueCoverBodyPrimary;
  List<bool> get listValueCoverStangKemudi => _listValueCoverStangKemudi;
  List<bool> get listValueEmblem => _listValueEmblem;
  List<bool> get listValueFootStepBracket => _listValueFootStepBracket;
  List<bool> get listValueJokMotor => _listValueJokMotor;
  List<bool> get listValueKnalpot => _listValueKnalpot;
  List<bool> get listValueLampuBelakang => _listValueLampuBelakang;
  List<bool> get listValueLampuBesarDepan => _listValueLampuBesarDepan;
  List<bool> get listValueLampuSein => _listValueLampuSein;
  List<bool> get listValueWings => _listValueWings;
  List<bool> get listValueSpionMotorCycle => _listValueSpionMotorCycle;
  List<bool> get listValueStarter => _listValueStarter;
  List<bool> get listValueIronTankCover => _listValueIronTankCover;
  List<bool> get listValueBakBelakang => _listValueBakBelakang;
  List<bool> get listValueMachineBlokMachine => _listValueMachineBlokMachine;
  List<bool> get listValueRadiatorWaterPump => _listValueRadiatorWaterPump;
  List<bool> get listValueKarburator => _listValueKarburator;
  List<bool> get listValueGearSetCVT => _listValueGearSetCVT;
  List<bool> get listValueMechanicBreak => _listValueMechanicBreak;
  List<bool> get listValueSwingArmBosh => _listValueSwingArmBosh;
  List<bool> get listValueShockAbsorber => _listValueShockAbsorber;
  List<bool> get listValueStangForkShockDepan => _listValueStangForkShockDepan;
  List<bool> get listValueVelg => _listValueVelg;

  // void calculateEngineSoundMotorCycle(String data){
  //   int value = double.parse(data).round();
  //   _listValueEngineSoundMotorCycle.clear();
  //   if(value < 10){
  //     _listValueEngineSoundMotorCycle.add(true);
  //     _listValueEngineSoundMotorCycle.add(false);
  //     _listValueEngineSoundMotorCycle.add(false);
  //   }else if(value > 10 && value <= 30){
  //     _listValueEngineSoundMotorCycle.add(false);
  //     _listValueEngineSoundMotorCycle.add(true);
  //     _listValueEngineSoundMotorCycle.add(false);
  //   }
  //   else{
  //     _listValueEngineSoundMotorCycle.add(false);
  //     _listValueEngineSoundMotorCycle.add(false);
  //     _listValueEngineSoundMotorCycle.add(true);
  //   }
  //   notifyListeners();
  // }
  //
  // void calculateEngineCleanMotorCycle(String data){
  //   int value = double.parse(data).round();
  //   _listValueEngineCleanMotorCycle.clear();
  //   if(value < 10){
  //     _listValueEngineCleanMotorCycle.add(true);
  //     _listValueEngineCleanMotorCycle.add(false);
  //     _listValueEngineCleanMotorCycle.add(false);
  //   }else if(value > 10 && value <= 30){
  //     _listValueEngineCleanMotorCycle.add(false);
  //     _listValueEngineCleanMotorCycle.add(true);
  //     _listValueEngineCleanMotorCycle.add(false);
  //   }
  //   else{
  //     _listValueEngineCleanMotorCycle.add(false);
  //     _listValueEngineCleanMotorCycle.add(false);
  //     _listValueEngineCleanMotorCycle.add(true);
  //   }
  //   notifyListeners();
  // }
  //
  // void calculateTires(String data){
  //   int value = double.parse(data).round();
  //   _listValueTires.clear();
  //   if(value < 10){
  //     _listValueTires.add(true);
  //     _listValueTires.add(false);
  //     _listValueTires.add(false);
  //   }else if(value > 10 && value <= 30){
  //     _listValueTires.add(false);
  //     _listValueTires.add(true);
  //     _listValueTires.add(false);
  //   }
  //   else{
  //     _listValueTires.add(false);
  //     _listValueTires.add(false);
  //     _listValueTires.add(true);
  //   }
  //   notifyListeners();
  // }
  //
  // void calculateChassis(String data){
  //   int value = double.parse(data).round();
  //   _listValueChassis.clear();
  //   if(value < 10){
  //     _listValueChassis.add(true);
  //     _listValueChassis.add(false);
  //     _listValueChassis.add(false);
  //   }else if(value > 10 && value <= 30){
  //     _listValueChassis.add(false);
  //     _listValueChassis.add(true);
  //     _listValueChassis.add(false);
  //   }
  //   else{
  //     _listValueChassis.add(false);
  //     _listValueChassis.add(false);
  //     _listValueChassis.add(true);
  //   }
  //   notifyListeners();
  // }
  //
  // void calculateCombinationMeter(String data){
  //   int value = double.parse(data).round();
  //   _listValueCombinationMeter.clear();
  //   if(value < 10){
  //     _listValueCombinationMeter.add(true);
  //     _listValueCombinationMeter.add(false);
  //     _listValueCombinationMeter.add(false);
  //   }else if(value > 10 && value <= 30){
  //     _listValueCombinationMeter.add(false);
  //     _listValueCombinationMeter.add(true);
  //     _listValueCombinationMeter.add(false);
  //   }
  //   else{
  //     _listValueCombinationMeter.add(false);
  //     _listValueCombinationMeter.add(false);
  //     _listValueCombinationMeter.add(true);
  //   }
  //   notifyListeners();
  // }
  //
  // void calculateCoverBodyPrimary(String data){
  //   int value = double.parse(data).round();
  //   _listValueCoverBodyPrimary.clear();
  //   if(value < 10){
  //     _listValueCoverBodyPrimary.add(true);
  //     _listValueCoverBodyPrimary.add(false);
  //     _listValueCoverBodyPrimary.add(false);
  //   }else if(value > 10 && value <= 30){
  //     _listValueCoverBodyPrimary.add(false);
  //     _listValueCoverBodyPrimary.add(true);
  //     _listValueCoverBodyPrimary.add(false);
  //   }
  //   else{
  //     _listValueCoverBodyPrimary.add(false);
  //     _listValueCoverBodyPrimary.add(false);
  //     _listValueCoverBodyPrimary.add(true);
  //   }
  //   notifyListeners();
  // }
  //
  // void calculateCoverStangKemudi(String data){
  //   int value = double.parse(data).round();
  //   _listValueCoverStangKemudi.clear();
  //   if(value < 10){
  //     _listValueCoverStangKemudi.add(true);
  //     _listValueCoverStangKemudi.add(false);
  //     _listValueCoverStangKemudi.add(false);
  //   }else if(value > 10 && value <= 30){
  //     _listValueCoverStangKemudi.add(false);
  //     _listValueCoverStangKemudi.add(true);
  //     _listValueCoverStangKemudi.add(false);
  //   }
  //   else{
  //     _listValueCoverStangKemudi.add(false);
  //     _listValueCoverStangKemudi.add(false);
  //     _listValueCoverStangKemudi.add(true);
  //   }
  //   notifyListeners();
  // }
  //
  // void calculateEmblem(String data){
  //   int value = double.parse(data).round();
  //   _listValueEmblem.clear();
  //   if(value < 10){
  //     _listValueEmblem.add(true);
  //     _listValueEmblem.add(false);
  //     _listValueEmblem.add(false);
  //   }else if(value > 10 && value <= 30){
  //     _listValueEmblem.add(false);
  //     _listValueEmblem.add(true);
  //     _listValueEmblem.add(false);
  //   }
  //   else{
  //     _listValueEmblem.add(false);
  //     _listValueEmblem.add(false);
  //     _listValueEmblem.add(true);
  //   }
  //   notifyListeners();
  // }
  //
  // void calculateFootStepBracket(String data){
  //   int value = double.parse(data).round();
  //   _listValueFootStepBracket.clear();
  //   if(value < 10){
  //     _listValueFootStepBracket.add(true);
  //     _listValueFootStepBracket.add(false);
  //     _listValueFootStepBracket.add(false);
  //   }else if(value > 10 && value <= 30){
  //     _listValueFootStepBracket.add(false);
  //     _listValueFootStepBracket.add(true);
  //     _listValueFootStepBracket.add(false);
  //   }
  //   else{
  //     _listValueFootStepBracket.add(false);
  //     _listValueFootStepBracket.add(false);
  //     _listValueFootStepBracket.add(true);
  //   }
  //   notifyListeners();
  // }
  //
  // void calculateJokMotor(String data){
  //   int value = double.parse(data).round();
  //   _listValueJokMotor.clear();
  //   if(value < 10){
  //     _listValueJokMotor.add(true);
  //     _listValueJokMotor.add(false);
  //     _listValueJokMotor.add(false);
  //   }else if(value > 10 && value <= 30){
  //     _listValueJokMotor.add(false);
  //     _listValueJokMotor.add(true);
  //     _listValueJokMotor.add(false);
  //   }
  //   else{
  //     _listValueJokMotor.add(false);
  //     _listValueJokMotor.add(false);
  //     _listValueJokMotor.add(true);
  //   }
  //   notifyListeners();
  // }
  //
  // void calculateKnalpot(String data){
  //   int value = double.parse(data).round();
  //   _listValueKnalpot.clear();
  //   if(value < 10){
  //     _listValueKnalpot.add(true);
  //     _listValueKnalpot.add(false);
  //     _listValueKnalpot.add(false);
  //   }else if(value > 10 && value <= 30){
  //     _listValueKnalpot.add(false);
  //     _listValueKnalpot.add(true);
  //     _listValueKnalpot.add(false);
  //   }
  //   else{
  //     _listValueKnalpot.add(false);
  //     _listValueKnalpot.add(false);
  //     _listValueKnalpot.add(true);
  //   }
  //   notifyListeners();
  // }
  //
  // void calculateLampuBelakang(String data){
  //   int value = double.parse(data).round();
  //   _listValueLampuBelakang.clear();
  //   if(value < 10){
  //     _listValueLampuBelakang.add(true);
  //     _listValueLampuBelakang.add(false);
  //     _listValueLampuBelakang.add(false);
  //   }else if(value > 10 && value <= 30){
  //     _listValueLampuBelakang.add(false);
  //     _listValueLampuBelakang.add(true);
  //     _listValueLampuBelakang.add(false);
  //   }
  //   else{
  //     _listValueLampuBelakang.add(false);
  //     _listValueLampuBelakang.add(false);
  //     _listValueLampuBelakang.add(true);
  //   }
  //   notifyListeners();
  // }
  //
  // void calculateLampuBesarDepan(String data){
  //   int value = double.parse(data).round();
  //   _listValueLampuBesarDepan.clear();
  //   if(value < 10){
  //     _listValueLampuBesarDepan.add(true);
  //     _listValueLampuBesarDepan.add(false);
  //     _listValueLampuBesarDepan.add(false);
  //   }else if(value > 10 && value <= 30){
  //     _listValueLampuBesarDepan.add(false);
  //     _listValueLampuBesarDepan.add(true);
  //     _listValueLampuBesarDepan.add(false);
  //   }
  //   else{
  //     _listValueLampuBesarDepan.add(false);
  //     _listValueLampuBesarDepan.add(false);
  //     _listValueLampuBesarDepan.add(true);
  //   }
  //   notifyListeners();
  // }
  //
  // void calculateLampuSein(String data){
  //   int value = double.parse(data).round();
  //   _listValueLampuSein.clear();
  //   if(value < 10){
  //     _listValueLampuSein.add(true);
  //     _listValueLampuSein.add(false);
  //     _listValueLampuSein.add(false);
  //   }else if(value > 10 && value <= 30){
  //     _listValueLampuSein.add(false);
  //     _listValueLampuSein.add(true);
  //     _listValueLampuSein.add(false);
  //   }
  //   else{
  //     _listValueLampuSein.add(false);
  //     _listValueLampuSein.add(false);
  //     _listValueLampuSein.add(true);
  //   }
  //   notifyListeners();
  // }
  //
  // void calculateWings(String data){
  //   int value = double.parse(data).round();
  //   _listValueWings.clear();
  //   if(value < 10){
  //     _listValueWings.add(true);
  //     _listValueWings.add(false);
  //     _listValueWings.add(false);
  //   }else if(value > 10 && value <= 30){
  //     _listValueWings.add(false);
  //     _listValueWings.add(true);
  //     _listValueWings.add(false);
  //   }
  //   else{
  //     _listValueWings.add(false);
  //     _listValueWings.add(false);
  //     _listValueWings.add(true);
  //   }
  //   notifyListeners();
  // }
  //
  // void calculateSpionMotorCycle(String data){
  //   int value = double.parse(data).round();
  //   _listValueSpionMotorCycle.clear();
  //   if(value < 10){
  //     _listValueSpionMotorCycle.add(true);
  //     _listValueSpionMotorCycle.add(false);
  //     _listValueSpionMotorCycle.add(false);
  //   }else if(value > 10 && value <= 30){
  //     _listValueSpionMotorCycle.add(false);
  //     _listValueSpionMotorCycle.add(true);
  //     _listValueSpionMotorCycle.add(false);
  //   }
  //   else{
  //     _listValueSpionMotorCycle.add(false);
  //     _listValueSpionMotorCycle.add(false);
  //     _listValueSpionMotorCycle.add(true);
  //   }
  //   notifyListeners();
  // }
  //
  // void calculateStarter(String data){
  //   int value = double.parse(data).round();
  //   _listValueStarter.clear();
  //   if(value < 10){
  //     _listValueStarter.add(true);
  //     _listValueStarter.add(false);
  //     _listValueStarter.add(false);
  //   }else if(value > 10 && value <= 30){
  //     _listValueStarter.add(false);
  //     _listValueStarter.add(true);
  //     _listValueStarter.add(false);
  //   }
  //   else{
  //     _listValueStarter.add(false);
  //     _listValueStarter.add(false);
  //     _listValueStarter.add(true);
  //   }
  //   notifyListeners();
  // }
  //
  // void calculateIronTankCover(String data){
  //   int value = double.parse(data).round();
  //   _listValueIronTankCover.clear();
  //   if(value < 10){
  //     _listValueIronTankCover.add(true);
  //     _listValueIronTankCover.add(false);
  //     _listValueIronTankCover.add(false);
  //   }else if(value > 10 && value <= 30){
  //     _listValueIronTankCover.add(false);
  //     _listValueIronTankCover.add(true);
  //     _listValueIronTankCover.add(false);
  //   }
  //   else{
  //     _listValueIronTankCover.add(false);
  //     _listValueIronTankCover.add(false);
  //     _listValueIronTankCover.add(true);
  //   }
  //   notifyListeners();
  // }
  //
  // void calculateBakBelakang(String data){
  //   int value = double.parse(data).round();
  //   _listValueBakBelakang.clear();
  //   if(value < 10){
  //     _listValueBakBelakang.add(true);
  //     _listValueBakBelakang.add(false);
  //     _listValueBakBelakang.add(false);
  //   }else if(value > 10 && value <= 30){
  //     _listValueBakBelakang.add(false);
  //     _listValueBakBelakang.add(true);
  //     _listValueBakBelakang.add(false);
  //   }
  //   else{
  //     _listValueBakBelakang.add(false);
  //     _listValueBakBelakang.add(false);
  //     _listValueBakBelakang.add(true);
  //   }
  //   notifyListeners();
  // }
  //
  // void calculateMachineAndBlokMachine(String data){
  //   int value = double.parse(data).round();
  //   _listValueMachineBlokMachine.clear();
  //   if(value < 10){
  //     _listValueMachineBlokMachine.add(true);
  //     _listValueMachineBlokMachine.add(false);
  //     _listValueMachineBlokMachine.add(false);
  //   }else if(value > 10 && value <= 30){
  //     _listValueMachineBlokMachine.add(false);
  //     _listValueMachineBlokMachine.add(true);
  //     _listValueMachineBlokMachine.add(false);
  //   }
  //   else{
  //     _listValueMachineBlokMachine.add(false);
  //     _listValueMachineBlokMachine.add(false);
  //     _listValueMachineBlokMachine.add(true);
  //   }
  //   notifyListeners();
  // }
  //
  // void calculateRadiatorWaterPump(String data){
  //   int value = double.parse(data).round();
  //   _listValueRadiatorWaterPump.clear();
  //   if(value < 10){
  //     _listValueRadiatorWaterPump.add(true);
  //     _listValueRadiatorWaterPump.add(false);
  //     _listValueRadiatorWaterPump.add(false);
  //   }else if(value > 10 && value <= 30){
  //     _listValueRadiatorWaterPump.add(false);
  //     _listValueRadiatorWaterPump.add(true);
  //     _listValueRadiatorWaterPump.add(false);
  //   }
  //   else{
  //     _listValueRadiatorWaterPump.add(false);
  //     _listValueRadiatorWaterPump.add(false);
  //     _listValueRadiatorWaterPump.add(true);
  //   }
  //   notifyListeners();
  // }
  //
  // void calculateKarburator(String data){
  //   int value = double.parse(data).round();
  //   _listValueKarburator.clear();
  //   if(value < 10){
  //     _listValueKarburator.add(true);
  //     _listValueKarburator.add(false);
  //     _listValueKarburator.add(false);
  //   }else if(value > 10 && value <= 30){
  //     _listValueKarburator.add(false);
  //     _listValueKarburator.add(true);
  //     _listValueKarburator.add(false);
  //   }
  //   else{
  //     _listValueKarburator.add(false);
  //     _listValueKarburator.add(false);
  //     _listValueKarburator.add(true);
  //   }
  //   notifyListeners();
  // }
  //
  // void calculateGearSetCVT(String data){
  //   int value = double.parse(data).round();
  //   _listValueGearSetCVT.clear();
  //   if(value < 10){
  //     _listValueGearSetCVT.add(true);
  //     _listValueGearSetCVT.add(false);
  //     _listValueGearSetCVT.add(false);
  //   }else if(value > 10 && value <= 30){
  //     _listValueGearSetCVT.add(false);
  //     _listValueGearSetCVT.add(true);
  //     _listValueGearSetCVT.add(false);
  //   }
  //   else{
  //     _listValueGearSetCVT.add(false);
  //     _listValueGearSetCVT.add(false);
  //     _listValueGearSetCVT.add(true);
  //   }
  //   notifyListeners();
  // }
  //
  // void calculateMechanicBreak(String data){
  //   int value = double.parse(data).round();
  //   _listValueMechanicBreak.clear();
  //   if(value < 10){
  //     _listValueMechanicBreak.add(true);
  //     _listValueMechanicBreak.add(false);
  //     _listValueMechanicBreak.add(false);
  //   }else if(value > 10 && value <= 30){
  //     _listValueMechanicBreak.add(false);
  //     _listValueMechanicBreak.add(true);
  //     _listValueMechanicBreak.add(false);
  //   }
  //   else{
  //     _listValueMechanicBreak.add(false);
  //     _listValueMechanicBreak.add(false);
  //     _listValueMechanicBreak.add(true);
  //   }
  //   notifyListeners();
  // }
  //
  // void calculateSwingArmBosh(String data){
  //   int value = double.parse(data).round();
  //   _listValueSwingArmBosh.clear();
  //   if(value < 10){
  //     _listValueSwingArmBosh.add(true);
  //     _listValueSwingArmBosh.add(false);
  //     _listValueSwingArmBosh.add(false);
  //   }else if(value > 10 && value <= 30){
  //     _listValueSwingArmBosh.add(false);
  //     _listValueSwingArmBosh.add(true);
  //     _listValueSwingArmBosh.add(false);
  //   }
  //   else{
  //     _listValueSwingArmBosh.add(false);
  //     _listValueSwingArmBosh.add(false);
  //     _listValueSwingArmBosh.add(true);
  //   }
  //   notifyListeners();
  // }
  //
  // void calculateShockAbsorber(String data){
  //   int value = double.parse(data).round();
  //   _listValueShockAbsorber.clear();
  //   if(value < 10){
  //     _listValueShockAbsorber.add(true);
  //     _listValueShockAbsorber.add(false);
  //     _listValueShockAbsorber.add(false);
  //   }else if(value > 10 && value <= 30){
  //     _listValueShockAbsorber.add(false);
  //     _listValueShockAbsorber.add(true);
  //     _listValueShockAbsorber.add(false);
  //   }
  //   else{
  //     _listValueShockAbsorber.add(false);
  //     _listValueShockAbsorber.add(false);
  //     _listValueShockAbsorber.add(true);
  //   }
  //   notifyListeners();
  // }
  //
  // void calculateStangForkAbsorber(String data){
  //   int value = double.parse(data).round();
  //   _listValueStangForkShockDepan.clear();
  //   if(value < 10){
  //     _listValueStangForkShockDepan.add(true);
  //     _listValueStangForkShockDepan.add(false);
  //     _listValueStangForkShockDepan.add(false);
  //   }else if(value > 10 && value <= 30){
  //     _listValueStangForkShockDepan.add(false);
  //     _listValueStangForkShockDepan.add(true);
  //     _listValueStangForkShockDepan.add(false);
  //   }
  //   else{
  //     _listValueStangForkShockDepan.add(false);
  //     _listValueStangForkShockDepan.add(false);
  //     _listValueStangForkShockDepan.add(true);
  //   }
  //   notifyListeners();
  // }
  //
  // void calculateVelg(String data){
  //   int value = double.parse(data).round();
  //   _listValueVelg.clear();
  //   if(value < 10){
  //     _listValueVelg.add(true);
  //     _listValueVelg.add(false);
  //     _listValueVelg.add(false);
  //   }else if(value > 10 && value <= 30){
  //     _listValueVelg.add(false);
  //     _listValueVelg.add(true);
  //     _listValueVelg.add(false);
  //   }
  //   else{
  //     _listValueVelg.add(false);
  //     _listValueVelg.add(false);
  //     _listValueVelg.add(true);
  //   }
  //   notifyListeners();
  // }

  List _listGlobal = [];
  void setStatus(){
    for(int i=0; i < this._listTaksasi.length; i++) {
      if(this._listTaksasi[i].CONTROLLER.text != "") {
        if(double.parse(this._listTaksasi[i].CONTROLLER.text) <= 10)  {
          this._listTaksasi[i].STATUS = [true, false, false];
        } else if(double.parse(this._listTaksasi[i].CONTROLLER.text) > 10 && double.parse(this._listTaksasi[i].CONTROLLER.text) <= 30){
          this._listTaksasi[i].STATUS = [false, true, false];
        } else{
          this._listTaksasi[i].STATUS = [false, false, true];
        }
      }
    }
    notifyListeners();
  }

  void clearTaksasiUnit(){
    this._listTaksasi.clear();
    this._controllerTotalNilaiBobot.clear();
    this._controllerEngineSound.clear();
    this._controllerMachineClean.clear();
    this._controllerAC.clear();
    this._controllerRadioTapeCD.clear();
    this._controllerPowerWindow.clear();
    this._controllerCentralLock.clear();
    this._controllerDashboard.clear();
    this._controllerJok.clear();
    this._controllerPlafond.clear();
    this._controllerAlarm.clear();
    this._controllerBumper.clear();
    this._controllerLampu.clear();
    this._controllerSpion.clear();
    this._controllerFender.clear();
    this._controllerTiresAndWheels.clear();
    this._controllerWindshield.clear();
    this._controllerFrameAndPole.clear();
    this._controllerHood.clear();
    this._controllerDoor.clear();
    this._controllerCarTrunk.clear();
    this._controllerBoxIronTubTank.clear();

    this._controllerEngineSoundMotorCycle.clear();
    this._controllerMachineCleanMotorCycle.clear();
    this._controllerTires.clear();
    this._controllerChassis.clear();
    this._controllerCombinationMeter.clear();
    this._controllerCoverBodyUtama.clear();
    this._controllerCoverStangKemudi.clear();
    this._controllerEmblem.clear();
    this._controllerFootStepBracket.clear();
    this._controllerJokMotor.clear();
    this._controllerKnalpot.clear();
    this._controllerLampuBelakang.clear();
    this._controllerLampuBesarDepan.clear();
    this._controllerLampuSein.clear();
    this._controllerWings.clear();
    this._controllerSpionMotor.clear();
    this._controllerStarter.clear();
    this._controllerIronTubAndCover.clear();
    this._controllerBakBelakang.clear();
    this._controllerMachineAndBlokMachine.clear();
    this._controllerRadiatorWaterPump.clear();
    this._controllerKarburator.clear();
    this._controllerGearSetChainCVT.clear();
    this._controllerMechanicBreak.clear();
    this._controllerSwingArmBosh.clear();
    this._controllerBackShockAbsorber.clear();
    this._controllerStangForkShock.clear();
    this._controllerVelg.clear();
  }

  List<TaksasiUnitModel> get listTaksasi => _listTaksasi;

  // Get List Taksasi
  Future<void> getListTaksasi(BuildContext context,int index) async {
    var _objectGroup = Provider.of<InformationCollateralChangeNotifier>(context, listen: false).groupObjectSelected != null ? Provider.of<InformationCollateralChangeNotifier>(context, listen: false).groupObjectSelected.KODE : "";
    // var _objectPurpose = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).objectPurposeSelected.id;
    var _objectUsage = Provider.of<InformationCollateralChangeNotifier>(context, listen: false).objectUsageSelected != null ? Provider.of<InformationCollateralChangeNotifier>(context, listen: false).objectUsageSelected.id : "";
    if(_listTaksasi.isEmpty){
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);
      var _body = jsonEncode({
        "P_OBJT_GROUP" : _objectGroup,
        "P_OBJT_PURPOSE" : _objectUsage
      });

      print("body taksasi = $_body");
      String _taksasiList = await storage.read(key: "TaksasiList");
      final _response = await _http.post(
          "${BaseUrl.urlGeneral}$_taksasiList",
          // "${urlPublic}api/taksasi/get-list-taksasi",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      );

      if(_response.statusCode == 200){
        List _result = jsonDecode(_response.body);
        print("cek result api taksasi $_result");
        List<bool> status = [false, false, false];
        if(_result.isNotEmpty){
          for(int i=0; i < _result.length; i++) {
            _listTaksasi.add(TaksasiUnitModel(_result[i]['TAKSASI_CODE'], _result[i]['TAKSASI_DESC'], "", status, TextEditingController(), TextEditingController(), false, GlobalKey<FormState>()));
          }
          if(_objectGroup != null){
            await setDataTaksasi(context,index);
          }
        }
      }
      else {
        showSnackBar("Error response status ${_response.statusCode}");
      }
    }
    // notifyListeners();
  }

  // Motor
  void calculatedMotorCycle(BuildContext context) async {
    var _objectUsage = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).objectUsageModel.id;
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);
    // var _data = [];
    // for(int i=0; i < _listTaksasi.length; i++) {
    //   _data.add({
    //     "${_listTaksasi[i].TAKSASI_DESC}" : "${_listTaksasi[i].CONTROLLER.text}"
    //   });
    // }

    // var mapData = {};
    // _listTaksasi.forEach((customer) => mapData[customer.TAKSASI_DESC] = customer.CONTROLLER.text);
    // var _body = jsonEncode(mapData);

    var _body = jsonEncode({
      "P_SUARAMESIN": _listTaksasi[0].CONTROLLER.text != "" ? "${_listTaksasi[0].CONTROLLER.text}" : "0",
      "P_KEBERSIHANMESIN": _listTaksasi[1].CONTROLLER.text != "" ? "${_listTaksasi[1].CONTROLLER.text}" : "0",
      "P_CHASIS": _listTaksasi[2].CONTROLLER.text != "" ? "${_listTaksasi[2].CONTROLLER.text}" : "0",
      "P_BANDEPANBELAKANG": _listTaksasi[3].CONTROLLER.text != "" ? "${_listTaksasi[3].CONTROLLER.text}" : "0",
      "P_COMBINATIONMETER": _listTaksasi[4].CONTROLLER.text != "" ? "${_listTaksasi[4].CONTROLLER.text}" : "0",
      "P_COVERBODY": _listTaksasi[5].CONTROLLER.text != "" ? "${_listTaksasi[5].CONTROLLER.text}" : "0",
      "P_COVERSTANG": _listTaksasi[6].CONTROLLER.text != "" ? "${_listTaksasi[6].CONTROLLER.text}" : "0",
      "P_EMBLEM": _listTaksasi[7].CONTROLLER.text != "" ? "${_listTaksasi[7].CONTROLLER.text}" : "0",
      "P_FOOTSTEP": _listTaksasi[8].CONTROLLER.text != "" ? "${_listTaksasi[8].CONTROLLER.text}" : "0",
      "P_JOK": _listTaksasi[9].CONTROLLER.text != "" ? "${_listTaksasi[9].CONTROLLER.text}" : "0",
      "P_KNALPOT": _listTaksasi[10].CONTROLLER.text != "" ? "${_listTaksasi[10].CONTROLLER.text}" : "0",
      "P_LAMPUBELAKANG": _listTaksasi[11].CONTROLLER.text != "" ? "${_listTaksasi[11].CONTROLLER.text}" : "0",
      "P_LAMPUBESAR": _listTaksasi[12].CONTROLLER.text != "" ? "${_listTaksasi[12].CONTROLLER.text}" : "0",
      "P_LAMPUSEIN": _listTaksasi[13].CONTROLLER.text != "" ? "${_listTaksasi[13].CONTROLLER.text}" : "0",
      "P_SAYAPBODY": _listTaksasi[14].CONTROLLER.text != "" ? "${_listTaksasi[14].CONTROLLER.text}" : "0",
      "P_SPION": _listTaksasi[15].CONTROLLER.text != "" ? "${_listTaksasi[15].CONTROLLER.text}" : "0",
      "P_STARTER": _listTaksasi[16].CONTROLLER.text != "" ? "${_listTaksasi[16].CONTROLLER.text}" : "0",
      "P_TANGKIBESI": _listTaksasi[17].CONTROLLER.text != "" ? "${_listTaksasi[17].CONTROLLER.text}" : "0",
      "P_BANBELAKANG": _listTaksasi[18].CONTROLLER.text != "" ? "${_listTaksasi[18].CONTROLLER.text}" : "0",
      "P_MESINBLOK": _listTaksasi[19].CONTROLLER.text != "" ? "${_listTaksasi[19].CONTROLLER.text}" : "0",
      "P_RADIATOR": _listTaksasi[20].CONTROLLER.text != "" ? "${_listTaksasi[20].CONTROLLER.text}" : "0",
      "P_KARBURATOR": _listTaksasi[21].CONTROLLER.text != "" ? "${_listTaksasi[21].CONTROLLER.text}" : "0",
      "P_GEARSET": _listTaksasi[22].CONTROLLER.text != "" ? "${_listTaksasi[22].CONTROLLER.text}" : "0",
      "P_MEKANIKREM": _listTaksasi[23].CONTROLLER.text != "" ? "${_listTaksasi[23].CONTROLLER.text}" : "0",
      "P_SHOCK": _listTaksasi[24].CONTROLLER.text != "" ? "${_listTaksasi[24].CONTROLLER.text}" : "0",
      "P_STANGFORK": _listTaksasi[25].CONTROLLER.text != "" ? "${_listTaksasi[25].CONTROLLER.text}" : "0",
      "P_SWINGARM": _listTaksasi[26].CONTROLLER.text != "" ? "${_listTaksasi[26].CONTROLLER.text}" : "0",
      "P_VELG": _listTaksasi[27].CONTROLLER.text != "" ? "${_listTaksasi[27].CONTROLLER.text}" : "0",
      "P_OBJT_PURPOSE": _objectUsage != "" ? _objectUsage != "02" ? "01" : _objectUsage : "0",
    });
    print(_body);
    String _taksasiMotor = await storage.read(key: "TaksasiMotor");
    final _response = await _http.post(
      "${BaseUrl.urlGeneral}$_taksasiMotor",
      // "${urlPublic}api/taksasi/get-nilai-taksasi-mcy",
      body: _body,
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );

    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      print('result : $_result');
      _listGlobal.clear();
      if(_result.isNotEmpty){
        _listGlobal.add(_result[0]['SUARAMESIN']);
        _listGlobal.add(_result[0]['KEBERSIHANMESIN']);
        _listGlobal.add(_result[0]['BANDEPANBELAKANG']);
        _listGlobal.add(_result[0]['CHASIS']);
        _listGlobal.add(_result[0]['COMBINATIONMETER']);
        _listGlobal.add(_result[0]['COVERBODY']);
        _listGlobal.add(_result[0]['COVERSTANG']);
        _listGlobal.add(_result[0]['EMBLEM']);
        _listGlobal.add(_result[0]['FOOTSTEP']);
        _listGlobal.add(_result[0]['JOK']);
        _listGlobal.add(_result[0]['KNALPOT']);
        _listGlobal.add(_result[0]['LAMPUBELAKANG']);
        _listGlobal.add(_result[0]['LAMPUBESAR']);
        _listGlobal.add(_result[0]['LAMPUSEIN']);
        _listGlobal.add(_result[0]['SAYAPBODY']);
        _listGlobal.add(_result[0]['SPION']);
        _listGlobal.add(_result[0]['STARTER']);
        _listGlobal.add(_result[0]['TANGKIBESI']);
        _listGlobal.add(_result[0]['BANBELAKANG']);
        _listGlobal.add(_result[0]['MESINBLOK']);
        _listGlobal.add(_result[0]['RADIATOR']);
        _listGlobal.add(_result[0]['KARBURATOR']);
        _listGlobal.add(_result[0]['GEARSET']);
        _listGlobal.add(_result[0]['MEKANIKREM']);
        _listGlobal.add(_result[0]['SWINGARM']);
        _listGlobal.add(_result[0]['SHOCK']);
        _listGlobal.add(_result[0]['STANGFORK']);
        _listGlobal.add(_result[0]['VELG']);
        this._totalNilaiBobot = 0;
        for(int i=0; i < this._listTaksasi.length; i++) {
          this._listTaksasi[i].CONTROLLER_NILAI_BOBOT.text = this._listGlobal[i];
          this._totalNilaiBobot = this._totalNilaiBobot + double.parse(this._listGlobal[i]);
        }
        this._controllerTotalNilaiBobot.text = formatCurrency.formatCurrencyPoint2(this._totalNilaiBobot.toString());
        setStatus();
      }
      this._loadData = false;
    } else {
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  // Mobil
  void calculatedCar(BuildContext context) async {
    var _objectPurpose = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).objectUsageModel.id;
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);
    var _body = jsonEncode({
      "P_SUARAMESIN": _listTaksasi[0].CONTROLLER.text != "" ? "${_listTaksasi[0].CONTROLLER.text}" : "0",
      "P_KEBERSIHANMESIN ": _listTaksasi[1].CONTROLLER.text != "" ? "${_listTaksasi[1].CONTROLLER.text}" : "0",
      "P_AC ": _listTaksasi[2].CONTROLLER.text != "" ? "${_listTaksasi[2].CONTROLLER.text}" : "0",
      "P_RADIO ": _listTaksasi[3].CONTROLLER.text != "" ? "${_listTaksasi[3].CONTROLLER.text}" : "0",
      "P_POWERWINDOW ": _listTaksasi[4].CONTROLLER.text != "" ? "${_listTaksasi[4].CONTROLLER.text}" : "0",
      "P_CENTRALLOCK ": _listTaksasi[5].CONTROLLER.text != "" ? "${_listTaksasi[5].CONTROLLER.text}" : "0",
      "P_DASHBOARD ": _listTaksasi[6].CONTROLLER.text != "" ? "${_listTaksasi[6].CONTROLLER.text}" : "0",
      "P_JOK ": _listTaksasi[7].CONTROLLER.text != "" ? "${_listTaksasi[7].CONTROLLER.text}" : "0",
      "P_PLAFOND ": _listTaksasi[8].CONTROLLER.text != "" ? "${_listTaksasi[8].CONTROLLER.text}" : "0",
      "P_ALARM ": _listTaksasi[9].CONTROLLER.text != "" ? "${_listTaksasi[9].CONTROLLER.text}" : "0",
      "P_BUMPER ": _listTaksasi[10].CONTROLLER.text != "" ? "${_listTaksasi[10].CONTROLLER.text}" : "0",
      "P_LAMPU ": _listTaksasi[11].CONTROLLER.text != "" ? "${_listTaksasi[11].CONTROLLER.text}" : "0",
      "P_SPION ": _listTaksasi[12].CONTROLLER.text != "" ? "${_listTaksasi[12].CONTROLLER.text}" : "0",
      "P_FENDER ": _listTaksasi[13].CONTROLLER.text != "" ? "${_listTaksasi[13].CONTROLLER.text}" : "0",
      "P_BANVELG ": _listTaksasi[14].CONTROLLER.text != "" ? "${_listTaksasi[14].CONTROLLER.text}" : "0",
      "P_KACA ": _listTaksasi[15].CONTROLLER.text != "" ? "${_listTaksasi[15].CONTROLLER.text}" : "0",
      "P_RANGKATILANG ": _listTaksasi[16].CONTROLLER.text != "" ? "${_listTaksasi[16].CONTROLLER.text}" : "0",
      "P_KAPMESIN ": _listTaksasi[17].CONTROLLER.text != "" ? "${_listTaksasi[17].CONTROLLER.text}" : "0",
      "P_PINTU ": _listTaksasi[18].CONTROLLER.text != "" ? "${_listTaksasi[18].CONTROLLER.text}" : "0",
      "P_BAGASI ": _listTaksasi[19].CONTROLLER.text != "" ? "${_listTaksasi[19].CONTROLLER.text}" : "0",
      "P_BOX ": _listTaksasi[20].CONTROLLER.text != "" ? "${_listTaksasi[20].CONTROLLER.text}" : "0",
      "P_OBJT_PURPOSE": _objectPurpose != "" ? _objectPurpose : "0",
    });
    String _taksasiMobil = await storage.read(key: "TaksasiMobil");
    final _response = await _http.post(
      "${BaseUrl.urlGeneral}$_taksasiMobil",
      // "${urlPublic}api/taksasi/get-nilai-taksasi-car",
      body: _body,
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );

    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      _listGlobal.clear();
      if(_result.isNotEmpty){
        _listGlobal.add(_result[0]['SUARAMESIN']);
        _listGlobal.add(_result[0]['KEBERSIHANMESIN']);
        _listGlobal.add(_result[0]['AC']);
        _listGlobal.add(_result[0]['RADIO']);
        _listGlobal.add(_result[0]['POWERWINDOW']);
        _listGlobal.add(_result[0]['CENTRALLOCK']);
        _listGlobal.add(_result[0]['DASHBOARD']);
        _listGlobal.add(_result[0]['JOK']);
        _listGlobal.add(_result[0]['PLAFOND']);
        _listGlobal.add(_result[0]['ALARM']);
        _listGlobal.add(_result[0]['BUMPER']);
        _listGlobal.add(_result[0]['LAMPU']);
        _listGlobal.add(_result[0]['SPION']);
        _listGlobal.add(_result[0]['FENDER']);
        _listGlobal.add(_result[0]['BANVELG']);
        _listGlobal.add(_result[0]['KACA']);
        _listGlobal.add(_result[0]['RANGKATILANG']);
        _listGlobal.add(_result[0]['KAPMESIN']);
        _listGlobal.add(_result[0]['PINTU']);
        _listGlobal.add(_result[0]['BAGASI']);
        _listGlobal.add(_result[0]['BOX']);
        this._totalNilaiBobot = 0;
        for(int i=0; i<_listTaksasi.length; i++) {
          this._listTaksasi[i].CONTROLLER_NILAI_BOBOT.text = this._listGlobal[i];
          this._totalNilaiBobot = this._totalNilaiBobot + double.parse(this._listGlobal[i]);
        }
        this._controllerTotalNilaiBobot.text = formatCurrency.formatCurrencyPoint2(this._totalNilaiBobot.toString());
        setStatus();
      }
      this._loadData = false;
    } else {
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  Future<void> saveToSQLite(BuildContext context) async{
    List<MS2ApplTaksasiModel> _listData = [];
    print("cek taksasi = ${this._listTaksasi.length}");
    for(int i=0; i <this._listTaksasi.length; i++){
      _listData.add(MS2ApplTaksasiModel(
          null,
          Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).objectUsageModel.id,
          Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).objectUsageModel.name,
          _listTaksasi[i].TAKSASI_CODE,
          _listTaksasi[i].TAKSASI_DESC,
          _listTaksasi[i].CONTROLLER.text.isNotEmpty ? double.parse(_listTaksasi[i].CONTROLLER.text) : 0,
          null,
          null,
          null,
          null,
          null
      ));
    }
    _dbHelper.insertMS2ApplTaksasi(_listData);
    String _message = await _submitDataPartial.submitTaksasi(11,_listData);
    return _message;
  }

  Future<bool> deleteSQLite() async{
    return await _dbHelper.deleteMS2ApplTaksasi();
  }

  void limitInput(int index, String value){
    if (int.parse(value) > 100) {
      Future.delayed(Duration(milliseconds: 100), () {
        this._listTaksasi[index].CONTROLLER.clear();
      });
    } else {
      setValidate(index);
    }
  }

  void setValidate(int index){
    this._listTaksasi[index].AUTOVALIDATE = true;
    notifyListeners();
  }

  Future<void> setDataTaksasi(BuildContext context,int index) async{
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _providerColla = Provider.of<InformationCollateralChangeNotifier>(context, listen: false);
    // if(kode == Provider.of<TaksasiUnitChangeNotifier>(context, listen: false).kode){
      List _data = await _dbHelper.selectTaksasi();
      print("set data taksasi = $_data");
      // if(_data.isNotEmpty){
      //   for(int i=0;i < _listTaksasi.length; i++){
      //     this._listTaksasi[i].CONTROLLER.text = _data[i]['nilai_input_rekondisi'].toString();
      //   }
      // }
      if(_data.isNotEmpty){
        for(int i=0;i < _listTaksasi.length; i++){
          for(int j=0; j<_data.length; j++){
            if(_listTaksasi[i].TAKSASI_CODE == _data[j]['taksasi_code']){
              this._listTaksasi[i].CONTROLLER.text = _data[i]['nilai_input_rekondisi'].toString();
            }
          }
        }
        if(_providerColla.groupObjectSelected.KODE == "002") {
          if(_providerColla.objectSelected.id != "003") {
            calculatedCar(context);
          }
        }
        else {
          if(_providerColla.objectSelected.id != "001") {
            calculatedMotorCycle(context);
          }
        }
        setStatus();
      }
    // }
    var _providerMarketingNotes = Provider.of<MarketingNotesChangeNotifier>(context,listen: false);
    if(_preferences.getString("last_known_state") == "IDE" && index != null){
      if(index != 11){
        await _providerMarketingNotes.setDataFromSQLite();
        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).isTaksasiUnitDone = true;
        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).selectedIndex +=1;
      }
      else{
        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).selectedIndex = 11;
      }
    }
    notifyListeners();
  }
}