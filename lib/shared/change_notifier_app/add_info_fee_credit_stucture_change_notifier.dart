import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/data_cl_model.dart';
import 'package:ad1ms2_dev/models/fee_type_model.dart';
import 'package:ad1ms2_dev/models/info_fee_credit_structure_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_struktur_kredit_biaya_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_application_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/dashboard/dashboard_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';
import 'package:ad1ms2_dev/shared/format_currency.dart';
import 'package:ad1ms2_dev/shared/resource/get_set_vme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AddInfoFeeCreditStructureChangeNotifier with ChangeNotifier{
  FeeTypeModel _feeTypeSelected;
  FeeTypeModel _feeTypeTemp;
  bool _autoValidate = false;
  bool _loadData = false;
  FormatCurrency _formatCurrency = FormatCurrency();
  TextEditingController _controllerCashCost = TextEditingController();
  TextEditingController _controllerCreditCost = TextEditingController();
  TextEditingController _controllerTotalCost = TextEditingController();
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  String _cashCostTemp,_creditCostTemp,_totalCostTemp;
  String _custType;
  String _lastKnownState;
  String _jenisPenawaran;
  DbHelper _dbHelper = DbHelper();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  double _minBiayaAdmin = 0.0;
  double _totalCost = 0.0;
  String _orderFeeID;
  bool _isDisablePACIA = false;
  DataCLModel _dataCLSelected;
  String _lmeId = "";

  bool _isEditDakor = false;
  bool _costTypeDakor = false;
  bool _cashDakor = false;
  bool _creditDakor = false;
  bool _totalPriceDakor = false;

  List<FeeTypeModel> _listFeeType = [
    FeeTypeModel("01", "BIAYA ADMIN"),
    FeeTypeModel("02", "BIAYA PROVISI"),
    FeeTypeModel("03", "MATERAI"),
    FeeTypeModel("04", "NOTARIS"),
    FeeTypeModel("05", "KREDIT"),
  ];

  FeeTypeModel get feeTypeSelected => _feeTypeSelected;

  set feeTypeSelected(FeeTypeModel value) {
    this._feeTypeSelected = value;
    if(value.id != "01") {
      if(this._controllerCashCost.text != null || this._controllerCashCost.text == "") {
        this._controllerCashCost.clear();
        this._controllerCreditCost.clear();
        this._controllerTotalCost.clear();
      }
    }
    notifyListeners();
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  FormatCurrency get formatCurrency => _formatCurrency;

  String get lmeId => _lmeId;

  set lmeId(String value) {
    this._lmeId = value;
    notifyListeners();
  }

  DataCLModel get dataCLSelected => _dataCLSelected;

  set dataCLSelected(DataCLModel value) {
    this._dataCLSelected = value;
    notifyListeners();
  }

  String get jenisPenawaran => _jenisPenawaran;

  set jenisPenawaran(String value) {
    this._jenisPenawaran = value;
    notifyListeners();
  }

  bool get isDisablePACIA => _isDisablePACIA;

  set isDisablePACIA(bool value) {
    this._isDisablePACIA = value;
    notifyListeners();
  }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
    notifyListeners();
  }

  double get minBiayaAdmin => _minBiayaAdmin;

  set minBiayaAdmin(double value) {
    this._minBiayaAdmin = value;
    notifyListeners();
  }

  double get totalCost => _totalCost;

  set totalCost(double value) {
    this._totalCost = value;
    notifyListeners();
  }

  TextEditingController get controllerCashCost => _controllerCashCost;

  TextEditingController get controllerCreditCost => _controllerCreditCost;

  TextEditingController get controllerTotalCost => _controllerTotalCost;

  GlobalKey<FormState> get key => _key;

  UnmodifiableListView<FeeTypeModel> get listFeeType {
    return UnmodifiableListView(this._listFeeType);
  }

  String get custType => _custType;

  set custType(String value) {
    this._custType = value;
    notifyListeners();
  }

  String get lastKnownState => _lastKnownState;

  set lastKnownState(String value) {
    this._lastKnownState = value;
    notifyListeners();
  }

  String get orderFeeID => _orderFeeID;

  set orderFeeID(String value) {
    this._orderFeeID = value;
  }

  // void checkValidateAdminFee(String value) async { //ga kepake
  //   if(this._feeTypeSelected.id == "01") {
  //     if(double.parse(value.replaceAll(",", "")) < this._minBiayaAdmin) {
  //       this._controllerCashCost.text = formatCurrency.formatCurrency(this._minBiayaAdmin.toString());
  //       calculateTotalCost();
  //       showSnackBar("Input Biaya Tunai tidak boleh lebih kecil dari Biaya Admin");
  //     }
  //   }
  // }

  void getBiayaAdmin(BuildContext context) async {
    print("getBiayaAdmin");
    this._loadData = true;
    // SharedPreferences _preference = await SharedPreferences.getInstance();
    var _providerRincianFoto = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
    var _providerInfoApplication = Provider.of<InfoAppChangeNotifier>(context, listen: false);
    var _providerInfoUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
    var _providerInfoCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false);

    try{
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);
      var _body = jsonEncode({
        "P_APPLICATION_DATE": "${_providerInfoApplication.controllerOrderDate.text}",
        "P_FIN_TYPE_ID": _custType == "PER" ? "${_providerRincianFoto.typeOfFinancingModelSelected.financingTypeId}" : "${_providerInfoUnitObject.typeOfFinancingModelSelected.financingTypeId}",
        "P_OBJECT_BRAND_ID": "${_providerInfoUnitObject.brandObjectSelected.id}",
        "P_OBJECT_ID": "${_providerInfoUnitObject.objectSelected.id}",
        "P_OBJECT_PROD_MATRIX_ID": "${_providerInfoUnitObject.prodMatrixId}",
        "P_OBJECT_TYPE_ID": "${_providerInfoUnitObject.objectTypeSelected.id}",
        "P_OTR": "${_providerInfoCreditStructure.controllerObjectPrice.text.replaceAll(",", "").split(".")[0]}",
        "P_TENOR": "${_providerInfoCreditStructure.periodOfTimeSelected}"
      });
      print("body: $_body");

      var storage = FlutterSecureStorage();
      String _biayaAdmin = await storage.read(key: "BiayaAdmin");
      final _response = await _http.post(
        "${BaseUrl.urlGeneral}$_biayaAdmin",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      );
      if(_response.statusCode == 200){
        var _result = jsonDecode(_response.body);
        var _data = _result['data'];
        print("data: $_data");
        if(_result.isNotEmpty){
          _minBiayaAdmin = double.parse(_data['P_MIN_ADMIN'].toString());
          this._controllerCashCost.text = formatCurrency.formatCurrency(_minBiayaAdmin.toString());
          var _cashCost = this._controllerCashCost.text.isNotEmpty ? double.parse(this._controllerCashCost.text.replaceAll(",", "")) : 0.0;
          var _creditCost = this._controllerCreditCost.text.isNotEmpty ? double.parse(this._controllerCreditCost.text.replaceAll(",", "")) : 0.0;
          var _totalCost = _cashCost + _creditCost;
          this._controllerTotalCost.text = formatCurrency.formatCurrency(_totalCost.toString());
          showDialog(
              context: context,
              barrierDismissible: false,
              builder: (BuildContext context){
                return Theme(
                  data: ThemeData(
                      fontFamily: "NunitoSans",
                      primaryColor: Colors.black,
                      primarySwatch: primaryOrange,
                      accentColor: myPrimaryColor
                  ),
                  child: AlertDialog(
                    title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
                    content: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text("Biaya Admin Minimal ${formatCurrency.formatCurrency(_minBiayaAdmin.toString())}",),
                      ],
                    ),
                    actions: <Widget>[
                      new FlatButton(
                        onPressed: () => Navigator.of(context).pop(true),
                        child: new Text('Close'),
                      ),
                      // new FlatButton(
                      //   onPressed: () => Navigator.of(context).pop(true),
                      //   child: new Text('Tidak'),
                      // ),
                    ],
                  ),
                );
              }
          );
        }
        this._loadData = false;
      } else {
        showSnackBar("Error response status ${_response.statusCode}");
        this._loadData = false;
      }
    }
    catch(e){
      loadData = false;
      showSnackBar(e.toString());
    }
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  void check(BuildContext context,int flag,int index){
    var _provider = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false);
    final _form = this._key.currentState;
    calculateTotalCost();
    if (flag == 0) {
      if (_form.validate()) {
        _provider.addInfoFeeCreditStructure(
            InfoFeeCreditStructureModel(
              this._feeTypeSelected,
              this._controllerCashCost.text,
              this._controllerCreditCost.text,
              this._controllerTotalCost.text,
              null,
              costTypeDakor,
              cashDakor,
              creditDakor,
              totalPriceDakor,
            )
        );
        // _provider.calculateCredit(context);
        Navigator.pop(context);
      } else {
        autoValidate = true;
      }
    }
    else {
      if (_form.validate()) {
        if(this._totalCost < this._minBiayaAdmin) {
          showSnackBar("Gagal ditambahkan, karena Total Biaya < Biaya Admin");
        } else {
          _provider.updateListInfoFeeCreditStructure(
              index,
              InfoFeeCreditStructureModel(
                this._feeTypeSelected,
                this._controllerCashCost.text,
                this._controllerCreditCost.text,
                this._controllerTotalCost.text,
                this._orderFeeID,
                costTypeDakor,
                cashDakor,
                creditDakor,
                totalPriceDakor,
              )
          );
          Navigator.pop(context);
        }
      } else {
        autoValidate = true;
      }
    }
  }

  Future<void> setValue(BuildContext context, InfoFeeCreditStructureModel value, int index, int flag) async {
    setPreference();
    getDataFromDashboard(context);
    var _providerInfoFeeCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false);

    if(flag != 0){
      print('edit info struktur kredit biaya');
      // set list
      for (int i = 0; i < this._listFeeType.length; i++) {
        if (value.feeTypeModel.id == this._listFeeType[i].id) {
          this._feeTypeSelected = this._listFeeType[i];
          this._feeTypeTemp = this._listFeeType[i];
        }
      }
      // hapus list
      for (int i = 0; i < this._listFeeType.length; i++) {
        print("value: ${value.feeTypeModel.id}");
        print("list biaya: ${this._listFeeType[i].id}");
        if (value.feeTypeModel.id != this._listFeeType[i].id) {
          this._listFeeType.removeAt(i);
        }
      }
      this._controllerTotalCost.text = "${value.totalCost}";
      this._totalCostTemp = this._controllerTotalCost.text;
      this._controllerCreditCost.text = "${value.creditCost}";
      this._creditCostTemp = this._controllerCreditCost.text;
      this._controllerCashCost.text = "${value.cashCost}";
      this._cashCostTemp = this._controllerCashCost.text;
      this._orderFeeID = value.orderFeeID;
      checkDataDakor(index);
    } else {
      for(int i=0; i < _providerInfoFeeCreditStructure.listInfoFeeCreditStructure.length; i++) {
        for(int j=0; j < this._listFeeType.length; j++){
          if(this._listFeeType[j].id == _providerInfoFeeCreditStructure.listInfoFeeCreditStructure[i].feeTypeModel.id){
            this._listFeeType.removeAt(j);
          }
        }
      }
    }
    formatting();
    calculateTotalCost();
  }

  get totalCostTemp => _totalCostTemp;

  get creditCostTemp => _creditCostTemp;

  String get cashCostTemp => _cashCostTemp;

  FeeTypeModel get feeTypeTemp => _feeTypeTemp;

  void formatting() {
    this._controllerCashCost.text = formatCurrency.formatCurrency(this._controllerCashCost.text);
    this._controllerCreditCost.text = formatCurrency.formatCurrency(this._controllerCreditCost.text);
    this._controllerTotalCost.text = formatCurrency.formatCurrency(this._controllerTotalCost.text);
  }

  void calculateTotalCost() {
    var _cashCost = this._controllerCashCost.text.isNotEmpty ? double.parse(this._controllerCashCost.text.replaceAll(",", "")) : 0.0;
    var _creditCost = this._controllerCreditCost.text.isNotEmpty ? double.parse(this._controllerCreditCost.text.replaceAll(",", "")) : 0.0;
    this._totalCost = _cashCost + _creditCost;
    if(this._feeTypeSelected.id != "01"){
      this._controllerTotalCost.text = _formatCurrency.formatCurrency(this._totalCost.toString());
    }
    else{
      checkAdminFee();
    }
  }

  void checkAdminFee(){
    this._controllerTotalCost.text = formatCurrency.formatCurrency(_minBiayaAdmin.toString());
    if(this._totalCost < this._minBiayaAdmin && this._controllerCreditCost.text.isNotEmpty && this._controllerCashCost.text.isNotEmpty) {
      this._controllerCashCost.clear();
      this._controllerCreditCost.clear();
      showSnackBar("Total Biaya tidak boleh lebih kecil dari Biaya Admin");
    }
    if(this._controllerCreditCost.text.isNotEmpty && this._controllerCashCost.text.isNotEmpty){
      this._controllerTotalCost.text = _formatCurrency.formatCurrency(this._totalCost.toString());
    }
  }

  // void checkDataDakor(int index) async{
  //   costTypeDakor = _feeTypeSelected.id !=
  //   cashDakor = _controllerCashCost.text !=
  //   creditDakor = _controllerCreditCost.text !=
  //   totalPriceDakor = _controllerTotalCost.text !=
  //
  //   if(costTypeDakor || cashDakor || creditDakor || totalPriceDakor){
  //    isEditDakor = true;
  //   } else {
  //     isEditDakor = false;
  //   }
  //   notifyListeners();
  // }

  bool get totalPriceDakor => _totalPriceDakor;

  set totalPriceDakor(bool value) {
    _totalPriceDakor = value;
    notifyListeners();
  }

  bool get creditDakor => _creditDakor;

  set creditDakor(bool value) {
    _creditDakor = value;
    notifyListeners();
  }

  bool get cashDakor => _cashDakor;

  set cashDakor(bool value) {
    _cashDakor = value;
    notifyListeners();
  }

  bool get costTypeDakor => _costTypeDakor;

  set costTypeDakor(bool value) {
    _costTypeDakor = value;
    notifyListeners();
  }

  bool get isEditDakor => _isEditDakor;

  set isEditDakor(bool value) {
    _isEditDakor = value;
    notifyListeners();
  }

  void setPreference() async {
    SharedPreferences _preference = await SharedPreferences.getInstance();
    this._custType = _preference.getString("cust_type");
    this._lastKnownState = _preference.getString("last_known_state");
    this._jenisPenawaran = _preference.getString("jenis_penawaran");
    if(this._lastKnownState == "PAC" || this._lastKnownState == "IA" || this._lastKnownState == "AOS" || this._lastKnownState == "CONA") {
      this._isDisablePACIA = true;
    }
  }

  ShowMandatoryInfoStrukturKreditBiayaModel _showMandatoryInfoStrukturKreditBiayaIdeModel;
  ShowMandatoryInfoStrukturKreditBiayaModel _showMandatoryInfoStrukturKreditBiayaIdeCompanyModel;
  void showMandatoryIdeModel(BuildContext context){
    _showMandatoryInfoStrukturKreditBiayaIdeModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditBiayaIdeModel;
    _showMandatoryInfoStrukturKreditBiayaIdeCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditBiayaIdeCompanyModel;
  }

  ShowMandatoryInfoStrukturKreditBiayaModel _showMandatoryInfoStrukturKreditBiayaRegulerSurveyModel;
  ShowMandatoryInfoStrukturKreditBiayaModel _showMandatoryInfoStrukturKreditBiayaRegulerSurveyCompanyModel;
  void showMandatoryRegulerSurveyModel(BuildContext context){
    _showMandatoryInfoStrukturKreditBiayaRegulerSurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditBiayaRegulerSurveyModel;
    _showMandatoryInfoStrukturKreditBiayaRegulerSurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditBiayaRegulerSurveyCompanyModel;
  }

  ShowMandatoryInfoStrukturKreditBiayaModel _showMandatoryInfoStrukturKreditBiayaPacModel;
  ShowMandatoryInfoStrukturKreditBiayaModel _showMandatoryInfoStrukturKreditBiayaPacCompanyModel;
  void showMandatoryPacModel(BuildContext context){
    _showMandatoryInfoStrukturKreditBiayaPacModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditBiayaPacModel;
    _showMandatoryInfoStrukturKreditBiayaPacCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditBiayaPacCompanyModel;
  }

  ShowMandatoryInfoStrukturKreditBiayaModel _showMandatoryInfoStrukturKreditBiayaResurveyModel;
  ShowMandatoryInfoStrukturKreditBiayaModel _showMandatoryInfoStrukturKreditBiayaResurveyCompanyModel;
  void showMandatoryResurveyModel(BuildContext context){
    _showMandatoryInfoStrukturKreditBiayaResurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditBiayaResurveyModel;
    _showMandatoryInfoStrukturKreditBiayaResurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditBiayaResurveyCompanyModel;
  }

  ShowMandatoryInfoStrukturKreditBiayaModel _showMandatoryInfoStrukturKreditBiayaDakorModel;
  ShowMandatoryInfoStrukturKreditBiayaModel _showMandatoryInfoStrukturKreditBiayaDakorCompanyModel;
  void showMandatoryDakorModel(BuildContext context){
    _showMandatoryInfoStrukturKreditBiayaDakorModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditBiayaDakorModel;
    _showMandatoryInfoStrukturKreditBiayaDakorCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditBiayaDakorCompanyModel;
  }

  void getDataFromDashboard(BuildContext context){
    showMandatoryIdeModel(context);
    showMandatoryRegulerSurveyModel(context);
    showMandatoryPacModel(context);
    showMandatoryResurveyModel(context);
    showMandatoryDakorModel(context);
  }

  bool isFeeTypeSelectedVisible() {
    bool value = false;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditBiayaIdeModel.isFeeTypeSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditBiayaRegulerSurveyModel.isFeeTypeSelectedVisible;
      } else if (_lastKnownState == "PAC" || _lastKnownState == "IA" || _lastKnownState == "AOS" || _lastKnownState == "CONA") {
        value = _showMandatoryInfoStrukturKreditBiayaPacModel.isFeeTypeSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditBiayaResurveyModel.isFeeTypeSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditBiayaDakorModel.isFeeTypeSelectedVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditBiayaIdeCompanyModel.isFeeTypeSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditBiayaRegulerSurveyCompanyModel.isFeeTypeSelectedVisible;
      } else if (_lastKnownState == "PAC" || _lastKnownState == "IA" || _lastKnownState == "AOS" || _lastKnownState == "CONA") {
        value = _showMandatoryInfoStrukturKreditBiayaPacCompanyModel.isFeeTypeSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditBiayaResurveyCompanyModel.isFeeTypeSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditBiayaDakorCompanyModel.isFeeTypeSelectedVisible;
      }
    }
    return value;
  }

  bool isCashCostVisible() {
    bool value = false;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditBiayaIdeModel.isCashCostVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditBiayaRegulerSurveyModel.isCashCostVisible;
      } else if (_lastKnownState == "PAC" || _lastKnownState == "IA" || _lastKnownState == "AOS" || _lastKnownState == "CONA") {
        value = _showMandatoryInfoStrukturKreditBiayaPacModel.isCashCostVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditBiayaResurveyModel.isCashCostVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditBiayaDakorModel.isCashCostVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditBiayaIdeCompanyModel.isCashCostVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditBiayaRegulerSurveyCompanyModel.isCashCostVisible;
      } else if (_lastKnownState == "PAC" || _lastKnownState == "IA" || _lastKnownState == "AOS" || _lastKnownState == "CONA") {
        value = _showMandatoryInfoStrukturKreditBiayaPacCompanyModel.isCashCostVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditBiayaResurveyCompanyModel.isCashCostVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditBiayaDakorCompanyModel.isCashCostVisible;
      }
    }
    return value;
  }

  bool isCreditCostVisible() {
    bool value = false;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditBiayaIdeModel.isCreditCostVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditBiayaRegulerSurveyModel.isCreditCostVisible;
      } else if (_lastKnownState == "PAC" || _lastKnownState == "IA" || _lastKnownState == "AOS" || _lastKnownState == "CONA") {
        value = _showMandatoryInfoStrukturKreditBiayaPacModel.isCreditCostVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditBiayaResurveyModel.isCreditCostVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditBiayaDakorModel.isCreditCostVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditBiayaIdeCompanyModel.isCreditCostVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditBiayaRegulerSurveyCompanyModel.isCreditCostVisible;
      } else if (_lastKnownState == "PAC" || _lastKnownState == "IA" || _lastKnownState == "AOS" || _lastKnownState == "CONA") {
        value = _showMandatoryInfoStrukturKreditBiayaPacCompanyModel.isCreditCostVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditBiayaResurveyCompanyModel.isCreditCostVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditBiayaDakorCompanyModel.isCreditCostVisible;
      }
    }
    return value;
  }

  bool isTotalCostVisible() {
    bool value = false;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditBiayaIdeModel.isTotalCostVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditBiayaRegulerSurveyModel.isTotalCostVisible;
      } else if (_lastKnownState == "PAC" || _lastKnownState == "IA" || _lastKnownState == "AOS" || _lastKnownState == "CONA") {
        value = _showMandatoryInfoStrukturKreditBiayaPacModel.isTotalCostVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditBiayaResurveyModel.isTotalCostVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditBiayaDakorModel.isTotalCostVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditBiayaIdeCompanyModel.isTotalCostVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditBiayaRegulerSurveyCompanyModel.isTotalCostVisible;
      } else if (_lastKnownState == "PAC" || _lastKnownState == "IA" || _lastKnownState == "AOS" || _lastKnownState == "CONA") {
        value = _showMandatoryInfoStrukturKreditBiayaPacCompanyModel.isTotalCostVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditBiayaResurveyCompanyModel.isTotalCostVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditBiayaDakorCompanyModel.isTotalCostVisible;
      }
    }
    return value;
  }

  bool isFeeTypeSelectedMandatory() {
    bool value = false;
    print("test $value");
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditBiayaIdeModel.isFeeTypeSelectedMandatory;
        print("ini val ${_showMandatoryInfoStrukturKreditBiayaIdeModel.isFeeTypeSelectedMandatory}");
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditBiayaRegulerSurveyModel.isFeeTypeSelectedMandatory;
        print("ini val 2 ${_showMandatoryInfoStrukturKreditBiayaIdeModel.isFeeTypeSelectedMandatory}");
      } else if (_lastKnownState == "PAC" || _lastKnownState == "IA" || _lastKnownState == "AOS" || _lastKnownState == "CONA") {
        value = _showMandatoryInfoStrukturKreditBiayaPacModel.isFeeTypeSelectedMandatory;
        print("ini val 3 ${_showMandatoryInfoStrukturKreditBiayaIdeModel.isFeeTypeSelectedMandatory}");
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditBiayaResurveyModel.isFeeTypeSelectedMandatory;
        print("ini val 4 ${_showMandatoryInfoStrukturKreditBiayaIdeModel.isFeeTypeSelectedMandatory}");
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditBiayaDakorModel.isFeeTypeSelectedMandatory;
        print("ini val 5 ${_showMandatoryInfoStrukturKreditBiayaIdeModel.isFeeTypeSelectedMandatory}");
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditBiayaIdeCompanyModel.isFeeTypeSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditBiayaRegulerSurveyCompanyModel.isFeeTypeSelectedMandatory;
      } else if (_lastKnownState == "PAC" || _lastKnownState == "IA" || _lastKnownState == "AOS" || _lastKnownState == "CONA") {
        value = _showMandatoryInfoStrukturKreditBiayaPacCompanyModel.isFeeTypeSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditBiayaResurveyCompanyModel.isFeeTypeSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditBiayaDakorCompanyModel.isFeeTypeSelectedMandatory;
      }
    }
    return value;
  }

  bool isCashCostMandatory() {
    bool value = false;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditBiayaIdeModel.isCashCostMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditBiayaRegulerSurveyModel.isCashCostMandatory;
      } else if (_lastKnownState == "PAC" || _lastKnownState == "IA" || _lastKnownState == "AOS" || _lastKnownState == "CONA") {
        value = _showMandatoryInfoStrukturKreditBiayaPacModel.isCashCostMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditBiayaResurveyModel.isCashCostMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditBiayaDakorModel.isCashCostMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditBiayaIdeCompanyModel.isCashCostMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditBiayaRegulerSurveyCompanyModel.isCashCostMandatory;
      } else if (_lastKnownState == "PAC" || _lastKnownState == "IA" || _lastKnownState == "AOS" || _lastKnownState == "CONA") {
        value = _showMandatoryInfoStrukturKreditBiayaPacCompanyModel.isCashCostMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditBiayaResurveyCompanyModel.isCashCostMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditBiayaDakorCompanyModel.isCashCostMandatory;
      }
    }
    return value;
  }

  bool isCreditCostMandatory() {
    bool value = false;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditBiayaIdeModel.isCreditCostMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditBiayaRegulerSurveyModel.isCreditCostMandatory;
      } else if (_lastKnownState == "PAC" || _lastKnownState == "IA" || _lastKnownState == "AOS" || _lastKnownState == "CONA") {
        value = _showMandatoryInfoStrukturKreditBiayaPacModel.isCreditCostMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditBiayaResurveyModel.isCreditCostMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditBiayaDakorModel.isCreditCostMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditBiayaIdeCompanyModel.isCreditCostMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditBiayaRegulerSurveyCompanyModel.isCreditCostMandatory;
      } else if (_lastKnownState == "PAC" || _lastKnownState == "IA" || _lastKnownState == "AOS" || _lastKnownState == "CONA") {
        value = _showMandatoryInfoStrukturKreditBiayaPacCompanyModel.isCreditCostMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditBiayaResurveyCompanyModel.isCreditCostMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditBiayaDakorCompanyModel.isCreditCostMandatory;
      }
    }
    return value;
  }

  bool isTotalCostMandatory() {
    bool value = false;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditBiayaIdeModel.isTotalCostMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditBiayaRegulerSurveyModel.isTotalCostMandatory;
      } else if (_lastKnownState == "PAC" || _lastKnownState == "IA" || _lastKnownState == "AOS" || _lastKnownState == "CONA") {
        value = _showMandatoryInfoStrukturKreditBiayaPacModel.isTotalCostMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditBiayaResurveyModel.isTotalCostMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditBiayaDakorModel.isTotalCostMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditBiayaIdeCompanyModel.isTotalCostMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditBiayaRegulerSurveyCompanyModel.isTotalCostMandatory;
      } else if (_lastKnownState == "PAC" || _lastKnownState == "IA" || _lastKnownState == "AOS" || _lastKnownState == "CONA") {
        value = _showMandatoryInfoStrukturKreditBiayaPacCompanyModel.isTotalCostMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditBiayaResurveyCompanyModel.isTotalCostMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditBiayaDakorCompanyModel.isTotalCostMandatory;
      }
    }
    return value;
  }

  void checkDataDakor(int index) async{
    var _check = await _dbHelper.selectMS2ApplFee();
    if(_check.isNotEmpty && _lastKnownState == "DKR"){
      costTypeDakor = this._feeTypeSelected.id != _check[index]['fee_type'] || _check[index]['edit_fee_type'] == "1";
      cashDakor =  double.parse(this._controllerCashCost.text.replaceAll(",", "")) != _check[index]['fee_cash'] || _check[index]['edit_fee_cash'] == "1";
      creditDakor = double.parse(this._controllerCreditCost.text.replaceAll(",", "")) != _check[index]['fee_credit'] || _check[index]['edit_fee_credit'] == "1";
      totalPriceDakor = double.parse(this._controllerTotalCost.text.replaceAll(",", "")) != _check[index]['total_fee'] || _check[index]['edit_total_fee'] == "1";
    }
  }

  void getDataCL(BuildContext context) async {
    var _date = dateFormat3.format(DateTime.now());
    SharedPreferences _preference = await SharedPreferences.getInstance();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
    var _oid = Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false).obligorID.toString();
    List _dataCL = await _dbHelper.selectMS2LME();

    if(int.parse(_dataCL[0]['pencairan_ke']) > 1) {
      _minBiayaAdmin = 0;
      this._controllerCashCost.text = formatCurrency.formatCurrency(_minBiayaAdmin.toString());
      var _cashCost = this._controllerCashCost.text.isNotEmpty ? double.parse(this._controllerCashCost.text.replaceAll(",", "")) : 0.0;
      var _creditCost = this._controllerCreditCost.text.isNotEmpty ? double.parse(this._controllerCreditCost.text.replaceAll(",", "")) : 0.0;
      var _totalCost = _cashCost + _creditCost;
      this._controllerTotalCost.text = formatCurrency.formatCurrency(_totalCost.toString());
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context){
            return Theme(
              data: ThemeData(
                  fontFamily: "NunitoSans",
                  primaryColor: Colors.black,
                  primarySwatch: primaryOrange,
                  accentColor: myPrimaryColor
              ),
              child: AlertDialog(
                title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
                content: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text("Biaya Admin Minimal ${formatCurrency.formatCurrency(_minBiayaAdmin.toString())}",),
                  ],
                ),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () => Navigator.of(context).pop(true),
                    child: new Text('Close'),
                  ),
                ],
              ),
            );
          }
      );
      this._loadData = false;
    }
    else {
      var _body = jsonEncode({
        "lme_id": _dataCL[0]['lme_id'],
        "source_reff_id": "MS2${_preference.getString("username")}-$_oid",
        "req_date": _date.toString(),
        "source_channel_id": "MS2"
      });
      print("body cek data cl: $_body");

      String _getDataCL = await storage.read(key: "GetDataCL");
      try {
        final _response = await _http.post(
            "${BaseUrl.urlLME}$_getDataCL",
            body: _body,
            headers: {
              "Content-Type":"application/json",
              "Authorization":"bearer $token",
            }
        ).timeout(Duration(seconds: 10));

        if(_response.statusCode == 200){
          final _result = jsonDecode(_response.body);
          this._dataCLSelected = DataCLModel(
            _result['cl_data']['jenis_kegiatan_usaha'].toString(),
            _result['cl_data']['biaya_hari_berjalan'].toString(),
            _result['cl_data']['biaya_admin_angsuran'].toString(),
            _result['cl_data']['biaya_simpan_bpkb'].toString(),
            _result['cl_data']['biaya_admin_restruktur'].toString(),
            _result['cl_data']['biaya_total'].toString(),
            _result['cl_data']['sisa_ph'].toString(),
            _result['cl_data']['denda'].toString(),
            _result['cl_data']['sisa_tenor'].toString(),
            _result['cl_data']['jumlah_pelunasan'].toString(),
            _result['cl_data']['jumlah_tunggakan'].toString(),
            _result['cl_data']['bunga_hari_berjalan'].toString(),
            _result['cl_data']['bunga_dihapuskan'].toString(),
            _result['cl_data']['pinalty_plus'].toString(),
            _result['cl_data']['max_plafon_cl'].toString(),
            _result['cl_data']['nilai_aktivasi'].toString(),
            _result['cl_data']['tenor_max'].toString(),
            _result['cl_data']['tenor'].toString(),
            _result['cl_data']['min_pencairan'].toString(),
            _result['cl_data']['biaya_fidusia'].toString(),
            _result['cl_data']['biaya_asuransi'].toString(),
            _result['cl_data']['biaya_administrasi'].toString(),
            _result['cl_data']['total_kewajiban'].toString(),
            _result['cl_data']['total_titipan'].toString(),
            _result['cl_data']['total_bayar'].toString(),
            _result['cl_data']['total_pencairan'].toString(),
          );
          _minBiayaAdmin = double.parse(_result['cl_data']['biaya_fidusia'].toString()) + double.parse(_result['cl_data']['biaya_administrasi'].toString());
          this._controllerCashCost.text = formatCurrency.formatCurrency(_minBiayaAdmin.toString());
          var _cashCost = this._controllerCashCost.text.isNotEmpty ? double.parse(this._controllerCashCost.text.replaceAll(",", "")) : 0.0;
          var _creditCost = this._controllerCreditCost.text.isNotEmpty ? double.parse(this._controllerCreditCost.text.replaceAll(",", "")) : 0.0;
          var _totalCost = _cashCost + _creditCost;
          this._controllerTotalCost.text = formatCurrency.formatCurrency(_totalCost.toString());
          showDialog(
              context: context,
              barrierDismissible: false,
              builder: (BuildContext context){
                return Theme(
                  data: ThemeData(
                      fontFamily: "NunitoSans",
                      primaryColor: Colors.black,
                      primarySwatch: primaryOrange,
                      accentColor: myPrimaryColor
                  ),
                  child: AlertDialog(
                    title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
                    content: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text("Biaya Admin Minimal ${formatCurrency.formatCurrency(_minBiayaAdmin.toString())}",),
                      ],
                    ),
                    actions: <Widget>[
                      new FlatButton(
                        onPressed: () => Navigator.of(context).pop(true),
                        child: new Text('Close'),
                      ),
                    ],
                  ),
                );
              }
          );
          this._loadData = false;
        }
        else{
          showSnackBar("Error get data cl response ${_response.statusCode}");
          this._loadData = false;
        }
      }
      on TimeoutException catch(_){
        showSnackBar("Request Timeout");
        this._loadData = false;
      }
      catch(e) {
        showSnackBar("Error $e");
        this._loadData = false;
      }
    }
  }
}