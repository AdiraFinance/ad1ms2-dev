import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/additional_insurance_model.dart';
import 'package:ad1ms2_dev/models/company_model.dart';
import 'package:ad1ms2_dev/models/coverage_type_model.dart';
import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:ad1ms2_dev/models/insurance_type_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_insurance_model.dart';
import 'package:ad1ms2_dev/models/product_model.dart';
import 'package:ad1ms2_dev/shared/format_currency.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';
import 'info_major_insurance_change_notifier.dart';
import 'information_collatelar_change_notifier.dart';

class InfoAdditionalInsuranceChangeNotifier with ChangeNotifier{
    List<AdditionalInsuranceModel> _listAdditionalInsurance = [];
    bool _autoValidate = false;
    bool _flag = false;
    bool _isDurable = false;
    DbHelper _dbHelper = DbHelper();
    List<AdditionalInsuranceModel> get listFormAdditionalInsurance => _listAdditionalInsurance;
    bool _type = false;
    FormatCurrency _formatCurrency = FormatCurrency();
    bool _isCheckData = false;
    String _custType;
    String _lastKnownState;
    bool _disableJenisPenawaran = false;

    String get custType => _custType;

    set custType(String value) {
        this._custType = value;
        notifyListeners();
    }

    String get lastKnownState => _lastKnownState;

    set lastKnownState(String value) {
        this._lastKnownState = value;
        notifyListeners();
    }

    bool get disableJenisPenawaran => _disableJenisPenawaran;

    set disableJenisPenawaran(bool value) {
        this._disableJenisPenawaran = value;
        notifyListeners();
    }

    bool get isCheckData => _isCheckData;

    set isCheckData(bool value) {
        this._isCheckData = value;
        notifyListeners();
    }

    FormatCurrency get formatCurrency => _formatCurrency;

    bool get autoValidate => _autoValidate;

    set autoValidate(bool value) {
        this._autoValidate = value;
        notifyListeners();
    }

    bool get type => _type;

    set type(bool value) {
      this._type = value;
    }

    void getDataColla(BuildContext context){//ga kepake
        _isDurable = Provider.of<InformationCollateralChangeNotifier>(context, listen: false).collateralTypeModel.id == "003";
        if(Provider.of<InformationCollateralChangeNotifier>(context, listen: false).collateralTypeModel.id != "003"){
            this._type = Provider.of<InformationCollateralChangeNotifier>(context, listen: false).groupObjectSelected.KODE == "002";
        } else {
            this._type = false;
        }
    }

    void getMandatory(BuildContext context){
        var _provider = Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false);
        this._type = false;
        for(int i=0;i<_provider.listMandatoryAsuransi.length;i++){
            if(_provider.listMandatoryAsuransi[i] == "Tambahan"){
                this._type = true;
            }
        }
    }

    void deleteListInfoAdditionalInsurance(BuildContext context, int index) {
        showDialog(
            context: context,
            barrierDismissible: true,
            builder: (BuildContext context){
                return Theme(
                    data: ThemeData(
                        fontFamily: "NunitoSans",
                        primaryColor: Colors.black,
                        primarySwatch: primaryOrange,
                        accentColor: myPrimaryColor
                    ),
                    child: AlertDialog(
                        title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
                        content: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                                Text("Apakah kamu yakin menghapus asuransi utama ini?",),
                            ],
                        ),
                        actions: <Widget>[
                            new FlatButton(
                                onPressed: () {
                                    this._listAdditionalInsurance.removeAt(index);
                                    notifyListeners();
                                    Navigator.pop(context);
                                },
                                child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                            ),
                            new FlatButton(
                                onPressed: () => Navigator.of(context).pop(true),
                                child: new Text('Tidak'),
                            ),
                        ],
                    ),
                );
            }
        );
    }

    void updateListInfoAdditionalInsurance(AdditionalInsuranceModel mInfoAdditionalInsurance, BuildContext context, int index) {
        this._listAdditionalInsurance[index] = mInfoAdditionalInsurance;
        notifyListeners();
    }

    void addListInfoAdditionalInsurance(AdditionalInsuranceModel value) {
        _listAdditionalInsurance.add(value);
        if (this._autoValidate) autoValidate = false;
        notifyListeners();
    }

    void clearListAdditionalInsurance(){
        this._autoValidate = false;
        this._flag = false;
        this._listAdditionalInsurance.clear();
        this._isCheckData = false;
        disableJenisPenawaran = false;
    }

    bool get flag => _flag;

    set flag(bool value) {
        this._flag = value;
        notifyListeners();
    }

    Future<bool> onBackPress() async{
        if(_listAdditionalInsurance.isNotEmpty) {
            flag = false;
            this._isCheckData = true;
        }
        else{
            if(this._type){
                flag = true;
                this._isCheckData = false;
            }
            else{
                flag = false;
                this._isCheckData = true;
            }
        }
        // if (_listAdditionalInsurance.length != 0) {
        //     flag = false;
        // } else {
        //     if(_type){
        //         flag = true;
        //     } else {
        //         flag = false;
        //     }
        // }
        // if(_isDurable){
        //     flag = false;
        // }
        return true;
    }

    Future<void> setPreference() async {
        SharedPreferences _preference = await SharedPreferences.getInstance();
        this._custType = _preference.getString("cust_type");
        this._lastKnownState = _preference.getString("last_known_state");
        this._disableJenisPenawaran = false;
        if(_preference.getString("jenis_penawaran") == "002"){
            this._disableJenisPenawaran = true;
        }
        notifyListeners();
    }

    Future<void> saveToSQLite() async {
        print("save asuransi tambahan");
        List<MS2ApplInsuranceModel> _listApplInsurance = [];
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        if(_listAdditionalInsurance.isNotEmpty) {
            for(int i=0; i<_listAdditionalInsurance.length; i++){
                _listApplInsurance.add(MS2ApplInsuranceModel(
                    "",
                    this._listAdditionalInsurance[i].orderProductInsuranceID != null ? this._listAdditionalInsurance[i].orderProductInsuranceID : "NEW",
                    this._listAdditionalInsurance[i].colaType,
                    null,
                    this._listAdditionalInsurance[i].insuranceType != null ? this._listAdditionalInsurance[i].insuranceType.KODE : "",
                    this._listAdditionalInsurance[i].company != null ? this._listAdditionalInsurance[i].company.KODE : "",
                    this._listAdditionalInsurance[i].product != null ? this._listAdditionalInsurance[i].product.KODE : "",
                    this._listAdditionalInsurance[i].product != null ? this._listAdditionalInsurance[i].product.DESKRIPSI : "",
                    this._listAdditionalInsurance[i].periodType != "" ? int.parse(this._listAdditionalInsurance[i].periodType) : 0,
                    null,
                    null,
                    null,
                    null,
                    null,
                    this._listAdditionalInsurance[i].coverageType != null ? this._listAdditionalInsurance[i].coverageType.KODE : "",
                    this._listAdditionalInsurance[i].coverageType != null ? this._listAdditionalInsurance[i].coverageType.DESKRIPSI : "",
                    this._listAdditionalInsurance[i].coverageValue != "" ? double.parse(this._listAdditionalInsurance[i].coverageValue.replaceAll(",", "")) : 0,
                    this._listAdditionalInsurance[i].upperLimitRate != "" ? double.parse(this._listAdditionalInsurance[i].upperLimitRate) : 0,
                    this._listAdditionalInsurance[i].upperLimit != "" ? double.parse(this._listAdditionalInsurance[i].upperLimit.replaceAll(",", "")) : 0,
                    this._listAdditionalInsurance[i].lowerLimitRate != "" ? double.parse(this._listAdditionalInsurance[i].lowerLimitRate) : 0,
                    this._listAdditionalInsurance[i].lowerLimit != "" ? double.parse(this._listAdditionalInsurance[i].lowerLimit.replaceAll(",", "")) : 0,
                    this._listAdditionalInsurance[i].priceCash != "" ? double.parse(this._listAdditionalInsurance[i].priceCash.replaceAll(",", "")) : 0,
                    this._listAdditionalInsurance[i].priceCredit != "" ? double.parse(this._listAdditionalInsurance[i].priceCredit.replaceAll(",", "")) : 0,
                    null,
                    this._listAdditionalInsurance[i].totalPriceRate != "" ? double.parse(this._listAdditionalInsurance[i].totalPriceRate.replaceAll(",", "")) : 0,
                    this._listAdditionalInsurance[i].totalPrice != "" ? double.parse(this._listAdditionalInsurance[i].totalPrice.replaceAll(",", "")) : 0,
                    DateTime.now().toString(),
                    _preferences.getString("username"),
                    null,
                    null,
                    1,
                    i+1,
                    this._listAdditionalInsurance[i].insuranceType != null ? this._listAdditionalInsurance[i].insuranceType.DESKRIPSI : "",
                    this._listAdditionalInsurance[i].insuranceType != null ? this._listAdditionalInsurance[i].insuranceType.PARENT_KODE : "",
                    this._listAdditionalInsurance[i].company != null ? this._listAdditionalInsurance[i].company.DESKRIPSI : "",
                    this._listAdditionalInsurance[i].company != null ? this._listAdditionalInsurance[i].company.PARENT_KODE.toString() : "",
                    null,
                    null,
                    null,
                    null,
                    this._listAdditionalInsurance[i].insuranceTypeDakor ? "1" : "0",
                    null,
                    this._listAdditionalInsurance[i].companyDakor ? "1" : "0",
                    this._listAdditionalInsurance[i].productDakor ? "1" : "0",
                    this._listAdditionalInsurance[i].periodDakor ? "1" : "0",
                    null,
                    null,
                    null,
                    null,
                    this._listAdditionalInsurance[i].coverageTypeDakor ? "1" : "0",
                    this._listAdditionalInsurance[i].coverageValueDakor ? "1" : "0",
                    this._listAdditionalInsurance[i].upperLimitRateDakor ? "1" : "0",
                    this._listAdditionalInsurance[i].upperLimitValueDakor ? "1" : "0",
                    this._listAdditionalInsurance[i].lowerLimitRateDakor ? "1" : "0",
                    this._listAdditionalInsurance[i].lowerLimitValueDakor ? "1" : "0",
                    this._listAdditionalInsurance[i].cashDakor ? "1" : "0",
                    this._listAdditionalInsurance[i].creditDakor ? "1" : "0",
                    null,
                    this._listAdditionalInsurance[i].totalPriceRateDakor ? "1" : "0",
                    this._listAdditionalInsurance[i].totalPriceDakor ? "1" : "0",
                ));
            }
        }
        _dbHelper.insertMS2ApplInsuranceAdditional(_listApplInsurance);
    }

    Future<bool> deleteSQLite() async{
        return await _dbHelper.deleteMS2ApplInsuranceAdditional();
    }

    Future<void> setDataFromSQLite(BuildContext context) async{
        var _check = await _dbHelper.selectMS2ApplInsuranceAdditional();
        print("cek data asuransi tambahan = $_check");
        if(_check.isNotEmpty){
            for(int i=0; i<_check.length; i++){
                InsuranceTypeModel insurance = InsuranceTypeModel(_check[i]['insuran_type_parent'].toString(), _check[i]['insuran_type'].toString(), _check[i]['insuran_type_desc']);
                CompanyTypeInsuranceModel company = CompanyTypeInsuranceModel(0, _check[i]['company_id'], _check[i]['company_desc']);
                ProductModel product = ProductModel(_check[i]['product'], _check[i]['product_desc']);
                CoverageTypeModel coverageType = CoverageTypeModel(_check[i]['coverage_type'], _check[i]['coverage_type_desc']);
                _listAdditionalInsurance.add(AdditionalInsuranceModel(insurance, _check[i]['cola_type'].toString(), company, product, _check[i]['period'].toString(),
                    coverageType,
                    _check[i]['coverage_amt'].toString(),
                    _check[i]['upper_limit_pct'].toString(),
                    _check[i]['lower_limit_pct'].toString(),
                    _check[i]['upper_limit_amt'].toString(),
                    _check[i]['lower_limit_amt'].toString(),
                    _check[i]['cash_amt'].toString(),
                    _check[i]['credit_amt'].toString(),
                    _check[i]['total_pct'].toString(),
                    _check[i]['total_amt'].toString(),
                    _check[i]['edit_insuran_type_parent'] == "1" || _check[i]['edit_company_id'] == "1" ||
                        _check[i]['edit_product'] == "1" || _check[i]['edit_period'] == "1" || _check[i]['edit_coverage_type'] == "1" ||
                        _check[i]['edit_coverage_amt'] == "1" || _check[i]['edit_upper_limit_pct'] == "1" ||
                        _check[i]['edit_upper_limit_amt'] == "1" || _check[i]['edit_lower_limit_pct'] == "1" || _check[i]['edit_lower_limit_amt'] == "1" ||
                        _check[i]['edit_cash_amt'] == "1" || _check[i]['edit_credit_amt'] == "1" || _check[i]['edit_total_pct'] == "1" ||
                        _check[i]['edit_total_amt'] == "1",
                    _check[i]['edit_insuran_type_parent'] == "1",
                    _check[i]['edit_company_id'] == "1",
                    _check[i]['edit_product'] == "1",
                    _check[i]['edit_period'] == "1",
                    _check[i]['edit_coverage_type'] == "1",
                    _check[i]['edit_coverage_amt'] == "1",
                    _check[i]['edit_upper_limit_pct'] == "1",
                    _check[i]['edit_upper_limit_amt'] == "1",
                    _check[i]['edit_lower_limit_pct'] == "1",
                    _check[i]['edit_lower_limit_amt'] == "1",
                    _check[i]['edit_cash_amt'] == "1",
                    _check[i]['edit_credit_amt'] == "1",
                    _check[i]['edit_total_pct'] == "1",
                    _check[i]['edit_total_amt'] == "1",
                    _check[i]['orderProductInsuranceID']
                ));
            }
        }
        notifyListeners();
    }
}