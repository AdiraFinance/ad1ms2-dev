import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/fee_type_model.dart';
import 'package:ad1ms2_dev/models/info_fee_credit_structure_model.dart';
import 'package:ad1ms2_dev/models/installment_type_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_fee_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_object_model.dart';
import 'package:ad1ms2_dev/models/payment_method_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_struktur_kredit_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_application_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_income_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_type_installment_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_subsidy_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_major_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_object_karoseri_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_collatelar_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_salesman_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/dashboard/dashboard_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_rincian_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_individu_change_notifier/form_m_parent_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';
import 'package:ad1ms2_dev/shared/format_currency.dart';
import 'package:ad1ms2_dev/shared/info_wmp_change_notifier.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:ad1ms2_dev/shared/regex_input_formatter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'info_additional_insurance_change_notifier.dart';

class InfoCreditStructureChangeNotifier with ChangeNotifier{
  bool _autoValidate = false;
  bool _autoValidateInfoFeeCreditStruture = false;
  bool _isListTenorNotEmpty = true;
  InstallmentTypeModel _installmentTypeSelected;
  String _periodOfTimeSelected;
  String _dpFlag;
  String _custType;
  String _lastKnownState;
  TextEditingController _controllerPaymentPerYear = TextEditingController();
  PaymentMethodModel _paymentMethodSelected;
  TextEditingController _controllerInterestRateEffective = TextEditingController();
  TextEditingController _controllerInterestRateFlat = TextEditingController();
  TextEditingController _controllerObjectPrice = TextEditingController();
  TextEditingController _controllerKaroseriTotalPrice = TextEditingController();
  TextEditingController _controllerTotalPrice = TextEditingController();
  TextEditingController _controllerNetDP = TextEditingController();
  TextEditingController _controllerBranchDP = TextEditingController();
  TextEditingController _controllerGrossDP = TextEditingController();
  TextEditingController _controllerTotalLoan = TextEditingController();
  TextEditingController _controllerInstallment = TextEditingController();
  TextEditingController _controllerLTV = TextEditingController();
  bool _isVisible = false;
  FormatCurrency _formatCurrency = FormatCurrency();
  RegExInputFormatter _amountValidator = RegExInputFormatter.withRegex('^[0-9]{0,13}(\\.[0-9]{0,2})?\$');
  DbHelper _dbHelper = DbHelper();
  bool _loadData = false;
  var storage = FlutterSecureStorage();
  String _messageError = '';
  int _interestAmount = 0;
  double _installmentDeclineN = 0;

  bool _installmentTypeDakor = false;
  bool _paymentPerYearDakor = false;
  bool _periodTimeDakor = false;
  bool _paymentMethodDakor = false;
  bool _interestRateEffDakor = false;
  bool _interestRateFlatDakor = false;
  bool _objectPriceDakor = false;
  bool _totalPriceKaroseriDakor = false;
  bool _totalPriceDakor = false;
  bool _netDPDakor = false;
  bool _branchDPDakor = false;
  bool _grossDPDakor = false;
  bool _totalLoanDakor = false;
  bool _installmentDakor = false;
  bool _ltvDakor = false;
  bool _flag = false;
  bool _isDisableEffRate = false;
  bool _isDisablePACIAAOSCONA = false;

  GlobalKey<FormState> _key = GlobalKey<FormState>();

  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  bool get isDisableEffRate => _isDisableEffRate;

  set isDisableEffRate(bool value) {
    this._isDisableEffRate = value;
    notifyListeners();
  }

  int get interestAmount => _interestAmount;

  set interestAmount(int value) {
    this._interestAmount = value;
    notifyListeners();
  }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  String get lastKnownState => _lastKnownState;

  set lastKnownState(String value) {
    this._lastKnownState = value;
    notifyListeners();
  }

  bool get autoValidateInfoFeeCreditStructure =>
      _autoValidateInfoFeeCreditStruture;

  set autoValidateInfoFeeCreditStructure(bool value) {
    this._autoValidateInfoFeeCreditStruture = value;
    notifyListeners();
  }

  InstallmentTypeModel get installmentTypeSelected => _installmentTypeSelected;

  set installmentTypeSelected(InstallmentTypeModel value) {
    this._installmentTypeSelected = value;
    if(value.id  == "04") {
      // Jenis Angsuran = Seasonal
      this._listPaymentMethod = PaymentMethod().listPaymentMethod;
      this._listPaymentMethod.removeAt(0);
      this._isDisableEffRate = true;
      this._controllerPaymentPerYear.clear();
    }
    else if(value.id == "03"){
      // Jenis Angsuran = Decline N
      _listPaymentMethod = PaymentMethod().listPaymentMethod;
      this._listPaymentMethod.removeAt(0);
      this._controllerPaymentPerYear.clear();
      this._isDisableEffRate = false;
    }
    else if(value.id == "08") {
      _listPaymentMethod = PaymentMethod().listPaymentMethod;
      this._listPaymentMethod.removeAt(0);
    }
    else {
      _listPaymentMethod = PaymentMethod().listPaymentMethod;
      // if(this._listPaymentMethod.length == 1) {
      //   this._listPaymentMethod.add(PaymentMethodModel("02", "ARREAR"));
      this._controllerPaymentPerYear.clear();
      // }
      this._isDisableEffRate = false;
    }
    // setIsVisible(this._installmentTypeSelected.id);
    notifyListeners();
  }

  String get periodOfTimeSelected => _periodOfTimeSelected;

  set periodOfTimeSelected(String value) {
    this._periodOfTimeSelected = value;
    checkTenorCreditLimit();
    notifyListeners();
  }

  TextEditingController get controllerPaymentPerYear => _controllerPaymentPerYear;

  PaymentMethodModel get paymentMethodSelected => _paymentMethodSelected;

  set paymentMethodSelected(PaymentMethodModel value) {
    this._paymentMethodSelected = value;
    notifyListeners();
  }

  TextEditingController get controllerInterestRateEffective => _controllerInterestRateEffective;

  TextEditingController get controllerInterestRateFlat => _controllerInterestRateFlat;

  TextEditingController get controllerObjectPrice => _controllerObjectPrice;

  TextEditingController get controllerKaroseriTotalPrice => _controllerKaroseriTotalPrice;

  TextEditingController get controllerTotalPrice => _controllerTotalPrice;

  TextEditingController get controllerNetDP => _controllerNetDP;

  TextEditingController get controllerBranchDP => _controllerBranchDP;

  TextEditingController get controllerGrossDP => _controllerGrossDP;

  TextEditingController get controllerTotalLoan => _controllerTotalLoan;

  TextEditingController get controllerInstallment => _controllerInstallment;

  TextEditingController get controllerLTV => _controllerLTV;

  List<InstallmentTypeModel> _listInstallmentType = [];//InstallmentType().listInstallmentType;

  List<String> _listPeriodOfTime = [];

  List<PaymentMethodModel> _listPaymentMethod = []; //PaymentMethod().listPaymentMethod;

  List<InfoFeeCreditStructureModel> _listInfoFeeCreditStructure = [];

  List<InfoFeeCreditStructureModel> get listInfoFeeCreditStructure => _listInfoFeeCreditStructure;

  UnmodifiableListView<InstallmentTypeModel> get listInstallmentType {
    return UnmodifiableListView(this._listInstallmentType);
  }

  UnmodifiableListView<String> get listPeriodOfTime {
    return UnmodifiableListView(this._listPeriodOfTime);
  }

  UnmodifiableListView<PaymentMethodModel> get listPaymentMethod {
    return UnmodifiableListView(this._listPaymentMethod);
  }

  void addInfoFeeCreditStructure(InfoFeeCreditStructureModel value){
    this._listInfoFeeCreditStructure.add(value);
    if(autoValidateInfoFeeCreditStructure) autoValidateInfoFeeCreditStructure = false;
    notifyListeners();
  }

  void updateListInfoFeeCreditStructure(int index,InfoFeeCreditStructureModel value){
    this._listInfoFeeCreditStructure[index] = value;
    notifyListeners();
  }

  bool get isVisible => _isVisible;

  set isVisible(bool value) {
    this._isVisible = value;
    notifyListeners();
  }

  void setIsVisible(String id){
    if(id != null){
      if(id == "05" || id == "06" || id == "07" || id == "08" || id == "10"){
        isVisible = true;
      }
      else{
        isVisible =  false;
      }
    }
  }

  GlobalKey<FormState> get keyForm => _key;

  bool get flag {
    return _flag;
  }

  set flag(bool value) {
    this._flag = value;
    notifyListeners();
  }

  void check(BuildContext context) async {
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
      await checkAngsuranCreditLimit(context);
      // _getMinDP(context);
      checkDataDakor();
      // Navigator.pop(context);
    } else {
      flag = false;
      autoValidate = true;
    }
  }

  Future<bool> onBackPress() async{
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
      if(this._messageError != ''){
        flag = false;
        autoValidate = true;
      }
      checkDataDakor();
    } else {
      flag = false;
      autoValidate = true;
    }
    return true;
  }

  void clearInfoCreditStructure(){
    this._autoValidate = false;
    this._flag = false;
    this._listInstallmentType.clear();
    this._installmentTypeSelected = null;
    this._listPeriodOfTime.clear();
    this._periodOfTimeSelected = null;
    this._controllerPaymentPerYear.clear();
    this._listPaymentMethod.clear();
    this._paymentMethodSelected = null;
    this._controllerInterestRateEffective.clear();
    this._controllerInterestRateFlat.clear();
    this._controllerObjectPrice.clear();
    this._controllerKaroseriTotalPrice.clear();
    this._controllerTotalPrice.clear();
    this._controllerNetDP.clear();
    this._controllerBranchDP.clear();
    this._controllerGrossDP.clear();
    this._controllerTotalLoan.clear();
    this._controllerInstallment.clear();
    this._controllerLTV.clear();
    this._listInfoFeeCreditStructure.clear();
    isDisablePACIAAOSCONA = false;
  }

  void clearInfoCreditStructureWithoutInstallmentType(){
    this._periodOfTimeSelected = null;
    this._controllerPaymentPerYear.clear();
    this._paymentMethodSelected = null;
    this._controllerInterestRateEffective.clear();
    this._controllerInterestRateFlat.clear();
    this._controllerObjectPrice.clear();
    this._controllerKaroseriTotalPrice.clear();
    this._controllerTotalPrice.clear();
    this._controllerNetDP.clear();
    this._controllerBranchDP.clear();
    this._controllerGrossDP.clear();
    this._controllerTotalLoan.clear();
    this._controllerInstallment.clear();
    this._controllerLTV.clear();
    this._listInfoFeeCreditStructure.clear();
  }

  RegExInputFormatter get amountValidator => _amountValidator;

  FormatCurrency get formatCurrency => _formatCurrency;

  double get installmentDeclineN => _installmentDeclineN;

  set installmentDeclineN(double value) {
    this._installmentDeclineN = value;
  }

  void checkTenorCreditLimit() async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    List _dataCL = await _dbHelper.selectMS2LME();

    if(_preferences.getString("jenis_penawaran") == "002") {
      if(int.parse(this._periodOfTimeSelected) > int.parse(_dataCL[0]['remaining_tenor'])) {
        showSnackBar("Tenor Pengajuan Melebihi Tenor Aktivasi. Mohon lakukan penyesuaian");
        this._periodOfTimeSelected = null;
        notifyListeners();
      }
    }
  }

  void updateMS2ApplObjectInfStrukturKredit() async {
    print("update struktur kredit");
    SharedPreferences _preferences = await SharedPreferences.getInstance();

    _dbHelper.updateMS2ApplObjectInfStrukturKredit(MS2ApplObjectModel(
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        "",
        this._installmentTypeSelected != null ? this._installmentTypeSelected.id : "",
        this._installmentTypeSelected != null ? this._installmentTypeSelected.name : "",
        this._periodOfTimeSelected != null ? int.parse(this._periodOfTimeSelected) : null,
        this._paymentMethodSelected != null ? this._paymentMethodSelected.id : "",
        this._paymentMethodSelected != null ? this._paymentMethodSelected.name : "",
        this._controllerInterestRateEffective.text == "" || this._controllerInterestRateEffective.text == null ? null : double.parse(this._controllerInterestRateEffective.text.replaceAll(",", "")) ,
        this._controllerInterestRateFlat.text == "" || this._controllerInterestRateFlat.text == null ? null : double.parse(this._controllerInterestRateFlat.text.replaceAll(",", "")),
        this._controllerObjectPrice.text != "" ? double.parse(this._controllerObjectPrice.text.replaceAll(",", "")) : null,
        this._controllerKaroseriTotalPrice.text != "" ? double.parse(this._controllerKaroseriTotalPrice.text.replaceAll(",", "")) : null,
        this._controllerTotalPrice.text == "" || this._controllerTotalPrice.text == null ? null : double.parse(this._controllerTotalPrice.text.replaceAll(",", "")) ,
        this._controllerNetDP.text == "" || this._controllerNetDP.text == null ? null : double.parse(this._controllerNetDP.text.replaceAll(",", "")),
        null, // installment decline n
        this._controllerPaymentPerYear.text == "" ? null : int.parse(this._controllerPaymentPerYear.text),
        this._controllerBranchDP.text != "" ? double.parse(this._controllerBranchDP.text.replaceAll(",", "")) : null,
        this._controllerGrossDP.text == "" || this._controllerGrossDP.text == null ? null : double.parse(this._controllerGrossDP.text.replaceAll(",", "")),
        this._controllerTotalLoan.text == "" || this._controllerTotalLoan.text == null ? null : double.parse(this._controllerTotalLoan.text.replaceAll(",", "")),
        this._controllerInstallment.text == "" || this._controllerInstallment.text == null ? null : double.parse(this._controllerInstallment.text.replaceAll(",", "")),
        this._interestAmount,
        this._controllerLTV.text == "" ? null : double.parse(this._controllerLTV.text.replaceAll(",", "")),// ltv
        null,
        null,
        null,
        null,
        null, // gp_type
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        DateTime.now().toString(),
        _preferences.getString("username"),
        1,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null
    ));
  }

  void saveToSQLiteInfStrukturKreditBiaya() async {
    List<MS2ApplFeeModel> _listApplFee = [];
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    for(int i=0; i<_listInfoFeeCreditStructure.length; i++){
      _listApplFee.add(MS2ApplFeeModel(
        "",
        this._listInfoFeeCreditStructure[i].orderFeeID != null ? this._listInfoFeeCreditStructure[i].orderFeeID : "NEW",
        this._listInfoFeeCreditStructure[i].feeTypeModel != null ? this._listInfoFeeCreditStructure[i].feeTypeModel.id : "",
        this._listInfoFeeCreditStructure[i].feeTypeModel != null ? this._listInfoFeeCreditStructure[i].feeTypeModel.name : "",
        this._listInfoFeeCreditStructure[i].cashCost == null || _listInfoFeeCreditStructure[i].cashCost == "" ? 0 : double.parse(this._listInfoFeeCreditStructure[i].cashCost.replaceAll(",", "")) ,
        this._listInfoFeeCreditStructure[i].creditCost != null ? double.parse(this._listInfoFeeCreditStructure[i].creditCost.replaceAll(",", "")) : 0,
        this._listInfoFeeCreditStructure[i].totalCost == null || this._listInfoFeeCreditStructure[i].totalCost == "" ? 0 : double.parse(this._listInfoFeeCreditStructure[i].totalCost.replaceAll(",", "")),
        1,
        DateTime.now().toString(),
        _preferences.getString("username"),
        null,
        null,
        this._listInfoFeeCreditStructure[i].editFeeType ? "1" : "0",
        this._listInfoFeeCreditStructure[i].editFeeCash ? "1" : "0",
        this._listInfoFeeCreditStructure[i].editFeeCredit ? "1" : "0",
        this._listInfoFeeCreditStructure[i].editTotalFee ? "1" : "0",
      ));
    }
    _dbHelper.insertMS2ApplFee(_listApplFee);
  }

  Future<bool> deleteSQLite() async{
    return await _dbHelper.deleteMS2ApplFee();
  }

  Future<void> setDataFromSQLite(BuildContext context) async{
    var _data = await _dbHelper.selectMS2ApplObject();
    // var _providerUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
    var _providerInfoStructureCreditTypeInstallment = Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false);
    print("select kredit struktur = $_data");
    // var _body = jsonEncode(_data);
    if(_data.isNotEmpty){
      if(this._listPaymentMethod.isEmpty){
        this._listPaymentMethod = PaymentMethod().listPaymentMethod;
      }
      print("panjang payment method ${_listPaymentMethod.length}");
      if(_data[0]['payment_method'] != "null" && _data[0]['payment_method'] != ""){
        for(int i = 0; i < _listPaymentMethod.length; i++){
          if(_data[0]['payment_method'] == _listPaymentMethod[i].id){
            this._paymentMethodSelected = _listPaymentMethod[i];
          }
        }
      }
      if(_data[0]['appl_top'].toString() != "null" && _data[0]['appl_top'].toString() != ""){
        debugPrint("CHECK_CREDIT_STRUC");
        await getDataTenor(context);
        for(int i = 0; i < _listPeriodOfTime.length; i++){
          if(_data[0]['appl_top'].toString() == _listPeriodOfTime[i]){
            this._periodOfTimeSelected = _listPeriodOfTime[i];
          }
        }
      }
      if(this._listInstallmentType.isEmpty){
        await installmentTypeSyariah(context);
      }
      print("panjang syariahhhhhh ${_listInstallmentType.length}");
      if(_data[0]['installment_type'].toString() != "null" && _data[0]['installment_type'].toString() != ""){
        for(int i=0; i < _listInstallmentType.length; i++){
          if(_data[0]['installment_type'].toString() == _listInstallmentType[i].id){
            this._installmentTypeSelected = _listInstallmentType[i];
          }
        }
        setIsVisible(this._installmentTypeSelected.id);
      }
      _data[0]['eff_rate'] != "null" && _data[0]['eff_rate'] != "" ? this._controllerInterestRateEffective.text = _data[0]['eff_rate'].toString() : this._controllerInterestRateEffective.text = "";
      _data[0]['flat_rate'] != "null" && _data[0]['flat_rate'] != "" ? this._controllerInterestRateFlat.text = _data[0]['flat_rate'].toString() : this._controllerInterestRateFlat.text = "";
      _data[0]['object_price'] != "null" && _data[0]['object_price'] != "" ? this._controllerObjectPrice.text = _data[0]['object_price'].toString() : this._controllerObjectPrice.text = "";
      _data[0]['karoseri_price'] != "null" && _data[0]['karoseri_price'] != "" ? this._controllerKaroseriTotalPrice.text = _data[0]['karoseri_price'].toString() : this._controllerKaroseriTotalPrice.text = "";
      _data[0]['total_amt'] != "null" && _data[0]['total_amt'] != "" ? this._controllerTotalPrice.text = _data[0]['total_amt'].toString() : this._controllerTotalPrice.text = "";
      _data[0]['dp_net'] != "null" && _data[0]['dp_net'] != "" ? this._controllerNetDP.text = _data[0]['dp_net'].toString() : this._controllerNetDP.text = "";
      _data[0]['payment_per_year'] != "null" && _data[0]['payment_per_year'] != "" ? this._controllerPaymentPerYear.text = _data[0]['payment_per_year'].toString() : this._controllerPaymentPerYear.text = "";
      _data[0]['dp_branch'] != "null" && _data[0]['dp_branch'] != "" ? this._controllerBranchDP.text = _data[0]['dp_branch'].toString() : this._controllerBranchDP.text = "";
      _data[0]['dp_gross'] != "null" && _data[0]['dp_gross'] != "" ? this._controllerGrossDP.text = _data[0]['dp_gross'].toString() : this._controllerGrossDP.text = "";
      _data[0]['principal_amt'] != "null" && _data[0]['principal_amt'] != "" ? this._controllerTotalLoan.text = _data[0]['principal_amt'].toString() : this._controllerTotalLoan.text = "";
      _data[0]['installment_paid'] != "null" && _data[0]['installment_paid'] != "" ? this._controllerInstallment.text = _data[0]['installment_paid'].toString() : this._controllerInstallment.text = "";
      _data[0]['interest_amt'] != "null" && _data[0]['interest_amt'] != "" ? this._interestAmount = _data[0]['interest_amt'] : this._interestAmount = 0;
      _data[0]['ltv'] != "null" && _data[0]['ltv'] != "" ? this._controllerLTV.text = _data[0]['ltv'] : this._controllerLTV.text = "";

      // BP
      _data[0]['balloon_type'] != "null" && _data[0]['balloon_type'] != "" ? _providerInfoStructureCreditTypeInstallment.radioValueBalloonPHOrBalloonInstallment = int.parse(_data[0]['balloon_type']) : 0;
      _data[0]['last_percen_installment'] != "null" && _data[0]['last_percen_installment'] != "" ? _providerInfoStructureCreditTypeInstallment.controllerInstallmentPercentage.text = _data[0]['last_percen_installment'].toString() : "";
      _data[0]['last_installment_amt'] != "null" && _data[0]['last_installment_amt'] != "" ? _providerInfoStructureCreditTypeInstallment.controllerInstallmentValue.text = _data[0]['last_installment_amt'].toString() : "";
      formatting();
      notifyListeners();
    }
    // checkDataDakor();
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
    notifyListeners();
  }

  String get dpFlag => _dpFlag;

  set dpFlag(String value) {
    this._dpFlag = value;
    // notifyListeners();
  }

  String get custType => _custType;

  set custType(String value) {
    this._custType = value;
  }

  Future<void> setCustType(BuildContext context) async{
    SharedPreferences _preference = await SharedPreferences.getInstance();
    this._custType = _preference.getString("cust_type");
    this._lastKnownState = _preference.getString("last_known_state");
    if(this._listInstallmentType.isEmpty){
      await installmentTypeSyariah(context);
    }
    if(this._listPaymentMethod.isEmpty){
      this._listPaymentMethod = PaymentMethod().listPaymentMethod;
    }
    getDataFromDashboard(context);
    if(this._listPeriodOfTime.isEmpty){
      await getDataTenor(context); //sementara
    }
    hideShowEffRate(context);
    // await setDataFromSQLite(context);

    if(this._lastKnownState == "AOS" || this._lastKnownState == "CONA" || this._lastKnownState == "IA" || this._lastKnownState == "PAC") {
      this._isDisablePACIAAOSCONA = true;
    }
    checkDataDakor();
  }


  bool get isListTenorNotEmpty => _isListTenorNotEmpty;

  set isListTenorNotEmpty(bool value) {
    this._isListTenorNotEmpty = value;
  }

  Future<void> getDataTenor(BuildContext context) async{
    // await Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).setCustType();
    // getDataFromDashboard(context);
    SharedPreferences _preference = await SharedPreferences.getInstance();
    this._custType = _preference.getString("cust_type");
    debugPrint("CEK_CUST_TYPE $_custType");
    // var _listOID = Provider.of<ListOidChangeNotifier>(context,listen: false);
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
    var _providerUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    if(_providerUnitObject.groupObjectSelected != null && _providerUnitObject.objectSelected != null){
      this._listPeriodOfTime.clear();
      // this._periodOfTimeSelected = null;
      //try{
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);

      String _getTenor = await storage.read(key: "GetTenor");
      var _body = jsonEncode(
          {
            "P_OBJECT_GROUP_ID": "${_providerUnitObject.groupObjectSelected.KODE}",
            "P_OBJECT_ID": "${_providerUnitObject.objectSelected.id}",
            "P_OJK_BUSS_DETAIL_ID" : this._custType == "COM" ? "${_providerUnitObject.businessActivitiesTypeModelSelected.id}" : "${_providerFoto.jenisKegiatanUsahaSelected.id}",
            "P_OJK_BUSS_ID" : this._custType == "COM" ? "${_providerUnitObject.businessActivitiesModelSelected.id}" : "${_providerFoto.kegiatanUsahaSelected.id}"
          }
      );
      print("body tenor = $_body");

      final _response = await _http.post(
        // "${BaseUrl.urlOccupation}api/occupation/get-jenis-pekerjaan"
          "${BaseUrl.urlGeneral}$_getTenor",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      );
      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        List _data = _result['data'];
        print("result tenor = $_result");
        if(_data.isNotEmpty){
          for(int i=0; i < _data.length;i++){
            _listPeriodOfTime.add(_data[i].toString());
          }
        }
        else{
          this._isListTenorNotEmpty = false;
          debugPrint('Jangka waktu tidak tersedia di API GetTenor');
          // Dimatikan karena tidak ada scaffonld key
          //showSnackBarInfo("Jangka waktu tidak tersedia di API GetTenor");
        }
        await getDPFlag(context);
      }
      else{
        showSnackBar("Jangka waktu Error response ${_response.statusCode} ");
      }


      // Catch dimatikan karena terjadi eror snackbar key null
      //}
      // catch (e) {
      //   print("error tenor ${e.toString()}");
      //   showSnackBar("Error API Jangka waktu ${e.toString()}");
      // }
      notifyListeners();
    }
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor,duration: Duration(seconds: 5),));
  }

  void showSnackBarInfo(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: Text("$text",style: TextStyle(color: Colors.black)), behavior: SnackBarBehavior.floating, backgroundColor: Colors.yellow,duration: Duration(seconds: 5),));
  }

  void formatting() {
    this._controllerInterestRateEffective.text = formatCurrency.formatCurrencyPoint5(_controllerInterestRateEffective.text);
    this._controllerInterestRateFlat.text = formatCurrency.formatCurrencyPoint5(_controllerInterestRateFlat.text);
    this._controllerObjectPrice.text = formatCurrency.formatCurrency2(_controllerObjectPrice.text);
    this._controllerKaroseriTotalPrice.text = formatCurrency.formatCurrency2(_controllerKaroseriTotalPrice.text);
    this._controllerTotalPrice.text =formatCurrency.formatCurrency2(_controllerTotalPrice.text);
    this._controllerNetDP.text = formatCurrency.formatCurrency2(_controllerNetDP.text);
    this._controllerBranchDP.text = formatCurrency.formatCurrency2(_controllerBranchDP.text);
    this._controllerGrossDP.text = formatCurrency.formatCurrency2(_controllerGrossDP.text);
    this._controllerTotalLoan.text = formatCurrency.formatCurrency2(_controllerTotalLoan.text);
    this._controllerInstallment.text = formatCurrency.formatCurrency2(_controllerInstallment.text);
    this._controllerLTV.text = formatCurrency.formatCurrencyPoint5(_controllerLTV.text);
  }

  void countPHMaxColla(BuildContext context){
    var _providerColla = Provider.of<InformationCollateralChangeNotifier>(context, listen: false);
    if(_providerColla.radioValueIsCollateralSameWithUnitOto == 0){
      _providerColla.controllerHargaJualShowroom.text = this._controllerObjectPrice.text;
      _providerColla.countPHMax();
    }
  }

  Future<void> getDPFlag(BuildContext context) async {
    // var _pObjectGroupID = "";
    var _providerUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    // var _providerColla = Provider.of<InformationCollateralChangeNotifier>(context,listen: false);
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);
    // per tanggal 18.05.2021 dirubah ngambil group object dari colla yang sebelumnya dari unit
    // if(_providerColla.collateralTypeModel != null) {
    //   if(_providerColla.collateralTypeModel.id != "001") {
    //     // jenis jaminan property dan durable
    //     if(_providerUnitObject.groupObjectSelected != null) {
    //       if(_providerUnitObject.groupObjectSelected.KODE == "003") { // group object unit selain durable
    //         _pObjectGroupID = _providerUnitObject.groupObjectSelected.KODE;
    //       }
    //       else if(_providerUnitObject.groupObjectSelected.KODE == "005"){
    //         _pObjectGroupID = _providerUnitObject.groupObjectSelected.KODE;
    //       }
    //       else {
    //         _pObjectGroupID = _providerColla.groupObjectSelected.KODE;
    //       }
    //     }
    //   } else {
    //     // jenis jaminan automotive
    //     if(_providerColla.groupObjectSelected != null) {
    //       _pObjectGroupID = _providerColla.groupObjectSelected.KODE;
    //     }
    //   }
    // }
    var _body = jsonEncode({
      "P_OBJECT_GROUP_ID": _providerUnitObject.groupObjectSelected != null ? _providerUnitObject.groupObjectSelected.KODE : "",
      // "P_OBJECT_GROUP_ID": _providerColla.groupObjectSelected != null ? _providerColla.groupObjectSelected.KODE : "",
      // "P_OBJECT_GROUP_ID": _pObjectGroupID,
    });
    print("body dp flag $_body");
    String _getDPFlag = await storage.read(key: "GetDPFlag");
    try{
      final _response = await _http.post(
          "${BaseUrl.urlGeneral}$_getDPFlag",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      );

      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        print("result dp flag $_result");
        if(_result == null) {
          showSnackBar(_result['data']['P_MSG']);
        }
        else {
          _dpFlag = _result['data']['P_FLAG'];
        }
      } else {
        showSnackBar("Error get response ${_response.statusCode}");
      }
    } catch(e){
      showSnackBar("Error $e");
    }
    notifyListeners();
  }

  void conditionGetMinDP(BuildContext context){
    if(this._installmentTypeSelected != null){
      _getMinDP(context);
    }
    else{
      showSnackBarInfo("Silahkan pilih tipe angsuran terlebih dahulu");
    }
  }

  String get messageError => _messageError;

  set messageError(String value) {
    this._messageError = value;
  }

  Future<void> checkAngsuranCreditLimit(BuildContext context) async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    List _dataCL = await _dbHelper.selectMS2LME();

    if(_dataCL.isNotEmpty){
      if(_dataCL[0]['flag_eligible'] == "00") {
        showSnackBarInfo("Nasabah Eligible");
      } else {
        showSnackBarInfo("Nasabah tidak eligible karena kualitas bayar");
      }
    }

    if(_preferences.getString("jenis_penawaran") == "002") {
      if(double.parse(this._controllerTotalLoan.text.replaceAll(",", "")) > double.parse(_dataCL[0]['max_ph'])) {
        showSnackBar("PH pengajuan lebih besar dari PH aktivasi.\n\nPH Pengajuan Rp${formatCurrency.formatCurrency2(this._controllerTotalLoan.text)}.\nPH Aktivasi Rp${formatCurrency.formatCurrency2(_dataCL[0]['max_ph'].toString())}.\n\nMohon melakukan penyesuaian");
      } else {
        Future.delayed(Duration(seconds: 1), () async {
          await _getMinDP(context);
        });
      }
    } else if(_preferences.getString("jenis_penawaran") == "003") {
      if(double.parse(this._controllerInstallment.text.replaceAll(",", "")) > double.parse(_dataCL[0]['max_installment'])) {
        showSnackBar("Aplikasi melewati limit nasabah.\n\nNilai Angsuran Rp${formatCurrency.formatCurrency2(this._controllerInstallment.text)}.\nLimit Angsuran Rp${formatCurrency.formatCurrency2(_dataCL[0]['max_installment'].toString())}.\n\nSilahkan rubah struktur kredit untuk meningkatkan potensi IA nasabah");
      }
      Future.delayed(Duration(seconds: 1), () async {
        await _getMinDP(context);
      });
    } else {
      Future.delayed(Duration(seconds: 1), () async {
        await _getMinDP(context);
      });
    }
  }

  Future<void> _getMinDP(BuildContext context) async {
    // var _listOID = Provider.of<ListOidChangeNotifier>(context,listen: false);
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
    var _providerUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    // String _flag = "";
    // double _rateValue;
    loadData = true;

    // if((this._controllerInterestRateEffective.text == "" && this._controllerInterestRateFlat.text == "") || (_controllerInterestRateFlat.text == "0" && this._controllerInterestRateEffective.text == "0")){
    //  print("masuk 1");
    //   _flag = "FLAT_RATE";
    //   _rateValue = 0;
    // }
    // else if((this._controllerInterestRateEffective.text != "" && this._controllerInterestRateEffective.text != "0") && (this._controllerInterestRateFlat.text == "" || _controllerInterestRateFlat.text == "0")){
    //   print("masuk 2");
    //   _flag = "EFF_RATE";
    //   _rateValue = double.parse(_controllerInterestRateEffective.text.replaceAll(",", ""));
    // }
    // else if((this._controllerInterestRateEffective.text == "" && this._controllerInterestRateEffective.text == "0") && (this._controllerInterestRateFlat.text != "" && _controllerInterestRateFlat.text != "0")){
    //   print("masuk 3");
    //   _flag = "FLAT_RATE";
    //   _rateValue = double.parse(_controllerInterestRateFlat.text.replaceAll(",", ""));
    // }
    // else{
    //   print("masuk 4");
    //   _flag = "FLAT_RATE";
    //   _rateValue = double.parse(_controllerInterestRateFlat.text.replaceAll(",", ""));
    // }

    // if(flag == "EFF_RATE") {
    //   _rateValue = double.parse(_controllerInterestRateEffective.text);
    // } else if(flag == "FLAT_RATE") {
    //   _rateValue = double.parse(_controllerInterestRateFlat.text);
    // } else {
    //   _rateValue = 0;
    // }

    // SharedPreferences _preferences = await SharedPreferences.getInstance();
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var _body = jsonEncode({
      "P_OBJECT_GROUP_ID": _providerUnitObject.groupObjectSelected != null ? _providerUnitObject.groupObjectSelected.KODE : "",
      "P_OBJECT_ID": _providerUnitObject.objectSelected != null ? _providerUnitObject.objectSelected.id : "",
      "P_OJK_BUSS_DETAIL_ID": this._custType != "COM"
          ? _providerFoto.jenisKegiatanUsahaSelected != null
          ? _providerFoto.jenisKegiatanUsahaSelected.id : ""
          : _providerUnitObject.businessActivitiesTypeModelSelected != null
          ? _providerUnitObject.businessActivitiesTypeModelSelected.id : "",
      "P_OJK_BUSS_ID": this._custType != "COM"
          ? _providerFoto.kegiatanUsahaSelected != null
          ? _providerFoto.kegiatanUsahaSelected.id : ""
          : _providerUnitObject.businessActivitiesModelSelected != null
          ? _providerUnitObject.businessActivitiesModelSelected.id : ""
    });
    print("cek body credit structure $_body");

    String _getMinDP = await storage.read(key: "GetMinDP");
    print("${BaseUrl.urlGeneral}$_getMinDP");
    try{
      final _response = await _http.post(
          "${BaseUrl.urlGeneral}$_getMinDP",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      );
      print("cek response credit structure ${_response.body}");
      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        if(_result['data']['P_MSG'] != "SUCCESS"){
          showSnackBar(_result['data']['P_MSG']);
          flag = false;
          autoValidate = true;
          this._messageError = _result['data']['P_MSG'];
        }
        else{
          double _totalPrice = this._controllerTotalPrice.text != "" ?  double.parse(this._controllerTotalPrice.text.replaceAll(",", "")) : 0;
          double _minDp = double.parse(_result['data']['P_MIN_DP']) * _totalPrice / 100;
          double _netDP = this._controllerNetDP.text != "" ? double.parse(this._controllerNetDP.text.replaceAll(",", "")) : 0;
          if(_minDp > _netDP){
            showSnackBar("Min DP kurang dari ${_result['data']['P_MIN_DP']}% dari ${this._controllerTotalPrice.text}");
            this._messageError = "Min DP kurang dari ${_result['data']['P_MIN_DP']}% dari ${this._controllerTotalPrice.text}";
            loadData = false;
            flag = false;
            autoValidate = true;
          }
          else{
            Navigator.pop(context);
            loadData = false;
            this._messageError = '';
          }
        }
        // if(_result == null) {
        //   showSnackBar(_result['data']['P_MSG']);
        //   loadData = false;
        // }
        // else {

        // _controllerNetDP.text = _result['data']['P_MIN_DP'].toString();
        // _controllerGrossDP.text = _result['data']['P_MIN_DP'].toString();
        // calculateCredit(context, _flag, _rateValue.toString(), _result['data']['P_MIN_DP'].toString());
        // }
      }
      else {
        showSnackBar("Error get min dp ${_response.statusCode}");
        loadData = false;
      }
    } catch(e){
      showSnackBar("Error $e");
      loadData = false;
    }
    notifyListeners();
  }

  // void calculateCredit(BuildContext context, String flag, String rate, String dp) async {
  Future<void> calculateCredit(BuildContext context) async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    // Form IDE Individu
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
    var _providerInfoNasabah = Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false);

    // Form IDE Company
    var _providerRincian = Provider.of<FormMCompanyRincianChangeNotifier>(context, listen: false);

    // Form App
    var _providerUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    // var _providerCollaOto = Provider.of<InformationCollateralChangeNotifier>(context,listen: false);
    var _providerKolateral = Provider.of<InformationCollateralChangeNotifier>(context, listen: false);
    var _providerSales = Provider.of<InformationSalesmanChangeNotifier>(context,listen: false);
    var _providerKaroseri = Provider.of<InfoObjectKaroseriChangeNotifier>(context,listen: false);
    var _providerCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false);
    var _providerMajorInsurance = Provider.of<InfoMajorInsuranceChangeNotifier>(context,listen: false);
    var _providerAdditionalInsurance = Provider.of<InfoAdditionalInsuranceChangeNotifier>(context,listen: false);
    var _providerCreditIncome = Provider.of<InfoCreditIncomeChangeNotifier>(context,listen: false);
    var _providerWmp = Provider.of<InfoWMPChangeNotifier>(context,listen: false);
    var _providerSubsidy = Provider.of<InfoCreditSubsidyChangeNotifier>(context,listen: false);
    // var _providerSubsidyDetail = Provider.of<AddCreditSubsidyChangeNotifier>(context, listen: false);
    var _providerCreditStructureTypeInstallment = Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false);
    var _providerParentIndividu = Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false);

    double _rateEff = _controllerInterestRateEffective.text != "" ? double.parse(this._controllerInterestRateEffective.text.replaceAll(",", "")) : 0;
    double _rateFlat = _controllerInterestRateFlat.text != "" ? double.parse(this._controllerInterestRateFlat.text.replaceAll(",", "")) : 0;
    double _rate;
    double _dp;
    String _flag;

    if(_dpFlag == "0"){
      _dp = this._controllerGrossDP.text != "" ? double.parse(this._controllerGrossDP.text.replaceAll(",", "")) : 0;
    }
    else{
      _dp = this._controllerNetDP.text != "" ? double.parse(this._controllerNetDP.text.replaceAll(",", "")) : 0;
    }

    String _finType;
    if(_preferences.getString("cust_type") == "PER"){
      _finType = _providerFoto.typeOfFinancingModelSelected.financingTypeId;
    }
    else{
      _finType = _providerUnitObject.typeOfFinancingModelSelected.financingTypeId;
    }

    if(_finType == "1"){
      if(this._installmentTypeSelected.id == "04") {
        // Seasonal
        _rate = _rateFlat;
        _flag = "FLAT_RATE";
      } else {
        // Selain Seasonal
        if(_rateEff > 0){
          _rate = _rateEff;
          _flag = "EFF_RATE";
        }
        else{
          _rate = _rateFlat;
          _flag = "FLAT_RATE";
        }
      }
    }
    else{
      _rate = _rateFlat;
      _flag = "FLAT_RATE";
    }

    // Dedup
    // var _listOID = Provider.of<ListOidChangeNotifier>(context,listen: false);

    print("cek data dsr ${_providerCreditIncome.controllerDebtComparison.text}");

    // var _rateValue = double.parse(rate);
    // var _dpValue = double.parse(dp);
    var _collaType = _providerKolateral.collateralTypeModel != null
        ? _providerKolateral.collateralTypeModel.id != "003"
        ? _providerKolateral.collateralTypeModel.name : "TIDAK_ADA" :"";

    var _dateFormat = DateFormat("dd-MM-yyyy HH:mm:ss");
    var _dateTime = _dateFormat.format(DateTime.now());

    var _orderProductSaleses = [];
    if(_providerSales.listInfoSales.isNotEmpty) {
      for(int i=0; i < _providerSales.listInfoSales.length; i++) {
        _orderProductSaleses.add(
            {
              "orderProductSalesID": "NEW",
              "salesType": _providerSales.listInfoSales[i].salesmanTypeModel != null ? _providerSales.listInfoSales[i].salesmanTypeModel.id : "",
              "employeeJob": _providerSales.listInfoSales[i].employeeModel != null ? _providerSales.listInfoSales[i].employeeModel.Nama : "",
              "employeeID": _providerSales.listInfoSales[i].employeeModel != null ? _providerSales.listInfoSales[i].employeeModel.NIK : "",
              "employeeHeadID": _providerSales.listInfoSales[i].employeeHeadModel != null ? _providerSales.listInfoSales[i].employeeHeadModel.NIK : "",
              "referalContractNumber": _providerUnitObject.referenceNumberModel != null ? _providerUnitObject.referenceNumberModel.noPK : "",
              "employeeHeadJob": null,
              "creationalSpecification": {
                "createdAt": _dateTime,
                "createdBy": _preferences.getString("username"),
                "modifiedAt": _dateTime,
                "modifiedBy": _preferences.getString("username"),
              },
              "status": "ACTIVE"
            }
        );
      }
    }

    var _orderKaroseris = [];
    if(_providerKaroseri.listFormKaroseriObject.isNotEmpty) {
      for(int i=0; i < _providerKaroseri.listFormKaroseriObject.length; i++) {
        _orderKaroseris.add(
            {
              "orderKaroseriID": "NEW",
              "isPksKaroseri": _providerKaroseri.listFormKaroseriObject[i].PKSKaroseri != -1 ? _providerKaroseri.listFormKaroseriObject[i].PKSKaroseri == 1 ? true : false : false,
              "karoseriCompanyID": _providerKaroseri.listFormKaroseriObject[i].company != null ? _providerKaroseri.listFormKaroseriObject[i].company.id : "",
              "karoseriID": _providerKaroseri.listFormKaroseriObject[i].karoseri != null ? _providerKaroseri.listFormKaroseriObject[i].karoseri.kode : "",
              "karoseriPrice": _providerKaroseri.listFormKaroseriObject[i].price != null ? double.parse(_providerKaroseri.listFormKaroseriObject[i].price.replaceAll(",", "")) : 0,
              "karoseriQty": _providerKaroseri.listFormKaroseriObject[i].jumlahKaroseri != null ? double.parse(_providerKaroseri.listFormKaroseriObject[i].jumlahKaroseri.replaceAll(",", "")) : 0,
              "karoseriTotalPrice": _providerKaroseri.listFormKaroseriObject[i].totalPrice != null ? double.parse(_providerKaroseri.listFormKaroseriObject[i].totalPrice.replaceAll(",", "")) : 0,
              "uangMukaKaroseri": 0,
              "creationalSpecification": {
                "createdAt": _dateTime,
                "createdBy": _preferences.getString("username"),
                "modifiedAt": _dateTime,
                "modifiedBy": _preferences.getString("username"),
              },
              "status": "ACTIVE"
            }
        );
      }
    }

    var _orderProductInsurances = [];
    if(_providerMajorInsurance.listFormMajorInsurance.isNotEmpty) {
      for(int i=0; i < _providerMajorInsurance.listFormMajorInsurance.length; i++) {
        _orderProductInsurances.add(
            {
              "orderProductInsuranceID": "NEW",
              "insuranceCollateralReferenceSpecification": {
                "collateralID": "NEW",
                "collateralTypeID": ""
              },
              "insuranceTypeID": _providerMajorInsurance.listFormMajorInsurance[i].insuranceType != null ? _providerMajorInsurance.listFormMajorInsurance[i].insuranceType.KODE : "",
              "insuranceCompanyID": _providerMajorInsurance.listFormMajorInsurance[i].company != null ? _providerMajorInsurance.listFormMajorInsurance[i].company.KODE : "",
              "insuranceProductID": _providerMajorInsurance.listFormMajorInsurance[i].product != null ? _providerMajorInsurance.listFormMajorInsurance[i].product.KODE : "",
              "period": _providerMajorInsurance.listFormMajorInsurance[i].periodType != null ? _providerMajorInsurance.listFormMajorInsurance[i].periodType : 0,
              "type": "",
              "type1": "",
              "type2": "",
              "subType": "",
              "subTypeCoverage": "",
              "insuranceSourceTypeID": "",
              "insuranceValue": _providerMajorInsurance.listFormMajorInsurance[i].coverageValue.isNotEmpty ? double.parse(_providerMajorInsurance.listFormMajorInsurance[i].coverageValue.replaceAll(",", "")) : 0,
              "upperLimitPercentage": _providerMajorInsurance.listFormMajorInsurance[i].upperLimitRate.isNotEmpty ? double.parse( _providerMajorInsurance.listFormMajorInsurance[i].upperLimitRate.replaceAll(",", "")) : 0,
              "upperLimitAmount": _providerMajorInsurance.listFormMajorInsurance[i].upperLimit.isNotEmpty ? double.parse(_providerMajorInsurance.listFormMajorInsurance[i].upperLimit.replaceAll(",", "")) : 0,
              "lowerLimitPercentage": _providerMajorInsurance.listFormMajorInsurance[i].lowerLimitRate.isNotEmpty ? double.parse(_providerMajorInsurance.listFormMajorInsurance[i].lowerLimitRate.replaceAll(",", "")) : 0,
              "lowerLimitAmount": _providerMajorInsurance.listFormMajorInsurance[i].lowerLimit.isNotEmpty ? double.parse(_providerMajorInsurance.listFormMajorInsurance[i].lowerLimit.replaceAll(",", "")) : 0,
              "insuranceCashFee": _providerMajorInsurance.listFormMajorInsurance[i].priceCash.isNotEmpty ? double.parse(_providerMajorInsurance.listFormMajorInsurance[i].priceCash.replaceAll(",", "")) : 0,
              "insuranceCreditFee": _providerMajorInsurance.listFormMajorInsurance[i].priceCredit.isNotEmpty ? double.parse(_providerMajorInsurance.listFormMajorInsurance[i].priceCredit.replaceAll(",", "")) : 0,
              "totalSplitFee": _providerMajorInsurance.listFormMajorInsurance[i].totalPriceSplit.isNotEmpty ? double.parse(_providerMajorInsurance.listFormMajorInsurance[i].totalPriceSplit.replaceAll(",", "")) : 0,
              "totalInsuranceFeePercentage": _providerMajorInsurance.listFormMajorInsurance[i].totalPriceRate.isNotEmpty ? double.parse(_providerMajorInsurance.listFormMajorInsurance[i].totalPriceRate.replaceAll(",", "")) : 0,
              "totalInsuranceFeeAmount": _providerMajorInsurance.listFormMajorInsurance[i].totalPrice.isNotEmpty ? double.parse(_providerMajorInsurance.listFormMajorInsurance[i].totalPrice.replaceAll(",", "")) : 0,
              "creationalSpecification": {
                "createdAt": _dateTime,
                "createdBy": _preferences.getString("username"),
                "modifiedAt": _dateTime,
                "modifiedBy": _preferences.getString("username"),
              },
              "status": "ACTIVE",
              "insurancePaymentType": "CASH"
            }
        );
      }
    }

    if(_providerAdditionalInsurance.listFormAdditionalInsurance.isNotEmpty) {
      // debugPrint("CHECK INSURANCE ${_providerAdditionalInsurance.listFormAdditionalInsurance.length}");
      for(int i=0; i<_providerAdditionalInsurance.listFormAdditionalInsurance.length; i++) {
        _orderProductInsurances.add({
          "orderProductInsuranceID": "NEW",
          "insuranceCollateralReferenceSpecification": {
            "collateralID": "NEW",
            "collateralTypeID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].colaType, //_providerKolateral.collateralTypeModel.id
          },
          "insuranceTypeID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].insuranceType.KODE,
          "insuranceCompanyID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].company  == null ? "02" : _providerAdditionalInsurance.listFormAdditionalInsurance[i].company.KODE,
          "insuranceProductID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].product.KODE,
          "period": _providerAdditionalInsurance.listFormAdditionalInsurance[i].periodType == "" ? null : int.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].periodType),
          "type": "",
          "type1": "",
          "type2": "",
          "subType": "",
          "subTypeCoverage": "",
          "insuranceSourceTypeID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageType != null ? _providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageType.KODE : "", // field jenis pertanggungan
          "insuranceValue": _providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageValue.isEmpty ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageValue.replaceAll(",", "")).round(),
          "upperLimitPercentage": _providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimitRate == "" ? 0.0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimitRate),
          "upperLimitAmount": _providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimit == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimit.replaceAll(",", "")),
          "lowerLimitPercentage": _providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimitRate == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimitRate),
          "lowerLimitAmount": _providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimit == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimit.replaceAll(",", "")),
          "insuranceCashFee": _providerAdditionalInsurance.listFormAdditionalInsurance[i].priceCash == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].priceCash.replaceAll(",", "")).round(),
          "insuranceCreditFee": _providerAdditionalInsurance.listFormAdditionalInsurance[i].priceCredit == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].priceCredit.replaceAll(",", "")).round(),
          "totalSplitFee": "",
          "totalInsuranceFeePercentage": _providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPriceRate == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPriceRate),
          "totalInsuranceFeeAmount": _providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPrice == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPrice.replaceAll(",", "")),
          "creationalSpecification": {
            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
            "createdBy": _preferences.getString("username"),
            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
            "modifiedBy": _preferences.getString("username"),
          },
          "status": "ACTIVE",
          "insurancePaymentType": null
        });
      }
    }

    var _installmentDetails = [];
    if(_installmentTypeSelected.id == "06") {
      // Stepping
      if(_providerCreditStructureTypeInstallment.listInfoCreditStructureStepping.isNotEmpty) {
        for(int i=0; i < _providerCreditStructureTypeInstallment.listInfoCreditStructureStepping.length; i++) {
          _installmentDetails.add(
              {
                "installmentID": "NEW",
                "installmentNumber": 0,
                "percentage": _providerCreditStructureTypeInstallment.listInfoCreditStructureStepping[i].controllerPercentage.text != "" ? double.parse(_providerCreditStructureTypeInstallment.listInfoCreditStructureStepping[i].controllerPercentage.text.replaceAll(",", "")) : 0,
                "amount": _providerCreditStructureTypeInstallment.listInfoCreditStructureStepping[i].controllerValue.text != "" ? double.parse(_providerCreditStructureTypeInstallment.listInfoCreditStructureStepping[i].controllerValue.text.replaceAll(",", "")) : 0,
                "status": "ACTIVE",
                "creationalSpecification": {
                  "createdAt": _dateTime,
                  "createdBy": _preferences.getString("username"),
                  "modifiedAt": _dateTime,
                  "modifiedBy": _preferences.getString("username"),
                },
              }
          );
        }
      }
    } else if(_installmentTypeSelected.id == "07") {
      // Irreguler
      if(_providerCreditStructureTypeInstallment.listInfoCreditStructureIrregularModel.isNotEmpty) {
        for(int i=0; i < _providerCreditStructureTypeInstallment.listInfoCreditStructureIrregularModel.length; i++) {
          _installmentDetails.add(
              {
                "installmentID": "NEW",
                "installmentNumber": i+1,
                "percentage": 0,
                "amount": _providerCreditStructureTypeInstallment.listInfoCreditStructureIrregularModel[i].controller.text != "" ? double.parse(_providerCreditStructureTypeInstallment.listInfoCreditStructureIrregularModel[i].controller.text.replaceAll(",", "")) : 0,
                "status": "ACTIVE",
                "creationalSpecification": {
                  "createdAt": _dateTime,
                  "createdBy": _preferences.getString("username"),
                  "modifiedAt": _dateTime,
                  "modifiedBy": _preferences.getString("username"),
                },
              }
          );
        }
      }
    }

    var _orderFees = [];
    if(_providerCreditStructure.listInfoFeeCreditStructure.isNotEmpty) {
      for(int i=0; i < _providerCreditStructure.listInfoFeeCreditStructure.length; i++) {
        _orderFees.add(
            {
              "orderFeeID": "NEW",
              "feeTypeID": "NEW",
              "cashFee": _providerCreditStructure.listInfoFeeCreditStructure[i].cashCost != null ? double.parse(_providerCreditStructure.listInfoFeeCreditStructure[i].cashCost.replaceAll(",", "")) : 0,
              "creditFee": _providerCreditStructure.listInfoFeeCreditStructure[i].creditCost != null ? double.parse(_providerCreditStructure.listInfoFeeCreditStructure[i].creditCost.replaceAll(",", "")) : 0,
              "totalFee": _providerCreditStructure.listInfoFeeCreditStructure[i].totalCost == null || _providerCreditStructure.listInfoFeeCreditStructure[i].totalCost == "" ? 0 : double.parse(_providerCreditStructure.listInfoFeeCreditStructure[i].totalCost.replaceAll(",", "")),
              "creationalSpecification": {
                "createdAt": _dateTime,
                "createdBy": _preferences.getString("username"),
                "modifiedAt": _dateTime,
                "modifiedBy": _preferences.getString("username"),
              },
              "status": "ACTIVE"
            }
        );
      }
    }

    var _orderSubsidies = [];
    var _orderSubsidyDetails = [];
    if(_providerSubsidy.listInfoCreditSubsidy.isNotEmpty) {
      for(int i=0; i < _providerSubsidy.listInfoCreditSubsidy.length; i++) {
        print("cek nilai klaim ${_providerSubsidy.listInfoCreditSubsidy[i].claimValue}");
        if(_providerSubsidy.listInfoCreditSubsidy[i].installmentDetail != null){
          for(int j=0; j < _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail.length; j++) {
            _orderSubsidyDetails.add(
                {
                  "orderSubsidyDetailID": "NEW",
                  "installmentNumber": _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentIndex != null ? double.parse(_providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentIndex.replaceAll(",", "")) : 0,
                  "installmentAmount": _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentSubsidy != null ? double.parse(_providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentSubsidy.replaceAll(",", "")) : 0,
                  "creationalSpecification": {
                    "createdAt": _dateTime,
                    "createdBy": _preferences.getString("username"),
                    "modifiedAt": _dateTime,
                    "modifiedBy": _preferences.getString("username"),
                  },
                  "status": "ACTIVE"
                }
            );
          }
        }
        _orderSubsidies.add(
            {
              "orderSubsidyID": "NEW",
              "subsidyProviderID": "1",
              "subsidyTypeID": _providerSubsidy.listInfoCreditSubsidy[i].type != null ? _providerSubsidy.listInfoCreditSubsidy[i].type.id : "",
              "subsidyMethodTypeID": _providerSubsidy.listInfoCreditSubsidy[i].cuttingMethod != null ? _providerSubsidy.listInfoCreditSubsidy[i].cuttingMethod.id : "",
              "refundAmount": _providerSubsidy.listInfoCreditSubsidy[i].claimValue == null || _providerSubsidy.listInfoCreditSubsidy[i].claimValue == "" ? 0 : double.parse(_providerSubsidy.listInfoCreditSubsidy[i].claimValue.replaceAll(",", "")),
              "refundAmountKlaim": 0,
              "effectiveRate": _providerSubsidy.listInfoCreditSubsidy[i].rateBeforeEff == null || _providerSubsidy.listInfoCreditSubsidy[i].rateBeforeEff == "" ? 0 : double.parse(_providerSubsidy.listInfoCreditSubsidy[i].rateBeforeEff.replaceAll(",", "")),
              "flatRate": _providerSubsidy.listInfoCreditSubsidy[i].rateBeforeFlat == null || _providerSubsidy.listInfoCreditSubsidy[i].rateBeforeFlat == "" ? 0 : double.parse(_providerSubsidy.listInfoCreditSubsidy[i].rateBeforeFlat.replaceAll(",", "")),
              "dpReal": 1,
              "totalInstallment": _providerSubsidy.listInfoCreditSubsidy[i].totalInstallment == null || _providerSubsidy.listInfoCreditSubsidy[i].totalInstallment == "" ? 0 : double.parse(_providerSubsidy.listInfoCreditSubsidy[i].totalInstallment.replaceAll(",", "")),
              "interestRate": 0,
              "creationalSpecification": {
                "createdAt": _dateTime,
                "createdBy": _preferences.getString("username"),
                "modifiedAt": _dateTime,
                "modifiedBy": _preferences.getString("username"),
              },
              "status": "ACTIVE",
              "orderSubsidyDetails": _orderSubsidyDetails
            }
        );
      }
    }

    var _orderWmps = [];
    if(_providerWmp.listWMP.isNotEmpty) {
      for(int i=0; i < _providerWmp.listWMP.length; i++) {
        _orderWmps.add(
          {
            "orderWmpID": "NEW",
            "wmpNumber": _providerWmp.listWMP[i].noProposal,
            "wmpType": _providerWmp.listWMP[i].type,
            "wmpJob": "",
            "wmpSubsidyTypeID": _providerWmp.listWMP[i].typeSubsidi,
            "maxRefundAmount": _providerWmp.listWMP[i].amount,
            "status": "ACTIVE",
            "creationalSpecification": {
              "createdAt": _dateTime,
              "createdBy": _preferences.getString("username"),
              "modifiedAt": _dateTime,
              "modifiedBy": _preferences.getString("username"),
            },
          },
        );
      }
    }

    var _body = jsonEncode(
        {
          "orderProductID": "NEW",
          "orderProductSpecification": {
            "financingTypeID": this._custType != "COM" ? "${_providerFoto.typeOfFinancingModelSelected.financingTypeId}" : "${_providerUnitObject.typeOfFinancingModelSelected.financingTypeId}",
            "ojkBusinessTypeID": this._custType == "COM" ? "${_providerUnitObject.businessActivitiesModelSelected.id}" : "${_providerFoto.kegiatanUsahaSelected.id}",
            "ojkBussinessDetailID": this._custType == "COM" ? "${_providerUnitObject.businessActivitiesTypeModelSelected.id}" : "${_providerFoto.jenisKegiatanUsahaSelected.id}",
            "orderUnitSpecification": {
              "objectGroupID": _providerUnitObject.groupObjectSelected != null ? _providerUnitObject.groupObjectSelected.KODE : "",
              "objectID": _providerUnitObject.objectSelected != null ? _providerUnitObject.objectSelected.id : "",
              "productTypeID": _providerUnitObject.productTypeSelected != null ? _providerUnitObject.productTypeSelected.id : "",
              "objectBrandID": _providerUnitObject.brandObjectSelected != null ? _providerUnitObject.brandObjectSelected.id : "",
              "objectTypeID": _providerUnitObject.objectTypeSelected != null ? _providerUnitObject.objectTypeSelected.id : "",
              "objectModelID": _providerUnitObject.modelObjectSelected != null ? _providerUnitObject.modelObjectSelected.id : "",
              "modelDetail": "",
              "objectUsageID": _providerUnitObject.objectUsageModel != null ? _providerUnitObject.objectUsageModel.id : "",
              "objectPurposeID": _providerUnitObject.objectPurposeSelected != null ? _providerUnitObject.objectPurposeSelected.id : ""
            },
            "salesGroupID": "00003",
            "groupID": "",
            "orderSourceID": "1",
            "orderSourceName": null,
            "orderProductDealerSpecification": {
              "isThirdParty": true,
              "thirdPartyTypeID": _providerUnitObject.thirdPartyTypeSelected != null ? _providerUnitObject.thirdPartyTypeSelected.kode : "",
              "thirdPartyID": _providerUnitObject.thirdPartySelected != null ? _providerUnitObject.thirdPartySelected.kode : "",
              "thirdPartyActivity": "1",
              "sentradID": _preferences.getString("SentraD"),
              "unitdID": _preferences.getString("UnitD"),
              "dealerMatrix": _providerUnitObject.matriksDealerSelected != null ? _providerUnitObject.matriksDealerSelected.kode : ""
            },
            "outletID": "",
            "programID": _providerUnitObject.programSelected != null ? _providerUnitObject.programSelected.kode : "",
            "rehabType": _providerUnitObject.rehabTypeSelected != null ? _providerUnitObject.rehabTypeSelected.id : "",
            "referenceNumber": _providerUnitObject.referenceNumberModel != null ? _providerUnitObject.referenceNumberModel.noPK : "",
            "applicationUnitContractNumber": null,
            "orderProductSaleses": _orderProductSaleses,
            "orderKaroseris": _orderKaroseris,
            "orderProductInsurances": _orderProductInsurances,
            "orderWmps": _orderWmps
          },
          "orderCreditStructure": {
            "installmentTypeID": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.id : "",
            "tenor": _providerCreditStructure.periodOfTimeSelected != null ? int.parse(_providerCreditStructure.periodOfTimeSelected) : 0,
            "paymentMethodID": _providerCreditStructure.paymentMethodSelected != null ? _providerCreditStructure.paymentMethodSelected.id : "",
            "effectiveRate": _providerCreditStructure.controllerInterestRateEffective.text != "" ? double.parse(_providerCreditStructure.controllerInterestRateEffective.text.replaceAll(",", "")) : 0,
            "flatRate": _providerCreditStructure.controllerInterestRateFlat.text != "" ? double.parse(_providerCreditStructure.controllerInterestRateFlat.text.replaceAll(",", "")) : 0,
            "objectPrice": _providerCreditStructure.controllerObjectPrice.text != "" ? double.parse(_providerCreditStructure.controllerObjectPrice.text.replaceAll(",", "")) : 0,
            "karoseriTotalPrice": _providerCreditStructure.controllerKaroseriTotalPrice.text != "" ? double.parse(_providerCreditStructure.controllerKaroseriTotalPrice.text.replaceAll(",", "")) : 0,
            "totalPrice": _providerCreditStructure.controllerTotalPrice.text != "" ? double.parse(_providerCreditStructure.controllerTotalPrice.text.replaceAll(",", "")) : 0,
            "insentifSales": 0,
            "insentifPayment": 0,
            "nettDownPayment": _providerCreditStructure.controllerNetDP.text != "" ? double.parse(_providerCreditStructure.controllerNetDP.text.replaceAll(",", "")) : 0,
            "declineNInstallment": 0,
            "paymentOfYear": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.id == "04" ? int.parse(_providerCreditStructure.controllerPaymentPerYear.text) : 1 : 1,
            "branchDownPayment": _providerCreditStructure.controllerBranchDP.text != "" ? double.parse(_providerCreditStructure.controllerBranchDP.text.replaceAll(",", "")) : 0,
            "grossDownPayment": _providerCreditStructure.controllerGrossDP.text != "" ? double.parse(_providerCreditStructure.controllerGrossDP.text.replaceAll(",", "")) : 0,
            "totalLoan": _providerCreditStructure.controllerTotalLoan.text != "" ? double.parse(_providerCreditStructure.controllerTotalLoan.text.replaceAll(",", "")) : 0,
            "installmentAmount": _providerCreditStructure.controllerInstallment.text != "" ? double.parse(_providerCreditStructure.controllerInstallment.text.replaceAll(",", "")) : 0,
            "ltvRatio": _providerCreditStructure.controllerLTV.text != "" ? double.parse(_providerCreditStructure.controllerLTV.text.replaceAll(",", "")) : 0,
            "interestAmount": this._interestAmount,
            "pencairan": 0,
            "realDSR": null,
            "gpType": _providerCreditStructureTypeInstallment.gpTypeSelected != null ? _providerCreditStructureTypeInstallment.gpTypeSelected.id : "",
            "newTenor": _providerCreditStructureTypeInstallment.controllerNewTenor.text != "" ? double.parse(_providerCreditStructureTypeInstallment.controllerNewTenor.text.replaceAll(",", "")) : 0,
            "totalStepping": _providerCreditStructureTypeInstallment.totalSteppingSelected != null ? _providerCreditStructureTypeInstallment.totalSteppingSelected : 0,
            // "uangMukaKaroseri": 0, // diset 0 diganti di dalam
            "uangMukaChasisNet": _providerCreditStructure.controllerNetDP.text != "" ? double.parse(_providerCreditStructure.controllerNetDP.text.replaceAll(",", "")) : 0, // set sama kaya nettDownPayment
            "uangMukaChasisGross": _providerCreditStructure.controllerGrossDP.text != "" ? double.parse(_providerCreditStructure.controllerGrossDP.text.replaceAll(",", "")) : 0, // set sama kaya grossDownPayment
            "installmentBalloonPayment": {
              "balloonType": _providerCreditStructureTypeInstallment.radioValueBalloonPHOrBalloonInstallment == 1 ? true : false, // _providerCreditStructureTypeInstallment.radioValueBalloonPHOrBalloonInstallment != -1 ? _providerCreditStructureTypeInstallment.radioValueBalloonPHOrBalloonInstallment == 1 ? true : false : false,
              "lastInstallmentPercentage": _providerCreditStructureTypeInstallment.controllerInstallmentPercentage.text != "" ? double.parse(_providerCreditStructureTypeInstallment.controllerInstallmentPercentage.text.replaceAll(",", "")) : 0, // _providerCreditStructureTypeInstallment.radioValueBalloonPHOrBalloonInstallment == 1 ? _providerCreditStructureTypeInstallment.controllerInstallmentPercentage.text != "" ? double.parse(_providerCreditStructureTypeInstallment.controllerInstallmentPercentage.text.replaceAll(",", "")) : 0 : 0,
              "lastInstallmentValue": _providerCreditStructureTypeInstallment.controllerInstallmentValue.text != "" ? double.parse(_providerCreditStructureTypeInstallment.controllerInstallmentValue.text.replaceAll(",", "")) : 0
            },
            "installmentDetails": _installmentDetails,
            "installmentSchedule": {
              "installmentType": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.name : "",
              "paymentMethodType": _providerCreditStructure.paymentMethodSelected != null ? _providerCreditStructure.paymentMethodSelected.name : "",
              "roundingValue": null,
              "installment": 0,
              "installmentRound": 0,
              "installmentMethod": 0,
              "lastKnownOutstanding": 0,
              "minInterest": 0,
              "maxInterest": 0,
              "futureValue": 0,
              "isInstallment": false,
              "balloonInstallment": 0,
              "gracePeriode": 0,
              "gracePeriodes": [],
              "seasonal": 1,
              "steppings": [],
              "installmentScheduleDetails": []
            },
            "firstInstallment": null,
            "dsr": _providerCreditIncome.controllerDebtComparison.text == "" ||  _providerCreditIncome.controllerDebtComparison.text == "null" ? 0 : double.parse(_providerCreditIncome.controllerDebtComparison.text.replaceAll(",", "")),
            "dir": _providerCreditIncome.controllerIncomeComparison.text == "" || _providerCreditIncome.controllerIncomeComparison.text == "null" ? 0 : double.parse(_providerCreditIncome.controllerIncomeComparison.text.replaceAll(",", "")),
            "dsc": _providerCreditIncome.controllerDSC.text == "" || _providerCreditIncome.controllerDSC.text == "null" ? 0 : double.parse(_providerCreditIncome.controllerDSC.text.replaceAll(",", "")),
            "irr": _providerCreditIncome.controllerIRR.text == "" || _providerCreditIncome.controllerIRR.text == "null"? 0 : double.parse(_providerCreditIncome.controllerIRR.text.replaceAll(",", ""))
          },
          "orderFees": _orderFees,
          "orderSubsidies": _orderSubsidies,
          "isWithoutColla": false,
          "applSendFlag": null,
          "pelunsanaAmount": null,
          "collateralAutomotives": _providerKolateral.collateralTypeModel.id == "001" ?
          [
            {
              "collateralID": "NEW",
              "isCollaSameUnit": _providerKolateral.radioValueIsCollateralSameWithUnitOto == 0 ? true : false,
              "isMultiUnitCollateral": true,
              "isCollaNameSameWithCustomerName": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0 ? true : false,
              "identitySpecification": {
                "identityType": _preferences.getString("cust_type") == "PER"
                    ? _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? _providerInfoNasabah.identitasModel != null
                    ? _providerInfoNasabah.identitasModel.id : ""
                    : _providerKolateral.identityTypeSelectedAuto != null
                    ? _providerKolateral.identityTypeSelectedAuto.id : ""
                    : _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? ""
                    : _providerKolateral.identityTypeSelectedAuto != null
                    ? _providerKolateral.identityTypeSelectedAuto.id : "",
                "identityNumber": _preferences.getString("cust_type") == "PER"
                    ? _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? _providerInfoNasabah.controllerNoIdentitas.text != ""
                    ? _providerInfoNasabah.controllerNoIdentitas.text : ""
                    : _providerKolateral.controllerIdentityNumberAuto.text != ""
                    ? _providerKolateral.controllerIdentityNumberAuto.text : ""
                    : _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? _providerRincian.controllerNPWP.text != ""
                    ? _providerRincian.controllerNPWP.text : ""
                    : _providerKolateral.identityTypeSelectedAuto != null
                    ? _providerKolateral.identityTypeSelectedAuto.id : "",
                "identityName": _preferences.getString("cust_type") == "PER"
                    ? _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? _providerInfoNasabah.controllerNamaLengkapSesuaiIdentitas.text != ""
                    ? _providerInfoNasabah.controllerNamaLengkapSesuaiIdentitas.text : ""
                    : _providerKolateral.controllerNameOnCollateralAuto.text != ""
                    ? _providerKolateral.controllerNameOnCollateralAuto.text : ""
                    : _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? _providerRincian.controllerFullNameNPWP.text != ""
                    ? _providerRincian.controllerFullNameNPWP.text : ""
                    : _providerKolateral.controllerNameOnCollateralAuto.text != ""
                    ? _providerKolateral.controllerNameOnCollateralAuto.text : "",
                "fullName": _preferences.getString("cust_type") == "PER"
                    ? _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? _providerInfoNasabah.controllerNamaLengkap.text != ""
                    ? _providerInfoNasabah.controllerNamaLengkap.text : ""
                    : _providerKolateral.controllerNameOnCollateralAuto.text != ""
                    ? _providerKolateral.controllerNameOnCollateralAuto.text : ""
                    : _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? _providerRincian.controllerFullNameNPWP.text != ""
                    ? _providerRincian.controllerFullNameNPWP.text : ""
                    : _providerKolateral.controllerNameOnCollateralAuto.text != ""
                    ? _providerKolateral.controllerNameOnCollateralAuto.text : "",
                "alias": null,
                "title": null,
                "dateOfBirth": _providerKolateral.controllerBirthDateAuto.text != "" ? "${_providerKolateral.controllerBirthDateAuto.text} 00:00:00" : "01-01-1990 00:00:00",
                // _preferences.getString("cust_type") == "PER"
                //     ? _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0
                //     ? _providerInfoNasabah.controllerTglLahir.text != ""
                //     ? "${_providerInfoNasabah.controllerTglLahir.text} 00:00:00": ""
                //     : _providerKolateral.initialDateForBirthDateAuto.millisecondsSinceEpoch.toString() != ""
                //     ? _providerKolateral.initialDateForBirthDateAuto.millisecondsSinceEpoch.toString() : ""
                //     : null,
                "placeOfBirth": _preferences.getString("cust_type") == "PER"
                    ? _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? _providerInfoNasabah.controllerTempatLahirSesuaiIdentitas.text != ""
                    ? _providerInfoNasabah.controllerTempatLahirSesuaiIdentitas.text : ""
                    : _providerKolateral.controllerBirthPlaceValidWithIdentityAuto.text != ""
                    ? _providerKolateral.controllerBirthPlaceValidWithIdentityAuto.text : ""
                    : null,
                "placeOfBirthKabKota": _preferences.getString("cust_type") == "PER"
                    ? _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? _providerInfoNasabah.birthPlaceSelected != null
                    ? _providerInfoNasabah.birthPlaceSelected.KABKOT_ID : ""
                    : _providerKolateral.birthPlaceAutoSelected != null
                    ? _providerKolateral.birthPlaceAutoSelected.KABKOT_ID : ""
                    : null,
                "gender": _preferences.getString("cust_type") == "PER"
                    ? _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? _providerInfoNasabah.radioValueGender == "01"
                    ? "Laki-Laki" : "Perempuan" : null
                    : null,
                "identityActiveStart": null,
                "identityActiveEnd": null,
                "isLifetime": false,
                "religion": _preferences.getString("cust_type") == "PER" ? _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1 ? _providerInfoNasabah.religionSelected != null ? _providerInfoNasabah.religionSelected.id : null : null : null,
                "occupationID": _preferences.getString("cust_type") == "PER" ? _providerFoto.occupationSelected != null ? _providerFoto.occupationSelected.KODE : "" : null,
                "positionID": null,
                "maritalStatusID": _preferences.getString("cust_type") == "PER" ? _providerInfoNasabah.maritalStatusSelected != null ? _providerInfoNasabah.maritalStatusSelected.id : "" : null
              },
              "collateralAutomotiveSpecification": {
                "orderUnitSpecification": {
                  "objectGroupID": _providerKolateral.groupObjectSelected != null ? _providerKolateral.groupObjectSelected.KODE : "",
                  "objectID": _providerKolateral.objectSelected != null ? _providerKolateral.objectSelected.id : "",
                  "productTypeID": _providerKolateral.productTypeSelected != null ? _providerKolateral.productTypeSelected.id : "",
                  "objectBrandID": _providerKolateral.brandObjectSelected != null ? _providerKolateral.brandObjectSelected.id : "",
                  "objectTypeID": _providerKolateral.objectTypeSelected != null ? _providerKolateral.objectTypeSelected.id : "",
                  "objectModelID": _providerKolateral.modelObjectSelected != null ? _providerKolateral.modelObjectSelected.id : "",
                  "modelDetail": "",
                  "objectUsageID": _providerKolateral.objectUsageSelected != null ? _providerKolateral.objectUsageSelected.id : "",
                  "objectPurposeID": ""
                },
                "productionYear": _providerKolateral.yearProductionSelected != null ? int.parse(_providerKolateral.yearProductionSelected) : "",
                "registrationYear": _providerKolateral.yearRegistrationSelected != null ? int.parse(_providerKolateral.yearRegistrationSelected) : "",
                "isYellowPlate": _providerKolateral.radioValueYellowPlat == 0 ? true : false,
                "isBuiltUp": _providerKolateral.radioValueBuiltUpNonATPM == 0 ? true : false,
                "utjSpecification": {
                  "bpkbNumber": _providerKolateral.controllerBPKPNumber.text != "" ? _providerKolateral.controllerBPKPNumber.text : "",
                  "chassisNumber": _providerKolateral.controllerFrameNumber.text != "" ? _providerKolateral.controllerFrameNumber.text : "",
                  "engineNumber": _providerKolateral.controllerMachineNumber.text != "" ? _providerKolateral.controllerMachineNumber.text : "",
                  "policeNumber": _providerKolateral.controllerPoliceNumber.text != "" ? _providerKolateral.controllerPoliceNumber.text : ""
                },
                "gradeUnit": _providerKolateral.controllerGradeUnit.text != "" ? _providerKolateral.controllerGradeUnit.text : "",
                "utjFacility": _providerKolateral.controllerFasilitasUTJ.text != "" ? _providerKolateral.controllerFasilitasUTJ.text : "",
                "bidderName": _providerKolateral.controllerNamaBidder.text != "" ? _providerKolateral.controllerNamaBidder.text : "",
                "showroomPrice": _providerKolateral.controllerHargaJualShowroom.text != "" ? double.parse(_providerKolateral.controllerHargaJualShowroom.text.replaceAll(",", "")) : 0,
                "additionalEquipment": _providerKolateral.controllerPerlengkapanTambahan.text != "" ? _providerKolateral.controllerPerlengkapanTambahan.text : "",
                "mpAdira": _providerKolateral.controllerMPAdira.text != "" ? _providerKolateral.controllerMPAdira.text.replaceAll(",", "") : 0,
                "physicalRecondition": _providerKolateral.controllerRekondisiFisik.text != "" ? double.parse(_providerKolateral.controllerRekondisiFisik.text.replaceAll(",", "")) : 0,
                "isProper": _providerKolateral.radioValueWorthyOrUnworthy == 0 ? false : true,
                "ltvRatio": _providerKolateral.controllerLTV.text != "" ? double.parse(_providerKolateral.controllerLTV.text.replaceAll(",", "")) : 0,
                "appraisalSpecification": {
                  "collateralDp": _providerKolateral.controllerDPJaminan.text != "" ? double.parse(_providerKolateral.controllerDPJaminan.text.replaceAll(",", "")) : 0,
                  "maximumPh": _providerKolateral.controllerPHMax.text != "" ? double.parse(_providerKolateral.controllerPHMax.text.replaceAll(",", "")) : 0,
                  "appraisalPrice": _providerKolateral.controllerTaksasiPriceAutomotive.text != "" ? double.parse(_providerKolateral.controllerTaksasiPriceAutomotive.text.replaceAll(",", "")) : 0,
                  "collateralUtilizationID": ""
                },
                "mpAdiraUpld": 0,
                "collateralAutomotiveAdditionalSpecification": {
                  "capacity": 0,
                  "color": null,
                  "manufacturer": null,
                  "serialNumber": null,
                  "invoiceNumber": null,
                  "stnkActiveDate": null,
                  "bpkbAddress": null,
                  "bpkbIdentityTypeID": null
                },
              },
              "status": "ACTIVE",
              "creationalSpecification": {
                "createdAt": _dateTime,
                "createdBy": _preferences.getString("username"),
                "modifiedAt": _dateTime,
                "modifiedBy": _preferences.getString("username"),
              }
            }
          ]
              :
          [],
          "collateralProperties": _providerKolateral.collateralTypeModel.id == "002" ?
          [
            {
              "isMultiUnitCollateral": true,
              "isCollaNameSameWithCustomerName": _providerKolateral.radioValueIsCollateralSameWithApplicantProperty == 0,
              "collateralID": "NEW",
              "identitySpecification": {
                "identityType": _preferences.getString("cust_type") == "PER"
                    ? _providerKolateral.radioValueIsCollateralSameWithApplicantProperty == 1
                    ? _providerInfoNasabah.identitasModel != null
                    ? _providerInfoNasabah.identitasModel.id : ""
                    : _providerKolateral.identityModel != null
                    ? _providerKolateral.identityModel.id : ""
                    : _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? ""
                    : _providerKolateral.identityTypeSelectedAuto != null
                    ? _providerKolateral.identityTypeSelectedAuto.id : "",
                "identityNumber": _preferences.getString("cust_type") == "PER"
                    ? _providerKolateral.radioValueIsCollateralSameWithApplicantProperty == 1
                    ? _providerInfoNasabah.controllerNoIdentitas.text != ""
                    ? _providerInfoNasabah.controllerNoIdentitas.text : ""
                    : _providerKolateral.controllerIdentityNumber.text != ""
                    ? _providerKolateral.controllerIdentityNumber.text : ""
                    : _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? _providerRincian.controllerNPWP.text != ""
                    ? _providerRincian.controllerNPWP.text : ""
                    : _providerKolateral.identityTypeSelectedAuto != null
                    ? _providerKolateral.identityTypeSelectedAuto.id : "",
                "identityName": _preferences.getString("cust_type") == "PER"
                    ? _providerKolateral.radioValueIsCollateralSameWithApplicantProperty == 1
                    ? _providerInfoNasabah.controllerNamaLengkapSesuaiIdentitas.text != ""
                    ? _providerInfoNasabah.controllerNamaLengkapSesuaiIdentitas.text : ""
                    : _providerKolateral.controllerNameOnCollateral.text != ""
                    ? _providerKolateral.controllerNameOnCollateral.text : ""
                    : _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? _providerRincian.controllerFullNameNPWP.text != ""
                    ? _providerRincian.controllerFullNameNPWP.text : ""
                    : _providerKolateral.controllerNameOnCollateralAuto.text != ""
                    ? _providerKolateral.controllerNameOnCollateralAuto.text : "",
                "fullName": _preferences.getString("cust_type") == "PER"
                    ? _providerKolateral.radioValueIsCollateralSameWithApplicantProperty == 1
                    ? _providerInfoNasabah.controllerNamaLengkap.text != ""
                    ? _providerInfoNasabah.controllerNamaLengkap.text : ""
                    : _providerKolateral.controllerNameOnCollateral.text != ""
                    ? _providerKolateral.controllerNameOnCollateral.text : ""
                    : _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? _providerRincian.controllerFullNameNPWP.text != ""
                    ? _providerRincian.controllerFullNameNPWP.text : ""
                    : _providerKolateral.controllerNameOnCollateralAuto.text != ""
                    ? _providerKolateral.controllerNameOnCollateralAuto.text : "",
                "alias": null,
                "title": null,
                "dateOfBirth": _providerKolateral.controllerBirthDateProp.text != "" ? "${_providerKolateral.controllerBirthDateProp.text} 00:00:00": "01-01-1990 00:00:00",
                // _preferences.getString("cust_type") == "PER"
                //     ? _providerKolateral.initialDateForBirthDateAuto != null
                //     ? _providerKolateral.initialDateForBirthDateAuto.millisecondsSinceEpoch.toString() : ""
                //     : null,
                "placeOfBirth": _providerKolateral.controllerBirthPlaceValidWithIdentity1.text,
                // _preferences.getString("cust_type") == "PER"
                //     ? _providerKolateral.controllerBirthPlaceValidWithIdentityAuto.text != ""
                //     ? _providerKolateral.controllerBirthPlaceValidWithIdentityAuto.text : ""
                //     : null,
                "placeOfBirthKabKota": _providerKolateral.birthPlaceSelected.KABKOT_ID,
                // _preferences.getString("cust_type") == "PER"
                //     ? _providerKolateral.controllerBirthPlaceValidWithIdentityLOVAuto.text != ""
                //     ? _providerKolateral.controllerBirthPlaceValidWithIdentityLOVAuto.text : ""
                //     : null,
                "gender": _preferences.getString("cust_type") == "PER"
                    ? _providerKolateral.radioValueIsCollateralSameWithApplicantProperty == 1
                    ? _providerInfoNasabah.radioValueGender == "01"
                    ? "Laki-Laki" : "Perempuan"
                    : null
                    : null,
                "identityActiveStart": null,
                "identityActiveEnd": null,
                "isLifetime": false,
                "religion": _preferences.getString("cust_type") == "PER"
                    ? _providerKolateral.radioValueIsCollateralSameWithApplicantProperty == 1
                    ? _providerInfoNasabah.religionSelected != null
                    ? _providerInfoNasabah.religionSelected.id : null
                    : null
                    : null,
                "occupationID": _preferences.getString("cust_type") == "PER"
                    ? _providerFoto.occupationSelected != null
                    ? _providerFoto.occupationSelected != null ? _providerFoto.occupationSelected.KODE : ""
                    : null
                    : null,
                "positionID": null,
                "maritalStatusID": null
              },
              "detailAddresses": [
                {
                  "addressID": "NEW",
                  "foreignBusinessID": "NEW",
                  "addressSpecification": {
                    "koresponden": "1",
                    "matrixAddr": "1",
                    "address": _providerKolateral.controllerAddress.text != "" ? _providerKolateral.controllerAddress.text : "",
                    "rt": _providerKolateral.controllerRT.text != "" ? _providerKolateral.controllerRT.text : "",
                    "rw": _providerKolateral.controllerRW.text != "" ? _providerKolateral.controllerRW.text : "",
                    "provinsiID": _providerKolateral.kelurahanSelected != null ? _providerKolateral.kelurahanSelected.PROV_ID : "",
                    "kabkotID": _providerKolateral.kelurahanSelected != null ? _providerKolateral.kelurahanSelected.KABKOT_ID : "",
                    "kecamatanID": _providerKolateral.kelurahanSelected != null ? _providerKolateral.kelurahanSelected.KEC_ID : "",
                    "kelurahanID": _providerKolateral.kelurahanSelected != null ? _providerKolateral.kelurahanSelected.KEL_ID : "",
                    "zipcode": _providerKolateral.kelurahanSelected != null ? _providerKolateral.kelurahanSelected.ZIPCODE : "",
                  },
                  "contactSpecification": {
                    "telephoneArea1": "",
                    "telephone1": "",
                    "telephoneArea2": "",
                    "telephone2": "",
                    "faxArea": "",
                    "fax": "",
                    "handphone": "",
                    "email": "",
                    "noWa": null,
                    "noWa2": null,
                    "noWa3": null
                  },
                  "addressType": _providerKolateral.addressTypeSelected != null ? _providerKolateral.addressTypeSelected.KODE : "",
                  "creationalSpecification": {
                    "createdAt": _dateTime,
                    "createdBy": _preferences.getString("username"),
                    "modifiedAt": _dateTime,
                    "modifiedBy": _preferences.getString("username"),
                  },
                  "status": "ACTIVE"
                }
              ],
              "collateralPropertySpecification": {
                "certificateNumber": _providerKolateral.controllerCertificateNumber.text != "" ? _providerKolateral.controllerCertificateNumber.text : "",
                "certificateTypeID": _providerKolateral.certificateTypeSelected != null ? _providerKolateral.certificateTypeSelected.id : "",
                "propertyTypeID": _providerKolateral.propertyTypeSelected != null ? _providerKolateral.propertyTypeSelected.id : "",
                "buildingArea": _providerKolateral.controllerBuildingArea.text != "" ? double.parse(_providerKolateral.controllerBuildingArea.text.replaceAll(",", "")) : 0,
                "landArea": _providerKolateral.controllerSurfaceArea.text != "" ? double.parse(_providerKolateral.controllerSurfaceArea.text.replaceAll(",", "")) : 0,
                "appraisalSpecification": {
                  "collateralDp": 1,
                  "maximumPh": _providerKolateral.controllerPHMax.text != "" ? double.parse(_providerKolateral.controllerPHMax.text.replaceAll(",", "")) : 0,
                  "appraisalPrice": 0,
                  "collateralUtilizationID": _providerKolateral.collateralUsagePropertySelected != null ? _providerKolateral.collateralUsagePropertySelected.id : "" //"" field tujuan penggunaan collateral
                },
                "positiveFasumDistance": _providerKolateral.controllerJarakFasumPositif.text != "" ? double.parse(_providerKolateral.controllerJarakFasumPositif.text.replaceAll(",", "")) : 0,
                "negativeFasumDistance": _providerKolateral.controllerJarakFasumNegatif.text != "" ? double.parse(_providerKolateral.controllerJarakFasumNegatif.text.replaceAll(",", "")) : 0,
                "landPrice": _providerKolateral.controllerHargaTanah.text != "" ? double.parse(_providerKolateral.controllerHargaTanah.text.replaceAll(",", "")) : 0,
                "njopPrice": _providerKolateral.controllerHargaNJOP.text != "" ? double.parse(_providerKolateral.controllerHargaNJOP.text.replaceAll(",", "")) : 0,
                "buildingPrice": _providerKolateral.controllerHargaBangunan.text != "" ? double.parse(_providerKolateral.controllerHargaBangunan.text.replaceAll(",", "")) : 0,
                "roofTypeID": _providerKolateral.typeOfRoofSelected != null ? _providerKolateral.typeOfRoofSelected.id : "",
                "wallTypeID": _providerKolateral.wallTypeSelected != null ? _providerKolateral.wallTypeSelected.id : "",
                "floorTypeID": _providerKolateral.floorTypeSelected != null ? _providerKolateral.floorTypeSelected.id : "",
                "foundationTypeID": _providerKolateral.foundationTypeSelected != null ? _providerKolateral.foundationTypeSelected.id : "",
                "roadTypeID": _providerKolateral.streetTypeSelected != null ? _providerKolateral.streetTypeSelected.id : "",
                "isCarPassable": _providerKolateral.radioValueAccessCar != -1 ? _providerKolateral.radioValueAccessCar == 0 ? true : false : false,
                "totalOfHouseInRadius": _providerKolateral.controllerJumlahRumahDalamRadius.text != "" ? double.parse(_providerKolateral.controllerJumlahRumahDalamRadius.text.replaceAll(",", "")).toInt() : 0,
                "sifatJaminan": _providerKolateral.controllerSifatJaminan.text != "" ? _providerKolateral.controllerSifatJaminan.text : "",
                "buktiKepemilikanTanah": _providerKolateral.controllerBuktiKepemilikan.text != "" ? _providerKolateral.controllerBuktiKepemilikan.text : "",
                "tgglTerbitSertifikat": _providerKolateral.controllerCertificateReleaseDate.text != "" ? "${_providerKolateral.controllerCertificateReleaseDate.text} 00:00:00" : "01-01-1900 00:00:00",//_providerKolateral.initialDateCertificateRelease != null ? _providerKolateral.initialDateCertificateRelease.millisecondsSinceEpoch.toString() : "",
                "tahunTerbitSertifikat": _providerKolateral.controllerCertificateReleaseYear.text != "" ? double.parse(_providerKolateral.controllerCertificateReleaseYear.text.replaceAll(",", "")) : 0,
                "namaPemegangHak": _providerKolateral.controllerNamaPemegangHak.text != "" ? _providerKolateral.controllerNamaPemegangHak.text : "",
                "noSuratUkurGmbrSituasi": _providerKolateral.controllerNoSuratUkur.text != "" ? _providerKolateral.controllerNoSuratUkur.text : "",
                "tgglSuratUkurGmbrSituasi": _providerKolateral.controllerDateOfMeasuringLetter.text != "" ? "${_providerKolateral.controllerDateOfMeasuringLetter.text} 00:00:00" : "01-01-1900 00:00:00",//_providerKolateral.initialDateOfMeasuringLetter != null ? _providerKolateral.initialDateOfMeasuringLetter.millisecondsSinceEpoch.toString() : "",
                "sertifikatSuratUkurDikeluarkanOleh": _providerKolateral.controllerCertificateOfMeasuringLetter.text != "" ? _providerKolateral.controllerCertificateOfMeasuringLetter.text : "",
                "masaBerlakuHak": _providerKolateral.controllerMasaHakBerlaku.text != "" ? _providerKolateral.controllerMasaHakBerlaku.text : "",
                "noImb": _providerKolateral.controllerNoIMB.text != "" ? _providerKolateral.controllerNoIMB.text : "",
                "tgglImb": _providerKolateral.controllerDateOfIMB.text != "" ? "${_providerKolateral.controllerDateOfIMB.text} 00:00:00" : "01-01-1900 00:00:00",//_providerKolateral.initialDateOfIMB != null ? _providerKolateral.initialDateOfIMB.millisecondsSinceEpoch.toString() : "",
                "luasBangunanImb": _providerKolateral.controllerLuasBangunanIMB.text != "" ? double.parse(_providerKolateral.controllerLuasBangunanIMB.text.replaceAll(",", "")) : 0,
                "ltvRatio": _providerKolateral.controllerLTV.text != "" ? double.parse(_providerKolateral.controllerLTV.text.replaceAll(",", "")) : 0,
                "letakTanah": "",
                "propertyGroupObjtID": "",
                "propertyObjtID": ""
              },
              "status": "ACTIVE",
              "creationalSpecification": {
                "createdAt": _dateTime,
                "createdBy": _preferences.getString("username"),
                "modifiedAt": _dateTime,
                "modifiedBy": _preferences.getString("username"),
              },
            }
          ]
              :
          [],
          "orderProductAdditionalSpecification": {
            "virtualAccountID": null,
            "virtualAccountValue": 0,
            "cashingPurposeID": null,
            "cashingTypeID": null
          },
          "autoDebitSpecification": {
            "isAutoDebit": null,
            "bankID": "",
            "accountNumber": "",
            "accountBehalf": "",
            "accountPurposeTypeID": "",
            "downPaymentSourceID": ""
          },
          "orderProductProcessingCompletion": {
            "lastKnownOrderProductState": "APPLICATION_UNIT",
            "nextKnownOrderProductState": "APPLICATION_KAROSERI"
          },
          "tacSpesification": {
            "tacActual": 0,
            "tacMax": 0
          },
          "isSaveKaroseri": false,
          "creationalSpecification": {
            "createdAt": _dateTime,
            "createdBy": _preferences.getString("username"),
            "modifiedAt": _dateTime,
            "modifiedBy": _preferences.getString("username"),
          },
          "status": "ACTIVE",
          "nik": _preferences.getString("username"),
          "nama": _preferences.getString("fullname"),
          "jabatan": _preferences.getString("job_id"),
        }
    );
    print(_body);

    try{
      DateTime _timeStartValidate = DateTime.now();
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);
      String _calculateStrukturKredit = await storage.read(key: "CalculateStrukturKredit");
      print("${BaseUrl.urlGeneral}$_calculateStrukturKredit"+"rate=$_rate&rateType=$_flag&downPayment=$_dp&collateralType=$_collaType");
      final _response = await _http.post(
        // "http://10.134.177.143:82/ad1ms2_backend/public/api/testSubmit",
        // "http://192.168.57.98:82/ad1ms2_backend/public/api/strukturKredit",
        //     "http://192.168.57.122/orca_api/public/login",
          "${BaseUrl.urlGeneral}$_calculateStrukturKredit"+"rate=$_rate&rateType=$_flag&downPayment=$_dp&collateralType=$_collaType",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      );
      _providerParentIndividu.insertLog(context, _timeStartValidate, DateTime.now(), _body, "${BaseUrl.urlGeneral}$_calculateStrukturKredit"+"rate=$_rate&rateType=$_flag&downPayment=$_dp&collateralType=$_collaType", "Start Calculate Credit");
      if(_response.statusCode == 200 || _response.statusCode == 302){
        final _result = jsonDecode(_response.body);
        // interest amount
        this._interestAmount = _result['interestAmount'];
        //baloon payment
        if(this._installmentTypeSelected != null) {
          if(this._installmentTypeSelected.id != "08") {
            _providerCreditStructureTypeInstallment.radioValueBalloonPHOrBalloonInstallment = _result['installmentBalloonPayment']['balloonType'] == "false" ? 0 : 1;
            _providerCreditStructureTypeInstallment.controllerInstallmentPercentage.text = _result['installmentBalloonPayment']['lastInstallmentPercentage'].toString();
            _providerCreditStructureTypeInstallment.controllerInstallmentValue.text = formatCurrency.formatCurrency2(_result['installmentBalloonPayment']['lastInstallmentValue'].toString());
          }
        }
        print("cek data last installment = ${_providerCreditStructureTypeInstallment.controllerInstallmentValue.text} ===== api = ${_result['installmentBalloonPayment']['lastInstallmentValue']}");
        //automotive
        if(_providerKolateral.collateralTypeModel.id == "001"){
          //mobil|motor bekas
          if((_providerKolateral.objectSelected.id == "002" || _providerKolateral.objectSelected.id == "004") && _providerKolateral.controllerPHMaxAutomotive.text.isNotEmpty){
            if(_result['totalLoan'] <= double.parse(_providerKolateral.controllerPHMaxAutomotive.text.replaceAll(",", ""))){ //dirubah jadi lebih kecil sama dengan instruksi bu Wienny 22.04.2021
              this._controllerInterestRateEffective.text = _result['effectiveRate'].toString();
              this._controllerInterestRateFlat.text = _result['flatRate'].toString();
              this._controllerGrossDP.text = _result['grossDownPayment'].toString();
              this._controllerObjectPrice.text = _result['objectPrice'].toString();
              this._controllerKaroseriTotalPrice.text = _result['karoseriTotalPrice'].toString();
              this._controllerTotalPrice.text = _result['totalPrice'].toString();
              this._controllerNetDP.text = _result['nettDownPayment'].toString();
              this._controllerBranchDP.text = _result['branchDownPayment'].toString();
              this._controllerTotalLoan.text = _result['totalLoan'].toString();
              this._controllerInstallment.text = _result['installmentAmount'].toString();
              this._controllerLTV.text = _result['ltvRatio'].toString();
              installmentDeclineN = double.parse(_result['declineNInstallment'].toString());
              formatting();
            }
            else{
              print("MASUK SINI");
              print("Jumlah Pinjaman (${formatCurrency.formatCurrency2(_result['totalLoan'].toString())}) melebihi nilai PH Maximum (${_providerKolateral.controllerPHMaxAutomotive.text})");
              showSnackBar("Jumlah Pinjaman (${formatCurrency.formatCurrency2(_result['totalLoan'].toString())}) melebihi nilai PH Maximum (${_providerKolateral.controllerPHMaxAutomotive.text})");
            }
          }
          //mobil|motor baru
          else{
            this._controllerInterestRateEffective.text = _result['effectiveRate'].toString();
            this._controllerInterestRateFlat.text = _result['flatRate'].toString();
            this._controllerGrossDP.text = _result['grossDownPayment'].toString();
            this._controllerObjectPrice.text = _result['objectPrice'].toString();
            this._controllerKaroseriTotalPrice.text = _result['karoseriTotalPrice'].toString();
            this._controllerTotalPrice.text = _result['totalPrice'].toString();
            this._controllerNetDP.text = _result['nettDownPayment'].toString();
            this._controllerBranchDP.text = _result['branchDownPayment'].toString();
            this._controllerTotalLoan.text = _result['totalLoan'].toString();
            this._controllerInstallment.text = _result['installmentAmount'].toString();
            this._controllerLTV.text = _result['ltvRatio'].toString();
            installmentDeclineN = double.parse(_result['declineNInstallment'].toString());
            formatting();
          }
        }
        //prop dan durable
        else{
          this._controllerInterestRateEffective.text = _result['effectiveRate'].toString();
          this._controllerInterestRateFlat.text = _result['flatRate'].toString();
          this._controllerGrossDP.text = _result['grossDownPayment'].toString();
          this._controllerObjectPrice.text = _result['objectPrice'].toString();
          this._controllerKaroseriTotalPrice.text = _result['karoseriTotalPrice'].toString();
          this._controllerTotalPrice.text = _result['totalPrice'].toString();
          this._controllerNetDP.text = _result['nettDownPayment'].toString();
          this._controllerBranchDP.text = _result['branchDownPayment'].toString();
          this._controllerTotalLoan.text = _result['totalLoan'].toString();
          this._controllerInstallment.text = _result['installmentAmount'].toString();
          this._controllerLTV.text = _result['ltvRatio'].toString();
          installmentDeclineN = double.parse(_result['declineNInstallment'].toString());
          formatting();
        }
        loadData = false;
        _providerParentIndividu.insertLog(context, _timeStartValidate, DateTime.now(), _body, _response.body, "Calculate Credit");
      }
      else{
        _providerParentIndividu.insertLog(context, _timeStartValidate, DateTime.now(), _body, _response.body, "Calculate Credit");
        showSnackBar("Error calculate credit ${_response.statusCode}");
        loadData = false;
      }
    } catch (e) {
      showSnackBar(e.toString());
      loadData = false;
    }
    notifyListeners();
  }

  bool hideShowPaymentPerYear() {
    bool _showHide = false;
    if(this._installmentTypeSelected != null) {
      if(this._installmentTypeSelected.id == "04") {
        _showHide = true;
      } else {
        _showHide = false;
      }
    }
    return _showHide;
  }

  bool hideShowEffRate(BuildContext context){
    bool _showHide = true;
    if(this._installmentTypeSelected != null){
      if(this._custType == "PER"){
        if(Provider.of<FormMFotoChangeNotifier>(context,listen: false).typeOfFinancingModelSelected.financingTypeId == "2" || this._installmentTypeSelected.id == "09"){
          // Syariah Straightline
          _showHide = false;
        }
        // else if(this._installmentTypeSelected != null) {
        //   if(this._installmentTypeSelected.id == "04") {
        //     // Konvensional/Syariah Seasonal
        //     _isDisableEffRate = false;
        //   }
        // }
      }
      else{
        if(Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).typeOfFinancingModelSelected.financingTypeId == "2" || this._installmentTypeSelected.id == "09"){
          // Syariah Straightline
          _showHide = false;
        }
        // else if(this._installmentTypeSelected != null) {
        //   if(this._installmentTypeSelected.id == "04") {
        //     // Konvensional/Syariah Seasonal
        //     _isDisableEffRate = false;
        //   }
        // }
      }
    }
    else {
      if(this._custType == "PER"){
        if(Provider.of<FormMFotoChangeNotifier>(context,listen: false).typeOfFinancingModelSelected.financingTypeId == "2"){
          _showHide = false;
        }
        // else if(this._installmentTypeSelected != null) {
        //   if(this._installmentTypeSelected.id == "04") {
        //     // Konvensional/Syariah Seasonal
        //     _isDisableEffRate = false;
        //   }
        // }
      }
      else{
        if(Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).typeOfFinancingModelSelected.financingTypeId == "2"){
          _showHide = false;
        }
        // else if(this._installmentTypeSelected != null) {
        //   if(this._installmentTypeSelected.id == "04") {
        //     // Konvensional/Syariah Seasonal
        //     _isDisableEffRate = false;
        //   }
        // }
      }
    }
    return _showHide;
  }

  bool enableInterestRateEffective(context){ //ga kepake
    bool _enableField = true;
    if(this._custType != "COM"){
      if(Provider.of<FormMFotoChangeNotifier>(context,listen: false).typeOfFinancingModelSelected.financingTypeId == "1"){
        if(this._installmentTypeSelected != null){
          if(this._installmentTypeSelected.id == "04") _enableField = false;
        }
      }
    }
    else{
      if(Provider.of<FormMFotoChangeNotifier>(context,listen: false).typeOfFinancingModelSelected != null){
        if(Provider.of<FormMFotoChangeNotifier>(context,listen: false).typeOfFinancingModelSelected.financingTypeId == "1"){
          _enableField = false;
        }
      }
    }
    return _enableField;
  }

  ShowMandatoryInfoStrukturKreditModel _showMandatoryInfoStrukturKreditIdeModel;
  ShowMandatoryInfoStrukturKreditModel _showMandatoryInfoStrukturKreditIdeCompanyModel;
  void showMandatoryIdeModel(BuildContext context){
    _showMandatoryInfoStrukturKreditIdeModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditIdeModel;
    _showMandatoryInfoStrukturKreditIdeCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditIdeCompanyModel;
  }

  ShowMandatoryInfoStrukturKreditModel _showMandatoryInfoStrukturKreditRegulerSurveyModel;
  ShowMandatoryInfoStrukturKreditModel _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel;
  void showMandatoryRegulerSurveyModel(BuildContext context){
    _showMandatoryInfoStrukturKreditRegulerSurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditRegulerSurveyModel;
    _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel;
  }

  ShowMandatoryInfoStrukturKreditModel _showMandatoryInfoStrukturKreditPacModel;
  ShowMandatoryInfoStrukturKreditModel _showMandatoryInfoStrukturKreditPacCompanyModel;
  void showMandatoryPacModel(BuildContext context){
    _showMandatoryInfoStrukturKreditPacModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditPacModel;
    _showMandatoryInfoStrukturKreditPacCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditPacCompanyModel;
  }

  ShowMandatoryInfoStrukturKreditModel _showMandatoryInfoStrukturKreditResurveyModel;
  ShowMandatoryInfoStrukturKreditModel _showMandatoryInfoStrukturKreditResurveyCompanyModel;
  void showMandatoryResurveyModel(BuildContext context){
    _showMandatoryInfoStrukturKreditResurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditResurveyModel;
    _showMandatoryInfoStrukturKreditResurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditResurveyCompanyModel;
  }

  ShowMandatoryInfoStrukturKreditModel _showMandatoryInfoStrukturKreditDakorModel;
  ShowMandatoryInfoStrukturKreditModel _showMandatoryInfoStrukturKreditDakorCompanyModel;
  void showMandatoryDakorModel(BuildContext context){
    _showMandatoryInfoStrukturKreditDakorModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditDakorModel;
    _showMandatoryInfoStrukturKreditDakorCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditDakorCompanyModel;
  }

  void getDataFromDashboard(BuildContext context) async{
    showMandatoryIdeModel(context);
    showMandatoryRegulerSurveyModel(context);
    showMandatoryPacModel(context);
    showMandatoryResurveyModel(context);
    showMandatoryDakorModel(context);
  }

  // Jenis Angsuran
  bool isInstallmentTypeSelectedVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isInstallmentTypeSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isInstallmentTypeSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isInstallmentTypeSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isInstallmentTypeSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isInstallmentTypeSelectedVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isInstallmentTypeSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isInstallmentTypeSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isInstallmentTypeSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isInstallmentTypeSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isInstallmentTypeSelectedVisible;
      }
    }
    return value;
  }

  // Jangka Waktu
  bool isPeriodOfTimeSelectedVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isPeriodOfTimeSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isPeriodOfTimeSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isPeriodOfTimeSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isPeriodOfTimeSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isPeriodOfTimeSelectedVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isPeriodOfTimeSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isPeriodOfTimeSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isPeriodOfTimeSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isPeriodOfTimeSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isPeriodOfTimeSelectedVisible;
      }
    }
    return value;
  }

  // Jangka Waktu
  bool isPaymentPerYearVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isPaymentPerYearVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isPaymentPerYearVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isPaymentPerYearVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isPaymentPerYearVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isPaymentPerYearVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isPaymentPerYearVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isPaymentPerYearVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isPaymentPerYearVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isPaymentPerYearVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isPaymentPerYearVisible;
      }
    }
    return value;
  }

  // Metode Pembayaran
  bool isPaymentMethodSelectedVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isPaymentMethodSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isPaymentMethodSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isPaymentMethodSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isPaymentMethodSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isPaymentMethodSelectedVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isPaymentMethodSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isPaymentMethodSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isPaymentMethodSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isPaymentMethodSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isPaymentMethodSelectedVisible;
      }
    }
    return value;
  }

  // Suku Bunga Eff
  bool isInterestRateEffectiveVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isInterestRateEffectiveVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isInterestRateEffectiveVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isInterestRateEffectiveVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isInterestRateEffectiveVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isInterestRateEffectiveVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isInterestRateEffectiveVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isInterestRateEffectiveVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isInterestRateEffectiveVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isInterestRateEffectiveVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isInterestRateEffectiveVisible;
      }
    }
    return value;
  }

  // Suku Bunga Flat
  bool isInterestRateFlatVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isInterestRateFlatVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isInterestRateFlatVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isInterestRateFlatVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isInterestRateFlatVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isInterestRateFlatVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isInterestRateFlatVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isInterestRateFlatVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isInterestRateFlatVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isInterestRateFlatVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isInterestRateFlatVisible;
      }
    }
    return value;
  }

  // Harga Objek
  bool isObjectPriceVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isObjectPriceVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isObjectPriceVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isObjectPriceVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isObjectPriceVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isObjectPriceVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isObjectPriceVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isObjectPriceVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isObjectPriceVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isObjectPriceVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isObjectPriceVisible;
      }
    }
    return value;
  }

  // Total Harga Karoseri
  bool isKaroseriTotalPriceVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isKaroseriTotalPriceVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isKaroseriTotalPriceVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isKaroseriTotalPriceVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isKaroseriTotalPriceVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isKaroseriTotalPriceVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isKaroseriTotalPriceVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isKaroseriTotalPriceVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isKaroseriTotalPriceVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isKaroseriTotalPriceVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isKaroseriTotalPriceVisible;
      }
    }
    return value;
  }

  // Total Harga
  bool isTotalPriceVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isTotalPriceVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isTotalPriceVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isTotalPriceVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isTotalPriceVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isTotalPriceVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isTotalPriceVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isTotalPriceVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isTotalPriceVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isTotalPriceVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isTotalPriceVisible;
      }
    }
    return value;
  }

  // Uang Muka (Net)
  bool isNetDPVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isNetDPVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isNetDPVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isNetDPVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isNetDPVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isNetDPVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isNetDPVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isNetDPVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isNetDPVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isNetDPVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isNetDPVisible;
      }
    }
    return value;
  }

  // Uang Muka (Cabang)
  bool isBranchDPVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isBranchDPVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isBranchDPVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isBranchDPVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isBranchDPVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isBranchDPVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isBranchDPVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isBranchDPVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isBranchDPVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isBranchDPVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isBranchDPVisible;
      }
    }
    return value;
  }

  // Uang Muka (Gross)
  bool isGrossDPVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isGrossDPVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isGrossDPVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isGrossDPVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isGrossDPVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isGrossDPVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isGrossDPVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isGrossDPVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isGrossDPVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isGrossDPVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isGrossDPVisible;
      }
    }
    return value;
  }

  // Jumlah Pinjaman
  bool isTotalLoanVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isTotalLoanVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isTotalLoanVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isTotalLoanVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isTotalLoanVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isTotalLoanVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isTotalLoanVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isTotalLoanVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isTotalLoanVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isTotalLoanVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isTotalLoanVisible;
      }
    }
    return value;
  }

  // Angsuran
  bool isInstallmentVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isInstallmentVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isInstallmentVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isInstallmentVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isInstallmentVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isInstallmentVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isInstallmentVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isInstallmentVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isInstallmentVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isInstallmentVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isInstallmentVisible;
      }
    }
    return value;
  }

  // LTV
  bool isLTVVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isLTVVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isLTVVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isLTVVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isLTVVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isLTVVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isLTVVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isLTVVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isLTVVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isLTVVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isLTVVisible;
      }
    }
    return value;
  }

  // Jenis Angsuran
  bool isInstallmentTypeSelectedMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isInstallmentTypeSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isInstallmentTypeSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isInstallmentTypeSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isInstallmentTypeSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isInstallmentTypeSelectedMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isInstallmentTypeSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isInstallmentTypeSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isInstallmentTypeSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isInstallmentTypeSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isInstallmentTypeSelectedMandatory;
      }
    }
    return value;
  }

  // Jangka Waktu
  bool isPeriodOfTimeSelectedMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isPeriodOfTimeSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isPeriodOfTimeSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isPeriodOfTimeSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isPeriodOfTimeSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isPeriodOfTimeSelectedMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isPeriodOfTimeSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isPeriodOfTimeSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isPeriodOfTimeSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isPeriodOfTimeSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isPeriodOfTimeSelectedMandatory;
      }
    }
    return value;
  }

  // Jangka Waktu
  bool isPaymentPerYearMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isPaymentPerYearMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isPaymentPerYearMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isPaymentPerYearMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isPaymentPerYearMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isPaymentPerYearMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isPaymentPerYearMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isPaymentPerYearMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isPaymentPerYearMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isPaymentPerYearMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isPaymentPerYearMandatory;
      }
    }
    return value;
  }

  // Metode Pembayaran
  bool isPaymentMethodSelectedMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isPaymentMethodSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isPaymentMethodSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isPaymentMethodSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isPaymentMethodSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isPaymentMethodSelectedMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isPaymentMethodSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isPaymentMethodSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isPaymentMethodSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isPaymentMethodSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isPaymentMethodSelectedMandatory;
      }
    }
    return value;
  }

  // Suku Bunga Eff
  bool isInterestRateEffectiveMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isInterestRateEffectiveMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isInterestRateEffectiveMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isInterestRateEffectiveMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isInterestRateEffectiveMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isInterestRateEffectiveMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isInterestRateEffectiveMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isInterestRateEffectiveMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isInterestRateEffectiveMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isInterestRateEffectiveMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isInterestRateEffectiveMandatory;
      }
    }
    return value;
  }

  // Suku Bunga Flat
  bool isInterestRateFlatMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isInterestRateFlatMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isInterestRateFlatMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isInterestRateFlatMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isInterestRateFlatMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isInterestRateFlatMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isInterestRateFlatMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isInterestRateFlatMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isInterestRateFlatMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isInterestRateFlatMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isInterestRateFlatMandatory;
      }
    }
    return value;
  }

  // Harga Objek
  bool isObjectPriceMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isObjectPriceMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isObjectPriceMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isObjectPriceMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isObjectPriceMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isObjectPriceMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isObjectPriceMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isObjectPriceMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isObjectPriceMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isObjectPriceMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isObjectPriceMandatory;
      }
    }
    return value;
  }

  // Total Harga Karoseri
  bool isKaroseriTotalPriceMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isKaroseriTotalPriceMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isKaroseriTotalPriceMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isKaroseriTotalPriceMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isKaroseriTotalPriceMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isKaroseriTotalPriceMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isKaroseriTotalPriceMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isKaroseriTotalPriceMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isKaroseriTotalPriceMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isKaroseriTotalPriceMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isKaroseriTotalPriceMandatory;
      }
    }
    return value;
  }

  // Total Harga
  bool isTotalPriceMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isTotalPriceMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isTotalPriceMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isTotalPriceMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isTotalPriceMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isTotalPriceMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isTotalPriceMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isTotalPriceMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isTotalPriceMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isTotalPriceMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isTotalPriceMandatory;
      }
    }
    return value;
  }

  // Uang Muka (Net)
  bool isNetDPMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isNetDPMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isNetDPMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isNetDPMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isNetDPMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isNetDPMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isNetDPMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isNetDPMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isNetDPMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isNetDPMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isNetDPMandatory;
      }
    }
    return value;
  }

  // Uang Muka (Cabang)
  bool isBranchDPMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isBranchDPMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isBranchDPMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isBranchDPMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isBranchDPMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isBranchDPMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isBranchDPMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isBranchDPMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isBranchDPMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isBranchDPMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isBranchDPMandatory;
      }
    }
    return value;
  }

  // Uang Muka (Gross)
  bool isGrossDPMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isGrossDPMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isGrossDPMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isGrossDPMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isGrossDPMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isGrossDPMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isGrossDPMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isGrossDPMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isGrossDPMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isGrossDPMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isGrossDPMandatory;
      }
    }
    return value;
  }

  // Jumlah Pinjaman
  bool isTotalLoanMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isTotalLoanMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isTotalLoanMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isTotalLoanMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isTotalLoanMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isTotalLoanMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isTotalLoanMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isTotalLoanMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isTotalLoanMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isTotalLoanMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isTotalLoanMandatory;
      }
    }
    return value;
  }

  // Angsuran
  bool isInstallmentMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isInstallmentMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isInstallmentMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isInstallmentMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isInstallmentMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isInstallmentMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isInstallmentMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isInstallmentMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isInstallmentMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isInstallmentMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isInstallmentMandatory;
      }
    }
    return value;
  }

  // LTV
  bool isLTVMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeModel.isLTVMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyModel.isLTVMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacModel.isLTVMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyModel.isLTVMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorModel.isLTVMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIdeCompanyModel.isLTVMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditRegulerSurveyCompanyModel.isLTVMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditPacCompanyModel.isLTVMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditResurveyCompanyModel.isLTVMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditDakorCompanyModel.isLTVMandatory;
      }
    }
    return value;
  }

  void checkDataDakor() async{
    var _check = await _dbHelper.selectMS2ApplObject();
    if(_check.isNotEmpty && _lastKnownState == "DKR"){
      _installmentTypeDakor = this._installmentTypeSelected.id != _check[0]['installment_type'] || _check[0]['edit_installment_type'] == "1";
      _paymentPerYearDakor = this._controllerPaymentPerYear.text != _check[0]['payment_per_year'].toString() || _check[0]['edit_payment_per_year'] == "1";
      _periodTimeDakor = this._periodOfTimeSelected != _check[0]['appl_top'].toString() || _check[0]['edit_appl_top'] == "1";
      _paymentMethodDakor = this._paymentMethodSelected.id != _check[0]['payment_method'] || _check[0]['edit_payment_method'] == "1";
      _interestRateEffDakor = double.parse(this._controllerInterestRateEffective.text.replaceAll(",", "")) != _check[0]['eff_rate'] || _check[0]['edit_eff_rate'] == "1";
      _interestRateFlatDakor = double.parse(this._controllerInterestRateFlat.text.replaceAll(",", "")) != _check[0]['flat_rate'] || _check[0]['edit_flat_rate'] == "1";
      _objectPriceDakor = double.parse(this._controllerObjectPrice.text.replaceAll(",", "")) != _check[0]['object_price'] || _check[0]['edit_object_price'] == "1";
      _totalPriceKaroseriDakor = double.parse(this._controllerKaroseriTotalPrice.text.replaceAll(",", "")) != _check[0]['karoseri_price'] || _check[0]['edit_karoseri_price'] == "1";
      _totalPriceDakor = double.parse(this._controllerTotalPrice.text.replaceAll(",", "")) != _check[0]['total_amt'] || _check[0]['edit_total_amt'] == "1";
      _netDPDakor = double.parse(this._controllerNetDP.text.replaceAll(",", "")) != _check[0]['dp_net'] || _check[0]['edit_dp_net'] == "1";
      _branchDPDakor = double.parse(this._controllerBranchDP.text.replaceAll(",", "")) != _check[0]['dp_branch'] || _check[0]['edit_dp_branch'] == "1";
      _grossDPDakor = double.parse(this._controllerGrossDP.text.replaceAll(",", "")) != _check[0]['dp_gross'] || _check[0]['edit_dp_gross'] == "1";
      // _totalLoanDakor = this._controllerTotalLoan.text != _check[0][''] || _check[0][''] == "1";
      _installmentDakor = double.parse(this._controllerInstallment.text.replaceAll(",", "")) != _check[0]['installment_paid'] || _check[0]['edit_installment_paid'] == "1";
      _ltvDakor = double.parse(this._controllerLTV.text.replaceAll(",", "")).toString() != _check[0]['ltv'] || _check[0]['edit_ltv'] == "1";
    }
  }

  bool get ltvDakor => _ltvDakor;

  set ltvDakor(bool value) {
    _ltvDakor = value;
    notifyListeners();
  }

  bool get totalLoanDakor => _totalLoanDakor;

  set totalLoanDakor(bool value) {
    _totalLoanDakor = value;
    notifyListeners();
  }

  bool get grossDPDakor => _grossDPDakor;

  set grossDPDakor(bool value) {
    _grossDPDakor = value;
    notifyListeners();
  }

  bool get branchDPDakor => _branchDPDakor;

  set branchDPDakor(bool value) {
    _branchDPDakor = value;
    notifyListeners();
  }

  bool get netDPDakor => _netDPDakor;

  set netDPDakor(bool value) {
    _netDPDakor = value;
    notifyListeners();
  }

  bool get totalPriceDakor => _totalPriceDakor;

  set totalPriceDakor(bool value) {
    _totalPriceDakor = value;
    notifyListeners();
  }

  bool get totalPriceKaroseriDakor => _totalPriceKaroseriDakor;

  set totalPriceKaroseriDakor(bool value) {
    _totalPriceKaroseriDakor = value;
    notifyListeners();
  }

  bool get objectPriceDakor => _objectPriceDakor;

  set objectPriceDakor(bool value) {
    _objectPriceDakor = value;
    notifyListeners();
  }

  bool get interestRateEffDakor => _interestRateEffDakor;

  set interestRateEffDakor(bool value) {
    _interestRateEffDakor = value;
    notifyListeners();
  }

  bool get paymentMethodDakor => _paymentMethodDakor;

  set paymentMethodDakor(bool value) {
    _paymentMethodDakor = value;
    notifyListeners();
  }

  bool get periodTimeDakor => _periodTimeDakor;

  set periodTimeDakor(bool value) {
    _periodTimeDakor = value;
    notifyListeners();
  }

  bool get installmentTypeDakor => _installmentTypeDakor;

  set installmentTypeDakor(bool value) {
    _installmentTypeDakor = value;
    notifyListeners();
  }

  bool get paymentPerYearDakor => _paymentPerYearDakor;

  set paymentPerYearDakor(bool value) {
    _paymentPerYearDakor = value;
    notifyListeners();
  }

  bool get installmentDakor => _installmentDakor;

  set installmentDakor(bool value) {
    _installmentDakor = value;
    notifyListeners();
  }

  bool get interestRateFlatDakor => _interestRateFlatDakor;

  set interestRateFlatDakor(bool value) {
    _interestRateFlatDakor = value;
    notifyListeners();
  }

  Future<void> installmentTypeSyariah(BuildContext context) async {
    if(_custType == "PER") {
      if(Provider.of<FormMFotoChangeNotifier>(context,listen: false).typeOfFinancingModelSelected != null){
        if(Provider.of<FormMFotoChangeNotifier>(context,listen: false).typeOfFinancingModelSelected.financingTypeId == "2") {
          this._listInstallmentType = InstallmentType().listInstallmentType2;
          // Syariah
          // if(this._listInstallmentType[1].id == "03"){
          //   this._listInstallmentType.removeAt(1);
          // }
        }
        else{
          this._listInstallmentType = InstallmentType().listInstallmentType;
        }
      }
    } else {
      if(Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).typeOfFinancingModelSelected != null){
        if(Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).typeOfFinancingModelSelected.financingTypeId == "2") {
          this._listInstallmentType = InstallmentType().listInstallmentType2;
          // Syariah
          // if(this._listInstallmentType[1].id == "03"){
          //   this._listInstallmentType.removeAt(1);
          // }
        }
        else{
          this._listInstallmentType = InstallmentType().listInstallmentType;
        }
      }
    }
  }

  setDataFromSQLiteFeeCreditStructure() async{
    List _data = await _dbHelper.selectMS2ApplFee();
    print("cek length fee ${_data.length}");
    FeeTypeModel _feeTypeModel;
    if(_data.isNotEmpty){
      _listInfoFeeCreditStructure.clear();
      for(int i=0; i < _data.length; i++){
        _listInfoFeeCreditStructure.add(InfoFeeCreditStructureModel(
          _data[i]['fee_type'] != "" ? FeeTypeModel(_data[i]['fee_type'].toString(), _data[i]['fee_type_desc'].toString()) : _feeTypeModel,
          _data[i]['fee_cash'].toString(),
          _data[i]['fee_credit'].toString(),
          _data[i]['total_fee'].toString(),
          _data[i]['orderFeeID'],
          _data[0]['edit_fee_type'] == "1",
          _data[0]['edit_fee_cash'] == "1",
          _data[0]['edit_fee_credit'] == "1",
          _data[0]['edit_total_fee'] == "1",
        ));
      }
    }
    notifyListeners();
  }

  void deleteListInfoFeeCreditStructure(BuildContext context, int index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                primaryColor: Colors.black,
                primarySwatch: primaryOrange,
                accentColor: myPrimaryColor
            ),
            child: AlertDialog(
              title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Apakah kamu yakin menghapus struktur kredit biaya ini?",),
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    this._listInfoFeeCreditStructure.removeAt(index);
                    notifyListeners();
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                ),
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Tidak'),
                ),
              ],
            ),
          );
        }
    );
  }

  // fungsi calculate credit trigger by button next
  Future<String> calculateCreditNew(BuildContext context) async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    // Form IDE Individu
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
    var _providerInfoNasabah = Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false);

    // Form IDE Company
    var _providerRincian = Provider.of<FormMCompanyRincianChangeNotifier>(context, listen: false);

    // Form App
    var _providerUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    // var _providerCollaOto = Provider.of<InformationCollateralChangeNotifier>(context,listen: false);
    var _providerKolateral = Provider.of<InformationCollateralChangeNotifier>(context, listen: false);
    var _providerSales = Provider.of<InformationSalesmanChangeNotifier>(context,listen: false);
    var _providerKaroseri = Provider.of<InfoObjectKaroseriChangeNotifier>(context,listen: false);
    var _providerCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false);
    var _providerMajorInsurance = Provider.of<InfoMajorInsuranceChangeNotifier>(context,listen: false);
    var _providerAdditionalInsurance = Provider.of<InfoAdditionalInsuranceChangeNotifier>(context,listen: false);
    var _providerCreditIncome = Provider.of<InfoCreditIncomeChangeNotifier>(context,listen: false);
    var _providerWmp = Provider.of<InfoWMPChangeNotifier>(context,listen: false);
    var _providerSubsidy = Provider.of<InfoCreditSubsidyChangeNotifier>(context,listen: false);
    // var _providerSubsidyDetail = Provider.of<AddCreditSubsidyChangeNotifier>(context, listen: false);
    var _providerCreditStructureTypeInstallment = Provider.of<InfoCreditStructureTypeInstallmentChangeNotifier>(context,listen: false);
    var _providerParentIndividu = Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false);

    double _rateEff = _controllerInterestRateEffective.text != "" ? double.parse(this._controllerInterestRateEffective.text.replaceAll(",", "")) : 0;
    double _rateFlat = _controllerInterestRateFlat.text != "" ? double.parse(this._controllerInterestRateFlat.text.replaceAll(",", "")) : 0;
    double _rate;
    double _dp;
    String _flag;

    if(_dpFlag == "0"){
      _dp = this._controllerGrossDP.text != "" ? double.parse(this._controllerGrossDP.text.replaceAll(",", "")) : 0;
    }
    else{
      _dp = this._controllerNetDP.text != "" ? double.parse(this._controllerNetDP.text.replaceAll(",", "")) : 0;
    }

    String _finType;
    if(_preferences.getString("cust_type") == "PER"){
      _finType = _providerFoto.typeOfFinancingModelSelected.financingTypeId;
    }
    else{
      _finType = _providerUnitObject.typeOfFinancingModelSelected.financingTypeId;
    }

    if(_finType == "1"){
      if(this._installmentTypeSelected.id == "04") {
        // Seasonal
        _rate = _rateFlat;
        _flag = "FLAT_RATE";
      } else {
        // Selain Seasonal
        if(_rateEff > 0){
          _rate = _rateEff;
          _flag = "EFF_RATE";
        }
        else{
          _rate = _rateFlat;
          _flag = "FLAT_RATE";
        }
      }
    }
    else{
      _rate = _rateFlat;
      _flag = "FLAT_RATE";
    }

    // Dedup
    // var _listOID = Provider.of<ListOidChangeNotifier>(context,listen: false);

    print("cek data dsr ${_providerCreditIncome.controllerDebtComparison.text}");

    // var _rateValue = double.parse(rate);
    // var _dpValue = double.parse(dp);
    var _collaType = _providerKolateral.collateralTypeModel != null
        ? _providerKolateral.collateralTypeModel.id != "003"
        ? _providerKolateral.collateralTypeModel.name : "TIDAK_ADA" :"";

    var _dateFormat = DateFormat("dd-MM-yyyy HH:mm:ss");
    var _dateTime = _dateFormat.format(DateTime.now());

    var _orderProductSaleses = [];
    if(_providerSales.listInfoSales.isNotEmpty) {
      for(int i=0; i < _providerSales.listInfoSales.length; i++) {
        _orderProductSaleses.add(
            {
              "orderProductSalesID": "NEW",
              "salesType": _providerSales.listInfoSales[i].salesmanTypeModel != null ? _providerSales.listInfoSales[i].salesmanTypeModel.id : "",
              "employeeJob": _providerSales.listInfoSales[i].employeeModel != null ? _providerSales.listInfoSales[i].employeeModel.Nama : "",
              "employeeID": _providerSales.listInfoSales[i].employeeModel != null ? _providerSales.listInfoSales[i].employeeModel.NIK : "",
              "employeeHeadID": _providerSales.listInfoSales[i].employeeHeadModel != null ? _providerSales.listInfoSales[i].employeeHeadModel.NIK : "",
              "referalContractNumber": _providerUnitObject.referenceNumberModel != null ? _providerUnitObject.referenceNumberModel.noPK : "",
              "employeeHeadJob": null,
              "creationalSpecification": {
                "createdAt": _dateTime,
                "createdBy": _preferences.getString("username"),
                "modifiedAt": _dateTime,
                "modifiedBy": _preferences.getString("username"),
              },
              "status": "ACTIVE"
            }
        );
      }
    }

    var _orderKaroseris = [];
    if(_providerKaroseri.listFormKaroseriObject.isNotEmpty) {
      for(int i=0; i < _providerKaroseri.listFormKaroseriObject.length; i++) {
        _orderKaroseris.add(
            {
              "orderKaroseriID": "NEW",
              "isPksKaroseri": _providerKaroseri.listFormKaroseriObject[i].PKSKaroseri != -1 ? _providerKaroseri.listFormKaroseriObject[i].PKSKaroseri == 1 ? true : false : false,
              "karoseriCompanyID": _providerKaroseri.listFormKaroseriObject[i].company != null ? _providerKaroseri.listFormKaroseriObject[i].company.id : "",
              "karoseriID": _providerKaroseri.listFormKaroseriObject[i].karoseri != null ? _providerKaroseri.listFormKaroseriObject[i].karoseri.kode : "",
              "karoseriPrice": _providerKaroseri.listFormKaroseriObject[i].price != null ? double.parse(_providerKaroseri.listFormKaroseriObject[i].price.replaceAll(",", "")) : 0,
              "karoseriQty": _providerKaroseri.listFormKaroseriObject[i].jumlahKaroseri != null ? double.parse(_providerKaroseri.listFormKaroseriObject[i].jumlahKaroseri.replaceAll(",", "")) : 0,
              "karoseriTotalPrice": _providerKaroseri.listFormKaroseriObject[i].totalPrice != null ? double.parse(_providerKaroseri.listFormKaroseriObject[i].totalPrice.replaceAll(",", "")) : 0,
              "uangMukaKaroseri": 0,
              "creationalSpecification": {
                "createdAt": _dateTime,
                "createdBy": _preferences.getString("username"),
                "modifiedAt": _dateTime,
                "modifiedBy": _preferences.getString("username"),
              },
              "status": "ACTIVE"
            }
        );
      }
    }

    var _orderProductInsurances = [];
    if(_providerMajorInsurance.listFormMajorInsurance.isNotEmpty) {
      for(int i=0; i < _providerMajorInsurance.listFormMajorInsurance.length; i++) {
        _orderProductInsurances.add(
            {
              "orderProductInsuranceID": "NEW",
              "insuranceCollateralReferenceSpecification": {
                "collateralID": "NEW",
                "collateralTypeID": ""
              },
              "insuranceTypeID": _providerMajorInsurance.listFormMajorInsurance[i].insuranceType != null ? _providerMajorInsurance.listFormMajorInsurance[i].insuranceType.KODE : "",
              "insuranceCompanyID": _providerMajorInsurance.listFormMajorInsurance[i].company != null ? _providerMajorInsurance.listFormMajorInsurance[i].company.KODE : "",
              "insuranceProductID": _providerMajorInsurance.listFormMajorInsurance[i].product != null ? _providerMajorInsurance.listFormMajorInsurance[i].product.KODE : "",
              "period": _providerMajorInsurance.listFormMajorInsurance[i].periodType != null ? _providerMajorInsurance.listFormMajorInsurance[i].periodType : 0,
              "type": "",
              "type1": "",
              "type2": "",
              "subType": "",
              "subTypeCoverage": "",
              "insuranceSourceTypeID": "",
              "insuranceValue": _providerMajorInsurance.listFormMajorInsurance[i].coverageValue.isNotEmpty ? double.parse(_providerMajorInsurance.listFormMajorInsurance[i].coverageValue.replaceAll(",", "")) : 0,
              "upperLimitPercentage": _providerMajorInsurance.listFormMajorInsurance[i].upperLimitRate.isNotEmpty ? double.parse( _providerMajorInsurance.listFormMajorInsurance[i].upperLimitRate.replaceAll(",", "")) : 0,
              "upperLimitAmount": _providerMajorInsurance.listFormMajorInsurance[i].upperLimit.isNotEmpty ? double.parse(_providerMajorInsurance.listFormMajorInsurance[i].upperLimit.replaceAll(",", "")) : 0,
              "lowerLimitPercentage": _providerMajorInsurance.listFormMajorInsurance[i].lowerLimitRate.isNotEmpty ? double.parse(_providerMajorInsurance.listFormMajorInsurance[i].lowerLimitRate.replaceAll(",", "")) : 0,
              "lowerLimitAmount": _providerMajorInsurance.listFormMajorInsurance[i].lowerLimit.isNotEmpty ? double.parse(_providerMajorInsurance.listFormMajorInsurance[i].lowerLimit.replaceAll(",", "")) : 0,
              "insuranceCashFee": _providerMajorInsurance.listFormMajorInsurance[i].priceCash.isNotEmpty ? double.parse(_providerMajorInsurance.listFormMajorInsurance[i].priceCash.replaceAll(",", "")) : 0,
              "insuranceCreditFee": _providerMajorInsurance.listFormMajorInsurance[i].priceCredit.isNotEmpty ? double.parse(_providerMajorInsurance.listFormMajorInsurance[i].priceCredit.replaceAll(",", "")) : 0,
              "totalSplitFee": _providerMajorInsurance.listFormMajorInsurance[i].totalPriceSplit.isNotEmpty ? double.parse(_providerMajorInsurance.listFormMajorInsurance[i].totalPriceSplit.replaceAll(",", "")) : 0,
              "totalInsuranceFeePercentage": _providerMajorInsurance.listFormMajorInsurance[i].totalPriceRate.isNotEmpty ? double.parse(_providerMajorInsurance.listFormMajorInsurance[i].totalPriceRate.replaceAll(",", "")) : 0,
              "totalInsuranceFeeAmount": _providerMajorInsurance.listFormMajorInsurance[i].totalPrice.isNotEmpty ? double.parse(_providerMajorInsurance.listFormMajorInsurance[i].totalPrice.replaceAll(",", "")) : 0,
              "creationalSpecification": {
                "createdAt": _dateTime,
                "createdBy": _preferences.getString("username"),
                "modifiedAt": _dateTime,
                "modifiedBy": _preferences.getString("username"),
              },
              "status": "ACTIVE",
              "insurancePaymentType": "CASH"
            }
        );
      }
    }

    if(_providerAdditionalInsurance.listFormAdditionalInsurance.isNotEmpty) {
      // debugPrint("CHECK INSURANCE ${_providerAdditionalInsurance.listFormAdditionalInsurance.length}");
      for(int i=0; i<_providerAdditionalInsurance.listFormAdditionalInsurance.length; i++) {
        _orderProductInsurances.add({
          "orderProductInsuranceID": "NEW",
          "insuranceCollateralReferenceSpecification": {
            "collateralID": "NEW",
            "collateralTypeID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].colaType, //_providerKolateral.collateralTypeModel.id
          },
          "insuranceTypeID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].insuranceType.KODE,
          "insuranceCompanyID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].company  == null ? "02" : _providerAdditionalInsurance.listFormAdditionalInsurance[i].company.KODE,
          "insuranceProductID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].product.KODE,
          "period": _providerAdditionalInsurance.listFormAdditionalInsurance[i].periodType == "" ? null : int.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].periodType),
          "type": "",
          "type1": "",
          "type2": "",
          "subType": "",
          "subTypeCoverage": "",
          "insuranceSourceTypeID": _providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageType != null ? _providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageType.KODE : "", // field jenis pertanggungan
          "insuranceValue": _providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageValue.isEmpty ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].coverageValue.replaceAll(",", "")).round(),
          "upperLimitPercentage": _providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimitRate == "" ? 0.0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimitRate),
          "upperLimitAmount": _providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimit == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].upperLimit.replaceAll(",", "")),
          "lowerLimitPercentage": _providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimitRate == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimitRate),
          "lowerLimitAmount": _providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimit == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].lowerLimit.replaceAll(",", "")),
          "insuranceCashFee": _providerAdditionalInsurance.listFormAdditionalInsurance[i].priceCash == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].priceCash.replaceAll(",", "")).round(),
          "insuranceCreditFee": _providerAdditionalInsurance.listFormAdditionalInsurance[i].priceCredit == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].priceCredit.replaceAll(",", "")).round(),
          "totalSplitFee": "",
          "totalInsuranceFeePercentage": _providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPriceRate == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPriceRate),
          "totalInsuranceFeeAmount": _providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPrice == "" ? 0 : double.parse(_providerAdditionalInsurance.listFormAdditionalInsurance[i].totalPrice.replaceAll(",", "")),
          "creationalSpecification": {
            "createdAt": formatDateValidateAndSubmit(DateTime.now()),
            "createdBy": _preferences.getString("username"),
            "modifiedAt": formatDateValidateAndSubmit(DateTime.now()),
            "modifiedBy": _preferences.getString("username"),
          },
          "status": "ACTIVE",
          "insurancePaymentType": null
        });
      }
    }

    var _installmentDetails = [];
    if(_installmentTypeSelected.id == "06") {
      // Stepping
      if(_providerCreditStructureTypeInstallment.listInfoCreditStructureStepping.isNotEmpty) {
        for(int i=0; i < _providerCreditStructureTypeInstallment.listInfoCreditStructureStepping.length; i++) {
          _installmentDetails.add(
              {
                "installmentID": "NEW",
                "installmentNumber": 0,
                "percentage": _providerCreditStructureTypeInstallment.listInfoCreditStructureStepping[i].controllerPercentage.text != "" ? double.parse(_providerCreditStructureTypeInstallment.listInfoCreditStructureStepping[i].controllerPercentage.text.replaceAll(",", "")) : 0,
                "amount": _providerCreditStructureTypeInstallment.listInfoCreditStructureStepping[i].controllerValue.text != "" ? double.parse(_providerCreditStructureTypeInstallment.listInfoCreditStructureStepping[i].controllerValue.text.replaceAll(",", "")) : 0,
                "status": "ACTIVE",
                "creationalSpecification": {
                  "createdAt": _dateTime,
                  "createdBy": _preferences.getString("username"),
                  "modifiedAt": _dateTime,
                  "modifiedBy": _preferences.getString("username"),
                },
              }
          );
        }
      }
    } else if(_installmentTypeSelected.id == "07") {
      // Irreguler
      if(_providerCreditStructureTypeInstallment.listInfoCreditStructureIrregularModel.isNotEmpty) {
        for(int i=0; i < _providerCreditStructureTypeInstallment.listInfoCreditStructureIrregularModel.length; i++) {
          _installmentDetails.add(
              {
                "installmentID": "NEW",
                "installmentNumber": i+1,
                "percentage": 0,
                "amount": _providerCreditStructureTypeInstallment.listInfoCreditStructureIrregularModel[i].controller.text != "" ? double.parse(_providerCreditStructureTypeInstallment.listInfoCreditStructureIrregularModel[i].controller.text.replaceAll(",", "")) : 0,
                "status": "ACTIVE",
                "creationalSpecification": {
                  "createdAt": _dateTime,
                  "createdBy": _preferences.getString("username"),
                  "modifiedAt": _dateTime,
                  "modifiedBy": _preferences.getString("username"),
                },
              }
          );
        }
      }
    }

    var _orderFees = [];
    if(_providerCreditStructure.listInfoFeeCreditStructure.isNotEmpty) {
      for(int i=0; i < _providerCreditStructure.listInfoFeeCreditStructure.length; i++) {
        _orderFees.add(
            {
              "orderFeeID": "NEW",
              "feeTypeID": "NEW",
              "cashFee": _providerCreditStructure.listInfoFeeCreditStructure[i].cashCost != null ? double.parse(_providerCreditStructure.listInfoFeeCreditStructure[i].cashCost.replaceAll(",", "")) : 0,
              "creditFee": _providerCreditStructure.listInfoFeeCreditStructure[i].creditCost != null ? double.parse(_providerCreditStructure.listInfoFeeCreditStructure[i].creditCost.replaceAll(",", "")) : 0,
              "totalFee": _providerCreditStructure.listInfoFeeCreditStructure[i].totalCost == null || _providerCreditStructure.listInfoFeeCreditStructure[i].totalCost == "" ? 0 : double.parse(_providerCreditStructure.listInfoFeeCreditStructure[i].totalCost.replaceAll(",", "")),
              "creationalSpecification": {
                "createdAt": _dateTime,
                "createdBy": _preferences.getString("username"),
                "modifiedAt": _dateTime,
                "modifiedBy": _preferences.getString("username"),
              },
              "status": "ACTIVE"
            }
        );
      }
    }

    var _orderSubsidies = [];
    var _orderSubsidyDetails = [];
    if(_providerSubsidy.listInfoCreditSubsidy.isNotEmpty) {
      for(int i=0; i < _providerSubsidy.listInfoCreditSubsidy.length; i++) {
        print("cek nilai klaim ${_providerSubsidy.listInfoCreditSubsidy[i].claimValue}");
        if(_providerSubsidy.listInfoCreditSubsidy[i].installmentDetail != null){
          for(int j=0; j < _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail.length; j++) {
            _orderSubsidyDetails.add(
                {
                  "orderSubsidyDetailID": "NEW",
                  "installmentNumber": _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentIndex != null ? double.parse(_providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentIndex.replaceAll(",", "")) : 0,
                  "installmentAmount": _providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentSubsidy != null ? double.parse(_providerSubsidy.listInfoCreditSubsidy[i].installmentDetail[j].installmentSubsidy.replaceAll(",", "")) : 0,
                  "creationalSpecification": {
                    "createdAt": _dateTime,
                    "createdBy": _preferences.getString("username"),
                    "modifiedAt": _dateTime,
                    "modifiedBy": _preferences.getString("username"),
                  },
                  "status": "ACTIVE"
                }
            );
          }
        }
        _orderSubsidies.add(
            {
              "orderSubsidyID": "NEW",
              "subsidyProviderID": "1",
              "subsidyTypeID": _providerSubsidy.listInfoCreditSubsidy[i].type != null ? _providerSubsidy.listInfoCreditSubsidy[i].type.id : "",
              "subsidyMethodTypeID": _providerSubsidy.listInfoCreditSubsidy[i].cuttingMethod != null ? _providerSubsidy.listInfoCreditSubsidy[i].cuttingMethod.id : "",
              "refundAmount": _providerSubsidy.listInfoCreditSubsidy[i].claimValue == null || _providerSubsidy.listInfoCreditSubsidy[i].claimValue == "" ? 0 : double.parse(_providerSubsidy.listInfoCreditSubsidy[i].claimValue.replaceAll(",", "")),
              "refundAmountKlaim": 0,
              "effectiveRate": _providerSubsidy.listInfoCreditSubsidy[i].rateBeforeEff == null || _providerSubsidy.listInfoCreditSubsidy[i].rateBeforeEff == "" ? 0 : double.parse(_providerSubsidy.listInfoCreditSubsidy[i].rateBeforeEff.replaceAll(",", "")),
              "flatRate": _providerSubsidy.listInfoCreditSubsidy[i].rateBeforeFlat == null || _providerSubsidy.listInfoCreditSubsidy[i].rateBeforeFlat == "" ? 0 : double.parse(_providerSubsidy.listInfoCreditSubsidy[i].rateBeforeFlat.replaceAll(",", "")),
              "dpReal": 1,
              "totalInstallment": _providerSubsidy.listInfoCreditSubsidy[i].totalInstallment == null || _providerSubsidy.listInfoCreditSubsidy[i].totalInstallment == "" ? 0 : double.parse(_providerSubsidy.listInfoCreditSubsidy[i].totalInstallment.replaceAll(",", "")),
              "interestRate": 0,
              "creationalSpecification": {
                "createdAt": _dateTime,
                "createdBy": _preferences.getString("username"),
                "modifiedAt": _dateTime,
                "modifiedBy": _preferences.getString("username"),
              },
              "status": "ACTIVE",
              "orderSubsidyDetails": _orderSubsidyDetails
            }
        );
      }
    }

    var _orderWmps = [];
    if(_providerWmp.listWMP.isNotEmpty) {
      for(int i=0; i < _providerWmp.listWMP.length; i++) {
        _orderWmps.add(
          {
            "orderWmpID": "NEW",
            "wmpNumber": _providerWmp.listWMP[i].noProposal,
            "wmpType": _providerWmp.listWMP[i].type,
            "wmpJob": "",
            "wmpSubsidyTypeID": _providerWmp.listWMP[i].typeSubsidi,
            "maxRefundAmount": _providerWmp.listWMP[i].amount,
            "status": "ACTIVE",
            "creationalSpecification": {
              "createdAt": _dateTime,
              "createdBy": _preferences.getString("username"),
              "modifiedAt": _dateTime,
              "modifiedBy": _preferences.getString("username"),
            },
          },
        );
      }
    }

    var _body = jsonEncode(
        {
          "orderProductID": "NEW",
          "orderProductSpecification": {
            "financingTypeID": this._custType != "COM" ? "${_providerFoto.typeOfFinancingModelSelected.financingTypeId}" : "${_providerUnitObject.typeOfFinancingModelSelected.financingTypeId}",
            "ojkBusinessTypeID": this._custType == "COM" ? "${_providerUnitObject.businessActivitiesModelSelected.id}" : "${_providerFoto.kegiatanUsahaSelected.id}",
            "ojkBussinessDetailID": this._custType == "COM" ? "${_providerUnitObject.businessActivitiesTypeModelSelected.id}" : "${_providerFoto.jenisKegiatanUsahaSelected.id}",
            "orderUnitSpecification": {
              "objectGroupID": _providerUnitObject.groupObjectSelected != null ? _providerUnitObject.groupObjectSelected.KODE : "",
              "objectID": _providerUnitObject.objectSelected != null ? _providerUnitObject.objectSelected.id : "",
              "productTypeID": _providerUnitObject.productTypeSelected != null ? _providerUnitObject.productTypeSelected.id : "",
              "objectBrandID": _providerUnitObject.brandObjectSelected != null ? _providerUnitObject.brandObjectSelected.id : "",
              "objectTypeID": _providerUnitObject.objectTypeSelected != null ? _providerUnitObject.objectTypeSelected.id : "",
              "objectModelID": _providerUnitObject.modelObjectSelected != null ? _providerUnitObject.modelObjectSelected.id : "",
              "modelDetail": "",
              "objectUsageID": _providerUnitObject.objectUsageModel != null ? _providerUnitObject.objectUsageModel.id : "",
              "objectPurposeID": _providerUnitObject.objectPurposeSelected != null ? _providerUnitObject.objectPurposeSelected.id : ""
            },
            "salesGroupID": "00003",
            "groupID": "",
            "orderSourceID": "1",
            "orderSourceName": null,
            "orderProductDealerSpecification": {
              "isThirdParty": true,
              "thirdPartyTypeID": _providerUnitObject.thirdPartyTypeSelected != null ? _providerUnitObject.thirdPartyTypeSelected.kode : "",
              "thirdPartyID": _providerUnitObject.thirdPartySelected != null ? _providerUnitObject.thirdPartySelected.kode : "",
              "thirdPartyActivity": "1",
              "sentradID": _preferences.getString("SentraD"),
              "unitdID": _preferences.getString("UnitD"),
              "dealerMatrix": _providerUnitObject.matriksDealerSelected != null ? _providerUnitObject.matriksDealerSelected.kode : ""
            },
            "outletID": "",
            "programID": _providerUnitObject.programSelected != null ? _providerUnitObject.programSelected.kode : "",
            "rehabType": _providerUnitObject.rehabTypeSelected != null ? _providerUnitObject.rehabTypeSelected.id : "",
            "referenceNumber": _providerUnitObject.referenceNumberModel != null ? _providerUnitObject.referenceNumberModel.noPK : "",
            "applicationUnitContractNumber": null,
            "orderProductSaleses": _orderProductSaleses,
            "orderKaroseris": _orderKaroseris,
            "orderProductInsurances": _orderProductInsurances,
            "orderWmps": _orderWmps
          },
          "orderCreditStructure": {
            "installmentTypeID": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.id : "",
            "tenor": _providerCreditStructure.periodOfTimeSelected != null ? int.parse(_providerCreditStructure.periodOfTimeSelected) : 0,
            "paymentMethodID": _providerCreditStructure.paymentMethodSelected != null ? _providerCreditStructure.paymentMethodSelected.id : "",
            "effectiveRate": _providerCreditStructure.controllerInterestRateEffective.text != "" ? double.parse(_providerCreditStructure.controllerInterestRateEffective.text.replaceAll(",", "")) : 0,
            "flatRate": _providerCreditStructure.controllerInterestRateFlat.text != "" ? double.parse(_providerCreditStructure.controllerInterestRateFlat.text.replaceAll(",", "")) : 0,
            "objectPrice": _providerCreditStructure.controllerObjectPrice.text != "" ? double.parse(_providerCreditStructure.controllerObjectPrice.text.replaceAll(",", "")) : 0,
            "karoseriTotalPrice": _providerCreditStructure.controllerKaroseriTotalPrice.text != "" ? double.parse(_providerCreditStructure.controllerKaroseriTotalPrice.text.replaceAll(",", "")) : 0,
            "totalPrice": _providerCreditStructure.controllerTotalPrice.text != "" ? double.parse(_providerCreditStructure.controllerTotalPrice.text.replaceAll(",", "")) : 0,
            "insentifSales": 0,
            "insentifPayment": 0,
            "nettDownPayment": _providerCreditStructure.controllerNetDP.text != "" ? double.parse(_providerCreditStructure.controllerNetDP.text.replaceAll(",", "")) : 0,
            "declineNInstallment": 0,
            "paymentOfYear": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.id == "04" ? int.parse(_providerCreditStructure.controllerPaymentPerYear.text) : 1 : 1,
            "branchDownPayment": _providerCreditStructure.controllerBranchDP.text != "" ? double.parse(_providerCreditStructure.controllerBranchDP.text.replaceAll(",", "")) : 0,
            "grossDownPayment": _providerCreditStructure.controllerGrossDP.text != "" ? double.parse(_providerCreditStructure.controllerGrossDP.text.replaceAll(",", "")) : 0,
            "totalLoan": _providerCreditStructure.controllerTotalLoan.text != "" ? double.parse(_providerCreditStructure.controllerTotalLoan.text.replaceAll(",", "")) : 0,
            "installmentAmount": _providerCreditStructure.controllerInstallment.text != "" ? double.parse(_providerCreditStructure.controllerInstallment.text.replaceAll(",", "")) : 0,
            "ltvRatio": _providerCreditStructure.controllerLTV.text != "" ? double.parse(_providerCreditStructure.controllerLTV.text.replaceAll(",", "")) : 0,
            "interestAmount": this._interestAmount,
            "pencairan": 0,
            "realDSR": null,
            "gpType": _providerCreditStructureTypeInstallment.gpTypeSelected != null ? _providerCreditStructureTypeInstallment.gpTypeSelected.id : "",
            "newTenor": _providerCreditStructureTypeInstallment.controllerNewTenor.text != "" ? double.parse(_providerCreditStructureTypeInstallment.controllerNewTenor.text.replaceAll(",", "")) : 0,
            "totalStepping": _providerCreditStructureTypeInstallment.totalSteppingSelected != null ? _providerCreditStructureTypeInstallment.totalSteppingSelected : 0,
            // "uangMukaKaroseri": 0, // diset 0 diganti di dalam
            "uangMukaChasisNet": _providerCreditStructure.controllerNetDP.text != "" ? double.parse(_providerCreditStructure.controllerNetDP.text.replaceAll(",", "")) : 0, // set sama kaya nettDownPayment
            "uangMukaChasisGross": _providerCreditStructure.controllerGrossDP.text != "" ? double.parse(_providerCreditStructure.controllerGrossDP.text.replaceAll(",", "")) : 0, // set sama kaya grossDownPayment
            "installmentBalloonPayment": {
              "balloonType": _providerCreditStructureTypeInstallment.radioValueBalloonPHOrBalloonInstallment == 1 ? true : false, // _providerCreditStructureTypeInstallment.radioValueBalloonPHOrBalloonInstallment != -1 ? _providerCreditStructureTypeInstallment.radioValueBalloonPHOrBalloonInstallment == 1 ? true : false : false,
              "lastInstallmentPercentage": _providerCreditStructureTypeInstallment.controllerInstallmentPercentage.text != "" ? double.parse(_providerCreditStructureTypeInstallment.controllerInstallmentPercentage.text.replaceAll(",", "")) : 0, // _providerCreditStructureTypeInstallment.radioValueBalloonPHOrBalloonInstallment == 1 ? _providerCreditStructureTypeInstallment.controllerInstallmentPercentage.text != "" ? double.parse(_providerCreditStructureTypeInstallment.controllerInstallmentPercentage.text.replaceAll(",", "")) : 0 : 0,
              "lastInstallmentValue": _providerCreditStructureTypeInstallment.controllerInstallmentValue.text != "" ? double.parse(_providerCreditStructureTypeInstallment.controllerInstallmentValue.text.replaceAll(",", "")) : 0
            },
            "installmentDetails": _installmentDetails,
            "installmentSchedule": {
              "installmentType": _providerCreditStructure.installmentTypeSelected != null ? _providerCreditStructure.installmentTypeSelected.name : "",
              "paymentMethodType": _providerCreditStructure.paymentMethodSelected != null ? _providerCreditStructure.paymentMethodSelected.name : "",
              "roundingValue": null,
              "installment": 0,
              "installmentRound": 0,
              "installmentMethod": 0,
              "lastKnownOutstanding": 0,
              "minInterest": 0,
              "maxInterest": 0,
              "futureValue": 0,
              "isInstallment": false,
              "balloonInstallment": 0,
              "gracePeriode": 0,
              "gracePeriodes": [],
              "seasonal": 1,
              "steppings": [],
              "installmentScheduleDetails": []
            },
            "firstInstallment": null,
            "dsr": _providerCreditIncome.controllerDebtComparison.text == "" ||  _providerCreditIncome.controllerDebtComparison.text == "null" ? 0 : double.parse(_providerCreditIncome.controllerDebtComparison.text.replaceAll(",", "")),
            "dir": _providerCreditIncome.controllerIncomeComparison.text == "" || _providerCreditIncome.controllerIncomeComparison.text == "null" ? 0 : double.parse(_providerCreditIncome.controllerIncomeComparison.text.replaceAll(",", "")),
            "dsc": _providerCreditIncome.controllerDSC.text == "" || _providerCreditIncome.controllerDSC.text == "null" ? 0 : double.parse(_providerCreditIncome.controllerDSC.text.replaceAll(",", "")),
            "irr": _providerCreditIncome.controllerIRR.text == "" || _providerCreditIncome.controllerIRR.text == "null"? 0 : double.parse(_providerCreditIncome.controllerIRR.text.replaceAll(",", ""))
          },
          "orderFees": _orderFees,
          "orderSubsidies": _orderSubsidies,
          "isWithoutColla": false,
          "applSendFlag": null,
          "pelunsanaAmount": null,
          "collateralAutomotives": _providerKolateral.collateralTypeModel.id == "001" ?
          [
            {
              "collateralID": "NEW",
              "isCollaSameUnit": _providerKolateral.radioValueIsCollateralSameWithUnitOto == 0 ? true : false,
              "isMultiUnitCollateral": true,
              "isCollaNameSameWithCustomerName": _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0 ? true : false,
              "identitySpecification": {
                "identityType": _preferences.getString("cust_type") == "PER"
                    ? _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? _providerInfoNasabah.identitasModel != null
                    ? _providerInfoNasabah.identitasModel.id : ""
                    : _providerKolateral.identityTypeSelectedAuto != null
                    ? _providerKolateral.identityTypeSelectedAuto.id : ""
                    : _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? ""
                    : _providerKolateral.identityTypeSelectedAuto != null
                    ? _providerKolateral.identityTypeSelectedAuto.id : "",
                "identityNumber": _preferences.getString("cust_type") == "PER"
                    ? _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? _providerInfoNasabah.controllerNoIdentitas.text != ""
                    ? _providerInfoNasabah.controllerNoIdentitas.text : ""
                    : _providerKolateral.controllerIdentityNumberAuto.text != ""
                    ? _providerKolateral.controllerIdentityNumberAuto.text : ""
                    : _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? _providerRincian.controllerNPWP.text != ""
                    ? _providerRincian.controllerNPWP.text : ""
                    : _providerKolateral.identityTypeSelectedAuto != null
                    ? _providerKolateral.identityTypeSelectedAuto.id : "",
                "identityName": _preferences.getString("cust_type") == "PER"
                    ? _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? _providerInfoNasabah.controllerNamaLengkapSesuaiIdentitas.text != ""
                    ? _providerInfoNasabah.controllerNamaLengkapSesuaiIdentitas.text : ""
                    : _providerKolateral.controllerNameOnCollateralAuto.text != ""
                    ? _providerKolateral.controllerNameOnCollateralAuto.text : ""
                    : _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? _providerRincian.controllerFullNameNPWP.text != ""
                    ? _providerRincian.controllerFullNameNPWP.text : ""
                    : _providerKolateral.controllerNameOnCollateralAuto.text != ""
                    ? _providerKolateral.controllerNameOnCollateralAuto.text : "",
                "fullName": _preferences.getString("cust_type") == "PER"
                    ? _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? _providerInfoNasabah.controllerNamaLengkap.text != ""
                    ? _providerInfoNasabah.controllerNamaLengkap.text : ""
                    : _providerKolateral.controllerNameOnCollateralAuto.text != ""
                    ? _providerKolateral.controllerNameOnCollateralAuto.text : ""
                    : _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? _providerRincian.controllerFullNameNPWP.text != ""
                    ? _providerRincian.controllerFullNameNPWP.text : ""
                    : _providerKolateral.controllerNameOnCollateralAuto.text != ""
                    ? _providerKolateral.controllerNameOnCollateralAuto.text : "",
                "alias": null,
                "title": null,
                "dateOfBirth": _providerKolateral.controllerBirthDateAuto.text != "" ? "${_providerKolateral.controllerBirthDateAuto.text} 00:00:00" : "01-01-1990 00:00:00",
                // _preferences.getString("cust_type") == "PER"
                //     ? _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 0
                //     ? _providerInfoNasabah.controllerTglLahir.text != ""
                //     ? "${_providerInfoNasabah.controllerTglLahir.text} 00:00:00": ""
                //     : _providerKolateral.initialDateForBirthDateAuto.millisecondsSinceEpoch.toString() != ""
                //     ? _providerKolateral.initialDateForBirthDateAuto.millisecondsSinceEpoch.toString() : ""
                //     : null,
                "placeOfBirth": _preferences.getString("cust_type") == "PER"
                    ? _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? _providerInfoNasabah.controllerTempatLahirSesuaiIdentitas.text != ""
                    ? _providerInfoNasabah.controllerTempatLahirSesuaiIdentitas.text : ""
                    : _providerKolateral.controllerBirthPlaceValidWithIdentityAuto.text != ""
                    ? _providerKolateral.controllerBirthPlaceValidWithIdentityAuto.text : ""
                    : null,
                "placeOfBirthKabKota": _preferences.getString("cust_type") == "PER"
                    ? _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? _providerInfoNasabah.birthPlaceSelected != null
                    ? _providerInfoNasabah.birthPlaceSelected.KABKOT_ID : ""
                    : _providerKolateral.birthPlaceAutoSelected != null
                    ? _providerKolateral.birthPlaceAutoSelected.KABKOT_ID : ""
                    : null,
                "gender": _preferences.getString("cust_type") == "PER"
                    ? _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? _providerInfoNasabah.radioValueGender == "01"
                    ? "Laki-Laki" : "Perempuan" : null
                    : null,
                "identityActiveStart": null,
                "identityActiveEnd": null,
                "isLifetime": false,
                "religion": _preferences.getString("cust_type") == "PER" ? _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1 ? _providerInfoNasabah.religionSelected != null ? _providerInfoNasabah.religionSelected.id : null : null : null,
                "occupationID": _preferences.getString("cust_type") == "PER" ? _providerFoto.occupationSelected != null ? _providerFoto.occupationSelected.KODE : "" : null,
                "positionID": null,
                "maritalStatusID": _preferences.getString("cust_type") == "PER" ? _providerInfoNasabah.maritalStatusSelected != null ? _providerInfoNasabah.maritalStatusSelected.id : "" : null
              },
              "collateralAutomotiveSpecification": {
                "orderUnitSpecification": {
                  "objectGroupID": _providerKolateral.groupObjectSelected != null ? _providerKolateral.groupObjectSelected.KODE : "",
                  "objectID": _providerKolateral.objectSelected != null ? _providerKolateral.objectSelected.id : "",
                  "productTypeID": _providerKolateral.productTypeSelected != null ? _providerKolateral.productTypeSelected.id : "",
                  "objectBrandID": _providerKolateral.brandObjectSelected != null ? _providerKolateral.brandObjectSelected.id : "",
                  "objectTypeID": _providerKolateral.objectTypeSelected != null ? _providerKolateral.objectTypeSelected.id : "",
                  "objectModelID": _providerKolateral.modelObjectSelected != null ? _providerKolateral.modelObjectSelected.id : "",
                  "modelDetail": "",
                  "objectUsageID": _providerKolateral.objectUsageSelected != null ? _providerKolateral.objectUsageSelected.id : "",
                  "objectPurposeID": ""
                },
                "productionYear": _providerKolateral.yearProductionSelected != null ? int.parse(_providerKolateral.yearProductionSelected) : "",
                "registrationYear": _providerKolateral.yearRegistrationSelected != null ? int.parse(_providerKolateral.yearRegistrationSelected) : "",
                "isYellowPlate": _providerKolateral.radioValueYellowPlat == 0 ? true : false,
                "isBuiltUp": _providerKolateral.radioValueBuiltUpNonATPM == 0 ? true : false,
                "utjSpecification": {
                  "bpkbNumber": _providerKolateral.controllerBPKPNumber.text != "" ? _providerKolateral.controllerBPKPNumber.text : "",
                  "chassisNumber": _providerKolateral.controllerFrameNumber.text != "" ? _providerKolateral.controllerFrameNumber.text : "",
                  "engineNumber": _providerKolateral.controllerMachineNumber.text != "" ? _providerKolateral.controllerMachineNumber.text : "",
                  "policeNumber": _providerKolateral.controllerPoliceNumber.text != "" ? _providerKolateral.controllerPoliceNumber.text : ""
                },
                "gradeUnit": _providerKolateral.controllerGradeUnit.text != "" ? _providerKolateral.controllerGradeUnit.text : "",
                "utjFacility": _providerKolateral.controllerFasilitasUTJ.text != "" ? _providerKolateral.controllerFasilitasUTJ.text : "",
                "bidderName": _providerKolateral.controllerNamaBidder.text != "" ? _providerKolateral.controllerNamaBidder.text : "",
                "showroomPrice": _providerKolateral.controllerHargaJualShowroom.text != "" ? double.parse(_providerKolateral.controllerHargaJualShowroom.text.replaceAll(",", "")) : 0,
                "additionalEquipment": _providerKolateral.controllerPerlengkapanTambahan.text != "" ? _providerKolateral.controllerPerlengkapanTambahan.text : "",
                "mpAdira": _providerKolateral.controllerMPAdira.text != "" ? _providerKolateral.controllerMPAdira.text.replaceAll(",", "") : 0,
                "physicalRecondition": _providerKolateral.controllerRekondisiFisik.text != "" ? double.parse(_providerKolateral.controllerRekondisiFisik.text.replaceAll(",", "")) : 0,
                "isProper": _providerKolateral.radioValueWorthyOrUnworthy == 0 ? false : true,
                "ltvRatio": _providerKolateral.controllerLTV.text != "" ? double.parse(_providerKolateral.controllerLTV.text.replaceAll(",", "")) : 0,
                "appraisalSpecification": {
                  "collateralDp": _providerKolateral.controllerDPJaminan.text != "" ? double.parse(_providerKolateral.controllerDPJaminan.text.replaceAll(",", "")) : 0,
                  "maximumPh": _providerKolateral.controllerPHMax.text != "" ? double.parse(_providerKolateral.controllerPHMax.text.replaceAll(",", "")) : 0,
                  "appraisalPrice": _providerKolateral.controllerTaksasiPriceAutomotive.text != "" ? double.parse(_providerKolateral.controllerTaksasiPriceAutomotive.text.replaceAll(",", "")) : 0,
                  "collateralUtilizationID": ""
                },
                "mpAdiraUpld": 0,
                "collateralAutomotiveAdditionalSpecification": {
                  "capacity": 0,
                  "color": null,
                  "manufacturer": null,
                  "serialNumber": null,
                  "invoiceNumber": null,
                  "stnkActiveDate": null,
                  "bpkbAddress": null,
                  "bpkbIdentityTypeID": null
                },
              },
              "status": "ACTIVE",
              "creationalSpecification": {
                "createdAt": _dateTime,
                "createdBy": _preferences.getString("username"),
                "modifiedAt": _dateTime,
                "modifiedBy": _preferences.getString("username"),
              }
            }
          ]
              :
          [],
          "collateralProperties": _providerKolateral.collateralTypeModel.id == "002" ?
          [
            {
              "isMultiUnitCollateral": true,
              "isCollaNameSameWithCustomerName": _providerKolateral.radioValueIsCollateralSameWithApplicantProperty == 0,
              "collateralID": "NEW",
              "identitySpecification": {
                "identityType": _preferences.getString("cust_type") == "PER"
                    ? _providerKolateral.radioValueIsCollateralSameWithApplicantProperty == 1
                    ? _providerInfoNasabah.identitasModel != null
                    ? _providerInfoNasabah.identitasModel.id : ""
                    : _providerKolateral.identityModel != null
                    ? _providerKolateral.identityModel.id : ""
                    : _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? ""
                    : _providerKolateral.identityTypeSelectedAuto != null
                    ? _providerKolateral.identityTypeSelectedAuto.id : "",
                "identityNumber": _preferences.getString("cust_type") == "PER"
                    ? _providerKolateral.radioValueIsCollateralSameWithApplicantProperty == 1
                    ? _providerInfoNasabah.controllerNoIdentitas.text != ""
                    ? _providerInfoNasabah.controllerNoIdentitas.text : ""
                    : _providerKolateral.controllerIdentityNumber.text != ""
                    ? _providerKolateral.controllerIdentityNumber.text : ""
                    : _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? _providerRincian.controllerNPWP.text != ""
                    ? _providerRincian.controllerNPWP.text : ""
                    : _providerKolateral.identityTypeSelectedAuto != null
                    ? _providerKolateral.identityTypeSelectedAuto.id : "",
                "identityName": _preferences.getString("cust_type") == "PER"
                    ? _providerKolateral.radioValueIsCollateralSameWithApplicantProperty == 1
                    ? _providerInfoNasabah.controllerNamaLengkapSesuaiIdentitas.text != ""
                    ? _providerInfoNasabah.controllerNamaLengkapSesuaiIdentitas.text : ""
                    : _providerKolateral.controllerNameOnCollateral.text != ""
                    ? _providerKolateral.controllerNameOnCollateral.text : ""
                    : _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? _providerRincian.controllerFullNameNPWP.text != ""
                    ? _providerRincian.controllerFullNameNPWP.text : ""
                    : _providerKolateral.controllerNameOnCollateralAuto.text != ""
                    ? _providerKolateral.controllerNameOnCollateralAuto.text : "",
                "fullName": _preferences.getString("cust_type") == "PER"
                    ? _providerKolateral.radioValueIsCollateralSameWithApplicantProperty == 1
                    ? _providerInfoNasabah.controllerNamaLengkap.text != ""
                    ? _providerInfoNasabah.controllerNamaLengkap.text : ""
                    : _providerKolateral.controllerNameOnCollateral.text != ""
                    ? _providerKolateral.controllerNameOnCollateral.text : ""
                    : _providerKolateral.radioValueIsCollaNameSameWithApplicantOto == 1
                    ? _providerRincian.controllerFullNameNPWP.text != ""
                    ? _providerRincian.controllerFullNameNPWP.text : ""
                    : _providerKolateral.controllerNameOnCollateralAuto.text != ""
                    ? _providerKolateral.controllerNameOnCollateralAuto.text : "",
                "alias": null,
                "title": null,
                "dateOfBirth": _providerKolateral.controllerBirthDateProp.text != "" ? "${_providerKolateral.controllerBirthDateProp.text} 00:00:00" : "01-01-1990 00:00:00",
                // _preferences.getString("cust_type") == "PER"
                //     ? _providerKolateral.initialDateForBirthDateAuto != null
                //     ? _providerKolateral.initialDateForBirthDateAuto.millisecondsSinceEpoch.toString() : ""
                //     : null,
                "placeOfBirth": _providerKolateral.controllerBirthPlaceValidWithIdentity1.text,
                // _preferences.getString("cust_type") == "PER"
                //     ? _providerKolateral.controllerBirthPlaceValidWithIdentityAuto.text != ""
                //     ? _providerKolateral.controllerBirthPlaceValidWithIdentityAuto.text : ""
                //     : null,
                "placeOfBirthKabKota": _providerKolateral.birthPlaceSelected.KABKOT_ID,
                // _preferences.getString("cust_type") == "PER"
                //     ? _providerKolateral.controllerBirthPlaceValidWithIdentityLOVAuto.text != ""
                //     ? _providerKolateral.controllerBirthPlaceValidWithIdentityLOVAuto.text : ""
                //     : null,
                "gender": _preferences.getString("cust_type") == "PER"
                    ? _providerKolateral.radioValueIsCollateralSameWithApplicantProperty == 1
                    ? _providerInfoNasabah.radioValueGender == "01"
                    ? "Laki-Laki" : "Perempuan"
                    : null
                    : null,
                "identityActiveStart": null,
                "identityActiveEnd": null,
                "isLifetime": false,
                "religion": _preferences.getString("cust_type") == "PER"
                    ? _providerKolateral.radioValueIsCollateralSameWithApplicantProperty == 1
                    ? _providerInfoNasabah.religionSelected != null
                    ? _providerInfoNasabah.religionSelected.id : null
                    : null
                    : null,
                "occupationID": _preferences.getString("cust_type") == "PER"
                    ? _providerFoto.occupationSelected != null
                    ? _providerFoto.occupationSelected != null ? _providerFoto.occupationSelected.KODE : ""
                    : null
                    : null,
                "positionID": null,
                "maritalStatusID": null
              },
              "detailAddresses": [
                {
                  "addressID": "NEW",
                  "foreignBusinessID": "NEW",
                  "addressSpecification": {
                    "koresponden": "1",
                    "matrixAddr": "1",
                    "address": _providerKolateral.controllerAddress.text != "" ? _providerKolateral.controllerAddress.text : "",
                    "rt": _providerKolateral.controllerRT.text != "" ? _providerKolateral.controllerRT.text : "",
                    "rw": _providerKolateral.controllerRW.text != "" ? _providerKolateral.controllerRW.text : "",
                    "provinsiID": _providerKolateral.kelurahanSelected != null ? _providerKolateral.kelurahanSelected.PROV_ID : "",
                    "kabkotID": _providerKolateral.kelurahanSelected != null ? _providerKolateral.kelurahanSelected.KABKOT_ID : "",
                    "kecamatanID": _providerKolateral.kelurahanSelected != null ? _providerKolateral.kelurahanSelected.KEC_ID : "",
                    "kelurahanID": _providerKolateral.kelurahanSelected != null ? _providerKolateral.kelurahanSelected.KEL_ID : "",
                    "zipcode": _providerKolateral.kelurahanSelected != null ? _providerKolateral.kelurahanSelected.ZIPCODE : "",
                  },
                  "contactSpecification": {
                    "telephoneArea1": "",
                    "telephone1": "",
                    "telephoneArea2": "",
                    "telephone2": "",
                    "faxArea": "",
                    "fax": "",
                    "handphone": "",
                    "email": "",
                    "noWa": null,
                    "noWa2": null,
                    "noWa3": null
                  },
                  "addressType": _providerKolateral.addressTypeSelected != null ? _providerKolateral.addressTypeSelected.KODE : "",
                  "creationalSpecification": {
                    "createdAt": _dateTime,
                    "createdBy": _preferences.getString("username"),
                    "modifiedAt": _dateTime,
                    "modifiedBy": _preferences.getString("username"),
                  },
                  "status": "ACTIVE"
                }
              ],
              "collateralPropertySpecification": {
                "certificateNumber": _providerKolateral.controllerCertificateNumber.text != "" ? _providerKolateral.controllerCertificateNumber.text : "",
                "certificateTypeID": _providerKolateral.certificateTypeSelected != null ? _providerKolateral.certificateTypeSelected.id : "",
                "propertyTypeID": _providerKolateral.propertyTypeSelected != null ? _providerKolateral.propertyTypeSelected.id : "",
                "buildingArea": _providerKolateral.controllerBuildingArea.text != "" ? double.parse(_providerKolateral.controllerBuildingArea.text.replaceAll(",", "")) : 0,
                "landArea": _providerKolateral.controllerSurfaceArea.text != "" ? double.parse(_providerKolateral.controllerSurfaceArea.text.replaceAll(",", "")) : 0,
                "appraisalSpecification": {
                  "collateralDp": 1,
                  "maximumPh": _providerKolateral.controllerPHMax.text != "" ? double.parse(_providerKolateral.controllerPHMax.text.replaceAll(",", "")) : 0,
                  "appraisalPrice": 0,
                  "collateralUtilizationID": _providerKolateral.collateralUsagePropertySelected != null ? _providerKolateral.collateralUsagePropertySelected.id : "" //"" field tujuan penggunaan collateral
                },
                "positiveFasumDistance": _providerKolateral.controllerJarakFasumPositif.text != "" ? double.parse(_providerKolateral.controllerJarakFasumPositif.text.replaceAll(",", "")) : 0,
                "negativeFasumDistance": _providerKolateral.controllerJarakFasumNegatif.text != "" ? double.parse(_providerKolateral.controllerJarakFasumNegatif.text.replaceAll(",", "")) : 0,
                "landPrice": _providerKolateral.controllerHargaTanah.text != "" ? double.parse(_providerKolateral.controllerHargaTanah.text.replaceAll(",", "")) : 0,
                "njopPrice": _providerKolateral.controllerHargaNJOP.text != "" ? double.parse(_providerKolateral.controllerHargaNJOP.text.replaceAll(",", "")) : 0,
                "buildingPrice": _providerKolateral.controllerHargaBangunan.text != "" ? double.parse(_providerKolateral.controllerHargaBangunan.text.replaceAll(",", "")) : 0,
                "roofTypeID": _providerKolateral.typeOfRoofSelected != null ? _providerKolateral.typeOfRoofSelected.id : "",
                "wallTypeID": _providerKolateral.wallTypeSelected != null ? _providerKolateral.wallTypeSelected.id : "",
                "floorTypeID": _providerKolateral.floorTypeSelected != null ? _providerKolateral.floorTypeSelected.id : "",
                "foundationTypeID": _providerKolateral.foundationTypeSelected != null ? _providerKolateral.foundationTypeSelected.id : "",
                "roadTypeID": _providerKolateral.streetTypeSelected != null ? _providerKolateral.streetTypeSelected.id : "",
                "isCarPassable": _providerKolateral.radioValueAccessCar != -1 ? _providerKolateral.radioValueAccessCar == 0 ? true : false : false,
                "totalOfHouseInRadius": _providerKolateral.controllerJumlahRumahDalamRadius.text != "" ? double.parse(_providerKolateral.controllerJumlahRumahDalamRadius.text.replaceAll(",", "")).toInt() : 0,
                "sifatJaminan": _providerKolateral.controllerSifatJaminan.text != "" ? _providerKolateral.controllerSifatJaminan.text : "",
                "buktiKepemilikanTanah": _providerKolateral.controllerBuktiKepemilikan.text != "" ? _providerKolateral.controllerBuktiKepemilikan.text : "",
                "tgglTerbitSertifikat": _providerKolateral.controllerCertificateReleaseDate.text != "" ? "${_providerKolateral.controllerCertificateReleaseDate.text} 00:00:00" : "01-01-1900 00:00:00",//_providerKolateral.initialDateCertificateRelease != null ? _providerKolateral.initialDateCertificateRelease.millisecondsSinceEpoch.toString() : "",
                "tahunTerbitSertifikat": _providerKolateral.controllerCertificateReleaseYear.text != "" ? double.parse(_providerKolateral.controllerCertificateReleaseYear.text.replaceAll(",", "")) : 0,
                "namaPemegangHak": _providerKolateral.controllerNamaPemegangHak.text != "" ? _providerKolateral.controllerNamaPemegangHak.text : "",
                "noSuratUkurGmbrSituasi": _providerKolateral.controllerNoSuratUkur.text != "" ? _providerKolateral.controllerNoSuratUkur.text : "",
                "tgglSuratUkurGmbrSituasi": _providerKolateral.controllerDateOfMeasuringLetter.text != "" ? "${_providerKolateral.controllerDateOfMeasuringLetter.text} 00:00:00" : "01-01-1900 00:00:00",//_providerKolateral.initialDateOfMeasuringLetter != null ? _providerKolateral.initialDateOfMeasuringLetter.millisecondsSinceEpoch.toString() : "",
                "sertifikatSuratUkurDikeluarkanOleh": _providerKolateral.controllerCertificateOfMeasuringLetter.text != "" ? _providerKolateral.controllerCertificateOfMeasuringLetter.text : "",
                "masaBerlakuHak": _providerKolateral.controllerMasaHakBerlaku.text != "" ? _providerKolateral.controllerMasaHakBerlaku.text : "",
                "noImb": _providerKolateral.controllerNoIMB.text != "" ? _providerKolateral.controllerNoIMB.text : "",
                "tgglImb": _providerKolateral.controllerDateOfIMB.text != "" ? "${_providerKolateral.controllerDateOfIMB.text} 00:00:00" : "01-01-1900 00:00:00",//_providerKolateral.initialDateOfIMB != null ? _providerKolateral.initialDateOfIMB.millisecondsSinceEpoch.toString() : "",
                "luasBangunanImb": _providerKolateral.controllerLuasBangunanIMB.text != "" ? double.parse(_providerKolateral.controllerLuasBangunanIMB.text.replaceAll(",", "")) : 0,
                "ltvRatio": _providerKolateral.controllerLTV.text != "" ? double.parse(_providerKolateral.controllerLTV.text.replaceAll(",", "")) : 0,
                "letakTanah": "",
                "propertyGroupObjtID": "",
                "propertyObjtID": ""
              },
              "status": "ACTIVE",
              "creationalSpecification": {
                "createdAt": _dateTime,
                "createdBy": _preferences.getString("username"),
                "modifiedAt": _dateTime,
                "modifiedBy": _preferences.getString("username"),
              },
            }
          ]
              :
          [],
          "orderProductAdditionalSpecification": {
            "virtualAccountID": null,
            "virtualAccountValue": 0,
            "cashingPurposeID": null,
            "cashingTypeID": null
          },
          "autoDebitSpecification": {
            "isAutoDebit": null,
            "bankID": "",
            "accountNumber": "",
            "accountBehalf": "",
            "accountPurposeTypeID": "",
            "downPaymentSourceID": ""
          },
          "orderProductProcessingCompletion": {
            "lastKnownOrderProductState": "APPLICATION_UNIT",
            "nextKnownOrderProductState": "APPLICATION_KAROSERI"
          },
          "tacSpesification": {
            "tacActual": 0,
            "tacMax": 0
          },
          "isSaveKaroseri": false,
          "creationalSpecification": {
            "createdAt": _dateTime,
            "createdBy": _preferences.getString("username"),
            "modifiedAt": _dateTime,
            "modifiedBy": _preferences.getString("username"),
          },
          "status": "ACTIVE",
          "nik": _preferences.getString("username"),
          "nama": _preferences.getString("fullname"),
          "jabatan": _preferences.getString("job_id"),
        }
    );
    print(_body);

    // try{
    DateTime _timeStartValidate = DateTime.now();
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
    String _calculateStrukturKredit = await storage.read(key: "CalculateStrukturKredit");
    print("${BaseUrl.urlGeneral}$_calculateStrukturKredit"+"rate=$_rate&rateType=$_flag&downPayment=$_dp&collateralType=$_collaType");
    final _response = await _http.post(
      // "http://10.134.177.143:82/ad1ms2_backend/public/api/testSubmit",
      // "http://192.168.57.98:82/ad1ms2_backend/public/api/strukturKredit",
      //     "http://192.168.57.122/orca_api/public/login",
        "${BaseUrl.urlGeneral}$_calculateStrukturKredit"+"rate=$_rate&rateType=$_flag&downPayment=$_dp&collateralType=$_collaType",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );
    _providerParentIndividu.insertLog(context, _timeStartValidate, DateTime.now(), _body, "${BaseUrl.urlGeneral}$_calculateStrukturKredit"+"rate=$_rate&rateType=$_flag&downPayment=$_dp&collateralType=$_collaType", "Start Calculate Credit");
    if(_response.statusCode == 200 || _response.statusCode == 302){
      final _result = jsonDecode(_response.body);
      // interest amount
      this._interestAmount = _result['interestAmount'];
      //baloon payment
      if(this._installmentTypeSelected != null) {
        if(this._installmentTypeSelected.id != "08") {
          _providerCreditStructureTypeInstallment.radioValueBalloonPHOrBalloonInstallment = _result['installmentBalloonPayment']['balloonType'] == "false" ? 0 : 1;
          _providerCreditStructureTypeInstallment.controllerInstallmentPercentage.text = _result['installmentBalloonPayment']['lastInstallmentPercentage'].toString();
          _providerCreditStructureTypeInstallment.controllerInstallmentValue.text = formatCurrency.formatCurrency2(_result['installmentBalloonPayment']['lastInstallmentValue'].toString());
        }
      }
      print("cek data last installment = ${_providerCreditStructureTypeInstallment.controllerInstallmentValue.text} ===== api = ${_result['installmentBalloonPayment']['lastInstallmentValue']}");
      //automotive
      if(_providerKolateral.collateralTypeModel.id == "001"){
        //mobil|motor bekas
        if((_providerKolateral.objectSelected.id == "002" || _providerKolateral.objectSelected.id == "004") && _providerKolateral.controllerPHMaxAutomotive.text.isNotEmpty){
          if(_result['totalLoan'] <= double.parse(_providerKolateral.controllerPHMaxAutomotive.text.replaceAll(",", ""))){ //dirubah jadi lebih kecil sama dengan instruksi bu Wienny 22.04.2021
            this._controllerInterestRateEffective.text = _result['effectiveRate'].toString();
            this._controllerInterestRateFlat.text = _result['flatRate'].toString();
            this._controllerGrossDP.text = _result['grossDownPayment'].toString();
            this._controllerObjectPrice.text = _result['objectPrice'].toString();
            this._controllerKaroseriTotalPrice.text = _result['karoseriTotalPrice'].toString();
            this._controllerTotalPrice.text = _result['totalPrice'].toString();
            this._controllerNetDP.text = _result['nettDownPayment'].toString();
            this._controllerBranchDP.text = _result['branchDownPayment'].toString();
            this._controllerTotalLoan.text = _result['totalLoan'].toString();
            this._controllerInstallment.text = _result['installmentAmount'].toString();
            this._controllerLTV.text = _result['ltvRatio'].toString();
            installmentDeclineN = double.parse(_result['declineNInstallment'].toString());
            formatting();
          }
          else{
            print("MASUK SINI NEW");
            print("Jumlah Pinjaman (${formatCurrency.formatCurrency2(_result['totalLoan'].toString())}) melebihi nilai PH Maximum (${_providerKolateral.controllerPHMaxAutomotive.text})");
            throw "Jumlah Pinjaman (${formatCurrency.formatCurrency2(_result['totalLoan'].toString())}) melebihi nilai PH Maximum (${_providerKolateral.controllerPHMaxAutomotive.text})";
          }
        }
        //mobil|motor baru
        else{
          this._controllerInterestRateEffective.text = _result['effectiveRate'].toString();
          this._controllerInterestRateFlat.text = _result['flatRate'].toString();
          this._controllerGrossDP.text = _result['grossDownPayment'].toString();
          this._controllerObjectPrice.text = _result['objectPrice'].toString();
          this._controllerKaroseriTotalPrice.text = _result['karoseriTotalPrice'].toString();
          this._controllerTotalPrice.text = _result['totalPrice'].toString();
          this._controllerNetDP.text = _result['nettDownPayment'].toString();
          this._controllerBranchDP.text = _result['branchDownPayment'].toString();
          this._controllerTotalLoan.text = _result['totalLoan'].toString();
          this._controllerInstallment.text = _result['installmentAmount'].toString();
          this._controllerLTV.text = _result['ltvRatio'].toString();
          installmentDeclineN = double.parse(_result['declineNInstallment'].toString());
          formatting();
        }
      }
      //prop dan durable
      else{
        this._controllerInterestRateEffective.text = _result['effectiveRate'].toString();
        this._controllerInterestRateFlat.text = _result['flatRate'].toString();
        this._controllerGrossDP.text = _result['grossDownPayment'].toString();
        this._controllerObjectPrice.text = _result['objectPrice'].toString();
        this._controllerKaroseriTotalPrice.text = _result['karoseriTotalPrice'].toString();
        this._controllerTotalPrice.text = _result['totalPrice'].toString();
        this._controllerNetDP.text = _result['nettDownPayment'].toString();
        this._controllerBranchDP.text = _result['branchDownPayment'].toString();
        this._controllerTotalLoan.text = _result['totalLoan'].toString();
        this._controllerInstallment.text = _result['installmentAmount'].toString();
        this._controllerLTV.text = _result['ltvRatio'].toString();
        installmentDeclineN = double.parse(_result['declineNInstallment'].toString());
        formatting();
      }
      loadData = false;
      _providerParentIndividu.insertLog(context, _timeStartValidate, DateTime.now(), _body, _response.body, "Calculate Credit");
      return "SUCCESS";
    }
    else{
      _providerParentIndividu.insertLog(context, _timeStartValidate, DateTime.now(), _body, _response.body, "Calculate Credit");
      throw "Error calculate credit ${_response.statusCode}";
    }
    // } catch (e) {
    //   throw "Error calculate credit ${e.toString}";
    // }
    notifyListeners();
  }


  // fungsi untuk cek jika admin fee sudah di isi
  bool checkIsAdminFeeExist(BuildContext context){
    debugPrint("KREDIT JALAN");
    bool _isAdminFeeExist = false;
    // if(Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false).groupObjectSelected.KODE  != "003"){
    for(int i=0; i < this._listInfoFeeCreditStructure.length; i++){
      if(this._listInfoFeeCreditStructure[i].feeTypeModel.id == "01"){
        _isAdminFeeExist = true;
      }
    }
    // }
    return _isAdminFeeExist;
  }

  bool get isDisablePACIAAOSCONA => _isDisablePACIAAOSCONA;

  set isDisablePACIAAOSCONA(bool value) {
    this._isDisablePACIAAOSCONA = value;
    notifyListeners();
  }

  void checkPaymentValid(String paymentPerYear) {
    if(paymentPerYear != "") {
      int periodOfTime = int.parse(this._periodOfTimeSelected.toString());
      int hasilMod = periodOfTime % int.parse(this._controllerPaymentPerYear.text);
      int hasilModPayment12 = 12 % int.parse(this._controllerPaymentPerYear.text);
      if(hasilMod != 0 && hasilModPayment12 != 0) {
        Future.delayed(Duration(milliseconds: 100), () {
          this._controllerPaymentPerYear.clear();
        });
        showSnackBar("Pembayaran Per Tahun tidak valid mohon isi dengan data yang valid");
      } else {
        return;
      }
    }
  }
}