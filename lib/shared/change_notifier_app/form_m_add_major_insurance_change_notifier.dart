import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/assurance_model.dart';
import 'package:ad1ms2_dev/models/coverage1_model.dart';
import 'package:ad1ms2_dev/models/coverage_type_model.dart';
import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:ad1ms2_dev/models/insurance_type_model.dart';
import 'package:ad1ms2_dev/models/product_model.dart';
import 'package:ad1ms2_dev/models/major_insurance_model.dart';
import 'package:ad1ms2_dev/models/showhide_mandatory_visibility/set_major_insurance_model.dart';
import 'package:ad1ms2_dev/models/sub_type_model.dart';
import 'package:ad1ms2_dev/screens/search_assurance.dart';
import 'package:ad1ms2_dev/screens/search_company_insurance.dart';
import 'package:ad1ms2_dev/screens/search_coverage1.dart';
import 'package:ad1ms2_dev/screens/search_product.dart';
import 'package:ad1ms2_dev/screens/search_sub_type.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_application_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_major_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/dashboard/dashboard_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_individu_change_notifier/form_m_parent_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/format_currency.dart';
import 'package:ad1ms2_dev/shared/regex_input_formatter.dart';
import 'package:ad1ms2_dev/shared/search_assurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_company_insurance_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_coverage1_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_product_change_notifier.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../models/insurance_type_model.dart';
import '../search_sub_type_change_notifier.dart';
import 'info_additional_insurance_change_notifier.dart';
import 'information_collatelar_change_notifier.dart';

class FormMAddMajorInsuranceChangeNotifier with ChangeNotifier{
    GlobalKey<FormState> _key = GlobalKey<FormState>();
    List<MajorInsuranceModel> _listMajorInsurance = [];
    bool _autoValidate = false;
    bool _isListInsuranceTypeNotEmpty = true;
    bool _loadData = false;
    bool _flagDisableNilaiPertanggungan = true;
    GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
    InsuranceTypeModel _insuranceTypeSelected;
    String _numberOfCollateral;
    AssuranceModel _assuranceSelected;
    TextEditingController _controllerAssurance = TextEditingController();
    CompanyTypeInsuranceModel _companySelected;
    TextEditingController _controllerCompany = TextEditingController();
    ProductModel _productSelected;
    TextEditingController _controllerProduct = TextEditingController();
    String _typeSelected;
    TextEditingController _controllerPeriodType = TextEditingController();
    CoverageModel _coverage1Selected;
    TextEditingController _controllerCoverage1 = TextEditingController();
    CoverageModel _coverage2Selected;
    TextEditingController _controllerCoverageType = TextEditingController();
    CoverageTypeModel _coverageTypeSelected;
    TextEditingController _controllerSubType = TextEditingController();
    SubTypeModel _subTypeSelected;
    TextEditingController _controllerCoverage2 = TextEditingController();
    TextEditingController _controllerCoverageValue = TextEditingController();
    TextEditingController _controllerUpperLimit = TextEditingController();
    TextEditingController _controllerLowerLimit = TextEditingController();
    TextEditingController _controllerUpperLimitRate = TextEditingController();
    TextEditingController _controllerLowerLimitRate = TextEditingController();
    TextEditingController _controllerTotalPriceSplit = TextEditingController();
    TextEditingController _controllerPriceCash = TextEditingController();
    TextEditingController _controllerPriceCredit = TextEditingController();
    TextEditingController _controllerTotalPrice = TextEditingController();
    TextEditingController _controllerTotalPriceRate = TextEditingController();
    TextEditingController _controllerType = TextEditingController();
    FormatCurrency _formatCurrency = FormatCurrency();
    RegExInputFormatter _amountValidator = RegExInputFormatter.withRegex('^[0-9]{0,13}(\\.[0-9]{0,2})?\$');
    var storage = FlutterSecureStorage();
    // SetMajorInsuranceModel _setMajorInsuranceModel;
    DbHelper _dbHelper = DbHelper();
    String _custType;
    String _lastKnownState;
    bool _isAsuransiPerluasanType = false;
    String _orderProductInsuranceID;
    String _colaType;
    bool _disableJenisPenawaran = false;
    bool _isEdit = false;
    bool _isDisablePACIAAOSCONA = false;

    bool _insuranceTypeDakor = false;
    bool _installmentNoDakor = false;
    bool _companyDakor = false;
    bool _productDakor = false;
    bool _periodDakor = false;
    bool _typeDakor = false;
    bool _coverage1Dakor = false;
    bool _coverage2Dakor = false;
    bool _subTypeDakor = false;
    bool _coverageTypeDakor = false;
    bool _coverageValueDakor = false;
    bool _upperLimitRateDakor = false;
    bool _upperLimitValueDakor = false;
    bool _lowerLimitRateDakor = false;
    bool _lowerLimitValueDakor = false;
    bool _cashDakor = false;
    bool _creditDakor = false;
    bool _totalPriceSplitDakor = false;
    bool _totalPriceRateDakor = false;
    bool _totalPriceDakor = false;

    List<MajorInsuranceModel> get listFormMajorInsuranceObject => _listMajorInsurance;

    List<InsuranceTypeModel> _listInsuranceType = [
        InsuranceTypeModel(null, "2","Asuransi Utama"),
        InsuranceTypeModel(null, "3","Asuransi Perluasan")
    ];

    bool get disableJenisPenawaran => _disableJenisPenawaran;

    set disableJenisPenawaran(bool value) {
      this._disableJenisPenawaran = value;
      notifyListeners();
    }

    String get colaType => _colaType;

    set colaType(String value) {
      this._colaType = value;
      notifyListeners();
    }

    bool get isDisablePACIAAOSCONA => _isDisablePACIAAOSCONA;

    set isDisablePACIAAOSCONA(bool value) {
      this._isDisablePACIAAOSCONA = value;
      notifyListeners();
    }

    bool get isAsuransiPerluasanType => _isAsuransiPerluasanType;

    set isAsuransiPerluasanType(bool value) {
      _isAsuransiPerluasanType = value;
      notifyListeners();
    }

    bool get flagDisableNilaiPertanggungan => _flagDisableNilaiPertanggungan;

    set flagDisableNilaiPertanggungan(bool value) {
        this._flagDisableNilaiPertanggungan = value;
        notifyListeners();
    }

    bool get loadData => _loadData;

    set loadData(bool value) {
        this._loadData = value;
    }

    String get custType => _custType;

    set custType(String value) {
        _custType = value;
        notifyListeners();
    }

    String get lastKnownState => _lastKnownState;

    set lastKnownState(String value) {
        _lastKnownState = value;
        notifyListeners();
    }

    GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

    InsuranceTypeModel get insuranceTypeSelected {
        return this._insuranceTypeSelected;
    }

    set insuranceTypeSelected(InsuranceTypeModel value) {
        this._insuranceTypeSelected = value;
        this._assuranceSelected = null;
        this._controllerAssurance.clear();
        this._coverage2Selected = null;
        this._controllerCoverage2.clear();
        if(value.KODE == "2"){
            isAsuransiPerluasanType = false;
        }
        else {
            isAsuransiPerluasanType = true;
        }
        notifyListeners();
    }

    UnmodifiableListView<InsuranceTypeModel> get listInsurance {
        return UnmodifiableListView(this._listInsuranceType);
    }

    void searchAssurance(BuildContext context) async {
        AssuranceModel _data = await Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ChangeNotifierProvider(
                    create: (context) => SearchAssuranceChangeNotifier(),
                    child: SearchAssurance())));
        if (_data != null) {
            this._assuranceSelected = _data;
            this._controllerAssurance.text = "${_data.id} - ${_data.text}";
            notifyListeners();
        }
    }

    AssuranceModel get assuranceSelected => _assuranceSelected;

    set assuranceSelected(AssuranceModel value) {
        _assuranceSelected = value;
        notifyListeners();
    }

    TextEditingController get controllerAssurance {
        return this._controllerAssurance;
    }

    set controllerAssurance(TextEditingController value) {
        _controllerAssurance = value;
    }

    void searchCompany(BuildContext context) async {
        CompanyTypeInsuranceModel _data = await Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => ChangeNotifierProvider(
                    create: (context) => SearchCompanyInsuranceChangeNotifier(),
                    child: SearchCompanyInsurance(insuranceTypeSelected.KODE))));
        if (_data != null) {
            print(_data.PARENT_KODE);
            this._companySelected = _data;
            print(_companySelected.PARENT_KODE);
            this._controllerCompany.text = "${_data.KODE} - ${_data.DESKRIPSI}";
            notifyListeners();
        }
    }

    CompanyTypeInsuranceModel get companySelected => _companySelected;

    set companySelected(CompanyTypeInsuranceModel value) {
        _companySelected = value;
        notifyListeners();
    }

    TextEditingController get controllerCompany {
        return this._controllerCompany;
    }

    set controllerCompany(TextEditingController value) {
        _controllerCompany = value;
    }

    String get orderProductInsuranceID => _orderProductInsuranceID;

    set orderProductInsuranceID(String value) {
      this._orderProductInsuranceID = value;
    }

  void searchProduct(BuildContext context) async {
        if(_companySelected != null){
            ProductModel _data = await Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ChangeNotifierProvider(
                        create: (context) => SearchProductChangeNotifier(),
                        child: SearchProduct(flag: "2", company: _companySelected.PARENT_KODE.toString()))));
            if (_data != null) {
                bool _check = await _cekPrimeKPM(context, _data.KODE);
                if(_check){
                    this._productSelected = _data;
                    this._controllerProduct.text = "${_data.KODE} - ${_data.DESKRIPSI}";
                }
                else {
                    showSnackBar("Proses Cek KPM Prima Gagal");
                    this._productSelected = null;
                    this._controllerProduct.clear();
                }
                notifyListeners();
            }
        }
        else {
            showSnackBar("Silahkan pilih perusahaan");
        }
    }

    Future<bool> _cekPrimeKPM(BuildContext context, String product) async{
        this._loadData = true;
        bool _cekPrime = false;
        var _providerInfoUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
        try{
            final ioc = new HttpClient();
            ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
            final _http = IOClient(ioc);

            var _body = jsonEncode(
                {
                    "jenisProduk": _providerInfoUnitObject.productTypeSelected.id,
                    "levelAsuransi": this._insuranceTypeSelected.KODE,
                    "produkAsuransi": product
                }
            );
            print("body kpm = $_body");

            String _cekPrimeKPM = await storage.read(key: "CekPrimeKPM");
            final _response = await _http.post(
                "${BaseUrl.urlGeneral}$_cekPrimeKPM",
                body: _body,
                headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
            ).timeout(Duration(seconds: 30));
            final _result = jsonDecode(_response.body);
            print("result kpm = $_result");
            if(_response.statusCode == 200){
                if(_result['message'] == "SUCCESS" || _result['message'] == "Success"){
                    _cekPrime = true;
                }
                else {
                    _cekPrime = false;
                }
            }
            this._loadData = false;
        }
        catch(e){
            print("catch = ${e.toString()}");
            this._loadData = false;
            _cekPrime = false;
        }
        notifyListeners();
        return _cekPrime;
    }

    ProductModel get productSelected => _productSelected;

    set productSelected(ProductModel value) {
        _productSelected = value;
        notifyListeners();
    }

    TextEditingController get controllerProduct {
        return this._controllerProduct;
    }

    set controllerProduct(TextEditingController value) {
        _controllerProduct = value;
    }

    TextEditingController get controllerPeriodType {
        return this._controllerPeriodType;
    }

    set controllerPeriodType(TextEditingController value) {
        _controllerPeriodType = value;
    }

    List<String> _listType = [];

    String get typeSelected {
        return this._typeSelected;
    }

    set typeSelected(String value) {
        if(this._typeSelected != null){
            if(this._typeSelected.toString() != value.toString()){
                this._coverage1Selected = null;
                this._controllerCoverage1.clear();
                this._coverage2Selected = null;
                this._controllerCoverage2.clear();
                this._subTypeSelected = null;
                this._controllerSubType.clear();
                this._controllerCoverageType.clear();
                this._controllerCoverageValue.clear();
                this._controllerUpperLimit.clear();
                this._controllerLowerLimit.clear();
                this._controllerUpperLimitRate.clear();
                this._controllerLowerLimitRate.clear();
                this._controllerPriceCash.clear();
                this._controllerPriceCredit.clear();
                this._controllerTotalPriceSplit.clear();
                this._controllerTotalPriceRate.clear();
                this._controllerTotalPrice.clear();
            }
        }
        this._typeSelected = value;
        notifyListeners();
    }

    UnmodifiableListView<String> get listType {
        return UnmodifiableListView(this._listType);
    }

    void searchCoverage(BuildContext context,int flag) async {
        if(_companySelected != null && _productSelected != null){
            if(flag == 1){
                CoverageModel _data = await Navigator.push(context, MaterialPageRoute(
                    builder: (context) => ChangeNotifierProvider(
                        create: (context) => SearchCoverage1ChangeNotifier(),
                        child: SearchCoverage1(flag: 1, company: _companySelected.KODE, product: _productSelected.KODE))));
                if (_data != null) {
                    this._coverage1Selected = _data;
                    this._controllerCoverage1.text = "${_data.KODE} - ${_data.DESKRIPSI}";
                    this._coverage2Selected = null;
                    this._controllerCoverage2.text = "";
                    notifyListeners();
                }
            }
            else{
                if(_coverage1Selected != null){
                    CoverageModel _data = await Navigator.push(context, MaterialPageRoute(
                        builder: (context) => ChangeNotifierProvider(
                            create: (context) => SearchCoverage1ChangeNotifier(),
                            child: SearchCoverage1(flag: 2, company: _companySelected.KODE, product: _productSelected.KODE, coverage1: _coverage1Selected.KODE, period: _controllerPeriodType.text, type: _typeSelected))));
                    if (_data != null) {
                        this._coverage2Selected = _data;
                        this._controllerCoverage2.text = "${_data.KODE} - ${_data.DESKRIPSI}";
                        notifyListeners();
                    }
                }
                else {
                    showSnackBar("Silahkan pilih Coverage 1");
                }
            }
            if(_insuranceTypeSelected != null){
                if(_insuranceTypeSelected.KODE == "2"){
                    _getBatasAtasBawahUtama(context);
                }
            }
        }
        else if(_companySelected == null){
            showSnackBar("Silahkan pilih Perusahaan");
        }
        else {
            showSnackBar("Silahkan pilih Produk");
        }
    }

    CoverageModel get coverage1Selected => _coverage1Selected;

    set coverage1Selected(CoverageModel value) {
        _coverage1Selected = value;
        notifyListeners();
    }

    TextEditingController get controllerCoverage1 {
        return this._controllerCoverage1;
    }

//    void searchCoverage2(BuildContext context) async {
//        Coverage2Model _data = await Navigator.push(
//            context,
//            MaterialPageRoute(
//                builder: (context) => ChangeNotifierProvider(
//                    create: (context) => SearchCoverage2ChangeNotifier(),
//                    child: SearchCoverage2())));
//        if (_data != null) {
//            this._coverage2Selected = _data;
//            this._controllerCoverage2.text = "${_data.id} - ${_data.text}";
//            notifyListeners();
//        }
//    }

    void searchSubType(BuildContext context) async {
        if(this._coverage1Selected != null){
            SubTypeModel _data = await Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ChangeNotifierProvider(
                    create: (context) => SearchSubTypeChangeNotifier(),
                    child: SearchSubType(this._coverage1Selected.PARENT_KODE))));
            if (_data != null) {
                this._subTypeSelected = _data;
                this._controllerSubType.text = "${_data.KODE} - ${_data.DESKRIPSI}";
                notifyListeners();
                _getBatasAtasBawahPerluasan(context);
            }
        } else {
            showSnackBar("Silahkan pilih coverage 1");
        }
    }

    SubTypeModel get subTypeSelected => _subTypeSelected;

    set subTypeSelected(SubTypeModel value) {
        _subTypeSelected = value;
        notifyListeners();
    }

    CoverageModel get coverage2Selected => _coverage2Selected;

    set coverage2Selected(CoverageModel value) {
        _coverage2Selected = value;
        notifyListeners();
    }

    CoverageTypeModel get coverageTypeSelected => _coverageTypeSelected;

    set coverageTypeSelected(CoverageTypeModel value) {
      _coverageTypeSelected = value;
      notifyListeners();
    }

    TextEditingController get controllerCoverage2 {
        return this._controllerCoverage2;
    }

    TextEditingController get controllerCoverageType {
        return this._controllerCoverageType;
    }

    set controllerCoverageType(TextEditingController value) {
        _controllerCoverageType = value;
    }

    TextEditingController get controllerSubType {
        return this._controllerSubType;
    }

    set controllerSubType(TextEditingController value) {
        _controllerSubType = value;
    }

    TextEditingController get controllerCoverageValue {
        return this._controllerCoverageValue;
    }

    set controllerCoverageValue(TextEditingController value) {
        _controllerCoverageValue = value;
    }

    TextEditingController get controllerUpperLimit {
        return this._controllerUpperLimit;
    }

    set controllerUpperLimit(TextEditingController value) {
        _controllerUpperLimit = value;
    }

    TextEditingController get controllerLowerLimit {
        return this._controllerLowerLimit;
    }

    set controllerLowerLimit(TextEditingController value) {
        _controllerLowerLimit = value;
    }

    TextEditingController get controllerUpperLimitRate {
        return this._controllerUpperLimitRate;
    }

    set controllerUpperLimitRate(TextEditingController value) {
        _controllerUpperLimitRate = value;
    }

    TextEditingController get controllerLowerLimitRate {
        return this._controllerLowerLimitRate;
    }

    set controllerLowerLimitRate(TextEditingController value) {
        _controllerLowerLimitRate = value;
    }

    TextEditingController get controllerPriceCash {
        return this._controllerPriceCash;
    }

    set controllerPriceCash(TextEditingController value) {
        _controllerPriceCash = value;
    }

    TextEditingController get controllerPriceCredit{
        return this._controllerPriceCredit;
    }

    set controllerPriceCredit(TextEditingController value) {
        _controllerPriceCredit = value;
    }

    TextEditingController get controllerTotalPriceSplit {
        return this._controllerTotalPriceSplit;
    }

    set controllerTotalPriceSplit(TextEditingController value) {
        _controllerTotalPriceSplit = value;
    }

    TextEditingController get controllerTotalPrice{
        return this._controllerTotalPrice;
    }

    TextEditingController get controllerType => _controllerType;

    set controllerTotalPrice(TextEditingController value) {
        _controllerTotalPrice = value;
    }

    TextEditingController get controllerTotalPriceRate{
        return this._controllerTotalPriceRate;
    }

    set controllerTotalPriceRate(TextEditingController value) {
        _controllerTotalPriceRate = value;
    }

    bool get autoValidate => _autoValidate;

    set autoValidate(bool value) {
        this._autoValidate = value;
        notifyListeners();
    }

    GlobalKey<FormState> get key => _key;

    String get numberOfCollateral => _numberOfCollateral;

    set numberOfCollateral(String value) {
        this._numberOfCollateral = value;
        notifyListeners();
    }

    List<String> _listNumberOfColla = [];

    UnmodifiableListView<String> get listNumberOfColla {
        return UnmodifiableListView(this._listNumberOfColla);
    }

    void check(BuildContext context, int flag, int index) {
        final _form = _key.currentState;
        var _providerCollateral = Provider.of<InformationCollateralChangeNotifier>(context, listen: false);
        if (flag == 0) {
            if (_form.validate()) {
                calculateTotalCost();
                Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false)
                    .addListInfoMajorInsurance(MajorInsuranceModel(
                    this._insuranceTypeSelected,
                    _providerCollateral.collateralTypeModel.id,
                    this._numberOfCollateral == null ? "" : this._numberOfCollateral,
                    this._companySelected,
                    this._productSelected,
                    this._controllerPeriodType.text,
                    this._switchType ? this._controllerType.text : this._typeSelected,
                    this._coverage1Selected,
                    this._coverage2Selected,
                    this._subTypeSelected,
                    this._coverageTypeSelected,
                    this._controllerCoverageValue.text,
                    this._controllerUpperLimit.text,
                    this._controllerLowerLimit.text,
                    this.controllerUpperLimitRate.text,
                    this.controllerLowerLimitRate.text,
                    this._controllerPriceCash.text,
                    this._controllerPriceCredit.text,
                    this._controllerTotalPriceSplit.text,
                    this.controllerTotalPriceRate.text,
                    this._controllerTotalPrice.text,
                    this._isEdit,
                    this._insuranceTypeDakor,
                    this._installmentNoDakor,
                    this._companyDakor,
                    this._productDakor,
                    this._periodDakor,
                    this._typeDakor,
                    this._coverage1Dakor,
                    this._coverage2Dakor,
                    this._subTypeDakor,
                    this._coverageTypeDakor,
                    this._coverageValueDakor,
                    this._upperLimitRateDakor,
                    this._upperLimitValueDakor,
                    this._lowerLimitRateDakor,
                    this._lowerLimitValueDakor,
                    this._cashDakor,
                    this._creditDakor,
                    this._totalPriceSplitDakor,
                    this._totalPriceRateDakor,
                    this._totalPriceDakor,
                    "NEW"
                ));
                Navigator.pop(context);
            } else {
                autoValidate = true;
            }
        } else {
            checkDataDakor(index);
            if (_form.validate()) {
                calculateTotalCost();
                debugPrint("CEK INSURANCE ${this._orderProductInsuranceID}");
                Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false)
                    .updateListInfoMajorInsurance(MajorInsuranceModel(
                    this._insuranceTypeSelected,
                    this._colaType,
                    this._numberOfCollateral,
                    this._companySelected,
                    this._productSelected,
                    this._controllerPeriodType.text,
                    this._switchType ? this._controllerType.text : this._typeSelected,
                    this._coverage1Selected,
                    this._coverage2Selected,
                    this._subTypeSelected,
                    this._coverageTypeSelected,
                    this._controllerCoverageValue.text,
                    this._controllerUpperLimit.text,
                    this._controllerLowerLimit.text,
                    this.controllerUpperLimitRate.text,
                    this.controllerLowerLimitRate.text,
                    this._controllerPriceCash.text,
                    this._controllerPriceCredit.text,
                    this._controllerTotalPriceSplit.text,
                    this.controllerTotalPriceRate.text,
                    this._controllerTotalPrice.text,
                    this._isEdit,
                    this._insuranceTypeDakor,
                    this._installmentNoDakor,
                    this._companyDakor,
                    this._productDakor,
                    this._periodDakor,
                    this._typeDakor,
                    this._coverage1Dakor,
                    this._coverage2Dakor,
                    this._subTypeDakor,
                    this._coverageTypeDakor,
                    this._coverageValueDakor,
                    this._upperLimitRateDakor,
                    this._upperLimitValueDakor,
                    this._lowerLimitRateDakor,
                    this._lowerLimitValueDakor,
                    this._cashDakor,
                    this._creditDakor,
                    this._totalPriceSplitDakor,
                    this._totalPriceRateDakor,
                    this._totalPriceDakor,
                    this._orderProductInsuranceID
                ), context, index);
                Navigator.pop(context);
            } else {
                autoValidate = true;
            }
        }
    }

    void setValueForEditMajorInsurance(BuildContext context, MajorInsuranceModel data, int index, String tenor)  {
        for (int i = 0; i < this._listInsuranceType.length; i++) {
            if (data.insuranceType.KODE == this._listInsuranceType[i].KODE) {
                this._insuranceTypeSelected = this._listInsuranceType[i];
            }
        }
        getJenisPeriode(context, tenor);
        if(this._insuranceTypeSelected.KODE == "2"){
            isAsuransiPerluasanType = false;
        }
        else {
            isAsuransiPerluasanType = true;
        }
        this._colaType = data.colaType;
        this._controllerAssurance.text = data.numberOfColla;
        this._companySelected = data.company;
        this._controllerCompany.text = data.company.KODE +" - "+data.company.DESKRIPSI;
        this._productSelected = data.product;
        this._controllerProduct.text = data.product.KODE +" - "+data.product.DESKRIPSI;
        this._controllerPeriodType.text = data.periodType;
        this._typeSelected = data.type;
        for (int i = 0; i < this._listType.length; i++) {
            if (data.type == this._listType[i]) {
                this._typeSelected = this._listType[i];
            }
        }
        this._coverage1Selected = data.coverage1;
        this._controllerCoverage1.text = data.coverage1.KODE +" - "+data.coverage1.DESKRIPSI;
        this._coverage2Selected = data.coverage2 != null ? data.coverage2 : null;
        this._controllerCoverage2.text = data.coverage2 != null ? data.coverage2.KODE +" - "+data.coverage2.DESKRIPSI : "";
        this._subTypeSelected = data.subType != null ? data.subType : null;
        this._controllerSubType.text = data.subType != null ? data.subType.KODE +" - "+data.subType.DESKRIPSI : "";
        this._coverageTypeSelected = data.coverageType != null ? data.coverageType : null;
        this._controllerCoverageType.text = data.coverageType != null ? data.coverageType.KODE +" - "+data.coverageType.DESKRIPSI : "";
        this._controllerCoverageValue.text = data.coverageValue;
        this._controllerUpperLimit.text = data.upperLimit;
        this._controllerLowerLimit.text = data.lowerLimit;
        this._controllerUpperLimitRate.text = data.upperLimitRate;
        this._controllerLowerLimitRate.text = data.lowerLimitRate;
        this._controllerPriceCash.text = data.priceCash;
        this._controllerPriceCredit.text = data.priceCredit;
        this._controllerTotalPriceSplit.text = data.totalPriceSplit;
        this._controllerTotalPriceRate.text = data.totalPriceRate;
        this._controllerTotalPrice.text = data.totalPrice;
        this._orderProductInsuranceID = data.orderProductInsuranceID;
        formatting();
        notifyListeners();
        calculatePeriod(1,data);
        checkDataDakor(index);
    }

    void clearData(){
        this._insuranceTypeSelected = null;
        this._numberOfCollateral = "";
        this._assuranceSelected = null;
        this._controllerAssurance.clear();
        this._companySelected = null;
        this._controllerCompany.clear();
        this._productSelected = null;
        this._controllerProduct.clear();
        this._typeSelected = null;
        this._controllerPeriodType.clear();
        this._coverage1Selected = null;
        this._controllerCoverage1.clear();
        this._coverage2Selected = null;
        this._controllerCoverage2.clear();
        this._subTypeSelected = null;
        this._controllerSubType.clear();
        this._controllerCoverageType.clear();
        this._controllerCoverageValue.clear();
        this._controllerUpperLimit.clear();
        this._controllerLowerLimit.clear();
        this._controllerUpperLimitRate.clear();
        this._controllerLowerLimitRate.clear();
        this._controllerTotalPriceSplit.clear();
        this._controllerPriceCash.clear();
        this._controllerPriceCredit.clear();
        this._controllerTotalPrice.clear();
        this._controllerTotalPriceRate.clear();
        this._controllerType.clear();
        isDisablePACIAAOSCONA = false;
        disableJenisPenawaran = false;
    }

    Future<void> addData(BuildContext context,MajorInsuranceModel data,int flag, int index) async{
        await setPreference();
        getDataFromDashboard(context);
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        List _dataCL = await _dbHelper.selectMS2LME();
        var _providerAsuransiUtama = Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false);
        var _providerInfoCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false);
        // var _providerUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
        // this._controllerCoverageValue.text = _providerInfoCreditStructure.controllerObjectPrice.text;
        // if (int.parse(_providerInfoCreditStructure.periodOfTimeSelected) <= 6){
        //     if (_providerUnitObject.rehabTypeSelected != null){
        //         if(_providerUnitObject.rehabTypeSelected.id == "001"){
        //             this._controllerPeriodType.text = "12";
        //         } else{
        //             this._controllerPeriodType.text = "6";
        //         }
        //     }else{
        //         this._controllerPeriodType.text = "6";
        //     }
        // }
        // else {
        //     this._controllerPeriodType.text = ((int.parse(_providerInfoCreditStructure.periodOfTimeSelected)/6).ceil()*6).toString();
        // }
        await getPeriode(context, _preferences.getString("jenis_penawaran") == "002" && _dataCL[0]['pencairan_ke'] == "1" ? data.type : _providerInfoCreditStructure.periodOfTimeSelected); // ditambah kondisi if karena ketika aoro, data struktur kredit bisa null
        if(flag == 0){
            if(_providerAsuransiUtama.listFormMajorInsurance.isEmpty){
                this._insuranceTypeSelected = this._listInsuranceType[0];
            }
            for(int i=0; i<_providerAsuransiUtama.listFormMajorInsurance.length; i++){
                print("masuk fot ke $i");
                if(_providerAsuransiUtama.listFormMajorInsurance[i].insuranceType.KODE == "2"){
                    this._listInsuranceType.removeAt(0);
                    this._insuranceTypeSelected = this._listInsuranceType[0];
                    // getJenisPeriode(context, _providerInfoCreditStructure.periodOfTimeSelected);
                }
                else{
                    this._insuranceTypeSelected = this._listInsuranceType[0];
                    // getJenisPeriode(context, _providerInfoCreditStructure.periodOfTimeSelected);
                }
            }
            getJenisPeriode(context, _preferences.getString("jenis_penawaran") == "002" && _dataCL[0]['pencairan_ke'] == "1" ? data.type : _providerInfoCreditStructure.periodOfTimeSelected); // ditambah kondisi if karena ketika aoro, data struktur kredit bisa null
            if(this._insuranceTypeSelected.KODE == "2"){
                isAsuransiPerluasanType = false;
            }
            else {
                isAsuransiPerluasanType = true;
            }
        }
        // getType(int.parse(this._controllerPeriodType.text));
        _insertListNumberOfColla(context,data,flag);
        // await _getListInsuranceType(context,data,flag, index);
        if(flag != 0){
            setValueForEditMajorInsurance(context, data, index, _preferences.getString("jenis_penawaran") == "002" && _dataCL[0]['pencairan_ke'] == "1" ? data.type : _providerInfoCreditStructure.periodOfTimeSelected); // ditambah kondisi if karena ketika aoro, data struktur kredit bisa null
        }
        notifyListeners();
    }

    bool get isListInsuranceTypeNotEmpty => _isListInsuranceTypeNotEmpty;

    set isListInsuranceTypeNotEmpty(bool value) {
        this._isListInsuranceTypeNotEmpty = value;
    }

//     Future<void> _getListInsuranceType(BuildContext context,MajorInsuranceModel data,int flag, int index) async{
//         _listInsuranceType.clear();
//         this._insuranceTypeSelected = null;
//         try{
//             final ioc = new HttpClient();
//             ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
//             final _http = IOClient(ioc);
//             var _body = jsonEncode({
//                 "P_INSR_COMPANY_ID" : "02",
//                 "P_INSR_LEVEL" : "2",
//                 "P_INSR_PRODUCT_ID" :"005",
//                 "P_PARA_OBJECT_ID" :"004"
// //                Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).objectSelected.id
//             });
//
//             String _fieldTipeAsuransi = await storage.read(key: "FieldTipeAsuransi");
//             final _response = await _http.post(
//                 "${BaseUrl.urlGeneral}$_fieldTipeAsuransi",
//                 // "${urlPublic}api/asuransi/get-insurance-type",
//                 body: _body,
//                 headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
//             );
//             print(_response.statusCode);
//             final _data = jsonDecode(_response.body);
//             List _resultData = _data['data'];
//             if(_response.statusCode == 200){
//                 if(_resultData.isNotEmpty){
//                     for(int i=0; i < _data['data'].length;i++){
//                         _listInsuranceType.add(InsuranceTypeModel(_data['data'][i]['PARENT_KODE'], _data['data'][i]['KODE'], _data['data'][i]['DESKRIPSI'].toString().trim()));
//                     }
//                     if(flag != 0){
//                         setValueForEditMajorInsurance(data, index);
//                     }
//                 }
//                 else{
//                     this._isListInsuranceTypeNotEmpty = false;
//                     showSnackBarInfo("Tipe Asuransi tidak tersedia pada API FieldTipeAsuransi");
//                 }
//             }
//             else{
//                 showSnackBar("Tipe Asuransi error ${_response.statusCode}");
//             }
//
//         }
//         catch (e) {
//             print("error ${e.toString()}");
//             showSnackBar("Tipe Asuransi $e");
//         }
//         notifyListeners();
//         // _getType();
//         // _insertListNumberOfCOlla(context,data,flag);
//     }

    void getType(int periode){
        this._listType.clear();
        this._typeSelected = null;
        if(periode >= 3){
            int _jenis = periode % 12;
            int _panjangJenis;
            if(_jenis == 0){
                _panjangJenis = (periode / 12).round();
            }
            else{
                _panjangJenis = (periode / 12).ceil();
            }

            for(int i= 1; i <= _panjangJenis; i++){
                this._listType.add((i*12).toString());
            }
        }
       notifyListeners();
    }

    Future<void> getPeriode(BuildContext context, String tenor) async{
        this._loadData = true;
        var _providerInfoUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
        try{
            final ioc = new HttpClient();
            ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
            final _http = IOClient(ioc);

            var _body = jsonEncode(
                {
                    "P_JENIS_REHAB": _providerInfoUnitObject.rehabTypeSelected != null ?_providerInfoUnitObject.rehabTypeSelected.id : "",
                    "P_TENOR": int.parse(tenor)
                }
            );
            String _fieldJenisAsuransi = await storage.read(key: "FieldPeriodeAsuransi");
            final _response = await _http.post(
                "${BaseUrl.urlGeneral}$_fieldJenisAsuransi",
                body: _body,
                headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
            ).timeout(Duration(seconds: 30));
            final _result = jsonDecode(_response.body);
            List _data = _result['data'];
            if(_response.statusCode == 200){
                this._controllerPeriodType.text = _data[0];
            }
            this._loadData = false;
        }
        catch(e){
            print("catch = ${e.toString()}");
            this._loadData = false;
        }
        notifyListeners();
    }

    void getJenisPeriode(BuildContext context, String tenor) async{
        this._loadData = true;
        this._listType.clear();
        this._typeSelected = null;
        var _providerColla = Provider.of<InformationCollateralChangeNotifier>(context,listen: false);
        try{
            final ioc = new HttpClient();
            ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
            final _http = IOClient(ioc);

            var _body = jsonEncode(
                {
                    "P_GRUP_OBJEK": _providerColla.groupObjectSelected.KODE,
                    "P_LEVEL_ASURANSI": this._insuranceTypeSelected != null ? this._insuranceTypeSelected.KODE : "",
                    "P_PERIODE": int.parse(this._controllerPeriodType.text),
                    "P_TENOR": int.parse(tenor)
                }
            );
            print("body jenis = $_body");
            String _fieldJenisAsuransi = await storage.read(key: "FieldJenisAsuransi");
            final _response = await _http.post(
                "${BaseUrl.urlGeneral}$_fieldJenisAsuransi",
                body: _body,
                headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
            ).timeout(Duration(seconds: 30));
            final _result = jsonDecode(_response.body);
            List _data = _result['data'];
            if(_response.statusCode == 200){
                for(int i=0; i< _data.length; i++){
                    if(_providerColla.groupObjectSelected.KODE == "001"){
                        this._listType.add(_data[i]);
                        if(this._controllerPeriodType.text == _data[i]){
                            this._typeSelected = this._listType[0];
                        }
                    }
                    else {
                        this._listType.add(_data[i]);
                    }
                }
            }
            this._loadData = false;
        }
        catch(e){
            print("catch ${e.toString()}");
            this._loadData = false;
        }
        notifyListeners();
    }

    void _insertListNumberOfColla(BuildContext context,MajorInsuranceModel data,int flag){
        var _provider = Provider.of<InfoAppChangeNotifier>(context,listen: false);
        var _providerInfoMajorInsurance = Provider.of<InfoMajorInsuranceChangeNotifier>(context,listen: false);
        int _numberColla = int.parse(_provider.controllerTotalObject.text);
        // if(flag == 0){
            if(_providerInfoMajorInsurance.listFormMajorInsurance.isNotEmpty){
                for(int i=0; i < _numberColla; i++){
                    _listNumberOfColla.add((i+1).toString());
                }

                // for(int i=0; i < _providerInfoMajorInsurance.listFormMajorInsurance.length; i++){
                //     for(int j=0; j < _listNumberOfColla.length; j++){
                //         if(_listNumberOfColla[j] == _providerInfoMajorInsurance.listFormMajorInsurance[i].numberOfColla){
                //             _listNumberOfColla.removeAt(j);
                //         }
                //     }
                // }
            }
            else{
                for(int i=0; i < _numberColla; i++){
                    _listNumberOfColla.add((i+1).toString());
                }
            }
            if(flag == 1){
              debugPrint("CHECK_DATA NUMBER OF COLLA ${data.numberOfColla}");
               for(int i=0; i < _listNumberOfColla.length; i++){
                   if(data.numberOfColla == _listNumberOfColla[i]){
                       this._numberOfCollateral = _listNumberOfColla[i];
                   }
               }
            }
        // }
        // else{
        //     this._listNumberOfColla.add(data.numberOfColla);
        //     this._numberOfCollateral = this._listNumberOfColla[0];
        // }
        notifyListeners();
    }

    bool _switchType = false;

    bool get switchType => _switchType;

    set switchType(bool value) {
      this._switchType = value;
      notifyListeners();
    }

    void calculatePeriod(int flag,MajorInsuranceModel data){
          this._listType.clear();
          if(flag == 0){
              int _valueType;
              if(switchType){
                  _valueType = int.parse(this._controllerType.text);
                  for(int i =0; i< _listType.length; i++){
                      if(_valueType.toString() == _listType[i]){
                          this._typeSelected = _listType[i];
                      }
                  }
              }
              else{
                  _valueType = int.parse(this._typeSelected);
                  this._controllerType.clear();
                  this._controllerPeriodType.clear();
              }

              if(_valueType < 36){
                  if(_valueType <= 6){
                      this._controllerPeriodType.text = 6.toString();
                  }
                  else if(_valueType > 6 && _valueType <= 12){
                      this._controllerPeriodType.text = 12.toString();
                  }
                  else if(_valueType > 12 && _valueType <= 18){
                      this._controllerPeriodType.text = 18.toString();
                  }
                  else if(_valueType > 18 && _valueType <= 24){
                      this._controllerPeriodType.text = 24.toString();
                  }
                  else if(_valueType > 24 && _valueType <= 30){
                      this._controllerPeriodType.text = 30.toString();
                  }
                  else {
                      this._controllerPeriodType.text = 36.toString();
                  }
                  switchType = false;
                  notifyListeners();
              }
              else{
                  switchType = true;
                  this._controllerPeriodType.clear();
                  if(this._controllerType.text != ""){
                      if(_valueType >= 36){
                          if(_valueType >= 36 && _valueType <= 48){
                              this._controllerPeriodType.text = 48.toString();
                          }
                          else if(_valueType > 48 && _valueType <= 60){
                              this._controllerPeriodType.text = 60.toString();
                          }
                          else{
                              this._controllerType.text = 60.toString();
                              this._controllerPeriodType.text = 60.toString();
                          }
                      }
                      else{
                          for(int i=0; i < _listType.length; i++){
                              if(this._controllerType.text == _listType[i]){
                                  this._typeSelected = _listType[i];
                              }
                          }
                          switchType = false;
                          calculatePeriod(0,null);
                          this._controllerType.clear();
                      }
                      notifyListeners();
                  }
              }
          }
          else{
              if(data.type.isNotEmpty && int.parse(data.type) < 36){
                  this._typeSelected = data.type;
                  this._controllerPeriodType.text = data.periodType;
                  switchType = false;
              }
              else{
                  this._controllerType.text = data.type;
                  this._controllerPeriodType.text = data.periodType;
                  switchType = true;
              }

          }
    }

    RegExInputFormatter get amountValidator => _amountValidator;

    FormatCurrency get formatCurrency => _formatCurrency;

    void _getBatasAtasBawahPerluasan(BuildContext context) async{
        var _providerPhoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
        var _providerInfoAppl = Provider.of<InfoAppChangeNotifier>(context,listen: false);
        var _providerInfoUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
        SharedPreferences _preference = await SharedPreferences.getInstance();
        this._loadData = true;
        final ioc = new HttpClient();
        ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
        final _http = IOClient(ioc);

        var _body = jsonEncode(
            {
                "P_APPLICATION_DATE": _providerInfoAppl.controllerOrderDate.text,
                "P_FIN_TYPE_ID": _preference.getString("cust_type") != "COM" ? "${_providerPhoto.typeOfFinancingModelSelected.financingTypeId}":"${_providerInfoUnitObject.typeOfFinancingModelSelected.financingTypeId}",
                "P_INSR_COMPANY_ID": this._companySelected != null ? this._companySelected.KODE : '',
                "P_INSR_PRODUCT_ID": "${this._productSelected.KODE}",
                "P_INSR_SUBTYPE_ID": this._subTypeSelected.KODE,
                "P_INSR_TYPE_ID": this.coverage1Selected != null ? this.coverage1Selected.KODE : "",
                "P_PARA_CP_CENTRA_ID": "${_preference.getString("SentraD")}",
            }
        );
        print("body batas atas perluasan $_body");

        try{
            String _batasAtasBawahPerluasan = await storage.read(key: "BatasAtasBawahPerluasan");
            final _response = await _http.post(
                "${BaseUrl.urlGeneral}$_batasAtasBawahPerluasan",
                body: _body,
                headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
            ).timeout(Duration(seconds: 30));
            final _result = jsonDecode(_response.body);
            print("result batas atas = $_result");
            List _data = _result['data'];
            if(_data.isNotEmpty){
                if(_data[0]['PARA_LOWER_LIMIT'].toString() == "0" && _data[0]['PARA_UPPER_LIMIT'].toString() == "0"){
                    showSnackBar("Mappingan untuk rate asuransi belum ada");
                }
                else {
                    this._coverageTypeSelected = CoverageTypeModel(_data[0]['PARA_LIMIT'].toString(), _data[0]['PARA_LIMIT_DESC'].toString());
                    this._controllerCoverageType.text = "${_coverageTypeSelected.KODE} - ${_coverageTypeSelected.DESKRIPSI}";
                    getNilaiPertanggungan(context, _data[0]);
                }
                // _countLimitLowerUpper(_data[0]);
            }
            else {
                showSnackBar("Mappingan untuk rate asuransi belum ada");
            }
            this._loadData = false;
        }
        catch(e){
            this._loadData = false;
            showSnackBar("error BatasAtasBawahPerluasan ${e.toString()}");
        }
        notifyListeners();
    }

    void _getBatasAtasBawahUtama(BuildContext context) async{
        var _providerPhoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
        var _providerInfoAppl = Provider.of<InfoAppChangeNotifier>(context,listen: false);
        var _providerInfoUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
        var _providerInfoCollateral = Provider.of<InformationCollateralChangeNotifier>(context,listen: false);
        var _providerInfCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false);
        SharedPreferences _preference = await SharedPreferences.getInstance();
        this._loadData = true;
        final ioc = new HttpClient();
        ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
        final _http = IOClient(ioc);

        var _body = jsonEncode(
            {
                "P_APPLICATION_DATE": _providerInfoAppl.controllerOrderDate.text,
                "P_INSR_COMPANY_ID": "${this._companySelected.KODE}",
                "P_INSR_PRODUCT_ID": "${this._productSelected.KODE}",
                "P_INSR_COV_1": this._coverage1Selected != null ? this._coverage1Selected.KODE : "",
                "P_INSR_COV_2": this._coverage2Selected != null ? this._coverage2Selected.KODE : "",
                "P_INSR_FIN_TYPE_ID": _preference.getString("cust_type") != "COM" ? "${_providerPhoto.typeOfFinancingModelSelected.financingTypeId}":"${_providerInfoUnitObject.typeOfFinancingModelSelected.financingTypeId}",
                "P_INSR_JENIS": "${this._typeSelected}",
                "P_INSR_MAX_OTR": _providerInfCreditStructure.controllerObjectPrice.text.isEmpty ? 0 : "${double.parse(_providerInfCreditStructure.controllerObjectPrice.text.replaceAll(",", "")).round()}", //_providerInfoCollateral.collateralTypeModel.id == "001" ? int.parse(_providerInfoCollateral.controllerHargaJualShowroom.text.replaceAll(",", "").split(".")[0]) : int.parse(_providerInfoCollateral.controllerHargaBangunan.text.replaceAll(",", "").split(".")[0]), //_providerInfCreditStructure.controllerObjectPrice.text.isEmpty ? 0 : "${double.parse(_providerInfCreditStructure.controllerObjectPrice.text.replaceAll(",", "")).round()}", // harga object dr inf structure credit
                "P_INSR_OBJT_PURPOSE_ID": "${_providerInfoCollateral.collateralUsageOtoSelected.id}",
                "P_INSR_OBJT_TYPE_ID": "${_providerInfoCollateral.objectTypeSelected.id}",
                "P_INSR_PERIOD": "${this._controllerPeriodType.text}",
                "P_PARA_CP_CENTRA_ID": "${_preference.getString("SentraD")}",
                "P_PROD_TYPE_ID": "${_providerInfoUnitObject.productTypeSelected.id}",
            }
        );
        print("body batas atas $_body");
        var _timeStart = DateTime.now();
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, "Start get batas atas dan batas bawah ${_preferences.getString("last_known_state")}","Start get batas atas dan batas bawah ${_preferences.getString("last_known_state")}");
        try{
            String _batasAtasBawahUtama = await storage.read(key: "BatasAtasBawahUtama");
            final _response = await _http.post(
                "${BaseUrl.urlGeneral}$_batasAtasBawahUtama",
                body: _body,
                headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
            ).timeout(Duration(seconds: 30));
            final _result = jsonDecode(_response.body);
            print("result batas atas = $_result");
            List _data = _result['data'];
            if(_data.isNotEmpty){
                if(_data[0]['PARA_LOWER_LIMIT'].toString() == "0" && _data[0]['PARA_UPPER_LIMIT'].toString() == "0"){
                    showSnackBar("Mappingan untuk rate asuransi belum ada");
                }
                else {
                    this._coverageTypeSelected = CoverageTypeModel(_data[0]['PARA_INSR_SOURCE_TYPE_ID'], _data[0]['PARA_INSR_SOURCE_TYPE_NAME']);
                    this._controllerCoverageType.text = "${_data[0]['PARA_INSR_SOURCE_TYPE_ID']} - ${_data[0]['PARA_INSR_SOURCE_TYPE_NAME']}";
                    getNilaiPertanggungan(context, _data[0]);
                }
                // _countLimitLowerUpper(_data[0]);
            }
            else{
                showSnackBar("Mappingan untuk rate asuransi belum ada");
            }
            this._loadData = false;
        }
        catch(e){
            this._loadData = false;
            showSnackBar("error BatasAtasBawahUtama ${e.toString()}");
        }
        notifyListeners();
    }

    void getNilaiPertanggungan(BuildContext context, Map data) async {
        var _providerInfoUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
        var _providerInfoCollateral = Provider.of<InformationCollateralChangeNotifier>(context,listen: false);
        var _providerInfCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false);
        var _providerAsuransiUtama = Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false);
        var _providerAsuransiTambahan = Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false);
        int _allCredit = 0;
        int _creditInsrLife = 0;
        //fee credit
        for(int i=0; i<_providerInfCreditStructure.listInfoFeeCreditStructure.length; i++){
          if(_providerInfCreditStructure.listInfoFeeCreditStructure[i].feeTypeModel.id == "01" || _providerInfCreditStructure.listInfoFeeCreditStructure[i].feeTypeModel.id == "02"){
            _allCredit += double.parse(_providerInfCreditStructure.listInfoFeeCreditStructure[i].creditCost.replaceAll(",", "")).toInt();
          }
        }
        //asuransi utama, perluasan
        for(int i=0; i<_providerAsuransiUtama.listFormMajorInsurance.length; i++){
          _allCredit += double.parse(_providerAsuransiUtama.listFormMajorInsurance[i].priceCredit.replaceAll(",", "")).toInt();
        }
        //asuransi tambahan
        for(int i=0; i<_providerAsuransiTambahan.listFormAdditionalInsurance.length; i++){
          if(_providerAsuransiTambahan.listFormAdditionalInsurance[i].product.KODE != "007")
          _allCredit += double.parse(_providerAsuransiTambahan.listFormAdditionalInsurance[i].priceCredit.replaceAll(",", "")).toInt();
        }

        for(int i=0; i<_providerAsuransiTambahan.listFormAdditionalInsurance.length; i++){
          if(_providerAsuransiTambahan.listFormAdditionalInsurance[i].product.KODE == "007")
            _creditInsrLife += double.parse(_providerAsuransiTambahan.listFormAdditionalInsurance[i].priceCredit.replaceAll(",", "")).toInt();
        }
        print("total all credit = $_allCredit");

        this._loadData = true;
        final ioc = new HttpClient();
        ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
        final _http = IOClient(ioc);
        var _body = jsonEncode(
            {
                "all_Credit": _allCredit,
                "appraisal_Price": _providerInfoCollateral.controllerTaksasiPrice.text.isNotEmpty ? int.parse(_providerInfoCollateral.controllerTaksasiPrice.text.replaceAll(",", "").split(".")[0]) : 0,
                "building_Price": _providerInfoCollateral.controllerBuildingArea.text.isNotEmpty ? int.parse(_providerInfoCollateral.controllerBuildingArea.text.replaceAll(",", "").split(".")[0]) : 0,
                "colla_Same_Unit": _providerInfoCollateral.radioValueIsCollateralSameWithUnitOto == 0 ? false : true,
                "colla_Type_ID": _providerInfoCollateral.collateralTypeModel != null ? _providerInfoCollateral.collateralTypeModel.id : "",
                "credit_Insr_Life": this._insuranceTypeSelected != null && this._coverageTypeSelected != null ? this._insuranceTypeSelected.KODE == "1" && this._coverageTypeSelected.KODE != "007" ? 0 : _creditInsrLife : _creditInsrLife,
                "harga_Objek": int.parse(_providerInfCreditStructure.controllerObjectPrice.text.replaceAll(",", "").split(".")[0]),
                "jaminan_DP": _providerInfoCollateral.controllerDPJaminan.text.isNotEmpty ? int.parse(_providerInfoCollateral.controllerDPJaminan.text.replaceAll(",", "").split(".")[0]) : 0,
                "jenis_Pertanggungan": this._coverageTypeSelected != null ? this._coverageTypeSelected.KODE : "",
                "jenis_Rehab": _providerInfoUnitObject.rehabTypeSelected != null ? _providerInfoUnitObject.rehabTypeSelected.id : "",
                "jumlah_Pinjaman": int.parse(_providerInfCreditStructure.controllerTotalLoan.text.replaceAll(",", "").split(".")[0]),
                "level_Asuransi": this._insuranceTypeSelected != null ? this._insuranceTypeSelected.KODE : "",
                "limit_Asuransi_Perluasan": this._insuranceTypeSelected.KODE != "02" ? this.typeSelected : "",
                "nett_DP": int.parse(_providerInfCreditStructure.controllerNetDP.text.replaceAll(",", "").split(".")[0]),
                "showroom_Price": _providerInfoCollateral.collateralTypeModel != null ? _providerInfoCollateral.collateralTypeModel.id != "003" ? int.parse(_providerInfoCollateral.controllerHargaJualShowroom.text.replaceAll(",", "").split(".")[0]) : 0 : 0,
                "total_Price": int.parse(_providerInfCreditStructure.controllerTotalPrice.text.replaceAll(",", "").split(".")[0])
            }
        );
        print("body nilai pertanggunan = $_body");
        var _timeStart = DateTime.now();
        SharedPreferences _preferences = await SharedPreferences.getInstance();
        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, "Start get nilai pertanggungan ${_preferences.getString("last_known_state")}","Start get nilai pertanggungan ${_preferences.getString("last_known_state")}");
        try{
            String _fieldNilaiPertanggungan = await storage.read(key: "FieldNilaiPertanggungan");
            print("${BaseUrl.urlGeneral}$_fieldNilaiPertanggungan");
            final _response = await _http.post(
                "${BaseUrl.urlGeneral}$_fieldNilaiPertanggungan",
                body: _body,
                headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
            ).timeout(Duration(seconds: 30));

            final _result = jsonDecode(_response.body);
            print("cek result nilai pertanggungan $_result");
            if(_response.statusCode == 200){
                if(_result['data']['new_id_jenis_pertanggungan'] != "" && _result['data']['new_desc_jenis_pertanggungan'] != ""){
                    this._coverageTypeSelected = CoverageTypeModel(_result['data']['new_id_jenis_pertanggungan'].toString(), _result['data']['new_desc_jenis_pertanggungan'].toString());
                    this._controllerCoverageType.text = "${_coverageTypeSelected.KODE} - ${_coverageTypeSelected.DESKRIPSI}";
                }
                this._controllerCoverageValue.text = formatCurrency.formatCurrency(_result['data']['nilai_Pertanggungan'].toString());
                this._flagDisableNilaiPertanggungan = _result['data']['flag_Dsble_Nilai_Pertanggungan']; // true = field disable | false = field enable
                _countLimitLowerUpper(data);
            }
            else {
                this._controllerCoverageValue.text = "0.00";
            }
            this._loadData = false;
        }
        catch(e){
            this._loadData = false;
            showSnackBar("error FieldNilaiPertanggungan ${e.toString()}");
        }
        notifyListeners();
    }

    void formatting() {
        _controllerCoverageValue.text = formatCurrency.formatCurrency(_controllerCoverageValue.text);
        _controllerUpperLimit.text = _formatCurrency.formatCurrency(_controllerUpperLimit.text);
        _controllerLowerLimit.text = _formatCurrency.formatCurrency(_controllerLowerLimit.text);
        _controllerPriceCash.text = formatCurrency.formatCurrency(_controllerPriceCash.text);
        _controllerPriceCredit.text = formatCurrency.formatCurrency(_controllerPriceCredit.text);
        _controllerTotalPrice.text = formatCurrency.formatCurrency(_controllerTotalPrice.text);
    }

    void _countLimitLowerUpper(Map data){
        print("hitung");
        double _valueUpperLimit = double.parse(this._controllerUpperLimitRate.text = data['PARA_UPPER_LIMIT'].toString());
        double _valueLowerLimit = double.parse(this._controllerLowerLimitRate.text = data['PARA_LOWER_LIMIT'].toString());
        double _valueCoverage = double.parse(this._controllerCoverageValue.text.replaceAll(",", ""));
        double _resultValueUpperLimit = _valueUpperLimit * _valueCoverage / 100;
        double _resultValueLowerLimit = _valueLowerLimit * _valueCoverage / 100;
        this._controllerUpperLimit.text = this._formatCurrency.formatCurrency(_resultValueUpperLimit.round().toString());
        this._controllerLowerLimit.text = this._formatCurrency.formatCurrency(_resultValueLowerLimit.round().toString());
        this._controllerPriceCredit.text = this._formatCurrency.formatCurrency(_resultValueLowerLimit.round().toString());
    }

    // void searchCoverageType(BuildContext context) async {
    //     Map data = await Navigator.push(context, MaterialPageRoute(
    //             builder: (context) => ChangeNotifierProvider(
    //                 create: (context) => SearchCoverageTypeChangeNotifier(),
    //                 child: SearchCoverageType(coverage1: this._coverage1Selected,coverage2: this._coverage2Selected,periodType: this._controllerPeriodType.text,product: this._productSelected,type: this._typeSelected))));
    //     if (data != null) {
    //         // this._sourceOrderNameSelected = data;
    //         this._controllerCoverageType.text = "${data['']} - ${data['']}";
    //         notifyListeners();
    //     } else {
    //         return;
    //     }
    // }

    Future<void> setPreference() async {
        SharedPreferences _preference = await SharedPreferences.getInstance();
        this._custType = _preference.getString("cust_type");
        this._lastKnownState = _preference.getString("last_known_state");
        this._disableJenisPenawaran = false;
        if(_preference.getString("jenis_penawaran") == "002"){
          this._disableJenisPenawaran = true;
        }
        if(this._lastKnownState == "AOS" || this._lastKnownState == "CONA" || this._lastKnownState == "IA" || this._lastKnownState == "PAC") {
          this._isDisablePACIAAOSCONA = true;
        }
    }

    void calculateTotalCost() {
        if(_controllerUpperLimitRate.text.isNotEmpty && _controllerLowerLimitRate.text.isNotEmpty){
            var _coverageValue = _controllerCoverageValue.text.isNotEmpty ? double.parse(_controllerCoverageValue.text.replaceAll(",", "")) : 0;
            var _cashCost = _controllerPriceCash.text.isNotEmpty ? double.parse(_controllerPriceCash.text.replaceAll(",", "")) : 0;
            var _creditCost = _controllerPriceCredit.text.isNotEmpty ? double.parse(_controllerPriceCredit.text.replaceAll(",", "")) : 0;
            var _upperLimit = _controllerUpperLimit.text.isNotEmpty ? double.parse(_controllerUpperLimit.text.replaceAll(",", "")) : 0;
            var _lowerLimit = _controllerLowerLimit.text.isNotEmpty ? double.parse(_controllerLowerLimit.text.replaceAll(",", "")) : 0;
            var _totalCost = _cashCost + _creditCost;
            if(_controllerPriceCash.text.isEmpty || _controllerPriceCredit.text.isEmpty){
                if(_totalCost > _upperLimit){
                    showSnackBar("Total biaya harus dibawah batas atas");
                    this._controllerPriceCash.clear();
                    this._controllerPriceCredit.clear();
                    // this._controllerTotalPriceSplit.clear();
                    this._controllerTotalPriceRate.clear();
                    this._controllerTotalPrice.clear();
                } else {
                    this._controllerTotalPrice.text = _formatCurrency.formatCurrency(_totalCost.toString());
                    // var _totalSplit = _cashCost - _creditCost;
                    this._controllerTotalPriceSplit.text = "0.00";
                    var _totalCostRate = _totalCost / (_coverageValue/100);
                    this._controllerTotalPriceRate.text = _formatCurrency.formatCurrency(_totalCostRate.toString());
                }
            }
            else if(_totalCost != 0) {
                if(_totalCost <= _upperLimit && _totalCost >= _lowerLimit) {
                    this._controllerTotalPrice.text = _formatCurrency.formatCurrency(_totalCost.toString());
                    // var _totalSplit = _cashCost - _creditCost;
                    this._controllerTotalPriceSplit.text = "0.00";
                    var _totalCostRate = _totalCost / (_coverageValue/100);
                    this._controllerTotalPriceRate.text = _formatCurrency.formatCurrency(_totalCostRate.toString());
                } else {
                    showSnackBar("Total biaya harus diantara batas atas dan bawah");
                    this._controllerPriceCash.clear();
                    this._controllerPriceCredit.clear();
                    // this._controllerTotalPriceSplit.clear();
                    this._controllerTotalPriceRate.clear();
                    this._controllerTotalPrice.clear();
                }
            }
            else {
                showSnackBar("Harap isi Biaya Tunai dan Biaya Kredit");
                this._controllerPriceCash.clear();
                this._controllerPriceCredit.clear();
                // this._controllerTotalPriceSplit.clear();
                this._controllerTotalPriceRate.clear();
                this._controllerTotalPrice.clear();
            }
        }
        notifyListeners();
    }

    SetMajorInsuranceModel _showMandatoryInfoAsuransiUtamaIdeModel;
    SetMajorInsuranceModel _showMandatoryInfoAsuransiUtamaIdeCompanyModel;
    void showMandatoryIdeModel(BuildContext context){
        _showMandatoryInfoAsuransiUtamaIdeModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAsuransiUtamaIdeModel;
        _showMandatoryInfoAsuransiUtamaIdeCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAsuransiUtamaIdeCompanyModel;
    }

    SetMajorInsuranceModel _showMandatoryInfoAsuransiUtamaRegulerSurveyModel;
    SetMajorInsuranceModel _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel;
    void showMandatoryRegulerSurveyModel(BuildContext context){
        _showMandatoryInfoAsuransiUtamaRegulerSurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAsuransiUtamaRegulerSurveyModel;
        _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel;
    }

    SetMajorInsuranceModel _showMandatoryInfoAsuransiUtamaPacModel;
    SetMajorInsuranceModel _showMandatoryInfoAsuransiUtamaPacCompanyModel;
    void showMandatoryPacModel(BuildContext context){
        _showMandatoryInfoAsuransiUtamaPacModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAsuransiUtamaPacModel;
        _showMandatoryInfoAsuransiUtamaPacCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAsuransiUtamaPacCompanyModel;
    }

    SetMajorInsuranceModel _showMandatoryInfoAsuransiUtamaResurveyModel;
    SetMajorInsuranceModel _showMandatoryInfoAsuransiUtamaResurveyCompanyModel;
    void showMandatoryResurveyModel(BuildContext context){
        _showMandatoryInfoAsuransiUtamaResurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAsuransiUtamaResurveyModel;
        _showMandatoryInfoAsuransiUtamaResurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAsuransiUtamaResurveyCompanyModel;
    }

    SetMajorInsuranceModel _showMandatoryInfoAsuransiUtamaDakorModel;
    SetMajorInsuranceModel _showMandatoryInfoAsuransiUtamaDakorCompanyModel;
    void showMandatoryDakorModel(BuildContext context){
        _showMandatoryInfoAsuransiUtamaDakorModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAsuransiUtamaDakorModel;
        _showMandatoryInfoAsuransiUtamaDakorCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAsuransiUtamaDakorCompanyModel;
    }

    void getDataFromDashboard(BuildContext context){
        showMandatoryIdeModel(context);
        showMandatoryRegulerSurveyModel(context);
        showMandatoryPacModel(context);
        showMandatoryResurveyModel(context);
        showMandatoryDakorModel(context);
    }

    bool isInsuranceTypeVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.insuranceTypeVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.insuranceTypeVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.insuranceTypeVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.insuranceTypeVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.insuranceTypeVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.insuranceTypeVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.insuranceTypeVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.insuranceTypeVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.insuranceTypeVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.insuranceTypeVisible;
            }
        }
        return value;
    }
    bool isInstallmentNoVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.installmentNoVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.installmentNoVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.installmentNoVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.installmentNoVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.installmentNoVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.installmentNoVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.installmentNoVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.installmentNoVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.installmentNoVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.installmentNoVisible;
            }
        }
        return value;
    }
    bool isCompanyVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.companyVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.companyVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.companyVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.companyVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.companyVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.companyVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.companyVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.companyVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.companyVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.companyVisible;
            }
        }
        return value;
    }
    bool isProductVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.productVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.productVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.productVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.productVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.productVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.productVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.productVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.productVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.productVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.productVisible;
            }
        }
        return value;
    }
    bool isPeriodVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.periodVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.periodVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.periodVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.periodVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.periodVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.periodVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.periodVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.periodVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.periodVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.periodVisible;
            }
        }
        return value;
    }
    bool isTypeVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.typeVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.typeVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.typeVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.typeVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.typeVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.typeVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.typeVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.typeVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.typeVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.typeVisible;
            }
        }
        return value;
    }
    bool isCoverage1Visible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.coverage1Visible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.coverage1Visible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.coverage1Visible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.coverage1Visible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.coverage1Visible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.coverage1Visible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.coverage1Visible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.coverage1Visible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.coverage1Visible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.coverage1Visible;
            }
        }
        return value;
    }
    bool isCoverage2Visible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.coverage2Visible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.coverage2Visible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.coverage2Visible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.coverage2Visible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.coverage2Visible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.coverage2Visible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.coverage2Visible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.coverage2Visible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.coverage2Visible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.coverage2Visible;
            }
        }
        return value;
    }
    bool isSubTypeVisible() {
        bool value = true;
        return value;
    }
    bool isCoverageTypeVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.coverageTypeVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.coverageTypeVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.coverageTypeVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.coverageTypeVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.coverageTypeVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.coverageTypeVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.coverageTypeVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.coverageTypeVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.coverageTypeVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.coverageTypeVisible;
            }
        }
        return value;
    }
    bool isCoverageValueVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.coverageValueVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.coverageValueVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.coverageValueVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.coverageValueVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.coverageValueVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.coverageValueVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.coverageValueVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.coverageValueVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.coverageValueVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.coverageValueVisible;
            }
        }
        return value;
    }
    bool isUpperLimitRateVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.upperLimitRateVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.upperLimitRateVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.upperLimitRateVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.upperLimitRateVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.upperLimitRateVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.upperLimitRateVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.upperLimitRateVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.upperLimitRateVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.upperLimitRateVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.upperLimitRateVisible;
            }
        }
        return value;
    }
    bool isUpperLimitValueVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.upperLimitValueVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.upperLimitValueVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.upperLimitValueVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.upperLimitValueVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.upperLimitValueVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.upperLimitValueVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.upperLimitValueVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.upperLimitValueVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.upperLimitValueVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.upperLimitValueVisible;
            }
        }
        return value;
    }
    bool isLowerLimitRateVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.lowerLimitRateVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.lowerLimitRateVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.lowerLimitRateVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.lowerLimitRateVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.lowerLimitRateVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.lowerLimitRateVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.lowerLimitRateVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.lowerLimitRateVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.lowerLimitRateVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.lowerLimitRateVisible;
            }
        }
        return value;
    }
    bool isLowerLimitValueVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.lowerLimitValueVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.lowerLimitValueVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.lowerLimitValueVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.lowerLimitValueVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.lowerLimitValueVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.lowerLimitValueVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.lowerLimitValueVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.lowerLimitValueVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.lowerLimitValueVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.lowerLimitValueVisible;
            }
        }
        return value;
    }
    bool isCashVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.cashVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.cashVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.cashVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.cashVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.cashVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.cashVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.cashVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.cashVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.cashVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.cashVisible;
            }
        }
        return value;
    }
    bool isCreditVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.creditVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.creditVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.creditVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.creditVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.creditVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.creditVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.creditVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.creditVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.creditVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.creditVisible;
            }
        }
        return value;
    }
    bool isTotalPriceSplitVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.totalPriceSplitVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.totalPriceSplitVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.totalPriceSplitVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.totalPriceSplitVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.totalPriceSplitVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.totalPriceSplitVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.totalPriceSplitVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.totalPriceSplitVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.totalPriceSplitVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.totalPriceSplitVisible;
            }
        }
        return value;
    }
    bool isTotalPriceRateVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.totalPriceRateVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.totalPriceRateVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.totalPriceRateVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.totalPriceRateVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.totalPriceRateVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.totalPriceRateVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.totalPriceRateVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.totalPriceRateVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.totalPriceRateVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.totalPriceRateVisible;
            }
        }
        return value;
    }
    bool isTotalPriceVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.totalPriceVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.totalPriceVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.totalPriceVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.totalPriceVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.totalPriceVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.totalPriceVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.totalPriceVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.totalPriceVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.totalPriceVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.totalPriceVisible;
            }
        }
        return value;
    }

    bool isInsuranceTypeMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.insuranceTypeMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.insuranceTypeMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.insuranceTypeMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.insuranceTypeMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.insuranceTypeMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.insuranceTypeMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.insuranceTypeMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.insuranceTypeMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.insuranceTypeMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.insuranceTypeMandatory;
            }
        }
        return value;
    }
    bool isInstallmentNoMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.installmentNoMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.installmentNoMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.installmentNoMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.installmentNoMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.installmentNoMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.installmentNoMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.installmentNoMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.installmentNoMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.installmentNoMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.installmentNoMandatory;
            }
        }
        return value;
    }
    bool isCompanyMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.companyMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.companyMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.companyMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.companyMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.companyMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.companyMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.companyMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.companyMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.companyMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.companyMandatory;
            }
        }
        return value;
    }
    bool isProductMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.productMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.productMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.productMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.productMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.productMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.productMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.productMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.productMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.productMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.productMandatory;
            }
        }
        return value;
    }
    bool isPeriodMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.periodMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.periodMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.periodMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.periodMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.periodMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.periodMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.periodMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.periodMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.periodMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.periodMandatory;
            }
        }
        return value;
    }
    bool isTypeMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.typeMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.typeMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.typeMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.typeMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.typeMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.typeMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.typeMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.typeMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.typeMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.typeMandatory;
            }
        }
        return value;
    }
    bool isCoverage1Mandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.coverage1Mandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.coverage1Mandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.coverage1Mandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.coverage1Mandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.coverage1Mandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.coverage1Mandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.coverage1Mandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.coverage1Mandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.coverage1Mandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.coverage1Mandatory;
            }
        }
        return value;
    }
    bool isCoverage2Mandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.coverage2Mandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.coverage2Mandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.coverage2Mandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.coverage2Mandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.coverage2Mandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.coverage2Mandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.coverage2Mandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.coverage2Mandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.coverage2Mandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.coverage2Mandatory;
            }
        }
        return value;
    }
    bool isCoverageTypeMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.coverageTypeMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.coverageTypeMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.coverageTypeMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.coverageTypeMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.coverageTypeMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.coverageTypeMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.coverageTypeMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.coverageTypeMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.coverageTypeMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.coverageTypeMandatory;
            }
        }
        return value;
    }
    bool isCoverageValueMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.coverageValueMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.coverageValueMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.coverageValueMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.coverageValueMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.coverageValueMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.coverageValueMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.coverageValueMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.coverageValueMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.coverageValueMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.coverageValueMandatory;
            }
        }
        return value;
    }
    bool isUpperLimitRateMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.upperLimitRateMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.upperLimitRateMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.upperLimitRateMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.upperLimitRateMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.upperLimitRateMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.upperLimitRateMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.upperLimitRateMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.upperLimitRateMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.upperLimitRateMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.upperLimitRateMandatory;
            }
        }
        return value;
    }
    bool isUpperLimitValueMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.upperLimitValueMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.upperLimitValueMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.upperLimitValueMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.upperLimitValueMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.upperLimitValueMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.upperLimitValueMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.upperLimitValueMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.upperLimitValueMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.upperLimitValueMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.upperLimitValueMandatory;
            }
        }
        return value;
    }
    bool isLowerLimitRateMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.lowerLimitRateMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.lowerLimitRateMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.lowerLimitRateMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.lowerLimitRateMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.lowerLimitRateMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.lowerLimitRateMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.lowerLimitRateMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.lowerLimitRateMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.lowerLimitRateMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.lowerLimitRateMandatory;
            }
        }
        return value;
    }
    bool isLowerLimitValueMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.lowerLimitValueMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.lowerLimitValueMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.lowerLimitValueMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.lowerLimitValueMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.lowerLimitValueMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.lowerLimitValueMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.lowerLimitValueMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.lowerLimitValueMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.lowerLimitValueMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.lowerLimitValueMandatory;
            }
        }
        return value;
    }
    bool isCashMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.cashMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.cashMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.cashMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.cashMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.cashMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.cashMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.cashMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.cashMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.cashMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.cashMandatory;
            }
        }
        return value;
    }
    bool isCreditMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.creditMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.creditMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.creditMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.creditMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.creditMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.creditMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.creditMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.creditMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.creditMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.creditMandatory;
            }
        }
        return value;
    }
    bool isTotalPriceSplitMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.totalPriceSplitMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.totalPriceSplitMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.totalPriceSplitMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.totalPriceSplitMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.totalPriceSplitMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.totalPriceSplitMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.totalPriceSplitMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.totalPriceSplitMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.totalPriceSplitMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.totalPriceSplitMandatory;
            }
        }
        return value;
    }
    bool isTotalPriceRateMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.totalPriceRateMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.totalPriceRateMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.totalPriceRateMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.totalPriceRateMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.totalPriceRateMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.totalPriceRateMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.totalPriceRateMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.totalPriceRateMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.totalPriceRateMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.totalPriceRateMandatory;
            }
        }
        return value;
    }
    bool isTotalPriceMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeModel.totalPriceMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyModel.totalPriceMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacModel.totalPriceMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyModel.totalPriceMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorModel.totalPriceMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoAsuransiUtamaIdeCompanyModel.totalPriceMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoAsuransiUtamaRegulerSurveyCompanyModel.totalPriceMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoAsuransiUtamaPacCompanyModel.totalPriceMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoAsuransiUtamaResurveyCompanyModel.totalPriceMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoAsuransiUtamaDakorCompanyModel.totalPriceMandatory;
            }
        }
        return value;
    }

    void checkDataDakor(int index) async{
        var _check = await _dbHelper.selectMS2ApplInsuranceMain();
        if(_check.isNotEmpty && _lastKnownState == "DKR"){
          print("dakor asuransi utama perluasan");
          insuranceTypeDakor = this._insuranceTypeSelected.KODE.toString() != _check[index]['insuran_type'].toString() || _check[index]['edit_insuran_type_parent'] == "1";
          installmentNoDakor = this._numberOfCollateral.toString() != _check[index]['insuran_index'].toString() || _check[index]['edit_insuran_index'] == "1";
          companyDakor = this._companySelected.KODE != _check[index]['company_id'] || _check[index]['edit_company_id'] == "1";
          productDakor = this._productSelected.KODE != _check[index]['product'] || _check[index]['edit_product'] == "1";
          periodDakor = this._controllerPeriodType.text.toString() != _check[index]['period'].toString() || _check[index]['edit_period'] == "1";
          typeDakor = this._typeSelected.toString() != _check[index]['type'].toString() || _check[index]['edit_type'] == "1";
          coverage1Dakor = this._coverage1Selected.KODE != _check[index]['type_1'] || _check[index]['edit_type_1'] == "1";
          coverage2Dakor = this._coverage2Selected.KODE != _check[index]['type_2'] || _check[index]['edit_type_2'] == "1";
          subTypeDakor = this._subTypeSelected.KODE != _check[index]['sub_type'] || _check[index]['edit_sub_type'] == "1";
          coverageTypeDakor = this._coverageTypeSelected != null ? this._coverageTypeSelected.KODE != _check[index]['coverage_type'].toString() : false || _check[index]['edit_coverage_type'] == "1";
          coverageValueDakor = double.parse(this._controllerCoverageValue.text.replaceAll(",", "")).toString().split(".")[0] != _check[index]['coverage_amt'].toString() || _check[index]['edit_coverage_amt'] == "1";
          upperLimitRateDakor =  this._controllerUpperLimitRate.text.toString() != _check[index]['upper_limit_pct'].toString() || _check[index]['edit_upper_limit_pct'] == "1";
          upperLimitValueDakor = double.parse(this._controllerUpperLimit.text.replaceAll(",", "")).toString().split(".")[0] != _check[index]['upper_limit_amt'].toString() || _check[index]['edit_upper_limit_amt'] == "1";
          lowerLimitRateDakor = this._controllerLowerLimitRate.text.toString() != _check[index]['lower_limit_pct'].toString() || _check[index]['edit_lower_limit_pct'] == "1";
          lowerLimitValueDakor = double.parse(this._controllerLowerLimit.text.replaceAll(",", "")).toString().split(".")[0] != _check[index]['lower_limit_amt'].toString() || _check[index]['edit_lower_limit_amt'] == "1";
          cashDakor = double.parse(this._controllerPriceCash.text.replaceAll(",", "")).toString().split(".")[0] != _check[index]['cash_amt'].toString() || _check[index]['edit_cash_amt'] == "1";
          creditDakor = double.parse(this._controllerPriceCredit.text.replaceAll(",", "")).toString().split(".")[0] != _check[index]['credit_amt'].toString() || _check[index]['edit_credit_amt'] == "1";
          totalPriceSplitDakor = false; //this._controllerTotalPriceSplit.text != _check[index]['total_split'] || _check[index]['edit_total_split'] == "1";
          totalPriceRateDakor = this._controllerTotalPriceRate.text != _check[index]['total_pct'].toString() || _check[index]['edit_total_pct'] == "1";
          totalPriceDakor = double.parse(this._controllerTotalPrice.text.replaceAll(",", "")).toString().split(".")[0] != _check[index]['total_amt'].toString() || _check[index]['edit_total_amt'] == "1";

          print("1 = ${double.parse(this._controllerTotalPrice.text.replaceAll(",", "")).toString().split(".")[0]}");
          print("2 = ${_check[index]['total_amt']}");
          print("3 = $installmentNoDakor");

          if(insuranceTypeDakor || installmentNoDakor || companyDakor || productDakor || periodDakor || typeDakor || coverage1Dakor ||
              coverage2Dakor || subTypeDakor || coverageTypeDakor || coverageValueDakor || upperLimitRateDakor || upperLimitValueDakor || lowerLimitRateDakor ||
              lowerLimitValueDakor || cashDakor || creditDakor || totalPriceSplitDakor || totalPriceRateDakor || totalPriceDakor){
              isEdit = true;
          } else {
              isEdit = false;
          }
        }
        notifyListeners();
    }

    bool get totalPriceDakor => _totalPriceDakor;

    set totalPriceDakor(bool value) {
      _totalPriceDakor = value;
      notifyListeners();
    }

    bool get totalPriceRateDakor => _totalPriceRateDakor;

    set totalPriceRateDakor(bool value) {
      _totalPriceRateDakor = value;
      notifyListeners();
    }

    bool get totalPriceSplitDakor => _totalPriceSplitDakor;

    set totalPriceSplitDakor(bool value) {
      _totalPriceSplitDakor = value;
      notifyListeners();
    }

    bool get creditDakor => _creditDakor;

    set creditDakor(bool value) {
      _creditDakor = value;
      notifyListeners();
    }

    bool get cashDakor => _cashDakor;

    set cashDakor(bool value) {
      _cashDakor = value;
      notifyListeners();
    }

    bool get lowerLimitValueDakor => _lowerLimitValueDakor;

    set lowerLimitValueDakor(bool value) {
      _lowerLimitValueDakor = value;
      notifyListeners();
    }

    bool get lowerLimitRateDakor => _lowerLimitRateDakor;

    set lowerLimitRateDakor(bool value) {
      _lowerLimitRateDakor = value;
      notifyListeners();
    }

    bool get upperLimitValueDakor => _upperLimitValueDakor;

    set upperLimitValueDakor(bool value) {
      _upperLimitValueDakor = value;
      notifyListeners();
    }

    bool get upperLimitRateDakor => _upperLimitRateDakor;

    set upperLimitRateDakor(bool value) {
      _upperLimitRateDakor = value;
      notifyListeners();
    }

    bool get coverageValueDakor => _coverageValueDakor;

    set coverageValueDakor(bool value) {
      _coverageValueDakor = value;
      notifyListeners();
    }

    bool get coverageTypeDakor => _coverageTypeDakor;

    set coverageTypeDakor(bool value) {
      _coverageTypeDakor = value;
      notifyListeners();
    }

    bool get coverage2Dakor => _coverage2Dakor;

    set coverage2Dakor(bool value) {
      _coverage2Dakor = value;
      notifyListeners();
    }

    bool get subTypeDakor => _subTypeDakor;

    set subTypeDakor(bool value) {
        _subTypeDakor = value;
        notifyListeners();
    }

    bool get coverage1Dakor => _coverage1Dakor;

    set coverage1Dakor(bool value) {
      _coverage1Dakor = value;
      notifyListeners();
    }

    bool get typeDakor => _typeDakor;

    set typeDakor(bool value) {
      _typeDakor = value;
      notifyListeners();
    }

    bool get periodDakor => _periodDakor;

    set periodDakor(bool value) {
      _periodDakor = value;
      notifyListeners();
    }

    bool get productDakor => _productDakor;

    set productDakor(bool value) {
      _productDakor = value;
      notifyListeners();
    }

    bool get companyDakor => _companyDakor;

    set companyDakor(bool value) {
      _companyDakor = value;
      notifyListeners();
    }

    bool get installmentNoDakor => _installmentNoDakor;

    set installmentNoDakor(bool value) {
      _installmentNoDakor = value;
      notifyListeners();
    }

    bool get insuranceTypeDakor => _insuranceTypeDakor;

    set insuranceTypeDakor(bool value) {
      _insuranceTypeDakor = value;
      notifyListeners();
    }

    bool get isEdit => _isEdit;

    set isEdit(bool value) {
      _isEdit = value;
      notifyListeners();
    }

    void calculateUpperLowerLimit() {
      if(_flagDisableNilaiPertanggungan == false) {
        double _valueUpperLimit = double.parse(this._controllerUpperLimitRate.text.toString());
        double _valueLowerLimit = double.parse(this._controllerLowerLimitRate.text.toString());
        double _valueCoverage = double.parse(this._controllerCoverageValue.text.replaceAll(",", ""));
        double _resultValueUpperLimit = _valueUpperLimit * _valueCoverage / 100;
        double _resultValueLowerLimit = _valueLowerLimit * _valueCoverage / 100;
        this._controllerUpperLimit.text = this._formatCurrency.formatCurrency(_resultValueUpperLimit.round().toString());
        this._controllerLowerLimit.text = this._formatCurrency.formatCurrency(_resultValueLowerLimit.round().toString());
      }
    }

    void showSnackBar(String text){
        this._scaffoldKey.currentState.showSnackBar(new SnackBar(
            content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor,duration: Duration(seconds: 5),));
    }

    void showSnackBarInfo(String text){
        this._scaffoldKey.currentState.showSnackBar(new SnackBar(
            content: Text("$text",style: TextStyle(color: Colors.black)), behavior: SnackBarBehavior.floating, backgroundColor: Colors.yellow,duration: Duration(seconds: 5),));
    }

  Future<void> getBatasAtasBawahUtamaUpdated(BuildContext context, MajorInsuranceModel dataInsurance, int index) async {
    var _providerPhoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
    var _providerInfoAppl = Provider.of<InfoAppChangeNotifier>(context,listen: false);
    var _providerInfoUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    var _providerInfoCollateral = Provider.of<InformationCollateralChangeNotifier>(context,listen: false);
    var _providerInfCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false);
    SharedPreferences _preference = await SharedPreferences.getInstance();
    this._loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);

    var _body = jsonEncode(
        {
          "P_APPLICATION_DATE": _providerInfoAppl.controllerOrderDate.text,
          "P_INSR_COMPANY_ID": dataInsurance.company.KODE,
          "P_INSR_PRODUCT_ID": dataInsurance.product.KODE,
          "P_INSR_COV_1": dataInsurance.coverage1.KODE,
          "P_INSR_COV_2": dataInsurance.coverage2.KODE,
          "P_INSR_FIN_TYPE_ID": _preference.getString("cust_type") != "COM" ? "${_providerPhoto.typeOfFinancingModelSelected.financingTypeId}" : "${_providerInfoUnitObject.typeOfFinancingModelSelected.financingTypeId}",
          "P_INSR_JENIS": dataInsurance.type.toString(),
          "P_INSR_MAX_OTR": _providerInfCreditStructure.controllerObjectPrice.text.isEmpty ? 0 : "${double.parse(_providerInfCreditStructure.controllerObjectPrice.text.replaceAll(",", "")).round()}",
          "P_INSR_OBJT_PURPOSE_ID": "${_providerInfoCollateral.collateralUsageOtoSelected.id}",
          "P_INSR_OBJT_TYPE_ID": "${_providerInfoCollateral.objectTypeSelected.id}",
          "P_INSR_PERIOD": dataInsurance.periodType.toString(),
          "P_PARA_CP_CENTRA_ID": "${_preference.getString("SentraD")}",
          "P_PROD_TYPE_ID": "${_providerInfoUnitObject.productTypeSelected.id}",
        }
    );
    print("body batas atas $_body");

    try{
      String _batasAtasBawahUtama = await storage.read(key: "BatasAtasBawahUtama");
      final _response = await _http.post(
          "${BaseUrl.urlGeneral}$_batasAtasBawahUtama",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));
      final _result = jsonDecode(_response.body);
      print("result batas atas = $_result");
      List _data = _result['data'];
      if(_data.isNotEmpty){
        if(_data[0]['PARA_LOWER_LIMIT'].toString() == "0" && _data[0]['PARA_UPPER_LIMIT'].toString() == "0"){
          showSnackBar("Mappingan untuk rate asuransi belum ada");
        }
        else {
          dataInsurance.coverageType = CoverageTypeModel(_data[0]['PARA_INSR_SOURCE_TYPE_ID'], _data[0]['PARA_INSR_SOURCE_TYPE_NAME']);
          getNilaiPertanggunganUpdated(context, _data[0], dataInsurance, index);
        }
      }
      else{
        showSnackBar("Mappingan untuk rate asuransi belum ada");
      }
      this._loadData = false;
    }
    catch(e){
      this._loadData = false;
      showSnackBar("error BatasAtasBawahUtama ${e.toString()}");
    }
    notifyListeners();
  }

  Future<void> getBatasAtasBawahPerluasanUpdated(BuildContext context, MajorInsuranceModel dataInsurance, int index) async {
    var _providerPhoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
    var _providerInfoAppl = Provider.of<InfoAppChangeNotifier>(context,listen: false);
    var _providerInfoUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    SharedPreferences _preference = await SharedPreferences.getInstance();
    this._loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);

    var _body = jsonEncode(
        {
          "P_APPLICATION_DATE": _providerInfoAppl.controllerOrderDate.text,
          "P_FIN_TYPE_ID": _preference.getString("cust_type") != "COM" ? "${_providerPhoto.typeOfFinancingModelSelected.financingTypeId}":"${_providerInfoUnitObject.typeOfFinancingModelSelected.financingTypeId}",
          "P_INSR_COMPANY_ID": dataInsurance.company.KODE,
          "P_INSR_PRODUCT_ID": dataInsurance.product.KODE,
          "P_INSR_SUBTYPE_ID": dataInsurance.subType.KODE,
          "P_INSR_TYPE_ID": dataInsurance.coverage1.KODE,
          "P_PARA_CP_CENTRA_ID": "${_preference.getString("SentraD")}",
        }
    );
    print("body batas atas perluasan $_body");
    var _providerParentIndividu = Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false);
    DateTime _timeStartValidate = DateTime.now();
    _providerParentIndividu.insertLog(context, _timeStartValidate, DateTime.now(), _body, "Re-Calculate Batas Atas dan Batas Bawah Utama/Perluasan", "Re-Calculate Batas Atas dan Batas Bawah Utama/Perluasan");

    try{
      String _batasAtasBawahPerluasan = await storage.read(key: "BatasAtasBawahPerluasan");
      final _response = await _http.post(
          "${BaseUrl.urlGeneral}$_batasAtasBawahPerluasan",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));
      final _result = jsonDecode(_response.body);
      print("result batas atas = $_result");
      List _data = _result['data'];
      if(_data.isNotEmpty){
        if(_data[0]['PARA_LOWER_LIMIT'].toString() == "0" && _data[0]['PARA_UPPER_LIMIT'].toString() == "0"){
          showSnackBar("Mappingan untuk rate asuransi belum ada");
        }
        else {
          dataInsurance.coverageType = CoverageTypeModel(_data[0]['PARA_LIMIT'].toString(), _data[0]['PARA_LIMIT_DESC'].toString());
          getNilaiPertanggunganUpdated(context, _data[0], dataInsurance, index);
        }
      }
      else {
        showSnackBar("Mappingan untuk rate asuransi belum ada");
      }
      this._loadData = false;
    }
    catch(e){
      this._loadData = false;
      showSnackBar("error BatasAtasBawahPerluasan ${e.toString()}");
    }
    notifyListeners();
  }

  Future<void> getNilaiPertanggunganUpdated(BuildContext context, Map data, MajorInsuranceModel dataInsurance, int index) async {
    var _providerInfoUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    var _providerInfoCollateral = Provider.of<InformationCollateralChangeNotifier>(context,listen: false);
    var _providerInfCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false);
    var _providerAsuransiUtama = Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false);
    var _providerAsuransiTambahan = Provider.of<InfoAdditionalInsuranceChangeNotifier>(context, listen: false);
    int _allCredit = 0;
    int _creditInsrLife = 0;
    //fee credit
    for(int i=0; i<_providerInfCreditStructure.listInfoFeeCreditStructure.length; i++){
      if(_providerInfCreditStructure.listInfoFeeCreditStructure[i].feeTypeModel.id == "01" || _providerInfCreditStructure.listInfoFeeCreditStructure[i].feeTypeModel.id == "02"){
        _allCredit += double.parse(_providerInfCreditStructure.listInfoFeeCreditStructure[i].creditCost.replaceAll(",", "")).toInt();
      }
    }
    //asuransi utama, perluasan
    for(int i=0; i<_providerAsuransiUtama.listFormMajorInsurance.length; i++){
      _allCredit += double.parse(_providerAsuransiUtama.listFormMajorInsurance[i].priceCredit.replaceAll(",", "")).toInt();
    }
    //asuransi tambahan
    for(int i=0; i<_providerAsuransiTambahan.listFormAdditionalInsurance.length; i++){
      if(_providerAsuransiTambahan.listFormAdditionalInsurance[i].product.KODE != "007")
        _allCredit += double.parse(_providerAsuransiTambahan.listFormAdditionalInsurance[i].priceCredit.replaceAll(",", "")).toInt();
    }

    for(int i=0; i<_providerAsuransiTambahan.listFormAdditionalInsurance.length; i++){
      if(_providerAsuransiTambahan.listFormAdditionalInsurance[i].product.KODE == "007")
        _creditInsrLife += double.parse(_providerAsuransiTambahan.listFormAdditionalInsurance[i].priceCredit.replaceAll(",", "")).toInt();
    }
    print("total all credit = $_allCredit");

    this._loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
    var _body = jsonEncode(
        {
          "all_Credit": _allCredit,
          "appraisal_Price": _providerInfoCollateral.controllerTaksasiPrice.text.isNotEmpty ? int.parse(_providerInfoCollateral.controllerTaksasiPrice.text.replaceAll(",", "").split(".")[0]) : 0,
          "building_Price": _providerInfoCollateral.controllerBuildingArea.text.isNotEmpty ? int.parse(_providerInfoCollateral.controllerBuildingArea.text.replaceAll(",", "").split(".")[0]) : 0,
          "colla_Same_Unit": _providerInfoCollateral.radioValueIsCollateralSameWithUnitOto == 0 ? false : true,
          "colla_Type_ID": _providerInfoCollateral.collateralTypeModel != null ? _providerInfoCollateral.collateralTypeModel.id : "",
          "credit_Insr_Life": dataInsurance.insuranceType != null && dataInsurance.coverageType != null ? dataInsurance.insuranceType.KODE == "1" && dataInsurance.coverageType.KODE == "007" ? 0 : _creditInsrLife : _creditInsrLife,
          "harga_Objek": int.parse(_providerInfCreditStructure.controllerObjectPrice.text.replaceAll(",", "").split(".")[0]),
          "jaminan_DP": _providerInfoCollateral.controllerDPJaminan.text.isNotEmpty ? int.parse(_providerInfoCollateral.controllerDPJaminan.text.replaceAll(",", "").split(".")[0]) : 0,
          "jenis_Pertanggungan": dataInsurance.coverageType.KODE,
          "jenis_Rehab": _providerInfoUnitObject.rehabTypeSelected != null ? _providerInfoUnitObject.rehabTypeSelected.id : "",
          "jumlah_Pinjaman": int.parse(_providerInfCreditStructure.controllerTotalLoan.text.replaceAll(",", "").split(".")[0]),
          "level_Asuransi": dataInsurance.insuranceType != null ? dataInsurance.insuranceType.KODE : "",
          "limit_Asuransi_Perluasan": dataInsurance.insuranceType.KODE != "02" ? dataInsurance.type : "",
          "nett_DP": int.parse(_providerInfCreditStructure.controllerNetDP.text.replaceAll(",", "").split(".")[0]),
          "showroom_Price": _providerInfoCollateral.collateralTypeModel != null ? _providerInfoCollateral.collateralTypeModel.id != "003" ? int.parse(_providerInfoCollateral.controllerHargaJualShowroom.text.replaceAll(",", "").split(".")[0]) : 0 : 0,
          "total_Price": int.parse(_providerInfCreditStructure.controllerTotalPrice.text.replaceAll(",", "").split(".")[0])
        }
    );
    print("body nilai pertanggunan = $_body");
    var _providerParentIndividu = Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false);
    DateTime _timeStartValidate = DateTime.now();
    _providerParentIndividu.insertLog(context, _timeStartValidate, DateTime.now(), _body, "Re-Calculate Nilai Pertanggungan Utama/Perluasan", "Re-Calculate Nilai Pertanggungan Utama/Perluasan");

    try{
      String _fieldNilaiPertanggungan = await storage.read(key: "FieldNilaiPertanggungan");
      print("${BaseUrl.urlGeneral}$_fieldNilaiPertanggungan");
      final _response = await _http.post(
          "${BaseUrl.urlGeneral}$_fieldNilaiPertanggungan",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));

      final _result = jsonDecode(_response.body);
      print("cek result nilai pertanggungan $_result");
      if(_response.statusCode == 200){
        if(_result['data']['new_id_jenis_pertanggungan'] != "" && _result['data']['new_desc_jenis_pertanggungan'] != ""){
          dataInsurance.coverageType = CoverageTypeModel(_result['data']['new_id_jenis_pertanggungan'].toString(), _result['data']['new_desc_jenis_pertanggungan'].toString());
        }
        if(dataInsurance.coverageValue.replaceAll(",", "").toString() != _result['data']['nilai_Pertanggungan'].toString()) {
          dataInsurance.coverageValue = formatCurrency.formatCurrency(_result['data']['nilai_Pertanggungan'].toString());
          countLimitLowerUpperUpdated(context, data, dataInsurance, index);
        }
      }
      else {
        this._controllerCoverageValue.text = "0.00";
      }
      this._loadData = false;
    }
    catch(e){
      this._loadData = false;
      showSnackBar("error FieldNilaiPertanggungan ${e.toString()}");
    }
    notifyListeners();
  }

  Future<void> countLimitLowerUpperUpdated(BuildContext context, Map data, MajorInsuranceModel dataInsurance, int index) {
    double _valueUpperLimit = double.parse(dataInsurance.upperLimitRate = data['PARA_UPPER_LIMIT'].toString());
    double _valueLowerLimit = double.parse(dataInsurance.lowerLimitRate = data['PARA_LOWER_LIMIT'].toString());
    double _valueCoverage = double.parse(dataInsurance.coverageValue.replaceAll(",", ""));
    double _resultValueUpperLimit = _valueUpperLimit * _valueCoverage / 100;
    double _resultValueLowerLimit = _valueLowerLimit * _valueCoverage / 100;
    dataInsurance.upperLimit = this._formatCurrency.formatCurrency(_resultValueUpperLimit.round().toString());
    dataInsurance.lowerLimit = this._formatCurrency.formatCurrency(_resultValueLowerLimit.round().toString());
    dataInsurance.priceCash = "0";
    dataInsurance.priceCredit = this._formatCurrency.formatCurrency(_resultValueLowerLimit.round().toString());
    var _coverageValue = dataInsurance.coverageValue.isNotEmpty ? double.parse(dataInsurance.coverageValue.replaceAll(",", "")) : 0;
    var _cashCost = dataInsurance.priceCash.isNotEmpty ? double.parse(dataInsurance.priceCash.replaceAll(",", "")) : 0;
    var _creditCost = dataInsurance.priceCredit.isNotEmpty ? double.parse(dataInsurance.priceCredit.replaceAll(",", "")) : 0;
    var _totalCost = _cashCost + _creditCost;
    dataInsurance.totalPrice = _formatCurrency.formatCurrency(_totalCost.toString());
    dataInsurance.totalPriceSplit = "0";
    var _totalCostRate = _totalCost / (_coverageValue/100);
    dataInsurance.totalPriceRate = _formatCurrency.formatCurrency(_totalCostRate.toString());
    checkUpdated(context, dataInsurance, index);
  }

  void checkUpdated(BuildContext context, MajorInsuranceModel dataInsurance, int index) {
    checkDataDakorUpdated(dataInsurance, index);
    Provider.of<InfoMajorInsuranceChangeNotifier>(context, listen: false)
        .updateListInfoMajorInsurance(MajorInsuranceModel(
          dataInsurance.insuranceType,
          dataInsurance.colaType,
          dataInsurance.numberOfColla,
          dataInsurance.company,
          dataInsurance.product,
          dataInsurance.periodType,
          dataInsurance.type,
          dataInsurance.coverage1,
          dataInsurance.coverage2,
          dataInsurance.subType,
          dataInsurance.coverageType,
          dataInsurance.coverageValue,
          dataInsurance.upperLimit,
          dataInsurance.lowerLimit,
          dataInsurance.upperLimitRate,
          dataInsurance.lowerLimitRate,
          dataInsurance.priceCash,
          dataInsurance.priceCredit,
          dataInsurance.totalPriceSplit,
          dataInsurance.totalPriceRate,
          dataInsurance.totalPrice,
          dataInsurance.isEdit,
          dataInsurance.insuranceTypeDakor,
          dataInsurance.installmentNoDakor,
          dataInsurance.companyDakor,
          dataInsurance.productDakor,
          dataInsurance.periodDakor,
          dataInsurance.typeDakor,
          dataInsurance.coverage1Dakor,
          dataInsurance.coverage2Dakor,
          dataInsurance.subTypeDakor,
          dataInsurance.coverageTypeDakor,
          dataInsurance.coverageValueDakor,
          dataInsurance.upperLimitRateDakor,
          dataInsurance.upperLimitValueDakor,
          dataInsurance.lowerLimitRateDakor,
          dataInsurance.lowerLimitValueDakor,
          dataInsurance.cashDakor,
          dataInsurance.creditDakor,
          dataInsurance.totalPriceSplitDakor,
          dataInsurance.totalPriceRateDakor,
          dataInsurance.totalPriceDakor,
          dataInsurance.orderProductInsuranceID
      ), context, index);
  }

  void checkDataDakorUpdated(MajorInsuranceModel dataInsurance, int index) async {
    var _check = await _dbHelper.selectMS2ApplInsuranceMain();
    if(_check.isNotEmpty && _lastKnownState == "DKR"){
      insuranceTypeDakor = dataInsurance.insuranceType.KODE != _check[index]['insuran_type'].toString() || _check[index]['edit_insuran_type_parent'] == "1";
      installmentNoDakor = dataInsurance.numberOfColla != _check[index]['insuran_index'].toString() || _check[index]['edit_insuran_index'] == "1";
      companyDakor = dataInsurance.company.KODE != _check[index]['company_id'] || _check[index]['edit_company_id'] == "1";
      productDakor = dataInsurance.product.KODE != _check[index]['product'] || _check[index]['edit_product'] == "1";
      periodDakor = dataInsurance.periodType != _check[index]['period'].toString() || _check[index]['edit_period'] == "1";
      typeDakor = dataInsurance.type != _check[index]['type'].toString() || _check[index]['edit_type'] == "1";
      coverage1Dakor = dataInsurance.coverage1.KODE != _check[index]['type_1'] || _check[index]['edit_type_1'] == "1";
      coverage2Dakor = dataInsurance.coverage2.KODE != _check[index]['type_2'] || _check[index]['edit_type_2'] == "1";
      subTypeDakor = dataInsurance.subType.KODE != _check[index]['sub_type'] || _check[index]['edit_sub_type'] == "1";
      coverageTypeDakor = dataInsurance.coverageType != null ? dataInsurance.coverageType.KODE != _check[index]['coverage_type'].toString() : false || _check[index]['edit_coverage_type'] == "1";
      coverageValueDakor = double.parse(dataInsurance.coverageValue.replaceAll(",", "")).toString().split(".")[0] != _check[index]['coverage_amt'].toString() || _check[index]['edit_coverage_amt'] == "1";
      upperLimitRateDakor =  dataInsurance.upperLimitRate != _check[index]['upper_limit_pct'].toString() || _check[index]['edit_upper_limit_pct'] == "1";
      upperLimitValueDakor = double.parse(dataInsurance.upperLimit.replaceAll(",", "")).toString().split(".")[0] != _check[index]['upper_limit_amt'].toString() || _check[index]['edit_upper_limit_amt'] == "1";
      lowerLimitRateDakor = dataInsurance.lowerLimitRate != _check[index]['lower_limit_pct'].toString() || _check[index]['edit_lower_limit_pct'] == "1";
      lowerLimitValueDakor = double.parse(dataInsurance.lowerLimit.replaceAll(",", "")).toString().split(".")[0] != _check[index]['lower_limit_amt'].toString() || _check[index]['edit_lower_limit_amt'] == "1";
      cashDakor = double.parse(dataInsurance.priceCash.replaceAll(",", "")).toString().split(".")[0] != _check[index]['cash_amt'].toString() || _check[index]['edit_cash_amt'] == "1";
      creditDakor = double.parse(dataInsurance.priceCredit.replaceAll(",", "")).toString().split(".")[0] != _check[index]['credit_amt'].toString() || _check[index]['edit_credit_amt'] == "1";
      totalPriceSplitDakor = false;
      totalPriceRateDakor = dataInsurance.totalPriceRate != _check[index]['total_pct'].toString() || _check[index]['edit_total_pct'] == "1";
      totalPriceDakor = double.parse(dataInsurance.totalPrice.replaceAll(",", "")).toString().split(".")[0] != _check[index]['total_amt'].toString() || _check[index]['edit_total_amt'] == "1";

      if(insuranceTypeDakor || installmentNoDakor || companyDakor || productDakor || periodDakor || typeDakor || coverage1Dakor ||
          coverage2Dakor || subTypeDakor || coverageTypeDakor || coverageValueDakor || upperLimitRateDakor || upperLimitValueDakor || lowerLimitRateDakor ||
          lowerLimitValueDakor || cashDakor || creditDakor || totalPriceSplitDakor || totalPriceRateDakor || totalPriceDakor){
        isEdit = true;
      } else {
        isEdit = false;
      }
    }
    notifyListeners();
  }
}