import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/employee_head_model.dart';
import 'package:ad1ms2_dev/models/employee_model.dart';
import 'package:ad1ms2_dev/models/infromation_sales_model.dart';
import 'package:ad1ms2_dev/models/position_model.dart';
import 'package:ad1ms2_dev/models/salesman_type_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_salesman_model.dart';
import 'package:ad1ms2_dev/screens/search_employee.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_salesman_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/dashboard/dashboard_change_notif.dart';
import 'package:ad1ms2_dev/shared/resource/get_set_vme.dart';
import 'package:ad1ms2_dev/shared/search_employee_change_notifier.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'information_object_unit_change_notifier.dart';

class AddSalesmanChangeNotifier with ChangeNotifier {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _autoValidate = false;
  bool _loadData = false;
  TextEditingController _controllerEmployee = TextEditingController();
  SalesmanTypeModel _salesmanTypeModelSelected;
  SalesmanTypeModel _salesmanTypeModelTemp;
  PositionModel _positionModelSelected;
  PositionModel _positionModeltemp;
  EmployeeHeadModel _employeeHeadModelSelected;
  EmployeeHeadModel _employeeHeadModelTemp;
  EmployeeModel _employeeModelSelected;
  String _employeeTemp;
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  List<SalesmanTypeModel> _listSalesmanType = []; //Sales().salesmanTypeList;
  List<PositionModel> _listPosition = [];
  String _custType;
  String _lastKnownState;
  DbHelper _dbHelper = DbHelper();
  String _groupAlias;
  String _orderProductSalesID;
  bool _isDisablePACIAAOSCONA = false;

  bool _isEdit = false;
  bool _isSalesmanTypeModelChanges = false;
  bool _isPositionModelChanges = false;
  bool _isEmployeeChanges = false;
  bool _isEmployeeHeadModelChanges = false;

  List<EmployeeHeadModel> _listEmployeeHead = [
    // EmployeeHeadModel("01", "ATASAN 1"),
    // EmployeeHeadModel("02", "ATASAN 2"),
    // EmployeeHeadModel("03", "ATASAN 3")
  ];

  String get groupAlias => _groupAlias;

  set groupAlias(String value) {
    this._groupAlias = value;
    notifyListeners();
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  String get custType => _custType;

  set custType(String value) {
    this._custType = value;
    notifyListeners();
  }

  String get lastKnownState => _lastKnownState;

  set lastKnownState(String value) {
    this._lastKnownState = value;
    notifyListeners();
  }

  SalesmanTypeModel get salesmanTypeModelSelected => _salesmanTypeModelSelected;

  TextEditingController get controllerEmployee => _controllerEmployee;

  set salesmanTypeModelSelected(SalesmanTypeModel value) {
    this._salesmanTypeModelSelected = value;
    this._listPosition.clear();
    this._positionModelSelected = null;
    this._employeeModelSelected = null;
    this._controllerEmployee.clear();
    this._employeeHeadModelSelected = null;
    this._listEmployeeHead.clear();
    notifyListeners();
  }

  PositionModel get positionModelSelected => _positionModelSelected;

  set positionModelSelected(PositionModel value) {
    this._positionModelSelected = value;
    this._employeeModelSelected = null;
    this._controllerEmployee.clear();
    this._employeeHeadModelSelected = null;
    this._listEmployeeHead.clear();
    notifyListeners();
  }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  EmployeeHeadModel get employeeHeadModelSelected => _employeeHeadModelSelected;

  set employeeHeadModelSelected(EmployeeHeadModel value) {
    this._employeeHeadModelSelected = value;
    notifyListeners();
  }

  GlobalKey<FormState> get key => _key;

  UnmodifiableListView<SalesmanTypeModel> get listSalesmanType {
    return UnmodifiableListView(this._listSalesmanType);
  }

  UnmodifiableListView<PositionModel> get listPosition {
    return UnmodifiableListView(this._listPosition);
  }

  UnmodifiableListView<EmployeeHeadModel> get listEmployeeHead {
    return UnmodifiableListView(this._listEmployeeHead);
  }

  String get orderProductSalesID => _orderProductSalesID;

  set orderProductSalesID(String value) {
    this._orderProductSalesID = value;
  }

  Future<void> getSalesType(BuildContext context) async{
    this._listSalesmanType.clear();
    this._salesmanTypeModelSelected = null;
    String _fieldTipeSales = await storage.read(key: "FieldTipeSales");
    try{
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);

      var _body = jsonEncode(
          {
            "P_FORM_TYPE": "IDE",//_preferences.getString("last_known_state"),
          }
      );
      final _response = await _http.post(
          "${BaseUrl.urlGeneral}$_fieldTipeSales",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      );
      final _result = jsonDecode(_response.body);
      final _data = _result['data'];
      if(_response.statusCode == 302 || _response.statusCode == 200){
        for(int i=0; i<_data.length; i++){
          this._listSalesmanType.add(SalesmanTypeModel(_data[i]['KODE'], _data[i]['DESKRIPSI']));
        }
      }
    } catch (e) {
      print(e.toString());
    }
    notifyListeners();
  }

  Future<void> getSalesPosition(BuildContext context) async{
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    this._listPosition.clear();
    this._positionModelSelected = null;
    this._controllerEmployee.clear();
    this._employeeModelSelected = null;
    this._employeeHeadModelSelected = null;
    this._listEmployeeHead.clear();
    String _fieldJabatan = await storage.read(key: this._salesmanTypeModelSelected.id == "001" ? "FieldJabatanInternal" : "FieldJabatanEksternal");
    var _providerUnitObject = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
    try{
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);

      var _body = jsonEncode(
          {
            "P_EMPL_JOB_MAYOR": _preferences.getString("is_mayor"),
            "P_EMPL_JOB_STATUS": "",
            "P_OBJECT_BRAND_ID": _providerUnitObject.brandObjectSelected.id,
            "P_PROD_MATRIX_ID": _providerUnitObject.prodMatrixId,
            "P_PROD_SALES_GROUP_ID": _providerUnitObject.groupSalesSelected.kode
          }
      );
      final _response = this._salesmanTypeModelSelected.id == "001" ?
      await _http.post(
          "${BaseUrl.urlGeneral}$_fieldJabatan",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      )
          :
      await _http.get(
          "${BaseUrl.urlGeneral}$_fieldJabatan",
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      );

      final _result = jsonDecode(_response.body);
      final _data = _result['data'];
      if(_response.statusCode == 302 || _response.statusCode == 200){
        for(int i=0; i<_data.length; i++){
          this._listPosition.add(PositionModel(_data[i]['KODE'], _data[i]['DESKRIPSI'].toString().trim()));
        }
      }
    } catch (e) {
      print(e.toString());
    }
    notifyListeners();
  }

  Future<void> getSalesHead(BuildContext context) async{
    this._listEmployeeHead.clear();
    this._employeeHeadModelSelected = null;
    String _fieldAtasan = await storage.read(key: this._salesmanTypeModelSelected.id == "001" ? "FieldAtasanInternal" : "FieldAtasanEksternal");
    try{
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);

      var _body = jsonEncode(
          this._salesmanTypeModelSelected.id == "001" ?
          {
            "NIK": this._employeeModelSelected.NIK,
          }
            :
          {
            "P_NIK": this._employeeModelSelected.NIK,
          }
      );
      print("body atasan = $_body");
      final _response = await _http.post(
          "${BaseUrl.urlGeneral}$_fieldAtasan",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      );
      final _result = jsonDecode(_response.body);
      final _data = _result['data'];
      print("result atasan = $_result");
      if(_data.isEmpty){
        showSnackBar("Data Atasan kosong");
      }
      if(_response.statusCode == 302 || _response.statusCode == 200){
        for(int i=0; i<_data.length; i++){
          this._listEmployeeHead.add(EmployeeHeadModel(_data[i]['NIKAtasan'], _data[i]['NamaAtasan'], null, null));
        }
      }
    } catch (e) {
      print(e.toString());
    }
    notifyListeners();
  }

  void searchEmployee(BuildContext context) async {
    if(this._positionModelSelected != null && this._salesmanTypeModelSelected != null){
      EmployeeModel _data = await Navigator.push(context, MaterialPageRoute(
          builder: (context) => ChangeNotifierProvider(
              create: (context) => SearchEmployeeChangeNotifier(),
              child: SearchEmployee(salesType: this._salesmanTypeModelSelected.id, positionId: this._positionModelSelected.id,  position: this._positionModelSelected.positionName))));
      if (_data != null) {
        this._employeeModelSelected = _data;
        this._controllerEmployee.text = "${_data.NIK} - ${_data.Nama}";
        // var _nikPegawai = _controllerEmployee.text;
        checkStopSelling(context, _data.NIK);
        await getSalesHead(context);
        notifyListeners();
      }
    } else {
      showSnackBar("Pilih jabatan terlebih dahulu");
    }
  }

  void checkStopSelling(BuildContext context, String nikPegawai) async {
    // print("checkStopSelling");
    final ioc = new HttpClient();
    final _http = IOClient(ioc);
    String _salesStopSelling = await storage.read(key: "SalesStopSelling");
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    var _bodySalesStopSelling = jsonEncode({
      "SZNIK": nikPegawai
    });
    // print("body: ${_bodySalesStopSelling}");
    try {
      final _response = await _http.post(
          "${BaseUrl.urlGeneral}$_salesStopSelling",
          body: _bodySalesStopSelling,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      );
      final _resultStopSelling = jsonDecode(_response.body);
      // print("result: ${_resultStopSelling}");
      if (_resultStopSelling['message'] == "SUCCESS") {
        showDialog(
            context: context,
            barrierDismissible: true,
            builder: (BuildContext context){
              return Theme(
                data: ThemeData(
                    fontFamily: "NunitoSans",
                    primaryColor: Colors.black,
                    primarySwatch: primaryOrange,
                    accentColor: myPrimaryColor
                ),
                child: AlertDialog(
                  title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
                  content: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text("SO Stop Selling",),
                    ],
                  ),
                  actions: <Widget>[
                    new Text("Lanjutkan?"),
                    new FlatButton(
                      onPressed: () => Navigator.of(context).pop(true),
                      child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                    ),
                    new FlatButton(
                      onPressed: () => {
                          this._employeeModelSelected = null,
                          this._controllerEmployee.clear(),
                          Navigator.of(context).pop(true),
                      },
                      child: new Text('Tidak'),
                    ),
                  ],
                ),
              );
            }
        );
      }
    } catch (e) {
      // loginProcess = false;
      showSnackBar(e.toString());
    }
  }

  Future<void> getAtasan(BuildContext context) async{ // gak kepake
    SharedPreferences _preference = await SharedPreferences.getInstance();
    List _temp = [];
    this._listEmployeeHead.clear();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);

    for(int i=0; i < 3; i++) {
      if(i == 0) {
        _groupAlias = "CMH";
      } else if(i == 1) {
        _groupAlias = "CRH";
      } else if(i == 2) {
        _groupAlias = "DBM";
      }
      var _body = jsonEncode({
        "GroupAlias": ["IDM.ROL_$_groupAlias"],
        "LocationAlias": _preference.getString("branchid"),
        "isAlias": true
      });

      var storage = FlutterSecureStorage();
      String _fieldPegawai = await storage.read(key: "FieldPegawai");
      final _response = await _http.post(
          "${BaseUrl.urlGeneral}$_fieldPegawai",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      );
      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        if(_result == null){
          showSnackBar("Atasan tidak ditemukan");
          loadData = false;
        }
        else{
          for(int i=0; i <_result.length; i++){
            if(!_temp.contains(_result[i]['NIK'])){
              this._listEmployeeHead.add(EmployeeHeadModel(_result[i]['NIK'], _result[i]['Nama'], _result[i]['Jabatan'], _result[i]['AliasJabatan']));
            }
            _temp.add(_result[i]['NIK']);
          }
          loadData = false;
        }
      }
      else{
        showSnackBar("Error response status ${_response.statusCode}");
        this._loadData = false;
      }
    }
    notifyListeners();
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  void check(BuildContext context, int flag, int index) {
    var _provider = Provider.of<InformationSalesmanChangeNotifier>(context, listen: false);
    final _form = this._key.currentState;
    if (flag == 0) {
      if (_form.validate()) {
        _provider.addInfoSales(InformationSalesModel(
            this._salesmanTypeModelSelected,
            this._positionModelSelected,
            this._employeeModelSelected,
            this._employeeHeadModelSelected,
            _isEdit,
            _isSalesmanTypeModelChanges,
            _isPositionModelChanges,
            _isEmployeeChanges,
            _isEmployeeHeadModelChanges,
          null,
        ));
        _provider.isCheckData = true;
        Navigator.pop(context);
      } else {
        _provider.isCheckData = false;
        autoValidate = true;
      }
    } else {
      if (_form.validate()) {
        checkDataDakor(index);
        _provider.updateListInfoSales(index,InformationSalesModel(
            this._salesmanTypeModelSelected,
            this._positionModelSelected,
            this._employeeModelSelected,
            this._employeeHeadModelSelected,
            _isEdit,
            _isSalesmanTypeModelChanges,
            _isPositionModelChanges,
            _isEmployeeChanges,
            _isEmployeeHeadModelChanges,
            orderProductSalesID
        ));
        _provider.isCheckData = true;
        Navigator.pop(context);
      } else {
        _provider.isCheckData = false;
        autoValidate = true;
      }
    }
  }

  SalesmanTypeModel get salesmanTypeModelTemp => _salesmanTypeModelTemp;

  PositionModel get positionModeltemp => _positionModeltemp;

  EmployeeHeadModel get employeeHeadModelTemp => _employeeHeadModelTemp;

  String get employeeTemp => _employeeTemp;

  Future<void> setValue(int flag, int index, InformationSalesModel value, BuildContext context) async {
    var _providerListSales = Provider.of<InformationSalesmanChangeNotifier>(context, listen : false);
    await setPreference();
    await getDataFromDashboard(context);
    // await getAtasan(context);
    await getSalesType(context);
    if(flag == 1){
      for (int i = 0; i < this._listSalesmanType.length; i++) {
        if (value.salesmanTypeModel.id == this._listSalesmanType[i].id) {
          // if(value.salesmanTypeModel.id != "001"){
          //   this._listSalesmanType.removeAt(0);
          // }
          this._salesmanTypeModelSelected = this._listSalesmanType[i];
          this._salesmanTypeModelTemp = this._listSalesmanType[i];
        }
      }
      await getSalesPosition(context);
      for (int i = 0; i < this._listPosition.length; i++) {
        if (value.positionModel.id == this._listPosition[i].id) {
          this._positionModelSelected = this._listPosition[i];
          this._positionModeltemp = this._listPosition[i];
        }
      }
      orderProductSalesID = value.orderProductSalesID;
      this._employeeModelSelected = value.employeeModel;
      this._controllerEmployee.text = "${value.employeeModel.NIK} - ${value.employeeModel.Nama}";
      this._employeeTemp = this._controllerEmployee.text;
      await getSalesHead(context);

      for (int i = 0; i < this._listEmployeeHead.length; i++) {
        if (value.employeeHeadModel.NIK == this._listEmployeeHead[i].NIK) {
          this._employeeHeadModelSelected = this._listEmployeeHead[i];
          this._employeeHeadModelTemp = this._listEmployeeHead[i];
        }
      }
      checkDataDakor(index);
    }
    else{
      //clear
      for(int i=0; i<_providerListSales.listInfoSales.length; i++){
        for(int j=0; j<this._listSalesmanType.length; j++){
          if(this._listSalesmanType[j].id == "001"){ //if(_providerListSales.listInfoSales[i].salesmanTypeModel.id == this._listSalesmanType[j].id){
            this._listSalesmanType.removeAt(j);
          }
        }
      }
    }
  }

  Future<void> setPreference() async {
    SharedPreferences _preference = await SharedPreferences.getInstance();
    this._custType = _preference.getString("cust_type");
    this._lastKnownState = _preference.getString("last_known_state");
    if(this._lastKnownState == "AOS" || this._lastKnownState == "CONA" || this._lastKnownState == "IA" || this._lastKnownState == "PAC") {
      this._isDisablePACIAAOSCONA = true;
    }
    notifyListeners();
  }

  ShowMandatoryInfoSalesmanModel _showMandatoryInfoSalesmanIdeModel;
  ShowMandatoryInfoSalesmanModel _showMandatoryInfoSalesmanIdeCompanyModel;

  void showMandatoryIdeModel(BuildContext context){
    _showMandatoryInfoSalesmanIdeModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoSalesmanIdeModel;
    _showMandatoryInfoSalesmanIdeCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoSalesmanIdeCompanyModel;
  }

  ShowMandatoryInfoSalesmanModel _showMandatoryInfoSalesmanRegulerSurveyModel;
  ShowMandatoryInfoSalesmanModel _showMandatoryInfoSalesmanRegulerSurveyCompanyModel;
  void showMandatoryRegulerSurveyModel(BuildContext context){
    _showMandatoryInfoSalesmanRegulerSurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoSalesmanRegulerSurveyModel;
    _showMandatoryInfoSalesmanRegulerSurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoSalesmanRegulerSurveyCompanyModel;
  }

  ShowMandatoryInfoSalesmanModel _showMandatoryInfoSalesmanPacModel;
  ShowMandatoryInfoSalesmanModel _showMandatoryInfoSalesmanPacCompanyModel;
  void showMandatoryPacModel(BuildContext context){
    _showMandatoryInfoSalesmanPacModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoSalesmanPacModel;
    _showMandatoryInfoSalesmanPacCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoSalesmanPacCompanyModel;
  }

  ShowMandatoryInfoSalesmanModel _showMandatoryInfoSalesmanResurveyModel;
  ShowMandatoryInfoSalesmanModel _showMandatoryInfoSalesmanResurveyCompanyModel;
  void showMandatoryResurveyModel(BuildContext context){
    _showMandatoryInfoSalesmanResurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoSalesmanResurveyModel;
    _showMandatoryInfoSalesmanResurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoSalesmanResurveyCompanyModel;
  }

  ShowMandatoryInfoSalesmanModel _showMandatoryInfoSalesmanDakorModel;
  ShowMandatoryInfoSalesmanModel _showMandatoryInfoSalesmanDakorCompanyModel;
  void showMandatoryDakorModel(BuildContext context){
    _showMandatoryInfoSalesmanDakorModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoSalesmanDakorModel;
    _showMandatoryInfoSalesmanDakorCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoSalesmanDakorCompanyModel;
  }

  Future<void> getDataFromDashboard(BuildContext context)async{
    showMandatoryIdeModel(context);
    showMandatoryRegulerSurveyModel(context);
    showMandatoryPacModel(context);
    showMandatoryResurveyModel(context);
    showMandatoryDakorModel(context);
  }

  // Jenis Sales
  bool isSalesmanTypeModelVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoSalesmanIdeModel.isSalesmanTypeModelVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoSalesmanRegulerSurveyModel.isSalesmanTypeModelVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoSalesmanPacModel.isSalesmanTypeModelVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoSalesmanResurveyModel.isSalesmanTypeModelVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoSalesmanDakorModel.isSalesmanTypeModelVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoSalesmanIdeCompanyModel.isSalesmanTypeModelVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoSalesmanRegulerSurveyCompanyModel.isSalesmanTypeModelVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoSalesmanPacCompanyModel.isSalesmanTypeModelVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoSalesmanResurveyCompanyModel.isSalesmanTypeModelVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoSalesmanDakorCompanyModel.isSalesmanTypeModelVisible;
      }
    }
    return value;
  }

// Jabatan
  bool isPositionModelVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoSalesmanIdeModel.isPositionModelVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoSalesmanRegulerSurveyModel.isPositionModelVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoSalesmanPacModel.isPositionModelVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoSalesmanResurveyModel.isPositionModelVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoSalesmanDakorModel.isPositionModelVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoSalesmanIdeCompanyModel.isPositionModelVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoSalesmanRegulerSurveyCompanyModel.isPositionModelVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoSalesmanPacCompanyModel.isPositionModelVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoSalesmanResurveyCompanyModel.isPositionModelVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoSalesmanDakorCompanyModel.isPositionModelVisible;
      }
    }
    return value;
  }

// Pegawai
  bool isEmployeeVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoSalesmanIdeModel.isEmployeeVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoSalesmanRegulerSurveyModel.isEmployeeVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoSalesmanPacModel.isEmployeeVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoSalesmanResurveyModel.isEmployeeVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoSalesmanDakorModel.isEmployeeVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoSalesmanIdeCompanyModel.isEmployeeVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoSalesmanRegulerSurveyCompanyModel.isEmployeeVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoSalesmanPacCompanyModel.isEmployeeVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoSalesmanResurveyCompanyModel.isEmployeeVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoSalesmanDakorCompanyModel.isEmployeeVisible;
      }
    }
    return value;
  }

// Atasan
  bool isEmployeeHeadModelVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoSalesmanIdeModel.isEmployeeHeadModelVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoSalesmanRegulerSurveyModel.isEmployeeHeadModelVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoSalesmanPacModel.isEmployeeHeadModelVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoSalesmanResurveyModel.isEmployeeHeadModelVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoSalesmanDakorModel.isEmployeeHeadModelVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoSalesmanIdeCompanyModel.isEmployeeHeadModelVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoSalesmanRegulerSurveyCompanyModel.isEmployeeHeadModelVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoSalesmanPacCompanyModel.isEmployeeHeadModelVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoSalesmanResurveyCompanyModel.isEmployeeHeadModelVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoSalesmanDakorCompanyModel.isEmployeeHeadModelVisible;
      }
    }
    return value;
  }

  // Jenis Sales
  bool isSalesmanTypeModelMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoSalesmanIdeModel.isSalesmanTypeModelMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoSalesmanRegulerSurveyModel.isSalesmanTypeModelMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoSalesmanPacModel.isSalesmanTypeModelMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoSalesmanResurveyModel.isSalesmanTypeModelMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoSalesmanDakorModel.isSalesmanTypeModelMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoSalesmanIdeCompanyModel.isSalesmanTypeModelMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoSalesmanRegulerSurveyCompanyModel.isSalesmanTypeModelMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoSalesmanPacCompanyModel.isSalesmanTypeModelMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoSalesmanResurveyCompanyModel.isSalesmanTypeModelMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoSalesmanDakorCompanyModel.isSalesmanTypeModelMandatory;
      }
    }
    return value;
  }

// Jabatan
  bool isPositionModelMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoSalesmanIdeModel.isPositionModelMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoSalesmanRegulerSurveyModel.isPositionModelMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoSalesmanPacModel.isPositionModelMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoSalesmanResurveyModel.isPositionModelMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoSalesmanDakorModel.isPositionModelMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoSalesmanIdeCompanyModel.isPositionModelMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoSalesmanRegulerSurveyCompanyModel.isPositionModelMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoSalesmanPacCompanyModel.isPositionModelMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoSalesmanResurveyCompanyModel.isPositionModelMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoSalesmanDakorCompanyModel.isPositionModelMandatory;
      }
    }
    return value;
  }

// Pegawai
  bool isEmployeeMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoSalesmanIdeModel.isEmployeeMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoSalesmanRegulerSurveyModel.isEmployeeMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoSalesmanPacModel.isEmployeeMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoSalesmanResurveyModel.isEmployeeMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoSalesmanDakorModel.isEmployeeMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoSalesmanIdeCompanyModel.isEmployeeMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoSalesmanRegulerSurveyCompanyModel.isEmployeeMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoSalesmanPacCompanyModel.isEmployeeMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoSalesmanResurveyCompanyModel.isEmployeeMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoSalesmanDakorCompanyModel.isEmployeeMandatory;
      }
    }
    return value;
  }

// Atasan
  bool isEmployeeHeadModelMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoSalesmanIdeModel.isEmployeeHeadModelMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoSalesmanRegulerSurveyModel.isEmployeeHeadModelMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoSalesmanPacModel.isEmployeeHeadModelMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoSalesmanResurveyModel.isEmployeeHeadModelMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoSalesmanDakorModel.isEmployeeHeadModelMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoSalesmanIdeCompanyModel.isEmployeeHeadModelMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoSalesmanRegulerSurveyCompanyModel.isEmployeeHeadModelMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoSalesmanPacCompanyModel.isEmployeeHeadModelMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoSalesmanResurveyCompanyModel.isEmployeeHeadModelMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoSalesmanDakorCompanyModel.isEmployeeHeadModelMandatory;
      }
    }
    return value;
  }

  void checkDataDakor(int index) async{
    print("halo cek dakor sales");
    List _check = await _dbHelper.selectMS2ObjekSales();
    if(_check.isNotEmpty && _lastKnownState == "DKR"){
      isSalesmanTypeModelChanges = _salesmanTypeModelSelected.id != _check[index]['sales_type'] || _check[index]['edit_sales_type'] == "1";
      // print("1 = ${_salesmanTypeModelSelected.id}");
      // print("2 = ${_check[index]['sales_type']}");
      // print("3 = ${_check[index]['edit_sales_type']}");
      // print("4 = $isSalesmanTypeModelChanges");
      isPositionModelChanges = _positionModelSelected.id != _check[index]['empl_job'] || _check[index]['edit_empl_job'] == "1";
      isEmployeeChanges = _employeeModelSelected.NIK != _check[index]['empl_id'] || _check[index]['edit_empl_name'] == "1";
      isEmployeeHeadModelChanges = _employeeHeadModelSelected.NIK != _check[index]['empl_head_id'] || _check[index]['edit_empl_head_id'] == "1";
      if(isSalesmanTypeModelChanges || isPositionModelChanges || isEmployeeChanges || isEmployeeHeadModelChanges){
        isEdit = true;
      } else {
        isEdit = false;
      }
    }
  }

  bool get isEdit => _isEdit;

  set isEdit(bool value) {
    _isEdit = value;
    notifyListeners();
  }

  bool get isEmployeeHeadModelChanges => _isEmployeeHeadModelChanges;

  set isEmployeeHeadModelChanges(bool value) {
    this._isEmployeeHeadModelChanges = value;
    notifyListeners();
  }

  bool get isEmployeeChanges => _isEmployeeChanges;

  set isEmployeeChanges(bool value) {
    this._isEmployeeChanges = value;
    notifyListeners();
  }

  bool get isPositionModelChanges => _isPositionModelChanges;

  set isPositionModelChanges(bool value) {
    this._isPositionModelChanges = value;
    notifyListeners();
  }

  bool get isSalesmanTypeModelChanges => _isSalesmanTypeModelChanges;

  set isSalesmanTypeModelChanges(bool value) {
    this._isSalesmanTypeModelChanges = value;
    notifyListeners();
  }

  bool get isDisablePACIAAOSCONA => _isDisablePACIAAOSCONA;

  set isDisablePACIAAOSCONA(bool value) {
    this._isDisablePACIAAOSCONA = value;
    notifyListeners();
  }
}
