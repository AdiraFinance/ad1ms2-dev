import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/employee_head_model.dart';
import 'package:ad1ms2_dev/models/employee_model.dart';
import 'package:ad1ms2_dev/models/infromation_sales_model.dart';
import 'package:ad1ms2_dev/models/ms2_objek_sales_model.dart';
import 'package:ad1ms2_dev/models/position_model.dart';
import 'package:ad1ms2_dev/models/salesman_type_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_individu_change_notifier/form_m_parent_individu_change_notifier.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InformationSalesmanChangeNotifier with ChangeNotifier {
  List<InformationSalesModel> _listInfoSales = [];
  bool _autoValidate = false;
  DbHelper _dbHelper = DbHelper();
  String _custType;
  String _lastKnownState;
  bool _isDisablePACIAAOSCONA = false;
  bool _isCheckData = false;

  bool get isCheckData => _isCheckData;

  set isCheckData(bool value) {
    this._isCheckData = value;
    notifyListeners();
  }

  List<InformationSalesModel> get listInfoSales => _listInfoSales;

  void addInfoSales(InformationSalesModel value) {
    this._listInfoSales.add(value);
    notifyListeners();
  }

  Future<void> setPreference() async {
    if(this._listInfoSales.length != 0) {
      this._isCheckData = true;
    } else {
      this._isCheckData = false;
    }
    SharedPreferences _preference = await SharedPreferences.getInstance();
    this._custType = _preference.getString("cust_type");
    this._lastKnownState = _preference.getString("last_known_state");
    if(this._lastKnownState == "AOS" || this._lastKnownState == "CONA" || this._lastKnownState == "IA" || this._lastKnownState == "PAC") {
      this._isDisablePACIAAOSCONA = true;
    }
    notifyListeners();
  }

  void deleteListInfoSales(BuildContext context, int index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                primaryColor: Colors.black,
                primarySwatch: primaryOrange,
                accentColor: myPrimaryColor
            ),
            child: AlertDialog(
              title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Apakah kamu yakin menghapus salesman ini?",),
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    this._listInfoSales.removeAt(index);
                    notifyListeners();
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                ),
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Tidak'),
                ),
              ],
            ),
          );
        }
    );
  }

  void updateListInfoSales(int index, InformationSalesModel value) {
    this._listInfoSales[index] = value;
    notifyListeners();
  }

  void clearDataInfoSales(){
    this._autoValidate = false;
    this._listInfoSales.clear();
    this._isCheckData = false;
    isDisablePACIAAOSCONA = false;
  }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    _autoValidate = value;
    notifyListeners();
  }

  Future<bool> onBackPress() async{
    autoValidate = true;
    for(int i=0; i<_listInfoSales.length; i++){
      if(_listInfoSales[i].salesmanTypeModel.id == "001"){ //sales internal
        autoValidate = false;
        this._isCheckData = true;
      }
    }
    // if (_listInfoSales.length != 0) {
    //   autoValidate = false;
    // } else {
    //   autoValidate = true;
    // }
    return true;
  }

  void saveToSQLite(BuildContext context) async {
    debugPrint("save sales");
    List<MS2ObjekSalesModel> _listObjekSales = [];
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var noReference = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).controllerReferenceNumber.text;
    for(int i=0; i<_listInfoSales.length; i++){

      // final String Jabatan;
      // final String AliasJabatan;
      _listObjekSales.add(MS2ObjekSalesModel(
        "",
        this._listInfoSales[i].orderProductSalesID != null ? this._listInfoSales[i].orderProductSalesID : "NEW",
        this._listInfoSales[i].employeeModel != null ? this._listInfoSales[i].employeeModel.NIK : "",
        this._listInfoSales[i].salesmanTypeModel != null ? this._listInfoSales[i].salesmanTypeModel.id : "",
        this._listInfoSales[i].salesmanTypeModel != null ? this._listInfoSales[i].salesmanTypeModel.salesmanType : "",
        this._listInfoSales[i].employeeHeadModel != null ? this._listInfoSales[i].employeeHeadModel.NIK : "",
        this._listInfoSales[i].positionModel != null ? this._listInfoSales[i].positionModel.id : "",
        this._listInfoSales[i].positionModel != null ? this._listInfoSales[i].positionModel.positionName : "",
        noReference,
        1,
        DateTime.now().toString(),
        _preferences.getString("username"),
        null,
        null,
        this._listInfoSales[i].employeeHeadModel != null ? this._listInfoSales[i].employeeHeadModel.Jabatan : "",
        this._listInfoSales[i].employeeHeadModel != null ? this._listInfoSales[i].employeeHeadModel.AliasJabatan : "",
        this._listInfoSales[i].employeeHeadModel != null ? this._listInfoSales[i].employeeHeadModel.Nama : "",
        this._listInfoSales[i].employeeModel != null ? this._listInfoSales[i].employeeModel.Nama : "",
        this._listInfoSales[i].employeeModel != null ? this._listInfoSales[i].employeeModel.Jabatan : "",
        this._listInfoSales[i].employeeModel != null ? this._listInfoSales[i].employeeModel.AliasJabatan : "",
        this._listInfoSales[i].isSalesmanTypeModelChanges ? "1" : "0",
        this._listInfoSales[i].isPositionModelChanges ? "1" : "0",
        this._listInfoSales[i].isEmployeeChanges ? "1" : "0",
        this._listInfoSales[i].isEmployeeHeadModelChanges ? "1" : "0",
      ));
    }
    _dbHelper.insertMS2ObjekSales(_listObjekSales);
  }

  Future<bool> deleteSQLite() async{
    return await _dbHelper.deleteMS2ObjekSales();
  }

  Future<void> setDataFromSQLite(BuildContext context) async{
    List _check = await _dbHelper.selectMS2ObjekSales();
    print("sales = $_check");
    if(_check.isNotEmpty){
      print(_check.length);
      for(int i=0; i<_check.length; i++){
        var _salesTypeModel = SalesmanTypeModel(_check[i]['sales_type'], _check[i]['sales_type_desc']);
        var _salesPosition = PositionModel(_check[i]['empl_job'], _check[i]['empl_job_desc']);
        var _employeeModel = EmployeeModel(_check[i]['empl_id'], _check[i]['empl_name'], _check[i]['empl_jabatan'], _check[i]['empl_jabatan_alias']);
        var _salesHead = EmployeeHeadModel(_check[i]['empl_head_id'], _check[i]['empl_head_name'], _check[i]['empl_head_job'], _check[i]['empl_head_job_desc']);
        _listInfoSales.add(InformationSalesModel(
          _salesTypeModel,
          _salesPosition,
          _employeeModel,
          _salesHead,
          _check[i]['edit_sales_type'] == "1" || _check[i]['edit_empl_job'] == "1" ||
          _check[i]['edit_empl_name'] == "1" || _check[i]['edit_empl_head_id'] == "1",
          _check[i]['edit_sales_type'] == "1",
          _check[i]['edit_empl_job'] == "1",
          _check[i]['edit_empl_name'] == "1",
          _check[i]['edit_empl_head_id'] == "1",
            _check[i]['orderProductSalesID']
        ));
      }
    }
    notifyListeners();
  }

  String get custType => _custType;

  set custType(String value) {
    this._custType = value;
    notifyListeners();
  }

  String get lastKnownState => _lastKnownState;

  set lastKnownState(String value) {
    this._lastKnownState = value;
    notifyListeners();
  }

  bool get isDisablePACIAAOSCONA => _isDisablePACIAAOSCONA;

  set isDisablePACIAAOSCONA(bool value) {
    this._isDisablePACIAAOSCONA = value;
    notifyListeners();
  }
}
