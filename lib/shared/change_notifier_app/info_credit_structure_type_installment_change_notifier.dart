import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/gp_type_model.dart';
import 'package:ad1ms2_dev/models/info_credit_structure_floating_model.dart';
import 'package:ad1ms2_dev/models/info_credit_structure_gp_model.dart';
import 'package:ad1ms2_dev/models/info_credit_structure_irregular_model.dart';
import 'package:ad1ms2_dev/models/info_credit_structure_stepping_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_loan_detail.dart';
import 'package:ad1ms2_dev/models/ms2_appl_object_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_struktur_kredit_bp_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_struktur_kredit_floating_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_struktur_kredit_gp_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_struktur_kredit_irreguler_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_struktur_kredit_stepping_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/dashboard/dashboard_change_notif.dart';
import 'package:ad1ms2_dev/shared/format_currency.dart';
import 'package:ad1ms2_dev/shared/regex_input_formatter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InfoCreditStructureTypeInstallmentChangeNotifier with ChangeNotifier{
  List<InfoCreditStructureFloatingModel> _listInfoCreditStructureFloatingModel = [];
  List<String> _listInstallment = [];
  String _installmentSelected;
  bool _autoValidateDialogFloating = false;
  TextEditingController _controllerInterestRate = TextEditingController();
  GlobalKey<FormState> _keyFloating = GlobalKey<FormState>();
  DbHelper _dbHelper = DbHelper();
  String _custType;
  String _lastKnownState;
  bool _autoValidate = false;
  bool _isDisablePACIAAOSCONA = false;
  FormatCurrency _formatCurrency = FormatCurrency();
  RegExInputFormatter _amountValidator = RegExInputFormatter.withRegex('^[0-9]{0,13}(\\.[0-9]{0,2})?\$');

  RegExInputFormatter get amountValidator => _amountValidator;

  FormatCurrency get formatCurrency => _formatCurrency;

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  String get custType => _custType;

  set custType(String value) {
    this._custType = value;
    notifyListeners();
  }

  String get lastKnownState => _lastKnownState;

  set lastKnownState(String value) {
    this._lastKnownState = value;
    notifyListeners();
  }

  Future<void> setPreference(BuildContext context) async {
    SharedPreferences _preference = await SharedPreferences.getInstance();
    var _providerCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false);
    this._custType = _preference.getString("cust_type");
    this._lastKnownState = _preference.getString("last_known_state");
    getDataFromDashboard(context);
    setListInstallment(context);
    await setValue(context);
    await setValueTotalStepping(context);
    if(this._lastKnownState == "AOS" || this._lastKnownState == "CONA" || this._lastKnownState == "IA" || this._lastKnownState == "PAC") {
      this._isDisablePACIAAOSCONA = true;
    }
    if(_providerCreditStructure.installmentTypeSelected != null) {
      if(_providerCreditStructure.installmentTypeSelected.id == "08") {
        formattingBP();
      }
    }
  }

  void getDataFromDashboard(BuildContext context){
    // Inf Struktur Kredit Floating
    showMandatoryInfoStrukturKreditFloatingIdeModel(context);
    showMandatoryInfoStrukturKreditFloatingRegulerSurveyModel(context);
    showMandatoryInfoStrukturKreditFloatingPacModel(context);
    showMandatoryInfoStrukturKreditFloatingResurveyModel(context);
    showMandatoryInfoStrukturKreditFloatingDakorModel(context);

    // Inf Struktur Kredit GP
    showMandatoryInfoStrukturKreditGPIdeModel(context);
    showMandatoryInfoStrukturKreditGPRegulerSurveyModel(context);
    showMandatoryInfoStrukturKreditGPPacModel(context);
    showMandatoryInfoStrukturKreditGPResurveyModel(context);
    showMandatoryInfoStrukturKreditGPDakorModel(context);

    // Inf Struktur Kredit Stepping
    showMandatoryInfoStrukturKreditSteppingIdeModel(context);
    showMandatoryInfoStrukturKreditSteppingRegulerSurveyModel(context);
    showMandatoryInfoStrukturKreditSteppingPacModel(context);
    showMandatoryInfoStrukturKreditSteppingResurveyModel(context);
    showMandatoryInfoStrukturKreditSteppingDakorModel(context);

    // Inf Struktur Kredit Irreguler
    showMandatoryInfoStrukturKreditIrregulerIdeModel(context);
    showMandatoryInfoStrukturKreditIrregulerRegulerSurveyModel(context);
    showMandatoryInfoStrukturKreditIrregulerPacModel(context);
    showMandatoryInfoStrukturKreditIrregulerResurveyModel(context);
    showMandatoryInfoStrukturKreditIrregulerDakorModel(context);

    // Inf Struktur Kredit BP
    showMandatoryInfoStrukturKreditBPIdeModel(context);
    showMandatoryInfoStrukturKreditBPRegulerSurveyModel(context);
    showMandatoryInfoStrukturKreditBPPacModel(context);
    showMandatoryInfoStrukturKreditBPResurveyModel(context);
    showMandatoryInfoStrukturKreditBPDakorModel(context);
  }

  /*
  * Floating
  **/
  List<InfoCreditStructureFloatingModel>
      get listInfoCreditStructureFloatingModel =>
          _listInfoCreditStructureFloatingModel;

  void checkFloating(int flag,InfoCreditStructureFloatingModel value,int index,BuildContext context){
    final _form = _keyFloating.currentState;
    if(_form.validate()){
      if(flag == 0){
        addInfoCreditStructureFloating(value,context);
      }
      else{
        updateInfoCreditStructureFloating(value,index,context);
      }
    }
    else{
      autoValidateDialogFloating = true;
    }
  }

  void addInfoCreditStructureFloating(InfoCreditStructureFloatingModel value,BuildContext context){
    this._listInfoCreditStructureFloatingModel.add(value);
    if(autoValidateDialogFloating) autoValidateDialogFloating = false;
    notifyListeners();
    Navigator.pop(context);
  }

  void updateInfoCreditStructureFloating(InfoCreditStructureFloatingModel value,int index,BuildContext context){
    this._listInfoCreditStructureFloatingModel[index] = value;
    if(autoValidateDialogFloating) autoValidateDialogFloating = false;
    notifyListeners();
    Navigator.pop(context);
  }

  void deleteInfoCreditStructureFloating(int index){
    this._listInfoCreditStructureFloatingModel.removeAt(index);
    notifyListeners();
  }

  List<String> get listInstallment => _listInstallment;


  String get installmentSelected => _installmentSelected;

  set installmentSelected(String value) {
    this._installmentSelected = value;
    notifyListeners();
  }

  int _tenor;

  void setListInstallment(BuildContext context){
    var _provider = Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false);
    _tenor = int.parse(_provider.periodOfTimeSelected);
    this._listInstallment.clear();
    if(_provider.installmentTypeSelected.id == "05"){
      for(int i = 1; i <= _tenor -1; i++){
        this._listInstallment.add(i.toString());
      }
    }
    else{
      for(int i = 1; i <= _tenor; i++){
        this._listInstallment.add(i.toString());
      }
    }
  }

  bool get autoValidateDialogFloating => _autoValidateDialogFloating;

  set autoValidateDialogFloating(bool value) {
    this._autoValidateDialogFloating = value;
    notifyListeners();
  }

  TextEditingController get controllerInterestRate => _controllerInterestRate;

  GlobalKey<FormState> get keyFloating => _keyFloating;

  void clearDialogFloating(){
    this._installmentSelected = null;
    this._controllerInterestRate.clear();
  }

  void setValueForEditFloating(InfoCreditStructureFloatingModel value){
    this._installmentSelected = value.installment;
    this._controllerInterestRate.text = value.interestRateEffFloat;
  }

  ShowMandatoryInfoStrukturKreditFloatingModel _showMandatoryInfoStrukturKreditFloatingIdeModel;
  ShowMandatoryInfoStrukturKreditFloatingModel _showMandatoryInfoStrukturKreditFloatingIdeCompanyModel;
  void showMandatoryInfoStrukturKreditFloatingIdeModel(BuildContext context){
    _showMandatoryInfoStrukturKreditFloatingIdeModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditFloatingIdeModel;
    _showMandatoryInfoStrukturKreditFloatingIdeCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditFloatingIdeCompanyModel;
  }

  ShowMandatoryInfoStrukturKreditFloatingModel _showMandatoryInfoStrukturKreditFloatingRegulerSurveyModel;
  ShowMandatoryInfoStrukturKreditFloatingModel _showMandatoryInfoStrukturKreditFloatingRegulerSurveyCompanyModel;
  void showMandatoryInfoStrukturKreditFloatingRegulerSurveyModel(BuildContext context){
    _showMandatoryInfoStrukturKreditFloatingRegulerSurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditFloatingRegulerSurveyModel;
    _showMandatoryInfoStrukturKreditFloatingRegulerSurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditFloatingRegulerSurveyCompanyModel;
  }

  ShowMandatoryInfoStrukturKreditFloatingModel _showMandatoryInfoStrukturKreditFloatingPacModel;
  ShowMandatoryInfoStrukturKreditFloatingModel _showMandatoryInfoStrukturKreditFloatingPacCompanyModel;
  void showMandatoryInfoStrukturKreditFloatingPacModel(BuildContext context){
    _showMandatoryInfoStrukturKreditFloatingPacModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditFloatingPacModel;
    _showMandatoryInfoStrukturKreditFloatingPacCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditFloatingPacCompanyModel;
  }

  ShowMandatoryInfoStrukturKreditFloatingModel _showMandatoryInfoStrukturKreditFloatingResurveyModel;
  ShowMandatoryInfoStrukturKreditFloatingModel _showMandatoryInfoStrukturKreditFloatingResurveyCompanyModel;
  void showMandatoryInfoStrukturKreditFloatingResurveyModel(BuildContext context){
    _showMandatoryInfoStrukturKreditFloatingResurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditFloatingResurveyModel;
    _showMandatoryInfoStrukturKreditFloatingResurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditFloatingResurveyCompanyModel;
  }

  ShowMandatoryInfoStrukturKreditFloatingModel _showMandatoryInfoStrukturKreditFloatingDakorModel;
  ShowMandatoryInfoStrukturKreditFloatingModel _showMandatoryInfoStrukturKreditFloatingDakorCompanyModel;
  void showMandatoryInfoStrukturKreditFloatingDakorModel(BuildContext context){
    _showMandatoryInfoStrukturKreditFloatingDakorModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditFloatingDakorModel;
    _showMandatoryInfoStrukturKreditFloatingDakorCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditFloatingDakorCompanyModel;
  }

  bool isInstallmentSelectedFloatingVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditFloatingIdeModel.isInstallmentSelectedFloatingVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditFloatingRegulerSurveyModel.isInstallmentSelectedFloatingVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditFloatingPacModel.isInstallmentSelectedFloatingVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditFloatingResurveyModel.isInstallmentSelectedFloatingVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditFloatingDakorModel.isInstallmentSelectedFloatingVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditFloatingIdeCompanyModel.isInstallmentSelectedFloatingVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditFloatingRegulerSurveyCompanyModel.isInstallmentSelectedFloatingVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditFloatingPacCompanyModel.isInstallmentSelectedFloatingVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditFloatingResurveyCompanyModel.isInstallmentSelectedFloatingVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditFloatingDakorCompanyModel.isInstallmentSelectedFloatingVisible;
      }
    }
    return value;
  }

  bool isInterestRateVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditFloatingIdeModel.isInterestRateVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditFloatingRegulerSurveyModel.isInterestRateVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditFloatingPacModel.isInterestRateVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditFloatingResurveyModel.isInterestRateVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditFloatingDakorModel.isInterestRateVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditFloatingIdeCompanyModel.isInterestRateVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditFloatingRegulerSurveyCompanyModel.isInterestRateVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditFloatingPacCompanyModel.isInterestRateVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditFloatingResurveyCompanyModel.isInterestRateVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditFloatingDakorCompanyModel.isInterestRateVisible;
      }
    }
    return value;
  }

  bool isInstallmentSelectedFloatingMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditFloatingIdeModel.isInstallmentSelectedFloatingMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditFloatingRegulerSurveyModel.isInstallmentSelectedFloatingMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditFloatingPacModel.isInstallmentSelectedFloatingMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditFloatingResurveyModel.isInstallmentSelectedFloatingMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditFloatingDakorModel.isInstallmentSelectedFloatingMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditFloatingIdeCompanyModel.isInstallmentSelectedFloatingMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditFloatingRegulerSurveyCompanyModel.isInstallmentSelectedFloatingMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditFloatingPacCompanyModel.isInstallmentSelectedFloatingMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditFloatingResurveyCompanyModel.isInstallmentSelectedFloatingMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditFloatingDakorCompanyModel.isInstallmentSelectedFloatingMandatory;
      }
    }
    return value;
  }

  bool isInterestRateMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditFloatingIdeModel.isInterestRateMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditFloatingRegulerSurveyModel.isInterestRateMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditFloatingPacModel.isInterestRateMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditFloatingResurveyModel.isInterestRateMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditFloatingDakorModel.isInterestRateMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditFloatingIdeCompanyModel.isInterestRateMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditFloatingRegulerSurveyCompanyModel.isInterestRateMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditFloatingPacCompanyModel.isInterestRateMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditFloatingResurveyCompanyModel.isInterestRateMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditFloatingDakorCompanyModel.isInterestRateMandatory;
      }
    }
    return value;
  }

  /*
  * GP
  **/
  GPTypeModel _gpTypeSelected;
  GlobalKey<FormState> _keyGP = GlobalKey<FormState>();
  GPTypeModel get gpTypeSelected => _gpTypeSelected;
  TextEditingController _controllerNewTenor = TextEditingController();
  bool _autoValidateDialogGP = false;
  List<InfoCreditStructureGPModel> _listInfoCreditStructureGPModel = [];

  set gpTypeSelected(GPTypeModel value) {
    this._gpTypeSelected = value;
    notifyListeners();
  }

  List<GPTypeModel> _listGPType = [
    GPTypeModel("01", "TETAP"),
    GPTypeModel("02", "MUNDUR")
  ];

  List<GPTypeModel> get listGPType => _listGPType;

  GlobalKey<FormState> get keyGP => _keyGP;

  TextEditingController get controllerNewTenor => _controllerNewTenor;

  bool get autoValidateDialogGP => _autoValidateDialogGP;

  set autoValidateDialogGP(bool value) {
    this._autoValidateDialogGP = value;
    notifyListeners();
  }

  List<InfoCreditStructureGPModel> get listInfoCreditStructureGPModel =>
      _listInfoCreditStructureGPModel;

  void addInfoCreditStructureGPModel(InfoCreditStructureGPModel value,BuildContext context){
    this._listInfoCreditStructureGPModel.add(value);
    if(autoValidateDialogGP) autoValidateDialogGP = false;
    notifyListeners();
    Navigator.pop(context);
  }

  void updateInfoCreditStructureGPModel(InfoCreditStructureGPModel value,int index,BuildContext context){
    this._listInfoCreditStructureGPModel[index] = value;
    if(autoValidateDialogGP) autoValidateDialogGP = false;
    notifyListeners();
    Navigator.pop(context);
  }

  void deleteInfoCreditStructureGPModel(BuildContext context, int index){
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                primaryColor: Colors.black,
                primarySwatch: primaryOrange,
                accentColor: myPrimaryColor
            ),
            child: AlertDialog(
              title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Apakah kamu yakin menghapus dokumen unit object ini?",),
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    this._listInfoCreditStructureGPModel.removeAt(index);
                    notifyListeners();
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                ),
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Tidak'),
                ),
              ],
            ),
          );
        }
    );
  }

  void checkGP(int flag,InfoCreditStructureGPModel value,int index,BuildContext context){
    final _form = _keyGP.currentState;
    if(_form.validate()){
      if(flag == 0){
        addInfoCreditStructureGPModel(value,context);
      }
      else{
        updateInfoCreditStructureGPModel(value,index,context);
      }
    }
    else{
      autoValidateDialogGP = true;
    }
  }

  void clearDialogGP(){
    this._gpTypeSelected = null;
    this._installmentSelected = null;
    this._controllerNewTenor.clear();
    notifyListeners();
  }

  void setValueForEditGP(InfoCreditStructureGPModel value){
    this._gpTypeSelected = value.gpTypeModel;
    this._installmentSelected = value.installment;
    this._controllerNewTenor.text = value.tenor;
  }

  void setValueNewTenor(){
      if(_gpTypeSelected.id == "01"){
        this._controllerNewTenor.text = _tenor.toString();
      }
      else{
        this._controllerNewTenor.text = (_tenor+this._listInstallment.length).toString();
      }
      notifyListeners();
  }

  ShowMandatoryInfoStrukturKreditGPModel _showMandatoryInfoStrukturKreditGPIdeModel;
  ShowMandatoryInfoStrukturKreditGPModel _showMandatoryInfoStrukturKreditGPIdeCompanyModel;
  void showMandatoryInfoStrukturKreditGPIdeModel(BuildContext context){
    _showMandatoryInfoStrukturKreditGPIdeModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditGPIdeModel;
    _showMandatoryInfoStrukturKreditGPIdeCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditGPIdeCompanyModel;
  }

  ShowMandatoryInfoStrukturKreditGPModel _showMandatoryInfoStrukturKreditGPRegulerSurveyModel;
  ShowMandatoryInfoStrukturKreditGPModel _showMandatoryInfoStrukturKreditGPRegulerSurveyCompanyModel;
  void showMandatoryInfoStrukturKreditGPRegulerSurveyModel(BuildContext context){
    _showMandatoryInfoStrukturKreditGPRegulerSurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditGPRegulerSurveyModel;
    _showMandatoryInfoStrukturKreditGPRegulerSurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditGPRegulerSurveyCompanyModel;
  }

  ShowMandatoryInfoStrukturKreditGPModel _showMandatoryInfoStrukturKreditGPPacModel;
  ShowMandatoryInfoStrukturKreditGPModel _showMandatoryInfoStrukturKreditGPPacCompanyModel;
  void showMandatoryInfoStrukturKreditGPPacModel(BuildContext context){
    _showMandatoryInfoStrukturKreditGPPacModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditGPPacModel;
    _showMandatoryInfoStrukturKreditGPPacCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditGPPacCompanyModel;
  }

  ShowMandatoryInfoStrukturKreditGPModel _showMandatoryInfoStrukturKreditGPResurveyModel;
  ShowMandatoryInfoStrukturKreditGPModel _showMandatoryInfoStrukturKreditGPResurveyCompanyModel;
  void showMandatoryInfoStrukturKreditGPResurveyModel(BuildContext context){
    _showMandatoryInfoStrukturKreditGPResurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditGPResurveyModel;
    _showMandatoryInfoStrukturKreditGPResurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditGPResurveyCompanyModel;
  }

  ShowMandatoryInfoStrukturKreditGPModel _showMandatoryInfoStrukturKreditGPDakorModel;
  ShowMandatoryInfoStrukturKreditGPModel _showMandatoryInfoStrukturKreditGPDakorCompanyModel;
  void showMandatoryInfoStrukturKreditGPDakorModel(BuildContext context){
    _showMandatoryInfoStrukturKreditGPDakorModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditGPDakorModel;
    _showMandatoryInfoStrukturKreditGPDakorCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditGPDakorCompanyModel;
  }

  bool isGpTypeSelectedVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditGPIdeModel.isGpTypeSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditGPRegulerSurveyModel.isGpTypeSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditGPPacModel.isGpTypeSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditGPResurveyModel.isGpTypeSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditGPDakorModel.isGpTypeSelectedVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditGPIdeCompanyModel.isGpTypeSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditGPRegulerSurveyCompanyModel.isGpTypeSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditGPPacCompanyModel.isGpTypeSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditGPResurveyCompanyModel.isGpTypeSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditGPDakorCompanyModel.isGpTypeSelectedVisible;
      }
    }
    return value;
  }

  bool isInstallmentSelectedGPVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditGPIdeModel.isInstallmentSelectedGPVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditGPRegulerSurveyModel.isInstallmentSelectedGPVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditGPPacModel.isInstallmentSelectedGPVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditGPResurveyModel.isInstallmentSelectedGPVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditGPDakorModel.isInstallmentSelectedGPVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditGPIdeCompanyModel.isInstallmentSelectedGPVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditGPRegulerSurveyCompanyModel.isInstallmentSelectedGPVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditGPPacCompanyModel.isInstallmentSelectedGPVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditGPResurveyCompanyModel.isInstallmentSelectedGPVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditGPDakorCompanyModel.isInstallmentSelectedGPVisible;
      }
    }
    return value;
  }

  bool isNewTenorVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditGPIdeModel.isNewTenorVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditGPRegulerSurveyModel.isNewTenorVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditGPPacModel.isNewTenorVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditGPResurveyModel.isNewTenorVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditGPDakorModel.isNewTenorVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditGPIdeCompanyModel.isNewTenorVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditGPRegulerSurveyCompanyModel.isNewTenorVisible;
      } else if (_lastKnownState == "AOS") {
        value = _showMandatoryInfoStrukturKreditGPPacCompanyModel.isNewTenorVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditGPResurveyCompanyModel.isNewTenorVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditGPDakorCompanyModel.isNewTenorVisible;
      }
    }
    return value;
  }

  bool isGpTypeSelectedMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditGPIdeModel.isGpTypeSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditGPRegulerSurveyModel.isGpTypeSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditGPPacModel.isGpTypeSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditGPResurveyModel.isGpTypeSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditGPDakorModel.isGpTypeSelectedMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditGPIdeCompanyModel.isGpTypeSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditGPRegulerSurveyCompanyModel.isGpTypeSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditGPPacCompanyModel.isGpTypeSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditGPResurveyCompanyModel.isGpTypeSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditGPDakorCompanyModel.isGpTypeSelectedMandatory;
      }
    }
    return value;
  }

  bool isInstallmentSelectedGPMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditGPIdeModel.isInstallmentSelectedGPMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditGPRegulerSurveyModel.isInstallmentSelectedGPMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditGPPacModel.isInstallmentSelectedGPMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditGPResurveyModel.isInstallmentSelectedGPMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditGPDakorModel.isInstallmentSelectedGPMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditGPIdeCompanyModel.isInstallmentSelectedGPMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditGPRegulerSurveyCompanyModel.isInstallmentSelectedGPMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditGPPacCompanyModel.isInstallmentSelectedGPMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditGPResurveyCompanyModel.isInstallmentSelectedGPMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditGPDakorCompanyModel.isInstallmentSelectedGPMandatory;
      }
    }
    return value;
  }

  bool isNewTenorMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditGPIdeModel.isNewTenorMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditGPRegulerSurveyModel.isNewTenorMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditGPPacModel.isNewTenorMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditGPResurveyModel.isNewTenorMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditGPDakorModel.isNewTenorMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditGPIdeCompanyModel.isNewTenorMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditGPRegulerSurveyCompanyModel.isNewTenorMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditGPPacCompanyModel.isNewTenorMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditGPResurveyCompanyModel.isNewTenorMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditGPDakorCompanyModel.isNewTenorMandatory;
      }
    }
    return value;
  }

  /*
  *Irregular
  **/
  int _valuePeriodOfTimeSelected = 0;

  int get valuePeriodOfTimeSelected => _valuePeriodOfTimeSelected;
  List<InfoCreditStructureIrregularModel> _listInfoCreditStructureIrregularModel = [];

  set valuePeriodOfTimeSelected(int value) {
    this._valuePeriodOfTimeSelected = value;
    notifyListeners();
  }

  List<InfoCreditStructureIrregularModel>
      get listInfoCreditStructureIrregularModel =>
          _listInfoCreditStructureIrregularModel;

  Future<void> setValue(BuildContext context) async{
    _valuePeriodOfTimeSelected = int.parse(Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).periodOfTimeSelected);
    this._listInfoCreditStructureIrregularModel.clear();
    for(int i=0; i < _valuePeriodOfTimeSelected; i++){
      _listInfoCreditStructureIrregularModel.add(
          InfoCreditStructureIrregularModel(TextEditingController(),true,false,GlobalKey<FormState>())
      );
    }
  }

  void checkIrregular(int index){
    final _form = this._listInfoCreditStructureIrregularModel[index].keyIrregular.currentState;
    if(_form.validate()){
      setDisableTf(index);
    }
    else{
      this._listInfoCreditStructureIrregularModel[index].autoValidate = true;
      notifyListeners();
    }
  }

  void setEnableTf(int index){
    this._listInfoCreditStructureIrregularModel[index].isEnable = false;
    notifyListeners();
  }

  void setDisableTf(int index){
    print(this._listInfoCreditStructureIrregularModel[index].controller.text);
    this._listInfoCreditStructureIrregularModel[index].isEnable = true;
    notifyListeners();
  }

  ShowMandatoryInfoStrukturKreditIrregulerModel _showMandatoryInfoStrukturKreditIrregulerIdeModel;
  ShowMandatoryInfoStrukturKreditIrregulerModel _showMandatoryInfoStrukturKreditIrregulerIdeCompanyModel;
  void showMandatoryInfoStrukturKreditIrregulerIdeModel(BuildContext context){
    _showMandatoryInfoStrukturKreditIrregulerIdeModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditIrregulerIdeModel;
    _showMandatoryInfoStrukturKreditIrregulerIdeCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditIrregulerIdeCompanyModel;
  }

  ShowMandatoryInfoStrukturKreditIrregulerModel _showMandatoryInfoStrukturKreditIrregulerRegulerSurveyModel;
  ShowMandatoryInfoStrukturKreditIrregulerModel _showMandatoryInfoStrukturKreditIrregulerRegulerSurveyCompanyModel;
  void showMandatoryInfoStrukturKreditIrregulerRegulerSurveyModel(BuildContext context){
    _showMandatoryInfoStrukturKreditIrregulerRegulerSurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditIrregulerRegulerSurveyModel;
    _showMandatoryInfoStrukturKreditIrregulerRegulerSurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditIrregulerRegulerSurveyCompanyModel;
  }

  ShowMandatoryInfoStrukturKreditIrregulerModel _showMandatoryInfoStrukturKreditIrregulerPacModel;
  ShowMandatoryInfoStrukturKreditIrregulerModel _showMandatoryInfoStrukturKreditIrregulerPacCompanyModel;
  void showMandatoryInfoStrukturKreditIrregulerPacModel(BuildContext context){
    _showMandatoryInfoStrukturKreditIrregulerPacModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditIrregulerPacModel;
    _showMandatoryInfoStrukturKreditIrregulerPacCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditIrregulerPacCompanyModel;
  }

  ShowMandatoryInfoStrukturKreditIrregulerModel _showMandatoryInfoStrukturKreditIrregulerResurveyModel;
  ShowMandatoryInfoStrukturKreditIrregulerModel _showMandatoryInfoStrukturKreditIrregulerResurveyCompanyModel;
  void showMandatoryInfoStrukturKreditIrregulerResurveyModel(BuildContext context){
    _showMandatoryInfoStrukturKreditIrregulerResurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditIrregulerResurveyModel;
    _showMandatoryInfoStrukturKreditIrregulerResurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditIrregulerResurveyCompanyModel;
  }

  ShowMandatoryInfoStrukturKreditIrregulerModel _showMandatoryInfoStrukturKreditIrregulerDakorModel;
  ShowMandatoryInfoStrukturKreditIrregulerModel _showMandatoryInfoStrukturKreditIrregulerDakorCompanyModel;
  void showMandatoryInfoStrukturKreditIrregulerDakorModel(BuildContext context){
    _showMandatoryInfoStrukturKreditIrregulerDakorModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditIrregulerDakorModel;
    _showMandatoryInfoStrukturKreditIrregulerDakorCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditIrregulerDakorCompanyModel;
  }

  bool isControllerVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIrregulerIdeModel.isControllerVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditIrregulerRegulerSurveyModel.isControllerVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditIrregulerPacModel.isControllerVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditIrregulerResurveyModel.isControllerVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditIrregulerDakorModel.isControllerVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIrregulerIdeCompanyModel.isControllerVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditIrregulerRegulerSurveyCompanyModel.isControllerVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditIrregulerPacCompanyModel.isControllerVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditIrregulerResurveyCompanyModel.isControllerVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditIrregulerDakorCompanyModel.isControllerVisible;
      }
    }
    return value;
  }

  bool isControllerMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIrregulerIdeModel.isControllerMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditIrregulerRegulerSurveyModel.isControllerMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditIrregulerPacModel.isControllerMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditIrregulerResurveyModel.isControllerMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditIrregulerDakorModel.isControllerMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditIrregulerIdeCompanyModel.isControllerMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditIrregulerRegulerSurveyCompanyModel.isControllerMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditIrregulerPacCompanyModel.isControllerMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditIrregulerResurveyCompanyModel.isControllerMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditIrregulerDakorCompanyModel.isControllerMandatory;
      }
    }
    return value;
  }

  /*
  *BP
  **/
  int _radioValueBalloonPHOrBalloonInstallment = 0;
  bool _autoValidateBP = false;
  bool _visibleTfPercentageInstallment = true;

  int get radioValueBalloonPHOrBalloonInstallment =>
      _radioValueBalloonPHOrBalloonInstallment;

  set radioValueBalloonPHOrBalloonInstallment(int value) {
    this._radioValueBalloonPHOrBalloonInstallment = value;
    setVisibleTfPercentageInstallment(value);
    notifyListeners();
  }

  TextEditingController _controllerInstallmentPercentage = TextEditingController();

  TextEditingController _controllerInstallmentValue = TextEditingController();

  TextEditingController get controllerInstallmentPercentage =>
      _controllerInstallmentPercentage;

  TextEditingController get controllerInstallmentValue =>
      _controllerInstallmentValue;

  bool get autoValidateBP => _autoValidateBP;

  set autoValidateBP(bool value) {
    this._autoValidateBP = value;
    notifyListeners();
  }

  bool get visibleTfPercentageInstallment => _visibleTfPercentageInstallment;

  set visibleTfPercentageInstallment(bool value) {
    this._visibleTfPercentageInstallment = value;
    notifyListeners();
  }

  void checkBP(BuildContext context) {
    var _providerCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false);
    if(this.radioValueBalloonPHOrBalloonInstallment == 0) {
      double _nilaiAngsuran = double.parse(_providerCreditStructure.controllerTotalLoan.text.replaceAll(",", "")) * (double.parse(this._controllerInstallmentPercentage.text) / 100);
      this._controllerInstallmentValue.text = formatCurrency.formatCurrency2(_nilaiAngsuran.toString());
    }
    Navigator.pop(context);
  }

  void formattingBP() {
    this._controllerInstallmentValue.text = formatCurrency.formatCurrency2(this._controllerInstallmentValue.text);
  }

  void setVisibleTfPercentageInstallment(int flag){
    if(flag == 0){
      visibleTfPercentageInstallment = true;
    }
    else{
      visibleTfPercentageInstallment = false;
    }
  }

  void calculatePresentaseOrNilai(BuildContext context, int flag) {
    var _providerCreditStructure = Provider.of<InfoCreditStructureChangeNotifier>(context, listen: false);
    if(this._controllerInstallmentPercentage.text.isNotEmpty && this._controllerInstallmentValue.text.isEmpty) {
      print("calculate 1");
      double _nilaiAngsuran = double.parse(_providerCreditStructure.controllerTotalLoan.text.replaceAll(",", "")) * (double.parse(this._controllerInstallmentPercentage.text) / 100);
      this._controllerInstallmentValue.text = formatCurrency.formatCurrency2(_nilaiAngsuran.toString());
    } else if(this._controllerInstallmentPercentage.text.isEmpty && this._controllerInstallmentValue.text.isNotEmpty) {
      print("calculate 2");
      double _nilaiAngsuran = (double.parse(this._controllerInstallmentValue.text) / double.parse(_providerCreditStructure.controllerTotalLoan.text.replaceAll(",", ""))) * 100;
      this._controllerInstallmentPercentage.text = _nilaiAngsuran.toString();
    } else if(this._controllerInstallmentPercentage.text.isNotEmpty && this._controllerInstallmentValue.text.isNotEmpty) {
      if(flag == 1) {
        print("calculate 3");
        double _nilaiAngsuran = double.parse(_providerCreditStructure.controllerTotalLoan.text.replaceAll(",", "")) * (double.parse(this._controllerInstallmentPercentage.text) / 100);
        this._controllerInstallmentValue.text = formatCurrency.formatCurrency2(_nilaiAngsuran.toString());
      } else if(flag == 2) {
        print("calculate 4");
        double _nilaiAngsuran = (double.parse(this._controllerInstallmentValue.text) / double.parse(_providerCreditStructure.controllerTotalLoan.text.replaceAll(",", ""))) * 100;
        this._controllerInstallmentPercentage.text = _nilaiAngsuran.toString();
        this._controllerInstallmentValue.text = formatCurrency.formatCurrency2(this._controllerInstallmentValue.text);
      }
    }
    notifyListeners();
  }

  ShowMandatoryInfoStrukturKreditBPModel _showMandatoryInfoStrukturKreditBPIdeModel;
  ShowMandatoryInfoStrukturKreditBPModel _showMandatoryInfoStrukturKreditBPIdeCompanyModel;
  void showMandatoryInfoStrukturKreditBPIdeModel(BuildContext context){
    _showMandatoryInfoStrukturKreditBPIdeModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditBPIdeModel;
    _showMandatoryInfoStrukturKreditBPIdeCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditBPIdeCompanyModel;
  }

  ShowMandatoryInfoStrukturKreditBPModel _showMandatoryInfoStrukturKreditBPRegulerSurveyModel;
  ShowMandatoryInfoStrukturKreditBPModel _showMandatoryInfoStrukturKreditBPRegulerSurveyCompanyModel;
  void showMandatoryInfoStrukturKreditBPRegulerSurveyModel(BuildContext context){
    _showMandatoryInfoStrukturKreditBPRegulerSurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditBPRegulerSurveyModel;
    _showMandatoryInfoStrukturKreditBPRegulerSurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditBPRegulerSurveyCompanyModel;
  }

  ShowMandatoryInfoStrukturKreditBPModel _showMandatoryInfoStrukturKreditBPPacModel;
  ShowMandatoryInfoStrukturKreditBPModel _showMandatoryInfoStrukturKreditBPPacCompanyModel;
  void showMandatoryInfoStrukturKreditBPPacModel(BuildContext context){
    _showMandatoryInfoStrukturKreditBPPacModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditBPPacModel;
    _showMandatoryInfoStrukturKreditBPPacCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditBPPacCompanyModel;
  }

  ShowMandatoryInfoStrukturKreditBPModel _showMandatoryInfoStrukturKreditBPResurveyModel;
  ShowMandatoryInfoStrukturKreditBPModel _showMandatoryInfoStrukturKreditBPResurveyCompanyModel;
  void showMandatoryInfoStrukturKreditBPResurveyModel(BuildContext context){
    _showMandatoryInfoStrukturKreditBPResurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditBPResurveyModel;
    _showMandatoryInfoStrukturKreditBPResurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditBPResurveyCompanyModel;
  }

  ShowMandatoryInfoStrukturKreditBPModel _showMandatoryInfoStrukturKreditBPDakorModel;
  ShowMandatoryInfoStrukturKreditBPModel _showMandatoryInfoStrukturKreditBPDakorCompanyModel;
  void showMandatoryInfoStrukturKreditBPDakorModel(BuildContext context){
    _showMandatoryInfoStrukturKreditBPDakorModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditBPDakorModel;
    _showMandatoryInfoStrukturKreditBPDakorCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditBPDakorCompanyModel;
  }

  bool isRadioValueBalloonPHOrBalloonInstallmentVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditBPIdeModel.isRadioValueBalloonPHOrBalloonInstallmentVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditBPRegulerSurveyModel.isRadioValueBalloonPHOrBalloonInstallmentVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditBPPacModel.isRadioValueBalloonPHOrBalloonInstallmentVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditBPResurveyModel.isRadioValueBalloonPHOrBalloonInstallmentVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditBPDakorModel.isRadioValueBalloonPHOrBalloonInstallmentVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditBPIdeCompanyModel.isRadioValueBalloonPHOrBalloonInstallmentVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditBPRegulerSurveyCompanyModel.isRadioValueBalloonPHOrBalloonInstallmentVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditBPPacCompanyModel.isRadioValueBalloonPHOrBalloonInstallmentVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditBPResurveyCompanyModel.isRadioValueBalloonPHOrBalloonInstallmentVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditBPDakorCompanyModel.isRadioValueBalloonPHOrBalloonInstallmentVisible;
      }
    }
    return value;
  }

  bool isInstallmentPercentageVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditBPIdeModel.isInstallmentPercentageVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditBPRegulerSurveyModel.isInstallmentPercentageVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditBPPacModel.isInstallmentPercentageVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditBPResurveyModel.isInstallmentPercentageVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditBPDakorModel.isInstallmentPercentageVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditBPIdeCompanyModel.isInstallmentPercentageVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditBPRegulerSurveyCompanyModel.isInstallmentPercentageVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditBPPacCompanyModel.isInstallmentPercentageVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditBPResurveyCompanyModel.isInstallmentPercentageVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditBPDakorCompanyModel.isInstallmentPercentageVisible;
      }
    }
    return value;
  }

  bool isInstallmentValueVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditBPIdeModel.isInstallmentValueVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditBPRegulerSurveyModel.isInstallmentValueVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditBPPacModel.isInstallmentValueVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditBPResurveyModel.isInstallmentValueVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditBPDakorModel.isInstallmentValueVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditBPIdeCompanyModel.isInstallmentValueVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditBPRegulerSurveyCompanyModel.isInstallmentValueVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditBPPacCompanyModel.isInstallmentValueVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditBPResurveyCompanyModel.isInstallmentValueVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditBPDakorCompanyModel.isInstallmentValueVisible;
      }
    }
    return value;
  }

  bool isRadioValueBalloonPHOrBalloonInstallmentMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditBPIdeModel.isRadioValueBalloonPHOrBalloonInstallmentMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditBPRegulerSurveyModel.isRadioValueBalloonPHOrBalloonInstallmentMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditBPPacModel.isRadioValueBalloonPHOrBalloonInstallmentMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditBPResurveyModel.isRadioValueBalloonPHOrBalloonInstallmentMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditBPDakorModel.isRadioValueBalloonPHOrBalloonInstallmentMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditBPIdeCompanyModel.isRadioValueBalloonPHOrBalloonInstallmentMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditBPRegulerSurveyCompanyModel.isRadioValueBalloonPHOrBalloonInstallmentMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditBPPacCompanyModel.isRadioValueBalloonPHOrBalloonInstallmentMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditBPResurveyCompanyModel.isRadioValueBalloonPHOrBalloonInstallmentMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditBPDakorCompanyModel.isRadioValueBalloonPHOrBalloonInstallmentMandatory;
      }
    }
    return value;
  }

  bool isInstallmentPercentageMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditBPIdeModel.isInstallmentPercentageMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditBPRegulerSurveyModel.isInstallmentPercentageMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditBPPacModel.isInstallmentPercentageMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditBPResurveyModel.isInstallmentPercentageMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditBPDakorModel.isInstallmentPercentageMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditBPIdeCompanyModel.isInstallmentPercentageMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditBPRegulerSurveyCompanyModel.isInstallmentPercentageMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditBPPacCompanyModel.isInstallmentPercentageMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditBPResurveyCompanyModel.isInstallmentPercentageMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditBPDakorCompanyModel.isInstallmentPercentageMandatory;
      }
    }
    return value;
  }

  bool isInstallmentValueMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditBPIdeModel.isInstallmentValueMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditBPRegulerSurveyModel.isInstallmentValueMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditBPPacModel.isInstallmentValueMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditBPResurveyModel.isInstallmentValueMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditBPDakorModel.isInstallmentValueMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditBPIdeCompanyModel.isInstallmentValueMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditBPRegulerSurveyCompanyModel.isInstallmentValueMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditBPPacCompanyModel.isInstallmentValueMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditBPResurveyCompanyModel.isInstallmentValueMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditBPDakorCompanyModel.isInstallmentValueMandatory;
      }
    }
    return value;
  }

  /*
  *Stepping
  **/
  List<String> _lisTotalStepping = [];
  String _totalSteppingSelected;
  List<InfoCreditStructureSteppingModel> _listInfoCreditStructureStepping = [];

  List<String> get lisTotalStepping => _lisTotalStepping;

  String get totalSteppingSelected => _totalSteppingSelected;

  set totalSteppingSelected(String value) {
    this._totalSteppingSelected = value;
    notifyListeners();
  }

  List<InfoCreditStructureSteppingModel> get listInfoCreditStructureStepping =>
      _listInfoCreditStructureStepping;

  set listInfoCreditStructureStepping(
      List<InfoCreditStructureSteppingModel> value) {
    this._listInfoCreditStructureStepping = value;
    notifyListeners();
  }

  Future<void> setValueTotalStepping(BuildContext context) async{
    int _totalStepping = int.parse(Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).periodOfTimeSelected);

    this._lisTotalStepping.clear();
    for(int i = 1; i <= _totalStepping; i++){
      if(_totalStepping % i == 0){
        _lisTotalStepping.add(i.toString());
      }
    }
  }

  void setListDataTotalStepping(String value,BuildContext context){
    int _totalStepping = int.parse(Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false)
        .periodOfTimeSelected);
    int _dataTotalStepping = int.parse(value);
    String _value1 = "";
    int _firstValue = 1;
    double _secondValue = _totalStepping/_dataTotalStepping;
    this._listInfoCreditStructureStepping.clear();
    for(int i=1; i <= _dataTotalStepping; i++){
      if(_totalStepping/_dataTotalStepping == 1){
        _value1 = (i).toString();
      }
      else{
        if(i==1){
          _value1 = "$_firstValue-${_secondValue.floor()}";
        }
        else{
          _value1 = "${_firstValue+=(_totalStepping/_dataTotalStepping).floor()}-${(_secondValue +=(_totalStepping/_dataTotalStepping)).floor()}";
        }
      }
      _listInfoCreditStructureStepping.add(InfoCreditStructureSteppingModel(TextEditingController(), TextEditingController(), true, _value1));
    }
  }

  void clearStepping(){
    this._listInfoCreditStructureStepping.clear();
    this._totalSteppingSelected = null;
  }

  ShowMandatoryInfoStrukturKreditSteppingModel _showMandatoryInfoStrukturKreditSteppingIdeModel;
  ShowMandatoryInfoStrukturKreditSteppingModel _showMandatoryInfoStrukturKreditSteppingIdeCompanyModel;
  void showMandatoryInfoStrukturKreditSteppingIdeModel(BuildContext context){
    _showMandatoryInfoStrukturKreditSteppingIdeModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditSteppingIdeModel;
    _showMandatoryInfoStrukturKreditSteppingIdeCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditSteppingIdeCompanyModel;
  }

  ShowMandatoryInfoStrukturKreditSteppingModel _showMandatoryInfoStrukturKreditSteppingRegulerSurveyModel;
  ShowMandatoryInfoStrukturKreditSteppingModel _showMandatoryInfoStrukturKreditSteppingRegulerSurveyCompanyModel;
  void showMandatoryInfoStrukturKreditSteppingRegulerSurveyModel(BuildContext context){
    _showMandatoryInfoStrukturKreditSteppingRegulerSurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditSteppingRegulerSurveyModel;
    _showMandatoryInfoStrukturKreditSteppingRegulerSurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditSteppingRegulerSurveyCompanyModel;
  }

  ShowMandatoryInfoStrukturKreditSteppingModel _showMandatoryInfoStrukturKreditSteppingPacModel;
  ShowMandatoryInfoStrukturKreditSteppingModel _showMandatoryInfoStrukturKreditSteppingPacCompanyModel;
  void showMandatoryInfoStrukturKreditSteppingPacModel(BuildContext context){
    _showMandatoryInfoStrukturKreditSteppingPacModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditSteppingPacModel;
    _showMandatoryInfoStrukturKreditSteppingPacCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditSteppingPacCompanyModel;
  }

  ShowMandatoryInfoStrukturKreditSteppingModel _showMandatoryInfoStrukturKreditSteppingResurveyModel;
  ShowMandatoryInfoStrukturKreditSteppingModel _showMandatoryInfoStrukturKreditSteppingResurveyCompanyModel;
  void showMandatoryInfoStrukturKreditSteppingResurveyModel(BuildContext context){
    _showMandatoryInfoStrukturKreditSteppingResurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditSteppingResurveyModel;
    _showMandatoryInfoStrukturKreditSteppingResurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditSteppingResurveyCompanyModel;
  }

  ShowMandatoryInfoStrukturKreditSteppingModel _showMandatoryInfoStrukturKreditSteppingDakorModel;
  ShowMandatoryInfoStrukturKreditSteppingModel _showMandatoryInfoStrukturKreditSteppingDakorCompanyModel;
  void showMandatoryInfoStrukturKreditSteppingDakorModel(BuildContext context){
    _showMandatoryInfoStrukturKreditSteppingDakorModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditSteppingDakorModel;
    _showMandatoryInfoStrukturKreditSteppingDakorCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoStrukturKreditSteppingDakorCompanyModel;
  }

  bool isTotalSteppingSelectedVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditSteppingIdeModel.isTotalSteppingSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditSteppingRegulerSurveyModel.isTotalSteppingSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditSteppingPacModel.isTotalSteppingSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditSteppingResurveyModel.isTotalSteppingSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditSteppingDakorModel.isTotalSteppingSelectedVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditSteppingIdeCompanyModel.isTotalSteppingSelectedVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditSteppingRegulerSurveyCompanyModel.isTotalSteppingSelectedVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditSteppingPacCompanyModel.isTotalSteppingSelectedVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditSteppingResurveyCompanyModel.isTotalSteppingSelectedVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditSteppingDakorCompanyModel.isTotalSteppingSelectedVisible;
      }
    }
    return value;
  }

  bool isPercentageVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditSteppingIdeModel.isPercentageVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditSteppingRegulerSurveyModel.isPercentageVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditSteppingPacModel.isPercentageVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditSteppingResurveyModel.isPercentageVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditSteppingDakorModel.isPercentageVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditSteppingIdeCompanyModel.isPercentageVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditSteppingRegulerSurveyCompanyModel.isPercentageVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditSteppingPacCompanyModel.isPercentageVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditSteppingResurveyCompanyModel.isPercentageVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditSteppingDakorCompanyModel.isPercentageVisible;
      }
    }
    return value;
  }

  bool isValueVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditSteppingIdeModel.isValueVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditSteppingRegulerSurveyModel.isValueVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditSteppingPacModel.isValueVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditSteppingResurveyModel.isValueVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditSteppingDakorModel.isValueVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditSteppingIdeCompanyModel.isValueVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditSteppingRegulerSurveyCompanyModel.isValueVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditSteppingPacCompanyModel.isValueVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditSteppingResurveyCompanyModel.isValueVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditSteppingDakorCompanyModel.isValueVisible;
      }
    }
    return value;
  }

  bool isTotalSteppingSelectedMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditSteppingIdeModel.isTotalSteppingSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditSteppingRegulerSurveyModel.isTotalSteppingSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditSteppingPacModel.isTotalSteppingSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditSteppingResurveyModel.isTotalSteppingSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditSteppingDakorModel.isTotalSteppingSelectedMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditSteppingIdeCompanyModel.isTotalSteppingSelectedMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditSteppingRegulerSurveyCompanyModel.isTotalSteppingSelectedMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditSteppingPacCompanyModel.isTotalSteppingSelectedMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditSteppingResurveyCompanyModel.isTotalSteppingSelectedMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditSteppingDakorCompanyModel.isTotalSteppingSelectedMandatory;
      }
    }
    return value;
  }

  bool isPercentageMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditSteppingIdeModel.isPercentageMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditSteppingRegulerSurveyModel.isPercentageMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditSteppingPacModel.isPercentageMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditSteppingResurveyModel.isPercentageMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditSteppingDakorModel.isPercentageMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditSteppingIdeCompanyModel.isPercentageMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditSteppingRegulerSurveyCompanyModel.isPercentageMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditSteppingPacCompanyModel.isPercentageMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditSteppingResurveyCompanyModel.isPercentageMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditSteppingDakorCompanyModel.isPercentageMandatory;
      }
    }
    return value;
  }

  bool isValueMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditSteppingIdeModel.isValueMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditSteppingRegulerSurveyModel.isValueMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditSteppingPacModel.isValueMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditSteppingResurveyModel.isValueMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditSteppingDakorModel.isValueMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoStrukturKreditSteppingIdeCompanyModel.isValueMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoStrukturKreditSteppingRegulerSurveyCompanyModel.isValueMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoStrukturKreditSteppingPacCompanyModel.isValueMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoStrukturKreditSteppingResurveyCompanyModel.isValueMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoStrukturKreditSteppingDakorCompanyModel.isValueMandatory;
      }
    }
    return value;
  }

  void clearData(){
    this._listInfoCreditStructureFloatingModel.clear();
    this._listInfoCreditStructureGPModel.clear();
    clearStepping();
    clearDialogFloating();
    clearDialogGP();
    this._listInfoCreditStructureIrregularModel.clear();
    this. _controllerInstallmentPercentage.clear();
    this._controllerInstallmentValue.clear();
    isDisablePACIAAOSCONA = false;
  }

  void updateMS2ApplObjectInfStrukturKreditGP() async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();

    _dbHelper.updateMS2ApplObjectInfStrukturKreditGP(MS2ApplObjectModel(
      "1",
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      this._gpTypeSelected != null ? int.parse(this._gpTypeSelected.id) : 0,
      this._controllerNewTenor.text,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      DateTime.now().toString(),
      _preferences.getString("username"),
      1,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,null, null, null, null, null, null, null
    ));
  }

  void saveToSQLiteInfStrukturKreditStepping() async {
    List<MS2ApplLoanDetailModel> _listApplLoanDetail = [];
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    for(int i=0; i<_listInfoCreditStructureStepping.length; i++){
      _listApplLoanDetail.add(MS2ApplLoanDetailModel(
        "1",
        null,
        this._listInfoCreditStructureStepping[i].controllerPercentage.text != "" ? int.parse(this._listInfoCreditStructureStepping[i].controllerPercentage.text) : 0,
        this._listInfoCreditStructureStepping[i].controllerValue.text != "" ? double.parse(this._listInfoCreditStructureStepping[i].controllerValue.text) : 0,
        1,
        DateTime.now().toString(),
        _preferences.getString("username"),
        null,
        null,
      ));
    }
    _dbHelper.insertMS2ApplLoanDetailStepping(_listApplLoanDetail);
  }

  void saveToSQLiteInfStrukturKreditIrreguler() async {
    List<MS2ApplLoanDetailModel> _listApplLoanDetail = [];
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    for(int i=0; i<_listInfoCreditStructureIrregularModel.length; i++){
      _listApplLoanDetail.add(MS2ApplLoanDetailModel(
        "1",
        i+1,
        null,
        this._listInfoCreditStructureIrregularModel[i].controller.text != "" ? double.parse(this._listInfoCreditStructureIrregularModel[i].controller.text) : 0,
        1,
        DateTime.now().toString(),
        _preferences.getString("username"),
        null,
        null,
      ));
    }
    _dbHelper.insertMS2ApplLoanDetailIrreguler(_listApplLoanDetail);
  }

  void updateMS2ApplObjectInfStrukturKreditBP() async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();

    _dbHelper.updateMS2ApplObjectInfStrukturKreditBP(MS2ApplObjectModel(
      "1",
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      this._radioValueBalloonPHOrBalloonInstallment.toString(),
      this._controllerInstallmentPercentage.text != "" ? double.parse(this._controllerInstallmentPercentage.text.replaceAll(",", "")) : 0,
      this._controllerInstallmentValue.text != "" ? double.parse(this._controllerInstallmentValue.text.replaceAll(",", "")) : 0,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      DateTime.now().toString(),
      _preferences.getString("username"),
      1,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,null, null, null, null, null, null, null
    ));
  }

  void updateMS2ApplObjectInfStrukturStepping() async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();

    _dbHelper.updateMS2ApplObjectInfStrukturStepping(MS2ApplObjectModel(
      "1",
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      this.totalSteppingSelected != null ? int.parse(this.totalSteppingSelected) : 0,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      DateTime.now().toString(),
      _preferences.getString("username"),
      1,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null
    ));
  }

  // Floating
  // GP
  bool _isGpTypeSelectedChanges = false;
  bool _isNewTenorChanges = false;
  // Stepping
  bool _isTotalSteppingSelectedChanges = false;
  bool _isPercentageChanges = false;
  bool _isValueChanges = false;
  // Irreguler
  bool _isControllerChanges = false;
  // BP
  bool _isRadioValueBalloonPHOrBalloonInstallmentChanges = false;
  bool _isInstallmentPercentage = false;
  bool _isInstallmentValue = false;

  Future<void> checkDataDakor() async{
    // Floating

    // GP
    var _gpHeader = await _dbHelper.selectMS2ApplObject();
    if(_gpHeader.isNotEmpty){
      _isGpTypeSelectedChanges = this._gpTypeSelected.id != _gpHeader[0]['gp_type'] || _gpHeader[0]['edit_gp_type'] == "1";
      _isNewTenorChanges = this._controllerNewTenor.text != _gpHeader[0]['new_tenor'] || _gpHeader[0]['edit_new_tenor'] == "1";
    }

    // Stepping
    var _steppingHeader = await _dbHelper.selectMS2ApplObject();
    var _steppingDetail = await _dbHelper.selectMS2ApplLoanDetail();
    if(_steppingHeader.isNotEmpty){
      _isTotalSteppingSelectedChanges = this._totalSteppingSelected != _steppingHeader[0]['total_stepping'] || _steppingHeader[0]['edit_total_stepping'] == "1";
    }
    if(_steppingDetail.isNotEmpty){
      for(int s = 0; s < _listInfoCreditStructureStepping.length; s++) {
        _isPercentageChanges = this._listInfoCreditStructureStepping[s].controllerPercentage != _steppingDetail[s]['ac_percentage'] || _steppingDetail[s]['edit_ac_percentage'];
        _isValueChanges = this._listInfoCreditStructureStepping[s].controllerValue != _steppingDetail[s]['ac_amount'] || _steppingDetail[s]['edit_ac_amount'];
      }
    }

    // Irreguler
    var _irregulerDetail = await _dbHelper.selectMS2ApplLoanDetail();
    if(_irregulerDetail.isNotEmpty){
      for(int i = 0; i < _listInfoCreditStructureIrregularModel.length; i++) {
        _isControllerChanges = this._listInfoCreditStructureIrregularModel[i].controller != _irregulerDetail[i]['ac_amount'] || _irregulerDetail[i]['edit_ac_amount'];
      }
    }

    // BP
    var _bpHeader = await _dbHelper.selectMS2ApplObject();
    if(_bpHeader.isNotEmpty){
      _isRadioValueBalloonPHOrBalloonInstallmentChanges = this._radioValueBalloonPHOrBalloonInstallment != _bpHeader[0]['balloon_type'] || _bpHeader[0]['edit_balloon_type'] == "1";
      _isInstallmentPercentage = this._controllerInstallmentPercentage.text != _bpHeader[0]['last_percen_installment'] || _bpHeader[0]['edit_last_percen_installment'] == "1";
      _isInstallmentValue = this._controllerInstallmentPercentage.text != _bpHeader[0]['last_installment_amt'] || _bpHeader[0]['edit_last_installment_amt'] == "1";
    }
  }

  bool get isTotalSteppingSelectedChanges => _isTotalSteppingSelectedChanges;

  set isTotalSteppingSelectedChanges(bool value) {
    this._isTotalSteppingSelectedChanges = value;
    notifyListeners();
  }

  bool get isPercentageChanges => _isPercentageChanges;

  set isPercentageChanges(bool value) {
    this._isPercentageChanges = value;
    notifyListeners();
  }

  bool get isValueChanges => _isValueChanges;

  set isValueChanges(bool value) {
    this._isValueChanges = value;
    notifyListeners();
  }

  bool get isControllerChanges => _isControllerChanges;

  set isControllerChanges(bool value) {
    this._isControllerChanges = value;
    notifyListeners();
  }

  bool get isInstallmentValue => _isInstallmentValue;

  set isInstallmentValue(bool value) {
    this._isInstallmentValue = value;
    notifyListeners();
  }

  bool get isInstallmentPercentage => _isInstallmentPercentage;

  set isInstallmentPercentage(bool value) {
    this._isInstallmentPercentage = value;
    notifyListeners();
  }

  bool get isRadioValueBalloonPHOrBalloonInstallmentChanges =>
      _isRadioValueBalloonPHOrBalloonInstallmentChanges;

  set isRadioValueBalloonPHOrBalloonInstallmentChanges(bool value) {
    this._isRadioValueBalloonPHOrBalloonInstallmentChanges = value;
    notifyListeners();
  }

  bool get isNewTenorChanges => _isNewTenorChanges;

  set isNewTenorChanges(bool value) {
    this._isNewTenorChanges = value;
    notifyListeners();
  }

  bool get isGpTypeSelectedChanges => _isGpTypeSelectedChanges;

  set isGpTypeSelectedChanges(bool value) {
    this._isGpTypeSelectedChanges = value;
    notifyListeners();
  }

  bool get isDisablePACIAAOSCONA => _isDisablePACIAAOSCONA;

  set isDisablePACIAAOSCONA(bool value) {
    this._isDisablePACIAAOSCONA = value;
    notifyListeners();
  }

  void limitInput(String value){
    if (int.parse(value) > 100) {
      Future.delayed(Duration(milliseconds: 100), () {
        this._controllerInstallmentPercentage.clear();
      });
    }
  }
}