import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_karoseri_model.dart';
import 'package:ad1ms2_dev/screens/search_company.dart';
import 'package:ad1ms2_dev/screens/search_karoseri.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_object_karoseri_change_notifier.dart';
import 'package:ad1ms2_dev/shared/dashboard/dashboard_change_notif.dart';
import 'package:ad1ms2_dev/shared/format_currency.dart';
import 'package:ad1ms2_dev/shared/regex_input_formatter.dart';
import 'package:ad1ms2_dev/shared/search_company_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_karoseri_change_notifier.dart';
import 'package:flutter/foundation.dart';
import 'package:ad1ms2_dev/models/company_model.dart';
import 'package:ad1ms2_dev/models/karoseri_model.dart';
import 'package:flutter/material.dart';
import 'package:ad1ms2_dev/models/karoseri_object_model.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';

class AddInfoObjectKaroseriChangeNotifier with ChangeNotifier{
    GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
    GlobalKey<FormState> _key = GlobalKey<FormState>();
    List<KaroseriObjectModel> _listKaroseriObject = [];
    int _radioPKSKaroseri = 0;
    FormatCurrency _formatCurrency = FormatCurrency();
    CompanyTypeModel _companySelected;
    TextEditingController _controllerCompany = TextEditingController();
    KaroseriModel _karoseriSelected;
    TextEditingController _controllerKaroseri = TextEditingController();
    TextEditingController _controllerJumlahKaroseri = TextEditingController();
    TextEditingController _controllerPrice = TextEditingController();
    TextEditingController _controllerTotalPrice = TextEditingController();
    bool _autoValidate = false;
    RegExInputFormatter _amountValidator = RegExInputFormatter.withRegex('^[0-9]{0,9}(\\.[0-9]{0,2})?\$');
    String _custType;
    String _lastKnownState;
    DbHelper _dbHelper = DbHelper();
    String _orderKaroseriID;
    bool _isDisablePACIAAOSCONA = false;

    bool _isEdit = false;
    bool _isRadioValuePKSKaroseriChanges = false;
    bool _isCompanyChanges = false;
    bool _isKaroseriChanges = false;
    bool _isJumlahKaroseriChanges = false;
    bool _isPriceChanges = false;
    bool _isTotalPriceChanges = false;

    List<KaroseriObjectModel> get listFormKaroseriObject => _listKaroseriObject;

    String get custType => _custType;

    set custType(String value) {
        this._custType = value;
        notifyListeners();
    }

    String get lastKnownState => _lastKnownState;

    set lastKnownState(String value) {
        this._lastKnownState = value;
        notifyListeners();
    }

    int get radioValuePKSKaroseri {
        return _radioPKSKaroseri;
    }

    set radioValuePKSKaroseri(int value) {
        this._radioPKSKaroseri = value;
        this._controllerJumlahKaroseri.text = "1";
        clearData();
        notifyListeners();
    }

    void searchCompany(BuildContext context) async {
        CompanyTypeModel _data = await Navigator.push(context, MaterialPageRoute(
                builder: (context) => ChangeNotifierProvider(
                    create: (context) => SearchCompanyChangeNotifier(),
                    child: SearchCompany())));
        if (_data != null) {
            this._companySelected = _data;
            this._controllerCompany.text = "${_data.id} - ${_data.desc}";
            notifyListeners();
        }
    }

    CompanyTypeModel get companyModelSelected => _companySelected;

    set companyModelSelected(CompanyTypeModel value) {
      _companySelected = value;
      notifyListeners();
    }

    TextEditingController get controllerCompany {
        return this._controllerCompany;
    }

    set controllerCompany(TextEditingController value) {
        this._controllerCompany = value;
        this.notifyListeners();
    }

    void searchKaroseri(BuildContext context, int radioPKSKaroseri) async {
        if(this._companySelected != null || this._radioPKSKaroseri == 2){
            KaroseriModel _data = await Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ChangeNotifierProvider(
                        create: (context) => SearchKaroseriChangeNotifier(),
                        child: SearchKaroseri(radioPKSKaroseri: radioPKSKaroseri, company: this._companySelected != null ? this._companySelected.id : ''))));
            if (_data != null) {
                this._karoseriSelected = _data;
                this._controllerKaroseri.text = "${_data.kode} - ${_data.deskripsi}";
                notifyListeners();
            }
        }
        else {
            showSnackBar("Perusahaan tidak boleh kosong");
        }
    }

    void clearData(){
        this._companySelected = null;
        this._controllerCompany.clear();
        this._karoseriSelected = null;
        this._controllerKaroseri.clear();
        this._controllerPrice.clear();
        this._controllerTotalPrice.clear();
        // isDisablePACIAAOSCONA = false;
    }

    KaroseriModel get karoseriSelected => _karoseriSelected;

    set karoseriSelected(KaroseriModel value) {
        _karoseriSelected = value;
        notifyListeners();
    }

    TextEditingController get controllerKaroseri {
        return this._controllerKaroseri;
    }

    set controllerKaroseri(TextEditingController value) {
        this._controllerKaroseri = value;
        this.notifyListeners();
    }

    TextEditingController get controllerJumlahKaroseri {
        return this._controllerJumlahKaroseri;
    }

    set controllerJumlahKaroseri(TextEditingController value) {
        this._controllerJumlahKaroseri = value;
        this.notifyListeners();
    }

    TextEditingController get controllerPrice {
        return this._controllerPrice;
    }

    set controllerPrice(TextEditingController value) {
        this._controllerPrice = value;
        this.notifyListeners();
    }

    TextEditingController get controllerTotalPrice {
        return this._controllerTotalPrice;
    }

    set controllerTotalPrice(TextEditingController value) {
        this._controllerTotalPrice = value;
        this.notifyListeners();
    }

    bool get autoValidate => _autoValidate;

    set autoValidate(bool value) {
        this._autoValidate = value;
        notifyListeners();
    }

    GlobalKey<FormState> get key => _key;

    String get orderKaroseriID => _orderKaroseriID;

    set orderKaroseriID(String value) {
      this._orderKaroseriID = value;
    }

    void check(BuildContext context, int flag, int index) {
        final _form = _key.currentState;
        calculatedTotalPrice();
        if (flag == 0) {
            if (_form.validate()) {
                Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false)
                    .addListInfoObjKaroseri(KaroseriObjectModel(
                    this.radioValuePKSKaroseri,
                    this._companySelected,
                    this._karoseriSelected,
                    this._controllerJumlahKaroseri.text,
                    this._controllerPrice.text,
                    this._controllerTotalPrice.text,
                    this._isEdit,
                    this._isRadioValuePKSKaroseriChanges,
                    this._isCompanyChanges,
                    this._isKaroseriChanges,
                    this._isJumlahKaroseriChanges,
                    this._isPriceChanges,
                    this._isTotalPriceChanges,
                    null
                ));
                Navigator.pop(context);
            } else {
                autoValidate = true;
            }
        } else {
            if (_form.validate()) {
                Provider.of<InfoObjectKaroseriChangeNotifier>(context, listen: false)
                    .updateListInfoObjKaroseri(KaroseriObjectModel(
                    this.radioValuePKSKaroseri,
                    this._companySelected,
                    this._karoseriSelected,
                    this._controllerJumlahKaroseri.text,
                    this._controllerPrice.text,
                    this._controllerTotalPrice.text,
                    this._isEdit,
                    this._isRadioValuePKSKaroseriChanges,
                    this._isCompanyChanges,
                    this._isKaroseriChanges,
                    this._isJumlahKaroseriChanges,
                    this._isPriceChanges,
                    this._isTotalPriceChanges,
                    this._orderKaroseriID
                ), context, index);
                Navigator.pop(context);
            } else {
                autoValidate = true;
            }
            checkDataDakor(index);
        }
    }

    void calculatedTotalPrice() {
        var _jumlahKaroseri = _controllerJumlahKaroseri.text.isNotEmpty ? double.parse(_controllerJumlahKaroseri.text.replaceAll(",", "")) : 0.0;
        var _price = _controllerPrice.text.isNotEmpty ? double.parse(_controllerPrice.text.replaceAll(",", "")) : 0.0;
        var _totalPrice = _price * _jumlahKaroseri;
        _controllerPrice.text = _formatCurrency.formatCurrency(_price.toString());
        _controllerTotalPrice.text = _formatCurrency.formatCurrency(_totalPrice.toString());
    }

    Future<void> setValueForEditInfoObjKaroseri(KaroseriObjectModel data, int index,BuildContext context) async {
        await getDataFromDashboard(context);
        this._radioPKSKaroseri = data.PKSKaroseri;
        this._companySelected = data.company;
        this._controllerCompany.text = data.company.id +" - "+data.company.desc;
        this._karoseriSelected = data.karoseri;
        this._controllerKaroseri.text = data.karoseri.kode +" - "+data.karoseri.deskripsi;
        this._controllerJumlahKaroseri.text = data.jumlahKaroseri;
        this._controllerPrice.text = data.price;
        this._controllerTotalPrice.text = data.totalPrice;
        orderKaroseriID = data.orderKaroseriID;
        calculatedTotalPrice();
        notifyListeners();
        checkDataDakor(index);
    }

    GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

    void showSnackBar(String text){
        this._scaffoldKey.currentState.showSnackBar(new SnackBar(
            content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
    }

    FormatCurrency get formatCurrency => _formatCurrency;

    RegExInputFormatter get amountValidator => _amountValidator;

    void setPreference() async {
        SharedPreferences _preference = await SharedPreferences.getInstance();
        this._custType = _preference.getString("cust_type");
        this._lastKnownState = _preference.getString("last_known_state");
        if(this._lastKnownState == "AOS" || this._lastKnownState == "CONA" || this._lastKnownState == "IA" || this._lastKnownState == "PAC") {
            this._isDisablePACIAAOSCONA = true;
        }
    }

    ShowMandatoryInfoKaroseriModel _showMandatoryInfoKaroseriIdeModel;
    ShowMandatoryInfoKaroseriModel _showMandatoryInfoKaroseriIdeCompanyModel;
    void showMandatoryIdeModel(BuildContext context){
        _showMandatoryInfoKaroseriIdeModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoKaroseriIdeModel;
        _showMandatoryInfoKaroseriIdeCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoKaroseriIdeCompanyModel;
    }

    ShowMandatoryInfoKaroseriModel _showMandatoryInfoKaroseriRegulerSurveyModel;
    ShowMandatoryInfoKaroseriModel _showMandatoryInfoKaroseriRegulerSurveyCompanyModel;
    void showMandatoryRegulerSurveyModel(BuildContext context){
        _showMandatoryInfoKaroseriRegulerSurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoKaroseriRegulerSurveyModel;
        _showMandatoryInfoKaroseriRegulerSurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoKaroseriRegulerSurveyCompanyModel;
    }

    ShowMandatoryInfoKaroseriModel _showMandatoryInfoKaroseriPacModel;
    ShowMandatoryInfoKaroseriModel _showMandatoryInfoKaroseriPacCompanyModel;
    void showMandatoryPacModel(BuildContext context){
        _showMandatoryInfoKaroseriPacModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoKaroseriPacModel;
        _showMandatoryInfoKaroseriPacCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoKaroseriPacCompanyModel;
    }

    ShowMandatoryInfoKaroseriModel _showMandatoryInfoKaroseriResurveyModel;
    ShowMandatoryInfoKaroseriModel _showMandatoryInfoKaroseriResurveyCompanyModel;
    void showMandatoryResurveyModel(BuildContext context){
        _showMandatoryInfoKaroseriResurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoKaroseriResurveyModel;
        _showMandatoryInfoKaroseriResurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoKaroseriResurveyCompanyModel;
    }

    ShowMandatoryInfoKaroseriModel _showMandatoryInfoKaroseriDakorModel;
    ShowMandatoryInfoKaroseriModel _showMandatoryInfoKaroseriDakorCompanyModel;
    void showMandatoryDakorModel(BuildContext context){
        _showMandatoryInfoKaroseriDakorModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoKaroseriDakorModel;
        _showMandatoryInfoKaroseriDakorCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoKaroseriDakorCompanyModel;
    }

    Future<void> getDataFromDashboard(BuildContext context) async{
        showMandatoryIdeModel(context);
        showMandatoryRegulerSurveyModel(context);
        showMandatoryPacModel(context);
        showMandatoryResurveyModel(context);
        showMandatoryDakorModel(context);
    }

    // PKS Karoseri
    bool isRadioValuePKSKaroseriVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoKaroseriIdeModel.isRadioValuePKSKaroseriVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoKaroseriRegulerSurveyModel.isRadioValuePKSKaroseriVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoKaroseriPacModel.isRadioValuePKSKaroseriVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoKaroseriResurveyModel.isRadioValuePKSKaroseriVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoKaroseriDakorModel.isRadioValuePKSKaroseriVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoKaroseriIdeCompanyModel.isRadioValuePKSKaroseriVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoKaroseriRegulerSurveyCompanyModel.isRadioValuePKSKaroseriVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoKaroseriPacCompanyModel.isRadioValuePKSKaroseriVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoKaroseriResurveyCompanyModel.isRadioValuePKSKaroseriVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoKaroseriDakorCompanyModel.isRadioValuePKSKaroseriVisible;
            }
        }
        return value;
    }

// Perusahaan
    bool isCompanyVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoKaroseriIdeModel.isCompanyVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoKaroseriRegulerSurveyModel.isCompanyVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoKaroseriPacModel.isCompanyVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoKaroseriResurveyModel.isCompanyVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoKaroseriDakorModel.isCompanyVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoKaroseriIdeCompanyModel.isCompanyVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoKaroseriRegulerSurveyCompanyModel.isCompanyVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoKaroseriPacCompanyModel.isCompanyVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoKaroseriResurveyCompanyModel.isCompanyVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoKaroseriDakorCompanyModel.isCompanyVisible;
            }
        }
        return value;
    }

// Karoseri
    bool isKaroseriVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoKaroseriIdeModel.isKaroseriVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoKaroseriRegulerSurveyModel.isKaroseriVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoKaroseriPacModel.isKaroseriVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoKaroseriResurveyModel.isKaroseriVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoKaroseriDakorModel.isKaroseriVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoKaroseriIdeCompanyModel.isKaroseriVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoKaroseriRegulerSurveyCompanyModel.isKaroseriVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoKaroseriPacCompanyModel.isKaroseriVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoKaroseriResurveyCompanyModel.isKaroseriVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoKaroseriDakorCompanyModel.isKaroseriVisible;
            }
        }
        return value;
    }

// Jumlah Karoseri
    bool isJumlahKaroseriVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoKaroseriIdeModel.isJumlahKaroseriVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoKaroseriRegulerSurveyModel.isJumlahKaroseriVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoKaroseriPacModel.isJumlahKaroseriVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoKaroseriResurveyModel.isJumlahKaroseriVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoKaroseriDakorModel.isJumlahKaroseriVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoKaroseriIdeCompanyModel.isJumlahKaroseriVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoKaroseriRegulerSurveyCompanyModel.isJumlahKaroseriVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoKaroseriPacCompanyModel.isJumlahKaroseriVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoKaroseriResurveyCompanyModel.isJumlahKaroseriVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoKaroseriDakorCompanyModel.isJumlahKaroseriVisible;
            }
        }
        return value;
    }

// Harga
    bool isPriceVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoKaroseriIdeModel.isPriceVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoKaroseriRegulerSurveyModel.isPriceVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoKaroseriPacModel.isPriceVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoKaroseriResurveyModel.isPriceVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoKaroseriDakorModel.isPriceVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoKaroseriIdeCompanyModel.isPriceVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoKaroseriRegulerSurveyCompanyModel.isPriceVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoKaroseriPacCompanyModel.isPriceVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoKaroseriResurveyCompanyModel.isPriceVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoKaroseriDakorCompanyModel.isPriceVisible;
            }
        }
        return value;
    }

// Total Harga
    bool isTotalPriceVisible() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoKaroseriIdeModel.isTotalPriceVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoKaroseriRegulerSurveyModel.isTotalPriceVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoKaroseriPacModel.isTotalPriceVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoKaroseriResurveyModel.isTotalPriceVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoKaroseriDakorModel.isTotalPriceVisible;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoKaroseriIdeCompanyModel.isTotalPriceVisible;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoKaroseriRegulerSurveyCompanyModel.isTotalPriceVisible;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoKaroseriPacCompanyModel.isTotalPriceVisible;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoKaroseriResurveyCompanyModel.isTotalPriceVisible;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoKaroseriDakorCompanyModel.isTotalPriceVisible;
            }
        }
        return value;
    }

    // PKS Karoseri
    bool isRadioValuePKSKaroseriMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoKaroseriIdeModel.isRadioValuePKSKaroseriMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoKaroseriRegulerSurveyModel.isRadioValuePKSKaroseriMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoKaroseriPacModel.isRadioValuePKSKaroseriMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoKaroseriResurveyModel.isRadioValuePKSKaroseriMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoKaroseriDakorModel.isRadioValuePKSKaroseriMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoKaroseriIdeCompanyModel.isRadioValuePKSKaroseriMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoKaroseriRegulerSurveyCompanyModel.isRadioValuePKSKaroseriMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoKaroseriPacCompanyModel.isRadioValuePKSKaroseriMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoKaroseriResurveyCompanyModel.isRadioValuePKSKaroseriMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoKaroseriDakorCompanyModel.isRadioValuePKSKaroseriMandatory;
            }
        }
        return value;
    }

// Perusahaan
    bool isCompanyMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoKaroseriIdeModel.isCompanyMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoKaroseriRegulerSurveyModel.isCompanyMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoKaroseriPacModel.isCompanyMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoKaroseriResurveyModel.isCompanyMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoKaroseriDakorModel.isCompanyMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoKaroseriIdeCompanyModel.isCompanyMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoKaroseriRegulerSurveyCompanyModel.isCompanyMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoKaroseriPacCompanyModel.isCompanyMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoKaroseriResurveyCompanyModel.isCompanyMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoKaroseriDakorCompanyModel.isCompanyMandatory;
            }
        }
        return value;
    }

// Karoseri
    bool isKaroseriMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoKaroseriIdeModel.isKaroseriMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoKaroseriRegulerSurveyModel.isKaroseriMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoKaroseriPacModel.isKaroseriMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoKaroseriResurveyModel.isKaroseriMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoKaroseriDakorModel.isKaroseriMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoKaroseriIdeCompanyModel.isKaroseriMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoKaroseriRegulerSurveyCompanyModel.isKaroseriMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoKaroseriPacCompanyModel.isKaroseriMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoKaroseriResurveyCompanyModel.isKaroseriMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoKaroseriDakorCompanyModel.isKaroseriMandatory;
            }
        }
        return value;
    }

// Jumlah Karoseri
    bool isJumlahKaroseriMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoKaroseriIdeModel.isJumlahKaroseriMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoKaroseriRegulerSurveyModel.isJumlahKaroseriMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoKaroseriPacModel.isJumlahKaroseriMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoKaroseriResurveyModel.isJumlahKaroseriMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoKaroseriDakorModel.isJumlahKaroseriMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoKaroseriIdeCompanyModel.isJumlahKaroseriMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoKaroseriRegulerSurveyCompanyModel.isJumlahKaroseriMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoKaroseriPacCompanyModel.isJumlahKaroseriMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoKaroseriResurveyCompanyModel.isJumlahKaroseriMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoKaroseriDakorCompanyModel.isJumlahKaroseriMandatory;
            }
        }
        return value;
    }

// Harga
    bool isPriceMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoKaroseriIdeModel.isPriceMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoKaroseriRegulerSurveyModel.isPriceMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoKaroseriPacModel.isPriceMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoKaroseriResurveyModel.isPriceMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoKaroseriDakorModel.isPriceMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoKaroseriIdeCompanyModel.isPriceMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoKaroseriRegulerSurveyCompanyModel.isPriceMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoKaroseriPacCompanyModel.isPriceMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoKaroseriResurveyCompanyModel.isPriceMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoKaroseriDakorCompanyModel.isPriceMandatory;
            }
        }
        return value;
    }

// Total Harga
    bool isTotalPriceMandatory() {
        bool value;
        if(_custType == "PER") {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoKaroseriIdeModel.isTotalPriceMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoKaroseriRegulerSurveyModel.isTotalPriceMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoKaroseriPacModel.isTotalPriceMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoKaroseriResurveyModel.isTotalPriceMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoKaroseriDakorModel.isTotalPriceMandatory;
            }
        } else {
            if(_lastKnownState == "IDE") {
                value = _showMandatoryInfoKaroseriIdeCompanyModel.isTotalPriceMandatory;
            } else if (_lastKnownState == "SRE") {
                value = _showMandatoryInfoKaroseriRegulerSurveyCompanyModel.isTotalPriceMandatory;
            } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
                value = _showMandatoryInfoKaroseriPacCompanyModel.isTotalPriceMandatory;
            } else if (_lastKnownState == "RSVY") {
                value = _showMandatoryInfoKaroseriResurveyCompanyModel.isTotalPriceMandatory;
            } else if (_lastKnownState == "DKR") {
                value = _showMandatoryInfoKaroseriDakorCompanyModel.isTotalPriceMandatory;
            }
        }
        return value;
    }

    void checkDataDakor(int index) async{
        // print("dakor karoseri");
        var _check = await _dbHelper.selectMS2ObjtKaroseri();
        if(_check.isNotEmpty && _lastKnownState == "DKR"){
            isRadioValuePKSKaroseriChanges = radioValuePKSKaroseri.toString() != _check[index]['pks_karoseri'].toString() || _check[index]['edit_pks_karoseri'] == "1";
            isCompanyChanges =  _companySelected.id != _check[index]['comp_karoseri'] || _check[index]['edit_comp_karoseri'] == "1";
            isKaroseriChanges = _karoseriSelected.kode != _check[index]['karoseri'] || _check[index]['edit_karoseri'] == "1";
            isJumlahKaroseriChanges = _controllerJumlahKaroseri.text != _check[index]['karoseri_qty'].toString() || _check[index]['edit_karoseri_qty'] == "1";
            isPriceChanges = _controllerPrice.text != _check[index]['price'].toString() || _check[index]['edit_price'] == "1";
            isTotalPriceChanges = double.parse(_controllerTotalPrice.text.replaceAll(",", "")).toString().split(".")[0] != _check[index]['total_karoseri'].toString() || _check[index]['edit_total_karoseri'] == "1";
            if(isRadioValuePKSKaroseriChanges || isCompanyChanges || isKaroseriChanges || isJumlahKaroseriChanges || isPriceChanges || isTotalPriceChanges){
                isEdit = true;
            } else {
                isEdit = false;
            }
        }
        notifyListeners();
    }

    bool get isEdit => _isEdit;

    set isEdit(bool value) {
      _isEdit = value;
      notifyListeners();
    }

    bool get isTotalPriceChanges => _isTotalPriceChanges;

    set isTotalPriceChanges(bool value) {
        this._isTotalPriceChanges = value;
        notifyListeners();
    }

    bool get isPriceChanges => _isPriceChanges;

    set isPriceChanges(bool value) {
        this._isPriceChanges = value;
        notifyListeners();
    }

    bool get isJumlahKaroseriChanges => _isJumlahKaroseriChanges;

    set isJumlahKaroseriChanges(bool value) {
        this._isJumlahKaroseriChanges = value;
        notifyListeners();
    }

    bool get isKaroseriChanges => _isKaroseriChanges;

    set isKaroseriChanges(bool value) {
        this._isKaroseriChanges = value;
        notifyListeners();
    }

    bool get isCompanyChanges => _isCompanyChanges;

    set isCompanyChanges(bool value) {
        this._isCompanyChanges = value;
        notifyListeners();
    }

    bool get isRadioValuePKSKaroseriChanges => _isRadioValuePKSKaroseriChanges;

    set isRadioValuePKSKaroseriChanges(bool value) {
        this._isRadioValuePKSKaroseriChanges = value;
        notifyListeners();
    }

    bool get isDisablePACIAAOSCONA => _isDisablePACIAAOSCONA;

    set isDisablePACIAAOSCONA(bool value) {
        this._isDisablePACIAAOSCONA = value;
        notifyListeners();
    }
}