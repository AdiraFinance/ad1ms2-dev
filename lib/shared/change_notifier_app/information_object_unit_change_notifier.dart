import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/activities_model.dart';
import 'package:ad1ms2_dev/models/brand_object_model.dart';
import 'package:ad1ms2_dev/models/brand_type_model_genre_model.dart';
import 'package:ad1ms2_dev/models/form_m_foto_model.dart';
import 'package:ad1ms2_dev/models/grup_id_model.dart';
import 'package:ad1ms2_dev/models/group_object_model.dart';
import 'package:ad1ms2_dev/models/group_sales_model.dart';
import 'package:ad1ms2_dev/models/matriks_dealer_model.dart';
import 'package:ad1ms2_dev/models/model_object_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_object_model.dart';
import 'package:ad1ms2_dev/models/object_model.dart';
import 'package:ad1ms2_dev/models/object_purpose_model.dart';
import 'package:ad1ms2_dev/models/object_type_model.dart';
import 'package:ad1ms2_dev/models/object_usage_model.dart';
import 'package:ad1ms2_dev/models/product_type_model.dart';
import 'package:ad1ms2_dev/models/program_model.dart';
import 'package:ad1ms2_dev/models/reference_number_model.dart';
import 'package:ad1ms2_dev/models/rehab_type_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_object_unit_model.dart';
import 'package:ad1ms2_dev/models/source_order_model.dart';
import 'package:ad1ms2_dev/models/source_order_name_model.dart';
import 'package:ad1ms2_dev/models/third_party_model.dart';
import 'package:ad1ms2_dev/models/third_party_type_model.dart';
import 'package:ad1ms2_dev/models/type_of_financing_model.dart';
import 'package:ad1ms2_dev/screens/search_activity.dart';
import 'package:ad1ms2_dev/screens/search_brand_object.dart';
import 'package:ad1ms2_dev/screens/search_group_object.dart';
import 'package:ad1ms2_dev/screens/search_group_sales.dart';
import 'package:ad1ms2_dev/screens/search_grup_id.dart';
import 'package:ad1ms2_dev/screens/search_matriks_dealer.dart';
import 'package:ad1ms2_dev/screens/search_model_object.dart';
import 'package:ad1ms2_dev/screens/search_object.dart';
import 'package:ad1ms2_dev/screens/search_object_purpose.dart';
import 'package:ad1ms2_dev/screens/search_object_type.dart';
import 'package:ad1ms2_dev/screens/search_product_type.dart';
import 'package:ad1ms2_dev/screens/search_program.dart';
import 'package:ad1ms2_dev/screens/search_reference_number.dart';
import 'package:ad1ms2_dev/screens/search_rehab_type.dart';
import 'package:ad1ms2_dev/screens/search_source_order.dart';
import 'package:ad1ms2_dev/screens/search_source_order_name.dart';
import 'package:ad1ms2_dev/screens/search_source_order_name_axi.dart';
import 'package:ad1ms2_dev/screens/search_source_order_name_customer.dart';
import 'package:ad1ms2_dev/screens/search_source_order_name_dsa.dart';
import 'package:ad1ms2_dev/screens/search_source_order_name_karyawan.dart';
import 'package:ad1ms2_dev/screens/search_source_order_name_keday.dart';
import 'package:ad1ms2_dev/screens/search_source_order_name_spg.dart';
import 'package:ad1ms2_dev/screens/search_third_party.dart';
import 'package:ad1ms2_dev/screens/search_third_party_type.dart';
import 'package:ad1ms2_dev/screens/search_usage_object.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_application_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_income_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_collatelar_change_notifier.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/taksasi_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/dashboard/dashboard_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_info_keluarga_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_lookup_utj_change_notif.dart';
import 'package:ad1ms2_dev/shared/info_wmp_change_notifier.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_activity_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_brand_object_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_group_object_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_group_sales_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_grup_id_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_matriks_dealer_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_model_object_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_object_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_object_purpose_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_object_type_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_object_usage_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_product_type_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_program_change_notfier.dart';
import 'package:ad1ms2_dev/shared/search_reference_number_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_rehab_type_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_source_order_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_source_order_name_axi_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_source_order_name_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_source_order_name_customer_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_source_order_name_dsa_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_source_order_name_karyawan_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_source_order_name_keday_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_source_order_name_spg_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_third_party_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_third_party_type_change_notifier.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'info_object_karoseri_change_notifier.dart';

class InformationObjectUnitChangeNotifier with ChangeNotifier {
  bool _autoValidate = false;
  List<FinancingTypeModel> _listTypeOfFinancing = TypeOfFinancingList().financingTypeList;
  FinancingTypeModel _typeOfFinancingModelSelected;
  KegiatanUsahaModel _businessActivitiesModelSelected;
  JenisKegiatanUsahaModel _businessActivitiesTypeModelSelected;
  ModelObjectModel _modelObjectSelected;
  GroupObjectModel _groupObjectSelected;
  ObjectModel _objectSelected;
  ProductTypeModel _productTypeSelected;
  BrandObjectModel _brandObjectSelected;
  ObjectTypeModel _objectTypeSelected;
  ObjectPurposeModel _objectPurposeSelected;
  GroupSalesModel _groupSalesSelected;
  GrupIdModel _grupIdSelected;
  SourceOrderModel _sourceOrderSelected;
  SourceOrderNameModel _sourceOrderNameSelected;
  ThirdPartyTypeModel _thirdPartyTypeSelected;
  ThirdPartyModel _thirdPartySelected;
  MatriksDealerModel _matriksDealerSelected;
  ProgramModel _programSelected;
  RehabTypeModel _rehabTypeSelected;
  ReferenceNumberModel _referenceNumberModel;
  ObjectUsageModel _objectUsageModel;
  ActivitiesModel _activitiesModel;
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  bool _flag = false;
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String _prodMatrixId;
  DbHelper _dbHelper = DbHelper();
  String _custType;
  String _lastKnownState;
  var storage = FlutterSecureStorage();
  String _applObjtID = "";
  bool _disableJenisPenawaran = false;
  bool _isDisablePACIAAOSCONA = false;

  bool _isTypeOfFinancingModelChanges = false;
  bool _isBusinessActivitiesModelChanges = false;
  bool _isBusinessActivitiesTypeModelChanges = false;
  bool _isGroupObjectChanges = false;
  bool _isObjectChanges = false;
  bool _isTypeProductChanges = false;
  bool _isBrandObjectChanges = false;
  bool _isObjectTypeChanges = false;
  bool _isModelObjectChanges = false;
  bool _isDetailModelChanges = false;
  bool _isUsageObjectModelChanges = false;
  bool _isObjectPurposeChanges = false;
  bool _isGroupSalesChanges = false;
  bool _isGrupIdChanges = false;
  bool _isSourceOrderChanges = false;
  bool _isSourceOrderNameChanges = false;
  bool _isThirdPartyTypeChanges = false;
  bool _isThirdPartyChanges = false;
  bool _isMatriksDealerChanges = false;
  bool _isKegiatanChanges = false;
  bool _isSentraDChanges = false;
  bool _isUnitDChanges = false;
  bool _isProgramChanges = false;
  bool _isRehabTypeChanges = false;
  bool _isReferenceNumberChanges = false;

  List<KegiatanUsahaModel> _listBusinessActivities = [
    // KegiatanUsahaModel(1, "Investasi"),
    // KegiatanUsahaModel(2, "Multiguna"),
    // KegiatanUsahaModel(3, "Fasilitas Dana"),
  ];

  List<JenisKegiatanUsahaModel> _listBusinessActivitiesType = [
    // JenisKegiatanUsahaModel(3, "PPSA Pendapatan Non Tetap"),
    // JenisKegiatanUsahaModel(3, "PPSA Kelembagaan"),
    // JenisKegiatanUsahaModel(3, "Fin lease Pendapatan Non Tetap"),
    // JenisKegiatanUsahaModel(3, "Fin Lease Kelembagaan")
  ];

  TextEditingController _controllerGroupObject = TextEditingController();
  TextEditingController _controllerObject = TextEditingController();
  TextEditingController _controllerModelObject = TextEditingController();
  TextEditingController _controllerObjectType = TextEditingController();
  TextEditingController _controllerBrandObject = TextEditingController();
  TextEditingController _controllerTypeProduct = TextEditingController();
  TextEditingController _controllerObjectPurpose = TextEditingController();
  TextEditingController _controllerGroupSales = TextEditingController();
  TextEditingController _controllerGrupId = TextEditingController();
  TextEditingController _controllerSourceOrder = TextEditingController();
  TextEditingController _controllerSourceOrderName = TextEditingController();
  TextEditingController _controllerThirdPartyType = TextEditingController();
  TextEditingController _controllerThirdParty = TextEditingController();
  TextEditingController _controllerMatriksDealer = TextEditingController();
  TextEditingController _controllerDetailModel = TextEditingController();
  TextEditingController _controllerUsageObjectModel = TextEditingController();
  TextEditingController _controllerProgram = TextEditingController();
  TextEditingController _controllerRehabType = TextEditingController();
  TextEditingController _controllerReferenceNumber = TextEditingController();
  TextEditingController _controllerSentraD = TextEditingController();
  TextEditingController _controllerUnitD = TextEditingController();

  TextEditingController _controllerKegiatan = TextEditingController();
  TextEditingController _controllerWMP = TextEditingController();

  bool get isDisablePACIAAOSCONA => _isDisablePACIAAOSCONA;

  set isDisablePACIAAOSCONA(bool value) {
    this._isDisablePACIAAOSCONA = value;
    notifyListeners();
  }

  bool get disableJenisPenawaran => _disableJenisPenawaran;

  set disableJenisPenawaran(bool value) {
    this._disableJenisPenawaran = value;
    notifyListeners();
  }

  TextEditingController get controllerKegiatan => _controllerKegiatan;
  TextEditingController get controllerWMP => _controllerWMP;

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  String get custType => _custType;

  set custType(String value) {
    this._custType = value;
    notifyListeners();
  }

  String get lastKnownState => _lastKnownState;

  set lastKnownState(String value) {
    this._lastKnownState = value;
    notifyListeners();
  }

  MatriksDealerModel get matriksDealerSelected => _matriksDealerSelected;

  set matriksDealerSelected(MatriksDealerModel value) {
    this._matriksDealerSelected = value;
    notifyListeners();
  }

  FinancingTypeModel get typeOfFinancingModelSelected => _typeOfFinancingModelSelected;

  set typeOfFinancingModelSelected(FinancingTypeModel value) {
    this._typeOfFinancingModelSelected = value;
    notifyListeners();
  }

  KegiatanUsahaModel get businessActivitiesModelSelected =>
      _businessActivitiesModelSelected;

  set businessActivitiesModelSelected(KegiatanUsahaModel value) {
    this._businessActivitiesModelSelected = value;
    notifyListeners();
  }

  JenisKegiatanUsahaModel get businessActivitiesTypeModelSelected =>
      _businessActivitiesTypeModelSelected;

  set businessActivitiesTypeModelSelected(JenisKegiatanUsahaModel value) {
    this._businessActivitiesTypeModelSelected = value;
    notifyListeners();
  }

  UnmodifiableListView<FinancingTypeModel> get listTypeOfFinancing {
    return UnmodifiableListView(this._listTypeOfFinancing);
  }

  UnmodifiableListView<KegiatanUsahaModel> get listBusinessActivities {
    return UnmodifiableListView(this._listBusinessActivities);
  }

  UnmodifiableListView<JenisKegiatanUsahaModel>
      get listBusinessActivitiesType {
    return UnmodifiableListView(this._listBusinessActivitiesType);
  }

  TextEditingController get controllerGroupObject => _controllerGroupObject;

  TextEditingController get controllerObject => _controllerObject;

  TextEditingController get controllerTypeProduct => _controllerTypeProduct;

  TextEditingController get controllerBrandObject => _controllerBrandObject;

  TextEditingController get controllerObjectType => _controllerObjectType;

  TextEditingController get controllerModelObject => _controllerModelObject;

  TextEditingController get controllerObjectPurpose => _controllerObjectPurpose;

  TextEditingController get controllerGroupSales => _controllerGroupSales;

  TextEditingController get controllerGrupId => _controllerGrupId;

  TextEditingController get controllerSourceOrder => _controllerSourceOrder;

  TextEditingController get controllerSourceOrderName => _controllerSourceOrderName;

  TextEditingController get controllerThirdPartyType => _controllerThirdPartyType;

  TextEditingController get controllerThirdParty => _controllerThirdParty;

  TextEditingController get controllerDetailModel => _controllerDetailModel;

  TextEditingController get controllerProgram => _controllerProgram;

  TextEditingController get controllerRehabType => _controllerRehabType;

  TextEditingController get controllerReferenceNumber => _controllerReferenceNumber;

  TextEditingController get controllerSentraD => _controllerSentraD;

  TextEditingController get controllerUnitD => _controllerUnitD;

  TextEditingController get controllerUsageObjectModel => _controllerUsageObjectModel;

  set groupObjectSelected(GroupObjectModel value) {
    this._groupObjectSelected = value;
  }

  GroupObjectModel get groupObjectSelected => _groupObjectSelected;

  ModelObjectModel get modelObjectSelected => _modelObjectSelected;

  ProductTypeModel get productTypeSelected => _productTypeSelected;

  BrandObjectModel get brandObjectSelected => _brandObjectSelected;

  ObjectTypeModel get objectTypeSelected => _objectTypeSelected;

  ObjectModel get objectSelected => _objectSelected;

  ObjectUsageModel get objectUsageModel => _objectUsageModel;

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  ReferenceNumberModel get referenceNumberModel => _referenceNumberModel;

  set referenceNumberModel(ReferenceNumberModel value) {
    this._referenceNumberModel = value;
  }

  void searchModelObject(BuildContext context,String flag,int flagByBrandModelType) async {
    BrandTypeModelGenreModel data = await Navigator.push(context, MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchModelObjectChangeNotifier(),
                child: SearchModelObject(flag: flag,flagByBrandModelType: flagByBrandModelType, kodeGroupObject: this._groupObjectSelected.KODE, kodeObject: this._objectSelected.id, prodMatrix: this._prodMatrixId))));
    if (data != null) {
      this._brandObjectSelected = data.brandObjectModel;
      this._controllerBrandObject.text = "${_brandObjectSelected.id} - ${_brandObjectSelected.name}";
      this._objectTypeSelected = data.objectTypeModel;
      this._controllerObjectType.text = "${_objectTypeSelected.id} - ${_objectTypeSelected.name}";
      this._modelObjectSelected = data.modelObjectModel;
      print("cek data model object di info unit object ${_modelObjectSelected.id}- ${_modelObjectSelected.name}");
      this._controllerModelObject.text = "${_modelObjectSelected.id} - ${_modelObjectSelected.name}";
      this._objectUsageModel = data.objectUsageModel;
      this._controllerUsageObjectModel.text = "${_objectUsageModel.id} - ${_objectUsageModel.name}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchGroupObject(BuildContext context,String flag) async {
    GroupObjectModel data = await Navigator.push(context, MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchGroupObjectChangeNotifier(),
                child: SearchGroupObject(flag: flag, unitColla: "1"))));
    if (data != null) {
      if(this._groupObjectSelected != null){
        if(this._groupObjectSelected.KODE != data.KODE){
          this._objectSelected = null;
          this._controllerObject.clear();
          this._productTypeSelected = null;
          this._controllerTypeProduct.clear();
          this._brandObjectSelected = null;
          this._controllerBrandObject.clear();
          this._objectTypeSelected = null;
          this._controllerObjectType.clear();
          this._modelObjectSelected = null;
          this._controllerModelObject.clear();
          this._controllerDetailModel.clear();
          this._objectUsageModel = null;
          this._controllerUsageObjectModel.clear();
          this._objectPurposeSelected = null;
          this._controllerObjectPurpose.clear();
          this._groupSalesSelected = null;
          this._controllerGroupSales.clear();
          this._grupIdSelected = null;
          this._controllerGrupId.clear();
          this._sourceOrderSelected = null;
          this._controllerSourceOrder.clear();
          this._sourceOrderNameSelected = null;
          this._controllerSourceOrderName.clear();
          this._thirdPartyTypeSelected = null;
          this._controllerThirdPartyType.clear();
          this._thirdPartySelected = null;
          this._controllerThirdParty.clear();
          this._matriksDealerSelected = null;
          this._controllerMatriksDealer.clear();
          this._activitiesModel = null;
          this._controllerKegiatan.clear();
          this._programSelected = null;
          this._controllerProgram.clear();
          if(this._lastKnownState == "IDE" && this._disableJenisPenawaran == false){
            Provider.of<FormMLookupUTJChangeNotif>(context,listen: false).clearData();
            Provider.of<InformationCollateralChangeNotifier>(context,listen: false).clearDataInfoCollateral();
            Provider.of<InformationCollateralChangeNotifier>(context,listen: false).flag = false;
            Provider.of<TaksasiUnitChangeNotifier>(context,listen: false).listTaksasi.clear();
          }
        }
      }
      this._groupObjectSelected = data;
      this._controllerGroupObject.text = "${data.KODE} - ${data.DESKRIPSI}";
      // print("test ${data.KODE}");
      Provider.of<InfoObjectKaroseriChangeNotifier>(context,listen: false).checkKaroseriNeedVisible(data.KODE);
      //Provider.of<InfoObjectKaroseriChangeNotifier>(context,listen: false).setIsVisible(data.KODE);
      notifyListeners();
    } else {
      return;
    }
  }

  void searchObject(BuildContext context) async {
    ObjectModel data = await Navigator.push(context, MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchObjectChangeNotifier(),
                child: SearchObject(kodeObject: this._groupObjectSelected.KODE, unitColla: "1"))));
    if (data != null) {
      if(this._objectSelected != null){
        if(this._objectSelected.id != data.id){
          this._productTypeSelected = null;
          this._controllerTypeProduct.clear();
          this._brandObjectSelected = null;
          this._controllerBrandObject.clear();
          this._objectTypeSelected = null;
          this._controllerObjectType.clear();
          this._modelObjectSelected = null;
          this._controllerModelObject.clear();
          this._controllerDetailModel.clear();
          this._objectUsageModel = null;
          this._controllerUsageObjectModel.clear();
          this._objectPurposeSelected = null;
          this._controllerObjectPurpose.clear();
          this._groupSalesSelected = null;
          this._controllerGroupSales.clear();
          this._grupIdSelected = null;
          this._controllerGrupId.clear();
          this._sourceOrderSelected = null;
          this._controllerSourceOrder.clear();
          this._sourceOrderNameSelected = null;
          this._controllerSourceOrderName.clear();
          this._thirdPartyTypeSelected = null;
          this._controllerThirdPartyType.clear();
          this._thirdPartySelected = null;
          this._controllerThirdParty.clear();
          this._matriksDealerSelected = null;
          this._controllerMatriksDealer.clear();
          this._activitiesModel = null;
          this._controllerKegiatan.clear();
          this._programSelected = null;
          this._controllerProgram.clear();
          if(this._lastKnownState == "IDE" && this._disableJenisPenawaran == false){
            Provider.of<FormMLookupUTJChangeNotif>(context,listen: false).clearData();
            Provider.of<InformationCollateralChangeNotifier>(context,listen: false).clearDataInfoCollateral();
            Provider.of<InformationCollateralChangeNotifier>(context,listen: false).flag = false;
            Provider.of<TaksasiUnitChangeNotifier>(context,listen: false).listTaksasi.clear();
          }
        }
      }
      this._objectSelected = data;
      this._controllerObject.text = "${data.id} - ${data.name}";
      notifyListeners();
    } else {
      return;
    }
  }

  String get prodMatrixId => _prodMatrixId;

  set prodMatrixId(String value) {
    this._prodMatrixId = value;
  }

  void searchProductType(BuildContext context,String flag) async {
    ProductTypeModel data = await Navigator.push(context, MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchProductTypeChangeNotifier(),
                child: SearchProductType(flag: flag, kodeGroupObject: this._groupObjectSelected.KODE, kodeObject: this._objectSelected.id))));
    if (data != null) {
      this._productTypeSelected = data;
      this._controllerTypeProduct.text = "${data.id} - ${data.name}";
      Provider.of<InfoWMPChangeNotifier>(context, listen: false).flag = false;
      Provider.of<InfoWMPChangeNotifier>(context, listen: false).listWMP.clear();
      notifyListeners();
      await _getProdMatrixID(context, true);
    } else {
      return;
    }
  }

  void searchBrandObject(BuildContext context, String flag, int flagByBrandModelType) async {
    BrandTypeModelGenreModel data = await Navigator.push(context, MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchBrandObjectChangeNotifier(),
                child: SearchBrandObject(flag: flag, flagByBrandModelType: flagByBrandModelType, kodeGroupObject: this._groupObjectSelected.KODE, kodeObject: this._objectSelected.id, prodMatrix: this._prodMatrixId))));
    if (data != null) {
      this._brandObjectSelected = data.brandObjectModel;
      this._controllerBrandObject.text = "${_brandObjectSelected.id} - ${_brandObjectSelected.name}";
      this._objectTypeSelected = data.objectTypeModel;
      this._controllerObjectType.text = "${_objectTypeSelected.id} - ${_objectTypeSelected.name}";
      this._modelObjectSelected = data.modelObjectModel;
      this._controllerModelObject.text = "${_modelObjectSelected.id} - ${_modelObjectSelected.name}";
      this._objectUsageModel = data.objectUsageModel;
      this._controllerUsageObjectModel.text = "${_objectUsageModel.id} - ${_objectUsageModel.name}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchObjectType(BuildContext context, String flag, int flagByBrandModelType) async {
    BrandTypeModelGenreModel data = await Navigator.push(context, MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchObjectTypeChangeNotifier(),
                child: SearchObjectType(flagByBrandModelType: flagByBrandModelType, flag: flag, kodeGroupObject: this._groupObjectSelected.KODE, kodeObject: this._objectSelected.id, prodMatrix: this._prodMatrixId))));
    if (data != null) {
      this._brandObjectSelected = data.brandObjectModel;
      this._controllerBrandObject.text = "${_brandObjectSelected.id} - ${_brandObjectSelected.name}";
      this._objectTypeSelected = data.objectTypeModel;
      this._controllerObjectType.text = "${_objectTypeSelected.id} - ${_objectTypeSelected.name}";
      this._modelObjectSelected = data.modelObjectModel;
      this._controllerModelObject.text = "${_modelObjectSelected.id} - ${_modelObjectSelected.name}";
      this._objectUsageModel = data.objectUsageModel;
      this._controllerUsageObjectModel.text = "${_objectUsageModel.id} - ${_objectUsageModel.name}";
      notifyListeners();
    } else {
      return;
    }
  }

  ObjectPurposeModel get objectPurposeSelected => _objectPurposeSelected;

  set objectPurposeSelected(ObjectPurposeModel value) {
    this._objectPurposeSelected = value;
    notifyListeners();
  }

  void searchObjectPurpose(String flag, BuildContext context) async {
    ObjectPurposeModel data = await Navigator.push(context, MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchObjectPurposeChangeNotifier(),
                child: SearchObjectPurpose(flag: flag))));
    if (data != null) {
      this._objectPurposeSelected = data;
      this._controllerObjectPurpose.text = "${data.id} - ${data.name}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchGroupSales(BuildContext context) async {
    GroupSalesModel data = await Navigator.push(context, MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchGroupSalesChangeNotifier(),
                child: SearchGroupSales())));
    if (data != null) {
      this._groupSalesSelected = data;
      this._controllerGroupSales.text = "${data.kode} - ${data.deskripsi}";
      this._grupIdSelected = null;
      this._controllerGrupId.clear();
      notifyListeners();
    } else {
      return;
    }
  }

  void searchGrupId(BuildContext context) async {
    GrupIdModel data = await Navigator.push(context, MaterialPageRoute(
        builder: (context) => ChangeNotifierProvider(
            create: (context) => SearchGrupIdChangeNotifier(),
            child: SearchGrupId())));
    if (data != null) {
      this._grupIdSelected = data;
      this._controllerGrupId.text = "${data.kode} - ${data.deskripsi}";
      notifyListeners();
    } else {
      return;
    }
  }

  SourceOrderModel get sourceOrderSelected => _sourceOrderSelected;

  set sourceOrderSelected(SourceOrderModel value) {
    this._sourceOrderSelected = value;
    notifyListeners();
  }

  void searchSourceOrder(BuildContext context) async {
    SourceOrderModel data = await Navigator.push(context, MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchSourceOrderChangeNotifier(),
                child: SearchSourceOrder())));
    if (data != null) {
      this._sourceOrderSelected = data;
      this._controllerSourceOrder.text = "${data.kode} - ${data.deskripsi}";
      this._sourceOrderNameSelected = null;
      this._controllerSourceOrderName.clear();
      notifyListeners();
    } else {
      return;
    }
  }

  GroupSalesModel get groupSalesSelected => _groupSalesSelected;

  GrupIdModel get grupIdSelected => _grupIdSelected;

  ActivitiesModel get activitiesModel => _activitiesModel;

  SourceOrderNameModel get sourceOrderNameSelected => _sourceOrderNameSelected;

  set sourceOrderNameSelected(SourceOrderNameModel value) {
    this._sourceOrderNameSelected = value;
  }

  void searchSourceOrderName(BuildContext context) async {
    if(this._sourceOrderSelected.kode == "008") {
      // 008-AXI
      SourceOrderNameModel data = await Navigator.push(context, MaterialPageRoute(
        builder: (context) => ChangeNotifierProvider(
          create: (context) => SearchSourceOrderNameAXIChangeNotifier(),
          child: SearchSourceOrderNameAXI()
        )
      ));
      if (data != null) {
        this._sourceOrderNameSelected = data;
        this._controllerSourceOrderName.text = "${data.kode} - ${data.deskripsi}";
        notifyListeners();
      } else {
        return;
      }
    }
    else if(this._sourceOrderSelected.kode == "009") {
      // 009-Keday
      SourceOrderNameModel data = await Navigator.push(context, MaterialPageRoute(
          builder: (context) => ChangeNotifierProvider(
              create: (context) => SearchSourceOrderNameKedayChangeNotifier(),
              child: SearchSourceOrderNameKeday())));
      if (data != null) {
        this._sourceOrderNameSelected = data;
        this._controllerSourceOrderName.text = "${data.kode} - ${data.deskripsi}";
        notifyListeners();
      } else {
        return;
      }
    }
    else if(this._sourceOrderSelected.kode == "010") {
      // 010-Karyawan
      SourceOrderNameModel data = await Navigator.push(context, MaterialPageRoute(
          builder: (context) => ChangeNotifierProvider(
              create: (context) => SearchSourceOrderNameKaryawanChangeNotifier(),
              child: SearchSourceOrderNameKaryawan())));
      if (data != null) {
        this._sourceOrderNameSelected = data;
        this._controllerSourceOrderName.text = "${data.kode} - ${data.deskripsi}";
        notifyListeners();
      } else {
        return;
      }
    }
    else if(this._sourceOrderSelected.kode == "011") {
      // 011-SPG
      SourceOrderNameModel data = await Navigator.push(context, MaterialPageRoute(
          builder: (context) => ChangeNotifierProvider(
              create: (context) => SearchSourceOrderNameSPGChangeNotifier(),
              child: SearchSourceOrderNameSPG())));
      if (data != null) {
        this._sourceOrderNameSelected = data;
        this._controllerSourceOrderName.text = "${data.kode} - ${data.deskripsi}";
        notifyListeners();
      } else {
        return;
      }
    }
    else if(this._sourceOrderSelected.kode == "013") {
      // 013-Customer/Nasabah
      SourceOrderNameModel data = await Navigator.push(context, MaterialPageRoute(
          builder: (context) => ChangeNotifierProvider(
              create: (context) => SearchSourceOrderNameCustomerChangeNotifier(),
              child: SearchSourceOrderNameCustomer())));
      if (data != null) {
        this._sourceOrderNameSelected = data;
        this._controllerSourceOrderName.text = "${data.kode} - ${data.deskripsi}";
        notifyListeners();
      } else {
        return;
      }
    }
    else if(this._sourceOrderSelected.kode == "020") {
      // 013-DSA
      SourceOrderNameModel data = await Navigator.push(context, MaterialPageRoute(
          builder: (context) => ChangeNotifierProvider(
              create: (context) => SearchSourceOrderNameDSAChangeNotifier(),
              child: SearchSourceOrderNameDSA())));
      if (data != null) {
        this._sourceOrderNameSelected = data;
        this._controllerSourceOrderName.text = "${data.kode} - ${data.deskripsi}";
        notifyListeners();
      } else {
        return;
      }
    }
    else {
      SourceOrderNameModel data = await Navigator.push(context, MaterialPageRoute(
          builder: (context) => ChangeNotifierProvider(
              create: (context) => SearchSourceOrderNameChangeNotifier(),
              child: SearchSourceOrderName())));
      if (data != null) {
        this._sourceOrderNameSelected = data;
        this._controllerSourceOrderName.text = "${data.kode} - ${data.deskripsi}";
        notifyListeners();
      } else {
        return;
      }
    }
  }

  ThirdPartyTypeModel get thirdPartyTypeSelected => _thirdPartyTypeSelected;

  set thirdPartyTypeSelected(ThirdPartyTypeModel value) {
    this._thirdPartyTypeSelected = value;
  }

  void searchThirdPartyType(BuildContext context) async {
    ThirdPartyTypeModel data = await Navigator.push(context, MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchThirdPartyTypeChangeNotifier(),
                child: SearchThirdPartyType())));
    if (data != null) {
      this._thirdPartyTypeSelected = data;
      this._controllerThirdPartyType.text = "${data.kode} - ${data.deskripsi}";
      notifyListeners();
    } else {
      return;
    }
  }

  ThirdPartyModel get thirdPartySelected => _thirdPartySelected;

  set thirdPartySelected(ThirdPartyModel value) {
    this._thirdPartySelected = value;
  }

  void searchThirdParty(String flag, BuildContext context) async {
    ThirdPartyModel data = await Navigator.push(context, MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchThirdPartyChangeNotifier(),
                child: SearchThirdParty(flag))));
    if (data != null) {
      this._thirdPartySelected = data;
      this._controllerThirdParty.text = "${data.kode} - ${data.deskripsi}";
      notifyListeners();
      getMatriksDealer(context);
    } else {
      return;
    }
  }

  void searchMatriksDealer(BuildContext context) async {
    MatriksDealerModel data = await Navigator.push(context, MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchMatriksDealerChangeNotifier(),
                child: SearchMatriksDealer())));
    if (data != null) {
      this._matriksDealerSelected = data;
      this._controllerMatriksDealer.text = "${data.kode} - ${data.deskripsi}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchProgram(BuildContext context) async {
    ProgramModel data = await Navigator.push(context, MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchProgramChangeNotifier(),
                child: SearchProgram())));
    if (data != null) {
      this._programSelected = data;
      this._controllerProgram.text = "${data.kode} - ${data.deskripsi}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchRehabType(BuildContext context) async {
    RehabTypeModel data = await Navigator.push(context, MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchRehabTypeChangeNotifier(),
                child: SearchRehabType())));
    if (data != null) {
      this._rehabTypeSelected = data;
      this._controllerRehabType.text = "${data.id} - ${data.name}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchReferenceNumber(BuildContext context) async {
    if(this._rehabTypeSelected != null){
      ReferenceNumberModel data = await Navigator.push(context, MaterialPageRoute(
          builder: (context) => ChangeNotifierProvider(
              create: (context) => SearchReferenceNumberChangeNotifier(),
              child: SearchReferenceNumber())));
      if (data != null) {
        this._referenceNumberModel = data;
        this._controllerReferenceNumber.text = "${data.noPK} - ${data.custName}";
      } else {
        return;
      }
    }
    else {
      showSnackBar("Silahkan pilih jenis Rehab");
    }
    notifyListeners();
  }

  void searchUsageObject(BuildContext context) async {
    ObjectUsageModel data = await Navigator.push(context, MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchObjectUsageChangeNotifier(),
                child: SearchUsageObject())));
    if (data != null) {
      this._objectUsageModel = data;
      this._controllerUsageObjectModel.text = "${data.id} - ${data.name}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchActivities(BuildContext context) async {
    ActivitiesModel data = await Navigator.push(context, MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchActivityChangeNotifier(),
                child: SearchActivity())));
    if (data != null) {
      this._activitiesModel = data;
      this._controllerKegiatan.text = "${data.kode} - ${data.deskripsi}";
      notifyListeners();
    } else {
      return;
    }
  }

  GlobalKey<FormState> get keyForm => _key;

  bool get flag => _flag;

  set flag(bool value) {
    _flag = value;
    notifyListeners();
  }

  void check(BuildContext context) {
    final _form = _key.currentState;
    if (_form.validate()) {
      checkDataDakor();
      // var _providerRincianNasabah = Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false);
      // var _providerRincianKeluarga = Provider.of<FormMInfoKeluargaChangeNotif>(context, listen: false);
      // bool _isPasanganExist = false;
      // if (_providerRincianNasabah.maritalStatusSelected.id == "01" && _groupObjectSelected.KODE != "003") { // Sudah Menikah dan Group Objek Selain Durable
      //   for(int i = 0; i < _providerRincianKeluarga.listFormInfoKel.length; i++) {
      //     if(_providerRincianKeluarga.listFormInfoKel[i].relationshipStatusModel.PARA_FAMILY_TYPE_ID == "01" || _providerRincianKeluarga.listFormInfoKel[i].relationshipStatusModel.PARA_FAMILY_TYPE_ID == "02") {
      //       _isPasanganExist = true;
      //     }
      //   }
      //   if(!_isPasanganExist) {
      //     showDialog(
      //         context: context,
      //         barrierDismissible: true,
      //         builder: (BuildContext context){
      //           return Theme(
      //             data: ThemeData(
      //                 fontFamily: "NunitoSans",
      //                 primaryColor: Colors.black,
      //                 primarySwatch: primaryOrange,
      //                 accentColor: myPrimaryColor
      //             ),
      //             child: AlertDialog(
      //               title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
      //               content: Column(
      //                 crossAxisAlignment: CrossAxisAlignment.start,
      //                 mainAxisSize: MainAxisSize.min,
      //                 children: <Widget>[
      //                   Text("Wajib mengisi data pasangan terlebih dahulu pada Rincian Keluarga.",),
      //                 ],
      //               ),
      //               actions: <Widget>[
      //                 new FlatButton(
      //                   onPressed: () {
      //                     Navigator.of(context).pop(true);
      //                   },
      //                   child: new Text('Close'),
      //                 ),
      //                 // new FlatButton(
      //                 //   onPressed: () => Navigator.of(context).pop(true),
      //                 //   child: new Text('Tidak'),
      //                 // ),
      //               ],
      //             ),
      //           );
      //         }
      //     );
      //   } else {
      //     flag = true;
      //     autoValidate = false;
      //     Navigator.pop(context);
      //   }
      // }
      // else {
        flag = true;
        autoValidate = false;
        Navigator.pop(context);
      // }
    } else {
      flag = false;
      autoValidate = true;
    }
  }

  void checkStatusMenikahDurable() async {

  }

  Future<bool> onBackPress() async{
    final _form = _key.currentState;
    checkDataDakor();
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
    } else {
      flag = false;
      autoValidate = true;
    }
    return true;
  }

  void clearDataInfoUnitObject(){
      _autoValidate = false;
      _flag = false;
      _typeOfFinancingModelSelected = null;
      _businessActivitiesModelSelected = null;
      _businessActivitiesTypeModelSelected = null;
      _modelObjectSelected = null;
      _groupObjectSelected = null;
      _objectSelected = null;
      _productTypeSelected = null;
      _brandObjectSelected = null;
      _objectTypeSelected = null;
      _objectPurposeSelected = null;
      _groupSalesSelected = null;
      _grupIdSelected = null;
      _sourceOrderSelected = null;
      _sourceOrderNameSelected = null;
      _thirdPartyTypeSelected = null;
      _thirdPartySelected = null;
      _matriksDealerSelected = null;
      _programSelected = null;
      _rehabTypeSelected = null;
      _referenceNumberModel = null;
      _objectUsageModel = null;
      _activitiesModel = null;
      _controllerGroupObject.clear();
      _controllerObject.clear();
      _controllerTypeProduct.clear();
      _controllerBrandObject.clear();
      _controllerObjectType.clear();
      _controllerModelObject.clear();
      _controllerDetailModel.clear();
      _controllerUsageObjectModel.clear();
      _controllerObjectPurpose.clear();
      _controllerGroupSales.clear();
      _controllerGrupId.clear();
      _controllerSourceOrder.clear();
      _controllerSourceOrderName.clear();
      _controllerThirdPartyType.clear();
      _controllerThirdParty.clear();
      _controllerSentraD.clear();
      _controllerUnitD.clear();
      _controllerProgram.clear();
      _controllerRehabType.clear();
      _controllerReferenceNumber.clear();
      _controllerMatriksDealer.clear();
      _prodMatrixId = '';

      _controllerKegiatan.clear();
      _controllerWMP.clear();
      _controllerMatriksDealer.clear();

      _isTypeOfFinancingModelChanges = false;
      _isBusinessActivitiesModelChanges = false;
      _isBusinessActivitiesTypeModelChanges = false;
      _isGroupObjectChanges = false;
      _isObjectChanges = false;
      _isTypeProductChanges = false;
      _isBrandObjectChanges = false;
      _isObjectTypeChanges = false;
      _isModelObjectChanges = false;
      _isUsageObjectModelChanges = false;
      _isObjectPurposeChanges = false;
      _isGroupSalesChanges = false;
      _isGrupIdChanges = false;
      _isSourceOrderChanges = false;
      _isSourceOrderNameChanges = false;
      _isThirdPartyTypeChanges = false;
      _isThirdPartyChanges = false;
      _isMatriksDealerChanges = false;
      _isKegiatanChanges = false;
      _isSentraDChanges = false;
      _isUnitDChanges = false;
      _isProgramChanges = false;
      _isRehabTypeChanges = false;
      _isReferenceNumberChanges = false;
      disableJenisPenawaran = false;
      isDisablePACIAAOSCONA = false;
  }

  Future<void> getListBusinessActivitiesType(String flag, BuildContext context) async{
    try{
      this._listBusinessActivitiesType.clear();
      this._businessActivitiesTypeModelSelected = null;
      loadData = true;
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);
      var _body = jsonEncode({
        "P_OJK_BUSS_ID" : this._businessActivitiesModelSelected.id
      });
      String _fieldJenisKegiatanUsaha = await storage.read(key: "FieldJenisKegiatanUsaha");
      final _response = await _http.post(
          "${BaseUrl.urlGeneral}$_fieldJenisKegiatanUsaha",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));
      final _result = jsonDecode(_response.body);
      final _data = _result['data'];
      if(_data.length > 0){
        for(int i=0; i<_data.length; i++){
          _listBusinessActivitiesType.add(JenisKegiatanUsahaModel(_data[i]['KODE'], _data[i]['DESKRIPSI']));
        }
        loadData = false;
      }
      else{
        loadData = false;
        showSnackBar("Jenis kegiatan usaha tidak ditemukan");
      }
    }
    on TimeoutException catch(_){
      loadData = false;
      showSnackBar("Timeout Connection");
    }
    on SocketException catch(_){
      loadData = false;
      showSnackBar("Please check connection or contact server");
    }
    catch(e){
      loadData = false;
      showSnackBar("Error ${e.toString()}");
    }
    notifyListeners();
  }

  Future<void> getBusinessActivities(String flag,BuildContext context) async{
    this._listBusinessActivities.clear();
    this._businessActivitiesModelSelected = null;
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
    var _body = jsonEncode({
      "P_FIN_TYPE" : flag == "COM"
          ? this._typeOfFinancingModelSelected.financingTypeId
          : Provider.of<FormMFotoChangeNotifier>(context,listen: false).typeOfFinancingModelSelected.financingTypeId,
    });
    String _fieldKegiatanUsaha = await storage.read(key: "FieldKegiatanUsaha");
    try{
      final _response = await _http.post(
          "${BaseUrl.urlGeneral}$_fieldKegiatanUsaha",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));
      final _result = jsonDecode(_response.body);
      final _data = _result['data'];
      if(_data.length > 0){
        for(int i=0; i < _data.length; i++){
          this._listBusinessActivities.add(KegiatanUsahaModel(_data[i]['KODE'], _data[i]['DESKRIPSI']));
        }
        loadData = false;
      }
      else{
        loadData = false;
        showSnackBar("Kegiatan usaha data tidak ditemukan");
      }
    }
    on TimeoutException catch(_){
      loadData = false;
      showSnackBar("Timeout Connection");
    }
    on SocketException catch(_){
      loadData = false;
      showSnackBar("Please check connection or contact server");
    }
    catch(e){
      loadData = false;
      showSnackBar("Error ${e.toString()}");
    }
    notifyListeners();
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  Future<void> _getProdMatrixID(BuildContext context, bool snackbar) async{
    debugPrint("PROD_MATRIX_JALAN");
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    String _custType = _preferences.getString("cust_type");
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
    var _providerObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context,listen: false);
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
    var _body = jsonEncode(
        {
          "P_JENIS_PRODUK_ID": "${this._productTypeSelected.id}",//Jenis produk
          "P_OBJECT_GROUP_ID": "${this._groupObjectSelected.KODE}",// grup objek
          "P_OBJECT_ID": "${this._objectSelected.id}",// objek
          "P_OJK_BUSS_DETAIL_ID" : _custType == "COM" ? "${_providerObjectUnit.businessActivitiesTypeModelSelected.id}" : "${_providerFoto.jenisKegiatanUsahaSelected.id}",
          "P_OJK_BUSS_ID" : _custType == "COM" ? "${_providerObjectUnit.businessActivitiesModelSelected.id}" : "${_providerFoto.kegiatanUsahaSelected.id}"
        }
    );
    print("body produk matrix = $_body");
    String _productMatrix = await storage.read(key: "ProductMatrix");
    try{
      final _response = await _http.post(
          "${BaseUrl.urlGeneral}$_productMatrix",
          // "${urlPublic}api/parameter/get-product-matrix",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));

      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        final _data = _result['data'];
        if(_data.isEmpty){
          if(snackbar){
            showSnackBar("Gagal mendapat Prod Matrix ID");
          }
        }
        else{
          print(_result['data']);
          this._prodMatrixId = _data[0]['PROD_MATRIX_ID'];
          notifyListeners();
        }
      }else{
        if(snackbar){
          showSnackBar("Error ${_response.statusCode}");
        }
      }
    }
    on TimeoutException catch(_){
      showSnackBar("Timeout connection");
    }
    catch(e){
      showSnackBar("Error ${e.toString()}");
    }
  }

  ProgramModel get programSelected => _programSelected;

  set programSelected(ProgramModel value) {
    this._programSelected = value;
  }

  RehabTypeModel get rehabTypeSelected => _rehabTypeSelected;

  set rehabTypeSelected(RehabTypeModel value) {
    this._rehabTypeSelected = value;
  }

  void saveToSQLite(BuildContext context) async {
    debugPrint("save unit objek");
    debugPrint(this._groupObjectSelected != null ? this._groupObjectSelected.KODE : "haloooo");
    var _providerStructureCredit = Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false);
    var _providerStructureCreditIncome = Provider.of<InfoCreditIncomeChangeNotifier>(context,listen: false);
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    // var _listOID = Provider.of<ListOidChangeNotifier>(context,listen: false);
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
    _dbHelper.insertMS2ApplObject(MS2ApplObjectModel(
      "",
      applObjtID,
      _preferences.getString("cust_type") == "COM" ? this._typeOfFinancingModelSelected.financingTypeId : _providerFoto.typeOfFinancingModelSelected.financingTypeId,
      _preferences.getString("cust_type") == "COM" ? this._businessActivitiesModelSelected.id.toString() : _providerFoto.kegiatanUsahaSelected.id.toString(),
      _preferences.getString("cust_type") == "COM" ? this._businessActivitiesModelSelected.text : _providerFoto.kegiatanUsahaSelected.text,
      _preferences.getString("cust_type") == "COM" ? this._businessActivitiesTypeModelSelected.id.toString() : _providerFoto.jenisKegiatanUsahaSelected.id.toString(),
      _preferences.getString("cust_type") == "COM" ? this._businessActivitiesTypeModelSelected.text : _providerFoto.jenisKegiatanUsahaSelected.text,
      this._groupObjectSelected != null ? this._groupObjectSelected.KODE : "",
      this._groupObjectSelected != null ? this._groupObjectSelected.DESKRIPSI : "",
      this._objectSelected != null ? this._objectSelected.id : "",
      this._objectSelected != null ? this._objectSelected.name : "",
      this._productTypeSelected != null ? this._productTypeSelected.id : "",
      this._productTypeSelected != null ? this._productTypeSelected.name : "",
      this._brandObjectSelected != null ? this._brandObjectSelected.id : "",
      this._brandObjectSelected != null ? this._brandObjectSelected.name : "",
      this._objectTypeSelected != null ? this._objectTypeSelected.id : "",
      this._objectTypeSelected != null ? this._objectTypeSelected.name : "",
      this._modelObjectSelected != null ? this._modelObjectSelected.id : "",
      this._modelObjectSelected != null ? this._modelObjectSelected.name : "",
      "",//this._controllerDetailModel.text != "" ? this._controllerDetailModel.text : "",
      this._objectUsageModel != null ? this._objectUsageModel.id : "",
      this._objectUsageModel != null ? this._objectUsageModel.name : "",
      this._objectPurposeSelected != null ? this._objectPurposeSelected.id : "",
      this._objectPurposeSelected != null ? this._objectPurposeSelected.name : "",
      this._groupSalesSelected != null ? this._groupSalesSelected.kode : "",
      this._groupSalesSelected != null ? this._groupSalesSelected.deskripsi : "",
      this._grupIdSelected != null ? this._grupIdSelected.kode : "",
      this._grupIdSelected != null ? this._grupIdSelected.deskripsi : "",
      this._sourceOrderSelected != null ? this._sourceOrderSelected.kode : "",
      this._sourceOrderNameSelected != null ? this._sourceOrderNameSelected.kode : "",//this._sourceOrderSelected != null ? this._sourceOrderSelected.deskripsi : "",
      null,
      this._thirdPartyTypeSelected != null ? this._thirdPartyTypeSelected.kode : "",
      this._thirdPartyTypeSelected != null ? this._thirdPartyTypeSelected.deskripsi : "",
      this._thirdPartySelected != null ? this._thirdPartySelected.kode : "",
      this._activitiesModel != null ? this._activitiesModel.kode : "",
      this._activitiesModel != null ? this._activitiesModel.deskripsi : "",
      null,
      this._controllerSentraD.text,
      null,
      this._controllerUnitD.text,
      this._programSelected != null ? this._programSelected.kode : "",
      this._programSelected != null ? this._programSelected.deskripsi : "",
      this._rehabTypeSelected != null ? this._rehabTypeSelected.id : "",
      this._rehabTypeSelected != null ? this._rehabTypeSelected.name : "",
      this._referenceNumberModel != null ? this._referenceNumberModel.noPK : "",
      this._referenceNumberModel != null ? this._referenceNumberModel.custName : "",
      null,
      _providerStructureCredit.installmentTypeSelected != null ? _providerStructureCredit.installmentTypeSelected.id : "",
      _providerStructureCredit.installmentTypeSelected != null ? _providerStructureCredit.installmentTypeSelected.name : "",
      _providerStructureCredit.periodOfTimeSelected != null ? int.parse(_providerStructureCredit.periodOfTimeSelected) : null,
      _providerStructureCredit.paymentMethodSelected != null ? _providerStructureCredit.paymentMethodSelected.id : "",
      _providerStructureCredit.paymentMethodSelected != null ? _providerStructureCredit.paymentMethodSelected.name : "",
      _providerStructureCredit.controllerInterestRateEffective.text == "" || _providerStructureCredit.controllerInterestRateEffective.text == null ? null : double.parse(_providerStructureCredit.controllerInterestRateEffective.text) ,
      _providerStructureCredit.controllerInterestRateFlat.text == "" || _providerStructureCredit.controllerInterestRateFlat.text == null ? null : double.parse(_providerStructureCredit.controllerInterestRateFlat.text),
      _providerStructureCredit.controllerObjectPrice.text != "" ? double.parse(_providerStructureCredit.controllerObjectPrice.text.replaceAll(",", "")) : null,
      _providerStructureCredit.controllerKaroseriTotalPrice.text != "" ? double.parse(_providerStructureCredit.controllerKaroseriTotalPrice.text.replaceAll(",", "")) : null,
      _providerStructureCredit.controllerTotalPrice.text == "" || _providerStructureCredit.controllerTotalPrice.text == null ? 0 : double.parse(_providerStructureCredit.controllerTotalPrice.text.replaceAll(",", "")) ,
      _providerStructureCredit.controllerNetDP.text == "" || _providerStructureCredit.controllerNetDP.text == null ? 0 : double.parse(_providerStructureCredit.controllerNetDP.text.replaceAll(",", "")),
      null, // installment decline n
      _providerStructureCredit.controllerPaymentPerYear.text != "" ? int.parse(_providerStructureCredit.controllerPaymentPerYear.text) : null,// payment per year
      _providerStructureCredit.controllerBranchDP.text != "" ? double.parse(_providerStructureCredit.controllerBranchDP.text.replaceAll(",", "")) : null,
      _providerStructureCredit.controllerGrossDP.text == "" || _providerStructureCredit.controllerGrossDP.text == null ? 0 : double.parse(_providerStructureCredit.controllerGrossDP.text.replaceAll(",", "")),
      _providerStructureCredit.controllerTotalLoan.text == "" || _providerStructureCredit.controllerTotalLoan.text == null ? 0 : double.parse(_providerStructureCredit.controllerTotalLoan.text.replaceAll(",", "")),
      _providerStructureCredit.controllerInstallment.text == "" || _providerStructureCredit.controllerInstallment.text == null ? 0 : double.parse(_providerStructureCredit.controllerInstallment.text.replaceAll(",", "")),
      null,
      _providerStructureCredit.controllerLTV.text == "" ? 0 : double.parse(_providerStructureCredit.controllerLTV.text.replaceAll(",", "")),// ltv//sini
      null,
     _providerStructureCreditIncome.controllerDebtComparison.text == "" ? 0 : double.parse(_providerStructureCreditIncome.controllerDebtComparison.text.replaceAll(",", "")),//dsr dkk
     _providerStructureCreditIncome.controllerIncomeComparison.text == "" ? 0 : double.parse(_providerStructureCreditIncome.controllerIncomeComparison.text.replaceAll(",", "")),//dsr dkk
     _providerStructureCreditIncome.controllerDSC.text == "" ? 0 : double.parse(_providerStructureCreditIncome.controllerDSC.text.replaceAll(",", "")),//dsr dkk
     _providerStructureCreditIncome.controllerIRR.text == "" ? 0 : double.parse(_providerStructureCreditIncome.controllerIRR.text.replaceAll(",", "")),//dsr dkk
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      DateTime.now().toString(),
      _preferences.getString("username"),
      null,
      null,
      1,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      this._controllerDetailModel.text != "" ? this._controllerDetailModel.text : "",
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      this._matriksDealerSelected != null ? this._matriksDealerSelected.kode : "",
      this._matriksDealerSelected != null ? this._matriksDealerSelected.deskripsi : "",
      null,
      null,
      null,
      null,
      null,
      null,
      this._sourceOrderSelected != null ? this._sourceOrderSelected.deskripsi : "",//this._sourceOrderNameSelected != null ? this._sourceOrderNameSelected.kode : "",
      this._sourceOrderNameSelected != null ? this._sourceOrderNameSelected.deskripsi : "",
      this._thirdPartySelected != null ? this._thirdPartySelected.deskripsi : "",
      this._isTypeOfFinancingModelChanges ? "1" : "0",
      this._isBusinessActivitiesModelChanges ? "1" : "0",
      this._isBusinessActivitiesTypeModelChanges ? "1" : "0",
      this._isGroupObjectChanges ? "1" : "0",
      this._isObjectChanges ? "1" : "0",
      this._isTypeProductChanges ? "1" : "0",
      this._isBrandObjectChanges ? "1" : "0",
      this._isObjectTypeChanges ? "1" : "0",
      this._isModelObjectChanges ? "1" : "0",
      this._isDetailModelChanges ? "1" : "0",
      this._isUsageObjectModelChanges ? "1" : "0",
      this._isObjectPurposeChanges ? "1" : "0",
      this._isGroupSalesChanges ? "1" : "0",
      this._isGrupIdChanges ? "1" : "0",
      this._isSourceOrderChanges ? "1" : "0",
      this._isSourceOrderNameChanges ? "1" : "0",
      this._isThirdPartyTypeChanges ? "1" : "0",
      this._isThirdPartyChanges ? "1" : "0",
      this._isMatriksDealerChanges ? "1" : "0",
      this._isKegiatanChanges ? "1" : "0",
      this._isSentraDChanges ? "1" : "0",
      this._isUnitDChanges ? "1" : "0",
      this._isProgramChanges ? "1" : "0",
      this._isRehabTypeChanges ? "1" : "0",
      this._isReferenceNumberChanges ? "1" : "0",
    ));
  }

  Future<bool> deleteSQLite() async{
    return await _dbHelper.deleteMS2ApplObject();
  }

  String get applObjtID => _applObjtID;

  set applObjtID(String value) {
    this._applObjtID = value;
  }

  Future<void> setDataFromSQLite(BuildContext context) async{
    debugPrint("OBJECT JALAN");
    SharedPreferences _preference = await SharedPreferences.getInstance();
    debugPrint("cust type = ${_preference.getString("cust_type")}");
    var _check = await _dbHelper.selectMS2ApplObject();
    debugPrint("CEK DATA UNIT OBJECT $_check");
    if(_check.isNotEmpty){
      if(_preference.getString("cust_type") == "COM"){
        for(int i=0; i<listTypeOfFinancing.length; i++){
          if(_check[0]['financing_type'] == listTypeOfFinancing[i].financingTypeId){
            this._typeOfFinancingModelSelected = listTypeOfFinancing[i];
          }
        }

        if(_typeOfFinancingModelSelected != null){
          await getBusinessActivities(_preference.getString("cust_type"), context);
          for(int i=0; i<listBusinessActivities.length; i++){
            debugPrint("CEK_OBJECT_COM ${_check[0]['buss_activities'] == listBusinessActivities[i].id.toString()}");
            if(_check[0]['buss_activities'] == listBusinessActivities[i].id.toString()){
              this._businessActivitiesModelSelected = listBusinessActivities[i];
            }
          }
        }
        debugPrint("CEK_OBJECT_COM_BUSS_1 ${this._businessActivitiesModelSelected.id}");

        if(_businessActivitiesModelSelected != null){
          await getListBusinessActivitiesType(_preference.getString("cust_type"), context);
          for(int i=0; i<_listBusinessActivitiesType.length; i++){
            if(_check[0]['buss_activities_type'] == _listBusinessActivitiesType[i].id.toString()){
              this._businessActivitiesTypeModelSelected = _listBusinessActivitiesType[i];
            }
          }
        }
        debugPrint("CEK_OBJECT_COM_BUSS_TYPE ${this._businessActivitiesTypeModelSelected.id}");
      }

      this._groupObjectSelected = GroupObjectModel(_check[0]['group_object'], _check[0]['group_object_desc']);
      _check[0]['group_object'] != "" && _check[0]['group_object'] != "null" ? this._controllerGroupObject.text = "${_check[0]['group_object']} - ${_check[0]['group_object_desc']}" : '';

      this._objectSelected = ObjectModel(_check[0]['object'], _check[0]['object_desc']);
      _check[0]['object'] != "" && _check[0]['object'] != "null" ? this._controllerObject.text = "${_check[0]['object']} - ${_check[0]['object_desc']}" : '';

      this._productTypeSelected = ProductTypeModel(_check[0]['product_type'], _check[0]['product_type_desc']);
      _check[0]['product_type'] != "" && _check[0]['product_type'] != "null" ? this._controllerTypeProduct.text = "${_check[0]['product_type']} - ${_check[0]['product_type_desc']}" : '';

      this._brandObjectSelected = BrandObjectModel(_check[0]['brand_object'], _check[0]['brand_object_desc']);
      _check[0]['brand_object'] != "" && _check[0]['brand_object'] != "null" ? this._controllerBrandObject.text = "${_check[0]['brand_object']} - ${_check[0]['brand_object_desc']}" : '';

      this._objectTypeSelected = ObjectTypeModel(_check[0]['object_type'], _check[0]['object_type_desc']);
      _check[0]['object_type'] != "" && _check[0]['object_type'] != "null" ? this._controllerObjectType.text = "${_check[0]['object_type']} - ${_check[0]['object_type_desc']}" : '';

      this._modelObjectSelected = ModelObjectModel(_check[0]['object_model'], _check[0]['object_model_desc']);
      _check[0]['object_model'] != "" && _check[0]['object_model'] != "null" ? this._controllerModelObject.text = "${_check[0]['object_model']} - ${_check[0]['object_model_desc']}" : '';

      this._controllerDetailModel.text = _check[0]['appl_model_dtl'] != "" && _check[0]['appl_model_dtl'] != "null" ? _check[0]['appl_model_dtl'] : "";

      // field pemakaian objek
      this._objectUsageModel = ObjectUsageModel(_check[0]['object_used'], _check[0]['object_used_desc']);
      _check[0]['object_used'] != "" && _check[0]['object_used'] != "null" ? this._controllerUsageObjectModel.text = "${_check[0]['object_used']} - ${_check[0]['object_used_desc']}" : '';

      // field tujuan penggunaan objek
      this._objectPurposeSelected = ObjectPurposeModel(_check[0]['object_purpose'], _check[0]['object_purpose_desc']);
      _check[0]['object_purpose'] != "" && _check[0]['object_purpose'] != "null" ? this._controllerObjectPurpose.text = "${_check[0]['object_purpose']} - ${_check[0]['object_purpose_desc']}" : '';

      this._groupSalesSelected = GroupSalesModel(_check[0]['group_sales'], _check[0]['group_sales_desc']);
      _check[0]['group_sales'] != "" && _check[0]['group_sales'] != "null" ? this._controllerGroupSales.text = "${_check[0]['group_sales']} - ${_check[0]['group_sales_desc']}" : '';

      this._grupIdSelected = GrupIdModel(_check[0]['grup_id'], _check[0]['grup_id_desc']);
      _check[0]['grup_id'] != "" && _check[0]['grup_id'] != "null" ? this._controllerGrupId.text = "${_check[0]['grup_id']} - ${_check[0]['grup_id_desc']}" : '';

      this._sourceOrderSelected = SourceOrderModel(_check[0]['order_source'], _check[0]['name_order_source']);
      _check[0]['order_source'] != "" && _check[0]['order_source'] != "null" ? this._controllerSourceOrder.text = "${_check[0]['order_source']} - ${_check[0]['name_order_source']}" : '';

      this._sourceOrderNameSelected = SourceOrderNameModel(_check[0]['order_source_name'], _check[0]['name_order_source_desc']);
      _check[0]['order_source_name'] != "" && _check[0]['order_source_name'] != "null" ? this._controllerSourceOrderName.text = "${_check[0]['order_source_name']} - ${_check[0]['name_order_source_desc']}" : '';

      this._thirdPartyTypeSelected = ThirdPartyTypeModel(_check[0]['third_party_type'],_check[0]['third_party_type_desc']);
      _check[0]['third_party_type'] != "" && _check[0]['third_party_type'] != "null" ? this._controllerThirdPartyType.text = "${_check[0]['third_party_type']} - ${_check[0]['third_party_type_desc']}" : '';

      this._thirdPartySelected = ThirdPartyModel(_check[0]['third_party_id'],_check[0]['third_party_desc'],_check[0]['sentra_d'], _check[0]['sentra_d_desc'], _check[0]['unit_d'], _check[0]['unit_d_desc'], null, null, null);
      _check[0]['third_party_id'] != "" && _check[0]['third_party_id'] != "null" ? this._controllerThirdParty.text = "${_check[0]['third_party_id']} - ${_check[0]['third_party_desc']}" : '';

      this._matriksDealerSelected = MatriksDealerModel(_check[0]['dealer_matrix_id'], _check[0]['dealer_matrix']); //kurang id
      _check[0]['dealer_matrix_id'] != "" && _check[0]['dealer_matrix_id'] != "null" ? this._controllerMatriksDealer.text = "${_check[0]['dealer_matrix_id']} - ${_check[0]['dealer_matrix']}" : '';

      this._activitiesModel = ActivitiesModel(_check[0]['work'], _check[0]['work_desc']);
      _check[0]['work'] != "" && _check[0]['work'] != "null" ? this._controllerKegiatan.text = "${_check[0]['work']} - ${_check[0]['work_desc']}" : '';

      this._controllerSentraD.text = _check[0]['sentra_d_desc'];
      this._controllerUnitD.text = _check[0]['unit_d_desc'];

      this._programSelected = ProgramModel(_check[0]['program'],_check[0]['program_desc']);
      _check[0]['program'] != "" && _check[0]['program'] != "null" ? this._controllerProgram.text = "${_check[0]['program']} - ${_check[0]['program_desc']}" : '';

      this._rehabTypeSelected = RehabTypeModel(_check[0]['rehab_type'],_check[0]['rehab_type_desc']);
      _check[0]['rehab_type'] != "" && _check[0]['rehab_type'] != "null" ? this._controllerRehabType.text = "${_check[0]['rehab_type']} - ${_check[0]['rehab_type_desc']}" : '';

      this._referenceNumberModel = ReferenceNumberModel(_check[0]['reference_no'], _check[0]['reference_no_desc']);
      _check[0]['reference_no'] != "" && _check[0]['reference_no'] != "null" ? this._controllerReferenceNumber.text = "${_check[0]['reference_no']} - ${_check[0]['reference_no_desc']}" : '';
      applObjtID = _check[0]['applObjtID'];
      // Provider.of<TaksasiUnitChangeNotifier>(context,listen: false).setIsVisible(_check[0]['group_object']); old
      await _getProdMatrixID(context, false);
    }
    if(_lastKnownState == "DKR") {
      checkDataDakor();
    }
    notifyListeners();
  }

  TextEditingController get controllerMatriksDealer => _controllerMatriksDealer;

  set controllerMatriksDealer(TextEditingController value) {
    this._controllerMatriksDealer = value;
    notifyListeners();
  }

  void getMatriksDealer(BuildContext context) async {
    try {
      // this._listMatriksDealer.clear();
      loadData = true;
      final ioc = new HttpClient();
      ioc.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;

      final _http = IOClient(ioc);

      var _body = jsonEncode({
        "refOne": thirdPartySelected.kode,
        "refTwo": Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).objectSelected.id,
        "refThree": Provider.of<InfoAppChangeNotifier>(context,listen: false).controllerOrderDate.text
      });
      print("matrix dealer = $_body");

      String _fieldMatriksDealer = await storage.read(key: "FieldMatriksDealer");
      final _response = await _http.post(
        "${BaseUrl.urlGeneral}$_fieldMatriksDealer",
        //   "${urlPublic}pihak-ketiga/get_info_dealermatrix",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      );
      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        if(_result.isEmpty){
          showSnackBar("Matriks Dealer tidak ditemukan");
          loadData = false;
        } else{
          this._matriksDealerSelected = MatriksDealerModel(_result[0]['kode'], _result[0]['deskripsi']);
          this._controllerMatriksDealer.text = "${_result[0]['kode']} - ${_result[0]['deskripsi']}";
          notifyListeners();
          // for(int i=0; i <_result.length; i++){
          //   this._listMatriksDealer.add(
          //       MatriksDealerModel(_result[i]['kode'], _result[i]['deskripsi'])
          //   );
          // }
          loadData = false;
        }
      }
      else{
        showSnackBar("Error response status ${_response.statusCode}");
        this._loadData = false;
      }
    } catch(e){
      loadData = false;
      showSnackBar("Error ${e.toString()}");
    }
    notifyListeners();
  }

  Future<void> setPreference(BuildContext context) async {
    getDataFromDashboard(context);
    SharedPreferences _preference = await SharedPreferences.getInstance();
    this._custType = _preference.getString("cust_type");
    this._lastKnownState = _preference.getString("last_known_state");
    this._controllerSentraD.text = _preference.getString("SentraD");
    this._controllerUnitD.text = _preference.getString("UnitD");
    await disablePACIAAOSCONA();
  }

  Future<void> disablePACIAAOSCONA() async {
    SharedPreferences _preference = await SharedPreferences.getInstance();
    if(this._lastKnownState == "AOS" || this._lastKnownState == "CONA" || this._lastKnownState == "IA" || this._lastKnownState == "PAC") {
      this._isDisablePACIAAOSCONA = true;
    }
    this._disableJenisPenawaran = false;
    if(_preference.getString("jenis_penawaran") == "002"){
      this._disableJenisPenawaran = true;
    }
  }

  ShowMandatoryInfoObjectUnitModel _showMandatoryInfoObjectUnitIdeModel;
  ShowMandatoryInfoObjectUnitModel _showMandatoryInfoObjectUnitIdeCompanyModel;
  void showMandatoryIdeModel(BuildContext context){
    _showMandatoryInfoObjectUnitIdeModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoObjectUnitIdeModel;
    _showMandatoryInfoObjectUnitIdeCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoObjectUnitIdeCompanyModel;
  }

  ShowMandatoryInfoObjectUnitModel _showMandatoryInfoObjectUnitRegulerSurveyModel;
  ShowMandatoryInfoObjectUnitModel _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel;
  void showMandatoryRegulerSurveyModel(BuildContext context){
    _showMandatoryInfoObjectUnitRegulerSurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoObjectUnitRegulerSurveyModel;
    _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoObjectUnitRegulerSurveyCompanyModel;
  }

  ShowMandatoryInfoObjectUnitModel _showMandatoryInfoObjectUnitPacModel;
  ShowMandatoryInfoObjectUnitModel _showMandatoryInfoObjectUnitPacCompanyModel;
  void showMandatoryPacModel(BuildContext context){
    _showMandatoryInfoObjectUnitPacModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoObjectUnitPacModel;
    _showMandatoryInfoObjectUnitPacCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoObjectUnitPacCompanyModel;
  }

  ShowMandatoryInfoObjectUnitModel _showMandatoryInfoObjectUnitResurveyModel;
  ShowMandatoryInfoObjectUnitModel _showMandatoryInfoObjectUnitResurveyCompanyModel;
  void showMandatoryResurveyModel(BuildContext context){
    _showMandatoryInfoObjectUnitResurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoObjectUnitResurveyModel;
    _showMandatoryInfoObjectUnitResurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoObjectUnitResurveyCompanyModel;
  }

  ShowMandatoryInfoObjectUnitModel _showMandatoryInfoObjectUnitDakorModel;
  ShowMandatoryInfoObjectUnitModel _showMandatoryInfoObjectUnitDakorCompanyModel;
  void showMandatoryDakorModel(BuildContext context){
    _showMandatoryInfoObjectUnitDakorModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoObjectUnitDakorModel;
    _showMandatoryInfoObjectUnitDakorCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoObjectUnitDakorCompanyModel;
  }

  void getDataFromDashboard(BuildContext context){
    showMandatoryIdeModel(context);
    showMandatoryRegulerSurveyModel(context);
    showMandatoryPacModel(context);
    showMandatoryResurveyModel(context);
    showMandatoryDakorModel(context);
  }

  // Jenis Pembiayaan
  bool isTypeOfFinancingModelVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isTypeOfFinancingModelVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isTypeOfFinancingModelVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isTypeOfFinancingModelVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isTypeOfFinancingModelVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isTypeOfFinancingModelVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isTypeOfFinancingModelVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isTypeOfFinancingModelVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isTypeOfFinancingModelVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isTypeOfFinancingModelVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isTypeOfFinancingModelVisible;
      }
    }
    return value;
  }

  // Kegiatan Usaha
  bool isBusinessActivitiesModelVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isBusinessActivitiesModelVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isBusinessActivitiesModelVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isBusinessActivitiesModelVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isBusinessActivitiesModelVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isBusinessActivitiesModelVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isBusinessActivitiesModelVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isBusinessActivitiesModelVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isBusinessActivitiesModelVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isBusinessActivitiesModelVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isBusinessActivitiesModelVisible;
      }
    }
    return value;
  }

  // Jenis Kegiatan Usaha
  bool isBusinessActivitiesTypeModelVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isBusinessActivitiesTypeModelVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isBusinessActivitiesTypeModelVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isBusinessActivitiesTypeModelVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isBusinessActivitiesTypeModelVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isBusinessActivitiesTypeModelVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isBusinessActivitiesTypeModelVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isBusinessActivitiesTypeModelVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isBusinessActivitiesTypeModelVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isBusinessActivitiesTypeModelVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isBusinessActivitiesTypeModelVisible;
      }
    }
    return value;
  }

  // Grup Objek
  bool isGroupObjectVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isGroupObjectVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isGroupObjectVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isGroupObjectVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isGroupObjectVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isGroupObjectVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isGroupObjectVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isGroupObjectVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isGroupObjectVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isGroupObjectVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isGroupObjectVisible;
      }
    }
    return value;
  }

  // Objek
  bool isObjectVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isObjectVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isObjectVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isObjectVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isObjectVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isObjectVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isObjectVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isObjectVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isObjectVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isObjectVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isObjectVisible;
      }
    }
    return value;
  }

  // Jenis Produk
  bool isTypeProductVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isTypeProductVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isTypeProductVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isTypeProductVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isTypeProductVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isTypeProductVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isTypeProductVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isTypeProductVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isTypeProductVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isTypeProductVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isTypeProductVisible;
      }
    }
    return value;
  }

  // Merk Objek
  bool isBrandObjectVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isBrandObjectVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isBrandObjectVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isBrandObjectVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isBrandObjectVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isBrandObjectVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isBrandObjectVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isBrandObjectVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isBrandObjectVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isBrandObjectVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isBrandObjectVisible;
      }
    }
    return value;
  }

  // Jenis Objek
  bool isObjectTypeVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isObjectTypeVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isObjectTypeVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isObjectTypeVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isObjectTypeVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isObjectTypeVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isObjectTypeVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isObjectTypeVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isObjectTypeVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isObjectTypeVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isObjectTypeVisible;
      }
    }
    return value;
  }

  // Model Objek
  bool isModelObjectVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isModelObjectVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isModelObjectVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isModelObjectVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isModelObjectVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isModelObjectVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isModelObjectVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isModelObjectVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isModelObjectVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isModelObjectVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isModelObjectVisible;
      }
    }
    return value;
  }

  // Model Detail
  bool isDetailModelVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isDetailModelVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isDetailModelVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isDetailModelVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isDetailModelVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isDetailModelVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isDetailModelVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isDetailModelVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isDetailModelVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isDetailModelVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isDetailModelVisible;
      }
    }
    return value;
  }

  // Pemakaian Objek
  bool isUsageObjectModelVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isUsageObjectModelVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isUsageObjectModelVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isUsageObjectModelVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isUsageObjectModelVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isUsageObjectModelVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isUsageObjectModelVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isUsageObjectModelVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isUsageObjectModelVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isUsageObjectModelVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isUsageObjectModelVisible;
      }
    }
    return value;
  }

  // Tujuan Penggunaan Objek
  bool isObjectPurposeVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isObjectPurposeVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isObjectPurposeVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isObjectPurposeVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isObjectPurposeVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isObjectPurposeVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isObjectPurposeVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isObjectPurposeVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isObjectPurposeVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isObjectPurposeVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isObjectPurposeVisible;
      }
    }
    return value;
  }

  // Grup Sales
  bool isGroupSalesVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isGroupSalesVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isGroupSalesVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isGroupSalesVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isGroupSalesVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isGroupSalesVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isGroupSalesVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isGroupSalesVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isGroupSalesVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isGroupSalesVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isGroupSalesVisible;
      }
    }
    return value;
  }

  // Group Id
  bool isGrupIdVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isGrupIdVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isGrupIdVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isGrupIdVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isGrupIdVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isGrupIdVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isGrupIdVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isGrupIdVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isGrupIdVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isGrupIdVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isGrupIdVisible;
      }
    }
    return value;
  }

  // Sumber Order
  bool isSourceOrderVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isSourceOrderVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isSourceOrderVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isSourceOrderVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isSourceOrderVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isSourceOrderVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isSourceOrderVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isSourceOrderVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isSourceOrderVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isSourceOrderVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isSourceOrderVisible;
      }
    }
    return value;
  }

  // Nama Sumber Order
  bool isSourceOrderNameVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isSourceOrderNameVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isSourceOrderNameVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isSourceOrderNameVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isSourceOrderNameVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isSourceOrderNameVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isSourceOrderNameVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isSourceOrderNameVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isSourceOrderNameVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isSourceOrderNameVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isSourceOrderNameVisible;
      }
    }
    return value;
  }

  // Jenis Pihak Ketiga
  bool isThirdPartyTypeVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isThirdPartyTypeVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isThirdPartyTypeVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isThirdPartyTypeVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isThirdPartyTypeVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isThirdPartyTypeVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isThirdPartyTypeVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isThirdPartyTypeVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isThirdPartyTypeVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isThirdPartyTypeVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isThirdPartyTypeVisible;
      }
    }
    return value;
  }

  // Pihak Ketiga
  bool isThirdPartyVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isThirdPartyVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isThirdPartyVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isThirdPartyVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isThirdPartyVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isThirdPartyVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isThirdPartyVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isThirdPartyVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isThirdPartyVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isThirdPartyVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isThirdPartyVisible;
      }
    }
    return value;
  }

  // Matriks Dealer
  bool isMatriksDealerVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isMatriksDealerVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isMatriksDealerVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isMatriksDealerVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isMatriksDealerVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isMatriksDealerVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isMatriksDealerVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isMatriksDealerVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isMatriksDealerVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isMatriksDealerVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isMatriksDealerVisible;
      }
    }
    return value;
  }

  // Kegiatan
  bool isKegiatanVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isKegiatanVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isKegiatanVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isKegiatanVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isKegiatanVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isKegiatanVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isKegiatanVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isKegiatanVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isKegiatanVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isKegiatanVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isKegiatanVisible;
      }
    }
    return value;
  }

  // Sentra D
  bool isSentraDVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isSentraDVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isSentraDVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isSentraDVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isSentraDVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isSentraDVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isSentraDVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isSentraDVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isSentraDVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isSentraDVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isSentraDVisible;
      }
    }
    return value;
  }

  // Unit D
  bool isUnitDVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isUnitDVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isUnitDVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isUnitDVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isUnitDVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isUnitDVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isUnitDVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isUnitDVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isUnitDVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isUnitDVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isUnitDVisible;
      }
    }
    return value;
  }

  // Program
  bool isProgramVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isProgramVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isProgramVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isProgramVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isProgramVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isProgramVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isProgramVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isProgramVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isProgramVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isProgramVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isProgramVisible;
      }
    }
    return value;
  }

  // Jenis Rehab
  bool isRehabTypeVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isRehabTypeVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isRehabTypeVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isRehabTypeVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isRehabTypeVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isRehabTypeVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isRehabTypeVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isRehabTypeVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isRehabTypeVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isRehabTypeVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isRehabTypeVisible;
      }
    }
    return value;
  }

  // No Reference
  bool isReferenceNumberVisible() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isReferenceNumberVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isReferenceNumberVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isReferenceNumberVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isReferenceNumberVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isReferenceNumberVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isReferenceNumberVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isReferenceNumberVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isReferenceNumberVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isReferenceNumberVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isReferenceNumberVisible;
      }
    }
    return value;
  }

  // Jenis Pembiayaan
  bool isTypeOfFinancingModelMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isTypeOfFinancingModelMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isTypeOfFinancingModelMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isTypeOfFinancingModelMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isTypeOfFinancingModelMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isTypeOfFinancingModelMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isTypeOfFinancingModelMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isTypeOfFinancingModelMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isTypeOfFinancingModelMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isTypeOfFinancingModelMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isTypeOfFinancingModelMandatory;
      }
    }
    return value;
  }

  // Kegiatan Usaha
  bool isBusinessActivitiesModelMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isBusinessActivitiesModelMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isBusinessActivitiesModelMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isBusinessActivitiesModelMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isBusinessActivitiesModelMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isBusinessActivitiesModelMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isBusinessActivitiesModelMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isBusinessActivitiesModelMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isBusinessActivitiesModelMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isBusinessActivitiesModelMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isBusinessActivitiesModelMandatory;
      }
    }
    return value;
  }

  // Jenis Kegiatan Usaha
  bool isBusinessActivitiesTypeModelMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isBusinessActivitiesTypeModelMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isBusinessActivitiesTypeModelMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isBusinessActivitiesTypeModelMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isBusinessActivitiesTypeModelMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isBusinessActivitiesTypeModelMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isBusinessActivitiesTypeModelMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isBusinessActivitiesTypeModelMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isBusinessActivitiesTypeModelMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isBusinessActivitiesTypeModelMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isBusinessActivitiesTypeModelMandatory;
      }
    }
    return value;
  }

  // Grup Objek
  bool isGroupObjectMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isGroupObjectMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isGroupObjectMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isGroupObjectMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isGroupObjectMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isGroupObjectMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isGroupObjectMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isGroupObjectMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isGroupObjectMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isGroupObjectMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isGroupObjectMandatory;
      }
    }
    return value;
  }

  // Objek
  bool isObjectMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isObjectMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isObjectMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isObjectMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isObjectMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isObjectMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isObjectMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isObjectMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isObjectMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isObjectMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isObjectMandatory;
      }
    }
    return value;
  }

  // Jenis Produk
  bool isTypeProductMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isTypeProductMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isTypeProductMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isTypeProductMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isTypeProductMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isTypeProductMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isTypeProductMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isTypeProductMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isTypeProductMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isTypeProductMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isTypeProductMandatory;
      }
    }
    return value;
  }

  // Merk Objek
  bool isBrandObjectMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isBrandObjectMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isBrandObjectMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isBrandObjectMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isBrandObjectMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isBrandObjectMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isBrandObjectMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isBrandObjectMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isBrandObjectMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isBrandObjectMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isBrandObjectMandatory;
      }
    }
    return value;
  }

  // Jenis Objek
  bool isObjectTypeMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isObjectTypeMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isObjectTypeMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isObjectTypeMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isObjectTypeMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isObjectTypeMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isObjectTypeMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isObjectTypeMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isObjectTypeMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isObjectTypeMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isObjectTypeMandatory;
      }
    }
    return value;
  }

  // Model Objek
  bool isModelObjectMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isModelObjectMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isModelObjectMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isModelObjectMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isModelObjectMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isModelObjectMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isModelObjectMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isModelObjectMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isModelObjectMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isModelObjectMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isModelObjectMandatory;
      }
    }
    return value;
  }

  // Model Detail
  bool isDetailModelMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isDetailModelMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isDetailModelMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isDetailModelMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isDetailModelMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isDetailModelMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isDetailModelMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isDetailModelMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isDetailModelMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isDetailModelMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isDetailModelMandatory;
      }
    }
    return value;
  }

  // Pemakaian Objek
  bool isUsageObjectModelMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isUsageObjectModelMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isUsageObjectModelMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isUsageObjectModelMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isUsageObjectModelMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isUsageObjectModelMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isUsageObjectModelMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isUsageObjectModelMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isUsageObjectModelMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isUsageObjectModelMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isUsageObjectModelMandatory;
      }
    }
    return value;
  }

  // Tujuan Penggunaan Objek
  bool isObjectPurposeMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isObjectPurposeMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isObjectPurposeMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isObjectPurposeMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isObjectPurposeMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isObjectPurposeMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isObjectPurposeMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isObjectPurposeMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isObjectPurposeMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isObjectPurposeMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isObjectPurposeMandatory;
      }
    }
    return value;
  }

  // Grup Sales
  bool isGroupSalesMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isGroupSalesMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isGroupSalesMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isGroupSalesMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isGroupSalesMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isGroupSalesMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isGroupSalesMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isGroupSalesMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isGroupSalesMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isGroupSalesMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isGroupSalesMandatory;
      }
    }
    return value;
  }

  // Group Id
  bool isGrupIdMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isGrupIdMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isGrupIdMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isGrupIdMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isGrupIdMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isGrupIdMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isGrupIdMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isGrupIdMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isGrupIdMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isGrupIdMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isGrupIdMandatory;
      }
    }
    return value;
  }

  // Sumber Order
  bool isSourceOrderMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isSourceOrderMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isSourceOrderMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isSourceOrderMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isSourceOrderMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isSourceOrderMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isSourceOrderMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isSourceOrderMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isSourceOrderMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isSourceOrderMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isSourceOrderMandatory;
      }
    }
    return value;
  }

  // Nama Sumber Order
  bool isSourceOrderNameMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isSourceOrderNameMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isSourceOrderNameMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isSourceOrderNameMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isSourceOrderNameMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isSourceOrderNameMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isSourceOrderNameMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isSourceOrderNameMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isSourceOrderNameMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isSourceOrderNameMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isSourceOrderNameMandatory;
      }
    }
    return value;
  }

  // Jenis Pihak Ketiga
  bool isThirdPartyTypeMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isThirdPartyTypeMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isThirdPartyTypeMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isThirdPartyTypeMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isThirdPartyTypeMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isThirdPartyTypeMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isThirdPartyTypeMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isThirdPartyTypeMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isThirdPartyTypeMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isThirdPartyTypeMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isThirdPartyTypeMandatory;
      }
    }
    return value;
  }

  // Pihak Ketiga
  bool isThirdPartyMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isThirdPartyMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isThirdPartyMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isThirdPartyMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isThirdPartyMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isThirdPartyMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isThirdPartyMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isThirdPartyMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isThirdPartyMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isThirdPartyMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isThirdPartyMandatory;
      }
    }
    return value;
  }

  // Matriks Dealer
  bool isMatriksDealerMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isMatriksDealerMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isMatriksDealerMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isMatriksDealerMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isMatriksDealerMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isMatriksDealerMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isMatriksDealerMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isMatriksDealerMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isMatriksDealerMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isMatriksDealerMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isMatriksDealerMandatory;
      }
    }
    return value;
  }

  // Kegiatan
  bool isKegiatanMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isKegiatanMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isKegiatanMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isKegiatanMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isKegiatanMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isKegiatanMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isKegiatanMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isKegiatanMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isKegiatanMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isKegiatanMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isKegiatanMandatory;
      }
    }
    return value;
  }

  // Sentra D
  bool isSentraDMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isSentraDMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isSentraDMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isSentraDMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isSentraDMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isSentraDMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isSentraDMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isSentraDMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isSentraDMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isSentraDMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isSentraDMandatory;
      }
    }
    return value;
  }

  // Unit D
  bool isUnitDMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isUnitDMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isUnitDMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isUnitDMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isUnitDMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isUnitDMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isUnitDMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isUnitDMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isUnitDMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isUnitDMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isUnitDMandatory;
      }
    }
    return value;
  }

  // Program
  bool isProgramMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isProgramMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isProgramMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isProgramMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isProgramMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isProgramMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isProgramMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isProgramMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isProgramMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isProgramMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isProgramMandatory;
      }
    }
    return value;
  }

  // Jenis Rehab
  bool isRehabTypeMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isRehabTypeMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isRehabTypeMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isRehabTypeMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isRehabTypeMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isRehabTypeMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isRehabTypeMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isRehabTypeMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isRehabTypeMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isRehabTypeMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isRehabTypeMandatory;
      }
    }
    return value;
  }

  // No Reference
  bool isReferenceNumberMandatory() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeModel.isReferenceNumberMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyModel.isReferenceNumberMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacModel.isReferenceNumberMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyModel.isReferenceNumberMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorModel.isReferenceNumberMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoObjectUnitIdeCompanyModel.isReferenceNumberMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoObjectUnitRegulerSurveyCompanyModel.isReferenceNumberMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoObjectUnitPacCompanyModel.isReferenceNumberMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoObjectUnitResurveyCompanyModel.isReferenceNumberMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoObjectUnitDakorCompanyModel.isReferenceNumberMandatory;
      }
    }
    return value;
  }

  void checkDataDakor() async{
    var _check = await _dbHelper.selectMS2ApplObject();
    // debugPrint("model detail ${this._controllerDetailModel.text} ${_check[0]['appl_model_dtl']}  ${_check[0]['edit_model_detail']}");
    if(_check.isNotEmpty && _lastKnownState == "DKR"){
      print("cek ${_check[0]['edit_financing_type']}");
      if(this._custType == "COM"){
        isTypeOfFinancingModelChanges = this._typeOfFinancingModelSelected.financingTypeId != _check[0]['financing_type'] || _check[0]['edit_financing_type'] == "1";
        isBusinessActivitiesModelChanges = this._businessActivitiesModelSelected.id.toString() != _check[0]['buss_activities'] || _check[0]['edit_buss_activities'] == "1";
        isBusinessActivitiesTypeModelChanges = this._businessActivitiesTypeModelSelected.id.toString() != _check[0]['buss_activities_type'] || _check[0]['edit_buss_activities_type'] == "1";
      }
      isGroupObjectChanges = this._groupObjectSelected.KODE != _check[0]['group_object'] || _check[0]['edit_group_object'] == "1";
      isObjectChanges = this._objectSelected.id != _check[0]['object'] || _check[0]['edit_object'] == "1";
      isTypeProductChanges = this._productTypeSelected.id != _check[0]['product_type'] || _check[0]['edit_product_type'] == "1";
      isBrandObjectChanges = this._brandObjectSelected.id != _check[0]['brand_object'] || _check[0]['edit_brand_object'] == "1";
      isObjectTypeChanges = this._objectTypeSelected.id != _check[0]['object_type'] || _check[0]['edit_object_type'] == "1";
      isModelObjectChanges = this._modelObjectSelected.id != _check[0]['object_model'] || _check[0]['edit_object_model'] == "1";
      isDetailModelChanges = this._controllerDetailModel.text != _check[0]['appl_model_dtl'] || _check[0]['edit_model_detail'] == "1";
      isUsageObjectModelChanges = this._objectUsageModel.id != _check[0]['object_used'] || _check[0]['edit_object_used'] == "1";
      isObjectPurposeChanges = this._objectPurposeSelected.id != _check[0]['object_purpose'] || _check[0]['edit_object_purpose'] == "1";
      isGroupSalesChanges = this._groupSalesSelected.kode != _check[0]['group_sales'] || _check[0]['edit_group_sales'] == "1";
      isGrupIdChanges = this._grupIdSelected.kode != _check[0]['grup_id'] || _check[0]['edit_grup_id'] == "1";
      isSourceOrderChanges = this._sourceOrderSelected.kode != _check[0]['order_source'] || _check[0]['edit_order_source'] == "1";
      isSourceOrderNameChanges = this._sourceOrderNameSelected.kode != _check[0]['order_source_name'] || _check[0]['edit_order_source_name'] == "1";
      isThirdPartyTypeChanges = this._thirdPartyTypeSelected.kode != _check[0]['third_party_type'] || _check[0]['edit_third_party_type'] == "1";
      isThirdPartyChanges = this._thirdPartySelected.kode != _check[0]['third_party_id'] || _check[0]['edit_third_party_id'] == "1";
      isMatriksDealerChanges = this._matriksDealerSelected.kode != _check[0]['dealer_matrix_id'] || _check[0]['edit_dealer_matrix'] == "1";
      isKegiatanChanges = this._activitiesModel.kode != _check[0]['work'].toString() || _check[0]['edit_work'] == "1";
      isSentraDChanges = this._controllerSentraD.text != _check[0]['sentra_d_desc'] || _check[0]['edit_sentra_d_desc'] == "1";
      isUnitDChanges = this._controllerUnitD.text != _check[0]['unit_d_desc'] || _check[0]['edit_unit_d_desc'] == "1";
      isProgramChanges = this._programSelected.kode != _check[0]['program'] || _check[0]['edit_program'] == "1";
      isRehabTypeChanges = this._rehabTypeSelected.id != _check[0]['rehab_type'] || _check[0]['edit_rehab_type'] == "1";
      isReferenceNumberChanges = this._referenceNumberModel.noPK != _check[0]['reference_no'] || _check[0]['edit_reference_no'] == "1";
    }
  }

  bool get isReferenceNumberChanges => _isReferenceNumberChanges;

  set isReferenceNumberChanges(bool value) {
    this._isReferenceNumberChanges = value;
    notifyListeners();
  }

  bool get isRehabTypeChanges => _isRehabTypeChanges;

  set isRehabTypeChanges(bool value) {
    this._isRehabTypeChanges = value;
    notifyListeners();
  }

  bool get isProgramChanges => _isProgramChanges;

  set isProgramChanges(bool value) {
    this._isProgramChanges = value;
    notifyListeners();
  }

  bool get isUnitDChanges => _isUnitDChanges;

  set isUnitDChanges(bool value) {
    this._isUnitDChanges = value;
    notifyListeners();
  }

  bool get isSentraDChanges => _isSentraDChanges;

  set isSentraDChanges(bool value) {
    this._isSentraDChanges = value;
    notifyListeners();
  }

  bool get isKegiatanChanges => _isKegiatanChanges;

  set isKegiatanChanges(bool value) {
    this._isKegiatanChanges = value;
    notifyListeners();
  }

  bool get isMatriksDealerChanges => _isMatriksDealerChanges;

  set isMatriksDealerChanges(bool value) {
    this._isMatriksDealerChanges = value;
    notifyListeners();
  }

  bool get isThirdPartyChanges => _isThirdPartyChanges;

  set isThirdPartyChanges(bool value) {
    this._isThirdPartyChanges = value;
    notifyListeners();
  }

  bool get isThirdPartyTypeChanges => _isThirdPartyTypeChanges;

  set isThirdPartyTypeChanges(bool value) {
    this._isThirdPartyTypeChanges = value;
    notifyListeners();
  }

  bool get isSourceOrderNameChanges => _isSourceOrderNameChanges;

  set isSourceOrderNameChanges(bool value) {
    this._isSourceOrderNameChanges = value;
    notifyListeners();
  }

  bool get isSourceOrderChanges => _isSourceOrderChanges;

  set isSourceOrderChanges(bool value) {
    this._isSourceOrderChanges = value;
    notifyListeners();
  }

  bool get isGroupSalesChanges => _isGroupSalesChanges;

  set isGroupSalesChanges(bool value) {
    this._isGroupSalesChanges = value;
    notifyListeners();
  }

  bool get isGrupIdChanges => _isGrupIdChanges;

  set isGrupIdChanges(bool value) {
    this._isGrupIdChanges = value;
    notifyListeners();
  }

  bool get isObjectPurposeChanges => _isObjectPurposeChanges;

  set isObjectPurposeChanges(bool value) {
    this._isObjectPurposeChanges = value;
    notifyListeners();
  }

  bool get isUsageObjectModelChanges => _isUsageObjectModelChanges;

  set isUsageObjectModelChanges(bool value) {
    this._isUsageObjectModelChanges = value;
    notifyListeners();
  }

  bool get isModelObjectChanges => _isModelObjectChanges;

  set isModelObjectChanges(bool value) {
    this._isModelObjectChanges = value;
    notifyListeners();
  }

  bool get isObjectTypeChanges => _isObjectTypeChanges;

  set isObjectTypeChanges(bool value) {
    this._isObjectTypeChanges = value;
    notifyListeners();
  }

  bool get isBrandObjectChanges => _isBrandObjectChanges;

  set isBrandObjectChanges(bool value) {
    this._isBrandObjectChanges = value;
    notifyListeners();
  }

  bool get isTypeProductChanges => _isTypeProductChanges;

  set isTypeProductChanges(bool value) {
    this._isTypeProductChanges = value;
    notifyListeners();
  }

  bool get isObjectChanges => _isObjectChanges;

  set isObjectChanges(bool value) {
    this._isObjectChanges = value;
    notifyListeners();
  }

  bool get isGroupObjectChanges => _isGroupObjectChanges;

  set isGroupObjectChanges(bool value) {
    this._isGroupObjectChanges = value;
    notifyListeners();
  }

  bool get isBusinessActivitiesModelChanges =>
      _isBusinessActivitiesModelChanges;

  set isBusinessActivitiesModelChanges(bool value) {
    this._isBusinessActivitiesModelChanges = value;
    notifyListeners();
  }

  bool get isTypeOfFinancingModelChanges => _isTypeOfFinancingModelChanges;

  set isTypeOfFinancingModelChanges(bool value) {
    this._isTypeOfFinancingModelChanges = value;
    notifyListeners();
  }

  bool get isBusinessActivitiesTypeModelChanges => _isBusinessActivitiesTypeModelChanges;

  set isBusinessActivitiesTypeModelChanges(bool value) {
    this._isBusinessActivitiesTypeModelChanges = value;
    notifyListeners();
  }

  bool get isDetailModelChanges => _isDetailModelChanges;

  set isDetailModelChanges(bool value) {
    this._isDetailModelChanges = value;
    notifyListeners();
  }
}