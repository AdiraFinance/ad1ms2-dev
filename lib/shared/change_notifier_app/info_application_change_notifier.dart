import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/form_m_foto_model.dart';
import 'package:ad1ms2_dev/models/ms2_application_model.dart';
import 'package:ad1ms2_dev/models/proportional_type_of_insurance_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_info_app_model.dart';
import 'package:ad1ms2_dev/screens/form_m/menu_object_information.dart';
import 'package:ad1ms2_dev/shared/dashboard/dashboard_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_parent_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_individu_change_notifier/form_m_parent_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:ad1ms2_dev/shared/resource/submit_data_partial.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';
import '../constants.dart';
import 'information_object_unit_change_notifier.dart';
import 'information_salesman_change_notifier.dart';
import 'marketing_notes_change_notifier.dart';

class InfoAppChangeNotifier with ChangeNotifier {
  bool _autoValidate = false;
  var storage = FlutterSecureStorage();
  DateTime _initialDateOrder = DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);
  DateTime _startSurveyDate = DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);
  DateTime _initialSurveyDate = DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);
  TextEditingController _controllerOrderDate = TextEditingController();
  TextEditingController _controllerSurveyAppointmentDate = TextEditingController();
  TextEditingController _controllerSurveyAppointmentTime = TextEditingController();
  TextEditingController _controllerTotalObject = TextEditingController();
  DateFormat _formatter = DateFormat("dd-MM-yyyy");
  bool _isSignedPK = false;
  JenisKonsepModel _conceptTypeModelSelected;
  ProportionalTypeOfInsuranceModel _proportionalTypeOfInsuranceModelSelected;
  String _numberOfUnitSelected;
  DbHelper _dbHelper = DbHelper();
  String _applNo = "";
  String _custType;
  String _lastKnownState;
  TimeOfDay _selectedTime = TimeOfDay.now();
  String _hour, _minute, _time;
  SubmitDataPartial _submitDataPartial = SubmitDataPartial();
  String _initialRecomendation = "";
  String _finalRecomendation = "";
  int _brmsScoring = 0;
  bool _isDisablePACIAAOSCONA = false;

  bool _isOrderDateChanges = false;
  bool _isSurveyAppointmentDateChanges = false;
  bool _isIsSignedPKChanges = false;
  bool _isConceptTypeModelChanges = false;
  bool _isTotalObjectChanges = false;
  bool _isPropotionalObjectChanges = false;
  bool _isNumberOfUnitChanges = false;

  List<JenisKonsepModel> _listConceptType = [
    JenisKonsepModel("01", "SATU UNIT MULTI JAMINAN"),
    JenisKonsepModel("02", "MULTI UNIT SATU  JAMINAN"),
    JenisKonsepModel("03", "MULTI UNIT DENGAN JAMINAN DAN TANPA JAMINAN")
  ];

  List<ProportionalTypeOfInsuranceModel> _listProportionalTypeOfInsurance = [
    ProportionalTypeOfInsuranceModel("01", "PRORATE"),
    ProportionalTypeOfInsuranceModel("02", "UNIT"),
  ];

  bool get isDisablePACIAAOSCONA => _isDisablePACIAAOSCONA;

  set isDisablePACIAAOSCONA(bool value) {
    this._isDisablePACIAAOSCONA = value;
    notifyListeners();
  }

  List<String> _listNumberUnit = [];

  String get initialRecomendation => _initialRecomendation;

  set initialRecomendation(String value) {
    this._initialRecomendation = value;
    notifyListeners();
  }

  String get finalRecomendation => _finalRecomendation;

  set finalRecomendation(String value) {
    this._finalRecomendation = value;
    notifyListeners();
  }

  int get brmsScoring => _brmsScoring;

  set brmsScoring(int value) {
    this._brmsScoring = value;
    notifyListeners();
  }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  TextEditingController get controllerOrderDate => _controllerOrderDate;

  TextEditingController get controllerSurveyAppointmentDate => _controllerSurveyAppointmentDate;

  TextEditingController get controllerTotalObject => _controllerTotalObject;

  TextEditingController get controllerSurveyAppointmentTime => _controllerSurveyAppointmentTime;

  TimeOfDay get selectedTime => _selectedTime;

  bool get isSignedPK => _isSignedPK;

  set isSignedPK(bool value) {
    this._isSignedPK = value;
    notifyListeners();
  }

  JenisKonsepModel get conceptTypeModelSelected => _conceptTypeModelSelected;

  set conceptTypeModelSelected(JenisKonsepModel value) {
    this._conceptTypeModelSelected = value;
    notifyListeners();
  }

  ProportionalTypeOfInsuranceModel
  get proportionalTypeOfInsuranceModelSelected => _proportionalTypeOfInsuranceModelSelected;

  set proportionalTypeOfInsuranceModelSelected(ProportionalTypeOfInsuranceModel value) {
    this._proportionalTypeOfInsuranceModelSelected = value;
    notifyListeners();
  }

  List<String> get listNumberUnit => _listNumberUnit;

  String get numberOfUnitSelected => _numberOfUnitSelected;

  set numberOfUnitSelected(String value) {
    this._numberOfUnitSelected = value;
    notifyListeners();
  }

  Future<void> addNumberOfUnitList(BuildContext context, int index) async{
    Future.delayed(Duration.zero, () async {
      setPreference();
      if(_custType != "" && index != 99){
        await _setDataFromSQLite(context, index);
      }
      else{
        if (this._controllerTotalObject.text == "0") {
          this._controllerTotalObject.clear();
        }
        this._listNumberUnit.clear();
        this._numberOfUnitSelected = null;
        if (this._controllerTotalObject.text != "") {
          int _lengthList = int.parse(this._controllerTotalObject.text);
          for (int i = 0; i < _lengthList; i++) {
            this._listNumberUnit.add("${i + 1}");
          }
          this._numberOfUnitSelected = _listNumberUnit[0];
        }
      }
      notifyListeners();
    });
  }

  UnmodifiableListView<JenisKonsepModel> get listConceptType {
    return UnmodifiableListView(this._listConceptType);
  }

  UnmodifiableListView<ProportionalTypeOfInsuranceModel>
  get listProportionalTypeOfInsurance {
    return UnmodifiableListView(this._listProportionalTypeOfInsurance);
  }

  DateTime get initialDateOrder => _initialDateOrder;

  set initialDateOrder(DateTime value) {
    this._initialDateOrder = value;
  }

  void selectDateOrder(BuildContext context) async {
    // final DateTime _picked = await showDatePicker(
    //     context: context,
    //     initialDate: _initialDateOrder,
    //     firstDate: DateTime(
    //         DateTime.now().year, DateTime.now().month - 1, DateTime.now().day),
    //     lastDate: DateTime(
    //         DateTime.now().year + 2, DateTime.now().month, DateTime.now().day));
    // if (_picked != null) {
    //   this._initialDateOrder = _picked;
    //   this._controllerOrderDate.text = _formatter.format(_picked);
    //   notifyListeners();
    // } else {
    //   return;
    // }
    SharedPreferences _preference = await SharedPreferences.getInstance();
    var _tanggalAplikasi = _preference.getString("order_date");
    var _picked = await selectDateApplication(context, DateTime.parse(_tanggalAplikasi));
    if (_picked != null) {
      this._initialDateOrder = _picked;
      this._controllerOrderDate.text = _formatter.format(_picked);
      notifyListeners();
    } else {
      return;
    }
  }

  void selectTimeSurvey(BuildContext context) async {
    final TimeOfDay _picked = await showTimePicker(
      context: context,
      initialTime: _selectedTime,
      initialEntryMode: TimePickerEntryMode.dial,
    );
    if (_picked != null){
      _selectedTime = _picked;
      _hour = _selectedTime.hour.toString();
      _minute = _selectedTime.minute.toString();
      if(_hour.length < 2){
        _hour = "0${_selectedTime.hour.toString()}";
      }
      else{
        _hour = _selectedTime.hour.toString();
      }
      if(_minute.length < 2){
        _minute = "0${_selectedTime.minute.toString()}";
      }
      else{
        _minute = _selectedTime.minute.toString();
      }
      _time = _hour + ':' + _minute;
      _controllerSurveyAppointmentTime.text = _time;
      notifyListeners();
    }
    else{
      return;
    }
  }

  void selectSurveyDate(BuildContext context) async {
    // final DateTime _picked = await showDatePicker(
    //     context: context,
    //     initialDate: _initialSurveyDate,
    //     firstDate: DateTime(
    //         DateTime.now().year, DateTime.now().month, DateTime.now().day),
    //     lastDate: DateTime(
    //         DateTime.now().year + 2, DateTime.now().month, DateTime.now().day));
    // if (_picked != null) {
    //   this._initialSurveyDate = _picked;
    //   this._controllerSurveyAppointmentDate.text = _formatter.format(_picked);
    //   notifyListeners();
    // } else {
    //   return;
    // }
    var _picked = await selectDateFirstToday(context, _startSurveyDate, _initialSurveyDate);
    if (_picked != null) {
      this._initialSurveyDate = _picked;
      this._controllerSurveyAppointmentDate.text = _formatter.format(_picked);
      notifyListeners();
    } else {
      return;
    }
  }

  DateTime get initialSurveyDate => _initialSurveyDate;

  Future<void> clearDataInfoApp() async {
      _initialSurveyDate = DateTime.now();
      _autoValidate = false;
      _initialRecomendation = "";
      _finalRecomendation = "";
      _brmsScoring = 0;
      _controllerOrderDate.clear();
      _controllerSurveyAppointmentDate.clear();
      _isSignedPK = false;
      _conceptTypeModelSelected = null;
      _controllerTotalObject.clear();
      _selectedTime = TimeOfDay.now();
      _controllerSurveyAppointmentTime.clear();
      _proportionalTypeOfInsuranceModelSelected = null;
      _isOrderDateChanges = false;
      _isSurveyAppointmentDateChanges = false;
      _isIsSignedPKChanges = false;
      _isConceptTypeModelChanges = false;
      _isTotalObjectChanges = false;
      _isPropotionalObjectChanges = false;
      _isNumberOfUnitChanges = false;
      isDisablePACIAAOSCONA = false;
  }

  bool _isProporsionalEnable = true;

  bool get isProporsionalEnable => _isProporsionalEnable;

  set isProporsionalEnable(bool value) {
    this._isProporsionalEnable = value;
  }

  String get custType => _custType;

  set custType(String value) {
    this._custType = value;
    notifyListeners();
  }

  String get lastKnownState => _lastKnownState;

  set lastKnownState(String value) {
    this._lastKnownState = value;
    notifyListeners();
  }

  Future<void> setPreference() async {
    SharedPreferences _preference = await SharedPreferences.getInstance();
    this._custType = _preference.getString("cust_type");
    this._lastKnownState = _preference.getString("last_known_state");
  }

  Future<void> disablePACIA() async {
    if(this._lastKnownState == "AOS" || this._lastKnownState == "CONA" || this._lastKnownState == "IA" || this._lastKnownState == "PAC") {
      this._isDisablePACIAAOSCONA = true;
    }
  }

  Future<void> setDefaultValue(BuildContext context)async{
    print("cek time $_selectedTime");
    await getDataFromDashboard(context);
    SharedPreferences _preference = await SharedPreferences.getInstance();
    await setPreference();
    await disablePACIA();
    // this._controllerOrderDate.text = _formatter.format(_initialDateOrder);
    var _tanggalAplikasi = _preference.getString("order_date");
    this._initialDateOrder = DateTime.parse(_tanggalAplikasi);
    this._controllerOrderDate.text = _formatter.format(DateTime.parse(_tanggalAplikasi));
    if(this._controllerTotalObject.text.isEmpty){
      this._controllerTotalObject.text = "1";
    }
    this._listNumberUnit.clear();
    this._numberOfUnitSelected = null;
    this._proportionalTypeOfInsuranceModelSelected = this._listProportionalTypeOfInsurance[1];
    if (this._controllerTotalObject.text != "") {
      int _lengthList = int.parse(this._controllerTotalObject.text);
      for (int i = 0; i < _lengthList; i++) {
        this._listNumberUnit.add("${i + 1}");
      }
      this._numberOfUnitSelected = _listNumberUnit[0];
    }
    if(_custType == "COM"){
      this._conceptTypeModelSelected = this._listConceptType[2];
      if(this._conceptTypeModelSelected != null){
        if(this._conceptTypeModelSelected.id == "03"){
          isProporsionalEnable = false;
        }
        else{
          isProporsionalEnable = true;
        }
      }
    }
    else{
      if(Provider.of<FormMFotoChangeNotifier>(context,listen: false).jenisKonsepSelected.id == "03"){
        isProporsionalEnable = false;
      }
      else{
        isProporsionalEnable = true;
      }
    }
  }

  // di ganti string
  Future<void> saveToSQLite(BuildContext context) async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context,listen: false);
    var _providerMarketingNotes = Provider.of<MarketingNotesChangeNotifier>(context,listen: false);
    var _surveyAppointmentDateTime = "";
    if(_controllerSurveyAppointmentDate.text != "" && _controllerSurveyAppointmentTime.text != ""){
      debugPrint("cek save survey ${"${dateFormatSurveyApp.format(_initialSurveyDate)}T${_controllerSurveyAppointmentTime.text}"}");
      _surveyAppointmentDateTime = "${dateFormatSurveyApp.format(_initialSurveyDate)}T${_controllerSurveyAppointmentTime.text}:00";
      debugPrint("CEK_SURVEY_DATE ${dateFormatSurveyApp.format(_initialSurveyDate)}T${_controllerSurveyAppointmentTime.text}:00");
      _initialSurveyDate = DateTime.parse(_surveyAppointmentDateTime);
      debugPrint("CEK_SURVEY_DATE $_initialSurveyDate");
    }
    // _dbHelper.insertMS2Application(MS2ApplicationModel(
    //   null,
    //   null,
    //   null,
    //   null,
    //   this._controllerOrderDate.text != "" ? this._controllerOrderDate.text : "",
    //   this._initialDateOrder.toString(),
    //   _surveyAppointmentDateTime,
    //   null,
    //   null,
    //   null,
    //   null,
    //   null,
    //   null,
    //   null,
    //   null,
    //   null,
    //   null,
    //   null,
    //   _isSignedPK ? 0 : 1,
    //   null,
    //   null,
    //   this._custType == "COM" ? this.conceptTypeModelSelected.id : _providerFoto.jenisKonsepSelected.id,
    //   this._custType == "COM" ? this.conceptTypeModelSelected.text : _providerFoto.jenisKonsepSelected.text,
    //   this._controllerTotalObject.text != "" ? int.parse(this._controllerTotalObject.text) : 0,
    //   this.proportionalTypeOfInsuranceModelSelected != null ? this.proportionalTypeOfInsuranceModelSelected.kode : "",
    //   this.proportionalTypeOfInsuranceModelSelected != null ? this.proportionalTypeOfInsuranceModelSelected.description : "",
    //   int.parse(this._numberOfUnitSelected),
    //   null,
    //   null,
    //   null,
    //   null,
    //   null,
    //   null,
    //   null,
    //   null,
    //   null,
    //   _preferences.getString("username"),
    //   null,
    //   null,
    //   null,
    //   null,
    //   null,
    //   null,
    //   null,
    //   null,
    //   null,
    //   null,
    //   null,
    //   null,
    //   null,
    //   null,
    //   null,
    //   null,
    //   null,
    //   this._isOrderDateChanges ? "1" : "0",
    //   this._isSurveyAppointmentDateChanges ? "1" : "0",
    //   this._isIsSignedPKChanges ? "1" : "0",
    //   this._isConceptTypeModelChanges ? "1" : "0",
    //   this._isTotalObjectChanges ? "1" : "0",
    //   this._isPropotionalObjectChanges ? "1" : "0",
    //   this._isNumberOfUnitChanges ? "1" : "0",
    // ));
    var _model = MS2ApplicationModel(
      null,
      applNo,
      null,
      null,
      null,
      this._initialDateOrder.toString(),//this._controllerOrderDate.text != "" ? this._controllerOrderDate.text : "",
      null,
      _surveyAppointmentDateTime,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      _initialRecomendation,
      _finalRecomendation,
      _brmsScoring,
      null,
      _isSignedPK ? 1 : 0,
      null,
      null,
      this._custType == "COM" ? this.conceptTypeModelSelected.id : _providerFoto.jenisKonsepSelected.id,
      this._custType == "COM" ? this.conceptTypeModelSelected.text : _providerFoto.jenisKonsepSelected.text,
      this._controllerTotalObject.text != "" ? int.parse(this._controllerTotalObject.text) : 0,
      this.proportionalTypeOfInsuranceModelSelected != null ? this.proportionalTypeOfInsuranceModelSelected.kode : "",
      this.proportionalTypeOfInsuranceModelSelected != null ? this.proportionalTypeOfInsuranceModelSelected.description : "",
      int.parse(this._numberOfUnitSelected),
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      _providerMarketingNotes.controllerMarketingNotes.text, //marketing notes
      null,
      _preferences.getString("username"),
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      this._isOrderDateChanges ? "1" : "0",
      this._isSurveyAppointmentDateChanges ? "1" : "0",
      this._isIsSignedPKChanges ? "1" : "0",
      this._isConceptTypeModelChanges ? "1" : "0",
      this._isTotalObjectChanges ? "1" : "0",
      this._isPropotionalObjectChanges ? "1" : "0",
      this._isNumberOfUnitChanges ? "1" : "0",
      null //marketing notes
    );
    _dbHelper.insertMS2Application(_model);
    // String _message = await _submitDataPartial.submitApplication(5,_model);
    // return _message;
  }

  Future<bool> deleteSQLite() async{
    return await _dbHelper.deleteMS2Application();
  }

  String get applNo => _applNo;

  set applNo(String value) {
    this._applNo = value;
  }

  Future<void> _setDataFromSQLite(BuildContext context, int index) async{
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _check = await _dbHelper.selectMS2Application();
    if(_check.isNotEmpty){
      if(_custType == "COM"){
        for(int i=0; i<listConceptType.length;i++){
          if(_check[0]['konsep_type'] == _listConceptType[i].id){
            _conceptTypeModelSelected = _listConceptType[i];
          }
        }
      }

      // print("cek order date${_check[0]['order_date']}");
      // print("cek order date${_check[0]['svy_appointment_date']}");
      // int _yearOrder = int.parse(_check[0]['order_date'].toString().split("-")[2]);
      // int _monthOrder = int.parse(_check[0]['order_date'].toString().split("-")[1]);
      // int _dayOrder = int.parse(_check[0]['order_date'].toString().split("-")[0]);
      _initialDateOrder = DateTime.parse(_check[0]['order_date']);//DateTime(_yearOrder,_monthOrder,_dayOrder);
      this._controllerOrderDate.text = dateFormat.format(DateTime.parse(_check[0]['order_date'].toString()));
      List _splitTime = _check[0]['svy_appointment_date'].toString().replaceAll("T", " ").split(" ");
      print("cek datetime app ${_splitTime.isNotEmpty} ${_check[0]['svy_appointment_date']}");
      if(_splitTime.isNotEmpty && _check[0]['svy_appointment_date'] != ""){
        int _hour = int.parse(_splitTime[1].toString().split(":")[0]);
        int _minute = int.parse(_splitTime[1].toString().split(":")[1]);
        _selectedTime = TimeOfDay(hour: _hour, minute: _minute);

        // int _year = int.parse(_splitTime[0].toString().split("-")[2]);
        // int _month = int.parse(_splitTime[0].toString().split("-")[1]);
        // int _day = int.parse(_splitTime[0].toString().split("-")[0]);
        _initialSurveyDate = DateTime.parse(_splitTime[0]);
        this._controllerSurveyAppointmentDate.text = dateFormat.format(DateTime.parse(_splitTime[0]));
        List _splitFormatTime = _splitTime[1].toString().split(":");
        this._controllerSurveyAppointmentTime.text = "${_splitFormatTime[0]}:${_splitFormatTime[1]}";//_splitTime[1];
      }
      print("check sign pk: ${_check[0]['sign_pk']} ${_check[0]['sign_pk'] == 1}");
      if(_preferences.getString("last_known_state") == "IA") {
        isSignedPK = true;
      } else {
        isSignedPK = _check[0]['sign_pk'] == 1;
      }
      print("cek sign pk $isSignedPK");
      // _check[0]['sign_pk'] == "0" ? this.isSignedPK = true : this.isSignedPK = false;
      this._controllerTotalObject.text = _check[0]['objt_qty'].toString() != "null" && _check[0]['objt_qty'].toString() != "" ? _check[0]['objt_qty'].toString() : "1";
      for(int i=0; i<_listProportionalTypeOfInsurance.length; i++){
        if(_check[0]['prop_insr_type'] == _listProportionalTypeOfInsurance[i].kode){
          _proportionalTypeOfInsuranceModelSelected = _listProportionalTypeOfInsurance[i];
        }
      }

      applNo = _check[0]['applNo'];
      _initialRecomendation = _check[0]['initial_recomendation'];
      _finalRecomendation = _check[0]['final_recomendation'];
      _brmsScoring = _check[0]['brms_scoring'] != "null" && _check[0]['brms_scoring'] != "" ? _check[0]['brms_scoring'] : 0;

      if (this._controllerTotalObject.text == "0") {
        this._controllerTotalObject.clear();
      }
      this._listNumberUnit.clear();
      this._numberOfUnitSelected = null;
      if (this._controllerTotalObject.text != "") {
        int _lengthList = int.parse(this._controllerTotalObject.text);
        for (int i = 0; i < _lengthList; i++) {
          this._listNumberUnit.add("${i + 1}");
        }
        this._numberOfUnitSelected = _listNumberUnit[0];
      }
      for(int i=0; i<_listNumberUnit.length; i++){
        if(_check[0]['unit'].toString() == _listNumberUnit[i]){
          this._numberOfUnitSelected = _listNumberUnit[i];
        }
      }
      await checkDataDakor();
    }

    if(_lastKnownState == "IDE" && index != null){
      MenuObjectInformation _menuObjectInfo = MenuObjectInformation();
      if(index != 5){
        if(_custType == "PER"){
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).isInfoAppDone = true;
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).selectedIndex +=1;
        }
        else {
          Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).isInfoAppDone = true;
          Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).selectedIndex +=1;
        }
        await _menuObjectInfo.setNextState(context, index);
      }
      else{
        if(_custType == "PER"){
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).selectedIndex = 5;
        } else {
          Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).selectedIndex = 5;
        }
      }
    }
    notifyListeners();
  }

  //ga kepake
  Future<void> getInfoAppShowHide() async{
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);
    String urlPublic = await storage.read(key: "urlPublic");
    final _response = await _http.get(
        "${urlPublic}api-form-web/get_mide_individuhs",
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      final _data = _result['hideshow'];

      // this._isGCVisible  = _data[0]['gc'] == "1";
      // this._isIdentitasModelVisible = _data[0]['jenisidentitaspribadi'] == "1";
      // this._isNoIdentitasVisible = _data[0]['noidentitaspribadi'] == "1";
      // this._isNamaLengkapSesuaiIdentitasVisible = _data[0]['namalengkapidpribadi'] == "1";
      // this._isNamaLengkapVisible = _data[0]['namalengkappribadi'] == "1";
      // this._isTglLahirVisible = _data[0]['tanggallahirpribadi'] == "1";
      // this._isTempatLahirSesuaiIdentitasVisible = _data[0]['tempatlahiridpribadi'] == "1";
      // this._isTempatLahirSesuaiIdentitasLOVVisible = _data[0]['tempatlahiridlovpribadi'] == "1";
      // this._isGenderVisible = _data[0]['jeniskelaminpribadi'] == "1";
      // this._isEducationSelectedVisible = _data[0]['pendidikan'] == "1";
      // this._isMaritalStatusSelectedVisible = _data[0]['statuspernikahan'] == "1";
      // this._isNoHpVisible = _data[0]['handphone1'] == "1";
      // this._isNoHp1WAVisible = _data[0]['nowa1'] == "1";
      // this._isHaveNPWPVisible = _data[0]['punyanpwp'] == "1";
      // this._isAlamatSesuaiNPWPVisible = _data[0]['alamatnpwp'] == "1";
    }
    else{
      throw Exception("Failed get data error ${_response.statusCode}");
    }
  }

  ShowMandatoryInfoAppModel _showMandatoryInfoAppIdeModel;
  ShowMandatoryInfoAppModel _showMandatoryInfoAppIdeCompanyModel;
  void showMandatoryIdeModel(BuildContext context){
    _showMandatoryInfoAppIdeModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAppIdeModel;
    _showMandatoryInfoAppIdeCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAppIdeCompanyModel;
  }

  ShowMandatoryInfoAppModel _showMandatoryInfoAppRegulerSurveyModel;
  ShowMandatoryInfoAppModel _showMandatoryInfoAppRegulerSurveyCompanyModel;
  void showMandatoryRegulerSurveyModel(BuildContext context){
    _showMandatoryInfoAppRegulerSurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAppRegulerSurveyModel;
    _showMandatoryInfoAppRegulerSurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAppRegulerSurveyCompanyModel;
  }

  ShowMandatoryInfoAppModel _showMandatoryInfoAppPacModel;
  ShowMandatoryInfoAppModel _showMandatoryInfoAppPacCompanyModel;
  void showMandatoryPacModel(BuildContext context){
    _showMandatoryInfoAppPacModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAppPacModel;
    _showMandatoryInfoAppPacCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAppPacCompanyModel;
  }

  ShowMandatoryInfoAppModel _showMandatoryInfoAppResurveyModel;
  ShowMandatoryInfoAppModel _showMandatoryInfoAppResurveyCompanyModel;
  void showMandatoryResurveyModel(BuildContext context){
    _showMandatoryInfoAppResurveyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAppResurveyModel;
    _showMandatoryInfoAppResurveyCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAppResurveyCompanyModel;
  }

  ShowMandatoryInfoAppModel _showMandatoryInfoAppDakorModel;
  ShowMandatoryInfoAppModel _showMandatoryInfoAppDakorCompanyModel;
  void showMandatoryDakorModel(BuildContext context){
    _showMandatoryInfoAppDakorModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAppDakorModel;
    _showMandatoryInfoAppDakorCompanyModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryInfoAppDakorCompanyModel;
  }

  bool _loadData = false;

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  Future<void> getDataFromDashboard(BuildContext context) async{
    showMandatoryIdeModel(context);
    showMandatoryRegulerSurveyModel(context);
    showMandatoryPacModel(context);
    showMandatoryResurveyModel(context);
    showMandatoryDakorModel(context);
  }

  bool isOrderDateVisibleIde() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
       value = _showMandatoryInfoAppIdeModel.isOrderDateVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoAppRegulerSurveyModel.isOrderDateVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoAppPacModel.isOrderDateVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoAppResurveyModel.isOrderDateVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoAppDakorModel.isOrderDateVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoAppIdeCompanyModel.isOrderDateVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoAppRegulerSurveyCompanyModel.isOrderDateVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoAppPacCompanyModel.isOrderDateVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoAppResurveyCompanyModel.isOrderDateVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoAppDakorCompanyModel.isOrderDateVisible;
      }
    }
    return value;
  }
  bool isSurveyAppointmentDateVisibleIde() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoAppIdeModel.isSurveyAppointmentDateVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoAppRegulerSurveyModel.isSurveyAppointmentDateVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoAppPacModel.isSurveyAppointmentDateVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoAppResurveyModel.isSurveyAppointmentDateVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoAppDakorModel.isSurveyAppointmentDateVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoAppIdeCompanyModel.isSurveyAppointmentDateVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoAppRegulerSurveyCompanyModel.isSurveyAppointmentDateVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoAppPacCompanyModel.isSurveyAppointmentDateVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoAppResurveyCompanyModel.isSurveyAppointmentDateVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoAppDakorCompanyModel.isSurveyAppointmentDateVisible;
      }
    }
    return value;
  }
  bool isIsSignedPKVisibleIde() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoAppIdeModel.isIsSignedPKVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoAppRegulerSurveyModel.isIsSignedPKVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoAppPacModel.isIsSignedPKVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoAppResurveyModel.isIsSignedPKVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoAppDakorModel.isIsSignedPKVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoAppIdeCompanyModel.isIsSignedPKVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoAppRegulerSurveyCompanyModel.isIsSignedPKVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoAppPacCompanyModel.isIsSignedPKVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoAppResurveyCompanyModel.isIsSignedPKVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoAppDakorCompanyModel.isIsSignedPKVisible;
      }
    }
    return value;
  }
  bool isConceptTypeModelVisibleIde() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoAppIdeModel.isConceptTypeModelVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoAppRegulerSurveyModel.isConceptTypeModelVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoAppPacModel.isConceptTypeModelVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoAppResurveyModel.isConceptTypeModelVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoAppDakorModel.isConceptTypeModelVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoAppIdeCompanyModel.isConceptTypeModelVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoAppRegulerSurveyCompanyModel.isConceptTypeModelVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoAppPacCompanyModel.isConceptTypeModelVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoAppResurveyCompanyModel.isConceptTypeModelVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoAppDakorCompanyModel.isConceptTypeModelVisible;
      }
    }
    return value;
  }
  bool isTotalObjectVisibleIde() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoAppIdeModel.isTotalObjectVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoAppRegulerSurveyModel.isTotalObjectVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoAppPacModel.isTotalObjectVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoAppResurveyModel.isTotalObjectVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoAppDakorModel.isTotalObjectVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoAppIdeCompanyModel.isTotalObjectVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoAppRegulerSurveyCompanyModel.isTotalObjectVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoAppPacCompanyModel.isTotalObjectVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoAppResurveyCompanyModel.isTotalObjectVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoAppDakorCompanyModel.isTotalObjectVisible;
      }
    }
    return value;
  }
  bool isPropotionalObjectVisibleIde() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoAppIdeModel.isPropotionalObjectVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoAppRegulerSurveyModel.isPropotionalObjectVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoAppPacModel.isPropotionalObjectVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoAppResurveyModel.isPropotionalObjectVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoAppDakorModel.isPropotionalObjectVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoAppIdeCompanyModel.isPropotionalObjectVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoAppRegulerSurveyCompanyModel.isPropotionalObjectVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoAppPacCompanyModel.isPropotionalObjectVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoAppResurveyCompanyModel.isPropotionalObjectVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoAppDakorCompanyModel.isPropotionalObjectVisible;
      }
    }
    return value;
  }
  bool isNumberOfUnitVisibleIde() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoAppIdeModel.isNumberOfUnitVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoAppRegulerSurveyModel.isNumberOfUnitVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoAppPacModel.isNumberOfUnitVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoAppResurveyModel.isNumberOfUnitVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoAppDakorModel.isNumberOfUnitVisible;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoAppIdeCompanyModel.isNumberOfUnitVisible;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoAppRegulerSurveyCompanyModel.isNumberOfUnitVisible;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoAppPacCompanyModel.isNumberOfUnitVisible;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoAppResurveyCompanyModel.isNumberOfUnitVisible;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoAppDakorCompanyModel.isNumberOfUnitVisible;
      }
    }
    return value;
  }

  bool isOrderDateMandatoryIde() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoAppIdeModel.isOrderDateMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoAppRegulerSurveyModel.isOrderDateMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoAppPacModel.isOrderDateMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoAppResurveyModel.isOrderDateMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoAppDakorModel.isOrderDateMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoAppIdeCompanyModel.isOrderDateMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoAppRegulerSurveyCompanyModel.isOrderDateMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoAppPacCompanyModel.isOrderDateMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoAppResurveyCompanyModel.isOrderDateMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoAppDakorCompanyModel.isOrderDateMandatory;
      }
    }
    return value;
  }
  bool isSurveyAppointmentDateMandatoryIde() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoAppIdeModel.isSurveyAppointmentDateMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoAppRegulerSurveyModel.isSurveyAppointmentDateMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoAppPacModel.isSurveyAppointmentDateMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoAppResurveyModel.isSurveyAppointmentDateMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoAppDakorModel.isSurveyAppointmentDateMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoAppIdeCompanyModel.isSurveyAppointmentDateMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoAppRegulerSurveyCompanyModel.isSurveyAppointmentDateMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoAppPacCompanyModel.isSurveyAppointmentDateMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoAppResurveyCompanyModel.isSurveyAppointmentDateMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoAppDakorCompanyModel.isSurveyAppointmentDateMandatory;
      }
    }
    return value;
  }
  bool isIsSignedPKMandatoryIde() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoAppIdeModel.isIsSignedPKMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoAppRegulerSurveyModel.isIsSignedPKMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoAppPacModel.isIsSignedPKMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoAppResurveyModel.isIsSignedPKMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoAppDakorModel.isIsSignedPKMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoAppIdeCompanyModel.isIsSignedPKMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoAppRegulerSurveyCompanyModel.isIsSignedPKMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoAppPacCompanyModel.isIsSignedPKMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoAppResurveyCompanyModel.isIsSignedPKMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoAppDakorCompanyModel.isIsSignedPKMandatory;
      }
    }
    return value;
  }
  bool isConceptTypeModelMandatoryIde() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoAppIdeModel.isConceptTypeModelMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoAppRegulerSurveyModel.isConceptTypeModelMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoAppPacModel.isConceptTypeModelMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoAppResurveyModel.isConceptTypeModelMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoAppDakorModel.isConceptTypeModelMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoAppIdeCompanyModel.isConceptTypeModelMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoAppRegulerSurveyCompanyModel.isConceptTypeModelMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoAppPacCompanyModel.isConceptTypeModelMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoAppResurveyCompanyModel.isConceptTypeModelMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoAppDakorCompanyModel.isConceptTypeModelMandatory;
      }
    }
    return value;
  }
  bool isTotalObjectMandatoryIde() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoAppIdeModel.isTotalObjectMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoAppRegulerSurveyModel.isTotalObjectMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoAppPacModel.isTotalObjectMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoAppResurveyModel.isTotalObjectMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoAppDakorModel.isTotalObjectMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoAppIdeCompanyModel.isTotalObjectMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoAppRegulerSurveyCompanyModel.isTotalObjectMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoAppPacCompanyModel.isTotalObjectMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoAppResurveyCompanyModel.isTotalObjectMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoAppDakorCompanyModel.isTotalObjectMandatory;
      }
    }
    return value;
  }
  bool isPropotionalObjectMandatoryIde() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoAppIdeModel.isPropotionalObjectMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoAppRegulerSurveyModel.isPropotionalObjectMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoAppPacModel.isPropotionalObjectMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoAppResurveyModel.isPropotionalObjectMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoAppDakorModel.isPropotionalObjectMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoAppIdeCompanyModel.isPropotionalObjectMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoAppRegulerSurveyCompanyModel.isPropotionalObjectMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoAppPacCompanyModel.isPropotionalObjectMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoAppResurveyCompanyModel.isPropotionalObjectMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoAppDakorCompanyModel.isPropotionalObjectMandatory;
      }
    }
    return value;
  }
  bool isNumberOfUnitMandatoryIde() {
    bool value;
    if(_custType == "PER") {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoAppIdeModel.isNumberOfUnitMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoAppRegulerSurveyModel.isNumberOfUnitMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoAppPacModel.isNumberOfUnitMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoAppResurveyModel.isNumberOfUnitMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoAppDakorModel.isNumberOfUnitMandatory;
      }
    } else {
      if(_lastKnownState == "IDE") {
        value = _showMandatoryInfoAppIdeCompanyModel.isNumberOfUnitMandatory;
      } else if (_lastKnownState == "SRE") {
        value = _showMandatoryInfoAppRegulerSurveyCompanyModel.isNumberOfUnitMandatory;
      } else if (_lastKnownState == "AOS" || _lastKnownState == "CONA" || _lastKnownState == "IA" || _lastKnownState == "PAC") {
        value = _showMandatoryInfoAppPacCompanyModel.isNumberOfUnitMandatory;
      } else if (_lastKnownState == "RSVY") {
        value = _showMandatoryInfoAppResurveyCompanyModel.isNumberOfUnitMandatory;
      } else if (_lastKnownState == "DKR") {
        value = _showMandatoryInfoAppDakorCompanyModel.isNumberOfUnitMandatory;
      }
    }
    return value;
  }

  Future<void> checkDataDakor() async{
    var _check = await _dbHelper.selectMS2Application();
    if(_check.isNotEmpty && _lastKnownState == "DKR"){
      if(_custType == "COM"){
        isConceptTypeModelChanges = _conceptTypeModelSelected.id != _check[0]['konsep_type'] || _check[0]['edit_konsep_type'] == "1";
      }
      isOrderDateChanges = this._initialDateOrder != DateTime.parse(_check[0]['order_date']) || _check[0]['edit_order_date'] == "1";
      isSurveyAppointmentDateChanges = "${dateFormatSurveyApp.format(this._initialSurveyDate)} ${this._controllerSurveyAppointmentTime.text}:00" != _check[0]['svy_appointment_date'] || _check[0]['edit_svy_appointment_date'] == "1";
      isIsSignedPKChanges = this._isSignedPK != (_check[0]['sign_pk'] == 1) || _check[0]['edit_sign_pk'] == "1";
      isTotalObjectChanges = this._controllerTotalObject.text != _check[0]['objt_qty'].toString() || _check[0]['edit_objt_qty'] == "1";
      isPropotionalObjectChanges = _proportionalTypeOfInsuranceModelSelected.kode != _check[0]['prop_insr_type'] || _check[0]['edit_prop_insr_type'] == "1";
      isNumberOfUnitChanges = this._numberOfUnitSelected != _check[0]['unit'].toString() || _check[0]['edit_unit'] == "1";
    }
  }

  bool get isOrderDateChanges => _isOrderDateChanges;

  set isOrderDateChanges(bool value) {
    this._isOrderDateChanges = value;
    notifyListeners();
  }

  bool get isSurveyAppointmentDateChanges => _isSurveyAppointmentDateChanges;

  set isSurveyAppointmentDateChanges(bool value) {
    this._isSurveyAppointmentDateChanges = value;
    notifyListeners();
  }

  bool get isIsSignedPKChanges => _isIsSignedPKChanges;

  set isIsSignedPKChanges(bool value) {
    this._isIsSignedPKChanges = value;
    notifyListeners();
  }

  bool get isConceptTypeModelChanges => _isConceptTypeModelChanges;

  set isConceptTypeModelChanges(bool value) {
    this._isConceptTypeModelChanges = value;
    notifyListeners();
  }

  bool get isTotalObjectChanges => _isTotalObjectChanges;

  set isTotalObjectChanges(bool value) {
    this._isTotalObjectChanges = value;
    notifyListeners();
  }

  bool get isPropotionalObjectChanges => _isPropotionalObjectChanges;

  set isPropotionalObjectChanges(bool value) {
    this._isPropotionalObjectChanges = value;
    notifyListeners();
  }

  bool get isNumberOfUnitChanges => _isNumberOfUnitChanges;

  set isNumberOfUnitChanges(bool value) {
    this._isNumberOfUnitChanges = value;
    notifyListeners();
  }
}
