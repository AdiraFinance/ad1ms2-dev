import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/check_limit_model.dart';
import 'package:ad1ms2_dev/models/check_mpl_activation_model.dart';
import 'package:ad1ms2_dev/models/inq_voucher_model.dart';
import 'package:ad1ms2_dev/models/list_disbursement_model.dart';
import 'package:ad1ms2_dev/models/list_oid_model.dart';
import 'package:ad1ms2_dev/models/list_voucher_model.dart';
import 'package:ad1ms2_dev/models/ms2_credit_limit_model.dart';
import 'package:ad1ms2_dev/models/ms2_lme_detail_model.dart';
import 'package:ad1ms2_dev/models/ms2_lme_model.dart';
import 'package:ad1ms2_dev/models/product_lme_model.dart';
import 'package:ad1ms2_dev/models/reference_number_cl_model.dart';
import 'package:ad1ms2_dev/models/type_offer_model.dart';
import 'package:ad1ms2_dev/screens/dashboard.dart';
import 'package:ad1ms2_dev/screens/form_m/search_reference_number_cl.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:ad1ms2_dev/shared/resource/submit_data_partial.dart';
import 'package:ad1ms2_dev/shared/search_reference_number_cl_change_notifier.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';
import 'constants.dart';
import 'form_m_individu_change_notifier/form_m_parent_individu_change_notifier.dart';

class FormMCreditLimitChangeNotifier with ChangeNotifier {
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final storage = FlutterSecureStorage();
  bool _autoValidate = false;
  bool _flag = false;
  bool _loadData = false;
  bool _processSubmit = false;
  int _radioProceedProcess = 0;
  TextEditingController _controllerConsumerName = TextEditingController();
  TextEditingController _controllerTelephoneNumber = TextEditingController();
  TextEditingController _controllerConsumerAddress = TextEditingController();
  TextEditingController _controllerGrading = TextEditingController();
  TextEditingController _controllerOfferType = TextEditingController();
  TextEditingController _controllerReferenceNumberCL = TextEditingController();
  TextEditingController _controllerCustomerType = TextEditingController();
  TextEditingController _controllerNotes = TextEditingController();

  TextEditingController _controllerMaxPH = TextEditingController();
  TextEditingController _controllerMaxInstallment = TextEditingController();
  // TextEditingController _controllerBiddingType = TextEditingController();
  TextEditingController _controllerPortofolio = TextEditingController();
  TextEditingController _controllerTenor = TextEditingController();
  // TextEditingController _controllerReference = TextEditingController();
  TextEditingController _controllerMultidisburseOption = TextEditingController();
  TextEditingController _controllerDisbursementNo = TextEditingController();
  List<TypeOfferModel> _listTypeOffer = ListTypeOffer().listTypeOffer;
  CheckLimitModel _checkLimitSelected;
  TypeOfferModel _typeOfferSelected;
  List<String> _referenceNumberList = [];
  ReferenceNumberCLModel _referenceNumberCLSelected;
  ListVoucher _listVoucherCLSelected;
  ListDisbursement _listDisbursementCLSelected;
  CheckMplActivationModel _checkMplActivationCLSelected;
  InqVoucherModel _inqVoucherSelected;

  DbHelper _dbHelper = DbHelper();
  String _lmeId = "";
  List<ProductLMEModel> _listProductLME = ProductLMEList().productLMEList;
  SubmitDataPartial _submitDataPartial = SubmitDataPartial();
  String _oid = "";
  String _flagEligible = "";
  String _disburseType = "1"; // awalnya int
  String _productId = "";
  String _productIdDesc = "";
  String _installmentAmount = "";
  String _phAmount = "";
  String _voucherCode = "";
  String _disbursementNo = "";
  String _opsiMultidisburse = "";
  String _noReferensi = "";
  String _maxInstallment = "0"; // awalnya double
  String _portofolio = "";
  String _portofolioDesc = "";
  String _remainingTenor = "0"; // awalnya int
  String _activationDate = "";
  String _statusAoro = "0";
  String _contractReference = "";

  // BaseUrl _baseUrl = BaseUrl();

  String get contractReference => _contractReference;

  set contractReference(String value) {
    this._contractReference = value;
    notifyListeners();
  }

  String get lmeId => _lmeId;

  set lmeId(String value) {
    this._lmeId = value;
    notifyListeners();
  }

  String get oid => _oid;

  set oid(String value) {
    this._oid = value;
    notifyListeners();
  }

  String get activationDate => _activationDate;

  set activationDate(String value) {
    this._activationDate = value;
    notifyListeners();
  }

  String get remainingTenor => _remainingTenor;

  set remainingTenor(String value) {
    this._remainingTenor = value;
    notifyListeners();
  }

  String get portofolioDesc => _portofolioDesc;

  set portofolioDesc(String value) {
    this._portofolioDesc = value;
    notifyListeners();
  }

  String get portofolio => _portofolio;

  set portofolio(String value) {
    this._portofolio = value;
    notifyListeners();
  }

  String get maxInstallment => _maxInstallment;

  set maxInstallment(String value) {
    this._maxInstallment = value;
    notifyListeners();
  }

  String get noReferensi => _noReferensi;

  set noReferensi(String value) {
    this._noReferensi = value;
    notifyListeners();
  }

  String get opsiMultidisburse => _opsiMultidisburse;

  set opsiMultidisburse(String value) {
    this._opsiMultidisburse = value;
    notifyListeners();
  }

  String get disbursementNo => _disbursementNo;

  set disbursementNo(String value) {
    this._disbursementNo = value;
    notifyListeners();
  }

  String get voucherCode => _voucherCode;

  set voucherCode(String value) {
    this._voucherCode = value;
    notifyListeners();
  }

  String get phAmount => _phAmount;

  set phAmount(String value) {
    this._phAmount = value;
    notifyListeners();
  }

  String get installmentAmount => _installmentAmount;

  set installmentAmount(String value) {
    this._installmentAmount = value;
    notifyListeners();
  }

  String get productIdDesc => _productIdDesc;

  set productIdDesc(String value) {
    this._productIdDesc = value;
    notifyListeners();
  }

  String get productId => _productId;

  set productId(String value) {
    this._productId = value;
    notifyListeners();
  }

  String get disburseType => _disburseType;

  set disburseType(String value) {
    this._disburseType = value;
    notifyListeners();
  }

  String get flagEligible => _flagEligible;

  set flagEligible(String value) {
    this._flagEligible = value;
    notifyListeners();
  }

  String get statusAoro => _statusAoro;

  set statusAoro(String value) {
    this._statusAoro = value;
    notifyListeners();
  }

  UnmodifiableListView<ProductLMEModel> get listProductLME {
    return UnmodifiableListView(this._listProductLME);
  }

  GlobalKey<FormState> get keyForm => _key;
  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  // Auto Validate
  bool get autoValidate {
    return _autoValidate;
  }

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  // Load Data
  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  // Flag
  bool get flag {
    return _flag;
  }

  set flag(bool value) {
    this._flag = value;
    notifyListeners();
  }

  ListVoucher get listVoucherCLSelected => _listVoucherCLSelected;

  set listVoucherCLSelected(ListVoucher value) {
    this._listVoucherCLSelected = value;
    notifyListeners();
  }

  ListDisbursement get listDisbursementCLSelected =>
      _listDisbursementCLSelected;

  set listDisbursementCLSelected(ListDisbursement value) {
    this._listDisbursementCLSelected = value;
    notifyListeners();
  }

  bool get processSubmit => _processSubmit;

  set processSubmit(bool value) {
    this._processSubmit = value;
    notifyListeners();
  }

  InqVoucherModel get inqVoucherSelected => _inqVoucherSelected;

  set inqVoucherSelected(InqVoucherModel value) {
    this._inqVoucherSelected = value;
    notifyListeners();
  }

  CheckMplActivationModel get checkMplActivationCLSelected => _checkMplActivationCLSelected;

  set checkMplActivationCLSelected(CheckMplActivationModel value) {
    this._checkMplActivationCLSelected = value;
    notifyListeners();
  }

  // Notes
  TextEditingController get controllerNotes => _controllerNotes;

  set controllerNotes(TextEditingController value) {
    this._controllerNotes = value;
    notifyListeners();
  }

  // Jenis Nasabah
  TextEditingController get controllerCustomerType => _controllerCustomerType;

  set controllerCustomerType(TextEditingController value) {
    this._controllerCustomerType = value;
    notifyListeners();
  }

  // No Referensi
  TextEditingController get controllerReferenceNumberCL => _controllerReferenceNumberCL;

  set controllerReferenceNumber(TextEditingController value) {
    this._controllerReferenceNumberCL = value;
    notifyListeners();
  }

  // Jenis Penawaran
  TextEditingController get controllerOfferType => _controllerOfferType;

  set controllerOfferType(TextEditingController value) {
    this._controllerOfferType = value;
    notifyListeners();
  }

  // Grading
  TextEditingController get controllerGrading => _controllerGrading;

  set controllerGrading(TextEditingController value) {
    this._controllerGrading = value;
    notifyListeners();
  }

  // Alamat Konsumen
  TextEditingController get controllerConsumerAddress => _controllerConsumerAddress;

  set controllerConsumerAddress(TextEditingController value) {
    this._controllerConsumerAddress = value;
    notifyListeners();
  }

  // Nomer Telepon
  TextEditingController get controllerTelephoneNumber => _controllerTelephoneNumber;

  set controllerTelephoneNumber(TextEditingController value) {
    this._controllerTelephoneNumber = value;
    notifyListeners();
  }

  // Nama Konsumen
  TextEditingController get controllerConsumerName => _controllerConsumerName;

  set controllerConsumerName(TextEditingController value) {
    this._controllerConsumerName = value;
    notifyListeners();
  }

  // Apakah akan melanjutkan proses penginputan atau tidak?
  int get radioProceedProcess => _radioProceedProcess;

  set radioProceedProcess(int value) {
    this._radioProceedProcess = value;
    notifyListeners();
  }

  // Pencarian ke
  TextEditingController get controllerDisbursementNo => _controllerDisbursementNo;

  set controllerDisbursementNo(TextEditingController value) {
    this._controllerDisbursementNo = value;
    notifyListeners();
  }

  // Opsi Multidisburse
  TextEditingController get controllerMultidisburseOption => _controllerMultidisburseOption;

  set controllerMultidisburseOption(TextEditingController value) {
    this._controllerMultidisburseOption = value;
    notifyListeners();
  }

  // No Reference (Informasi Awal)
  // TextEditingController get controllerReference => _controllerReference;
  //
  // set controllerReference(TextEditingController value) {
  //   this._controllerReference = value;
  //   notifyListeners();
  // }

  // Tenor
  TextEditingController get controllerTenor => _controllerTenor;

  set controllerTenor(TextEditingController value) {
    this._controllerTenor = value;
    notifyListeners();
  }

  // Portofolio
  TextEditingController get controllerPortofolio => _controllerPortofolio;

  set controllerPortofolio(TextEditingController value) {
    this._controllerPortofolio = value;
    notifyListeners();
  }

  // Tipe Penawaran
  // TextEditingController get controllerBiddingType => _controllerBiddingType;
  //
  // set controllerBiddingType(TextEditingController value) {
  //   this._controllerBiddingType = value;
  //   notifyListeners();
  // }

  // Max Installment
  TextEditingController get controllerMaxInstallment => _controllerMaxInstallment;

  set controllerMaxInstallment(TextEditingController value) {
    this._controllerMaxInstallment = value;
    notifyListeners();
  }

  // Max PH
  TextEditingController get controllerMaxPH => _controllerMaxPH;

  set controllerMaxPH(TextEditingController value) {
    this._controllerMaxPH = value;
    notifyListeners();
  }

  TypeOfferModel get typeOfferSelected => _typeOfferSelected;

  set typeOfferSelected(TypeOfferModel value) {
    this._typeOfferSelected = value;
    this._controllerReferenceNumberCL.clear();

    if(value.KODE == "002"){
      this._controllerMaxPH.clear();
      this._controllerMaxInstallment.clear();
      this._controllerTenor.clear();
      this._controllerMultidisburseOption.clear();
      this._controllerDisbursementNo.clear();
    }
    notifyListeners();
  }

  UnmodifiableListView<TypeOfferModel> get listTypeOffer {
    return UnmodifiableListView(this._listTypeOffer);
  }

  // UnmodifiableListView<CheckLimitModel> get listCheckLimit {
  //   return UnmodifiableListView(this._listCheckLimit);
  // }

  CheckLimitModel get checkLimitSelected => _checkLimitSelected;

  set checkLimitSelected(CheckLimitModel value) {
    this._checkLimitSelected = value;
    notifyListeners();
  }

  UnmodifiableListView<String> get referenceNumberList {
    return UnmodifiableListView(this._referenceNumberList);
  }

  ReferenceNumberCLModel get referenceNumberCLSelected => _referenceNumberCLSelected;

  set referenceNumberSelected(ReferenceNumberCLModel value) {
    this._referenceNumberCLSelected = value;
    notifyListeners();
  }

  Future<bool> onBackPress() async{
    // Navigator.pop(context);
    // final _form = _key.currentState;
    // if (_form.validate()) {
    //   flag = true;
    //   autoValidate = false;
    // } else {
    //   flag = false;
    //   autoValidate = true;
    // }
    this._controllerConsumerName.clear();
    this._controllerTelephoneNumber.clear();
    this._controllerConsumerAddress.clear();
    this._controllerCustomerType.clear();
    this._typeOfferSelected = null;
    this._listVoucherCLSelected = null;
    this._controllerReferenceNumberCL.clear();
    this._controllerGrading.clear();
    this._radioProceedProcess = 0;
    this._controllerNotes.clear();
    this._controllerMaxPH.clear();
    this._controllerMaxInstallment.clear();
    this._controllerPortofolio.clear();
    this._controllerTenor.clear();
    this._controllerMultidisburseOption.clear();
    this._controllerDisbursementNo.clear();
    return true;
  }

  void check(BuildContext context, String identityNumber, String fullname, DateTime birthDate, String birthPlace,
      String motherName, String identityAddress, String type) async {
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
      if(this._typeOfferSelected.KODE == "001") {
        checkLimitEligibleStatus(context, identityNumber, fullname, birthDate, birthPlace, motherName, identityAddress, type);
      } else {
        submitJoinOid(context, identityNumber, fullname, birthDate, birthPlace, motherName, identityAddress, type);
      }
    } else {
      flag = false;
      autoValidate = true;
    }
  }

  void submitJoinOid(BuildContext context, String identityNumber, String fullname, DateTime birthDate, String birthPlace,
      String motherName, String identityAddress, String flag) async {
    var _timeStart = DateTime.now();
    debugPrint("submitJoinOid EXEXCUTED");
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _providerListOid = Provider.of<ListOidChangeNotifier>(context, listen: false);
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
    processSubmit = true;

    var _statusAoro;
    if(_providerListOid.statusAoro.toString() == "NEW") {
      _statusAoro = "0";
    } else if(_providerListOid.statusAoro.toString() == "AO") {
      _statusAoro = "1";
    } else if(_providerListOid.statusAoro.toString() == "RO") {
      _statusAoro = "2";
    }

    var _body = jsonEncode({
      "P_ADDRESS": identityAddress != null ? identityAddress : "",
      "P_CP_UNIT_ID ": _preferences.getString("branchid"),
      "P_CREATED_BY ": _preferences.getString("username"),
      "P_CUST_NAME": fullname != null ? fullname : "",
      "P_DATE_BIRTH": formatDateDedup(birthDate) != null ? formatDateDedup(birthDate) : "",
      "P_FLAG_DETAIL": "0",
      "P_FLAG_SOURCE": "013", // diganti dari 006 ke 103 sesuai arahan mas Oji (per tanggal 06.07.2021)
      "P_ID_NO": identityNumber != null ? identityNumber : "",
      "P_MOTHER_NAME": motherName != null ? motherName : "",
      "P_PARA_CUST_TYPE_ID": flag,
      "P_PLACE_BIRTH": birthPlace != null ? birthPlace : "",
      "P_STATUS_AORO": _statusAoro,
      "P_OBLIGOR_ID": _providerListOid.listOIDPersonalSelected.AC_CUST_ID.toString(),
    });
    print("body submit join oid: $_body");
    Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, "Start Submit Join OID", "Start Submit Join OID");
    String _submitNewOID = await storage.read(key: "SubmitNewOID");
    try{
      final _response = await _http.post(
          "${BaseUrl.urlGeneral}$_submitNewOID",
          // "${urlPublic}api/save-draft/insert-dedup-customer",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 10));
      print(_response.statusCode);
      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        print("result submit join oid: $_result");
        final _data = _result['status'];
        if(_data != "1"){
          processSubmit = false;
          showSnackBar("${_result['message']}");
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, _response.body, "Submit Join OID");
        } else{
          // print("berhasil, mulai menjalankan function save lme");
          // showSnackBar("${_result['message']}");
          // _dbHelper.insertDataDedup(DataDedupModel(flag, identityNumber, fullname, birthDate.toString(), birthPlace, motherName, identityAddress, Provider.of<ListOidChangeNotifier>(context, listen: false).statusAoro, DateTime.now().toString(), _preferences.getString('username'), DateTime.now().toString(), _preferences.getString('username')),_result['data']);
          _preferences.setString("order_no", _result['data'].toString()); //dipakai
          // _preferences.setString("status_aoro", _providerListOid.statusAoro.toString());
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, _response.body, "Submit Join OID");
          await saveLME(context, _result['data'].toString());
        }
      } else{
        debugPrint("ERROR JOIN OID  ${_response.body}");
        showSnackBar("Error submit join OID response ${_response.body}");
        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, _response.body, "Submit Join OID");
        processSubmit = false;
      }
    }
    on TimeoutException catch(_){
      showSnackBar("Request Timeout");
      processSubmit = false;
    }
    catch(e){
      showSnackBar("Error $e");
      Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, e.toString(), "Submit Join OID");
      processSubmit = false;
    }
    processSubmit = false;
    notifyListeners();
  }

  Future<void> saveLME(BuildContext context, String orderNo) async {
    print("masuk save lme");
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _timeStart = DateTime.now();
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
    processSubmit = true;

    await setDataToVariable(context);

    var _body = jsonEncode({
      "P_CREATED_BY": _preferences.getString("username"),
      "P_CUST_ADDRESS": this._controllerConsumerAddress.text != "" || this._controllerConsumerAddress.text != "-" ? this._controllerConsumerAddress.text : "", // ngambil dari Field Alamat
      "P_CUST_NAME": this._controllerConsumerName.text != "" || this._controllerConsumerName.text != "-" ? this._controllerConsumerName.text : "", // ngambil dari Field Nama Nasabah
      "P_CUST_TYPE": this._controllerCustomerType.text != "" || this._controllerCustomerType.text != "-" ? this._controllerCustomerType.text : "", // ngambil dari Field Jenis Nasabah
      "P_GRADING": this._controllerGrading.text != "" || this._controllerGrading.text != "-" ? this._controllerGrading.text : "", // ngambil dari Field Grading
      "P_JENIS_PENAWARAN": this._typeOfferSelected != null ? this._typeOfferSelected.KODE : "", // ngambil dari Field Jenis Penawaran
      "P_JENIS_PENAWARAN_DESC": this._typeOfferSelected != null ? this._typeOfferSelected.DESCRIPTION : "", // ngambil dari Field Jenis Penawaran
      "P_MAX_PH": this._controllerMaxPH.text != "" || this._controllerMaxPH.text != "0" ? double.parse(this._controllerMaxPH.text.replaceAll(",", "").toString()) : 0, // ngambil dari Field Max PH
      "P_MAX_TENOR": this._controllerTenor.text != "" || this._controllerTenor.text != "0" ? int.parse(this._controllerTenor.text) : 0, // ngambil dari Field Max Installment | per tanggal 25.06.2021 dari _controllerMaxInstallment diganti ke _controllerTenor
      "P_NOREFF": this._controllerReferenceNumberCL.text != "" || this._controllerReferenceNumberCL.text != "-" ? this._controllerReferenceNumberCL.text : "", // ngambil dari Field Nomor Referensi
      "P_NOTES": this._controllerNotes.text != "" || this._controllerNotes.text != "-" ? this._controllerNotes.text : "", // ngambil dari Field Notes
      "P_OID": Provider.of<ListOidChangeNotifier>(context, listen: false).listOIDPersonalSelected.AC_CUST_ID,
      "P_OPSI_MULTIDISBURSE": _opsiMultidisburse,
      "P_ORDER_NO": orderNo, // _preferences.getString("order_no"),
      "P_PENCAIRAN_KE": this._controllerDisbursementNo.text != "" ? int.parse(this._controllerDisbursementNo.text) : 0, // ngambil dari Field Pencairan Ke
      "P_PORTFOLIO":  this._controllerPortofolio.text != "" ? this._controllerPortofolio.text : "", // ngambil dari Field Portofolio
      "P_TENOR": this._controllerTenor.text != "" || this._controllerTenor.text != "0" ? int.parse(this._controllerTenor.text) : 0, // ngambil dari Field Tenor
      "P_LME_ID": _lmeId,
      "P_APPL_NO": "",
      "P_FLAG_ELIGIBLE": _flagEligible,
      "P_DISBURSE_TYPE": int.parse(_disburseType.toString()),
      "P_PRODUCT_ID": _productId,
      "P_PRODUCT_ID_DESC": _productIdDesc,
      "P_INSTALLMENT_AMOUNT": int.parse(_installmentAmount.toString()),
      "P_PH_AMOUNT": int.parse(_phAmount.toString()),
      "P_VOUCHER_CODE": _voucherCode,
      "P_MAX_INSTALLMENT": double.parse(_maxInstallment.replaceAll(",", "").toString()),
      "P_PLAFOND_MRP_RISK": 0,
      "P_REMAINING_TENOR": int.parse(_remainingTenor.toString()),
      "P_ACTIVATION_DATE": _activationDate.replaceAll("T", " ")
    });
    print("body save lme: $_body");

    String _insertLME = await storage.read(key: "InsertLME");
    Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, "Start Submit LME", "Start Submit LME");
    try{
      final _response = await _http.post(
          "${BaseUrl.urlGeneral}$_insertLME",
          // "${urlPublic}api/save-draft/insert-dedup-customer",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 10));
      print("response status code save lme: ${_response.statusCode}");
      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        print("result save lme: $_result");
        final _data = _result['status'];
        if(_data != "1"){
          showSnackBar("${_result['message']}");
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, _response.body, "Submit LME");
          processSubmit = false;
        } else{
          processSubmit = false;
          showSnackBarSuccess("Sukses save data");
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, _response.body, "Submit LME");
          await saveToSQLiteMS2LME(context, orderNo);
          await autofilledLME(context, orderNo);
          // await saveToSQLiteMS2CreditLimit(context, _data.toString());
          Timer(Duration(milliseconds: 1000), () {
            Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => Dashboard(flag: 2,)), (Route <dynamic> route) => false);
          });
        }
      } else{
        showSnackBar("Error save CL response ${_response.statusCode}");
        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, _response.body, "Submit LME");
        processSubmit = false;
      }
    }
    on TimeoutException catch(_){
      showSnackBar("Request Timeout");
      processSubmit = false;
    }
    catch(e){
      showSnackBar("Error $e");
      Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, e, "Submit LME");
      processSubmit = false;
    }
    processSubmit = false;
    notifyListeners();
  }

  Future<void> setDataToVariable(BuildContext context) async {
    var _providerListOid = Provider.of<ListOidChangeNotifier>(context, listen: false);

    // Activation Date
    if(this._typeOfferSelected != null) {
      if(this._typeOfferSelected.KODE == "001") {
        // VOUCHER
        _activationDate = "";
      } else if(this._typeOfferSelected.KODE == "002") {
        // CREDIT LIMIT
        _activationDate = this._checkMplActivationCLSelected != null ? this._checkMplActivationCLSelected.activation_date.toString() : "";
      } else if(this._typeOfferSelected.KODE == "003") {
        // NON VOUCHER
        _activationDate = "";
      }
    }
    // Disburse Type
    if(this._typeOfferSelected != null) {
      if(this._typeOfferSelected.KODE == "001") {
        // VOUCHER
        _disburseType = "1"; // this._listVoucherCLSelected != null ? this._listVoucherCLSelected.disburse_type.toString() : "1";
      } else if(this._typeOfferSelected.KODE == "002") {
        // CREDIT LIMIT
        _disburseType = "2";
      } else if(this._typeOfferSelected.KODE == "003") {
        // NON VOUCHER
        _disburseType = "1"; // this._checkLimitSelected != null ? this._checkLimitSelected.disburse_type.toString() : "1";
      }
    }
    // Flag Eligible
    if(this._typeOfferSelected != null) {
      if(this._typeOfferSelected.KODE == "001") {
        _flagEligible = this._checkLimitSelected != null ? this._checkLimitSelected.elligible_status.toString() : "";
      } else if(this._typeOfferSelected.KODE == "002") {
        // CREDIT LIMIT
        _flagEligible = this._checkLimitSelected != null ? this._checkLimitSelected.elligible_status.toString() : "";
      } else if(this._typeOfferSelected.KODE == "003") {
        // NON VOUCHER
        _flagEligible = this._checkLimitSelected != null ? this._checkLimitSelected.elligible_status.toString() : "";
      }
    }
    // Installment Amount
    if(this._typeOfferSelected != null) {
      if(this._typeOfferSelected.KODE == "001") {
        // VOUCHER
        _installmentAmount = "0";
      } else if(this._typeOfferSelected.KODE == "002") {
        // CREDIT LIMIT
        _installmentAmount = "0";
      } else if(this._typeOfferSelected.KODE == "003") {
        // NON VOUCHER
        _installmentAmount = "0";
      }
    }
    // Max Installment
    if(this._typeOfferSelected != null) {
      if(this._typeOfferSelected.KODE == "001") {
        // VOUCHER
        _maxInstallment = this._controllerMaxInstallment.text != "" ? this._controllerMaxInstallment.text.replaceAll(",", "").toString() : "0";
      } else if(this._typeOfferSelected.KODE == "002") {
        // CREDIT LIMIT
        _maxInstallment = this._controllerMaxInstallment.text != "" ? this._controllerMaxInstallment.text.replaceAll(",", "").toString() : "0";
      } else if(this._typeOfferSelected.KODE == "003") {
        // NON VOUCHER
        _maxInstallment = this._checkLimitSelected != null ? this._checkLimitSelected.capacity_limit.toString() : "0";
      }
    }
    // Nomor Referensi (TIDAK DIPAKAI)
    // if(this._typeOfferSelected != null) {
    //   if(this._typeOfferSelected.KODE == "001") {
    //     // VOUCHER
    //     _noReferensi = this._listVoucherCLSelected != null ? this._listVoucherCLSelected.voucher_code : "";
    //   } else if(this._typeOfferSelected.KODE == "002") {
    //     // CREDIT LIMIT
    //     _noReferensi = this._checkMplActivationCLSelected != null ? this._checkMplActivationCLSelected.contract_reference : "";
    //   } else if(this._typeOfferSelected.KODE == "003") {
    //     // NON VOUCHER
    //     _noReferensi = "";
    //   }
    // }
    // Opsi Multidisburse
    if(this._typeOfferSelected != null) {
      if(this._typeOfferSelected.KODE == "001") {
        // VOUCHER
        _opsiMultidisburse = "";
      } else if(this._typeOfferSelected.KODE == "002") {
        // CREDIT LIMIT
        _opsiMultidisburse = this._checkMplActivationCLSelected != null ? this._checkMplActivationCLSelected.mpl_type.toString() : "";
      } else if(this._typeOfferSelected.KODE == "003") {
        // NON VOUCHER
        _opsiMultidisburse = "";
      }
    }
    // Pencairan Ke (TIDAK DIPAKAI)
    // if(this._typeOfferSelected != null) {
    //   if(this._typeOfferSelected.KODE == "001") {
    //     // VOUCHER
    //     _disbursementNo = this._controllerDisbursementNo.text != "" ? this._controllerDisbursementNo.text : "";
    //   } else if(this._typeOfferSelected.KODE == "002") {
    //     // CREDIT LIMIT
    //     _disbursementNo = this._checkMplActivationCLSelected != null ? this._checkMplActivationCLSelected.potensial_disbursement_no : "0";
    //   } else if(this._typeOfferSelected.KODE == "003") {
    //     // NON VOUCHER
    //     _disbursementNo = "0";
    //   }
    // }
    // PH Amount
    if(this._typeOfferSelected != null) {
      if(this._typeOfferSelected.KODE == "001") {
        // VOUCHER
        _phAmount = "0";
      } else if(this._typeOfferSelected.KODE == "002") {
        // CREDIT LIMIT
        _phAmount = "0";
      } else if(this._typeOfferSelected.KODE == "003") {
        // NON VOUCHER
        _phAmount = "0";
      }
    }
    // Portofolio (TIDAK DIPAKAI)
    // if(this._typeOfferSelected != null) {
    //   if(this._typeOfferSelected.KODE == "001") {
    //     // VOUCHER
    //     // for(int i=0; i < this._listProductLME.length; i++) {
    //     //   if(this._listProductLME[i].productId == this._listVoucherCLSelected.product_type) {
    //     //     if(this._listProductLME[i].productId == "") {
    //     //       _portofolio = "";
    //     //     } else if(this._listProductLME[i].productId == "001" || this._listProductLME[i].productId == "002") {
    //     //       // _productId = "01";
    //     //       // _productIdDesc = "MOTOR";
    //     //       _portofolio = "01";
    //     //     } else if(this._listProductLME[i].productId == "003" || this._listProductLME[i].productId == "004") {
    //     //       // _productId = "02";
    //     //       // _productIdDesc = "MOBIL";
    //     //       _portofolio = "02";
    //     //     } else if(this._listProductLME[i].productId == "005") {
    //     //       // _productId = "03";
    //     //       // _productIdDesc = "DURABLE";
    //     //       _portofolio = "03";
    //     //     } else {
    //     //       // _productId = "04";
    //     //       // _productIdDesc = "MPL";
    //     //       _portofolio = "04";
    //     //     }
    //     //   }
    //     // }
    //     _portofolio = "";
    //   } else if(this._typeOfferSelected.KODE == "002") {
    //     // CREDIT LIMIT
    //     // _portofolio = "03";
    //     _portofolio = this._controllerPortofolio.text;
    //   } else if(this._typeOfferSelected.KODE == "003") {
    //     // NON VOUCHER
    //     _portofolio = "";
    //   }
    // }
    // Product
    if(this._typeOfferSelected != null) {
      if(this._typeOfferSelected.KODE == "001" && this._listVoucherCLSelected != null) {
        // VOUCHER
        for(int i=0; i < this._listProductLME.length; i++) {
          if(this._listProductLME[i].productId == this._listVoucherCLSelected.product_type) {
            if(this._listProductLME[i].productId == "001" || this._listProductLME[i].productId == "002") {
              _productId = "01";
              _productIdDesc = "MOTOR";
            } else if(this._listProductLME[i].productId == "003" || this._listProductLME[i].productId == "004") {
              _productId = "02";
              _productIdDesc = "MOBIL";
            } else if(this._listProductLME[i].productId == "005") {
              _productId = "03";
              _productIdDesc = "DURABLE";
            } else {
              _productId = "04";
              _productIdDesc = "MPL";
            }
          }
        }
      }
      else if(this._typeOfferSelected.KODE == "002") {
        // CREDIT LIMIT
        _productId = "03";
        _productIdDesc = "DURABLE";
      }
      else if(this._typeOfferSelected.KODE == "003" && this._checkLimitSelected != null) {
        // NON VOUCHER
        for(int i=0; i < this._listProductLME.length; i++) {
          if(this._listProductLME[i].productId == this._checkLimitSelected.product_id) {
            _productId = this._checkLimitSelected.product_id;
            _productIdDesc = this._listProductLME[i].productDesc;
          }
        }
      }
    }
    // Remaining Tenor
    if(this._typeOfferSelected != null) {
      if(this._typeOfferSelected.KODE == "001") {
        // VOUCHER
        _remainingTenor = "0";
      } else if(this._typeOfferSelected.KODE == "002") {
        // CREDIT LIMIT
        _remainingTenor = this._checkMplActivationCLSelected != null ? this._checkMplActivationCLSelected.remaining_tenor.toString() : "0";
      } else if(this._typeOfferSelected.KODE == "003") {
        // NON VOUCHER
        _remainingTenor = "0";
      }
    }
    // Status AORO
    if(_providerListOid.statusAoro.toString() == "NEW") {
      _statusAoro = "0";
    }
    else if(_providerListOid.statusAoro.toString() == "AO") {
      _statusAoro = "1";
    }
    else if(_providerListOid.statusAoro.toString() == "RO") {
      _statusAoro = "2";
    }
    // Voucher Code
    if(this._typeOfferSelected != null) {
      if(this._typeOfferSelected.KODE == "001") {
        // VOUCHER
        _voucherCode = this._listVoucherCLSelected != null ? this._listVoucherCLSelected.voucher_code.toString() : "";
      } else if(this._typeOfferSelected.KODE == "002") {
        // CREDIT LIMIT
        _voucherCode = "";
      } else if(this._typeOfferSelected.KODE == "003") {
        // NON VOUCHER
        _voucherCode = "";
      }
    }
  }

  void searchReferenceNumberCreditLimit(BuildContext context) async {
    this._referenceNumberCLSelected = null;
    this._checkMplActivationCLSelected = null;
    this._controllerReferenceNumberCL.clear();
    if(typeOfferSelected.KODE == "001") {
      // VOUCHER
      ListVoucher data = await Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ChangeNotifierProvider(
                  create: (context) => SearchReferenceNumberCLChangeNotifier(),
                  child: SearchReferenceNumberCL(jenisPenawaran: this._typeOfferSelected.KODE))));
      if (data != null) {
        // await getCheckLimit(context);
        this._listVoucherCLSelected = data;
        this._controllerReferenceNumberCL.text = "${data.voucher_code}";
        this._controllerMaxPH.text = this._checkLimitSelected != null ? _formatCurrency(this._checkLimitSelected.ph_max) : "0";
        this._controllerMaxInstallment.text = this._checkLimitSelected != null ? _formatCurrency(this._checkLimitSelected.capacity_limit) : "0";
        var _productIdDesc;
        for(int i=0; i < this._listProductLME.length; i++) {
          if(this._listProductLME[i].productId == data.product_type) {
            if(this._listProductLME[i].productId == "001" || this._listProductLME[i].productId == "002") {
              _productIdDesc = "MOTOR";
            } else if(this._listProductLME[i].productId == "003" || this._listProductLME[i].productId == "004") {
              _productIdDesc = "MOBIL";
            } else if(this._listProductLME[i].productId == "005") {
              _productIdDesc = "DURABLE";
            } else {
              _productIdDesc = "MPL";
            }
          }
        }
        this._controllerPortofolio.text = _productIdDesc;
        this._controllerTenor.text = "${data.tenor}";
        this._controllerMultidisburseOption.text = "";
        this._controllerDisbursementNo.text = "";
        notifyListeners();
      } else {
        return;
      }
    }
    // TIDAK DIPAKAI
    // else if(typeOfferSelected.KODE == "002") {
    //   // CREDIT LIMIT
    //   ListDisbursement data = await Navigator.push(
    //       context,
    //       MaterialPageRoute(
    //           builder: (context) => ChangeNotifierProvider(
    //               create: (context) => SearchReferenceNumberCLChangeNotifier(),
    //               child: SearchReferenceNumberCL(jenisPenawaran: this._typeOfferSelected.KODE))));
    //   if (data != null) {
    //     this._listDisbursementCLSelected = data;
    //     this._controllerReferenceNumberCL.text = "${this._checkMplActivationCLSelected.contract_reference}";
    //
    //     this._controllerDisbursementNo.text = this._checkMplActivationCLSelected.potensial_disbursement_no.toString();
    //     notifyListeners();
    //   } else {
    //     return;
    //   }
    // }
    // END TIDAK DIPAKAI
  }

  void setValue(BuildContext context, ListOidModel data) async {
    await clearData();
    this._controllerConsumerName.text = data.AC_CUST_NAME;
    this._controllerTelephoneNumber.text = "-";
    this._controllerConsumerAddress.text = data.AC_ADDRESS;
    this._controllerCustomerType.text = Provider.of<ListOidChangeNotifier>(context, listen: false).statusAoro.toString();
    this._controllerGrading.text = "-";
    this._controllerNotes.text = Provider.of<ListOidChangeNotifier>(context, listen: false).statusNasabah.toString();
    this._controllerMaxPH.text = "0";
    this._controllerMaxInstallment.text = "0";
    this._controllerPortofolio.text = "-";
    this._controllerTenor.text = "0";
    this._controllerMultidisburseOption.text = "0";
    this._controllerDisbursementNo.text = "0";
  }

  Future<void> clearData() {
    this._controllerConsumerName.clear();
    this._controllerTelephoneNumber.clear();
    this._controllerConsumerAddress.clear();
    this._controllerCustomerType.clear();
    this._typeOfferSelected = null;
    this._controllerReferenceNumberCL.clear();
    this._controllerGrading.clear();
    this._controllerNotes.clear();
    this._controllerMaxPH.clear();
    this._controllerMaxInstallment.clear();
    this._controllerPortofolio.clear();
    this._controllerTenor.clear();
    this._controllerMultidisburseOption.clear();
    this._controllerDisbursementNo.clear();
  }

  void dialogStatusAORO(BuildContext context) {
    var _providerListOID = Provider.of<ListOidChangeNotifier>(context, listen: false);
    this._controllerCustomerType.text = _providerListOID.statusAoro.toString();
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                primaryColor: Colors.black,
                primarySwatch: primaryOrange,
                accentColor: myPrimaryColor
            ),
            child: AlertDialog(
              title: Text("Informasi Status AORO", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Customer ${_providerListOID.statusAoro.toString()}",),
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Close'),
                ),
                // new FlatButton(
                //   onPressed: () => Navigator.of(context).pop(true),
                //   child: new Text('Tidak'),
                // ),
              ],
            ),
          );
        }
    );
  }

  Future<void> getCheckLimit(BuildContext context) async {
    var _date = dateFormat3.format(DateTime.now());
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
    var _providerOID = Provider.of<ListOidChangeNotifier>(context, listen: false);

    var _body = jsonEncode({
      "oid" : _providerOID.listOIDPersonalSelected.AC_CUST_ID.toString(),
      "lme_id" : "",
      "source_reff_id" : "MS2${_preferences.getString("username")}",
      "source_channel_id" : "revamp-ms2",
      "disburse_type" : this._typeOfferSelected != null ? this._typeOfferSelected.KODE =="002" ? "2" : "1" : "1",
      "product_id" : "",
      "contract_no" : "",
      "installment_amount" : "0",
      "ph_amount" : "",
      "voucher_code" : "",
      "req_date" : _date.toString(),
      "tenor" : "",
      "cust_id" : _providerOID.listOIDPersonalSelected.AC_ID_NO,
      "branch_code" : _preferences.getString("branchid"),
      "appl_no" : "",
      "flag_book" : "",
      "jenis_kegiatan_usaha" : ""
    });

    print("body checklimit: $_body");
    String _creditLimitCHeckLimit = await storage.read(key: "CreditLimitCheckLimit");
    try {
      final _response = await _http.post(
        "${BaseUrl.urlLME}$_creditLimitCHeckLimit",
        // "${getUrlLme}InqService/checkLimit",
        body: _body,
        headers: {
          "Content-Type":"application/json",
          "Authorization":"bearer $token",
        }
      ).timeout(Duration(seconds: 10));
      print("response check limit: ${_response.statusCode}");
      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        if(_result['response_code'] == "00") {
          this._checkLimitSelected = CheckLimitModel(
              _result['reff_id'].toString(),
              _result['oid'].toString(),
              null, //_result['lme_id'],
              _result['source_reff_id'].toString(),
              _result['source_channel_id'].toString(),
              _result['disburse_type'].toString(),
              _result['product_id'].toString(),
              _result['contract_no'].toString(),
              _result['installment_amount'].toString(),
              _result['capacity_limit'].toString(),
              _result['capacity_ph'].toString(),
              _result['voucher_code'].toString(),
              _result['tenor'].toString(),
              null, //_result['cust_id'],
              _result['elligible_status'].toString(),
              _result['grading_nasabah'].toString(),
              null, //_result['branch_code'],
              null, //_result['appl_no'],
              null, //_result['flag_book'],
              null, //_result['jenis_kegiatan_usaha'],
              _result['ph_max'].toString(),
              _result['disbursement_no'].toString(),
              _result['response_code'].toString(),
              _result['response_desc'].toString(),
              _result['response_date'].toString(),
              _result['req_date'].toString()
          );
          this._controllerGrading.text = this._checkLimitSelected.grading_nasabah;
          this._controllerMaxPH.text = this._checkLimitSelected != null ? _formatCurrency(this._checkLimitSelected.ph_max.toString()) : "0";
          this._controllerMaxInstallment.text = this._checkLimitSelected != null ? _formatCurrency(this._checkLimitSelected.capacity_limit.toString()) : "0";
          var _productView = "-";
          for(int i=0; i < this._listProductLME.length; i++) {
            if(this._listProductLME[i].productId == this._checkLimitSelected.product_id) {
              // _productView = "${this._listProductLME[i].productId} - ${this._listProductLME[i].productDesc}";
              _productView = "${this._listProductLME[i].productDesc}";
            }
          }
          this._controllerPortofolio.text = _productView;
          this._controllerTenor.text = this._checkLimitSelected.tenor.toString();
          this._controllerMultidisburseOption.text = "";
          this._controllerDisbursementNo.text = "0";
          loadData = false;
        }
        else {
          showSnackBar("check limit: ${_result['response_desc']}");
          loadData = false;
        }
      }
      else{
        showSnackBar("Error get check limit response ${_response.statusCode}");
        loadData = false;
      }
    }
    on TimeoutException catch(_){
      showSnackBar("Request Timeout");
      loadData = false;
    }
    catch(e) {
      showSnackBar("Error $e");
      this._loadData = false;
    }
  }

  void getMplActivation(BuildContext context) async {
    var _date = dateFormat3.format(DateTime.now());
    SharedPreferences _preference = await SharedPreferences.getInstance();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
    var _oid = Provider.of<ListOidChangeNotifier>(context, listen: false).listOIDPersonalSelected.AC_CUST_ID.toString();

    var _body = jsonEncode({
      "oid": _oid,
      "lme_id": "",
      "branch_code": _preference.getString("branchid").toString(),
      "source_reff_id": "MS2${_preference.getString("username")}-$_oid",
      "req_date": _date.toString(),
      "source_channel_id": "MS2"
    });
    print("body checkmplactivation: $_body");

    String _getMplActivation = await storage.read(key: "CreditLimitCheckMplActivation");
    try {
      final _response = await _http.post(
        "${BaseUrl.urlLME}$_getMplActivation",
        body: _body,
        headers: {
          "Content-Type":"application/json",
          "Authorization":"bearer $token",
        }
      ).timeout(Duration(seconds: 10));

      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        if(_result['response_code'] == "00") {
          _lmeId = _result['lme_id'];
          getMplActivationWithReffId(context);
          loadData = false;
        } else {
          showSnackBar("${_result['response_desc']}");
          loadData = false;
        }
      }
      else{
        showSnackBar("Error get check mpl activation response ${_response.statusCode}");
        loadData = false;
      }
    }
    on TimeoutException catch(_){
      showSnackBar("Request Timeout");
      loadData = false;
    }
    catch(e) {
      showSnackBar("Error $e");
      loadData = false;
    }
  }

  void getMplActivationWithReffId(BuildContext context) async {
    var _date = dateFormat3.format(DateTime.now());
    SharedPreferences _preference = await SharedPreferences.getInstance();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
    var _oid = Provider.of<ListOidChangeNotifier>(context, listen: false).listOIDPersonalSelected.AC_CUST_ID.toString();

    var _body = jsonEncode({
      "oid": _oid,
      "lme_id": _lmeId,
      "branch_code": _preference.getString("branchid").toString(),
      "source_reff_id": "MS2${_preference.getString("username")}-$_oid",
      "req_date": _date.toString(),
      "source_channel_id": "MS2"
    });
    print("body checkmplactivation: $_body");

    String _getMplActivation = await storage.read(key: "CreditLimitCheckMplActivation");
    try {
      final _response = await _http.post(
        "${BaseUrl.urlLME}$_getMplActivation",
        body: _body,
        headers: {
          "Content-Type":"application/json",
          "Authorization":"bearer $token",
        }
      ).timeout(Duration(seconds: 10));

      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        if(_result['response_code'] == "00") {
          this._checkMplActivationCLSelected = CheckMplActivationModel(
              _result['branch_code'].toString(),
              _result['source_reff_id'].toString(),
              _result['reff_id'].toString(),
              _result['source_channel_id'].toString(),
              _result['oid'].toString(),
              _result['lme_id'].toString(),
              _result['mpl_type'].toString(),
              _result['jenis_kegiatan_usaha'].toString(),
              _result['mpl_status'].toString(),
              _result['disbursement_number'].toString(),
              _result['potensial_disbursement_no'].toString(),
              _result['capacity_limit_mpl'].toString(),
              _result['capacity_ph_mpl'].toString(),
              _result['activation_date'].toString(),
              _result['remaining_tenor'].toString(),
              _result['contract_reference'].toString(),
              _result['activation_branch_code'].toString(),
              _result['activation_channel'].toString(),
              _result['response_code'].toString(),
              _result['response_desc'].toString(),
              _result['response_date'].toString(),
              _result['req_date'].toString()
          );
          this._controllerReferenceNumberCL.text = this._checkMplActivationCLSelected.contract_reference.toString(); // (dari mas Niko) | _result['lme_id'].toString(); (dari mas raymond) catatan: perubahan per tanggal 24.06.2021
          this._controllerMaxPH.text = this._checkMplActivationCLSelected != null ? _formatCurrency(this._checkMplActivationCLSelected.capacity_ph_mpl.toString()) : "0";
          this._controllerMaxInstallment.text = this._checkMplActivationCLSelected != null ? _formatCurrency(this._checkMplActivationCLSelected.capacity_limit_mpl.toString()) : "0";
          this._controllerPortofolio.text = "Jasa/Durable";
          this._controllerTenor.text = this._checkMplActivationCLSelected.remaining_tenor.toString();
          this._controllerMultidisburseOption.text = this._checkMplActivationCLSelected.mpl_type == "01" ? "Opsi B" : this._checkMplActivationCLSelected.mpl_type == "02" ? "Opsi C" : "Opsi D";
          this._controllerDisbursementNo.text = this._checkMplActivationCLSelected.potensial_disbursement_no.toString();
          this._contractReference = this._checkMplActivationCLSelected.contract_reference.toString();
          loadData = false;
        } else {
          showSnackBar("${_result['response_desc']}");
          loadData = false;
        }
      }
      else{
        showSnackBar("Error get check mpl activation response ${_response.statusCode}");
        loadData = false;
      }
    }
    on TimeoutException catch(_){
      showSnackBar("Request Timeout");
      loadData = false;
    }
    catch(e) {
      showSnackBar("Error $e");
      loadData = false;
    }
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  void showSnackBarSuccess(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating,
        backgroundColor: Colors.green, duration: Duration(seconds: 2))
    );
  }

  Future<void> saveToSQLiteMS2LME(BuildContext context, String orderNo) async {
    if(await _dbHelper.deleteMS2LME()) {
      SharedPreferences _preferences = await SharedPreferences.getInstance();
      // var _providerListOid = Provider.of<ListOidChangeNotifier>(context, listen: false);
      _oid = Provider.of<ListOidChangeNotifier>(context, listen: false).listOIDPersonalSelected.AC_CUST_ID;

      _dbHelper.insertMS2LME(MS2LMEModel(
          orderNo,
          Provider.of<ListOidChangeNotifier>(context, listen: false).listOIDPersonalSelected.AC_CUST_ID,
          _lmeId, // lme_id
          "", // appl_no
          _flagEligible.toString(), // flag_eligible
          _disburseType.toString(), // disburse_type
          _productId.toString(),
          _productIdDesc.toString(),
          _installmentAmount.toString(), // installment_amount
          _phAmount.toString(), // ph_amount
          _voucherCode.toString(),
          this._controllerTenor.text,
          DateTime.now().toString(),
          _preferences.getString("username"),
          null,
          null,
          1,
          this._controllerGrading.text, // field grading
          this._typeOfferSelected != null ? this._typeOfferSelected.KODE : "", // jenis_penawaran
          this._typeOfferSelected != null ? this._typeOfferSelected.DESCRIPTION : "", // jenis_penawaran_desc
          this._controllerDisbursementNo.text, // field pencairan_ke
          _opsiMultidisburse, // field opsi_multidisburse
          this._controllerMaxPH.text.replaceAll(",", "").toString(), // field max ph
          this._controllerTenor.text, // field tenor
          this._controllerReferenceNumberCL.text, // field no_referensi
          this._controllerMaxInstallment.text.replaceAll(",", "").toString(), // field max_installment
          this._controllerPortofolio.text, // field portofolio
          "", // plafond_mrp_risk
          _remainingTenor.toString(), // remaining_tenor
          _activationDate.toString(), // activation_date
          this._controllerConsumerName.text, // field nama konsumen
          this._controllerConsumerAddress.text, // field alamat konsumen
          _statusAoro,
          this._controllerNotes.text // field notes
      ));
      // saveToSQLiteMS2LMEDetail(context, orderNo);
    }
  }

  Future<void> autofilledLME(BuildContext context, String orderNo) async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _date = dateFormat3.format(DateTime.now());
    var _timeStart = DateTime.now();
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
    processSubmit = true;

    await setDataToVariable(context);

    var _body = jsonEncode({
      "orderno": orderNo,
      "lme_id": _lmeId != "" ? _lmeId : "",
      "source_reff_id": "MS2${_preferences.getString("username")}",
      "req_date": _date.toString(),
      "source_channel_id": "revamp-ms2",
      "P_OBJT_CONTRACT_NO": _contractReference != "" ? _contractReference : ""
    });
    print("body autofilled lme: $_body");

    String _autofilledLME = await storage.read(key: "AutofilledLME");
    Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, "Start Autofilled LME", "Start Autofilled LME");
    try{
      final _response = await _http.post(
          "${BaseUrl.urlGeneral}$_autofilledLME",
          // "${urlPublic}api/save-draft/insert-dedup-customer",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 10));
      print("response status code autofilled lme: ${_response.statusCode}");
      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        print("result autofilled lme: $_result");
        if(_result['SZFLAGRESPONSE'] != "1"){
          showSnackBar("${_result['SZMESSAGE']}");
          processSubmit = false;
        } else{
          processSubmit = false;
          showSnackBarSuccess("${_result['SZMESSAGE']}");
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).insertLog(context, _timeStart, DateTime.now(), _body, _response.body, "Submit Autofilled LME");
          // Timer(Duration(milliseconds: 1000), () {
          //   Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => Dashboard(flag: 2,)), (Route <dynamic> route) => false);
          // });
        }
      } else{
        showSnackBar("Error Autofilled LME response ${_response.statusCode}");
        processSubmit = false;
      }
    }
    on TimeoutException catch(_){
      showSnackBar("Request Timeout");
      processSubmit = false;
    }
    catch(e){
      showSnackBar("Error $e");
      processSubmit = false;
    }
    processSubmit = false;
    notifyListeners();
  }

  NumberFormat _oCcy = NumberFormat("#,##0.00", "en_US");
  String _formatCurrency(String value) {
    String _newValue;
    if(value != ""){
      if (value.contains(",")) {
        _newValue = value.replaceAll(",", "");
      }
      else {
        _newValue = value;
      }
      double _number = double.parse(_newValue);
      double _numberFormat = double.parse((_number).toStringAsFixed(2));
      String _currency = _oCcy.format(_numberFormat);
      return _currency;
    }
    else{
      return "";
    }
  }

  Future<void> checkLimitEligibleStatus(BuildContext context, String identityNumber, String fullname, DateTime birthDate, String birthPlace,
      String motherName, String identityAddress, String flag) async {
    processSubmit = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _date = dateFormat3.format(DateTime.now());
    var _providerOID = Provider.of<ListOidChangeNotifier>(context, listen: false);

    var _body = jsonEncode({
      "oid" : _providerOID.listOIDPersonalSelected.AC_CUST_ID.toString(),
      "lme_id" : _lmeId != "" ? _lmeId : "",
      "source_reff_id" : "MS2${_preferences.getString("username")}",
      "source_channel_id" : "revamp-ms2",
      "disburse_type" : this._typeOfferSelected != null ? this._typeOfferSelected.KODE =="002" ? "2" : "1" : "1",
      "product_id" : this._listVoucherCLSelected != null ? this._listVoucherCLSelected.product_type : "",
      "contract_no" : "",
      "installment_amount" : "0",
      "ph_amount" : "",
      "voucher_code" : this._controllerReferenceNumberCL.text != "" ? this._controllerReferenceNumberCL.text : "",
      "req_date" : _date.toString(),
      "tenor" : "",
      "cust_id" : _providerOID.listOIDPersonalSelected.AC_ID_NO,
      "branch_code" : _preferences.getString("branchid"),
      "appl_no" : "",
      "flag_book" : "N",
      "jenis_kegiatan_usaha" : ""
    });

    print("body checklimit: $_body");
    String _creditLimitCHeckLimit = await storage.read(key: "CreditLimitCheckLimit");
    try {
      final _response = await _http.post(
          "${BaseUrl.urlLME}$_creditLimitCHeckLimit",
          // "${getUrlLme}InqService/checkLimit",
          body: _body,
          headers: {
            "Content-Type":"application/json",
            "Authorization":"bearer $token",
          }
      ).timeout(Duration(seconds: 10));
      print("response check limit: ${_response.statusCode}");
      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        if(_result['elligible_status'] == "02") {
          showSnackBar("Voucher tidak valid, mohon gunakan voucher lain.");
          this._controllerReferenceNumberCL.clear();
          processSubmit = false;
        } else {
          showSnackBarSuccess("Voucher valid.");
          processSubmit = false;
          submitJoinOid(context, identityNumber, fullname, birthDate, birthPlace, motherName, identityAddress, flag);
        }
      }
      else{
        showSnackBar("Error get check limit response ${_response.statusCode}");
        processSubmit = false;
      }
    }
    on TimeoutException catch(_){
      showSnackBar("Request Timeout");
      processSubmit = false;
    }
    catch(e) {
      showSnackBar("Error $e");
      processSubmit = false;
    }
  }

  // void saveToSQLiteMS2LMEDetail(BuildContext context, String orderNo) async {
  //   SharedPreferences _preferences = await SharedPreferences.getInstance();
  //   var _oid = Provider.of<ListOidChangeNotifier>(context, listen: false).listOIDPersonalSelected.AC_CUST_ID.toString();
  //   _dbHelper.insertMS2LMEDetail(MS2LMEDetailModel(
  //     orderNo,
  //     "MS2${_preferences.getString("username")}-$_oid",
  //     null,
  //     null,
  //     DateTime.now().toString(),
  //     _preferences.getString("username"),
  //     null,
  //     null,
  //     1,
  //   ));
  //   // _submitDataPartial.submitCreditLimitDetail(context);
  // }

  // Future<void> saveToSQLiteMS2CreditLimit(BuildContext context, String orderNo) async {
  //   _dbHelper.insertMS2CreditLimit(MS2CreditLimitModel(
  //     orderNo,
  //     this._controllerConsumerName.text,
  //     this._controllerTelephoneNumber.text,
  //     this._controllerConsumerAddress.text,
  //     this._controllerCustomerType.text,
  //     "", //type desc
  //     this._typeOfferSelected != null ? this._typeOfferSelected.KODE : "", // 5
  //     this._typeOfferSelected != null ? this._typeOfferSelected.DESCRIPTION : "",
  //     this._typeOfferSelected != null && this._typeOfferSelected.KODE != "02" ?  this._controllerReferenceNumberCL.text : "",
  //     this._controllerGrading.text,
  //     "", // grading desc
  //     this._radioProceedProcess.toString(),
  //     this._radioProceedProcess == 0 ? this._controllerNotes.text : "",
  //     this._radioProceedProcess == 1 ? this._controllerMaxPH.text : "",
  //     this._radioProceedProcess == 1 ? this._controllerMaxInstallment.text : "",
  //     this._radioProceedProcess == 1 ? this._controllerPortofolio.text : "",
  //     this._radioProceedProcess == 1 ? this._controllerTenor.text : "",
  //     this._radioProceedProcess == 1 ? this._controllerMultidisburseOption.text : "",
  //     this._radioProceedProcess == 1 ? this.controllerSearchingFrom.text : "",
  //   ));
  //   Timer(Duration(milliseconds: 1000), () {
  //     Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => Dashboard(flag: 2)), (Route <dynamic> route) => false);
  //   });
  // }
}
