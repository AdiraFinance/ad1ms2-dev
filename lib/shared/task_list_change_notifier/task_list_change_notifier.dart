import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/add_detail_installment_credit_subsidy.dart';
import 'package:ad1ms2_dev/models/asset_type_model.dart';
import 'package:ad1ms2_dev/models/data_dedup_model.dart';
import 'package:ad1ms2_dev/models/electricity_type_model.dart';
import 'package:ad1ms2_dev/models/form_m_company_manajemen_pic_model.dart' as pic;
import 'package:ad1ms2_dev/models/form_m_company_rincian_model.dart';
import 'package:ad1ms2_dev/models/form_m_foto_model.dart';
import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:ad1ms2_dev/models/info_document_type_model.dart';
import 'package:ad1ms2_dev/models/manajemen_pic_sqlite_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_fee_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_insurance_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_object_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_objt_wmp_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_objt_coll_oto_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_objt_coll_prop_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_photo.dart';
import 'package:ad1ms2_dev/models/ms2_appl_refund_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_taksasi_model.dart';
import 'package:ad1ms2_dev/models/ms2_application_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_addr_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_income_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_ind_occupation_model.dart';
import 'package:ad1ms2_dev/models/birth_place_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_family_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_shrhldr_com_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_shrhldr_ind_model.dart';
import 'package:ad1ms2_dev/models/ms2_customer_company_model.dart';
import 'package:ad1ms2_dev/models/ms2_customer_individu_model.dart';
import 'package:ad1ms2_dev/models/ms2_customer_model.dart';
import 'package:ad1ms2_dev/models/ms2_lme_model.dart';
import 'package:ad1ms2_dev/models/ms2_objek_sales_model.dart';
import 'package:ad1ms2_dev/models/ms2_document_model.dart';
import 'package:ad1ms2_dev/models/ms2_grntr_comp_model.dart';
import 'package:ad1ms2_dev/models/ms2_grntr_ind_model.dart';
import 'package:ad1ms2_dev/models/ms2_objt_karoseri_model.dart';
import 'package:ad1ms2_dev/models/ms2_sby_assgnmt_model.dart';
import 'package:ad1ms2_dev/models/ms2_svy_rslt_asst_model.dart';
import 'package:ad1ms2_dev/models/ms2_svy_rslt_dtl_model.dart';
import 'package:ad1ms2_dev/models/ownership_model.dart';
import 'package:ad1ms2_dev/models/photo_type_model.dart';
import 'package:ad1ms2_dev/models/road_type_model.dart';
import 'package:ad1ms2_dev/models/survey_photo_model.dart';
import 'package:ad1ms2_dev/models/task_list_model.dart';
import 'package:ad1ms2_dev/models/type_of_financing_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/dashboard/dashboard_change_notif.dart';
import 'package:ad1ms2_dev/shared/home/home_change_notifier.dart';
import 'package:ad1ms2_dev/shared/resource/get_data_cust_info.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';
import '../document_unit_model.dart';
import '../group_unit_object_model.dart';

class TaskListChangeNotifier with ChangeNotifier{
  var storage = FlutterSecureStorage();
  bool _showTextFieldSearch = false;
  TextEditingController _controllerSearch = TextEditingController();
  List<TaskListModel> _taskList = [
    // TaskListModel("001", "Lima Tuju", DateTime(dateNow.year,dateNow.month,dateNow.day - 3), "004","NP"),
    // TaskListModel("002", "Lima Lapan", DateTime(dateNow.year,dateNow.month,dateNow.day - 2), "003","HP"),
    // TaskListModel("003", "Lima Sembilan", DateTime(dateNow.year,dateNow.month,dateNow.day - 1), "002","LP"),
    // TaskListModel("004", "Lima Enam", DateTime(dateNow.year,dateNow.month,dateNow.day), "001","LP"),
  ];
  List<TaskListModel> _taskListTemp = [];

  bool _loadData = false;
  // GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<TaskListModel> get taskList => _taskList;

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showTextFieldSearch => _showTextFieldSearch;

  set showTextFieldSearch(bool value) {
    this._showTextFieldSearch = value;
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
    notifyListeners();
  }

  // GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  UnmodifiableListView<TaskListModel> get taskListTemp {
    return UnmodifiableListView(this._taskListTemp);
  }

  // void navigateForm(String typeFormId, BuildContext context) async{
  //   if(typeFormId == "SA"){
  //     Navigator.push(context, MaterialPageRoute(builder: (context) => SAJobMayor()));
  //   }
  //   else if(typeFormId == "AOS"){
  //     Navigator.push(context, MaterialPageRoute(builder: (context) => FormAOS()));
  //   }
  //   else if(typeFormId == "IA"){
  //     Navigator.push(context, MaterialPageRoute(builder: (context) => FormIA()));
  //   }
  //   else{
  //     Navigator.push(context, MaterialPageRoute(builder: (context) => FormMParent(typeFormId: typeFormId)));
  //   }
  // }

  Future<String> getTaskList() async {
    print("masuk getTaskList");
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    this._taskList.clear();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    try {
      var _body = jsonEncode({
        "P_NIK": _preferences.getString("username"),
      });

      String _urlTaskList = await storage.read(key: "TaskList");
      print("${BaseUrl.urlGeneral}$_urlTaskList");
      final _response = await _http.post(
        "${BaseUrl.urlGeneral}$_urlTaskList",
          // "https://103.110.89.34/public/ms2dev/api/tasklist/get-tasklist-ms2",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      );
      print(_response.statusCode);

      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        // print("cek result $_result");
        if(_result.isEmpty) {
          // showSnackBar("Task List tidak ditemukan");
          loadData = false;
          return "isEmpty";
        } else {
          for(int i=0; i < _result.length; i++){
            var _date = _result[i]['ORDER_DATE'] != null ? DateTime.parse(_result[i]['ORDER_DATE'].toString().replaceAll("T", " ")) : DateTime.now();
            var _dateMS2 = _result[i]['MS2PROCESS_DATE'] != null ? DateTime.parse(_result[i]['MS2PROCESS_DATE'].toString().replaceAll("T", " ")) : DateTime.now();
            _taskList.add(
              TaskListModel(
                _result[i]['ORDER_NO'],
                _date,
                _result[i]['CUST_NAME'],
                _result[i]['CUST_TYPE'],
                _result[i]['LAST_KNOWN_STATE'],
                _result[i]['LAST_KNOWN_HANDLED_BY'],
                _result[i]['IDX'],
                _result[i]['SOURCE_APPLICATION'],
                _result[i]['PRIORITY'],
                _result[i]['OBLIGOR_ID'],
                _result[i]['STATUS_AORO'],
                _dateMS2,
                _result[i]['JENIS_PENAWARAN'],
                _result[i]['NO_REFERENSI'],
                _result[i]['OPSI_MULTIDISBURSE'],
              )
            );
          }
          loadData = false;
          return "isNotEmpty";
        }
      } else {
        // showSnackBar("Error response status ${_response.statusCode}");
        loadData = false;
        throw "Error response status ${_response.statusCode}";
      }
    } catch(e) {
      // showSnackBar("Error $e");
      loadData = false;
      throw "Error response status ${e.toString()}";
    }
  }

  String searchTaskList(String query) {
    print("masuk search");
    print("query search: $query");
    String _message = "";
    if(query.length < 3) {
      _message = "Input minimal 3 karakter";
      // showSnackBar("Input minimal 3 karakter");
      // _taskListTemp.clear();
      // _showTextFieldSearch = false;
    } else {
      _taskListTemp.clear();
      if (query.isEmpty) {
        _message = "";
      }

      _taskList.forEach((dataTaskList) {
        if (dataTaskList.ORDER_NO.contains(query) || dataTaskList.CUST_NAME.contains(query) || dataTaskList.LAST_KNOWN_STATE.contains(query)) {
          _taskListTemp.add(dataTaskList);
        }
      });
      _showTextFieldSearch = true;
      notifyListeners();
    }
    return _message;
  }

  void clearSearchTemp() {
    _taskListTemp.clear();
    _showTextFieldSearch = false;
    _controllerSearch.clear();
    notifyListeners();
  }

  void clearSearch() {
    _taskListTemp.clear();
    _showTextFieldSearch = false;
    _controllerSearch.clear();
    notifyListeners();
  }

  // void showSnackBar(String text){
  //   this._scaffoldKey.currentState.showSnackBar(new SnackBar(
  //       content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  // }

  Future<void> getDataCustInfo(String typeFormId, BuildContext context,String orderNo,String orderDate,String custName, String custType,String IDX,bool isFromHome) async{
    DbHelper _dbHelper = DbHelper();
    Provider.of<DashboardChangeNotif>(context, listen:false).setShowHideMandatoryFormM(context);
    List _lastStep = await _dbHelper.selectLastStep();
    int _index;
    if(_lastStep.isNotEmpty){
      _index = int.parse(_lastStep[0]['idx']);
    }
    else{
      _index = 0;
    }
    Map _data;
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    loadData = true;
    print("cek kondisi index $_index IDX $IDX ${_index <= int.parse(IDX)}");
    if(_index <= int.parse(IDX)){
      print("di if tasklist");
      // try{
      Provider.of<HomeChangeNotifier>(context, listen: false).loadData = true;
      if(custType == "PER"){
        _data = await getCustInfoPer(orderNo).timeout(Duration(seconds: 30));
      }
      else{
        _data = await getCustInfoCom(orderNo).timeout(Duration(seconds: 30));
      }

      if(custType == "PER") {
        //LME
        if(_data['custLme'].isNotEmpty){
          if (await _dbHelper.deleteMS2LME()) {
            _dbHelper.insertMS2LME(MS2LMEModel(
              orderNo,
              _data['custLme'][0]['OID'] != null ? _data['custLme'][0]['OID'].toString(): "",
              _data['custLme'][0]['LME_ID'] != null ? _data['custLme'][0]['LME_ID'].toString() : "",
              _data['custLme'][0]['APPL_NO'] != null ? _data['custLme'][0]['APPL_NO'].toString() : "",
              _data['custLme'][0]['FLAG_ELIGIBLE'] != null ? _data['custLme'][0]['FLAG_ELIGIBLE'].toString() : "",
              _data['custLme'][0]['DISBURSE_TYPE'] != null ? _data['custLme'][0]['DISBURSE_TYPE'].toString() : "",
              _data['custLme'][0]['PRODUCT_ID'] != null ? _data['custLme'][0]['PRODUCT_ID'].toString() : "",
              _data['custLme'][0]['PRODUCT_ID_DESC'] != null ? _data['custLme'][0]['PRODUCT_ID_DESC'].toString() : "",
              _data['custLme'][0]['INSTALLMENT_AMOUNT'] != null ? _data['custLme'][0]['INSTALLMENT_AMOUNT'].toString() : "",
              _data['custLme'][0]['PH_AMOUNT'] != null ? _data['custLme'][0]['PH_AMOUNT'].toString() : "",
              _data['custLme'][0]['VOUCHER_CODE'] != null ? _data['custLme'][0]['VOUCHER_CODE'].toString() : "",
              _data['custLme'][0]['TENOR'].toString(),
              DateTime.now().toString(),
              _preferences.getString("username"),
              null,
              null,
              1,
              _data['custLme'][0]['GRADING'] != null ? _data['custLme'][0]['GRADING'].toString() : "",
              _data['custLme'][0]['JENIS_PENAWARAN'] != null ? _data['custLme'][0]['JENIS_PENAWARAN'].toString() : "",
              _data['custLme'][0]['JENIS_PENAWARAN_DESC'] != null ? _data['custLme'][0]['JENIS_PENAWARAN_DESC'].toString() : "",
              _data['custLme'][0]['PENCAIRAN_KE'] != null ? _data['custLme'][0]['PENCAIRAN_KE'].toString() : "",
              _data['custLme'][0]['OPSI_MULTIDISBURSE'] != null ? _data['custLme'][0]['OPSI_MULTIDISBURSE'].toString() : "",
              _data['custLme'][0]['MAX_PH'] != null ? _data['custLme'][0]['MAX_PH'].toString() : "",
              _data['custLme'][0]['MAX_TENOR'] != null ? _data['custLme'][0]['MAX_TENOR'].toString() : "",
              _data['custLme'][0]['NO_REFERENSI'] != null ? _data['custLme'][0]['NO_REFERENSI'].toString() : "",
              _data['custLme'][0]['MAX_INSTALLMENT'] != null ? _data['custLme'][0]['MAX_INSTALLMENT'].toString() : "",
              _data['custLme'][0]['PORTFOLIO'] != null ? _data['custLme'][0]['PORTFOLIO'].toString() : "",
              _data['custLme'][0]['PLAFOND_MRP_RISK'] != null ? _data['custLme'][0]['PLAFOND_MRP_RISK'].toString() : "",
              _data['custLme'][0]['REMAINING_TENOR'] != null ? _data['custLme'][0]['REMAINING_TENOR'].toString() : "",
              _data['custLme'][0]['ACTIVATION_DATE'] != null ? _data['custLme'][0]['ACTIVATION_DATE'].toString() : "",
              _data['custLme'][0]['CUST_NAME'] != null ? _data['custLme'][0]['CUST_NAME'].toString() : "",
              _data['custLme'][0]['CUST_ADDRESS'] != null ? _data['custLme'][0]['CUST_ADDRESS'].toString() : "",
              _data['custLme'][0]['CUST_TYPE'] != null ? _data['custLme'][0]['CUST_TYPE'].toString() : "",
              _data['custLme'][0]['NOTES'] != null ? _data['custLme'][0]['NOTES'].toString() : "",
            ));
          }
        }

        //LME Detail
        // if(_data['custLmeDetail'].isNotEmpty){
        //   _dbHelper.insertMS2LMEDetail(MS2LMEDetailModel(
        //     orderNo,
        //     _data['custLmeDetail'][0]['AC_SOURCE_REFF_ID'].toString(), // oid "MS2${_preferences.getString("username")}-$_oid",
        //     _data['custLmeDetail'][0]['AC_APPL_OBJT_ID'].toString(),
        //     _data['custLmeDetail'][0]['AC_FLAG_LME'].toString(),
        //     DateTime.now().toString(),
        //     _preferences.getString("username"),
        //     null,
        //     null,
        //     1,
        //   ));
        // }

        // rincian foto
        // if(_preferences.getString("last_known_state") != "IDE") {
        debugPrint("Jalan SRE");
        OccupationModel _occupationModel;
        FinancingTypeModel _financingTypeModel;
        KegiatanUsahaModel _kegiatanUsahaModel;
        JenisKegiatanUsahaModel _jenisKegiatanUsahaModel;
        JenisKonsepModel _jenisKonsepModel;

        if(_data['custOccupation'].isNotEmpty) {
          var _dataOccupation = _data['custOccupation'][0]['OCCUPATION'].toString();
          var _dataOccupationDesc = _data['custOccupation'][0]['OCCUPATION_DESC'];
          _occupationModel = OccupationModel(_dataOccupation, _dataOccupationDesc);
        }
        if(_data['custApplication'].isNotEmpty) {
          var _dataJenisKonsep = _data['custApplication'][0]['KONSEP_TYPE'].toString();
          var _dataJenisKonsepDesc = _data['custApplication'][0]['KONSEP_TYPE_DESC'];
          _jenisKonsepModel = JenisKonsepModel(_dataJenisKonsep, _dataJenisKonsepDesc);
        }
        if(_data['custObject'].isNotEmpty) {
          var _dataFinType = _data['custObject'][0]['FINANCING_TYPE'].toString();
          var _dataKegiatanUsaha = int.parse(_data['custObject'][0]['BUSS_ACTIVITIES']);
          var _dataKegiatanUsahaDesc = _data['custObject'][0]['BUSS_ACTIVITIES_DESC'];
          var _dataJenisKegiatanUsaha = int.parse(_data['custObject'][0]['BUSS_ACTIVITIES_TYPE']);
          var _dataJenisKegiatanUsahaDesc = _data['custObject'][0]['BUSS_ACTIVITIES_TYPE_DESC'];

          _financingTypeModel = FinancingTypeModel(_dataFinType, null);
          _kegiatanUsahaModel = KegiatanUsahaModel(_dataKegiatanUsaha, _dataKegiatanUsahaDesc);
          _jenisKegiatanUsahaModel = JenisKegiatanUsahaModel(_dataJenisKegiatanUsaha, _dataJenisKegiatanUsahaDesc);
        }

        //get image api
        List<ImageFileModel> _listFotoTempatTinggal = []; // foto tempat tinggal
        List<ImageFileModel> _listFotoKegiatanUsaha = []; // foto usaha
        List<Map> _listGroupUnitObjectTemp1 = []; //grup unit
        List<Map> _listGroupUnitObjectTemp2 = []; //grup unit
        List<GroupUnitObjectModel> _listGroupUnitObject = []; // grup unit
        List<DocumentUnitModel> _listDocumentUnit = []; // dokumen unit

        //tempat usaha, tempat tinggal dan grup unit
        if(_data['custPhotoOpsi1'].isNotEmpty){
          for(int i=0; i<_data['custPhotoOpsi1'].length; i++){
            String _docLink = _data['custPhotoOpsi1'][i]['DOC_PATH'];
            if(_docLink != null){
              if(_data['custPhotoOpsi1'][i]['DOC_PATH'].toString()[0] == "{"){
                final ioc = new HttpClient();
                ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
                final _http = IOClient(ioc);
                String _getObjectPathByID = await storage.read(key: "GetObjectPathByID");
                String _url = "${BaseUrl.urlGeneral}$_getObjectPathByID"+"objectId=$_docLink&requestId=2020369&objectStore=ADIRAOS&application=REVAMPMS2";
                //get object path by id
                final _response = await _http.get(_url,
                  headers: {"x-api-key": xApiKey, "Authorization":"bearer $token"},
                );
                if(_response.statusCode == 200){
                  final _result = jsonDecode(_response.body);
                  print("result path = $_result");
                  String _objectPath = _result['objectPath'];
                  String _requestId = _result['requestId'];
                  String _objectId = _result['objectId'];
                  //get file
                  String _getFile = await storage.read(key: "GetFile");
                  String _url = "${BaseUrl.urlGeneral}$_getFile"+"objectPath=$_objectPath&objectId=$_objectId&requestId=$_requestId&objectStore=ADIRAOS&application=REVAMPMS2";
                  final _responseFile = await _http.get(_url,
                    headers: {"x-api-key": xApiKey, "Authorization":"bearer $token"},
                  );

                  if(_responseFile.statusCode == 200){
                    final _result = _responseFile.bodyBytes;
                    //foto tempat tinggal
                    if(_data['custPhotoOpsi1'][i]['DOC_TYPE_ID'] == "SC2"){
                      var _location = File('$globalPath/${_data['custPhotoOpsi1'][i]['DOC_NAME']}');
                      File _img = await _location.writeAsBytes(_result);
                      _listFotoTempatTinggal.add(ImageFileModel(_img, null, null, _img.path, _data['custPhotoOpsi1'][i]['DOC_PATH']));
                    }
                    //foto kegiatan usaha
                    else if(_data['custPhotoOpsi1'][i]['DOC_TYPE_ID'] == "SC3"){
                      var _location = File('$globalPath/${_data['custPhotoOpsi1'][i]['DOC_NAME']}');
                      File _img = await _location.writeAsBytes(_result);
                      _listFotoKegiatanUsaha.add(ImageFileModel(_img, null, null, _img.path, _data['custPhotoOpsi1'][i]['DOC_PATH']));
                    }
                    //foto grup unit
                    else if(_data['custPhotoOpsi1'][i]['DOC_TYPE_ID'] == "SC1"){
                      if(_listGroupUnitObjectTemp1.isEmpty){
                        Map _mapData;
                        _mapData = _data['custPhotoOpsi1'][i];
                        _mapData['file'] = _result;
                        _listGroupUnitObjectTemp1.add(_mapData);
                      }
                      else if(_data['custPhotoOpsi1'][i]['OBJECT_GROUP'] == _listGroupUnitObjectTemp1[0]['OBJECT_GROUP'] && _data['custPhotoOpsi1'][i]['OBJECT'] == _listGroupUnitObjectTemp1[0]['OBJECT']){
                        Map _mapData;
                        _mapData = _data['custPhotoOpsi1'][i];
                        _mapData['file'] = _result;
                        _listGroupUnitObjectTemp1.add(_mapData);
                      }
                      else{
                        Map _mapData;
                        _mapData = _data['custPhotoOpsi1'][i];
                        _mapData['file'] = _result;
                        _listGroupUnitObjectTemp2.add(_mapData);
                      }
                    }
                  }
                }
              }
              else{
                print("ini DISSSS custPhotoOpsi1");
                String _date = formatDateListOID(DateTime.now()).replaceAll("-", "");
                String _time = formatTime(DateTime.now()).replaceAll(":", "");

                final ioc = new HttpClient();
                ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
                final _http = IOClient(ioc);
                String _getFileDIS = await storage.read(key: "GetFileDIS");
                final _response = await _http.post("${BaseUrl.urlGeneralPublic}$_getFileDIS",
                  body: {
                    "HEADER_ID" : _docLink
                  },
                );
                if(_response.statusCode == 200){
                  final _result = _response.bodyBytes;
                  //foto tempat tinggal
                  if(_data['custPhotoOpsi1'][i]['DOC_TYPE_ID'] == "SC2"){
                    var _location = File('$globalPath/DIS_${_date}_${_time}_${_data['custPhotoOpsi1'][i]['DOC_NAME']}');
                    File _img = await _location.writeAsBytes(_result);
                    _listFotoTempatTinggal.add(ImageFileModel(_img, null, null, _img.path, _data['custPhotoOpsi1'][i]['DOC_PATH']));
                  }
                  //foto kegiatan usaha
                  else if(_data['custPhotoOpsi1'][i]['DOC_TYPE_ID'] == "SC3"){
                    var _location = File('$globalPath/DIS_${_date}_${_time}_${_data['custPhotoOpsi1'][i]['DOC_NAME']}');
                    File _img = await _location.writeAsBytes(_result);
                    _listFotoKegiatanUsaha.add(ImageFileModel(_img, null, null, _img.path, _data['custPhotoOpsi1'][i]['DOC_PATH']));
                  }
                  //foto grup unit
                  else if(_data['custPhotoOpsi1'][i]['DOC_TYPE_ID'] == "SC1"){
                    if(_listGroupUnitObjectTemp1.isEmpty){
                      Map _mapData;
                      _mapData = _data['custPhotoOpsi1'][i];
                      _mapData['file'] = _result;
                      _mapData['dis'] = "1";
                      _listGroupUnitObjectTemp1.add(_mapData);
                    }
                    else if(_data['custPhotoOpsi1'][i]['OBJECT_GROUP'] == _listGroupUnitObjectTemp1[0]['OBJECT_GROUP'] && _data['custPhotoOpsi1'][i]['OBJECT'] == _listGroupUnitObjectTemp1[0]['OBJECT']){
                      Map _mapData;
                      _mapData = _data['custPhotoOpsi1'][i];
                      _mapData['file'] = _result;
                      _mapData['dis'] = "1";
                      _listGroupUnitObjectTemp1.add(_mapData);
                    }
                    else{
                      Map _mapData;
                      _mapData = _data['custPhotoOpsi1'][i];
                      _mapData['file'] = _result;
                      _mapData['dis'] = "1";
                      _listGroupUnitObjectTemp2.add(_mapData);
                    }
                  }
                }
                else{
                  if(_data['custPhotoOpsi1'][i]['DOC_TYPE_ID'] == "SC1"){
                    //foto grup unit
                    if(_listGroupUnitObjectTemp1.isEmpty){
                      Map _mapData;
                      _mapData = _data['custPhotoOpsi1'][i];
                      _listGroupUnitObjectTemp1.add(_mapData);
                    }
                    else if(_data['custPhotoOpsi1'][i]['OBJECT_GROUP'] == _listGroupUnitObjectTemp1[0]['OBJECT_GROUP'] && _data['custPhotoOpsi1'][i]['OBJECT'] == _listGroupUnitObjectTemp1[0]['OBJECT']){
                      Map _mapData;
                      _mapData = _data['custPhotoOpsi1'][i];
                      _listGroupUnitObjectTemp1.add(_mapData);
                    }
                    else{
                      Map _mapData;
                      _mapData = _data['custPhotoOpsi1'][i];
                      _listGroupUnitObjectTemp2.add(_mapData);
                    }
                  }
                }
              }
            }
          }
        }

        //list grup unit 1
        if(_listGroupUnitObjectTemp1.isNotEmpty){
          String _date = formatDateListOID(DateTime.now()).replaceAll("-", "");
          String _time = formatTime(DateTime.now()).replaceAll(":", "");
          List<ImageFileModel> _listImageGroupObjectUnit = [];
          var _groupObjectSelected = GroupObjectUnit(_listGroupUnitObjectTemp1[0]['OBJECT_GROUP'], _listGroupUnitObjectTemp1[0]['OBJECT_GROUP_DESC']);
          var _unitObject = ObjectUnit(_listGroupUnitObjectTemp1[0]['OBJECT'], _listGroupUnitObjectTemp1[0]['OBJECT_DESC']);
          for(int i=0; i<_listGroupUnitObjectTemp1.length; i++){
            if(_listGroupUnitObjectTemp1[i]['file'] != null){
              var _location;
              if(_listGroupUnitObjectTemp1[i]['dis'] == "1"){
                _location = File('$globalPath/DIS_${_date}_${_time}_${_listGroupUnitObjectTemp1[i]['DOC_NAME']}');
              }
              else{
                _location = File('$globalPath/${_listGroupUnitObjectTemp1[i]['DOC_NAME']}');
              }
              // var _location = File('$globalPath/${_listGroupUnitObjectTemp1[i]['DOC_NAME']}');
              File _img = await _location.writeAsBytes(_listGroupUnitObjectTemp1[i]['file']);
              _listImageGroupObjectUnit.add(ImageFileModel(_img, null, null, _img.path, _listGroupUnitObjectTemp1[i]['DOC_PATH']));
            }
          }
          _listGroupUnitObject.add(GroupUnitObjectModel(
            _groupObjectSelected, _unitObject, _listImageGroupObjectUnit, _listGroupUnitObjectTemp1[0]['DOC_PATH'] != "",
            false, false, false, false
          ));
        }

        //list grup unit 2
        if(_listGroupUnitObjectTemp2.isNotEmpty){
          String _date = formatDateListOID(DateTime.now()).replaceAll("-", "");
          String _time = formatTime(DateTime.now()).replaceAll(":", "");
          List<ImageFileModel> _listImageGroupObjectUnit = [];
          var _groupObjectSelected = GroupObjectUnit(_listGroupUnitObjectTemp2[0]['OBJECT_GROUP'], _listGroupUnitObjectTemp2[0]['OBJECT_GROUP_DESC']);
          var _unitObject = ObjectUnit(_listGroupUnitObjectTemp2[0]['OBJECT'], _listGroupUnitObjectTemp2[0]['OBJECT_DESC']);
          for(int i=0; i<_listGroupUnitObjectTemp2.length; i++){
            if(_listGroupUnitObjectTemp2[i]['file'] != null){
              var _location;
              if(_listGroupUnitObjectTemp1[i]['dis'] == "1"){
                _location = File('$globalPath/DIS_${_date}_${_time}_${_listGroupUnitObjectTemp2[i]['DOC_NAME']}');
              }
              else{
                _location = File('$globalPath/${_listGroupUnitObjectTemp2[i]['DOC_NAME']}');
              }
              // var _location = File('$globalPath/${_listGroupUnitObjectTemp2[i]['DOC_NAME']}');
              File _img = await _location.writeAsBytes(_listGroupUnitObjectTemp2[i]['file']);
              _listImageGroupObjectUnit.add(ImageFileModel(_img, null, null, _img.path, _listGroupUnitObjectTemp2[i]['DOC_PATH']));
            }
          }
          _listGroupUnitObject.add(GroupUnitObjectModel(
              _groupObjectSelected, _unitObject, _listImageGroupObjectUnit, _listGroupUnitObjectTemp2[0]['DOC_PATH'] != "",
              false, false, false, false
          ));
        }

        //dokumen unit
        if(_data['custPhotoOpsi3'].isNotEmpty){
          for(int i=0; i<_data['custPhotoOpsi3'].length; i++){
            String _docLink = _data['custPhotoOpsi3'][i]['DOC_PATH'];
            if(_docLink != null){
              if(_data['custPhotoOpsi3'][i]['DOC_PATH'].toString()[0] == "{"){
                final ioc = new HttpClient();
                ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
                final _http = IOClient(ioc);
                String _getObjectPathByID = await storage.read(key: "GetObjectPathByID"); //get path
                String _url = "${BaseUrl.urlGeneral}$_getObjectPathByID"+"objectId=$_docLink&requestId=2020369&objectStore=ADIRAOS&application=REVAMPMS2";
                //get object path by id
                final _response = await _http.get(_url,
                  headers: {"x-api-key": xApiKey, "Authorization":"bearer $token"},
                );
                if(_response.statusCode == 200){
                  final _result = jsonDecode(_response.body);
                  print("result path = $_result");
                  String _objectPath = _result['objectPath'];
                  String _requestId = _result['requestId'];
                  String _objectId = _result['objectId'];
                  //get file
                  String _getFile = await storage.read(key: "GetFile"); //get file untuk mendapatkan balikan bytes dan nanti dijadikan img
                  String _url = "${BaseUrl.urlGeneral}$_getFile"+"objectPath=$_objectPath&objectId=$_objectId&requestId=$_requestId&objectStore=ADIRAOS&application=REVAMPMS2";
                  final _responseFile = await _http.get(_url,
                    headers: {"x-api-key": xApiKey, "Authorization":"bearer $token"},
                  );

                  if(_responseFile.statusCode == 200){
                    final _result = _responseFile.bodyBytes;
                    var _location = File('$globalPath/${_data['custPhotoOpsi3'][i]['DOC_NAME']}');
                    File _img = await _location.writeAsBytes(_result); //bytes diubah menjadi file img
                    var _fileDocument = {
                      "file":_img,
                      "file_name": _img.path.split("/").last,
                    };
                    var jenisDokumen = InfoDocumentTypeModel(
                      _data['custPhotoOpsi3'][i]['DOC_TYPE_ID'],
                      _data['custPhotoOpsi3'][i]['DOC_TYPE_DESC'],
                      int.parse(_data['custPhotoOpsi3'][i]['FLAG_MANDATORY']),
                      int.parse(_data['custPhotoOpsi3'][i]['FLAG_DISPLAY']),
                      int.parse(_data['custPhotoOpsi3'][i]['FLAG_UNIT']),
                    );
                    var dateReceive = DateTime.parse(_data['custPhotoOpsi3'][i]['RECEIVED_DATE']);

                    _listDocumentUnit.add(DocumentUnitModel(
                        jenisDokumen,
                        _data['custPhotoOpsi3'][i]['DOC_ID'],
                        dateReceive,
                        _fileDocument,
                        _img.path, //_data['custPhotoOpsi3'][i]['path_photo'],
                        null, //double.parse(_data['custPhotoOpsi3'][i]['latitude']),
                        null, //double.parse(_data['custPhotoOpsi3'][i]['longitude']),
                        null,
                        _data['custPhotoOpsi3'][i]['DOC_PATH'],
                        false,
                        false,
                        false,
                        false
                    ));
                  }
                }
              }
              else{
                print("ini DISSSS custPhotoOpsi3");
                String _date = formatDateListOID(DateTime.now()).replaceAll("-", "");
                String _time = formatTime(DateTime.now()).replaceAll(":", "");

                final ioc = new HttpClient();
                ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
                final _http = IOClient(ioc);
                String _getFileDIS = await storage.read(key: "GetFileDIS");
                final _response = await _http.post("${BaseUrl.urlGeneralPublic}$_getFileDIS",
                  body: {
                    "HEADER_ID" : _docLink
                  },
                );
                if(_response.statusCode == 200){
                  final _result = _response.bodyBytes;
                  var _location = File('$globalPath/DIS_${_date}_${_time}_${_data['custPhotoOpsi3'][i]['DOC_NAME']}');
                  File _img = await _location.writeAsBytes(_result);
                  var _fileDocument = {
                    "file":_img,
                    "file_name": _img.path.split("/").last,
                  };
                  var jenisDokumen = InfoDocumentTypeModel(
                    _data['custPhotoOpsi3'][i]['DOC_TYPE_ID'],
                    _data['custPhotoOpsi3'][i]['DOC_TYPE_DESC'],
                    int.parse(_data['custPhotoOpsi3'][i]['FLAG_MANDATORY']),
                    int.parse(_data['custPhotoOpsi3'][i]['FLAG_DISPLAY']),
                    int.parse(_data['custPhotoOpsi3'][i]['FLAG_UNIT']),
                  );
                  var dateReceive = DateTime.parse(_data['custPhotoOpsi3'][i]['RECEIVED_DATE']);

                  _listDocumentUnit.add(DocumentUnitModel(
                      jenisDokumen,
                      "NEW",
                      dateReceive,
                      _fileDocument,
                      _img.path, //_data['custPhotoOpsi3'][i]['path_photo'],
                      null, //double.parse(_data['custPhotoOpsi3'][i]['latitude']),
                      null, //double.parse(_data['custPhotoOpsi3'][i]['longitude']),
                      null,
                      _data['custPhotoOpsi3'][i]['DOC_PATH'],
                      false,
                      false,
                      false,
                      false
                  ));
                }
                else{
                  var jenisDokumen = InfoDocumentTypeModel(
                    _data['custPhotoOpsi3'][i]['DOC_TYPE_ID'],
                    _data['custPhotoOpsi3'][i]['DOC_TYPE_DESC'],
                    int.parse(_data['custPhotoOpsi3'][i]['FLAG_MANDATORY']),
                    int.parse(_data['custPhotoOpsi3'][i]['FLAG_DISPLAY']),
                    int.parse(_data['custPhotoOpsi3'][i]['FLAG_UNIT']),
                  );
                  var dateReceive = DateTime.parse(_data['custPhotoOpsi3'][i]['RECEIVED_DATE']);
                  _listDocumentUnit.add(DocumentUnitModel(
                      jenisDokumen,
                      _data['custPhotoOpsi3'][i]['DOC_ID'],
                      dateReceive,
                      null,
                      null, //_data['custPhotoOpsi3'][i]['path_photo'],
                      null, //double.parse(_data['custPhotoOpsi3'][i]['latitude']),
                      null, //double.parse(_data['custPhotoOpsi3'][i]['longitude']),
                      null,
                      _data['custPhotoOpsi3'][i]['DOC_PATH'],
                      false,
                      false,
                      false,
                      false
                  ));
                }
              }
            }
          }
        }

        if(_listFotoTempatTinggal.isNotEmpty || _listFotoKegiatanUsaha.isNotEmpty || _listGroupUnitObject.isNotEmpty || _listDocumentUnit.isNotEmpty || _data['custOccupation'].isNotEmpty ||  _data['custApplication'].isNotEmpty || _data['custObject'].isNotEmpty) {
          if(await _dbHelper.deleteMS2PhotoHeader(_listFotoTempatTinggal.isNotEmpty || _listFotoKegiatanUsaha.isNotEmpty || _listGroupUnitObject.isNotEmpty || _listDocumentUnit.isNotEmpty ? false : true)) {
            await _dbHelper.insertMS2PhotoHeader(MS2ApplPhotoModel(
                _listFotoTempatTinggal, //foto tempat tinggal
                _occupationModel,
                _listFotoKegiatanUsaha, //foto usaha
                _financingTypeModel,
                _kegiatanUsahaModel,
                _jenisKegiatanUsahaModel,
                _jenisKonsepModel,
                _listGroupUnitObject, //list grup objek
                _listDocumentUnit, //list dokumen
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
            ));
          }
        }

        //cust info
        print("cek data custDetailInfoPersonal ${_data['custDetailInfoPersonal']}");
        if(_preferences.getString("status_aoro") != "0" && _preferences.getString("last_known_state") == "IDE"){
          if (_data['custDetailInfoPersonal'].isNotEmpty) {
            print("di if custDetailInfoPersonal");
            if(await _dbHelper.deleteMS2CustomerPersonal()){
              await _dbHelper.insertMS2CustomerPersonal(MS2CustomerIndividuModel(
                null,
                _data['custDetailInfoPersonal'][0]['CUST_IND_ID'],
                _data['custDetailInfoPersonal'][0]['NO_OF_LIABILITY'] != null ? int.parse(_data['custDetailInfoPersonal'][0]['NO_OF_LIABILITY'].toString().replaceAll(",", "")) : null,
                GCModel(_data['custDetailInfoPersonal'][0]['CUST_GROUP'], _data['custDetailInfoPersonal'][0]['CUST_GROUP_DESC']),
                EducationModel(_data['custDetailInfoPersonal'][0]['EDUCATION'], _data['custDetailInfoPersonal'][0]['EDUCATION_DESC']),
                IdentityModel(_data['custDetailInfoPersonal'][0]['ID_TYPE'], _data['custDetailInfoPersonal'][0]['ID_DESC']),
                _data['custDedup'][0]['ID_NO'],//_data['custDetailInfoPersonal'][0]['ID_NO'] != null ? _data['custDetailInfoPersonal'][0]['ID_NO'] : "",
                _data['custDetailInfoPersonal'][0]['FULL_NAME_ID'] != null ? _data['custDetailInfoPersonal'][0]['FULL_NAME_ID'] : "",
                _data['custDedup'][0]['CUST_NAME'],//_data['custDetailInfoPersonal'][0]['FULL_NAME'] != null ? _data['custDetailInfoPersonal'][0]['FULL_NAME'] : "",
                "",
                "",
                _data['custDedup'][0]['DATE_BIRTH'],//_data['custDetailInfoPersonal'][0]['DATE_OF_BIRTH'] != null ? _data['custDetailInfoPersonal'][0]['DATE_OF_BIRTH'] : "",
                _data['custDedup'][0]['PLACE_BIRTH'],//_data['custDetailInfoPersonal'][0]['PLACE_OF_BIRTH'] != null ? _data['custDetailInfoPersonal'][0]['PLACE_OF_BIRTH'] : "",
                BirthPlaceModel(_data['custDetailInfoPersonal'][0]['PLACE_OF_BIRTH_KABKOTA'], _data['custDetailInfoPersonal'][0]['PLACE_OF_BIRTH_KABKOTA_DESC']),
                _data['custDetailInfoPersonal'][0]['GENDER'] != null ? _data['custDetailInfoPersonal'][0]['GENDER'] : "",
                _data['custDetailInfoPersonal'][0]['GENDER_DESC'] != null ? _data['custDetailInfoPersonal'][0]['GENDER_DESC'] : "",
                _data['custDetailInfoPersonal'][0]['ID_DATE'] != null ? _data['custDetailInfoPersonal'][0]['ID_DATE'] : "",
                _data['custDetailInfoPersonal'][0]['ID_EXPIRE_DATE'] != null ? _data['custDetailInfoPersonal'][0]['ID_EXPIRE_DATE'] : "",
                ReligionModel(_data['custDetailInfoPersonal'][0]['RELIGION'], _data['custDetailInfoPersonal'][0]['RELIGION_DESC']),
                MaritalStatusModel(_data['custDetailInfoPersonal'][0]['MARITAL_STATUS'], _data['custDetailInfoPersonal'][0]['MARITAL_STATUS_DESC']),
                _data['custDetailInfoPersonal'][0]['HANDPHONE_NO'] != null ? _data['custDetailInfoPersonal'][0]['HANDPHONE_NO'].toString().replaceRange(0, 2, "") : "",
                _data['custDetailInfoPersonal'][0]['EMAIL'] != null ? _data['custDetailInfoPersonal'][0]['EMAIL'] : "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                null,
                null,
                "",
                "",
                "",
                "",
                "",
                "",
                1,
                2,
                _data['custDetailInfoPersonal'][0]['NO_WA'],
                _data['custDetailInfoPersonal'][0]['NO_WA_2'],
                _data['custDetailInfoPersonal'][0]['NO_WA_3'],
                _data['custDetailInfoPersonal'][0]['edit_cust_group'] == '1',
                _data['custDetailInfoPersonal'][0]['edit_id_type'] == '1',
                _data['custDetailInfoPersonal'][0]['edit_id_no'] == '1',
                _data['custDetailInfoPersonal'][0]['edit_id_date'] == '1',
                _data['custDetailInfoPersonal'][0]['edit_flag_id_lifetime'] == '1',
                _data['custDetailInfoPersonal'][0]['edit_id_expired_date'] == '1',
                _data['custDetailInfoPersonal'][0]['edit_full_name_id'] == '1',
                _data['custDetailInfoPersonal'][0]['edit_full_name'] == '1',
                _data['custDetailInfoPersonal'][0]['edit_date_of_birth'] == '1',
                _data['custDetailInfoPersonal'][0]['edit_place_of_birth'] == '1',
                _data['custDetailInfoPersonal'][0]['edit_place_of_birth_kabkota'] == '1',
                _data['custDetailInfoPersonal'][0]['edit_gender'] == '1',
                _data['custDetailInfoPersonal'][0]['edit_religion'] == '1',
                _data['custDetailInfoPersonal'][0]['edit_education'] == '1',
                _data['custDetailInfoPersonal'][0]['edit_tanggungan'] == '1',
                _data['custDetailInfoPersonal'][0]['edit_marital_status'] == '1',
                _data['custDetailInfoPersonal'][0]['edit_email'] == '1',
                _data['custDetailInfoPersonal'][0]['edit_handphone_no'] == '1',
                _data['custDetailInfoPersonal'][0]['edit_no_wa'] == '1',
                _data['custDetailInfoPersonal'][0]['edit_no_wa_2'] == '1',
                _data['custDetailInfoPersonal'][0]['edit_no_wa_2'] == '1',
                _data['custDetailInfoPersonal'][0]['edit_no_wa_3'] == '1',
                _data['custDetailInfoPersonal'][0]['edit_no_wa_3'] == '1',
              ));
            }
          }
          else{
            print("di else custDetailInfoPersonal");
            print(_data['custDedup']);
            if(await _dbHelper.deleteMS2CustomerPersonal()){
              await _dbHelper.insertMS2CustomerPersonal(MS2CustomerIndividuModel(
                null,
                null,
                null,
                null,
                null,
                null,
                _data['custDedup'][0]['ID_NO'],
                null,
                _data['custDedup'][0]['CUST_NAME'],
                null,
                null,
                _data['custDedup'][0]['DATE_BIRTH'],
                _data['custDedup'][0]['PLACE_BIRTH'],
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
              ));
            }
          }
        }
        else{
          if (_data['custDetailInfoPersonal'].isNotEmpty) {
            List _dataDakor = [];
            if(_preferences.getString("last_known_state") == "DKR"){
              print("di if custDetailInfoPersonal DKR");
              _dataDakor = await _dbHelper.selectDataInfoNasabah();
            }

            if(await _dbHelper.deleteMS2CustomerPersonal()){
              await _dbHelper.insertMS2CustomerPersonal(MS2CustomerIndividuModel(
                null,
                _data['custDetailInfoPersonal'][0]['CUST_IND_ID'],
                _data['custDetailInfoPersonal'][0]['NO_OF_LIABILITY'] != null ? int.parse(_data['custDetailInfoPersonal'][0]['NO_OF_LIABILITY'].toString().replaceAll(",", "")) : null,
                GCModel(_data['custDetailInfoPersonal'][0]['CUST_GROUP'], _data['custDetailInfoPersonal'][0]['CUST_GROUP_DESC']),
                EducationModel(_data['custDetailInfoPersonal'][0]['EDUCATION'], _data['custDetailInfoPersonal'][0]['EDUCATION_DESC']),
                IdentityModel(_data['custDetailInfoPersonal'][0]['ID_TYPE'], _data['custDetailInfoPersonal'][0]['ID_DESC']),
                _data['custDetailInfoPersonal'][0]['ID_NO'] != null ? _data['custDetailInfoPersonal'][0]['ID_NO'] : "",
                _data['custDetailInfoPersonal'][0]['FULL_NAME_ID'] != null ? _data['custDetailInfoPersonal'][0]['FULL_NAME_ID'] : "",
                _data['custDetailInfoPersonal'][0]['FULL_NAME'] != null ? _data['custDetailInfoPersonal'][0]['FULL_NAME'] : "",
                "",
                "",
                _data['custDetailInfoPersonal'][0]['DATE_OF_BIRTH'] != null ? _data['custDetailInfoPersonal'][0]['DATE_OF_BIRTH'] : "",
                _data['custDetailInfoPersonal'][0]['PLACE_OF_BIRTH'] != null ? _data['custDetailInfoPersonal'][0]['PLACE_OF_BIRTH'] : "",
                BirthPlaceModel(_data['custDetailInfoPersonal'][0]['PLACE_OF_BIRTH_KABKOTA'], _data['custDetailInfoPersonal'][0]['PLACE_OF_BIRTH_KABKOTA_DESC']),
                _data['custDetailInfoPersonal'][0]['GENDER'] != null ? _data['custDetailInfoPersonal'][0]['GENDER'] : "",
                _data['custDetailInfoPersonal'][0]['GENDER_DESC'] != null ? _data['custDetailInfoPersonal'][0]['GENDER_DESC'] : "",
                _data['custDetailInfoPersonal'][0]['ID_DATE'] != null ? _data['custDetailInfoPersonal'][0]['ID_DATE'] : "",
                _data['custDetailInfoPersonal'][0]['ID_EXPIRE_DATE'] != null ? _data['custDetailInfoPersonal'][0]['ID_EXPIRE_DATE'] : "",
                ReligionModel(_data['custDetailInfoPersonal'][0]['RELIGION'], _data['custDetailInfoPersonal'][0]['RELIGION_DESC']),
                MaritalStatusModel(_data['custDetailInfoPersonal'][0]['MARITAL_STATUS'], _data['custDetailInfoPersonal'][0]['MARITAL_STATUS_DESC']),
                _data['custDetailInfoPersonal'][0]['HANDPHONE_NO'] != null ? _data['custDetailInfoPersonal'][0]['HANDPHONE_NO'].toString().replaceRange(0, 2, "") : "",
                _data['custDetailInfoPersonal'][0]['EMAIL'] != null ? _data['custDetailInfoPersonal'][0]['EMAIL'] : "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                null,
                null,
                "",
                "",
                "",
                "",
                "",
                "",
                1,
                2,
                _data['custDetailInfoPersonal'][0]['NO_WA'],
                _data['custDetailInfoPersonal'][0]['NO_WA_2'],
                _data['custDetailInfoPersonal'][0]['NO_WA_3'],
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_cust_group'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_id_type'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_id_no'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_id_date'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_flag_id_lifetime'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_id_expired_date'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_full_name_id'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_full_name'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_date_of_birth'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_place_of_birth'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_place_of_birth_kabkota'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_gender'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_religion'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_education'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_no_of_liability'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_marital_status'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_email'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_handphone_no'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_no_wa'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_no_wa_2'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_no_wa_2'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_no_wa_3'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_no_wa_3'] == '1' : false,
              ));
            }
          }
          else{
            print("di else custDetailInfoPersonal");
            print(_data['custDedup']);
            if(await _dbHelper.deleteMS2CustomerPersonal()){
              await _dbHelper.insertMS2CustomerPersonal(MS2CustomerIndividuModel(
                null,
                null,
                null,
                null,
                null,
                null,
                _data['custDedup'][0]['ID_NO'],
                null,
                _data['custDedup'][0]['CUST_NAME'],
                null,
                null,
                _data['custDedup'][0]['DATE_BIRTH'],
                _data['custDedup'][0]['PLACE_BIRTH'],
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
              ));
            }
          }
        }

        // npwp
        if(_data['custDetailInfoPersonal'].isNotEmpty){
          List _dataDakor = [];
          if(_preferences.getString("last_known_state") == "DKR"){
            _dataDakor = await _dbHelper.selectMS2Customer();
          }

          if(await _dbHelper.deleteMS2Customer()){
            debugPrint("ID_OBLIGOR TASK_LIST CHANGE NOTIFIER ${ _data['custDetailInfoPersonal'][0]['ID_OBLIGOR']}");
            await _dbHelper.insertMS2Customer(
                MS2CustomerModel(
                    null,
                    _data['custDetailInfoPersonal'][0]['CUST_ID'],
                    _data['custDetailInfoPersonal'][0]['ID_OBLIGOR'],
                    null,
                    _data['custDetailInfoPersonal'][0]['NPWP_ADDRESS'] != null ? _data['custDetailInfoPersonal'][0]['NPWP_ADDRESS'] : "",
                    _data['custDetailInfoPersonal'][0]['NPWP_NAME'] != null ? _data['custDetailInfoPersonal'][0]['NPWP_NAME'] : "",
                    _data['custDetailInfoPersonal'][0]['NPWP_NO'] != null ? _data['custDetailInfoPersonal'][0]['NPWP_NO'].replaceAll(".", "").replaceAll("-", "") : "",
                    JenisNPWPModel(_data['custDetailInfoPersonal'][0]['NPWP_TYPE'], _data['custDetailInfoPersonal'][0]['NPWP_TYPE_DESC']),
                    _data['custDetailInfoPersonal'][0]['FLAG_NPWP'] != null ? int.parse(_data['custDetailInfoPersonal'][0]['FLAG_NPWP']) : 0,
                    _data['custDetailInfoPersonal'][0]['PKP_FLAG'] != null ? _data['custDetailInfoPersonal'][0]['PKP_FLAG'].toString() : "",
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    1,
                    _dataDakor.isNotEmpty ? _dataDakor[0]['edit_handphone_no'] : '0',
                    _dataDakor.isNotEmpty ? _dataDakor[0]['edit_no_wa'] : '0',
                    _dataDakor.isNotEmpty ? _dataDakor[0]['edit_no_wa_2'] : '0',
                    _dataDakor.isNotEmpty ? _dataDakor[0]['edit_no_wa_2'] : '0',
                    _dataDakor.isNotEmpty ? _dataDakor[0]['edit_no_wa_3'] : '0',
                    _dataDakor.isNotEmpty ? _dataDakor[0]['edit_no_wa_3'] : '0',
                )
            );
          }
        }

        //cust family
        if(_data['custFamily'].isNotEmpty){
          print("data family tidak kosong ");
          if(_preferences.getString("status_aoro") != "0" && _preferences.getString("last_known_state") == "IDE"){
            if(await _dbHelper.deleteMS2CustFamily()){
              for(int i = 0; i < _data['custFamily'].length; i++){
                if(_data['custFamily'][i]['RELATION_STATUS'] == "05"){
                  await _dbHelper.insertMS2CustFamily(MS2CustFamilyModel(
                      null,
                      _data['custFamily'][i]['FAMILY_ID'],
                      _data['custFamily'][i]['RELATION_STATUS'] != null ? _data['custFamily'][i]['RELATION_STATUS'] : "",
                      _data['custFamily'][i]['RELATION_STATUS_DESC'] != null ? _data['custFamily'][i]['RELATION_STATUS_DESC'] : "",
                      _data['custFamily'][i]['FULL_NAME_ID'] != null ? _data['custFamily'][i]['FULL_NAME_ID'] : "",
                      _data['custDedup'][0]['MOTHER_NAME'],//_data['custFamily'][i]['FULL_NAME'] != null ? _data['custFamily'][i]['FULL_NAME'] : "",
                      _data['custFamily'][i]['ID_NO'] != null ? _data['custFamily'][i]['ID_NO'] : "",
                      _data['custFamily'][i]['DATE_OF_BIRTH'] != null ? _data['custFamily'][i]['DATE_OF_BIRTH'] : null,
                      _data['custFamily'][i]['PLACE_OF_BIRTH'] != null ? _data['custFamily'][i]['PLACE_OF_BIRTH'] : "",
                      _data['custFamily'][i]['PLACE_OF_BIRTH_KABKOTA'] != null ? _data['custFamily'][i]['PLACE_OF_BIRTH_KABKOTA'] : "",
                      _data['custFamily'][i]['PLACE_OF_BIRTH_KABKOTA_DESC'] != null ? _data['custFamily'][i]['PLACE_OF_BIRTH_KABKOTA_DESC'] : "",
                      _data['custFamily'][i]['GENDER'] != null ? _data['custFamily'][i]['GENDER'] : "",
                      _data['custFamily'][i]['GENDER_DESC'] != null ? _data['custFamily'][i]['GENDER_DESC'] : "",
                      null,
                      _data['custFamily'][i]['ID_TYPE'] != null ? _data['custFamily'][i]['ID_TYPE'] : "",
                      _data['custFamily'][i]['ID_DESC'] != null ? _data['custFamily'][i]['ID_DESC'] : "",
                      null,
                      null,
                      null,
                      null,
                      1,
                      null,
                      _data['custFamily'][i]['PHONE'] != null ? _data['custFamily'][i]['PHONE'] : "",
                      _data['custFamily'][i]['PHONE_AREA'] != null ? _data['custFamily'][i]['PHONE_AREA'] : "",
                      _data['custFamily'][i]['HANDPHONE_NO'] != null  && _data['custFamily'][i]['HANDPHONE_NO'].toString().length > 3 ? _data['custFamily'][i]['HANDPHONE_NO'].toString().replaceRange(0, 2, "") : "",
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null
                  ));
                }
                await _dbHelper.insertMS2CustFamily(MS2CustFamilyModel(
                    null,
                    _data['custFamily'][i]['FAMILY_ID'],
                    _data['custFamily'][i]['RELATION_STATUS'] != null ? _data['custFamily'][i]['RELATION_STATUS'] : "",
                    _data['custFamily'][i]['RELATION_STATUS_DESC'] != null ? _data['custFamily'][i]['RELATION_STATUS_DESC'] : "",
                    _data['custFamily'][i]['FULL_NAME_ID'] != null ? _data['custFamily'][i]['FULL_NAME_ID'] : "",
                    _data['custFamily'][i]['FULL_NAME'] != null ? _data['custFamily'][i]['FULL_NAME'] : "",
                    _data['custFamily'][i]['ID_NO'] != null ? _data['custFamily'][i]['ID_NO'] : "",
                    _data['custFamily'][i]['DATE_OF_BIRTH'] != null ? _data['custFamily'][i]['DATE_OF_BIRTH'] : null,
                    _data['custFamily'][i]['PLACE_OF_BIRTH'] != null ? _data['custFamily'][i]['PLACE_OF_BIRTH'] : "",
                    _data['custFamily'][i]['PLACE_OF_BIRTH_KABKOTA'] != null ? _data['custFamily'][i]['PLACE_OF_BIRTH_KABKOTA'] : "",
                    _data['custFamily'][i]['PLACE_OF_BIRTH_KABKOTA_DESC'] != null ? _data['custFamily'][i]['PLACE_OF_BIRTH_KABKOTA_DESC'] : "",
                    _data['custFamily'][i]['GENDER'] != null ? _data['custFamily'][i]['GENDER'] : "",
                    _data['custFamily'][i]['GENDER_DESC'] != null ? _data['custFamily'][i]['GENDER_DESC'] : "",
                    null,
                    _data['custFamily'][i]['ID_TYPE'] != null ? _data['custFamily'][i]['ID_TYPE'] : "",
                    _data['custFamily'][i]['ID_DESC'] != null ? _data['custFamily'][i]['ID_DESC'] : "",
                    null,
                    null,
                    null,
                    null,
                    1,
                    null,
                    _data['custFamily'][i]['PHONE'] != null ? _data['custFamily'][i]['PHONE'] : "",
                    _data['custFamily'][i]['PHONE_AREA'] != null ? _data['custFamily'][i]['PHONE_AREA'] : "",
                    _data['custFamily'][i]['HANDPHONE_NO'] != null  && _data['custFamily'][i]['HANDPHONE_NO'].toString().length > 3 ? _data['custFamily'][i]['HANDPHONE_NO'].toString().replaceRange(0, 2, "") : "",
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                ));
              }
            }
          }
          else{
            List _dataDakor = [];
            List _dataDakorIbu = await _dbHelper.selectDataInfoKeluarga("05");
            List _dataDakorFamily = await _dbHelper.selectDataInfoKeluarga("");
            for(int i=0; i < _dataDakorIbu.length; i++){
              _dataDakor.add(_dataDakorIbu[i]);
            }

            for(int i=0; i < _dataDakorFamily.length; i++){
              _dataDakor.add(_dataDakorFamily[i]);
            }
            if(_preferences.getString("last_known_state") == "DKR" && _dataDakor.isNotEmpty){
              print("data family tidak kosong DKR");
              if(await _dbHelper.deleteMS2CustFamily()){
                for(int i = 0; i < _data['custFamily'].length; i++){
                  for(int j=0; j < _dataDakor.length; j++){
                    if(_data['custFamily'][i]['RELATION_STATUS'] == _dataDakor[j]['relation_status']){
                      await _dbHelper.insertMS2CustFamily(MS2CustFamilyModel(
                          null,
                          _data['custFamily'][i]['FAMILY_ID'],
                          _data['custFamily'][i]['RELATION_STATUS'] != null ? _data['custFamily'][i]['RELATION_STATUS'] : "",
                          _data['custFamily'][i]['RELATION_STATUS_DESC'] != null ? _data['custFamily'][i]['RELATION_STATUS_DESC'] : "",
                          _data['custFamily'][i]['FULL_NAME_ID'] != null ? _data['custFamily'][i]['FULL_NAME_ID'] : "",
                          _data['custFamily'][i]['FULL_NAME'] != null ? _data['custFamily'][i]['FULL_NAME'] : "",
                          _data['custFamily'][i]['ID_NO'] != null ? _data['custFamily'][i]['ID_NO'] : "",
                          _data['custFamily'][i]['DATE_OF_BIRTH'] != null ? _data['custFamily'][i]['DATE_OF_BIRTH'] : null,
                          _data['custFamily'][i]['PLACE_OF_BIRTH'] != null ? _data['custFamily'][i]['PLACE_OF_BIRTH'] : "",
                          _data['custFamily'][i]['PLACE_OF_BIRTH_KABKOTA'] != null ? _data['custFamily'][i]['PLACE_OF_BIRTH_KABKOTA'] : "",
                          _data['custFamily'][i]['PLACE_OF_BIRTH_KABKOTA_DESC'] != null ? _data['custFamily'][i]['PLACE_OF_BIRTH_KABKOTA_DESC'] : "",
                          _data['custFamily'][i]['GENDER'] != null ? _data['custFamily'][i]['GENDER'] : "",
                          _data['custFamily'][i]['GENDER_DESC'] != null ? _data['custFamily'][i]['GENDER_DESC'] : "",
                          null,
                          _data['custFamily'][i]['ID_TYPE'] != null ? _data['custFamily'][i]['ID_TYPE'] : "",
                          _data['custFamily'][i]['ID_DESC'] != null ? _data['custFamily'][i]['ID_DESC'] : "",
                          null,
                          null,
                          null,
                          null,
                          1,
                          null,
                          _data['custFamily'][i]['PHONE'] != null ? _data['custFamily'][i]['PHONE'] : "",
                          _data['custFamily'][i]['PHONE_AREA'] != null ? _data['custFamily'][i]['PHONE_AREA'] : "",
                          _data['custFamily'][i]['HANDPHONE_NO'] != null  && _data['custFamily'][i]['HANDPHONE_NO'].toString().length > 3 ? _data['custFamily'][i]['HANDPHONE_NO'].toString().replaceRange(0, 2, "") : "",
                          _dataDakor.isNotEmpty ? _dataDakor[j]['edit_relation_status'] : "0",
                          _dataDakor.isNotEmpty ? _dataDakor[j]['edit_full_name_id'] : "0",
                          _dataDakor.isNotEmpty ? _dataDakor[j]['edit_full_name'] : "0",
                          _dataDakor.isNotEmpty ? _dataDakor[j]['edit_id_no'] : "0",
                          _dataDakor.isNotEmpty ? _dataDakor[j]['edit_date_of_birth'] : "0",
                          _dataDakor.isNotEmpty ? _dataDakor[j]['edit_place_of_birth_kabkota'] : "0",
                          _dataDakor.isNotEmpty ? _dataDakor[j]['edit_place_of_birth'] : "0",
                          _dataDakor.isNotEmpty ? _dataDakor[j]['edit_gender'] : "0",
                          _dataDakor.isNotEmpty ? _dataDakor[j]['edit_degree'] : "0",
                          _dataDakor.isNotEmpty ? _dataDakor[j]['edit_id_type'] : "0",
                          _dataDakor.isNotEmpty ? _dataDakor[j]['edit_dedup_score'] : "0",
                          _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone1'] : "0",
                          _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone1_area'] : "0",
                          _dataDakor.isNotEmpty ? _dataDakor[j]['edit_handphone_no'] : "0"
                      ));
                    }
                  }
                }
              }
            }
            else {
              if(await _dbHelper.deleteMS2CustFamily()){
                for(int i = 0; i < _data['custFamily'].length; i++){
                  await _dbHelper.insertMS2CustFamily(MS2CustFamilyModel(
                      null,
                      _data['custFamily'][i]['FAMILY_ID'],
                      _data['custFamily'][i]['RELATION_STATUS'] != null ? _data['custFamily'][i]['RELATION_STATUS'] : "",
                      _data['custFamily'][i]['RELATION_STATUS_DESC'] != null ? _data['custFamily'][i]['RELATION_STATUS_DESC'] : "",
                      _data['custFamily'][i]['FULL_NAME_ID'] != null ? _data['custFamily'][i]['FULL_NAME_ID'] : "",
                      _data['custFamily'][i]['FULL_NAME'] != null ? _data['custFamily'][i]['FULL_NAME'] : "",
                      _data['custFamily'][i]['ID_NO'] != null ? _data['custFamily'][i]['ID_NO'] : "",
                      _data['custFamily'][i]['DATE_OF_BIRTH'] != null ? _data['custFamily'][i]['DATE_OF_BIRTH'] : null,
                      _data['custFamily'][i]['PLACE_OF_BIRTH'] != null ? _data['custFamily'][i]['PLACE_OF_BIRTH'] : "",
                      _data['custFamily'][i]['PLACE_OF_BIRTH_KABKOTA'] != null ? _data['custFamily'][i]['PLACE_OF_BIRTH_KABKOTA'] : "",
                      _data['custFamily'][i]['PLACE_OF_BIRTH_KABKOTA_DESC'] != null ? _data['custFamily'][i]['PLACE_OF_BIRTH_KABKOTA_DESC'] : "",
                      _data['custFamily'][i]['GENDER'] != null ? _data['custFamily'][i]['GENDER'] : "",
                      _data['custFamily'][i]['GENDER_DESC'] != null ? _data['custFamily'][i]['GENDER_DESC'] : "",
                      null,
                      _data['custFamily'][i]['ID_TYPE'] != null ? _data['custFamily'][i]['ID_TYPE'] : "",
                      _data['custFamily'][i]['ID_DESC'] != null ? _data['custFamily'][i]['ID_DESC'] : "",
                      null,
                      null,
                      null,
                      null,
                      1,
                      null,
                      _data['custFamily'][i]['PHONE'] != null ? _data['custFamily'][i]['PHONE'] : "",
                      _data['custFamily'][i]['PHONE_AREA'] != null ? _data['custFamily'][i]['PHONE_AREA'] : "",
                      _data['custFamily'][i]['HANDPHONE_NO'] != null  && _data['custFamily'][i]['HANDPHONE_NO'].toString().length > 3 ? _data['custFamily'][i]['HANDPHONE_NO'].toString().replaceRange(0, 2, "") : "",
                      "0",
                      "0",
                      "0",
                      "0",
                      "0",
                      "0",
                      "0",
                      "0",
                      "0",
                      "0",
                      "0",
                      "0",
                      "0",
                      "0"
                  ));
                }
              }
            }
          }
        }

        List<MS2CustAddrModel> _listAddressCust = [];
        if(_data['custAddress'].isNotEmpty){
          List _dataDakor = [];
          _dataDakor = await _dbHelper.selectMS2CustAddr("5");
          if(_preferences.getString("last_known_state") == "DKR" && _dataDakor.isNotEmpty){
            debugPrint("cek dakorrrrr brada");
            if(await _dbHelper.deleteMS2CustAddr("5")){
              for(int i = 0; i < _data['custAddress'].length; i++){
                for(int j = 0; j < _dataDakor.length; j++){
                  debugPrint("cek dakorrrrr ${_dataDakor[j]['edit_addr_type']}");
                  if(_data['custAddress'][i]['ADDR_TYPE'] == _dataDakor[j]['addr_type']){
                    _listAddressCust.add(MS2CustAddrModel(
                        "123",
                        _data['custAddress'][i]['ADDRESS_ID'],
                        _data['custAddress'][i]['FOREIGN_BK'], //perlu adjust
                        _data['custAddress'][i]['ID_NO'], //tambahan id nomor
                        _data['custAddress'][i]['KORESPONDEN'], // == "1" ? "true" : "false",
                        "5",
                        _data['custAddress'][i]['ADDRESS'] != null ? _data['custAddress'][i]['ADDRESS'] : "",
                        _data['custAddress'][i]['RT'] != null ? _data['custAddress'][i]['RT'] : "",
                        null,
                        _data['custAddress'][i]['RW'] != null ? _data['custAddress'][i]['RW'] : "",
                        null,
                        _data['custAddress'][i]['PROVINSI'] != null ? _data['custAddress'][i]['PROVINSI'] : "",
                        _data['custAddress'][i]['PROVINSI_DESC'] != null ? _data['custAddress'][i]['PROVINSI_DESC'].toString().replaceAll("'", "`") : "",
                        _data['custAddress'][i]['KABKOT'] != null ? _data['custAddress'][i]['KABKOT'] : "",
                        _data['custAddress'][i]['KABKOT_DESC'] != null ? _data['custAddress'][i]['KABKOT_DESC'].toString().replaceAll("'", "`") : "",
                        _data['custAddress'][i]['KECAMATAN'] != null ? _data['custAddress'][i]['KECAMATAN'] : "",
                        _data['custAddress'][i]['KECAMATAN_DESC'] != null ? _data['custAddress'][i]['KECAMATAN_DESC'].toString().replaceAll("'", "`") : "",
                        _data['custAddress'][i]['KELURAHAN'] != null ? _data['custAddress'][i]['KELURAHAN'] : "",
                        _data['custAddress'][i]['KELURAHAN_DESC'] != null ? _data['custAddress'][i]['KELURAHAN_DESC'].toString().replaceAll("'", "`") : "",
                        _data['custAddress'][i]['ZIP_CODE'] != null ? _data['custAddress'][i]['ZIP_CODE'] : "",
                        null,
                        _data['custAddress'][i]['PHONE'] != null ? _data['custAddress'][i]['PHONE'] : "",
                        _data['custAddress'][i]['PHONE_AREA'] != null ? _data['custAddress'][i]['PHONE_AREA'] : "",
                        _data['custAddress'][i]['PHONE_DUA'] != null ? _data['custAddress'][i]['PHONE_DUA'] : "",
                        _data['custAddress'][i]['PHONE_AREA_DUA'] != null ? _data['custAddress'][i]['PHONE_AREA_DUA'] : "",
                        _data['custAddress'][i]['FAX'] != null ? _data['custAddress'][i]['FAX'] : "",
                        _data['custAddress'][i]['FAX_AREA'] != null ? _data['custAddress'][i]['FAX_AREA'] : "",
                        _data['custAddress'][i]['ADDR_TYPE'] != null ? _data['custAddress'][i]['ADDR_TYPE'] : "",
                        _data['custAddress'][i]['ADDR_DESC'] != null ? _data['custAddress'][i]['ADDR_DESC'] : "",
                        _data['custAddress'][i]['LATITUDE'] != null ? _data['custAddress'][i]['LATITUDE'] : "",
                        _data['custAddress'][i]['LONGITUDE'] != null ? _data['custAddress'][i]['LONGITUDE'] : "",
                        _data['custAddress'][i]['ADDRESS_FROM_MAP'] != null ? _data['custAddress'][i]['ADDRESS_FROM_MAP'] : "",
                        null,
                        null,
                        null,
                        null,
                        0,
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_addr_type'] : '0',//
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_address'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_rt'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_rw'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_kelurahan'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_kecamatan'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_kabkot'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_provinsi'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_zip_code'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone1_area'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone1'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone_2_area'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone_2'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_fax_area'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_fax'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_address_from_map'] : '0'));
                  }
                }
              }
              await _dbHelper.insertMS2CustAddr(_listAddressCust);
            }
          }
          else {
            if(await _dbHelper.deleteMS2CustAddr("5")){
              for(int i = 0; i < _data['custAddress'].length; i++){
                _listAddressCust.add(MS2CustAddrModel(
                    "123",
                    _data['custAddress'][i]['ADDRESS_ID'],
                    _data['custAddress'][i]['FOREIGN_BK'], //perlu adjust
                    _data['custAddress'][i]['ID_NO'], //tambahan id nomor
                    _data['custAddress'][i]['KORESPONDEN'], // == "1" ? "true" : "false",
                    "5",
                    _data['custAddress'][i]['ADDRESS'] != null ? _data['custAddress'][i]['ADDRESS'] : "",
                    _data['custAddress'][i]['RT'] != null ? _data['custAddress'][i]['RT'] : "",
                    null,
                    _data['custAddress'][i]['RW'] != null ? _data['custAddress'][i]['RW'] : "",
                    null,
                    _data['custAddress'][i]['PROVINSI'] != null ? _data['custAddress'][i]['PROVINSI'] : "",
                    _data['custAddress'][i]['PROVINSI_DESC'] != null ? _data['custAddress'][i]['PROVINSI_DESC'].toString().replaceAll("'", "`") : "",
                    _data['custAddress'][i]['KABKOT'] != null ? _data['custAddress'][i]['KABKOT'] : "",
                    _data['custAddress'][i]['KABKOT_DESC'] != null ? _data['custAddress'][i]['KABKOT_DESC'].toString().replaceAll("'", "`") : "",
                    _data['custAddress'][i]['KECAMATAN'] != null ? _data['custAddress'][i]['KECAMATAN'] : "",
                    _data['custAddress'][i]['KECAMATAN_DESC'] != null ? _data['custAddress'][i]['KECAMATAN_DESC'].toString().replaceAll("'", "`") : "",
                    _data['custAddress'][i]['KELURAHAN'] != null ? _data['custAddress'][i]['KELURAHAN'] : "",
                    _data['custAddress'][i]['KELURAHAN_DESC'] != null ? _data['custAddress'][i]['KELURAHAN_DESC'].toString().replaceAll("'", "`") : "",
                    _data['custAddress'][i]['ZIP_CODE'] != null ? _data['custAddress'][i]['ZIP_CODE'] : "",
                    null,
                    _data['custAddress'][i]['PHONE'] != null ? _data['custAddress'][i]['PHONE'] : "",
                    _data['custAddress'][i]['PHONE_AREA'] != null ? _data['custAddress'][i]['PHONE_AREA'] : "",
                    _data['custAddress'][i]['PHONE_DUA'] != null ? _data['custAddress'][i]['PHONE_DUA'] : "",
                    _data['custAddress'][i]['PHONE_AREA_DUA'] != null ? _data['custAddress'][i]['PHONE_AREA_DUA'] : "",
                    _data['custAddress'][i]['FAX'] != null ? _data['custAddress'][i]['FAX'] : "",
                    _data['custAddress'][i]['FAX_AREA'] != null ? _data['custAddress'][i]['FAX_AREA'] : "",
                    _data['custAddress'][i]['ADDR_TYPE'] != null ? _data['custAddress'][i]['ADDR_TYPE'] : "",
                    _data['custAddress'][i]['ADDR_DESC'] != null ? _data['custAddress'][i]['ADDR_DESC'] : "",
                    _data['custAddress'][i]['LATITUDE'] != null ? _data['custAddress'][i]['LATITUDE'] : "",
                    _data['custAddress'][i]['LONGITUDE'] != null ? _data['custAddress'][i]['LONGITUDE'] : "",
                    _data['custAddress'][i]['ADDRESS_FROM_MAP'] != null ? _data['custAddress'][i]['ADDRESS_FROM_MAP'] : "",
                    null,
                    null,
                    null,
                    null,
                    0,
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0'));
              }
              await _dbHelper.insertMS2CustAddr(_listAddressCust);
            }
          }
        }

        //pekerjaan
        if(_data['custOccupation'].isNotEmpty){
          List _dataDakor = [];
          if(_preferences.getString("last_known_state") == "DKR"){
            _dataDakor = await _dbHelper.selectMS2CustIndOccupation();
          }
          print("cek if custOccupation");
          if(await _dbHelper.deleteMS2CustIndOccupation()){
            print("cek total lama usaha ${_data['custOccupation'][0]['NO_OF_YEAR_WORK']}");
            print("sector economic : ${_data['custOccupation'][0]['SECTOR_ECONOMIC']}");
            print("sector economic : ${_data['custOccupation'][0]['SECTOR_ECONOMIC_DESC']}");
            print("CUST_OCCUPATION_ID : ${_data['custOccupation'][0]['CUST_OCCUPATION_ID']}");
            await _dbHelper.insertMS2CustIndOccupation(MS2CustIndOccupationModel(
                "123",
                _data['custOccupation'][0]['CUST_OCCUPATION_ID'],
                _data['custOccupation'][0]['OCCUPATION'] != null ? _data['custOccupation'][0]['OCCUPATION'].toString() : "",
                _data['custOccupation'][0]['OCCUPATION_DESC'] != null ? _data['custOccupation'][0]['OCCUPATION_DESC'].toString() : "",
                _data['custOccupation'][0]['BUSS_NAME'] != null ? _data['custOccupation'][0]['BUSS_NAME'].toString() : "",
                _data['custOccupation'][0]['BUSS_TYPE'] != null ? _data['custOccupation'][0]['BUSS_TYPE'].toString() : "",
                _data['custOccupation'][0]['PEP_TYPE'] != null ? _data['custOccupation'][0]['PEP_TYPE'].toString() : "",
                _data['custOccupation'][0]['PEP_DESC'] != null ? _data['custOccupation'][0]['PEP_DESC'].toString() : "",
                _data['custOccupation'][0]['EMP_STATUS'] != null ? _data['custOccupation'][0]['EMP_STATUS'].toString() : "",
                _data['custOccupation'][0]['EMP_STATUS_DESC'] != null ? _data['custOccupation'][0]['EMP_STATUS_DESC'].toString() : "",
                _data['custOccupation'][0]['PROFESSION_TYPE'] != null ? _data['custOccupation'][0]['PROFESSION_TYPE'].toString() : "",
                _data['custOccupation'][0]['PROFESSION_DESC'] != null ? _data['custOccupation'][0]['PROFESSION_DESC'].toString() : "",
                _data['custOccupation'][0]['SECTOR_ECONOMIC'] != null ? _data['custOccupation'][0]['SECTOR_ECONOMIC'].toString() : "",
                _data['custOccupation'][0]['SECTOR_ECONOMIC_DESC'] != null ? _data['custOccupation'][0]['SECTOR_ECONOMIC_DESC'].toString() : "",
                _data['custOccupation'][0]['NATURE_OF_BUSS'] != null ? _data['custOccupation'][0]['NATURE_OF_BUSS'].toString() : "",
                _data['custOccupation'][0]['NATURE_OF_BUSS_DESC'] != null ? _data['custOccupation'][0]['NATURE_OF_BUSS_DESC'].toString() : "",
                _data['custOccupation'][0]['LOCATION_STATUS'] != null ? _data['custOccupation'][0]['LOCATION_STATUS'].toString() : "",
                _data['custOccupation'][0]['LOCATION_STATUS_DESC'] != null ? _data['custOccupation'][0]['LOCATION_STATUS_DESC'].toString() : "",
                _data['custOccupation'][0]['BUSS_LOCATION'] != null ? _data['custOccupation'][0]['BUSS_LOCATION'].toString() : "",
                _data['custOccupation'][0]['BUSS_LOCATION_DESC'] != null ? _data['custOccupation'][0]['BUSS_LOCATION_DESC'].toString() : "",
                _data['custOccupation'][0]['TOTAL_EMP'] != null ? _data['custOccupation'][0]['TOTAL_EMP'] : null,
                _data['custOccupation'][0]['NO_OF_YEAR_WORK'] != null ? _data['custOccupation'][0]['NO_OF_YEAR_WORK'] : null,
                _data['custOccupation'][0]['NO_OF_YEAR_BUSS'] != null ? _data['custOccupation'][0]['NO_OF_YEAR_BUSS'] : null,
                null,
                null,
                null,
                null,
                1,
                null,
                null,
                null,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_occupation'] : "0",
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_buss_name'] : "0",
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_buss_type'] : "0",
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_pep_type'] : "0",
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_emp_status'] : "0",
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_profession_type'] : "0",
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_sector_economic'] : "0",
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_nature_of_buss'] : "0",
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_location_status'] : "0",
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_buss_location'] : "0",
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_total_emp'] : "0",
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_no_of_year_work'] : "0",
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_no_of_year_buss'] : "0",
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_siup_no'] : "0",
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_scheme_kur'] : "0",
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_modal_usaha'] : "0",
            ));
          }
        }

        //alamat pekerjaan
        List<MS2CustAddrModel> _listAddressOccupation = [];
        if(_data['custOccupationAddr'].isNotEmpty){
          List _dataDakor = [];
          _dataDakor = await _dbHelper.selectMS2CustAddr("4");
          if(_preferences.getString("last_known_state") == "DKR" && _dataDakor.isNotEmpty){
            if(await _dbHelper.deleteMS2CustAddr("4")){
              for (int i = 0; i < _data['custOccupationAddr'].length; i++) {
                for(int j=0; j < _dataDakor.length; j++){
                  if(_data['custOccupationAddr'][i]['ADDR_TYPE'] == _dataDakor[j]['addr_type']){
                    _listAddressOccupation.add(MS2CustAddrModel(
                        "123",
                        _data['custOccupationAddr'][i]['ADDRESS_ID'],
                        _data['custOccupationAddr'][i]['FOREIGN_BK'], //perlu adjust
                        _data['custOccupationAddr'][i]['ID_NO'], //tambahan id nomor
                        _data['custOccupationAddr'][i]['KORESPONDEN'], //== "1" ? "true" : "false",
                        "4",
                        _data['custOccupationAddr'][i]['ADDRESS'],
                        _data['custOccupationAddr'][i]['RT'],
                        null,
                        _data['custOccupationAddr'][i]['RW'],
                        null,
                        _data['custOccupationAddr'][i]['PROVINSI'] != null ? _data['custOccupationAddr'][i]['PROVINSI'] : "",
                        _data['custOccupationAddr'][i]['PROVINSI_DESC'] != null ? _data['custOccupationAddr'][i]['PROVINSI_DESC'].toString().replaceAll("'", "`") : "",
                        _data['custOccupationAddr'][i]['KABKOT'] != null ? _data['custOccupationAddr'][i]['KABKOT'] : "",
                        _data['custOccupationAddr'][i]['KABKOT_DESC'] != null ? _data['custOccupationAddr'][i]['KABKOT_DESC'].toString().replaceAll("'", "`") : "",
                        _data['custOccupationAddr'][i]['KECAMATAN'] != null ? _data['custOccupationAddr'][i]['KECAMATAN'] : "",
                        _data['custOccupationAddr'][i]['KECAMATAN_DESC'] != null ? _data['custOccupationAddr'][i]['KECAMATAN_DESC'].toString().replaceAll("'", "`") : "",
                        _data['custOccupationAddr'][i]['KELURAHAN'] != null ? _data['custOccupationAddr'][i]['KELURAHAN'] : "",
                        _data['custOccupationAddr'][i]['KELURAHAN_DESC'] != null ? _data['custOccupationAddr'][i]['KELURAHAN_DESC'].toString().replaceAll("'", "`") : "",
                        _data['custOccupationAddr'][i]['ZIP_CODE'] != null ? _data['custOccupationAddr'][i]['ZIP_CODE'] : "",
                        null,
                        _data['custOccupationAddr'][i]['PHONE'] != null ? _data['custOccupationAddr'][i]['PHONE'] : "",
                        _data['custOccupationAddr'][i]['PHONE_AREA'] != null ? _data['custOccupationAddr'][i]['PHONE_AREA'] : "",
                        _data['custOccupationAddr'][i]['PHONE_DUA'] != null ? _data['custOccupationAddr'][i]['PHONE_DUA'] : "",
                        _data['custOccupationAddr'][i]['PHONE_AREA_DUA'] != null ? _data['custOccupationAddr'][i]['PHONE_AREA_DUA'] : "",
                        _data['custOccupationAddr'][i]['FAX'] != null ? _data['custOccupationAddr'][i]['FAX'] : "",
                        _data['custOccupationAddr'][i]['FAX_AREA'] != null ? _data['custOccupationAddr'][i]['FAX_AREA'] : "",
                        _data['custOccupationAddr'][i]['ADDR_TYPE'] != null ? _data['custOccupationAddr'][i]['ADDR_TYPE'] : "",
                        _data['custOccupationAddr'][i]['ADDR_DESC'] != null ? _data['custOccupationAddr'][i]['ADDR_DESC'] : "",
                        _data['custOccupationAddr'][i]['LATITUDE'] != null ? _data['custOccupationAddr'][i]['LATITUDE'] : "",
                        _data['custOccupationAddr'][i]['LONGITUDE'] != null ? _data['custOccupationAddr'][i]['LONGITUDE'] : "",
                        _data['custOccupationAddr'][i]['ADDRESS_FROM_MAP'] != null ? _data['custOccupationAddr'][i]['ADDRESS_FROM_MAP'] : "",
                        null,
                        null,
                        null,
                        null,
                        0,
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_addr_type'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_address'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_rt'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_rw'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_kelurahan'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_kecamatan'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_kabkot'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_provinsi'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_zip_code'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone1_area'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone1'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone_2_area'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone_2'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_fax_area'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_fax'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_address_from_map'] : '0'));
                  }
                }
              }
              await _dbHelper.insertMS2CustAddr(_listAddressOccupation);
            }
          } else {
            if(await _dbHelper.deleteMS2CustAddr("4")){
              for (int i = 0; i < _data['custOccupationAddr'].length; i++) {
                _listAddressOccupation.add(MS2CustAddrModel(
                    "123",
                    _data['custOccupationAddr'][i]['ADDRESS_ID'],
                    _data['custOccupationAddr'][i]['FOREIGN_BK'], //perlu adjust
                    _data['custOccupationAddr'][i]['ID_NO'], //tambahan id nomor
                    _data['custOccupationAddr'][i]['KORESPONDEN'], //== "1" ? "true" : "false",
                    "4",
                    _data['custOccupationAddr'][i]['ADDRESS'],
                    _data['custOccupationAddr'][i]['RT'],
                    null,
                    _data['custOccupationAddr'][i]['RW'],
                    null,
                    _data['custOccupationAddr'][i]['PROVINSI'] != null ? _data['custOccupationAddr'][i]['PROVINSI'] : "",
                    _data['custOccupationAddr'][i]['PROVINSI_DESC'] != null ? _data['custOccupationAddr'][i]['PROVINSI_DESC'].toString().replaceAll("'", "`") : "",
                    _data['custOccupationAddr'][i]['KABKOT'] != null ? _data['custOccupationAddr'][i]['KABKOT'] : "",
                    _data['custOccupationAddr'][i]['KABKOT_DESC'] != null ? _data['custOccupationAddr'][i]['KABKOT_DESC'].toString().replaceAll("'", "`") : "",
                    _data['custOccupationAddr'][i]['KECAMATAN'] != null ? _data['custOccupationAddr'][i]['KECAMATAN'] : "",
                    _data['custOccupationAddr'][i]['KECAMATAN_DESC'] != null ? _data['custOccupationAddr'][i]['KECAMATAN_DESC'].toString().replaceAll("'", "`") : "",
                    _data['custOccupationAddr'][i]['KELURAHAN'] != null ? _data['custOccupationAddr'][i]['KELURAHAN'] : "",
                    _data['custOccupationAddr'][i]['KELURAHAN_DESC'] != null ? _data['custOccupationAddr'][i]['KELURAHAN_DESC'].toString().replaceAll("'", "`") : "",
                    _data['custOccupationAddr'][i]['ZIP_CODE'] != null ? _data['custOccupationAddr'][i]['ZIP_CODE'] : "",
                    null,
                    _data['custOccupationAddr'][i]['PHONE'] != null ? _data['custOccupationAddr'][i]['PHONE'] : "",
                    _data['custOccupationAddr'][i]['PHONE_AREA'] != null ? _data['custOccupationAddr'][i]['PHONE_AREA'] : "",
                    _data['custOccupationAddr'][i]['PHONE_DUA'] != null ? _data['custOccupationAddr'][i]['PHONE_DUA'] : "",
                    _data['custOccupationAddr'][i]['PHONE_AREA_DUA'] != null ? _data['custOccupationAddr'][i]['PHONE_AREA_DUA'] : "",
                    _data['custOccupationAddr'][i]['FAX'] != null ? _data['custOccupationAddr'][i]['FAX'] : "",
                    _data['custOccupationAddr'][i]['FAX_AREA'] != null ? _data['custOccupationAddr'][i]['FAX_AREA'] : "",
                    _data['custOccupationAddr'][i]['ADDR_TYPE'] != null ? _data['custOccupationAddr'][i]['ADDR_TYPE'] : "",
                    _data['custOccupationAddr'][i]['ADDR_DESC'] != null ? _data['custOccupationAddr'][i]['ADDR_DESC'] : "",
                    _data['custOccupationAddr'][i]['LATITUDE'] != null ? _data['custOccupationAddr'][i]['LATITUDE'] : "",
                    _data['custOccupationAddr'][i]['LONGITUDE'] != null ? _data['custOccupationAddr'][i]['LONGITUDE'] : "",
                    _data['custOccupationAddr'][i]['ADDRESS_FROM_MAP'] != null ? _data['custOccupationAddr'][i]['ADDRESS_FROM_MAP'] : "",
                    null,
                    null,
                    null,
                    null,
                    0,
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0'));
              }
              await _dbHelper.insertMS2CustAddr(_listAddressOccupation);
            }
          }
        }

        if(_data['custDedup'].isNotEmpty){
          print("dedup cust info ${_data['custDedup']}");
          if(await _dbHelper.deleteMS2Dedup()){
            await _dbHelper.insertDataDedup(DataDedupModel(
                custType,
                _data['custDedup'][0]['ID_NO'],
                _data['custDedup'][0]['CUST_NAME'],
                _data['custDedup'][0]['DATE_BIRTH'],
                _data['custDedup'][0]['PLACE_BIRTH'],
                _data['custDedup'][0]['MOTHER_NAME'],
                _data['custDedup'][0]['ADDRESS'],
                null,
                null,
                null,
                null,
                null
              ),
              _data['custDedup'][0]['ORDER_NO']
            );
          }
        }
      }
      else{
        // info nasabah lembaga
        if(_preferences.getString("status_aoro") != "0" && _preferences.getString("last_known_state") == "IDE"){
          if(_data['custDetailInfoCompany'].isNotEmpty){
            if(await _dbHelper.deleteMS2CustomerCompany()){
              await _dbHelper.insertMS2CustomerCompany(MS2CustomerCompanyModel(
                null,
                _data['custDetailInfoCompany'][0]['CUST_COMP_ID'],
                TypeInstitutionModel(_data['custDetailInfoCompany'][0]['COMP_TYPE'], _data['custDetailInfoCompany'][0]['COMP_DESC']),
                ProfilModel(_data['custDetailInfoCompany'][0]['PROFIL'], _data['custDetailInfoCompany'][0]['PROFIL_DESC']),
                _data['custDedup'][0]['COMP_NAME'],//_data['custDetailInfoCompany'][0]['COMP_NAME'],
                "",
                "",
                "",
                "",
                "",
                "",
                _data['custDedup'][0]['EST_DATE'],//_data['custDetailInfoCompany'][0]['ESTABLISH_DATE'] != null ? _data['custDetailInfoCompany'][0]['ESTABLISH_DATE'] : "",
                "",
                "",
                "",
                SectorEconomicModel(_data['custDetailInfoCompany'][0]['SECTOR_ECONOMIC'], _data['custDetailInfoCompany'][0]['SECTOR_ECONOMIC_DESC']),
                BusinessFieldModel(_data['custDetailInfoCompany'][0]['NATURE_OF_BUSS'], _data['custDetailInfoCompany'][0]['NATURE_OF_BUSS_DESC']),
                StatusLocationModel(_data['custDetailInfoCompany'][0]['LOCATION_STATUS'], _data['custDetailInfoCompany'][0]['LOCATION_STATUS_DESC']),
                BusinessLocationModel(_data['custDetailInfoCompany'][0]['BUSS_LOCATION'], _data['custDetailInfoCompany'][0]['BUSS_LOCATION_DESC']),//null,
                _data['custDetailInfoCompany'][0]['TOTAL_EMP'] != null ? _data['custDetailInfoCompany'][0]['TOTAL_EMP'] : "",
                _data['custDetailInfoCompany'][0]['NO_OF_YEAR_WORK'] != null ? _data['custDetailInfoCompany'][0]['NO_OF_YEAR_WORK'] : "",
                _data['custDetailInfoCompany'][0]['NO_OF_YEAR_BUSS'] != null ? _data['custDetailInfoCompany'][0]['NO_OF_YEAR_BUSS'] : "",
                null,
                "",
                null,
                _data['custDetailInfoCompany'][0]['NPWP_NO'] != null ? _data['custDetailInfoCompany'][0]['NPWP_NO'].replaceAll(".", "").replaceAll("-", "") : "",
                "",
                "",
                "",
                "",
                "",
                "",
                null,
                "",
                "",
                "",
                "",
                null,
                null,
                "",
                "",
                "",
                null,
                null,
                "",
                "",
                "",
                "",
                null,
                null,
                false,//_data['custDetailInfoCompany'][0]['edit_comp_type'] == '1',
                false,//_data['custDetailInfoCompany'][0]['edit_profil'] == '1',
                false,//_data['custDetailInfoCompany'][0]['edit_comp_name'] == '1',
                false,//_data['custDetailInfoCompany'][0]['edit_establish_date'] == '1',
                false,//_data['custDetailInfoCompany'][0]['edit_sector_economic'] == '1',
                false,//_data['custDetailInfoCompany'][0]['edit_nature_of_buss'] == '1',
                false,//_data['custDetailInfoCompany'][0]['edit_location_status'] == '1',
                false,//_data['custDetailInfoCompany'][0]['edit_buss_location'] == '1',
                false,//_data['custDetailInfoCompany'][0]['edit_total_emp'] == '1',
                false,//_data['custDetailInfoCompany'][0]['edit_no_of_year_work'] == '1',
                false,//_data['custDetailInfoCompany'][0]['edit_no_of_year_buss'] == '1',
              ));
            }
          }
          else{
            print("di else custDetailInfoPersonal");
            print(_data['custDedup']);
            await _dbHelper.insertMS2CustomerCompany(MS2CustomerCompanyModel(
                null,
                null,
                null,//TypeInstitutionModel(_data['custDetailInfoCompany'][0]['COMP_TYPE'], _data['custDetailInfoCompany'][0]['COMP_DESC']),
                null,//ProfilModel(_data['custDetailInfoCompany'][0]['PROFIL'], _data['custDetailInfoCompany'][0]['PROFIL_DESC']),
                _data['custDedup'][0]['COMP_NAME'],
                "",
                "",
                "",
                "",
                "",
                "",
                _data['custDedup'][0]['EST_DATE'] != null ? _data['custDedup'][0]['EST_DATE'] : "",
                "",
                "",
                "",
                null,//SectorEconomicModel(_data['custDetailInfoCompany'][0]['SECTOR_ECONOMIC'], _data['custDetailInfoCompany'][0]['SECTOR_ECONOMIC_DESC']),
                null,//BusinessFieldModel(_data['custDetailInfoCompany'][0]['NATURE_OF_BUSS'], _data['custDetailInfoCompany'][0]['NATURE_OF_BUSS_DESC']),
                null,//StatusLocationModel(_data['custDetailInfoCompany'][0]['LOCATION_STATUS'], _data['custDetailInfoCompany'][0]['LOCATION_STATUS_DESC']),
                null,
                null,//_data['custDetailInfoCompany'][0]['TOTAL_EMP'] != null ? _data['custDetailInfoCompany'][0]['TOTAL_EMP'] : "",
                null,//_data['custDetailInfoCompany'][0]['NO_OF_YEAR_WORK'] != null ? _data['custDetailInfoCompany'][0]['NO_OF_YEAR_WORK'] : "",
                null,//_data['custDetailInfoCompany'][0]['NO_OF_YEAR_BUSS'] != null ? _data['custDetailInfoCompany'][0]['NO_OF_YEAR_BUSS'] : "",
                null,
                "",
                null,
                null,//_data['custDedup'][0]['ID_NO'] != null ? _data['custDedup'][0]['ID_NO'] : "",
                "",
                "",
                "",
                "",
                "",
                "",
                null,
                "",
                "",
                "",
                "",
                null,
                null,
                "",
                "",
                "",
                null,
                null,
                "",
                "",
                "",
                "",
                null,
                null,
                null,//_data['custDetailInfoCompany'][0]['edit_comp_type'] == '1',
                null,//_data['custDetailInfoCompany'][0]['edit_profil'] == '1',
                null,//_data['custDetailInfoCompany'][0]['edit_comp_name'] == '1',
                null,//_data['custDetailInfoCompany'][0]['edit_establish_date'] == '1',
                null,//_data['custDetailInfoCompany'][0]['edit_sector_economic'] == '1',
                null,//_data['custDetailInfoCompany'][0]['edit_nature_of_buss'] == '1',
                null,//_data['custDetailInfoCompany'][0]['edit_location_status'] == '1',
                null,//_data['custDetailInfoCompany'][0]['edit_buss_location'] == '1',
                null,//_data['custDetailInfoCompany'][0]['edit_total_emp'] == '1',
                null,//_data['custDetailInfoCompany'][0]['edit_no_of_year_work'] == '1',
                null
            ));//_data['custDetailInfoCompany'][0]['edit_no_of_year_buss'] == '1',
          }
        }
        else{
          if(_data['custDetailInfoCompany'].isNotEmpty){
            List _dataDakor = [];
            if(_preferences.getString("last_known_state") == "DKR"){
              _dataDakor = await _dbHelper.selectMS2CustomerCompany();
            }
            print("di if custDetailInfoCompany");
            debugPrint("CEK_SECTOR_ECONOMI ${_data['custDetailInfoCompany'][0]['SECTOR_ECONOMIC']}");
            if(await _dbHelper.deleteMS2CustomerCompany()){
              await _dbHelper.insertMS2CustomerCompany(MS2CustomerCompanyModel(
                null,
                _data['custDetailInfoCompany'][0]['CUST_COMP_ID'],
                TypeInstitutionModel(_data['custDetailInfoCompany'][0]['COMP_TYPE'], _data['custDetailInfoCompany'][0]['COMP_DESC']),
                ProfilModel(_data['custDetailInfoCompany'][0]['PROFIL'], _data['custDetailInfoCompany'][0]['PROFIL_DESC']),
                _data['custDetailInfoCompany'][0]['COMP_NAME'],
                "",
                "",
                "",
                "",
                "",
                "",
                _data['custDetailInfoCompany'][0]['ESTABLISH_DATE'] != null ? _data['custDetailInfoCompany'][0]['ESTABLISH_DATE'] : "",
                "",
                "",
                "",
                SectorEconomicModel(_data['custDetailInfoCompany'][0]['SECTOR_ECONOMIC'], _data['custDetailInfoCompany'][0]['SECTOR_ECONOMIC_DESC']),
                BusinessFieldModel(_data['custDetailInfoCompany'][0]['NATURE_OF_BUSS'], _data['custDetailInfoCompany'][0]['NATURE_OF_BUSS_DESC']),
                StatusLocationModel(_data['custDetailInfoCompany'][0]['LOCATION_STATUS'], _data['custDetailInfoCompany'][0]['LOCATION_STATUS_DESC']),
                BusinessLocationModel(_data['custDetailInfoCompany'][0]['BUSS_LOCATION'], _data['custDetailInfoCompany'][0]['BUSS_LOCATION_DESC']),//null,
                _data['custDetailInfoCompany'][0]['TOTAL_EMP'] != null ? _data['custDetailInfoCompany'][0]['TOTAL_EMP'] : "",
                _data['custDetailInfoCompany'][0]['NO_OF_YEAR_WORK'] != null ? _data['custDetailInfoCompany'][0]['NO_OF_YEAR_WORK'] : "",
                _data['custDetailInfoCompany'][0]['NO_OF_YEAR_BUSS'] != null ? _data['custDetailInfoCompany'][0]['NO_OF_YEAR_BUSS'] : "",
                null,
                "",
                null,
                _data['custDetailInfoCompany'][0]['NPWP_NO'] != null ? _data['custDetailInfoCompany'][0]['NPWP_NO'].replaceAll(".", "").replaceAll("-", "") : "",
                "",
                "",
                "",
                "",
                "",
                "",
                null,
                "",
                "",
                "",
                "",
                null,
                null,
                "",
                "",
                "",
                null,
                null,
                "",
                "",
                "",
                "",
                null,
                null,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_comp_type'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_profil'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_comp_name'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_establish_date'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_sector_economic'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_nature_of_buss'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_location_status'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_buss_location'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_total_emp'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_no_of_year_work'] == '1' : false,
                _dataDakor.isNotEmpty ? _dataDakor[0]['edit_no_of_year_buss'] == '1' : false,
              ));
            }
          }
          else{
            print("di else custDetailInfoPersonal");
            print(_data['custDedup']);
            await _dbHelper.insertMS2CustomerCompany(MS2CustomerCompanyModel(
                null,
                null,
                null,//TypeInstitutionModel(_data['custDetailInfoCompany'][0]['COMP_TYPE'], _data['custDetailInfoCompany'][0]['COMP_DESC']),
                null,//ProfilModel(_data['custDetailInfoCompany'][0]['PROFIL'], _data['custDetailInfoCompany'][0]['PROFIL_DESC']),
                _data['custDedup'][0]['COMP_NAME'],
                "",
                "",
                "",
                "",
                "",
                "",
                _data['custDedup'][0]['EST_DATE'] != null ? _data['custDedup'][0]['EST_DATE'] : "",
                "",
                "",
                "",
                null,//SectorEconomicModel(_data['custDetailInfoCompany'][0]['SECTOR_ECONOMIC'], _data['custDetailInfoCompany'][0]['SECTOR_ECONOMIC_DESC']),
                null,//BusinessFieldModel(_data['custDetailInfoCompany'][0]['NATURE_OF_BUSS'], _data['custDetailInfoCompany'][0]['NATURE_OF_BUSS_DESC']),
                null,//StatusLocationModel(_data['custDetailInfoCompany'][0]['LOCATION_STATUS'], _data['custDetailInfoCompany'][0]['LOCATION_STATUS_DESC']),
                null,
                null,//_data['custDetailInfoCompany'][0]['TOTAL_EMP'] != null ? _data['custDetailInfoCompany'][0]['TOTAL_EMP'] : "",
                null,//_data['custDetailInfoCompany'][0]['NO_OF_YEAR_WORK'] != null ? _data['custDetailInfoCompany'][0]['NO_OF_YEAR_WORK'] : "",
                null,//_data['custDetailInfoCompany'][0]['NO_OF_YEAR_BUSS'] != null ? _data['custDetailInfoCompany'][0]['NO_OF_YEAR_BUSS'] : "",
                null,
                "",
                null,
                null,//_data['custDedup'][0]['ID_NO'] != null ? _data['custDedup'][0]['ID_NO'] : "",
                "",
                "",
                "",
                "",
                "",
                "",
                null,
                "",
                "",
                "",
                "",
                null,
                null,
                "",
                "",
                "",
                null,
                null,
                "",
                "",
                "",
                "",
                null,
                null,
                null,//_data['custDetailInfoCompany'][0]['edit_comp_type'] == '1',
                null,//_data['custDetailInfoCompany'][0]['edit_profil'] == '1',
                null,//_data['custDetailInfoCompany'][0]['edit_comp_name'] == '1',
                null,//_data['custDetailInfoCompany'][0]['edit_establish_date'] == '1',
                null,//_data['custDetailInfoCompany'][0]['edit_sector_economic'] == '1',
                null,//_data['custDetailInfoCompany'][0]['edit_nature_of_buss'] == '1',
                null,//_data['custDetailInfoCompany'][0]['edit_location_status'] == '1',
                null,//_data['custDetailInfoCompany'][0]['edit_buss_location'] == '1',
                null,//_data['custDetailInfoCompany'][0]['edit_total_emp'] == '1',
                null,//_data['custDetailInfoCompany'][0]['edit_no_of_year_work'] == '1',
                null
            ));//_data['custDetailInfoCompany'][0]['edit_no_of_year_buss'] == '1',
          }
        }

        // npwp
        if(_preferences.getString("status_aoro") != "0" && _preferences.getString("last_known_state") == "IDE"){
          if(_data['custDetailInfoCompany'].isNotEmpty){
            List _dataDakor = [];
            if(_preferences.getString("last_known_state") == "DKR"){
              _dataDakor = await _dbHelper.selectMS2Customer();
            }
            if(await _dbHelper.deleteMS2Customer()){
              await _dbHelper.insertMS2Customer(MS2CustomerModel(
                  null,
                  _data['custDetailInfoCompany'][0]['CUST_ID'],
                  _data['custDetailInfoCompany'][0]['ID_OBLIGOR'],
                  null,
                  _data['custDedup'][0]['ADDRESS'] != null ? _data['custDedup'][0]['ADDRESS'] : "",
                  _data['custDetailInfoCompany'][0]['NPWP_NAME'] != null ? _data['custDetailInfoCompany'][0]['NPWP_NAME'] : "",
                  _data['custDedup'][0]['ID_NO'] != null ? _data['custDedup'][0]['ID_NO'] : "",
                  JenisNPWPModel(_data['custDetailInfoCompany'][0]['NPWP_TYPE'], _data['custDetailInfoCompany'][0]['NPWP_TYPE_DESC']),
                  _data['custDetailInfoCompany'][0]['FLAG_NPWP'] != null ? int.parse(_data['custDetailInfoCompany'][0]['FLAG_NPWP']) : 0,
                  _data['custDetailInfoCompany'][0]['PKP_FLAG'] != null ? _data['custDetailInfoCompany'][0]['PKP_FLAG'].toString() : "",
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  1,
                  _dataDakor.isNotEmpty ? _dataDakor[0]['edit_flag_npwp'] : "0",
                  _dataDakor.isNotEmpty ? _dataDakor[0]['edit_npwp_no'] : "0",
                  _dataDakor.isNotEmpty ? _dataDakor[0]['edit_npwp_name'] : "0",
                  _dataDakor.isNotEmpty ? _dataDakor[0]['edit_npwp_type'] : "0",
                  _dataDakor.isNotEmpty ? _dataDakor[0]['edit_pkp_flag'] : "0",
                  _dataDakor.isNotEmpty ? _dataDakor[0]['edit_npwp_address'] : "0"));
            }
          }
          else{
            await _dbHelper.insertMS2Customer(MS2CustomerModel(
                null,
                null,//_data['custDetailInfoCompany'][0]['CUST_ID'],
                null,
                null,
                _data['custDedup'][0]['ADDRESS'] != null ? _data['custDedup'][0]['ADDRESS'] : "",
                null,//_data['custDetailInfoCompany'][0]['NPWP_NAME'] != null ? _data['custDetailInfoCompany'][0]['NPWP_NAME'] : "",
                _data['custDedup'][0]['ID_NO'] != null ? _data['custDedup'][0]['ID_NO'] : "",
                null,//JenisNPWPModel(_data['custDetailInfoCompany'][0]['NPWP_TYPE'], _data['custDetailInfoCompany'][0]['NPWP_TYPE_DESC']),
                null,//_data['custDetailInfoCompany'][0]['FLAG_NPWP'] != null ? int.parse(_data['custDetailInfoCompany'][0]['FLAG_NPWP']) : 0,
                null,//_data['custDetailInfoCompany'][0]['PKP_FLAG'] != null ? _data['custDetailInfoCompany'][0]['PKP_FLAG'].toString() : "",
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                1,
                null,
                null,
                null,
                null,
                null,
                null
            ));
          }
        }
        else{
          if(_data['custDetailInfoCompany'].isNotEmpty){
            if(await _dbHelper.deleteMS2Customer()){
              await _dbHelper.insertMS2Customer(MS2CustomerModel(
                  null,
                  _data['custDetailInfoCompany'][0]['CUST_ID'],
                  _data['custDetailInfoCompany'][0]['ID_OBLIGOR'],
                  null,
                  _data['custDetailInfoCompany'][0]['NPWP_ADDRESS'] != null ? _data['custDetailInfoCompany'][0]['NPWP_ADDRESS'] : "",
                  _data['custDetailInfoCompany'][0]['NPWP_NAME'] != null ? _data['custDetailInfoCompany'][0]['NPWP_NAME'] : "",
                  _data['custDetailInfoCompany'][0]['NPWP_NO'] != null ? _data['custDetailInfoCompany'][0]['NPWP_NO'].replaceAll(".", "").replaceAll("-", "") : "",
                  JenisNPWPModel(_data['custDetailInfoCompany'][0]['NPWP_TYPE'], _data['custDetailInfoCompany'][0]['NPWP_TYPE_DESC']),
                  _data['custDetailInfoCompany'][0]['FLAG_NPWP'] != null ? int.parse(_data['custDetailInfoCompany'][0]['FLAG_NPWP']) : 0,
                  _data['custDetailInfoCompany'][0]['PKP_FLAG'] != null ? _data['custDetailInfoCompany'][0]['PKP_FLAG'].toString() : "",
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  1,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null));
            }
          }
          else{
            print("CEK_DATA_BRO ${_data['custDedup'][0]['ADDRESS']}");
            if(await _dbHelper.deleteMS2Customer()){
              await _dbHelper.insertMS2Customer(MS2CustomerModel(
                  null,
                  null,//_data['custDetailInfoCompany'][0]['CUST_ID'],
                  null,
                  null,
                  _data['custDedup'][0]['ADDRESS'] != null ? _data['custDedup'][0]['ADDRESS'] : "",
                  null,//_data['custDetailInfoCompany'][0]['NPWP_NAME'] != null ? _data['custDetailInfoCompany'][0]['NPWP_NAME'] : "",
                  _data['custDedup'][0]['ID_NO'] != null ? _data['custDedup'][0]['ID_NO'] : "",
                  null,//JenisNPWPModel(_data['custDetailInfoCompany'][0]['NPWP_TYPE'], _data['custDetailInfoCompany'][0]['NPWP_TYPE_DESC']),
                  null,//_data['custDetailInfoCompany'][0]['FLAG_NPWP'] != null ? int.parse(_data['custDetailInfoCompany'][0]['FLAG_NPWP']) : 0,
                  null,//_data['custDetailInfoCompany'][0]['PKP_FLAG'] != null ? _data['custDetailInfoCompany'][0]['PKP_FLAG'].toString() : "",
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  1,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null
              ));
            }
          }
        }

        if(_data['custDedup'].isNotEmpty){
          print("dedup cust info ${_data['custDedup']}");
          if(await _dbHelper.deleteMS2Dedup()){
            await _dbHelper.insertDataDedup(DataDedupModel(
                custType,
                _data['custDedup'][0]['ID_NO'],
                _data['custDedup'][0]['COMP_NAME'],
                _data['custDedup'][0]['EST_DATE'],
                null,
                null,
                _data['custDedup'][0]['ADDRESS'],
                null,
                null,
                null,
                null,
                null
            ),
            _data['custDedup'][0]['ORDER_NO']
            );
          }
        }

        //address
        List<MS2CustAddrModel> _listAddressCust = [];
        if(_data['custAddress'].isNotEmpty){
          List _dataDakor = [];
          _dataDakor = await _dbHelper.selectMS2CustAddr("10");
          if(_preferences.getString("last_known_state") == "DKR" && _dataDakor.isNotEmpty){
            if(await _dbHelper.deleteMS2CustAddr("10")){
              for (int i = 0; i < _data['custAddress'].length; i++) {
                for(int j=0; j < _dataDakor.length; j++){
                  if(_data['custAddress'][i]['ADDR_TYPE'] == _dataDakor[j]['addr_type']){
                    _listAddressCust.add(MS2CustAddrModel(
                        "",
                        _data['custAddress'][i]['ADDRESS_ID'],
                        _data['custAddress'][i]['FOREIGN_BK'], //perlu adjust
                        _data['custAddress'][i]['ID_NO'], //tambahan id nomor
                        _data['custAddress'][i]['KORESPONDEN'], // == "1" ? "true" : "false",
                        "10",
                        _data['custAddress'][i]['ADDRESS'] != null ? _data['custAddress'][i]['ADDRESS'] : "",
                        _data['custAddress'][i]['RT'] != null ? _data['custAddress'][i]['RT'] : "",
                        null,
                        _data['custAddress'][i]['RW'] != null ? _data['custAddress'][i]['RW'] : "",
                        null,
                        _data['custAddress'][i]['PROVINSI'] != null ? _data['custAddress'][i]['PROVINSI'] : "",
                        _data['custAddress'][i]['PROVINSI_DESC'] != null ? _data['custAddress'][i]['PROVINSI_DESC'].toString().replaceAll("'", "`") : "",
                        _data['custAddress'][i]['KABKOT'] != null ? _data['custAddress'][i]['KABKOT'] : "",
                        _data['custAddress'][i]['KABKOT_DESC'] != null ? _data['custAddress'][i]['KABKOT_DESC'].toString().replaceAll("'", "`") : "",
                        _data['custAddress'][i]['KECAMATAN'] != null ? _data['custAddress'][i]['KECAMATAN'] : "",
                        _data['custAddress'][i]['KECAMATAN_DESC'] != null ? _data['custAddress'][i]['KECAMATAN_DESC'].toString().replaceAll("'", "`") : "",
                        _data['custAddress'][i]['KELURAHAN'] != null ? _data['custAddress'][i]['KELURAHAN'] : "",
                        _data['custAddress'][i]['KELURAHAN_DESC'] != null ? _data['custAddress'][i]['KELURAHAN_DESC'].toString().replaceAll("'", "`") : "",
                        _data['custAddress'][i]['ZIP_CODE'] != null ? _data['custAddress'][i]['ZIP_CODE'] : "",
                        null,
                        _data['custAddress'][i]['PHONE'] != null ? _data['custAddress'][i]['PHONE'] : "",
                        _data['custAddress'][i]['PHONE_AREA'] != null ? _data['custAddress'][i]['PHONE_AREA'] : "",
                        _data['custAddress'][i]['PHONE_DUA'] != null ? _data['custAddress'][i]['PHONE_DUA'] : "",
                        _data['custAddress'][i]['PHONE_AREA_DUA'] != null ? _data['custAddress'][i]['PHONE_AREA_DUA'] : "",
                        _data['custAddress'][i]['FAX'] != null ? _data['custAddress'][i]['FAX'] : "",
                        _data['custAddress'][i]['FAX_AREA'] != null ? _data['custAddress'][i]['FAX_AREA'] : "",
                        _data['custAddress'][i]['ADDR_TYPE'] != null ? _data['custAddress'][i]['ADDR_TYPE'] : "",
                        _data['custAddress'][i]['ADDR_DESC'] != null ? _data['custAddress'][i]['ADDR_DESC'] : "",
                        _data['custAddress'][i]['LATITUDE'] != null ? _data['custAddress'][i]['LATITUDE'] : "",
                        _data['custAddress'][i]['LONGITUDE'] != null ? _data['custAddress'][i]['LONGITUDE'] : "",
                        _data['custAddress'][i]['ADDRESS_FROM_MAP'] != null ? _data['custAddress'][i]['ADDRESS_FROM_MAP'] : "",
                        null,
                        null,
                        null,
                        null,
                        0,
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_addr_type'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_address'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_rt'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_rw'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_kelurahan'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_kecamatan'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_kabkot'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_provinsi'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_zip_code'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone1_area'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone1'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone_2_area'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone_2'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_fax_area'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_fax'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_address_from_map'] : '0'));
                  }
                }
              }
              await _dbHelper.insertMS2CustAddr(_listAddressCust);
            }
          }
          else {
            if(await _dbHelper.deleteMS2CustAddr("10")){
              for (int i = 0; i < _data['custAddress'].length; i++) {
                _listAddressCust.add(MS2CustAddrModel(
                    "",
                    _data['custAddress'][i]['ADDRESS_ID'],
                    _data['custAddress'][i]['FOREIGN_BK'], //perlu adjust
                    _data['custAddress'][i]['ID_NO'], //tambahan id nomor
                    _data['custAddress'][i]['KORESPONDEN'], // == "1" ? "true" : "false",
                    "10",
                    _data['custAddress'][i]['ADDRESS'] != null ? _data['custAddress'][i]['ADDRESS'] : "",
                    _data['custAddress'][i]['RT'] != null ? _data['custAddress'][i]['RT'] : "",
                    null,
                    _data['custAddress'][i]['RW'] != null ? _data['custAddress'][i]['RW'] : "",
                    null,
                    _data['custAddress'][i]['PROVINSI'] != null ? _data['custAddress'][i]['PROVINSI'] : "",
                    _data['custAddress'][i]['PROVINSI_DESC'] != null ? _data['custAddress'][i]['PROVINSI_DESC'].toString().replaceAll("'", "`") : "",
                    _data['custAddress'][i]['KABKOT'] != null ? _data['custAddress'][i]['KABKOT'] : "",
                    _data['custAddress'][i]['KABKOT_DESC'] != null ? _data['custAddress'][i]['KABKOT_DESC'].toString().replaceAll("'", "`") : "",
                    _data['custAddress'][i]['KECAMATAN'] != null ? _data['custAddress'][i]['KECAMATAN'] : "",
                    _data['custAddress'][i]['KECAMATAN_DESC'] != null ? _data['custAddress'][i]['KECAMATAN_DESC'].toString().replaceAll("'", "`") : "",
                    _data['custAddress'][i]['KELURAHAN'] != null ? _data['custAddress'][i]['KELURAHAN'] : "",
                    _data['custAddress'][i]['KELURAHAN_DESC'] != null ? _data['custAddress'][i]['KELURAHAN_DESC'].toString().replaceAll("'", "`") : "",
                    _data['custAddress'][i]['ZIP_CODE'] != null ? _data['custAddress'][i]['ZIP_CODE'] : "",
                    null,
                    _data['custAddress'][i]['PHONE'] != null ? _data['custAddress'][i]['PHONE'] : "",
                    _data['custAddress'][i]['PHONE_AREA'] != null ? _data['custAddress'][i]['PHONE_AREA'] : "",
                    _data['custAddress'][i]['PHONE_DUA'] != null ? _data['custAddress'][i]['PHONE_DUA'] : "",
                    _data['custAddress'][i]['PHONE_AREA_DUA'] != null ? _data['custAddress'][i]['PHONE_AREA_DUA'] : "",
                    _data['custAddress'][i]['FAX'] != null ? _data['custAddress'][i]['FAX'] : "",
                    _data['custAddress'][i]['FAX_AREA'] != null ? _data['custAddress'][i]['FAX_AREA'] : "",
                    _data['custAddress'][i]['ADDR_TYPE'] != null ? _data['custAddress'][i]['ADDR_TYPE'] : "",
                    _data['custAddress'][i]['ADDR_DESC'] != null ? _data['custAddress'][i]['ADDR_DESC'] : "",
                    _data['custAddress'][i]['LATITUDE'] != null ? _data['custAddress'][i]['LATITUDE'] : "",
                    _data['custAddress'][i]['LONGITUDE'] != null ? _data['custAddress'][i]['LONGITUDE'] : "",
                    _data['custAddress'][i]['ADDRESS_FROM_MAP'] != null ? _data['custAddress'][i]['ADDRESS_FROM_MAP'] : "",
                    null,
                    null,
                    null,
                    null,
                    0,
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0'));
              }
              await _dbHelper.insertMS2CustAddr(_listAddressCust);
            }
          }
        }

        // managementPIC
        if(_data['custManagementPic'].isNotEmpty){
          List _dataDakor = [];
          if(_preferences.getString("last_known_state") == "DKR") {
            _dataDakor = await _dbHelper.selectMS2CustomerCompanyForPIC();
          }
          await _dbHelper.insertMS2CustomerCompanyForPIC(ManagementPICSQLiteModel(
            pic.TypeIdentityModel(_data['custManagementPic'][0]['ID_TYPE'],_data['custManagementPic'][0]['ID_DESC']),
            _data['custManagementPic'][0]['ID_NO'],
            _data['custManagementPic'][0]['ID_FULLNAME'],
            _data['custManagementPic'][0]['FULL_NAME'],
            _data['custManagementPic'][0]['ALIAS_NAME'],
            _data['custManagementPic'][0]['DEGREE'],
            _data['custManagementPic'][0]['DATE_OF_BIRTH'],
            _data['custManagementPic'][0]['PLACE_OF_BIRTH'],
            BirthPlaceModel(_data['custManagementPic'][0]['PLACE_OF_BIRTH_KABKOTA'],_data['custManagementPic'][0]['PLACE_OF_BIRTH_KABKOTA_DESC']),
            null,
            null,
            null,
            null,
            null,
            null,
            _data['custManagementPic'][0]['AC_LEVEL'],
            null,
            _data['custManagementPic'][0]['HANDPHONE_NO'] != null && _data['custManagementPic'][0]['HANDPHONE_NO'].toString().length > 3 ? _data['custManagementPic'][0]['HANDPHONE_NO'].toString().replaceRange(0, 2, "") : "",
            _data['custManagementPic'][0]['EMAIL'] != null ? _data['custManagementPic'][0]['EMAIL'] : "",
            null,
            null,
            DateTime.now().toString(),
            _preferences.getString("username"),
            null,
            null,
            1,
            null,
            _dataDakor.isNotEmpty ? _dataDakor[0]['identityTypeDakor'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['identityNoDakor'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['fullnameIdDakor'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['fullnameDakor'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['dateOfBirthDakor'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['placeOfBirthDakor'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['placeOfBirthLOVDakor'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['positionDakor'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['emailDakor'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['phoneDakor'] : "0",
          ));
        }

        // address PIC
        List<MS2CustAddrModel> _listAddressCustPIC = [];
        if(_data['custManagementPicAddr'].isNotEmpty){
          List _dataDakor = [];
          _dataDakor = await _dbHelper.selectMS2CustAddr("9");
          if(_preferences.getString("last_known_state") == "DKR" && _dataDakor.isNotEmpty) {
            if(await _dbHelper.deleteMS2CustAddr("9")){
              for (int i = 0; i < _data['custManagementPicAddr'].length; i++) {
                for(int j=0; j < _dataDakor.length; j++) {
                  if(_data['custManagementPicAddr'][i]['ADDR_TYPE'] == _dataDakor[j]['addr_type']) {
                    _listAddressCustPIC.add(MS2CustAddrModel(
                        "",
                        _data['custManagementPicAddr'][i]['ADDRESS_ID'],
                        _data['custManagementPicAddr'][i]['FOREIGN_BK'], //perlu adjust
                        null, //tambahan id nomor
                        _data['custManagementPicAddr'][i]['KORESPONDEN'], // == "1" ? "true" : "false",
                        "9",
                        _data['custManagementPicAddr'][i]['ADDRESS'] != null ? _data['custManagementPicAddr'][i]['ADDRESS'] : "",
                        _data['custManagementPicAddr'][i]['RT'] != null ? _data['custManagementPicAddr'][i]['RT'] : "",
                        null,
                        _data['custManagementPicAddr'][i]['RW'] != null ? _data['custManagementPicAddr'][i]['RW'] : "",
                        null,
                        _data['custManagementPicAddr'][i]['PROVINSI'] != null ? _data['custManagementPicAddr'][i]['PROVINSI'] : "",
                        _data['custManagementPicAddr'][i]['PROVINSI_DESC'] != null ? _data['custManagementPicAddr'][i]['PROVINSI_DESC'].toString().replaceAll("'", "`") : "",
                        _data['custManagementPicAddr'][i]['KABKOT'] != null ? _data['custManagementPicAddr'][i]['KABKOT'] : "",
                        _data['custManagementPicAddr'][i]['KABKOT_DESC'] != null ? _data['custManagementPicAddr'][i]['KABKOT_DESC'].toString().replaceAll("'", "`") : "",
                        _data['custManagementPicAddr'][i]['KECAMATAN'] != null ? _data['custManagementPicAddr'][i]['KECAMATAN'] : "",
                        _data['custManagementPicAddr'][i]['KECAMATAN_DESC'] != null ? _data['custManagementPicAddr'][i]['KECAMATAN_DESC'].toString().replaceAll("'", "`") : "",
                        _data['custManagementPicAddr'][i]['KELURAHAN'] != null ? _data['custManagementPicAddr'][i]['KELURAHAN'] : "",
                        _data['custManagementPicAddr'][i]['KELURAHAN_DESC'] != null ? _data['custManagementPicAddr'][i]['KELURAHAN_DESC'].toString().replaceAll("'", "`") : "",
                        _data['custManagementPicAddr'][i]['ZIP_CODE'] != null ? _data['custManagementPicAddr'][i]['ZIP_CODE'] : "",
                        null,
                        _data['custManagementPicAddr'][i]['PHONE'] != null ? _data['custManagementPicAddr'][i]['PHONE'] : "",
                        _data['custManagementPicAddr'][i]['PHONE_AREA'] != null ? _data['custManagementPicAddr'][i]['PHONE_AREA'] : "",
                        _data['custManagementPicAddr'][i]['PHONE_DUA'] != null ? _data['custManagementPicAddr'][i]['PHONE_DUA'] : "",
                        _data['custManagementPicAddr'][i]['PHONE_AREA_DUA'] != null ? _data['custManagementPicAddr'][i]['PHONE_AREA_DUA'] : "",
                        _data['custManagementPicAddr'][i]['FAX'] != null ? _data['custManagementPicAddr'][i]['FAX'] : "",
                        _data['custManagementPicAddr'][i]['FAX_AREA'] != null ? _data['custManagementPicAddr'][i]['FAX_AREA'] : "",
                        _data['custManagementPicAddr'][i]['ADDR_TYPE'] != null ? _data['custManagementPicAddr'][i]['ADDR_TYPE'] : "",
                        _data['custManagementPicAddr'][i]['ADDR_DESC'] != null ? _data['custManagementPicAddr'][i]['ADDR_DESC'] : "",
                        _data['custManagementPicAddr'][i]['LATITUDE'] != null ? _data['custManagementPicAddr'][i]['LATITUDE'] : "",
                        _data['custManagementPicAddr'][i]['LONGITUDE'] != null ? _data['custManagementPicAddr'][i]['LONGITUDE'] : "",
                        _data['custManagementPicAddr'][i]['ADDRESS_FROM_MAP'] != null ? _data['custManagementPicAddr'][i]['ADDRESS_FROM_MAP'] : "",
                        null,
                        null,
                        null,
                        null,
                        0,
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_addr_type'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_address'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_rt'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_rw'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_kelurahan'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_kecamatan'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_kabkot'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_provinsi'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_zip_code'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone1_area'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone1'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone_2_area'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone_2'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_fax_area'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_fax'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_address_from_map'] : "0",
                    ));
                  }
                }
              }
              await _dbHelper.insertMS2CustAddr(_listAddressCustPIC);
            }
          }
          else {
            if(await _dbHelper.deleteMS2CustAddr("9")){
              for (int i = 0; i < _data['custManagementPicAddr'].length; i++) {
                _listAddressCustPIC.add(MS2CustAddrModel(
                    "",
                    _data['custManagementPicAddr'][i]['ADDRESS_ID'],
                    _data['custManagementPicAddr'][i]['FOREIGN_BK'], //perlu adjust
                    null, //tambahan id nomor
                    _data['custManagementPicAddr'][i]['KORESPONDEN'], // == "1" ? "true" : "false",
                    "9",
                    _data['custManagementPicAddr'][i]['ADDRESS'] != null ? _data['custManagementPicAddr'][i]['ADDRESS'] : "",
                    _data['custManagementPicAddr'][i]['RT'] != null ? _data['custManagementPicAddr'][i]['RT'] : "",
                    null,
                    _data['custManagementPicAddr'][i]['RW'] != null ? _data['custManagementPicAddr'][i]['RW'] : "",
                    null,
                    _data['custManagementPicAddr'][i]['PROVINSI'] != null ? _data['custManagementPicAddr'][i]['PROVINSI'] : "",
                    _data['custManagementPicAddr'][i]['PROVINSI_DESC'] != null ? _data['custManagementPicAddr'][i]['PROVINSI_DESC'].toString().replaceAll("'", "`") : "",
                    _data['custManagementPicAddr'][i]['KABKOT'] != null ? _data['custManagementPicAddr'][i]['KABKOT'] : "",
                    _data['custManagementPicAddr'][i]['KABKOT_DESC'] != null ? _data['custManagementPicAddr'][i]['KABKOT_DESC'].toString().replaceAll("'", "`") : "",
                    _data['custManagementPicAddr'][i]['KECAMATAN'] != null ? _data['custManagementPicAddr'][i]['KECAMATAN'] : "",
                    _data['custManagementPicAddr'][i]['KECAMATAN_DESC'] != null ? _data['custManagementPicAddr'][i]['KECAMATAN_DESC'].toString().replaceAll("'", "`") : "",
                    _data['custManagementPicAddr'][i]['KELURAHAN'] != null ? _data['custManagementPicAddr'][i]['KELURAHAN'] : "",
                    _data['custManagementPicAddr'][i]['KELURAHAN_DESC'] != null ? _data['custManagementPicAddr'][i]['KELURAHAN_DESC'].toString().replaceAll("'", "`") : "",
                    _data['custManagementPicAddr'][i]['ZIP_CODE'] != null ? _data['custManagementPicAddr'][i]['ZIP_CODE'] : "",
                    null,
                    _data['custManagementPicAddr'][i]['PHONE'] != null ? _data['custManagementPicAddr'][i]['PHONE'] : "",
                    _data['custManagementPicAddr'][i]['PHONE_AREA'] != null ? _data['custManagementPicAddr'][i]['PHONE_AREA'] : "",
                    _data['custManagementPicAddr'][i]['PHONE_DUA'] != null ? _data['custManagementPicAddr'][i]['PHONE_DUA'] : "",
                    _data['custManagementPicAddr'][i]['PHONE_AREA_DUA'] != null ? _data['custManagementPicAddr'][i]['PHONE_AREA_DUA'] : "",
                    _data['custManagementPicAddr'][i]['FAX'] != null ? _data['custManagementPicAddr'][i]['FAX'] : "",
                    _data['custManagementPicAddr'][i]['FAX_AREA'] != null ? _data['custManagementPicAddr'][i]['FAX_AREA'] : "",
                    _data['custManagementPicAddr'][i]['ADDR_TYPE'] != null ? _data['custManagementPicAddr'][i]['ADDR_TYPE'] : "",
                    _data['custManagementPicAddr'][i]['ADDR_DESC'] != null ? _data['custManagementPicAddr'][i]['ADDR_DESC'] : "",
                    _data['custManagementPicAddr'][i]['LATITUDE'] != null ? _data['custManagementPicAddr'][i]['LATITUDE'] : "",
                    _data['custManagementPicAddr'][i]['LONGITUDE'] != null ? _data['custManagementPicAddr'][i]['LONGITUDE'] : "",
                    _data['custManagementPicAddr'][i]['ADDRESS_FROM_MAP'] != null ? _data['custManagementPicAddr'][i]['ADDRESS_FROM_MAP'] : "",
                    null,
                    null,
                    null,
                    null,
                    0,
                    "0",
                    "0",
                    "0",
                    "0",
                    "0",
                    "0",
                    "0",
                    "0",
                    "0",
                    "0",
                    "0",
                    "0",
                    "0",
                    "0",
                    "0",
                    "0"
                ));
              }
              await _dbHelper.insertMS2CustAddr(_listAddressCustPIC);
            }
          }
        }

        // pemegang saham lembaga
        List<MS2CustShrhldrComModel> _listPemegangSahamLembaga = [];
        if(_data['custShareholderCompany'].isNotEmpty){
          List _dataDakor = [];
          _dataDakor = await _dbHelper.selectMS2CustShrhldrCom();
          if(_preferences.getString("last_known_state") == "DKR" && _dataDakor.isNotEmpty){
            if(await _dbHelper.deleteMS2CustShrhldrCom()){
              for (int i = 0; i < _data['custShareholderCompany'].length; i++) {
                for(int j=0; j < _dataDakor.length; j++){
                  if(_data['custShareholderCompany'][i]['SHRHLDR_COMP_ID'] == _dataDakor[j]['shareholdersCorporateID']){
                    _listPemegangSahamLembaga.add(MS2CustShrhldrComModel(
                      "",
                      _data['custShareholderCompany'][i]['SHRHLDR_COMP_ID'],//this._listPemegangSahamLembaga[i].shareholdersCorporateID,
                      TypeInstitutionModel(_data['custShareholderCompany'][i]['COMP_TYPE'], _data['custShareholderCompany'][i]['COMP_TYPE_DESC']),//this._listPemegangSahamLembaga[i].typeInstitutionModel,
                      null,
                      null,
                      _data['custShareholderCompany'][i]['COMP_NAME'],//this._listPemegangSahamLembaga[i].institutionName,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      _data['custShareholderCompany'][i]['ESTABLISH_DATE'],//dateFormat.format(DateTime.parse(_data['custShareholderCompany'][i]['ESTABLISH_DATE'])),//this._listPemegangSahamLembaga[i].initialDateForDateOfEstablishment,
                      0,
                      null,
                      null,
                      null,
                      null,
                      _data['custShareholderCompany'][i]['NPWP_NO'] != null ? _data['custShareholderCompany'][i]['NPWP_NO'].replaceAll(".", "").replaceAll("-", "") : "",//this._listPemegangSahamLembaga[i].npwp,
                      null,
                      null,
                      null,//_data['custShareholderCompany'][i][''],//this._listPemegangSahamLembaga[i].npwp != "" ? 1 : 0,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      _data['custShareholderCompany'][i]['SHRHLDNG_PERCENT'] != null ? int.parse(_data['custShareholderCompany'][i]['SHRHLDNG_PERCENT']) : null,//this._listPemegangSahamLembaga[i].percentShare.isEmpty ? 0 : int.parse(this._listPemegangSahamLembaga[i].percentShare),
                      null,
                      DateTime.now().toString(),
                      null,
                      null,
                      null,
                      1,
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_comp_type'] : "0",//this._listPemegangSahamLembaga[i].isTypeInstitutionModelChange ? "1" : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_comp_name'] : "0",//this._listPemegangSahamLembaga[i].isInstitutionNameChange ? "1" : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_establish_date'] : "0",//this._listPemegangSahamLembaga[i].isInitialDateForDateOfEstablishmentChange ? "1" : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_npwp_no'] : "0",//this._listPemegangSahamLembaga[i].isNpwpChange ? "1" : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_shrldng_percent'] : "0",//this._listPemegangSahamLembaga[i].isPercentShareChange ? "1" : "0",
                    ));
                  }
                }
              }
              await _dbHelper.insertMS2CustShrhldrCom(_listPemegangSahamLembaga);
            }
          }
          else {
            if(await _dbHelper.deleteMS2CustShrhldrCom()){
              for (int i = 0; i < _data['custShareholderCompany'].length; i++) {
                _listPemegangSahamLembaga.add(MS2CustShrhldrComModel(
                  "",
                  _data['custShareholderCompany'][i]['SHRHLDR_COMP_ID'],//this._listPemegangSahamLembaga[i].shareholdersCorporateID,
                  TypeInstitutionModel(_data['custShareholderCompany'][i]['COMP_TYPE'], _data['custShareholderCompany'][i]['COMP_TYPE_DESC']),//this._listPemegangSahamLembaga[i].typeInstitutionModel,
                  null,
                  null,
                  _data['custShareholderCompany'][i]['COMP_NAME'],//this._listPemegangSahamLembaga[i].institutionName,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  _data['custShareholderCompany'][i]['ESTABLISH_DATE'],//dateFormat.format(DateTime.parse(_data['custShareholderCompany'][i]['ESTABLISH_DATE'])),//this._listPemegangSahamLembaga[i].initialDateForDateOfEstablishment,
                  0,
                  null,
                  null,
                  null,
                  null,
                  _data['custShareholderCompany'][i]['NPWP_NO'] != null ? _data['custShareholderCompany'][i]['NPWP_NO'].replaceAll(".", "").replaceAll("-", "") : "", //this._listPemegangSahamLembaga[i].npwp,
                  null,
                  null,
                  null,//_data['custShareholderCompany'][i][''],//this._listPemegangSahamLembaga[i].npwp != "" ? 1 : 0,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  _data['custShareholderCompany'][i]['SHRHLDNG_PERCENT'] != null ? int.parse(_data['custShareholderCompany'][i]['SHRHLDNG_PERCENT']) : null,//this._listPemegangSahamLembaga[i].percentShare.isEmpty ? 0 : int.parse(this._listPemegangSahamLembaga[i].percentShare),
                  null,
                  DateTime.now().toString(),
                  null,
                  null,
                  null,
                  1,
                  "0",//this._listPemegangSahamLembaga[i].isTypeInstitutionModelChange ? "1" : "0",
                  "0",//this._listPemegangSahamLembaga[i].isInstitutionNameChange ? "1" : "0",
                  "0",//this._listPemegangSahamLembaga[i].isInitialDateForDateOfEstablishmentChange ? "1" : "0",
                  "0",//this._listPemegangSahamLembaga[i].isNpwpChange ? "1" : "0",
                  "0",//this._listPemegangSahamLembaga[i].isPercentShareChange ? "1" : "0",
                ));
              }
              await _dbHelper.insertMS2CustShrhldrCom(_listPemegangSahamLembaga);
            }
          }
        }

        //pemegang saham personal
        List<MS2CustShrhldrIndModel> _listPemegangSahamPersonal = [];
        if(_data['custShareholderPersonal'].isNotEmpty){
          List _dataDakor = [];
          _dataDakor = await _dbHelper.selectMS2CustShrhldrInd();
          if(_preferences.getString("last_known_state") == "DKR" && _dataDakor.isNotEmpty){
            if(await _dbHelper.deleteMS2CustShrhldrInd()){
              for (int i = 0; i < _data['custShareholderPersonal'].length; i++) {
                for(int j = 0; j < _dataDakor.length; j++){
                  if(_data['custShareholderPersonal'][i]['SHRHLDR_IND_ID'] == _dataDakor[j]['shareholdersIndividualID']){
                    _listPemegangSahamPersonal.add(MS2CustShrhldrIndModel(
                      "",
                      _data['custShareholderPersonal'][i]['SHRHLDR_IND_ID'],//this._listPemegangSahamPribadi[i].shareholdersIndividualID,
                      _data['custShareholderPersonal'][i]['SHRHLDNG_PERCENT'] != null ? int.parse(_data['custShareholderPersonal'][i]['SHRHLDNG_PERCENT']) : null,//this._listPemegangSahamPribadi[i].percentShare.isEmpty ? 0 : int.parse(this._listPemegangSahamPribadi[i].percentShare),
                      0,
                      _data['custShareholderPersonal'][i]['RELATION_STATUS'],//this._listPemegangSahamPribadi[i].relationshipStatusSelected.PARA_FAMILY_TYPE_ID,
                      _data['custShareholderPersonal'][i]['RELATION_STATUS_DESC'],//this._listPemegangSahamPribadi[i].relationshipStatusSelected.PARA_FAMILY_TYPE_NAME,
                      _data['custShareholderPersonal'][i]['ID_TYPE'],//this._listPemegangSahamPribadi[i].typeIdentitySelected.id,
                      _data['custShareholderPersonal'][i]['ID_TYPE_DESC'],//this._listPemegangSahamPribadi[i].typeIdentitySelected.name,
                      _data['custShareholderPersonal'][i]['ID_NO'],//this._listPemegangSahamPribadi[i].identityNumber,
                      _data['custShareholderPersonal'][i]['FULL_NAME_ID'],//this._listPemegangSahamPribadi[i].fullNameIdentity,
                      _data['custShareholderPersonal'][i]['FULL_NAME'],//this._listPemegangSahamPribadi[i].fullName,
                      null,
                      null,
                      _data['custShareholderPersonal'][i]['DATE_OF_BIRTH'],//this._listPemegangSahamPribadi[i].birthOfDate.toString(),
                      _data['custShareholderPersonal'][i]['PLACE_OF_BIRTH'],//this._listPemegangSahamPribadi[i].placeOfBirthIdentity,
                      _data['custShareholderPersonal'][i]['PLACE_OF_BIRTH_KABKOTA'],//this._listPemegangSahamPribadi[i].birthPlaceIdentityLOV != null ? this._listPemegangSahamPribadi[i].birthPlaceIdentityLOV.KABKOT_ID : null,
                      _data['custShareholderPersonal'][i]['PLACE_OF_BIRTH_KABKOTA_DESC'],//this._listPemegangSahamPribadi[i].birthPlaceIdentityLOV != null ? this._listPemegangSahamPribadi[i].birthPlaceIdentityLOV.KABKOT_NAME : null,
                      _data['custShareholderPersonal'][i]['GENDER'],//this._listPemegangSahamPribadi[i].gender,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_relation_status'] : "0",//this._listPemegangSahamPribadi[i].isRelationshipStatusChange ? "1" : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_id_type'] : "0",//this._listPemegangSahamPribadi[i].isIdentityTypeChange ? "1" : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_id_no'] : "0",//this._listPemegangSahamPribadi[i].isIdentityNoChange ? "1" : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_full_name_id'] : "0",//this._listPemegangSahamPribadi[i].isFullnameIDChange ? "1" : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_full_name'] : "0",//this._listPemegangSahamPribadi[i].isFullnameChange ? "1" : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_date_of_birth'] : "0",//this._listPemegangSahamPribadi[i].isDateOfBirthChange ? "1" : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_place_of_birth'] : "0",//this._listPemegangSahamPribadi[i].isPlaceOfBirthChange ? "1" : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_place_of_birth_kabkota'] : "0",//this._listPemegangSahamPribadi[i].isPlaceOfBirthLOVChange ? "1" : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_gender'] : "0",//this._listPemegangSahamPribadi[i].isGenderChange ? "1" : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_shrhldng_percent'] : "0",//this._listPemegangSahamPribadi[i].isShareChange ? "1" : "0",
                    ));
                  }
                }
              }
              await _dbHelper.insertMS2CustShrhldrInd(_listPemegangSahamPersonal);
            }
          }
          else {
            if(await _dbHelper.deleteMS2CustShrhldrInd()){
              for (int i = 0; i < _data['custShareholderPersonal'].length; i++) {
                _listPemegangSahamPersonal.add(MS2CustShrhldrIndModel(
                  "",
                  _data['custShareholderPersonal'][i]['SHRHLDR_IND_ID'],//this._listPemegangSahamPribadi[i].shareholdersIndividualID,
                  _data['custShareholderPersonal'][i]['SHRHLDNG_PERCENT'] != null ? int.parse(_data['custShareholderPersonal'][i]['SHRHLDNG_PERCENT']) : null,//this._listPemegangSahamPribadi[i].percentShare.isEmpty ? 0 : int.parse(this._listPemegangSahamPribadi[i].percentShare),
                  0,
                  _data['custShareholderPersonal'][i]['RELATION_STATUS'],//this._listPemegangSahamPribadi[i].relationshipStatusSelected.PARA_FAMILY_TYPE_ID,
                  _data['custShareholderPersonal'][i]['RELATION_STATUS_DESC'],//this._listPemegangSahamPribadi[i].relationshipStatusSelected.PARA_FAMILY_TYPE_NAME,
                  _data['custShareholderPersonal'][i]['ID_TYPE'],//this._listPemegangSahamPribadi[i].typeIdentitySelected.id,
                  _data['custShareholderPersonal'][i]['ID_TYPE_DESC'],//this._listPemegangSahamPribadi[i].typeIdentitySelected.name,
                  _data['custShareholderPersonal'][i]['ID_NO'],//this._listPemegangSahamPribadi[i].identityNumber,
                  _data['custShareholderPersonal'][i]['FULL_NAME_ID'],//this._listPemegangSahamPribadi[i].fullNameIdentity,
                  _data['custShareholderPersonal'][i]['FULL_NAME'],//this._listPemegangSahamPribadi[i].fullName,
                  null,
                  null,
                  _data['custShareholderPersonal'][i]['DATE_OF_BIRTH'],//this._listPemegangSahamPribadi[i].birthOfDate.toString(),
                  _data['custShareholderPersonal'][i]['PLACE_OF_BIRTH'],//this._listPemegangSahamPribadi[i].placeOfBirthIdentity,
                  _data['custShareholderPersonal'][i]['PLACE_OF_BIRTH_KABKOTA'],//this._listPemegangSahamPribadi[i].birthPlaceIdentityLOV != null ? this._listPemegangSahamPribadi[i].birthPlaceIdentityLOV.KABKOT_ID : null,
                  _data['custShareholderPersonal'][i]['PLACE_OF_BIRTH_KABKOTA_DESC'],//this._listPemegangSahamPribadi[i].birthPlaceIdentityLOV != null ? this._listPemegangSahamPribadi[i].birthPlaceIdentityLOV.KABKOT_NAME : null,
                  _data['custShareholderPersonal'][i]['GENDER'],//this._listPemegangSahamPribadi[i].gender,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  "0",//this._listPemegangSahamPribadi[i].isRelationshipStatusChange ? "1" : "0",
                  "0",//this._listPemegangSahamPribadi[i].isIdentityTypeChange ? "1" : "0",
                  "0",//this._listPemegangSahamPribadi[i].isIdentityNoChange ? "1" : "0",
                  "0",//this._listPemegangSahamPribadi[i].isFullnameIDChange ? "1" : "0",
                  "0",//this._listPemegangSahamPribadi[i].isFullnameChange ? "1" : "0",
                  "0",//this._listPemegangSahamPribadi[i].isDateOfBirthChange ? "1" : "0",
                  "0",//this._listPemegangSahamPribadi[i].isPlaceOfBirthChange ? "1" : "0",
                  "0",//this._listPemegangSahamPribadi[i].isPlaceOfBirthLOVChange ? "1" : "0",
                  "0",//this._listPemegangSahamPribadi[i].isGenderChange ? "1" : "0",
                  "0",//this._listPemegangSahamPribadi[i].isShareChange ? "1" : "0",
                ));
              }
              await _dbHelper.insertMS2CustShrhldrInd(_listPemegangSahamPersonal);
            }
          }
        }

        // address pemegang saham personal
        List<MS2CustAddrModel> _listAddressPemegangSahamPER = [];
        if(_data['custShareholderPersonalAddr'].isNotEmpty){
          List _dataDakor = [];
          _dataDakor = await _dbHelper.selectMS2CustAddr("11");
          if(_preferences.getString("last_known_state") == "DKR" && _dataDakor.isNotEmpty){
            if(await _dbHelper.deleteMS2CustAddr("11")){
              for (int i = 0; i < _data['custShareholderPersonalAddr'].length; i++) {
                for(int j=0; j < _dataDakor.length; j++){
                  if(_data['custShareholderPersonalAddr'][i]['ADDR_TYPE'] == _dataDakor[j]['addr_type']){
                    _listAddressPemegangSahamPER.add(MS2CustAddrModel(
                        "",
                        _data['custShareholderPersonalAddr'][i]['ADDRESS_ID'],
                        _data['custShareholderPersonalAddr'][i]['FOREIGN_BK'], //perlu adjust
                        _data['custShareholderPersonalAddr'][i]['ID_NO'], //tambahan id nomor
                        _data['custShareholderPersonalAddr'][i]['KORESPONDEN'], // == "1" ? "true" : "false",
                        "11",
                        _data['custShareholderPersonalAddr'][i]['ADDRESS'] != null ? _data['custShareholderPersonalAddr'][i]['ADDRESS'] : "",
                        _data['custShareholderPersonalAddr'][i]['RT'] != null ? _data['custShareholderPersonalAddr'][i]['RT'] : "",
                        null,
                        _data['custShareholderPersonalAddr'][i]['RW'] != null ? _data['custShareholderPersonalAddr'][i]['RW'] : "",
                        null,
                        _data['custShareholderPersonalAddr'][i]['PROVINSI'] != null ? _data['custShareholderPersonalAddr'][i]['PROVINSI'] : "",
                        _data['custShareholderPersonalAddr'][i]['PROVINSI_DESC'] != null ? _data['custShareholderPersonalAddr'][i]['PROVINSI_DESC'].toString().replaceAll("'", "`") : "",
                        _data['custShareholderPersonalAddr'][i]['KABKOT'] != null ? _data['custShareholderPersonalAddr'][i]['KABKOT'] : "",
                        _data['custShareholderPersonalAddr'][i]['KABKOT_DESC'] != null ? _data['custShareholderPersonalAddr'][i]['KABKOT_DESC'].toString().replaceAll("'", "`") : "",
                        _data['custShareholderPersonalAddr'][i]['KECAMATAN'] != null ? _data['custShareholderPersonalAddr'][i]['KECAMATAN'] : "",
                        _data['custShareholderPersonalAddr'][i]['KECAMATAN_DESC'] != null ? _data['custShareholderPersonalAddr'][i]['KECAMATAN_DESC'].toString().replaceAll("'", "`") : "",
                        _data['custShareholderPersonalAddr'][i]['KELURAHAN'] != null ? _data['custShareholderPersonalAddr'][i]['KELURAHAN'] : "",
                        _data['custShareholderPersonalAddr'][i]['KELURAHAN_DESC'] != null ? _data['custShareholderPersonalAddr'][i]['KELURAHAN_DESC'].toString().replaceAll("'", "`") : "",
                        _data['custShareholderPersonalAddr'][i]['ZIP_CODE'] != null ? _data['custShareholderPersonalAddr'][i]['ZIP_CODE'] : "",
                        null,
                        _data['custShareholderPersonalAddr'][i]['PHONE'] != null ? _data['custShareholderPersonalAddr'][i]['PHONE'] : "",
                        _data['custShareholderPersonalAddr'][i]['PHONE_AREA'] != null ? _data['custShareholderPersonalAddr'][i]['PHONE_AREA'] : "",
                        _data['custShareholderPersonalAddr'][i]['PHONE_DUA'] != null ? _data['custShareholderPersonalAddr'][i]['PHONE_DUA'] : "",
                        _data['custShareholderPersonalAddr'][i]['PHONE_AREA_DUA'] != null ? _data['custShareholderPersonalAddr'][i]['PHONE_AREA_DUA'] : "",
                        _data['custShareholderPersonalAddr'][i]['FAX'] != null ? _data['custShareholderPersonalAddr'][i]['FAX'] : "",
                        _data['custShareholderPersonalAddr'][i]['FAX_AREA'] != null ? _data['custShareholderPersonalAddr'][i]['FAX_AREA'] : "",
                        _data['custShareholderPersonalAddr'][i]['ADDR_TYPE'] != null ? _data['custShareholderPersonalAddr'][i]['ADDR_TYPE'] : "",
                        _data['custShareholderPersonalAddr'][i]['ADDR_DESC'] != null ? _data['custShareholderPersonalAddr'][i]['ADDR_DESC'] : "",
                        _data['custShareholderPersonalAddr'][i]['LATITUDE'] != null ? _data['custShareholderPersonalAddr'][i]['LATITUDE'] : "",
                        _data['custShareholderPersonalAddr'][i]['LONGITUDE'] != null ? _data['custShareholderPersonalAddr'][i]['LONGITUDE'] : "",
                        _data['custShareholderPersonalAddr'][i]['ADDRESS_FROM_MAP'] != null ? _data['custShareholderPersonalAddr'][i]['ADDRESS_FROM_MAP'] : "",
                        null,
                        null,
                        null,
                        null,
                        0,
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_addr_type'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_address'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_rt'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_rw'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_kelurahan'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_kecamatan'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_kabkot'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_provinsi'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_zip_code'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone1_area'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone1'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone_2_area'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone_2'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_fax_area'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_fax'] : '0',
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_address_from_map'] : '0'
                    ));
                  }
                }
              }
              await _dbHelper.insertMS2CustAddr(_listAddressPemegangSahamPER);
            }
          }
          else {
            if(await _dbHelper.deleteMS2CustAddr("11")){
              for (int i = 0; i < _data['custShareholderPersonalAddr'].length; i++) {
                _listAddressPemegangSahamPER.add(MS2CustAddrModel(
                    "",
                    _data['custShareholderPersonalAddr'][i]['ADDRESS_ID'],
                    _data['custShareholderPersonalAddr'][i]['FOREIGN_BK'], //perlu adjust
                    _data['custShareholderPersonalAddr'][i]['ID_NO'], //tambahan id nomor
                    _data['custShareholderPersonalAddr'][i]['KORESPONDEN'], // == "1" ? "true" : "false",
                    "11",
                    _data['custShareholderPersonalAddr'][i]['ADDRESS'] != null ? _data['custShareholderPersonalAddr'][i]['ADDRESS'] : "",
                    _data['custShareholderPersonalAddr'][i]['RT'] != null ? _data['custShareholderPersonalAddr'][i]['RT'] : "",
                    null,
                    _data['custShareholderPersonalAddr'][i]['RW'] != null ? _data['custShareholderPersonalAddr'][i]['RW'] : "",
                    null,
                    _data['custShareholderPersonalAddr'][i]['PROVINSI'] != null ? _data['custShareholderPersonalAddr'][i]['PROVINSI'] : "",
                    _data['custShareholderPersonalAddr'][i]['PROVINSI_DESC'] != null ? _data['custShareholderPersonalAddr'][i]['PROVINSI_DESC'].toString().replaceAll("'", "`") : "",
                    _data['custShareholderPersonalAddr'][i]['KABKOT'] != null ? _data['custShareholderPersonalAddr'][i]['KABKOT'] : "",
                    _data['custShareholderPersonalAddr'][i]['KABKOT_DESC'] != null ? _data['custShareholderPersonalAddr'][i]['KABKOT_DESC'].toString().replaceAll("'", "`") : "",
                    _data['custShareholderPersonalAddr'][i]['KECAMATAN'] != null ? _data['custShareholderPersonalAddr'][i]['KECAMATAN'] : "",
                    _data['custShareholderPersonalAddr'][i]['KECAMATAN_DESC'] != null ? _data['custShareholderPersonalAddr'][i]['KECAMATAN_DESC'].toString().replaceAll("'", "`") : "",
                    _data['custShareholderPersonalAddr'][i]['KELURAHAN'] != null ? _data['custShareholderPersonalAddr'][i]['KELURAHAN'] : "",
                    _data['custShareholderPersonalAddr'][i]['KELURAHAN_DESC'] != null ? _data['custShareholderPersonalAddr'][i]['KELURAHAN_DESC'].toString().replaceAll("'", "`") : "",
                    _data['custShareholderPersonalAddr'][i]['ZIP_CODE'] != null ? _data['custShareholderPersonalAddr'][i]['ZIP_CODE'] : "",
                    null,
                    _data['custShareholderPersonalAddr'][i]['PHONE'] != null ? _data['custShareholderPersonalAddr'][i]['PHONE'] : "",
                    _data['custShareholderPersonalAddr'][i]['PHONE_AREA'] != null ? _data['custShareholderPersonalAddr'][i]['PHONE_AREA'] : "",
                    _data['custShareholderPersonalAddr'][i]['PHONE_DUA'] != null ? _data['custShareholderPersonalAddr'][i]['PHONE_DUA'] : "",
                    _data['custShareholderPersonalAddr'][i]['PHONE_AREA_DUA'] != null ? _data['custShareholderPersonalAddr'][i]['PHONE_AREA_DUA'] : "",
                    _data['custShareholderPersonalAddr'][i]['FAX'] != null ? _data['custShareholderPersonalAddr'][i]['FAX'] : "",
                    _data['custShareholderPersonalAddr'][i]['FAX_AREA'] != null ? _data['custShareholderPersonalAddr'][i]['FAX_AREA'] : "",
                    _data['custShareholderPersonalAddr'][i]['ADDR_TYPE'] != null ? _data['custShareholderPersonalAddr'][i]['ADDR_TYPE'] : "",
                    _data['custShareholderPersonalAddr'][i]['ADDR_DESC'] != null ? _data['custShareholderPersonalAddr'][i]['ADDR_DESC'] : "",
                    _data['custShareholderPersonalAddr'][i]['LATITUDE'] != null ? _data['custShareholderPersonalAddr'][i]['LATITUDE'] : "",
                    _data['custShareholderPersonalAddr'][i]['LONGITUDE'] != null ? _data['custShareholderPersonalAddr'][i]['LONGITUDE'] : "",
                    _data['custShareholderPersonalAddr'][i]['ADDRESS_FROM_MAP'] != null ? _data['custShareholderPersonalAddr'][i]['ADDRESS_FROM_MAP'] : "",
                    null,
                    null,
                    null,
                    null,
                    0,
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0',
                    '0'
                ));
              }
              await _dbHelper.insertMS2CustAddr(_listAddressPemegangSahamPER);
            }
          }
        }

        // address pemegang saham lembaga
        List<MS2CustAddrModel> _listAddressPemegangSahamCOM = [];
        if(_data['custShareholderCompanyAddr'].isNotEmpty){
          List _dataDakor = [];
          _dataDakor = await _dbHelper.selectMS2CustAddr("12");
          if(_preferences.getString("last_known_state") == "DKR" && _dataDakor.isNotEmpty){
            if(await _dbHelper.deleteMS2CustAddr("12")){
              for (int i = 0; i < _data['custShareholderCompanyAddr'].length; i++) {
                for(int j=0; j < _dataDakor.length; j++){
                  if(_data['custShareholderCompanyAddr'][i]['ADDR_TYPE'] == _dataDakor[j]['addr_type']){
                    _listAddressPemegangSahamCOM.add(MS2CustAddrModel(
                        "",
                        _data['custShareholderCompanyAddr'][i]['ADDRESS_ID'],
                        _data['custShareholderCompanyAddr'][i]['FOREIGN_BK'], //perlu adjust
                        _data['custShareholderCompanyAddr'][i]['ID_NO'], //tambahan id nomor
                        _data['custShareholderCompanyAddr'][i]['KORESPONDEN'], // == "1" ? "true" : "false",
                        "12",
                        _data['custShareholderCompanyAddr'][i]['ADDRESS'] != null ? _data['custShareholderCompanyAddr'][i]['ADDRESS'] : "",
                        _data['custShareholderCompanyAddr'][i]['RT'] != null ? _data['custShareholderCompanyAddr'][i]['RT'] : "",
                        null,
                        _data['custShareholderCompanyAddr'][i]['RW'] != null ? _data['custShareholderCompanyAddr'][i]['RW'] : "",
                        null,
                        _data['custShareholderCompanyAddr'][i]['PROVINSI'] != null ? _data['custShareholderCompanyAddr'][i]['PROVINSI'] : "",
                        _data['custShareholderCompanyAddr'][i]['PROVINSI_DESC'] != null ? _data['custShareholderCompanyAddr'][i]['PROVINSI_DESC'].toString().replaceAll("'", "`") : "",
                        _data['custShareholderCompanyAddr'][i]['KABKOT'] != null ? _data['custShareholderCompanyAddr'][i]['KABKOT'] : "",
                        _data['custShareholderCompanyAddr'][i]['KABKOT_DESC'] != null ? _data['custShareholderCompanyAddr'][i]['KABKOT_DESC'].toString().replaceAll("'", "`") : "",
                        _data['custShareholderCompanyAddr'][i]['KECAMATAN'] != null ? _data['custShareholderCompanyAddr'][i]['KECAMATAN'] : "",
                        _data['custShareholderCompanyAddr'][i]['KECAMATAN_DESC'] != null ? _data['custShareholderCompanyAddr'][i]['KECAMATAN_DESC'].toString().replaceAll("'", "`") : "",
                        _data['custShareholderCompanyAddr'][i]['KELURAHAN'] != null ? _data['custShareholderCompanyAddr'][i]['KELURAHAN'] : "",
                        _data['custShareholderCompanyAddr'][i]['KELURAHAN_DESC'] != null ? _data['custShareholderCompanyAddr'][i]['KELURAHAN_DESC'].toString().replaceAll("'", "`") : "",
                        _data['custShareholderCompanyAddr'][i]['ZIP_CODE'] != null ? _data['custShareholderCompanyAddr'][i]['ZIP_CODE'] : "",
                        null,
                        _data['custShareholderCompanyAddr'][i]['PHONE'] != null ? _data['custShareholderCompanyAddr'][i]['PHONE'] : "",
                        _data['custShareholderCompanyAddr'][i]['PHONE_AREA'] != null ? _data['custShareholderCompanyAddr'][i]['PHONE_AREA'] : "",
                        _data['custShareholderCompanyAddr'][i]['PHONE_DUA'] != null ? _data['custShareholderCompanyAddr'][i]['PHONE_DUA'] : "",
                        _data['custShareholderCompanyAddr'][i]['PHONE_AREA_DUA'] != null ? _data['custShareholderCompanyAddr'][i]['PHONE_AREA_DUA'] : "",
                        _data['custShareholderCompanyAddr'][i]['FAX'] != null ? _data['custShareholderCompanyAddr'][i]['FAX'] : "",
                        _data['custShareholderCompanyAddr'][i]['FAX_AREA'] != null ? _data['custShareholderCompanyAddr'][i]['FAX_AREA'] : "",
                        _data['custShareholderCompanyAddr'][i]['ADDR_TYPE'] != null ? _data['custShareholderCompanyAddr'][i]['ADDR_TYPE'] : "",
                        _data['custShareholderCompanyAddr'][i]['ADDR_DESC'] != null ? _data['custShareholderCompanyAddr'][i]['ADDR_DESC'] : "",
                        _data['custShareholderCompanyAddr'][i]['LATITUDE'] != null ? _data['custShareholderCompanyAddr'][i]['LATITUDE'] : "",
                        _data['custShareholderCompanyAddr'][i]['LONGITUDE'] != null ? _data['custShareholderCompanyAddr'][i]['LONGITUDE'] : "",
                        _data['custShareholderCompanyAddr'][i]['ADDRESS_FROM_MAP'] != null ? _data['custShareholderCompanyAddr'][i]['ADDRESS_FROM_MAP'] : "",
                        null,
                        null,
                        null,
                        null,
                        0,
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_addr_type'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_address'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_rt'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_rw'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_kelurahan'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_kecamatan'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_kabkot'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_provinsi'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_zip_code'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone1_area'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone1'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone_2_area'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone_2'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_fax_area'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_fax'] : "0",
                        _dataDakor.isNotEmpty ? _dataDakor[j]['edit_address_from_map'] : "0"
                    ));
                  }
                }
              }
              await _dbHelper.insertMS2CustAddr(_listAddressPemegangSahamCOM);
            }
          }
          else {
            if(await _dbHelper.deleteMS2CustAddr("12")){
              for (int i = 0; i < _data['custShareholderCompanyAddr'].length; i++) {
                _listAddressPemegangSahamCOM.add(MS2CustAddrModel(
                    "",
                    _data['custShareholderCompanyAddr'][i]['ADDRESS_ID'],
                    _data['custShareholderCompanyAddr'][i]['FOREIGN_BK'], //perlu adjust
                    _data['custShareholderCompanyAddr'][i]['ID_NO'], //tambahan id nomor
                    _data['custShareholderCompanyAddr'][i]['KORESPONDEN'], // == "1" ? "true" : "false",
                    "12",
                    _data['custShareholderCompanyAddr'][i]['ADDRESS'] != null ? _data['custShareholderCompanyAddr'][i]['ADDRESS'] : "",
                    _data['custShareholderCompanyAddr'][i]['RT'] != null ? _data['custShareholderCompanyAddr'][i]['RT'] : "",
                    null,
                    _data['custShareholderCompanyAddr'][i]['RW'] != null ? _data['custShareholderCompanyAddr'][i]['RW'] : "",
                    null,
                    _data['custShareholderCompanyAddr'][i]['PROVINSI'] != null ? _data['custShareholderCompanyAddr'][i]['PROVINSI'] : "",
                    _data['custShareholderCompanyAddr'][i]['PROVINSI_DESC'] != null ? _data['custShareholderCompanyAddr'][i]['PROVINSI_DESC'].toString().replaceAll("'", "`") : "",
                    _data['custShareholderCompanyAddr'][i]['KABKOT'] != null ? _data['custShareholderCompanyAddr'][i]['KABKOT'] : "",
                    _data['custShareholderCompanyAddr'][i]['KABKOT_DESC'] != null ? _data['custShareholderCompanyAddr'][i]['KABKOT_DESC'].toString().replaceAll("'", "`") : "",
                    _data['custShareholderCompanyAddr'][i]['KECAMATAN'] != null ? _data['custShareholderCompanyAddr'][i]['KECAMATAN'] : "",
                    _data['custShareholderCompanyAddr'][i]['KECAMATAN_DESC'] != null ? _data['custShareholderCompanyAddr'][i]['KECAMATAN_DESC'].toString().replaceAll("'", "`") : "",
                    _data['custShareholderCompanyAddr'][i]['KELURAHAN'] != null ? _data['custShareholderCompanyAddr'][i]['KELURAHAN'] : "",
                    _data['custShareholderCompanyAddr'][i]['KELURAHAN_DESC'] != null ? _data['custShareholderCompanyAddr'][i]['KELURAHAN_DESC'].toString().replaceAll("'", "`") : "",
                    _data['custShareholderCompanyAddr'][i]['ZIP_CODE'] != null ? _data['custShareholderCompanyAddr'][i]['ZIP_CODE'] : "",
                    null,
                    _data['custShareholderCompanyAddr'][i]['PHONE'] != null ? _data['custShareholderCompanyAddr'][i]['PHONE'] : "",
                    _data['custShareholderCompanyAddr'][i]['PHONE_AREA'] != null ? _data['custShareholderCompanyAddr'][i]['PHONE_AREA'] : "",
                    _data['custShareholderCompanyAddr'][i]['PHONE_DUA'] != null ? _data['custShareholderCompanyAddr'][i]['PHONE_DUA'] : "",
                    _data['custShareholderCompanyAddr'][i]['PHONE_AREA_DUA'] != null ? _data['custShareholderCompanyAddr'][i]['PHONE_AREA_DUA'] : "",
                    _data['custShareholderCompanyAddr'][i]['FAX'] != null ? _data['custShareholderCompanyAddr'][i]['FAX'] : "",
                    _data['custShareholderCompanyAddr'][i]['FAX_AREA'] != null ? _data['custShareholderCompanyAddr'][i]['FAX_AREA'] : "",
                    _data['custShareholderCompanyAddr'][i]['ADDR_TYPE'] != null ? _data['custShareholderCompanyAddr'][i]['ADDR_TYPE'] : "",
                    _data['custShareholderCompanyAddr'][i]['ADDR_DESC'] != null ? _data['custShareholderCompanyAddr'][i]['ADDR_DESC'] : "",
                    _data['custShareholderCompanyAddr'][i]['LATITUDE'] != null ? _data['custShareholderCompanyAddr'][i]['LATITUDE'] : "",
                    _data['custShareholderCompanyAddr'][i]['LONGITUDE'] != null ? _data['custShareholderCompanyAddr'][i]['LONGITUDE'] : "",
                    _data['custShareholderCompanyAddr'][i]['ADDRESS_FROM_MAP'] != null ? _data['custShareholderCompanyAddr'][i]['ADDRESS_FROM_MAP'] : "",
                    null,
                    null,
                    null,
                    null,
                    0,
                    "0",
                    "0",
                    "0",
                    "0",
                    "0",
                    "0",
                    "0",
                    "0",
                    "0",
                    "0",
                    "0",
                    "0",
                    "0",
                    "0",
                    "0",
                    "0"
                ));
              }
              await _dbHelper.insertMS2CustAddr(_listAddressPemegangSahamCOM);
            }
          }
        }
      }

      //income
      List<MS2CustIncomeModel> _listIncome = [];
      if(_data['custIncome'].isNotEmpty){
        List _dataDakor = [];
        _dataDakor = await _dbHelper.selectDataIncome();
        if(_preferences.getString("last_known_state") == "DKR" && _dataDakor.isNotEmpty){
          if(await _dbHelper.deleteMS2CustIncome()){
            for(int i=0; i<_data['custIncome'].length; i++){
              for(int j=0; j < _dataDakor.length; j++){
                if(_data['custIncome'][i]['INCOME_TYPE'] == _dataDakor[j]['income_type']){
                  _listIncome.add(MS2CustIncomeModel(
                      "123",
                      _data['custIncome'][i]['CUST_INCOME_ID'],
                      _data['custIncome'][i]['TYPE_INCOME_FRML'] != null ? _data['custIncome'][i]['TYPE_INCOME_FRML'] : "",
                      _data['custIncome'][i]['INCOME_TYPE'] != null ? _data['custIncome'][i]['INCOME_TYPE'].toString() : "",
                      _data['custIncome'][i]['INCOME_DESC'] != null ? _data['custIncome'][i]['INCOME_DESC'] : "",
                      _data['custIncome'][i]['INCOME_VALUE'] != null ? _data['custIncome'][i]['INCOME_VALUE'].toString() : "",
                      null,
                      null,
                      null,
                      null,
                      1,
                      i,
                      _dataDakor.isNotEmpty ? _dataDakor[j]['income_type'] : "0")
                  );
                }
              }
            }
            await _dbHelper.insertMS2CustIncome(_listIncome);
          }
        }
        else {
          if(await _dbHelper.deleteMS2CustIncome()){
            for(int i=0; i<_data['custIncome'].length; i++){
              _listIncome.add(MS2CustIncomeModel(
                  "123",
                  _data['custIncome'][i]['CUST_INCOME_ID'],
                  _data['custIncome'][i]['TYPE_INCOME_FRML'] != null ? _data['custIncome'][i]['TYPE_INCOME_FRML'] : "",
                  _data['custIncome'][i]['INCOME_TYPE'] != null ? _data['custIncome'][i]['INCOME_TYPE'].toString() : "",
                  _data['custIncome'][i]['INCOME_DESC'] != null ? _data['custIncome'][i]['INCOME_DESC'] : "",
                  _data['custIncome'][i]['INCOME_VALUE'] != null ? _data['custIncome'][i]['INCOME_VALUE'].toString() : "",
                  null,
                  null,
                  null,
                  null,
                  1,
                  i,
                  "0"));
            }
            await _dbHelper.insertMS2CustIncome(_listIncome);
          }
        }
      }

      //guarantor personal
      List<MS2GrntrIndModel> _listDataIndividu = [];
      if(_data['custGuarantorPersonal'].isNotEmpty){
        List _dataDakor = [];
        _dataDakor = await _dbHelper.selectGuarantorInd();
        if(_preferences.getString("last_known_state") == "DKR" && _dataDakor.isNotEmpty) {
          if(await _dbHelper.deleteMS2GrntrInd()){
            for(int i=0; i<_data['custGuarantorPersonal'].length;i++){
              for(int j=0; j < _dataDakor.length; j++) {
                if(_data['custGuarantorPersonal'][i]['GRNTR_IND_ID'] == _dataDakor[j]['guarantorIndividualID']) {
                  _listDataIndividu.add(MS2GrntrIndModel(
                    null,
                    _data['custGuarantorPersonal'][i]['GRNTR_IND_ID'] != null ? _data['custGuarantorPersonal'][i]['GRNTR_IND_ID'] : "",
                    _data['custGuarantorPersonal'][i]['RELATION_STATUS'] != null ? _data['custGuarantorPersonal'][i]['RELATION_STATUS'] : "",
                    _data['custGuarantorPersonal'][i]['RELATION_STATUS_DESC'] != null ? _data['custGuarantorPersonal'][i]['RELATION_STATUS_DESC'] : "",
                    0,
                    _data['custGuarantorPersonal'][i]['ID_TYPE'] != null ? _data['custGuarantorPersonal'][i]['ID_TYPE'] : "",
                    _data['custGuarantorPersonal'][i]['ID_DESC'] != null ? _data['custGuarantorPersonal'][i]['ID_DESC'] : "",
                    _data['custGuarantorPersonal'][i]['ID_NO'] != null ? _data['custGuarantorPersonal'][i]['ID_NO'] : "",
                    _data['custGuarantorPersonal'][i]['FULL_NAME_ID'] != null ? _data['custGuarantorPersonal'][i]['FULL_NAME_ID'] : "",
                    _data['custGuarantorPersonal'][i]['FULL_NAME'] != null ? _data['custGuarantorPersonal'][i]['FULL_NAME'] : "",
                    null,
                    null,
                    _data['custGuarantorPersonal'][i]['DATE_OF_BIRTH'] != null ? _data['custGuarantorPersonal'][i]['DATE_OF_BIRTH'] : "",
                    _data['custGuarantorPersonal'][i]['PLACE_OF_BIRTH'] != null ? _data['custGuarantorPersonal'][i]['PLACE_OF_BIRTH'] : "",
                    _data['custGuarantorPersonal'][i]['PLACE_OF_BIRTH_KABKOTA'] != null ? _data['custGuarantorPersonal'][i]['PLACE_OF_BIRTH_KABKOTA'] : "",
                    _data['custGuarantorPersonal'][i]['PLACE_OF_BIRTH_KABKOTA_DESC'] != null ? _data['custGuarantorPersonal'][i]['PLACE_OF_BIRTH_KABKOTA_DESC'] : "",
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    _data['custGuarantorPersonal'][i]['GENDER'] != null ? _data['custGuarantorPersonal'][i]['GENDER'] : "",
                    _data['custGuarantorPersonal'][i]['HANDPHONE_NO'] != null  && _data['custGuarantorPersonal'][i]['HANDPHONE_NO'].toString().length > 3 ? _data['custGuarantorPersonal'][i]['HANDPHONE_NO'].toString().replaceRange(0, 2, "") : "",
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_relation_status'] : "0",
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_id_type'] : "0",
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_id_no'] : "0",
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_full_name_id'] : "0",
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_full_name'] : "0",
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_date_of_birth'] : "0",
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_place_of_birth'] : "0",
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_place_of_birth_kabkota'] : "0",
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_gender'] : "0",
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_handphone_no'] : "0",
                  ));
                }
              }
            }
            await _dbHelper.insertMS2GrntrInd(_listDataIndividu);
          }
        }
        else {
          if(await _dbHelper.deleteMS2GrntrInd()){
            for(int i=0; i<_data['custGuarantorPersonal'].length;i++){
              _listDataIndividu.add(MS2GrntrIndModel(
                null,
                _data['custGuarantorPersonal'][i]['GRNTR_IND_ID'],
                _data['custGuarantorPersonal'][i]['RELATION_STATUS'] != null ? _data['custGuarantorPersonal'][i]['RELATION_STATUS'] : "",
                _data['custGuarantorPersonal'][i]['RELATION_STATUS_DESC'] != null ? _data['custGuarantorPersonal'][i]['RELATION_STATUS_DESC'] : "",
                0,
                _data['custGuarantorPersonal'][i]['ID_TYPE'] != null ? _data['custGuarantorPersonal'][i]['ID_TYPE'] : "",
                _data['custGuarantorPersonal'][i]['ID_DESC'] != null ? _data['custGuarantorPersonal'][i]['ID_DESC'] : "",
                _data['custGuarantorPersonal'][i]['ID_NO'] != null ? _data['custGuarantorPersonal'][i]['ID_NO'] : "",
                _data['custGuarantorPersonal'][i]['FULL_NAME_ID'] != null ? _data['custGuarantorPersonal'][i]['FULL_NAME_ID'] : "",
                _data['custGuarantorPersonal'][i]['FULL_NAME'] != null ? _data['custGuarantorPersonal'][i]['FULL_NAME'] : "",
                null,
                null,
                _data['custGuarantorPersonal'][i]['DATE_OF_BIRTH'] != null ? _data['custGuarantorPersonal'][i]['DATE_OF_BIRTH'] : "",
                _data['custGuarantorPersonal'][i]['PLACE_OF_BIRTH'] != null ? _data['custGuarantorPersonal'][i]['PLACE_OF_BIRTH'] : "",
                _data['custGuarantorPersonal'][i]['PLACE_OF_BIRTH_KABKOTA'] != null ? _data['custGuarantorPersonal'][i]['PLACE_OF_BIRTH_KABKOTA'] : "",
                _data['custGuarantorPersonal'][i]['PLACE_OF_BIRTH_KABKOTA_DESC'] != null ? _data['custGuarantorPersonal'][i]['PLACE_OF_BIRTH_KABKOTA_DESC'] : "",
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                _data['custGuarantorPersonal'][i]['GENDER'] != null ? _data['custGuarantorPersonal'][i]['GENDER'] : "",
                _data['custGuarantorPersonal'][i]['HANDPHONE_NO'] != null  && _data['custGuarantorPersonal'][i]['HANDPHONE_NO'].toString().length > 3 ? _data['custGuarantorPersonal'][i]['HANDPHONE_NO'].toString().replaceRange(0, 2, "") : "",
                null,
                null,
                null,
                null,
                null,
                null,
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
              ));
            }
            await _dbHelper.insertMS2GrntrInd(_listDataIndividu);
          }
        }
      }

      //guarantor company
      List<MS2GrntrCompModel> _listDataCompany = [];
      if(_data['custGuarantorCompany'].isNotEmpty){
        List _dataDakor = [];
        _dataDakor = await _dbHelper.selectMS2ApplObject();
        if(_preferences.getString("last_known_state") == "DKR" && _dataDakor.isNotEmpty) {
          if(await _dbHelper.deleteMS2GrntrComp()){
            for(int i=0; i < _data['custGuarantorCompany'].length;i++){
              for(int j=0; j < _dataDakor.length; j++) {
                if(_data['custGuarantorCompany'][i]['GRNTR_COMP_ID'] == _dataDakor[j]['guarantorCorporateID']) {
                  _listDataCompany.add(MS2GrntrCompModel(
                      null,
                      _data['custGuarantorCompany'][i]['GRNTR_COMP_ID'],
                      0,
                      _data['custGuarantorCompany'][i]['COMP_TYPE'] != null ? _data['custGuarantorCompany'][i]['COMP_TYPE'] : "",
                      _data['custGuarantorCompany'][i]['COMP_DESC'] != null ? _data['custGuarantorCompany'][i]['COMP_DESC'] : "",
                      _data['custGuarantorCompany'][i]['PROFIL'] != null ? _data['custGuarantorCompany'][i]['PROFIL'] : "",
                      _data['custGuarantorCompany'][i]['PROFIL_DESC'] != null ? _data['custGuarantorCompany'][i]['PROFIL_DESC'] : "",
                      _data['custGuarantorCompany'][i]['COMP_NAME'] != null ? _data['custGuarantorCompany'][i]['COMP_NAME'] : "",
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      _data['custGuarantorCompany'][i]['ESTABLISH_DATE'],//dateFormat.format(DateTime.parse(_data['custGuarantorCompany'][i]['ESTABLISH_DATE'])),
                      null,
                      null,
                      null,
                      null,
                      null,
                      _data['custGuarantorCompany'][i]['NPWP_NO'] != null ? _data['custGuarantorCompany'][i]['NPWP_NO'].replaceAll(".", "").replaceAll("-", "") : "",
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_comp_type'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_comp_name'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_establish_date'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_npwp_no'] : "0"
                  ));
                }
              }
            }
            await _dbHelper.insertMS2GrntrComp(_listDataCompany);
          }
        }
        else {
          if(await _dbHelper.deleteMS2GrntrComp()){
            for(int i=0; i < _data['custGuarantorCompany'].length;i++){
              _listDataCompany.add(MS2GrntrCompModel(
                  null,
                  _data['custGuarantorCompany'][i]['GRNTR_COMP_ID'],
                  0,
                  _data['custGuarantorCompany'][i]['COMP_TYPE'] != null ? _data['custGuarantorCompany'][i]['COMP_TYPE'] : "",
                  _data['custGuarantorCompany'][i]['COMP_DESC'] != null ? _data['custGuarantorCompany'][i]['COMP_DESC'] : "",
                  _data['custGuarantorCompany'][i]['PROFIL'] != null ? _data['custGuarantorCompany'][i]['PROFIL'] : "",
                  _data['custGuarantorCompany'][i]['PROFIL_DESC'] != null ? _data['custGuarantorCompany'][i]['PROFIL_DESC'] : "",
                  _data['custGuarantorCompany'][i]['COMP_NAME'] != null ? _data['custGuarantorCompany'][i]['COMP_NAME'] : "",
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  _data['custGuarantorCompany'][i]['ESTABLISH_DATE'],//dateFormat.format(DateTime.parse(_data['custGuarantorCompany'][i]['ESTABLISH_DATE'])),
                  null,
                  null,
                  null,
                  null,
                  null,
                  _data['custGuarantorCompany'][i]['NPWP_NO'] != null ? _data['custGuarantorCompany'][i]['NPWP_NO'].replaceAll(".", "").replaceAll("-", "") : "",
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  null,
                  "0",
                  "0",
                  "0",
                  "0"
              ));
            }
            await _dbHelper.insertMS2GrntrComp(_listDataCompany);
          }
        }
      }

      //guarantor company address
      List<MS2CustAddrModel> _listAddressGuarantorCom = [];
      if(_data['custGuarantorCompanyAddr'].isNotEmpty){
        List _dataDakor = [];
        custType == "PER" ? _dataDakor = await _dbHelper.selectMS2CustAddr("2") : _dataDakor = await _dbHelper.selectMS2CustAddr("8");
        if(_preferences.getString("last_known_state") == "DKR" && _dataDakor.isNotEmpty) {
          if(await _dbHelper.deleteMS2CustAddr(custType == "PER" ? "2" : "8")){
            for(int i=0; i<_data['custGuarantorCompanyAddr'].length; i++) {
              for(int j=0; j < _dataDakor.length; j++) {
                if(_data['custGuarantorCompanyAddr'][i]['ADDR_TYPE'] == _dataDakor[j]['addr_type']) {
                  debugPrint("CEK_GET_DATA_ID_NO_ALAMAT_LEMBAGA_PENJAMIN ${_data['custGuarantorCompanyAddr'][i]['ID_NO']}");
                  _listAddressGuarantorCom.add(MS2CustAddrModel(
                      "123",
                      _data['custGuarantorCompanyAddr'][i]['ADDRESS_ID'],
                      _data['custGuarantorCompanyAddr'][i]['FOREIGN_BK'], //perlu adjust
                      _data['custGuarantorCompanyAddr'][i]['ID_NO'], //tambahan id nomor
                      _data['custGuarantorCompanyAddr'][i]['KORESPONDEN'], //== "1" ? "true" : "false",
                      custType == "PER" ? "2" : "8",
                      _data['custGuarantorCompanyAddr'][i]['ADDRESS'] != null ? _data['custGuarantorCompanyAddr'][i]['ADDRESS'] : "",
                      _data['custGuarantorCompanyAddr'][i]['RT'] != null ? _data['custGuarantorCompanyAddr'][i]['RT'] : "",
                      null,
                      _data['custGuarantorCompanyAddr'][i]['RW'],
                      null,
                      _data['custGuarantorCompanyAddr'][i]['PROVINSI'] != null ? _data['custGuarantorCompanyAddr'][i]['PROVINSI'] : "",
                      _data['custGuarantorCompanyAddr'][i]['PROVINSI_DESC'] != null ? _data['custGuarantorCompanyAddr'][i]['PROVINSI_DESC'].toString().replaceAll("'", "`") : "",
                      _data['custGuarantorCompanyAddr'][i]['KABKOT'] != null ? _data['custGuarantorCompanyAddr'][i]['KABKOT'] : "",
                      _data['custGuarantorCompanyAddr'][i]['KABKOT_DESC'] != null ? _data['custGuarantorCompanyAddr'][i]['KABKOT_DESC'].toString().replaceAll("'", "`") : "",
                      _data['custGuarantorCompanyAddr'][i]['KECAMATAN'] != null ? _data['custGuarantorCompanyAddr'][i]['KECAMATAN'] : "",
                      _data['custGuarantorCompanyAddr'][i]['KECAMATAN_DESC'] != null ? _data['custGuarantorCompanyAddr'][i]['KECAMATAN_DESC'].toString().replaceAll("'", "`") : "",
                      _data['custGuarantorCompanyAddr'][i]['KELURAHAN'] != null ? _data['custGuarantorCompanyAddr'][i]['KELURAHAN'] : "",
                      _data['custGuarantorCompanyAddr'][i]['KELURAHAN_DESC'] != null ? _data['custGuarantorCompanyAddr'][i]['KELURAHAN_DESC'].toString().replaceAll("'", "`") : "",
                      _data['custGuarantorCompanyAddr'][i]['ZIP_CODE'] != null ? _data['custGuarantorCompanyAddr'][i]['ZIP_CODE'] : "",
                      null,
                      _data['custGuarantorCompanyAddr'][i]['PHONE'] != null ? _data['custGuarantorCompanyAddr'][i]['PHONE'] : "",
                      _data['custGuarantorCompanyAddr'][i]['PHONE_AREA'] != null ? _data['custGuarantorCompanyAddr'][i]['PHONE_AREA'] : "",
                      _data['custGuarantorCompanyAddr'][i]['PHONE_DUA'] != null ? _data['custGuarantorCompanyAddr'][i]['PHONE_DUA'] : "",
                      _data['custGuarantorCompanyAddr'][i]['PHONE_AREA_DUA'] != null ? _data['custGuarantorCompanyAddr'][i]['PHONE_AREA_DUA'] : "",
                      _data['custGuarantorCompanyAddr'][i]['FAX'] != null ? _data['custGuarantorCompanyAddr'][i]['FAX'] : "",
                      _data['custGuarantorCompanyAddr'][i]['FAX_AREA'] != null ? _data['custGuarantorCompanyAddr'][i]['FAX_AREA'] : "",
                      _data['custGuarantorCompanyAddr'][i]['ADDR_TYPE'] != null ? _data['custGuarantorCompanyAddr'][i]['ADDR_TYPE'] : "",
                      _data['custGuarantorCompanyAddr'][i]['ADDR_DESC'] != null ? _data['custGuarantorCompanyAddr'][i]['ADDR_DESC'] : "",
                      _data['custGuarantorCompanyAddr'][i]['LATITUDE'] != null ? _data['custGuarantorCompanyAddr'][i]['LATITUDE'] : "",
                      _data['custGuarantorCompanyAddr'][i]['LONGITUDE'] != null ? _data['custGuarantorCompanyAddr'][i]['LONGITUDE'] : "",
                      _data['custGuarantorCompanyAddr'][i]['ADDRESS_FROM_MAP'] != null ? _data['custGuarantorCompanyAddr'][i]['ADDRESS_FROM_MAP'] : "",
                      null,
                      null,
                      null,
                      null,
                      0,
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_addr_type'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_address'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_rt'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_rw'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_kelurahan'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_kecamatan'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_kabkot'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_provinsi'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_zip_code'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone1_area'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone1'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone_2_area'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone_2'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_fax_area'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_fax'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_address_from_map'] : "0"));
                }
              }
            }
            await _dbHelper.insertMS2CustAddr(_listAddressGuarantorCom);
          }
        }
        else {
          if(await _dbHelper.deleteMS2CustAddr(custType == "PER" ? "2" : "8")){
            for(int i=0; i<_data['custGuarantorCompanyAddr'].length; i++) {
              _listAddressGuarantorCom.add(MS2CustAddrModel(
                  "123",
                  _data['custGuarantorCompanyAddr'][i]['ADDRESS_ID'],
                  _data['custGuarantorCompanyAddr'][i]['FOREIGN_BK'], //perlu adjust
                  _data['custGuarantorCompanyAddr'][i]['ID_NO'], //tambahan id nomor
                  _data['custGuarantorCompanyAddr'][i]['KORESPONDEN'], //== "1" ? "true" : "false",
                  custType == "PER" ? "2" : "8",
                  _data['custGuarantorCompanyAddr'][i]['ADDRESS'] != null ? _data['custGuarantorCompanyAddr'][i]['ADDRESS'] : "",
                  _data['custGuarantorCompanyAddr'][i]['RT'] != null ? _data['custGuarantorCompanyAddr'][i]['RT'] : "",
                  null,
                  _data['custGuarantorCompanyAddr'][i]['RW'],
                  null,
                  _data['custGuarantorCompanyAddr'][i]['PROVINSI'] != null ? _data['custGuarantorCompanyAddr'][i]['PROVINSI'] : "",
                  _data['custGuarantorCompanyAddr'][i]['PROVINSI_DESC'] != null ? _data['custGuarantorCompanyAddr'][i]['PROVINSI_DESC'].toString().replaceAll("'", "`") : "",
                  _data['custGuarantorCompanyAddr'][i]['KABKOT'] != null ? _data['custGuarantorCompanyAddr'][i]['KABKOT'] : "",
                  _data['custGuarantorCompanyAddr'][i]['KABKOT_DESC'] != null ? _data['custGuarantorCompanyAddr'][i]['KABKOT_DESC'].toString().replaceAll("'", "`") : "",
                  _data['custGuarantorCompanyAddr'][i]['KECAMATAN'] != null ? _data['custGuarantorCompanyAddr'][i]['KECAMATAN'] : "",
                  _data['custGuarantorCompanyAddr'][i]['KECAMATAN_DESC'] != null ? _data['custGuarantorCompanyAddr'][i]['KECAMATAN_DESC'].toString().replaceAll("'", "`") : "",
                  _data['custGuarantorCompanyAddr'][i]['KELURAHAN'] != null ? _data['custGuarantorCompanyAddr'][i]['KELURAHAN'] : "",
                  _data['custGuarantorCompanyAddr'][i]['KELURAHAN_DESC'] != null ? _data['custGuarantorCompanyAddr'][i]['KELURAHAN_DESC'].toString().replaceAll("'", "`") : "",
                  _data['custGuarantorCompanyAddr'][i]['ZIP_CODE'] != null ? _data['custGuarantorCompanyAddr'][i]['ZIP_CODE'] : "",
                  null,
                  _data['custGuarantorCompanyAddr'][i]['PHONE'] != null ? _data['custGuarantorCompanyAddr'][i]['PHONE'] : "",
                  _data['custGuarantorCompanyAddr'][i]['PHONE_AREA'] != null ? _data['custGuarantorCompanyAddr'][i]['PHONE_AREA'] : "",
                  _data['custGuarantorCompanyAddr'][i]['PHONE_DUA'] != null ? _data['custGuarantorCompanyAddr'][i]['PHONE_DUA'] : "",
                  _data['custGuarantorCompanyAddr'][i]['PHONE_AREA_DUA'] != null ? _data['custGuarantorCompanyAddr'][i]['PHONE_AREA_DUA'] : "",
                  _data['custGuarantorCompanyAddr'][i]['FAX'] != null ? _data['custGuarantorCompanyAddr'][i]['FAX'] : "",
                  _data['custGuarantorCompanyAddr'][i]['FAX_AREA'] != null ? _data['custGuarantorCompanyAddr'][i]['FAX_AREA'] : "",
                  _data['custGuarantorCompanyAddr'][i]['ADDR_TYPE'] != null ? _data['custGuarantorCompanyAddr'][i]['ADDR_TYPE'] : "",
                  _data['custGuarantorCompanyAddr'][i]['ADDR_DESC'] != null ? _data['custGuarantorCompanyAddr'][i]['ADDR_DESC'] : "",
                  _data['custGuarantorCompanyAddr'][i]['LATITUDE'] != null ? _data['custGuarantorCompanyAddr'][i]['LATITUDE'] : "",
                  _data['custGuarantorCompanyAddr'][i]['LONGITUDE'] != null ? _data['custGuarantorCompanyAddr'][i]['LONGITUDE'] : "",
                  _data['custGuarantorCompanyAddr'][i]['ADDRESS_FROM_MAP'] != null ? _data['custGuarantorCompanyAddr'][i]['ADDRESS_FROM_MAP'] : "",
                  null,
                  null,
                  null,
                  null,
                  0,
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0"));
            }
            await _dbHelper.insertMS2CustAddr(_listAddressGuarantorCom);
          }
        }
      }

      //guarantor personal address
      List<MS2CustAddrModel> _listAddressGuarantorPer = [];
      if(_data['custGuarantorPersonalAddr'].isNotEmpty){
        List _dataDakor = [];
        custType == "PER" ? _dataDakor = await _dbHelper.selectMS2CustAddr("1") : _dataDakor = await _dbHelper.selectMS2CustAddr("7");
        if(_preferences.getString("last_known_state") == "DKR" && _dataDakor.isNotEmpty) {
          if(await _dbHelper.deleteMS2CustAddr(custType == "PER" ? "1" : "7")){
            for(int i=0; i < _data['custGuarantorPersonalAddr'].length; i++) {
              for(int j=0; j < _dataDakor.length; j++) {
                if(_data['custGuarantorPersonalAddr'][i]['ADDR_TYPE'] == _dataDakor[j]['addr_type']) {
                  _listAddressGuarantorPer.add(MS2CustAddrModel(
                      "123",
                      _data['custGuarantorPersonalAddr'][i]['ADDRESS_ID'],
                      _data['custGuarantorPersonalAddr'][i]['FOREIGN_BK'], //perlu adjust
                      _data['custGuarantorPersonalAddr'][i]['ID_NO'], //tambahan id nomor
                      _data['custGuarantorPersonalAddr'][i]['KORESPONDEN'], //== "1" ? "true" : "false",
                      custType == "PER" ? "1" : "7",
                      _data['custGuarantorPersonalAddr'][i]['ADDRESS'],
                      _data['custGuarantorPersonalAddr'][i]['RT'],
                      null,
                      _data['custGuarantorPersonalAddr'][i]['RW'],
                      null,
                      _data['custGuarantorPersonalAddr'][i]['PROVINSI'] != null ? _data['custGuarantorPersonalAddr'][i]['PROVINSI'] : "",
                      _data['custGuarantorPersonalAddr'][i]['PROVINSI_DESC'] != null ? _data['custGuarantorPersonalAddr'][i]['PROVINSI_DESC'].toString().replaceAll("'", "`") : "",
                      _data['custGuarantorPersonalAddr'][i]['KABKOT'] != null ? _data['custGuarantorPersonalAddr'][i]['KABKOT'] : "",
                      _data['custGuarantorPersonalAddr'][i]['KABKOT_DESC'] != null ? _data['custGuarantorPersonalAddr'][i]['KABKOT_DESC'].toString().replaceAll("'", "`") : "",
                      _data['custGuarantorPersonalAddr'][i]['KECAMATAN'] != null ? _data['custGuarantorPersonalAddr'][i]['KECAMATAN'] : "",
                      _data['custGuarantorPersonalAddr'][i]['KECAMATAN_DESC'] != null ? _data['custGuarantorPersonalAddr'][i]['KECAMATAN_DESC'].toString().replaceAll("'", "`") : "",
                      _data['custGuarantorPersonalAddr'][i]['KELURAHAN'] != null ? _data['custGuarantorPersonalAddr'][i]['KELURAHAN'] : "",
                      _data['custGuarantorPersonalAddr'][i]['KELURAHAN_DESC'] != null ? _data['custGuarantorPersonalAddr'][i]['KELURAHAN_DESC'].toString().replaceAll("'", "`") : "",
                      _data['custGuarantorPersonalAddr'][i]['ZIP_CODE'] != null ? _data['custGuarantorPersonalAddr'][i]['ZIP_CODE'] : "",
                      null,
                      _data['custGuarantorPersonalAddr'][i]['PHONE'] != null ? _data['custGuarantorPersonalAddr'][i]['PHONE'] : "",
                      _data['custGuarantorPersonalAddr'][i]['PHONE_AREA'] != null ? _data['custGuarantorPersonalAddr'][i]['PHONE_AREA'] : "",
                      _data['custGuarantorPersonalAddr'][i]['PHONE_DUA'] != null ? _data['custGuarantorPersonalAddr'][i]['PHONE_DUA'] : "",
                      _data['custGuarantorPersonalAddr'][i]['PHONE_AREA_DUA'] != null ? _data['custGuarantorPersonalAddr'][i]['PHONE_AREA_DUA'] : "",
                      _data['custGuarantorPersonalAddr'][i]['FAX'] != null ? _data['custGuarantorPersonalAddr'][i]['FAX'] : "",
                      _data['custGuarantorPersonalAddr'][i]['FAX_AREA'] != null ? _data['custGuarantorPersonalAddr'][i]['FAX_AREA'] : "",
                      _data['custGuarantorPersonalAddr'][i]['ADDR_TYPE'] != null ? _data['custGuarantorPersonalAddr'][i]['ADDR_TYPE'] : "",
                      _data['custGuarantorPersonalAddr'][i]['ADDR_DESC'] != null ? _data['custGuarantorPersonalAddr'][i]['ADDR_DESC'] : "",
                      _data['custGuarantorPersonalAddr'][i]['LATITUDE'] != null ? _data['custGuarantorPersonalAddr'][i]['LATITUDE'] : "",
                      _data['custGuarantorPersonalAddr'][i]['LONGITUDE'] != null ? _data['custGuarantorPersonalAddr'][i]['LONGITUDE'] : "",
                      _data['custGuarantorPersonalAddr'][i]['ADDRESS_FROM_MAP'] != null ? _data['custGuarantorPersonalAddr'][i]['ADDRESS_FROM_MAP'] : "",
                      null,
                      null,
                      null,
                      null,
                      0,
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_addr_type'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_address'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_rt'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_rw'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_kelurahan'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_kecamatan'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_kabkot'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_provinsi'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_zip_code'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone1_area'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone1'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone_2_area'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone_2'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_fax_area'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_fax'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_address_from_map'] : "0"));
                }
              }
            }
            await _dbHelper.insertMS2CustAddr(_listAddressGuarantorPer);
          }
        }
        else {
          if(await _dbHelper.deleteMS2CustAddr(custType == "PER" ? "1" : "7")){
            for(int i=0; i < _data['custGuarantorPersonalAddr'].length; i++) {
              _listAddressGuarantorPer.add(MS2CustAddrModel(
                  "123",
                  _data['custGuarantorPersonalAddr'][i]['ADDRESS_ID'],
                  _data['custGuarantorPersonalAddr'][i]['FOREIGN_BK'], //perlu adjust
                  _data['custGuarantorPersonalAddr'][i]['ID_NO'], //tambahan id nomor
                  _data['custGuarantorPersonalAddr'][i]['KORESPONDEN'], //== "1" ? "true" : "false",
                  custType == "PER" ? "1" : "7",
                  _data['custGuarantorPersonalAddr'][i]['ADDRESS'],
                  _data['custGuarantorPersonalAddr'][i]['RT'],
                  null,
                  _data['custGuarantorPersonalAddr'][i]['RW'],
                  null,
                  _data['custGuarantorPersonalAddr'][i]['PROVINSI'] != null ? _data['custGuarantorPersonalAddr'][i]['PROVINSI'] : "",
                  _data['custGuarantorPersonalAddr'][i]['PROVINSI_DESC'] != null ? _data['custGuarantorPersonalAddr'][i]['PROVINSI_DESC'].toString().replaceAll("'", "`") : "",
                  _data['custGuarantorPersonalAddr'][i]['KABKOT'] != null ? _data['custGuarantorPersonalAddr'][i]['KABKOT'] : "",
                  _data['custGuarantorPersonalAddr'][i]['KABKOT_DESC'] != null ? _data['custGuarantorPersonalAddr'][i]['KABKOT_DESC'].toString().replaceAll("'", "`") : "",
                  _data['custGuarantorPersonalAddr'][i]['KECAMATAN'] != null ? _data['custGuarantorPersonalAddr'][i]['KECAMATAN'] : "",
                  _data['custGuarantorPersonalAddr'][i]['KECAMATAN_DESC'] != null ? _data['custGuarantorPersonalAddr'][i]['KECAMATAN_DESC'].toString().replaceAll("'", "`") : "",
                  _data['custGuarantorPersonalAddr'][i]['KELURAHAN'] != null ? _data['custGuarantorPersonalAddr'][i]['KELURAHAN'] : "",
                  _data['custGuarantorPersonalAddr'][i]['KELURAHAN_DESC'] != null ? _data['custGuarantorPersonalAddr'][i]['KELURAHAN_DESC'].toString().replaceAll("'", "`") : "",
                  _data['custGuarantorPersonalAddr'][i]['ZIP_CODE'] != null ? _data['custGuarantorPersonalAddr'][i]['ZIP_CODE'] : "",
                  null,
                  _data['custGuarantorPersonalAddr'][i]['PHONE'] != null ? _data['custGuarantorPersonalAddr'][i]['PHONE'] : "",
                  _data['custGuarantorPersonalAddr'][i]['PHONE_AREA'] != null ? _data['custGuarantorPersonalAddr'][i]['PHONE_AREA'] : "",
                  _data['custGuarantorPersonalAddr'][i]['PHONE_DUA'] != null ? _data['custGuarantorPersonalAddr'][i]['PHONE_DUA'] : "",
                  _data['custGuarantorPersonalAddr'][i]['PHONE_AREA_DUA'] != null ? _data['custGuarantorPersonalAddr'][i]['PHONE_AREA_DUA'] : "",
                  _data['custGuarantorPersonalAddr'][i]['FAX'] != null ? _data['custGuarantorPersonalAddr'][i]['FAX'] : "",
                  _data['custGuarantorPersonalAddr'][i]['FAX_AREA'] != null ? _data['custGuarantorPersonalAddr'][i]['FAX_AREA'] : "",
                  _data['custGuarantorPersonalAddr'][i]['ADDR_TYPE'] != null ? _data['custGuarantorPersonalAddr'][i]['ADDR_TYPE'] : "",
                  _data['custGuarantorPersonalAddr'][i]['ADDR_DESC'] != null ? _data['custGuarantorPersonalAddr'][i]['ADDR_DESC'] : "",
                  _data['custGuarantorPersonalAddr'][i]['LATITUDE'] != null ? _data['custGuarantorPersonalAddr'][i]['LATITUDE'] : "",
                  _data['custGuarantorPersonalAddr'][i]['LONGITUDE'] != null ? _data['custGuarantorPersonalAddr'][i]['LONGITUDE'] : "",
                  _data['custGuarantorPersonalAddr'][i]['ADDRESS_FROM_MAP'] != null ? _data['custGuarantorPersonalAddr'][i]['ADDRESS_FROM_MAP'] : "",
                  null,
                  null,
                  null,
                  null,
                  0,
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0"));
            }
            await _dbHelper.insertMS2CustAddr(_listAddressGuarantorPer);
          }
        }
      }

      //info app
      if(_data['custApplication'].isNotEmpty){
        List _dataDakor = [];
        if(_preferences.getString("last_known_state") == "DKR") {
          _dataDakor = await _dbHelper.selectMS2Application();
        }
        if(await _dbHelper.deleteMS2Application()){
          await _dbHelper.insertMS2Application(MS2ApplicationModel(
              "",
              typeFormId != "IDE" ? _data['custApplication'][0]['APPL_NO'] : null,
              null,
              null,
              null,
              _data['custApplication'][0]['ORDER_DATE'] != null ? _data['custApplication'][0]['ORDER_DATE'] : "",
              null,
              _data['custApplication'][0]['SVY_APPOINTMENT_DATE'] != null ? _data['custApplication'][0]['SVY_APPOINTMENT_DATE'] : "",
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              _data['custApplication'][0]['INITIAL_RECOMENDATION'] != null ? _data['custApplication'][0]['INITIAL_RECOMENDATION'] : "",
              _data['custApplication'][0]['FINAL_RECOMENDATION'] != null ? _data['custApplication'][0]['FINAL_RECOMENDATION'] : "",
              _data['custApplication'][0]['BRMS_SCORING'] != null ? int.parse(_data['custApplication'][0]['BRMS_SCORING'].toString()) : null,
              null,
              _data['custApplication'][0]['SIGN_PK'] != null ? _data['custApplication'][0]['SIGN_PK'] : 0,
              null,
              null,
              _data['custApplication'][0]['KONSEP_TYPE'] != null ? _data['custApplication'][0]['KONSEP_TYPE'] : "",
              _data['custApplication'][0]['KONSEP_TYPE_DESC'].toString() != null ? _data['custApplication'][0]['KONSEP_TYPE_DESC'].toString() : "",
              _data['custApplication'][0]['OBJT_QTY'] != null ? _data['custApplication'][0]['OBJT_QTY'] : null,
              _data['custApplication'][0]['PROP_INSR_TYPE'] != null ? _data['custApplication'][0]['PROP_INSR_TYPE'] : "",
              _data['custApplication'][0]['PROP_INSR_TYPE_DESC'].toString() != null ? _data['custApplication'][0]['PROP_INSR_TYPE_DESC'].toString() : "",
              _data['custApplication'][0]['UNIT'] != null ? _data['custApplication'][0]['UNIT'] : "",
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              _data['custApplication'][0]['DEALER_NOTE'] != null ? _data['custApplication'][0]['DEALER_NOTE'] : "", //marketing notes
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_order_date'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_svy_appointment_date'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_sign_pk'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_konsep_type'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_objt_qty'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_prop_insr_type'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_unit'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_marketing_notes'] : "0" //marketing notes
          ));
        }
      }

      // unit object
      if(_data['custObject'].isNotEmpty){
        List _dataDakor = [];
        if(_preferences.getString("last_known_state") == "DKR") {
          _dataDakor = await _dbHelper.selectMS2ApplObject();
        }
        if(await _dbHelper.deleteMS2ApplObject()){
          debugPrint("CEK_THIRD_PARTY_TYPE_DESC ${_data['custObject'][0]['THIRD_PARTY_TYPE_DESC']}");
          await _dbHelper.insertMS2ApplObject(MS2ApplObjectModel(
              "",
              _data['custObject'][0]['APPL_OBJT_ID'],
              _data['custObject'][0]['FINANCING_TYPE'] != null ? _data['custObject'][0]['FINANCING_TYPE'].toString() : "",
              _data['custObject'][0]['BUSS_ACTIVITIES'] != null ? _data['custObject'][0]['BUSS_ACTIVITIES'].toString() : "",
              _data['custObject'][0]['BUSS_ACTIVITIES_DESC'] != null ? _data['custObject'][0]['BUSS_ACTIVITIES_DESC'].toString() : "",
              _data['custObject'][0]['BUSS_ACTIVITIES_TYPE'] != null ? _data['custObject'][0]['BUSS_ACTIVITIES_TYPE'].toString() : "",
              _data['custObject'][0]['BUSS_ACTIVITIES_TYPE_DESC'] != null ? _data['custObject'][0]['BUSS_ACTIVITIES_TYPE_DESC'].toString() : "",
              _data['custObject'][0]['GROUP_OBJECT'] != null ? _data['custObject'][0]['GROUP_OBJECT'].toString() : "",
              _data['custObject'][0]['GROUP_OBJECT_DESC'] != null ? _data['custObject'][0]['GROUP_OBJECT_DESC'].toString() : "",
              _data['custObject'][0]['OBJECT'] != null ? _data['custObject'][0]['OBJECT'].toString() : "",
              _data['custObject'][0]['OBJECT_DESC'] != null ? _data['custObject'][0]['OBJECT_DESC'].toString() : "",
              _data['custObject'][0]['PRODUCT_TYPE'] != null ? _data['custObject'][0]['PRODUCT_TYPE'].toString() : "",
              _data['custObject'][0]['PRODUCT_TYPE_DESC'] != null ? _data['custObject'][0]['PRODUCT_TYPE_DESC'].toString() : "",
              _data['custObject'][0]['BRAND_OBJECT'] != null ? _data['custObject'][0]['BRAND_OBJECT'].toString() : "",
              _data['custObject'][0]['BRAND_OBJECT_DESC'] != null ? _data['custObject'][0]['BRAND_OBJECT_DESC'].toString() : "",
              _data['custObject'][0]['OBJECT_TYPE'] != null ? _data['custObject'][0]['OBJECT_TYPE'].toString() : "",
              _data['custObject'][0]['OBJECT_TYPE_DESC'] != null ? _data['custObject'][0]['OBJECT_TYPE_DESC'].toString() : "",
              _data['custObject'][0]['OBJECT_MODEL'] != null ? _data['custObject'][0]['OBJECT_MODEL'].toString() : "",
              _data['custObject'][0]['OBJECT_MODEL_DESC'] != null ? _data['custObject'][0]['OBJECT_MODEL_DESC'].toString() : "",
              "", //_data['custObject'][0]['MODEL_DETAIL'] != null ? _data['custObject'][0]['MODEL_DETAIL'].toString() : "",
              _data['custObject'][0]['OBJECT_USED'] != null ? _data['custObject'][0]['OBJECT_USED'].toString() : "",
              _data['custObject'][0]['OBJECT_USED_DESC'] != null ? _data['custObject'][0]['OBJECT_USED_DESC'].toString() : "",
              _data['custObject'][0]['OBJECT_PURPOSE'] != null ? _data['custObject'][0]['OBJECT_PURPOSE'].toString() : "",
              _data['custObject'][0]['OBJECT_PURPOSE_DESC'] != null ? _data['custObject'][0]['OBJECT_PURPOSE_DESC'].toString() : "",
              _data['custObject'][0]['GROUP_SALES'] != null ? _data['custObject'][0]['GROUP_SALES'].toString() : "",
              _data['custObject'][0]['GROUP_SALES_DESC'] != null ? _data['custObject'][0]['GROUP_SALES_DESC'].toString() : "",
              _data['custObject'][0]['GROUP_ID'] != null ? _data['custObject'][0]['GROUP_ID'].toString() : "",
              _data['custObject'][0]['GROUP_ID_DESC'] != null ? _data['custObject'][0]['GROUP_ID_DESC'].toString() : "",
              _data['custObject'][0]['ORDER_SOURCE'] != null ? _data['custObject'][0]['ORDER_SOURCE'].toString() : "",
              _data['custObject'][0]['ORDER_SOURCE_NAME'] != null ? _data['custObject'][0]['ORDER_SOURCE_NAME'].toString() : "",
              null,
              _data['custObject'][0]['THIRD_PARTY_TYPE'] != null ? _data['custObject'][0]['THIRD_PARTY_TYPE'].toString() : "",
              _data['custObject'][0]['THIRD_PARTY_TYPE_DESC'] != null ? _data['custObject'][0]['THIRD_PARTY_TYPE_DESC'].toString() : "",
              _data['custObject'][0]['THIRD_PARTY_ID'] != null ? _data['custObject'][0]['THIRD_PARTY_ID'].toString() : "",
              _data['custObject'][0]['WORK'] != null ? _data['custObject'][0]['WORK'].toString() : "",
              _data['custObject'][0]['WORK_DESC'] != null ? _data['custObject'][0]['WORK_DESC'].toString() : "",
              _data['custObject'][0]['SENTRAD'] != null ? _data['custObject'][0]['SENTRAD'].toString() : "",
              _data['custObject'][0]['SENTRAD_DESC'] != null ? _data['custObject'][0]['SENTRAD_DESC'].toString() : "",
              _data['custObject'][0]['UNITD'] != null ? _data['custObject'][0]['UNITD'].toString() : "",
              _data['custObject'][0]['UNITD_DESC'] != null ? _data['custObject'][0]['UNITD_DESC'].toString() : "",
              _data['custObject'][0]['PROGRAM'] != null ? _data['custObject'][0]['PROGRAM'].toString() : "",
              _data['custObject'][0]['PROGRAM_DESC'] != null ? _data['custObject'][0]['PROGRAM_DESC'].toString() : "",
              _data['custObject'][0]['REHAB_TYPE'] != null ? _data['custObject'][0]['REHAB_TYPE'].toString() : "",
              _data['custObject'][0]['REHAB_TYPE_DESC'] != null ? _data['custObject'][0]['REHAB_TYPE_DESC'].toString() : "",
              _data['custObject'][0]['REFERENCE_NO'] != null ? _data['custObject'][0]['REFERENCE_NO'].toString() : "",
              _data['custObject'][0]['REFERENCE_DESC'] != null ? _data['custObject'][0]['REFERENCE_DESC'].toString() : "",
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              _data['custObject'][0]['DSR'] != null ? double.parse(_data['custObject'][0]['DSR']) : null,
              _data['custObject'][0]['DIR'] != null ? double.parse(_data['custObject'][0]['DIR']) : null,
              _data['custObject'][0]['DSC'] != null ? double.parse(_data['custObject'][0]['DSC']) : null,
              _data['custObject'][0]['IRR'] != null ? double.parse(_data['custObject'][0]['IRR']) : null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              DateTime.now().toString(),
              null,
              null,
              null,
              1,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              _data['custObject'][0]['APPL_MODEL_DTL'] != null ? _data['custObject'][0]['APPL_MODEL_DTL'].toString() : "", // model detail yang dipakai
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              _data['custObject'][0]['DEALER_MATRIX'] != null ? _data['custObject'][0]['DEALER_MATRIX'].toString() : "",
              _data['custObject'][0]['DEALER_MATRIX_DESC'] != null ? _data['custObject'][0]['DEALER_MATRIX_DESC'].toString() : "",
              null,
              null,
              null,
              null,
              null,
              null,
              _data['custObject'][0]['ORDER_SOURCE_DESC'] != null ? _data['custObject'][0]['ORDER_SOURCE_DESC'].toString() : "",
              _data['custObject'][0]['ORDER_SOURCE_NAME_DESC'] != null ? _data['custObject'][0]['ORDER_SOURCE_NAME_DESC'].toString() : "",
              _data['custObject'][0]['THIRD_PARTY_ID_DESC'] != null ? _data['custObject'][0]['THIRD_PARTY_ID_DESC'].toString() : "",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_financing_type'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_buss_activities'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_buss_activities_type'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_group_object'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_object'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_product_type'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_brand_object'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_object_type'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_object_model'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_model_detail'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_object_used'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_object_purpose'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_group_sales'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_grup_id'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_order_source'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_name_order_source'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_third_party_type'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_third_party_id'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_dealer_matrix'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_work'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_sentra_d'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_unit_d'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_program'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_rehab_type'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[0]['edit_reference_no'] : "0"
          ));
        }
      }

      // structure credit
      if(_data['custStrukturKredit'].isNotEmpty){
        await _dbHelper.updateMS2ApplObjectInfStrukturKredit(MS2ApplObjectModel(
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            _data['custStrukturKredit'][0]['INSTALLMENT_TYPE'] != null ? _data['custStrukturKredit'][0]['INSTALLMENT_TYPE'] : "",
            _data['custStrukturKredit'][0]['INSTALLMENT_TYPE_DESC'] != null ? _data['custStrukturKredit'][0]['INSTALLMENT_TYPE_DESC'] : "",
            _data['custStrukturKredit'][0]['APPL_TOP'] != null ? _data['custStrukturKredit'][0]['APPL_TOP'] : null, //int.parse
            _data['custStrukturKredit'][0]['PAYMENT_METHOD'] != null ? _data['custStrukturKredit'][0]['PAYMENT_METHOD'] : "",
            _data['custStrukturKredit'][0]['PAYMENT_METHOD_DESC'] != null ? _data['custStrukturKredit'][0]['PAYMENT_METHOD_DESC'] : "",
            _data['custStrukturKredit'][0]['EFF_RATE'] != null ? double.parse("${_data['custStrukturKredit'][0]['EFF_RATE']}") : null,
            _data['custStrukturKredit'][0]['FLAT_RATE'] != null ? double.parse("${_data['custStrukturKredit'][0]['FLAT_RATE']}") : null,
            _data['custStrukturKredit'][0]['OBJECT_PRICE'] != null ? double.parse("${_data['custStrukturKredit'][0]['OBJECT_PRICE']}") : null,
            _data['custStrukturKredit'][0]['KAROSERI_PRICE'] != null ? double.parse("${_data['custStrukturKredit'][0]['KAROSERI_PRICE']}") : null,
            _data['custStrukturKredit'][0]['TOTAL_AMT'] != null ? double.parse("${_data['custStrukturKredit'][0]['TOTAL_AMT']}") : null,
            _data['custStrukturKredit'][0]['DP_NET'] != null ? double.parse("${_data['custStrukturKredit'][0]['DP_NET']}") : null,
            null,// installment decline n
            _data['custStrukturKredit'][0]['PAYMENT_PER_YEAR'] != null ? int.parse("${_data['custStrukturKredit'][0]['PAYMENT_PER_YEAR']}") : null,
            _data['custStrukturKredit'][0]['DP_BRANCH'] != null ? double.parse("${_data['custStrukturKredit'][0]['DP_BRANCH']}") : null,
            _data['custStrukturKredit'][0]['DP_GROSS'] != null ? double.parse("${_data['custStrukturKredit'][0]['DP_GROSS']}") : null,
            _data['custStrukturKredit'][0]['PRINCIPAL_AMT'] != null ? double.parse("${_data['custStrukturKredit'][0]['PRINCIPAL_AMT']}") : null,
            _data['custStrukturKredit'][0]['INSTALLMENT_PAID'] != null ? double.parse("${_data['custStrukturKredit'][0]['INSTALLMENT_PAID']}") : null,
            _data['custStrukturKredit'][0]['INTEREST_AMT'] != null ? int.parse("${_data['custStrukturKredit'][0]['INTEREST_AMT']}") : null,
            _data['custStrukturKredit'][0]['LTV'] != null  ? double.parse("${_data['custStrukturKredit'][0]['LTV']}") : null,// ltv
            null,
            null,
            null,
            null,
            null, // gp_type
            null,
            null,
            null,
            _data['custStrukturKredit'][0]['BALLOON_TYPE'] != null  ? _data['custStrukturKredit'][0]['BALLOON_TYPE'] : null, // balloon payment
            _data['custStrukturKredit'][0]['LAST_PERCEN_INSTALLMENT'] != null  ? double.parse("${_data['custStrukturKredit'][0]['LAST_PERCEN_INSTALLMENT']}") : null,
            _data['custStrukturKredit'][0]['LAST_INSTALLMENT_AMT'] != null  ? double.parse("${_data['custStrukturKredit'][0]['LAST_INSTALLMENT_AMT']}") : null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            DateTime.now().toString(),
            _preferences.getString("username"),
            1,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null
        ));
      }

      // colla oto
      if(_data['custCollaOto'].isNotEmpty){
        List _dataDakor = [];
        if(_preferences.getString("last_known_state") == "DKR") {
          _dataDakor = await _dbHelper.selectDataCollaOto();
        }
        if(await _dbHelper.deleteApplCollaOto()){
          await _dbHelper.insertMS2ApplObjtCollOto(MS2ApplObjtCollOtoModel(
            "",
            _data['custCollaOto'][0]['COLL_OTO_ID'],
            _data['custCollaOto'][0]['FLAG_MULTI_COLL'] != null ? int.parse(_data['custCollaOto'][0]['FLAG_MULTI_COLL']) : null,
            _data['custCollaOto'][0]['FLAG_COLL_IS_UNIT'] != null ? int.parse(_data['custCollaOto'][0]['FLAG_COLL_IS_UNIT']) : null,
            _data['custCollaOto'][0]['FLAG_COLL_NAME_IS_APPL'] != null ? int.parse(_data['custCollaOto'][0]['FLAG_COLL_NAME_IS_APPL']) : null,
            _data['custCollaOto'][0]['ID_TYPE'] != null ? _data['custCollaOto'][0]['ID_TYPE'] : "",
            _data['custCollaOto'][0]['ID_TYPE_DESC'] != null ? _data['custCollaOto'][0]['ID_TYPE_DESC'] : "",
            _data['custCollaOto'][0]['ID_NO'] != null ? _data['custCollaOto'][0]['ID_NO'] : "",
            _data['custCollaOto'][0]['COLLA_NAME'] != null ? _data['custCollaOto'][0]['COLLA_NAME'] : "",
            _data['custCollaOto'][0]['DATE_OF_BIRTH'] != null ? _data['custCollaOto'][0]['DATE_OF_BIRTH'] : "",
            _data['custCollaOto'][0]['PLACE_OF_BIRTH'] != null ? _data['custCollaOto'][0]['PLACE_OF_BIRTH'] : "",
            _data['custCollaOto'][0]['PLACE_OF_BIRTH_KABKOTA'] != null ? _data['custCollaOto'][0]['PLACE_OF_BIRTH_KABKOTA'] : "",
            _data['custCollaOto'][0]['PLACE_OF_BIRTH_KABKOTA_DESC'] != null ? _data['custCollaOto'][0]['PLACE_OF_BIRTH_KABKOTA_DESC'] : "",
            _data['custCollaOto'][0]['GROUP_OBJECT'] != null ? _data['custCollaOto'][0]['GROUP_OBJECT'] : "",
            _data['custCollaOto'][0]['GROUP_OBJECT_DESC'] != null ? _data['custCollaOto'][0]['GROUP_OBJECT_DESC'] : "",
            _data['custCollaOto'][0]['OBJECT'] != null ? _data['custCollaOto'][0]['OBJECT'] : "",
            _data['custCollaOto'][0]['OBJECT_DESC'] != null ? _data['custCollaOto'][0]['OBJECT_DESC'] : "",
            "",//_data['custCollaOto'][0]['PRODUCT_TYPE'] != null ? _data['custCollaOto'][0]['PRODUCT_TYPE'] : "",
            "",//_data['custCollaOto'][0]['PRODUCT_TYPE_DESC'] != null ? _data['custCollaOto'][0]['PRODUCT_TYPE_DESC'] : "",
            _data['custCollaOto'][0]['BRAND_OBJECT'] != null ? _data['custCollaOto'][0]['BRAND_OBJECT'] : "",
            _data['custCollaOto'][0]['BRAND_OBJECT_DESC'] != null ? _data['custCollaOto'][0]['BRAND_OBJECT_DESC'] : "",
            _data['custCollaOto'][0]['OBJECT_TYPE'] != null ? _data['custCollaOto'][0]['OBJECT_TYPE'] : "",
            _data['custCollaOto'][0]['OBJECT_TYPE_DESC'] != null ? _data['custCollaOto'][0]['OBJECT_TYPE_DESC'] : "",
            _data['custCollaOto'][0]['OBJECT_MODEL'] != null ? _data['custCollaOto'][0]['OBJECT_MODEL'] : "",
            _data['custCollaOto'][0]['OBJECT_MODEL_DESC'] != null ? _data['custCollaOto'][0]['OBJECT_MODEL_DESC'] : "",
            _data['custCollaOto'][0]['OBJECT_PURPOSE'] != null ? _data['custCollaOto'][0]['OBJECT_PURPOSE'] : "",
            _data['custCollaOto'][0]['OBJECT_PURPOSE_DESC'] != null ? _data['custCollaOto'][0]['OBJECT_PURPOSE_DESC'] : "",
            _data['custCollaOto'][0]['MFG_YEAR'] != null ? int.parse(_data['custCollaOto'][0]['MFG_YEAR']) : null,
            _data['custCollaOto'][0]['REGISTRATION_YEAR'] != null ? int.parse(_data['custCollaOto'][0]['REGISTRATION_YEAR']) : null,
            _data['custCollaOto'][0]['YELLOW_PLATE'] != null ? int.parse(_data['custCollaOto'][0]['YELLOW_PLATE']) : null,
            _data['custCollaOto'][0]['BUILT_UP'] != null ? int.parse(_data['custCollaOto'][0]['BUILT_UP']) : null,
            _data['custCollaOto'][0]['BPKB_NO'] != null ? _data['custCollaOto'][0]['BPKB_NO'] : "",
            _data['custCollaOto'][0]['FRAME_NO'] != null ? _data['custCollaOto'][0]['FRAME_NO'] : "",
            _data['custCollaOto'][0]['ENGINE_NO'] != null ? _data['custCollaOto'][0]['ENGINE_NO'] : "",
            _data['custCollaOto'][0]['POLICE_NO'] != null ? _data['custCollaOto'][0]['POLICE_NO'] : "",
            _data['custCollaOto'][0]['GRADE_UNIT'] != null ? _data['custCollaOto'][0]['GRADE_UNIT'] : "",
            _data['custCollaOto'][0]['UTJ_FACILITIES'] != null ? _data['custCollaOto'][0]['UTJ_FACILITIES'] == "1" ? "TUNAI" : "TOP" : "",
            _data['custCollaOto'][0]['BIDDER_NAME'] != null ? _data['custCollaOto'][0]['BIDDER_NAME'] : "",
            _data['custCollaOto'][0]['SHOWROOM_PRICE'] != null ? double.parse(_data['custCollaOto'][0]['SHOWROOM_PRICE'].toString().replaceAll(",", "")) : null,
            _data['custCollaOto'][0]['ADD_EQUIP'] != null ? _data['custCollaOto'][0]['ADD_EQUIP'] : "",
            _data['custCollaOto'][0]['MP_ADIRA'] != null ? double.parse(_data['custCollaOto'][0]['MP_ADIRA'].toString().replaceAll(",", "")) : null,
            _data['custCollaOto'][0]['REKONDISI_FISIK'] != null ? double.parse(_data['custCollaOto'][0]['REKONDISI_FISIK'].toString().replaceAll(",", "")) : null,
            _data['custCollaOto'][0]['RESULT'] != null ? int.parse(_data['custCollaOto'][0]['RESULT']) : null,
            null,
            _data['custCollaOto'][0]['DP_JAMINAN'] != null ? double.parse(_data['custCollaOto'][0]['DP_JAMINAN'].toString().replaceAll(",", "")) : null,
            _data['custCollaOto'][0]['MAX_PH'] != null ? double.parse(_data['custCollaOto'][0]['MAX_PH'].toString().replaceAll(",", "")) : null,
            _data['custCollaOto'][0]['TAKSASI_PRICE'] != null ? double.parse(_data['custCollaOto'][0]['TAKSASI_PRICE'].toString().replaceAll(",", "")) : null,
            _data['custCollaOto'][0]['COLA_PURPOSE'] != null ? _data['custCollaOto'][0]['COLA_PURPOSE'] : "",
            _data['custCollaOto'][0]['COLA_PURPOSE_DESC'] != null ? _data['custCollaOto'][0]['COLA_PURPOSE_DESC'] : "",
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            1,
            null,
            null,
            null,
            null,
            null,
            null,
            _data['custCollaOto'][0]['MP_ADIRA_UPLD'] != null ? int.parse(_data['custCollaOto'][0]['MP_ADIRA_UPLD']) : null,
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_flag_multi_coll'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_flag_coll_is_unit'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_flag_coll_name_is_appl'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_id_type'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_id_no'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_colla_name'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_date_of_birth'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_place_of_birth'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_place_of_birth_kabkota'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_group_object'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_object'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_product_type'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_brand_object'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_object_type'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_object_model'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_object_purpose'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_mfg_year'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_registration_year'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_yellow_plate'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_built_up'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_bpkp_no'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_frame_no'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_engine_no'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_police_no'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_grade_unit'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_utj_facilities'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_bidder_name'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_showroom_price'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_add_equip'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_mp_adira'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_mp_adira_upld'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_rekondisi_fisik'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_result'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_ltv'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_dp_jaminan'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_max_ph'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_taksasi_price'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_cola_purpose'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_capacity'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_color'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_made_in'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_serial_no'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_no_invoice'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_stnk_valid'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_bpkp_address'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_bpkb_id_type'] : "0",
          ));
        }
      }

      // colla prop
      if(_data['custCollaProp'].isNotEmpty){
        List _dataDakor = [];
        if(_preferences.getString("last_known_state") == "DKR") {
          _dataDakor = await _dbHelper.selectDataCollaProp();
        }
        if(await _dbHelper.deleteApplCollaProp()){
          await _dbHelper.insertMS2ApplObjtCollProp(MS2ApplObjtCollPropModel(
            null,
            _data['custCollaProp'][0]['COLL_PROP_ID'],
            _data['custCollaProp'][0]['FLAG_MULTI_COLL'] != null ? int.parse(_data['custCollaProp'][0]['FLAG_MULTI_COLL']) : null,
            _data['custCollaProp'][0]['FLAG_COLL_NAME_IS_APPL'] != null ? int.parse(_data['custCollaProp'][0]['FLAG_COLL_NAME_IS_APPL']) : null,
            _data['custCollaProp'][0]['ID_TYPE'] != null ? _data['custCollaProp'][0]['ID_TYPE'] : "",
            _data['custCollaProp'][0]['ID_TYPE_DESC'] != null ? _data['custCollaProp'][0]['ID_TYPE_DESC'] : "",
            _data['custCollaProp'][0]['ID_NO'] != null ? _data['custCollaProp'][0]['ID_NO'] : "",
            _data['custCollaProp'][0]['COLA_NAME'] != null ? _data['custCollaProp'][0]['COLA_NAME'] : "",
            _data['custCollaProp'][0]['DATE_OF_BIRTH'] != null ? _data['custCollaProp'][0]['DATE_OF_BIRTH'] : "",
            _data['custCollaProp'][0]['PLACE_OF_BIRTH'] != null ? _data['custCollaProp'][0]['PLACE_OF_BIRTH'] : "",
            _data['custCollaProp'][0]['PLACE_OF_BIRTH_KABKOTA'] != null ? _data['custCollaProp'][0]['PLACE_OF_BIRTH_KABKOTA'] : "",
            _data['custCollaProp'][0]['PLACE_OF_BIRTH_KABKOTA_DESC'] != null ? _data['custCollaProp'][0]['PLACE_OF_BIRTH_KABKOTA_DESC'] : "",
            _data['custCollaProp'][0]['SERTIFIKAT_NO'] != null ? _data['custCollaProp'][0]['SERTIFIKAT_NO'] : "",
            _data['custCollaProp'][0]['SERTIFIKAT_TYPE'] != null ? _data['custCollaProp'][0]['SERTIFIKAT_TYPE'] : "",
            _data['custCollaProp'][0]['SERTIFIKAT_TYPE_DESC'] != null ? _data['custCollaProp'][0]['SERTIFIKAT_TYPE_DESC'] : "",
            _data['custCollaProp'][0]['PROPERTY_TYPE'] != null ? _data['custCollaProp'][0]['PROPERTY_TYPE'] : "",
            _data['custCollaProp'][0]['PROPERTY_TYPE_DESC'] != null ? _data['custCollaProp'][0]['PROPERTY_TYPE_DESC'] : "",
            _data['custCollaProp'][0]['BUILDING_AREA'] != null ? double.parse(_data['custCollaProp'][0]['BUILDING_AREA']) : null,
            _data['custCollaProp'][0]['LAND_AREA'] != null ? double.parse(_data['custCollaProp'][0]['LAND_AREA']) : null,
            _data['custCollaProp'][0]['DP_GUARANTEE'] != null ? double.parse(_data['custCollaProp'][0]['DP_GUARANTEE']) : null,
            _data['custCollaProp'][0]['MAX_PH'] != null ? double.parse(_data['custCollaProp'][0]['MAX_PH']) : null,
            _data['custCollaProp'][0]['TAKSASI_PRICE'] != null ? double.parse(_data['custCollaProp'][0]['TAKSASI_PRICE']) : null,
            _data['custCollaProp'][0]['COLA_PURPOSE'] != null ? _data['custCollaProp'][0]['COLA_PURPOSE'] : "",
            _data['custCollaProp'][0]['COLA_PURPOSE_DESC'] != null ? _data['custCollaProp'][0]['COLA_PURPOSE_DESC'] : "",
            _data['custCollaProp'][0]['JARAK_FASUM_POSITIF'] != null ? double.parse(_data['custCollaProp'][0]['JARAK_FASUM_POSITIF']) : null,
            _data['custCollaProp'][0]['JARAK_FASUM_NEGATIF'] != null ? double.parse(_data['custCollaProp'][0]['JARAK_FASUM_NEGATIF']) : null,
            _data['custCollaProp'][0]['LAND_PRICE'] != null ? double.parse(_data['custCollaProp'][0]['LAND_PRICE']) : null,
            _data['custCollaProp'][0]['NJOP_PRICE'] != null ? double.parse(_data['custCollaProp'][0]['NJOP_PRICE']) : null,
            _data['custCollaProp'][0]['BUILDING_PRICE'] != null ? double.parse(_data['custCollaProp'][0]['BUILDING_PRICE']) : null,
            _data['custCollaProp'][0]['ROOF_TYPE'] != null ? _data['custCollaProp'][0]['ROOF_TYPE']: "",
            _data['custCollaProp'][0]['ROOF_TYPE_DESC'] != null ? _data['custCollaProp'][0]['ROOF_TYPE_DESC']: "",
            _data['custCollaProp'][0]['WALL_TYPE'] != null ? _data['custCollaProp'][0]['WALL_TYPE']: "",
            _data['custCollaProp'][0]['WALL_TYPE_DESC'] != null ? _data['custCollaProp'][0]['WALL_TYPE_DESC']: "",
            _data['custCollaProp'][0]['FLOOR_TYPE'] != null ? _data['custCollaProp'][0]['FLOOR_TYPE']: "",
            _data['custCollaProp'][0]['FLOOR_TYPE_DESC'] != null ? _data['custCollaProp'][0]['FLOOR_TYPE_DESC']: "",
            _data['custCollaProp'][0]['FOUNDATION_TYPE'] != null ? _data['custCollaProp'][0]['FOUNDATION_TYPE']: "",
            _data['custCollaProp'][0]['FOUNDATION_TYPE_DESC'] != null ? _data['custCollaProp'][0]['FOUNDATION_TYPE_DESC']: "",
            _data['custCollaProp'][0]['ROAD_TYPE'] != null ? _data['custCollaProp'][0]['ROAD_TYPE']: "",
            _data['custCollaProp'][0]['ROAD_TYPE_DESC'] != null ? _data['custCollaProp'][0]['ROAD_TYPE_DESC']: "",
            _data['custCollaProp'][0]['FLAG_CAR_PAS'] != null ? int.parse(_data['custCollaProp'][0]['FLAG_CAR_PAS']) : null,
            _data['custCollaProp'][0]['NO_OF_HOUSE'] != null ? int.parse(_data['custCollaProp'][0]['NO_OF_HOUSE']) : null,
            _data['custCollaProp'][0]['GUARANTEE_CHARACTER'] != null ? _data['custCollaProp'][0]['GUARANTEE_CHARACTER'] : "",
            _data['custCollaProp'][0]['GRNTR_EVDNCE_OWNR'] != null ? _data['custCollaProp'][0]['GRNTR_EVDNCE_OWNR'] : "",
            _data['custCollaProp'][0]['PBLSH_SERTIFIKAT_DATE'] != null ? _data['custCollaProp'][0]['PBLSH_SERTIFIKAT_DATE'] : "",
            _data['custCollaProp'][0]['PBLSH_SERTIFIKAT_YEAR'] != null ? int.parse(_data['custCollaProp'][0]['PBLSH_SERTIFIKAT_YEAR']) : null,
            _data['custCollaProp'][0]['LICENSE_NAME'] != null ? _data['custCollaProp'][0]['LICENSE_NAME'] : "",
            _data['custCollaProp'][0]['NO_SRT_UKUR'] != null ? _data['custCollaProp'][0]['NO_SRT_UKUR'] : "",
            _data['custCollaProp'][0]['TGL_SRT_UKUR'] != null ? _data['custCollaProp'][0]['TGL_SRT_UKUR'] : "",
            _data['custCollaProp'][0]['SRTFKT_PBLSH_BY'] != null ? _data['custCollaProp'][0]['SRTFKT_PBLSH_BY'] : "",
            _data['custCollaProp'][0]['EXPIRE_RIGHT'] != null ? _data['custCollaProp'][0]['EXPIRE_RIGHT'] : "",
            _data['custCollaProp'][0]['IMB_NO'] != null ? _data['custCollaProp'][0]['IMB_NO'] : "",
            _data['custCollaProp'][0]['IMB_DATE'] != null ? _data['custCollaProp'][0]['IMB_DATE'] : "",
            _data['custCollaProp'][0]['SIZE_OF_IMB'] != null ? double.parse( _data['custCollaProp'][0]['SIZE_OF_IMB'].toString().replaceAll(",", "")) : null,
            _data['custCollaProp'][0]['LTV'] != null ? double.parse(_data['custCollaProp'][0]['LTV'].toString().replaceAll(",", "")) : null,
            null,
            1,
            DateTime.now().toString(),
            null,
            null,
            null,
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_flag_multi_coll'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_flag_coll_name_is_appl'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_id_type'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_id_no'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_cola_name'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_date_of_birth'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_place_of_birth'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_place_of_birth_kabkota'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_sertifikat_no'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_seritifikat_type'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_property_type'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_building_area'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_land_area'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_dp_guarantee'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_max_ph'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_taksasi_price'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_cola_purpose'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_jarak_fasum_positif'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_jarak_fasum_negatif'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_land_price'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_njop_price'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_building_price'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_roof_type'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_wall_type'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_floor_type'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_foundation_type'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_road_type'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_flag_car_pas'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_no_of_house'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_guarantee_character'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_grntr_evdnce_ownr'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_pblsh_sertifikat_date'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_pblsh_sertifikat_year'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_license_name'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_no_srt_ukur'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_tgl_srt_ukur'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_srtfkt_pblsh_by'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_expire_right'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_imb_no'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_imb_date'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_size_of_imb'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_ltv'] : "0",
            _dataDakor.isNotEmpty ? _dataDakor[0]['edit_land_location'] : "0",
          ));
        }
      }

      // alamat Colla prop
      List<MS2CustAddrModel> _listAddressCollateralProp = [];
      List _dataDakor = [];
      custType == "PER" ? _dataDakor = await _dbHelper.selectMS2CustAddr("6") : _dataDakor = await _dbHelper.selectMS2CustAddr("13");
      if(_preferences.getString("last_known_state") == "DKR" && _dataDakor.isNotEmpty) {
        if(await _dbHelper.deleteMS2CustAddr(custType == "PER" ? "6" : "13")) {
          if(_data['custCollaPropAddr'].isNotEmpty){
            for(int i=0; i < _data['custCollaPropAddr'].length; i++) {
              for(int j=0; j < _dataDakor.length; j++) {
                if(_data['custCollaPropAddr'][i]['ADDR_TYPE'] == _dataDakor[j]['addr_type']) {
                  _listAddressCollateralProp.add(MS2CustAddrModel(
                      "123",
                      _data['custCollaPropAddr'][i]['ADDRESS_ID'],
                      _data['custCollaPropAddr'][i]['FOREIGN_BK'], //perlu adjust
                      _data['custCollaPropAddr'][i]['ID_NO'], //tambahan id nomor
                      _data['custCollaPropAddr'][i]['KORESPONDEN'], //== "1" ? "true" : "false",
                      custType == "PER" ? "6" : "13",
                      _data['custCollaPropAddr'][i]['ADDRESS'],
                      _data['custCollaPropAddr'][i]['RT'],
                      null,
                      _data['custCollaPropAddr'][i]['RW'],
                      null,
                      _data['custCollaPropAddr'][i]['PROVINSI'],
                      _data['custCollaPropAddr'][i]['PROVINSI_DESC'],
                      _data['custCollaPropAddr'][i]['KABKOT'],
                      _data['custCollaPropAddr'][i]['KABKOT_DESC'],
                      _data['custCollaPropAddr'][i]['KECAMATAN'],
                      _data['custCollaPropAddr'][i]['KECAMATAN_DESC'],
                      _data['custCollaPropAddr'][i]['KELURAHAN'],
                      _data['custCollaPropAddr'][i]['KELURAHAN_DESC'],
                      _data['custCollaPropAddr'][i]['ZIP_CODE'],
                      null,
                      _data['custCollaPropAddr'][i]['PHONE'],
                      _data['custCollaPropAddr'][i]['PHONE_AREA'],
                      _data['custCollaPropAddr'][i]['PHONE_DUA'],
                      _data['custCollaPropAddr'][i]['PHONE_AREA_DUA'],
                      _data['custCollaPropAddr'][i]['FAX'],
                      _data['custCollaPropAddr'][i]['FAX_AREA'],
                      _data['custCollaPropAddr'][i]['ADDR_TYPE'],
                      _data['custCollaPropAddr'][i]['ADDR_DESC'],
                      _data['custCollaPropAddr'][i]['LATITUDE'],
                      _data['custCollaPropAddr'][i]['LONGITUDE'],
                      _data['custCollaPropAddr'][i]['ADDRESS_FROM_MAP'],
                      null,
                      null,
                      null,
                      null,
                      0,
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_addr_type'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_address'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_rt'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_rw'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_kelurahan'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_kecamatan'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_kabkot'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_provinsi'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_zip_code'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone1_area'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone1'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone_2_area'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_phone_2'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_fax_area'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_fax'] : "0",
                      _dataDakor.isNotEmpty ? _dataDakor[j]['edit_address_from_map'] : "0"));
                }
              }
            }
            await _dbHelper.insertMS2CustAddr(_listAddressCollateralProp);
          }
        }
      }
      else {
        if(await _dbHelper.deleteMS2CustAddr(custType == "PER" ? "6" : "13")) {
          if(_data['custCollaPropAddr'].isNotEmpty){
            for(int i=0; i < _data['custCollaPropAddr'].length; i++) {
              _listAddressCollateralProp.add(MS2CustAddrModel(
                  "123",
                  _data['custCollaPropAddr'][i]['ADDRESS_ID'],
                  _data['custCollaPropAddr'][i]['FOREIGN_BK'], //perlu adjust
                  _data['custCollaPropAddr'][i]['ID_NO'], //tambahan id nomor
                  _data['custCollaPropAddr'][i]['KORESPONDEN'], //== "1" ? "true" : "false",
                  custType == "PER" ? "6" : "13",
                  _data['custCollaPropAddr'][i]['ADDRESS'],
                  _data['custCollaPropAddr'][i]['RT'],
                  null,
                  _data['custCollaPropAddr'][i]['RW'],
                  null,
                  _data['custCollaPropAddr'][i]['PROVINSI'],
                  _data['custCollaPropAddr'][i]['PROVINSI_DESC'],
                  _data['custCollaPropAddr'][i]['KABKOT'],
                  _data['custCollaPropAddr'][i]['KABKOT_DESC'],
                  _data['custCollaPropAddr'][i]['KECAMATAN'],
                  _data['custCollaPropAddr'][i]['KECAMATAN_DESC'],
                  _data['custCollaPropAddr'][i]['KELURAHAN'],
                  _data['custCollaPropAddr'][i]['KELURAHAN_DESC'],
                  _data['custCollaPropAddr'][i]['ZIP_CODE'],
                  null,
                  _data['custCollaPropAddr'][i]['PHONE'],
                  _data['custCollaPropAddr'][i]['PHONE_AREA'],
                  _data['custCollaPropAddr'][i]['PHONE_DUA'],
                  _data['custCollaPropAddr'][i]['PHONE_AREA_DUA'],
                  _data['custCollaPropAddr'][i]['FAX'],
                  _data['custCollaPropAddr'][i]['FAX_AREA'],
                  _data['custCollaPropAddr'][i]['ADDR_TYPE'],
                  _data['custCollaPropAddr'][i]['ADDR_DESC'],
                  _data['custCollaPropAddr'][i]['LATITUDE'],
                  _data['custCollaPropAddr'][i]['LONGITUDE'],
                  _data['custCollaPropAddr'][i]['ADDRESS_FROM_MAP'],
                  null,
                  null,
                  null,
                  null,
                  0,
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0",
                  "0"));
            }
            await _dbHelper.insertMS2CustAddr(_listAddressCollateralProp);
          }
        }
      }

      // cust sales
      List<MS2ObjekSalesModel> _listObjekSales = [];
      if(_data['custSales'].isNotEmpty){
        List _dataDakor = [];
        if(_preferences.getString("last_known_state") == "DKR") {
          _dataDakor = await _dbHelper.selectMS2ObjekSales();
        }
        if(await _dbHelper.deleteMS2ObjekSales()){
          for(int i=0; i < _data['custSales'].length; i++){
            _listObjekSales.add(MS2ObjekSalesModel(
                "",
                _data['custSales'][i]['APPL_SALES_ID'],
                _data['custSales'][i]['EMPL_ID'] != null ? _data['custSales'][i]['EMPL_ID'] : "",
                _data['custSales'][i]['SALES_TYPE'] != null ? _data['custSales'][i]['SALES_TYPE'] : "",
                _data['custSales'][i]['SALES_TYPE_DESC'] != null ? _data['custSales'][i]['SALES_TYPE_DESC'] : "",
                _data['custSales'][i]['EMPL_HEAD_ID'] != null ? _data['custSales'][i]['EMPL_HEAD_ID'] : "",// empl_head_id
                _data['custSales'][i]['EMPL_JOB'] != null ? _data['custSales'][i]['EMPL_JOB'] : "",
                _data['custSales'][i]['EMPL_JOB_DESC'] != null ? _data['custSales'][i]['EMPL_JOB_DESC'] : "",
                _data['custSales'][i]['REF_CONTRACT_NO'] != null ? _data['custSales'][i]['REF_CONTRACT_NO'] : "",// ref_contract_no
                1,
                DateTime.now().toString(),
                _preferences.getString("username"),
                null,
                null,
                _data['custSales'][i]['EMPL_HEAD_JOB'] != null ? _data['custSales'][i]['EMPL_HEAD_JOB'] : "",// empl_head_job
                _data['custSales'][i]['EMPL_HEAD_JOB_DESC'] != null ? _data['custSales'][i]['EMPL_HEAD_JOB_DESC'] : "",// empl_head_job_desc
                _data['custSales'][i]['EMPL_HEAD_NAME'] != null ? _data['custSales'][i]['EMPL_HEAD_NAME'] : "", // empl_head_name
                _data['custSales'][i]['EMPL_NAME'] != null ? _data['custSales'][i]['EMPL_NAME'] : "", // empl_name
                null,//_data['custSales'][i][''], // empl_jabatan
                null,//_data['custSales'][i][''], // empl_jabatan_alias
                _dataDakor.isNotEmpty ? _dataDakor[i]['edit_sales_type'] : "0",
                _dataDakor.isNotEmpty ? _dataDakor[i]['edit_empl_job'] : "0",
                _dataDakor.isNotEmpty ? _dataDakor[i]['edit_empl_name'] : "0",
                _dataDakor.isNotEmpty ? _dataDakor[i]['edit_empl_head_id'] : "0",
            ));
          }
          await _dbHelper.insertMS2ObjekSales(_listObjekSales);
        }
      }

      // karoseri
      List<MS2ObjtKaroseriModel> _listObjtKaroseri = [];
      if(_data['custKaroseri'].isNotEmpty){
        List _dataDakor = [];
        if(_preferences.getString("last_known_state") == "DKR") {
          _dataDakor = await _dbHelper.selectMS2ObjtKaroseri();
        }
        if(await _dbHelper.deleteMS2ObjtKaroseri()){
          for(int i=0; i < _data['custKaroseri'].length; i++){
            _listObjtKaroseri.add(MS2ObjtKaroseriModel(
                "",
                _data['custKaroseri'][i]['APPL_KAROSERI_ID'],
                _data['custKaroseri'][i]['PKS_KAROSERI'] != null ? int.parse(_data['custKaroseri'][i]['PKS_KAROSERI']) : null,
                _data['custKaroseri'][i]['COMP_KAROSERI'] != null ? _data['custKaroseri'][i]['COMP_KAROSERI'] : "",
                _data['custKaroseri'][i]['COMP_KAROSERI_DESC'] != null ? _data['custKaroseri'][i]['COMP_KAROSERI_DESC'] : "",
                _data['custKaroseri'][i]['KAROSERI'] != null ? _data['custKaroseri'][i]['KAROSERI'] : "",
                _data['custKaroseri'][i]['KAROSERI_DESC'] != null ? _data['custKaroseri'][i]['KAROSERI_DESC'] : "",
                _data['custKaroseri'][i]['PRICE'] != null ? double.parse(_data['custKaroseri'][i]['PRICE']) : null,
                _data['custKaroseri'][i]['KAROSERI_QTY'] != null ? int.parse(_data['custKaroseri'][i]['KAROSERI_QTY']) : null,
                _data['custKaroseri'][i]['TOTAL_KAROSERI'] != null ? double.parse(_data['custKaroseri'][i]['TOTAL_KAROSERI']) : null,
                null,
                null,
                null,
                null,
                1,
                null,
                0,
              _dataDakor.isNotEmpty ? _dataDakor[i]['edit_pks_karoseri'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[i]['edit_comp_karoseri'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[i]['edit_karoseri'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[i]['edit_karoseri_qty'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[i]['edit_price'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[i]['edit_total_karoseri'] : "0",
            ));
          }
          await _dbHelper.insertMS2ObjtKaroseri(_listObjtKaroseri);
        }
      }

      // insurance
      List<MS2ApplInsuranceModel> _listApplInsurance = [];
      if(_data['custInsurance'].isNotEmpty){
        List _dataDakor = [];
        List _dataDakorInsuranceMain = [];
        List _dataDakorInsuranceAdditional = [];
        _dataDakorInsuranceMain = await _dbHelper.selectMS2ApplInsuranceMain();
        _dataDakorInsuranceAdditional = await _dbHelper.selectMS2ApplInsuranceAdditional();
        for(int i=0; i < _dataDakorInsuranceMain.length; i++) {
          _dataDakor.add(_dataDakorInsuranceMain[i]);
        }
        for(int i=0; i < _dataDakorInsuranceAdditional.length; i++) {
          _dataDakor.add(_dataDakorInsuranceAdditional[i]);
        }
        if(_preferences.getString("last_known_state") == "DKR" && _dataDakor.isNotEmpty) {
          if(await _dbHelper.deleteMS2ApplInsuranceMain() && await _dbHelper.deleteMS2ApplInsurancePeruluasan() && await _dbHelper.deleteMS2ApplInsuranceAdditional()){
            for(int i=0; i < _data['custInsurance'].length; i++){
              for(int j=0; j < _dataDakor.length; j++) {
                if(_data['custInsurance'][i]['INSURANCE_TYPE'] == _dataDakor[j]['insuran_type'].toString()) {
                  _listApplInsurance.add(MS2ApplInsuranceModel(
                    "",
                    _data['custInsurance'][i]['INSURANCE_ID'],
                    _data['custInsurance'][i]['COLA_TYPE'],
                    _data['custInsurance'][i]['JAMINAN_KE'] != null ? _data['custInsurance'][i]['JAMINAN_KE'] : "",
                    _data['custInsurance'][i]['INSURANCE_TYPE'] != null ? _data['custInsurance'][i]['INSURANCE_TYPE'] : "",
                    _data['custInsurance'][i]['COMPANY_ID'] != null ? _data['custInsurance'][i]['COMPANY_ID'] : "",
                    _data['custInsurance'][i]['PRODUCT'] != null ? _data['custInsurance'][i]['PRODUCT'] : "",
                    _data['custInsurance'][i]['PRODUCT_DESC'] != null ? _data['custInsurance'][i]['PRODUCT_DESC'] : "",
                    _data['custInsurance'][i]['PERIOD'] != null ? int.parse(_data['custInsurance'][i]['PERIOD'].toString()) : null,
                    _data['custInsurance'][i]['TYPE'] != null ? _data['custInsurance'][i]['TYPE'] : "",
                    _data['custInsurance'][i]['TYPE_SATU'] != null ? _data['custInsurance'][i]['TYPE_SATU'] : "",
                    _data['custInsurance'][i]['TYPE_DUA'] != null ? _data['custInsurance'][i]['TYPE_DUA'] : "",
                    _data['custInsurance'][i]['SUB_TYPE'] != null ? _data['custInsurance'][i]['SUB_TYPE'] : "",//kode
                    _data['custInsurance'][i]['SUB_TYPE_DESC'] != null ? _data['custInsurance'][i]['SUB_TYPE_DESC'] : "",//deskripsi
                    _data['custInsurance'][i]['COVERAGE_TYPE'] != null ? _data['custInsurance'][i]['COVERAGE_TYPE'] : "",//kode
                    _data['custInsurance'][i]['COVERAGE_TYPE_DESC'] != null ? _data['custInsurance'][i]['COVERAGE_TYPE_DESC'] : "",//deskripsi
                    _data['custInsurance'][i]['COVERAGE_AMT'] != null ? double.parse(_data['custInsurance'][i]['COVERAGE_AMT'].toString().replaceAll(",", "")) : null,
                    _data['custInsurance'][i]['UPPER_LIMIT_PCT'] != null ? double.parse(_data['custInsurance'][i]['UPPER_LIMIT_PCT'].toString().replaceAll(",", "")): null,
                    _data['custInsurance'][i]['UPPER_LIMIT_AMT'] != null ? double.parse(_data['custInsurance'][i]['UPPER_LIMIT_AMT'].toString().replaceAll(",", "")) : null,
                    _data['custInsurance'][i]['LOWER_LIMIT_PCT'] != null ? double.parse(_data['custInsurance'][i]['LOWER_LIMIT_PCT'].toString().replaceAll(",", "")) : null,
                    _data['custInsurance'][i]['LOWER_LIMIT_AMT'] != null ? double.parse(_data['custInsurance'][i]['LOWER_LIMIT_AMT'].toString().replaceAll(",", "")) : null,
                    _data['custInsurance'][i]['CASH_AMT'] != null ? double.parse(_data['custInsurance'][i]['CASH_AMT'].toString().replaceAll(",", "")) : null,
                    _data['custInsurance'][i]['CREDIT_AMT'] != null ? double.parse(_data['custInsurance'][i]['CREDIT_AMT'].toString().replaceAll(",", "")) : null,
                    _data['custInsurance'][i]['TOTAL_SPLIT'] != null ? double.parse(_data['custInsurance'][i]['TOTAL_SPLIT'].toString().replaceAll(",", "")) : null,
                    _data['custInsurance'][i]['TOTAL_PCT'] != null ? double.parse(_data['custInsurance'][i]['TOTAL_PCT'].toString().replaceAll(",", "")) : null,
                    _data['custInsurance'][i]['TOTAL_AMT'] != null ? double.parse(_data['custInsurance'][i]['TOTAL_AMT'].toString().replaceAll(",", "")) : null,
                    null,
                    null,
                    null,
                    null,
                    1,
                    i+1,
                    _data['custInsurance'][i]['INSURANCE_TYPE_DESC'] != null ? _data['custInsurance'][i]['INSURANCE_TYPE_DESC'] : "",
                    _data['custInsurance'][i]['INSURANCE_PARENT_KODE'] != null ? _data['custInsurance'][i]['INSURANCE_PARENT_KODE'] : "", //insurance parent kode
                    _data['custInsurance'][i]['COMPANY_DESC'] != null ? _data['custInsurance'][i]['COMPANY_DESC'] : "",
                    _data['custInsurance'][i]['COMPANY_PARENT'] != null ? _data['custInsurance'][i]['COMPANY_PARENT'] : "",//parent code com
                    _data['custInsurance'][i]['TYPE_SATU_PARENT_KODE'] != null ? _data['custInsurance'][i]['TYPE_SATU_PARENT_KODE'] : "", //type 1 parent
                    _data['custInsurance'][i]['TYPE_SATU_DESC'] != null ? _data['custInsurance'][i]['TYPE_SATU_DESC'] : "",
                    _data['custInsurance'][i]['TYPE_DUA_PARENT_KODE'] != null ? _data['custInsurance'][i]['TYPE_DUA_PARENT_KODE'] : "", //type 2 parent
                    _data['custInsurance'][i]['TYPE_DUA_DESC'] != null ? _data['custInsurance'][i]['TYPE_DUA_DESC'] : "",
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_insuran_type_parent'] : "0",
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_insuran_index'] : "0",
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_company_id'] : "0",
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_product'] : "0",
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_period'] : "0",
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_type'] : "0",
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_type_1'] : "0",
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_type_2'] : "0",
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_sub_type'] : "0",
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_coverage_type'] : "0",
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_coverage_amt'] : "0",
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_upper_limit_pct'] : "0",
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_upper_limit_amt'] : "0",
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_lower_limit_pct'] : "0",
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_lower_limit_amt'] : "0",
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_cash_amt'] : "0",
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_credit_amt'] : "0",
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_total_split'] : "0",
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_total_pct'] : "0",
                    _dataDakor.isNotEmpty ? _dataDakor[j]['edit_total_amt'] : "0",
                  ));
                }
              }
            }
            await _dbHelper.insertMS2ApplInsuranceMain(_listApplInsurance);
          }
        }
        else {
          if(await _dbHelper.deleteMS2ApplInsuranceMain() && await _dbHelper.deleteMS2ApplInsurancePeruluasan() && await _dbHelper.deleteMS2ApplInsuranceAdditional()){
            for(int i=0; i < _data['custInsurance'].length; i++){
              _listApplInsurance.add(MS2ApplInsuranceModel(
                "",
                _data['custInsurance'][i]['INSURANCE_ID'],
                _data['custInsurance'][i]['COLA_TYPE'],
                _data['custInsurance'][i]['JAMINAN_KE'] != null ? _data['custInsurance'][i]['JAMINAN_KE'] : "",
                _data['custInsurance'][i]['INSURANCE_TYPE'] != null ? _data['custInsurance'][i]['INSURANCE_TYPE'] : "",
                _data['custInsurance'][i]['COMPANY_ID'] != null ? _data['custInsurance'][i]['COMPANY_ID'] : "",
                _data['custInsurance'][i]['PRODUCT'] != null ? _data['custInsurance'][i]['PRODUCT'] : "",
                _data['custInsurance'][i]['PRODUCT_DESC'] != null ? _data['custInsurance'][i]['PRODUCT_DESC'] : "",
                _data['custInsurance'][i]['PERIOD'] != null ? int.parse(_data['custInsurance'][i]['PERIOD'].toString()) : null,
                _data['custInsurance'][i]['TYPE'] != null ? _data['custInsurance'][i]['TYPE'] : "",
                _data['custInsurance'][i]['TYPE_SATU'] != null ? _data['custInsurance'][i]['TYPE_SATU'] : "",
                _data['custInsurance'][i]['TYPE_DUA'] != null ? _data['custInsurance'][i]['TYPE_DUA'] : "",
                _data['custInsurance'][i]['SUB_TYPE'] != null ? _data['custInsurance'][i]['SUB_TYPE'] : "",//kode
                _data['custInsurance'][i]['SUB_TYPE_DESC'] != null ? _data['custInsurance'][i]['SUB_TYPE_DESC'] : "",//deskripsi
                _data['custInsurance'][i]['COVERAGE_TYPE'] != null ? _data['custInsurance'][i]['COVERAGE_TYPE'] : "",//kode
                _data['custInsurance'][i]['COVERAGE_TYPE_DESC'] != null ? _data['custInsurance'][i]['COVERAGE_TYPE_DESC'] : "",//deskripsi
                _data['custInsurance'][i]['COVERAGE_AMT'] != null ? double.parse(_data['custInsurance'][i]['COVERAGE_AMT'].toString().replaceAll(",", "")) : null,
                _data['custInsurance'][i]['UPPER_LIMIT_PCT'] != null ? double.parse(_data['custInsurance'][i]['UPPER_LIMIT_PCT'].toString().replaceAll(",", "")): null,
                _data['custInsurance'][i]['UPPER_LIMIT_AMT'] != null ? double.parse(_data['custInsurance'][i]['UPPER_LIMIT_AMT'].toString().replaceAll(",", "")) : null,
                _data['custInsurance'][i]['LOWER_LIMIT_PCT'] != null ? double.parse(_data['custInsurance'][i]['LOWER_LIMIT_PCT'].toString().replaceAll(",", "")) : null,
                _data['custInsurance'][i]['LOWER_LIMIT_AMT'] != null ? double.parse(_data['custInsurance'][i]['LOWER_LIMIT_AMT'].toString().replaceAll(",", "")) : null,
                _data['custInsurance'][i]['CASH_AMT'] != null ? double.parse(_data['custInsurance'][i]['CASH_AMT'].toString().replaceAll(",", "")) : null,
                _data['custInsurance'][i]['CREDIT_AMT'] != null ? double.parse(_data['custInsurance'][i]['CREDIT_AMT'].toString().replaceAll(",", "")) : null,
                _data['custInsurance'][i]['TOTAL_SPLIT'] != null ? double.parse(_data['custInsurance'][i]['TOTAL_SPLIT'].toString().replaceAll(",", "")) : null,
                _data['custInsurance'][i]['TOTAL_PCT'] != null ? double.parse(_data['custInsurance'][i]['TOTAL_PCT'].toString().replaceAll(",", "")) : null,
                _data['custInsurance'][i]['TOTAL_AMT'] != null ? double.parse(_data['custInsurance'][i]['TOTAL_AMT'].toString().replaceAll(",", "")) : null,
                null,
                null,
                null,
                null,
                1,
                i+1,
                _data['custInsurance'][i]['INSURANCE_TYPE_DESC'] != null ? _data['custInsurance'][i]['INSURANCE_TYPE_DESC'] : "",
                _data['custInsurance'][i]['INSURANCE_PARENT_KODE'] != null ? _data['custInsurance'][i]['INSURANCE_PARENT_KODE'] : "", //insurance parent kode
                _data['custInsurance'][i]['COMPANY_DESC'] != null ? _data['custInsurance'][i]['COMPANY_DESC'] : "",
                _data['custInsurance'][i]['COMPANY_PARENT'] != null ? _data['custInsurance'][i]['COMPANY_PARENT'] : "",//parent code com
                _data['custInsurance'][i]['TYPE_SATU_PARENT_KODE'] != null ? _data['custInsurance'][i]['TYPE_SATU_PARENT_KODE'] : "", //type 1 parent
                _data['custInsurance'][i]['TYPE_SATU_DESC'] != null ? _data['custInsurance'][i]['TYPE_SATU_DESC'] : "",
                _data['custInsurance'][i]['TYPE_DUA_PARENT_KODE'] != null ? _data['custInsurance'][i]['TYPE_DUA_PARENT_KODE'] : "", //type 2 parent
                _data['custInsurance'][i]['TYPE_DUA_DESC'] != null ? _data['custInsurance'][i]['TYPE_DUA_DESC'] : "",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
                "0",
              ));
            }
            await _dbHelper.insertMS2ApplInsuranceMain(_listApplInsurance);
          }
        }
      }

      List<MS2ApplFeeModel> _listApplFeeModel = [];
      if(_data['custFee'].isNotEmpty){
        List _dataDakor = [];
        if(_preferences.getString("last_known_state") == "DKR") {
          _dataDakor = await _dbHelper.selectMS2ApplFee();
        }
        if(await _dbHelper.deleteMS2ApplFee()){
          for(int i=0; i<_data['custFee'].length; i++){
            _listApplFeeModel.add(MS2ApplFeeModel(
              "",
              _data['custFee'][i]['FEE_ID'],
              _data['custFee'][i]['FEE_TYPE'] != null ? _data['custFee'][i]['FEE_TYPE'] : "",
              _data['custFee'][i]['FEE_TYPE_DESC'],
              _data['custFee'][i]['FEE_CASH'] != null ? double.parse(_data['custFee'][i]['FEE_CASH'].toString().replaceAll(",", "")) : 0 ,
              _data['custFee'][i]['FEE_CREDIT'] != null ? double.parse(_data['custFee'][i]['FEE_CREDIT'].toString().replaceAll(",", "")) : 0,
              _data['custFee'][i]['TOTAL_FEE'] != null  ?  double.parse(_data['custFee'][i]['TOTAL_FEE'].toString().replaceAll(",", "")) : 0 ,
              1,
              DateTime.now().toString(),
              _preferences.getString("username"),
              null,
              null,
              _dataDakor.isNotEmpty ? _dataDakor[i]['editFeeType'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[i]['editFeeCash'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[i]['editFeeCredit'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[i]['editTotalFee'] : "0",
            ));
          }
          await _dbHelper.insertMS2ApplFee(_listApplFeeModel);
        }
      }

      // wmp
      List<MS2ApplObjtWmpModel> _listDataWMP = [];
      if(_data['custWmp'].isNotEmpty){
        List _dataDakor = [];
        if(_preferences.getString("last_known_state") == "DKR") {
          _dataDakor = await _dbHelper.selectMS2ApplObjtWmp();
        }
        if(await _dbHelper.deleteMS2ApplOBJWmp()){
          for(int i=0; i<_data['custWmp'].length; i++){
            _listDataWMP.add(MS2ApplObjtWmpModel(
              _data['custWmp'][i]['ORDER_NO'] != null ? _data['custWmp'][i]['ORDER_NO'] : "",
              _data['custWmp'][i]['WMP_ID'] != null ? _data['custWmp'][i]['WMP_ID'] : "",
              _data['custWmp'][i]['WMP_NO'] != null ? _data['custWmp'][i]['WMP_NO'] : "",
              _data['custWmp'][i]['WMP_TYPE'] != null ? _data['custWmp'][i]['WMP_TYPE'] : "",
              _data['custWmp'][i]['WMP_TYPE_DESC'] != null ? _data['custWmp'][i]['WMP_TYPE_DESC'] : "",
              _data['custWmp'][i]['WMP_SUBSIDY_TYPE_ID'] != null ? _data['custWmp'][i]['WMP_SUBSIDY_TYPE_ID'] : "",
              _data['custWmp'][i]['WMP_SUBSIDY_TYPE_ID_DESC'] != null ? _data['custWmp'][i]['WMP_SUBSIDY_TYPE_ID_DESC'] : "",
              _data['custWmp'][i]['MAX_REFUND_AMT'] != null ? double.parse(_data['custWmp'][i]['MAX_REFUND_AMT']) : "",
              null,
              null,
              null,
              null,
              null,
              _dataDakor.isNotEmpty ? _dataDakor[i]['edit_wmp_no'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[i]['edit_wmp_type'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[i]['edit_wmp_subsidy_type_id'] : "0",
              _dataDakor.isNotEmpty ? _dataDakor[i]['edit_max_refund_amt'] : "0"
            ));
          }
          await _dbHelper.insertMS2ApplObjtWmp(_listDataWMP);
        }
      }

      List<MS2DocumentModel> _listDataDocument = []; // info dokumen

      //info dokumen
      if(_data['custPhotoOpsi4'].isNotEmpty){
        for(int i=0; i<_data['custPhotoOpsi4'].length; i++){
          String _docLink = _data['custPhotoOpsi4'][i]['DOC_PATH'];
          if(_docLink != null){
            // per tanggal 24.05.2021 dibuat kalau misal IDE DOC_ID selalu NEW
            // _data['custPhotoOpsi4'][i]['DOC_PATH'].toString()[0] == "{"
            if(_preferences.getString("last_known_state") != "IDE"){
              final ioc = new HttpClient();
              ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
              final _http = IOClient(ioc);
              String _getObjectPathByID = await storage.read(key: "GetObjectPathByID");
              String _url = "${BaseUrl.urlGeneral}$_getObjectPathByID"+"objectId=$_docLink&requestId=2020369&objectStore=ADIRAOS&application=REVAMPMS2";
              //get object path by id
              final _response = await _http.get(_url,
                headers: {"x-api-key": xApiKey, "Authorization":"bearer $token"},
              );
              if(_response.statusCode == 200){
                final _result = jsonDecode(_response.body);
                print("result path custPhotoOpsi4 = $_result");
                String _objectPath = _result['objectPath'];
                String _requestId = _result['requestId'];
                String _objectId = _result['objectId'];
                //get file
                String _getFile = await storage.read(key: "GetFile");
                String _url = "${BaseUrl.urlGeneral}$_getFile"+"objectPath=$_objectPath&objectId=$_objectId&requestId=$_requestId&objectStore=ADIRAOS&application=REVAMPMS2";
                final _responseFile = await _http.get(_url,
                  headers: {"x-api-key": xApiKey, "Authorization":"bearer $token"},
                );
                if(_responseFile.statusCode == 200){
                  final _result = _responseFile.bodyBytes;
                  var _location = File('$globalPath/${_data['custPhotoOpsi4'][i]['DOC_NAME']}');
                  File _img = await _location.writeAsBytes(_result);

                  if(await _dbHelper.deleteMS2Document()){
                    _listDataDocument.add(MS2DocumentModel(
                        "",
                        _data['custPhotoOpsi4'][i]['DOC_ID'], //_data['custPhotoOpsi4'][i]['SUP_DOC_ID'] != null ? _data['custPhotoOpsi4'][i]['SUP_DOC_ID'] : "",
                        _data['custPhotoOpsi4'][i]['APPL_NO'], //_data['custPhotoOpsi4'][i]['AC_APPL_NO'] != null ? _data['custPhotoOpsi4'][i]['AC_APPL_NO'] : "",
                        _data['custPhotoOpsi4'][i]['DOC_PATH'], //_data['custPhotoOpsi4'][i]['DOC_LINK'] != null ? _data['custPhotoOpsi4'][i]['DOC_LINK'] : "",
                        _img.path, //_data['custPhotoOpsi4'][i]['DOC_NAME'] != null ? _data['custPhotoOpsi4'][i]['DOC_NAME'] : "",
                        null,
                        null,
                        null,
                        null,
                        _data['custPhotoOpsi4'][i]['DOC_TYPE_ID'] != null ? _data['custPhotoOpsi4'][i]['DOC_TYPE_ID'] : "",
                        _data['custPhotoOpsi4'][i]['DOC_TYPE_DESC'] != null ? _data['custPhotoOpsi4'][i]['DOC_TYPE_DESC'] : "",
                        int.parse(_data['custPhotoOpsi4'][i]['FLAG_MANDATORY']),
                        int.parse(_data['custPhotoOpsi4'][i]['FLAG_DISPLAY']),
                        int.parse(_data['custPhotoOpsi4'][i]['FLAG_UNIT']),
                        _data['custPhotoOpsi4'][i]['RECEIVED_DATE'],
                        null,
                        null
                    ));
                    await _dbHelper.insertMS2Document(_listDataDocument);
                  }
                }
              }
            }
            else{
              print("ini DISSSS custPhotoOpsi4");
              String _date = formatDateListOID(DateTime.now()).replaceAll("-", "");
              String _time = formatTime(DateTime.now()).replaceAll(":", "");

              final ioc = new HttpClient();
              ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
              final _http = IOClient(ioc);
              String _getFileDIS = await storage.read(key: "GetFileDIS");
              final _response = await _http.post("${BaseUrl.urlGeneralPublic}$_getFileDIS",
                body: {
                  "HEADER_ID" : _docLink
                },
              );
              if(_response.statusCode == 200){
                final _result = _response.bodyBytes;
                var _location = File('$globalPath/DIS_${_date}_${_time}_${_data['custPhotoOpsi4'][i]['DOC_NAME']}');
                File _img = await _location.writeAsBytes(_result);

                if(await _dbHelper.deleteMS2Document()){
                  _listDataDocument.add(MS2DocumentModel(
                      "",
                      "NEW", //_data['custPhotoOpsi4'][i]['SUP_DOC_ID'] != null ? _data['custPhotoOpsi4'][i]['SUP_DOC_ID'] : "",
                      _data['custPhotoOpsi4'][i]['APPL_NO'], //_data['custPhotoOpsi4'][i]['AC_APPL_NO'] != null ? _data['custPhotoOpsi4'][i]['AC_APPL_NO'] : "",
                      _data['custPhotoOpsi4'][i]['DOC_PATH'], //_data['custPhotoOpsi4'][i]['DOC_LINK'] != null ? _data['custPhotoOpsi4'][i]['DOC_LINK'] : "",
                      _img.path, //_data['custPhotoOpsi4'][i]['DOC_NAME'] != null ? _data['custPhotoOpsi4'][i]['DOC_NAME'] : "",
                      null,
                      null,
                      null,
                      null,
                      _data['custPhotoOpsi4'][i]['DOC_TYPE_ID'] != null ? _data['custPhotoOpsi4'][i]['DOC_TYPE_ID'] : "",
                      _data['custPhotoOpsi4'][i]['DOC_TYPE_DESC'] != null ? _data['custPhotoOpsi4'][i]['DOC_TYPE_DESC'] : "",
                      int.parse(_data['custPhotoOpsi4'][i]['FLAG_MANDATORY']),
                      int.parse(_data['custPhotoOpsi4'][i]['FLAG_DISPLAY']),
                      int.parse(_data['custPhotoOpsi4'][i]['FLAG_UNIT']),
                      _data['custPhotoOpsi4'][i]['RECEIVED_DATE'],
                      null,
                      null
                  ));
                  await _dbHelper.insertMS2Document(_listDataDocument);
                }
              }
              else{
                if(await _dbHelper.deleteMS2Document()){
                  _listDataDocument.add(MS2DocumentModel(
                      "",
                      _data['custPhotoOpsi4'][i]['DOC_ID'], //_data['custPhotoOpsi4'][i]['SUP_DOC_ID'] != null ? _data['custPhotoOpsi4'][i]['SUP_DOC_ID'] : "",
                      _data['custPhotoOpsi4'][i]['APPL_NO'], //_data['custPhotoOpsi4'][i]['AC_APPL_NO'] != null ? _data['custPhotoOpsi4'][i]['AC_APPL_NO'] : "",
                      _data['custPhotoOpsi4'][i]['DOC_PATH'], //_data['custPhotoOpsi4'][i]['DOC_LINK'] != null ? _data['custPhotoOpsi4'][i]['DOC_LINK'] : "",
                      null, //_data['custPhotoOpsi4'][i]['DOC_NAME'] != null ? _data['custPhotoOpsi4'][i]['DOC_NAME'] : "",
                      null,
                      null,
                      null,
                      null,
                      _data['custPhotoOpsi4'][i]['DOC_TYPE_ID'] != null ? _data['custPhotoOpsi4'][i]['DOC_TYPE_ID'] : "",
                      _data['custPhotoOpsi4'][i]['DOC_TYPE_DESC'] != null ? _data['custPhotoOpsi4'][i]['DOC_TYPE_DESC'] : "",
                      int.parse(_data['custPhotoOpsi4'][i]['FLAG_MANDATORY']),
                      int.parse(_data['custPhotoOpsi4'][i]['FLAG_DISPLAY']),
                      int.parse(_data['custPhotoOpsi4'][i]['FLAG_UNIT']),
                      _data['custPhotoOpsi4'][i]['RECEIVED_DATE'],
                      null,
                      null
                  ));
                  await _dbHelper.insertMS2Document(_listDataDocument);
                }
              }
            }
          }
        }
      }

      //taksasi
      List<MS2ApplTaksasiModel> _listDataTaksasi = [];
      if(_data['custTaksasi'].isNotEmpty){
        if(await _dbHelper.deleteMS2ApplTaksasi()){
          for(int i=0; i <_data['custTaksasi'].length; i++){
            _listDataTaksasi.add(MS2ApplTaksasiModel(
                null,
                _data['custTaksasi'][i]['UNIT_TYPE'],
                _data['custTaksasi'][i]['UNIT_TYPE_DESC'],
                _data['custTaksasi'][i]['TAKSASI_CODE'],
                _data['custTaksasi'][i]['TAKSASI_DESC'],
                double.parse(_data['custTaksasi'][i]['NILAI_INPUT_REKONDISI']),
                null,
                null,
                null,
                null,
                null
            ));
          }
          await _dbHelper.insertMS2ApplTaksasi(_listDataTaksasi);
        }
      }

      //custSubsidiDetail
      List<AddInstallmentDetailModel> _custSubsidiDetail = [];
      if(_data['custSubsidiDetail'].isNotEmpty){
        for(int i=0; i < _data['custSubsidiDetail'].length; i++){
          _custSubsidiDetail.add(AddInstallmentDetailModel(
            _data['custSubsidiDetail'][i]['AC_INSTALLMENT_NO'],
            _data['custSubsidiDetail'][i]['AC_INSTALLMENT_AMT'].toString(),
            _data['custSubsidiDetail'][i]['AC_SUBSIDY_DTL_ID'],
          ));
        }
      }

      //custSubsidi
      List<MS2ApplRefundModel> _custSubsidi = [];
      if(_data['custSubsidi'].isNotEmpty){
        List _dataDakor = [];
        if(_preferences.getString("last_known_state") == "DKR") {
          _dataDakor = await _dbHelper.selectMS2ApplRefund();
        }
        if(await _dbHelper.deleteMS2ApplRefundHeader()){
          print("PANJANG ${_custSubsidiDetail.length}");
          for(int i=0; i <_data['custSubsidi'].length; i++){
            _custSubsidi.add(MS2ApplRefundModel(
                "",
                _data['custSubsidi'][i]['SUBSIDY_ID'],
                _data['custSubsidi'][i]['GIVER_REFUND'],
                _data['custSubsidi'][i]['TYPE_REFUND'] != null ? _data['custSubsidi'][i]['TYPE_REFUND'] : "",
                _data['custSubsidi'][i]['TYPE_REFUND_DESC'] != null ? _data['custSubsidi'][i]['TYPE_REFUND_DESC'] : "",
                _data['custSubsidi'][i]['DEDUCTION_METHOD'] != null ? _data['custSubsidi'][i]['DEDUCTION_METHOD'] : "",//deduction_method
                _data['custSubsidi'][i]['DEDUCTION_METHOD_DESC'] != null ? _data['custSubsidi'][i]['DEDUCTION_METHOD_DESC'] : "",
                _data['custSubsidi'][i]['REFUND_AMT'] != null ? double.parse(_data['custSubsidi'][i]['REFUND_AMT'].toString()) : 0, // refund_amt
                _data['custSubsidi'][i]['EFF_RATE_BEF'] != null ? double.parse(_data['custSubsidi'][i]['EFF_RATE_BEF'].toString()) : 0, //eff_rate_bef
                _data['custSubsidi'][i]['FLAT_RATE_BEF'] != null ? double.parse(_data['custSubsidi'][i]['FLAT_RATE_BEF'].toString()) : 0, //bunga
                _data['custSubsidi'][i]['DP_REAL'] != null ? double.parse(_data['custSubsidi'][i]['DP_REAL'].toString()).toInt() : 0,//DP_REAL
                _data['custSubsidi'][i]['INSTALLMENT_AMT'] != null ? double.parse(_data['custSubsidi'][i]['INSTALLMENT_AMT'].toString()) : 0, //installment_amt
                1,
                DateTime.now().toString(),
                _preferences.getString("username"),
                null,
                null,
                null,
                _data['custSubsidi'][i]['REFUND_AMT_KLAIM'] != null ? double.parse(_data['custSubsidi'][i]['REFUND_AMT_KLAIM'].toString()) : 0,//nilai klaim
                _custSubsidiDetail,// subsidi detail
                _dataDakor.isNotEmpty ? _dataDakor[i]['edit_giver_refund'] : "0",
                _dataDakor.isNotEmpty ? _dataDakor[i]['edit_type_refund'] : "0",
                _dataDakor.isNotEmpty ? _dataDakor[i]['edit_deduction_method'] : "0",
                _dataDakor.isNotEmpty ? _dataDakor[i]['edit_refund_amt'] : "0"
            ));
          }
          await _dbHelper.insertMS2ApplRefund(_custSubsidi);
        }
      }

      //survey
      if(typeFormId != "IDE"){
        //survey result
        List<MS2SvyRsltDtlModel> _listSvyRsltDtl = [];
        if(_data['custSurveyResult'].isNotEmpty){
          if(await _dbHelper.deleteResultSurveyDetail()){
            for(int i=0; i<_data['custSurveyResult'].length; i++){
              _listSvyRsltDtl.add(MS2SvyRsltDtlModel(
                null,
                _data['custSurveyResult'][i]['SVY_RSLT_DTL_ID'],
                _data['custSurveyResult'][i]['INFO_ENVIRNMT'],
                _data['custSurveyResult'][i]['INFO_ENVIRNMT_DESC'],
                _data['custSurveyResult'][i]['INFO_SOURCE'],
                _data['custSurveyResult'][i]['INFO_SOURCE_DESC'],
                _data['custSurveyResult'][i]['INFO_SOURCE_NAME'],
                1,
                null,
                null,
                null,
                null,
              ));
            }
            _dbHelper.insertMS2SvyRsltDtl(_listSvyRsltDtl);
          }
        }

        if(_data['custSurveyAssignment'].isNotEmpty){
          if(await _dbHelper.deleteMS2SvyAssgnmt()){
            _dbHelper.insertMS2SvyAssgnmt(MS2SvyAssgnmtModel(
                _data['custSurveyAssignment'][0]['SVY_ASSGNMT_ID'] != null ? _data['custSurveyAssignment'][0]['SVY_ASSGNMT_ID'] : null,
                null,
                _data['custSurveyAssignment'][0]['SURVEY_TYPE_ID'],
                _data['custSurveyAssignment'][0]['SURVEY_TYPE_DESC'],
                _data['custSurveyAssignment'][0]['EMPL_NIK'],
                null,
                _data['custSurveyAssignment'][0]['JANJI_SURVEY_DATE'],
                null,
                null,
                null,
                _data['custSurveyAssignment'][0]['STATUS_SURVEY'],
                1,
                DateTime.now().toString(),
                _preferences.getString("username"),
                null,
                null,
                _data['custSurveyAssignment'][0]['SURVEY_RESULT'],
                _data['custSurveyAssignment'][0]['SURVEY_RESULT_DESC'],
                _data['custSurveyAssignment'][0]['SURVEY_RECOMENDATION'],
                _data['custSurveyAssignment'][0]['SURVEY_RECOMENDATION_DESC'],
                _data['custSurveyAssignment'][0]['NOTES'],
                _data['custSurveyAssignment'][0]['SURVEY_RESULT_DATE'] != null ? formatDateAndTime(DateTime.parse(_data['custSurveyAssignment'][0]['SURVEY_RESULT_DATE'])) : formatDateAndTime(DateTime.now()),
                _data['custSurveyAssignment'][0]['DISTANCE_SENTRA'] != null ? double.parse(_data['custSurveyAssignment'][0]['DISTANCE_SENTRA']) : null,
                _data['custSurveyAssignment'][0]['DISTANCE_DEAL'] != null ? double.parse(_data['custSurveyAssignment'][0]['DISTANCE_DEAL']) : null,
                _data['custSurveyAssignment'][0]['DISTANCE_OBJT_PURP_SENTRA'] != null ? double.parse(_data['custSurveyAssignment'][0]['DISTANCE_OBJT_PURP_SENTRA']) : null,
                null,
                null,
              )
            );
          }
        }

        List<MS2SvyRsltAsstModel> _list = [];
        if(_data['custSurveyAsset'].isNotEmpty){
          if(await _dbHelper.deleteResultSurveyAsset()){
            for(int i=0; i < _data['custSurveyAsset'].length; i++){
              _list.add(
                  MS2SvyRsltAsstModel(
                      null,
                      _data['custSurveyAsset'][i]['SVY_RSLT_ASST_ID'],
                      AssetTypeModel(_data['custSurveyAsset'][i]['ASSET_TYPE'], _data['custSurveyAsset'][i]['ASSET_TYPE_DESC']),
                      _data['custSurveyAsset'][i]['ASSET_AMT'],
                      OwnershipModel(_data['custSurveyAsset'][i]['ASSET_OWN'],_data['custSurveyAsset'][i]['ASSET_OWN_DESC']),
                      _data['custSurveyAsset'][i]['SIZE_OF_LAND'],
                      RoadTypeModel(_data['custSurveyAsset'][i]['STREET_TYPE'], _data['custSurveyAsset'][i]['STREET_TYPE_DESC']),
                      ElectricityTypeModel(_data['custSurveyAsset'][i]['ELECTRICITY'], _data['custSurveyAsset'][i]['ELECTRICITY_DESC']),
                      _data['custSurveyAsset'][i]['ELECTRICITY_BILL'] != null ? double.parse(_data['custSurveyAsset'][i]['ELECTRICITY_BILL']) : "",
                      _data['custSurveyAsset'][i]['EXPIRED_DATE_CONTRACT'],
                      _data['custSurveyAsset'][i]['NO_OF_STAY'] != null ? int.parse(_data['custSurveyAsset'][i]['NO_OF_STAY']) : 0,
                      1,
                      DateTime.now().toString(),
                      _preferences.getString("username"),
                      null,
                      null,
                      "0",
                      "0",
                      "0",
                      "0",
                      "0",
                      "0",
                      "0",
                      "0",
                      "0",
                      "0"
                  )
              );
            }
            _dbHelper.insertMS2SvyRsltAsst(_list);
          }
        }

        List<Map> _listSurveySC1Temp = []; //foto survey
        List<Map> _listSurveySC2Temp = []; //foto survey
        List<Map> _listSurveySC3Temp = []; //foto survey
        List<Map> _listSurveySC4Temp = []; //foto survey
        List<SurveyPhotoModel> _listSurveyPhoto = []; //foto survey

        //survey foto
        if(_preferences.getString("last_known_state") != "RSVY"){
          if(_data['custPhotoOpsi2'].isNotEmpty){
            for(int i=0; i<_data['custPhotoOpsi2'].length; i++){
              String _docLink = _data['custPhotoOpsi2'][i]['DOC_PATH'];
              if(_data['custPhotoOpsi2'][i]['DOC_PATH'].toString()[0] == "{" && _docLink != null){
                final ioc = new HttpClient();
                ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
                final _http = IOClient(ioc);
                String _getObjectPathByID = await storage.read(key: "GetObjectPathByID");
                String _url = "${BaseUrl.urlGeneral}$_getObjectPathByID"+"objectId=$_docLink&requestId=2020369&objectStore=ADIRAOS&application=REVAMPMS2";
                //get object path by id
                final _response = await _http.get(_url,
                  headers: {"x-api-key": xApiKey, "Authorization":"bearer $token"},
                );
                if(_response.statusCode == 200){
                  final _result = jsonDecode(_response.body);
                  print("result path = $_result");
                  String _objectPath = _result['objectPath'];
                  String _requestId = _result['requestId'];
                  String _objectId = _result['objectId'];
                  //get file
                  String _getFile = await storage.read(key: "GetFile");
                  String _url = "${BaseUrl.urlGeneral}$_getFile"+"objectPath=$_objectPath&objectId=$_objectId&requestId=$_requestId&objectStore=ADIRAOS&application=REVAMPMS2";
                  final _responseFile = await _http.get(_url,
                    headers: {"x-api-key": xApiKey, "Authorization":"bearer $token"},
                  );
                  print("cekkkk ${_data['custPhotoOpsi2'][i]['DOC_TYPE_ID']}");
                  print("response survey ${_responseFile.statusCode}");

                  if(_responseFile.statusCode == 200){
                    final _result = _responseFile.bodyBytes;
                    if(_data['custPhotoOpsi2'][i]['DOC_TYPE_ID'] == "SC1") {
                      Map _mapData;
                      _mapData = _data['custPhotoOpsi2'][i];
                      _mapData['file'] = _result;
                      _listSurveySC1Temp.add(_mapData);
                    }
                    else if(_data['custPhotoOpsi2'][i]['DOC_TYPE_ID'] == "SC2"){
                      Map _mapData;
                      _mapData = _data['custPhotoOpsi2'][i];
                      _mapData['file'] = _result;
                      _listSurveySC2Temp.add(_mapData);
                    }
                    else if(_data['custPhotoOpsi2'][i]['DOC_TYPE_ID'] == "SC3"){
                      Map _mapData;
                      _mapData = _data['custPhotoOpsi2'][i];
                      _mapData['file'] = _result;
                      _listSurveySC3Temp.add(_mapData);
                    }
                    else if(_data['custPhotoOpsi2'][i]['DOC_TYPE_ID'] == "SC4"){
                      Map _mapData;
                      _mapData = _data['custPhotoOpsi2'][i];
                      _mapData['file'] = _result;
                      _listSurveySC4Temp.add(_mapData);
                    }
                  }
                }
              }
            }
          }

          //list survey SC1
          if(_listSurveySC1Temp.isNotEmpty){
            List<ImageFileModel> _listImageSurveyUnit = [];
            var _photoTypeSelected = PhotoTypeModel(_listSurveySC1Temp[0]['DOC_TYPE_ID'], _listSurveySC1Temp[0]['DOC_TYPE_DESC']);
            for(int i=0; i<_listSurveySC1Temp.length; i++){
              if(_listSurveySC1Temp[i]['file'] != null){
                var _location = File('$globalPath/${_listSurveySC1Temp[i]['DOC_NAME']}');
                File _img = await _location.writeAsBytes(_listSurveySC1Temp[i]['file']);
                _listImageSurveyUnit.add(ImageFileModel(_img, null, null, _img.path, _listSurveySC1Temp[i]['DOC_PATH']));
              }
            }
            _listSurveyPhoto.add(SurveyPhotoModel(_photoTypeSelected, _listImageSurveyUnit, false, false, false));
          }

          //list survey SC2
          if(_listSurveySC2Temp.isNotEmpty){
            List<ImageFileModel> _listImageSurveyUnit = [];
            var _photoTypeSelected = PhotoTypeModel(_listSurveySC2Temp[0]['DOC_TYPE_ID'], _listSurveySC2Temp[0]['DOC_TYPE_DESC']);
            for(int i=0; i<_listSurveySC2Temp.length; i++){
              if(_listSurveySC2Temp[i]['file'] != null){
                var _location = File('$globalPath/${_listSurveySC2Temp[i]['DOC_NAME']}');
                File _img = await _location.writeAsBytes(_listSurveySC2Temp[i]['file']);
                _listImageSurveyUnit.add(ImageFileModel(_img, null, null, _img.path, _listSurveySC2Temp[i]['DOC_PATH']));
              }
            }
            _listSurveyPhoto.add(SurveyPhotoModel(_photoTypeSelected, _listImageSurveyUnit, false, false, false));
          }

          //list survey SC3
          if(_listSurveySC3Temp.isNotEmpty){
            List<ImageFileModel> _listImageSurveyUnit = [];
            var _photoTypeSelected = PhotoTypeModel(_listSurveySC3Temp[0]['DOC_TYPE_ID'], _listSurveySC3Temp[0]['DOC_TYPE_DESC']);
            for(int i=0; i<_listSurveySC3Temp.length; i++){
              if(_listSurveySC3Temp[i]['file'] != null){
                var _location = File('$globalPath/${_listSurveySC3Temp[i]['DOC_NAME']}');
                File _img = await _location.writeAsBytes(_listSurveySC3Temp[i]['file']);
                _listImageSurveyUnit.add(ImageFileModel(_img, null, null, _img.path, _listSurveySC3Temp[i]['DOC_PATH']));
              }
            }
            _listSurveyPhoto.add(SurveyPhotoModel(_photoTypeSelected, _listImageSurveyUnit, false, false, false));
          }

          //list survey SC4
          if(_listSurveySC4Temp.isNotEmpty){
            List<ImageFileModel> _listImageSurveyUnit = [];
            var _photoTypeSelected = PhotoTypeModel(_listSurveySC4Temp[0]['DOC_TYPE_ID'], _listSurveySC4Temp[0]['DOC_TYPE_DESC']);
            for(int i=0; i<_listSurveySC4Temp.length; i++){
              if(_listSurveySC4Temp[i]['file'] != null){
                var _location = File('$globalPath/${_listSurveySC4Temp[i]['DOC_NAME']}');
                File _img = await _location.writeAsBytes(_listSurveySC4Temp[i]['file']);
                _listImageSurveyUnit.add(ImageFileModel(_img, null, null, _img.path, _listSurveySC4Temp[i]['DOC_PATH']));
              }
            }
            _listSurveyPhoto.add(SurveyPhotoModel(_photoTypeSelected, _listImageSurveyUnit, false, false, false));
          }
        }

        //save foto survey
        if(_listSurveyPhoto.isNotEmpty){
          if(await _dbHelper.deletePhotoSurvey()){
            await _dbHelper.insertMS2PhotoSurvey(_listSurveyPhoto);
          }
        }
      }

      await _dbHelper.saveLastStep(int.parse(IDX));
      navigateForm(typeFormId, context, orderNo, orderDate, custName, custType,isFromHome);
      loadData = false;
      Provider.of<HomeChangeNotifier>(context, listen: false).loadData = false;
      // }
      // on TimeoutException catch(_){
      //   loadData = false;
      //   Provider.of<HomeChangeNotifier>(context, listen: false).loadData = false;
      //   throw "Request timeout";
      // }
      // catch(e){
      //   loadData = false;
      //   Provider.of<HomeChangeNotifier>(context, listen: false).loadData = false;
      //   throw "${e.toString()}";
      //   // showSnackBar(e.toString());
      // }
    }
    else{
      print("di else tasklist");
      navigateForm(typeFormId, context, orderNo, orderDate, custName, custType,isFromHome);
      loadData = false;
      Provider.of<HomeChangeNotifier>(context, listen: false).loadData = false;
    }

  }
}