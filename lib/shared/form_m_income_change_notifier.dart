import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_income_model.dart';
import 'package:ad1ms2_dev/models/show_mandatory_income_model.dart';
import 'package:ad1ms2_dev/shared/dashboard/dashboard_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_guarantor_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_occupation_change_notif.dart';
import 'package:ad1ms2_dev/shared/format_currency.dart';
import 'package:ad1ms2_dev/shared/regex_input_formatter.dart';
import 'package:ad1ms2_dev/shared/resource/submit_data_partial.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'change_notifier_app/info_application_change_notifier.dart';
import 'form_m_individu_change_notifier/form_m_parent_individu_change_notifier.dart';
import 'form_m_info_nasabah_change_notif.dart';

class FormMIncomeChangeNotifier with ChangeNotifier {
  bool _autoValidate = false;
  FormatCurrency _formatCurrency = FormatCurrency();
  NumberFormat _oCcy = NumberFormat("#,##0.00", "en_US");
  RegExInputFormatter _amountValidator = RegExInputFormatter.withRegex('^[0-9]{0,13}(\\.[0-9]{0,2})?\$');
  TextEditingController _controllerIncome = TextEditingController();
  TextEditingController _controllerSpouseIncome = TextEditingController();
  TextEditingController _controllerOtherIncome = TextEditingController();
  TextEditingController _controllerTotalIncome = TextEditingController();
  TextEditingController _controllerCostOfLiving = TextEditingController();
  TextEditingController _controllerRestIncome = TextEditingController();
  TextEditingController _controllerOtherInstallments = TextEditingController();
  TextEditingController _controllerMonthlyIncome = TextEditingController();
  TextEditingController _controllerCostOfGoodsSold = TextEditingController();
  TextEditingController _controllerGrossProfit = TextEditingController();
  TextEditingController _controllerOperatingCosts = TextEditingController();
  TextEditingController _controllerOtherCosts = TextEditingController();
  TextEditingController _controllerNetProfitBeforeTax = TextEditingController();
  TextEditingController _controllerTax = TextEditingController();
  TextEditingController _controllerNetProfitAfterTax = TextEditingController();
  DbHelper _dbHelper = DbHelper();
  ShowMandatoryIncomeModel _showMandatoryIncomeModel;
  SubmitDataPartial _submitDataPartial = SubmitDataPartial();
  String _customerIncomeIDMonthlyIncome;
  String _customerIncomeIDOtherIncome;
  String _customerIncomeIDTotalIncome;
  String _customerIncomeIDCostOfGoodsSold;
  String _customerIncomeIDGrossProfit;
  String _customerIncomeIDOperatingCosts;
  String _customerIncomeIDOtherCosts;
  String _customerIncomeIDNetProfitBeforeTax;
  String _customerIncomeIDTax;
  String _customerIncomeIDNetProfitAfterTax;
  String _customerIncomeIDCostOfLiving;
  String _customerIncomeIDSpouseIncome;
  String _customerIncomeIDOtherInstallments;
  String _customerIncomeIDRestIncome;
  String _custType;
  String _lastKnownState;
  String _occupation;

  String get occupation => _occupation;

  set occupation(String value) {
    this._occupation = value;
    notifyListeners();
  }

  String get custType => _custType;

  set custType(String value) {
    this._custType = value;
    notifyListeners();
  }

  String get lastKnownState => _lastKnownState;

  set lastKnownState(String value) {
    this._lastKnownState = value;
    notifyListeners();
  }

  String get customerIncomeIDOtherIncome => _customerIncomeIDOtherIncome;

  set customerIncomeIDOtherIncome(String value) {
    this._customerIncomeIDOtherIncome = value;
    notifyListeners();
  }

  String get customerIncomeIDTotalIncome => _customerIncomeIDTotalIncome;

  set customerIncomeIDTotalIncome(String value) {
    this._customerIncomeIDTotalIncome = value;
    notifyListeners();
  }

  String get customerIncomeIDCostOfGoodsSold =>
      _customerIncomeIDCostOfGoodsSold;

  set customerIncomeIDCostOfGoodsSold(String value) {
    this._customerIncomeIDCostOfGoodsSold = value;
    notifyListeners();
  }

  String get customerIncomeIDGrossProfit => _customerIncomeIDGrossProfit;

  set customerIncomeIDGrossProfit(String value) {
    this._customerIncomeIDGrossProfit = value;
    notifyListeners();
  }

  String get customerIncomeIDOperatingCosts => _customerIncomeIDOperatingCosts;

  set customerIncomeIDOperatingCosts(String value) {
    this._customerIncomeIDOperatingCosts = value;
    notifyListeners();
  }

  String get customerIncomeIDOtherCosts => _customerIncomeIDOtherCosts;

  set customerIncomeIDOtherCosts(String value) {
    this._customerIncomeIDOtherCosts = value;
    notifyListeners();
  }

  String get customerIncomeIDNetProfitBeforeTax =>
      _customerIncomeIDNetProfitBeforeTax;

  set customerIncomeIDNetProfitBeforeTax(String value) {
    this._customerIncomeIDNetProfitBeforeTax = value;
    notifyListeners();
  }

  String get customerIncomeIDTax => _customerIncomeIDTax;

  set customerIncomeIDTax(String value) {
    this._customerIncomeIDTax = value;
    notifyListeners();
  }

  String get customerIncomeIDNetProfitAfterTax =>
      _customerIncomeIDNetProfitAfterTax;

  set customerIncomeIDNetProfitAfterTax(String value) {
    this._customerIncomeIDNetProfitAfterTax = value;
    notifyListeners();
  }

  String get customerIncomeIDCostOfLiving => _customerIncomeIDCostOfLiving;

  set customerIncomeIDCostOfLiving(String value) {
    this._customerIncomeIDCostOfLiving = value;
    notifyListeners();
  }

  String get customerIncomeIDSpouseIncome => _customerIncomeIDSpouseIncome;

  set customerIncomeIDSpouseIncome(String value) {
    this._customerIncomeIDSpouseIncome = value;
    notifyListeners();
  }

  String get customerIncomeIDOtherInstallments =>
      _customerIncomeIDOtherInstallments;

  set customerIncomeIDOtherInstallments(String value) {
    this._customerIncomeIDOtherInstallments = value;
    notifyListeners();
  }

  String get customerIncomeIDRestIncome => _customerIncomeIDRestIncome;

  set customerIncomeIDRestIncome(String value) {
    this._customerIncomeIDRestIncome = value;
    notifyListeners();
  }

  String get customerIncomeIDMonthlyIncome => _customerIncomeIDMonthlyIncome;

  set customerIncomeIDMonthlyIncome(String value) {
    this._customerIncomeIDMonthlyIncome = value;
    notifyListeners();
  }

  // bool _incomeNotMinus = false;

  // bool get incomeNotMinus => _incomeNotMinus;
  //
  // set incomeNotMinus(bool value) {
  //   this._incomeNotMinus = value;
  //   notifyListeners();
  // }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  TextEditingController get controllerOtherInstallments =>
      _controllerOtherInstallments;

  TextEditingController get controllerRestIncome => _controllerRestIncome;

  TextEditingController get controllerCostOfLiving => _controllerCostOfLiving;

  TextEditingController get controllerTotalIncome => _controllerTotalIncome;

  TextEditingController get controllerOtherIncome => _controllerOtherIncome;

  TextEditingController get controllerSpouseIncome => _controllerSpouseIncome;

  TextEditingController get controllerIncome => _controllerIncome;

  TextEditingController get controllerNetProfitAfterTax =>
      _controllerNetProfitAfterTax;

  TextEditingController get controllerTax => _controllerTax;

  TextEditingController get controllerNetProfitBeforeTax =>
      _controllerNetProfitBeforeTax;

  TextEditingController get controllerOtherCosts => _controllerOtherCosts;

  TextEditingController get controllerOperatingCosts =>
      _controllerOperatingCosts;

  TextEditingController get controllerGrossProfit => _controllerGrossProfit;

  TextEditingController get controllerCostOfGoodsSold =>
      _controllerCostOfGoodsSold;

  TextEditingController get controllerMonthlyIncome => _controllerMonthlyIncome;

  RegExInputFormatter get amountValidator => _amountValidator;

  set amountValidator(RegExInputFormatter value) {
    this._amountValidator = value;
    notifyListeners();
  }

  NumberFormat get oCcy => _oCcy;

  set oCcy(NumberFormat value) {
    this._oCcy = value;
    notifyListeners();
  }

  FormatCurrency get formatCurrency => _formatCurrency;

  bool _isMarried = false;

  bool get isMarried => _isMarried;

  set isMarried(bool value) {
    this._isMarried = value;
  }

  Future<void> setIsMarried(BuildContext context) async {
    debugPrint("jalan function setIsMarried");
    var model = Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false).maritalStatusSelected;
    if(model.id == "01") {
      isMarried = true;
    } else {
      isMarried = false;
    }
    setShowMandatoryIncome(context);
    // setDataFromSQLite(context, null);
  }

  Future<void> setPreference(BuildContext context) async {
    SharedPreferences _preference = await SharedPreferences.getInstance();
    this._custType = _preference.getString("cust_type");
    this._lastKnownState = _preference.getString("last_known_state");
    this._occupation = Provider.of<FormMFotoChangeNotifier>(context, listen: false).occupationSelected.KODE;// dirubah ambil data dr FormOccupationChangeNotif ke FormMFotoChangeNotifier karena belum diset di occupation datanya
  }

  // Pekerjaan Wiraswasta
  void calculateTotalIncomeEntrepreneur() {
    print(isMarried);
    if(isMarried) {
      var _monthlyIncome = _controllerMonthlyIncome.text.isNotEmpty ? double.parse(_controllerMonthlyIncome.text.replaceAll(",", "")) : 0.0;
      // var _spouseIncome = _controllerSpouseIncome.text.isNotEmpty ? double.parse(_controllerSpouseIncome.text.replaceAll(",", "")) : 0.0;
      var _otherIncome = _controllerOtherIncome.text.isNotEmpty ? double.parse(_controllerOtherIncome.text.replaceAll(",", "")) : 0.0;
      // var _totalIncome = _income + _spouseIncome + _otherIncome;
      var _totalIncome = _monthlyIncome + _otherIncome;
      _controllerTotalIncome.text = _formatCurrency.formatCurrency2(_totalIncome.toString());
    } else {
      var _monthlyIncome = _controllerMonthlyIncome.text.isNotEmpty ? double.parse(_controllerMonthlyIncome.text.replaceAll(",", "")) : 0.0;
      var _otherIncome = _controllerOtherIncome.text.isNotEmpty ? double.parse(_controllerOtherIncome.text.replaceAll(",", "")) : 0.0;
      var _totalIncome = _monthlyIncome + _otherIncome;
      _controllerTotalIncome.text = _formatCurrency.formatCurrency2(_totalIncome.toString());
    }
  }

  void calculateGrossProfitEntrepreneur() {
    var _totalIncome = _controllerTotalIncome.text.isNotEmpty ? double.parse(_controllerTotalIncome.text.replaceAll(",", "")) : 0.0;
    var _costOfGoodsSold = _controllerCostOfGoodsSold.text.isNotEmpty ? double.parse(_controllerCostOfGoodsSold.text.replaceAll(",", "")) : 0.0;
    var _grossProfit = _totalIncome - _costOfGoodsSold;
    if(_grossProfit < 0) {
      _controllerCostOfGoodsSold.clear();
      _controllerGrossProfit.clear();
    } else {
      _controllerGrossProfit.text = _formatCurrency.formatCurrency2(_grossProfit.toString());
    }
  }

  void calculateNetProfitBeforeTaxEntrepreneur() {
    var _grossProfit = _controllerGrossProfit.text.isNotEmpty ? double.parse(_controllerGrossProfit.text.replaceAll(",", "")) : 0.0;
    var _operatingCosts = _controllerOperatingCosts.text.isNotEmpty ? double.parse(_controllerOperatingCosts.text.replaceAll(",", "")) : 0.0;
    var _otherCosts = _controllerOtherCosts.text.isNotEmpty ? double.parse(_controllerOtherCosts.text.replaceAll(",", "")) : 0.0;
    var _netProfitBeforeTax = _grossProfit - _operatingCosts - _otherCosts;
    if(_netProfitBeforeTax < 0) {
      _controllerOperatingCosts.clear();
      _controllerOtherCosts.clear();
      _controllerNetProfitBeforeTax.clear();
    } else {
      _controllerNetProfitBeforeTax.text = _formatCurrency.formatCurrency2(_netProfitBeforeTax.toString());
    }
  }

  void calculateNetProfitAfterTaxEntrepreneur() {
    var _netProfitBeforeTax = _controllerNetProfitBeforeTax.text.isNotEmpty ? double.parse(_controllerNetProfitBeforeTax.text.replaceAll(",", "")) : 0.0;
    var _tax = _controllerTax.text.isNotEmpty ? double.parse(_controllerTax.text.replaceAll(",", "")) : 0.0;
    var _netProfitAfterTax = _netProfitBeforeTax - _tax;
    if(_netProfitAfterTax < 0) {
      _controllerTax.clear();
      _controllerNetProfitAfterTax.clear();
    } else {
      _controllerNetProfitAfterTax.text = _formatCurrency.formatCurrency2(_netProfitAfterTax.toString());
    }
  }

  void calculatedRestOfIncomeEntrepreneur() {
    var _netProfitATax = _controllerNetProfitAfterTax.text.isNotEmpty ? double.parse(_controllerNetProfitAfterTax.text.replaceAll(",", "")) : 0.0;
    var _costOfLiving = _controllerCostOfLiving.text.isNotEmpty ? double.parse(_controllerCostOfLiving.text.replaceAll(",", "")) : 0.0;
    var _restIncome = _netProfitATax - _costOfLiving;
    if(_restIncome < 0) {
      _controllerCostOfLiving.clear();
      _controllerNetProfitAfterTax.clear();
    } else {
      _controllerRestIncome.text = _formatCurrency.formatCurrency2(_restIncome.toString());
    }
  }
  // End Pekerjaan Wiraswasta

  // Pekerjaan Profesional dan Lainnya
  void calculateTotalIncomeNonEntrepreneur() {
    print('isMarried');
    print(isMarried);
    if(isMarried) {
      var _income = _controllerMonthlyIncome.text.isNotEmpty ? double.parse(_controllerMonthlyIncome.text.replaceAll(",", "")) : 0.0;
      var _spouseIncome = _controllerSpouseIncome.text.isNotEmpty ? double.parse(_controllerSpouseIncome.text.replaceAll(",", ""))  : 0.0;
      var _otherIncome = _controllerOtherIncome.text.isNotEmpty ? double.parse(_controllerOtherIncome.text.replaceAll(",", ""))  : 0.0;
      var _totalIncome = _income + _spouseIncome + _otherIncome;
      _controllerTotalIncome.text = _formatCurrency.formatCurrency2(_totalIncome.toString());
    } else {
      var _income = _controllerMonthlyIncome.text.isNotEmpty ? double.parse(_controllerMonthlyIncome.text.replaceAll(",", "")) : 0.0;
      // var _spouseIncome = 0.0;
      var _otherIncome = _controllerOtherIncome.text.isNotEmpty ? double.parse(_controllerOtherIncome.text.replaceAll(",", "")) : 0.0;
      var _totalIncome = _income + _otherIncome;
      _controllerTotalIncome.text = _formatCurrency.formatCurrency2(_totalIncome.toString());
    }
  }

  void calculateRestIncomeNonEntrepreneur() {
    var _totalIncome = _controllerTotalIncome.text.isNotEmpty ? double.parse(_controllerTotalIncome.text.replaceAll(",", "")) : 0.0;
    var _costOfLiving = _controllerCostOfLiving.text.isNotEmpty ? double.parse(_controllerCostOfLiving.text.replaceAll(",", "")) : 0.0;
    var _restIncome = _totalIncome - _costOfLiving;
    if(_restIncome < 0) {
      _controllerCostOfLiving.clear();
      _controllerRestIncome.clear();
    } else {
      _controllerRestIncome.text = _formatCurrency.formatCurrency2(_restIncome.toString());
    }
  }
  // End Pekerjaan Profesional dan Lainnya

  // formating apabila tidak di klik done pada keyboard
  void formattingWiraswasta() {
    _controllerMonthlyIncome.text = formatCurrency.formatCurrency2(_controllerMonthlyIncome.text);
    _controllerOtherIncome.text = formatCurrency.formatCurrency2(_controllerOtherIncome.text);
    _controllerCostOfGoodsSold.text = formatCurrency.formatCurrency2(_controllerCostOfGoodsSold.text);
    _controllerOperatingCosts.text = formatCurrency.formatCurrency2(_controllerOperatingCosts.text);
    _controllerOtherCosts.text = formatCurrency.formatCurrency2(_controllerOtherCosts.text);
    _controllerTax.text = formatCurrency.formatCurrency2(_controllerTax.text);
    _controllerCostOfLiving.text = formatCurrency.formatCurrency2(_controllerCostOfLiving.text);
    _controllerSpouseIncome.text = formatCurrency.formatCurrency2(_controllerSpouseIncome.text);
    _controllerOtherInstallments.text = formatCurrency.formatCurrency2(_controllerOtherInstallments.text);
  }

  void formattingProfessionalOther() {
    this._controllerMonthlyIncome.text = formatCurrency.formatCurrency2(_controllerMonthlyIncome.text);
    this._controllerSpouseIncome.text = formatCurrency.formatCurrency2(_controllerSpouseIncome.text);
    this._controllerOtherIncome.text = formatCurrency.formatCurrency2(_controllerOtherIncome.text);
    this._controllerCostOfLiving.text = formatCurrency.formatCurrency2(_controllerCostOfLiving.text);
    this._controllerOtherInstallments.text = formatCurrency.formatCurrency2(_controllerOtherInstallments.text);
  }

  void clearData(){
    this._listIncome.clear();
    this._autoValidate = false;
    this._controllerIncome.clear();
    this._controllerSpouseIncome.clear();
    this._controllerOtherIncome.clear();
    this._controllerTotalIncome.clear();
    this._controllerCostOfLiving.clear();
    this._controllerRestIncome.clear();
    this._controllerOtherInstallments.clear();
    this._controllerMonthlyIncome.clear();
    this._controllerCostOfGoodsSold.clear();
    this._controllerGrossProfit.clear();
    this._controllerOperatingCosts.clear();
    this._controllerOtherCosts.clear();
    this._controllerNetProfitBeforeTax.clear();
    this._controllerTax.clear();
    this._controllerNetProfitAfterTax.clear();
    this._customerIncomeIDMonthlyIncome = "";
    this._customerIncomeIDOtherIncome = "";
    this._customerIncomeIDTotalIncome = "";
    this._customerIncomeIDCostOfGoodsSold = "";
    this._customerIncomeIDGrossProfit = "";
    this._customerIncomeIDOperatingCosts = "";
    this._customerIncomeIDOtherCosts = "";
    this._customerIncomeIDNetProfitBeforeTax = "";
    this._customerIncomeIDTax = "";
    this._customerIncomeIDNetProfitAfterTax = "";
    this._customerIncomeIDCostOfLiving = "";
    this._customerIncomeIDSpouseIncome = "";
    this._customerIncomeIDOtherInstallments = "";
    this._customerIncomeIDRestIncome = "";

  }

  // Future<void> clearDataIncome() async {
  void clearDataIncome() {
    this._autoValidate = false;
    // Wiraswasta
    this._controllerMonthlyIncome.clear();
    this._controllerOtherIncome.clear();
    this._controllerTotalIncome.clear();
    this._controllerCostOfGoodsSold.clear();
    this._controllerGrossProfit.clear();
    this._controllerOperatingCosts.clear();
    this._controllerOtherCosts.clear();
    this._controllerNetProfitBeforeTax.clear();
    this._controllerTax.clear();
    this._controllerNetProfitAfterTax.clear();
    this._controllerCostOfLiving.clear();
    this._controllerNetProfitAfterTax.clear();
    this._controllerSpouseIncome.clear();
    this._controllerOtherInstallments.clear();
    // Profesional
    this._controllerIncome.clear();
    this._controllerSpouseIncome.clear();
    // this._controllerOtherIncome.clear();
    // this._controllerTotalIncome.clear();
    // this._controllerCostOfLiving.clear();
    this._controllerRestIncome.clear();
    // this._controllerOtherInstallments.clear();
    this._listIncome.clear();
  }

  List<MS2CustIncomeModel> _listIncome = [];

  List<MS2CustIncomeModel> get listIncome => _listIncome;

  Future<void> saveToSQLite(BuildContext context) async {
    debugPrint("save income");
    await setIsMarried(context);
    addIncome(context);
    // var occupation = Provider.of<FormMFotoChangeNotifier>(context, listen: false).occupationSelected.KODE;
    //
    // this._listIncome.clear();
    // if(occupation == "05" || occupation == "07"){
    // //  wiraswasta
    //   if(isMarried){
    //   //  nikah
    //     print("usaha nikah");
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDMonthlyIncome, "S", "001", "PENDAPATAN PERBULAN", _controllerMonthlyIncome.text.replaceAll(",", ""),
    //         null, null, null, null, 1, 1, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDOtherIncome, "+", "002", "PENDAPATAN LAINNYA", _controllerOtherIncome.text.replaceAll(",", ""),
    //         null, null, null, null, 1, 2, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDTotalIncome, "=", "003", "TOTAL PENDAPATAN", _controllerTotalIncome.text.replaceAll(",", ""),
    //         null, null, null, null, 1, 3, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDCostOfGoodsSold, "-", "004", "HARGA POKOK PENDAPATAN", _controllerCostOfGoodsSold.text.replaceAll(",", ""),
    //         null, null, null, null, 1, 4, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDGrossProfit, "=", "005", "LABA KOTOR", _controllerGrossProfit.text.replaceAll(",", ""),
    //         null, null, null, null, 1, 5, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDOperatingCosts, "-", "006", "BIAYA OPERASIONAL", _controllerOperatingCosts.text.replaceAll(",", ""),
    //         null, null, null, null, 1, 6, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDOtherCosts, "-", "007", "BIAYA LAINNYA", _controllerOtherCosts.text.replaceAll(",", ""),
    //         null, null, null, null, 1, 7, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDNetProfitBeforeTax, "=", "008", "LABA BERSIH SEBELUM PAJAK", _controllerNetProfitBeforeTax.text.replaceAll(",", ""),
    //         null, null, null, null, 1, 8, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDTax, "-", "009", "PAJAK", _controllerTax.text.replaceAll(",", ""),
    //         null, null, null, null, 1, 9, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDNetProfitAfterTax, "=", "010", "LABA BERSIH SETELAH PAJAK", _controllerNetProfitAfterTax.text.replaceAll(",", ""),
    //         null, null, null, null, 1, 10, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDCostOfLiving, "-", "011", "BIAYA HIDUP", _controllerCostOfLiving.text.replaceAll(",", ""),
    //         null, null, null, null, 1, 11, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDRestIncome, "=", "017", "SISA PENDAPATAN", _controllerRestIncome.text.replaceAll(",", ""),
    //         null, null, null, null, 1, 12, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDSpouseIncome, "N", "012", "PENDAPATAN PASANGAN", _controllerSpouseIncome.text.replaceAll(",", ""),
    //         null, null, null, null, 1, null, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDOtherInstallments, "N", "013", "ANGSURAN LAINNYA", _controllerOtherInstallments.text.replaceAll(",", ""),
    //         null, null, null, null, 1, null, null));
    //   }
    //   else {
    //     print("usaha belum nikah");
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDMonthlyIncome, "S", "001", "PENDAPATAN PERBULAN", _controllerMonthlyIncome.text.replaceAll(",", ""),
    //         null, null, null, null, 1, 1, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDOtherIncome, "+", "002", "PENDAPATAN LAINNYA", _controllerOtherIncome.text.replaceAll(",", ""),
    //         null, null, null, null, 1, 2, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDTotalIncome, "=", "003", "TOTAL PENDAPATAN", _controllerTotalIncome.text.replaceAll(",", ""),
    //         null, null, null, null, 1, 3, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDCostOfGoodsSold, "-", "004", "HARGA POKOK PENDAPATAN", _controllerCostOfGoodsSold.text.replaceAll(",", ""),
    //         null, null, null, null, 1, 4, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDGrossProfit, "=", "005", "LABA KOTOR", _controllerGrossProfit.text.replaceAll(",", ""),
    //         null, null, null, null, 1, 5, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDOperatingCosts, "-", "006", "BIAYA OPERASIONAL", _controllerOperatingCosts.text.replaceAll(",", ""),
    //         null, null, null, null, 1, 6, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDOtherCosts, "-", "007", "BIAYA LAINNYA", _controllerOtherCosts.text.replaceAll(",", ""),
    //         null, null, null, null, 1, 7, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDNetProfitBeforeTax, "=", "008", "LABA BERSIH SEBELUM PAJAK", _controllerNetProfitBeforeTax.text.replaceAll(",", ""),
    //         null, null, null, null, 1, 8, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDTax, "-", "009", "PAJAK", _controllerTax.text.replaceAll(",", ""),
    //         null, null, null, null, 1, 9, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDNetProfitAfterTax, "=", "010", "LABA BERSIH SETELAH PAJAK", _controllerNetProfitAfterTax.text.replaceAll(",", ""),
    //         null, null, null, null, 1, 10, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDCostOfLiving, "-", "011", "BIAYA HIDUP", _controllerCostOfLiving.text.replaceAll(",", ""),
    //         null, null, null, null, 1, 11, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDRestIncome, "=", "017", "SISA PENDAPATAN", _controllerRestIncome.text.replaceAll(",", ""),
    //         null, null, null, null, 1, 12, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDOtherInstallments, "N", "013", "ANGSURAN LAINNYA", _controllerOtherInstallments.text.replaceAll(",", ""),
    //         null, null, null, null, 1, null, null));
    //   }
    // }
    // else {
    // //  pegawai
    //   if(isMarried){
    //     print("pegawai married");
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDMonthlyIncome, "S", "016", "PENDAPATAN", _controllerMonthlyIncome.text,
    //       null, null, null, null, 1, 1, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDSpouseIncome, "+", "012", "PENDAPATAN PASANGAN", _controllerSpouseIncome.text,
    //         null, null, null, null, 1, null, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDOtherIncome, "+", "002", "PENDAPATAN LAINNYA", _controllerOtherIncome.text,
    //         null, null, null, null, 1, 2, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDTotalIncome, "=", "003", "TOTAL PENDAPATAN", _controllerTotalIncome.text,
    //         null, null, null, null, 1, 3, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDCostOfLiving, "-", "011", "BIAYA HIDUP", _controllerCostOfLiving.text,
    //         null, null, null, null, 1, 11, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDRestIncome, "=", "017", "SISA PENDAPATAN", _controllerRestIncome.text,
    //         null, null, null, null, 1, 12, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDOtherInstallments, "N", "013", "ANGSURAN LAINNYA", _controllerOtherInstallments.text,
    //         null, null, null, null, 1, null, null));
    //   }
    //   else {
    //     print("pegawai tdk married");
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDMonthlyIncome, "S", "016", "PENDAPATAN", _controllerMonthlyIncome.text,
    //         null, null, null, null, 1, 1, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDOtherIncome, "+", "002", "PENDAPATAN LAINNYA", _controllerOtherIncome.text,
    //         null, null, null, null, 1, 2, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDTotalIncome, "=", "003", "TOTAL PENDAPATAN", _controllerTotalIncome.text,
    //         null, null, null, null, 1, 3, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDCostOfLiving, "-", "011", "BIAYA HIDUP", _controllerCostOfLiving.text,
    //         null, null, null, null, 1, 11, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDRestIncome, "=", "017", "SISA PENDAPATAN", _controllerRestIncome.text,
    //         null, null, null, null, 1, 12, null));
    //     _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDOtherInstallments, "N", "013", "ANGSURAN LAINNYA", _controllerOtherInstallments.text,
    //         null, null, null, null, 1, null, null));
    //   }
    // }
    _dbHelper.insertMS2CustIncome(_listIncome);
    // String _message = await _submitDataPartial.submitIncome(idx,_listIncome);
    // return _message;
  }

  Future<bool> deleteSQLite() async{
    return await _dbHelper.deleteMS2CustIncome();
  }


  Future<void> setDataFromSQLite(BuildContext context, int index) async{
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    List _data = await _dbHelper.selectDataIncome();
    if(_data.isNotEmpty){
      for(int i=0; i<_data.length; i++){
        debugPrint("CHECK_customerIncomeID ${_data[i]['customerIncomeID'].toString()}");
        if(_data[i]['income_type'] == "001" || _data[i]['income_type'] == "016"){
          this._customerIncomeIDMonthlyIncome = _data[i]['customerIncomeID'].toString();
          this._controllerMonthlyIncome.text = formatCurrency.formatCurrency2(_data[i]['income_value'].toString());
          if(_occupation == "05" || _occupation == "07") {
            this._editIncomePerbulanWiraswasta = _data[i]['edit_income_value'] == "1";
          } else {
            this._editIncomeProfesional = _data[i]['edit_income_value'] == "1";
          }
        }
        else if(_data[i]['income_type'] == "002"){
          this._customerIncomeIDOtherIncome = _data[i]['customerIncomeID'].toString();
          this._controllerOtherIncome.text = formatCurrency.formatCurrency2(_data[i]['income_value'].toString());
          if(_occupation == "05" || _occupation == "07") {
            this._editIncomeLainnyaWiraswasta = _data[i]['edit_income_value'] == "1";
          } else {
            this._editIncomeLainnyaProfesional = _data[i]['edit_income_value'] == "1";
          }
        }
        else if(_data[i]['income_type'] == "003"){
          this._customerIncomeIDTotalIncome = _data[i]['customerIncomeID'].toString();
          this._controllerTotalIncome.text = formatCurrency.formatCurrency2(_data[i]['income_value'].toString());
          if(_occupation == "05" || _occupation == "07") {
            this._editTotalIncomeWiraswasta = _data[i]['edit_income_value'] == "1";
          } else {
            this._editTotalIncomeProfesional = _data[i]['edit_income_value'] == "1";
          }
        }
        else if(_data[i]['income_type'] == "004"){
          this._customerIncomeIDCostOfGoodsSold = _data[i]['customerIncomeID'].toString();
          this._controllerCostOfGoodsSold.text = formatCurrency.formatCurrency2(_data[i]['income_value'].toString());
          if(_occupation == "05" || _occupation == "07") {
            this._editPokokIncomeWiraswasta = _data[i]['edit_income_value'] == "1";
          }
        }
        else if(_data[i]['income_type'] == "005"){
          this._customerIncomeIDGrossProfit = _data[i]['customerIncomeID'].toString();
          this._controllerGrossProfit.text = formatCurrency.formatCurrency2(_data[i]['income_value'].toString());
          if(_occupation == "05" || _occupation == "07") {
            this._editLabaKotorWiraswasta = _data[i]['edit_income_value'] == "1";
          }
        }
        else if(_data[i]['income_type'] == "006"){
          this._customerIncomeIDOperatingCosts = _data[i]['customerIncomeID'].toString();
          this._controllerOperatingCosts.text = formatCurrency.formatCurrency2(_data[i]['income_value'].toString());
          if(_occupation == "05" || _occupation == "07") {
            this._editBiayaOperasionalWiraswasta = _data[i]['edit_income_value'] == "1";
          }
        }
        else if(_data[i]['income_type'] == "007"){
          this._customerIncomeIDOtherCosts = _data[i]['customerIncomeID'].toString();
          this._controllerOtherCosts.text = formatCurrency.formatCurrency2(_data[i]['income_value'].toString());
          if(_occupation == "05" || _occupation == "07") {
            this._editBiayaLainnyaWiraswasta = _data[i]['edit_income_value'] == "1";
          }
        }
        else if(_data[i]['income_type'] == "008"){
          this._customerIncomeIDNetProfitBeforeTax = _data[i]['customerIncomeID'].toString();
          this._controllerNetProfitBeforeTax.text = formatCurrency.formatCurrency2(_data[i]['income_value'].toString());
          if(_occupation == "05" || _occupation == "07") {
            this._editNetSebelumPajakWiraswasta = _data[i]['edit_income_value'] == "1";
          }
        }
        else if(_data[i]['income_type'] == "009"){
          this._customerIncomeIDTax = _data[i]['customerIncomeID'].toString();
          this._controllerTax.text = formatCurrency.formatCurrency2(_data[i]['income_value'].toString());
          if(_occupation == "05" || _occupation == "07") {
            this._editPajakWiraswasta = _data[i]['edit_income_value'] == "1";
          }
        }
        else if(_data[i]['income_type'] == "010"){
          this._customerIncomeIDNetProfitAfterTax = _data[i]['customerIncomeID'].toString();
          this._controllerNetProfitAfterTax.text = formatCurrency.formatCurrency2(_data[i]['income_value'].toString());
          if(_occupation == "05" || _occupation == "07") {
            this._editNetSetelahPajakWiraswasta = _data[i]['edit_income_value'] == "1";
          }
        }
        else if(_data[i]['income_type'] == "011"){
          this._customerIncomeIDCostOfLiving = _data[i]['customerIncomeID'].toString();
          this._controllerCostOfLiving.text = formatCurrency.formatCurrency2(_data[i]['income_value'].toString());
          if(_occupation == "05" || _occupation == "07") {
            this._editBiayaHidupWiraswasta = _data[i]['edit_income_value'] == "1";
          } else {
            this._editBiayaHidupProfesional = _data[i]['edit_income_value'] == "1";
          }
        }
        else if(_data[i]['income_type'] == "012"){
          this._customerIncomeIDSpouseIncome = _data[i]['customerIncomeID'].toString();
          this._controllerSpouseIncome.text = formatCurrency.formatCurrency2(_data[i]['income_value'].toString());
          if(_occupation == "05" || _occupation == "07") {
            this._editIncomePasanganWiraswasta = _data[i]['edit_income_value'] == "1";
          } else {
            this._editIncomePasanganProfesional = _data[i]['edit_income_value'] == "1";
          }
        }
        else if(_data[i]['income_type'] == "013"){
          this._customerIncomeIDOtherInstallments = _data[i]['customerIncomeID'].toString();
          this._controllerOtherInstallments.text = formatCurrency.formatCurrency2(_data[i]['income_value'].toString());
          if(_occupation == "05" || _occupation == "07") {
            this._editAngsuranLainnyaWiraswasta = _data[i]['edit_income_value'] == "1";
          } else {
            this._editAngsuranLainnyaProfesional = _data[i]['edit_income_value'] == "1";
          }
        }
        else if(_data[i]['income_type'] == "017"){
          this._customerIncomeIDRestIncome = _data[i]['customerIncomeID'].toString();
          this._controllerRestIncome.text = formatCurrency.formatCurrency2(_data[i]['income_value'].toString());
          if(_occupation == "05" || _occupation == "07") {
            this._editSisaIncomeWiraswasta = _data[i]['edit_income_value'] == "1";
          } else {
            this._editSisaIncomeProfesional = _data[i]['edit_income_value'] == "1";
          }
        }
      }
    }

    addIncome(context);

    if(_preferences.getString("last_known_state") == "IDE" && index != null){
      if(index != 3){
        var _providerGuarantor = Provider.of<FormMGuarantorChangeNotifier>(context, listen: false);
        var _providerInfoApp = Provider.of<InfoAppChangeNotifier>(context, listen: false);
        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).isIncomeDone = true;
        if(Provider.of<FormMInfoNasabahChangeNotif>(context,listen: false).isGuarantorVisible){
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).selectedIndex +=1;
          await _providerGuarantor.setDataFromSQLite(context, index, 1);
        } else {
          await _providerInfoApp.addNumberOfUnitList(context, index);
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).selectedIndex +=2;
        }
      }
      else{
        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).selectedIndex = 3;
      }
    }
    await checkDataDakor();
    notifyListeners();
  }

  void setShowMandatoryIncome(BuildContext context){
    _showMandatoryIncomeModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryIncomeModel;
  }

  bool isIncomePerbulanWiraswastaShow() => _showMandatoryIncomeModel.isIncomePerbulanWiraswastaShow;
  bool isIncomeLainnyaWiraswastaShow() => _showMandatoryIncomeModel.isIncomeLainnyaWiraswastaShow;
  bool isTotalIncomeWiraswastaShow() => _showMandatoryIncomeModel.isTotalIncomeWiraswastaShow;
  bool isPokokIncomeWiraswastaShow() => _showMandatoryIncomeModel.isPokokIncomeWiraswastaShow;
  bool isLabaKotorWiraswastaShow() => _showMandatoryIncomeModel.isLabaKotorWiraswastaShow;
  bool isBiayaOperasionalWiraswastaShow() => _showMandatoryIncomeModel.isBiayaOperasionalWiraswastaShow;
  bool isBiayaLainnyaWiraswastaShow() => _showMandatoryIncomeModel.isBiayaLainnyaWiraswastaShow;
  bool isNetSebelumPajakWiraswastaShow() => _showMandatoryIncomeModel.isNetSebelumPajakWiraswastaShow;
  bool isPajakWiraswastaShow() => _showMandatoryIncomeModel.isPajakWiraswastaShow;
  bool isNetSetelahPajakWiraswastaShow() => _showMandatoryIncomeModel.isNetSetelahPajakWiraswastaShow;
  bool isBiayaHidupWiraswastaShow() => _showMandatoryIncomeModel.isBiayaHidupWiraswastaShow;
  bool isSisaIncomeWiraswastaShow() => _showMandatoryIncomeModel.isSisaIncomeWiraswastaShow;
  bool isIncomePasanganWiraswastaShow() => _showMandatoryIncomeModel.isIncomePasanganWiraswastaShow;
  bool isAngsuranLainnyaWiraswastaShow() => _showMandatoryIncomeModel.isAngsuranLainnyaWiraswastaShow;
  bool isIncomeProfesionalShow() => _showMandatoryIncomeModel.isIncomeProfesionalShow;
  bool isIncomePasanganProfesionalShow() => _showMandatoryIncomeModel.isIncomePasanganProfesionalShow;
  bool isIncomeLainnyaProfesionalShow() => _showMandatoryIncomeModel.isIncomeLainnyaProfesionalShow;
  bool isTotalIncomeProfesionalShow() => _showMandatoryIncomeModel.isTotalIncomeProfesionalShow;
  bool isBiayaHidupProfesionalShow() => _showMandatoryIncomeModel.isBiayaHidupProfesionalShow;
  bool isSisaIncomeProfesionalShow() => _showMandatoryIncomeModel.isSisaIncomeProfesionalShow;
  bool isAngsuranLainnyaProfesionalShow() => _showMandatoryIncomeModel.isAngsuranLainnyaProfesionalShow;
  bool isIncomePerbulanWiraswastaMandatory() => _showMandatoryIncomeModel.isIncomePerbulanWiraswastaMandatory;
  bool isIncomeLainnyaWiraswastaMandatory() => _showMandatoryIncomeModel.isIncomeLainnyaWiraswastaMandatory;
  bool isTotalIncomeWiraswastaMandatory() => _showMandatoryIncomeModel.isTotalIncomeWiraswastaMandatory;
  bool isPokokIncomeWiraswastaMandatory() => _showMandatoryIncomeModel.isPokokIncomeWiraswastaMandatory;
  bool isLabaKotorWiraswastaMandatory() => _showMandatoryIncomeModel.isLabaKotorWiraswastaMandatory;
  bool isBiayaOperasionalWiraswastaMandatory() => _showMandatoryIncomeModel.isBiayaOperasionalWiraswastaMandatory;
  bool isBiayaLainnyaWiraswastaMandatory() => _showMandatoryIncomeModel.isBiayaLainnyaWiraswastaMandatory;
  bool isNetSebelumPajakWiraswastaMandatory() => _showMandatoryIncomeModel.isNetSebelumPajakWiraswastaMandatory;
  bool isPajakWiraswastaMandatory() => _showMandatoryIncomeModel.isPajakWiraswastaMandatory;
  bool isNetSetelahPajakWiraswastaMandatory() => _showMandatoryIncomeModel.isNetSetelahPajakWiraswastaMandatory;
  bool isBiayaHidupWiraswastaMandatory() => _showMandatoryIncomeModel.isBiayaHidupWiraswastaMandatory;
  bool isSisaIncomeWiraswastaMandatory() => _showMandatoryIncomeModel.isSisaIncomeWiraswastaMandatory;
  bool isIncomePasanganWiraswastaMandatory() => _showMandatoryIncomeModel.isIncomePasanganWiraswastaMandatory;
  bool isAngsuranLainnyaWiraswastaMandatory() => _showMandatoryIncomeModel.isAngsuranLainnyaWiraswastaMandatory;
  bool isIncomeProfesionalMandatory() => _showMandatoryIncomeModel.isIncomeProfesionalMandatory;
  bool isIncomePasanganProfesionalMandatory() => _showMandatoryIncomeModel.isIncomePasanganProfesionalMandatory;
  bool isIncomeLainnyaProfesionalMandatory() => _showMandatoryIncomeModel.isIncomeLainnyaProfesionalMandatory;
  bool isTotalIncomeProfesionalMandatory() => _showMandatoryIncomeModel.isTotalIncomeProfesionalMandatory;
  bool isBiayaHidupProfesionalMandatory() => _showMandatoryIncomeModel.isBiayaHidupProfesionalMandatory;
  bool isSisaIncomeProfesionalMandatory() => _showMandatoryIncomeModel.isSisaIncomeProfesionalMandatory;
  bool isAngsuranLainnyaProfesionalMandatory() => _showMandatoryIncomeModel.isAngsuranLainnyaProfesionalMandatory;

  bool _editIncomePerbulanWiraswasta = false;
  bool _editIncomeLainnyaWiraswasta = false;
  bool _editTotalIncomeWiraswasta = false;
  bool _editPokokIncomeWiraswasta = false;
  bool _editLabaKotorWiraswasta = false;
  bool _editBiayaOperasionalWiraswasta = false;
  bool _editBiayaLainnyaWiraswasta = false;
  bool _editNetSebelumPajakWiraswasta = false;
  bool _editPajakWiraswasta = false;
  bool _editNetSetelahPajakWiraswasta = false;
  bool _editBiayaHidupWiraswasta = false;
  bool _editSisaIncomeWiraswasta = false;
  bool _editIncomePasanganWiraswasta = false;
  bool _editAngsuranLainnyaWiraswasta = false;

  bool _editIncomeProfesional = false;
  bool _editIncomePasanganProfesional = false;
  bool _editIncomeLainnyaProfesional = false;
  bool _editTotalIncomeProfesional = false;
  bool _editBiayaHidupProfesional = false;
  bool _editSisaIncomeProfesional = false;
  bool _editAngsuranLainnyaProfesional = false;

  bool get editIncomePerbulanWiraswasta => _editIncomePerbulanWiraswasta;

  set editIncomePerbulanWiraswasta(bool value) {
    this._editIncomePerbulanWiraswasta = value;
  }

  bool get editIncomeLainnyaWiraswasta => _editIncomeLainnyaWiraswasta;

  set editIncomeLainnyaWiraswasta(bool value) {
    this._editIncomeLainnyaWiraswasta = value;
  }

  bool get editTotalIncomeWiraswasta => _editTotalIncomeWiraswasta;

  set editTotalIncomeWiraswasta(bool value) {
    this._editTotalIncomeWiraswasta = value;
  }

  bool get editPokokIncomeWiraswasta => _editPokokIncomeWiraswasta;

  set editPokokIncomeWiraswasta(bool value) {
    this._editPokokIncomeWiraswasta = value;
  }

  bool get editLabaKotorWiraswasta => _editLabaKotorWiraswasta;

  set editLabaKotorWiraswasta(bool value) {
    this._editLabaKotorWiraswasta = value;
  }

  bool get editBiayaOperasionalWiraswasta => _editBiayaOperasionalWiraswasta;

  set editBiayaOperasionalWiraswasta(bool value) {
    this._editBiayaOperasionalWiraswasta = value;
  }

  bool get editBiayaLainnyaWiraswasta => _editBiayaLainnyaWiraswasta;

  set editBiayaLainnyaWiraswasta(bool value) {
    this._editBiayaLainnyaWiraswasta = value;
  }

  bool get editNetSebelumPajakWiraswasta => _editNetSebelumPajakWiraswasta;

  set editNetSebelumPajakWiraswasta(bool value) {
    this._editNetSebelumPajakWiraswasta = value;
  }

  bool get editPajakWiraswasta => _editPajakWiraswasta;

  set editPajakWiraswasta(bool value) {
    this._editPajakWiraswasta = value;
  }

  bool get editNetSetelahPajakWiraswasta => _editNetSetelahPajakWiraswasta;

  set editNetSetelahPajakWiraswasta(bool value) {
    this._editNetSetelahPajakWiraswasta = value;
  }

  bool get editBiayaHidupWiraswasta => _editBiayaHidupWiraswasta;

  set editBiayaHidupWiraswasta(bool value) {
    this._editBiayaHidupWiraswasta = value;
  }

  bool get editSisaIncomeWiraswasta => _editSisaIncomeWiraswasta;

  set editSisaIncomeWiraswasta(bool value) {
    this._editSisaIncomeWiraswasta = value;
  }

  bool get editIncomePasanganWiraswasta => _editIncomePasanganWiraswasta;

  set editIncomePasanganWiraswasta(bool value) {
    this._editIncomePasanganWiraswasta = value;
  }

  bool get editAngsuranLainnyaWiraswasta => _editAngsuranLainnyaWiraswasta;

  set editAngsuranLainnyaWiraswasta(bool value) {
    this._editAngsuranLainnyaWiraswasta = value;
  }

  bool get editIncomeProfesional => _editIncomeProfesional;

  set editIncomeProfesional(bool value) {
    this._editIncomeProfesional = value;
  }

  bool get editIncomePasanganProfesional => _editIncomePasanganProfesional;

  set editIncomePasanganProfesional(bool value) {
    this._editIncomePasanganProfesional = value;
  }

  bool get editIncomeLainnyaProfesional => _editIncomeLainnyaProfesional;

  set editIncomeLainnyaProfesional(bool value) {
    this._editIncomeLainnyaProfesional = value;
  }

  bool get editTotalIncomeProfesional => _editTotalIncomeProfesional;

  set editTotalIncomeProfesional(bool value) {
    this._editTotalIncomeProfesional = value;
  }

  bool get editBiayaHidupProfesional => _editBiayaHidupProfesional;

  set editBiayaHidupProfesional(bool value) {
    this._editBiayaHidupProfesional = value;
  }

  bool get editSisaIncomeProfesional => _editSisaIncomeProfesional;

  set editSisaIncomeProfesional(bool value) {
    this._editSisaIncomeProfesional = value;
  }

  bool get editAngsuranLainnyaProfesional => _editAngsuranLainnyaProfesional;

  set editAngsuranLainnyaProfesional(bool value) {
    this._editAngsuranLainnyaProfesional = value;
  }

  Future<void> checkDataDakor() async{
    List _data = await _dbHelper.selectDataInfoNasabah();

    if(_data.isNotEmpty && _lastKnownState == "DKR") {
      if(this._occupation == "05" || this._occupation == "07") {
        for(int i=0; i<_data.length; i++){
          if(_data[i]['income_type'] == "001" || _data[i]['income_type'] == "016"){
            this._editIncomePerbulanWiraswasta = this._controllerMonthlyIncome.text != _data[i]['income_value'] || _data[i]['edit_income_value'] == "1";
          }
          else if(_data[i]['income_type'] == "002"){
            this._editIncomeLainnyaWiraswasta = this._controllerOtherIncome.text != _data[i]['income_value'] || _data[i]['edit_income_value'] == "1";
          }
          else if(_data[i]['income_type'] == "003"){
            this._editTotalIncomeWiraswasta = this._controllerTotalIncome.text != _data[i]['income_value'] || _data[i]['edit_income_value'] == "1";
          }
          else if(_data[i]['income_type'] == "004"){
            this._editPokokIncomeWiraswasta = this._controllerCostOfGoodsSold.text != _data[i]['income_value'] || _data[i]['edit_income_value'] == "1";
          }
          else if(_data[i]['income_type'] == "005"){
            this._editLabaKotorWiraswasta = this._controllerGrossProfit.text != _data[i]['income_value'] || _data[i]['edit_income_value'] == "1";
          }
          else if(_data[i]['income_type'] == "006"){
            this._editBiayaOperasionalWiraswasta = this._controllerOperatingCosts.text != _data[i]['income_value'] || _data[i]['edit_income_value'] == "1";
          }
          else if(_data[i]['income_type'] == "007"){
            this._editBiayaLainnyaWiraswasta = this._controllerOtherCosts.text != _data[i]['income_value'] || _data[i]['edit_income_value'] == "1";
          }
          else if(_data[i]['income_type'] == "008"){
            this._editNetSebelumPajakWiraswasta = this._controllerNetProfitBeforeTax.text != _data[i]['income_value'] || _data[i]['edit_income_value'] == "1";
          }
          else if(_data[i]['income_type'] == "009"){
            this._editPajakWiraswasta = this._controllerTax.text != _data[i]['income_value'] || _data[i]['edit_income_value'] == "1";
          }
          else if(_data[i]['income_type'] == "010"){
            this._editNetSetelahPajakWiraswasta = this._controllerNetProfitAfterTax.text != _data[i]['income_value'] || _data[i]['edit_income_value'] == "1";
          }
          else if(_data[i]['income_type'] == "011"){
            this._editBiayaHidupWiraswasta = this._controllerCostOfLiving.text != _data[i]['income_value'] || _data[i]['edit_income_value'] == "1";
          }
          else if(_data[i]['income_type'] == "012"){
            this._editSisaIncomeWiraswasta = this._controllerSpouseIncome.text != _data[i]['income_value'] || _data[i]['edit_income_value'] == "1";
          }
          else if(_data[i]['income_type'] == "013"){
            this._editIncomePasanganWiraswasta = this._controllerOtherInstallments.text != _data[i]['income_value'] || _data[i]['edit_income_value'] == "1";
          }
          else if(_data[i]['income_type'] == "017"){
            this._editAngsuranLainnyaWiraswasta = this._controllerRestIncome.text != _data[i]['income_value'] || _data[i]['edit_income_value'] == "1";
          }
        }
      } else {
        for(int i=0; i<_data.length; i++){
          if(_data[i]['income_type'] == "001" || _data[i]['income_type'] == "016"){
            this._editIncomeProfesional = this._controllerMonthlyIncome.text != _data[i]['income_value'] || _data[i]['edit_income_value'] == "1"; // dipakai
          }
          else if(_data[i]['income_type'] == "002"){
            this._editIncomeLainnyaProfesional = this._controllerOtherIncome.text != _data[i]['income_value'] || _data[i]['edit_income_value'] == "1"; // dipakai
          }
          else if(_data[i]['income_type'] == "003"){
            this._editTotalIncomeProfesional = this._controllerTotalIncome.text != _data[i]['income_value'] || _data[i]['edit_income_value'] == "1"; // dipakai
          }
          // else if(_data[i]['income_type'] == "004"){
          //   this._editPokokIncomeWiraswasta = this._controllerCostOfGoodsSold.text != _data[i]['income_value'] || _data[i]['edit_income_value'] == "1";
          // }
          // else if(_data[i]['income_type'] == "005"){
          //   this._editLabaKotorWiraswasta = this._controllerGrossProfit.text != _data[i]['income_value'] || _data[i]['edit_income_value'] == "1";
          // }
          // else if(_data[i]['income_type'] == "006"){
          //   this._editBiayaOperasionalWiraswasta = this._controllerOperatingCosts.text != _data[i]['income_value'] || _data[i]['edit_income_value'] == "1";
          // }
          // else if(_data[i]['income_type'] == "007"){
          //   this._editBiayaLainnyaWiraswasta = this._controllerOtherCosts.text != _data[i]['income_value'] || _data[i]['edit_income_value'] == "1";
          // }
          // else if(_data[i]['income_type'] == "008"){
          //   this._editNetSebelumPajakWiraswasta = this._controllerNetProfitBeforeTax.text != _data[i]['income_value'] || _data[i]['edit_income_value'] == "1";
          // }
          // else if(_data[i]['income_type'] == "009"){
          //   this._editPajakWiraswasta = this._controllerTax.text != _data[i]['income_value'] || _data[i]['edit_income_value'] == "1";
          // }
          // else if(_data[i]['income_type'] == "010"){
          //   this._editNetSetelahPajakWiraswasta = this._controllerNetProfitAfterTax.text != _data[i]['income_value'] || _data[i]['edit_income_value'] == "1";
          // }
          else if(_data[i]['income_type'] == "011"){
            this._editBiayaHidupProfesional = this._controllerCostOfLiving.text != _data[i]['income_value'] || _data[i]['edit_income_value'] == "1"; // dipakai
          }
          else if(_data[i]['income_type'] == "012"){
            this._editSisaIncomeProfesional = this._controllerRestIncome.text != _data[i]['income_value'] || _data[i]['edit_income_value'] == "1"; // dipakai
          }
          else if(_data[i]['income_type'] == "013"){
            this._editIncomePasanganProfesional = this._controllerSpouseIncome.text != _data[i]['income_value'] || _data[i]['edit_income_value'] == "1"; // dipakai
          }
          else if(_data[i]['income_type'] == "017"){
            this._editAngsuranLainnyaProfesional = this._controllerOtherInstallments.text != _data[i]['income_value'] || _data[i]['edit_income_value'] == "1"; // dipakai
          }
        }
      }
    }
  }

  void addIncome(BuildContext context){
    var occupation = Provider.of<FormMFotoChangeNotifier>(context, listen: false).occupationSelected != null ? Provider.of<FormMFotoChangeNotifier>(context, listen: false).occupationSelected.KODE : '';

    this._listIncome.clear();

    if(occupation == "05" || occupation == "07"){
      //  wiraswasta
      if(isMarried){
        //  nikah
        print("usaha nikah");
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDMonthlyIncome != "" ? _customerIncomeIDMonthlyIncome : "NEW", "S", "001", "PENDAPATAN PERBULAN", _controllerMonthlyIncome.text.replaceAll(",", ""),
            null, null, null, null, 1, 1, editIncomePerbulanWiraswasta ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDOtherIncome != "" ? _customerIncomeIDOtherIncome : "NEW", "+", "002", "PENDAPATAN LAINNYA", _controllerOtherIncome.text.replaceAll(",", ""),
            null, null, null, null, 1, 2, editIncomeLainnyaWiraswasta ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDTotalIncome != "" ? _customerIncomeIDTotalIncome : "NEW", "=", "003", "TOTAL PENDAPATAN", _controllerTotalIncome.text.replaceAll(",", ""),
            null, null, null, null, 1, 3, editTotalIncomeWiraswasta ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDCostOfGoodsSold != "" ? _customerIncomeIDCostOfGoodsSold : "NEW", "-", "004", "HARGA POKOK PENDAPATAN", _controllerCostOfGoodsSold.text.replaceAll(",", ""),
            null, null, null, null, 1, 4, editPokokIncomeWiraswasta ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDGrossProfit != "" ? _customerIncomeIDGrossProfit : "NEW", "=", "005", "LABA KOTOR", _controllerGrossProfit.text.replaceAll(",", ""),
            null, null, null, null, 1, 5, editLabaKotorWiraswasta ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDOperatingCosts != "" ? _customerIncomeIDOperatingCosts : "NEW", "-", "006", "BIAYA OPERASIONAL", _controllerOperatingCosts.text.replaceAll(",", ""),
            null, null, null, null, 1, 6, editBiayaOperasionalWiraswasta ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDOtherCosts != "" ? _customerIncomeIDOtherCosts : "NEW", "-", "007", "BIAYA LAINNYA", _controllerOtherCosts.text.replaceAll(",", ""),
            null, null, null, null, 1, 7, editBiayaLainnyaWiraswasta ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDNetProfitBeforeTax != "" ? _customerIncomeIDNetProfitBeforeTax : "NEW", "=", "008", "LABA BERSIH SEBELUM PAJAK", _controllerNetProfitBeforeTax.text.replaceAll(",", ""),
            null, null, null, null, 1, 8, editNetSebelumPajakWiraswasta ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDTax != "" ? _customerIncomeIDTax : "NEW", "-", "009", "PAJAK", _controllerTax.text.replaceAll(",", ""),
            null, null, null, null, 1, 9, editPajakWiraswasta ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDNetProfitAfterTax != "" ? _customerIncomeIDNetProfitAfterTax : "NEW", "=", "010", "LABA BERSIH SETELAH PAJAK", _controllerNetProfitAfterTax.text.replaceAll(",", ""),
            null, null, null, null, 1, 10, editNetSetelahPajakWiraswasta ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDCostOfLiving != "" ? _customerIncomeIDCostOfLiving : "NEW", "-", "011", "BIAYA HIDUP", _controllerCostOfLiving.text.replaceAll(",", ""),
            null, null, null, null, 1, 11, editBiayaHidupWiraswasta ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDRestIncome != "" ? _customerIncomeIDRestIncome : "NEW", "=", "017", "SISA PENDAPATAN", _controllerRestIncome.text.replaceAll(",", ""),
            null, null, null, null, 1, 12, editSisaIncomeWiraswasta ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDSpouseIncome != "" ? _customerIncomeIDSpouseIncome : "NEW", "N", "012", "PENDAPATAN PASANGAN", _controllerSpouseIncome.text.replaceAll(",", ""),
            null, null, null, null, 1, null, editIncomePasanganWiraswasta ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDOtherInstallments != "" ? _customerIncomeIDOtherInstallments : "NEW", "N", "013", "ANGSURAN LAINNYA", _controllerOtherInstallments.text.replaceAll(",", ""),
            null, null, null, null, 1, null, editAngsuranLainnyaWiraswasta ? "1" : "0"));
      }
      else {
        print("usaha belum nikah");
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDMonthlyIncome != "" ? _customerIncomeIDMonthlyIncome : "NEW", "S", "001", "PENDAPATAN PERBULAN", _controllerMonthlyIncome.text.replaceAll(",", ""),
            null, null, null, null, 1, 1, editIncomePerbulanWiraswasta ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDOtherIncome != "" ? _customerIncomeIDOtherIncome : "NEW", "+", "002", "PENDAPATAN LAINNYA", _controllerOtherIncome.text.replaceAll(",", ""),
            null, null, null, null, 1, 2, editIncomeLainnyaWiraswasta ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDTotalIncome != "" ? _customerIncomeIDTotalIncome : "NEW", "=", "003", "TOTAL PENDAPATAN", _controllerTotalIncome.text.replaceAll(",", ""),
            null, null, null, null, 1, 3, editTotalIncomeWiraswasta ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDCostOfGoodsSold != "" ? _customerIncomeIDCostOfGoodsSold : "NEW", "-", "004", "HARGA POKOK PENDAPATAN", _controllerCostOfGoodsSold.text.replaceAll(",", ""),
            null, null, null, null, 1, 4, editPokokIncomeWiraswasta ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDGrossProfit != "" ? _customerIncomeIDGrossProfit : "NEW", "=", "005", "LABA KOTOR", _controllerGrossProfit.text.replaceAll(",", ""),
            null, null, null, null, 1, 5, editLabaKotorWiraswasta ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDOperatingCosts != "" ? _customerIncomeIDOperatingCosts : "NEW", "-", "006", "BIAYA OPERASIONAL", _controllerOperatingCosts.text.replaceAll(",", ""),
            null, null, null, null, 1, 6, editBiayaOperasionalWiraswasta ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDOtherCosts != "" ? _customerIncomeIDOtherCosts : "NEW", "-", "007", "BIAYA LAINNYA", _controllerOtherCosts.text.replaceAll(",", ""),
            null, null, null, null, 1, 7, editBiayaLainnyaWiraswasta ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDNetProfitBeforeTax != "" ? _customerIncomeIDNetProfitBeforeTax : "NEW", "=", "008", "LABA BERSIH SEBELUM PAJAK", _controllerNetProfitBeforeTax.text.replaceAll(",", ""),
            null, null, null, null, 1, 8, editNetSebelumPajakWiraswasta ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDTax != "" ? _customerIncomeIDTax : "NEW", "-", "009", "PAJAK", _controllerTax.text.replaceAll(",", ""),
            null, null, null, null, 1, 9, editPajakWiraswasta ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDNetProfitAfterTax != "" ? _customerIncomeIDNetProfitAfterTax : "NEW", "=", "010", "LABA BERSIH SETELAH PAJAK", _controllerNetProfitAfterTax.text.replaceAll(",", ""),
            null, null, null, null, 1, 10, editNetSetelahPajakWiraswasta ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDCostOfLiving != "" ? _customerIncomeIDCostOfLiving : "NEW", "-", "011", "BIAYA HIDUP", _controllerCostOfLiving.text.replaceAll(",", ""),
            null, null, null, null, 1, 11, editBiayaHidupWiraswasta ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDRestIncome != "" ? _customerIncomeIDRestIncome : "NEW", "=", "017", "SISA PENDAPATAN", _controllerRestIncome.text.replaceAll(",", ""),
            null, null, null, null, 1, 12, editSisaIncomeWiraswasta ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDOtherInstallments != "" ? _customerIncomeIDOtherInstallments : "NEW", "N", "013", "ANGSURAN LAINNYA", _controllerOtherInstallments.text.replaceAll(",", ""),
            null, null, null, null, 1, null, editAngsuranLainnyaWiraswasta ? "1" : "0"));
      }
    }
    else {
      //  pegawai
      if(isMarried){
        print("pegawai married");
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDMonthlyIncome != "" ? _customerIncomeIDMonthlyIncome : "NEW", "S", "016", "PENDAPATAN", _controllerMonthlyIncome.text.replaceAll(",", ""),
            null, null, null, null, 1, 1, editIncomeProfesional ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDSpouseIncome != "" ? _customerIncomeIDSpouseIncome : "NEW", "+", "012", "PENDAPATAN PASANGAN", _controllerSpouseIncome.text.replaceAll(",", ""),
            null, null, null, null, 1, null, editIncomePasanganProfesional ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDOtherIncome != "" ? _customerIncomeIDOtherIncome : "NEW", "+", "002", "PENDAPATAN LAINNYA", _controllerOtherIncome.text.replaceAll(",", ""),
            null, null, null, null, 1, 2, editIncomeLainnyaProfesional ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDTotalIncome != "" ? _customerIncomeIDTotalIncome : "NEW", "=", "003", "TOTAL PENDAPATAN", _controllerTotalIncome.text.replaceAll(",", ""),
            null, null, null, null, 1, 3, editTotalIncomeProfesional ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDCostOfLiving != "" ? _customerIncomeIDCostOfLiving : "NEW", "-", "011", "BIAYA HIDUP", _controllerCostOfLiving.text.replaceAll(",", ""),
            null, null, null, null, 1, 11, editBiayaHidupProfesional ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDRestIncome != "" ? _customerIncomeIDRestIncome : "NEW", "=", "017", "SISA PENDAPATAN", _controllerRestIncome.text.replaceAll(",", ""),
            null, null, null, null, 1, 12, editSisaIncomeProfesional ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDOtherInstallments != "" ? _customerIncomeIDOtherInstallments : "NEW", "N", "013", "ANGSURAN LAINNYA", _controllerOtherInstallments.text.replaceAll(",", ""),
            null, null, null, null, 1, null, editAngsuranLainnyaProfesional ? "1" : "0"));
      }
      else {
        print("pegawai tdk married");
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDMonthlyIncome != "" ? _customerIncomeIDMonthlyIncome : "NEW", "S", "016", "PENDAPATAN", _controllerMonthlyIncome.text.replaceAll(",", ""),
            null, null, null, null, 1, 1, editIncomeProfesional ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDOtherIncome != "" ? _customerIncomeIDOtherIncome : "NEW", "+", "002", "PENDAPATAN LAINNYA", _controllerOtherIncome.text.replaceAll(",", ""),
            null, null, null, null, 1, 2, editIncomeLainnyaProfesional ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDTotalIncome != "" ? _customerIncomeIDTotalIncome : "NEW", "=", "003", "TOTAL PENDAPATAN", _controllerTotalIncome.text.replaceAll(",", ""),
            null, null, null, null, 1, 3, editTotalIncomeProfesional ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDCostOfLiving != "" ? _customerIncomeIDCostOfLiving : "NEW", "-", "011", "BIAYA HIDUP", _controllerCostOfLiving.text.replaceAll(",", ""),
            null, null, null, null, 1, 11, editBiayaHidupProfesional ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDRestIncome != "" ? _customerIncomeIDRestIncome : "NEW", "=", "017", "SISA PENDAPATAN", _controllerRestIncome.text.replaceAll(",", ""),
            null, null, null, null, 1, 12, editSisaIncomeProfesional ? "1" : "0"));
        _listIncome.add(MS2CustIncomeModel("123", _customerIncomeIDOtherInstallments != "" ? _customerIncomeIDOtherInstallments : "NEW", "N", "013", "ANGSURAN LAINNYA", _controllerOtherInstallments.text.replaceAll(",", ""),
            null, null, null, null, 1, null, editAngsuranLainnyaProfesional ? "1" : "0"));
      }
    }
  }
}
