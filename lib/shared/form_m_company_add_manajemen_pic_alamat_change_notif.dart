import 'dart:collection';

import 'package:ad1ms2_dev/models/form_m_company_manajemen_pic_alamat_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/screens/form_m/search_kelurahan.dart';
import 'package:ad1ms2_dev/shared/form_m_company_manajemen_pic_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/search_kelurahan_change_notif.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


//ga kepake
class FormMCompanyAddManajemenPICAlamatChangeNotifier with ChangeNotifier {
  bool _autoValidate = false;
  TextEditingController _controllerAlamat = TextEditingController();
  TextEditingController _controllerRT = TextEditingController();
  TextEditingController _controllerRW = TextEditingController();
  TextEditingController _controllerKelurahan = TextEditingController();
  TextEditingController _controllerKecamatan = TextEditingController();
  TextEditingController _controllerKota = TextEditingController();
  TextEditingController _controllerProvinsi = TextEditingController();
  TextEditingController _controllerPostalCode = TextEditingController();
  TextEditingController _controllerTeleponArea = TextEditingController();
  TextEditingController _controllerTelepon = TextEditingController();
  bool _isSameWithIdentity = false;
  JenisAlamatModel _jenisAlamatSelected;
  JenisAlamatModel _jenisAlamatSelectedTemp;
  KelurahanModel _kelurahanSelected;
  KelurahanModel _kelurahanSelectedTemp;
  AddressModel _addressModelTemp;
  String _alamatTemp,
      _rtTemp,
      _rwTemp,
      _kelurahanTemp,
      _kecamatanTemp,
      _kotaTemp,
      _provinsiTemp,
      _postalCodeTemp,
      _teleponAreaTemp,
      _teleponTemp;
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  bool _enableTfAddress = true;
  bool _enableTfRT = true;
  bool _enableTfRW = true;
  bool _enableTfKelurahan = true;
  bool _enableTfKecamatan = true;
  bool _enableTfKota = true;
  bool _enableTfProv = true;
  bool _enableTfPostalCode = true;
  bool _enableTfAreaCode = true;
  bool _enableTfPhone = true;
  Map _addressFromMap;

  GlobalKey<FormState> get key => _key;

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  List<JenisAlamatModel> _listJenisAlamat = [];
  JenisAlamatModel get jenisAlamatSelectedTemp => _jenisAlamatSelectedTemp;
  KelurahanModel get kelurahanSelectedTemp => _kelurahanSelectedTemp;

  // Jenis Alamat
  UnmodifiableListView<JenisAlamatModel> get listJenisAlamat {
    return UnmodifiableListView(this._listJenisAlamat);
  }

  JenisAlamatModel get jenisAlamatSelected => _jenisAlamatSelected;

  set jenisAlamatSelected(JenisAlamatModel value) {
    this._jenisAlamatSelected = value;
    notifyListeners();
  }

  TextEditingController get controllerAlamat => _controllerAlamat;
  TextEditingController get controllerRT => _controllerRT;
  TextEditingController get controllerRW => _controllerRW;
  TextEditingController get controllerKelurahan => _controllerKelurahan;
  TextEditingController get controllerKecamatan => _controllerKecamatan;
  TextEditingController get controllerKota => _controllerKota;
  TextEditingController get controllerProvinsi => _controllerProvinsi;
  TextEditingController get controllerPostalCode => _controllerPostalCode;
  TextEditingController get controllerTeleponArea => _controllerTeleponArea;
  TextEditingController get controllerTelepon => _controllerTelepon;

  // Sama Dengan Identitas
  bool get isSameWithIdentity => _isSameWithIdentity;

  set isSameWithIdentity(bool value) {
    this._isSameWithIdentity = value;
    notifyListeners();
  }

  void addDataListJenisAlamat(BuildContext context, int index) {
    var _provider =
    Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: false);
    if (index == null) {
      // add
      if (_provider.listManajemenPICAddress.isEmpty) {
        _listJenisAlamat.add(JenisAlamatModel("01", "Identitas"));
      } else {
        _listJenisAlamat = [
          JenisAlamatModel("02", "Domisili 1"),
          JenisAlamatModel("03", "Domisili 2"),
          JenisAlamatModel("04", "Domisili 3"),
          JenisAlamatModel("05", "Kantor 1"),
          JenisAlamatModel("05", "Kantor 2"),
          JenisAlamatModel("07", "Kantor 3")
        ];
      }
    } else {
      // edit
      if (_provider.listManajemenPICAddress[index].jenisAlamatModel.KODE == '01') {
        _listJenisAlamat.add(JenisAlamatModel("01", "Identitas"));
      } else {
        _listJenisAlamat = [
          JenisAlamatModel("02", "Domisili 1"),
          JenisAlamatModel("03", "Domisili 2"),
          JenisAlamatModel("04", "Domisili 3"),
          JenisAlamatModel("05", "Kantor 1"),
          JenisAlamatModel("05", "Kantor 2"),
          JenisAlamatModel("07", "Kantor 3")
        ];
      }
    }
  }

  void searchKelurahan(BuildContext context) async {
    final data = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ChangeNotifierProvider(
          create: (context) => SearchKelurahanChangeNotif(),
          child: SearchKelurahan()
        )
      )
    );
    if (data != null) {
      kelurahanSelected = data;
      this._controllerKelurahan.text = data.KEL_NAME;
      this._controllerKecamatan.text = data.KEC_NAME;
      this._controllerKota.text = data.KABKOT_NAME;
      this._controllerProvinsi.text = data.PROV_NAME;
      this._controllerPostalCode.text = data.ZIPCODE;
      notifyListeners();
    } else {
      return;
    }
  }

  void check(BuildContext context, int flag, int index) {
    final _form = _key.currentState;
    if (flag == 0) {
      if (_form.validate()) {
        // Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: false)
        //   .addManajemenPICAddress(AddressModel(
        //     this._jenisAlamatSelected,
        //     this._kelurahanSelected,
        //     this._controllerAlamat.text,
        //     this._controllerRT.text,
        //     this._controllerRW.text,
        //     this._controllerTeleponArea.text,
        //     this._controllerTelepon.text,
        //     this._isSameWithIdentity,
        //     this._addressFromMap, false));
        Navigator.pop(context);
      } else {
        autoValidate = true;
      }
    } else {
      if (_form.validate()) {
        // Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: false)
        //   .updateManajemenPICAddress(AddressModel(
        //     this._jenisAlamatSelected,
        //     this._kelurahanSelected,
        //     this._controllerAlamat.text,
        //     this._controllerRT.text,
        //     this._controllerRW.text,
        //     this._controllerTeleponArea.text,
        //     this._controllerTelepon.text,
        //     this._isSameWithIdentity,
        //     this._addressFromMap, false),
        //   index);
        Navigator.pop(context);
      } else {
        autoValidate = true;
      }
    }
  }

  void checkValidCodeArea(String value) {
    if (value == "08") {
      Future.delayed(Duration(milliseconds: 100), () {
        this._controllerTeleponArea.clear();
      });
    } else {
      return;
    }
  }

  Future<void> setValueForEdit(CompanyManajemenPICAlamatModel data, BuildContext context, int index, bool isSameWithIdentity) async {
    addDataListJenisAlamat(context, index);
//    if (isSameWithIdentity) {
//      setValueIsSameWithIdentity(isSameWithIdentity, context, data);
//    } else {
      for (int i = 0; i < this._listJenisAlamat.length; i++) {
        if (data.jenisAlamatModel.KODE == this._listJenisAlamat[i].KODE) {
          this._jenisAlamatSelected = this._listJenisAlamat[i];
          this._jenisAlamatSelectedTemp = this._listJenisAlamat[i];
        }
      }
      this._kelurahanSelected = data.kelurahanModel;
      this._controllerAlamat.text = data.address;
      this._alamatTemp = this._controllerAlamat.text;
      this._controllerRT.text = data.rt;
      this._rtTemp = this._controllerRT.text;
      this._controllerRW.text = data.rw;
      this._rwTemp = this._controllerRW.text;
      this._controllerKelurahan.text = data.kelurahanModel.KEL_NAME;
      this._kelurahanTemp = this._controllerKelurahan.text;
      this._controllerKecamatan.text = data.kelurahanModel.KEC_NAME;
      this._kecamatanTemp = this._controllerKecamatan.text;
      this._controllerKota.text = data.kelurahanModel.KABKOT_NAME;
      this._kotaTemp = this._controllerKota.text;
      this._controllerProvinsi.text = data.kelurahanModel.PROV_NAME;
      this._provinsiTemp = this._controllerProvinsi.text;
      this._controllerPostalCode.text = data.kelurahanModel.ZIPCODE;
      this._postalCodeTemp = this._controllerPostalCode.text;

      this._controllerTeleponArea.text = data.phoneAreaCode;
      this._teleponAreaTemp = this._controllerTeleponArea.text;
      this._controllerTelepon.text = data.phone;
      this._teleponTemp = this._controllerTelepon.text;
//    }
  }

  String get alamatTemp => _alamatTemp;
  get rtTemp => _rtTemp;
  get rwTemp => _rwTemp;
  get kotaTemp => _kotaTemp;
  get kecamatanTemp => _kecamatanTemp;
  get kelurahanTemp => _kelurahanTemp;
  get provinsiTemp => _provinsiTemp;
  get postalCodeTemp => _postalCodeTemp;
  get teleponAreaTemp => _teleponAreaTemp;
  get teleponTemp => _teleponTemp;

  KelurahanModel get kelurahanSelected => _kelurahanSelected;

  set kelurahanSelected(KelurahanModel value) {
    this._kelurahanSelected = value;
    notifyListeners();
  }

//  void setValueIsSameWithIdentity(bool value, BuildContext context, CompanyManajemenPICAlamatModel model) {
//    var _provider = Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: false);
//    CompanyManajemenPICAlamatModel data;
//    if (_addressModelTemp == null) {
//      if (value) {
//        for (int i = 0; i < _provider.listManajemenPICAddress.length; i++) {
//          if (_provider.listManajemenPICAddress[i].jenisAlamatModel.KODE == "01") {
//            data = _provider.listManajemenPICAddress[i];
//          }
//        }
//        if (model != null) {
//          for (int i = 0; i < _listJenisAlamat.length; i++) {
//            if (model.jenisAlamatModel.KODE == _listJenisAlamat[i].KODE) {
//              this._jenisAlamatSelected = _listJenisAlamat[i];
//              this._jenisAlamatSelectedTemp = this._jenisAlamatSelected;
//              this._isSameWithIdentity = value;
//            }
//          }
//        }
//        this._controllerAlamat.text = data.address;
//        this._controllerRT.text = data.rt;
//        this._controllerRW.text = data.rw;
//        this._kelurahanSelected = data.kelurahanModel;
//        this._controllerAlamat.text = data.address;
//        this._alamatTemp = this._controllerAlamat.text;
//        this._controllerRT.text = data.rt;
//        this._rtTemp = this._controllerRT.text;
//        this._controllerRW.text = data.rw;
//        this._rwTemp = this._controllerRW.text;
//        this._controllerKelurahan.text = data.kelurahanModel.KEL_NAME;
//        this._kelurahanTemp = this._controllerKelurahan.text;
//        this._controllerKecamatan.text = data.kelurahanModel.KEC_NAME;
//        this._kecamatanTemp = this._controllerKecamatan.text;
//        this._controllerKota.text = data.kelurahanModel.KABKOT_NAME;
//        this._kotaTemp = this._controllerKota.text;
//        this._controllerProvinsi.text = data.kelurahanModel.PROV_NAME;
//        this._provinsiTemp = this._controllerProvinsi.text;
//        this._controllerPostalCode.text = data.kelurahanModel.ZIPCODE;
//        this._postalCodeTemp = this._controllerPostalCode.text;
//
//        this._controllerTeleponArea.text = data.phoneAreaCode;
//        this._teleponAreaTemp = this._controllerTeleponArea.text;
//        this._controllerTelepon.text = data.phone;
//        this._teleponTemp = this._controllerTelepon.text;
//
//        enableTfAddress = !value;
//        enableTfRT = !value;
//        enableTfRW = !value;
//        enableTfKelurahan = !value;
//        enableTfKecamatan = !value;
//        enableTfKota = !value;
//        enableTfProv = !value;
//        enableTfPostalCode = !value;
//        enableTfAreaCode = !value;
//        enableTfPhone = !value;
//      } else {
//        this._controllerAlamat.clear();
//        this._controllerRT.clear();
//        this._controllerRW.clear();
//        this._kelurahanSelected = null;
//        this._controllerAlamat.clear();
//        this._controllerRT.clear();
//        this._controllerRW.clear();
//        this._controllerKelurahan.clear();
//        this._controllerKecamatan.clear();
//        this._controllerKota.clear();
//        this._controllerProvinsi.clear();
//        this._controllerPostalCode.clear();
//        this._controllerTeleponArea.clear();
//        this._controllerTelepon.clear();
//        enableTfAddress = !value;
//        enableTfRT = !value;
//        enableTfRW = !value;
//        enableTfKelurahan = !value;
//        enableTfKecamatan = !value;
//        enableTfKota = !value;
//        enableTfProv = !value;
//        enableTfPostalCode = !value;
//        enableTfAreaCode = !value;
//        enableTfPhone = !value;
//      }
//    } else {
//      if (value) {
//        for (int i = 0; i < _provider.listManajemenPICAddress.length; i++) {
//          if (_provider.listManajemenPICAddress[i].jenisAlamatModel.KODE == "01") {
//            data = _provider.listManajemenPICAddress[i];
//          }
//        }
//        this._controllerAlamat.text = data.address;
//        this._controllerRT.text = data.rt;
//        this._controllerRW.text = data.rw;
//        this._kelurahanSelected = data.kelurahanModel;
//        this._controllerAlamat.text = data.address;
//        this._controllerRT.text = data.rt;
//        this._controllerRW.text = data.rw;
//        this._controllerKelurahan.text = data.kelurahanModel.KEL_NAME;
//        this._controllerKecamatan.text = data.kelurahanModel.KEC_NAME;
//        this._controllerKota.text = data.kelurahanModel.KABKOT_NAME;
//        this._controllerProvinsi.text = data.kelurahanModel.PROV_NAME;
//        this._controllerPostalCode.text = data.kelurahanModel.ZIPCODE;
//        this._controllerTeleponArea.text = data.phoneAreaCode;
//        this._controllerTelepon.text = data.phone;
//        enableTfAddress = !value;
//        enableTfRT = !value;
//        enableTfRW = !value;
//        enableTfKelurahan = !value;
//        enableTfKecamatan = !value;
//        enableTfKota = !value;
//        enableTfProv = !value;
//        enableTfPostalCode = !value;
//        enableTfAreaCode = !value;
//        enableTfPhone = !value;
//      } else {
//        this._controllerAlamat.clear();
//        this._controllerRT.clear();
//        this._controllerRW.clear();
//        this._kelurahanSelected = null;
//        this._controllerAlamat.clear();
//        this._controllerRT.clear();
//        this._controllerRW.clear();
//        this._controllerKelurahan.clear();
//        this._controllerKecamatan.clear();
//        this._controllerKota.clear();
//        this._controllerProvinsi.clear();
//        this._controllerPostalCode.clear();
//        this._controllerTeleponArea.clear();
//        this._controllerTelepon.clear();
//        enableTfAddress = !value;
//        enableTfRT = !value;
//        enableTfRW = !value;
//        enableTfKelurahan = !value;
//        enableTfKecamatan = !value;
//        enableTfKota = !value;
//        enableTfProv = !value;
//        enableTfPostalCode = !value;
//        enableTfAreaCode = !value;
//        enableTfPhone = !value;
//      }
//    }
//  }

  bool get enableTfAddress => _enableTfAddress;

  set enableTfAddress(bool value) {
    this._enableTfAddress = value;
  }

  bool get enableTfRT => _enableTfRT;

  set enableTfRT(bool value) {
    this._enableTfRT = value;
  }

  bool get enableTfRW => _enableTfRW;

  set enableTfRW(bool value) {
    this._enableTfRW = value;
  }

  bool get enableTfKelurahan => _enableTfKelurahan;

  set enableTfKelurahan(bool value) {
    this._enableTfKelurahan = value;
  }

  bool get enableTfKecamatan => _enableTfKecamatan;

  set enableTfKecamatan(bool value) {
    this._enableTfKecamatan = value;
  }

  bool get enableTfKota => _enableTfKota;

  set enableTfKota(bool value) {
    this._enableTfKota = value;
  }

  bool get enableTfProv => _enableTfProv;

  set enableTfProv(bool value) {
    this._enableTfProv = value;
  }

  bool get enableTfPostalCode => _enableTfPostalCode;

  set enableTfPostalCode(bool value) {
    this._enableTfPostalCode = value;
  }

  bool get enableTfAreaCode => _enableTfAreaCode;

  set enableTfAreaCode(bool value) {
    this._enableTfAreaCode = value;
  }

  bool get enableTfPhone => _enableTfPhone;

  set enableTfPhone(bool value) {
    this._enableTfPhone = value;
  }

  // get areaCodeTemp => _teleponAreaTemp;

  // set areaCodeTemp(value) {
  //   this._teleponAreaTemp = value;
  // }
}
