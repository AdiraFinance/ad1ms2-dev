import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/form_m_foto_model.dart';
import 'package:ad1ms2_dev/models/info_document_type_model.dart';
import 'package:ad1ms2_dev/models/ms2_appl_photo.dart';
import 'package:ad1ms2_dev/models/show_mandatory_m_foto_model.dart';
import 'package:ad1ms2_dev/models/type_of_financing_model.dart';
import 'package:ad1ms2_dev/screens/app/menu_customer_detail.dart';
import 'package:ad1ms2_dev/screens/detail_image.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/info_credit_structure_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/document_unit_model.dart';
import 'package:ad1ms2_dev/shared/form_m_individu_change_notifier/form_m_parent_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/group_unit_object_model.dart';
import 'package:ad1ms2_dev/shared/resource/submit_data_partial.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/io_client.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';

import '../main.dart';
import 'dashboard/dashboard_change_notif.dart';

class FormMFotoChangeNotifier with ChangeNotifier {
  List<ImageFileModel> _listFotoTempatTinggal = [];
  bool _autoValidate = false;
  bool _autoValidateFotoTempatTinggal = false;
  bool _autoValidateGroupObjectUnit = false;
  bool _autoValidateDocumentObjectUnit = false;
  bool _autoValidateTempatUsaha = false;
  final _imagePicker = ImagePicker();
  OccupationModel _occupationSelected;
  List<ImageFileModel> _listFotoTempatUsaha = [];
  KegiatanUsahaModel _kegiatanUsahaSelected;
  JenisKegiatanUsahaModel _jenisKegiatanUsahaSelected;
  JenisKonsepModel _jenisKonsepSelected;
  List<GroupUnitObjectModel> _listGroupUnitObject = [];
  List<DocumentUnitModel> _listDocument = [];
  var storage = FlutterSecureStorage();
  SubmitDataPartial _submitDataPartial = SubmitDataPartial();
  bool _isDisablePACIAAOSCONA = false;
  List<OccupationModel> _listOccupation = [];
  List<FinancingTypeModel> _listTypeOfFinancing = TypeOfFinancingList().financingTypeList;
  String _pathFile;
  DbHelper _dbHelper = DbHelper();
  String _custType;
  String _lastKnownState;
  bool _disableJenisPenawaran = false;

  bool get isDisablePACIAAOSCONA => _isDisablePACIAAOSCONA;

  set isDisablePACIAAOSCONA(bool value) {
    this._isDisablePACIAAOSCONA = value;
    notifyListeners();
  }

  String get custType => _custType;

  set custType(String value) {
    this._custType = value;
    notifyListeners();
  }

  String get lastKnownState => _lastKnownState;

  set lastKnownState(String value) {
    this._lastKnownState = value;
    notifyListeners();
  }

  // Future<void> getListOccupation(BuildContext context) async{
  //   this._typeOfFinancingModelSelected = this._listTypeOfFinancing[0];
  //   try{
  //     _listOccupation.clear();
  //     final ioc = new HttpClient();
  //     ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
  //     final _http = IOClient(ioc);
  //     final _response = await _http.get("https://103.110.89.34/public/ms2dev/api/occupation/get-jenis-pekerjaan");
  //     final _data = jsonDecode(_response.body);
  //     for(int i=0; i < _data.length;i++){
  //       _listOccupation.add(OccupationModel(_data[i]['KODE'], _data[i]['DESKRIPSI']));
  //     }
  //     setValueEdit(context);
  //   } catch (e) {
  //     print(e);
  //   }
  // }

  Future<void> getListOccupation(BuildContext context) async {
    setPreference();
    this._typeOfFinancingModelSelected = this._listTypeOfFinancing[0];
    _listOccupation.clear();
    this._occupationSelected = null;
    loadData = true;
    String _fieldPekerjaan = await storage.read(key: "FieldPekerjaan");
    try{
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);
      final _response = await _http.get(
          "${BaseUrl.urlGeneral}$_fieldPekerjaan",
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
        // "${urlPublic}api/occupation/get-jenis-pekerjaan"
        // "https://103.110.89.34/public/ms2dev/api/occupation/get-jenis-pekerjaan"
      );
      final _data = jsonDecode(_response.body);
      for(int i=0; i < _data.length;i++){
        _listOccupation.add(OccupationModel(_data[i]['KODE'], _data[i]['DESKRIPSI']));
      }
      loadData = false;
      await getBusinessActivities(context);
      // setValueEdit(context);
    } catch (e) {
      loadData = false;
    }
    // notifyListeners();
  }

  List<JenisKegiatanUsahaModel> _listJenisKegiatanUsaha = [];

  // Future<void> getListKegiatanUsaha() async{
  //   try{
  //       _listJenisKegiatanUsaha.clear();
  //       final ioc = new HttpClient();
  //       ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
  //       final _http = IOClient(ioc);
  //       var _body = jsonEncode({
  //           "P_KODE" : _kegiatanUsahaSelected.id
  //       });
  //       final _response = await _http.post(
  //         "https://103.110.89.34/public/ms2dev/api/parameter/get-jenis-kegiatan-usaha-name",
  //         body: _body,
  //         headers: {"Content-Type":"application/json"}
  //       );
  //
  //       final _result = jsonDecode(_response.body);
  //       final _data = _result['data'];
  //       if(_response.statusCode == 200){
  //         for(int i=0; i<_data.length; i++){
  //           _listJenisKegiatanUsaha.add(JenisKegiatanUsahaModel(_data[i]['KODE'], _data[i]['DESKRIPSI']));
  //         }
  //       }
  //       notifyListeners();
  //   } catch(e){
  //     print(e);
  //   }
  // }

  void setDefaultValue() {
    this._jenisKonsepSelected = this._listJenisKonsep[2];
  }

  setValueEdit(BuildContext context){
    var data = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
//    occupation
    if(data.occupationSelected != null){
      for (int i = 0; i < this._listOccupation.length; i++) {
        if (data.occupationSelected.KODE == this._listOccupation[i].KODE) {
          this._occupationSelected = this._listOccupation[i];
        }
      }
    }
//    jenis kegiatan usaha
    if(data.jenisKegiatanUsahaSelected != null){
      for (int i = 0; i < this._listJenisKegiatanUsaha.length; i++) {
        if (data.jenisKegiatanUsahaSelected.id == this._listJenisKegiatanUsaha[i].id) {
          this._jenisKegiatanUsahaSelected = this._listJenisKegiatanUsaha[i];
        }
      }
    }
  }

  List<KegiatanUsahaModel> _listKegiatanUsaha = [
    // KegiatanUsahaModel(1, "Investasi"),
    // KegiatanUsahaModel(2, "Multiguna"),
    // KegiatanUsahaModel(3, "Modal Kerja"),
  ];

  List<JenisKonsepModel> _listJenisKonsep = [
    JenisKonsepModel("01", "SATU UNIT MULTI JAMINAN"),
    JenisKonsepModel("02", "MULTI UNIT SATU  JAMINAN"),
    JenisKonsepModel("03", "MULTI UNIT DENGAN JAMINAN DAN TANPA JAMINAN")
  ];

  List<ImageFileModel> get listFotoTempatTinggal => _listFotoTempatTinggal;

  // void addFotoTempatTinggal(ImageFileModel data) {
  //   this._listFotoTempatTinggal.add(data);
  //   checkDataChanges();
  //   notifyListeners();
  // }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  bool get autoValidateFotoTempatTinggal => _autoValidateFotoTempatTinggal;

  set autoValidateFotoTempatTinggal(bool value) {
    this._autoValidateFotoTempatTinggal = value;
    notifyListeners();
  }

  bool get autoValidateGroupObjectUnit => _autoValidateGroupObjectUnit;

  set autoValidateGroupObjectUnit(bool value) {
    this._autoValidateGroupObjectUnit = value;
    notifyListeners();
  }

  bool get autoValidateDocumentObjectUnit => _autoValidateDocumentObjectUnit;

  set autoValidateDocumentObjectUnit(bool value) {
    this._autoValidateDocumentObjectUnit = value;
    notifyListeners();
  }

  bool get autoValidateTempatUsaha => _autoValidateTempatUsaha;

  set autoValidateTempatUsaha(bool value) {
    this._autoValidateTempatUsaha = value;
    notifyListeners();
  }

  // void addFile(int idImageArray) async { //old
  //   var _image = await _imagePicker.getImage(
  //       source: ImageSource.camera,
  //       maxHeight: 1920.0,
  //       maxWidth: 1080.0,
  //       imageQuality: 10);
  //   savePhoto(_image, idImageArray);
  // }

  bool _loadImageTmptTinggal = false;
  bool _loadImageUsaha = false;

  bool get loadImageTmptTinggal => _loadImageTmptTinggal;

  set loadImageTmptTinggal(bool value) {
    this._loadImageTmptTinggal = value;
    notifyListeners();
  }

  bool get loadImageUsaha => _loadImageUsaha;

  set loadImageUsaha(bool value) {
    this._loadImageUsaha = value;
    notifyListeners();
  }

  void addFileCamera(int idImageArray) async {
    if(idImageArray == 0){
      loadImageTmptTinggal = true;
    }
    else{
      loadImageUsaha = true;
    }
    var _image = await _imagePicker.getImage(
        source: ImageSource.camera,
        maxHeight: 480.0,
        maxWidth: 640.0,
        imageQuality: 100);
    savePhoto(_image, idImageArray);
  }

  void addFileGallery(int idImageArray) async {
    if(idImageArray == 0){
      loadImageTmptTinggal = true;
    }
    else{
      loadImageUsaha = true;
    }
    var _image = await _imagePicker.getImage(
        source: ImageSource.gallery,
        maxHeight: 480.0,
        maxWidth: 640.0,
        imageQuality: 100);
    savePhoto(_image, idImageArray);
  }

  void showBottomSheetChooseFile(BuildContext context,int idImageArray) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Theme(
            data: ThemeData(fontFamily: "NunitoSans"),
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  FlatButton(
                      onPressed: () {
                        addFileCamera(idImageArray);
                        Navigator.pop(context);
                      },
                      child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              Icons.photo_camera,
                              color: myPrimaryColor,
                              size: 22.0,
                            ),
                            SizedBox(
                              width: 12.0,
                            ),
                            Text(
                              "Camera",
                              style: TextStyle(fontSize: 18.0),
                            )
                          ])),
                  FlatButton(
                      onPressed: () {
                        addFileGallery(idImageArray);
                        Navigator.pop(context);
                      },
                      child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              Icons.photo,
                              color: myPrimaryColor,
                              size: 22.0,
                            ),
                            SizedBox(
                              width: 12.0,
                            ),
                            Text(
                              "Gallery",
                              style: TextStyle(fontSize: 18.0),
                            )
                          ])),
                ],
              ),
            ),
          );
        });
  }

  UnmodifiableListView<OccupationModel> get listOccupation {
    return UnmodifiableListView(this._listOccupation);
  }

  OccupationModel get occupationSelected => _occupationSelected;

  String get pathFile => _pathFile;

  set occupationSelected(OccupationModel value) {
    this._occupationSelected = value;
    this._autoValidateTempatUsaha = false;
    notifyListeners();
  }

  List<ImageFileModel> get listFotoTempatUsaha => _listFotoTempatUsaha;

  UnmodifiableListView<KegiatanUsahaModel> get listKegiatanUsaha {
    return UnmodifiableListView(this._listKegiatanUsaha);
  }

  KegiatanUsahaModel get kegiatanUsahaSelected => _kegiatanUsahaSelected;

  set kegiatanUsahaSelected(KegiatanUsahaModel value) {
    this._kegiatanUsahaSelected = value;
    this._jenisKegiatanUsahaSelected = null;
    // getListKegiatanUsaha();
    notifyListeners();
  }

  UnmodifiableListView<JenisKegiatanUsahaModel> get listJenisKegiatanUsaha {
      return UnmodifiableListView(this._listJenisKegiatanUsaha);
  }

  JenisKegiatanUsahaModel get jenisKegiatanUsahaSelected =>
      _jenisKegiatanUsahaSelected;

  set jenisKegiatanUsahaSelected(JenisKegiatanUsahaModel value) {
    this._jenisKegiatanUsahaSelected = value;
    notifyListeners();
  }

  UnmodifiableListView<JenisKonsepModel> get listJenisKonsep {
    return UnmodifiableListView(this._listJenisKonsep);
  }

  JenisKonsepModel get jenisKonsepSelected => _jenisKonsepSelected;

  set jenisKonsepSelected(JenisKonsepModel value) {
    this._jenisKonsepSelected = value;
    notifyListeners();
  }

  List<GroupUnitObjectModel> get listGroupUnitObject => _listGroupUnitObject;

  void removeListGroupUnitObject(BuildContext context, int index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                primaryColor: Colors.black,
                primarySwatch: primaryOrange,
                accentColor: myPrimaryColor
            ),
            child: AlertDialog(
              title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Apakah kamu yakin menghapus group unit object ini?",),
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    this._listGroupUnitObject.removeAt(index);
                    notifyListeners();
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                ),
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Tidak'),
                ),
              ],
            ),
          );
        }
    );
  }

  void addToListGroupUnitObject(GroupObjectUnit groupObjectUnit, ObjectUnit objectUnit, List<ImageFileModel> imageFiles, bool isDeleted, BuildContext context) {
    this._listGroupUnitObject.add(GroupUnitObjectModel(groupObjectUnit, objectUnit, imageFiles, isDeleted, false, false, false, false));
    if (this._autoValidateGroupObjectUnit) {
      autoValidateGroupObjectUnit = false;
    }
    notifyListeners();
    Navigator.pop(context, true);
  }

  void updateToListGroupUnitObject(GroupObjectUnit groupObjectUnit, ObjectUnit objectUnit, List<ImageFileModel> imageFiles, bool isDeleted, BuildContext context, int index) {
    this._listGroupUnitObject[index] = GroupUnitObjectModel(groupObjectUnit, objectUnit, imageFiles, isDeleted, false, false, false, false);
    notifyListeners();
    Navigator.pop(context, true);
  }

  List<DocumentUnitModel> get listDocument => _listDocument;

  void addListDocument(DocumentUnitModel documentUnitModel, BuildContext context) {
    this._listDocument.add(documentUnitModel);
    if (this._autoValidateDocumentObjectUnit) {
      autoValidateDocumentObjectUnit = false;
    }
    notifyListeners();
    Navigator.pop(context);
  }

  void updateListDocumentObjectUnit(DocumentUnitModel documentUnitModel, BuildContext context, int index) {
    this._listDocument[index] = documentUnitModel;
    notifyListeners();
    Navigator.pop(context);
  }

  void deleteListDocumentObjectUnit(BuildContext context, int index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                primaryColor: Colors.black,
                primarySwatch: primaryOrange,
                accentColor: myPrimaryColor
            ),
            child: AlertDialog(
              title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Apakah kamu yakin menghapus dokumen unit object ini?",),
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    deleteFileDocument(index);
                    this._listDocument.removeAt(index);
                    notifyListeners();
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                ),
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Tidak'),
                ),
              ],
            ),
          );
        }
    );
  }

  void deleteFileDocument(int index) async{
    final file =  File("${this._listDocument[index].path}");
    await file.delete();
  }

  void actionDeleteDetailImageResidence(String action,int index,BuildContext context){
    if(TitlePopUpMenuButton.Delete == action){
      deleteFile(index, "tinggal");
      this._listFotoTempatTinggal.removeAt(index);
      notifyListeners();
    }
    else{
      Navigator.push(context, MaterialPageRoute(builder: (context) => DetailImage(imageFile: this._listFotoTempatTinggal[index].imageFile,),));
    }
  }

  void actionDeleteDetailImageBusinessPlace(String action,int index,BuildContext context){
    if(TitlePopUpMenuButton.Delete == action){
      deleteFile(index, "usaha");
      this._listFotoTempatUsaha.removeAt(index);
      notifyListeners();
    }
    else{
      Navigator.push(context, MaterialPageRoute(builder: (context) => DetailImage(imageFile: this._listFotoTempatUsaha[index].imageFile,),));
    }
  }

  UnmodifiableListView<FinancingTypeModel> get listTypeOfFinancing {
    return UnmodifiableListView(this._listTypeOfFinancing);
  }

  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  GlobalKey<ScaffoldState> _scaffoldKeyListGroupObjecUnit = new GlobalKey<ScaffoldState>();

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  Future<void> getListBusinessActivitiesType(BuildContext context) async{
    // Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).periodOfTimeSelected = null;
    // Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).installmentTypeSelected = null;
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    try{
      this._listJenisKegiatanUsaha.clear();
      this._jenisKegiatanUsahaSelected = null;
      loadData = true;
      final ioc = new HttpClient();
      ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);
      var _body = jsonEncode({
        "P_OJK_BUSS_ID" : this._kegiatanUsahaSelected.id
      });

      String _fieldJenisKegiatanUsaha = await storage.read(key: "FieldJenisKegiatanUsaha");
      final _response = await _http.post(
        "${BaseUrl.urlGeneral}$_fieldJenisKegiatanUsaha",
        // "${urlPublic}api/parameter/get-jenis-kegiatan-usaha",
          // "https://103.110.89.34/public/ms2dev/api/parameter/get-jenis-kegiatan-usaha",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));
      final _result = jsonDecode(_response.body);
      final _data = _result['data'];

      if(_data.length > 0){
        for(int i=0; i<_data.length; i++){
          if(_preferences.getString("jenis_penawaran") == "002") {
            if(this._kegiatanUsahaSelected.id == 1 || this._kegiatanUsahaSelected.id == 2) {
              if(_data[i]['KODE'] != 18 && _data[i]['KODE'] != 20 && _data[i]['KODE'] != 33 && _data[i]['KODE'] != 34) {
                _listJenisKegiatanUsaha.add(JenisKegiatanUsahaModel(_data[i]['KODE'], _data[i]['DESKRIPSI']));
              }
            } else {
              _listJenisKegiatanUsaha.add(JenisKegiatanUsahaModel(_data[i]['KODE'], _data[i]['DESKRIPSI']));
            }
          } else {
            _listJenisKegiatanUsaha.add(JenisKegiatanUsahaModel(_data[i]['KODE'], _data[i]['DESKRIPSI']));
          }
        }
        loadData = false;
      }
      else{
        loadData = false;
        showSnackBar("Jenis kegiatan usaha tidak ditemukan");
      }
      // setDataFromSQLite(context,index);
    }
    on TimeoutException catch(_){
      loadData = false;
      showSnackBar("Timeout Connection");
    }
    on SocketException catch(_){
      loadData = false;
      showSnackBar("Please check connection or contact server");
    }
    catch(e){
      loadData = false;
      showSnackBar("Error ${e.toString()}");
    }
    notifyListeners();
  }

  FinancingTypeModel _typeOfFinancingModelSelected;

  FinancingTypeModel get typeOfFinancingModelSelected =>
      _typeOfFinancingModelSelected;

  set typeOfFinancingModelSelected(FinancingTypeModel value) {
    this._typeOfFinancingModelSelected = value;
    notifyListeners();
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  GlobalKey<ScaffoldState> get scaffoldKeyListGroupObjecUnit =>
      _scaffoldKeyListGroupObjecUnit;

  Future<void> getBusinessActivities(BuildContext context) async {
    loadData = true;
    // Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).periodOfTimeSelected = null;
    // Provider.of<InfoCreditStructureChangeNotifier>(context,listen: false).installmentTypeSelected = null;
    this._listKegiatanUsaha.clear();
    this._kegiatanUsahaSelected = null;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
    var _body = jsonEncode({
      "P_FIN_TYPE" : this._typeOfFinancingModelSelected.financingTypeId
    });
    String _fieldKegiatanUsaha = await storage.read(key: "FieldKegiatanUsaha");
    try{
      final _response = await _http.post(
        "${BaseUrl.urlGeneral}$_fieldKegiatanUsaha",
          // "${urlPublic}api/parameter/get-kegiatan-usaha",
          // "https://103.110.89.34/public/ms2dev/api/parameter/get-kegiatan-usaha",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      ).timeout(Duration(seconds: 30));
      final _result = jsonDecode(_response.body);
      final _data = _result['data'];
      if(_data.length > 0){
        for(int i=0; i < _data.length; i++){
          this._listKegiatanUsaha.add(KegiatanUsahaModel(_data[i]['KODE'], _data[i]['DESKRIPSI']));
        }
        loadData = false;
      }
      else{
        loadData = false;
        showSnackBar("Kegiatan usaha data tidak ditemukan");
      }
      // getListBusinessActivitiesType(context,index);
      // if(index != null || creditLimit){
      //   await setDataFromSQLite(context,index);
      // }
    }
    on TimeoutException catch(_){
      loadData = false;
      showSnackBar("Timeout Connection");
    }
    on SocketException catch(_){
      loadData = false;
      showSnackBar("Please check connection or contact server");
    }
    catch(e){
      loadData = false;
      showSnackBar("Error ${e.toString()}");
    }
    // if(index == null){
      notifyListeners();
    // }
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  void showSnackBarListGroupUnitObject(String text){
    this._scaffoldKeyListGroupObjecUnit.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  void clearFormMFoto(){
    this._isDisablePACIAAOSCONA = false;
    this._disableJenisPenawaran = false;
    this._autoValidate = false;
    this._listFotoTempatTinggal.clear();
    this._occupationSelected = null;
    this._listOccupation.clear();
    this._listFotoTempatUsaha.clear();
    this._typeOfFinancingModelSelected = null;
    this._listKegiatanUsaha.clear();
    this._kegiatanUsahaSelected = null;
    this._jenisKegiatanUsahaSelected = null;
    this._listJenisKegiatanUsaha.clear();
    this._jenisKonsepSelected = null;
    this._listGroupUnitObject.clear();
    this._listDocument.clear();
  }

  void savePhoto(PickedFile data, int idImageArray) async{
    try{
      if (data != null) {
        String _date = formatDateListOID(DateTime.now()).replaceAll("-", "");
        String _time = formatTime(DateTime.now()).replaceAll(":", "");
        File _imageFile = File(data.path);
        //resize img
        final filePath = _imageFile.absolute.path;
        final lastIndex = filePath.lastIndexOf(new RegExp(r'.jp'));
        final splitted = filePath.substring(0, (lastIndex));
        final outPath = "${splitted}_out${filePath.substring(lastIndex)}";
        File _result = await FlutterImageCompress.compressAndGetFile(
          _imageFile.absolute.path, outPath,
          quality: 50,
        );
        // List<String> split = data.path.split("/");
        // String fileName = split[split.length-1];
        // File _path = await _imageFile.copy("$globalPath/$fileName");
        // _pathFile = _path.path;
        Position _position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.bestForNavigation);
        switch (idImageArray) {
          case 0:
            // File _path = await _imageFile.copy("$globalPath/FOTO_TEMPAT_TINGGAL_${this._listFotoTempatTinggal.length+1}.JPG");
            File _path = await _result.copy("$globalPath/FOTO_TEMPAT_TINGGAL_${this._listFotoTempatTinggal.length+1}_${_date}_$_time.JPG");
            _pathFile = _path.path;
            this._listFotoTempatTinggal.add(ImageFileModel(_path, _position.latitude, _position.longitude, this._pathFile, ""));
            if (this._autoValidateFotoTempatTinggal) {
              autoValidateFotoTempatTinggal = false;
            }
            notifyListeners();
            break;
          case 1:
            // File _path = await _imageFile.copy("$globalPath/FOTO_USAHA_${this._listFotoTempatUsaha.length+1}.JPG");
            File _path = await _result.copy("$globalPath/FOTO_USAHA_${this._listFotoTempatUsaha.length+1}_${_date}_$_time.JPG");
            _pathFile = _path.path;
            this._listFotoTempatUsaha.add(ImageFileModel(_path, _position.latitude, _position.longitude, this._pathFile, ""));
            if (this._autoValidateTempatUsaha) {
              autoValidateTempatUsaha = false;
            }
            notifyListeners();
            break;
        }
        loadImageTmptTinggal = false;
        loadImageUsaha = false;
      } else {
        loadImageTmptTinggal = false;
        loadImageUsaha = false;
        return;
      }
//      Uint8List bytes = this._fileDocumentUnitObject['file'].readAsBytesSync();
//      await file.writeAsBytes(bytes);
    }
    catch(e){
      loadImageTmptTinggal = false;
      loadImageUsaha = false;
      print(e);
    }
  }

  void deleteFile(int index, String type) async{
    var file;
    if(type == "usaha"){
      file =  File("${this._listFotoTempatUsaha[index].path}");
    }
    else {
      file =  File("${this._listFotoTempatTinggal[index].path}");
    }
    await file.delete();
  }

  Future<void> saveToSQLite(BuildContext context) async{
    _dbHelper.insertMS2PhotoHeader(MS2ApplPhotoModel(this._listFotoTempatTinggal, this._occupationSelected, this._listFotoTempatUsaha,
        this._typeOfFinancingModelSelected, this._kegiatanUsahaSelected, this._jenisKegiatanUsahaSelected,
        this._jenisKonsepSelected, this._listGroupUnitObject, this._listDocument,
        _islistFotoTempatTinggalChanges ? "1" : "0",
        _isoccupationSelectedChanges ? "1" : "0",
        _islistFotoTempatUsahaChanges ? "1" : "0",
        _istypeOfFinancingModelSelectedChanges ? "1" : "0",
        _iskegiatanUsahaSelectedChanges ? "1" : "0",
        _isjenisKegiatanUsahaSelectedChanges ? "1" : "0",
        _isjenisKonsepSelectedChanges ? "1" : "0",
        _islistDocumentChanges ? "1" : "0",
        _islistGroupUnitObjectChanges ? "1" : "0")
    );
    await _submitDataPartial.submitPhoto(context, "1");
    if(this._listDocument.isNotEmpty) await _submitDataPartial.submitPhoto(context, "3");
  }

  Future<void> setDataFromSQLite(BuildContext context,int index) async{
    var _check = await _dbHelper.selectMS2PhotoHeader(); // form awal
    var _check1 = await _dbHelper.selectMS2PhotoDetail();// detail pada grup dan dokumen unit
    var _check2 = await _dbHelper.selectMS2PhotoDetail2(); // foto
    debugPrint("CHECK_DETAIL_1 $_check1");
    debugPrint("CHECK_DETAIL_2 $_check2");
    await getListOccupation(context);
    //insert header
    if(_check.isNotEmpty){
      for(int i=0; i<_listOccupation.length;i++){
        if(_listOccupation[i].KODE == _check[0]['work']){
          _occupationSelected = _listOccupation[i];
        }
      }
      for(int i=0; i<_listTypeOfFinancing.length; i++){
        if(_listTypeOfFinancing[i].financingTypeId == _check[0]['financing_type']){
          _typeOfFinancingModelSelected = _listTypeOfFinancing[i];
        }
      }
      await getBusinessActivities(context);
      for(int i=0; i<_listKegiatanUsaha.length; i++){
        if(_listKegiatanUsaha[i].id.toString() == _check[0]['business_activities']){
          _kegiatanUsahaSelected = _listKegiatanUsaha[i];
        }
      }
      if(_kegiatanUsahaSelected != null){
        await getListBusinessActivitiesType(context);
        for(int i=0; i<_listJenisKegiatanUsaha.length; i++){
          if(_listJenisKegiatanUsaha[i].id.toString() == _check[0]['business_activities_type']){
            _jenisKegiatanUsahaSelected = _listJenisKegiatanUsaha[i];
          }
        }
      }
      print("cek jenis konsep ${_check[0]['concept_type']}");
      for(int i=0; i<_listJenisKonsep.length; i++){
        if(_listJenisKonsep[i].id.toString() == _check[0]['concept_type']){
          _jenisKonsepSelected = _listJenisKonsep[i];
        }
      }
      // this._jenisKonsepSelected = JenisKonsepModel(_check[0]['concept_type'], _check[0]['concept_type_desc']);
    }
    //insert foto tempat tinggal dan usaha
    if(_check2.isNotEmpty){
      debugPrint("CHECK_DETAIL_2 ${_check2.length}");
      for(int i=0; i<_check2.length; i++){
        if(_check2[i]['flag'] == "1"){ //tempat tinggal
          File _imageFile = File(_check2[i]['path_photo']);
          _listFotoTempatTinggal.add(ImageFileModel(_imageFile, _check2[i]['latitude'] != "null" ? double.parse(_check2[i]['latitude']) : null, _check2[i]['latitude'] != "null" ? double.parse(_check2[i]['longitude']) : null, _check2[i]['path_photo'], _check2[i]['file_header_id']));
        }
        if(_check2[i]['flag'] == "2"){ //usaha
          File _imageFile = File(_check2[i]['path_photo']);
          _listFotoTempatUsaha.add(ImageFileModel(_imageFile, _check2[i]['latitude'] != "null" ? double.parse(_check2[i]['latitude']) : null, _check2[i]['latitude'] != "null" ? double.parse(_check2[i]['longitude']) : null, _check2[i]['path_photo'], _check2[i]['file_header_id']));
        }
      }
    }
    //detail list
    if(_check1.isNotEmpty){
      for(int i=0; i<_check1.length; i++){
        //list group unit object
        if(_check1[i]['flag'] == "1"){
          var groupObjectSelected = GroupObjectUnit(_check1[i]['group_object'], _check1[i]['group_object_desc']);
          var unitObject = ObjectUnit(_check1[i]['unit_object'], _check1[i]['unit_object_desc']);
          List<ImageFileModel> listImageGroupObjectUnit = [];
          for(int j=0; j<_check2.length; j++){
            if(_check1[i]['detail_id'] == _check2[j]['detail_id']){ //filter by detail_id  yang dimiliki masing2 list
              File _imageFile = File(_check2[j]['path_photo']);
              listImageGroupObjectUnit.add(ImageFileModel(
                  _imageFile,
                  _check2[j]['latitude'] != "null" && _check2[j]['latitude'] != "" ? double.parse(_check2[j]['latitude']) : null,
                  _check2[j]['longitude'] != "null" && _check2[j]['longitude'] != "" ? double.parse(_check2[j]['longitude']) : null,
                  _check2[j]['path_photo'],
                  _check2[j]['file_header_id']
              ));
            }
          }
          _listGroupUnitObject.add(GroupUnitObjectModel(
            groupObjectSelected, unitObject, listImageGroupObjectUnit, _check2.isEmpty ? false : _check2[0]['file_header_id'] != "",
            _check1[i]['edit_group_object'] == "1" || _check1[i]['edit_unit_object'] == "1",
            _check1[i]['edit_group_object'] == "1",
            _check1[i]['edit_unit_object'] == "1",
            false
          ));
        }
        else if(_check1[i]['flag'] == "2"){
          for(int j=0; j<_check2.length; j++){
            if(_check1[i]['detail_id'] == _check2[j]['detail_id']){ //filter by detail_id  yang dimiliki masing2 list
              if(_check2[j]['flag'] == "4"){
                File _imageFile = File(_check2[j]['path_photo']);
                List _splitFileName = _check2[j]['path_photo'].split("/");
                int _idFileName = _splitFileName.length - 1;
                var _fileDocument = {
                  "file":_imageFile,
                  "file_name": _splitFileName[_idFileName],
                };
                var _docType = InfoDocumentTypeModel(_check1[i]['document_type'], _check1[i]['document_type_desc'], int.parse(_check1[i]['is_mandatory']), int.parse(_check1[i]['is_display']), int.parse(_check1[i]['is_unit']));
                var _dateReceive = DateTime.parse(_check1[i]['date_receive']);
                _listDocument.add(DocumentUnitModel(
                    _docType,
                    _check2[j]['orderSupportingDocumentID'],
                    _dateReceive,
                    _fileDocument,
                    _check2[j]['path_photo'],
                    _check2[j]['latitude'] != "null" && _check2[j]['latitude'] != "" ? double.parse(_check2[j]['latitude']) : null,
                    _check2[j]['longitude'] != "null" && _check2[j]['longitude'] != "" ? double.parse(_check2[j]['longitude']) : null,
                    null,
                    _check2[j]['file_header_id'],
                    false,
                    false,
                    false,
                    false
                ));
              }
            }
          }
        }
      }

    }

    if(_lastKnownState == "IDE" && index != null){
      MenuCustomerDetailState _menuCustomerDetail = MenuCustomerDetailState();
      if(index != 0){
        print("cek index foto $index");
        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).isPhotoDone = true;
        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).selectedIndex += 1;
        await _menuCustomerDetail.setNextState(context, index);
      }
      else{
        Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).selectedIndex = 0;
      }
    }
    await checkDataChanges();
    notifyListeners();
  }

  Future<bool> deleteSQLite(){
    debugPrint("delete foto masbro");
    return _dbHelper.deleteMS2PhotoHeader(false);
  }

  ShowMandatoryFotoModel _showMandatoryFotoModel;

  void setShowMandatoryFotoModel(BuildContext context){
    setDefaultValue();
    _showMandatoryFotoModel = Provider.of<DashboardChangeNotif>(context,listen: false).showMandatoryFotoModel;
  }

  bool isFotoTempatTinggalVisible() => _showMandatoryFotoModel.isFotoTempatTinggalVisible;
  bool isOccupationVisible() => _showMandatoryFotoModel.isOccupationVisible;
  bool isFotoTempatUsahaVisible() => _showMandatoryFotoModel.isFotoTempatUsahaVisible;
  bool istypeOfFinancingModelVisible() => _showMandatoryFotoModel.istypeOfFinancingModelVisible;
  bool iskegiatanUsahaVisible() => _showMandatoryFotoModel.iskegiatanUsahaVisible;
  bool isJenisKegiatanUsahaVisible() => _showMandatoryFotoModel.isJenisKegiatanUsahaVisible;
  bool isJenisKonsepVisible() => _showMandatoryFotoModel.isJenisKonsepVisible;
  bool isListGroupUnitObjectVisible() => _showMandatoryFotoModel.isListGroupUnitObjectVisible;
  bool isListDocumentVisible() => _showMandatoryFotoModel.isListDocumentVisible;
  bool isFotoTempatTinggalMandatory() => _showMandatoryFotoModel.isFotoTempatTinggalMandatory;
  bool isOccupationMandatory() => _showMandatoryFotoModel.isOccupationMandatory;
  bool isFotoTempatUsahaMandatory() => _showMandatoryFotoModel.isFotoTempatUsahaMandatory;
  bool istypeOfFinancingModelMandatory() => _showMandatoryFotoModel.istypeOfFinancingModelMandatory;
  bool iskegiatanUsahaMandatory() => _showMandatoryFotoModel.iskegiatanUsahaMandatory;
  bool isJenisKegiatanUsahaMandatory() => _showMandatoryFotoModel.isJenisKegiatanUsahaMandatory;
  bool isJenisKonsepMandatory() => _showMandatoryFotoModel.isJenisKonsepMandatory;
  bool isListGroupUnitObjectMandatory() => _showMandatoryFotoModel.isListGroupUnitObjectMandatory;
  bool isListDocumentMandatory() => _showMandatoryFotoModel.isListDocumentMandatory;

  bool isLimitAddGroupUnitObject() => _listGroupUnitObject.length < 2;

  bool _islistFotoTempatTinggalChanges = false;
  bool _isoccupationSelectedChanges = false;
  bool _islistFotoTempatUsahaChanges = false;
  bool _istypeOfFinancingModelSelectedChanges = false;
  bool _iskegiatanUsahaSelectedChanges = false;
  bool _isjenisKegiatanUsahaSelectedChanges = false;
  bool _isjenisKonsepSelectedChanges = false;
  bool _islistDocumentChanges = false;
  bool _islistGroupUnitObjectChanges = false;

  bool get islistFotoTempatTinggalChanges => _islistFotoTempatTinggalChanges;
  bool get isoccupationSelectedChanges => _isoccupationSelectedChanges;
  bool get islistFotoTempatUsahaChanges => _islistFotoTempatUsahaChanges;
  bool get istypeOfFinancingModelSelectedChanges => _istypeOfFinancingModelSelectedChanges;
  bool get iskegiatanUsahaSelectedChanges => _iskegiatanUsahaSelectedChanges;
  bool get isjenisKegiatanUsahaSelectedChanges => _isjenisKegiatanUsahaSelectedChanges;
  bool get isjenisKonsepSelectedChanges => _isjenisKonsepSelectedChanges;
  bool get islistDocumentChanges => _islistDocumentChanges;
  bool get islistGroupUnitObjectChanges => _islistGroupUnitObjectChanges;

  set islistFotoTempatTinggalChanges(bool value) {
    this._islistFotoTempatTinggalChanges = value;
    notifyListeners();
  }
  set isoccupationSelectedChanges(bool value) {
    this._isoccupationSelectedChanges = value;
    notifyListeners();
  }
  set islistFotoTempatUsahaChanges(bool value) {
    this._islistFotoTempatUsahaChanges = value;
    notifyListeners();
  }
  set istypeOfFinancingModelSelectedChanges(bool value) {
    this._istypeOfFinancingModelSelectedChanges = value;
    notifyListeners();
  }
  set iskegiatanUsahaSelectedChanges(bool value) {
    this._iskegiatanUsahaSelectedChanges = value;
    notifyListeners();
  }
  set isjenisKegiatanUsahaSelectedChanges(bool value) {
    this._isjenisKegiatanUsahaSelectedChanges = value;
    notifyListeners();
  }
  set isjenisKonsepSelectedChanges(bool value) {
    this._isjenisKonsepSelectedChanges = value;
    notifyListeners();
  }
  set islistDocumentChanges(bool value) {
    this._islistDocumentChanges = value;
    notifyListeners();
  }
  set islistGroupUnitObjectChanges(bool value) {
    this._islistGroupUnitObjectChanges = value;
    notifyListeners();
  }

  bool get disableJenisPenawaran => _disableJenisPenawaran;

  set disableJenisPenawaran(bool value) {
    this._disableJenisPenawaran = value;
  }

  void setPreference() async {
    SharedPreferences _preference = await SharedPreferences.getInstance();
    this._custType = _preference.getString("cust_type");
    this._lastKnownState = _preference.getString("last_known_state");
    if(_preference.getString("jenis_penawaran") == "002"){
      this._disableJenisPenawaran = true;
    }
    if(this._lastKnownState == "AOS" || this._lastKnownState == "CONA" || this._lastKnownState == "IA" || this._lastKnownState == "PAC") {
      this._isDisablePACIAAOSCONA = true;
    }
  }

  Future<void> checkDataChanges() async{
    if(_lastKnownState == "DKR"){
      var _data = await _dbHelper.selectMS2PhotoHeader();
      var _listFoto1 = await _dbHelper.selectMS2PhotoDetail();
      var _listFoto2 = await _dbHelper.selectMS2PhotoDetail2();
      var _listFoto1Flag1 = _listFoto1.where((i) => i['flag']=="1").toList();
      var _listFoto2Flag1 = _listFoto2.where((i) => i['flag']=="1").toList();
      var _listFoto2Flag2 = _listFoto2.where((i) => i['flag']=="2").toList();
      var _listFoto2Flag4 = _listFoto2.where((i) => i['flag']=="4").toList();

      if(_listFoto2Flag1.isNotEmpty && _lastKnownState == "DKR") {
        if (_listFotoTempatTinggal.length == _listFoto2Flag1.length){
          for (var i = 0; i < _listFoto2Flag1.length; i++) {
            _islistFotoTempatTinggalChanges = _listFotoTempatTinggal[i].path != _listFoto2Flag1[i]['path_photo'];
          }
        }
        else {
          _islistFotoTempatTinggalChanges = true;
        }
      }

      if(_listFoto2Flag2.isNotEmpty && _lastKnownState == "DKR") {
        if (_listFotoTempatUsaha.length == _listFoto2Flag2.length){
          for (var i = 0; i < _listFoto2Flag2.length; i++) {
            _islistFotoTempatUsahaChanges = _listFotoTempatUsaha[i].path != _listFoto2Flag2[i]['path_photo'];
          }
        }
        else {
          _islistFotoTempatUsahaChanges = true;
        }
      }

      if(_data.isNotEmpty && _lastKnownState == "DKR"){
        _isoccupationSelectedChanges = this._occupationSelected.KODE != _data[0]['work'] || _data[0]['edit_work'] == '1';
        _istypeOfFinancingModelSelectedChanges = _typeOfFinancingModelSelected.financingTypeId != _data[0]['financing_type'] || _data[0]['edit_financing_type'] == '1';
        _iskegiatanUsahaSelectedChanges = _kegiatanUsahaSelected.id.toString() != _data[0]['business_activities'] || _data[0]['edit_business_activities'] == '1';
        _isjenisKegiatanUsahaSelectedChanges = _jenisKegiatanUsahaSelected.id.toString() != _data[0]['business_activities_type']|| _data[0]['edit_business_activities_type'] == '1';
        _isjenisKonsepSelectedChanges = _jenisKonsepSelected.id != _data[0]['concept_type'] || _data[0]['edit_concept_type'] == '1';
      }

      if(_listFoto1Flag1.isNotEmpty && _lastKnownState == "DKR") {
        for(int i=0; i<_listFoto1Flag1.length; i++){
          for(int j=0; j<_listFoto2.length; j++){
            if(_listFoto1[i]['detail_id'] == _listFoto2[j]['detail_id']){
              _islistGroupUnitObjectChanges = _listGroupUnitObject[i].listImageGroupObjectUnit[0].path != _listFoto2[j]['path_photo'];
            }
          }
        }
      }

      _islistDocumentChanges = false;
      for(var i=0; i<_listDocument.length; i++){
        if(_listDocument[i].isEdit){
          _islistDocumentChanges = true;
        }
      }

      print("Cek islistFotoTempatTinggalChanges $_islistFotoTempatTinggalChanges"); //TODO : print("Cek islistFotoTempatTinggalChanges $_islistFotoTempatTinggalChanges");
      print("Cek islistFotoTempatUsahaChanges $_islistFotoTempatUsahaChanges"); //TODO : print("Cek islistFotoTempatUsahaChanges $_islistFotoTempatUsahaChanges");
      print("Cek islistDocumentChanges $_islistDocumentChanges"); //TODO : print("Cek islistDocumentChanges $_islistDocumentChanges");
      print("Cek islistGroupUnitObjectChanges $_islistGroupUnitObjectChanges"); //TODO : print("Cek islistGroupUnitObjectChanges $_islistGroupUnitObjectChanges");
    }
  }
}
