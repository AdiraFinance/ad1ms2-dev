import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/group_sales_model.dart';
import 'package:ad1ms2_dev/models/grup_id_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SearchGrupIdChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  TextEditingController _controllerSearch = TextEditingController();
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<GrupIdModel> _listGrupId = [];
  List<GrupIdModel> _listGrupIdTemp = [];

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<GrupIdModel> get listGrupId {
    return UnmodifiableListView(this._listGrupId);
  }

  UnmodifiableListView<GrupIdModel> get listGrupIdTemp {
    return UnmodifiableListView(this._listGrupIdTemp);
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  void getGrupId() async{
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    this._listGrupId.clear();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var storage = FlutterSecureStorage();
    String _grupID = await storage.read(key: "FieldGrupID");
    var _body = jsonEncode({
      "refOne" : _preferences.getString("branchid")
    });
    final _response = await _http.post(
      "${BaseUrl.urlGeneral}$_grupID",
      // "${urlPublic}group-sales/get_group_id",
      body: _body,
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      if(_result.isEmpty){
        showSnackBar("Grup ID tidak ditemukan");
        loadData = false;
      }
      else{
        for(int i=0; i <_result.length; i++){
          this._listGrupId.add(
              GrupIdModel(_result[i]['groupId'].toString(), _result[i]['groupName'].toString())
          );
        }
        loadData = false;
      }
    }
    else{
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void searchGrupId(String query) {
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    } else {
      _listGrupIdTemp.clear();
      if (query.isEmpty) {
        return;
      }

      _listGrupId.forEach((dataGrupID) {
        if (dataGrupID.kode.contains(query) || dataGrupID.deskripsi.contains(query)) {
          _listGrupIdTemp.add(dataGrupID);
        }
      });
    }
    notifyListeners();
  }

  void clearSearchTemp() {
    _listGrupIdTemp.clear();
  }
}
