import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/group_sales_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';

class SearchGroupSalesChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  TextEditingController _controllerSearch = TextEditingController();
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<GroupSalesModel> _listGroupSales = [
    // GroupSalesModel("00001", "COMMERCIAL"),
    // GroupSalesModel("0000P", "UMCY"),
    // GroupSalesModel("0000Q", "NMCY YAMAHA"),
    // GroupSalesModel("0000U", "UCAR"),
    // GroupSalesModel("00004", "NDS"),
    // GroupSalesModel("00003", "NCAR 2"),
    // GroupSalesModel("00001", "NCAR 1"),
    // GroupSalesModel("0000Y", "GROUP CUSTOMER"),
  ];
  List<GroupSalesModel> _listGroupSalesTemp = [];

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<GroupSalesModel> get listGroupSales {
    return UnmodifiableListView(this._listGroupSales);
  }

  UnmodifiableListView<GroupSalesModel> get listGroupSalesTemp {
    return UnmodifiableListView(this._listGroupSalesTemp);
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  void getGroupSales(BuildContext context) async{
    var _providerInfoObjectUnit = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
    this._listGroupSales.clear();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var storage = FlutterSecureStorage();
    String _fieldGrupSales = await storage.read(key: "FieldGrupSales");
    var _body = jsonEncode({
      "refOne" : _providerInfoObjectUnit.prodMatrixId != null ? _providerInfoObjectUnit.prodMatrixId : "",
      "refTwo" : _providerInfoObjectUnit.brandObjectSelected != null ? _providerInfoObjectUnit.brandObjectSelected.id : ""
    });
    final _response = await _http.post(
      "${BaseUrl.urlGeneral}$_fieldGrupSales",
      // "${urlPublic}group-sales/get_grup_sales",
      body: _body,
      headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );
    if(_response.statusCode == 200){
      List _result = jsonDecode(_response.body);
      print(_result);
      if(_result.isEmpty){
        showSnackBar("Group Sales tidak ditemukan");
        loadData = false;
      }
      else{
        for(int i=0; i <_result.length; i++){
          this._listGroupSales.add(
              GroupSalesModel(_result[i]['kode'], _result[i]['deskripsi'])
          );
        }
        loadData = false;
      }
    }
    else{
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void searchGrupSales(String query) {
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    } else {
      _listGroupSalesTemp.clear();
      if (query.isEmpty) {
        return;
      }

      _listGroupSales.forEach((dataProgram) {
        if (dataProgram.kode.contains(query) || dataProgram.deskripsi.contains(query)) {
          _listGroupSalesTemp.add(dataProgram);
        }
      });
    }
    notifyListeners();
  }

  void clearSearchTemp() {
    _listGroupSalesTemp.clear();
  }
}
