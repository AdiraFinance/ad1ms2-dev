import 'dart:collection';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/address_guarantor_model.dart';
import 'package:ad1ms2_dev/models/pemegang_saham_model.dart';
import 'package:ad1ms2_dev/models/showhide_mandatory_visibility/set_pemegang_saham_lembaga_company_model.dart';
import 'package:ad1ms2_dev/shared/pemegang_saham_lembaga_change_notif.dart';
import 'package:ad1ms2_dev/shared/resource/validate_data.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../main.dart';
import 'constants.dart';
import 'dashboard/dashboard_change_notif.dart';

class AddPemegangSahamLembagaChangeNotif with ChangeNotifier {
  bool _autoValidate = false;
  bool _loadData = false;
  TypeInstitutionModel _typeInstitutionSelected;
  TextEditingController _controllerInstitutionName = TextEditingController();
  TextEditingController _controllerDateOfEstablishment = TextEditingController();
  DateTime _initialDateForDateOfEstablishment = DateTime(dateNow.year, dateNow.month, dateNow.day);
  TextEditingController _controllerNPWP = TextEditingController();
  TextEditingController _controllerPercentShare = TextEditingController();
  DbHelper _dbHelper = DbHelper();
  String _custType;
  String _lastKnownState;
  String _shareholdersCorporateID;

  SetPemegangSahamLembagaCompanyModel _setPemegangSahamLembagaCompanyModel;

  bool _isEdit = false;
  bool _companyTypeDakor = false;
  bool _companyNameDakor = false;
  bool _establishDateDakor = false;
  bool _npwpDakor = false;
  bool _shareDakor = false;

  int _selectedIndex = -1;
  List<AddressModelCompany> _listPemegangSahamKelembagaanAddress = [];
  TextEditingController _controllerAddress = TextEditingController();
  TextEditingController _controllerAddressType = TextEditingController();
  TextEditingController _controllerRT = TextEditingController();
  TextEditingController _controllerRW = TextEditingController();
  TextEditingController _controllerKelurahan = TextEditingController();
  TextEditingController _controllerKecamatan = TextEditingController();
  TextEditingController _controllerKota = TextEditingController();
  TextEditingController _controllerProvinsi = TextEditingController();
  TextEditingController _controllerPostalCode = TextEditingController();
  TextEditingController _controllerTelepon1Area = TextEditingController();
  TextEditingController _controllerTelepon1 = TextEditingController();
  TextEditingController _controllerTelepon2Area = TextEditingController();
  TextEditingController _controllerTelepon2 = TextEditingController();
  TextEditingController _controllerFaxArea = TextEditingController();
  TextEditingController _controllerFax = TextEditingController();

  int get selectedIndex => _selectedIndex;

  set selectedIndex(int value) {
    this._selectedIndex = value;
    notifyListeners();
  }

  List<AddressModelCompany> get listPemegangSahamKelembagaanAddress => _listPemegangSahamKelembagaanAddress;

  // Alamat
  TextEditingController get controllerAddress => _controllerAddress;

  // Jenis Alamat
  TextEditingController get controllerAddressType => _controllerAddressType;

  // RT
  TextEditingController get controllerRT => _controllerRT;

  // RW
  TextEditingController get controllerRW => _controllerRW;

  // Kelurahan
  TextEditingController get controllerKelurahan => _controllerKelurahan;

  // Kecamatan
  TextEditingController get controllerKecamatan => _controllerKecamatan;

  // Kabupaten/Kota
  TextEditingController get controllerKota => _controllerKota;

  // Provinsi
  TextEditingController get controllerProvinsi => _controllerProvinsi;

  // Kode Pos
  TextEditingController get controllerPostalCode => _controllerPostalCode;

  // Telepon 1 Area
  TextEditingController get controllerTelepon1Area => _controllerTelepon1Area;

  // Telepon 1
  TextEditingController get controllerTelepon1 => _controllerTelepon1;

  // Telepon 2 Area
  TextEditingController get controllerTelepon2Area => _controllerTelepon2Area;

  // Telepon 2
  TextEditingController get controllerTelepon2 => _controllerTelepon2;

  // Kode Area Fax
  TextEditingController get controllerFaxArea => _controllerFaxArea;

  // Fax
  TextEditingController get controllerFax => _controllerFax;

  void addPemegangSahamKelembagaanAddress(AddressModelCompany value) {
    this._listPemegangSahamKelembagaanAddress.add(value);
    notifyListeners();
  }

  void updatePemegangSahamKelembagaanAddress(AddressModelCompany value, int index) {
    this._listPemegangSahamKelembagaanAddress[index] = value;
    notifyListeners();
  }

  void deletePemegangSahamKelembagaanAddress(BuildContext context, int index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                primaryColor: Colors.black,
                primarySwatch: primaryOrange,
                accentColor: myPrimaryColor
            ),
            child: AlertDialog(
              title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Apakah kamu yakin menghapus alamat ini?",),
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    if(this._lastKnownState == "IDE"){
                      this._listPemegangSahamKelembagaanAddress.removeAt(index);
                      if(selectedIndex == index){
                        selectedIndex = -1;
                        this._controllerAddress.clear();
                        this._controllerAddressType.clear();
                        this._controllerRT.clear();
                        this._controllerRW.clear();
                        this._controllerKelurahan.clear();
                        this._controllerKecamatan.clear();
                        this._controllerKota.clear();
                        this._controllerProvinsi.clear();
                        this._controllerPostalCode.clear();
                        this._controllerTelepon1Area.clear();
                        this._controllerTelepon1.clear();
                        this._controllerTelepon2Area.clear();
                        this._controllerTelepon2.clear();
                        this._controllerFaxArea.clear();
                        this._controllerFax.clear();
                      }
                    }
                    else{
                      this._listPemegangSahamKelembagaanAddress[index].active = 1;
                    }
                    notifyListeners();
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                ),
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Tidak'),
                ),
              ],
            ),
          );
        }
    );
  }

  void setAddress(AddressModelCompany value) {
    this._controllerAddress.text = value.address;
    this._controllerAddressType.text = value.jenisAlamatModel.KODE + " - " + value.jenisAlamatModel.DESKRIPSI;
    this._controllerRT.text = value.rt;
    this._controllerRW.text = value.rw;
    this._controllerKelurahan.text = value.kelurahanModel.KEL_ID + " - " + value.kelurahanModel.KEL_NAME;
    this._controllerKecamatan.text = value.kelurahanModel.KEC_ID + " - " + value.kelurahanModel.KEC_NAME;
    this._controllerKota.text = value.kelurahanModel.KABKOT_ID + " - " + value.kelurahanModel.KABKOT_NAME;
    this._controllerProvinsi.text = value.kelurahanModel.PROV_ID + " - " + value.kelurahanModel.PROV_NAME;
    this._controllerPostalCode.text = value.kelurahanModel.ZIPCODE;
    this._controllerTelepon1Area.text = value.phoneArea1;
    this._controllerTelepon1.text = value.phone1;
    this._controllerTelepon2Area.text = value.phoneArea2;
    this._controllerTelepon2.text = value.phone2;
    this._controllerFaxArea.text = value.faxArea;
    this._controllerFax.text = value.fax;
    notifyListeners();
  }

  void setCorrespondence(int index){
    for(int i=0; i < this._listPemegangSahamKelembagaanAddress.length; i++){
      if(this._listPemegangSahamKelembagaanAddress[i].isCorrespondence){
        this._listPemegangSahamKelembagaanAddress[i].isCorrespondence = false;
      }
    }
    this._listPemegangSahamKelembagaanAddress[index].isCorrespondence = true;
    notifyListeners();
  }

  void moreDialog(BuildContext context, index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans"
            ),
            child: AlertDialog(
              title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.check,
                          color: Colors.green,
                          size: 22.0,
                        ),
                        SizedBox(width: 12.0),
                        Expanded(
                          child: GestureDetector(
                            onTap: (){
                              selectedIndex = index;
                              _controllerAddress.clear();
                              setAddress(listPemegangSahamKelembagaanAddress[index]);
                              setCorrespondence(index);
                              Navigator.pop(context);
                              Navigator.pop(context);
                            },
                            child: Text(
                              "Pilih sebagai alamat korespondensi",
                              style: TextStyle(fontSize: 14.0),
                            ),
                          ),
                        )
                      ]
                  ),
                  SizedBox(height: 12.0),
                  listPemegangSahamKelembagaanAddress[index].jenisAlamatModel.KODE != "03" ?
                      // ? listPemegangSahamKelembagaanAddress[index].isSameWithIdentity
                      // ? SizedBox()
                      // :
                  Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.delete,
                          color: Colors.red,
                          size: 22.0,
                        ),
                        SizedBox(
                          width: 12.0,
                        ),
                        GestureDetector(
                          onTap: (){
                            Navigator.pop(context);
                            deletePemegangSahamKelembagaanAddress(context, index);
                          },
                          child: Text(
                            "Hapus",
                            style: TextStyle(fontSize: 14.0, color: Colors.red),
                          ),
                        )
                      ]
                  )
                      : SizedBox(),
                ],
              ),
              actions: <Widget>[
                FlatButton(
                    onPressed: (){
                      Navigator.pop(context);
                      // _updateStatusShowDialogSimilarity();
                    },
                    child: Text(
                        "CLOSE",
                        style: TextStyle(
                            color: primaryOrange,
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 1.25
                        )
                    )
                )
              ],
            ),
          );
        }
    );
  }

  void clearDataAlamat() {
    this._selectedIndex = -1;
    this._controllerAddress.clear();
    this._controllerAddressType.clear();
    this._controllerRT.clear();
    this._controllerRW.clear();
    this._controllerKelurahan.clear();
    this._controllerKecamatan.clear();
    this._controllerKota.clear();
    this._controllerProvinsi.clear();
    this._controllerPostalCode.clear();
    this._controllerTelepon1Area.clear();
    this._controllerTelepon1.clear();
    this._controllerTelepon2Area.clear();
    this._controllerTelepon2.clear();
    this._controllerFaxArea.clear();
    this._controllerFax.clear();
    this._listPemegangSahamKelembagaanAddress = [];
  }

  bool get autoValidate => _autoValidate;
  GlobalKey<FormState> _key = GlobalKey<FormState>();

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  List<TypeInstitutionModel> _listTypeInstitution = TypeInstitutionList().typeInstitutionItems;

  // Jenis Lembaga
  UnmodifiableListView<TypeInstitutionModel> get listTypeInstitution {
    return UnmodifiableListView(this._listTypeInstitution);
  }

  TypeInstitutionModel get typeInstitutionSelected => _typeInstitutionSelected;

  set typeInstitutionSelected(TypeInstitutionModel value) {
    this._typeInstitutionSelected = value;
    notifyListeners();
  }

  // Nama Lembaga
  TextEditingController get controllerInstitutionName {
    return this._controllerInstitutionName;
  }

  set controllerInstitutionName(value) {
    this._controllerInstitutionName = value;
    this.notifyListeners();
  }

  // Tanggal Pendirian
  TextEditingController get controllerDateOfEstablishment => _controllerDateOfEstablishment;

  DateTime get initialDateForBirthOfDate => _initialDateForDateOfEstablishment;

  void selectDateOfEstablishment(BuildContext context) async {
    // DatePickerShared _datePickerShared = DatePickerShared();
    // var _datePickerSelected = await _datePickerShared.selectStartDate(
    //     context, this._initialDateForDateOfEstablishment,
    //     canAccessNextDay: false);
    // if (_datePickerSelected != null) {
    //   this._controllerDateOfEstablishment.text = dateFormat.format(_datePickerSelected);
    //   this._initialDateForDateOfEstablishment = _datePickerSelected;
    //   notifyListeners();
    // } else {
    //   return;
    // }
    var _dateSelected = await selectDate(context, _initialDateForDateOfEstablishment);
    if (_dateSelected != null) {
      _initialDateForDateOfEstablishment = _dateSelected;
      this._controllerDateOfEstablishment.text = dateFormat.format(_dateSelected);
      notifyListeners();
    } else {
      return;
    }
  }

  // NPWP
  TextEditingController get controllerNPWP {
    return this._controllerNPWP;
  }

  set controllerNPWP(value) {
    this._controllerNPWP = value;
    this.notifyListeners();
  }

  // % Share
  TextEditingController get controllerPercentShare {
    return this._controllerPercentShare;
  }

  set controllerPercentShare(value) {
    this._controllerPercentShare = value;
    this.notifyListeners();
  }

  GlobalKey<FormState> get keyForm => _key;

  String get custType => _custType;

  set custType(String value) {
    this._custType = value;
    notifyListeners();
  }

  String get lastKnownState => _lastKnownState;

  set lastKnownState(String value) {
    this._lastKnownState = value;
    notifyListeners();
  }

  ValidateData _validateData = ValidateData();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void _showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating,backgroundColor: snackbarColor, duration: Duration(seconds: 4)));
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    _loadData = value;
    notifyListeners();
  }

  String get shareholdersCorporateID => _shareholdersCorporateID;

  set shareholdersCorporateID(String value) {
    this._shareholdersCorporateID = value;
  }

  Future<void> validateNameOrNPWP(BuildContext context,String typeValidate) async{
    try{
      if(typeValidate == "NPWP"){
        print("NPWP");
        if(this._controllerNPWP.text != ""){
          if(await _validateData.validateNPWP(this._controllerNPWP.text) != "SUCCESS"){
            _showSnackBar("${await _validateData.validateNPWP(this._controllerNPWP.text)}");
          }
        }
      }
      else{
        print("ELSE");
        loadData = true;
        if(await _validateData.validateNPWP(this._controllerNPWP.text) != "SUCCESS"){
          _showSnackBar("${await _validateData.validateNPWP(this._controllerNPWP.text)}");
        }
        else{
          Navigator.pop(context);
        }
        loadData = false;
      }
    }
    catch(e){
      _showSnackBar(e.toString());
    }
  }

  void check(BuildContext context, int flag, int index) {
    var _provider = Provider.of<PemegangSahamLembagaChangeNotifier>(context, listen: false);
    final _form = _key.currentState;
    bool _status = false;
    for(int i=0; i<_provider.listPemegangSahamLembaga.length; i++){
      if(this._controllerNPWP.text == _provider.listPemegangSahamLembaga[i].npwp && flag == 0){
        _status = true;
        _showSnackBar("NPWP telah digunakan");
      }
    }
    if (_form.validate()) {
      if(flag == 0){
        if(!_status){
          _provider.addPemegangSahamLembaga(PemegangSahamLembagaModel(
            this._typeInstitutionSelected,
            this._controllerInstitutionName.text,
            this._initialDateForDateOfEstablishment.toString(),// this._controllerDateOfEstablishment.text,
            this._controllerNPWP.text,
            this._controllerPercentShare.text,
            this._listPemegangSahamKelembagaanAddress,
            isEdit,
            this._companyTypeDakor,
            this._companyNameDakor,
            this._establishDateDakor,
            this._npwpDakor,
            this._shareDakor,null
          ));
        }
      }
      else{
        _provider.updateListPemegangSahamLembaga(index, PemegangSahamLembagaModel(
          this._typeInstitutionSelected,
          this._controllerInstitutionName.text,
          this._initialDateForDateOfEstablishment.toString(),
          this._controllerNPWP.text,
          this._controllerPercentShare.text,
          this._listPemegangSahamKelembagaanAddress,
          isEdit,
          this._companyTypeDakor,
          this._companyNameDakor,
          this._establishDateDakor,
          this._npwpDakor,
          this._shareDakor,shareholdersCorporateID
        ));
      }
      autoValidate = false;
      checkDataDakor();
      if(!_status){
        Navigator.pop(context);
      }
    }
    else {
      autoValidate = true;
    }
  }

  void clearDataPemegangSahamKelembagaan() {
    this._autoValidate = false;
    this._typeInstitutionSelected = null;
    this._controllerInstitutionName.clear();
    this._controllerDateOfEstablishment.clear();
    this._controllerNPWP.clear();
    this._controllerPercentShare.clear();
    this._companyTypeDakor = false;
    this._companyNameDakor = false;
    this._establishDateDakor = false;
    this._npwpDakor = false;
    this._shareDakor = false;
    clearDataAlamat();
  }

  Future<void> setValue(int flag, int index, PemegangSahamLembagaModel value, BuildContext context) async{
    await getDataFromDashboard(context);
    for (int i = 0; i < this._listTypeInstitution.length; i++) {
      if (value.typeInstitutionModel.PARA_ID == _listTypeInstitution[i].PARA_ID) {
        _typeInstitutionSelected = _listTypeInstitution[i];
      }
    }
    this._controllerInstitutionName.text = value.institutionName;
    if(value.initialDateForDateOfEstablishment != ""){
      this._initialDateForDateOfEstablishment = DateTime.parse(value.initialDateForDateOfEstablishment);
      this._controllerDateOfEstablishment.text = dateFormat.format(DateTime.parse(value.initialDateForDateOfEstablishment));
    }
    this._controllerNPWP.text = value.npwp;
    this._controllerPercentShare.text = value.percentShare;
    for (int i = 0; i < value.listAddress.length; i++) {
      if (value.listAddress[i].isCorrespondence) {
        this._selectedIndex = i;
        this._controllerAddress.text = value.listAddress[i].address;
        this._controllerAddressType.text = value.listAddress[i].jenisAlamatModel.KODE + " - " + value.listAddress[i].jenisAlamatModel.DESKRIPSI;
        this._controllerRT.text = value.listAddress[i].rt;
        this._controllerRW.text = value.listAddress[i].rw;
        this._controllerKelurahan.text = value.listAddress[i].kelurahanModel.KEL_ID + " - " + value.listAddress[i].kelurahanModel.KEL_NAME;
        this._controllerKecamatan.text = value.listAddress[i].kelurahanModel.KEC_ID + " - " + value.listAddress[i].kelurahanModel.KEC_NAME;
        this._controllerKota.text = value.listAddress[i].kelurahanModel.KABKOT_ID + " - " + value.listAddress[i].kelurahanModel.KABKOT_NAME;
        this._controllerProvinsi.text = value.listAddress[i].kelurahanModel.PROV_ID + " - " + value.listAddress[i].kelurahanModel.PROV_NAME;
        this._controllerTelepon1Area.text = value.listAddress[i].phoneArea1;
        this._controllerTelepon1.text = value.listAddress[i].phone1;
        this._controllerTelepon2Area.text = value.listAddress[i].phoneArea2;
        this._controllerTelepon2.text = value.listAddress[i].phone2;
        this._controllerFaxArea.text = value.listAddress[i].faxArea;
        this._controllerFax.text = value.listAddress[i].faxArea;
        this._controllerPostalCode.text = value.listAddress[i].kelurahanModel.ZIPCODE;
      }
    }
    for (int i = 0; i < value.listAddress.length; i++) {
      this._listPemegangSahamKelembagaanAddress.add(value.listAddress[i]);
    }
    shareholdersCorporateID = value.shareholdersCorporateID;
    checkDataDakor();
    notifyListeners();
  }

  Future<void> getDataFromDashboard(BuildContext context) async{
    _setPemegangSahamLembagaCompanyModel = Provider.of<DashboardChangeNotif>(context, listen: false).setPemegangSahamLembagaCompanyModel;
    SharedPreferences _preference = await SharedPreferences.getInstance();
    this._custType = _preference.getString("cust_type");
    this._lastKnownState = _preference.getString("last_known_state");
  }

  bool isCompanyTypeVisible() => _setPemegangSahamLembagaCompanyModel.companyTypeVisibility;
  bool isCompanyNameVisible() => _setPemegangSahamLembagaCompanyModel.companyNameVisibility;
  bool isEstablishDateVisible() => _setPemegangSahamLembagaCompanyModel.establishDateVisibility;
  bool isNpwpVisible() => _setPemegangSahamLembagaCompanyModel.npwpVisibility;
  bool isShareVisible() => _setPemegangSahamLembagaCompanyModel.shareVisibility;

  bool isCompanyTypeMandatory() => _setPemegangSahamLembagaCompanyModel.companyTypeMandatory;
  bool isCompanyNameMandatory() => _setPemegangSahamLembagaCompanyModel.companyNameMandatory;
  bool isEstablishDateMandatory() => _setPemegangSahamLembagaCompanyModel.establishDateMandatory;
  bool isNpwpMandatory() => _setPemegangSahamLembagaCompanyModel.npwpMandatory;
  bool isShareMandatory() => _setPemegangSahamLembagaCompanyModel.shareMandatory;

  // Fungsi validasi mandatory berdasarkan field untuk pemegang saham mandatory
  bool checkIsMandatoryPemegangSahamLembaga(){
    return !isCompanyTypeMandatory() && !isCompanyNameMandatory() && !isEstablishDateMandatory() && !isNpwpMandatory()
        && !isShareMandatory();
  }

  void checkDataDakor() async{
    List _data = await _dbHelper.selectMS2CustShrhldrCom();
    if(_data.isNotEmpty && _lastKnownState == "DKR"){
      if(this._typeInstitutionSelected != null){
        companyTypeDakor = this._typeInstitutionSelected.PARA_ID != _data[0]['comp_type'].toString() || _data[0]['edit_comp_type'] == "1";
      }
      companyNameDakor = this._controllerInstitutionName.text != _data[0]['comp_name'].toString() || _data[0]['edit_comp_name'] == "1";
      establishDateDakor = this._initialDateForDateOfEstablishment != DateTime.parse(_data[0]['establish_date']) || _data[0]['edit_establish_date'] == "1";
      npwpDakor = this._controllerNPWP.text != _data[0]['npwp_no'].toString() || _data[0]['edit_npwp_no'] == "1";
      shareDakor = this._controllerPercentShare.text != _data[0]['shrldng_percent'].toString() || _data[0]['edit_shrldng_percent'] == "1";
      if(companyTypeDakor || companyNameDakor || establishDateDakor || npwpDakor || shareDakor){
        isEdit = true;
      } else {
        isEdit = false;
      }
    }
    notifyListeners();
  }

  bool get isEdit => _isEdit;

  set isEdit(bool value) {
    _isEdit = value;
    notifyListeners();
  }

  bool get shareDakor => _shareDakor;

  set shareDakor(bool value) {
    _shareDakor = value;
    notifyListeners();
  }

  bool get npwpDakor => _npwpDakor;

  set npwpDakor(bool value) {
    _npwpDakor = value;
    notifyListeners();
  }

  bool get establishDateDakor => _establishDateDakor;

  set establishDateDakor(bool value) {
    _establishDateDakor = value;
    notifyListeners();
  }

  bool get companyNameDakor => _companyNameDakor;

  set companyNameDakor(bool value) {
    _companyNameDakor = value;
    notifyListeners();
  }

  bool get companyTypeDakor => _companyTypeDakor;

  set companyTypeDakor(bool value) {
    _companyTypeDakor = value;
    notifyListeners();
  }

  void limitInput(String value){
    if (int.parse(value) > 100) {
      Future.delayed(Duration(milliseconds: 100), () {
        this._controllerPercentShare.clear();
      });
    }
  }
}
