import 'dart:collection';

import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/address_guarantor_model.dart';
import 'package:ad1ms2_dev/models/form_m_company_rincian_model.dart';
import 'package:ad1ms2_dev/models/guarantor_idividual_model.dart';
import 'package:ad1ms2_dev/shared/form_m_company_guarantor_change_notifier.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

import 'constants.dart';

//ga kepake
class FormMCompanyAddGuarantorCompanyChangeNotifier with ChangeNotifier {
  int _oldListSize = 0;
  bool _autoValidate = false;
  int _selectedIndex = -1;
  int _oldSelectedIndex = -1;
  TypeInstitutionModel _typeInstitutionSelected;
  TypeInstitutionModel _typeInstitutionTemp;
  List<TypeInstitutionModel> _listTypeInstitution =
      TypeInstitutionList().typeInstitutionItems;
  ProfilModel _profilSelected;
  ProfilModel _profilTemp;
  TextEditingController _controllerInstitutionName = TextEditingController();
  TextEditingController _controllerNPWP = TextEditingController();
  TextEditingController _controllerAddress = TextEditingController();
  TextEditingController _controllerAddressType = TextEditingController();
  TextEditingController _controllerRT = TextEditingController();
  TextEditingController _controllerRW = TextEditingController();
  TextEditingController _controllerKelurahan = TextEditingController();
  TextEditingController _controllerKecamatan = TextEditingController();
  TextEditingController _controllerKota = TextEditingController();
  TextEditingController _controllerProvinsi = TextEditingController();
  TextEditingController _controllerPostalCode = TextEditingController();
  TextEditingController _controllerTelephoneArea1 = TextEditingController();
  TextEditingController _controllerTelephoneArea2 = TextEditingController();
  TextEditingController _controllerTelephone1 = TextEditingController();
  TextEditingController _controllerTelephone2 = TextEditingController();
  TextEditingController _controllerFaxArea = TextEditingController();
  TextEditingController _controllerFax = TextEditingController();
  List<AddressModelCompany> _listGuarantorAddress = [];
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  String _institutionNameTemp,
      _npwpTemp,
      _addressTemp,
      _addressTypeTemp,
      _rtTemp,
      _rwTemp,
      _kelurahanTemp,
      _kecamatanTemp,
      _kotaTemp,
      _provTemp,
      _postalCodeTemp,
      _phoneArea1Temp,
      _phone1Temp,
      _phoneArea2Temp,
      _phone2Temp,
      _faxAreaTemp,
      _faxTemp;
  int _sizeList = 0;

  List<ProfilModel> _listProfil = [
    ProfilModel("01", "PT"),
    ProfilModel("02", "CV"),
    ProfilModel("03", "FOUNDATION"),
    ProfilModel("05", "KOPERASI"),
  ];

  int get oldListSize => _oldListSize;

  set oldListSize(int value) {
    this._oldListSize = value;
    notifyListeners();
  }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  // Jenis Lembaga
  UnmodifiableListView<TypeInstitutionModel> get listTypeInstitution {
    return UnmodifiableListView(this._listTypeInstitution);
  }

  TypeInstitutionModel get typeInstitutionSelected => _typeInstitutionSelected;

  set typeInstitutionSelected(TypeInstitutionModel value) {
    this._typeInstitutionSelected = value;
    notifyListeners();
  }

  // Profil
  UnmodifiableListView<ProfilModel> get listProfil {
    return UnmodifiableListView(this._listProfil);
  }

  ProfilModel get profilSelected => _profilSelected;

  set profilSelected(ProfilModel value) {
    this._profilSelected = value;
    notifyListeners();
  }

  TextEditingController get controllerInstitutionName =>
      _controllerInstitutionName;

  TextEditingController get controllerNPWP => _controllerNPWP;

  TextEditingController get controllerAddress => _controllerAddress;

  TextEditingController get controllerAddressType => _controllerAddressType;

  TextEditingController get controllerRT => _controllerRT;

  TextEditingController get controllerRW => _controllerRW;

  TextEditingController get controllerKelurahan => _controllerKelurahan;

  TextEditingController get controllerKecamatan => _controllerKecamatan;

  TextEditingController get controllerKota => _controllerKota;

  TextEditingController get controllerProvinsi => _controllerProvinsi;

  TextEditingController get controllerPostalCode => _controllerPostalCode;

  TextEditingController get controllerTelephoneArea1 =>
      _controllerTelephoneArea1;

  TextEditingController get controllerTelephone1 => _controllerTelephone1;

  TextEditingController get controllerTelephoneArea2 =>
      _controllerTelephoneArea2;

  TextEditingController get controllerTelephone2 => _controllerTelephone2;

  TextEditingController get controllerFax => _controllerFax;

  TextEditingController get controllerFaxArea => _controllerFaxArea;

  GlobalKey<FormState> get key => _key;

  int get selectedIndex => _selectedIndex;

  set selectedIndex(int value) {
    this._selectedIndex = value;
    notifyListeners();
  }

  List<AddressModelCompany> get listGuarantorAddress =>
      _listGuarantorAddress;

  ProfilModel get profilTemp => _profilTemp;

  TypeInstitutionModel get typeInstitutionTemp => _typeInstitutionTemp;

  void addGuarantorAddress(AddressModelCompany value) {
    this._listGuarantorAddress.add(value);
    notifyListeners();
  }

  void updateGuarantorAddress(AddressModelCompany value, int index) {
    this._listGuarantorAddress[index] = value;
    if (index == this._selectedIndex) setCorrespondenceAddress(value, index);
    notifyListeners();
  }

  void deleteListGuarantorAddress(BuildContext context, int index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                primaryColor: Colors.black,
                primarySwatch: primaryOrange,
                accentColor: myPrimaryColor
            ),
            child: AlertDialog(
              title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Apakah kamu yakin menghapus alamat ini?",),
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    this._listGuarantorAddress.removeAt(index);
                    if (selectedIndex == index) {
                      selectedIndex = -1;
                      this._controllerAddress.clear();
                      this._controllerAddressType.clear();
                      this._controllerRT.clear();
                      this._controllerRW.clear();
                      this._controllerKelurahan.clear();
                      this._controllerKecamatan.clear();
                      this._controllerKota.clear();
                      this._controllerProvinsi.clear();
                      this._controllerPostalCode.clear();
                      this._controllerTelephoneArea1.clear();
                      this._controllerTelephone1.clear();
                      this._controllerTelephoneArea2.clear();
                      this._controllerTelephone2.clear();
                      this._controllerFaxArea.clear();
                      this._controllerFax.clear();
                    }
                    notifyListeners();
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                ),
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Tidak'),
                ),
              ],
            ),
          );
        }
    );
  }

  void setCorrespondenceAddress(AddressModelCompany value, index) {
    this._controllerAddress.text = value.address;
    this._controllerAddressType.text = value.jenisAlamatModel.DESKRIPSI;
    this._controllerRT.text = value.rt;
    this._controllerRW.text = value.rw;
    this._controllerKelurahan.text = value.kelurahanModel.KEL_NAME;
    this._controllerKecamatan.text = value.kelurahanModel.KEC_NAME;
    this._controllerKota.text = value.kelurahanModel.KABKOT_NAME;
    this._controllerProvinsi.text = value.kelurahanModel.PROV_NAME;
    this._controllerPostalCode.text = value.kelurahanModel.ZIPCODE;
    this._controllerTelephoneArea1.text = value.phoneArea1;
    this._controllerTelephone1.text = value.phone1;
    this._controllerTelephoneArea2.text = value.phoneArea2;
    this._controllerTelephone2.text = value.phone2;
    this._controllerFaxArea.text = value.faxArea;
    this._controllerFax.text = value.fax;
    notifyListeners();
  }

  void check(BuildContext context, int flag, int index) {
    final _form = _key.currentState;
    List<AddressModelCompany> _model = [];
    if (flag == 0) {
      if (_form.validate()) {
        for (int i = 0; i < this._listGuarantorAddress.length; i++) {
          if (_selectedIndex == i) {
            _model.add(AddressModelCompany(
                this._listGuarantorAddress[i].jenisAlamatModel,
                this._listGuarantorAddress[i].addressID,
                this._listGuarantorAddress[i].foreignBusinessID,
                null,
                this._listGuarantorAddress[i].kelurahanModel,
                this._controllerAddress.text,
                this._controllerRT.text,
                this._controllerRW.text,
                this._controllerTelephoneArea1.text,
                this._controllerTelephone1.text,
                this._controllerTelephoneArea2.text,
                this._controllerTelephone2.text,
                this._controllerFaxArea.text,
                this._controllerFax.text,
                this._listGuarantorAddress[i].isSameWithIdentity,
                this._listGuarantorAddress[i].addressLatLong,
                true,null,null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null));
          } else {
            _model.add(AddressModelCompany(
                this._listGuarantorAddress[i].jenisAlamatModel,
                this._listGuarantorAddress[i].addressID,
                this._listGuarantorAddress[i].foreignBusinessID,
                null,
                this._listGuarantorAddress[i].kelurahanModel,
                this._controllerAddress.text,
                this._controllerRT.text,
                this._controllerRW.text,
                this._controllerTelephoneArea1.text,
                this._controllerTelephone1.text,
                this._controllerTelephoneArea2.text,
                this._controllerTelephone2.text,
                this._controllerFaxArea.text,
                this._controllerFax.text,
                this._listGuarantorAddress[i].isSameWithIdentity,
                this._listGuarantorAddress[i].addressLatLong,
                false,null,null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null));
          }
        }
        // Provider.of<FormMCompanyGuarantorChangeNotifier>(context, listen: false)
        //     .addGuarantorCompany(GuarantorCompanyModel(
        //         this._typeInstitutionSelected,
        //         this._profilSelected,
        //         null,
        //         this._controllerInstitutionName.text,
        //         this._controllerNPWP.text,
        //         _model));
        clearData();
        if (this._autoValidate) autoValidate = false;
        selectedIndex = -1;
        Navigator.pop(context);
      } else {
        autoValidate = true;
      }
    } else {
      if (_form.validate()) {
        if (this._oldSelectedIndex != this._selectedIndex) {
          for (int i = 0; i < this._listGuarantorAddress.length; i++) {
            if (_oldSelectedIndex == i) {
              _listGuarantorAddress[i].isCorrespondence = false;
            }
          }
          for (int i = 0; i < this._listGuarantorAddress.length; i++) {
            if (_selectedIndex == i) {
              _model.add(AddressModelCompany(
                  this._listGuarantorAddress[i].jenisAlamatModel,
                  this._listGuarantorAddress[i].addressID,
                  this._listGuarantorAddress[i].foreignBusinessID,
                  null,
                  this._listGuarantorAddress[i].kelurahanModel,
                  this._controllerAddress.text,
                  this._controllerRT.text,
                  this._controllerRW.text,
                  this._controllerTelephoneArea1.text,
                  this._controllerTelephone1.text,
                  this._controllerTelephoneArea2.text,
                  this._controllerTelephone2.text,
                  this._controllerFaxArea.text,
                  this._controllerFax.text,
                  this._listGuarantorAddress[i].isSameWithIdentity,
                  this._listGuarantorAddress[i].addressLatLong,
                  true,null,null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null));
            } else {
              _model.add(AddressModelCompany(
                  this._listGuarantorAddress[i].jenisAlamatModel,
                  this._listGuarantorAddress[i].addressID,
                  this._listGuarantorAddress[i].foreignBusinessID,
                  null,
                  this._listGuarantorAddress[i].kelurahanModel,
                  this._controllerAddress.text,
                  this._controllerRT.text,
                  this._controllerRW.text,
                  this._controllerTelephoneArea1.text,
                  this._controllerTelephone1.text,
                  this._controllerTelephoneArea2.text,
                  this._controllerTelephone2.text,
                  this._controllerFaxArea.text,
                  this._controllerFax.text,
                  this._listGuarantorAddress[i].isSameWithIdentity,
                  this._listGuarantorAddress[i].addressLatLong,
                  false,null,null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null));
            }
          }
          // Provider.of<FormMCompanyGuarantorChangeNotifier>(context,
          //         listen: false)
          //     .updateListGuarantorCompany(
          //         GuarantorCompanyModel(
          //             this._typeInstitutionSelected,
          //             this._profilSelected,
          //             null,
          //             this._controllerInstitutionName.text,
          //             this._controllerNPWP.text,
          //             _model),
          //         index);
          clearData();
          if (this._autoValidate) autoValidate = false;
          selectedIndex = -1;
          this._oldSelectedIndex = -1;
          Navigator.pop(context);
        } else {
          for (int i = 0; i < this._listGuarantorAddress.length; i++) {
            if (_selectedIndex == i) {
              _model.add(AddressModelCompany(
                  this._listGuarantorAddress[i].jenisAlamatModel,
                  this._listGuarantorAddress[i].addressID,
                  this._listGuarantorAddress[i].foreignBusinessID,
                  null,
                  this._listGuarantorAddress[i].kelurahanModel,
                  this._controllerAddress.text,
                  this._controllerRT.text,
                  this._controllerRW.text,
                  this._controllerTelephoneArea1.text,
                  this._controllerTelephone1.text,
                  this._controllerTelephoneArea2.text,
                  this._controllerTelephone2.text,
                  this._controllerFaxArea.text,
                  this._controllerFax.text,
                  this._listGuarantorAddress[i].isSameWithIdentity,
                  this._listGuarantorAddress[i].addressLatLong,
                  true,null,null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null));
            } else {
              _model.add(AddressModelCompany(
                  this._listGuarantorAddress[i].jenisAlamatModel,
                  this._listGuarantorAddress[i].addressID,
                  this._listGuarantorAddress[i].foreignBusinessID,
                  null,
                  this._listGuarantorAddress[i].kelurahanModel,
                  this._controllerAddress.text,
                  this._controllerRT.text,
                  this._controllerRW.text,
                  this._controllerTelephoneArea1.text,
                  this._controllerTelephone1.text,
                  this._controllerTelephoneArea2.text,
                  this._controllerTelephone2.text,
                  this._controllerFaxArea.text,
                  this._controllerFax.text,
                  this._listGuarantorAddress[i].isSameWithIdentity,
                  this._listGuarantorAddress[i].addressLatLong,
                  false,null,null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null));
            }
          }
          // Provider.of<FormMCompanyGuarantorChangeNotifier>(context,
          //         listen: false)
          //     .updateListGuarantorCompany(
          //         GuarantorCompanyModel(
          //             this._typeInstitutionSelected,
          //             this._profilSelected,
          //             null,
          //             this._controllerInstitutionName.text,
          //             this._controllerNPWP.text,
          //             _model),
          //         index);
          clearData();
          if (this._autoValidate) autoValidate = false;
          selectedIndex = -1;
          this._oldSelectedIndex = -1;
          Navigator.pop(context);
        }
      } else {
        autoValidate = true;
      }
    }
  }

  Future<void> setValueForEdit(GuarantorCompanyModel data) async {
    this._typeInstitutionSelected = data.typeInstitutionModel;
    this._typeInstitutionTemp = this._typeInstitutionSelected;
    this._profilSelected = data.profilModel;
    this._profilTemp = this._profilSelected;
    this._controllerInstitutionName.text = data.institutionName;
    this._institutionNameTemp = this._controllerInstitutionName.text;
    this._controllerNPWP.text = data.npwp;
    this._npwpTemp = this._controllerNPWP.text;
    for (int i = 0; i < data.listAddressGuarantorModel.length; i++) {
      if (data.listAddressGuarantorModel[i].isCorrespondence) {
        this._selectedIndex = i;
        this._oldSelectedIndex = i;
        this._controllerAddress.text =
            data.listAddressGuarantorModel[i].address;
        this._addressTemp = this._controllerAddress.text;
        this._controllerAddressType.text =
            data.listAddressGuarantorModel[i].jenisAlamatModel.DESKRIPSI;
        this._addressTypeTemp = this._controllerAddressType.text;
        this._controllerRT.text = data.listAddressGuarantorModel[i].rt;
        this._rtTemp = this._controllerRT.text;
        this._controllerRW.text = data.listAddressGuarantorModel[i].rw;
        this._rwTemp = this._controllerRW.text;
        this._controllerKelurahan.text =
            data.listAddressGuarantorModel[i].kelurahanModel.KEL_NAME;
        this._kelurahanTemp = this._controllerKelurahan.text;
        this._controllerKecamatan.text =
            data.listAddressGuarantorModel[i].kelurahanModel.KEC_NAME;
        this._kecamatanTemp = this._controllerKecamatan.text;
        this._controllerKota.text =
            data.listAddressGuarantorModel[i].kelurahanModel.KABKOT_NAME;
        this._kotaTemp = this._controllerKota.text;
        this._controllerProvinsi.text =
            data.listAddressGuarantorModel[i].kelurahanModel.PROV_NAME;
        this._provTemp = this._controllerProvinsi.text;

        this._controllerTelephoneArea1.text =
            data.listAddressGuarantorModel[i].phoneArea1;
        this._phoneArea1Temp = this._controllerTelephoneArea1.text;

        this._controllerTelephone1.text =
            data.listAddressGuarantorModel[i].phone1;
        this._phone1Temp = this._controllerTelephone1.text;

        this._controllerTelephoneArea2.text =
            data.listAddressGuarantorModel[i].phoneArea2;
        this._phoneArea2Temp = this._controllerTelephoneArea2.text;

        this._controllerTelephone2.text =
            data.listAddressGuarantorModel[i].phone2;
        this._phone2Temp = this._controllerTelephone2.text;

        this._controllerFaxArea.text =
            data.listAddressGuarantorModel[i].faxArea;
        this._faxAreaTemp = this._controllerFaxArea.text;

        this._controllerFax.text = data.listAddressGuarantorModel[i].faxArea;
        this._faxTemp = this._controllerFax.text;

        this._controllerPostalCode.text =
            data.listAddressGuarantorModel[i].kelurahanModel.ZIPCODE;
        this._postalCodeTemp = this._controllerPostalCode.text;
      }
    }
    for (int i = 0; i < data.listAddressGuarantorModel.length; i++) {
      this._listGuarantorAddress.add(data.listAddressGuarantorModel[i]);
    }
    this._sizeList = this._listGuarantorAddress.length;
  }

  void clearData() {
    this._listGuarantorAddress.clear();
    this._typeInstitutionSelected = null;
    this._profilSelected = null;
    this._controllerInstitutionName.clear();
    this._controllerNPWP.clear();
    this._controllerAddress.clear();
    this._controllerAddressType.clear();
    this._controllerRT.clear();
    this._controllerRW.clear();
    this._controllerKelurahan.clear();
    this._controllerKecamatan.clear();
    this._controllerKota.clear();
    this._controllerProvinsi.clear();
    this._controllerPostalCode.clear();
    this._controllerTelephoneArea1.clear();
    this._controllerTelephone1.clear();
    this._controllerTelephoneArea2.clear();
    this._controllerTelephone2.clear();
    this._controllerFaxArea.clear();
    this._controllerFaxArea.clear();
  }

  get faxTemp => _faxTemp;

  get faxAreaTemp => _faxAreaTemp;

  get phone2Temp => _phone2Temp;

  get phoneArea2Temp => _phoneArea2Temp;

  get phone1Temp => _phone1Temp;

  get phoneArea1Temp => _phoneArea1Temp;

  get provTemp => _provTemp;

  get kotaTemp => _kotaTemp;

  get kecamatanTemp => _kecamatanTemp;

  get kelurahanTemp => _kelurahanTemp;

  get rwTemp => _rwTemp;

  get rtTemp => _rtTemp;

  get addressTypeTemp => _addressTypeTemp;

  get addressTemp => _addressTemp;

  get npwpTemp => _npwpTemp;

  get postalCodeTemp => _postalCodeTemp;

  String get institutionNameTemp => _institutionNameTemp;

  int get oldSelectedIndex => _oldSelectedIndex;

  int get sizeList => _sizeList;

  void iconShowDialog(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans"
            ),
            child: AlertDialog(
              title: Text("Information", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(
                    "∙ Tekan 1x untuk edit",
                  ),
                  Text(
                    "∙ Tekan lama untuk memilih alamat korespondensi",
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height/37,),
                  Text("* Tekan icon pada pojok kanan atas untuk melihat kembali",style: TextStyle(fontSize: 12, color: Colors.grey))
                ],
              ),
              actions: <Widget>[
                FlatButton(
                    onPressed: (){
                      Navigator.pop(context);
                      // _updateStatusShowDialogSimilarity();
                    },
                    child: Text(
                        "CLOSE",
                        style: TextStyle(
                            color: primaryOrange,
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 1.25
                        )
                    )
                )
              ],
            ),
          );
        }
    );
  }

  void isShowDialog(BuildContext context) {
    if(this.listGuarantorAddress.length == 1) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context){
            return Theme(
              data: ThemeData(
                  fontFamily: "NunitoSans"
              ),
              child: AlertDialog(
                title: Text("Information", style: TextStyle(fontWeight: FontWeight.bold)),
                content: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      "∙ Tekan 1x untuk edit",
                    ),
                    Text(
                      "∙ Tekan lama untuk memilih alamat korespondensi",
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height/37,),
                    Text("* Tekan icon pada pojok kanan atas untuk melihat kembali",style: TextStyle(fontSize: 12, color: Colors.grey))
                  ],
                ),
                actions: <Widget>[
                  FlatButton(
                      onPressed: (){
                        Navigator.pop(context);
                        // _updateStatusShowDialogSimilarity();
                      },
                      child: Text(
                          "CLOSE",
                          style: TextStyle(
                              color: primaryOrange,
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 1.25
                          )
                      )
                  )
                ],
              ),
            );
          }
      );
    }
  }

  void moreDialog(BuildContext context, index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans"
            ),
            child: AlertDialog(
              title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  FlatButton(
                      onPressed: () {
                        selectedIndex = index;
                        _controllerAddress.clear();
                        setCorrespondenceAddress(listGuarantorAddress[index], index);
                        Navigator.pop(context);
                        Navigator.pop(context);
                      },
                      child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              Icons.check,
                              color: Colors.green,
                              size: 22.0,
                            ),
                            SizedBox(
                              width: 12.0,
                            ),
                            Text(
                              "Pilih sebagai Alamat Korespondensi",
                              style: TextStyle(fontSize: 14.0),
                            )
                          ]
                      )
                  ),
                  listGuarantorAddress[index].jenisAlamatModel.KODE != "03"
                      ? listGuarantorAddress[index].isSameWithIdentity
                      ? SizedBox()
                      : FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                        deleteListGuarantorAddress(context, index);
                      },
                      child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              Icons.delete,
                              color: Colors.red,
                              size: 22.0,
                            ),
                            SizedBox(
                              width: 12.0,
                            ),
                            Text(
                              "Hapus",
                              style: TextStyle(fontSize: 14.0, color: Colors.red),
                            )
                          ]
                      )
                  )
                      : SizedBox(),
                ],
              ),
              actions: <Widget>[
                FlatButton(
                    onPressed: (){
                      Navigator.pop(context);
                      // _updateStatusShowDialogSimilarity();
                    },
                    child: Text(
                        "CLOSE",
                        style: TextStyle(
                            color: primaryOrange,
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 1.25
                        )
                    )
                )
              ],
            ),
          );
        }
    );
  }
}
