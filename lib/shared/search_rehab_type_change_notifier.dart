import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/main.dart';
import 'package:ad1ms2_dev/models/rehab_type_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';

class SearchRehabTypeChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  TextEditingController _controllerSearch = TextEditingController();
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  DbHelper _dbHelper = DbHelper();
  List<RehabTypeModel> _listRehabTYpe = [
//    RehabTypeModel("001", "CTO"),
//    RehabTypeModel("002", "CTO NDS - EKSTERNAL"),
//    RehabTypeModel("003", "DCIP"),
//    RehabTypeModel("004", "CTO NDS - INTERNAL"),
//    RehabTypeModel("005", "REFINANCING - NDS"),
//    RehabTypeModel("006", "NETT OFF - NDS"),
//    RehabTypeModel("007", "CREDIT LIMIT"),
  ];

  List<RehabTypeModel> _listRehabTYpeTemp = [];
  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<RehabTypeModel> get listRehabTYpe {
    return UnmodifiableListView(this._listRehabTYpe);
  }

  UnmodifiableListView<RehabTypeModel> get listRehabTYpeTemp {
    return UnmodifiableListView(this._listRehabTYpeTemp);
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  void getRehabType(BuildContext context) async{
    this._listRehabTYpe.clear();
    loadData = true;
    List _dataCL = await _dbHelper.selectMS2LME();
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    var storage = FlutterSecureStorage();
    String _fieldJenisRehab = await storage.read(key: "FieldJenisRehab");
    final _response = await _http.get(
      "${BaseUrl.urlGeneral}$_fieldJenisRehab",
      //   "${urlPublic}jenis-rehab/get_jenis_rehab",
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );
    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      if(_result.isEmpty){
        showSnackBar("Jenis pihak ketiga tidak ditemukan");
        loadData = false;
      }
      else{
        for(int i=0; i <_result['data'].length; i++){
          if(_dataCL.isNotEmpty) {
            if(_dataCL[0]['opsi_multidisburse'] == "2") {
              if(_result['data'][i]['kode'] == "006" || _result['data'][i]['kode'] == "007") {
                this._listRehabTYpe.add(
                    RehabTypeModel(_result['data'][i]['kode'], _result['data'][i]['deskripsi'])
                );
              }
            } else {
              this._listRehabTYpe.add(
                  RehabTypeModel(_result['data'][i]['kode'], _result['data'][i]['deskripsi'])
              );
            }
          } else {
            this._listRehabTYpe.add(
                RehabTypeModel(_result['data'][i]['kode'], _result['data'][i]['deskripsi'])
            );
          }
        }
        loadData = false;
      }
    }
    else{
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void searchRehabType(String query) {
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    } else {
      _listRehabTYpeTemp.clear();
      if (query.isEmpty) {
        return;
      }

      _listRehabTYpe.forEach((dataSourceOrder) {
        if (dataSourceOrder.id.contains(query) || dataSourceOrder.name.contains(query)) {
          _listRehabTYpeTemp.add(dataSourceOrder);
        }
      });
    }
    notifyListeners();
  }

  void clearSearchTemp() {
    _listRehabTYpeTemp.clear();
  }
}
