import 'package:ad1ms2_dev/models/info_document_type_model.dart';

class DocumentUnitModel{
  final InfoDocumentTypeModel jenisDocument;
  final String orderSupportingDocumentID;
  final DateTime dateTime;
  final Map fileDocumentUnitObject;
  final String path;
  final double latitude;
  final double longitude;
  final String symbolName;
  final String fileHeaderID;
  final bool isEdit;
  final bool isEditJenisDocument;
  final bool isEditDateTime;
  final bool isEditFileDocumentUnitObject;


  DocumentUnitModel(this.jenisDocument, this.orderSupportingDocumentID, this.dateTime, this.fileDocumentUnitObject,
      this.path, this.latitude, this.longitude, this.symbolName, this.fileHeaderID,
      this.isEdit, this.isEditJenisDocument, this.isEditDateTime, this.isEditFileDocumentUnitObject);
}

class JenisDocument{
  final String id;
  final String name;

  JenisDocument(this.id, this.name);
}