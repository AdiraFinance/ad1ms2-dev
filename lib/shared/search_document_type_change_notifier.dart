import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/models/info_document_type_model.dart';
import 'package:ad1ms2_dev/models/reference_number_model.dart';
import 'package:ad1ms2_dev/shared/change_notifier_app/information_object_unit_change_notifier.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';
import 'change_notifier_app/info_app_document_change_notifier.dart';

var storage = FlutterSecureStorage();
class SearchDocumentTypeChangeNotifier with ChangeNotifier {
  bool _showClear = false;
  bool _loadData = false;
  TextEditingController _controllerSearch = TextEditingController();
  List<InfoDocumentTypeModel> _listDocumentType = [];
  List<InfoDocumentTypeModel> _listDocumentTypeTemp = [];
  List<InfoDocumentTypeModel> _listDocumentTypeFotoTemp = [];
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<InfoDocumentTypeModel> get listDocumentType {
    return UnmodifiableListView(this._listDocumentType);
  }

  UnmodifiableListView<InfoDocumentTypeModel> get listDocumentTypeTemp {
    return UnmodifiableListView(this._listDocumentTypeTemp);
  }

  UnmodifiableListView<InfoDocumentTypeModel> get listDocumentTypeFotoTemp {
    return UnmodifiableListView(this._listDocumentTypeFotoTemp);
  }

  // void getDocumentTypeFoto(BuildContext context) async{
  //   // var _providerListOid = Provider.of<ListOidChangeNotifier>(context, listen: false);
  //   var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
  //   SharedPreferences _preference = await SharedPreferences.getInstance();
  //   this._listDocumentType.clear();
  //   loadData = true;
  //   final ioc = new HttpClient();
  //   ioc.badCertificateCallback =
  //       (X509Certificate cert, String host, int port) => true;
  //
  //
  // }

  void distinctListDocumentType() {
    _listDocumentType.clear();
    var _uniqueKode = _listDocumentTypeFotoTemp.map((e) => e.docTypeId.trim()).toSet().toList();
    var _uniqueDescription = _listDocumentTypeFotoTemp.map((e) => e.docTypeName.trim()).toSet().toList();
    // var _uniqueMandatory = _listDocumentTypeFotoTemp.map((e) => e.mandatory).toSet().toList();
    // var _uniqueDisplay = _listDocumentTypeFotoTemp.map((e) => e.display).toSet().toList();
    // var _uniqueFlaUnit = _listDocumentTypeFotoTemp.map((e) => e.flag_unit).toSet().toList();

    for(var i=0; i< _uniqueKode.length; i++){
      InfoDocumentTypeModel _myData = InfoDocumentTypeModel(_uniqueKode[i], _uniqueDescription[i], null, null, null);
      this._listDocumentType.add(_myData);
    }
    notifyListeners();
  }

  void getDocumentType(BuildContext context,int flag) async{
    // var _providerListOid = Provider.of<ListOidChangeNotifier>(context, listen: false);
    var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
    var _providerUnit = Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false);
    SharedPreferences _preference = await SharedPreferences.getInstance();
    String _fieldTipeDokumen = await storage.read(key: "FieldTipeDokumen");
    this._listDocumentType.clear();
    loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);
    print("${BaseUrl.urlGeneral}$_fieldTipeDokumen");
    if(flag == 1){
      // form m - step rincian foto
      // var _objectUnit = [];
      // var _firstData = "";
      // if(_providerFoto.listGroupUnitObject.isNotEmpty) {
      //   for(int i=0; i < _providerFoto.listGroupUnitObject.length; i++) {
      //     if(_firstData == "") {
      //       // pertama (cuman 1x pakai)
      //       _objectUnit.add(_providerFoto.listGroupUnitObject[i].groupObjectUnit.id);
      //     }
      //     if(_firstData != "" && _firstData != _providerFoto.listGroupUnitObject[i].groupObjectUnit.id) {
      //       _objectUnit.add(_providerFoto.listGroupUnitObject[i].groupObjectUnit.id);
      //     }
      //     _firstData = _providerFoto.listGroupUnitObject[i].groupObjectUnit.id;
      //   }
      // }
      // var _body;
      // if(_objectUnit.isNotEmpty) {
      //   for(int i=0; i < _objectUnit.length; i++) {
      //     _listDocumentTypeFotoTemp.clear();
      //     _body = jsonEncode({
      //       "refOne": _objectUnit[i] == "007" ? "" : _objectUnit[i],
      //       "refTwo": "099",
      //       "refThree": _preference.getString("cust_type"),
      //       "refFour": _preference.getString("cust_type") == "PER" ? _providerFoto.kegiatanUsahaSelected.id.toString() : null,
      //       "refFive": _preference.getString("cust_type") == "PER" ? _providerFoto.jenisKegiatanUsahaSelected.id.toString() : null,
      //     });
      //     print("body jenis dokumen $_body");
      //
      //     final _response = await _http.post(
      //         "${BaseUrl.urlGeneral}$_fieldTipeDokumen",
      //         // "${urlPublic}proses-dokumen/get_list_dokumen",
      //         body: _body,
      //         headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      //     );
      //     if(_response.statusCode == 200){
      //       final _result = jsonDecode(_response.body);
      //       final _data = _result;
      //       print("result jenis dokumen = $_data");
      //       if(_data.isNotEmpty){
      //         for(int i = 0; i < _data.length; i++){
      //           _listDocumentTypeFotoTemp.add(InfoDocumentTypeModel(
      //               _data[i]['docTypeId'],
      //               _data[i]['docTypeName'],
      //               _data[i]['mandatory'],
      //               _data[i]['display'],
      //               _data[i]['flag_unit']));
      //         }
      //         loadData = false;
      //       }
      //       else{
      //         showSnackBar("Tipe dokumen tidak ditemukan");
      //         loadData = false;
      //       }
      //     }
      //     else{
      //       showSnackBar("Error get document type response ${_response.statusCode}");
      //       loadData = false;
      //     }
      //   }
      //   distinctListDocumentType();
      // }
      // else {
      //   _listDocumentTypeFotoTemp.clear();

      var _providerDokumen = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
      var _body = jsonEncode({
          "refOne": "",
          "refTwo": "099",
          "refThree": _preference.getString("cust_type"),
          "refFour": _preference.getString("cust_type") == "PER" ? _providerFoto.kegiatanUsahaSelected.id : null,
          "refFive": _preference.getString("cust_type") == "PER" ? _providerFoto.jenisKegiatanUsahaSelected.id : null,
        });
        // print("body flag 1 $_body");

        final _response = await _http.post(
            "${BaseUrl.urlGeneral}$_fieldTipeDokumen",
            // "${urlPublic}proses-dokumen/get_list_dokumen",
            body: _body,
            headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
        );
        if(_response.statusCode == 200){
          final _result = jsonDecode(_response.body);
          final _data = _result;
          if(_data.isNotEmpty){
            for(int i = 0; i < _data.length; i++){
              this._listDocumentType.add(InfoDocumentTypeModel(
                  _data[i]['docTypeId'],
                  _data[i]['docTypeName'],
                  _data[i]['mandatory'],
                  _data[i]['display'],
                  _data[i]['flag_unit']));
            }
            //clear identical type
            for(int i=0; i<_providerDokumen.listDocument.length; i++){
              for(int j=0; j<_listDocumentType.length; j++){
                if(_listDocumentType[j].docTypeId == _providerDokumen.listDocument[i].jenisDocument.docTypeId){
                  _listDocumentType.removeAt(j);
                }
              }
            }
            var _providerInfoDokumen = Provider.of<InfoDocumentChangeNotifier>(context, listen: false);
            for(int i=0; i<_providerInfoDokumen.listInfoDocument.length; i++){
              for(int j=0; j<_listDocumentType.length; j++){
                if(_listDocumentType[j].docTypeId == _providerInfoDokumen.listInfoDocument[i].documentType.docTypeId){
                  _listDocumentType.removeAt(j);
                }
              }
            }
            loadData = false;
          }
          else{
            showSnackBar("Tipe dokumen tidak ditemukan");
            loadData = false;
          }
        }
        else{
          showSnackBar("Error get document type response ${_response.statusCode}");
          loadData = false;
        }
        // distinctListDocumentType();
      // }
    }
    else{
      // form app - step inf dokumen
      var _providerDokumen = Provider.of<InfoDocumentChangeNotifier>(context, listen: false);
      var _body = jsonEncode({
        "refOne": _providerUnit.objectSelected != null ? _providerUnit.objectSelected.id : "",
        "refTwo": "099",
        "refThree": _preference.getString("cust_type"),
        "refFour": _preference.getString("cust_type") == "PER" ? _providerFoto.kegiatanUsahaSelected.id : Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).businessActivitiesModelSelected.id,
        "refFive": _preference.getString("cust_type") == "PER" ? _providerFoto.jenisKegiatanUsahaSelected.id : Provider.of<InformationObjectUnitChangeNotifier>(context, listen: false).businessActivitiesTypeModelSelected.id,
      });

      final _response = await _http.post(
        "${BaseUrl.urlGeneral}$_fieldTipeDokumen",
          // "${urlPublic}proses-dokumen/get_list_dokumen",
          body: _body,
          headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
      );

      List<InfoDocumentTypeModel> _listTemp = [];
      _listTemp.clear();

      if(_response.statusCode == 200){
        final _result = jsonDecode(_response.body);
        final _data = _result;
        if(_data.isNotEmpty){
          for(int i = 0; i < _data.length; i++){
            _listDocumentType.add(InfoDocumentTypeModel(
                _data[i]['docTypeId'],
                _data[i]['docTypeName'],
                _data[i]['mandatory'],
                _data[i]['display'],
                _data[i]['flag_unit']));
          }
          //clear identical type
          for(int i=0; i<_providerDokumen.listInfoDocument.length; i++){
            for(int j=0; j<_listDocumentType.length; j++){
              if(_listDocumentType[j].docTypeId == _providerDokumen.listInfoDocument[i].documentType.docTypeId){
                _listDocumentType.removeAt(j);
              }
            }
          }
          var _providerFoto = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
          for(int i=0; i<_providerFoto.listDocument.length; i++){
            for(int j=0; j<_listDocumentType.length; j++){
              if(_listDocumentType[j].docTypeId == _providerFoto.listDocument[i].jenisDocument.docTypeId){
                _listDocumentType.removeAt(j);
              }
            }
          }
          loadData = false;
        }
        else{
          showSnackBar("Jenis tidak ditemukan");
          loadData = false;
        }
      }
      else{
        showSnackBar("Error get document type response ${_response.statusCode}");
        loadData = false;
      }
    }
    notifyListeners();
  }

  void searchDocumentType(String query) {
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    } else {
      _listDocumentTypeTemp.clear();
      if (query.isEmpty) {
        return;
      }

      _listDocumentType.forEach((dataDcoumentType) {
        if (dataDcoumentType.docTypeId.contains(query) || dataDcoumentType.docTypeName.contains(query)) {
          _listDocumentTypeTemp.add(dataDcoumentType);
        }
      });
    }
    notifyListeners();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }

  void clearSearchTemp() {
    _listDocumentTypeTemp.clear();
  }
}
