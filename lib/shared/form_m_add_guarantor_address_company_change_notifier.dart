import 'dart:collection';

import 'package:ad1ms2_dev/models/address_guarantor_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/screens/form_m/search_kelurahan.dart';
import 'package:ad1ms2_dev/shared/form_m_add_guarantor_company_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_kelurahan_change_notif.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';


//tidak dipakai
class FormMAddGuarantorAddressCompanyChangeNotifier with ChangeNotifier {
  bool _autoValidate = false;
  bool _isSameWithIdentity = false;
  bool _isCorrespondence = false;
  TextEditingController _controllerAlamat = TextEditingController();
  TextEditingController _controllerRT = TextEditingController();
  TextEditingController _controllerRW = TextEditingController();
  TextEditingController _controllerKelurahan = TextEditingController();
  TextEditingController _controllerKecamatan = TextEditingController();
  TextEditingController _controllerKota = TextEditingController();
  TextEditingController _controllerProvinsi = TextEditingController();
  TextEditingController _controllerTelephone1Area = TextEditingController();
  TextEditingController _controllerTelephone1 = TextEditingController();
  TextEditingController _controllerTelephone2Area = TextEditingController();
  TextEditingController _controllerTelephone2 = TextEditingController();
  TextEditingController _controllerFaxArea = TextEditingController();
  TextEditingController _controllerFax = TextEditingController();
  TextEditingController _controllerPostalCode = TextEditingController();
  JenisAlamatModel _jenisAlamatSelected;
  JenisAlamatModel _jenisAlamatSelectedTemp;
  KelurahanModel _kelurahanSelected;
  KelurahanModel _kelurahanSelectedTemp;
  AddressModelCompany _addressModelTemp;
  String _alamatTemp,
      _rtTemp,
      _rwTemp,
      _kelurahanTemp,
      _kecamatanTemp,
      _kotaTemp,
      _provinsiTemp,
      _phone1AreaTemp,
      _phone1Temp,
      _phone2AreaTemp,
      _phone2Temp,
      _faxAreaTemp,
      _faxTemp,
      _postalCodeTemp;
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  bool _enableTfAddress = true;
  bool _enableTfRT = true;
  bool _enableTfRW = true;
  bool _enableTfKelurahan = true;
  bool _enableTfKecamatan = true;
  bool _enableTfKota = true;
  bool _enableTfProv = true;
  bool _enableTfPostalCode = true;
  bool _enableTfTelephone1Area = true;
  bool _enableTfTelephone2Area = true;
  bool _enableTfPhone1 = true;
  bool _enableTfPhone2 = true;
  bool _enableTfFaxArea = true;
  bool _enableTfFax = true;
  Map _dummy;

  List<JenisAlamatModel> _listJenisAlamat = [];

  void addDataListAddressType(BuildContext context, int index) {
    var _provider = Provider.of<FormMAddGuarantorCompanyChangeNotifier>(context,
        listen: false);
    if (index == null) {
      if (_provider.listGuarantorAddress.isEmpty) {
        _listJenisAlamat.add(JenisAlamatModel("01", "Identitas"));
      } else {
//        bool _identitasIsExist = false;
//        for (int i = 0; i < _provider.listOccupationAddress.length; i++) {
//          if (_provider.listOccupationAddress[i].jenisAlamatModel.id == "01") {
//            _identitasIsExist = true;
//          }
//        }
//        if (_identitasIsExist) {
        _listJenisAlamat = [
          JenisAlamatModel("02", "Domisili 1"),
          JenisAlamatModel("03", "Domisili 2"),
          JenisAlamatModel("04", "Domisili 3"),
          JenisAlamatModel("05", "Kantor 1"),
          JenisAlamatModel("05", "Kantor 2"),
          JenisAlamatModel("07", "Kantor 3")
        ];
//        } else {
//          _listJenisAlamat.add(JenisAlamatModel("01", "Identitas"));
//        }
      }
    } else {
      if (_provider.listGuarantorAddress[index].jenisAlamatModel.KODE == '01') {
        _listJenisAlamat.add(JenisAlamatModel("01", "Identitas"));
      } else {
        _listJenisAlamat = [
          JenisAlamatModel("02", "Domisili 1"),
          JenisAlamatModel("03", "Domisili 2"),
          JenisAlamatModel("04", "Domisili 3"),
          JenisAlamatModel("05", "Kantor 1"),
          JenisAlamatModel("05", "Kantor 2"),
          JenisAlamatModel("07", "Kantor 3")
        ];
      }
    }
  }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  TextEditingController get controllerPostalCode => _controllerPostalCode;

  TextEditingController get controllerTelephone1 => _controllerTelephone1;

  TextEditingController get controllerTelephone1Area =>
      _controllerTelephone1Area;

  TextEditingController get controllerProvinsi => _controllerProvinsi;

  TextEditingController get controllerKota => _controllerKota;

  TextEditingController get controllerKecamatan => _controllerKecamatan;

  TextEditingController get controllerKelurahan => _controllerKelurahan;

  TextEditingController get controllerRT => _controllerRT;

  TextEditingController get controllerRW => _controllerRW;

  TextEditingController get controllerAlamat => _controllerAlamat;

  TextEditingController get controllerTelephone2Area =>
      _controllerTelephone2Area;

  TextEditingController get controllerTelephone2 => _controllerTelephone2;

  TextEditingController get controllerFax => _controllerFax;

  TextEditingController get controllerFaxArea => _controllerFaxArea;

  JenisAlamatModel get jenisAlamatSelected => _jenisAlamatSelected;

  set jenisAlamatSelected(JenisAlamatModel value) {
    this._jenisAlamatSelected = value;
    notifyListeners();
  }

  KelurahanModel get kelurahanSelected => _kelurahanSelected;

  set kelurahanSelected(KelurahanModel value) {
    this._kelurahanSelected = value;
    notifyListeners();
  }

  UnmodifiableListView<JenisAlamatModel> get listJenisAlamat {
    return UnmodifiableListView(this._listJenisAlamat);
  }

  bool get isCorrespondence => _isCorrespondence;

  set isCorrespondence(bool value) {
    this._isCorrespondence = value;
    notifyListeners();
  }

  bool get isSameWithIdentity => _isSameWithIdentity;

  set isSameWithIdentity(bool value) {
    this._isSameWithIdentity = value;
    notifyListeners();
  }

  bool get enableTfPhone1 => _enableTfPhone1;

  set enableTfPhone1(bool value) {
    this._enableTfPhone1 = value;
  }

  bool get enableTfTelephone1Area => _enableTfTelephone1Area;

  set enableTfTelephone1Area(bool value) {
    this._enableTfTelephone1Area = value;
  }

  bool get enableTfTelephone2Area => _enableTfTelephone2Area;

  set enableTfTelephone2Area(bool value) {
    this._enableTfTelephone2Area = value;
  }

  bool get enableTfPhone2 => _enableTfPhone2;

  set enableTfPhone2(bool value) {
    this._enableTfPhone2 = value;
  }

  bool get enableTfFax => _enableTfFax;

  set enableTfFax(bool value) {
    this._enableTfFax = value;
  }

  bool get enableTfFaxArea => _enableTfFaxArea;

  set enableTfFaxArea(bool value) {
    this._enableTfFaxArea = value;
  }

  bool get enableTfPostalCode => _enableTfPostalCode;

  set enableTfPostalCode(bool value) {
    this._enableTfPostalCode = value;
  }

  bool get enableTfProv => _enableTfProv;

  set enableTfProv(bool value) {
    this._enableTfProv = value;
  }

  bool get enableTfKota => _enableTfKota;

  set enableTfKota(bool value) {
    this._enableTfKota = value;
  }

  bool get enableTfKecamatan => _enableTfKecamatan;

  set enableTfKecamatan(bool value) {
    this._enableTfKecamatan = value;
  }

  bool get enableTfKelurahan => _enableTfKelurahan;

  set enableTfKelurahan(bool value) {
    this._enableTfKelurahan = value;
  }

  bool get enableTfRW => _enableTfRW;

  set enableTfRW(bool value) {
    this._enableTfRW = value;
  }

  bool get enableTfRT => _enableTfRT;

  set enableTfRT(bool value) {
    this._enableTfRT = value;
  }

  bool get enableTfAddress => _enableTfAddress;

  set enableTfAddress(bool value) {
    this._enableTfAddress = value;
  }

  void setValueIsSameWithIdentity(
      bool value, BuildContext context, AddressModelCompany model) {
    var _provider = Provider.of<FormMAddGuarantorCompanyChangeNotifier>(context,
        listen: false);
    AddressModelCompany data;
    if (_addressModelTemp == null) {
      if (value) {
        for (int i = 0; i < _provider.listGuarantorAddress.length; i++) {
          if (_provider.listGuarantorAddress[i].jenisAlamatModel.KODE == "01") {
            data = _provider.listGuarantorAddress[i];
          }
        }
        if (model != null) {
          for (int i = 0; i < _listJenisAlamat.length; i++) {
            if (model.jenisAlamatModel.KODE == _listJenisAlamat[i].KODE) {
              this._jenisAlamatSelected = _listJenisAlamat[i];
              this._jenisAlamatSelectedTemp = this._jenisAlamatSelected;
              this._isSameWithIdentity = value;
            }
          }
        }
        this._controllerAlamat.text = data.address;
        this._controllerRT.text = data.rt;
        this._controllerRW.text = data.rw;
        this._kelurahanSelected = data.kelurahanModel;
        this._controllerAlamat.text = data.address;
        this._alamatTemp = this._controllerAlamat.text;
        this._controllerRT.text = data.rt;
        this._rtTemp = this._controllerRT.text;
        this._controllerRW.text = data.rw;
        this._rwTemp = this._controllerRW.text;
        this._controllerKelurahan.text = data.kelurahanModel.KEL_NAME;
        this._kelurahanTemp = this._controllerKelurahan.text;
        this._controllerKecamatan.text = data.kelurahanModel.KEC_NAME;
        this._kecamatanTemp = this._controllerKecamatan.text;
        this._controllerKota.text = data.kelurahanModel.KABKOT_NAME;
        this._kotaTemp = this._controllerKota.text;
        this._controllerProvinsi.text = data.kelurahanModel.PROV_NAME;
        this._provinsiTemp = this._controllerProvinsi.text;

        this._controllerTelephone1Area.text = data.phoneArea1;
        this._phone1AreaTemp = this._controllerTelephone1Area.text;

        this._controllerTelephone1.text = data.phone1;
        this._phone1Temp = this._controllerTelephone1.text;

        this._controllerTelephone2Area.text = data.phoneArea2;
        this._phone2AreaTemp = this._controllerTelephone2Area.text;

        this._controllerTelephone2.text = data.phone2;
        this._phone2Temp = this._controllerTelephone2.text;

        this._controllerFaxArea.text = data.faxArea;
        this._faxAreaTemp = this._controllerFaxArea.text;

        this._controllerFax.text = data.fax;
        this._faxTemp = this._controllerFax.text;

        this._controllerPostalCode.text = data.kelurahanModel.ZIPCODE;
        this._postalCodeTemp = this._controllerPostalCode.text;
        enableTfAddress = !value;
        enableTfRT = !value;
        enableTfRW = !value;
        enableTfKelurahan = !value;
        enableTfKecamatan = !value;
        enableTfKota = !value;
        enableTfProv = !value;
        enableTfPostalCode = !value;
        enableTfTelephone1Area = !value;
        enableTfPhone1 = !value;
        enableTfTelephone2Area = !value;
        enableTfPhone2 = !value;
        enableTfFaxArea = !value;
        enableTfFax = !value;
      } else {
        this._controllerAlamat.clear();
        this._controllerRT.clear();
        this._controllerRW.clear();
        this._kelurahanSelected = null;
        this._controllerAlamat.clear();
//        this._alamatTemp = "";
        this._controllerRT.clear();
//        this._rtTemp = "";
        this._controllerRW.clear();
//        this._rwTemp = "";
        this._controllerKelurahan.clear();
//        this._kelurahanTemp = "";
        this._controllerKecamatan.clear();
//        this._kecamatanTemp = "";
        this._controllerKota.clear();
//        this._kotaTemp = "";
        this._controllerProvinsi.clear();
//        this._provinsiTemp = "";
        this._controllerTelephone1Area.clear();
//        this._areaCodeTemp = "";
        this._controllerTelephone1.clear();
//        this._phoneTemp = "";
        this._controllerPostalCode.clear();
//        this._postalCodeTemp = "";
        this._controllerTelephone1Area.clear();
        this._controllerTelephone1.clear();

        this._controllerTelephone2Area.clear();
        this._controllerTelephone2.clear();

        this._controllerFaxArea.clear();
        this._controllerFax.clear();

        enableTfAddress = !value;
        enableTfRT = !value;
        enableTfRW = !value;
        enableTfKelurahan = !value;
        enableTfKecamatan = !value;
        enableTfKota = !value;
        enableTfProv = !value;
        enableTfPostalCode = !value;
        enableTfTelephone1Area = !value;
        enableTfPhone1 = !value;
        enableTfTelephone2Area = !value;
        enableTfPhone2 = !value;
        enableTfFaxArea = !value;
        enableTfFax = !value;
      }
    } else {
      if (value) {
        for (int i = 0; i < _provider.listGuarantorAddress.length; i++) {
          if (_provider.listGuarantorAddress[i].jenisAlamatModel.KODE == "01") {
            data = _provider.listGuarantorAddress[i];
          }
        }
        this._controllerAlamat.text = data.address;
        this._controllerRT.text = data.rt;
        this._controllerRW.text = data.rw;
        this._kelurahanSelected = data.kelurahanModel;
        this._controllerAlamat.text = data.address;
//      this._alamatTemp = this._controllerAlamat.text;
        this._controllerRT.text = data.rt;
//      this._rtTemp = this._controllerRT.text;
        this._controllerRW.text = data.rw;
//      this._rwTemp = this._controllerRW.text;
        this._controllerKelurahan.text = data.kelurahanModel.KEL_NAME;
//      this._kelurahanTemp = this._controllerKelurahan.text;
        this._controllerKecamatan.text = data.kelurahanModel.KEC_NAME;
//      this._kecamatanTemp = this._controllerKecamatan.text;
        this._controllerKota.text = data.kelurahanModel.KABKOT_NAME;
//      this._kotaTemp = this._controllerKota.text;
        this._controllerProvinsi.text = data.kelurahanModel.PROV_NAME;
//      this._provinsiTemp = this._controllerProvinsi.text;
        this._controllerTelephone1Area.text = data.phoneArea1;
//      this._areaCodeTemp = this._controllerKodeArea.text;
        this._controllerTelephone1.text = data.phone1;
//      this._phoneTemp = this._controllerTlpn.text;

        this._controllerTelephone2Area.text = data.phoneArea2;
        this._controllerTelephone2.text = data.phone2;

        this._controllerFaxArea.text = data.faxArea;
        this._controllerFax.text = data.fax;

        this._controllerPostalCode.text = data.kelurahanModel.ZIPCODE;
//      this._postalCodeTemp = this._controllerPostalCode.text;
        enableTfAddress = !value;
        enableTfRT = !value;
        enableTfRW = !value;
        enableTfKelurahan = !value;
        enableTfKecamatan = !value;
        enableTfKota = !value;
        enableTfProv = !value;
        enableTfPostalCode = !value;
        enableTfTelephone1Area = !value;
        enableTfPhone1 = !value;
        enableTfTelephone2Area = !value;
        enableTfPhone2 = !value;
        enableTfFaxArea = !value;
        enableTfFax = !value;
      } else {
        this._controllerAlamat.clear();
        this._controllerRT.clear();
        this._controllerRW.clear();
        this._kelurahanSelected = null;
        this._controllerAlamat.clear();
//        this._alamatTemp = "";
        this._controllerRT.clear();
//        this._rtTemp = "";
        this._controllerRW.clear();
//        this._rwTemp = "";
        this._controllerKelurahan.clear();
//        this._kelurahanTemp = "";
        this._controllerKecamatan.clear();
//        this._kecamatanTemp = "";
        this._controllerKota.clear();
//        this._kotaTemp = "";
        this._controllerProvinsi.clear();
//        this._provinsiTemp = "";
        this._controllerTelephone1Area.clear();
//        this._areaCodeTemp = "";
        this._controllerTelephone1.clear();
//        this._phoneTemp = "";
        this._controllerPostalCode.clear();
//        this._postalCodeTemp = "";
        this._controllerTelephone1Area.clear();
        this._controllerTelephone1.clear();

        this._controllerTelephone2Area.clear();
        this._controllerTelephone2.clear();

        this._controllerFaxArea.clear();
        this._controllerFax.clear();

        enableTfAddress = !value;
        enableTfRT = !value;
        enableTfRW = !value;
        enableTfKelurahan = !value;
        enableTfKecamatan = !value;
        enableTfKota = !value;
        enableTfProv = !value;
        enableTfPostalCode = !value;
        enableTfTelephone1Area = !value;
        enableTfPhone1 = !value;
        enableTfTelephone2Area = !value;
        enableTfPhone2 = !value;
        enableTfFaxArea = !value;
        enableTfFax = !value;
      }
    }
  }

  Future<void> setValueForEdit(AddressModelCompany data,
      BuildContext context, int index, bool isSameWithIdentity) async {
    addDataListAddressType(context, index);
    if (isSameWithIdentity) {
      setValueIsSameWithIdentity(isSameWithIdentity, context, data);
    } else {
      _addressModelTemp = data;
      for (int i = 0; i < this._listJenisAlamat.length; i++) {
        if (data.jenisAlamatModel.KODE == this._listJenisAlamat[i].KODE) {
          this._jenisAlamatSelected = this._listJenisAlamat[i];
          this._jenisAlamatSelectedTemp = this._listJenisAlamat[i];
        }
      }
      this._kelurahanSelected = data.kelurahanModel;
      this._controllerAlamat.text = data.address;
      this._alamatTemp = this._controllerAlamat.text;
      this._controllerRT.text = data.rt;
      this._rtTemp = this._controllerRT.text;
      this._controllerRW.text = data.rw;
      this._rwTemp = this._controllerRW.text;
      this._controllerKelurahan.text = data.kelurahanModel.KEL_NAME;
      this._kelurahanTemp = this._controllerKelurahan.text;
      this._controllerKecamatan.text = data.kelurahanModel.KEC_NAME;
      this._kecamatanTemp = this._controllerKecamatan.text;
      this._controllerKota.text = data.kelurahanModel.KABKOT_NAME;
      this._kotaTemp = this._controllerKota.text;
      this._controllerProvinsi.text = data.kelurahanModel.PROV_NAME;
      this._provinsiTemp = this._controllerProvinsi.text;
      this._controllerTelephone1Area.text = data.phoneArea1;
      this._phone1AreaTemp = this._controllerTelephone1Area.text;
      this._controllerTelephone1.text = data.phone1;
      this._phone1Temp = this._controllerTelephone1.text;
      this._controllerTelephone2Area.text = data.phoneArea2;
      this._phone2AreaTemp = this._controllerTelephone2Area.text;
      this._controllerTelephone2.text = data.phone2;
      this._phone2Temp = this._controllerTelephone2.text;
      this._controllerFaxArea.text = data.faxArea;
      this._faxAreaTemp = this._controllerFaxArea.text;
      this._controllerFax.text = data.fax;
      this._faxTemp = this._controllerFax.text;
      this._controllerPostalCode.text = data.kelurahanModel.ZIPCODE;
      this._postalCodeTemp = this._controllerPostalCode.text;
    }
  }

  void checkValidCodeArea1(String value) {
    if (value == "08") {
      Future.delayed(Duration(milliseconds: 100), () {
        this._controllerTelephone1Area.clear();
      });
    } else {
      return;
    }
  }

  void checkValidCodeArea2(String value) {
    if (value == "08") {
      Future.delayed(Duration(milliseconds: 100), () {
        this._controllerTelephone2Area.clear();
      });
    } else {
      return;
    }
  }

  void checkValidCodeAreaFax(String value) {
    if (value == "08") {
      Future.delayed(Duration(milliseconds: 100), () {
        this._controllerFaxArea.clear();
      });
    } else {
      return;
    }
  }

  void searchKelurahan(BuildContext context) async {
    KelurahanModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchKelurahanChangeNotif(),
                child: SearchKelurahan())));
    if (data != null) {
      kelurahanSelected = data;
      this._controllerKelurahan.text = data.KEL_NAME;
      this._controllerKecamatan.text = data.KEC_NAME;
      this._controllerKota.text = data.KABKOT_NAME;
      this._controllerProvinsi.text = data.PROV_NAME;
      this._controllerPostalCode.text = data.ZIPCODE;
      notifyListeners();
    } else {
      return;
    }
  }

  get phone2Temp => _phone2Temp;

  get faxAreaTemp => _faxAreaTemp;

  get faxTemp => _faxTemp;

  get postalCodeTemp => _postalCodeTemp;

  get phone1AreaTemp => _phone1AreaTemp;

  get provinsiTemp => _provinsiTemp;

  get kotaTemp => _kotaTemp;

  get kecamatanTemp => _kecamatanTemp;

  get kelurahanTemp => _kelurahanTemp;

  get rwTemp => _rwTemp;

  get rtTemp => _rtTemp;

  get phone1Temp => _phone1Temp;

  get phone2AreaTemp => _phone2AreaTemp;

  String get alamatTemp => _alamatTemp;

  JenisAlamatModel get jenisAlamatSelectedTemp => _jenisAlamatSelectedTemp;

  KelurahanModel get kelurahanSelectedTemp => _kelurahanSelectedTemp;

  GlobalKey<FormState> get key => _key;

  // void check(BuildContext context, int flag, int index) {
  //   final _form = _key.currentState;
  //   if (flag == 0) {
  //     if (_form.validate()) {
  //       Provider.of<FormMAddGuarantorCompanyChangeNotifier>(context,
  //               listen: true)
  //           .addGuarantorAddress(AddressGuarantorModelCompany(
  //               this._jenisAlamatSelected,
  //               this._kelurahanSelected,
  //               this._controllerAlamat.text,
  //               this._controllerRT.text,
  //               this._controllerRW.text,
  //               this._controllerTelephone1Area.text,
  //               this._controllerTelephone1.text,
  //               this._controllerTelephone2Area.text,
  //               this._controllerTelephone2.text,
  //               this._controllerFaxArea.text,
  //               this._controllerFax.text,
  //               this._isSameWithIdentity,
  //               _dummy,
  //               this._isCorrespondence));
  //
  //
  //       print("Batas awal");
  //       print(_kelurahanSelected.KEL_ID);
  //       print(_kelurahanSelected.KEL_NAME);
  //       print(_kelurahanSelected.KEC_NAME);
  //       print(_kelurahanSelected.KABKOT_NAME);
  //       print(_kelurahanSelected.PROV_NAME);
  //       print(_kelurahanSelected.ZIPCODE);
  //       print(_kelurahanSelected.KEC_ID);
  //       print(_kelurahanSelected.KABKOT_ID);
  //       print(_kelurahanSelected.PROV_ID);
  //       print("Batas akhir");
  //
  //       Navigator.pop(context);
  //     } else {
  //       autoValidate = true;
  //     }
  //   } else {
  //     if (_form.validate()) {
  //       Provider.of<FormMAddGuarantorCompanyChangeNotifier>(context,
  //               listen: true)
  //           .updateGuarantorAddress(
  //               AddressGuarantorModelCompany(
  //                   this._jenisAlamatSelected,
  //                   this._kelurahanSelected,
  //                   this._controllerAlamat.text,
  //                   this._controllerRT.text,
  //                   this._controllerRW.text,
  //                   this._controllerTelephone1Area.text,
  //                   this._controllerTelephone1.text,
  //                   this._controllerTelephone2Area.text,
  //                   this._controllerTelephone2.text,
  //                   this._controllerFaxArea.text,
  //                   this._controllerFax.text,
  //                   this._isSameWithIdentity,
  //                   _dummy,
  //                   this._isCorrespondence),
  //               index);
  //
  //
  //       print("Batas awal");
  //       print(_kelurahanSelected.KEL_ID);
  //       print(_kelurahanSelected.KEL_NAME);
  //       print(_kelurahanSelected.KEC_NAME);
  //       print(_kelurahanSelected.KABKOT_NAME);
  //       print(_kelurahanSelected.PROV_NAME);
  //       print(_kelurahanSelected.ZIPCODE);
  //       print(_kelurahanSelected.KEC_ID);
  //       print(_kelurahanSelected.KABKOT_ID);
  //       print(_kelurahanSelected.PROV_ID);
  //       print("Batas akhir");
  //
  //       Navigator.pop(context);
  //     } else {
  //       autoValidate = true;
  //     }
  //   }
  // }
}
