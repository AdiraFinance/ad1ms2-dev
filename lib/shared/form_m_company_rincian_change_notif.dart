import 'dart:collection';
import 'dart:ffi';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/form_m_company_rincian_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:ad1ms2_dev/models/ms2_customer_company_model.dart';
import 'package:ad1ms2_dev/models/ms2_customer_model.dart';
import 'package:ad1ms2_dev/screens/form_m/search_business_field.dart';
import 'package:ad1ms2_dev/screens/form_m/search_sector_economi.dart';
import 'package:ad1ms2_dev/screens/search_business_location.dart';
import 'package:ad1ms2_dev/screens/search_status_location.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/date_picker.dart';
import 'package:ad1ms2_dev/shared/list_oid_change_notifier.dart';
import 'package:ad1ms2_dev/shared/search_business_field_change_notif.dart';
import 'package:ad1ms2_dev/shared/search_business_location_change_notif.dart';
import 'package:ad1ms2_dev/shared/search_sector_economic_change_notif.dart';
import 'package:ad1ms2_dev/shared/search_status_location_field_change_notif.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';

class FormMCompanyRincianChangeNotifier with ChangeNotifier {
  bool _autoValidate = false;
  bool _loadDataDedup = false;
  TypeInstitutionModel _typeInstitutionSelected;
  ProfilModel _profilSelected;
  TextEditingController _controllerInstitutionName = TextEditingController();
  int _radioValueIsHaveNPWP = 1;
  TextEditingController _controllerNPWP = TextEditingController();
  TextEditingController _controllerFullNameNPWP = TextEditingController();
  JenisNPWPModel _typeNPWPSelected;
  SignPKPModel _signPKPSelected;
  TextEditingController _controllerNPWPAddress = TextEditingController();
  TextEditingController _controllerDateEstablishment = TextEditingController();
  DateTime _initialDateForDateEstablishment = DateTime(dateNow.year, dateNow.month, dateNow.day);
  bool _enableDataDedup = false;
  TextEditingController _controllerSectorEconomic = TextEditingController();
  SectorEconomicModel _sectorEconomicModelSelected;

  TextEditingController _controllerBusinessField = TextEditingController();
  BusinessFieldModel _businessFieldModelSelected;

  StatusLocationModel _locationStatusSelected;
  BusinessLocationModel _businessLocationSelected;
  TextEditingController _controllerStatusLocation = TextEditingController();
  TextEditingController _controllerBusinessLocation = TextEditingController();

  TextEditingController _controllerTotalEmployees = TextEditingController();
  TextEditingController _controllerLamaUsahaBerjalan = TextEditingController();
  TextEditingController _controllerTotalLamaUsahaBerjalan = TextEditingController();
  int _radioValueOpenAccount = 0;
  TextEditingController _controllerFormNumber = TextEditingController();
  String _customerCorporateID;
  String _custType;
  String _lastKnownState;
  String _obligorID;
  String _customerID;

  //bool untuk show hide field
  bool _isCompanyTypeShow = false;
  bool _isProfileShow = false;
  bool _isCompanyNameShow = false;
  bool _isHaveNPWPShow = false;
  bool _isNoNPWPShow = false;
  bool _isNameNPWPShow = false;
  bool _isTypeNPWPShow = false;
  bool _isPKPSignShow = false;
  bool _isAddressNPWPShow = false;
  bool _isEstablishDateShow = false;
  bool _isSectorEconomicShow = false;
  bool _isBusinessFieldShow = false;
  bool _isLocationStatusShow = false;
  bool _isBusinessLocationShow = false;
  bool _isTotalEmpShow = false;
  bool _isLamaUsahaShow = false;
  bool _isTotalLamaUsahaShow = false;

  //bool untuk mandatory
  bool _isCompanyTypeMandatory = false;
  bool _isProfileMandatory = false;
  bool _isCompanyNameMandatory = false;
  bool _isHaveNPWPMandatory = false;
  bool _isNoNPWPMandatory = false;
  bool _isNameNPWPMandatory = false;
  bool _isTypeNPWPMandatory = false;
  bool _isPKPSignMandatory = false;
  bool _isAddressNPWPMandatory = false;
  bool _isEstablishDateMandatory = false;
  bool _isSectorEconomicMandatory = false;
  bool _isBusinessFieldMandatory = false;
  bool _isLocationStatusMandatory = false;
  bool _isBusinessLocationMandatory = false;
  bool _isTotalEmpMandatory = false;
  bool _isLamaUsahaMandatory = false;
  bool _isTotalLamaUsahaMandatory = false;

  //bool untuk dakor
  bool _isCompanyTypeDakor = false;
  bool _isProfileDakor = false;
  bool _isCompanyNameDakor = false;
  bool _isHaveNPWPDakor = false;
  bool _isNoNPWPDakor = false;
  bool _isNameNPWPDakor = false;
  bool _isTypeNPWPDakor = false;
  bool _isPKPSignDakor = false;
  bool _isAddressNPWPDakor = false;
  bool _isEstablishDateDakor = false;
  bool _isSectorEconomicDakor = false;
  bool _isBusinessFieldDakor = false;
  bool _isLocationStatusDakor = false;
  bool _isBusinessLocationDakor = false;
  bool _isTotalEmpDakor = false;
  bool _isLamaUsahaDakor = false;
  bool _isTotalLamaUsahaDakor = false;

  List<TypeInstitutionModel> _listTypeInstitution = TypeInstitutionList().typeInstitutionItems;
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  bool _flag = false;
  bool _isEnableFieldCompanyName = true;
  bool _isEnableFieldNumberNPWP = true;
  bool _isEnableFieldEstablishDate = true;
  DbHelper _dbHelper = DbHelper();

  bool get enableDataDedup => _enableDataDedup;

  set enableDataDedup(bool value) {
    this._enableDataDedup = value;
    notifyListeners();
  }

  String get customerCorporateID => _customerCorporateID;

  set customerCorporateID(String value) {
    this._customerCorporateID = value;
    notifyListeners();
  }

  String get customerID => _customerID;

  set customerID(String value) {
    this._customerID = value;
    notifyListeners();
  }

  List<ProfilModel> _listProfil = [
    ProfilModel("01", "PT"),
    ProfilModel("02", "CV"),
    ProfilModel("03", "FOUNDATION"),
    ProfilModel("05", "KOPERASI"),
  ];

  List<JenisNPWPModel> _listTypeNPWP = [
    JenisNPWPModel("1", "BADAN USAHA"),
    JenisNPWPModel("2", "PERORANGAN"),
  ];

  List<SignPKPModel> _listSignPKP = [
    SignPKPModel("1", "PKP"),
    SignPKPModel("2", "NON PKP"),
  ];

  List<SectorEconomicModel> _listSectorEconomic = [
    SectorEconomicModel("01", "A"),
    SectorEconomicModel("01", "B"),
  ];

  List<BusinessFieldModel> _listBusinessField = [
    BusinessFieldModel("01", "A"),
    BusinessFieldModel("01", "B"),
  ];

  List<StatusLocationModel> _listLocationStatus = [
    StatusLocationModel("01", "RUMAH"),
    StatusLocationModel("02", "PERTOKOAN"),
    StatusLocationModel("03", "PASAR"),
    StatusLocationModel("04", "INDUSTRI"),
    StatusLocationModel("05", "PERKANTORAN"),
    StatusLocationModel("06", "LAINNYA"),
  ];

  List<BusinessLocationModel> _listBusinessLocation = [
    BusinessLocationModel("01", "STRATEGIS"),
    BusinessLocationModel("02", "BIASA"),
    BusinessLocationModel("03", "TIDAK STRATEGIS"),
    BusinessLocationModel("04", "LAINNYA"),
  ];

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  // Jenis Lembaga
  UnmodifiableListView<TypeInstitutionModel> get listTypeInstitution {
    return UnmodifiableListView(this._listTypeInstitution);
  }

  TypeInstitutionModel get typeInstitutionSelected => _typeInstitutionSelected;

  set typeInstitutionSelected(TypeInstitutionModel value) {
    this._typeInstitutionSelected = value;
    notifyListeners();
  }

  // Profil
  UnmodifiableListView<ProfilModel> get listProfil {
    return UnmodifiableListView(this._listProfil);
  }

  ProfilModel get profilSelected => _profilSelected;

  set profilSelected(ProfilModel value) {
    this._profilSelected = value;
    notifyListeners();
  }

  // Nama Lembaga
  TextEditingController get controllerInstitutionName {
    return this._controllerInstitutionName;
  }

  set controllerInstitutionName(value) {
    this._controllerInstitutionName = value;
    this.notifyListeners();
  }

  // Punya NPWP
  int get radioValueIsHaveNPWP => _radioValueIsHaveNPWP;

  set radioValueIsHaveNPWP(int value) {
    this._radioValueIsHaveNPWP = value;
    notifyListeners();
  }

  // NPWP
  TextEditingController get controllerNPWP {
    return this._controllerNPWP;
  }

  set controllerNPWP(value) {
    this._controllerNPWP = value;
    this.notifyListeners();
  }

  // NPWP
  TextEditingController get controllerFullNameNPWP {
    return this._controllerFullNameNPWP;
  }

  set controllerFullNameNPWP(value) {
    this._controllerFullNameNPWP = value;
    this.notifyListeners();
  }

  // Jenis NPWP
  UnmodifiableListView<JenisNPWPModel> get listTypeNPWP {
    return UnmodifiableListView(this._listTypeNPWP);
  }

  JenisNPWPModel get typeNPWPSelected {
    return this._typeNPWPSelected;
  }

  set typeNPWPSelected(JenisNPWPModel value) {
    this._typeNPWPSelected = value;
    notifyListeners();
  }

  // Tanda PKP
  UnmodifiableListView<SignPKPModel> get listSignPKP {
    return UnmodifiableListView(this._listSignPKP);
  }

  SignPKPModel get signPKPSelected {
    return this._signPKPSelected;
  }

  set signPKPSelected(SignPKPModel value) {
    this._signPKPSelected = value;
    notifyListeners();
  }

  // Alamat NPWP
  TextEditingController get controllerNPWPAddress {
    return this._controllerNPWPAddress;
  }

  set controllerNPWPAddress(value) {
    this._controllerNPWPAddress = value;
    this.notifyListeners();
  }

  // Tanggal Pendirian
  TextEditingController get controllerDateEstablishment => _controllerDateEstablishment;

  DateTime get initialDateForDateEstablishment => _initialDateForDateEstablishment;

  set initialDateForDateEstablishment(DateTime value) {
    this._initialDateForDateEstablishment = value;
  }

  String get custType => _custType;

  set custType(String value) {
    this._custType = value;
    notifyListeners();
  }

  String get lastKnownState => _lastKnownState;

  set lastKnownState(String value) {
    this._lastKnownState = value;
    notifyListeners();
  }

  String get obligorID => _obligorID;

  set obligorID(String value) {
    this._obligorID = value;
    notifyListeners();
  }

  Future<void> setPreference(BuildContext context) async {
    SharedPreferences _preference = await SharedPreferences.getInstance();
    this._custType = _preference.getString("cust_type");
    this._lastKnownState = _preference.getString("last_known_state");
    if(this._lastKnownState == "SRE" || this._lastKnownState == "RSVY" || this._lastKnownState == "DKR") {
      this._enableDataDedup = true;
    }
  }

  void selectDateEstablishment(BuildContext context) async {
    // DatePickerShared _datePickerShared = DatePickerShared();
    // var _datePickerSelected = await _datePickerShared.selectStartDate(
    //     context, this._initialDateForDateEstablishment,
    //     canAccessNextDay: false);
    // if (_datePickerSelected != null) {
    //   this.controllerDateEstablishment.text = dateFormat.format(_datePickerSelected);
    //   this._initialDateForDateEstablishment = _datePickerSelected;
    //   notifyListeners();
    // } else {
    //   return;
    // }
    var _dateSelected = await selectDate(context, _initialDateForDateEstablishment);
    if (_dateSelected != null) {
      _initialDateForDateEstablishment = _dateSelected;
      this._controllerDateEstablishment.text = dateFormat.format(_dateSelected);
      notifyListeners();
    } else {
      return;
    }
  }

  // Sektor Ekonomi
  TextEditingController get controllerSectorEconomic => _controllerSectorEconomic;

  SectorEconomicModel get sectorEconomicModelSelected => _sectorEconomicModelSelected;

  set sectorEconomicModelSelected(SectorEconomicModel value) {
    this._sectorEconomicModelSelected = value;
  }

  void searchSectorEconomic(BuildContext context) async {
    SectorEconomicModel data = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ChangeNotifierProvider(
          create: (context) => SearchSectorEconomicChangeNotif(),
          child: SearchSectorEconomic())));
    if (data != null) {
      sectorEconomicModelSelected = data;
      this._controllerSectorEconomic.text = "${data.KODE} - ${data.DESKRIPSI}";
      businessFieldModelSelected = null;
      this._controllerBusinessField.text = "";
      notifyListeners();
    } else {
      return;
    }
  }

  // Lapangan Usaha
  TextEditingController get controllerBusinessField => _controllerBusinessField;

  BusinessFieldModel get businessFieldModelSelected => _businessFieldModelSelected;

  set businessFieldModelSelected(BusinessFieldModel value) {
    this._businessFieldModelSelected = value;
  }

  void searchBusinesField(BuildContext context) async {
    BusinessFieldModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchBusinessFieldChangeNotif(),
                child: SearchBusinessField(id: this._sectorEconomicModelSelected.KODE))));
    if (data != null) {
      businessFieldModelSelected = data;
      this._controllerBusinessField.text = "${data.KODE} - ${data.DESKRIPSI}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchStatusLocation(BuildContext context) async {
    StatusLocationModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchStatusLocationChangeNotif(),
                child: SearchStatusLocation())));
    if (data != null) {
      locationStatusSelected = data;
      this._controllerStatusLocation.text = "${data.id} - ${data.desc}";
      notifyListeners();
    } else {
      return;
    }
  }

  void searchBusinessLocation(BuildContext context) async {
    BusinessLocationModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchBusinessLocationChangeNotif(),
                child: SearchBusinessLocation())));
    if (data != null) {
      businessLocationSelected = data;
      this._controllerBusinessLocation.text = "${data.id} - ${data.desc}";
      notifyListeners();
    } else {
      return;
    }
  }

  // Status Lokasi
  UnmodifiableListView<StatusLocationModel> get listLocationStatus {
    return UnmodifiableListView(this._listLocationStatus);
  }

  StatusLocationModel get locationStatusSelected {
    return this._locationStatusSelected;
  }

  set locationStatusSelected(StatusLocationModel value) {
    this._locationStatusSelected = value;
    notifyListeners();
  }

  // Lokasi Usaha
  UnmodifiableListView<BusinessLocationModel> get listBusinessLocation {
    return UnmodifiableListView(this._listBusinessLocation);
  }

  BusinessLocationModel get businessLocationSelected {
    return this._businessLocationSelected;
  }

  set businessLocationSelected(BusinessLocationModel value) {
    this._businessLocationSelected = value;
    notifyListeners();
  }

  // Total Pegawai
  TextEditingController get controllerTotalEmployees {
    return this._controllerTotalEmployees;
  }

  set controllerTotalEmployees(value) {
    this._controllerTotalEmployees = value;
    this.notifyListeners();
  }

  TextEditingController get controllerStatusLocation => _controllerStatusLocation;

  set controllerStatusLocation(TextEditingController value) {
    _controllerStatusLocation = value;
    this.notifyListeners();
  }

  TextEditingController get controllerBusinessLocation => _controllerBusinessLocation;

  set controllerBusinessLocation(TextEditingController value) {
    _controllerBusinessLocation = value;
    this.notifyListeners();
  }

  // Lama Usaha/Kerja Berjalan (bln)
  TextEditingController get controllerLamaUsahaBerjalan {
    return this._controllerLamaUsahaBerjalan;
  }

  set controllerLamaUsahaBerjalan(value) {
    this._controllerLamaUsahaBerjalan = value;
    this.notifyListeners();
  }

  // Total Lama Usaha/Kerja Berjalan (bln)
  TextEditingController get controllerTotalLamaUsahaBerjalan {
    return this._controllerTotalLamaUsahaBerjalan;
  }

  set controllerTotalLamaUsahaBerjalan(value) {
    this._controllerTotalLamaUsahaBerjalan = value;
    this.notifyListeners();
  }

  // Buka Rekening
  int get radioValueOpenAccount => _radioValueOpenAccount;

  set radioValueOpenAccount(int value) {
    this._controllerFormNumber.clear();
    this._radioValueOpenAccount = value;
    notifyListeners();
  }

  // Nomor Form
  TextEditingController get controllerFormNumber {
    return this._controllerFormNumber;
  }

  set controllerFormNumber(value) {
    this._controllerFormNumber = value;
    this.notifyListeners();
  }

  GlobalKey<FormState> get keyForm => _key;

  bool get flag => _flag;

  set flag(bool value) {
    _flag = value;
    notifyListeners();
  }

  void check(BuildContext context) {
    final _form = _key.currentState;
    if (_form.validate()) {
      if(int.parse(this._controllerTotalLamaUsahaBerjalan.text) < int.parse(this._controllerLamaUsahaBerjalan.text)) {
        this._controllerLamaUsahaBerjalan.clear();
        showSnackBar("Lama Usaha tidak boleh lebih besar dari Total Lama Usaha");
      }
      else {
        flag = true;
        autoValidate = false;
        if(lastKnownState == "DKR")checkDataDakor();
        Navigator.pop(context);
      }
    } else {
      flag = false;
      autoValidate = true;
    }
  }

  Future<bool> onBackPress() async{
    final _form = _key.currentState;
    if (_form.validate()) {
      flag = true;
      autoValidate = false;
      if(lastKnownState == "DKR")checkDataDakor();
    } else {
      flag = false;
      autoValidate = true;
    }
    return true;
  }

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor,duration: Duration(seconds: 5)));
  }

  // Future<void> clearDataRincian() async {
  void clearDataRincian() {
    this._autoValidate = false;
    this._flag = false;
    this._typeInstitutionSelected = null;
    this._profilSelected = null;
    this._controllerInstitutionName.clear();
    this._radioValueIsHaveNPWP = 0;
    this._controllerNPWP.clear();
    this._controllerFullNameNPWP.clear();
    this._typeNPWPSelected = null;
    this._signPKPSelected = null;
    this._controllerNPWPAddress.clear();
    this._controllerDateEstablishment.clear();
    this._controllerSectorEconomic.clear();
    this._controllerBusinessField.clear();
    this._locationStatusSelected = null;
    this._controllerStatusLocation.clear();
    this._businessLocationSelected = null;
    this._controllerBusinessLocation.clear();
    this._controllerTotalEmployees.clear();
    this._controllerLamaUsahaBerjalan.clear();
    this._controllerTotalLamaUsahaBerjalan.clear();
    this._radioValueOpenAccount = 0;
    this._controllerFormNumber.clear();
    this._isCompanyTypeDakor = false;
    this._isProfileDakor = false;
    this._isCompanyNameDakor = false;
    this._isHaveNPWPDakor = false;
    this._isNoNPWPDakor = false;
    this._isNameNPWPDakor = false;
    this._isTypeNPWPDakor = false;
    this._isPKPSignDakor = false;
    this._isAddressNPWPDakor = false;
    this._isEstablishDateDakor = false;
    this._isSectorEconomicDakor = false;
    this._isBusinessFieldDakor = false;
    this._isLocationStatusDakor = false;
    this._isBusinessLocationDakor = false;
    this._isTotalEmpDakor = false;
    this._isLamaUsahaDakor = false;
    this._isTotalLamaUsahaDakor = false;
    enableDataDedup = false;
  }

  bool get isEnableFieldEstablishDate => _isEnableFieldEstablishDate;

  set isEnableFieldEstablishDate(bool value) {
    this._isEnableFieldEstablishDate = value;
  }

  bool get isEnableFieldNumberNPWP => _isEnableFieldNumberNPWP;

  set isEnableFieldNumberNPWP(bool value) {
    this._isEnableFieldNumberNPWP = value;
  }

  bool get isEnableFieldCompanyName => _isEnableFieldCompanyName;

  set isEnableFieldCompanyName(bool value) {
    this._isEnableFieldCompanyName = value;
  }

  void saveToSQLite(BuildContext context) async{
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _providerOID = Provider.of<ListOidChangeNotifier>(context,listen: false);
    _dbHelper.insertMS2CustomerCompany(MS2CustomerCompanyModel(
            "000001",
            _customerCorporateID,
            _typeInstitutionSelected,
            _profilSelected,
            _controllerInstitutionName.text,
            null,
            null,
            null,
            null,
            null,
            null,
            _initialDateForDateEstablishment.toString(),
            "1",
            null,
            null,
            _sectorEconomicModelSelected,
            _businessFieldModelSelected,
            _locationStatusSelected,
            _businessLocationSelected,
            _controllerTotalEmployees.text,
            _controllerLamaUsahaBerjalan.text,
            _controllerTotalLamaUsahaBerjalan.text,
            _radioValueOpenAccount,
            _controllerFormNumber.text,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            DateTime.now().toString(),
            _preferences.getString("username"),
            null,
            null,
            1,
            null,
            _isCompanyTypeDakor,
            _isProfileDakor,
            _isCompanyNameDakor,
            _isEstablishDateDakor,
            _isSectorEconomicDakor,
            _isBusinessFieldDakor,
            _isLocationStatusDakor,
            _isBusinessLocationDakor,
            _isTotalEmpDakor,
            _isLamaUsahaDakor,
            _isTotalLamaUsahaDakor
        )
    );
    //TODO perlu di adjut untuk customerID
    _dbHelper.insertMS2Customer(MS2CustomerModel(
        "00001",
        customerID,//customerID
        "00011",
        "${_providerOID.customerType}",
        _controllerNPWPAddress.text,
        _controllerFullNameNPWP.text,
        _controllerNPWP.text,
        _typeNPWPSelected,
        _radioValueIsHaveNPWP,
        _signPKPSelected != null ? _signPKPSelected.id : null,
        null,
        null,
        null,
        null,
        null,
        DateTime.now().toString(),
        _preferences.getString("username"),
        null,
        null,
        1,
        _isHaveNPWPDakor ? "1" : "0",
        _isNoNPWPDakor ? "1" : "0",
        _isNameNPWPDakor ? "1" : "0",
        _isTypeNPWPDakor ? "1" : "0",
        _isPKPSignDakor ? "1" : "0",
        _isAddressNPWPDakor ? "1" : "0"
    )
    );
  }

  Future<bool> deleteSQLite() async{
    return await _dbHelper.deleteMS2CustomerCompany();
  }

  bool get loadDataDedup => _loadDataDedup;

  set loadDataDedup(bool value) {
    _loadDataDedup = value;
    notifyListeners();
  }

  void setDataSQLite(BuildContext context) async{
    debugPrint("JALAN_COM");
    // setupData = true;
    List _dataCustomer = await _dbHelper.selectDataInfoNasabahFromDataDedup();
    List _dataCustomerCompany = await _dbHelper.selectMS2CustomerCompany();
    debugPrint("CEK_DATA_COM_INFO $_dataCustomerCompany");
    List _dataCustomerNpwp = await _dbHelper.selectMS2Customer();
    debugPrint("CEK_DATA_COM_NPWP $_dataCustomerNpwp");
    this._radioValueIsHaveNPWP = 1;
    // if(_dataCustomerCompany.isEmpty){
    //   if(_dataCustomer.isNotEmpty){
    //     loadDataDedup = true;
    //     this._controllerInstitutionName.text = _dataCustomer[0]['nama'];
    //     this._radioValueIsHaveNPWP = 1;
    //     this._controllerNPWP.text = _dataCustomer[0]['no_identitas'].toString();
    //     this._controllerDateEstablishment.text = dateFormat.format(DateTime.parse(_dataCustomer[0]['tanggal_lahir']));
    //     loadDataDedup = false;
    //   }
    // }

    // if(_dataCustomer.isNotEmpty){
    //   debugPrint("CEK_CUST_COM");
    //   loadDataDedup = true;
    //   this._controllerInstitutionName.text = _dataCustomer[0]['nama'];
    //   this._radioValueIsHaveNPWP = 1;
    //   this._controllerNPWP.text = _dataCustomer[0]['no_identitas'];
    //   this._controllerFullNameNPWP.text = _dataCustomer[0]['npwp_name'] != "null" || _dataCustomer[0]['npwp_name'] != "" ? _dataCustomer[0]['npwp_name'] : "";
    //   this._controllerDateEstablishment.text = dateFormat.format(DateTime.parse(_dataCustomer[0]['tanggal_lahir']));
    //   loadDataDedup = false;
    // }
    if(_dataCustomerNpwp.isNotEmpty){
      this._controllerNPWP.text = _dataCustomerNpwp[0]['npwp_no'].replaceAll(".", "").replaceAll("-", "");
      this._controllerFullNameNPWP.text = _dataCustomerNpwp[0]['npwp_name'] != "null" && _dataCustomerNpwp[0]['npwp_name'] != "" ? _dataCustomerNpwp[0]['npwp_name'] : "";
      for(int i=0; i<_listTypeNPWP.length; i++){
        debugPrint("CHECK_NPWP_TYPE ${_dataCustomerNpwp[0]['npwp_type']}");
        if(_dataCustomerNpwp[0]['npwp_type'] == _listTypeNPWP[i].id){
          _typeNPWPSelected = _listTypeNPWP[i];
        }
      }
      for(int i=0; i<listSignPKP.length; i++){
        if(listSignPKP[i].id == _dataCustomerNpwp[0]['pkp_flag']){
          _signPKPSelected = listSignPKP[i];
        }
      }
      obligorID = _dataCustomerNpwp[0]['id_obligor'];
      customerID = _dataCustomerNpwp[0]['customerID'];
      debugPrint("CHECK_NPWP_ADDRESS ${_dataCustomerNpwp[0]['npwp_address']}");
      this._controllerNPWPAddress.text= _dataCustomerNpwp[0]['npwp_address'] != "null" && _dataCustomerNpwp[0]['npwp_address'] != "" ? _dataCustomerNpwp[0]['npwp_address'] : "";
    }

    if(_dataCustomerCompany.isNotEmpty) {
      for(int i=0; i < this._listTypeInstitution.length; i++){
        debugPrint("CEK_TYPE ${_dataCustomerCompany[0]['comp_type']}");
        if(_dataCustomerCompany[0]['comp_type'] == _listTypeInstitution[i].PARA_ID){
          _typeInstitutionSelected = _listTypeInstitution[i];
        }
      }

      this._controllerInstitutionName.text = _dataCustomerCompany[0]['comp_name'].toString();
      for(int i=0; i < this._listProfil.length; i++){
        if(_dataCustomerCompany[0]['profil'] == _listProfil[i].id){
          _profilSelected = _listProfil[i];
        }
      }
      this._initialDateForDateEstablishment = DateTime.parse(_dataCustomerCompany[0]['establish_date']);
      this._controllerDateEstablishment.text = dateFormat.format(DateTime.parse(_dataCustomerCompany[0]['establish_date']));
      debugPrint("CEK_DATA_COM ${_dataCustomerCompany[0]['sector_economic']}");
      if(_dataCustomerCompany[0]['sector_economic'] != null && _dataCustomerCompany[0]['sector_economic'] != ""){
        this._sectorEconomicModelSelected = SectorEconomicModel(_dataCustomerCompany[0]['sector_economic'], _dataCustomerCompany[0]['sector_economic_desc']);
        this._controllerSectorEconomic.text = "${_dataCustomerCompany[0]['sector_economic'].toString()} - ${_dataCustomerCompany[0]['sector_economic_desc'].toString()}";
      }

      if(_dataCustomerCompany[0]['nature_of_buss'] != null && _dataCustomerCompany[0]['nature_of_buss'] != ""){
        this._businessFieldModelSelected = BusinessFieldModel(_dataCustomerCompany[0]['nature_of_buss'], _dataCustomerCompany[0]['nature_of_buss_desc']);
        this._controllerBusinessField.text = "${_dataCustomerCompany[0]['nature_of_buss'].toString()} - ${_dataCustomerCompany[0]['nature_of_buss_desc'].toString()}";
      }
      if(_dataCustomerCompany[0]['location_status'] != null && _dataCustomerCompany[0]['location_status'] != ""){
        this._locationStatusSelected = StatusLocationModel(_dataCustomerCompany[0]['location_status'], _dataCustomerCompany[0]['location_status_desc']);
        this._controllerStatusLocation.text = "${_locationStatusSelected.id} - ${_locationStatusSelected.desc}";
      }
      if(_dataCustomerCompany[0]['buss_location'] != null && _dataCustomerCompany[0]['buss_location'] != ""){
        this._businessLocationSelected = BusinessLocationModel(_dataCustomerCompany[0]['buss_location'], _dataCustomerCompany[0]['buss_location_desc']);
        this._controllerBusinessLocation.text = "${_businessLocationSelected.id} - ${_businessLocationSelected.desc}";
      }
      debugPrint("CHECK_TOTAL_EMP ${_dataCustomerCompany[0]['total_emp'] == "null"}");
      if(_dataCustomerCompany[0]['total_emp'] != "null" && _dataCustomerCompany[0]['total_emp'] != "")
        this._controllerTotalEmployees.text = _dataCustomerCompany[0]['total_emp'].toString();
      if(_dataCustomerCompany[0]['no_of_year_work'] != "null" && _dataCustomerCompany[0]['no_of_year_work'] != "")
        this._controllerLamaUsahaBerjalan.text = _dataCustomerCompany[0]['no_of_year_work'].toString();
      if(_dataCustomerCompany[0]['no_of_year_buss'] != "null" && _dataCustomerCompany[0]['no_of_year_buss'] != "")
        this._controllerTotalLamaUsahaBerjalan.text = _dataCustomerCompany[0]['no_of_year_buss'].toString();
      this._radioValueOpenAccount = _dataCustomerCompany[0]['flag_account'] != "null" ? int.parse(_dataCustomerCompany[0]['flag_account'].toString()) : 0;
      this._controllerFormNumber.text = _dataCustomerCompany[0]['account_no_form'];

      this._isCompanyTypeDakor = _dataCustomerCompany[0]['edit_comp_type'] == "true";
      this._isProfileDakor = _dataCustomerCompany[0]['edit_profil'] == "true";
      this._isCompanyNameDakor = _dataCustomerCompany[0]['edit_comp_name'] == "true";
      this._isEstablishDateDakor = _dataCustomerCompany[0]['edit_establish_date'] == "true";
      this._isSectorEconomicDakor = _dataCustomerCompany[0]['edit_sector_economic'] == "true";
      this._isBusinessFieldDakor = _dataCustomerCompany[0]['edit_nature_of_buss'] == "true";
      this._isLocationStatusDakor = _dataCustomerCompany[0]['edit_location_status'] == "true";
      this._isBusinessLocationDakor = _dataCustomerCompany[0]['edit_buss_location'] == "true";
      this._isTotalEmpDakor = _dataCustomerCompany[0]['edit_total_emp'] == "true";
      this._isLamaUsahaDakor = _dataCustomerCompany[0]['edit_no_of_year_work'] == "true";
      this._isTotalLamaUsahaDakor = _dataCustomerCompany[0]['edit_no_of_year_bus'] == "true";
      customerCorporateID = _dataCustomerCompany[0]['customerCorporateID'];
    }
    notifyListeners();
  }

  Future<void> checkDataDakor() async{
    List _dataCustomer = await _dbHelper.selectMS2Customer();
    List _dataCustomerCompany = await _dbHelper.selectMS2CustomerCompany();
    if(_dataCustomerCompany.isNotEmpty && _lastKnownState == "DKR"){
      if(_dataCustomer.isNotEmpty){
        isHaveNPWPDakor =  _dataCustomer[0]['flag_npwp'] != _radioValueIsHaveNPWP || _dataCustomer[0]['edit_flag_npwp'] == "1";
        isNoNPWPDakor = _dataCustomer[0]['npwp_no'].toString() != _controllerNPWP.text || _dataCustomer[0]['edit_npwp_no'] == "1";
        isNameNPWPDakor = _dataCustomer[0]['npwp_name'].toString() != _controllerFullNameNPWP.text || _dataCustomer[0]['edit_npwp_name'] == "1";
        isTypeNPWPDakor = _typeNPWPSelected != null ? _dataCustomer[0]['npwp_type'] != _typeNPWPSelected.id || _dataCustomer[0]['edit_npwp_type'] == "1" : false;
        isPKPSignDakor = _signPKPSelected != null ? _dataCustomer[0]['pkp_flag'] != _signPKPSelected.id || _dataCustomer[0]['edit_pkp_flag'] == "1" : false;
        isAddressNPWPDakor = _dataCustomer[0]['npwp_address'] != _controllerNPWPAddress.text || _dataCustomer[0]['edit_npwp_address'] == "1";
      }
      isCompanyTypeDakor = _dataCustomerCompany[0]['comp_type'] != _typeInstitutionSelected.PARA_ID || _dataCustomerCompany[0]['edit_comp_type'] == "true";//_dataCustomerCompany[0]['comp_type'] != _typeInstitutionSelected.PARA_ID || _dataCustomerCompany[0]['edit_comp_type'] == "1";
      isCompanyNameDakor = _dataCustomerCompany[0]['comp_name'].toString() != _controllerInstitutionName.text || _dataCustomerCompany[0]['edit_comp_name'] == "true";//_dataCustomerCompany[0]['comp_name'].toString() != _controllerInstitutionName.text || _dataCustomerCompany[0]['edit_comp_name'] == "1";
      isProfileDakor = _dataCustomerCompany[0]['profil'] != _profilSelected.id || _dataCustomerCompany[0]['edit_profil'] == "true";//_dataCustomerCompany[0]['profil'] != _profilSelected.id || _dataCustomerCompany[0]['edit_profil'] == "1";
      isEstablishDateDakor = dateFormat.format(DateTime.parse(_dataCustomerCompany[0]['establish_date'])) != _controllerDateEstablishment.text || _dataCustomerCompany[0]['edit_establish_date'] == "true";//dateFormat.format(DateTime.parse(_dataCustomerCompany[0]['establish_date'])) != _controllerDateEstablishment.text || _dataCustomerCompany[0]['edit_teTime'] == "1";
      isSectorEconomicDakor = _sectorEconomicModelSelected != null ? _dataCustomerCompany[0]['sector_economic'] != _sectorEconomicModelSelected.KODE : false || _dataCustomerCompany[0]['edit_sector_economic'] == "true";//_sectorEconomicModelSelected != null ? _dataCustomerCompany[0]['sector_economic'] != _sectorEconomicModelSelected.KODE || _dataCustomerCompany[0]['edit_sector_economic'] == "1" : false;
      isBusinessFieldDakor = _businessFieldModelSelected != null ? _dataCustomerCompany[0]['nature_of_buss'] != _businessFieldModelSelected.KODE : false || _dataCustomerCompany[0]['edit_nature_of_buss'] == "true";//_businessFieldModelSelected != null ? _dataCustomerCompany[0]['nature_of_buss'] != _businessFieldModelSelected.KODE || _dataCustomerCompany[0]['edit_nature_of_buss'] == "1" : false;
      isLocationStatusDakor = _locationStatusSelected != null ? _dataCustomerCompany[0]['location_status'] != _locationStatusSelected.id : false || _dataCustomerCompany[0]['edit_location_status'] == "true";//_locationStatusSelected != null ? _dataCustomerCompany[0]['location_status'] != _locationStatusSelected.id || _dataCustomerCompany[0]['edit_location_status'] == "1" : false;
      isBusinessLocationDakor = _businessLocationSelected != null ? _dataCustomerCompany[0]['buss_location'] != _businessLocationSelected.id : false || _dataCustomerCompany[0]['edit_buss_location'] == "true";//_businessLocationSelected != null ? _dataCustomerCompany[0]['location_status'] != _businessLocationSelected.id || _dataCustomerCompany[0]['edit_location_status'] == "1" : false;
      isTotalEmpDakor = _dataCustomerCompany[0]['total_emp'].toString() != _controllerTotalEmployees.text || _dataCustomerCompany[0]['edit_total_emp'] == "true";//_dataCustomerCompany[0]['total_emp'].toString() != _controllerTotalEmployees.text || _dataCustomerCompany[0]['edit_total_emp'] == "1";
      isLamaUsahaDakor = _dataCustomerCompany[0]['no_of_year_work'].toString() != _controllerLamaUsahaBerjalan.text || _dataCustomerCompany[0]['edit_no_of_year_work'] == "true";//_dataCustomerCompany[0]['no_of_year_work'].toString() != _controllerLamaUsahaBerjalan.text || _dataCustomerCompany[0]['edit_no_of_year_work'] == "1";
      isTotalLamaUsahaDakor = _dataCustomerCompany[0]['no_of_year_buss'].toString() != _controllerTotalLamaUsahaBerjalan.text || _dataCustomerCompany[0]['edit_no_of_year_bus'] == "true";//_dataCustomerCompany[0]['no_of_year_buss'].toString() != _controllerTotalLamaUsahaBerjalan.text || _dataCustomerCompany[0]['edit_no_of_year_buss'] == "1";
      // this._radioValueOpenAccount = _dataCustomerCompany[0]['flag_account'];
      // this._controllerFormNumber.text = _dataCustomerCompany[0]['account_no_form'];
    }
    notifyListeners();
  }

  bool get isTotalLamaUsahaMandatory => _isTotalLamaUsahaMandatory;

  set isTotalLamaUsahaMandatory(bool value) {
    _isTotalLamaUsahaMandatory = value;
    notifyListeners();
  }

  bool get isLamaUsahaMandatory => _isLamaUsahaMandatory;

  set isLamaUsahaMandatory(bool value) {
    _isLamaUsahaMandatory = value;
    notifyListeners();
  }

  bool get isTotalEmpMandatory => _isTotalEmpMandatory;

  set isTotalEmpMandatory(bool value) {
    _isTotalEmpMandatory = value;
    notifyListeners();
  }

  bool get isBusinessLocationMandatory => _isBusinessLocationMandatory;

  set isBusinessLocationMandatory(bool value) {
    _isBusinessLocationMandatory = value;
    notifyListeners();
  }

  bool get isLocationStatusMandatory => _isLocationStatusMandatory;

  set isLocationStatusMandatory(bool value) {
    _isLocationStatusMandatory = value;
    notifyListeners();
  }

  bool get isBusinessFieldMandatory => _isBusinessFieldMandatory;

  set isBusinessFieldMandatory(bool value) {
    _isBusinessFieldMandatory = value;
    notifyListeners();
  }

  bool get isSectorEconomicMandatory => _isSectorEconomicMandatory;

  set isSectorEconomicMandatory(bool value) {
    _isSectorEconomicMandatory = value;
    notifyListeners();
  }

  bool get isEstablishDateMandatory => _isEstablishDateMandatory;

  set isEstablishDateMandatory(bool value) {
    _isEstablishDateMandatory = value;
    notifyListeners();
  }

  bool get isAddressNPWPMandatory => _isAddressNPWPMandatory;

  set isAddressNPWPMandatory(bool value) {
    _isAddressNPWPMandatory = value;
    notifyListeners();
  }

  bool get isPKPSignMandatory => _isPKPSignMandatory;

  set isPKPSignMandatory(bool value) {
    _isPKPSignMandatory = value;
    notifyListeners();
  }

  bool get isTypeNPWPMandatory => _isTypeNPWPMandatory;

  set isTypeNPWPMandatory(bool value) {
    _isTypeNPWPMandatory = value;
    notifyListeners();
  }

  bool get isNameNPWPMandatory => _isNameNPWPMandatory;

  set isNameNPWPMandatory(bool value) {
    _isNameNPWPMandatory = value;
    notifyListeners();
  }

  bool get isNoNPWPMandatory => _isNoNPWPMandatory;

  set isNoNPWPMandatory(bool value) {
    _isNoNPWPMandatory = value;
    notifyListeners();
  }

  bool get isHaveNPWPMandatory => _isHaveNPWPMandatory;

  set isHaveNPWPMandatory(bool value) {
    _isHaveNPWPMandatory = value;
    notifyListeners();
  }

  bool get isCompanyNameMandatory => _isCompanyNameMandatory;

  set isCompanyNameMandatory(bool value) {
    _isCompanyNameMandatory = value;
    notifyListeners();
  }

  bool get isProfileMandatory => _isProfileMandatory;

  set isProfileMandatory(bool value) {
    _isProfileMandatory = value;
    notifyListeners();
  }

  bool get isCompanyTypeMandatory => _isCompanyTypeMandatory;

  set isCompanyTypeMandatory(bool value) {
    _isCompanyTypeMandatory = value;
    notifyListeners();
  }

  bool get isTotalLamaUsahaShow => _isTotalLamaUsahaShow;

  set isTotalLamaUsahaShow(bool value) {
    _isTotalLamaUsahaShow = value;
    notifyListeners();
  }

  bool get isLamaUsahaShow => _isLamaUsahaShow;

  set isLamaUsahaShow(bool value) {
    _isLamaUsahaShow = value;
    notifyListeners();
  }

  bool get isTotalEmpShow => _isTotalEmpShow;

  set isTotalEmpShow(bool value) {
    _isTotalEmpShow = value;
    notifyListeners();
  }

  bool get isBusinessLocationShow => _isBusinessLocationShow;

  set isBusinessLocationShow(bool value) {
    _isBusinessLocationShow = value;
    notifyListeners();
  }

  bool get isLocationStatusShow => _isLocationStatusShow;

  set isLocationStatusShow(bool value) {
    _isLocationStatusShow = value;
    notifyListeners();
  }

  bool get isBusinessFieldShow => _isBusinessFieldShow;

  set isBusinessFieldShow(bool value) {
    _isBusinessFieldShow = value;
    notifyListeners();
  }

  bool get isSectorEconomicShow => _isSectorEconomicShow;

  set isSectorEconomicShow(bool value) {
    _isSectorEconomicShow = value;
    notifyListeners();
  }

  bool get isEstablishDateShow => _isEstablishDateShow;

  set isEstablishDateShow(bool value) {
    _isEstablishDateShow = value;
    notifyListeners();
  }

  bool get isAddressNPWPShow => _isAddressNPWPShow;

  set isAddressNPWPShow(bool value) {
    _isAddressNPWPShow = value;
    notifyListeners();
  }

  bool get isPKPSignShow => _isPKPSignShow;

  set isPKPSignShow(bool value) {
    _isPKPSignShow = value;
    notifyListeners();
  }

  bool get isTypeNPWPShow => _isTypeNPWPShow;

  set isTypeNPWPShow(bool value) {
    _isTypeNPWPShow = value;
    notifyListeners();
  }

  bool get isNameNPWPShow => _isNameNPWPShow;

  set isNameNPWPShow(bool value) {
    _isNameNPWPShow = value;
    notifyListeners();
  }

  bool get isNoNPWPShow => _isNoNPWPShow;

  set isNoNPWPShow(bool value) {
    _isNoNPWPShow = value;
    notifyListeners();
  }

  bool get isHaveNPWPShow => _isHaveNPWPShow;

  set isHaveNPWPShow(bool value) {
    _isHaveNPWPShow = value;
    notifyListeners();
  }

  bool get isCompanyNameShow => _isCompanyNameShow;

  set isCompanyNameShow(bool value) {
    _isCompanyNameShow = value;
    notifyListeners();
  }

  bool get isProfileShow => _isProfileShow;

  set isProfileShow(bool value) {
    _isProfileShow = value;
    notifyListeners();
  }

  bool get isCompanyTypeShow => _isCompanyTypeShow;

  set isCompanyTypeShow(bool value) {
    _isCompanyTypeShow = value;
    notifyListeners();
  }

  bool get isTotalLamaUsahaDakor => _isTotalLamaUsahaDakor;

  set isTotalLamaUsahaDakor(bool value) {
    _isTotalLamaUsahaDakor = value;
    notifyListeners();
  }

  bool get isLamaUsahaDakor => _isLamaUsahaDakor;

  set isLamaUsahaDakor(bool value) {
    _isLamaUsahaDakor = value;
    notifyListeners();
  }

  bool get isTotalEmpDakor => _isTotalEmpDakor;

  set isTotalEmpDakor(bool value) {
    _isTotalEmpDakor = value;
    notifyListeners();
  }

  bool get isBusinessLocationDakor => _isBusinessLocationDakor;

  set isBusinessLocationDakor(bool value) {
    _isBusinessLocationDakor = value;
    notifyListeners();
  }

  bool get isLocationStatusDakor => _isLocationStatusDakor;

  set isLocationStatusDakor(bool value) {
    _isLocationStatusDakor = value;
    notifyListeners();
  }

  bool get isBusinessFieldDakor => _isBusinessFieldDakor;

  set isBusinessFieldDakor(bool value) {
    _isBusinessFieldDakor = value;
    notifyListeners();
  }

  bool get isSectorEconomicDakor => _isSectorEconomicDakor;

  set isSectorEconomicDakor(bool value) {
    _isSectorEconomicDakor = value;
    notifyListeners();
  }

  bool get isEstablishDateDakor => _isEstablishDateDakor;

  set isEstablishDateDakor(bool value) {
    _isEstablishDateDakor = value;
    notifyListeners();
  }

  bool get isAddressNPWPDakor => _isAddressNPWPDakor;

  set isAddressNPWPDakor(bool value) {
    _isAddressNPWPDakor = value;
    notifyListeners();
  }

  bool get isPKPSignDakor => _isPKPSignDakor;

  set isPKPSignDakor(bool value) {
    _isPKPSignDakor = value;
    notifyListeners();
  }

  bool get isTypeNPWPDakor => _isTypeNPWPDakor;

  set isTypeNPWPDakor(bool value) {
    _isTypeNPWPDakor = value;
    notifyListeners();
  }

  bool get isNameNPWPDakor => _isNameNPWPDakor;

  set isNameNPWPDakor(bool value) {
    _isNameNPWPDakor = value;
    notifyListeners();
  }

  bool get isNoNPWPDakor => _isNoNPWPDakor;

  set isNoNPWPDakor(bool value) {
    _isNoNPWPDakor = value;
    notifyListeners();
  }

  bool get isHaveNPWPDakor => _isHaveNPWPDakor;

  set isHaveNPWPDakor(bool value) {
    _isHaveNPWPDakor = value;
    notifyListeners();
  }

  bool get isCompanyNameDakor => _isCompanyNameDakor;

  set isCompanyNameDakor(bool value) {
    _isCompanyNameDakor = value;
    notifyListeners();
  }

  bool get isProfileDakor => _isProfileDakor;

  set isProfileDakor(bool value) {
    _isProfileDakor = value;
    notifyListeners();
  }

  bool get isCompanyTypeDakor => _isCompanyTypeDakor;

  set isCompanyTypeDakor(bool value) {
    _isCompanyTypeDakor = value;
    notifyListeners();
  }
}
