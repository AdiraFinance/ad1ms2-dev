import 'dart:collection';
import 'dart:io';

import 'package:ad1ms2_dev/models/form_m_company_penjamin_model.dart';
import 'package:ad1ms2_dev/shared/date_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'constants.dart';

class FormMCompanyPenjaminChangeNotifier with ChangeNotifier {
  bool _autoValidate = false;
  int _radioValueIsHaveGuarantor = 0;

  // Penjamin - Pribadi
  RelationshipStatusModel _relationshipStatusSelected;
  TypeIdentityModel _typeIdentitySelected;
  TextEditingController _controllerIdentityNumber = TextEditingController();
  TextEditingController _controllerFullNameIdentity = TextEditingController();
  TextEditingController _controllerFullName = TextEditingController();
  TextEditingController _controllerAlias = TextEditingController();
  TextEditingController _controllerDegree = TextEditingController();
  TextEditingController _controllerBirthDate = TextEditingController();
  DateTime _initialDateForBirthDate = DateTime(dateNow.year, dateNow.month, dateNow.day);
  TextEditingController _controllerPlaceOfBirthIdentity = TextEditingController();
  int _radioValueGender = 0;
  TextEditingController _controllerHandphoneNumber = TextEditingController();

  // Penjamin - Pribadi Alamat
  TextEditingController _controllerAlamat = TextEditingController();

  // Penjamin - Kelembagaan
  TypeInstitutionModel _typeInstitutionSelected;
  TextEditingController _controllerInstitutionName = TextEditingController();
  TextEditingController _controllerDateOfEstablishment = TextEditingController();
  DateTime _initialDateForDateOfEstablishment = DateTime(dateNow.year, dateNow.month, dateNow.day);
  TextEditingController _controllerNPWP = TextEditingController();

  // Penjamin - Kelembagaan Alamat

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  List<RelationshipStatusModel> _listRelationshipStatus = RelationshipStatusList().relationshipStatusItems;

  List<TypeIdentityModel> _listTypeIdentity = [
    TypeIdentityModel("01", "KTP"),
    TypeIdentityModel("02", "NPWP"),
  ];

  List<TypeInstitutionModel> _listTypeInstitution = TypeInstitutionList().typeInstitutionItems;

  // Ada Penjamin
  int get radioValueIsHaveGuarantor => _radioValueIsHaveGuarantor;

  set radioValueIsHaveGuarantor(int value) {
    this._radioValueIsHaveGuarantor = value;
    notifyListeners();
  }

  // Status Hubungan
  UnmodifiableListView<RelationshipStatusModel> get listRelationshipStatus {
    return UnmodifiableListView(this._listRelationshipStatus);
  }

  RelationshipStatusModel get relationshipStatusSelected =>
      _relationshipStatusSelected;

  set relationshipStatusSelected(RelationshipStatusModel value) {
    this._relationshipStatusSelected = value;
    notifyListeners();
  }

  // Jenis Identitas
  UnmodifiableListView<TypeIdentityModel> get listTypeIdentity {
    return UnmodifiableListView(this._listTypeIdentity);
  }

  TypeIdentityModel get typeIdentitySelected => _typeIdentitySelected;

  set typeIdentitySelected(TypeIdentityModel value) {
    this._typeIdentitySelected = value;
    notifyListeners();
  }

  // No Identitas
  TextEditingController get controllerIdentityNumber {
    return this._controllerIdentityNumber;
  }

  set controllerIdentityNumber(value) {
    this._controllerIdentityNumber = value;
    this.notifyListeners();
  }

  // Nama Lengkap Sesuai Identitas
  TextEditingController get controllerFullNameIdentity {
    return this._controllerFullNameIdentity;
  }

  set controllerFullNameIdentity(value) {
    this._controllerFullNameIdentity = value;
    this.notifyListeners();
  }

  // Nama Lengkap
  TextEditingController get controllerFullName {
    return this._controllerFullName;
  }

  set controllerFullName(value) {
    this._controllerFullName = value;
    this.notifyListeners();
  }

  // Alias
  TextEditingController get controllerAlias {
    return this._controllerAlias;
  }

  set controllerAlias(value) {
    this._controllerAlias = value;
    this.notifyListeners();
  }

  // Gelar
  TextEditingController get controllerDegree {
    return this._controllerDegree;
  }

  set controllerDegree(value) {
    this._controllerDegree = value;
    this.notifyListeners();
  }

  // Tanggal Lahir
  TextEditingController get controllerBirthDate => _controllerBirthDate;

  DateTime get initialDateForBirthDate => _initialDateForBirthDate;

  void selectBirthDate(BuildContext context) async {
    DatePickerShared _datePickerShared = DatePickerShared();
    var _datePickerSelected = await _datePickerShared.selectStartDate(
        context, this._initialDateForBirthDate,
        canAccessNextDay: false);
    if (_datePickerSelected != null) {
      this.controllerBirthDate.text = dateFormat.format(_datePickerSelected);
      this._initialDateForBirthDate = _datePickerSelected;
      notifyListeners();
    } else {
      return;
    }
  }

  // Tempat Lahir Sesuai Identitas
  TextEditingController get controllerPlaceOfBirthIdentity {
    return this._controllerPlaceOfBirthIdentity;
  }

  set controllerPlaceOfBirthIdentity(value) {
    this._controllerPlaceOfBirthIdentity = value;
    this.notifyListeners();
  }

  // Jenis Kalmin
  int get radioValueGender {
    return _radioValueGender;
  }

  set radioValueGender(int value) {
    this._radioValueGender = value;
    notifyListeners();
  }

  // No Handphone
  TextEditingController get controllerHandphoneNumber {
    return this._controllerHandphoneNumber;
  }

  set controllerHandphoneNumber(value) {
    this._controllerHandphoneNumber = value;
    this.notifyListeners();
  }

  // Alamat
  TextEditingController get controllerAlamat => _controllerAlamat;

  // Jenis Lembaga
  UnmodifiableListView<TypeInstitutionModel> get listTypeInstitution {
    return UnmodifiableListView(this._listTypeInstitution);
  }

  TypeInstitutionModel get typeInstitutionSelected => _typeInstitutionSelected;

  set typeInstitutionSelected(TypeInstitutionModel value) {
    this._typeInstitutionSelected = value;
    notifyListeners();
  }

  // Nama Lembaga
  TextEditingController get controllerInstitutionName {
    return this._controllerInstitutionName;
  }

  set controllerInstitutionName(value) {
    this._controllerInstitutionName = value;
    this.notifyListeners();
  }

  // Tanggal Pendirian
  TextEditingController get controllerDateOfEstablishment => _controllerDateOfEstablishment;

  DateTime get initialDateForDateOfEstablishment => _initialDateForDateOfEstablishment;

  void selectDateOfEstablishment(BuildContext context) async {
    DatePickerShared _datePickerShared = DatePickerShared();
    var _datePickerSelected = await _datePickerShared.selectStartDate(
        context, this._initialDateForDateOfEstablishment,
        canAccessNextDay: false);
    if (_datePickerSelected != null) {
      this.controllerDateOfEstablishment.text = dateFormat.format(_datePickerSelected);
      this._initialDateForDateOfEstablishment = _datePickerSelected;
      notifyListeners();
    } else {
      return;
    }
  }

  // NPWP
  TextEditingController get controllerNPWP {
    return this._controllerNPWP;
  }

  set controllerNPWP(value) {
    this._controllerNPWP = value;
    this.notifyListeners();
  }
}
