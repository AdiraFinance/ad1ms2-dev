import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/models/form_m_pekerjaan_model.dart';
import 'package:ad1ms2_dev/shared/search_sector_economic_change_notif.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';

import '../main.dart';
import 'constants.dart';
import 'form_m_company_rincian_change_notif.dart';

class SearchBusinessFieldChangeNotif with ChangeNotifier {
  bool _showClear = false;
  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController _controllerSearch = TextEditingController();
  // List<BusinessFieldModel> _listBusinessField = [
  //   BusinessFieldModel("01", "PERTANIAN PADI"),
  //   BusinessFieldModel("02", "PERTANIAN JAGUNG"),
  //   BusinessFieldModel("03", "PERTANIAN JERUK"),
  //   BusinessFieldModel("04", "PERTANIAN KACANG TANAH"),
  //   BusinessFieldModel("05", "PERKEBUNAN CENGKEH"),
  //   BusinessFieldModel("06", "PERKEBUNAN LADA"),
  //   BusinessFieldModel("07", "PERKEBUNAN COKLAT(KAKAO)"),
  //   BusinessFieldModel("08", "PERKEBUNAN KOPI"),
  // ];

  List<BusinessFieldModel> _listBusinessField = [];
  List<BusinessFieldModel> _listBusinessFieldTemp = [];

  TextEditingController get controllerSearch => _controllerSearch;

  bool get showClear => _showClear;

  set showClear(bool value) {
    this._showClear = value;
    notifyListeners();
  }

  void changeAction(String value) {
    if (value != "") {
      showClear = true;
    } else {
      showClear = false;
    }
  }

  UnmodifiableListView<BusinessFieldModel> get listBusinessField {
    return UnmodifiableListView(this._listBusinessField);
  }

  UnmodifiableListView<BusinessFieldModel> get listBusinessFieldTemp {
    return UnmodifiableListView(this._listBusinessFieldTemp);
  }

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
    notifyListeners();
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  void getBusinessField(String id) async{
    this._listBusinessField.clear();
    this._loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback = (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);
    var _body = jsonEncode({
      "P_ECONOMY_SECTOR_ID": id
    });

    var storage = FlutterSecureStorage();
    String _fieldLapanganUsaha = await storage.read(key: "FieldLapanganUsaha");
    final _response = await _http.post(
      "${BaseUrl.urlGeneral}$_fieldLapanganUsaha",
        // "${urlPublic}api/occupation/get-lapangan-usaha",
        body: _body,
        headers: {"Content-Type":"application/json", "Authorization":"bearer $token"}
    );

    if(_response.statusCode == 200){
      final _result = jsonDecode(_response.body);
      if(_result.isNotEmpty){
        for(int i = 0 ; i < _result.length ; i++) {
          _listBusinessField.add(BusinessFieldModel(_result[i]['KODE'], _result[i]['DESKRIPSI']));
        }
      }
      this._loadData = false;
    } else {
      showSnackBar("Error response status ${_response.statusCode}");
      this._loadData = false;
    }
    notifyListeners();
  }

  void searchBusinessField(String query) {
    if(query.length < 3) {
      showSnackBar("Input minimal 3 karakter");
    } else {
      _listBusinessFieldTemp.clear();
      if (query.isEmpty) {
        return;
      }

      _listBusinessField.forEach((dataBusinessField) {
        if (dataBusinessField.KODE.contains(query) || dataBusinessField.DESKRIPSI.contains(query)) {
          _listBusinessFieldTemp.add(dataBusinessField);
        }
      });
    }
    notifyListeners();
  }

  void clearSearchTemp() {
    _listBusinessFieldTemp.clear();
  }

  void showSnackBar(String text){
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"), behavior: SnackBarBehavior.floating, backgroundColor: snackbarColor));
  }
}
