import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/address_guarantor_model.dart';
import 'package:ad1ms2_dev/models/birth_place_model.dart';
import 'package:ad1ms2_dev/models/form_m_company_rincian_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';
import 'package:ad1ms2_dev/models/guarantor_idividual_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_addr_model.dart';
import 'package:ad1ms2_dev/models/ms2_grntr_comp_model.dart';
import 'package:ad1ms2_dev/models/ms2_grntr_ind_model.dart';
import 'package:ad1ms2_dev/screens/form_m_company/menu_management_pic.dart';
import 'package:ad1ms2_dev/shared/constants.dart';
import 'package:ad1ms2_dev/shared/form_m_add_guarantor_individual_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_add_guarantor_company_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_foto_change_notif.dart';
import 'package:ad1ms2_dev/shared/resource/submit_data_partial.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';
import 'change_notifier_app/info_application_change_notifier.dart';
import 'form_m_company_parent_change_notifier.dart';
import 'form_m_individu_change_notifier/form_m_parent_individu_change_notifier.dart';
import 'package:ad1ms2_dev/shared/form_m_info_nasabah_change_notif.dart';

class FormMGuarantorChangeNotifier with ChangeNotifier {
  int _radioValueIsWithGuarantor = 1;
  bool _autoValidate = false;
  List<GuarantorIndividualModel> _listGuarantorIndividual = [];
  String _custType;
  String _lastKnownState;
  SubmitDataPartial _submitDataPartial = SubmitDataPartial();
  bool _validationRadio = true;
  bool _isDisablePACIAAOSCONA = false;
  List<GuarantorCompanyModel> _listGuarantorCompany = [];
  DbHelper _dbHelper = DbHelper();

  int get radioValueIsWithGuarantor => _radioValueIsWithGuarantor;

  set radioValueIsWithGuarantor(int value) {
    this._radioValueIsWithGuarantor = value;
    notifyListeners();
  }

  List<GuarantorIndividualModel> get listGuarantorIndividual => _listGuarantorIndividual;
  List<GuarantorCompanyModel> get listGuarantorCompany => _listGuarantorCompany;

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  String get custType => _custType;

  set custType(String value) {
    this._custType = value;
    notifyListeners();
  }

  String get lastKnownState => _lastKnownState;

  set lastKnownState(String value) {
    this._lastKnownState = value;
    notifyListeners();
  }

  bool get isDisablePACIAAOSCONA => _isDisablePACIAAOSCONA;

  set isDisablePACIAAOSCONA(bool value) {
    this._isDisablePACIAAOSCONA = value;
    notifyListeners();
  }

  void addGuarantorIndividual(GuarantorIndividualModel model) {
    this._listGuarantorIndividual.add(model);
    if(this._autoValidate)autoValidate = false;
    notifyListeners();
  }

  void addGuarantorCompany(GuarantorCompanyModel model) {
    this._listGuarantorCompany.add(model);
    if(this._autoValidate)autoValidate = false;
    notifyListeners();
  }

  void deleteListGuarantorIndividual(BuildContext context, int index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                primaryColor: Colors.black,
                primarySwatch: primaryOrange,
                accentColor: myPrimaryColor
            ),
            child: AlertDialog(
              title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Apakah kamu yakin menghapus penjamin ini?",),
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    this._listGuarantorIndividual.removeAt(index);
                    notifyListeners();
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                ),
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Tidak'),
                ),
              ],
            ),
          );
        }
    );
  }

  void deleteListGuarantorCompany(BuildContext context, int index) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return Theme(
            data: ThemeData(
                fontFamily: "NunitoSans",
                primaryColor: Colors.black,
                primarySwatch: primaryOrange,
                accentColor: myPrimaryColor
            ),
            child: AlertDialog(
              title: Text("Warning", style: TextStyle(fontWeight: FontWeight.bold)),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Apakah kamu yakin menghapus penjamin ini?",),
                ],
              ),
              actions: <Widget>[
                new FlatButton(
                  onPressed: () {
                    this._listGuarantorCompany.removeAt(index);
                    notifyListeners();
                    Navigator.pop(context);
                  },
                  child: new Text('Ya', style: TextStyle(color: Colors.grey),),
                ),
                new FlatButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: new Text('Tidak'),
                ),
              ],
            ),
          );
        }
    );
  }

  void updateListGuarantorIndividual(GuarantorIndividualModel value, int index) {
    this._listGuarantorIndividual[index] = value;
    notifyListeners();
  }

  void updateListGuarantorCompany(GuarantorCompanyModel value, int index) {
    this._listGuarantorCompany[index] = value;
    notifyListeners();
  }

  // Future<void> clearDataGuarantor() async {
  void clearDataGuarantor() {
    this._autoValidate = false;
    this._radioValueIsWithGuarantor = 1;
    this._listGuarantorIndividual = [];
    this._listGuarantorCompany = [];
    this._listDataIndividu.clear();
    this._listDataCompany.clear();
    this._listAddressIndividu.clear();
    this._listAddressCompany.clear();
    isDisablePACIAAOSCONA = false;
  }

  List<MS2GrntrIndModel> _listDataIndividu = [];
  List<MS2GrntrCompModel> _listDataCompany = [];
  List<MS2CustAddrModel> _listAddressIndividu = [];
  List<MS2CustAddrModel> _listAddressCompany = [];

  List<MS2CustAddrModel> get listAddressIndividu => _listAddressIndividu;
  List<MS2CustAddrModel> get listAddressCompany => _listAddressCompany;

  List<MS2GrntrIndModel> get listDataIndividu => _listDataIndividu;
  List<MS2GrntrCompModel> get listDataCompany => _listDataCompany;

  //di ganti string
  Future<void> saveToSQLite(BuildContext context) async{
    this._listDataIndividu.clear();
    this._listDataCompany.clear();
    this._listAddressIndividu.clear();
    this._listAddressCompany.clear();

    for(int i=0; i<_listGuarantorIndividual.length;i++){
      _listDataIndividu.add(
          MS2GrntrIndModel(
            null,
            _listGuarantorIndividual[i].guarantorIndividualID,
            _listGuarantorIndividual[i].relationshipStatusModel.PARA_FAMILY_TYPE_ID,
            _listGuarantorIndividual[i].relationshipStatusModel.PARA_FAMILY_TYPE_NAME,
            0,
            _listGuarantorIndividual[i].identityModel.id,
            _listGuarantorIndividual[i].identityModel.name,
            _listGuarantorIndividual[i].identityNumber,
            _listGuarantorIndividual[i].fullNameIdentity,
            _listGuarantorIndividual[i].fullName,
            null,
            null,
            _listGuarantorIndividual[i].birthDate,
            _listGuarantorIndividual[i].birthPlaceIdentity1,
            _listGuarantorIndividual[i].birthPlaceIdentity2 != null ? _listGuarantorIndividual[i].birthPlaceIdentity2.KABKOT_ID : null,
            _listGuarantorIndividual[i].birthPlaceIdentity2 != null ? _listGuarantorIndividual[i].birthPlaceIdentity2.KABKOT_NAME : null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            _listGuarantorIndividual[i].gender != "" || _listGuarantorIndividual[i].gender != null ? _listGuarantorIndividual[i].gender : null,
            _listGuarantorIndividual[i].cellPhoneNumber != "" || _listGuarantorIndividual[i].cellPhoneNumber != null ? _listGuarantorIndividual[i].cellPhoneNumber : null,
            null,
            null,
            null,
            null,
            null,
            null,
            _listGuarantorIndividual[i].relationshipStatusDakor ? "1" : "0",
            _listGuarantorIndividual[i].identityTypeDakor ? "1" : "0",
            _listGuarantorIndividual[i].identityNoDakor ? "1" : "0",
            _listGuarantorIndividual[i].fullnameIDDakor ? "1" : "0",
            _listGuarantorIndividual[i].fullNameDakor ? "1" : "0",
            _listGuarantorIndividual[i].dateOfBirthDakor ? "1" : "0",
            _listGuarantorIndividual[i].placeOfBirthDakor ? "1" : "0",
            _listGuarantorIndividual[i].placeOfBirthLOVDakor ? "1" : "0",
            _listGuarantorIndividual[i].genderDakor ? "1" : "0",
            _listGuarantorIndividual[i].phoneDakor ? "1" : "0",
          )
      );
    }

    for(int i=0; i<_listGuarantorCompany.length;i++){
      _listDataCompany.add(MS2GrntrCompModel(
        null,
        _listGuarantorCompany[i].guarantorCorporateID,
        0,
        _listGuarantorCompany[i].typeInstitutionModel.PARA_ID,
        _listGuarantorCompany[i].typeInstitutionModel.PARA_NAME,
        null,
        null,
        _listGuarantorCompany[i].institutionName,
        null,
        null,
        null,
        null,
        null,
        null,
        _listGuarantorCompany[i].establishDate,
        null,
        null,
        null,
        null,
        null,
        _listGuarantorCompany[i].npwp,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        _listGuarantorCompany[i].companyTypeDakor ? "1" : "0",
        _listGuarantorCompany[i].companyNameDakor ? "1" : "0",
        _listGuarantorCompany[i].establishedDateDakor ? "1" : "0",
        _listGuarantorCompany[i].npwpDakor ? "1" : "0",
      )
      );
    }
    await _dbHelper.insertMS2GrntrInd(_listDataIndividu);
    await _dbHelper.insertMS2GrntrComp(_listDataCompany);
    await saveToSQLiteAddressGuarantor(context);
    // String _message = await saveToSQLiteAddressGuarantor(context,idx,_listDataIndividu, _listDataCompany);
    // if(isPersonal){
    //   _message = await _submitDataPartial.submitGuarantor(context,idx, _listDataIndividu, _listDataCompany, [], []);
    // }
    // else{
    //   _message = await saveToSQLiteAddressGuarantor(context,idx,_listDataIndividu, _listDataCompany);
    // }
    // return _message;
  }

  Future<void> saveToSQLiteAddressGuarantor(BuildContext context) async{
    //alamat guarantor individu
    for(int i=0; i<_listGuarantorIndividual.length; i++){
      for(int j=0; j<_listGuarantorIndividual[i].listAddressGuarantorModel.length; j++){
        _listAddressIndividu.add(MS2CustAddrModel(
          "123",
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].addressID,
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].foreignBusinessID,
          _listGuarantorIndividual[i].identityNumber,
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].isCorrespondence ? "1" : "0",
          _custType == "PER"? "1" : "7",
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].address,
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].rt,
          null,
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].rw,
          null,
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.PROV_ID,
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.PROV_NAME,
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_ID,
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_NAME,
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KEC_ID,
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KEC_NAME,
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KEL_ID,
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KEL_NAME,
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.ZIPCODE,
          null,
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].phone,
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].areaCode,
          null,
          null,
          null,
          null,
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].jenisAlamatModel.KODE,
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].jenisAlamatModel.DESKRIPSI,
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].addressLatLong != null ? _listGuarantorIndividual[i].listAddressGuarantorModel[j].addressLatLong['latitude'].toString() : null,
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].addressLatLong != null ? _listGuarantorIndividual[i].listAddressGuarantorModel[j].addressLatLong['longitude'].toString() : null,
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].addressLatLong != null ? _listGuarantorIndividual[i].listAddressGuarantorModel[j].addressLatLong['address'].toString() : null,
          null,
          null,
          null,
          null,
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].active,
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].isAddressTypeChanges ? "1" : "0",
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].isAlamatChanges ? "1" : "0",
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].isRTChanges ? "1" : "0",
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].isRWChanges ? "1" : "0",
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].isKelurahanChanges ? "1" : "0",
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].isKecamatanChanges ? "1" : "0",
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].isKotaChanges ? "1" : "0",
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].isProvinsiChanges ? "1" : "0",
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].isPostalCodeChanges ? "1" : "0",
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].isTeleponAreaChanges ? "1" : "0",
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].isTeleponChanges ? "1" : "0",
          null,
          null,
          null,
          null,
          _listGuarantorIndividual[i].listAddressGuarantorModel[j].isAddressFromMapChanges ? "1" : "0",
        ));
      }
    }

    //alamat guarantor com
    for(int i=0; i<_listGuarantorCompany.length; i++){
      for(int j=0; j<_listGuarantorCompany[i].listAddressGuarantorModel.length; j++){
        _listAddressCompany.add(MS2CustAddrModel(
          "123",
          _listGuarantorCompany[i].listAddressGuarantorModel[j].addressID,
          _listGuarantorCompany[i].listAddressGuarantorModel[j].foreignBusinessID,
          _listGuarantorCompany[i].npwp,
          _listGuarantorCompany[i].listAddressGuarantorModel[j].isCorrespondence ? "1" : "0",
          _custType == "PER" ? "2" : "8",
          _listGuarantorCompany[i].listAddressGuarantorModel[j].address,
          _listGuarantorCompany[i].listAddressGuarantorModel[j].rt,
          null,
          _listGuarantorCompany[i].listAddressGuarantorModel[j].rw,
          null,
          _listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.PROV_ID,
          _listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.PROV_NAME,
          _listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_ID,
          _listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_NAME,
          _listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEC_ID,
          _listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEC_NAME,
          _listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEL_ID,
          _listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEL_NAME,
          _listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.ZIPCODE,
          null,
          _listGuarantorCompany[i].listAddressGuarantorModel[j].phone1,
          _listGuarantorCompany[i].listAddressGuarantorModel[j].phoneArea1,
          _listGuarantorCompany[i].listAddressGuarantorModel[j].phone2,
          _listGuarantorCompany[i].listAddressGuarantorModel[j].phoneArea2,
          _listGuarantorCompany[i].listAddressGuarantorModel[j].fax,
          _listGuarantorCompany[i].listAddressGuarantorModel[j].faxArea,
          _listGuarantorCompany[i].listAddressGuarantorModel[j].jenisAlamatModel.KODE,
          _listGuarantorCompany[i].listAddressGuarantorModel[j].jenisAlamatModel.DESKRIPSI,
          _listGuarantorCompany[i].listAddressGuarantorModel[j].addressLatLong != null ?_listGuarantorCompany[i].listAddressGuarantorModel[j].addressLatLong['latitude'].toString() : null,
          _listGuarantorCompany[i].listAddressGuarantorModel[j].addressLatLong != null ?_listGuarantorCompany[i].listAddressGuarantorModel[j].addressLatLong['longitude'].toString() : null,
          _listGuarantorCompany[i].listAddressGuarantorModel[j].addressLatLong != null ?_listGuarantorCompany[i].listAddressGuarantorModel[j].addressLatLong['address'].toString() : null,
          null,
          null,
          null,
          null,
          _listGuarantorCompany[i].listAddressGuarantorModel[j].active,
          _listGuarantorCompany[i].listAddressGuarantorModel[j].isAddressTypeDakor ? "1" : "0",
          _listGuarantorCompany[i].listAddressGuarantorModel[j].isAddressDakor ? "1" : "0",
          _listGuarantorCompany[i].listAddressGuarantorModel[j].isRTDakor ? "1" : "0",
          _listGuarantorCompany[i].listAddressGuarantorModel[j].isRWDakor ? "1" : "0",
          _listGuarantorCompany[i].listAddressGuarantorModel[j].isKelurahanDakor ? "1" : "0",
          _listGuarantorCompany[i].listAddressGuarantorModel[j].isKecamatanDakor ? "1" : "0",
          _listGuarantorCompany[i].listAddressGuarantorModel[j].isKabKotDakor ? "1" : "0",
          _listGuarantorCompany[i].listAddressGuarantorModel[j].isProvinsiDakor ? "1" : "0",
          _listGuarantorCompany[i].listAddressGuarantorModel[j].isKodePosDakor ? "1" : "0",
          _listGuarantorCompany[i].listAddressGuarantorModel[j].isTelp1AreaDakor ? "1" : "0",
          _listGuarantorCompany[i].listAddressGuarantorModel[j].isTelp1Dakor ? "1" : "0",
          _listGuarantorCompany[i].listAddressGuarantorModel[j].isTelp2AreaDakor ? "1" : "0",
          _listGuarantorCompany[i].listAddressGuarantorModel[j].isTelp2Dakor ? "1" : "0",
          _listGuarantorCompany[i].listAddressGuarantorModel[j].isFaxAreaDakor ? "1" : "0",
          _listGuarantorCompany[i].listAddressGuarantorModel[j].isFaxDakor ? "1" : "0",
          _listGuarantorCompany[i].listAddressGuarantorModel[j].isDataLatLongDakor ? "1" : "0",
        ));
      }
    }
    _dbHelper.insertMS2CustAddr(_listAddressIndividu);
    _dbHelper.insertMS2CustAddr(_listAddressCompany);
    // String _message = await _submitDataPartial.submitGuarantor(context,idx, listDataIndividu, listDataCompany, _listAddressIndividu, _listAddressCompany);
    // return _message;
  }

  // void saveToSQLite(){
  //   List<MS2GrntrIndModel> _listDataIndividu = [];
  //   List<MS2GrntrCompModel> _listDataCompany = [];
  //   for(int i=0; i<_listGuarantorIndividual.length;i++){
  //     _listDataIndividu.add(MS2GrntrIndModel(
  //        null,
  //         _listGuarantorIndividual[i].relationshipStatusModel.PARA_FAMILY_TYPE_ID,
  //         _listGuarantorIndividual[i].relationshipStatusModel.PARA_FAMILY_TYPE_NAME,
  //        0,
  //         _listGuarantorIndividual[i].identityModel.id,
  //         _listGuarantorIndividual[i].identityModel.name,
  //         _listGuarantorIndividual[i].identityNumber,
  //         _listGuarantorIndividual[i].fullNameIdentity,
  //         _listGuarantorIndividual[i].fullName,
  //        null,
  //        null,
  //        _listGuarantorIndividual[i].birthDate.toString(),
  //        _listGuarantorIndividual[i].birthPlaceIdentity1,
  //       _listGuarantorIndividual[i].birthPlaceIdentity2 != null ? _listGuarantorIndividual[i].birthPlaceIdentity2.KABKOT_ID : null,
  //       _listGuarantorIndividual[i].birthPlaceIdentity2 != null ? _listGuarantorIndividual[i].birthPlaceIdentity2.KABKOT_NAME : null,
  //        null,
  //        null,
  //        null,
  //        null,
  //        null,
  //        null,
  //        null,
  //        _listGuarantorIndividual[i].gender != "" || _listGuarantorIndividual[i].gender != null ? _listGuarantorIndividual[i].gender : null,
  //        _listGuarantorIndividual[i].cellPhoneNumber != "" || _listGuarantorIndividual[i].cellPhoneNumber != null ? _listGuarantorIndividual[i].cellPhoneNumber : null,
  //        null,
  //        null,
  //        null,
  //        null,
  //        null,
  //        null,
  //        _listGuarantorIndividual[i].relationshipStatusDakor ? "1" : "0",
  //        _listGuarantorIndividual[i].identityTypeDakor ? "1" : "0",
  //        _listGuarantorIndividual[i].identityNoDakor ? "1" : "0",
  //        _listGuarantorIndividual[i].fullnameIDDakor ? "1" : "0",
  //        _listGuarantorIndividual[i].fullNameDakor ? "1" : "0",
  //        _listGuarantorIndividual[i].dateOfBirthDakor ? "1" : "0",
  //        _listGuarantorIndividual[i].placeOfBirthDakor ? "1" : "0",
  //        _listGuarantorIndividual[i].placeOfBirthLOVDakor ? "1" : "0",
  //        _listGuarantorIndividual[i].genderDakor ? "1" : "0",
  //        _listGuarantorIndividual[i].phoneDakor ? "1" : "0",
  //       )
  //     );
  //   }
  //   for(int i=0; i<_listGuarantorCompany.length;i++){
  //       print("bolean ${_listGuarantorCompany[i].companyTypeDakor} - ${_listGuarantorCompany[i].companyNameDakor} -  ${_listGuarantorCompany[i].establishedDateDakor} - ${_listGuarantorCompany[i].npwpDakor}");
  //     _listDataCompany.add(MS2GrntrCompModel(
  //       null,
  //       0,
  //       _listGuarantorCompany[i].typeInstitutionModel.PARA_ID,
  //       _listGuarantorCompany[i].typeInstitutionModel.PARA_NAME,
  //       null,
  //       null,
  //       _listGuarantorCompany[i].institutionName,
  //       null,
  //       null,
  //       null,
  //       null,
  //       null,
  //       null,
  //       _listGuarantorCompany[i].establishDate,
  //       null,
  //       null,
  //       null,
  //       null,
  //       null,
  //       _listGuarantorCompany[i].npwp,
  //       null,
  //       null,
  //       null,
  //       null,
  //       null,
  //       null,
  //       null,
  //       null,
  //       null,
  //       null,
  //       null,
  //       null,
  //       null,
  //       null,
  //       null,
  //       null,
  //       null,
  //       null,
  //       null,
  //       _listGuarantorCompany[i].companyTypeDakor ? "1" : "0",
  //       _listGuarantorCompany[i].companyNameDakor ? "1" : "0",
  //       _listGuarantorCompany[i].establishedDateDakor ? "1" : "0",
  //       _listGuarantorCompany[i].npwpDakor ? "1" : "0",
  //     )
  //     );
  //   }
  //   _dbHelper.insertMS2GrntrInd(_listDataIndividu);
  //   _dbHelper.insertMS2GrntrComp(_listDataCompany);
  // }
  //
  // void saveToSQLiteAddressGuarantor(){
  //   List<MS2CustAddrModel> _listAddressIndividu = [];
  //   List<MS2CustAddrModel> _listAddressCompany = [];
  //   for(int i=0; i<_listGuarantorIndividual.length; i++){
  //     for(int j=0; j<_listGuarantorIndividual[i].listAddressGuarantorModel.length; j++){
  //       _listAddressIndividu.add(MS2CustAddrModel(
  //           "123",
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].isCorrespondence.toString(),
  //           _custType == "PER"? "1" : "7",
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].address,
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].rt,
  //           null,
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].rw,
  //           null,
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.PROV_ID,
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.PROV_NAME,
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_ID,
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_NAME,
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KEC_ID,
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KEC_NAME,
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KEL_ID,
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.KEL_NAME,
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].kelurahanModel.ZIPCODE,
  //           null,
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].phone,
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].areaCode,
  //           null,
  //           null,
  //           null,
  //           null,
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].jenisAlamatModel.KODE,
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].jenisAlamatModel.DESKRIPSI,
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].addressLatLong['latitude'].toString(),
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].addressLatLong['longitude'].toString(),
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].addressLatLong['address'].toString(),
  //           null,
  //           null,
  //           null,
  //           null,
  //           1,
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].isAddressTypeChanges ? "1" : "0",
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].isAlamatChanges ? "1" : "0",
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].isRTChanges ? "1" : "0",
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].isRWChanges ? "1" : "0",
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].isKelurahanChanges ? "1" : "0",
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].isKecamatanChanges ? "1" : "0",
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].isKotaChanges ? "1" : "0",
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].isProvinsiChanges ? "1" : "0",
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].isPostalCodeChanges ? "1" : "0",
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].isTeleponAreaChanges ? "1" : "0",
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].isTeleponChanges ? "1" : "0",
  //           null,
  //           null,
  //           null,
  //           null,
  //           _listGuarantorIndividual[i].listAddressGuarantorModel[j].isAddressFromMapChanges ? "1" : "0",
  //       ));
  //     }
  //   }
  //
  //   for(int i=0; i<_listGuarantorCompany.length; i++){
  //     for(int j=0; j<_listGuarantorCompany[i].listAddressGuarantorModel.length; i++){
  //       _listAddressCompany.add(MS2CustAddrModel(
  //           "123",
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].isCorrespondence.toString(),
  //           _custType == "PER" ? "2" : "8",
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].address,
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].rt,
  //           null,
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].rw,
  //           null,
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.PROV_ID,
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.PROV_NAME,
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_ID,
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KABKOT_NAME,
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEC_ID,
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEC_NAME,
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEL_ID,
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.KEL_NAME,
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].kelurahanModel.ZIPCODE,
  //           null,
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].phone1,
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].phoneArea1,
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].phone2,
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].phoneArea2,
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].fax,
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].faxArea,
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].jenisAlamatModel.KODE,
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].jenisAlamatModel.DESKRIPSI,
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].addressLatLong['latitude'].toString(),
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].addressLatLong['longitude'].toString(),
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].addressLatLong['address'].toString(),
  //           null,
  //           null,
  //           null,
  //           null,
  //           1,
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].isAddressTypeDakor ? "1" : "0",
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].isAddressDakor ? "1" : "0",
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].isRTDakor ? "1" : "0",
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].isRWDakor ? "1" : "0",
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].isKelurahanDakor ? "1" : "0",
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].isKecamatanDakor ? "1" : "0",
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].isKabKotDakor ? "1" : "0",
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].isProvinsiDakor ? "1" : "0",
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].isKodePosDakor ? "1" : "0",
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].isTelp1AreaDakor ? "1" : "0",
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].isTelp1Dakor ? "1" : "0",
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].isTelp2AreaDakor ? "1" : "0",
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].isTelp2Dakor ? "1" : "0",
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].isFaxAreaDakor ? "1" : "0",
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].isFaxDakor ? "1" : "0",
  //           _listGuarantorCompany[i].listAddressGuarantorModel[j].isDataLatLongDakor ? "1" : "0",
  //       ));
  //     }
  //   }
  //   _dbHelper.insertMS2CustAddr(_listAddressIndividu);
  //   _dbHelper.insertMS2CustAddr(_listAddressCompany);
  // }

  Future<bool> deleteSQLite() async{
    if(await _dbHelper.deleteMS2GrntrComp() && await _dbHelper.deleteMS2GrntrInd() && await _dbHelper.deleteMS2CustAddr(_custType == "PER" ? "1" : "7") && await _dbHelper.deleteMS2CustAddr(_custType == "PER" ? "2" : "8")){
      return true;
    }else{
      return false;
    }
  }
  
  Future<void>  setDataFromSQLite(BuildContext context, int index, int type) async{
    //tipe 1 = per, 2 = com
    await setCustType(context);
    List _dataIndividu = await _dbHelper.selectGuarantorInd();
    List _dataCompany = await _dbHelper.selectGuarantorCom();
    List _addressIndividu = await _dbHelper.selectMS2CustAddr(_custType == "PER" ? "1" : "7");
    List _addressCompany = await _dbHelper.selectMS2CustAddr(_custType == "PER" ? "2" : "8");
    List<AddressModel> _listAddress = [];
    List<AddressModelCompany> _listAddressCom = [];

    if(_addressIndividu.isNotEmpty){
      for(int i=0; i<_addressIndividu.length; i++){
        var jenisAlamatModel = JenisAlamatModel(_addressIndividu[i]['addr_type'], _addressIndividu[i]['addr_desc']);
        var kelurahanModel = KelurahanModel(
            _addressIndividu[i]['kelurahan'],
            _addressIndividu[i]['kelurahan_desc'],
            _addressIndividu[i]['kecamatan_desc'],
            _addressIndividu[i]['kabkot_desc'],
            _addressIndividu[i]['provinsi_desc'],
            _addressIndividu[i]['zip_code'],
            _addressIndividu[i]['kecamatan'],
            _addressIndividu[i]['kabkot'],
            _addressIndividu[i]['provinsi']);
        var _addressFromMap = {
          "address": _addressIndividu[i]['address_from_map'] != "" && _addressIndividu[i]['address_from_map'] != "null" ? _addressIndividu[i]['address_from_map'] : '',
          "latitude": _addressIndividu[i]['latitude'],
          "longitude": _addressIndividu[i]['longitude']
        };
        var _koresponden = _addressIndividu[i]['koresponden'].toString().toLowerCase();
        _listAddress.add(AddressModel(
            jenisAlamatModel,
            _addressIndividu[i]['addressID'],
            _addressIndividu[i]['foreignBusinessID'],
            _addressIndividu[i]['id_no'],
            kelurahanModel,
            _addressIndividu[i]['address'] != "" && _addressIndividu[i]['address'] != "null" ? _addressIndividu[i]['address'] : '',
            _addressIndividu[i]['rt'] != "" && _addressIndividu[i]['rt'] != "null" ? _addressIndividu[i]['rt'] : '',
            _addressIndividu[i]['rw'] != "" && _addressIndividu[i]['rw'] != "null" ? _addressIndividu[i]['rw'] : '',
            _addressIndividu[i]['phone1_area'] != "" && _addressIndividu[i]['phone1_area'] != "null" ? _addressIndividu[i]['phone1_area'] : '',
            _addressIndividu[i]['phone1'] != "" && _addressIndividu[i]['phone1'] != "null" ? _addressIndividu[i]['phone1'] : '',
            false,
            _addressFromMap,
            _koresponden == '1' ? true : false,
            _addressIndividu[i]['active'],
            _addressIndividu[i]['edit_address'] == "1" ||
            _addressIndividu[i]['edit_addr_type'] == "1" ||
            _addressIndividu[i]['edit_rt'] == "1" ||
            _addressIndividu[i]['edit_rw'] == "1" ||
            _addressIndividu[i]['edit_kelurahan'] == "1" ||
            _addressIndividu[i]['edit_kecamatan'] == "1" ||
            _addressIndividu[i]['edit_kabkot'] == "1" ||
            _addressIndividu[i]['edit_provinsi'] == "1" ||
            _addressIndividu[i]['edit_zip_code'] == "1" ||
            _addressIndividu[i]['edit_address_from_map'] == "1" ||
            _addressIndividu[i]['edit_phone1_area'] == "1" ||
            _addressIndividu[i]['edit_phone1'] == "1" ? true : false,
            _addressIndividu[i]['edit_address'] == "1",
            _addressIndividu[i]['edit_addr_type'] == "1",
            _addressIndividu[i]['edit_rt'] == "1",
            _addressIndividu[i]['edit_rw'] == "1",
            _addressIndividu[i]['edit_kelurahan'] == "1",
            _addressIndividu[i]['edit_kecamatan'] == "1",
            _addressIndividu[i]['edit_kabkot'] == "1",
            _addressIndividu[i]['edit_provinsi'] == "1",
            _addressIndividu[i]['edit_zip_code'] == "1",
            _addressIndividu[i]['edit_address_from_map'] == "1",
            _addressIndividu[i]['edit_phone1_area'] == "1",
            _addressIndividu[i]['edit_phone1'] == "1",
        ));
        // if(_addressIndividu[i]['koresponden'] == "1"){
        //   Provider.of<FormMAddGuarantorIndividualChangeNotifier>(context, listen: false).selectedIndex = i;
        //   Provider.of<FormMAddGuarantorIndividualChangeNotifier>(context, listen: false).setCorrespondenceAddress(_listAddress[i], i);
        // }
      }
    }

    if(_addressCompany.isNotEmpty){
      for(int i=0; i<_addressCompany.length; i++){
        print("CEK_DATA_ADDRESS_ID_NO_LEMBAGA ${_addressCompany[i]['id_no']}");
        var jenisAlamatModel = JenisAlamatModel(_addressCompany[i]['addr_type'], _addressCompany[i]['addr_desc']);

        var kelurahanModel = KelurahanModel(_addressCompany[i]['kelurahan'], _addressCompany[i]['kelurahan_desc'],
            _addressCompany[i]['kecamatan_desc'], _addressCompany[i]['kabkot_desc'], _addressCompany[i]['provinsi_desc'], _addressCompany[i]['zip_code'],
            _addressCompany[i]['kecamatan'], _addressCompany[i]['kabkot'], _addressCompany[i]['provinsi']);

        var _addressFromMap = {
          "address": _addressCompany[i]['address_from_map'] != "" && _addressCompany[i]['address_from_map'] != "null" ? _addressCompany[i]['address_from_map'] : '',
          "latitude": _addressCompany[i]['latitude'],
          "longitude": _addressCompany[i]['longitude']
        };
        var _koresponden = _addressCompany[i]['koresponden'].toString().toLowerCase();
        _listAddressCom.add(AddressModelCompany(
          jenisAlamatModel,
          _addressCompany[i]['addressID'],
          _addressCompany[i]['foreignBusinessID'],
          _addressCompany[i]['id_no'],
          kelurahanModel,
          _addressCompany[i]['address'].toString(),
          _addressCompany[i]['rt'].toString(),
          _addressCompany[i]['rw'].toString(),
          _addressCompany[i]['phone1_area'].toString(),
          _addressCompany[i]['phone1'].toString(),
          _addressCompany[i]['phone_2_area'].toString(),
          _addressCompany[i]['phone_2'].toString(),
          _addressCompany[i]['fax_area'].toString(),
          _addressCompany[i]['fax'].toString(),
          false,
          _addressFromMap,
          _koresponden == '1' ? true : false,
          _addressCompany[i]['active'],
          _addressCompany[i]['edit_addr_type'] == "1" ||
          _addressCompany[i]['edit_address'] == "1" ||
          _addressCompany[i]['edit_rt'] == "1" ||
          _addressCompany[i]['edit_rw'] == "1" ||
          _addressCompany[i]['edit_kelurahan'] == "1" ||
          _addressCompany[i]['edit_kecamatan'] == "1" ||
          _addressCompany[i]['edit_kabkot'] == "1" ||
          _addressCompany[i]['edit_provinsi'] == "1" ||
          _addressCompany[i]['edit_zip_code'] == "1" ||
          _addressCompany[i]['edit_phone1_area'] == "1" ||
          _addressCompany[i]['edit_phone1'] == "1" ||
          _addressCompany[i]['edit_phone_2_area'] == "1" ||
          _addressCompany[i]['edit_phone_2'] == "1" ||
          _addressCompany[i]['edit_fax_area'] == "1" ||
          _addressCompany[i]['edit_fax'] == "1" ||
          _addressCompany[i]['edit_address_from_map'] == "1" ? true : false,
          _addressCompany[i]['edit_addr_type'] == "1",
          _addressCompany[i]['edit_address'] == "1",
          _addressCompany[i]['edit_rt'] == "1",
          _addressCompany[i]['edit_rw'] == "1",
          _addressCompany[i]['edit_kelurahan'] == "1",
          _addressCompany[i]['edit_kecamatan'] == "1",
          _addressCompany[i]['edit_kabkot'] == "1",
          _addressCompany[i]['edit_provinsi'] == "1",
          _addressCompany[i]['edit_zip_code'] == "1",
          _addressCompany[i]['edit_phone1_area'] == "1",
          _addressCompany[i]['edit_phone1'] == "1",
          _addressCompany[i]['edit_phone_2_area'] == "1",
          _addressCompany[i]['edit_phone_2'] == "1",
          _addressCompany[i]['edit_fax_area'] == "1",
          _addressCompany[i]['edit_fax'] == "1",
          _addressCompany[i]['edit_address_from_map'] == "1",
        )
        );
        // if(_addressCompany[i]['koresponden'] == "1"){
        //   Provider.of<FormMAddGuarantorCompanyChangeNotifier>(context, listen: false).selectedIndex = i;
        //   Provider.of<FormMAddGuarantorCompanyChangeNotifier>(context, listen: false).setCorrespondenceAddress(_addressIndividu[i], i);
        // }
      }
    }

    if(_dataIndividu.isNotEmpty){
      this._radioValueIsWithGuarantor = 0;
      _listGuarantorIndividual.clear();
      for(int i=0; i<_dataIndividu.length; i++){
        List<AddressModel> _listAddressGuarantorInd = [];
        print("panjang alamat individu penjamn ${_listAddress.length} == GET DATA ${_addressIndividu.length}");
        for(int j=0; j<_listAddress.length; j++){
          print("CEK_GUARANTOR_ADDRESS_INDIVIDU ${_dataIndividu[i]['id_no']} ${_listAddress[j].numberID}");
          if(_dataIndividu[i]['id_no'] == _listAddress[j].numberID){
            _listAddressGuarantorInd.add(_listAddress[j]);
          }
        }
        this._listGuarantorIndividual.add(
            GuarantorIndividualModel(
                RelationshipStatusModel(_dataIndividu[i]['relation_status'], _dataIndividu[i]['relation_status_desc']),
                IdentityModel(_dataIndividu[i]['id_type'], _dataIndividu[i]['id_desc']),
                _dataIndividu[i]['id_no'],
                _dataIndividu[i]['full_name_id'], 
                _dataIndividu[i]['full_name'],
                _dataIndividu[i]['date_of_birth'],
                _dataIndividu[i]['place_of_birth'],
                BirthPlaceModel(_dataIndividu[i]['place_of_birth_kabkota'],_dataIndividu[i]['place_of_birth_kabkota_desc']),
                _listAddressGuarantorInd,
                _dataIndividu[i]['handphone_no'],
                _dataIndividu[i]['gender'],
                _dataIndividu[i]['edit_relation_status_desc'] == "1" || _dataIndividu[i]['edit_id_type'] == "1" ||
                _dataIndividu[i]['edit_id_no'] == "1" || _dataIndividu[i]['edit_full_name_id'] == "1" ||
                _dataIndividu[i]['edit_full_name'] == "1" || _dataIndividu[i]['edit_date_of_birth'] == "1" ||
                _dataIndividu[i]['edit_place_of_birth'] == "1" || _dataIndividu[i]['edit_place_of_birth_kabkota'] == "1" ||
                _dataIndividu[i]['edit_gender'] == "1" || _dataIndividu[i]['edit_handphone_no'] == "1",
                _dataIndividu[i]['edit_relation_status_desc'] == "1",
                _dataIndividu[i]['edit_id_type'] == "1",
                _dataIndividu[i]['edit_id_no'] == "1",
                _dataIndividu[i]['edit_full_name_id'] == "1",
                _dataIndividu[i]['edit_full_name'] == "1",
                _dataIndividu[i]['edit_date_of_birth'] == "1",
                _dataIndividu[i]['edit_place_of_birth'] == "1",
                _dataIndividu[i]['edit_place_of_birth_kabkota'] == "1",
                _dataIndividu[i]['edit_gender'] == "1",
                _dataIndividu[i]['edit_handphone_no'] == "1",
                _dataIndividu[i]['guarantorIndividualID']
            )
        );
      }
    }

    if(_dataCompany.isNotEmpty){
      this._radioValueIsWithGuarantor = 0;
      _listGuarantorCompany.clear();
      for(int i=0; i < _dataCompany.length; i++){
        print("CEK_DATA_EST_DATE ${ _dataCompany[i]['establish_date']} == ${_listAddressCom.length}");
        List<AddressModelCompany> _listAddressGuarantor = [];
        for(int j=0; j<_listAddressCom.length; j++){
          debugPrint("CEK_DATA_ADDRESS_PENJAMIN_LEMBAGA ${_dataCompany[i]['npwp_no'] == _listAddressCom[j].numberID}");
          if(_dataCompany[i]['npwp_no'] == _listAddressCom[j].numberID){
            _listAddressGuarantor.add(_listAddressCom[j]);
          }
        }
        this._listGuarantorCompany.add(
            GuarantorCompanyModel(
                TypeInstitutionModel(_dataCompany[i]['comp_type'], _dataCompany[i]['comp_desc']),
                ProfilModel(_dataCompany[i]['comp_type'], _dataCompany[i]['comp_desc']),
                _dataCompany[i]['establish_date'],
                _dataCompany[i]['comp_name'],
                _dataCompany[i]['npwp_no'],
                _listAddressGuarantor,
                _dataCompany[i]['edit_comp_type'] == "1" || _dataCompany[i]['edit_establish_date'] == "1" ||
                _dataCompany[i]['edit_comp_name'] == "1" || _dataCompany[i]['edit_npwp_no'] == "1",
                _dataCompany[i]['edit_comp_type'] == "1",
                _dataCompany[i]['edit_comp_name'] == "1",
                _dataCompany[i]['edit_establish_date'] == "1",
                _dataCompany[i]['edit_npwp_no'] == "1",_dataCompany[i]['guarantorCorporateID']
            )
        );
      }
    }

    if(this._lastKnownState == "IDE" && index != null){
      if(type == 1){
        var _providerInfoApp = Provider.of<InfoAppChangeNotifier>(context, listen: false);
        if(index != 4){
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).isGuarantorDone = true;
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).selectedIndex +=1;
          await _providerInfoApp.addNumberOfUnitList(context, index);
        }
        else{
          Provider.of<FormMParentIndividualChangeNotifier>(context,listen: false).selectedIndex = 4;
        }
      }
      else {
        MenuManagementPIC _menuPIC = MenuManagementPIC();
        if(index != 2){
          Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).isPenjamin = true;
          Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).selectedIndex +=1;
          await _menuPIC.setNextState(context, index);
        }
        else{
          Provider.of<FormMParentCompanyChangeNotifier>(context,listen: false).selectedIndex = 2;
        }
      }
    }
    notifyListeners();
  }

  Future<void> setCustType(BuildContext context) async{
    SharedPreferences _preference = await SharedPreferences.getInstance();
    this._custType = _preference.getString("cust_type");
    this._lastKnownState = _preference.getString("last_known_state");
    if(Provider.of<FormMInfoNasabahChangeNotif>(context, listen: false).isGuarantorVisible){
        radioValueIsWithGuarantor = 0;
    }
    if(this._lastKnownState == "AOS" || this._lastKnownState == "CONA" || this._lastKnownState == "IA" || this._lastKnownState == "PAC") {
      this._isDisablePACIAAOSCONA = true;
    }
    checkNeedGuarantor(context);
  }

  bool get validationRadio => _validationRadio;

  set validationRadio(bool value) {
    this._validationRadio = value;
  }

  void checkNeedGuarantor(BuildContext context){
    this._validationRadio = true;
    var _providerPhoto = Provider.of<FormMFotoChangeNotifier>(context, listen: false);
    for(int i=0; i<_providerPhoto.listGroupUnitObject.length; i++){
      print("cek grup objek = ${_providerPhoto.listGroupUnitObject[i].groupObjectUnit.id}");
      if(_providerPhoto.listGroupUnitObject[i].groupObjectUnit.id == "003"){
        print("halo");
        this._validationRadio = false;
      }
    }
  }

}
