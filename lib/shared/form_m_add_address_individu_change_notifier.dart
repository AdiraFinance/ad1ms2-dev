import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:ad1ms2_dev/db/database_helper.dart';
import 'package:ad1ms2_dev/models/address_individu/set_address_individu.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';
import 'package:ad1ms2_dev/models/ms2_cust_addr_model.dart';
import 'package:ad1ms2_dev/screens/form_m/search_kelurahan.dart';
import 'package:ad1ms2_dev/shared/dashboard/dashboard_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_add_pemegang_saham_pribadi_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/form_m_company_manajemen_pic_alamat_change_notif.dart';
import 'package:ad1ms2_dev/screens/map_page.dart';
import 'package:ad1ms2_dev/shared/form_m_informasi_alamat_change_notif.dart';
import 'package:ad1ms2_dev/shared/resource/address_type_resource.dart';
import 'package:ad1ms2_dev/shared/search_kelurahan_change_notif.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/io_client.dart';
import 'package:provider/provider.dart';
import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';
import 'add_pemegang_saham_pribadi_change_notif.dart';
import 'form_m_add_guarantor_individual_change_notifier.dart';
import 'form_m_company_pemegang_saham_pribadi_alamat_change_notif.dart';
import 'form_m_occupation_change_notif.dart';

class FormMAddAddressIndividuChangeNotifier with ChangeNotifier {
  bool _autoValidate = false;
  TextEditingController _controllerAlamat = TextEditingController();
  TextEditingController _controllerRT = TextEditingController();
  TextEditingController _controllerRW = TextEditingController();
  TextEditingController _controllerKelurahan = TextEditingController();
  TextEditingController _controllerKecamatan = TextEditingController();
  TextEditingController _controllerKota = TextEditingController();
  TextEditingController _controllerProvinsi = TextEditingController();
  TextEditingController _controllerKodeArea = TextEditingController();
  TextEditingController _controllerTlpn = TextEditingController();
  TextEditingController _controllerPostalCode = TextEditingController();
  TextEditingController _controllerAddressFromMap = TextEditingController();
  Map _addressFromMap;
  bool _isSameWithIdentity = false;
  int _active = 0;
  JenisAlamatModel _jenisAlamatSelected;
  JenisAlamatModel _jenisAlamatSelectedTemp;
  KelurahanModel _kelurahanSelected;
  KelurahanModel _kelurahanSelectedTemp;
  String _alamatTemp,
      _rtTemp,
      _rwTemp,
      _kelurahanTemp,
      _kecamatanTemp,
      _kotaTemp,
      _provinsiTemp,
      _areaCodeTemp,
      _phoneTemp,
      _postalCodeTemp;
  bool _enableTfAddress = true;
  bool _enableTfRT = true;
  bool _enableTfRW = true;
  bool _enableTfKelurahan = true;
  bool _enableTfKecamatan = true;
  bool _enableTfKota = true;
  bool _enableTfProv = true;
  bool _enableTfPostalCode = true;
  bool _enableTfAreaCode = true;
  bool _enableTfPhone = true;
  bool _isCorrespondence;
  DbHelper _dbHelper = DbHelper();
  SetAddressIndividu _setAddressIndividuModelOcupation, _setAddressIndividuModelInfoNasabah, _setAddressIndividuPenjamin, _setAddressIndividuPenjaminCompany, _setAddressIndividuManajemenPIC, _setAddressIndividuPemegangSaham;
  var _custType;
  String _addressID = "NEW";
  String _foreignBusinessID = "NEW";
  bool _disableJenisPenawaran = false;
  bool _isDisablePACIAAOSCONA = false;

//  typeAddres 1 = list alamat info nasabah
//  typeAddres 2 = list alamat pekerjaan
//  typeAddres 3 = list alamat penjamin pribadi
//  typeAddres 4 = list alamat manajemen pic
//  typeAddres 5 = list alamat saham pribadi

  bool _isEditAddress = false;
  bool _isAlamatChanges = false;
  bool _isAddressTypeChanges = false;
  bool _isRTChanges = false;
  bool _isRWChanges = false;
  bool _isKelurahanChanges = false;
  bool _isKecamatanChanges = false;
  bool _isKotaChanges = false;
  bool _isProvinsiChanges = false;
  bool _isPostalCodeChanges = false;
  bool _isAddressFromMapChanges = false;
  bool _isTeleponAreaChanges = false;
  bool _isTeleponChanges = false;

  void setDataAddressIndividu(BuildContext context) async{
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    _custType = _preferences.getString("cust_type");
    _setAddressIndividuModelOcupation = Provider.of<DashboardChangeNotif>(context,listen: false).setAddressIndividuOccupation;
    _setAddressIndividuModelInfoNasabah = Provider.of<DashboardChangeNotif>(context,listen: false).setAddressIndividuInfoNasabah;
    _setAddressIndividuPenjamin = Provider.of<DashboardChangeNotif>(context,listen: false).setAddressIndividuPenjamin;
    _setAddressIndividuPenjaminCompany = Provider.of<DashboardChangeNotif>(context,listen: false).setAddressIndividuPenjaminCompany;
    _setAddressIndividuManajemenPIC = Provider.of<DashboardChangeNotif>(context,listen: false).setAddressIndividuManajemenPIC;
    _setAddressIndividuPemegangSaham = Provider.of<DashboardChangeNotif>(context,listen: false).setAddressIndividuPemegangSaham;
  }

  bool get isDisablePACIAAOSCONA => _isDisablePACIAAOSCONA;

  set isDisablePACIAAOSCONA(bool value) {
    this._isDisablePACIAAOSCONA = value;
    notifyListeners();
  }

  get custType => _custType;

  set custType(value) {
    this._custType = value;
  }

  GlobalKey<FormState> _key = GlobalKey<FormState>();

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  List<JenisAlamatModel> _listJenisAlamat = [];

  // Jenis Alamat
  UnmodifiableListView<JenisAlamatModel> get listJenisAlamat {
    return UnmodifiableListView(this._listJenisAlamat);
  }

  JenisAlamatModel get jenisAlamatSelected => _jenisAlamatSelected;

  set jenisAlamatSelected(JenisAlamatModel value) {
    this._jenisAlamatSelected = value;
    notifyListeners();
  }

  TextEditingController get controllerProvinsi => _controllerProvinsi;
  TextEditingController get controllerKota => _controllerKota;
  TextEditingController get controllerKecamatan => _controllerKecamatan;
  TextEditingController get controllerKelurahan => _controllerKelurahan;
  TextEditingController get controllerRW => _controllerRW;
  TextEditingController get controllerRT => _controllerRT;
  TextEditingController get controllerAlamat => _controllerAlamat;
  TextEditingController get controllerKodeArea => _controllerKodeArea;
  TextEditingController get controllerTlpn => _controllerTlpn;
  TextEditingController get controllerPostalCode => _controllerPostalCode;
  TextEditingController get controllerAddressFromMap => _controllerAddressFromMap;

  // Kelurahan
  KelurahanModel get kelurahanSelected => _kelurahanSelected;

  set kelurahanSelected(KelurahanModel value) {
    this._kelurahanSelected = value;
    notifyListeners();
  }

  // Sama Dengan Identitas
  bool get isSameWithIdentity => _isSameWithIdentity;

  set isSameWithIdentity(bool value) {
    this._isSameWithIdentity = value;
    notifyListeners();
  }

  JenisAlamatModel get jenisAlamatSelectedTemp => _jenisAlamatSelectedTemp;

  KelurahanModel get kelurahanSelectedTemp => _kelurahanSelectedTemp;

  bool get disableJenisPenawaran => _disableJenisPenawaran;

  set disableJenisPenawaran(bool value) {
    this._disableJenisPenawaran = value;
  }

  Future<void> addDataListJenisAlamat(AddressModel data, BuildContext context, int flag, bool isSameWithIdentity,int index, int typeAddress) async{
//  typeAddres 1 = list alamat info nasabah
//  typeAddres 2 = list alamat pekerjaan
//  typeAddres 3 = list alamat penjamin pribadi
//  typeAddres 4 = list alamat manajemen pic
//  typeAddres 5 = list alamat saham pribadi
    SharedPreferences _preference = await SharedPreferences.getInstance();
    this._custType = _preference.getString("cust_type");
    this._lastKnownState = _preference.getString("last_known_state");
    if(_preference.getString("jenis_penawaran") == "002"){
      this._disableJenisPenawaran = true;
    }
    var _provider = Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false);
    var _providerOccupation = Provider.of<FormMOccupationChangeNotif>(context, listen: false);
    var _providerPenjaminPribadi = Provider.of<FormMAddGuarantorIndividualChangeNotifier>(context, listen: false);
    var _providerManajemenPIC = Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: false);
    var _providerSahamPribadi = Provider.of<AddPemegangSahamPribadiChangeNotifier>(context, listen: false);
    try {
      this._listJenisAlamat.clear();
      if (flag == 0) {
        print("create baru");
        // add
        if(typeAddress == 1){
          if (_provider.listAlamatKorespondensi.isEmpty) {
            this._listJenisAlamat = await getAddressType(1);
            if(this._listJenisAlamat.isNotEmpty){
                for(int i=0; i<_listJenisAlamat.length; i++){
                    if (_listJenisAlamat[i].KODE == "03") {
                        this._jenisAlamatSelected = this._listJenisAlamat[i];
                    }
                }
                List _dedup = await _dbHelper.selectDataInfoNasabahFromDataDedup();
                if(_dedup.isNotEmpty){
                    if(_dedup[0]['alamat'] != "null"){
                        this._controllerAlamat.text = _dedup[0]['alamat'];
                    }
                }
            }
          }
          else {
            this._listJenisAlamat = await getAddressType(3);
            // remove used type
            for(int x=0; x<_provider.listAlamatKorespondensi.length; x++){
              for(int i=0; i < _listJenisAlamat.length;i++){
                if(_listJenisAlamat[i].KODE == _provider.listAlamatKorespondensi[x].jenisAlamatModel.KODE){
                  _listJenisAlamat.removeAt(i);
                }
              }
            }
            print("cek list ${_listJenisAlamat.length}");
          }
        }
        else if(typeAddress == 2){
          if (_providerOccupation.listOccupationAddress.isEmpty) {
            this._listJenisAlamat = await getAddressType(7);
            if(this._listJenisAlamat.isNotEmpty){
                for(int i=0; i<_listJenisAlamat.length; i++){
                    if (_listJenisAlamat[i].KODE == "02") {
                        this._jenisAlamatSelected = this._listJenisAlamat[i];
                    }
                }
            }
          }
          else {
            this._listJenisAlamat = await getAddressType(8);
            // remove used type
            for(int x=0; x<_providerOccupation.listOccupationAddress.length; x++){
              for(int i=0; i < _listJenisAlamat.length;i++){
                if(_listJenisAlamat[i].KODE == _providerOccupation.listOccupationAddress[x].jenisAlamatModel.KODE){
                  _listJenisAlamat.removeAt(i);
                }
              }
            }
          }
        }
        else if(typeAddress == 3){
          // if (_providerPenjaminPribadi.listGuarantorAddress.isEmpty) {
          //   this._listJenisAlamat = await getAddressType(1);
          //   if(this._listJenisAlamat.isNotEmpty){
          //       for(int i=0; i<_listJenisAlamat.length; i++){
          //           if (_listJenisAlamat[i].KODE == "03") {
          //               this._jenisAlamatSelected = this._listJenisAlamat[i];
          //           }
          //       }
          //   }
          // }
          // else {
          //   this._listJenisAlamat = await getAddressType(3);
          //   // remove used type
          //   for(int x=0; x<_providerPenjaminPribadi.listGuarantorAddress.length; x++){
          //     for(int i=0; i < _listJenisAlamat.length;i++){
          //       if(_listJenisAlamat[i].KODE == _providerPenjaminPribadi.listGuarantorAddress[x].jenisAlamatModel.KODE){
          //         _listJenisAlamat.removeAt(i);
          //       }
          //     }
          //   }
          // }

          this._listJenisAlamat = await getAddressType(10);
          // remove used type
          for(int x=0; x<_providerPenjaminPribadi.listGuarantorAddress.length; x++){
            for(int i=0; i < _listJenisAlamat.length;i++){
              if(_listJenisAlamat[i].KODE == _providerPenjaminPribadi.listGuarantorAddress[x].jenisAlamatModel.KODE){
                _listJenisAlamat.removeAt(i);
              }
            }
          }
        }
        else if(typeAddress == 4){
          if (_providerManajemenPIC.listManajemenPICAddress.isEmpty) {
            this._listJenisAlamat = await getAddressType(10);
            if(this._listJenisAlamat.isNotEmpty){
                for(int i=0; i<_listJenisAlamat.length; i++){
                    if (_listJenisAlamat[i].KODE == "03") {
                        this._jenisAlamatSelected = this._listJenisAlamat[i];
                    }
                }
            }
          }
          else {
            this._listJenisAlamat = await getAddressType(3);
            // remove used type
            for(int x=0; x<_providerManajemenPIC.listManajemenPICAddress.length; x++){
              for(int i=0; i < _listJenisAlamat.length;i++){
                if(_listJenisAlamat[i].KODE == _providerManajemenPIC.listManajemenPICAddress[x].jenisAlamatModel.KODE){
                  _listJenisAlamat.removeAt(i);
                }
              }
            }
          }
        }
        else if(typeAddress == 5){
          // if (_providerSahamPribadi.listPemegangSahamPribadiAddress.isEmpty) {
          //   this._listJenisAlamat = await getAddressType(1);
          //   if(this._listJenisAlamat.isNotEmpty){
          //       for(int i=0; i<_listJenisAlamat.length; i++){
          //           if (_listJenisAlamat[i].KODE == "03") {
          //               this._jenisAlamatSelected = this._listJenisAlamat[i];
          //           }
          //       }
          //   }
          // }
          // else {
          //   this._listJenisAlamat = await getAddressType(3);
          //   // remove used type
          //   for(int x=0; x<_providerSahamPribadi.listPemegangSahamPribadiAddress.length; x++){
          //     for(int i=0; i < _listJenisAlamat.length;i++){
          //       if(_listJenisAlamat[i].KODE == _providerSahamPribadi.listPemegangSahamPribadiAddress[x].jenisAlamatModel.KODE){
          //         _listJenisAlamat.removeAt(i);
          //       }
          //     }
          //   }
          // }
          this._listJenisAlamat = await getAddressType(10);
          // remove used type
          for(int x=0; x<_providerSahamPribadi.listPemegangSahamPribadiAddress.length; x++){
            for(int i=0; i < _listJenisAlamat.length;i++){
              if(_listJenisAlamat[i].KODE == _providerSahamPribadi.listPemegangSahamPribadiAddress[x].jenisAlamatModel.KODE){
                _listJenisAlamat.removeAt(i);
              }
            }
          }
        }
      }
      else {
        // edit
        if(typeAddress == 1){
          print("edit 1");
          if (_provider.listAlamatKorespondensi[index].jenisAlamatModel.KODE == '03') {
            this._listJenisAlamat = await getAddressType(1);
          } else if (_provider.listAlamatKorespondensi[index].jenisAlamatModel.KODE == '01'){
            this._listJenisAlamat = await getAddressType(2);
          }
          else {
            this._listJenisAlamat = await getAddressType(3);
          }
          // remove used type
          for(int x=0; x<_provider.listAlamatKorespondensi.length; x++){
            for(int i=0; i < _listJenisAlamat.length;i++){
              if(_listJenisAlamat[i].KODE == _provider.listAlamatKorespondensi[x].jenisAlamatModel.KODE && _listJenisAlamat[i].KODE != data.jenisAlamatModel.KODE){
                _listJenisAlamat.removeAt(i);
              }
            }
          }
        }
        else if (typeAddress == 2){
          print("edit 2");
          if (_providerOccupation.listOccupationAddress[index].jenisAlamatModel.KODE == '02') {
            this._listJenisAlamat = await getAddressType(7);
          }
          else {
            this._listJenisAlamat = await getAddressType(8);
          }
          // remove used type
          for(int x=0; x<_providerOccupation.listOccupationAddress.length; x++){
            for(int i=0; i < _listJenisAlamat.length;i++){
              if(_listJenisAlamat[i].KODE == _providerOccupation.listOccupationAddress[x].jenisAlamatModel.KODE && _listJenisAlamat[i].KODE != data.jenisAlamatModel.KODE){
                _listJenisAlamat.removeAt(i);
              }
            }
          }
        }
        else if (typeAddress == 3){
          print("edit 3");
          // if (_providerPenjaminPribadi.listGuarantorAddress[index].jenisAlamatModel.KODE == '03') {
          //   this._listJenisAlamat = await getAddressType(1);
          // } else if (_providerPenjaminPribadi.listGuarantorAddress[index].jenisAlamatModel.KODE == '01'){
          //   this._listJenisAlamat = await getAddressType(2);
          // }
          // else {
            this._listJenisAlamat = await getAddressType(10);
          // }
          // remove used type
          for(int x=0; x<_providerPenjaminPribadi.listGuarantorAddress.length; x++){
            for(int i=0; i < _listJenisAlamat.length;i++){
              if(_listJenisAlamat[i].KODE == _providerPenjaminPribadi.listGuarantorAddress[x].jenisAlamatModel.KODE && _listJenisAlamat[i].KODE != data.jenisAlamatModel.KODE){
                _listJenisAlamat.removeAt(i);
              }
            }
          }
        }
        else if (typeAddress == 4){
          print("edit 4");
          if (_providerManajemenPIC.listManajemenPICAddress[index].jenisAlamatModel.KODE == '03') {
            this._listJenisAlamat = await getAddressType(1);
          } else if (_providerManajemenPIC.listManajemenPICAddress[index].jenisAlamatModel.KODE == '01'){
            this._listJenisAlamat = await getAddressType(2);
          }
          else {
            this._listJenisAlamat = await getAddressType(3);
          }
          // remove used type
          for(int x=0; x<_providerManajemenPIC.listManajemenPICAddress.length; x++){
            for(int i=0; i < _listJenisAlamat.length;i++){
              if(_listJenisAlamat[i].KODE == _providerManajemenPIC.listManajemenPICAddress[x].jenisAlamatModel.KODE && _listJenisAlamat[i].KODE != data.jenisAlamatModel.KODE){
                _listJenisAlamat.removeAt(i);
              }
            }
          }
        }
        else if (typeAddress == 5){
          if (_providerSahamPribadi.listPemegangSahamPribadiAddress[index].jenisAlamatModel.KODE == '03') {
            this._listJenisAlamat = await getAddressType(1);
          } else if (_providerSahamPribadi.listPemegangSahamPribadiAddress[index].jenisAlamatModel.KODE == '01'){
            this._listJenisAlamat = await getAddressType(2);
          }
          else {
            this._listJenisAlamat = await getAddressType(3);
          }
          // remove used type
          for(int x=0; x<_providerSahamPribadi.listPemegangSahamPribadiAddress.length; x++){
            for(int i=0; i < _listJenisAlamat.length;i++){
              if(_listJenisAlamat[i].KODE == _providerSahamPribadi.listPemegangSahamPribadiAddress[x].jenisAlamatModel.KODE && _listJenisAlamat[i].KODE != data.jenisAlamatModel.KODE){
                _listJenisAlamat.removeAt(i);
              }
            }
          }
        }
        if(flag == 1) setValueForEdit(data, context, isSameWithIdentity, index, typeAddress);
      }
      notifyListeners();
    } catch (e) {
      print(e);
    }
  }

  void searchKelurahan(BuildContext context) async {
    KelurahanModel data = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => SearchKelurahanChangeNotif(),
                child: SearchKelurahan())));
    if (data != null) {
      kelurahanSelected = data;
      this._controllerKelurahan.text = data.KEL_ID + " - " + data.KEL_NAME;
      this._controllerKecamatan.text = data.KEC_ID + " - " + data.KEC_NAME;
      this._controllerKota.text = data.KABKOT_ID + " - " + data.KABKOT_NAME;
      this._controllerProvinsi.text = data.PROV_ID + " - " + data.PROV_NAME;
      this._controllerPostalCode.text = data.ZIPCODE;
      notifyListeners();
    } else {
      return;
    }
  }

  GlobalKey<FormState> get key => _key;

  String get addressID => _addressID;

  set addressID(String value) {
    this._addressID = value;
  }

  get foreignBusinessID => _foreignBusinessID;

  set foreignBusinessID(value) {
    this._foreignBusinessID = value;
  }

  void isShowDialog(BuildContext context, int flag, int index, int typeAddress) {
      showDialog(
          context: context,
          barrierDismissible: true,
          builder: (BuildContext context){
            return Theme(
              data: ThemeData(
                  primaryColor: Colors.black,
                  accentColor: myPrimaryColor,
                  fontFamily: "NunitoSans",
                  primarySwatch: primaryOrange
              ),
              child: AlertDialog(
                title: Text("Informasi", style: TextStyle(fontWeight: FontWeight.bold)),
                content: Text("Apakah alamat Domisili sama dengan alamat Identitas?"),
                actions: <Widget>[
                  FlatButton(
                      onPressed: (){
                        _isSameWithIdentity = true;
                        if(typeAddress == 1){
                          Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false)
                              .addAlamatKorespondensi(AddressModel(
                              this._jenisAlamatSelected,
                              this._addressID,
                              this._foreignBusinessID,
                              null, //no identitas
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              this._isSameWithIdentity,
                              this._addressFromMap,
                              false,
                              this._active,
                              this._isEditAddress,
                              this._isAlamatChanges,
                              this._isAddressTypeChanges,
                              this._isRTChanges,
                              this._isRWChanges,
                              this._isKelurahanChanges,
                              this._isKecamatanChanges,
                              this._isKotaChanges,
                              this._isProvinsiChanges,
                              this._isPostalCodeChanges,
                              this._isAddressFromMapChanges,
                              this._isTeleponAreaChanges,
                              this._isTeleponChanges,
                          ));

                          Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false)
                              .addAlamatKorespondensi(AddressModel(
                              JenisAlamatModel("01", "DOMISILI"),
                              this._addressID,
                              this._foreignBusinessID,
                              null, //no identitas
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              this._isSameWithIdentity,
                              this._addressFromMap,
                              false,
                              this._active,
                              this._isEditAddress,
                              this._isAlamatChanges,
                              this._isAddressTypeChanges,
                              this._isRTChanges,
                              this._isRWChanges,
                              this._isKelurahanChanges,
                              this._isKecamatanChanges,
                              this._isKotaChanges,
                              this._isProvinsiChanges,
                              this._isPostalCodeChanges,
                              this._isAddressFromMapChanges,
                              this._isTeleponAreaChanges,
                              this._isTeleponChanges,
                          ));
                        }
                        else if(typeAddress == 2){
                          Provider.of<FormMOccupationChangeNotif>(context, listen: false)
                              .addOccupationAddress(AddressModel(
                              this._jenisAlamatSelected,
                              this._addressID,
                              this._foreignBusinessID,
                              null, //no identitas
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              this._isSameWithIdentity,
                              this._addressFromMap,
                              false,
                              this._active,
                              this._isEditAddress,
                              this._isAlamatChanges,
                              this._isAddressTypeChanges,
                              this._isRTChanges,
                              this._isRWChanges,
                              this._isKelurahanChanges,
                              this._isKecamatanChanges,
                              this._isKotaChanges,
                              this._isProvinsiChanges,
                              this._isPostalCodeChanges,
                              this._isAddressFromMapChanges,
                              this._isTeleponAreaChanges,
                              this._isTeleponChanges,
                          ));

                          Provider.of<FormMOccupationChangeNotif>(context, listen: false)
                              .addOccupationAddress(AddressModel(
                              JenisAlamatModel("01", "DOMISILI"),
                              this._addressID,
                              this._foreignBusinessID,
                              null, //no identitas
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              this._isSameWithIdentity,
                              this._addressFromMap,
                              false,
                              this._active,
                              this._isEditAddress,
                              this._isAlamatChanges,
                              this._isAddressTypeChanges,
                              this._isRTChanges,
                              this._isRWChanges,
                              this._isKelurahanChanges,
                              this._isKecamatanChanges,
                              this._isKotaChanges,
                              this._isProvinsiChanges,
                              this._isPostalCodeChanges,
                              this._isAddressFromMapChanges,
                              this._isTeleponAreaChanges,
                              this._isTeleponChanges,
                          ));
                        }
                        else if(typeAddress == 3){
                          Provider.of<FormMAddGuarantorIndividualChangeNotifier>(context, listen: false)
                              .addGuarantorAddress(AddressModel(
                              this._jenisAlamatSelected,
                              this._addressID,
                              this._foreignBusinessID,
                              null, //no identitas
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              this._isSameWithIdentity,
                              this._addressFromMap,
                              false,
                              this._active,
                              this._isEditAddress,
                              this._isAlamatChanges,
                              this._isAddressTypeChanges,
                              this._isRTChanges,
                              this._isRWChanges,
                              this._isKelurahanChanges,
                              this._isKecamatanChanges,
                              this._isKotaChanges,
                              this._isProvinsiChanges,
                              this._isPostalCodeChanges,
                              this._isAddressFromMapChanges,
                              this._isTeleponAreaChanges,
                              this._isTeleponChanges,
                          ));

                          Provider.of<FormMAddGuarantorIndividualChangeNotifier>(context, listen: false)
                              .addGuarantorAddress(AddressModel(
                              JenisAlamatModel("01", "DOMISILI"),
                              this._addressID,
                              this._foreignBusinessID,
                              null, //no identitas
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              this._isSameWithIdentity,
                              this._addressFromMap,
                              false,
                              this._active,
                              this._isEditAddress,
                              this._isAlamatChanges,
                              this._isAddressTypeChanges,
                              this._isRTChanges,
                              this._isRWChanges,
                              this._isKelurahanChanges,
                              this._isKecamatanChanges,
                              this._isKotaChanges,
                              this._isProvinsiChanges,
                              this._isPostalCodeChanges,
                              this._isAddressFromMapChanges,
                              this._isTeleponAreaChanges,
                              this._isTeleponChanges,
                          ));
                        }
                        else if(typeAddress == 4){
                          Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: false)
                              .addManajemenPICAddress(AddressModel(
                              this._jenisAlamatSelected,
                              this._addressID,
                              this._foreignBusinessID,
                              null, //no identitas
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              this._isSameWithIdentity,
                              this._addressFromMap,
                              false,
                              this._active,
                              this._isEditAddress,
                              this._isAlamatChanges,
                              this._isAddressTypeChanges,
                              this._isRTChanges,
                              this._isRWChanges,
                              this._isKelurahanChanges,
                              this._isKecamatanChanges,
                              this._isKotaChanges,
                              this._isProvinsiChanges,
                              this._isPostalCodeChanges,
                              this._isAddressFromMapChanges,
                              this._isTeleponAreaChanges,
                              this._isTeleponChanges,
                          ));

                          Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: false)
                              .addManajemenPICAddress(AddressModel(
                              JenisAlamatModel("01", "DOMISILI"),
                              this._addressID,
                              this._foreignBusinessID,
                              null, //no identitas
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              this._isSameWithIdentity,
                              this._addressFromMap,
                              false,
                              this._active,
                              this._isEditAddress,
                              this._isAlamatChanges,
                              this._isAddressTypeChanges,
                              this._isRTChanges,
                              this._isRWChanges,
                              this._isKelurahanChanges,
                              this._isKecamatanChanges,
                              this._isKotaChanges,
                              this._isProvinsiChanges,
                              this._isPostalCodeChanges,
                              this._isAddressFromMapChanges,
                              this._isTeleponAreaChanges,
                              this._isTeleponChanges,
                          ));
                        }
                        else if(typeAddress == 5){
                          Provider.of<AddPemegangSahamPribadiChangeNotifier>(context, listen: false)
                              .addPemegangSahamPribadiAddress(AddressModel(
                              this._jenisAlamatSelected,
                              this._addressID,
                              this._foreignBusinessID,
                              null, //no identitas
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              this._isSameWithIdentity,
                              this._addressFromMap,
                              false,
                              this._active,
                              this._isEditAddress,
                              this._isAlamatChanges,
                              this._isAddressTypeChanges,
                              this._isRTChanges,
                              this._isRWChanges,
                              this._isKelurahanChanges,
                              this._isKecamatanChanges,
                              this._isKotaChanges,
                              this._isProvinsiChanges,
                              this._isPostalCodeChanges,
                              this._isAddressFromMapChanges,
                              this._isTeleponAreaChanges,
                              this._isTeleponChanges,
                          ));

                          Provider.of<AddPemegangSahamPribadiChangeNotifier>(context, listen: false)
                              .addPemegangSahamPribadiAddress(AddressModel(
                              JenisAlamatModel("01", "DOMISILI"),
                              this._addressID,
                              this._foreignBusinessID,
                              null, //no identitas
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              this._isSameWithIdentity,
                              this._addressFromMap,
                              false,
                              this._active,
                              this._isEditAddress,
                              this._isAlamatChanges,
                              this._isAddressTypeChanges,
                              this._isRTChanges,
                              this._isRWChanges,
                              this._isKelurahanChanges,
                              this._isKecamatanChanges,
                              this._isKotaChanges,
                              this._isProvinsiChanges,
                              this._isPostalCodeChanges,
                              this._isAddressFromMapChanges,
                              this._isTeleponAreaChanges,
                              this._isTeleponChanges,
                          ));
                        }
                        Navigator.pop(context);
                        Navigator.pop(context);
                      },
                      child: Text("Ya",
                          style: TextStyle(
                              color: primaryOrange,
                          )
                      )
                  ),
                  FlatButton(
                      onPressed: (){
                        //bikin identitas
                        if(typeAddress == 1){
                          Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false)
                              .addAlamatKorespondensi(AddressModel(
                              this._jenisAlamatSelected,
                              this._addressID,
                              this._foreignBusinessID,
                              null, //no identitas
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              false,
                              this._addressFromMap,
                              false,
                              this._active,
                              this._isEditAddress,
                              this._isAlamatChanges,
                              this._isAddressTypeChanges,
                              this._isRTChanges,
                              this._isRWChanges,
                              this._isKelurahanChanges,
                              this._isKecamatanChanges,
                              this._isKotaChanges,
                              this._isProvinsiChanges,
                              this._isPostalCodeChanges,
                              this._isAddressFromMapChanges,
                              this._isTeleponAreaChanges,
                              this._isTeleponChanges,
                          ));
                        }
                        else if(typeAddress == 2){
                          Provider.of<FormMOccupationChangeNotif>(context, listen: false)
                              .addOccupationAddress(AddressModel(
                              this._jenisAlamatSelected,
                              this._addressID,
                              this._foreignBusinessID,
                              null, //no identitas
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              false,
                              this._addressFromMap,
                              false,
                              this._active,
                              this._isEditAddress,
                              this._isAlamatChanges,
                              this._isAddressTypeChanges,
                              this._isRTChanges,
                              this._isRWChanges,
                              this._isKelurahanChanges,
                              this._isKecamatanChanges,
                              this._isKotaChanges,
                              this._isProvinsiChanges,
                              this._isPostalCodeChanges,
                              this._isAddressFromMapChanges,
                              this._isTeleponAreaChanges,
                              this._isTeleponChanges,
                          ));
                        }
                        else if(typeAddress == 3){
                          Provider.of<FormMAddGuarantorIndividualChangeNotifier>(context, listen: false)
                              .addGuarantorAddress(AddressModel(
                              this._jenisAlamatSelected,
                              this._addressID,
                              this._foreignBusinessID,
                              null, //no identitas
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              false,
                              this._addressFromMap,
                              false,
                              this._active,
                              this._isEditAddress,
                              this._isAlamatChanges,
                              this._isAddressTypeChanges,
                              this._isRTChanges,
                              this._isRWChanges,
                              this._isKelurahanChanges,
                              this._isKecamatanChanges,
                              this._isKotaChanges,
                              this._isProvinsiChanges,
                              this._isPostalCodeChanges,
                              this._isAddressFromMapChanges,
                              this._isTeleponAreaChanges,
                              this._isTeleponChanges,
                          ));
                        }
                        else if(typeAddress == 4){
                          Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: false)
                              .addManajemenPICAddress(AddressModel(
                              this._jenisAlamatSelected,
                              this._addressID,
                              this._foreignBusinessID,
                              null, //no identitas
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              this._isSameWithIdentity,
                              this._addressFromMap,
                              false,
                              this._active,
                              this._isEditAddress,
                              this._isAlamatChanges,
                              this._isAddressTypeChanges,
                              this._isRTChanges,
                              this._isRWChanges,
                              this._isKelurahanChanges,
                              this._isKecamatanChanges,
                              this._isKotaChanges,
                              this._isProvinsiChanges,
                              this._isPostalCodeChanges,
                              this._isAddressFromMapChanges,
                              this._isTeleponAreaChanges,
                              this._isTeleponChanges,
                          ));
                        }
                        else if(typeAddress == 5){
                          Provider.of<AddPemegangSahamPribadiChangeNotifier>(context, listen: false)
                              .addPemegangSahamPribadiAddress(AddressModel(
                              this._jenisAlamatSelected,
                              this._addressID,
                              this._foreignBusinessID,
                              null, //no identitas
                              this._kelurahanSelected,
                              this._controllerAlamat.text,
                              this._controllerRT.text,
                              this._controllerRW.text,
                              this._controllerKodeArea.text,
                              this._controllerTlpn.text,
                              this._isSameWithIdentity,
                              this._addressFromMap,
                              false,
                              this._active,
                              this._isEditAddress,
                              this._isAlamatChanges,
                              this._isAddressTypeChanges,
                              this._isRTChanges,
                              this._isRWChanges,
                              this._isKelurahanChanges,
                              this._isKecamatanChanges,
                              this._isKotaChanges,
                              this._isProvinsiChanges,
                              this._isPostalCodeChanges,
                              this._isAddressFromMapChanges,
                              this._isTeleponAreaChanges,
                              this._isTeleponChanges,
                          ));
                        }
                        Navigator.pop(context);
                        Navigator.pop(context);
                      },
                      child: Text("Tidak",
                          style: TextStyle(
                              color: Colors.grey
                          )
                      )
                  )
                ],
              ),
            );
          }
      );
  }

  void check(BuildContext context, int flag, int index, int typeAddress) {
//  typeAddres 1 = list alamat info nasabah
//  typeAddres 2 = list alamat pekerjaan
//  typeAddres 3 = list alamat penjamin pribadi
//  typeAddres 4 = list alamat manajemen pic
//  typeAddres 5 = list alamat saham pribadi
    final _form = _key.currentState;
    if (flag == 0) {
      if (_form.validate()) {
        if(typeAddress == 1){
          print("list alamat korespondensi new");
          if(jenisAlamatSelected.KODE == "03"){
            this._isSameWithIdentity = true;
            isShowDialog(context, flag, index, typeAddress);
          } else {
            Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false)
                .addAlamatKorespondensi(AddressModel(
                this._jenisAlamatSelected,
                this._addressID,
                this._foreignBusinessID,
                null, //no identitas
                this._kelurahanSelected,
                this._controllerAlamat.text,
                this._controllerRT.text,
                this._controllerRW.text,
                this._controllerKodeArea.text,
                this._controllerTlpn.text,
                this._isSameWithIdentity,
                this._addressFromMap,
                false,
                this._active,
                this._isEditAddress,
                this._isAlamatChanges,
                this._isAddressTypeChanges,
                this._isRTChanges,
                this._isRWChanges,
                this._isKelurahanChanges,
                this._isKecamatanChanges,
                this._isKotaChanges,
                this._isProvinsiChanges,
                this._isPostalCodeChanges,
                this._isAddressFromMapChanges,
                this._isTeleponAreaChanges,
                this._isTeleponChanges,
            ));
            Navigator.pop(context);
          }
        }
        else if(typeAddress == 2){
          print("list occupation address new");
          if(jenisAlamatSelected.KODE == "03"){
            this._isSameWithIdentity = true;
            isShowDialog(context, flag, index, typeAddress);
          } else {
            Provider.of<FormMOccupationChangeNotif>(context, listen: false)
                .addOccupationAddress(AddressModel(
                this._jenisAlamatSelected,
                this._addressID,
                this._foreignBusinessID,
                null, //no identitas
                this._kelurahanSelected,
                this._controllerAlamat.text,
                this._controllerRT.text,
                this._controllerRW.text,
                this._controllerKodeArea.text,
                this._controllerTlpn.text,
                this._isSameWithIdentity,
                this._addressFromMap,
                false,
                this._active,
                this._isEditAddress,
                this._isAlamatChanges,
                this._isAddressTypeChanges,
                this._isRTChanges,
                this._isRWChanges,
                this._isKelurahanChanges,
                this._isKecamatanChanges,
                this._isKotaChanges,
                this._isProvinsiChanges,
                this._isPostalCodeChanges,
                this._isAddressFromMapChanges,
                this._isTeleponAreaChanges,
                this._isTeleponChanges,
            ));
            Navigator.pop(context);
          }
        }
        else if(typeAddress == 3){
          print("list penjamin pribadi new");
          // if(jenisAlamatSelected.KODE == "03"){
          //   this._isSameWithIdentity = true;
          //   isShowDialog(context, flag, index, typeAddress);
          // }
          // else {
            Provider.of<FormMAddGuarantorIndividualChangeNotifier>(context, listen: false)
                .addGuarantorAddress(AddressModel(
                this._jenisAlamatSelected,
                this._addressID,
                this._foreignBusinessID,
                null, //no identitas
                this._kelurahanSelected,
                this._controllerAlamat.text,
                this._controllerRT.text,
                this._controllerRW.text,
                this._controllerKodeArea.text,
                this._controllerTlpn.text,
                this._isSameWithIdentity,
                this._addressFromMap,
                false,
                this._active,
                this._isEditAddress,
                this._isAlamatChanges,
                this._isAddressTypeChanges,
                this._isRTChanges,
                this._isRWChanges,
                this._isKelurahanChanges,
                this._isKecamatanChanges,
                this._isKotaChanges,
                this._isProvinsiChanges,
                this._isPostalCodeChanges,
                this._isAddressFromMapChanges,
                this._isTeleponAreaChanges,
                this._isTeleponChanges,
            ));
            Navigator.pop(context);
          // }
        }
        else if(typeAddress == 4){
          print("list manajemen pic new");
          if(jenisAlamatSelected.KODE == "03"){
            this._isSameWithIdentity = true;
            // isShowDialog(context, flag, index, typeAddress);
            Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: false)
                .addManajemenPICAddress(AddressModel(
              this._jenisAlamatSelected,
              this._addressID,
              this._foreignBusinessID,
              null, //no identitas
              this._kelurahanSelected,
              this._controllerAlamat.text,
              this._controllerRT.text,
              this._controllerRW.text,
              this._controllerKodeArea.text,
              this._controllerTlpn.text,
              this._isSameWithIdentity,
              this._addressFromMap,
              false,
              this._active,
              this._isEditAddress,
              this._isAlamatChanges,
              this._isAddressTypeChanges,
              this._isRTChanges,
              this._isRWChanges,
              this._isKelurahanChanges,
              this._isKecamatanChanges,
              this._isKotaChanges,
              this._isProvinsiChanges,
              this._isPostalCodeChanges,
              this._isAddressFromMapChanges,
              this._isTeleponAreaChanges,
              this._isTeleponChanges,
            ));
            Navigator.pop(context);
          } else {
            Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: false)
                .addManajemenPICAddress(AddressModel(
                this._jenisAlamatSelected,
                this._addressID,
                this._foreignBusinessID,
                null, //no identitas
                this._kelurahanSelected,
                this._controllerAlamat.text,
                this._controllerRT.text,
                this._controllerRW.text,
                this._controllerKodeArea.text,
                this._controllerTlpn.text,
                this._isSameWithIdentity,
                this._addressFromMap,
                false,
                this._active,
                this._isEditAddress,
                this._isAlamatChanges,
                this._isAddressTypeChanges,
                this._isRTChanges,
                this._isRWChanges,
                this._isKelurahanChanges,
                this._isKecamatanChanges,
                this._isKotaChanges,
                this._isProvinsiChanges,
                this._isPostalCodeChanges,
                this._isAddressFromMapChanges,
                this._isTeleponAreaChanges,
                this._isTeleponChanges,
            ));
            Navigator.pop(context);
          }
        }
        else if(typeAddress == 5){
          print("list saham pribadi new");
          if(jenisAlamatSelected.KODE == "03"){
            this._isSameWithIdentity = true;
            isShowDialog(context, flag, index, typeAddress);
          } else {
            Provider.of<AddPemegangSahamPribadiChangeNotifier>(context, listen: false)
                .addPemegangSahamPribadiAddress(AddressModel(
                this._jenisAlamatSelected,
                this._addressID,
                this._foreignBusinessID,
                null, //no identitas
                this._kelurahanSelected,
                this._controllerAlamat.text,
                this._controllerRT.text,
                this._controllerRW.text,
                this._controllerKodeArea.text,
                this._controllerTlpn.text,
                this._isSameWithIdentity,
                this._addressFromMap,
                false,
                this._active,
                this._isEditAddress,
                this._isAlamatChanges,
                this._isAddressTypeChanges,
                this._isRTChanges,
                this._isRWChanges,
                this._isKelurahanChanges,
                this._isKecamatanChanges,
                this._isKotaChanges,
                this._isProvinsiChanges,
                this._isPostalCodeChanges,
                this._isAddressFromMapChanges,
                this._isTeleponAreaChanges,
                this._isTeleponChanges,
            ));
            Navigator.pop(context);
          }
        }
      } else {
        autoValidate = true;
      }
    }
    else {
      if (_form.validate()) {
        if(typeAddress == 1){
          print("list alamat korespondensi edit");
          Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false)
              .updateAlamatKorespondensi(AddressModel(
              this._jenisAlamatSelected,
              this._addressID,
              this._foreignBusinessID,
              null, //no identitas
              this._kelurahanSelected,
              this._controllerAlamat.text,
              this._controllerRT.text,
              this._controllerRW.text,
              this._controllerKodeArea.text,
              this._controllerTlpn.text,
              this._isSameWithIdentity,
              this._addressFromMap,
              this._isCorrespondence,
              this._active,
              this._isEditAddress,
              this._isAlamatChanges,
              this._isAddressTypeChanges,
              this._isRTChanges,
              this._isRWChanges,
              this._isKelurahanChanges,
              this._isKecamatanChanges,
              this._isKotaChanges,
              this._isProvinsiChanges,
              this._isPostalCodeChanges,
              this._isAddressFromMapChanges,
              this._isTeleponAreaChanges,
              this._isTeleponChanges,
          ), index);
        }
        else if(typeAddress == 2){
          print("list occupation address edit");
          Provider.of<FormMOccupationChangeNotif>(context, listen: false)
              .updateOccupationAddress(AddressModel(
              this._jenisAlamatSelected,
              this._addressID,
              this._foreignBusinessID,
              null, //no identitas
              this._kelurahanSelected,
              this._controllerAlamat.text,
              this._controllerRT.text,
              this._controllerRW.text,
              this._controllerKodeArea.text,
              this._controllerTlpn.text,
              this._isSameWithIdentity,
              this._addressFromMap,
              this._isCorrespondence,
              this._active,
              this._isEditAddress,
              this._isAlamatChanges,
              this._isAddressTypeChanges,
              this._isRTChanges,
              this._isRWChanges,
              this._isKelurahanChanges,
              this._isKecamatanChanges,
              this._isKotaChanges,
              this._isProvinsiChanges,
              this._isPostalCodeChanges,
              this._isAddressFromMapChanges,
              this._isTeleponAreaChanges,
              this._isTeleponChanges,
          ), index);
        }
        else if(typeAddress == 3){
          print("list penjamin pribadi edit");
          Provider.of<FormMAddGuarantorIndividualChangeNotifier>(context, listen: false)
              .updateGuarantorAddress(AddressModel(
              this._jenisAlamatSelected,
              this._addressID,
              this._foreignBusinessID,
              null, //no identitas
              this._kelurahanSelected,
              this._controllerAlamat.text,
              this._controllerRT.text,
              this._controllerRW.text,
              this._controllerKodeArea.text,
              this._controllerTlpn.text,
              this._isSameWithIdentity,
              this._addressFromMap,
              this._isCorrespondence,
              this._active,
              this._isEditAddress,
              this._isAlamatChanges,
              this._isAddressTypeChanges,
              this._isRTChanges,
              this._isRWChanges,
              this._isKelurahanChanges,
              this._isKecamatanChanges,
              this._isKotaChanges,
              this._isProvinsiChanges,
              this._isPostalCodeChanges,
              this._isAddressFromMapChanges,
              this._isTeleponAreaChanges,
              this._isTeleponChanges,
          ), index);
        }
        else if(typeAddress == 4){
          print("list manajemen pic edit");
          Provider.of<FormMCompanyManajemenPICAlamatChangeNotifier>(context, listen: false)
              .updateManajemenPICAddress(AddressModel(
              this._jenisAlamatSelected,
              this._addressID,
              this._foreignBusinessID,
              null, //no identitas
              this._kelurahanSelected,
              this._controllerAlamat.text,
              this._controllerRT.text,
              this._controllerRW.text,
              this._controllerKodeArea.text,
              this._controllerTlpn.text,
              this._isSameWithIdentity,
              this._addressFromMap,
              this._isCorrespondence,
              this._active,
              this._isEditAddress,
              this._isAlamatChanges,
              this._isAddressTypeChanges,
              this._isRTChanges,
              this._isRWChanges,
              this._isKelurahanChanges,
              this._isKecamatanChanges,
              this._isKotaChanges,
              this._isProvinsiChanges,
              this._isPostalCodeChanges,
              this._isAddressFromMapChanges,
              this._isTeleponAreaChanges,
              this._isTeleponChanges,
          ), index);
        }
        else if(typeAddress == 5){
          print("list saham pribadi edit");
          Provider.of<AddPemegangSahamPribadiChangeNotifier>(context, listen: false)
              .updatePemegangSahamPribadiAddress(AddressModel(
              this._jenisAlamatSelected,
              this._addressID,
              this._foreignBusinessID,
              null, //no identitas
              this._kelurahanSelected,
              this._controllerAlamat.text,
              this._controllerRT.text,
              this._controllerRW.text,
              this._controllerKodeArea.text,
              this._controllerTlpn.text,
              this._isSameWithIdentity,
              this._addressFromMap,
              this._isCorrespondence,
              this._active,
              this._isEditAddress,
              this._isAlamatChanges,
              this._isAddressTypeChanges,
              this._isRTChanges,
              this._isRWChanges,
              this._isKelurahanChanges,
              this._isKecamatanChanges,
              this._isKotaChanges,
              this._isProvinsiChanges,
              this._isPostalCodeChanges,
              this._isAddressFromMapChanges,
              this._isTeleponAreaChanges,
              this._isTeleponChanges,
          ), index);
        }
        Navigator.pop(context);
      } else {
        autoValidate = true;
      }
    }
  }

  void checkValidCOdeArea(String value) {
    if (value == "08") {
      Future.delayed(Duration(milliseconds: 100), () {
        this._controllerKodeArea.clear();
      });
    } else {
      return;
    }
  }

  setValueForEdit(AddressModel data, BuildContext context,bool isSameWithIdentity, int index, int typeAddress){
//    if (isSameWithIdentity) {
//      setValueIsSameWithIdentity(isSameWithIdentity, context, data);
//    }
//    else {
      for (int i = 0; i < this._listJenisAlamat.length; i++) {
        if (data.jenisAlamatModel.KODE == this._listJenisAlamat[i].KODE) {
          this._jenisAlamatSelected = this._listJenisAlamat[i];
          this._jenisAlamatSelectedTemp = this._listJenisAlamat[i];
        }
      }
      this._kelurahanSelected = data.kelurahanModel;
      this._addressID = data.addressID;
      this._foreignBusinessID = data.foreignBusinessID;
      this._controllerAlamat.text = data.address;
      this._alamatTemp = this._controllerAlamat.text;
      this._controllerRT.text = data.rt;
      this._rtTemp = this._controllerRT.text;
      this._controllerRW.text = data.rw;
      this._rwTemp = this._controllerRW.text;
      this._controllerKelurahan.text = data.kelurahanModel.KEL_ID + " - " + data.kelurahanModel.KEL_NAME;
      this._kelurahanTemp = this._controllerKelurahan.text;
      this._controllerKecamatan.text = data.kelurahanModel.KEC_ID + " - " + data.kelurahanModel.KEC_NAME;
      this._kecamatanTemp = this._controllerKecamatan.text;
      this._controllerKota.text = data.kelurahanModel.KABKOT_ID + " - " + data.kelurahanModel.KABKOT_NAME;
      this._kotaTemp = this._controllerKota.text;
      this._controllerProvinsi.text = data.kelurahanModel.PROV_ID + " - " + data.kelurahanModel.PROV_NAME;
      this._provinsiTemp = this._controllerProvinsi.text;
      this._controllerKodeArea.text = data.areaCode;
      this._areaCodeTemp = this._controllerKodeArea.text;
      this._controllerTlpn.text = data.phone;
      this._phoneTemp = this._controllerTlpn.text;
      this._controllerPostalCode.text = data.kelurahanModel.ZIPCODE;
      this._postalCodeTemp = this._controllerPostalCode.text;
      this._isCorrespondence = data.isCorrespondence;
      this._addressFromMap = data.addressLatLong;
      this._active = data.active;
      if(data.addressLatLong != null) this._controllerAddressFromMap.text = this._addressFromMap['address'];
//    }
      checkDataDakor(data, context, index, typeAddress);
  }

  get postalCodeTemp => _postalCodeTemp;

  get areaCOdeTemp => _areaCodeTemp;

  get provinsiTemp => _provinsiTemp;

  get kotaTemp => _kotaTemp;

  get kecamatanTemp => _kecamatanTemp;

  get kelurahanTemp => _kelurahanTemp;

  get rwTemp => _rwTemp;

  get rtTemp => _rtTemp;

  get phoneTemp => _phoneTemp;

  String get alamatTemp => _alamatTemp;


  //gak kepake
  void setValueIsSameWithIdentity(bool value, BuildContext context, AddressModel model) {
    var _provider =
    Provider.of<FormMInfoAlamatChangeNotif>(context, listen: false);
    AddressModel data;
    if (_alamatTemp == null) {
      if (value) {
        for (int i = 0; i < _provider.listAlamatKorespondensi.length; i++) {
          if (_provider.listAlamatKorespondensi[i].jenisAlamatModel.KODE == "03") {
            data = _provider.listAlamatKorespondensi[i];
          }
        }
        if (model != null) {
          for (int i = 0; i < _listJenisAlamat.length; i++) {
            if (model.jenisAlamatModel.KODE == _listJenisAlamat[i].KODE) {
              this._jenisAlamatSelected = _listJenisAlamat[i];
              this._jenisAlamatSelectedTemp = this._jenisAlamatSelected;
              this._isSameWithIdentity = value;
            }
          }
        }
        this._controllerAlamat.text = data.address;
        this._controllerRT.text = data.rt;
        this._controllerRW.text = data.rw;
        this._kelurahanSelected = data.kelurahanModel;
        this._controllerAlamat.text = data.address;
        this._alamatTemp = this._controllerAlamat.text;
        this._controllerRT.text = data.rt;
        this._rtTemp = this._controllerRT.text;
        this._controllerRW.text = data.rw;
        this._rwTemp = this._controllerRW.text;
        this._controllerKelurahan.text = data.kelurahanModel.KEL_NAME;
        this._kelurahanTemp = this._controllerKelurahan.text;
        this._controllerKecamatan.text = data.kelurahanModel.KEC_NAME;
        this._kecamatanTemp = this._controllerKecamatan.text;
        this._controllerKota.text = data.kelurahanModel.KABKOT_NAME;
        this._kotaTemp = this._controllerKota.text;
        this._controllerProvinsi.text = data.kelurahanModel.PROV_NAME;
        this._provinsiTemp = this._controllerProvinsi.text;
        this._controllerPostalCode.text = data.kelurahanModel.ZIPCODE;
        this._postalCodeTemp = this._controllerPostalCode.text;

        this._controllerKodeArea.text = data.areaCode;
        this._areaCodeTemp = this._controllerKodeArea.text;
        this._controllerTlpn.text = data.phone;
        this._phoneTemp = this._controllerTlpn.text;
        enableTfAddress = !value;
        enableTfRT = !value;
        enableTfRW = !value;
        enableTfKelurahan = !value;
        enableTfKecamatan = !value;
        enableTfKota = !value;
        enableTfProv = !value;
        enableTfPostalCode = !value;
        enableTfAreaCode = !value;
        enableTfPhone = !value;
      } else {
        this._controllerAlamat.clear();
        this._controllerRT.clear();
        this._controllerRW.clear();
        this._kelurahanSelected = null;
        this._controllerAlamat.clear();
        this._controllerRT.clear();
        this._controllerRW.clear();
        this._controllerKelurahan.clear();
        this._controllerKecamatan.clear();
        this._controllerKota.clear();
        this._controllerProvinsi.clear();
        this._controllerPostalCode.clear();
        this._controllerKodeArea.clear();
        this._controllerTlpn.clear();
        enableTfAddress = !value;
        enableTfRT = !value;
        enableTfRW = !value;
        enableTfKelurahan = !value;
        enableTfKecamatan = !value;
        enableTfKota = !value;
        enableTfProv = !value;
        enableTfPostalCode = !value;
        enableTfAreaCode = !value;
        enableTfPhone = !value;
      }
    } else {
      if (value) {
        for (int i = 0; i < _provider.listAlamatKorespondensi.length; i++) {
          if (_provider.listAlamatKorespondensi[i].jenisAlamatModel.KODE == "03") {
            data = _provider.listAlamatKorespondensi[i];
          }
        }
        this._controllerAlamat.text = data.address;
        this._controllerRT.text = data.rt;
        this._controllerRW.text = data.rw;
        this._kelurahanSelected = data.kelurahanModel;
        this._controllerAlamat.text = data.address;
        this._controllerRT.text = data.rt;
        this._controllerRW.text = data.rw;
        this._controllerKelurahan.text = data.kelurahanModel.KEL_NAME;
        this._controllerKecamatan.text = data.kelurahanModel.KEC_NAME;
        this._controllerKota.text = data.kelurahanModel.KABKOT_NAME;
        this._controllerProvinsi.text = data.kelurahanModel.PROV_NAME;
        this._controllerPostalCode.text = data.kelurahanModel.ZIPCODE;
        this._controllerKodeArea.text = data.areaCode;
        this._controllerTlpn.text = data.phone;
        enableTfAddress = !value;
        enableTfRT = !value;
        enableTfRW = !value;
        enableTfKelurahan = !value;
        enableTfKecamatan = !value;
        enableTfKota = !value;
        enableTfProv = !value;
        enableTfPostalCode = !value;
        enableTfAreaCode = !value;
        enableTfPhone = !value;
      } else {
        this._controllerAlamat.clear();
        this._controllerRT.clear();
        this._controllerRW.clear();
        this._kelurahanSelected = null;
        this._controllerAlamat.clear();
        this._controllerRT.clear();
        this._controllerRW.clear();
        this._controllerKelurahan.clear();
        this._controllerKecamatan.clear();
        this._controllerKota.clear();
        this._controllerProvinsi.clear();
        this._controllerPostalCode.clear();
        this._controllerKodeArea.clear();
        this._controllerTlpn.clear();
        enableTfAddress = !value;
        enableTfRT = !value;
        enableTfRW = !value;
        enableTfKelurahan = !value;
        enableTfKecamatan = !value;
        enableTfKota = !value;
        enableTfProv = !value;
        enableTfPostalCode = !value;
        enableTfAreaCode = !value;
        enableTfPhone = !value;
      }
    }
  }

  bool get enableTfPhone => _enableTfPhone;

  set enableTfPhone(bool value) {
    this._enableTfPhone = value;
  }

  bool get enableTfAreaCode => _enableTfAreaCode;

  set enableTfAreaCode(bool value) {
    this._enableTfAreaCode = value;
  }

  bool get enableTfPostalCode => _enableTfPostalCode;

  set enableTfPostalCode(bool value) {
    this._enableTfPostalCode = value;
  }

  bool get enableTfProv => _enableTfProv;

  set enableTfProv(bool value) {
    this._enableTfProv = value;
  }

  bool get enableTfKota => _enableTfKota;

  set enableTfKota(bool value) {
    this._enableTfKota = value;
  }

  bool get enableTfKecamatan => _enableTfKecamatan;

  set enableTfKecamatan(bool value) {
    this._enableTfKecamatan = value;
  }

  bool get enableTfKelurahan => _enableTfKelurahan;

  set enableTfKelurahan(bool value) {
    this._enableTfKelurahan = value;
  }

  bool get enableTfRW => _enableTfRW;

  set enableTfRW(bool value) {
    this._enableTfRW = value;
  }

  bool get enableTfRT => _enableTfRT;

  set enableTfRT(bool value) {
    this._enableTfRT = value;
  }

  bool get enableTfAddress => _enableTfAddress;

  set enableTfAddress(bool value) {
    this._enableTfAddress = value;
  }

  get areaCodeTemp => _areaCodeTemp;

  set areaCodeTemp(value) {
    this._areaCodeTemp = value;
  }

  void setLocationAddressByMap(BuildContext context) async{
    Map _result = await Navigator.push(context, MaterialPageRoute(builder: (context) => MapPage()));
    if(_result != null){
      _addressFromMap = _result;
      this._controllerAddressFromMap.text = _result["address"];
      notifyListeners();
    }
  }

  bool setTelp1Show(int typeAddress){
    bool status = false;
    // info nasabah
    if(typeAddress == 1){
      status = _setAddressIndividuModelInfoNasabah.isTelp1Show;
    }
    // pekerjaan
    else if(typeAddress == 2){
      status = _setAddressIndividuModelOcupation.isTelp1Show;
    }
    // penjamin
    else if(typeAddress == 3){
      status = custType == "PER" ? _setAddressIndividuPenjamin.isTelp1Show : _setAddressIndividuPenjaminCompany.isTelp1Show;
    }
    // manajemen
    else if(typeAddress == 4){
      status = _setAddressIndividuManajemenPIC.isTelp1Show;
    }
    // saham
    else if(typeAddress == 5){
      status = _setAddressIndividuPemegangSaham.isTelp1Show;
    }
    return status;
  }

  bool setTelp1AreaShow(int typeAddress){
    bool status = false;
    // info nasabah
    if(typeAddress == 1){
      status = _setAddressIndividuModelInfoNasabah.isTelp1AreaShow;
    }
    // pekerjaan
    else if(typeAddress == 2){
      status = _setAddressIndividuModelOcupation.isTelp1AreaShow;
    }
    // penjamin
    else if(typeAddress == 3){
      status = custType == "PER" ? _setAddressIndividuPenjamin.isTelp1AreaShow : _setAddressIndividuPenjaminCompany.isTelp1AreaShow;
    }
    // manajemen
    else if(typeAddress == 4){
      status = _setAddressIndividuManajemenPIC.isTelp1AreaShow;
    }
    // saham
    else if(typeAddress == 5){
      status = _setAddressIndividuPemegangSaham.isTelp1AreaShow;
    }
    return status;
  }

  bool setKodePosShow(int typeAddress){
    bool status = false;
    // info nasabah
    if(typeAddress == 1){
      status = _setAddressIndividuModelInfoNasabah.isKodePosShow;
    }
    // pekerjaan
    else if(typeAddress == 2){
      status = _setAddressIndividuModelOcupation.isKodePosShow;
    }
    // penjamin
    else if(typeAddress == 3){
      status = custType == "PER" ? _setAddressIndividuPenjamin.isKodePosShow : _setAddressIndividuPenjaminCompany.isKodePosShow;
    }
    // manajemen
    else if(typeAddress == 4){
      status = _setAddressIndividuManajemenPIC.isKodePosShow;
    }
    // saham
    else if(typeAddress == 5){
      status = _setAddressIndividuPemegangSaham.isKodePosShow;
    }
    return status;
  }

  bool setProvinsiShow(int typeAddress){
    bool status = false;
    // info nasabah
    if(typeAddress == 1){
      status = _setAddressIndividuModelInfoNasabah.isProvinsiShow;
    }
    // pekerjaan
    else if(typeAddress == 2){
      status = _setAddressIndividuModelOcupation.isProvinsiShow;
    }
    // penjamin
    else if(typeAddress == 3){
      status = custType == "PER" ? _setAddressIndividuPenjamin.isProvinsiShow : _setAddressIndividuPenjaminCompany.isProvinsiShow;
    }
    // manajemen
    else if(typeAddress == 4){
      status = _setAddressIndividuManajemenPIC.isProvinsiShow;
    }
    // saham
    else if(typeAddress == 5){
      status = _setAddressIndividuPemegangSaham.isProvinsiShow;
    }
    return status;
  }

  bool setKabKotShow(int typeAddress){
    bool status = false;
    // info nasabah
    if(typeAddress == 1){
      status = _setAddressIndividuModelInfoNasabah.isKabKotShow;
    }
    // pekerjaan
    else if(typeAddress == 2){
      status = _setAddressIndividuModelOcupation.isKabKotShow;
    }
    // penjamin
    else if(typeAddress == 3){
      status = custType == "PER" ? _setAddressIndividuPenjamin.isKabKotShow : _setAddressIndividuPenjaminCompany.isKabKotShow;
    }
    // manajemen
    else if(typeAddress == 4){
      status = _setAddressIndividuManajemenPIC.isKabKotShow;
    }
    // saham
    else if(typeAddress == 5){
      status = _setAddressIndividuPemegangSaham.isKabKotShow;
    }
    return status;
  }

  bool setKecamatanShow(int typeAddress){
    bool status = false;
    // info nasabah
    if(typeAddress == 1){
      status = _setAddressIndividuModelInfoNasabah.isKecamatanShow;
    }
    // pekerjaan
    else if(typeAddress == 2){
      status = _setAddressIndividuModelOcupation.isKecamatanShow;
    }
    // penjamin
    else if(typeAddress == 3){
      status = custType == "PER" ? _setAddressIndividuPenjamin.isKecamatanShow : _setAddressIndividuPenjaminCompany.isKecamatanShow;
    }
    // manajemen
    else if(typeAddress == 4){
      status = _setAddressIndividuManajemenPIC.isKecamatanShow;
    }
    // saham
    else if(typeAddress == 5){
      status = _setAddressIndividuPemegangSaham.isKecamatanShow;
    }
    return status;
  }

  bool setKelurahanShow(int typeAddress){
    bool status = false;
    // info nasabah
    if(typeAddress == 1){
      status = _setAddressIndividuModelInfoNasabah.isKelurahanShow;
    }
    // pekerjaan
    else if(typeAddress == 2){
      status = _setAddressIndividuModelOcupation.isKelurahanShow;
    }
    // penjamin
    else if(typeAddress == 3){
      status = custType == "PER" ? _setAddressIndividuPenjamin.isKelurahanShow : _setAddressIndividuPenjaminCompany.isKelurahanShow;
    }
    // manajemen
    else if(typeAddress == 4){
      status = _setAddressIndividuManajemenPIC.isKelurahanShow;
    }
    // saham
    else if(typeAddress == 5){
      status = _setAddressIndividuPemegangSaham.isKelurahanShow;
    }
    return status;
  }

  bool setRWShow(int typeAddress){
    bool status = false;
    // info nasabah
    if(typeAddress == 1){
      status = _setAddressIndividuModelInfoNasabah.isRWShow;
    }
    // pekerjaan
    else if(typeAddress == 2){
      status = _setAddressIndividuModelOcupation.isRWShow;
    }
    // penjamin
    else if(typeAddress == 3){
      status = custType == "PER" ? _setAddressIndividuPenjamin.isRWShow : _setAddressIndividuPenjaminCompany.isRWShow;
    }
    // manajemen
    else if(typeAddress == 4){
      status = _setAddressIndividuManajemenPIC.isRWShow;
    }
    // saham
    else if(typeAddress == 5){
      status = _setAddressIndividuPemegangSaham.isRWShow;
    }
    return status;
  }

  bool setRTShow(int typeAddress){
    bool status = false;
    // info nasabah
    if(typeAddress == 1){
      status = _setAddressIndividuModelInfoNasabah.isRTShow;
    }
    // pekerjaan
    else if(typeAddress == 2){
      status = _setAddressIndividuModelOcupation.isRTShow;
    }
    // penjamin
    else if(typeAddress == 3){
      status = custType == "PER" ? _setAddressIndividuPenjamin.isRTShow : _setAddressIndividuPenjaminCompany.isRTShow;
    }
    // manajemen
    else if(typeAddress == 4){
      status = _setAddressIndividuManajemenPIC.isRTShow;
    }
    // saham
    else if(typeAddress == 5){
      status = _setAddressIndividuPemegangSaham.isRTShow;
    }
    return status;
  }

  bool setAddressShow(int typeAddress){
    bool status = false;
    // info nasabah
    if(typeAddress == 1){
      status = _setAddressIndividuModelInfoNasabah.isAddressShow;
    }
    // pekerjaan
    else if(typeAddress == 2){
      status = _setAddressIndividuModelOcupation.isAddressShow;
    }
    // penjamin
    else if(typeAddress == 3){
      status = custType == "PER" ? _setAddressIndividuPenjamin.isAddressShow : _setAddressIndividuPenjaminCompany.isAddressShow;
    }
    // manajemen
    else if(typeAddress == 4){
      status = _setAddressIndividuManajemenPIC.isAddressShow;
    }
    // saham
    else if(typeAddress == 5){
      status = _setAddressIndividuPemegangSaham.isAddressShow;
    }
    return status;
  }

  bool setAddressTypeShow(int typeAddress){
    bool status = false;
    // info nasabah
    if(typeAddress == 1){
      status = _setAddressIndividuModelInfoNasabah.isAddressTypeShow;
    }
    // pekerjaan
    else if(typeAddress == 2){
      status = _setAddressIndividuModelOcupation.isAddressTypeShow;
    }
    // penjamin
    else if(typeAddress == 3){
      status = custType == "PER" ? _setAddressIndividuPenjamin.isAddressTypeShow : _setAddressIndividuPenjaminCompany.isAddressTypeShow;
    }
    // manajemen
    else if(typeAddress == 4){
      status = _setAddressIndividuManajemenPIC.isAddressTypeShow;
    }
    // saham
    else if(typeAddress == 5){
      status = _setAddressIndividuPemegangSaham.isAddressTypeShow;
    }
    return status;
  }

  bool setTelp1Man(int typeAddress){
    bool status = false;
    // info nasabah
    if(typeAddress == 1){
      status = _setAddressIndividuModelInfoNasabah.isTelp1Mandatory;
    }
    // pekerjaan
    else if(typeAddress == 2){
      status = _setAddressIndividuModelOcupation.isTelp1Mandatory;
    }
    // penjamin
    else if(typeAddress == 3){
      status = custType == "PER" ? _setAddressIndividuPenjamin.isTelp1Mandatory : _setAddressIndividuPenjaminCompany.isTelp1Mandatory;
    }
    // manajemen
    else if(typeAddress == 4){
      status = _setAddressIndividuManajemenPIC.isTelp1Mandatory;
    }
    // saham
    else if(typeAddress == 5){
      status = _setAddressIndividuPemegangSaham.isTelp1Mandatory;
    }
    return status;
  }

  bool setTelp1AreaMan(int typeAddress){
    bool status = false;
    // info nasabah
    if(typeAddress == 1){
      status = _setAddressIndividuModelInfoNasabah.isTelp1AreaMandatory;
    }
    // pekerjaan
    else if(typeAddress == 2){
      status = _setAddressIndividuModelOcupation.isTelp1AreaMandatory;
    }
    // penjamin
    else if(typeAddress == 3){
      status = custType == "PER" ? _setAddressIndividuPenjamin.isTelp1AreaMandatory : _setAddressIndividuPenjaminCompany.isTelp1AreaMandatory;
    }
    // manajemen
    else if(typeAddress == 4){
      status = _setAddressIndividuManajemenPIC.isTelp1AreaMandatory;
    }
    // saham
    else if(typeAddress == 5){
      status = _setAddressIndividuPemegangSaham.isTelp1AreaMandatory;
    }
    return status;
  }

  bool setKodePosMan(int typeAddress){
    bool status = false;
    // info nasabah
    if(typeAddress == 1){
      status = _setAddressIndividuModelInfoNasabah.isKodePosMandatory;
    }
    // pekerjaan
    else if(typeAddress == 2){
      status = _setAddressIndividuModelOcupation.isKodePosMandatory;
    }
    // penjamin
    else if(typeAddress == 3){
      status = custType == "PER" ? _setAddressIndividuPenjamin.isKodePosMandatory : _setAddressIndividuPenjaminCompany.isKodePosMandatory;
    }
    // manejemen
    else if(typeAddress == 4){
      status = _setAddressIndividuManajemenPIC.isKodePosMandatory;
    }
    // saham
    else if(typeAddress == 5){
      status = _setAddressIndividuPemegangSaham.isKodePosMandatory;
    }
    return status;
  }

  bool setProvinsiMan(int typeAddress){
    bool status = false;
    // info nasabah
    if(typeAddress == 1){
      status = _setAddressIndividuModelInfoNasabah.isProvinsiMandatory;
    }
    // pekerjaan
    else if(typeAddress == 2){
      status = _setAddressIndividuModelOcupation.isProvinsiMandatory;
    }
    // penjamin
    else if(typeAddress == 3){
      status = custType == "PER" ? _setAddressIndividuPenjamin.isProvinsiMandatory : _setAddressIndividuPenjaminCompany.isProvinsiMandatory;
    }
    // manajemen
    else if(typeAddress == 4){
      status = _setAddressIndividuManajemenPIC.isProvinsiMandatory;
    }
    // saham
    else if(typeAddress == 5){
      status = _setAddressIndividuPemegangSaham.isProvinsiMandatory;
    }
    return status;
  }

  bool setKabKotMan(int typeAddress){
    bool status = false;
    // info nasabah
    if(typeAddress == 1){
      status = _setAddressIndividuModelInfoNasabah.isKabKotMandatory;
    }
    // pekerjaan
    else if(typeAddress == 2){
      status = _setAddressIndividuModelOcupation.isKabKotMandatory;
    }
    // penjamin
    else if(typeAddress == 3){
      status = custType == "PER" ? _setAddressIndividuPenjamin.isKabKotMandatory : _setAddressIndividuPenjaminCompany.isKabKotMandatory;
    }
    // manajemen
    else if(typeAddress == 4){
      status = _setAddressIndividuManajemenPIC.isKabKotMandatory;
    }
    // saham
    else if(typeAddress == 5){
      status = _setAddressIndividuPemegangSaham.isKabKotMandatory;
    }
    return status;
  }

  bool setKecamatanMan(int typeAddress){
    bool status = false;
    // info nasabah
    if(typeAddress == 1){
      status = _setAddressIndividuModelInfoNasabah.isKecamatanMandatory;
    }
    // pekerjaan
    else if(typeAddress == 2){
      status = _setAddressIndividuModelOcupation.isKecamatanMandatory;
    }
    // penjamin
    else if(typeAddress == 3){
      status = custType == "PER" ? _setAddressIndividuPenjamin.isKecamatanMandatory : _setAddressIndividuPenjaminCompany.isKecamatanMandatory;
    }
    // manajemen
    else if(typeAddress == 4){
      status = _setAddressIndividuManajemenPIC.isKecamatanMandatory;
    }
    // saham
    else if(typeAddress == 5){
      status = _setAddressIndividuPemegangSaham.isKecamatanMandatory;
    }
    return status;
  }

  bool setKelurahanMan(int typeAddress){
    bool status = false;
    // info nasabah
    if(typeAddress == 1){
      status = _setAddressIndividuModelInfoNasabah.isKelurahanShow;
    }
    // pekerjaan
    else if(typeAddress == 2){
      status = _setAddressIndividuModelOcupation.isKelurahanShow;
    }
    // penjamin
    else if(typeAddress == 3){
      status = custType == "PER" ? _setAddressIndividuPenjamin.isKelurahanShow : _setAddressIndividuPenjaminCompany.isKelurahanShow;
    }
    // manajemen
    else if(typeAddress == 4){
      status = _setAddressIndividuManajemenPIC.isKelurahanShow;
    }
    // saham
    else if(typeAddress == 5){
      status = _setAddressIndividuPemegangSaham.isKelurahanShow;
    }
    return status;
  }

  bool setRWMan(int typeAddress){
    bool status = false;
    // info nasabah
    if(typeAddress == 1){
      status = _setAddressIndividuModelInfoNasabah.isRWMandatory;
    }
    // pekerjaan
    else if(typeAddress == 2){
      status = _setAddressIndividuModelOcupation.isRWMandatory;
    }
    // penjamin
    else if(typeAddress == 3){
      status = custType == "PER" ? _setAddressIndividuPenjamin.isRWMandatory : _setAddressIndividuPenjaminCompany.isRWMandatory;
    }
    // manajemen
    else if(typeAddress == 4){
      status = _setAddressIndividuManajemenPIC.isRWMandatory;
    }
    // saham
    else if(typeAddress == 5){
      status = _setAddressIndividuPemegangSaham.isRWMandatory;
    }
    return status;
  }

  bool setRTMan(int typeAddress){
    bool status = false;
    // info nasabah
    if(typeAddress == 1){
      status = _setAddressIndividuModelInfoNasabah.isRTMandatory;
    }
    // pekerjaan
    else if(typeAddress == 2){
      status = _setAddressIndividuModelOcupation.isRTMandatory;
    }
    // penjamin
    else if(typeAddress == 3){
      status = custType == "PER" ? _setAddressIndividuPenjamin.isRTMandatory : _setAddressIndividuPenjaminCompany.isRTMandatory;
    }
    // manajemen
    else if(typeAddress == 4){
      status = _setAddressIndividuManajemenPIC.isRTMandatory;
    }
    // saham
    else if(typeAddress == 5){
      status = _setAddressIndividuPemegangSaham.isRTMandatory;
    }
    return status;
  }

  bool setAddressMan(int typeAddress){
    bool status = false;
    // info nasabah
    if(typeAddress == 1){
      status = _setAddressIndividuModelInfoNasabah.isAddressMandatory;
    }
    // pekerjaan
    else if(typeAddress == 2){
      status = _setAddressIndividuModelOcupation.isAddressMandatory;
    }
    // penjamin
    else if(typeAddress == 3){
      status = custType == "PER" ? _setAddressIndividuPenjamin.isAddressMandatory : _setAddressIndividuPenjaminCompany.isAddressMandatory;
    }
    // manajemen
    else if(typeAddress == 4){
      status = _setAddressIndividuManajemenPIC.isAddressMandatory;
    }
    // saham
    else if(typeAddress == 5){
      status = _setAddressIndividuPemegangSaham.isAddressMandatory;
    }
    return status;
  }

  bool setAddressTypeMan(int typeAddress){
    bool status = false;
    // info nasabah
    if(typeAddress == 1){
      status = _setAddressIndividuModelInfoNasabah.isAddressTypeMandatory;
    }
    // pekerjaan
    else if(typeAddress == 2){
      status = _setAddressIndividuModelOcupation.isAddressTypeMandatory;
    }
    // penjamin
    else if(typeAddress == 3){
      status = custType == "PER" ? _setAddressIndividuPenjamin.isAddressTypeMandatory : _setAddressIndividuPenjaminCompany.isAddressTypeMandatory;
    }
    // manajemen
    else if(typeAddress == 4){
      status = _setAddressIndividuManajemenPIC.isAddressTypeMandatory;
    }
    // saham
    else if(typeAddress == 5){
      status = _setAddressIndividuPemegangSaham.isAddressTypeMandatory;
    }
    return status;
  }

  void clearData(){
    this._disableJenisPenawaran = false;
    this._isDisablePACIAAOSCONA = false;
    this._autoValidate = false;
    this._kelurahanSelected = null;
    this._jenisAlamatSelected = null;
    this._controllerAlamat.clear();
    this._controllerRT.clear();
    this._controllerRW.clear();
    this._controllerKelurahan.clear();
    this._controllerKecamatan.clear();
    this._controllerKota.clear();
    this._controllerProvinsi.clear();
    this._controllerPostalCode.clear();
    this._controllerKodeArea.clear();
    this._controllerTlpn.clear();
    this._controllerAddressFromMap.clear();
    this._isEditAddress = false;
    this._isAlamatChanges = false;
    this._isAddressTypeChanges = false;
    this._isRTChanges = false;
    this._isRWChanges = false;
    this._isKelurahanChanges = false;
    this._isKecamatanChanges = false;
    this._isKotaChanges = false;
    this._isProvinsiChanges = false;
    this._isPostalCodeChanges = false;
    this._isAddressFromMapChanges = false;
    this._isTeleponAreaChanges = false;
    this._isTeleponChanges = false;
  }

  String _lastKnownState;

  String get lastKnownState => _lastKnownState;

  set lastKnownState(String value) {
    this._lastKnownState = value;
    notifyListeners();
  }

  void setPreference() async {
    SharedPreferences _preference = await SharedPreferences.getInstance();
    this._lastKnownState = _preference.getString("last_known_state");
    this._disableJenisPenawaran = false;
    if(_preference.getString("jenis_penawaran") == "002"){
      this._disableJenisPenawaran = true;
    }
    if(this._lastKnownState == "AOS" || this._lastKnownState == "CONA" || this._lastKnownState == "IA" || this._lastKnownState == "PAC") {
      this._isDisablePACIAAOSCONA = true;
    }
  }

  void checkDataDakor(AddressModel data, BuildContext context, int index, int typeAddress) async{
//  typeAddres 1 = list alamat info nasabah
//  typeAddres 2 = list alamat pekerjaan
//  typeAddres 3 = list alamat penjamin pribadi
//  typeAddres 4 = list alamat manajemen pic
//  typeAddres 5 = list alamat saham pribadi
      SharedPreferences _preferences = await SharedPreferences.getInstance();
      _custType = _preferences.getString("cust_type");
      if(_preferences.getString("last_known_state") == "DKR"){
        var _check = await _dbHelper.selectMS2CustAddr(typeAddress == 1 ? "5" : typeAddress == 2 ? "4" : typeAddress == 3 ? _custType == "PER" ? "1" : "7" : typeAddress == 4 ? "9" : "11");
        // var _check = await _dbHelper.selectMS2CustAddr(typeAddress == 1 ? _custType == "PER" ? "2" : "8" : typeAddress == 2 ? "10" : "12");
        if(_check.isNotEmpty && _lastKnownState == "DKR"){
          if(this._jenisAlamatSelected != null){
            isAddressTypeChanges = this._jenisAlamatSelected.DESKRIPSI != _check[index]['addr_desc'] || _check[index]['edit_addr_type'] == "1";
          }
          _isAlamatChanges = this._controllerAlamat.text != _check[index]['address'] || _check[index]['edit_address'] == "1";
          _isRTChanges = this._controllerRT.text != _check[index]['rt'] || _check[index]['edit_rt'] == "1";
          _isRWChanges = this._controllerRW.text != _check[index]['rw'] || _check[index]['edit_rw'] == "1";
          _isKelurahanChanges = this._controllerKelurahan.text != _check[index]['kelurahan_desc'] || _check[index]['edit_kelurahan'] == "1";
          _isKecamatanChanges = this._controllerKecamatan.text != _check[index]['kecamatan_desc'] || _check[index]['edit_kecamatan'] == "1";
          _isKotaChanges = this._controllerKota.text != _check[index]['kabkot_desc'] || _check[index]['edit_kabkot'] == "1";
          _isProvinsiChanges = this._controllerProvinsi.text != _check[index]['provinsi_desc'] || _check[index]['edit_provinsi'] == "1";
          _isPostalCodeChanges = this._controllerPostalCode.text != _check[index]['zip_code'] || _check[index]['edit_zip_code'] == "1";
          _isTeleponAreaChanges = this._controllerKodeArea.text != _check[index]['phone1_area'] || _check[index]['edit_phone1_area'] == "1";
          _isTeleponChanges = this._controllerTlpn.text != _check[index]['phone1'] || _check[index]['edit_phone1'] == "1";
          _isAddressFromMapChanges = this._controllerAddressFromMap.text != _check[index]['address_from_map'] || _check[index]['edit_address_from_map'] == "1";

          if(_isEditAddress || _isAlamatChanges || _isAddressTypeChanges || _isRTChanges || _isRWChanges ||
              _isKelurahanChanges || _isKecamatanChanges || _isKotaChanges || _isProvinsiChanges ||
              _isPostalCodeChanges || _isAddressFromMapChanges || _isTeleponAreaChanges || _isTeleponChanges){
            this._isEditAddress = true;
          }
          else {
            this._isEditAddress = false;
          }
        }
        notifyListeners();
      }
  }

  bool get isEditAddress => _isEditAddress;
  bool get isAlamatChanges => _isAlamatChanges;
  bool get isAddressTypeChanges => _isAddressTypeChanges;
  bool get isRTChanges => _isRTChanges;
  bool get isRWChanges => _isRWChanges;
  bool get isKelurahanChanges => _isKelurahanChanges;
  bool get isKecamatanChanges => _isKecamatanChanges;
  bool get isKotaChanges => _isKotaChanges;
  bool get isProvinsiChanges => _isProvinsiChanges;
  bool get isPostalCodeChanges => _isPostalCodeChanges;
  bool get isAddressFromMapChanges => _isAddressFromMapChanges;
  bool get isTeleponAreaChanges => _isTeleponAreaChanges;
  bool get isTeleponChanges => _isTeleponChanges;

  set isEditAddress(bool value) {
    _isEditAddress = value;
    notifyListeners();
  }

  set isAlamatChanges(bool value) {
    this._isAlamatChanges = value;
    notifyListeners();
  }
  set isAddressTypeChanges(bool value) {
    this._isAddressTypeChanges = value;
    notifyListeners();
  }
  set isRTChanges(bool value) {
    this._isRTChanges = value;
    notifyListeners();
  }
  set isRWChanges(bool value) {
    this._isRWChanges = value;
    notifyListeners();
  }
  set isKelurahanChanges(bool value) {
    this._isKelurahanChanges = value;
    notifyListeners();
  }
  set isKecamatanChanges(bool value) {
    this._isKecamatanChanges = value;
    notifyListeners();
  }
  set isKotaChanges(bool value) {
    this._isKotaChanges = value;
    notifyListeners();
  }
  set isProvinsiChanges(bool value) {
    this._isProvinsiChanges = value;
    notifyListeners();
  }
  set isPostalCodeChanges(bool value) {
    this._isPostalCodeChanges = value;
    notifyListeners();
  }
  set isAddressFromMapChanges(bool value) {
    this._isAddressFromMapChanges = value;
    notifyListeners();
  }
  set isTeleponAreaChanges(bool value) {
    this._isTeleponAreaChanges = value;
    notifyListeners();
  }
  set isTeleponChanges(bool value) {
    this._isTeleponChanges = value;
    notifyListeners();
  }
}
