class RecommModel{
  final String levelKomite;
  final String field;
  final String rekomendasi;
  final String keterangan;
  final String createdBy;

  RecommModel(this.levelKomite, this.field, this.rekomendasi, this.keterangan, this.createdBy);
}