class CheckLimitModel {
  final String reff_id;
  final String oid;
  final String lme_id;
  final String source_reff_id;
  final String source_channel_id;
  final String disburse_type;
  final String product_id;
  final String contract_no;
  final String installment_amount;
  final String capacity_limit;
  final String capacity_ph;
  final String voucher_code;
  final String tenor;
  final String cust_id;
  final String elligible_status;
  final String grading_nasabah;
  final String branch_code;
  final String appl_no;
  final String flag_book;
  final String jenis_kegiatan_usaha;
  final String ph_max;
  final String disbursement_no;
  final String response_code;
  final String response_desc;
  final String response_date;
  final String req_date;

  CheckLimitModel(
    this.reff_id,
    this.oid,
    this.lme_id,
    this.source_reff_id,
    this.source_channel_id,
    this.disburse_type,
    this.product_id,
    this.contract_no,
    this.installment_amount,
    this.capacity_limit,
    this.capacity_ph,
    this.voucher_code,
    this.tenor,
    this.cust_id,
    this.elligible_status,
    this.grading_nasabah,
    this.branch_code,
    this.appl_no,
    this.flag_book,
    this.jenis_kegiatan_usaha,
    this.ph_max,
    this.disbursement_no,
    this.response_code,
    this.response_desc,
    this.response_date,
    this.req_date,
  );
}