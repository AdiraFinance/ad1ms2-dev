class FormMPekerjaanModel {}

class TypeOfBusinessModel {
  final String id;
  final String text;

  TypeOfBusinessModel(this.id, this.text);
}

class StatusLocationModel {
  final String id;
  final String desc;

  StatusLocationModel(this.id, this.desc);
}

class BusinessLocationModel {
  final String id;
  final String desc;

  BusinessLocationModel(this.id, this.desc);
}

class SectorEconomicModel {
  final String KODE;
  final String DESKRIPSI;

  SectorEconomicModel(this.KODE, this.DESKRIPSI);
}

class BusinessFieldModel {
  final String KODE;
  final String DESKRIPSI;

  BusinessFieldModel(this.KODE, this.DESKRIPSI);
}

class ProfessionTypeModel {
  final String id;
  final String desc;

  ProfessionTypeModel(this.id, this.desc);
}

class CompanyTypeModel {
  final String id;
  final String desc;

  CompanyTypeModel(this.id, this.desc);
}

class CompanyTypeInsuranceModel {
  final int PARENT_KODE;
  final String KODE;
  final String DESKRIPSI;

  CompanyTypeInsuranceModel(this.PARENT_KODE, this.KODE, this.DESKRIPSI);
}

class EmployeeStatusModel {
  final String id;
  final String desc;

  EmployeeStatusModel(this.id, this.desc);
}

class PEPModel {
  final String KODE;
  final String DESKRIPSI;

  PEPModel(this.KODE, this.DESKRIPSI);
}
