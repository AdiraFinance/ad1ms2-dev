class ProductLMEModel {
  final String productId;
  final String productDesc;

  ProductLMEModel(this.productId, this.productDesc);
}
