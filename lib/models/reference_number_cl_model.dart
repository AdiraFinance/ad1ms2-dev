import 'package:ad1ms2_dev/models/list_voucher_model.dart';

class ReferenceNumberCLModel {
  final String reff_id;
  final String source_reff_id;
  final String oid;
  final String source_channel_id;
  final ListVoucher listVoucher;
  final String response_code;
  final String response_desc;
  final String response_date;
  final String req_date;

  ReferenceNumberCLModel(
    this.reff_id,
    this.source_reff_id,
    this.oid,
    this.source_channel_id,
    this.listVoucher,
    this.response_code,
    this.response_desc,
    this.response_date,
    this.req_date,
  );
}
