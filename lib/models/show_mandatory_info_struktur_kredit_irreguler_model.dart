class ShowMandatoryInfoStrukturKreditIrregulerModel {
  final bool isControllerVisible;
  final bool isControllerMandatory;

  ShowMandatoryInfoStrukturKreditIrregulerModel(
    this.isControllerVisible,
    this.isControllerMandatory,);
}