class ListDisbursement {
  final String disbursement_no;
  final String application_no;
  final String installment_amount;
  final String product_id;
  final String ph_amount;
  final String tenor;
  final String elligible_status;
  final String grading_nasabah;
  final String disbursement_date;
  final String disbursement_channel;
  final String transaction_status;

  ListDisbursement(this.disbursement_no, this.application_no,
      this.installment_amount, this.product_id, this.ph_amount, this.tenor,
      this.elligible_status, this.grading_nasabah, this.disbursement_date,
      this.disbursement_channel, this.transaction_status);
}