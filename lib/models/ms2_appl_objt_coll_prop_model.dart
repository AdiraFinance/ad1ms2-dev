class MS2ApplObjtCollPropModel {
  final String colla_prop_id;
  final String collateralID;
  final int flag_multi_coll;
  final int flag_coll_name_is_appl;
  final String id_type;
  final String id_type_desc;
  final String id_no;
  final String cola_name;
  final String date_of_birth;
  final String place_of_birth;
  final String place_of_birth_kabkota;
  final String place_of_birth_kabkota_desc;
  final String sertifikat_no;
  final String seritifikat_type;
  final String seritifikat_type_desc;
  final String property_type;
  final String property_type_desc;
  final double building_area;
  final double land_area;
  final double dp_guarantee;
  final double max_ph;
  final double taksasi_price;
  final String cola_purpose;
  final String cola_purpose_desc;
  final double jarak_fasum_positif;
  final double jarak_fasum_negatif;
  final double land_price;
  final double njop_price;
  final double building_price;
  final String roof_type;
  final String roof_type_desc;
  final String wall_type;
  final String wall_type_desc;
  final String floor_type;
  final String floor_type_desc;
  final String foundation_type;
  final String foundation_type_desc;
  final String road_type;
  final String road_type_desc;
  final int flag_car_pas;
  final int no_of_house;
  final String guarantee_character;
  final String grntr_evdnce_ownr;
  final String pblsh_sertifikat_date;
  final int pblsh_sertifikat_year;
  final String license_name;
  final String no_srt_ukur;
  final String tgl_srt_ukur;
  final String srtfkt_pblsh_by;
  final String expire_right;
  final String imb_no;
  final String imb_date;
  final double size_of_imb;
  final double ltv;
  final String land_location;
  final double active;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final String edit_flag_multi_coll;
  final String edit_flag_coll_name_is_appl;
  final String edit_id_type;
  final String edit_id_no;
  final String edit_cola_name;
  final String edit_date_of_birth;
  final String edit_place_of_birth;
  final String edit_place_of_birth_kabkota;
  final String edit_sertifikat_no;
  final String edit_seritifikat_type;
  final String edit_property_type;
  final String edit_building_area;
  final String edit_land_area;
  final String edit_dp_guarantee;
  final String edit_max_ph;
  final String edit_taksasi_price;
  final String edit_cola_purpose;
  final String edit_jarak_fasum_positif;
  final String edit_jarak_fasum_negatif;
  final String edit_land_price;
  final String edit_njop_price;
  final String edit_building_price;
  final String edit_roof_type;
  final String edit_wall_type;
  final String edit_floor_type;
  final String edit_foundation_type;
  final String edit_road_type;
  final String edit_flag_car_pas;
  final String edit_no_of_house;
  final String edit_guarantee_character;
  final String edit_grntr_evdnce_ownr;
  final String edit_pblsh_sertifikat_date;
  final String edit_pblsh_sertifikat_year;
  final String edit_license_name;
  final String edit_no_srt_ukur;
  final String edit_tgl_srt_ukur;
  final String edit_srtfkt_pblsh_by;
  final String edit_expire_right;
  final String edit_imb_no;
  final String edit_imb_date;
  final String edit_size_of_imb;
  final String edit_ltv;
  final String edit_land_location;

  MS2ApplObjtCollPropModel (
      this.colla_prop_id,
      this.collateralID,
      this.flag_multi_coll,
      this.flag_coll_name_is_appl,
      this.id_type,
      this.id_type_desc,
      this.id_no,
      this.cola_name,
      this.date_of_birth,
      this.place_of_birth,
      this.place_of_birth_kabkota,
      this.place_of_birth_kabkota_desc,
      this.sertifikat_no,
      this.seritifikat_type,
      this.seritifikat_type_desc,
      this.property_type,
      this.property_type_desc,
      this.building_area,
      this.land_area,
      this.dp_guarantee,
      this.max_ph,
      this.taksasi_price,
      this.cola_purpose,
      this.cola_purpose_desc,
      this.jarak_fasum_positif,
      this.jarak_fasum_negatif,
      this.land_price,
      this.njop_price,
      this.building_price,
      this.roof_type,
      this.roof_type_desc,
      this.wall_type,
      this.wall_type_desc,
      this.floor_type,
      this.floor_type_desc,
      this.foundation_type,
      this.foundation_type_desc,
      this.road_type,
      this.road_type_desc,
      this.flag_car_pas,
      this.no_of_house,
      this.guarantee_character,
      this.grntr_evdnce_ownr,
      this.pblsh_sertifikat_date,
      this.pblsh_sertifikat_year,
      this.license_name,
      this.no_srt_ukur,
      this.tgl_srt_ukur,
      this.srtfkt_pblsh_by,
      this.expire_right,
      this.imb_no,
      this.imb_date,
      this.size_of_imb,
      this.ltv,
      this.land_location,
      this.active,
      this.created_date,
      this.created_by,
      this.modified_date,
      this.modified_by,
      this.edit_flag_multi_coll,
      this.edit_flag_coll_name_is_appl,
      this.edit_id_type,
      this.edit_id_no,
      this.edit_cola_name,
      this.edit_date_of_birth,
      this.edit_place_of_birth,
      this.edit_place_of_birth_kabkota,
      this.edit_sertifikat_no,
      this.edit_seritifikat_type,
      this.edit_property_type,
      this.edit_building_area,
      this.edit_land_area,
      this.edit_dp_guarantee,
      this.edit_max_ph,
      this.edit_taksasi_price,
      this.edit_cola_purpose,
      this.edit_jarak_fasum_positif,
      this.edit_jarak_fasum_negatif,
      this.edit_land_price,
      this.edit_njop_price,
      this.edit_building_price,
      this.edit_roof_type,
      this.edit_wall_type,
      this.edit_floor_type,
      this.edit_foundation_type,
      this.edit_road_type,
      this.edit_flag_car_pas,
      this.edit_no_of_house,
      this.edit_guarantee_character,
      this.edit_grntr_evdnce_ownr,
      this.edit_pblsh_sertifikat_date,
      this.edit_pblsh_sertifikat_year,
      this.edit_license_name,
      this.edit_no_srt_ukur,
      this.edit_tgl_srt_ukur,
      this.edit_srtfkt_pblsh_by,
      this.edit_expire_right,
      this.edit_imb_no,
      this.edit_imb_date,
      this.edit_size_of_imb,
      this.edit_ltv,
      this.edit_land_location,
      );
}