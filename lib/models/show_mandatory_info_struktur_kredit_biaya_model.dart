class ShowMandatoryInfoStrukturKreditBiayaModel {
  final bool isFeeTypeSelectedVisible;
  final bool isCashCostVisible;
  final bool isCreditCostVisible;
  final bool isTotalCostVisible;
  final bool isFeeTypeSelectedMandatory;
  final bool isCashCostMandatory;
  final bool isCreditCostMandatory;
  final bool isTotalCostMandatory;

  ShowMandatoryInfoStrukturKreditBiayaModel(
      this.isFeeTypeSelectedVisible,
      this.isCashCostVisible,
      this.isCreditCostVisible,
      this.isTotalCostVisible,
      this.isFeeTypeSelectedMandatory,
      this.isCashCostMandatory,
      this.isCreditCostMandatory,
      this.isTotalCostMandatory);
}