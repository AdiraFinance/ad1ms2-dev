class WallTypeModel{
  final String id;
  final String name;

  WallTypeModel(this.id, this.name);
}