import 'package:ad1ms2_dev/models/gp_type_model.dart';

class InfoCreditStructureGPModel{
  final GPTypeModel gpTypeModel;
  final String installment;
  final String tenor;

  InfoCreditStructureGPModel(this.gpTypeModel, this.installment, this.tenor);
}