class SubsidyTypeModel{
    final String id;
    final String text;

    SubsidyTypeModel(this.id, this.text);
}