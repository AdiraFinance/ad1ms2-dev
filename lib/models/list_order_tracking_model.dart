class ListOrderTrackingModel {
  final String TANGGAL_APLIKASI;
  final String NO_APLIKASI;
  final String KODE_CABANG;
  final String NAMA_NASABAH;
  final String FINAL_RECOMENDATION;
  final String INITIAL_RECOMENDATION;
  final String LAST_KNOWN_STATE;
  final String TANGGAL_ORDER;
  final String DEALER;

  ListOrderTrackingModel(
      this.TANGGAL_APLIKASI,
      this.NO_APLIKASI,
      this.KODE_CABANG,
      this.NAMA_NASABAH,
      this.FINAL_RECOMENDATION,
      this.INITIAL_RECOMENDATION,
      this.LAST_KNOWN_STATE,
      this.TANGGAL_ORDER,
      this.DEALER);
}
