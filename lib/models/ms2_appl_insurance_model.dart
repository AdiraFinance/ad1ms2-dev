class MS2ApplInsuranceModel {
  final String insurance_id;
  final String orderProductInsuranceID;
  final String cola_type;
  final String insuran_index;
  final String insurance_type;
  final String company_id;
  final String product;
  final String product_desc;
  final int period;
  final String type;
  final String type_1;
  final String type_2;
  final String sub_type;
  final String sub_type_coverage;
  final String coverage_type;
  final String coverage_type_desc;
  final double coverage_amt;
  final double upper_limit_pct;
  final double upper_limit_amt;
  final double lower_limit_pct;
  final double lower_limit_amt;
  final double cash_amt;
  final double credit_amt;
  final double total_split;
  final double total_pct;
  final double total_amt;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final int active;
  final int idx;
  final String insurance_type_desc;
  final String insurance_type_parent;
  final String company_desc;
  final String company_parent;
  final String type_1_parent;
  final String type_1_desc;
  final String type_2_parent;
  final String type_2_desc;
  final String edit_insuran_type_parent;
  final String edit_insuran_index;
  final String edit_company_id;
  final String edit_product;
  final String edit_period;
  final String edit_type;
  final String edit_type_1;
  final String edit_type_2;
  final String edit_sub_type;
  final String edit_coverage_type;
  final String edit_coverage_amt;
  final String edit_upper_limit_pct;
  final String edit_upper_limit_amt;
  final String edit_lower_limit_pct;
  final String edit_lower_limit_amt;
  final String edit_cash_amt;
  final String edit_credit_amt;
  final String edit_total_split;
  final String edit_total_pct;
  final String edit_total_amt;

  MS2ApplInsuranceModel(
    this.insurance_id,
    this.orderProductInsuranceID,
    this.cola_type,
    this.insuran_index,
    this.insurance_type,
    this.company_id,
    this.product,
    this.product_desc,
    this.period,
    this.type,
    this.type_1,
    this.type_2,
    this.sub_type,
    this.sub_type_coverage,
    this.coverage_type,
    this.coverage_type_desc,
    this.coverage_amt,
    this.upper_limit_pct,
    this.upper_limit_amt,
    this.lower_limit_pct,
    this.lower_limit_amt,
    this.cash_amt,
    this.credit_amt,
    this.total_split,
    this.total_pct,
    this.total_amt,
    this.created_date,
    this.created_by,
    this.modified_date,
    this.modified_by,
    this.active,
    this.idx,
    this.insurance_type_desc,
    this.insurance_type_parent,
    this.company_desc,
    this.company_parent,
    this.type_1_parent,
    this.type_1_desc,
    this.type_2_parent,
    this.type_2_desc,
    this.edit_insuran_type_parent,
    this.edit_insuran_index,
    this.edit_company_id,
    this.edit_product,
    this.edit_period,
    this.edit_type,
    this.edit_type_1,
    this.edit_type_2,
    this.edit_sub_type,
    this.edit_coverage_type,
    this.edit_coverage_amt,
    this.edit_upper_limit_pct,
    this.edit_upper_limit_amt,
    this.edit_lower_limit_pct,
    this.edit_lower_limit_amt,
    this.edit_cash_amt,
    this.edit_credit_amt,
    this.edit_total_split,
    this.edit_total_pct,
    this.edit_total_amt,
  );
}
