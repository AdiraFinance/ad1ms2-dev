class SalesmanTypeModel {
  final String id;
  final String salesmanType;

  SalesmanTypeModel(this.id, this.salesmanType);
}
