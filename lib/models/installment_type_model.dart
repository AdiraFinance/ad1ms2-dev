class InstallmentTypeModel{
  final String id;
  final String name;

  InstallmentTypeModel(this.id, this.name);
}