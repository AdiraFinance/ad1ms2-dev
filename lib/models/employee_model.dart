class EmployeeModel {
  final String NIK;
  final String Nama;
  final String Jabatan;
  final String AliasJabatan;

  EmployeeModel(this.NIK, this.Nama, this.Jabatan, this.AliasJabatan);
}
