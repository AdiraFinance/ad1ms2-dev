class InfoDocumentTypeModel{
    final String docTypeId;
    final String docTypeName;
    final int mandatory;
    final int display;
    final int flag_unit;

    InfoDocumentTypeModel(this.docTypeId, this.docTypeName, this.mandatory, this.display, this.flag_unit);
}