class ThirdPartyModel {
  final String kode;
  final String deskripsi;
  final String sentraId;
  final String sentraName;
  final String unitId;
  final String unitName;
  final String kabKotID;
  final String kabKotName;
  final String address;

  ThirdPartyModel(this.kode, this.deskripsi, this.sentraId, this.sentraName, this.unitId, this.unitName, this.kabKotID, this.kabKotName, this.address);
}
