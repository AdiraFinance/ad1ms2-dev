class ShowMandatoryInfoSalesmanModel {
  final bool isSalesmanTypeModelVisible;
  final bool isPositionModelVisible;
  final bool isEmployeeVisible;
  final bool isEmployeeHeadModelVisible;
  final bool isSalesmanTypeModelMandatory;
  final bool isPositionModelMandatory;
  final bool isEmployeeMandatory;
  final bool isEmployeeHeadModelMandatory;

  ShowMandatoryInfoSalesmanModel(
      this.isSalesmanTypeModelVisible,
      this.isPositionModelVisible,
      this.isEmployeeVisible,
      this.isEmployeeHeadModelVisible,
      this.isSalesmanTypeModelMandatory,
      this.isPositionModelMandatory,
      this.isEmployeeMandatory,
      this.isEmployeeHeadModelMandatory);
}