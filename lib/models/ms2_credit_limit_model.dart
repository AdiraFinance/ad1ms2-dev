class MS2CreditLimitModel {
  final String order_no;
  final String consumer_name;
  final String phone_no;
  final String consumer_address;
  final String consumer_type;
  final String consumer_type_desc;
  final String offer_type;
  final String offer_type_desc;
  final String no_reference;
  final String grading;
  final String grading_desc;
  final String process;
  final String notes;
  final String max_ph;
  final String max_installment;
  final String portofolio;
  final String tenor;
  final String opsi_multidisburse;
  final String search_no;

  MS2CreditLimitModel(
      this.order_no,
      this.consumer_name,
      this.phone_no,
      this.consumer_address,
      this.consumer_type,
      this.consumer_type_desc,
      this.offer_type,
      this.offer_type_desc,
      this.no_reference,
      this.grading,
      this.grading_desc,
      this.process,
      this.notes,
      this.max_ph,
      this.max_installment,
      this.portofolio,
      this.tenor,
      this.opsi_multidisburse,
      this.search_no);
}
