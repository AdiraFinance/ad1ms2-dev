import 'company_model.dart';
import 'form_m_pekerjaan_model.dart';
import 'karoseri_model.dart';

class KaroseriObjectModel {
  final int PKSKaroseri;
  final CompanyTypeModel company;
  final KaroseriModel karoseri;
  final String jumlahKaroseri;
  final String price;
  final String totalPrice;
  final bool isEdit;
  final bool isRadioValuePKSKaroseriChanges;
  final bool isCompanyChanges;
  final bool isKaroseriChanges;
  final bool isJumlahKaroseriChanges;
  final bool isPriceChanges;
  final bool isTotalPriceChanges;
  final String orderKaroseriID;

  KaroseriObjectModel(
      this.PKSKaroseri,
      this.company,
      this.karoseri,
      this.jumlahKaroseri,
      this.price,
      this.totalPrice,
      this.isEdit,
      this.isRadioValuePKSKaroseriChanges,
      this.isCompanyChanges,
      this.isKaroseriChanges,
      this.isJumlahKaroseriChanges,
      this.isPriceChanges,
      this.isTotalPriceChanges,
      this.orderKaroseriID);
}
