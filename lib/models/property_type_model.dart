class PropertyTypeModel{
  final String id;
  final String name;

  PropertyTypeModel(this.id, this.name);
}