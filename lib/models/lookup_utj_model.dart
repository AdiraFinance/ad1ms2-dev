class LookupUTJModel {
  final String COLA_BPKB_NO;
  final String COLA_FRAME_NO;
  final String COLA_ENG_NO;
  final String COLA_PLAT_NO;
  final String COLA_YR_PRODUCT;
  final String AREC_GRADE;
  final String FAS_UKJ;
  final String NAMA_BIDDER;
  final String HARGA_PASAR;
  final String HARGA_JUAL;

  LookupUTJModel(
      this.COLA_BPKB_NO,
      this.COLA_FRAME_NO,
      this.COLA_ENG_NO,
      this.COLA_PLAT_NO,
      this.COLA_YR_PRODUCT,
      this.AREC_GRADE,
      this.FAS_UKJ,
      this.NAMA_BIDDER,
      this.HARGA_PASAR,
      this.HARGA_JUAL);
}
