class MS2LMEDetailModel {
  final String order_no;
  final String ac_source_reff_id;
  final String ac_appl_objt_id;
  final String ac_flag_lme;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final int active;

  MS2LMEDetailModel(
    this.order_no,
    this.ac_source_reff_id,
    this.ac_appl_objt_id,
    this.ac_flag_lme,
    this.created_date,
    this.created_by,
    this.modified_date,
    this.modified_by,
    this.active,
  );
}
