class MS2ObjekSalesModel {
  final String appl_sales_id;
  final String orderProductSalesID;
  final String empl_id;
  final String sales_type;
  final String sales_type_desc;
  final String empl_head_id;
  final String empl_job;
  final String empl_job_desc;
  final String ref_contract_no;
  final int active;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final String empl_head_job;
  final String empl_head_job_desc;
  final String empl_head_name;
  final String empl_name;
  final String empl_jabatan;
  final String empl_jabatan_alias;
  final String edit_sales_type;
  final String edit_empl_job;
  final String edit_empl_name;
  final String edit_empl_head_id;

  MS2ObjekSalesModel(
    this.appl_sales_id,
    this.orderProductSalesID,
    this.empl_id,
    this.sales_type,
    this.sales_type_desc,
    this.empl_head_id,
    this.empl_job,
    this.empl_job_desc,
    this.ref_contract_no,
    this.active,
    this.created_date,
    this.created_by,
    this.modified_date,
    this.modified_by,
    this.empl_head_job,
    this.empl_head_job_desc,
    this.empl_head_name,
    this.empl_name,
    this.empl_jabatan,
    this.empl_jabatan_alias,
    this.edit_sales_type,
    this.edit_empl_job,
    this.edit_empl_name,
    this.edit_empl_head_id,
  );
}
