import 'form_m_informasi_alamat_model.dart';

class AddressGuarantorModelIndividiual {
  final JenisAlamatModel jenisAlamatModel;
  final KelurahanModel kelurahanModel;
  final String address;
  final String rt;
  final String rw;
  final String areaCode;
  final String phone;
  final bool isSameWithIdentity;
  bool isCorrespondence;

  AddressGuarantorModelIndividiual(
      this.jenisAlamatModel,
      this.kelurahanModel,
      this.address,
      this.rt,
      this.rw,
      this.areaCode,
      this.phone,
      this.isSameWithIdentity,
      this.isCorrespondence);
}

class AddressModelCompany {
  final JenisAlamatModel jenisAlamatModel;
  final String addressID;
  final String foreignBusinessID;
  final String numberID;
  final KelurahanModel kelurahanModel;
  final String address;
  final String rt;
  final String rw;
  final String phoneArea1;
  final String phone1;
  final String phoneArea2;
  final String phone2;
  final String faxArea;
  final String fax;
  final bool isSameWithIdentity;
  final Map addressLatLong;
  bool isCorrespondence;
  int active;
  final bool isEditAddress;
  final bool isAddressTypeDakor;
  final bool isAddressDakor;
  final bool isRTDakor;
  final bool isRWDakor;
  final bool isKelurahanDakor;
  final bool isKecamatanDakor;
  final bool isKabKotDakor;
  final bool isProvinsiDakor;
  final bool isKodePosDakor;
  final bool isTelp1AreaDakor;
  final bool isTelp1Dakor;
  final bool isTelp2AreaDakor;
  final bool isTelp2Dakor;
  final bool isFaxAreaDakor;
  final bool isFaxDakor;
  final bool isDataLatLongDakor;

  AddressModelCompany(
      this.jenisAlamatModel,
      this.addressID,
      this.foreignBusinessID,
      this.numberID,
      this.kelurahanModel,
      this.address,
      this.rt,
      this.rw,
      this.phoneArea1,
      this.phone1,
      this.phoneArea2,
      this.phone2,
      this.faxArea,
      this.fax,
      this.isSameWithIdentity,
      this.addressLatLong,
      this.isCorrespondence,
      this.active,
      this.isEditAddress,
      this.isAddressTypeDakor,
      this.isAddressDakor,
      this.isRTDakor,
      this.isRWDakor,
      this.isKelurahanDakor,
      this.isKecamatanDakor,
      this.isKabKotDakor,
      this.isProvinsiDakor,
      this.isKodePosDakor,
      this.isTelp1AreaDakor,
      this.isTelp1Dakor,
      this.isTelp2AreaDakor,
      this.isTelp2Dakor,
      this.isFaxAreaDakor,
      this.isFaxDakor,
      this.isDataLatLongDakor);
}
