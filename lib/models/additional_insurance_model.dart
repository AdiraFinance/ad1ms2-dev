import 'package:ad1ms2_dev/models/product_model.dart';
import 'coverage_type_model.dart';
import 'form_m_pekerjaan_model.dart';
import 'insurance_type_model.dart';

class AdditionalInsuranceModel {
  final InsuranceTypeModel insuranceType;
  final String colaType;
  final CompanyTypeInsuranceModel company;
  final ProductModel product;
  final String periodType;
  CoverageTypeModel coverageType;
  String coverageValue;
  String upperLimitRate;
  String lowerLimitRate;
  String upperLimit;
  String lowerLimit;
  String priceCash;
  String priceCredit;
  String totalPriceRate;
  String totalPrice;
  final bool isEdit;
  final bool insuranceTypeDakor;
  final bool companyDakor;
  final bool productDakor;
  final bool periodDakor;
  final bool coverageTypeDakor;
  final bool coverageValueDakor;
  final bool upperLimitRateDakor;
  final bool upperLimitValueDakor;
  final bool lowerLimitRateDakor;
  final bool lowerLimitValueDakor;
  final bool cashDakor;
  final bool creditDakor;
  final bool totalPriceRateDakor;
  final bool totalPriceDakor;
  final String orderProductInsuranceID;

  AdditionalInsuranceModel(
      this.insuranceType,
      this.colaType,
      this.company,
      this.product,
      this.periodType,
      this.coverageType,
      this.coverageValue,
      this.upperLimitRate,
      this.lowerLimitRate,
      this.upperLimit,
      this.lowerLimit,
      this.priceCash,
      this.priceCredit,
      this.totalPriceRate,
      this.totalPrice,
      this.isEdit,
      this.insuranceTypeDakor,
      this.companyDakor,
      this.productDakor,
      this.periodDakor,
      this.coverageTypeDakor,
      this.coverageValueDakor,
      this.upperLimitRateDakor,
      this.upperLimitValueDakor,
      this.lowerLimitRateDakor,
      this.lowerLimitValueDakor,
      this.cashDakor,
      this.creditDakor,
      this.totalPriceRateDakor,
      this.totalPriceDakor,
      this.orderProductInsuranceID);
}
