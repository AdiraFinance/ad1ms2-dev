class FeeTypeModel{
  final String id;
  final String name;

  FeeTypeModel(this.id, this.name);
}