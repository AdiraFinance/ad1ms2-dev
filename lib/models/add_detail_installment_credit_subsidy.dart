class AddInstallmentDetailModel{
    final String installmentIndex;
    final String installmentSubsidy;
    final String orderSubsidyDetailID;

    AddInstallmentDetailModel(this.installmentIndex, this.installmentSubsidy, this.orderSubsidyDetailID);
}