class ShowMandatoryInfoAsuransiTambahanModel {
  final bool isInsuranceTypeSelectedVisible;
  final bool isCompanyVisible;
  final bool isProductVisible;
  final bool isPeriodTypeVisible;
  final bool isCoverageTypeVisible;
  final bool isCoverageValueVisible;
  final bool isUpperLimitRateVisible;
  final bool isUpperLimitVisible;
  final bool isLowerLimitRateVisible;
  final bool isLowerLimitVisible;
  final bool isPriceCashVisible;
  final bool isPriceCreditVisible;
  final bool isTotalPriceRateVisible;
  final bool isTotalPriceVisible;
  final bool isInsuranceTypeSelectedMandatory;
  final bool isCompanyMandatory;
  final bool isProductMandatory;
  final bool isPeriodTypeMandatory;
  final bool isCoverageTypeMandatory;
  final bool isCoverageValueMandatory;
  final bool isUpperLimitRateMandatory;
  final bool isUpperLimitMandatory;
  final bool isLowerLimitRateMandatory;
  final bool isLowerLimitMandatory;
  final bool isPriceCashMandatory;
  final bool isPriceCreditMandatory;
  final bool isTotalPriceRateMandatory;
  final bool isTotalPriceMandatory;

  ShowMandatoryInfoAsuransiTambahanModel(
  this.isInsuranceTypeSelectedVisible,
  this.isCompanyVisible,
  this.isProductVisible,
  this.isPeriodTypeVisible,
  this.isCoverageTypeVisible,
  this.isCoverageValueVisible,
  this.isUpperLimitRateVisible,
  this.isUpperLimitVisible,
  this.isLowerLimitRateVisible,
  this.isLowerLimitVisible,
  this.isPriceCashVisible,
  this.isPriceCreditVisible,
  this.isTotalPriceRateVisible,
  this.isTotalPriceVisible,
  this.isInsuranceTypeSelectedMandatory,
  this.isCompanyMandatory,
  this.isProductMandatory,
  this.isPeriodTypeMandatory,
  this.isCoverageTypeMandatory,
  this.isCoverageValueMandatory,
  this.isUpperLimitRateMandatory,
  this.isUpperLimitMandatory,
  this.isLowerLimitRateMandatory,
  this.isLowerLimitMandatory,
  this.isPriceCashMandatory,
  this.isPriceCreditMandatory,
  this.isTotalPriceRateMandatory,
  this.isTotalPriceMandatory);
}