class MS2CustPhotoModel {
  final String photo_id;
  final String order_no;
  final String work;
  final String work_desc;
  final String financing_type;
  final String financing_type_desc;
  final String business_activities;
  final String business_activities_desc;
  final String business_activities_type;
  final String business_activities_type_desc;
  final String concept_type;
  final String concept_type_desc;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final String active;

  MS2CustPhotoModel(
      this.photo_id,
      this.order_no,
      this.work,
      this.work_desc,
      this.financing_type,
      this.financing_type_desc,
      this.business_activities,
      this.business_activities_desc,
      this.business_activities_type,
      this.business_activities_type_desc,
      this.concept_type,
      this.concept_type_desc,
      this.created_date,
      this.created_by,
      this.modified_date,
      this.modified_by,
      this.active,
      );
}
