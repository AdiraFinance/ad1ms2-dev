class IdentityModel {
  final String id;
  final String name;

  IdentityModel(this.id, this.name);
}

class GCModel {
  final String id;
  final String name;

  GCModel(this.id, this.name);
}

class ReligionModel {
  final String id;
  final String text;

  ReligionModel(this.id, this.text);
}

class EducationModel {
  final String id;
  final String text;

  EducationModel(this.id, this.text);
}

class MaritalStatusModel {
  final String id;
  final String text;

  MaritalStatusModel(this.id, this.text);
}

class JenisNPWPModel {
  final String id;
  final String text;

  JenisNPWPModel(this.id, this.text);
}

class TandaPKPModel {
  final String id;
  final String text;

  TandaPKPModel(this.id, this.text);
}
