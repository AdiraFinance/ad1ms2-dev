class PhotoTypeModel{
  final String id;
  final String name;

  PhotoTypeModel(this.id, this.name);
}