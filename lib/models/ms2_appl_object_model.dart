class MS2ApplObjectModel {
  final String appl_objt_id;
  final String applObjtID;
  final String financing_type;
  final String buss_activities;
  final String buss_activities_desc;
  final String buss_activities_type;
  final String buss_activities_type_desc;
  final String group_object;
  final String group_object_desc;
  final String object;
  final String object_desc;
  final String product_type;
  final String product_type_desc;
  final String brand_object;
  final String brand_object_desc;
  final String object_type;
  final String object_type_desc;
  final String object_model;
  final String object_model_desc;
  final String model_detail;
  final String object_used;
  final String object_used_desc;
  final String object_purpose;
  final String object_purpose_desc;
  final String group_sales;
  final String group_sales_desc;
  final String grup_id;
  final String grup_id_desc;
  final String order_source;
  final String order_source_name;
  final int flag_third_party;
  final String third_party_type;
  final String third_party_type_desc;
  final String third_party_id;
  final String work;
  final String work_desc;
  final String sentra_d;
  final String sentra_d_desc;
  final String unit_d;
  final String unit_d_desc;
  final String program;
  final String program_desc;
  final String rehab_type;
  final String rehab_type_desc;
  final String reference_no;
  final String reference_no_desc;
  final String appl_contrac_no;
  final String installment_type;
  final String installment_type_desc;
  final int appl_top;
  final String payment_method;
  final String payment_method_desc;
  final double eff_rate;
  final double flat_rate;
  final double object_price;
  final double karoseri_price;
  final double total_amt;
  final double dp_net;
  final int installment_decline_n;
  final int payment_per_year;
  final double dp_branch;
  final double dp_gross;
  final double principal_amt;
  final double installment_paid;
  final int interest_amt;
  final double ltv;
  final int disbursement;
  final double dsr;
  final double dir;
  final double dsc;
  final double irr;
  final String gp_type;
  final int new_tenor;
  final int total_stepping;
  final String balloon_type;
  final double last_percen_installment;
  final double last_installment_amt;
  final int is_auto_debit;
  final String bank_id;
  final String account_no;
  final String account_behalf;
  final String purpose_type;
  final String dp_source;
  final String va_no;
  final int va_amt;
  final String disburse_purpose;
  final String disburse_type;
  final String appl_objt_last_state;
  final String appl_objt_next_state;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final int active;
  final int tact;
  final int tmax;
  final String appl_send_flag;
  final int is_without_colla;
  final int first_installment;
  final int flag_save_karoseri;
  final int pelunasan_amt;
  final String appl_withdrawal_flag;
  final String appl_model_dtl;
  final int insentif_sales;
  final int insentif_payment;
  final String nik;
  final String nama;
  final String jabatan;
  final String outlet_id;
  final String group_id;
  final String dealer_matrix_id;
  final String dealer_matrix;
  final String ppd_date;
  final String ppd_cancel_date;
  final int dp_chasis_net;
  final int dp_chasis_gross;
  final int ujrah_price;
  final int total_dp_krs;
  final String name_order_source;
  final String name_order_source_desc;
  final String third_party_desc;
  final String edit_financing_type;
  final String edit_buss_activities;
  final String edit_buss_activities_type;
  final String edit_group_object;
  final String edit_object;
  final String edit_product_type;
  final String edit_brand_object;
  final String edit_object_type;
  final String edit_object_model;
  final String edit_model_detail;
  final String edit_object_used;
  final String edit_object_purpose;
  final String edit_group_sales;
  final String edit_grup_id;
  final String edit_order_source;
  final String edit_name_order_source;
  final String edit_third_party_type;
  final String edit_third_party_id;
  final String edit_dealer_matrix;
  final String edit_work;
  final String edit_sentra_d;
  final String edit_unit_d;
  final String edit_program;
  final String edit_rehab_type;
  final String edit_reference_no;

  MS2ApplObjectModel (
      this.appl_objt_id,
      this.applObjtID,
      this.financing_type,
      this.buss_activities,
      this.buss_activities_desc,
      this.buss_activities_type,
      this.buss_activities_type_desc,
      this.group_object,
      this.group_object_desc,
      this.object,
      this.object_desc,
      this.product_type,
      this.product_type_desc,
      this.brand_object,
      this.brand_object_desc,
      this.object_type,
      this.object_type_desc,
      this.object_model,
      this.object_model_desc,
      this.model_detail,
      this.object_used,
      this.object_used_desc,
      this.object_purpose,
      this.object_purpose_desc,
      this.group_sales,
      this.group_sales_desc,
      this.grup_id,
      this.grup_id_desc,
      this.order_source,
      this.order_source_name,
      this.flag_third_party,
      this.third_party_type,
      this.third_party_type_desc,
      this.third_party_id,
      this.work,
      this.work_desc,
      this.sentra_d,
      this.sentra_d_desc,
      this.unit_d,
      this.unit_d_desc,
      this.program,
      this.program_desc,
      this.rehab_type,
      this.rehab_type_desc,
      this.reference_no,
      this.reference_no_desc,
      this.appl_contrac_no,
      this.installment_type,
      this.installment_type_desc,
      this.appl_top,
      this.payment_method,
      this.payment_method_desc,
      this.eff_rate,
      this.flat_rate,
      this.object_price,
      this.karoseri_price,
      this.total_amt,
      this.dp_net,
      this.installment_decline_n,
      this.payment_per_year,
      this.dp_branch,
      this.dp_gross,
      this.principal_amt,
      this.installment_paid,
      this.interest_amt,
      this.ltv,
      this.disbursement,
      this.dsr,
      this.dir,
      this.dsc,
      this.irr,
      this.gp_type,
      this.new_tenor,
      this.total_stepping,
      this.balloon_type,
      this.last_percen_installment,
      this.last_installment_amt,
      this.is_auto_debit,
      this.bank_id,
      this.account_no,
      this.account_behalf,
      this.purpose_type,
      this.dp_source,
      this.va_no,
      this.va_amt,
      this.disburse_purpose,
      this.disburse_type,
      this.appl_objt_last_state,
      this.appl_objt_next_state,
      this.created_date,
      this.created_by,
      this.modified_date,
      this.modified_by,
      this.active,
      this.tact,
      this.tmax,
      this.appl_send_flag,
      this.is_without_colla,
      this.first_installment,
      this.flag_save_karoseri,
      this.pelunasan_amt,
      this.appl_withdrawal_flag,
      this.appl_model_dtl,
      this.insentif_sales,
      this.insentif_payment,
      this.nik,
      this.nama,
      this.jabatan,
      this.outlet_id,
      this.group_id,
      this.dealer_matrix_id,
      this.dealer_matrix,
      this.ppd_date,
      this.ppd_cancel_date,
      this.dp_chasis_net,
      this.dp_chasis_gross,
      this.ujrah_price,
      this.total_dp_krs,
      this.name_order_source,
      this.name_order_source_desc,
      this.third_party_desc, this.edit_financing_type, this.edit_buss_activities, this.edit_buss_activities_type, this.edit_group_object, this.edit_object, this.edit_product_type, this.edit_brand_object, this.edit_object_type, this.edit_object_model, this.edit_model_detail, this.edit_object_used, this.edit_object_purpose, this.edit_group_sales, this.edit_grup_id, this.edit_order_source, this.edit_name_order_source, this.edit_third_party_type, this.edit_third_party_id, this.edit_dealer_matrix, this.edit_work, this.edit_sentra_d, this.edit_unit_d, this.edit_program, this.edit_rehab_type, this.edit_reference_no,
      );
}