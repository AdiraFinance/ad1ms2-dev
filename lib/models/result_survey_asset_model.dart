import 'package:ad1ms2_dev/models/asset_type_model.dart';
import 'package:ad1ms2_dev/models/electricity_type_model.dart';
import 'package:ad1ms2_dev/models/ownership_model.dart';
import 'package:ad1ms2_dev/models/road_type_model.dart';

class ResultSurveyAssetModel {
  final String surveyResultAssetID;
  final AssetTypeModel assetTypeModel;
  final String valueAsset;
  final OwnershipModel ownershipModel;
  final String surfaceBuildingArea;
  final RoadTypeModel roadTypeModel;
  final ElectricityTypeModel electricityTypeModel;
  final String electricityBills;
  final DateTime endDateLease;
  final String lengthStay;
  final bool isEditResultSurveyAsset;
  final bool isAssetTypeChanges;
  final bool isValueAssetChanges;
  final bool isOwneshipInfoChanges;
  final bool isSurfaceBuildingAreaInfoChanges;
  final bool isRoadTypeInfoChanges;
  final bool isElectricityTypeInfoChanges;
  final bool isElectricityBillInfoChanges;
  final bool isEndDateInfoChanges;
  final bool isLengthStayInfoChanges;

  ResultSurveyAssetModel(
      this.surveyResultAssetID,
      this.assetTypeModel,
      this.valueAsset,
      this.ownershipModel,
      this.surfaceBuildingArea,
      this.roadTypeModel,
      this.electricityTypeModel,
      this.electricityBills,
      this.endDateLease,
      this.lengthStay,
      this.isEditResultSurveyAsset,
      this.isAssetTypeChanges,
      this.isValueAssetChanges,
      this.isOwneshipInfoChanges,
      this.isSurfaceBuildingAreaInfoChanges,
      this.isRoadTypeInfoChanges,
      this.isElectricityTypeInfoChanges,
      this.isElectricityBillInfoChanges,
      this.isEndDateInfoChanges,
      this.isLengthStayInfoChanges);
}
