class TypeModel{
    final String id;
    final String text;

    TypeModel(this.id, this.text);
}