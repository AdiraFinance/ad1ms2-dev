class ShowMandatoryInfoStrukturKreditFloatingModel {
  final bool isInstallmentSelectedFloatingVisible;
  final bool isInterestRateVisible;
  final bool isInstallmentSelectedFloatingMandatory;
  final bool isInterestRateMandatory;

  ShowMandatoryInfoStrukturKreditFloatingModel(
    this.isInstallmentSelectedFloatingVisible,
    this.isInterestRateVisible,
    this.isInstallmentSelectedFloatingMandatory,
    this.isInterestRateMandatory);
}