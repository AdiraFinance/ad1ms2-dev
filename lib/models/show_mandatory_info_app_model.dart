class ShowMandatoryInfoAppModel {
  final bool isOrderDateVisible;
  final bool isSurveyAppointmentDateVisible;
  final bool isIsSignedPKVisible;
  final bool isConceptTypeModelVisible;
  final bool isTotalObjectVisible;
  final bool isPropotionalObjectVisible;
  final bool isNumberOfUnitVisible;
  final bool isOrderDateMandatory;
  final bool isSurveyAppointmentDateMandatory;
  final bool isIsSignedPKMandatory;
  final bool isConceptTypeModelMandatory;
  final bool isTotalObjectMandatory;
  final bool isPropotionalObjectMandatory;
  final bool isNumberOfUnitMandatory;

  ShowMandatoryInfoAppModel(
    this.isOrderDateVisible,
    this.isSurveyAppointmentDateVisible,
    this.isIsSignedPKVisible,
    this.isConceptTypeModelVisible,
    this.isTotalObjectVisible,
    this.isPropotionalObjectVisible,
    this.isNumberOfUnitVisible,
    this.isOrderDateMandatory,
    this.isSurveyAppointmentDateMandatory,
    this.isIsSignedPKMandatory,
    this.isConceptTypeModelMandatory,
    this.isTotalObjectMandatory,
    this.isPropotionalObjectMandatory,
    this.isNumberOfUnitMandatory);
}