class MS2GrntrIndModel {
  final String grntr_ind_id;
  final String guarantorIndividualID;
  final String relation_status;
  final String relation_status_desc;
  final int dedup_score;
  final String id_type;
  final String id_desc;
  final String id_no;
  final String full_name_id;
  final String full_name;
  final String alias_name;
  final String degree;
  final String date_of_birth;
  final String place_of_birth;
  final String place_of_birth_kabkota;
  final String place_of_birth_kabkota_desc;
  final String id_date;
  final String id_expire_date;
  final String religion;
  final String religion_desc;
  final String occupation;
  final String occupation_desc;
  final String ac_level;
  final String gender;
  final String handphone_no;
  final String email;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final int active;
  final String edit_relation_status;
  final String edit_id_type;
  final String edit_id_no;
  final String edit_full_name_id;
  final String edit_full_name;
  final String edit_date_of_birth;
  final String edit_place_of_birth;
  final String edit_place_of_birth_kabkota;
  final String edit_gender;
  final String edit_handphone_no;

  MS2GrntrIndModel(
      this.grntr_ind_id,
      this.guarantorIndividualID,
      this.relation_status,
      this.relation_status_desc,
      this.dedup_score,
      this.id_type,
      this.id_desc,
      this.id_no,
      this.full_name_id,
      this.full_name,
      this.alias_name,
      this.degree,
      this.date_of_birth,
      this.place_of_birth,
      this.place_of_birth_kabkota,
      this.place_of_birth_kabkota_desc,
      this.id_date,
      this.id_expire_date,
      this.religion,
      this.religion_desc,
      this.occupation,
      this.occupation_desc,
      this.ac_level,
      this.gender,
      this.handphone_no,
      this.email,
      this.created_date,
      this.created_by,
      this.modified_date,
      this.modified_by,
      this.active, this.edit_relation_status, this.edit_id_type, this.edit_id_no, this.edit_full_name_id, this.edit_full_name, this.edit_date_of_birth, this.edit_place_of_birth, this.edit_place_of_birth_kabkota, this.edit_gender, this.edit_handphone_no,
      );
}
