class ListVoucher {
  final String voucher_code;
  final String contract_no;
  final String product_type;
  final String product_type_desc;
  final String plafond_installment;
  final String tenor;
  final String disburse_type;
  final String start_date;
  final String end_date;

  ListVoucher(
    this.voucher_code,
    this.contract_no,
    this.product_type,
    this.product_type_desc,
    this.plafond_installment,
    this.tenor,
    this.disburse_type,
    this.start_date,
    this.end_date,
  );
}