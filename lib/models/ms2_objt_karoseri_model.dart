class MS2ObjtKaroseriModel {
  final String appl_karoseri_id;
  final String orderKaroseriID;
  final int pks_karoseri;
  final String comp_karoseri;
  final String comp_karoseri_desc;
  final String karoseri;
  final String karoseri_desc;
  final double price;
  final int karoseri_qty;
  final double total_karoseri;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final int active;
  final int dp_karoseri;
  final int flag_karoseri;
  final String edit_pks_karoseri;
  final String edit_comp_karoseri;
  final String edit_karoseri;
  final String edit_karoseri_qty;
  final String edit_price;
  final String edit_total_karoseri;

  MS2ObjtKaroseriModel(
    this.appl_karoseri_id,
    this.orderKaroseriID,
    this.pks_karoseri,
    this.comp_karoseri,
    this.comp_karoseri_desc,
    this.karoseri,
    this.karoseri_desc,
    this.price,
    this.karoseri_qty,
    this.total_karoseri,
    this.created_date,
    this.created_by,
    this.modified_date,
    this.modified_by,
    this.active,
    this.dp_karoseri,
    this.flag_karoseri,
    this.edit_pks_karoseri,
    this.edit_comp_karoseri,
    this.edit_karoseri,
    this.edit_karoseri_qty,
    this.edit_price,
    this.edit_total_karoseri,
  );
}
