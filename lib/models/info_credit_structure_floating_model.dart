class InfoCreditStructureFloatingModel{
  final String installment;
  final String interestRateEffFloat;

  InfoCreditStructureFloatingModel(this.installment, this.interestRateEffFloat);
}