import 'package:ad1ms2_dev/models/environmental_information_model.dart';
import 'package:ad1ms2_dev/models/recources_survey_info_model.dart';

class ResultSurveyDetailSurveyModel{
  final String surveyResultDetailID;
  final EnvironmentalInformationModel environmentalInformationModel;
  final ResourcesInfoSurveyModel recourcesInfoSurveyModel;
  final String resourceInformationName;
  final bool isEditResultSurveyDetail;
  final bool isEnvInformationChange;
  final bool isSourceInformationChange;
  final bool isSourceInfoNameChange;

  ResultSurveyDetailSurveyModel(this.surveyResultDetailID, this.environmentalInformationModel, this.recourcesInfoSurveyModel, this.resourceInformationName, this.isEnvInformationChange, this.isSourceInformationChange, this.isSourceInfoNameChange, this.isEditResultSurveyDetail);
}