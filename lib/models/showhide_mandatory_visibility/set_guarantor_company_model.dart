class SetGuarantorCompanyModel {
  final bool companyTypeVisible;
  final bool companyNameVisible;
  final bool establishedDateVisible;
  final bool npwpVisible;

  final bool companyTypeMandatory;
  final bool companyNameMandatory;
  final bool establishedDateMandatory;
  final bool npwpMandatory;

  SetGuarantorCompanyModel(
      this.companyTypeVisible,
      this.companyNameVisible,
      this.establishedDateVisible,
      this.npwpVisible,
      this.companyTypeMandatory,
      this.companyNameMandatory,
      this.establishedDateMandatory,
      this.npwpMandatory);
}
