class SetAddressCompanyModel {
  //show hide form
  final bool isAddressTypeShow;
  final bool isAddressShow;
  final bool isRTShow;
  final bool isRWShow;
  final bool isKelurahanShow;
  final bool isKecamatanShow;
  final bool isKabKotShow;
  final bool isProvinsiShow;
  final bool isKodePosShow;
  final bool isTelp1AreaShow;
  final bool isTelp1Show;
  final bool isTelp2AreaShow;
  final bool isTelp2Show;
  final bool isFaxAreaShow;
  final bool isFaxShow;

  //mandatory form
  final bool isAddressTypeMandatory;
  final bool isAddressMandatory;
  final bool isRTMandatory;
  final bool isRWMandatory;
  final bool isKelurahanMandatory;
  final bool isKecamatanMandatory;
  final bool isKabKotMandatory;
  final bool isProvinsiMandatory;
  final bool isKodePosMandatory;
  final bool isTelp1AreaMandatory;
  final bool isTelp1Mandatory;
  final bool isTelp2AreaMandatory;
  final bool isTelp2Mandatory;
  final bool isFaxAreaMandatory;
  final bool isFaxMandatory;

  SetAddressCompanyModel(
      this.isAddressTypeShow,
      this.isAddressShow,
      this.isRTShow,
      this.isRWShow,
      this.isKelurahanShow,
      this.isKecamatanShow,
      this.isKabKotShow,
      this.isProvinsiShow,
      this.isKodePosShow,
      this.isTelp1AreaShow,
      this.isTelp1Show,
      this.isTelp2AreaShow,
      this.isTelp2Show,
      this.isFaxAreaShow,
      this.isFaxShow,
      this.isAddressTypeMandatory,
      this.isAddressMandatory,
      this.isRTMandatory,
      this.isRWMandatory,
      this.isKelurahanMandatory,
      this.isKecamatanMandatory,
      this.isKabKotMandatory,
      this.isProvinsiMandatory,
      this.isKodePosMandatory,
      this.isTelp1AreaMandatory,
      this.isTelp1Mandatory,
      this.isTelp2AreaMandatory,
      this.isTelp2Mandatory,
      this.isFaxAreaMandatory,
      this.isFaxMandatory
      );

}
