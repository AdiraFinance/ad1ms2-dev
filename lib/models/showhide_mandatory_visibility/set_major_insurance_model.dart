class SetMajorInsuranceModel {
  final bool insuranceTypeVisible;
  final bool installmentNoVisible;
  final bool companyVisible;
  final bool productVisible;
  final bool periodVisible;
  final bool typeVisible;
  final bool coverage1Visible;
  final bool coverage2Visible;
  final bool coverageTypeVisible;
  final bool coverageValueVisible;
  final bool upperLimitRateVisible;
  final bool upperLimitValueVisible;
  final bool lowerLimitRateVisible;
  final bool lowerLimitValueVisible;
  final bool cashVisible;
  final bool creditVisible;
  final bool totalPriceSplitVisible;
  final bool totalPriceVisible;
  final bool totalPriceRateVisible;

  final bool insuranceTypeMandatory;
  final bool installmentNoMandatory;
  final bool companyMandatory;
  final bool productMandatory;
  final bool periodMandatory;
  final bool typeMandatory;
  final bool coverage1Mandatory;
  final bool coverage2Mandatory;
  final bool coverageTypeMandatory;
  final bool coverageValueMandatory;
  final bool upperLimitRateMandatory;
  final bool upperLimitValueMandatory;
  final bool lowerLimitRateMandatory;
  final bool lowerLimitValueMandatory;
  final bool cashMandatory;
  final bool creditMandatory;
  final bool totalPriceSplitMandatory;
  final bool totalPriceMandatory;
  final bool totalPriceRateMandatory;

  SetMajorInsuranceModel(
      this.insuranceTypeVisible,
      this.installmentNoVisible,
      this.companyVisible,
      this.productVisible,
      this.periodVisible,
      this.typeVisible,
      this.coverage1Visible,
      this.coverage2Visible,
      this.coverageTypeVisible,
      this.coverageValueVisible,
      this.upperLimitRateVisible,
      this.upperLimitValueVisible,
      this.lowerLimitRateVisible,
      this.lowerLimitValueVisible,
      this.cashVisible,
      this.creditVisible,
      this.totalPriceSplitVisible,
      this.totalPriceVisible,
      this.totalPriceRateVisible,
      this.insuranceTypeMandatory,
      this.installmentNoMandatory,
      this.companyMandatory,
      this.productMandatory,
      this.periodMandatory,
      this.typeMandatory,
      this.coverage1Mandatory,
      this.coverage2Mandatory,
      this.coverageTypeMandatory,
      this.coverageValueMandatory,
      this.upperLimitRateMandatory,
      this.upperLimitValueMandatory,
      this.lowerLimitRateMandatory,
      this.lowerLimitValueMandatory,
      this.cashMandatory,
      this.creditMandatory,
      this.totalPriceSplitMandatory,
      this.totalPriceMandatory,
      this.totalPriceRateMandatory);
}
