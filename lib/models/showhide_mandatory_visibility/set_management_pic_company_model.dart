class SetManagementPICCompanyModel {
  final bool identityTypeVisible;
  final bool identityNoVisible;
  final bool fullnameIdVisible;
  final bool fullnameVisible;
  final bool dateOfBirthVisible;
  final bool placeOfBirthVisible;
  final bool placeOfBirthLOVVisible;
  final bool positionVisible;
  final bool emailVisible;
  final bool phoneVisible;

  final bool identityTypeMandatory;
  final bool identityNoMandatory;
  final bool fullnameIdMandatory;
  final bool fullnameMandatory;
  final bool dateOfBirthMandatory;
  final bool placeOfBirthMandatory;
  final bool placeOfBirthLOVMandatory;
  final bool positionMandatory;
  final bool emailMandatory;
  final bool phoneMandatory;

  SetManagementPICCompanyModel(
      this.identityTypeVisible,
      this.identityNoVisible,
      this.fullnameIdVisible,
      this.fullnameVisible,
      this.dateOfBirthVisible,
      this.placeOfBirthVisible,
      this.placeOfBirthLOVVisible,
      this.positionVisible,
      this.emailVisible,
      this.phoneVisible,
      this.identityTypeMandatory,
      this.identityNoMandatory,
      this.fullnameIdMandatory,
      this.fullnameMandatory,
      this.dateOfBirthMandatory,
      this.placeOfBirthMandatory,
      this.placeOfBirthLOVMandatory,
      this.positionMandatory,
      this.emailMandatory,
      this.phoneMandatory);
}
