class SetPemegangSahamLembagaCompanyModel {
  final bool companyTypeVisibility;
  final bool companyNameVisibility;
  final bool establishDateVisibility;
  final bool npwpVisibility;
  final bool shareVisibility;

  final bool companyTypeMandatory;
  final bool companyNameMandatory;
  final bool establishDateMandatory;
  final bool npwpMandatory;
  final bool shareMandatory;

  SetPemegangSahamLembagaCompanyModel(
      this.companyTypeVisibility,
      this.companyNameVisibility,
      this.establishDateVisibility,
      this.npwpVisibility,
      this.shareVisibility,
      this.companyTypeMandatory,
      this.companyNameMandatory,
      this.establishDateMandatory,
      this.npwpMandatory,
      this.shareMandatory
  );
}
