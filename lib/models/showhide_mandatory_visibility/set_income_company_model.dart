class SetIncomeCompanyModel {
  final bool monthIncomeVisible;
  final bool otherIncomeVisible;
  final bool totalIncomeVisible;
  final bool hppIncomeVisible;
  final bool labaKotorVisible;
  final bool operationalCostVisible;
  final bool otherCostVisible;
  final bool labaBeforeTaxVisible;
  final bool taxVisible;
  final bool labaAfterTaxVisible;
  final bool otherInstallmentVisible;

  final bool monthIncomeMandatory;
  final bool otherIncomeMandatory;
  final bool totalIncomeMandatory;
  final bool hppIncomeMandatory;
  final bool labaKotorMandatory;
  final bool operationalCostMandatory;
  final bool otherCostMandatory;
  final bool labaBeforeVATMandatory;
  final bool taxMandatory;
  final bool labaAfterVatMandatory;
  final bool otherInstallmentMandatory;

  SetIncomeCompanyModel(
      this.monthIncomeVisible,
      this.otherIncomeVisible,
      this.totalIncomeVisible,
      this.hppIncomeVisible,
      this.labaKotorVisible,
      this.operationalCostVisible,
      this.otherCostVisible,
      this.labaBeforeTaxVisible,
      this.taxVisible,
      this.labaAfterTaxVisible,
      this.otherInstallmentVisible,
      this.monthIncomeMandatory,
      this.otherIncomeMandatory,
      this.totalIncomeMandatory,
      this.hppIncomeMandatory,
      this.labaKotorMandatory,
      this.operationalCostMandatory,
      this.otherCostMandatory,
      this.labaBeforeVATMandatory,
      this.taxMandatory,
      this.labaAfterVatMandatory,
      this.otherInstallmentMandatory);
}
