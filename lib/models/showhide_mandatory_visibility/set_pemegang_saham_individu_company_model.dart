class SetPemegangSahamIndividuCompanyModel {
  final bool relationshipStatusVisibility;
  final bool identityTypeVisibility;
  final bool identityNoVisibility;
  final bool fullnameIDVisibility;
  final bool fullnameVisibility;
  final bool dateOfBirthVisibility;
  final bool placeOfBirthVisibility;
  final bool placeOfBirthLOVVisibility;
  final bool genderVisibility;
  final bool shareVisibility;

  final bool relationshipStatusMandatory;
  final bool identityTypeMandatory;
  final bool identityNoMandatory;
  final bool fullnameIDMandatory;
  final bool fullnameMandatory;
  final bool dateOfBirthMandatory;
  final bool placeOfBirthMandatory;
  final bool placeOfBirthLOVMandatory;
  final bool genderMandatory;
  final bool shareMandatory;

  SetPemegangSahamIndividuCompanyModel(
      this.relationshipStatusVisibility,
      this.identityTypeVisibility,
      this.identityNoVisibility,
      this.fullnameIDVisibility,
      this.fullnameVisibility,
      this.dateOfBirthVisibility,
      this.placeOfBirthVisibility,
      this.placeOfBirthLOVVisibility,
      this.genderVisibility,
      this.shareVisibility,
      this.relationshipStatusMandatory,
      this.identityTypeMandatory,
      this.identityNoMandatory,
      this.fullnameIDMandatory,
      this.fullnameMandatory,
      this.dateOfBirthMandatory,
      this.placeOfBirthMandatory,
      this.placeOfBirthLOVMandatory,
      this.genderMandatory,
      this.shareMandatory);
}
