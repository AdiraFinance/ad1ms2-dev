class ShowMandatoryMarketingNotesModel {
  final bool isMarketingNotesVisible;
  final bool isMarketingNotesMandatory;

  ShowMandatoryMarketingNotesModel(
      this.isMarketingNotesVisible,
      this.isMarketingNotesMandatory,);
}