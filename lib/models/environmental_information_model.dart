class EnvironmentalInformationModel{
  final String id;
  final String name;

  EnvironmentalInformationModel(this.id, this.name);
}