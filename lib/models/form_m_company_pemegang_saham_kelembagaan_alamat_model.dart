import 'package:ad1ms2_dev/models/form_m_informasi_alamat_model.dart';

class CompanyPemegangSahamKelembagaanAlamatModel {
  final JenisAlamatModel jenisAlamatModel;
  final KelurahanModel kelurahanModel;
  final String address;
  final String rt;
  final String rw;
  final String phone1AreaCode;
  final String phone1;
  final String phone2AreaCode;
  final String phone2;
  final String faxAreaCode;
  final String fax;
  final bool isSameWithIdentity;

  CompanyPemegangSahamKelembagaanAlamatModel(this.jenisAlamatModel, this.kelurahanModel, this.address, this.rt, this.rw, this.phone1AreaCode, this.phone1, this.phone2AreaCode, this.phone2, this.faxAreaCode, this.fax, this.isSameWithIdentity);
}
