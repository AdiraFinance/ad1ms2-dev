class EmployeeHeadModel {
  final String NIK;
  final String Nama;
  final String Jabatan;
  final String AliasJabatan;

  EmployeeHeadModel(this.NIK, this.Nama, this.Jabatan, this.AliasJabatan);
}
