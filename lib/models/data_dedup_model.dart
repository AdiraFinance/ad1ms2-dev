class DataDedupModel{
  final String flag;
  final String no_identitas;
  final String nama;
  final String tanggal_lahir;
  final String tempat_lahir_sesuai_identitas;
  final String nama_gadis_ibu_kandung;
  final String alamat;
  final String status_aoro;
  final String created_date;
  final String created_by;
  final String updated_date;
  final String updated_by;

  DataDedupModel(this.flag, this.no_identitas, this.nama, this.tanggal_lahir, this.tempat_lahir_sesuai_identitas, this.nama_gadis_ibu_kandung, this.alamat, this.status_aoro, this.created_date, this.created_by, this.updated_date, this.updated_by);
}