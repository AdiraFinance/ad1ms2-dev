import 'package:ad1ms2_dev/models/birth_place_model.dart';
import 'package:ad1ms2_dev/models/form_m_company_manajemen_pic_model.dart';
import 'package:ad1ms2_dev/models/form_m_foto_model.dart';
import 'package:ad1ms2_dev/models/form_m_informasi_nasabah_model.dart';

class ManagementPICSQLiteModel {
  final TypeIdentityModel identityModel;
  final String id_no;
  final String full_name_id;
  final String full_name;
  final String alias_name;
  final String degree;
  final String date_of_birth;
  final String place_of_birth;
  final BirthPlaceModel birthPlaceModel;
  final String gender;
  final String gender_desc;
  final String id_date;
  final String id_expire_date;
  final ReligionModel religionModel;
  final OccupationModel occupationModel;
  final String ac_level;
  final String position;
  final String handphone_no;
  final String email;
  final int shrldng_percent;
  final int dedup_score;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final int active;
  final MaritalStatusModel maritalStatusModel;
  final String identityTypeDakor;
  final String identityNoDakor;
  final String fullnameIdDakor;
  final String fullnameDakor;
  final String dateOfBirthDakor;
  final String placeOfBirthDakor;
  final String placeOfBirthLOVDakor;
  final String positionDakor;
  final String emailDakor;
  final String phoneDakor;

  ManagementPICSQLiteModel(
      this.identityModel,
      this.id_no,
      this.full_name_id,
      this.full_name,
      this.alias_name,
      this.degree,
      this.date_of_birth,
      this.place_of_birth,
      this.birthPlaceModel,
      this.gender,
      this.gender_desc,
      this.id_date,
      this.id_expire_date,
      this.religionModel,
      this.occupationModel,
      this.ac_level,
      this.position,
      this.handphone_no,
      this.email,
      this.shrldng_percent,
      this.dedup_score,
      this.created_date,
      this.created_by,
      this.modified_date,
      this.modified_by,
      this.active,
      this.maritalStatusModel,
      this.identityTypeDakor,
      this.identityNoDakor,
      this.fullnameIdDakor,
      this.fullnameDakor,
      this.dateOfBirthDakor,
      this.placeOfBirthDakor,
      this.placeOfBirthLOVDakor,
      this.positionDakor,
      this.emailDakor,
      this.phoneDakor);
}
