class ModelObjectModel {
  final String id;
  final String name;

  ModelObjectModel(this.id, this.name);
}
