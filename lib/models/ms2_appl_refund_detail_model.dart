class MS2ApplRefundDetailModel {
  final String ac_subsidy_dtl_id;
  final String orderSubsidyDetailID;
  final int ac_installment_no;
  final double ac_installment_amt;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final int active;

  MS2ApplRefundDetailModel (
      this.ac_subsidy_dtl_id,
      this.orderSubsidyDetailID,
      this.ac_installment_no,
      this.ac_installment_amt,
      this.created_date,
      this.created_by,
      this.modified_date,
      this.modified_by,
      this.active,
      );
}