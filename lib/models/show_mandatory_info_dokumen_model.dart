class ShowMandatoryInfoDokumenModel {
  final bool isDocumentTypeVisible;
  final bool isDateDocumentVisible;
  final bool isBrowseDocumentVisible;
  final bool isDocumentTypeMandatory;
  final bool isDateDocumentMandatory;
  final bool isBrowseDocumentMandatory;

  ShowMandatoryInfoDokumenModel(
    this.isDocumentTypeVisible,
    this.isDateDocumentVisible,
    this.isBrowseDocumentVisible,
    this.isDocumentTypeMandatory,
    this.isDateDocumentMandatory,
    this.isBrowseDocumentMandatory);
}