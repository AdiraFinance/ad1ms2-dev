class FloorTypeModel{
  final String id;
  final String name;

  FloorTypeModel(this.id, this.name);
}