import 'package:ad1ms2_dev/models/fee_type_model.dart';

class InfoFeeCreditStructureModel {
  final FeeTypeModel feeTypeModel;
  final String cashCost;
  final String creditCost;
  final String totalCost;
  final String orderFeeID;
  final bool editFeeType;
  final bool editFeeCash;
  final bool editFeeCredit;
  final bool editTotalFee;

  InfoFeeCreditStructureModel(
      this.feeTypeModel,
      this.cashCost,
      this.creditCost,
      this.totalCost,
      this.orderFeeID,
      this.editFeeType,
      this.editFeeCash,
      this.editFeeCredit,
      this.editTotalFee);
}
