class MS2ApplicationModel {
  final String appl_no;
  final String applNo;
  final int flag_mayor;
  final int flag_fiducia;
  final String source_application;
  final String order_date;
  final String application_date;
  final String svy_appointment_date;
  final String order_type;
  final int flag_account;
  final String account_no_form;
  final String sentra_c;
  final String sentra_c_desc;
  final String unit_c;
  final String unit_c_desc;
  final String initial_recomendation;
  final String final_recomendation;
  final int brms_scoring;
  final int persetujuan_di_tempat;
  final int sign_pk;
  final int max_level_approval;
  final int flag_receive_information;
  final String konsep_type;
  final String konsep_type_desc;
  final int objt_qty;
  final String prop_insr_type;
  final String prop_insr_type_desc;
  final int unit;
  final String cola_type;
  final String cola_type_desc;
  final String appl_contract_no;
  final String calculated_at;
  final String last_known_appl_state;
  final String last_known_appl_position;
  final String last_known_appl_status;
  final String last_known_handled_by;
  final String marketing_notes;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final int active;
  final String doc_ver_status;
  final String cust_status;
  final String appr_aos_status;
  final int flag_das_pps;
  final int flag_upload;
  final int flag_ia;
  final int flag_dakor_ca;
  final int flag_bypasscapo;
  final int flag_dedup_ca;
  final int flag_disable_dedup_ca;
  final String order_ad1gate;
  final String user_ad1gate;
  final String dealer_note;
  final String flag_source_ms2;
  final String edit_order_date;
  final String edit_svy_appointment_date;
  final String edit_sign_pk;
  final String edit_konsep_type;
  final String edit_objt_qty;
  final String edit_prop_insr_type;
  final String edit_unit;
  final String edit_marketing_notes;

  MS2ApplicationModel(
    this.appl_no,
    this.applNo,
    this.flag_mayor,
    this.flag_fiducia,
    this.source_application,
    this.order_date,
    this.application_date,
    this.svy_appointment_date,
    this.order_type,
    this.flag_account,
    this.account_no_form,
    this.sentra_c,
    this.sentra_c_desc,
    this.unit_c,
    this.unit_c_desc,
    this.initial_recomendation,
    this.final_recomendation,
    this.brms_scoring,
    this.persetujuan_di_tempat,
    this.sign_pk,
    this.max_level_approval,
    this.flag_receive_information,
    this.konsep_type,
    this.konsep_type_desc,
    this.objt_qty,
    this.prop_insr_type,
    this.prop_insr_type_desc,
    this.unit,
    this.cola_type,
    this.cola_type_desc,
    this.appl_contract_no,
    this.calculated_at,
    this.last_known_appl_state,
    this.last_known_appl_position,
    this.last_known_appl_status,
    this.last_known_handled_by,
    this.marketing_notes,
    this.created_date,
    this.created_by,
    this.modified_date,
    this.modified_by,
    this.active,
    this.doc_ver_status,
    this.cust_status,
    this.appr_aos_status,
    this.flag_das_pps,
    this.flag_upload,
    this.flag_ia,
    this.flag_dakor_ca,
    this.flag_bypasscapo,
    this.flag_dedup_ca,
    this.flag_disable_dedup_ca,
    this.order_ad1gate,
    this.user_ad1gate,
    this.dealer_note,
    this.flag_source_ms2,
    this.edit_order_date,
    this.edit_svy_appointment_date,
    this.edit_sign_pk,
    this.edit_konsep_type,
    this.edit_objt_qty,
    this.edit_prop_insr_type,
    this.edit_unit,
    this.edit_marketing_notes,
  );
}
