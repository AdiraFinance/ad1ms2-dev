class CheckMplActivationModel {
  final String branch_code;
  final String source_reff_id;
  final String reff_id;
  final String source_channel_id;
  final String oid;
  final String lme_id;
  final String mpl_type;
  final String jenis_kegiatan_usaha;
  final String mpl_status;
  final String disbursement_number;
  final String potensial_disbursement_no;
  final String capacity_limit_mpl;
  final String capacity_ph_mpl;
  final String activation_date;
  final String remaining_tenor;
  final String contract_reference;
  final String activation_branch_code;
  final String activation_channel;
  final String response_code;
  final String response_desc;
  final String response_date;
  final String req_date;

  CheckMplActivationModel(
    this.branch_code,
    this.source_reff_id,
    this.reff_id,
    this.source_channel_id,
    this.oid,
    this.lme_id,
    this.mpl_type,
    this.jenis_kegiatan_usaha,
    this.mpl_status,
    this.disbursement_number,
    this.potensial_disbursement_no,
    this.capacity_limit_mpl,
    this.capacity_ph_mpl,
    this.activation_date,
    this.remaining_tenor,
    this.contract_reference,
    this.activation_branch_code,
    this.activation_channel,
    this.response_code,
    this.response_desc,
    this.response_date,
    this.req_date,
  );
}