class JenisAlamatModel {
  final String KODE;
  final String DESKRIPSI;

  JenisAlamatModel(this.KODE, this.DESKRIPSI);
}

class KelurahanModel {
  final String KEL_ID;
  final String KEL_NAME;
  final String KEC_ID;
  final String KEC_NAME;
  final String KABKOT_ID;
  final String KABKOT_NAME;
  final String PROV_ID;
  final String PROV_NAME;
  final String ZIPCODE;

  KelurahanModel(this.KEL_ID, this.KEL_NAME, this.KEC_NAME, this.KABKOT_NAME,
      this.PROV_NAME, this.ZIPCODE, this.KEC_ID, this.KABKOT_ID, this.PROV_ID);
}

class AddressModel {
  final JenisAlamatModel jenisAlamatModel;
  final String addressID;
  final String foreignBusinessID;
  final String numberID;
  final KelurahanModel kelurahanModel;
  final String address;
  final String rt;
  final String rw;
  final String areaCode;
  final String phone;
  final bool isSameWithIdentity;
  final Map addressLatLong;
  bool isCorrespondence;
  int active;
  final bool isEditAddress;
  final bool isAlamatChanges;
  final bool isAddressTypeChanges;
  final bool isRTChanges;
  final bool isRWChanges;
  final bool isKelurahanChanges;
  final bool isKecamatanChanges;
  final bool isKotaChanges;
  final bool isProvinsiChanges;
  final bool isPostalCodeChanges;
  final bool isAddressFromMapChanges;
  final bool isTeleponAreaChanges;
  final bool isTeleponChanges;

  AddressModel(
      this.jenisAlamatModel,
      this.addressID,
      this.foreignBusinessID,
      this.numberID,
      this.kelurahanModel,
      this.address,
      this.rt,
      this.rw,
      this.areaCode,
      this.phone,
      this.isSameWithIdentity,
      this.addressLatLong,
      this.isCorrespondence,
      this.active,
      this.isEditAddress,
      this.isAlamatChanges,
      this.isAddressTypeChanges,
      this.isRTChanges,
      this.isRWChanges,
      this.isKelurahanChanges,
      this.isKecamatanChanges,
      this.isKotaChanges,
      this.isProvinsiChanges,
      this.isPostalCodeChanges,
      this.isAddressFromMapChanges,
      this.isTeleponAreaChanges,
      this.isTeleponChanges);
}
