import 'dart:io';

class ProfilModel {
  final String id;
  final String text;

  ProfilModel(this.id, this.text);
}

class TypeNPWPModel {
  final String id;
  final String text;

  TypeNPWPModel(this.id, this.text);
}

class SignPKPModel {
  final String id;
  final String text;

  SignPKPModel(this.id, this.text);
}

// class SectorEconomicModel {
//   final String id;
//   final String desc;
//
//   SectorEconomicModel(this.id, this.desc);
// }
//
// class BusinessFieldModel {
//   final String id;
//   final String desc;
//
//   BusinessFieldModel(this.id, this.desc);
// }

class LocationStatusModel {
  final String id;
  final String text;

  LocationStatusModel(this.id, this.text);
}

// class BusinessLocationModel {
//   final String id;
//   final String text;
//
//   BusinessLocationModel(this.id, this.text);
// }