class ShowMandatoryInfoCollaPropModel {
  final bool isRadioValueIsCollateralSameWithApplicantPropertyVisible;
  final bool isIdentityModelVisible;
  final bool isIdentityNumberVisible;
  final bool isNameOnCollateralVisible;
  final bool isBirthDatePropVisible;
  final bool isBirthPlaceValidWithIdentity1Visible;
  final bool isBirthPlaceValidWithIdentityLOVVisible;
  final bool isCertificateNumberVisible;
  final bool isCertificateTypeSelectedVisible;
  final bool isPropertyTypeSelectedVisible;
  final bool isBuildingAreaVisible;
  final bool isSurfaceAreaVisible;
  final bool isTaksasiPriceVisible;
  final bool isSifatJaminanVisible;
  final bool isBuktiKepemilikanVisible;
  final bool isCertificateReleaseDateVisible;
  final bool isCertificateReleaseYearVisible;
  final bool isNamaPemegangHakVisible;
  final bool isNoSuratUkurVisible;
  final bool isDateOfMeasuringLetterVisible;
  final bool isCertificateOfMeasuringLetterVisible;
  final bool isDPJaminanVisible;
  final bool isPHMaxVisible;
  final bool isJarakFasumPositifVisible;
  final bool isJarakFasumNegatifVisible;
  final bool isHargaTanahVisible;
  final bool isHargaNJOPVisible;
  final bool isHargaBangunanVisible;
  final bool isTypeOfRoofSelectedVisible;
  final bool isWallTypeSelectedVisible;
  final bool isFloorTypeSelectedVisible;
  final bool isFoundationTypeSelectedVisible;
  final bool isStreetTypeSelectedVisible;
  final bool isRadioValueAccessCarVisible;
  final bool isJumlahRumahDalamRadiusVisible;
  final bool isMasaHakBerlakuVisible;
  final bool isNoIMBVisible;
  final bool isDateOfIMBVisible;
  final bool isLuasBangunanIMBVisible;
  final bool isUsageCollateralPropertyVisible;
  final bool isLTVVisible;
  final bool isRadioValueForAllUnitPropertyVisible;
  final bool isAddressTypeSelectedVisible;
  final bool isAddressVisible;
  final bool isRTVisible;
  final bool isRWVisible;
  final bool isKelurahanVisible;
  final bool isKecamatanVisible;
  final bool isKotaVisible;
  final bool isProvVisible;
  final bool isPostalCodeVisible;
  final bool isAddressFromMapVisible;
  final bool isRadioValueIsCollateralSameWithApplicantPropertyMandatory;
  final bool isIdentityModelMandatory;
  final bool isIdentityNumberMandatory;
  final bool isNameOnCollateralMandatory;
  final bool isBirthDatePropMandatory;
  final bool isBirthPlaceValidWithIdentity1Mandatory;
  final bool isBirthPlaceValidWithIdentityLOVMandatory;
  final bool isCertificateNumberMandatory;
  final bool isCertificateTypeSelectedMandatory;
  final bool isPropertyTypeSelectedMandatory;
  final bool isBuildingAreaMandatory;
  final bool isSurfaceAreaMandatory;
  final bool isTaksasiPriceMandatory;
  final bool isSifatJaminanMandatory;
  final bool isBuktiKepemilikanMandatory;
  final bool isCertificateReleaseDateMandatory;
  final bool isCertificateReleaseYearMandatory;
  final bool isNamaPemegangHakMandatory;
  final bool isNoSuratUkurMandatory;
  final bool isDateOfMeasuringLetterMandatory;
  final bool isCertificateOfMeasuringLetterMandatory;
  final bool isDPJaminanMandatory;
  final bool isPHMaxMandatory;
  final bool isJarakFasumPositifMandatory;
  final bool isJarakFasumNegatifMandatory;
  final bool isHargaTanahMandatory;
  final bool isHargaNJOPMandatory;
  final bool isHargaBangunanMandatory;
  final bool isTypeOfRoofSelectedMandatory;
  final bool isWallTypeSelectedMandatory;
  final bool isFloorTypeSelectedMandatory;
  final bool isFoundationTypeSelectedMandatory;
  final bool isStreetTypeSelectedMandatory;
  final bool isRadioValueAccessCarMandatory;
  final bool isJumlahRumahDalamRadiusMandatory;
  final bool isMasaHakBerlakuMandatory;
  final bool isNoIMBMandatory;
  final bool isDateOfIMBMandatory;
  final bool isLuasBangunanIMBMandatory;
  final bool isUsageCollateralPropertyMandatory;
  final bool isLTVMandatory;
  final bool isRadioValueForAllUnitPropertyMandatory;
  final bool isAddressTypeSelectedMandatory;
  final bool isAddressMandatory;
  final bool isRTMandatory;
  final bool isRWMandatory;
  final bool isKelurahanMandatory;
  final bool isKecamatanMandatory;
  final bool isKotaMandatory;
  final bool isProvMandatory;
  final bool isPostalCodeMandatory;
  final bool isAddressFromMapMandatory;

  ShowMandatoryInfoCollaPropModel(
      this.isRadioValueIsCollateralSameWithApplicantPropertyVisible,
      this.isIdentityModelVisible,
      this.isIdentityNumberVisible,
      this.isNameOnCollateralVisible,
      this.isBirthDatePropVisible,
      this.isBirthPlaceValidWithIdentity1Visible,
      this.isBirthPlaceValidWithIdentityLOVVisible,
      this.isCertificateNumberVisible,
      this.isCertificateTypeSelectedVisible,
      this.isPropertyTypeSelectedVisible,
      this.isBuildingAreaVisible,
      this.isSurfaceAreaVisible,
      this.isTaksasiPriceVisible,
      this.isSifatJaminanVisible,
      this.isBuktiKepemilikanVisible,
      this.isCertificateReleaseDateVisible,
      this.isCertificateReleaseYearVisible,
      this.isNamaPemegangHakVisible,
      this.isNoSuratUkurVisible,
      this.isDateOfMeasuringLetterVisible,
      this.isCertificateOfMeasuringLetterVisible,
      this.isDPJaminanVisible,
      this.isPHMaxVisible,
      this.isJarakFasumPositifVisible,
      this.isJarakFasumNegatifVisible,
      this.isHargaTanahVisible,
      this.isHargaNJOPVisible,
      this.isHargaBangunanVisible,
      this.isTypeOfRoofSelectedVisible,
      this.isWallTypeSelectedVisible,
      this.isFloorTypeSelectedVisible,
      this.isFoundationTypeSelectedVisible,
      this.isStreetTypeSelectedVisible,
      this.isRadioValueAccessCarVisible,
      this.isJumlahRumahDalamRadiusVisible,
      this.isMasaHakBerlakuVisible,
      this.isNoIMBVisible,
      this.isDateOfIMBVisible,
      this.isLuasBangunanIMBVisible,
      this.isUsageCollateralPropertyVisible,
      this.isLTVVisible,
      this.isRadioValueForAllUnitPropertyVisible,
      this.isAddressTypeSelectedVisible,
      this.isAddressVisible,
      this.isRTVisible,
      this.isRWVisible,
      this.isKelurahanVisible,
      this.isKecamatanVisible,
      this.isKotaVisible,
      this.isProvVisible,
      this.isPostalCodeVisible,
      this.isAddressFromMapVisible,
      this.isRadioValueIsCollateralSameWithApplicantPropertyMandatory,
      this.isIdentityModelMandatory,
      this.isIdentityNumberMandatory,
      this.isNameOnCollateralMandatory,
      this.isBirthDatePropMandatory,
      this.isBirthPlaceValidWithIdentity1Mandatory,
      this.isBirthPlaceValidWithIdentityLOVMandatory,
      this.isCertificateNumberMandatory,
      this.isCertificateTypeSelectedMandatory,
      this.isPropertyTypeSelectedMandatory,
      this.isBuildingAreaMandatory,
      this.isSurfaceAreaMandatory,
      this.isTaksasiPriceMandatory,
      this.isSifatJaminanMandatory,
      this.isBuktiKepemilikanMandatory,
      this.isCertificateReleaseDateMandatory,
      this.isCertificateReleaseYearMandatory,
      this.isNamaPemegangHakMandatory,
      this.isNoSuratUkurMandatory,
      this.isDateOfMeasuringLetterMandatory,
      this.isCertificateOfMeasuringLetterMandatory,
      this.isDPJaminanMandatory,
      this.isPHMaxMandatory,
      this.isJarakFasumPositifMandatory,
      this.isJarakFasumNegatifMandatory,
      this.isHargaTanahMandatory,
      this.isHargaNJOPMandatory,
      this.isHargaBangunanMandatory,
      this.isTypeOfRoofSelectedMandatory,
      this.isWallTypeSelectedMandatory,
      this.isFloorTypeSelectedMandatory,
      this.isFoundationTypeSelectedMandatory,
      this.isStreetTypeSelectedMandatory,
      this.isRadioValueAccessCarMandatory,
      this.isJumlahRumahDalamRadiusMandatory,
      this.isMasaHakBerlakuMandatory,
      this.isNoIMBMandatory,
      this.isDateOfIMBMandatory,
      this.isLuasBangunanIMBMandatory,
      this.isUsageCollateralPropertyMandatory,
      this.isLTVMandatory,
      this.isRadioValueForAllUnitPropertyMandatory,
      this.isAddressTypeSelectedMandatory,
      this.isAddressMandatory,
      this.isRTMandatory,
      this.isRWMandatory,
      this.isKelurahanMandatory,
      this.isKecamatanMandatory,
      this.isKotaMandatory,
      this.isProvMandatory,
      this.isPostalCodeMandatory,
      this.isAddressFromMapMandatory);
}