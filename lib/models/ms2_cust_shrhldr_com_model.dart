import 'package:ad1ms2_dev/models/form_m_company_rincian_model.dart';
import 'package:ad1ms2_dev/shared/constants.dart';

class MS2CustShrhldrComModel {
  final String shrhldr_comp_id;
  final String shareholdersCorporateID;
  final TypeInstitutionModel typeInstitutionModel;
  final String profil;
  final String profil_desc;
  final String comp_name;
  final String akta_no;
  final String akta_expire_date;
  final String siup_no;
  final String siup_start_date;
  final String tdp_no;
  final String tdp_start_date;
  final String establish_date;
  final int flag_comp;
  final int basic_capital;
  final int paid_capital;
  final String npwp_address;
  final String npwp_name;
  final String npwp_no;
  final String npwp_type;
  final String npwp_type_desc;
  final int flag_npwp;
  // final SignPKPModel pkp_flag;
  final String sector_economic;
  final String sector_economic_desc;
  final String nature_of_buss;
  final String nature_of_buss_desc;
  final String location_status;
  final String location_status_desc;
  final String buss_location;
  final String buss_location_desc;
  final int total_emp;
  final int no_of_year_work;
  final int no_of_year_buss;
  final String ac_level;
  final int shrldng_percent;
  final int dedup_score;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final int active;
  final String edit_comp_type;
  final String edit_comp_name;
  final String edit_establish_date;
  final String edit_npwp_no;
  final String edit_shrldng_percent;

  MS2CustShrhldrComModel(
      this.shrhldr_comp_id,
      this.shareholdersCorporateID,
      this.typeInstitutionModel,
      this.profil,
      this.profil_desc,
      this.comp_name,
      this.akta_no,
      this.akta_expire_date,
      this.siup_no,
      this.siup_start_date,
      this.tdp_no,
      this.tdp_start_date,
      this.establish_date,
      this.flag_comp,
      this.basic_capital,
      this.paid_capital,
      this.npwp_address,
      this.npwp_name,
      this.npwp_no,
      this.npwp_type,
      this.npwp_type_desc,
      this.flag_npwp,
      // this.pkp_flag,
      this.sector_economic,
      this.sector_economic_desc,
      this.nature_of_buss,
      this.nature_of_buss_desc,
      this.location_status,
      this.location_status_desc,
      this.buss_location,
      this.buss_location_desc,
      this.total_emp,
      this.no_of_year_work,
      this.no_of_year_buss,
      this.ac_level,
      this.shrldng_percent,
      this.dedup_score,
      this.created_date,
      this.created_by,
      this.modified_date,
      this.modified_by,
      this.active, this.edit_comp_type, this.edit_comp_name, this.edit_establish_date, this.edit_npwp_no, this.edit_shrldng_percent,
      );
}
