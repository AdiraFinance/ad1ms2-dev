class ShowMandatoryInfoStrukturKreditGPModel {
  final bool isGpTypeSelectedVisible;
  final bool isInstallmentSelectedGPVisible;
  final bool isNewTenorVisible;
  final bool isGpTypeSelectedMandatory;
  final bool isInstallmentSelectedGPMandatory;
  final bool isNewTenorMandatory;

  ShowMandatoryInfoStrukturKreditGPModel(
    this.isGpTypeSelectedVisible,
    this.isInstallmentSelectedGPVisible,
    this.isNewTenorVisible,
    this.isGpTypeSelectedMandatory,
    this.isInstallmentSelectedGPMandatory,
    this.isNewTenorMandatory);
}