import 'package:ad1ms2_dev/models/info_document_detail_model.dart';
import 'package:ad1ms2_dev/models/info_document_type_model.dart';

class InfoDocumentModel{
    final InfoDocumentTypeModel documentType;
    final String orderSupportingDocumentID;
    final InfoDocumentDetailModel documentDetail;
    final String date;
    final String path;
    final double latitude;
    final double longitude;
    final String symbolName;
    final String fileHeaderID;
    final String fileName;

    InfoDocumentModel(this.documentType, this.orderSupportingDocumentID, this.documentDetail, this.date, this.path, this.latitude, this.longitude, this.symbolName,this.fileHeaderID, this.fileName);
}