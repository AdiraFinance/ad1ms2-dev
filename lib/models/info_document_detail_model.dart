import 'dart:io';

class InfoDocumentDetailModel{
    final String fileName;
    final File file;
    final String path;

    InfoDocumentDetailModel(this.fileName, this.file, this.path);
}