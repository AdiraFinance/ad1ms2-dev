class ShowMandatoryInfoSubsidiModel {
  final bool isRadioValueGiverVisible;
  final bool isTypeSelectedVisible;
  final bool isCuttingMethodSelectedVisible;
  final bool isValueVisible;
  final bool isClaimValueVisible;
  final bool isRateBeforeEffVisible;
  final bool isRateBeforeFlatVisible;
  final bool isTotalInstallmentVisible;
  final bool isInstallmentIndexSelectedVisible;
  final bool isRadioValueGiverMandatory;
  final bool isTypeSelectedMandatory;
  final bool isCuttingMethodSelectedMandatory;
  final bool isValueMandatory;
  final bool isClaimValueMandatory;
  final bool isRateBeforeEffMandatory;
  final bool isRateBeforeFlatMandatory;
  final bool isTotalInstallmentMandatory;
  final bool isInstallmentIndexSelectedMandatory;

  ShowMandatoryInfoSubsidiModel(
      this.isRadioValueGiverVisible,
      this.isTypeSelectedVisible,
      this.isCuttingMethodSelectedVisible,
      this.isValueVisible,
      this.isClaimValueVisible,
      this.isRateBeforeEffVisible,
      this.isRateBeforeFlatVisible,
      this.isTotalInstallmentVisible,
      this.isInstallmentIndexSelectedVisible,
      this.isRadioValueGiverMandatory,
      this.isTypeSelectedMandatory,
      this.isCuttingMethodSelectedMandatory,
      this.isValueMandatory,
      this.isClaimValueMandatory,
      this.isRateBeforeEffMandatory,
      this.isRateBeforeFlatMandatory,
      this.isTotalInstallmentMandatory,
      this.isInstallmentIndexSelectedMandatory);
}