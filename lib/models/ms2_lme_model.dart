class MS2LMEModel {
  final String order_no;
  final String oid;
  final String lme_id;
  final String appl_no;
  final String flag_eligible;
  final String disburse_type;
  final String product_id;
  final String product_id_desc;
  final String installment_amount;
  final String ph_amount;
  final String voucher_code;
  final String tenor;
  final String created_date;
  final String created_by;
  final String modified_date;
  final String modified_by;
  final int active;
  final String grading;
  final String jenis_penawaran;
  final String jenis_penawaran_desc;
  final String pencairan_ke;
  final String opsi_multidisburse;
  final String max_ph;
  final String max_tenor;
  final String no_referensi;
  final String max_installment;
  final String portfolio;
  final String plafond_mrp_risk;
  final String remaining_tenor;
  final String activation_date;
  final String cust_name;
  final String cust_address;
  final String cust_type;
  final String notes;

  MS2LMEModel(
    this.order_no,
    this.oid,
    this.lme_id,
    this.appl_no,
    this.flag_eligible,
    this.disburse_type,
    this.product_id,
    this.product_id_desc,
    this.installment_amount,
    this.ph_amount,
    this.voucher_code,
    this.tenor,
    this.created_date,
    this.created_by,
    this.modified_date,
    this.modified_by,
    this.active,
    this.grading,
    this.jenis_penawaran,
    this.jenis_penawaran_desc,
    this.pencairan_ke,
    this.opsi_multidisburse,
    this.max_ph,
    this.max_tenor,
    this.no_referensi,
    this.max_installment,
    this.portfolio,
    this.plafond_mrp_risk,
    this.remaining_tenor,
    this.activation_date,
    this.cust_name,
    this.cust_address,
    this.cust_type,
    this.notes
  );
}
