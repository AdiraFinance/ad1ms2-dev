class ShowMandatoryIncomeModel {
  final bool isIncomePerbulanWiraswastaShow;
  final bool isIncomeLainnyaWiraswastaShow;
  final bool isTotalIncomeWiraswastaShow;
  final bool isPokokIncomeWiraswastaShow;
  final bool isLabaKotorWiraswastaShow;
  final bool isBiayaOperasionalWiraswastaShow;
  final bool isBiayaLainnyaWiraswastaShow;
  final bool isNetSebelumPajakWiraswastaShow;
  final bool isPajakWiraswastaShow;
  final bool isNetSetelahPajakWiraswastaShow;
  final bool isBiayaHidupWiraswastaShow;
  final bool isSisaIncomeWiraswastaShow;
  final bool isIncomePasanganWiraswastaShow;
  final bool isAngsuranLainnyaWiraswastaShow;
  final bool isIncomeProfesionalShow;
  final bool isIncomePasanganProfesionalShow;
  final bool isIncomeLainnyaProfesionalShow;
  final bool isTotalIncomeProfesionalShow;
  final bool isBiayaHidupProfesionalShow;
  final bool isSisaIncomeProfesionalShow;
  final bool isAngsuranLainnyaProfesionalShow;
  final bool isIncomePerbulanWiraswastaMandatory;
  final bool isIncomeLainnyaWiraswastaMandatory;
  final bool isTotalIncomeWiraswastaMandatory;
  final bool isPokokIncomeWiraswastaMandatory;
  final bool isLabaKotorWiraswastaMandatory;
  final bool isBiayaOperasionalWiraswastaMandatory;
  final bool isBiayaLainnyaWiraswastaMandatory;
  final bool isNetSebelumPajakWiraswastaMandatory;
  final bool isPajakWiraswastaMandatory;
  final bool isNetSetelahPajakWiraswastaMandatory;
  final bool isBiayaHidupWiraswastaMandatory;
  final bool isSisaIncomeWiraswastaMandatory;
  final bool isIncomePasanganWiraswastaMandatory;
  final bool isAngsuranLainnyaWiraswastaMandatory;
  final bool isIncomeProfesionalMandatory;
  final bool isIncomePasanganProfesionalMandatory;
  final bool isIncomeLainnyaProfesionalMandatory;
  final bool isTotalIncomeProfesionalMandatory;
  final bool isBiayaHidupProfesionalMandatory;
  final bool isSisaIncomeProfesionalMandatory;
  final bool isAngsuranLainnyaProfesionalMandatory;

  ShowMandatoryIncomeModel(
      this.isIncomePerbulanWiraswastaShow,
      this.isIncomeLainnyaWiraswastaShow,
      this.isTotalIncomeWiraswastaShow,
      this.isPokokIncomeWiraswastaShow,
      this.isLabaKotorWiraswastaShow,
      this.isBiayaOperasionalWiraswastaShow,
      this.isBiayaLainnyaWiraswastaShow,
      this.isNetSebelumPajakWiraswastaShow,
      this.isPajakWiraswastaShow,
      this.isNetSetelahPajakWiraswastaShow,
      this.isBiayaHidupWiraswastaShow,
      this.isSisaIncomeWiraswastaShow,
      this.isIncomePasanganWiraswastaShow,
      this.isAngsuranLainnyaWiraswastaShow,
      this.isIncomeProfesionalShow,
      this.isIncomePasanganProfesionalShow,
      this.isIncomeLainnyaProfesionalShow,
      this.isTotalIncomeProfesionalShow,
      this.isBiayaHidupProfesionalShow,
      this.isSisaIncomeProfesionalShow,
      this.isAngsuranLainnyaProfesionalShow,
      this.isIncomePerbulanWiraswastaMandatory,
      this.isIncomeLainnyaWiraswastaMandatory,
      this.isTotalIncomeWiraswastaMandatory,
      this.isPokokIncomeWiraswastaMandatory,
      this.isLabaKotorWiraswastaMandatory,
      this.isBiayaOperasionalWiraswastaMandatory,
      this.isBiayaLainnyaWiraswastaMandatory,
      this.isNetSebelumPajakWiraswastaMandatory,
      this.isPajakWiraswastaMandatory,
      this.isNetSetelahPajakWiraswastaMandatory,
      this.isBiayaHidupWiraswastaMandatory,
      this.isSisaIncomeWiraswastaMandatory,
      this.isIncomePasanganWiraswastaMandatory,
      this.isAngsuranLainnyaWiraswastaMandatory,
      this.isIncomeProfesionalMandatory,
      this.isIncomePasanganProfesionalMandatory,
      this.isIncomeLainnyaProfesionalMandatory,
      this.isTotalIncomeProfesionalMandatory,
      this.isBiayaHidupProfesionalMandatory,
      this.isSisaIncomeProfesionalMandatory,
      this.isAngsuranLainnyaProfesionalMandatory);
}
